::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	Script for setting paths
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Checking if project path is set
::----------------------------------------------------------
IF NOT DEFINED PROJECT_DIR GOTO ProjectPathNotDefined

::----------------------------------------------------------
:: Check arguments
::----------------------------------------------------------
IF "%1"=="skip-checking-arch" GOTO SkipCheckingArchitecture

::----------------------------------------------------------
:: Check architecture
::----------------------------------------------------------
IF NOT DEFINED MACHINE_PRODUCENT GOTO ArchitectureNotSelected
IF NOT DEFINED MACHINE_FAMILY GOTO ArchitectureNotSelected
IF NOT DEFINED MACHINE_NAME GOTO ArchitectureNotSelected

:SkipCheckingArchitecture
::----------------------------------------------------------
:: Basic variables
::----------------------------------------------------------
SET PROJECT_NAME=ChocoOS

::////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
::
::		DIRECTORIES
::
::////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

::----------------------------------------------------------
:: Basic directories
::----------------------------------------------------------
SET OUTPUT_DIR=%PROJECT_DIR%/Output
SET SOURCE_DIR=%PROJECT_DIR%/Source
SET TEST_DIR=%PROJECT_DIR%/Tests
SET TOOLS_DIR=%PROJECT_DIR%/Tools
SET RESOURCES_DIR=%PROJECT_DIR%/Resources
SET SYSTEM_DIR=%SOURCE_DIR%/system
SET USER_DIR=%SOURCE_DIR%/user
SET TEMPLATES_DIR=%RESOURCES_DIR%/templates/files
SET PROGRAM_TEMPLATES_DIR=%TEMPLATES_DIR%/program
SET FLASHFS_TEMPLATES_DIR=%TEMPLATES_DIR%/flashfs

::----------------------------------------------------------
:: System directories
::----------------------------------------------------------
SET SYSTEM_CONFIG_DIR=%SYSTEM_DIR%/config
SET SYSTEM_CORE_DIR=%SYSTEM_DIR%/core
SET SYSTEM_PORTABLE_DIR=%SYSTEM_DIR%/portable
SET SYSTEM_USER_DIR=%SYSTEM_DIR%/user
SET SYSTEM_LIBRARIES_DIR=%SYSTEM_DIR%/libraries
SET SYSTEM_PROGRAMS_DIR=%SYSTEM_DIR%/programs
SET SYSTEM_PROGRAM_DIR=%SYSTEM_PROGRAMS_DIR%/%PROGRAM_DIR_NAME%
SET SYSTEM_FLASH_DIR=%RESOURCES_DIR%/flash

::----------------------------------------------------------
:: User directories
::----------------------------------------------------------
SET USER_LIBRARIES_DIR=%USER_DIR%/libraries
SET USER_PROGRAMS_DIR=%USER_DIR%/programs
SET USER_PROGRAM_DIR=%USER_PROGRAMS_DIR%/%PROGRAM_DIR_NAME%

::----------------------------------------------------------
:: Configuration directories
::----------------------------------------------------------
SET SYSTEM_CONFIG_MACHINE_DIR=%SYSTEM_CONFIG_DIR%/%MACHINE_NAME%

::----------------------------------------------------------
:: System core directories
::----------------------------------------------------------
SET SYSTEM_CORE_INCLUDES_DIR= %SYSTEM_CORE_DIR%/inc
SET SYSTEM_CORE_INCLUDES_DRIVERS_DIR= %SYSTEM_CORE_INCLUDES_DIR%/drivers
SET SYSTEM_CORE_INCLUDES_KERNEL_DIR= %SYSTEM_CORE_INCLUDES_DIR%/kernel
SET SYSTEM_CORE_INCLUDES_BOOT_DIR= %SYSTEM_CORE_INCLUDES_DIR%/boot
SET SYSTEM_CORE_INCLUDES_TERMINALS_DIR= %SYSTEM_CORE_INCLUDES_DIR%/terminals
SET SYSTEM_CORE_INCLUDES_FS_DIR= %SYSTEM_CORE_INCLUDES_DIR%/fs
SET SYSTEM_CORE_INCLUDES_SCALLS_DIR= %SYSTEM_CORE_INCLUDES_DIR%/scalls
SET SYSTEM_CORE_INCLUDES_POSIX_DIR= %SYSTEM_CORE_INCLUDES_DIR%/posix
SET SYSTEM_CORE_INCLUDES_GUI_DIR= %SYSTEM_CORE_INCLUDES_DIR%/gui
SET SYSTEM_CORE_INCLUDES_FLASHFS_DIR=%SYSTEM_CORE_INCLUDES_FS_DIR%/flashfs
SET SYSTEM_CORE_INCLUDES_NET_DIR=%SYSTEM_CORE_INCLUDES_DIR%/net
SET SYSTEM_CORE_INCLUDES_PROTOCOLS_DIR=%SYSTEM_CORE_INCLUDES_NET_DIR%/protocols
SET SYSTEM_CORE_SOURCES_DIR= %SYSTEM_CORE_DIR%/src
SET SYSTEM_CORE_SOURCES_DRIVERS_DIR= %SYSTEM_CORE_SOURCES_DIR%/drivers
SET SYSTEM_CORE_SOURCES_KERNEL_DIR= %SYSTEM_CORE_SOURCES_DIR%/kernel
SET SYSTEM_CORE_SOURCES_BOOT_DIR= %SYSTEM_CORE_SOURCES_DIR%/boot
SET SYSTEM_CORE_SOURCES_TERMINALS_DIR= %SYSTEM_CORE_SOURCES_DIR%/terminals
SET SYSTEM_CORE_SOURCES_FS_DIR= %SYSTEM_CORE_SOURCES_DIR%/fs
SET SYSTEM_CORE_SOURCES_SCALLS_DIR= %SYSTEM_CORE_SOURCES_DIR%/scalls
SET SYSTEM_CORE_SOURCES_POSIX_DIR= %SYSTEM_CORE_SOURCES_DIR%/posix
SET SYSTEM_CORE_SOURCES_GUI_DIR= %SYSTEM_CORE_SOURCES_DIR%/gui
SET SYSTEM_CORE_SOURCES_FLASHFS_DIR=%SYSTEM_CORE_SOURCES_FS_DIR%/flashfs
SET SYSTEM_CORE_SOURCES_NET_DIR=%SYSTEM_CORE_SOURCES_DIR%/net
SET SYSTEM_CORE_SOURCES_PROTOCOLS_DIR=%SYSTEM_CORE_SOURCES_NET_DIR%/protocols

::----------------------------------------------------------
:: System libraries directories
::----------------------------------------------------------
SET SYSTEM_LIBRARIES_INCLUDES_DIR=%SYSTEM_LIBRARIES_DIR%/inc
SET SYSTEM_LIBRARIES_SOURCES_DIR=%SYSTEM_LIBRARIES_DIR%/src

::----------------------------------------------------------
:: System portable directories
::----------------------------------------------------------
SET SYSTEM_PORTABLE_INCLUDES_DIR=%SYSTEM_PORTABLE_DIR%/inc
SET SYSTEM_PORTABLE_INCLUDES_PRODUCENT_DIR=%SYSTEM_PORTABLE_INCLUDES_DIR%/%MACHINE_PRODUCENT%
SET SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR=%SYSTEM_PORTABLE_INCLUDES_PRODUCENT_DIR%/%MACHINE_FAMILY%
SET SYSTEM_PORTABLE_INCLUDES_MACHINE_DIR=%SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR%/%MACHINE_NAME%
SET SYSTEM_PORTABLE_INCLUDES_LLD_DIR=%SYSTEM_PORTABLE_INCLUDES_DIR%/lld
SET SYSTEM_PORTABLE_INCLUDES_MSLLD_DIR=%SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR%/mslld
SET SYSTEM_PORTABLE_INCLUDES_MCS_DIR=%SYSTEM_PORTABLE_INCLUDES_DIR%/mcs
SET SYSTEM_PORTABLE_INCLUDES_MCS_CORE_PRODUCENT_DIR=%SYSTEM_PORTABLE_INCLUDES_MCS_DIR%/%MACHINE_ARCHITECTURE%
SET SYSTEM_PORTABLE_INCLUDES_MCS_CORE_DIR=%SYSTEM_PORTABLE_INCLUDES_MCS_CORE_PRODUCENT_DIR%/%MACHINE_CORE_ARCHITECTURE_FAMILY%
SET SYSTEM_PORTABLE_SCRIPTS_DIR=%SYSTEM_PORTABLE_DIR%/scripts
SET SYSTEM_PORTABLE_SCRIPTS_PRODUCENT_DIR=%SYSTEM_PORTABLE_SCRIPTS_DIR%/%MACHINE_PRODUCENT%
SET SYSTEM_PORTABLE_SCRIPTS_FAMILY_DIR=%SYSTEM_PORTABLE_SCRIPTS_PRODUCENT_DIR%/%MACHINE_FAMILY%
SET SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR=%SYSTEM_PORTABLE_SCRIPTS_FAMILY_DIR%/%MACHINE_NAME%
SET SYSTEM_PORTABLE_SOURCES_DIR=%SYSTEM_PORTABLE_DIR%/src
SET SYSTEM_PORTABLE_SOURCES_PRODUCENT_DIR=%SYSTEM_PORTABLE_SOURCES_DIR%/%MACHINE_PRODUCENT%
SET SYSTEM_PORTABLE_SOURCES_FAMILY_DIR=%SYSTEM_PORTABLE_SOURCES_PRODUCENT_DIR%/%MACHINE_FAMILY%
SET SYSTEM_PORTABLE_SOURCES_MACHINE_DIR=%SYSTEM_PORTABLE_SOURCES_FAMILY_DIR%/%MACHINE_NAME%
SET SYSTEM_PORTABLE_SOURCES_LLD_DIR=%SYSTEM_PORTABLE_SOURCES_FAMILY_DIR%/lld
SET SYSTEM_PORTABLE_SOURCES_MSLLD_DIR=%SYSTEM_PORTABLE_SOURCES_FAMILY_DIR%/mslld
SET SYSTEM_PORTABLE_SOURCES_MCS_DIR=%SYSTEM_PORTABLE_SOURCES_DIR%/mcs
SET SYSTEM_PORTABLE_SOURCES_MCS_CORE_PRODUCENT_DIR=%SYSTEM_PORTABLE_SOURCES_MCS_DIR%/%MACHINE_ARCHITECTURE%
SET SYSTEM_PORTABLE_SOURCES_MCS_CORE_DIR=%SYSTEM_PORTABLE_SOURCES_MCS_CORE_PRODUCENT_DIR%/%MACHINE_CORE_ARCHITECTURE_FAMILY%

::----------------------------------------------------------
:: System user directories
::----------------------------------------------------------
SET SYSTEM_USER_INCLUDES_DIR=%SYSTEM_USER_DIR%/inc
SET SYSTEM_USER_SOURCES_DIR=%SYSTEM_USER_DIR%/src

::----------------------------------------------------------
:: User directories
::----------------------------------------------------------
SET USER_LIBRARIES_DIR=%USER_DIR%/libraries
SET USER_PROGRAM_DIR=%USER_DIR%/programs
SET USER_INCLUDES_DIR=%SYSTEM_USER_DIR%/inc
SET USER_SOURCES_DIR=%SYSTEM_USER_DIR%/src

::----------------------------------------------------------
:: Machine directories
::----------------------------------------------------------
SET MACHINE_SCRIPTS_DIR=%SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR%
SET MACHINE_OUTPUT_DIR=%OUTPUT_DIR%/%MACHINE_NAME%

::----------------------------------------------------------
:: Tools directories
::----------------------------------------------------------
SET OPENOCD_0_8_0_DIR=%TOOLS_DIR%/openocd-0-8-0
SET OPENOCD_0_8_0_BIN_DIR=%OPENOCD_0_8_0_DIR%/bin
SET OPENOCD_0_8_0_SCRIPTS_DIR=%OPENOCD_0_8_0_DIR%/scripts
SET OPENOCD_DIR=%TOOLS_DIR%/openocd
SET OPENOCD_BIN_DIR=%OPENOCD_DIR%/bin
SET OPENOCD_SCRIPTS_DIR=%OPENOCD_DIR%/share/openocd/scripts
SET COMPILERS_DIR=%TOOLS_DIR%/compilers
SET ARM_NONE_EABI_GCC_DIR=%COMPILERS_DIR%/arm_none_eabi_gcc
SET ARM_NONE_EABI_GCC_BIN_DIR=%ARM_NONE_EABI_GCC_DIR%/bin
SET ARM_NONE_EABI_DIR=%ARM_NONE_EABI_GCC_DIR%/arm_none_eabi
SET ARM_NONE_EABI_BIN_DIR=%ARM_NONE_EABI_DIR%/bin
SET ARM_NONE_EABI_INCLUDE_DIR=%ARM_NONE_EABI_DIR%/include
SET MAKE32_DIR=%COMPILERS_DIR%/make32
SET MAKE64_DIR=%COMPILERS_DIR%/make64
SET DRIVERGEN_DIR=%TOOLS_DIR%

::////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
::
::		FILES NAMES
::
::////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

::----------------------------------------------------------
:: Makefiles files names
::----------------------------------------------------------
SET LIBRARIES_SPACE_MK_FILE_NAME=libraries.mak
SET PORTABLE_SPACE_MK_FILE_NAME=portable.mak
SET CORE_SPACE_MK_FILE_NAME=core.mak
SET USER_SPACE_MK_FILE_NAME=user.mak
SET PROGRAM_DEFS_MK_FILE_NAME=Makefile.mak
SET PORTABLE_DEFS_MK_FILE_NAME=definitions.mak
SET GLOBAL_DEFS_MK_FILE_NAME=global_defs.mak
SET PROGRAM_MK_FILE_NAME=program.mak
SET LINKAGE_MK_FILE_NAME=linkage.mak
SET PROGRAM_LD_FILE_NAME=program.ld
SET PROGRAM_ELF_LD_FILE_NAME=program-elf.ld

::----------------------------------------------------------
:: Dependences files names
::----------------------------------------------------------
SET LIBRARIES_SPACE_DEP_FILE_NAME=libraries_dep.d
SET PORTABLE_SPACE_DEP_FILE_NAME=portable_dep.d
SET CORE_SPACE_DEP_FILE_NAME=core_dep.d
SET USER_SPACE_DEP_FILE_NAME=user_dep.d
SET PROGRAM_DEP_FILE_NAME=lib%PROGRAM_DIR_NAME%.a

::----------------------------------------------------------
:: Libraries files names
::----------------------------------------------------------
SET LIBRARIES_SPACE_LIB_FILE_NAME=liblibrariesspace.a
SET PORTABLE_SPACE_LIB_FILE_NAME=libportablespace.a
SET CORE_SPACE_LIB_FILE_NAME=libcorespace.a
SET USER_SPACE_LIB_FILE_NAME=libuserspace.dll
SET PROGRAM_LIB_FILE_NAME=lib%PROGRAM_DIR_NAME%.a

::----------------------------------------------------------
:: Output files names
::----------------------------------------------------------
SET PROJECT_MAP_FILE_NAME=%PROJECT_NAME%.map
SET PROJECT_ELF_FILE_NAME=%PROJECT_NAME%.elf
SET PROJECT_HEX_FILE_NAME=%PROJECT_NAME%.hex
SET PROJECT_BIN_FILE_NAME=%PROJECT_NAME%.bin
SET PROJECT_DMP_FILE_NAME=%PROJECT_NAME%.dmp
SET PROJECT_LST_FILE_NAME=%PROJECT_NAME%.lst
SET PROJECT_SIZE_FILE_NAME=%PROJECT_NAME%.size
SET PROGRAM_ELF_FILE_NAME=%PROGRAM_DIR_NAME%.elf
SET PROGRAM_BIN_FILE_NAME=%PROGRAM_DIR_NAME%.cbin
SET PROGRAM_LST_FILE_NAME=%PROGRAM_DIR_NAME%.lst

::----------------------------------------------------------
:: Machine files names
::----------------------------------------------------------
SET MACHINE_CONNECT_FILE_NAME=connect.bat
SET MACHINE_FLASH_FILE_NAME=flash.bat
SET MACHINE_ARCH_CFG_FILE_NAME=arch_cfg.bat
SET MACHINE_GDB_FILE_NAME=gdb_flash.gdb
SET MACHINE_STARTUP_FILE_NAME=startup.s
SET MACHINE_MEMORY_LD_FILE_NAME=memory_cfg.ld
SET MACHINE_COMMON_LD_FILE_NAME=common.ld

::----------------------------------------------------------
:: Special headers names
::----------------------------------------------------------
SET MACHINE_TYPES_HEADER_FILE_NAME=oc_machine_types.h
SET MACHINE_DEFS_HEADER_FILE_NAME=oc_machine_defs.h
SET MACHINE_LIST_HEADER_FILE_NAME=oc_machine_list.h

::----------------------------------------------------------
:: Scripts files names
::----------------------------------------------------------
SET FLASH_FILE_NAME=flash.bat
SET CONNECT_FILE_NAME=connect.bat
SET SELECT_ARCH_FILE_NAME=select_arch.bat
SET PREPARE_FLASHFS_FILE_NAME=pflashfs.bat

::----------------------------------------------------------
:: Tools files names
::----------------------------------------------------------
SET DRIVERGEN_FILE_NAME=drivergen.exe
SET TMPLPARS_FILE_NAME=tmplpars.exe

::////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
::
::		FILES PATHS
::
::////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


::----------------------------------------------------------
:: Makefiles files paths
::----------------------------------------------------------
SET LIBRARIES_SPACE_MK_FILE_PATH=%SYSTEM_LIBRARIES_DIR%/%LIBRARIES_SPACE_MK_FILE_NAME%
SET PORTABLE_SPACE_MK_FILE_PATH=%SYSTEM_PORTABLE_DIR%/%PORTABLE_SPACE_MK_FILE_NAME%
SET CORE_SPACE_MK_FILE_PATH=%SYSTEM_CORE_DIR%/%CORE_SPACE_MK_FILE_NAME%
SET USER_SPACE_MK_FILE_PATH=%SYSTEM_USER_DIR%/%USER_SPACE_MK_FILE_NAME%
SET SYSTEM_PROGRAM_DEFS_MK_FILE_PATH=%SYSTEM_PROGRAM_DIR%/%PROGRAM_DEFS_MK_FILE_NAME%
SET USER_PROGRAM_DEFS_MK_FILE_PATH=%USER_PROGRAM_DIR%/%PROGRAM_DEFS_MK_FILE_NAME%
SET PORTABLE_DEFS_MK_FILE_PATH=%SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR%/%PORTABLE_DEFS_MK_FILE_NAME%
SET GLOBAL_DEFS_MK_FILE_PATH=%PROJECT_DIR%/%GLOBAL_DEFS_MK_FILE_NAME%
SET PROGRAM_MK_FILE_PATH=%PROJECT_DIR%/%PROGRAM_MK_FILE_NAME%
SET LINKAGE_MK_FILE_PATH=%SOURCE_DIR%/%LINKAGE_MK_FILE_NAME%
SET PROGRAM_LD_FILE_PATH=%PROJECT_DIR%/%PROGRAM_LD_FILE_NAME%
SET PROGRAM_ELF_LD_FILE_PATH=%PROJECT_DIR%/%PROGRAM_ELF_LD_FILE_NAME%

::----------------------------------------------------------
:: Dependences files paths
::----------------------------------------------------------
SET LIBRARIES_SPACE_DEP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%LIBRARIES_SPACE_DEP_FILE_NAME%
SET PORTABLE_SPACE_DEP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PORTABLE_SPACE_DEP_FILE_NAME%
SET CORE_SPACE_DEP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%CORE_SPACE_DEP_FILE_NAME%
SET USER_SPACE_DEP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%USER_SPACE_DEP_FILE_NAME%
SET PROGRAM_DEP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROGRAM_DEP_FILE_NAME%

::----------------------------------------------------------
:: Libraries files paths
::----------------------------------------------------------
SET LIBRARIES_SPACE_LIB_FILE_PATH=%MACHINE_OUTPUT_DIR%/%LIBRARIES_SPACE_LIB_FILE_NAME%
SET PORTABLE_SPACE_LIB_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PORTABLE_SPACE_LIB_FILE_NAME%
SET CORE_SPACE_LIB_FILE_PATH=%MACHINE_OUTPUT_DIR%/%CORE_SPACE_LIB_FILE_NAME%
SET USER_SPACE_LIB_FILE_PATH=%MACHINE_OUTPUT_DIR%/%USER_SPACE_LIB_FILE_NAME%
SET PROGRAM_LIB_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROGRAM_LIB_FILE_NAME%

::----------------------------------------------------------
:: Output files paths
::----------------------------------------------------------
SET PROJECT_MAP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_MAP_FILE_NAME%
SET PROJECT_ELF_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_ELF_FILE_NAME%
SET PROJECT_HEX_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_HEX_FILE_NAME%
SET PROJECT_BIN_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_BIN_FILE_NAME%
SET PROJECT_DMP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_DMP_FILE_NAME%
SET PROJECT_LST_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_LST_FILE_NAME%
SET PROJECT_SIZE_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_SIZE_FILE_NAME%

::----------------------------------------------------------
:: Output files paths
::----------------------------------------------------------
SET PROJECT_MAP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_MAP_FILE_NAME%
SET PROJECT_ELF_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_ELF_FILE_NAME%
SET PROJECT_HEX_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_HEX_FILE_NAME%
SET PROJECT_BIN_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_BIN_FILE_NAME%
SET PROJECT_DMP_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_DMP_FILE_NAME%
SET PROJECT_LST_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_LST_FILE_NAME%
SET PROJECT_SIZE_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROJECT_SIZE_FILE_NAME%
SET PROGRAM_ELF_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROGRAM_ELF_FILE_NAME%
SET PROGRAM_BIN_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROGRAM_BIN_FILE_NAME%
SET PROGRAM_LST_FILE_PATH=%MACHINE_OUTPUT_DIR%/%PROGRAM_LST_FILE_NAME%

::----------------------------------------------------------
:: Machine files paths
::----------------------------------------------------------
SET MACHINE_CONNECT_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_CONNECT_FILE_NAME%
SET MACHINE_FLASH_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_FLASH_FILE_NAME%
SET MACHINE_ARCH_CFG_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_ARCH_CFG_FILE_NAME%
SET MACHINE_GDB_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_GDB_FILE_NAME%
SET MACHINE_STARTUP_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_STARTUP_FILE_NAME%
SET MACHINE_MEMORY_LD_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_MEMORY_LD_FILE_NAME%
SET MACHINE_COMMON_LD_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_COMMON_LD_FILE_NAME%

::----------------------------------------------------------
:: Scripts files paths
::----------------------------------------------------------
SET FLASH_FILE_PATH=%SOURCE_DIR%/flash.bat
SET CONNECT_FILE_PATH=%SOURCE_DIR%/connect.bat
SET SELECT_ARCH_FILE_PATH=%SOURCE_DIR%/%SELECT_ARCH_FILE_NAME%
SET PREPARE_FLASHFS_FILE_PATH=%SOURCE_DIR%/%PREPARE_FLASHFS_FILE_NAME%

::----------------------------------------------------------
:: Tools files paths
::----------------------------------------------------------
SET DRIVERGEN_FILE_PATH=%DRIVERGEN_DIR%/%DRIVERGEN_FILE_NAME%
SET TMPLPARS_FILE_PATH=%TOOLS_DIR%/%TMPLPARS_FILE_NAME%

::----------------------------------------------------------
:: Machine files paths
::----------------------------------------------------------
SET MACHINE_CONNECT_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_CONNECT_FILE_NAME%
SET MACHINE_FLASH_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_FLASH_FILE_NAME%
SET MACHINE_GDB_FILE_PATH=%MACHINE_SCRIPTS_DIR%/%MACHINE_GDB_FILE_NAME%
SET MACHINE_TYPES_HEADER_FILE_PATH=%SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR%/%MACHINE_TYPES_HEADER_FILE_NAME%
SET MACHINE_DEFS_HEADER_FILE_PATH=%SYSTEM_PORTABLE_INCLUDES_MACHINE_DIR%/%MACHINE_DEFS_HEADER_FILE_NAME%
SET MACHINE_LIST_HEADER_FILE_NAME=%SYSTEM_PORTABLE_INCLUDES_DIR%/%MACHINE_LIST_HEADER_FILE_NAME%

GOTO EOF

:ArchitectureNotSelected
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target architecture is not selected
echo = 			run select_arch.bat first
echo =
echo ==========================================================================
GOTO EOF

:ProjectPathNotDefined
echo ==========================================================================
echo =                                                                        
echo =      ERROR                                                            
echo =         		Project path is not defined! 
echo = 			    Set PROJECT_PATH variable first!
echo = 					
echo ==========================================================================

:EOF
