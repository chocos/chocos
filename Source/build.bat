::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/06/01
::	Description:	Build script for the system
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Default values
::----------------------------------------------------------
SET BUILD_SPACE=all
SET BUILD_TARGET=all
SET RESULT=0

::----------------------------------------------------------
:: Checking arguments
::----------------------------------------------------------
IF "%1"=="eclipse" GOTO Eclipse
IF NOT "%1"=="" SET BUILD_SPACE=%1
IF NOT "%2"=="" SET BUILD_TARGET=%2

GOTO Common
::----------------------------------------------------------
:: Checking arguments
::----------------------------------------------------------
:Eclipse
IF "%2"=="" GOTO EclipseUsage
IF "%3"=="" GOTO EclipseUsage
IF "%4"=="" GOTO EclipseUsage
IF NOT "%5"=="" SET BUILD_SPACE=%5
IF NOT "%6"=="" SET BUILD_TARGET=%6

::----------------------------------------------------------
:: Selection of project dir for start
::----------------------------------------------------------
cd %PROJECT_DIR%
SET START_DIR=%PROJECT_DIR%
	
::----------------------------------------------------------
:: Selection of architecture
::----------------------------------------------------------
call select_arch.bat %2 %3 %4

:Common
::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=%cd:\=/%/..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET START_DIR=%CD%

::----------------------------------------------------------
:: Check architecture
::----------------------------------------------------------
IF NOT DEFINED MACHINE_SELECTED GOTO ArchitectureNotSelected
IF "%MACHINE_SELECTED%"=="FALSE" GOTO ArchitectureNotSelected

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Generate list to build
::----------------------------------------------------------
SET BUILD_LIBRARIES=FALSE
SET BUILD_PORTABLE=FALSE
SET BUILD_CORE=FALSE
SET BUILD_USER=FALSE
SET BUILD_SYSTEM_PROGRAMS=FALSE
SET BUILD_USER_PROGRAMS=FALSE
SET RUN_LINKING=FALSE
	
IF "%BUILD_SPACE%"=="all" (
	SET BUILD_LIBRARIES=TRUE
	SET BUILD_PORTABLE=TRUE
	SET BUILD_CORE=TRUE
	SET BUILD_USER=TRUE
	SET BUILD_SYSTEM_PROGRAMS=TRUE
	SET BUILD_USER_PROGRAMS=TRUE
	SET RUN_LINKING=TRUE
) 

IF "%BUILD_SPACE%"=="libraries_space" SET BUILD_LIBRARIES=TRUE
IF "%BUILD_SPACE%"=="portable_space" SET BUILD_PORTABLE=TRUE
IF "%BUILD_SPACE%"=="core_space" SET BUILD_CORE=TRUE
IF "%BUILD_SPACE%"=="user_space" SET BUILD_USER=TRUE
IF "%BUILD_SPACE%"=="system_programs" SET BUILD_SYSTEM_PROGRAMS=TRUE
IF "%BUILD_SPACE%"=="user_programs" SET BUILD_USER_PROGRAMS=TRUE

::----------------------------------------------------------
:: Build libraries space
::----------------------------------------------------------
IF "%BUILD_LIBRARIES%"=="TRUE" (
	echo ---------------------------- building libraries --------------------------
	IF NOT EXIST %SYSTEM_LIBRARIES_DIR% (
		echo Libraries directory not exist!
	) ELSE (
		cd %SYSTEM_LIBRARIES_DIR%
		IF NOT EXIST build.bat (
			echo Build script not exist!
		) ELSE ( 
			call build.bat %BUILD_TARGET% 
			if errorlevel 1 (
				echo Build failed
				SET RESULT=1
			)
		)
		cd %START_DIR%
		IF NOT EXIST %LIBRARIES_SPACE_LIB_FILE_PATH% (
			GOTO EOF
		)
	)
)

::----------------------------------------------------------
:: Build portable space
::----------------------------------------------------------
IF "%BUILD_PORTABLE%"=="TRUE" IF "%RESULT%"=="0" (
	echo ---------------------------- building portable ---------------------------
	IF NOT EXIST %SYSTEM_PORTABLE_DIR% (
	echo Portable directory not exist!
	) ELSE (
		cd %SYSTEM_PORTABLE_DIR%
		IF NOT EXIST build.bat (
			echo Build script not exist!
		) ELSE (
			call build.bat %BUILD_TARGET%
			if errorlevel 1 (
				echo Build failed
				SET RESULT=1
			)
		)
		cd %START_DIR%
		IF NOT EXIST %PORTABLE_SPACE_LIB_FILE_PATH% (
			GOTO EOF
		)
	)
)

::----------------------------------------------------------
:: Build core space
::----------------------------------------------------------
IF "%BUILD_CORE%"=="TRUE" IF "%RESULT%"=="0" (
	echo ---------------------------- building core -------------------------------
	IF NOT EXIST %SYSTEM_CORE_DIR% (
	echo Core directory not exist!
	) ELSE (
		cd %SYSTEM_CORE_DIR%
		IF NOT EXIST build.bat (
			echo Build script not exist!
		) ELSE (
			call build.bat %BUILD_TARGET% 
			if errorlevel 1 (
				echo Build failed
				SET RESULT=1
			)
		)
		cd %START_DIR%
		IF NOT EXIST %CORE_SPACE_LIB_FILE_PATH% (
			GOTO EOF
		)
	)
)

::----------------------------------------------------------
:: Build user space
::----------------------------------------------------------
IF "%BUILD_USER%"=="TRUE" IF "%RESULT%"=="0" (
	echo ---------------------------- building user -------------------------------
	IF NOT EXIST %SYSTEM_USER_DIR% (
		echo User directory not exist!
	) ELSE (
		cd %SYSTEM_USER_DIR%
		IF NOT EXIST build.bat (
			echo Build script not exist!
		) ELSE (
			call build.bat %BUILD_TARGET% 
			if errorlevel 1 (
				echo Build failed
				SET RESULT=1
			)
		)
		cd %START_DIR%
		IF NOT EXIST %USER_SPACE_LIB_FILE_PATH% (
			GOTO EOF
		)
	)
)

::----------------------------------------------------------
:: Build system programs
::----------------------------------------------------------
IF "%BUILD_SYSTEM_PROGRAMS%"=="TRUE" IF "%RESULT%"=="0" (
	echo ---------------------------- building system programs --------------------
	IF NOT EXIST %SYSTEM_PROGRAMS_DIR% (
		echo System programs: '%SYSTEM_PROGRAMS_DIR%' directory not exist!
	) ELSE (
		cd %SYSTEM_PROGRAMS_DIR%
		IF NOT EXIST buildall.bat (
			echo Build script not exist!
		) ELSE (
			call buildall.bat %BUILD_TARGET% 
			if errorlevel 1 (
				echo Build failed
				SET RESULT=1
			)
		)
		cd %START_DIR%
	)
)

::----------------------------------------------------------
:: Build user programs
::----------------------------------------------------------
IF "%BUILD_USER_PROGRAMS%"=="TRUE" IF "%RESULT%"=="0" (
	echo ---------------------------- building user programs --------------------
	IF NOT EXIST %USER_PROGRAMS_DIR% (
		echo System programs directory not exist!
	) ELSE (
		cd %USER_PROGRAMS_DIR%
		IF NOT EXIST buildall.bat (
			echo Build script not exist!
		) ELSE (
			call buildall.bat %BUILD_TARGET% 
			if errorlevel 1 (
				echo Build failed
				SET RESULT=1
			)
		)
		cd %START_DIR%
	)
)


::----------------------------------------------------------
:: Linking all
::----------------------------------------------------------
IF "%RUN_LINKING%"=="TRUE" IF "%RESULT%"=="0" (
	echo ---------------------------- linking all ---------------------------------
	call link all
)

GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Scripts for setting paths not exists!                         =
echo =          file: '%SET_PATHS_FILE_PATH%'                                 =
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:ArchitectureNotSelected
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target architecture is not selected
echo = 			run select_arch.bat first
echo =
echo =========================================================================
GOTO EOF

:EclipseUsage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 eclipse [PRODUCENT] [FAMILY] [MACHINE_NAME] [SPACE] [TARGET]
echo =				
echo =			where PRODUCENT,FAMILY,MACHINE_NAME specify target architecture 
echo =			
echo =			where SPACE is:
echo =					libraries_space
echo =					portable_space
echo =					core_space
echo =					user_space
echo =                  system_programs
echo =                  user_programs
echo =					all
echo =				
echo = 			where TARGET is:
echo = 					all
echo = 					clean
echo ==========================================================================
GOTO EOF 

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 [SPACE] [TARGET]
echo =				
echo =			where PRODUCENT,FAMILY,MACHINE_NAME specify target architecture 
echo =				
echo =			where SPACE is:
echo =					libraries_space
echo =					portable_space
echo =					core_space
echo =					user_space
echo =                  system_programs
echo =                  user_programs
echo =					all
echo =			
echo = 			where TARGET is:
echo = 					all
echo = 					clean
echo ==========================================================================

:EOF
exit /b %RESULT%