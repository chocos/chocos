::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/11/03
::	Description:	Script for setting user name, surname, and additional informations
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Checking arguments
::----------------------------------------------------------
IF "%1" == "" GOTO Usage
IF "%2" == "" GOTO Usage
IF "%3" == "" GOTO Usage

::----------------------------------------------------------
:: Saving variables in current terminal variables
::----------------------------------------------------------
SET CHOCOOS_USER_NAME=%1 
SET CHOCOOS_USER_SURNAME=%2 
SET CHOCOOS_USER_EMAIL=%3
SET CHOCOOS_USER_DEFINED=TRUE

::----------------------------------------------------------
:: Saving variables system variables - the restart of the 
:: terminal is required to load it
::----------------------------------------------------------
SETX CHOCOOS_USER_NAME    %1 
SETX CHOCOOS_USER_SURNAME %2 
SETX CHOCOOS_USER_EMAIL   %3
SETX CHOCOOS_USER_DEFINED TRUE
GOTO Success


::----------------------------------------------------------
:: Messages
::----------------------------------------------------------
GOTO EOF
:Success
echo ==========================================================================
echo =                              SUCCESS                                   =
echo ==========================================================================
echo                  Name    set to %CHOCOOS_USER_NAME%                      
echo                  Surname set to %CHOCOOS_USER_SURNAME%                      
echo                  Email   set to %CHOCOOS_USER_EMAIL%                      
GOTO EOF

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 NAME SURNAME EMAIL
echo =
echo =           where:
echo =              NAME     - Your name 
echo =              SURNAME  - Your surname 
echo =              EMAIL    - Your email
echo = 
echo =      Note:
echo =          The script saves this informations only in your OS in 
echo =          environmental variables. It will be used only for code 
echo =          generators such as drivergen. It is not collected 
echo =          anywhere else. 
echo =
echo ==========================================================================

:EOF