::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/11/03
::	Description:	Script for generating new driver. It is simply caller to the main program drivergen from Tools directory
::================================================================================================================================
@echo off
::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=%cd:\=/%/..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking arguments
::----------------------------------------------------------
IF "%1"==""	GOTO Usage
IF NOT "%2"=="" IF /I NOT "%2"=="TRUE" IF /I NOT "%2"=="FALSE" GOTO Usage

::----------------------------------------------------------
:: Preparing variables
::----------------------------------------------------------
SET WITH_LLD=FALSE
SET DRIVER_NAME=%1
IF /I "%2"=="TRUE" SET WITH_LLD=TRUE

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Check architecture (only when build with LLD)
::----------------------------------------------------------
IF "%WITH_LLD%"=="TRUE" IF NOT DEFINED MACHINE_SELECTED  GOTO ArchitectureNotSelected
IF "%WITH_LLD%"=="TRUE" IF "%MACHINE_SELECTED%"=="FALSE" GOTO ArchitectureNotSelected

::----------------------------------------------------------
:: Check if user was set
::----------------------------------------------------------
IF NOT DEFINED CHOCOOS_USER_DEFINED GOTO UserNotSet
IF "CHOCOOS_USER_DEFINED"=="FALSE"  GOTO UserNotSet

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Checking if files exists
::----------------------------------------------------------
IF NOT EXIST %DRIVERGEN_DIR%        GOTO DriverGenDirNotExist
IF NOT EXIST %DRIVERGEN_FILE_PATH%  GOTO DriverGenFileNotExist

::----------------------------------------------------------
:: Saving current path and go to drivergen directory
::----------------------------------------------------------
SET NEWDRIVER_SAVED_PATH=%CD%
cd %DRIVERGEN_DIR%

::----------------------------------------------------------
:: Calling drivergen to create driver
::----------------------------------------------------------
IF "%WITH_LLD%"=="TRUE" (
	call "%DRIVERGEN_FILE_NAME%" %DRIVER_NAME% %WITH_LLD% %MACHINE_PRODUCENT% %MACHINE_FAMILY% %MACHINE_NAME% NAME=%CHOCOOS_USER_NAME% SURNAME=%CHOCOOS_USER_SURNAME% EMAIL=%CHOCOOS_USER_EMAIL% 
) ELSE (
	call "%DRIVERGEN_FILE_NAME%" %DRIVER_NAME% %WITH_LLD% NAME=%CHOCOOS_USER_NAME% SURNAME=%CHOCOOS_USER_SURNAME% EMAIL=%CHOCOOS_USER_EMAIL% 
)

::----------------------------------------------------------
:: Returning to old directory
::----------------------------------------------------------
cd %NEWDRIVER_SAVED_PATH%

::----------------------------------------------------------
:: Messages
::----------------------------------------------------------
GOTO EOF
:DriverGenFileNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			The drivergen tool file not exists!                           =
echo =          path: '%DRIVERGEN_FILE_PATH%'                                       
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:DriverGenDirNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Directory of drivergen tool not exists!                       =
echo =          path: '%DRIVERGEN_DIR%'                                       
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:UserNotSet
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target user data not set
echo = 			run set_user.bat first
echo =
echo =========================================================================
GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Scripts for setting paths not exists!                         =
echo =          file: '%SET_PATHS_FILE_PATH%'                                 
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:ArchitectureNotSelected
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target architecture is not selected
echo = 			run select_arch.bat first
echo =
echo =========================================================================
GOTO EOF

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 DRIVER_NAME [WITH_LLD]
echo =
echo =           where:
echo =              DRIVER_NAME     - name of the driver to create
echo =              WITH_LLD        - TRUE if the LLD should also be generated 
echo =                                The LLD is not generated by default (FALSE)
echo = 
echo =      Note:
echo =          This script creates new driver according to the templates files.
echo =          The LLD files will be created for currently selected architecture, 
echo =          so if you want to run it for some other then current, you should 
echo =          run the 'select_arch' script before.
echo =
echo ==========================================================================

:EOF
