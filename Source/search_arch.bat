::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2016/04/10
::	Description:	script for searching architecture
::================================================================================================================================
@echo off	

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking paths exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: checking parameters
::----------------------------------------------------------
IF "%1"=="" GOTO Usage

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH% skip-checking-arch

::----------------------------------------------------------
:: Checking paths exist 
::----------------------------------------------------------
IF NOT EXIST %SYSTEM_PORTABLE_SCRIPTS_DIR% GOTO PortableScriptsNotExist
IF NOT EXIST %SELECT_ARCH_FILE_PATH% GOTO SelectArchScriptNotExist

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
SET MACHINE_SELECTED=FALSE
SET MACHINE_TO_FIND=%1

call :FindProducent %MACHINE_TO_FIND%

IF NOT DEFINED MACHINE_SELECTED GOTO MachineNotFound
IF "%MACHINE_SELECTED%"=="FALSE" GOTO MachineNotFound

GOTO EOF

::----------------------------------------------------------
:: Function with loop for each producent. Searches for 
:: producent and sets machine producent variable
::
::      Arguments: 
::                  None
::
::      Required variables: MACHINE_FOUND=FALSE/TRUE
::----------------------------------------------------------
:FindProducent
for /d %%f in ("%SYSTEM_PORTABLE_SCRIPTS_DIR%\*") do (
    call :FindFamily %1 %%f %%~nxf 
)
exit /b

::----------------------------------------------------------
:: Function with loop for each family. Searches for 
:: family and sets machine family variable
::
::      Arguments: 
::                  Producent directory path
::
::      Required variables: MACHINE_FOUND=FALSE/TRUE
::----------------------------------------------------------
:FindFamily
for /d %%f in ("%~2\*") do (
    call :FindMachine %1 %%f %3 %%~nxf
)
exit /b

::----------------------------------------------------------
:: Function with loop for each machine. Searches for 
:: machine and sets machine name variable
::
::      Arguments: 
::                  Family directory path
::
::      Required variables: MACHINE_FOUND=FALSE/TRUE
::----------------------------------------------------------
:FindMachine
for /d %%f in ("%~2\*") do (
    IF /I "%1" == "%%~nxf" (
        CALL :MachineFound %3 %4 %%~nxf
    )
)
exit /b

GOTO EOF

:MachineFound
echo --------------------------------------------------------------------------
echo                           Machine has been found!
echo --------------------------------------------------------------------------
echo               Found data:
echo                       Machine producent:       %1
echo                       Machine family:          %2
echo                       Machine name:            %3

CALL %SELECT_ARCH_FILE_PATH% %1 %2 %3
EXIT /b 0
GOTO EOF

:MachineNotFound
echo --------------------------------------------------------------------------
echo -                                                                        -
echo -                                   ERROR                                -
echo -                                                                        -
echo -           Cannot find the machine:                                     - 
echo -                            %MACHINE_TO_FIND% 
echo -                                                                        -
echo -           If the name is correctly given, then it does mean that       -
echo -           the system does not support it yet.                          -
echo --------------------------------------------------------------------------
EXIT /b 1
GOTO EOF

:PortableScriptsNotExist
echo ==========================================================================
echo                                   ERROR
echo ==========================================================================
echo                   Portable scripts dir does not exist:
echo                       %SYSTEM_PORTABLE_SCRIPTS_DIR%
EXIT /b 1
GOTO EOF
:SelectArchScriptNotExist
echo ==========================================================================
echo                                   ERROR
echo ==========================================================================
echo                   Script for selection architecture does not exist:
echo                       %SELECT_ARCH_FILE_PATH%
EXIT /b 1
GOTO EOF
:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 MACHINE_NAME 
echo =				
echo = 			where:
echo = 				MACHINE_NAME is the name of the microcontroller to 
echo =              find
echo ==========================================================================
EXIT /b 1
:Error
echo ==========================================================================
echo                                   ERROR
echo ==========================================================================
echo                       This behavior is unexpected
echo                       The script can be damaged
EXIT /b 1
:EOF
SETLOCAL DISABLEDELAYEDEXPANSION