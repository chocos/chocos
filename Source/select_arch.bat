::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	script for selection architecture
::================================================================================================================================
@echo off	

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat
SET SEARCH_ARCH_FILE_PATH=%PROJECT_DIR%/Source/search_arch.bat

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist
IF NOT EXIST %SEARCH_ARCH_FILE_PATH% GOTO SeachArchScriptNotExist

::----------------------------------------------------------
:: checking parameters
::----------------------------------------------------------
IF "%1"=="" GOTO Usage
IF "%2"=="" GOTO SearchArchitecture
IF "%3"=="" GOTO Usage

::----------------------------------------------------------
:: setting machine definitions
::----------------------------------------------------------
SET MACHINE_PRODUCENT=%1
SET MACHINE_FAMILY=%2
SET MACHINE_NAME=%3
SET MACHINE_SELECTED=FALSE

GOTO CallSetPathsScript

::----------------------------------------------------------
:: Searching for machine
::----------------------------------------------------------
:SearchArchitecture
SET MACHINE_SELECTED=FALSE

call %SEARCH_ARCH_FILE_PATH% %1

GOTO EOF

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
:CallSetPathsScript
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: check if machine is set
::----------------------------------------------------------
IF NOT EXIST %SYSTEM_PORTABLE_DIR% GOTO PortableDirNotExists
IF NOT EXIST %SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR% GOTO MachineNotExists
IF NOT EXIST %PORTABLE_DEFS_MK_FILE_PATH% GOTO DefinitionsFileNotExists
IF NOT EXIST %MACHINE_CONNECT_FILE_PATH% GOTO ConnectFileNotExists
IF NOT EXIST %MACHINE_ARCH_CFG_FILE_PATH% GOTO ArchCfgFileNotExist

::----------------------------------------------------------
:: add make 32 or 64 to path
::----------------------------------------------------------
if %PROCESSOR_ARCHITECTURE%==x86 SET PATH=%cd%/../Tools/compilers/make32/;
if %PROCESSOR_ARCHITECTURE%==AMD64 SET PATH=%cd%/../Tools/compilers/make64/;

SET PATH=%PATH%;%cd%/../Tools/compilers/arm_none_eabi_gcc/bin;

:: Setting Architecture variables
call %MACHINE_ARCH_CFG_FILE_PATH%

:Success
set MACHINE_SELECTED=TRUE

echo -----------------------------------------------
echo              Architecture selected
echo -----------------------------------------------

GOTO EOF

:SeachArchScriptNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Scripts for searching architecture not exists!                =
echo =          file: '%SEARCH_ARCH_FILE_PATH%'                          
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:ArchCfgFileNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Scripts for setting architecture not exists!                  =
echo =          file: '%MACHINE_ARCH_CFG_FILE_PATH%'                          =
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Scripts for setting paths not exists!                         =
echo =          file: '%SET_PATHS_FILE_PATH%'                                 =
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:ConnectFileNotExists
echo Portable '%MACHINE_CONNECT_FILE_PATH%' file for connection with programmer does not exist
GOTO EOF

:PortableDirNotExists
echo Portable '%SYSTEM_PORTABLE_DIR%' dir does not exist
GOTO EOF

:DefinitionsFileNotExists
echo Definitions file '%PORTABLE_DEFS_MK_FILE_PATH%' does not exist
GOTO EOF

:MachineNotExists
echo Machine '%MACHINE_NAME%' does not exist
IF NOT EXIST %SYSTEM_PORTABLE_SCRIPTS_FAMILY_DIR% GOTO FamilyNotExists

GOTO EOF

:FamilyNotExists
echo Family '%MACHINE_FAMILY%' does not exist
IF NOT EXIST %SYSTEM_PORTABLE_SCRIPTS_PRODUCENT_DIR%\NUL GOTO ProducentNotExists
GOTO EOF

:ProducentNotExists
echo Producent '%MACHINE_PRODUCENT%' does not exist
IF NOT EXIST %SYSTEM_PORTABLE_SCRIPTS_DIR% GOTO ScriptsDirNotExists
GOTO EOF

:ScriptsDirNotExists
echo Scripts directory: %SYSTEM_PORTABLE_SCRIPTS_DIR% does not exists!
GOTO EOF
	
:Error
echo ==========================================================================
echo                                   ERROR
echo ==========================================================================
echo                       This behavior is unexpected
echo                       The script can be damaged
GOTO EOF
	
:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 MACHINE_PRODUCENT MACHINE_FAMILY MACHINE_NAME
echo =                   or:
echo =         %0 MACHINE_NAME
echo =				
echo = 			where:
echo = 					MACHINE_PRODUCENT is the name of the producent 
echo =                                                (e.g "ti" or "st")
echo =                  MACHINE_FAMILY is the name of the processor family 
echo =                                                (e.g "lm4f" or "stm32f7")
echo =                  MACHINE_NAME is the name of the machine ("stm32f746ngh6")
echo = 
echo ==========================================================================

:EOF