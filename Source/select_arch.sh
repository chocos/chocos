#!/bin/sh
#================================================================================================================================
#	Author:			Patryk Kubiak
#	Date:			2016/07/2
#	Description:	script for selection architecture
#================================================================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR! ($0)                                                 "
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

Usage()
{
    printf "\033[1;31;40m"
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=      Usage:                                                             "
    echo "=         $0 MACHINE_PRODUCENT MACHINE_FAMILY MACHINE_NAME                "
    echo "=                   or:                                                   "
    echo "=         $0 MACHINE_NAME                                                 "
    echo "=				                                                                "
    echo "=             where:                                                      "
    echo "=                  MACHINE_PRODUCENT is the name of the producent         "
    echo "=                                                (e.g ti or st)           "
    echo "=                  MACHINE_FAMILY is the name of the processor family     "
    echo "=                                                (e.g lm4f or stm32f7)    "
    echo "=                  MACHINE_NAME is the name of the machine (stm32f746ngh6)"
    echo "=                                                                         "
    echo "=========================================================================="
    printf "\033[0;40m"
    exit 1
}

MachineSelected()
{
    export MACHINE_SELECTED="true"
    SetForegroundColor "green"
    
    echo "-----------------------------------------------"
    echo "             Architecture selected             "
    echo "-----------------------------------------------"
}

#=====================================================================================================
#	Helper functions
#=====================================================================================================
SearchArchitecture()
{
    . $SEARCH_ARCH_FILE_PATH $1
}

ConfigureToolsPaths()
{
    . $CONFIGURE_FILE_PATH $1
    
    chmod +x $MACHINE_ARCH_CFG_FILE_PATH
    
    . $MACHINE_ARCH_CFG_FILE_PATH
}

#=====================================================================================================
#	PREPARE VARIABLES
#=====================================================================================================
PROJECT_DIR=$PWD/..
CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh
SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh
SEARCH_ARCH_FILE_PATH=$PROJECT_DIR/Source/search_arch.sh
SKIP_CHECKING_ARCH="true"

#=====================================================================================================
#	PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi
if [ ! -e $SEARCH_ARCH_FILE_PATH ]; then PathDoesNotExist $SEARCH_ARCH_FILE_PATH;   fi

#=====================================================================================================
#	ARGUMENTS VERIFICATION
#=====================================================================================================
if   [ "$1" = "" ]; then Usage;                 
elif [ "$2" = "" ]; then SearchArchitecture $1;    
elif [ "$3" = "" ]; then Usage; 
else
    #=====================================================================================================
    #	SETTING MACHINE DEFINITIONS
    #=====================================================================================================
    export MACHINE_PRODUCENT=$1
    export MACHINE_FAMILY=$2
    export MACHINE_NAME=$3
    export MACHINE_SELECTED="false"
    export ECLIPSE_BUILD=$4
    
    #=====================================================================================================
    #	IMPORT MODULES
    #=====================================================================================================
    . $CONSOLE_FILE_PATH
    . $SET_PATHS_FILE_PATH skip-checking-arch

    #=====================================================================================================
    #	MACHINE VERIFICATION
    #=====================================================================================================
    if [ ! -e $SYSTEM_PORTABLE_DIR ];                   then PathDoesNotExist $SYSTEM_PORTABLE_DIR;                   fi
    if [ ! -e $SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR ];   then PathDoesNotExist $SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR;   fi
    if [ ! -e $MACHINE_CONNECT_FILE_PATH ];             then PathDoesNotExist $MACHINE_CONNECT_FILE_PATH;             fi
    if [ ! -e $MACHINE_ARCH_CFG_FILE_PATH ];            then PathDoesNotExist $MACHINE_ARCH_CFG_FILE_PATH;            fi

    #=====================================================================================================
    #	SUCCESS
    #=====================================================================================================
    MachineSelected
    ConfigureToolsPaths $ECLIPSE_BUILD
fi