#!/bin/sh
# ======================================================================================
# 
# @author               Patryk Kubiak
# @file                 Prepare Flash File System
# @desc                 This is the script for preparation of the flash file system. 
#                       The script searches for all files stored in the flash directory
#                       and puts it in the /flash/ system path. 
# 
# ======================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR!                                                     ="
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR"         = "" ]; then PROJECT_DIR=$PWD/..;                          fi
if [ "$SET_PATHS_FILE_PATH" = "" ]; then SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh;fi
if [ "$CONSOLE_FILE_PATH"   = "" ]; then CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh;    fi
FLASH_MACHINE="false"
ARGUMENTS=
NUMBER_OF_FILES=0

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#    OTHER PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $SYSTEM_FLASH_DIR      ]; then PathDoesNotExist $SYSTEM_FLASH_DIR;        fi
if [ ! -e $TMPLPARS_FILE_PATH    ]; then PathDoesNotExist $TMPLPARS_FILE_PATH;      fi
if [ ! -e $FLASHFS_TEMPLATES_DIR ]; then PathDoesNotExist $FLASHFS_TEMPLATES_DIR;   fi

#=====================================================================================================
#    PREPARING 
#=====================================================================================================
for FILE_NAME in `ls $SYSTEM_FLASH_DIR/ | grep -v /`;
do
    echo "Found file $FILE_NAME"
    ARGUMENTS="$ARGUMENTS FILE$NUMBER_OF_FILES=[$SYSTEM_FLASH_DIR/$FILE_NAME]"
    NUMBER_OF_FILES=$(($NUMBER_OF_FILES + 1)); 
done

echo "Found $NUMBER_OF_FILES files"

for TEMPLATE_FILE_NAME in `ls $FLASHFS_TEMPLATES_DIR/ | grep -v /`;
do
    echo "Parsing template file $TEMPLATE_FILE_NAME"
    $TMPLPARS_FILE_PATH $FLASHFS_TEMPLATES_DIR/$TEMPLATE_FILE_NAME $SYSTEM_CORE_SOURCES_FLASHFS_DIR/$TEMPLATE_FILE_NAME $ARGUMENTS NUMBER_OF_FILES=$NUMBER_OF_FILES
done