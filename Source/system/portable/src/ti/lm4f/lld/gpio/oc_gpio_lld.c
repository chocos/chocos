/** ****************************************************************************************************************************************
 *
 * @file       oc_gpio_lld.c
 *
 * @brief      File with source for lm4f120h5qr machine for GPIO LLD module
 *
 * @author     Patryk Kubiak - (Created on: 15 08 2015 15:10:29)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_gpio_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_lld.h>
#include <oc_module.h>
#include <oc_mcs.h>
#include <oc_lsf.h>
#include <oc_interrupts.h>
#include <oc_channels.h>

/** ========================================================================================================================================
 *
 *              The section with local macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

#define PORT(Pins)               oC_Pin_GetPort(Pins)
#define PORT_INDEX(Pins)         oC_Channel_ToIndex(GPIO , PORT(Pins))
#define PINS(Pins)               oC_Pin_GetPinsInPort(Pins)
#define IsPoweredOn(Pins)        oC_Machine_IsChannelPoweredOn(PORT(Pins))
#define GPIODATA(Pins)           oC_Machine_Register(PORT(Pins),GPIODATA)
#define GPIOCR(Pins)             oC_Machine_Register(PORT(Pins),GPIOCR)
#define GPIODEN(Pins)            oC_Machine_Register(PORT(Pins),GPIODEN)
#define GPIODR2R(Pins)           oC_Machine_Register(PORT(Pins),GPIODR2R)
#define GPIODR4R(Pins)           oC_Machine_Register(PORT(Pins),GPIODR4R)
#define GPIODR8R(Pins)           oC_Machine_Register(PORT(Pins),GPIODR8R)
#define GPIODIR(Pins)            oC_Machine_Register(PORT(Pins),GPIODIR)
#define GPIOAFSEL(Pins)          oC_Machine_Register(PORT(Pins),GPIOAFSEL)
#define GPIOPUR(Pins)            oC_Machine_Register(PORT(Pins),GPIOPUR)
#define GPIOPDR(Pins)            oC_Machine_Register(PORT(Pins),GPIOPDR)
#define GPIOPCTL(Pins)           oC_Machine_Register(PORT(Pins),GPIOPCTL)
#define GPIOODR(Pins)            oC_Machine_Register(PORT(Pins),GPIOODR)
#define GPIOIM(Pins)             oC_Machine_Register(PORT(Pins),GPIOIM)
#define GPIOIS(Pins)             oC_Machine_Register(PORT(Pins),GPIOIS)
#define GPIOIBE(Pins)            oC_Machine_Register(PORT(Pins),GPIOIBE)
#define GPIOIEV(Pins)            oC_Machine_Register(PORT(Pins),GPIOIEV)
#define IsRam(Address)           oC_LSF_IsRamAddress(Address)
#define IsRom(Address)           oC_LSF_IsRomAddress(Address)

#undef  _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions connections to main driver
 *
 *  ======================================================================================================================================*/
#define _________________________________________MAIN_DRIVER_CONNECTIONS_SECTION____________________________________________________________

oC_LLD_ConnectToMainDriver(GPIO,IsPortCorrect);
oC_LLD_ConnectToMainDriver(GPIO,IsPortIndexCorrect);
oC_LLD_ConnectToMainDriver(GPIO,GetPinsMaskOfPins);
oC_LLD_ConnectToMainDriver(GPIO,GetPortOfPins);
oC_LLD_ConnectToMainDriver(GPIO,PortToPortIndex);
oC_LLD_ConnectToMainDriver(GPIO,PortIndexToPort);
oC_LLD_ConnectToMainDriver(GPIO,IsPinDefined);
oC_LLD_ConnectToMainDriver(GPIO,ArePinsDefined);
oC_LLD_ConnectToMainDriver(GPIO,ArePinsCorrect);
oC_LLD_ConnectToMainDriver(GPIO,IsSinglePin);
oC_LLD_ConnectToMainDriver(GPIO,GetPinsFor);
oC_LLD_ConnectToMainDriver(GPIO,GetPortName);
oC_LLD_ConnectToMainDriver(GPIO,GetPinName);
oC_LLD_ConnectToMainDriver(GPIO,SetPower);
oC_LLD_ConnectToMainDriver(GPIO,ReadPower);
oC_LLD_ConnectToMainDriver(GPIO,CheckIsPinUsed);
oC_LLD_ConnectToMainDriver(GPIO,ArePinsUnused);
oC_LLD_ConnectToMainDriver(GPIO,WriteData);
oC_LLD_ConnectToMainDriver(GPIO,ReadData);
oC_LLD_ConnectToMainDriver(GPIO,ReadDataReference);
oC_LLD_ConnectToMainDriver(GPIO,GetHighStatePins);
oC_LLD_ConnectToMainDriver(GPIO,GetLowStatePins);
oC_LLD_ConnectToMainDriver(GPIO,IsPinsState);
oC_LLD_ConnectToMainDriver(GPIO,SetPinsState);
oC_LLD_ConnectToMainDriver(GPIO,TogglePinsState);
oC_LLD_ConnectToMainDriver(GPIO,GetPinName);

#undef  _________________________________________MAIN_DRIVER_CONNECTIONS_SECTION____________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

static oC_PinsMask_t            UsedPinsArray[oC_ModuleChannel_NumberOfElements(GPIO)];
static oC_GPIO_LLD_Interrupt_t  InterruptHandler    = NULL;

#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_Module_TurnOn(oC_Module_GPIO_LLD);

        oC_GPIO_LLD_ForEachPort(port)
        {
            UsedPinsArray[oC_Channel_ToIndex(GPIO,port)] = 0;
        }

        InterruptHandler = NULL;
        errorCode        = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_Module_TurnOff(oC_Module_GPIO_LLD);

        InterruptHandler  = NULL;
        errorCode         = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPortCorrect( oC_Port_t Port )
{
    return oC_Channel_IsCorrect(GPIO , Port);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPortIndexCorrect( oC_PortIndex_t PortIndex )
{
    return PortIndex < oC_ModuleChannel_NumberOfElements(GPIO);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_PinsMask_t oC_GPIO_LLD_GetPinsMaskOfPins( oC_Pins_t Pins )
{
    return PINS(Pins);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_Port_t oC_GPIO_LLD_GetPortOfPins( oC_Pins_t Pins )
{
    return PORT(Pins);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_PortIndex_t oC_GPIO_LLD_PortToPortIndex( oC_Port_t Port )
{
    return oC_Channel_ToIndex(GPIO,Port);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_Port_t oC_GPIO_LLD_PortIndexToPort( oC_PortIndex_t PortIndex )
{
    return oC_Channel_FromIndex(GPIO,PortIndex);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPinDefined( oC_Pins_t Pin )
{
    bool defined = false;

    oC_Pin_ForeachDefined(pin)
    {
        if(pin->Pin == Pin)
        {
            defined = true;
            break;
        }
    }

    return defined;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_ArePinsDefined( oC_Pins_t Pins )
{
    bool            allDefined = false;
    oC_Port_t       port       = PORT(Pins);
    oC_PinsInPort_t pins       = PINS(Pins);

    if(oC_Channel_IsCorrect(GPIO,port))
    {
        allDefined = true;

        for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH && allDefined; bitIndex++ )
        {
            if(pins & (1<<bitIndex))
            {
                allDefined = oC_GPIO_LLD_IsPinDefined(oC_Pins_ToSinglePin(Pins,bitIndex));
            }
        }
    }

    return allDefined;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_ArePinsCorrect( oC_Pins_t Pins )
{
    return oC_Channel_IsCorrect(GPIO,PORT(Pins));
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsSinglePin( oC_Pins_t Pins )
{
    bool            isSinglePin     = false;
    oC_PinsInPort_t pins            = PINS(Pins);

    for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH ; bitIndex++)
    {
        if(pins & (1<<bitIndex))
        {
            isSinglePin = oC_Pins_ToSinglePin(Pins,bitIndex) == Pins;
            break;
        }
    }

    return isSinglePin;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_Pins_t oC_GPIO_LLD_GetPinsFor( oC_Port_t Port , oC_PinsMask_t Pins )
{
    return oC_Pin_Make(Port,Pins);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
const char * oC_GPIO_LLD_GetPortName( oC_Port_t Port )
{
    return oC_Channel_GetName(Port);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
const char * oC_GPIO_LLD_GetPinName( oC_Pins_t Pin )
{
    const char * name = "<INCORRECT_PIN>";
    oC_Port_t    port = PORT(Pin);

    if(oC_Channel_IsCorrect(GPIO,port))
    {
        name = "<UNDEFINED_PIN>";

        oC_Pin_ForeachDefined(pinData)
        {
            if(pinData->Pin == Pin)
            {
                name = pinData->Name;
                break;
            }
        }
    }

    return name;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetDriverInterruptHandler( oC_GPIO_LLD_Interrupt_t Handler )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( IsRam(Handler) || IsRom(Handler), oC_ErrorCode_WrongEventHandlerAddress) &&
            ErrorCondition( InterruptHandler == NULL,         oC_ErrorCode_InterruptHandlerAlreadySet)
            )
        {
            InterruptHandler = Handler;
            errorCode        = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_BeginConfiguration( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins)                            , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0                                             , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(PORT(Pins),oC_Power_On)  , oC_ErrorCode_CannotEnableChannel)
        )
        {
            //TODO: ask Patryk for this
            oC_Machine_RegisterIndirectDynamic(oC_BaseAddress_SystemControl , oC_RegisterOffset_GPIOHBCTL)|=1<<(PORT(Pins)-1);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes: nothing to do in this architecture
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_FinishConfiguration( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins), oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                  oC_ErrorCode_PinNotDefined)
            )
        {
            errorCode  = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPower( oC_Pins_t Pins , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),               oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( Power  == oC_Power_On || Power == oC_Power_Off, oC_ErrorCode_PowerStateNotCorrect)
            )
        {
            if(oC_Machine_SetPowerStateForChannel(PORT(Pins) , Power ))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_CannotEnableChannel;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadPower( oC_Pins_t Pins , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),   oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                    oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outPower),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_Power_t powerState = oC_Machine_GetPowerStateForChannel(PORT(Pins));

            if (powerState == oC_Power_NotHandled)
            {
                *outPower = oC_Power_On;
                errorCode = oC_ErrorCode_None;
            }
            else if (powerState == oC_Power_On || powerState == oC_Power_Off)
            {
                *outPower = powerState;
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPinProtected( oC_Pins_t Pins )
{
    bool pinProtected = false;

    if(oC_Module_IsTurnedOn(oC_Module_GPIO_LLD) && IsPoweredOn(Pins))
    {
        pinProtected = (~GPIOCR(Pins)->Value) & PINS(Pins);;
    }

    return pinProtected;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_UnlockProtection( oC_Pins_t Pins , oC_GPIO_LLD_Protection_t Protection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),                           oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                            oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                                          oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( Protection == oC_GPIO_LLD_Protection_UnlockProtectedPins,   oC_ErrorCode_PinNeedUnlock)
            )
        {
            oC_MCS_EnterCriticalSection();
            oC_Machine_WriteRegister(PORT(Pins) , GPIOLOCK , oC_MACHINE_GPIO_LOCK_KEY);

            GPIOCR(Pins)->Value  |= PINS(Pins);
            GPIODEN(Pins)->Value |= PINS(Pins);

            oC_Machine_WriteRegister(PORT(Pins) , GPIOLOCK , 0);

            errorCode = oC_ErrorCode_None;

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_LockProtection( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),                           oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                            oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                                          oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();

            oC_Machine_WriteRegister(PORT(Pins) , GPIOLOCK , oC_MACHINE_GPIO_LOCK_KEY);

            GPIOCR(Pins)->Value  &= ~PINS(Pins);
            GPIODEN(Pins)->Value &= ~PINS(Pins);

            oC_Machine_WriteRegister(PORT(Pins) , GPIOLOCK , 0);

            errorCode = oC_ErrorCode_None;

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_CheckIsPinUnlocked( oC_Pins_t Pins , bool * outPinUnlocked )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),                           oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                            oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                                          oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outPinUnlocked),                                      oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();

            oC_Machine_WriteRegister(PORT(Pins) , GPIOLOCK , oC_MACHINE_GPIO_LOCK_KEY);

            *outPinUnlocked = (GPIOCR(Pins)->Value & PINS(Pins)) == PINS(Pins);

            oC_Machine_WriteRegister(PORT(Pins) , GPIOLOCK , 0);

            errorCode = oC_ErrorCode_None;

            oC_MCS_ExitCriticalSection();
        }
}

return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetSpeed( oC_Pins_t Pins , oC_GPIO_LLD_Speed_t Speed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),   oC_ErrorCode_WrongChannel               ) &&
            ErrorCondition( PINS(Pins) != 0,                    oC_ErrorCode_PinNotDefined              ) &&
            ErrorCondition( IsPoweredOn(Pins),                  oC_ErrorCode_ChannelNotPoweredOn        ) &&
            ErrorCondition( Speed == oC_GPIO_LLD_Speed_Default, oC_ErrorCode_NotSupportedOnTargetMachine)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadSpeed( oC_Pins_t Pins , oC_GPIO_LLD_Speed_t * outSpeed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outSpeed),                   oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outSpeed = oC_GPIO_LLD_Speed_Maximum;
            errorCode = oC_ErrorCode_None;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetCurrent( oC_Pins_t Pins , oC_GPIO_LLD_Current_t Current )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            errorCode = oC_ErrorCode_None;

            switch(Current)
            {
                case oC_GPIO_LLD_Current_Default:
                case oC_GPIO_LLD_Current_Minimum:
                    GPIODR2R(Pins)->Value |= PINS(Pins);
                    break;
                case oC_GPIO_LLD_Current_Medium:
                    GPIODR4R(Pins)->Value |= PINS(Pins);
                    break;
                case oC_GPIO_LLD_Current_Maximum:
                    GPIODR8R(Pins)->Value |= PINS(Pins);
                    break;
                default:
                    errorCode = oC_ErrorCode_GPIOCurrentNotCorrect;
                    break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadCurrent( oC_Pins_t Pins , oC_GPIO_LLD_Current_t * outCurrent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outCurrent),                 oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            errorCode = oC_ErrorCode_None;

            if(GPIODR2R(Pins)->Value & PINS(Pins))
            {
                *outCurrent = oC_GPIO_LLD_Current_Minimum;
            }
            else if(GPIODR4R(Pins)->Value & PINS(Pins))
            {
                *outCurrent = oC_GPIO_LLD_Current_Medium;
            }
            else if(GPIODR4R(Pins)->Value & PINS(Pins))
            {
                *outCurrent = oC_GPIO_LLD_Current_Maximum;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetMode( oC_Pins_t Pins , oC_GPIO_LLD_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();
            errorCode = oC_ErrorCode_None;

            switch(Mode)
            {
                case oC_GPIO_LLD_Mode_Output:
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIODIR(Pins)->Value   , PINS(Pins));
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOAFSEL(Pins)->Value , PINS(Pins));
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIODEN(Pins)->Value   , PINS(Pins));
                    break;
                case oC_GPIO_LLD_Mode_Default:
                case oC_GPIO_LLD_Mode_Input:
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIODIR(Pins)->Value   , PINS(Pins));
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOAFSEL(Pins)->Value , PINS(Pins));
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIODEN(Pins)->Value   , PINS(Pins));
                    break;
                case oC_GPIO_LLD_Mode_Alternate:
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOAFSEL(Pins)->Value , PINS(Pins) );
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIODEN(Pins)->Value   , PINS(Pins) );
                    break;
                default:
                    errorCode = oC_ErrorCode_GPIOModeNotCorrect;
                    break;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadMode( oC_Pins_t Pins , oC_GPIO_LLD_Mode_t * outMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outMode),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();
            errorCode = oC_ErrorCode_None;

            if(
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIODIR(Pins)->Value   , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOAFSEL(Pins)->Value , PINS(Pins))
                    )
            {
                *outMode = oC_GPIO_LLD_Mode_Output;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitClearU32( GPIODIR(Pins)->Value   , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOAFSEL(Pins)->Value , PINS(Pins))
                    )
            {
                *outMode = oC_GPIO_LLD_Mode_Input;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIODEN(Pins)->Value   , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOAFSEL(Pins)->Value , PINS(Pins))
                    )
            {
                *outMode = oC_GPIO_LLD_Mode_Alternate;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPull( oC_Pins_t Pins , oC_GPIO_LLD_Pull_t Pull )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();
            errorCode = oC_ErrorCode_None;

            switch(Pull)
            {
                case oC_GPIO_LLD_Pull_Default:
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOAFSEL(Pins)->Value , PINS(Pins));
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIODEN(Pins)->Value   , PINS(Pins));
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOPDR(Pins)->Value   , PINS(Pins));
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOPUR(Pins)->Value   , PINS(Pins));
                    break;
                case oC_GPIO_LLD_Pull_Up:
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOPUR(Pins)->Value   , PINS(Pins));
                    break;
                case oC_GPIO_LLD_Pull_Down:
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOPDR(Pins)->Value   , PINS(Pins));
                    break;
                default:
                    errorCode = oC_ErrorCode_PullNotCorrect;
                    break;
            }

            oC_MCS_ExitCriticalSection();

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadPull( oC_Pins_t Pins , oC_GPIO_LLD_Pull_t * outPull )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outPull),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();

            errorCode = oC_ErrorCode_None;

            if(
                    oC_Bits_IsAtLeastOneBitSetU32( GPIOPUR(Pins)->Value   , PINS(Pins))
                    )
            {
                *outPull = oC_GPIO_LLD_Pull_Up;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitSetU32( GPIOPDR(Pins)->Value   , PINS(Pins))
                    )
            {
                *outPull = oC_GPIO_LLD_Pull_Down;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetOutputCircuit( oC_Pins_t Pins , oC_GPIO_LLD_OutputCircuit_t OutputCircuit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();
            errorCode = oC_ErrorCode_None;

            switch(OutputCircuit)
            {
                case oC_GPIO_LLD_OutputCircuit_Default:
                case oC_GPIO_LLD_OutputCircuit_PushPull:
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOODR(Pins)->Value   , PINS(Pins));
                    break;
                case oC_GPIO_LLD_OutputCircuit_OpenDrain:
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOODR(Pins)->Value   , PINS(Pins));
                    break;
                default:
                    errorCode = oC_ErrorCode_OutputCircuitNotCorrect;
                    break;
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadOutputCircuit( oC_Pins_t Pins , oC_GPIO_LLD_OutputCircuit_t * outOutputCircuit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outOutputCircuit),           oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();
            errorCode = oC_ErrorCode_None;

            if(
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOODR(Pins)->Value , PINS(Pins))
                    )
            {
                *outOutputCircuit = oC_GPIO_LLD_OutputCircuit_PushPull;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitSetU32( GPIOODR(Pins)->Value , PINS(Pins))
                    )
            {
                *outOutputCircuit = oC_GPIO_LLD_OutputCircuit_OpenDrain;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetInterruptTrigger( oC_Pins_t Pins , oC_GPIO_LLD_IntTrigger_t InterruptTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),               oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                              oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( InterruptTrigger == oC_GPIO_LLD_IntTrigger_Off, oC_ErrorCode_NotSupportedOnTargetMachine)
            )
        {
            oC_MCS_EnterCriticalSection();
            errorCode = oC_ErrorCode_None;

            switch(InterruptTrigger)
            {
                case oC_GPIO_LLD_IntTrigger_Off:
                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOIM(Pins)->Value    , PINS(Pins));
                    break;
                case oC_GPIO_LLD_IntTrigger_BothEdges:
                    oC_Channel_EnableInterrupt(PORT(Pins) , PeripheralInterrupt);
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIM(Pins)->Value    , PINS(Pins));

                    oC_Bits_ClearBitsU32(   (oC_uint32_t*)&GPIOIS(Pins)->Value    , PINS(Pins));
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIBE(Pins)->Value   , PINS(Pins));

                    break;

                case oC_GPIO_LLD_IntTrigger_BothLevels:
                    oC_Channel_EnableInterrupt(PORT(Pins) , PeripheralInterrupt);
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIM(Pins)->Value    , PINS(Pins));

                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIS(Pins)->Value    , PINS(Pins));
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIBE(Pins)->Value   , PINS(Pins));

                    break;
                case oC_GPIO_LLD_IntTrigger_FallingEdge:
                    oC_Channel_EnableInterrupt(PORT(Pins) , PeripheralInterrupt);
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIM(Pins)->Value    , PINS(Pins));

                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIS(Pins)->Value    , PINS(Pins));
                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIBE(Pins)->Value   , PINS(Pins));
                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIEV(Pins)->Value   , PINS(Pins));

                    break;

                case oC_GPIO_LLD_IntTrigger_RisingEdge:
                    oC_Channel_EnableInterrupt(PORT(Pins) , PeripheralInterrupt);
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIM(Pins)->Value    , PINS(Pins));

                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIS(Pins)->Value    , PINS(Pins));
                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIBE(Pins)->Value   , PINS(Pins));
                    oC_Bits_SetBitsU32(       (oC_uint32_t*)&GPIOIEV(Pins)->Value   , PINS(Pins));

                    break;
                case oC_GPIO_LLD_IntTrigger_HighLevel:
                    oC_Channel_EnableInterrupt(PORT(Pins) , PeripheralInterrupt);
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIM(Pins)->Value    , PINS(Pins));

                    oC_Bits_SetBitsU32(       (oC_uint32_t*)&GPIOIS(Pins)->Value    , PINS(Pins));
                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIBE(Pins)->Value   , PINS(Pins));
                    oC_Bits_SetBitsU32(       (oC_uint32_t*)&GPIOIEV(Pins)->Value   , PINS(Pins));

                    break;
                case oC_GPIO_LLD_IntTrigger_LowLevel:
                    oC_Channel_EnableInterrupt(PORT(Pins) , PeripheralInterrupt);
                    oC_Bits_SetBitsU32(     (oC_uint32_t*)&GPIOIM(Pins)->Value    , PINS(Pins));

                    oC_Bits_SetBitsU32(       (oC_uint32_t*)&GPIOIS(Pins)->Value    , PINS(Pins));
                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIBE(Pins)->Value   , PINS(Pins));
                    oC_Bits_ClearBitsU32(     (oC_uint32_t*)&GPIOIEV(Pins)->Value   , PINS(Pins));

                    break;
                default:
                    errorCode = oC_ErrorCode_GPIOIntTriggerNotCorrect;
                    break;
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadInterruptTrigger( oC_Pins_t Pins , oC_GPIO_LLD_IntTrigger_t * outInterruptTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outInterruptTrigger),        oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            errorCode = oC_ErrorCode_None;
            if(
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIM(Pins)->Value , PINS(Pins)) ||
                    (oC_Channel_IsInterruptEnabled( PORT(Pins) , PeripheralInterrupt ) == false)
                    )
            {
                *outInterruptTrigger = oC_GPIO_LLD_IntTrigger_Off;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIS(Pins)->Value  , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOIBE(Pins)->Value , PINS(Pins))
                    )
            {
                *outInterruptTrigger = oC_GPIO_LLD_IntTrigger_BothEdges;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOIS(Pins)->Value  , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOIBE(Pins)->Value , PINS(Pins))
                    )
            {
                *outInterruptTrigger = oC_GPIO_LLD_IntTrigger_BothLevels;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIS(Pins)->Value  , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIBE(Pins)->Value , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIEV(Pins)->Value , PINS(Pins))
                    )
            {
                *outInterruptTrigger = oC_GPIO_LLD_IntTrigger_FallingEdge;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIS(Pins)->Value  , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIBE(Pins)->Value , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOIEV(Pins)->Value , PINS(Pins))
                    )
            {
                *outInterruptTrigger = oC_GPIO_LLD_IntTrigger_RisingEdge;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOIS(Pins)->Value  , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIBE(Pins)->Value , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOIEV(Pins)->Value , PINS(Pins))
                    )
            {
                *outInterruptTrigger = oC_GPIO_LLD_IntTrigger_HighLevel;
            }
            else if(
                    oC_Bits_IsAtLeastOneBitSetU32(   GPIOIS(Pins)->Value  , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIBE(Pins)->Value , PINS(Pins)) &&
                    oC_Bits_IsAtLeastOneBitClearU32( GPIOIEV(Pins)->Value , PINS(Pins))
                    )
            {
                *outInterruptTrigger = oC_GPIO_LLD_IntTrigger_LowLevel;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPinsUsed( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined)
            )
        {
            oC_Bits_SetBitsU8(&UsedPinsArray[PORT_INDEX(Pins)] , PINS(Pins));
            errorCode                        = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPinsUnused( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined)
            )
        {
            oC_Bits_ClearBitsU8(&UsedPinsArray[PORT_INDEX(Pins)] , PINS(Pins));
            errorCode                        = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_CheckIsPinUsed( oC_Pins_t Pins , bool * outPinUsed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( oC_GPIO_LLD_IsSinglePin(Pins),     oC_ErrorCode_NotSinglePin) &&
            ErrorCondition( IsRam(outPinUsed),                 oC_ErrorCode_OutputAddressNotInRAM)

            )
        {
            *outPinUsed = oC_Bits_AreBitsSetU32(UsedPinsArray[PORT_INDEX(Pins)] , PINS(Pins));
            errorCode   = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ArePinsUnused( oC_Pins_t Pins , bool * outPinsUnused )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outPinsUnused),              oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outPinsUnused = oC_Bits_AreBitsClearU32(UsedPinsArray[PORT_INDEX(Pins)] , PINS(Pins));
            errorCode       = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//=========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_WriteData( oC_Pins_t Pins , oC_PinsMask_t Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined)
            )
        {
            GPIODATA(Pins)->Value &= ~PINS(Pins);
            GPIODATA(Pins)->Value |= Data & PINS(Pins);
            errorCode              = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadData( oC_Pins_t Pins , oC_PinsMask_t * outData )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outData),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outData    = GPIODATA(Pins)->Value & PINS(Pins);
            errorCode   = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadDataReference( oC_Pins_t Pins , oC_UInt_t ** outDataReference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outDataReference),           oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outDataReference = (oC_UInt_t*)&GPIODATA(Pins)->Value;
            errorCode           = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_Pins_t oC_GPIO_LLD_GetHighStatePins( oC_Pins_t Pins )
{
    return oC_GPIO_LLD_GetPinsFor(PORT(Pins) , (oC_PinsMask_t)(GPIODATA(Pins)->Value & Pins));
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_Pins_t oC_GPIO_LLD_GetLowStatePins( oC_Pins_t Pins )
{
    return oC_GPIO_LLD_GetPinsFor(PORT(Pins) , (oC_PinsMask_t)(~GPIODATA(Pins)->Value) & Pins);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPinsState( oC_Pins_t Pins , oC_GPIO_LLD_PinsState_t ExpectedPinsState )
{
    return (GPIODATA(Pins)->Value & PINS(Pins)) == (ExpectedPinsState & PINS(Pins));
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
void oC_GPIO_LLD_SetPinsState( oC_Pins_t Pins , oC_GPIO_LLD_PinsState_t PinsState )
{
    GPIODATA(Pins)->Value = (GPIODATA(Pins)->Value & (~PINS(Pins))) | (PinsState & PINS(Pins) );
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
void oC_GPIO_LLD_TogglePinsState( oC_Pins_t Pins )
{
    GPIODATA(Pins)->Value ^= PINS(Pins);
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with machine specific functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________MSLLD_INTERFACE_FUNCTIONS_SECTION__________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_MSLLD_SetAlternateNumber( oC_Pin_t Pin , oC_PinAlternateNumber_t Function )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
                ErrorCondition(oC_GPIO_LLD_IsSinglePin(Pin)          , oC_ErrorCode_NotSinglePin) &&
                ErrorCondition(oC_GPIO_LLD_IsPinDefined(Pin)         , oC_ErrorCode_PinNotDefined) &&
                ErrorCondition( IsPoweredOn(Pin),                      oC_ErrorCode_ChannelNotPoweredOn) &&
                ErrorCondition(Function <= oC_PinAlternateNumberMask , oC_ErrorCode_ValueTooBig)
         )

        {
            oC_PinsMask_t pinIndex = oC_Bits_GetBitNumberU32(PINS(Pin));

            oC_MCS_EnterCriticalSection();

            oC_Bits_SetValueU32((oC_uint32_t*)&GPIOPCTL(Pin)->Value , Function , (pinIndex*4) , ((pinIndex+1)*4) - 1);

            oC_MCS_EnterCriticalSection();

            errorCode = oC_ErrorCode_None;
        }
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_MSLLD_ConnectPin( oC_Pin_t Pin , oC_PinAlternateNumber_t Function )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition(oC_GPIO_LLD_IsSinglePin(Pin)                                 , oC_ErrorCode_NotSinglePin) &&
            ErrorCondition(oC_GPIO_LLD_IsPinDefined(Pin)                                , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition(Function <= oC_PinAlternateNumberMask                        , oC_ErrorCode_ValueTooBig)
            )
        {

            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pin)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pin , oC_GPIO_LLD_Mode_Alternate)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_SetAlternateNumber(Pin , Function)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pin))
                )
            {
                errorCode = oC_ErrorCode_None;
            }

        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

#undef  _________________________________________MSLLD_INTERFACE_FUNCTIONS_SECTION__________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION________________________________________________________________________


//oC_Machine_DefineInterruptHandlersForChannels;
#define MODULE_NAME         GPIO
#define INTERRUPT_TYPE_NAME PeripheralInterrupt

//==========================================================================================================================================
/**
 * @brief common handler for GPIO interrupts
 */
//==========================================================================================================================================
oC_Channel_InterruptHandler(Channel)
{
        if(InterruptHandler)
        {
            oC_Pins_t pins = oC_GPIO_LLD_GetPinsFor(
                                            Channel ,
                                            oC_Machine_Register(Channel , GPIORIS)->Value
                                            );
            InterruptHandler(pins);
        }
}
#undef INTERRUPT_TYPE_NAME


#undef  _________________________________________INTERRUPTS_SECTION________________________________________________________________________

