/** ****************************************************************************************************************************************
 *
 * @file       oc_dma_lld.c
 *
 * @brief      The file with source code for DMA-LLD
 *
 * @author     Patryk Kubiak - (Created on: 28 08 2015 15:52:50)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#include <oc_dma_lld.h>
#include <oc_machine.h>
#include <oc_bits.h>
#include <oc_gpio_mslld.h>
#include <oc_machine_defs.h>
#include <oc_clock_lld.h>
#include <oc_dma_lld.h>
#include <oc_array.h>
#include <oc_mem_lld.h>
#include <oc_math.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define ControlTableEntry(ChannelIndex)              ((ControlTableEntry_t*)ControlTable)[ChannelIndex]
#define AlternateControlTableEntry(ChannelIndex)     ((ControlTableEntry_t*)ControlTable)[oC_DMA_LLD_ChannelIndex_NumberOfElements + ChannelIndex]

#define DMACFG(Channel)                              oC_Machine_Register(Channel,DMACFG)
#define DMACHMAP0(Channel)                           oC_Machine_Register(Channel,DMACHMAP0)
#define DMACHMAP1(Channel)                           oC_Machine_Register(Channel,DMACHMAP1)
#define DMACHMAP2(Channel)                           oC_Machine_Register(Channel,DMACHMAP2)
#define DMACHMAP3(Channel)                           oC_Machine_Register(Channel,DMACHMAP3)

#define DMASWREQ(Channel)                            oC_Machine_Register(Channel,DMASWREQ)
#define DMAENACLR(Channel)                           oC_Machine_Register(Channel,DMAENACLR)
#define DMAENASET(Channel)                           oC_Machine_Register(Channel,DMAENASET)
#define DMAPRIOSET(Channel)                          oC_Machine_Register(Channel,DMAPRIOSET)
#define DMAPRIOCLR(Channel)                          oC_Machine_Register(Channel,DMAPRIOCLR)
#define DMAALTCLR(Channel)                           oC_Machine_Register(Channel,DMAALTCLR)
#define DMAUSEBURSTCLR(Channel)                      oC_Machine_Register(Channel,DMAUSEBURSTCLR)
#define DMAREQMASKCLR(Channel)                       oC_Machine_Register(Channel,DMAREQMASKCLR)
#define DMACHIS(Channel)                             oC_Machine_Register(Channel,DMACHIS)




#define IsEventHandlerCorrect(Pointer)               (oC_MEM_LLD_IsFlashAddress(Pointer) || oC_MEM_LLD_IsRamAddress(Pointer) || (Pointer == NULL) )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief transfer modes
 */
//==========================================================================================================================================
typedef enum
{
    TransferMode_Stop                               = 0x0 ,//!< TransferMode_Stop
    TransferMode_Basic                              = 0x1 ,//!< TransferMode_Basic
    TransferMode_AutoRequest                        = 0x2 ,//!< TransferMode_AutoRequest
    TransferMode_PingPong                           = 0x3 ,//!< TransferMode_PingPong
    TransferMode_MemoryScatterGather                = 0x4 ,//!< TransferMode_MemoryScatterGather
    TransferMode_AlternateMemoryScatterGather       = 0x5 ,//!< TransferMode_AlternateMemoryScatterGather
    TransferMode_PeripheralScatterGather            = 0x6 ,//!< TransferMode_PeripheralScatterGather
    TransferMode_AlternatePeripheralScatterGather   = 0x7  //!< TransferMode_AlternatePeripheralScatterGather
} TransferMode_t;

//==========================================================================================================================================
/**
 * @brief size of data
 */
//==========================================================================================================================================
typedef enum
{
    DataSize_Byte       = 0 ,//!< DataSize_Byte
    DataSize_HalfWord   = 1 ,//!< DataSize_HalfWord
    DataSize_Word       = 2  //!< DataSize_Word
} DataSize_t;

//==========================================================================================================================================
/**
 * @brief size of incrementing address
 */
//==========================================================================================================================================
typedef enum
{
    IncrementSize_Byte          = 0 ,//!< IncrementSize_Byte
    IncrementSize_HalfWord      = 1 ,//!< IncrementSize_HalfWord
    IncrementSize_Word          = 2 ,//!< IncrementSize_Word
    IncrementSize_NoIcrement    = 3  //!< IncrementSize_NoIcrement
} IncrementSize_t;

//==========================================================================================================================================
/**
 * @brief number of transfers, that are possible without new transfer searching
 */
//==========================================================================================================================================
typedef enum
{
    ArbitrationSize_1Transfer               = 0x0 ,//!< DMA module will search higher priority transfer each transfer
    ArbitrationSize_2Transfers              = 0x1 ,//!< DMA module will search higher priority transfer each 2 transfers
    ArbitrationSize_4Transfers              = 0x2 ,//!< DMA module will search higher priority transfer each 4 transfers
    ArbitrationSize_8Transfers              = 0x3 ,//!< DMA module will search higher priority transfer each 8 transfers
    ArbitrationSize_16Transfers             = 0x4 ,//!< DMA module will search higher priority transfer each 16 transfers
    ArbitrationSize_32Transfers             = 0x5 ,//!< DMA module will search higher priority transfer each 32 transfers
    ArbitrationSize_64Transfers             = 0x6 ,//!< DMA module will search higher priority transfer each 64 transfers
    ArbitrationSize_128Transfers            = 0x7 ,//!< DMA module will search higher priority transfer each 128 transfers
    ArbitrationSize_256Transfers            = 0x8 ,//!< DMA module will search higher priority transfer each 256 transfers
    ArbitrationSize_512Transfers            = 0x9 ,//!< DMA module will search higher priority transfer each 512 transfers
    ArbitrationSize_1024Transfers           = 0xA ,//!< DMA module will search higher priority transfer each 1024 transfers
} ArbitrationSize_t;

//==========================================================================================================================================
/**
 * @brief use burst mode -> look at docs
 */
//==========================================================================================================================================
typedef enum
{
    NextUseBurst_DontUse ,//!< NextUseBurst_DontUse
    NextUseBurst_Use      //!< NextUseBurst_Use
} NextUseBurst_t;

//==========================================================================================================================================
/**
 * @brief Control word type
 *
 * The types is for storing control word type for each dma channel
 */
//==========================================================================================================================================
typedef union
{
    struct
    {
        oC_UInt_t   XFERMODE:3;
        oC_UInt_t   NXTUSEBURST:1;
        oC_UInt_t   XFERSIZE:10;
        oC_UInt_t   ARBSIZE:4;
        oC_UInt_t   __reserved_18_23:6;
        oC_UInt_t   SRCSIZE:2;
        oC_UInt_t   SRCINC:2;
        oC_UInt_t   DSTSIZE:2;
        oC_UInt_t   DSTINC:2;
    };
    oC_UInt_t   Data;
} DMACHCTL_t;

typedef struct
{
    void *      SourceEndPointer;
    void *      DestinationEndPointer;
    DMACHCTL_t  ControlWord;
    oC_UInt_t   Unused;
} ControlTableEntry_t;

#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void ConfigureControlWord(
                ControlTableEntry_t *   Entry ,
                TransferMode_t          TransferMode ,
                IncrementSize_t         DestinationIncrement ,
                DataSize_t              DestinationSize ,
                IncrementSize_t         SourceIncrement ,
                DataSize_t              SourceSize ,
                ArbitrationSize_t       ArbitrationSize ,
                oC_uint16_t             TransferSize ,
                NextUseBurst_t          NextUseBurst
                );

static inline bool                                  IsNumberOfTransfersCorrect( oC_UInt_t NumberOfTransfers );
static        oC_Machine_DmaChannelAssignment_t     GetSoftwareChannelAssignment( oC_DMA_Channel_t DmaChannel );
static        oC_Machine_DmaChannelAssignment_t     GetPeripheralChannelAssignment( oC_DMA_Channel_t DmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with constant variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________CONSTANT_SECTION___________________________________________________________________________


#undef  _________________________________________CONSTANT_SECTION___________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION___________________________________________________________________

static bool                         ModuleEnabledFlag = false;
static bool                         ChannelUsedArray[oC_ModuleChannel_NumberOfElements(DMA)];
static oC_DMA_LLD_EventHandler_t    EventHandlersArray[oC_ModuleChannel_NumberOfElements(DMA)];
static oC_UInt_t                    ControlTable[1024] __attribute__ ((aligned(1024)));

#undef  _________________________________________LOCAL_VARIABLES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , !ModuleEnabledFlag , oC_ErrorCode_ModuleIsTurnedOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_EnableInterrupt( oC_DMA_Channel_uDMA0 , Software),  oC_ErrorCode_CannotEnableInterrupt)
        )
    {
        oC_DMA_LLD_ForeachChannel(Channel)
        {
            oC_DMA_LLD_RestoreDefaultStateOnChannel(Channel);
        }

        ModuleEnabledFlag = true;
        errorCode         = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag , oC_ErrorCode_ModuleNotStartedYet )
        )
    {
        ModuleEnabledFlag = false;

        /* Mark each channel as used to prevent some additional problems */
        oC_DMA_LLD_ForeachChannel(Channel)
        {
            oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(DMA,Channel);
            ChannelUsedArray[channelIndex] = true;
        }

        /* Turn off the uDMA0, because all DMA channels has only one register map */
        DMACFG(oC_DMA_Channel_uDMA0)->MASTEN = 0;
        oC_Machine_SetPowerStateForChannel(oC_DMA_Channel_uDMA0, oC_Power_Off);
        oC_Channel_DisableInterrupt( oC_DMA_Channel_uDMA0 , Software );

        errorCode         = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsChannelAvailable( oC_DMA_Channel_t Channel )
{
    return ChannelUsedArray[oC_Channel_ToIndex(DMA,Channel)] == false;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
void oC_DMA_LLD_RestoreDefaultStateOnChannel( oC_DMA_Channel_t Channel )
{
    if(ModuleEnabledFlag && oC_Channel_IsCorrect(DMA,Channel))
    {
        oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(DMA,Channel);

        ChannelUsedArray[oC_Channel_ToIndex(DMA,Channel)]   = false;
        EventHandlersArray[oC_Channel_ToIndex(DMA,Channel)] = NULL;

        ControlTableEntry(channelIndex).ControlWord.Data = 0;

        /* Start the transfer */
        oC_Bits_SetBitU32( (uint32_t*) &DMAENACLR(Channel)->Value, channelIndex );
        oC_Bits_SetBitU32( (uint32_t*) &DMASWREQ(Channel)->Value, channelIndex );

    }
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsChannelSupportedOnDmaChannel( oC_DMA_Channel_t DmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType )
{
    bool registerMapSupported = false;

    oC_ARRAY_FOREACH_IN_ARRAY(oC_DmaChannelAssignments,DmaChannelAssignment)
    {
        if(
            (oC_Machine_DmaChannelAssignment_GetDmaChannel( *DmaChannelAssignment) == DmaChannel          )  &&
            (oC_Machine_DmaChannelAssignment_GetChannel(    *DmaChannelAssignment) == Channel )  &&
            (oC_Machine_DmaChannelAssignment_GetDmaSignalType(          *DmaChannelAssignment) == SignalType       )
            )
        {
            registerMapSupported = true;
            oC_ARRAY_FOREACH_BREAK(DmaChannelAssignmentsArray);
        }
    }

    return registerMapSupported;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsSoftwareTradeSupportedOnDmaChannel( oC_DMA_Channel_t DmaChannel )
{
    bool softwareTradeSupported = false;

    oC_ARRAY_FOREACH_IN_ARRAY(oC_DmaChannelAssignments,DmaChannelAssignment)
    {
        if(
            (oC_Machine_DmaChannelAssignment_GetDmaChannel( *DmaChannelAssignment) == DmaChannel                      )  &&
            (oC_Machine_DmaChannelAssignment_GetChannel(    *DmaChannelAssignment) == oC_Channel_Software )
            )
        {
            softwareTradeSupported = true;
            oC_ARRAY_FOREACH_BREAK(DmaChannelAssignmentsArray);
        }
    }

    return softwareTradeSupported;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_DoesDmaHasAccessToAddress( oC_DMA_Channel_t Channel , const void * Address )
{
    return oC_MEM_LLD_IsRamAddress(Address);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_ConfigureSoftwareTrade( oC_DMA_Channel_t Channel , const oC_DMA_LLD_SoftwareTradeConfig_t * Config )
{
    oC_ErrorCode_t              errorCode               = oC_ErrorCode_ImplementError;
    oC_Machine_DmaChannelAssignment_t   dmaChannelAssignment    = GetSoftwareChannelAssignment(Channel);
    oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(DMA,Channel);

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                                      oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(DMA,Channel),                                oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , ChannelUsedArray[channelIndex] == false,                                 oC_ErrorCode_ChannelIsUsed ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , dmaChannelAssignment != 0,                                               oC_ErrorCode_DMASoftwareTransferNotPossibleOnThisChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(Config) || oC_MEM_LLD_IsFlashAddress(Config) ,   oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsEventHandlerCorrect(Config->TransferCompleteEventHandler) ,            oC_ErrorCode_WrongEventHandlerAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_DMA_LLD_DoesDmaHasAccessToAddress(Channel,Config->Source) ,           oC_ErrorCode_DMAAddressNotHandledByDma ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_DMA_LLD_DoesDmaHasAccessToAddress(Channel,Config->Destination) ,      oC_ErrorCode_DMAAddressNotHandledByDma ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Config->ElementSize <= oC_DMA_LLD_ElementSize_Word   ,                   oC_ErrorCode_DMAElementSizeNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsNumberOfTransfersCorrect(Config->BufferSize/(1<<Config->ElementSize)), oC_ErrorCode_SizeNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_DMA_LLD_IsSoftwareTradeSupportedOnDmaChannel(Channel) ,                  oC_ErrorCode_DMASoftwareTransferNotPossibleOnThisChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Machine_SetPowerStateForChannel(Channel,oC_Power_On) ,                 oC_ErrorCode_CannotEnableChannel )
        )
    {
        oC_UInt_t       numberOfTransfers = Config->BufferSize/(1<<Config->ElementSize);
        IncrementSize_t incrementSize     = (numberOfTransfers > 1) ? Config->ElementSize : IncrementSize_NoIcrement;

        ChannelUsedArray[channelIndex] = true;

        DMACFG(Channel)->MASTEN = 1;
        oC_Machine_WriteRegister( Channel , DMACTLBASE,(uint32_t)ControlTable);

        if(Config->Priority == oC_DMA_LLD_Priority_High)
        {
            oC_Bits_SetBitU32( (uint32_t*) &DMAPRIOSET(Channel)->Value, channelIndex );
        }
        else
        {
            oC_Bits_SetBitU32( (uint32_t*) &DMAPRIOCLR(Channel)->Value, channelIndex );
        }

        oC_Bits_SetBitU32( (uint32_t*) &DMAALTCLR(Channel)->Value, channelIndex );
        oC_Bits_SetBitU32( (uint32_t*) &DMAUSEBURSTCLR(Channel)->Value, channelIndex );
        oC_Bits_SetBitU32( (uint32_t*) &DMAREQMASKCLR(Channel)->Value, channelIndex );

        ControlTableEntry(channelIndex).SourceEndPointer      = (void*)((oC_UInt_t)Config->Source      + Config->BufferSize - (1<<Config->ElementSize));
        ControlTableEntry(channelIndex).DestinationEndPointer = (void*)((oC_UInt_t)Config->Destination + Config->BufferSize - (1<<Config->ElementSize));

        ConfigureControlWord(&ControlTableEntry(channelIndex),
                             TransferMode_AutoRequest ,
                             incrementSize,
                             Config->ElementSize ,
                             incrementSize ,
                             Config->ElementSize ,
                             ArbitrationSize_1Transfer ,
                             numberOfTransfers ,
                             NextUseBurst_DontUse
                             );

        EventHandlersArray[channelIndex] = Config->TransferCompleteEventHandler;

        uint32_t * DMACHMAP;

        if(channelIndex < 8)
        {
            DMACHMAP = (uint32_t*)&DMACHMAP0(Channel)->Value;
        }
        else if(channelIndex < 16)
        {
            DMACHMAP = (uint32_t*)&DMACHMAP1(Channel)->Value;
        }
        else if(channelIndex < 24)
        {
            DMACHMAP = (uint32_t*)&DMACHMAP2(Channel)->Value;
        }
        else
        {
            DMACHMAP = (uint32_t*)&DMACHMAP3(Channel)->Value;
        }

        oC_Bits_SetValueU32(
                        DMACHMAP ,
                        oC_Machine_DmaChannelAssignment_GetDmaEncodingValue(dmaChannelAssignment),
                         channelIndex    * 4 ,
                        (channelIndex+1) * 4 - 1);

        /* Start the transfer */
        oC_Bits_SetBitU32( (uint32_t*) &DMAENASET(Channel)->Value, channelIndex );
        oC_Bits_ClearBitU32( (uint32_t*) &DMASWREQ(Channel)->Value, channelIndex );

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_ConfigurePeripheralTrade( oC_DMA_Channel_t Channel , const oC_DMA_LLD_PeripheralTradeConfig_t * Config )
{
    oC_ErrorCode_t            errorCode            = oC_ErrorCode_ImplementError;
    oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(DMA,Channel);
    oC_Machine_DmaChannelAssignment_t dmaChannelAssignment = GetPeripheralChannelAssignment(Channel , Config->PeripheralChannel , Config->SignalType);

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                                      oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(DMA,Channel) ,                               oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , ChannelUsedArray[channelIndex] == false,                                 oC_ErrorCode_ChannelIsUsed ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(Config) || oC_MEM_LLD_IsFlashAddress(Config) ,   oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , dmaChannelAssignment != 0,                                               oC_ErrorCode_DMAPeripheralTransferNotPossibleOnThisChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsEventHandlerCorrect(Config->TransferCompleteEventHandler) ,            oC_ErrorCode_WrongEventHandlerAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_DMA_LLD_DoesDmaHasAccessToAddress(Channel,Config->Buffer) ,           oC_ErrorCode_DMAAddressNotHandledByDma ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Config->ElementSize <= oC_DMA_LLD_ElementSize_Word   ,                   oC_ErrorCode_DMAElementSizeNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Config->TransmitDirection <= oC_DMA_LLD_Direction_Receive   ,            oC_ErrorCode_DMATransmitDirectionNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsNumberOfTransfersCorrect(Config->BufferSize/(1<<Config->ElementSize)), oC_ErrorCode_DMABufferSizeMustBePowerOf2) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_DMA_LLD_IsSoftwareTradeSupportedOnDmaChannel(Channel) ,                  oC_ErrorCode_DMASoftwareTransferNotPossibleOnThisChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Machine_SetPowerStateForChannel(Channel,oC_Power_On) ,                 oC_ErrorCode_CannotEnableChannel )
        )
    {
        oC_UInt_t       numberOfTransfers       = Config->BufferSize/(1<<Config->ElementSize);
        IncrementSize_t sourceIncrementSize     = (numberOfTransfers > 1) ? Config->ElementSize : IncrementSize_NoIcrement;
        IncrementSize_t destinationIncrementSize= (numberOfTransfers > 1) ? Config->ElementSize : IncrementSize_NoIcrement;

        ChannelUsedArray[channelIndex] = true;

        DMACFG(Channel)->MASTEN = 1;
        oC_Machine_WriteRegister( Channel , DMACTLBASE,(uint32_t)ControlTable);

        if(Config->Priority == oC_DMA_LLD_Priority_High)
        {
            oC_Bits_SetBitU32( (uint32_t*) &DMAPRIOSET(Channel)->Value, channelIndex );
        }
        else
        {
            oC_Bits_SetBitU32( (uint32_t*) &DMAPRIOCLR(Channel)->Value, channelIndex );
        }

        oC_Bits_SetBitU32( (uint32_t*) &DMAALTCLR(Channel)->Value, channelIndex );
        oC_Bits_SetBitU32( (uint32_t*) &DMAUSEBURSTCLR(Channel)->Value, channelIndex );
        oC_Bits_SetBitU32( (uint32_t*) &DMAREQMASKCLR(Channel)->Value, channelIndex );

        if(Config->TransmitDirection == oC_DMA_LLD_Direction_Transmit)
        {
            ControlTableEntry(channelIndex).SourceEndPointer      = (void*)((oC_UInt_t)Config->Buffer + Config->BufferSize - (1<<Config->ElementSize));
            ControlTableEntry(channelIndex).DestinationEndPointer = Config->PeripheralData;
            destinationIncrementSize = IncrementSize_NoIcrement;
        }
        else
        {
            ControlTableEntry(channelIndex).SourceEndPointer      = Config->PeripheralData;
            ControlTableEntry(channelIndex).DestinationEndPointer = (void*)((oC_UInt_t)Config->Buffer + Config->BufferSize - (1<<Config->ElementSize));
            sourceIncrementSize = IncrementSize_NoIcrement;
        }


        ConfigureControlWord(&ControlTableEntry(channelIndex),
                             TransferMode_Basic ,
                             destinationIncrementSize,
                             Config->ElementSize ,
                             sourceIncrementSize ,
                             Config->ElementSize ,
                             ArbitrationSize_1Transfer ,
                             numberOfTransfers ,
                             NextUseBurst_DontUse
                             );

        EventHandlersArray[channelIndex] = Config->TransferCompleteEventHandler;

        uint32_t * DMACHMAP;

        if(channelIndex < 8)
        {
            DMACHMAP = (uint32_t*)&DMACHMAP0(Channel)->Value;
        }
        else if(channelIndex < 16)
        {
            DMACHMAP = (uint32_t*)&DMACHMAP1(Channel)->Value;
        }
        else if(channelIndex < 24)
        {
            DMACHMAP = (uint32_t*)&DMACHMAP2(Channel)->Value;
        }
        else
        {
            DMACHMAP = (uint32_t*)&DMACHMAP3(Channel)->Value;
        }

        oC_Bits_SetValueU32(
                        DMACHMAP ,
                        oC_Machine_DmaChannelAssignment_GetDmaEncodingValue(dmaChannelAssignment),
                         channelIndex    * 4 ,
                        (channelIndex+1) * 4 - 1);

        /* Start the transfer */
        oC_Bits_SetBitU32( (uint32_t*) &DMAENASET(Channel)->Value, channelIndex );
        oC_Bits_ClearBitU32( (uint32_t*) &DMASWREQ(Channel)->Value, channelIndex );

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsTransferCompleteOnChannel( oC_DMA_Channel_t Channel )
{
    oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(DMA,Channel);

    return ModuleEnabledFlag && oC_Channel_IsCorrect(DMA,Channel) && (ChannelUsedArray[channelIndex] == false);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_ReadChannelUsedReference( oC_DMA_Channel_t Channel , bool ** outChannelUsedFlag )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                               oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(DMA,Channel),         oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outChannelUsedFlag),      oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(DMA,Channel);

        *outChannelUsedFlag = &ChannelUsedArray[channelIndex];

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade( oC_DMA_Channel_t * outDmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag , oC_ErrorCode_ModuleNotStartedYet)
        )
    {
        errorCode = oC_ErrorCode_NoChannelAvailable;

        oC_ARRAY_FOREACH_IN_ARRAY(oC_DmaChannelAssignments,DmaChannelAssignment)
        {
            if(
                (oC_Machine_DmaChannelAssignment_GetChannel(       *DmaChannelAssignment) == Channel )  &&
                (oC_Machine_DmaChannelAssignment_GetDmaSignalType( *DmaChannelAssignment) == SignalType       )
                )
            {
                *outDmaChannel  = oC_Machine_DmaChannelAssignment_GetDmaChannel(*DmaChannelAssignment);
                errorCode       = oC_ErrorCode_None;
                oC_ARRAY_FOREACH_BREAK(oC_DmaChannelAssignments);
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief configures control word
 */
//==========================================================================================================================================
static void ConfigureControlWord(
                ControlTableEntry_t *   Entry ,
                TransferMode_t          TransferMode ,
                IncrementSize_t         DestinationIncrement ,
                DataSize_t              DestinationSize ,
                IncrementSize_t         SourceIncrement ,
                DataSize_t              SourceSize ,
                ArbitrationSize_t       ArbitrationSize ,
                oC_uint16_t             TransferSize ,
                NextUseBurst_t          NextUseBurst
                )
{
    Entry->ControlWord.XFERMODE     = TransferMode;
    Entry->ControlWord.NXTUSEBURST  = NextUseBurst;
    Entry->ControlWord.XFERSIZE     = TransferSize - 1;
    Entry->ControlWord.ARBSIZE      = ArbitrationSize;
    Entry->ControlWord.SRCSIZE      = SourceSize;
    Entry->ControlWord.SRCINC       = SourceIncrement;
    Entry->ControlWord.DSTSIZE      = DestinationSize;
    Entry->ControlWord.DSTINC       = DestinationIncrement;
}

//==========================================================================================================================================
/**
 * @brief checks if number of transfers is correct
 */
//==========================================================================================================================================
static inline bool IsNumberOfTransfersCorrect( oC_UInt_t NumberOfTransfers )
{
    return (NumberOfTransfers > 0) && (NumberOfTransfers <= 1024);
}

//==========================================================================================================================================
/**
 * @brief returns software DMA channel assignment
 */
//==========================================================================================================================================
static oC_Machine_DmaChannelAssignment_t GetSoftwareChannelAssignment( oC_DMA_Channel_t DmaChannel )
{
    oC_Machine_DmaChannelAssignment_t channelAssignment = 0;

    oC_ARRAY_FOREACH_IN_ARRAY(oC_DmaChannelAssignments,DmaChannelAssignment)
    {
        if(
            (oC_Machine_DmaChannelAssignment_GetDmaChannel( *DmaChannelAssignment) == DmaChannel)                        &&
            (oC_Machine_DmaChannelAssignment_GetChannel(    *DmaChannelAssignment) == oC_Channel_Software )
            )
        {
            channelAssignment = *DmaChannelAssignment;
            oC_ARRAY_FOREACH_BREAK(DmaChannelAssignmentsArray);
        }
    }

    return channelAssignment;
}

//==========================================================================================================================================
/**
 * @brief returns DMA channel assignments for peripheral
 */
//==========================================================================================================================================
static oC_Machine_DmaChannelAssignment_t     GetPeripheralChannelAssignment( oC_DMA_Channel_t DmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType )
{
    oC_Machine_DmaChannelAssignment_t channelAssignment = 0;

    oC_ARRAY_FOREACH_IN_ARRAY(oC_DmaChannelAssignments,DmaChannelAssignment)
    {
        if(
                (oC_Machine_DmaChannelAssignment_GetDmaChannel( *DmaChannelAssignment) == DmaChannel)  &&
                (oC_Machine_DmaChannelAssignment_GetChannel(    *DmaChannelAssignment) == Channel )  &&
                (oC_Machine_DmaChannelAssignment_GetDmaSignalType(          *DmaChannelAssignment) == SignalType       )
            )
        {
            channelAssignment = *DmaChannelAssignment;
            oC_ARRAY_FOREACH_BREAK(DmaChannelAssignmentsArray);
        }
    }

    return channelAssignment;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_INTERRUPTS_SECTION___________________________________________________________________

//==========================================================================================================================================
/**
 * @brief common interrupt for all uDMA channels
 */
//==========================================================================================================================================

//TODO: error if use oC_Channel_InterruptHandler
oC_InterruptHandler(uDMA0 , Software)
{
    uint32_t intStatus    = oC_Machine_ReadRegister(oC_DMA_Channel_uDMA0 , DMACHIS);

    while(intStatus)
    {
        oC_ChannelIndex_t channelIndex = oC_Bits_GetBitNumberU32(intStatus);
        oC_DMA_Channel_t channel = oC_Channel_ToIndex(DMA,channelIndex);
        oC_Bits_ClearBitU32(&intStatus, channelIndex);
        oC_Bits_SetBitU32( (uint32_t*) &DMACHIS(oC_DMA_Channel_uDMA0)->Value, channelIndex );


        if(EventHandlersArray[channelIndex])
        {
            EventHandlersArray[channelIndex](channel);
        }
        oC_DMA_LLD_RestoreDefaultStateOnChannel(channel);
    }

}

#undef  _________________________________________LOCAL_INTERRUPTS_SECTION___________________________________________________________________
