/** ****************************************************************************************************************************************
 *
 * @file       oc_clock_lld.c
 *
 * @brief      The file with interface functions for the CLOCK-LLD module.
 *
 * @author     Patryk Kubiak - (Created on: 25 06 2015 17:41:45)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_clock_lld.h>
#include <oc_mem_lld.h>
#include <oc_registers.h>
#include <oc_module.h>
#include <oc_mcs.h>
#include <oc_lsf.h>
#include <oc_sys_lld.h>
#include <oc_interrupts.h>
#include <oc_math.h>



/** ========================================================================================================================================
 *
 *              The section with local definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define RegisterRCC     oC_Register( SystemControl , RCC  )
#define RegisterRCC2    oC_Register( SystemControl , RCC2 )
#define RegisterRIS     oC_Register( SystemControl , RIS  )
#define MAX_FREQUENCY   oC_MACHINE_MAXIMUM_FREQUENCY

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(Address)              oC_LSF_IsRamAddress(Address)
#define IsRom(Address)              oC_LSF_IsRomAddress(Address)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    OscSource_MainOsc                   = 0x0 ,
    OscSource_PrecisionInternalOsc      = 0x1 ,
    OscSource_PrecisionInternalOscDiv4  = 0x2 ,
    OscSource_InternalOsc               = 0x3 ,
    OscSource_HibernationOsc            = 0x7
} OscSource_t;

typedef enum
{
    Pll_DontUse ,
    Pll_200MHz ,
    Pll_400MHz
} Pll_t;

typedef enum
{
    Xtal_NotCorrect     = 0 ,
    Xtal_4MHz           = 0x06 ,
    Xtal_4MHz096kHz     = 0x07 ,
    Xtal_4MHz915200Hz   = 0x08 ,
    Xtal_5MHz           = 0x09 ,
    Xtal_5MHz120kHz     = 0x0A ,
    Xtal_6MHz           = 0x0B ,
    Xtal_6MHz144kHz     = 0x0C ,
    Xtal_7MHz372800Hz   = 0x0D ,
    Xtal_8MHz           = 0x0E ,
    Xtal_8MHz192kHz     = 0x0F ,
    Xtal_10MHz          = 0x10 ,
    Xtal_12MHz          = 0x11 ,
    Xtal_12MHz288kHz    = 0x12 ,
    Xtal_13MHz560kHz    = 0x13 ,
    Xtal_14MHz318180Hz  = 0x14 ,
    Xtal_16MHz          = 0x15 ,
    Xtal_16MHz384kHz    = 0x16 ,
    Xtal_18MHz          = 0x17 ,
    Xtal_20MHz          = 0x18 ,
    Xtal_24MHz          = 0x19 ,
    Xtal_25MHz          = 0x1A
} Xtal_t;

typedef enum
{
    SysDiv_1    = 0x0 ,
    SysDiv_2    = 0x1 ,
    SysDiv_3    = 0x2 ,
    SysDiv_4    = 0x3 ,
    SysDiv_5    = 0x4 ,
    SysDiv_6    = 0x5 ,
    SysDiv_7    = 0x6 ,
    SysDiv_8    = 0x7 ,
    SysDiv_9    = 0x8 ,
    SysDiv_10   = 0x9 ,
    SysDiv_11   = 0xA ,
    SysDiv_12   = 0xB ,
    SysDiv_13   = 0xC ,
    SysDiv_14   = 0xD ,
    SysDiv_15   = 0xE ,
    SysDiv_16   = 0xF ,
    SysDiv_64   = 0x3F ,
    SysDiv_128  = 0x7F
} SysDiv_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool   ConfigureClock            ( OscSource_t OscSource , Xtal_t Xtal , Pll_t Pll , SysDiv_t SysDiv );
static Xtal_t GetXtalForFrequency       ( oC_Frequency_t Frequency , oC_Frequency_t AcceptableDifference );
static bool   FindDivisor               ( oC_Frequency_t Frequency , oC_Frequency_t Difference , oC_Frequency_t OscillatorFrequency , Pll_t Pll , SysDiv_t * outSysDiv );
static void   CallClockConfiguredEvent  ( void );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_Frequency_t               CurrentFrequency    = oC_MACHINE_PRECISION_INTERNAL_OSCILLATOR_FREQUENCY;
static oC_CLOCK_LLD_ClockSource_t   ClockSource         = oC_CLOCK_LLD_ClockSource_Internal;
static oC_CLOCK_LLD_Interrupt_t     ClockConfiguredHandler  = NULL;
static const oC_Frequency_t         XtalFrequencyArray[]= {
 [Xtal_4MHz           ] =  4000000 ,
 [Xtal_4MHz096kHz     ] =  4096000 ,
 [Xtal_4MHz915200Hz   ] =  4915200 ,
 [Xtal_5MHz           ] =  5000000 ,
 [Xtal_5MHz120kHz     ] =  5120000 ,
 [Xtal_6MHz           ] =  6000000 ,
 [Xtal_6MHz144kHz     ] =  6144000 ,
 [Xtal_7MHz372800Hz   ] =  7372800 ,
 [Xtal_8MHz           ] =  8000000 ,
 [Xtal_8MHz192kHz     ] =  8192000 ,
 [Xtal_10MHz          ] = 10000000 ,
 [Xtal_12MHz          ] = 12000000 ,
 [Xtal_12MHz288kHz    ] = 12288000 ,
 [Xtal_13MHz560kHz    ] = 13560000 ,
 [Xtal_14MHz318180Hz  ] = 14318180 ,
 [Xtal_16MHz          ] = 16000000 ,
 [Xtal_16MHz384kHz    ] = 16384000 ,
 [Xtal_18MHz          ] = 18000000 ,
 [Xtal_20MHz          ] = 20000000 ,
 [Xtal_24MHz          ] = 24000000 ,
 [Xtal_25MHz          ] = 25000000
};
static const oC_Frequency_t         InternalOscilatorsFrequenciesArray[] = {
 [OscSource_PrecisionInternalOsc      ] = oC_MACHINE_PRECISION_INTERNAL_OSCILLATOR_FREQUENCY,
 [OscSource_PrecisionInternalOscDiv4  ] = oC_MACHINE_PRECISION_INTERNAL_OSCILLATOR_FREQUENCY / 4,
 [OscSource_InternalOsc               ] = oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup CLOCK-LLD
//! @{


//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        /* ************************* NOTE ******************************
         * ClockSource and ClockFrequency is specially not initialized *
         * to not provide wrong informations about state of clock      *
         * configuration                                               *
         * *************************************************************/

        CurrentFrequency = oC_Machine_DefaultFrequency;
        ClockSource      = oC_CLOCK_LLD_ClockSource_Internal;
        ClockConfiguredHandler = NULL;

        oC_Module_TurnOn(oC_Module_CLOCK_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {

        oC_Module_TurnOff(oC_Module_CLOCK_LLD);
        errorCode = oC_CLOCK_LLD_ConfigureInternalClock(oC_Machine_DefaultFrequency,0);

        if(errorCode != oC_ErrorCode_None)
        {
            errorCode = oC_ErrorCode_CannotRestoreDefaultState;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_CLOCK_LLD_ClockSource_t oC_CLOCK_LLD_GetClockSource( void )
{
    return ClockSource;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_Frequency_t oC_CLOCK_LLD_GetClockFrequency( void )
{
    return CurrentFrequency;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
oC_Frequency_t oC_CLOCK_LLD_GetMaximumClockFrequency( void )
{
    return MAX_FREQUENCY;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_SetClockConfiguredInterrupt( oC_CLOCK_LLD_Interrupt_t Interrupt )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        if(
        oC_AssignErrorCodeIfFalse(&errorCode , ClockConfiguredHandler == NULL       , oC_ErrorCode_InterruptHandlerAlreadySet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRom(Interrupt) || IsRam(Interrupt) , oC_ErrorCode_WrongEventHandlerAddress )
        )
        {
            if( oC_MEM_LLD_IsFlashAddress(Interrupt) || oC_MEM_LLD_IsRamAddress(Interrupt) )
            {
                oC_MCS_EnterCriticalSection();
                ClockConfiguredHandler  = Interrupt;
                errorCode        = oC_ErrorCode_None;
                oC_MCS_ExitCriticalSection();
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_CLOCK_LLD_DelayForMicroseconds( oC_UInt_t Microseconds )
{
    oC_UInt_t numberOfCyclesPerMicrosecond = CurrentFrequency/1000000UL;

    oC_MCS_Delay(numberOfCyclesPerMicrosecond*Microseconds);

    return true;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_ConfigureInternalClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    Xtal_t         xtal      = GetXtalForFrequency(oC_Machine_DefaultFrequency , 0);

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency > 0 && PermissibleDifference >= 0 , oC_ErrorCode_WrongFrequency)
            )
        {
            oC_MCS_EnterCriticalSection();
            errorCode = oC_ErrorCode_FrequencyNotPossible;

            for(OscSource_t oscSource =OscSource_PrecisionInternalOsc;
                            (oscSource <= OscSource_InternalOsc) && (errorCode != oC_ErrorCode_None)
                                                    ;oscSource++)
            {
                for(Pll_t pll = Pll_DontUse ; pll <= Pll_400MHz ; pll++)
                {
                    SysDiv_t sysDiv = SysDiv_1;

                    if(FindDivisor(TargetFrequency,PermissibleDifference,InternalOscilatorsFrequenciesArray[oscSource],pll,&sysDiv))
                    {
                        if(ConfigureClock(oscSource , xtal , pll , sysDiv))
                        {
                            switch(pll)
                            {
                                case Pll_DontUse: CurrentFrequency = InternalOscilatorsFrequenciesArray[oscSource] / (sysDiv + 1); break;
                                case Pll_200MHz:  CurrentFrequency = oC_MHz(200) / (sysDiv + 1); break;
                                case Pll_400MHz:  CurrentFrequency = oC_MHz(400) / (sysDiv + 1); break;
                            }
                            ClockSource         = oC_CLOCK_LLD_ClockSource_Internal;
                            errorCode           = oC_ErrorCode_None;
                            CallClockConfiguredEvent();
                            break;
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_ClockConfigurationError;
                        }
                    }

                    /* Only the precision internal oscillator can be configured with PLL */
                    if(oscSource != OscSource_PrecisionInternalOsc)
                    {
                        break;
                    }
                }
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_ConfigureExternalClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference , oC_Frequency_t OscillatorFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    Xtal_t         xtal      = GetXtalForFrequency(OscillatorFrequency , 0);

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
     {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency > 0 && OscillatorFrequency > 0 && PermissibleDifference >= 0 , oC_ErrorCode_WrongFrequency) &&
            oC_AssignErrorCodeIfFalse(&errorCode , xtal != Xtal_NotCorrect    , oC_ErrorCode_UnsupportedOscillator)
            )
        {
            errorCode = oC_ErrorCode_FrequencyNotPossible;

            oC_MCS_EnterCriticalSection();

            for(Pll_t pll = Pll_DontUse ; pll <= Pll_400MHz ; pll++)
            {
                SysDiv_t sysDiv = SysDiv_1;

                if(FindDivisor(TargetFrequency,PermissibleDifference,OscillatorFrequency,pll,&sysDiv))
                {
                    if(ConfigureClock(OscSource_MainOsc , xtal , pll , sysDiv))
                    {
                        switch(pll)
                        {
                            case Pll_DontUse: CurrentFrequency = OscillatorFrequency / (sysDiv + 1); break;
                            case Pll_200MHz:  CurrentFrequency = oC_MHz(200) / (sysDiv + 1);         break;
                            case Pll_400MHz:  CurrentFrequency = oC_MHz(400) / (sysDiv + 1);         break;
                        }

                        ClockSource = oC_CLOCK_LLD_ClockSource_External;
                        errorCode   = oC_ErrorCode_None;
                        CallClockConfiguredEvent();
                        break;
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_ClockConfigurationError;
                    }
                }
            }

            oC_MCS_ExitCriticalSection();

        }
     }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_ConfigureHibernationClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference , oC_Frequency_t OscillatorFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency > 0 && OscillatorFrequency > 0 && PermissibleDifference >= 0 , oC_ErrorCode_WrongFrequency) &&
            oC_AssignErrorCodeIfFalse(&errorCode , OscillatorFrequency == oC_MACHINE_HIBERNATION_OSCILLATOR_FREQUENCY , oC_ErrorCode_UnsupportedOscillator)
            )
        {
            /* XTal is set to default, because there is not any information about this in documentation */
            Xtal_t   xtal   = GetXtalForFrequency(oC_Machine_DefaultFrequency , 0);
            SysDiv_t sysDiv = SysDiv_1;

            if(FindDivisor(TargetFrequency,PermissibleDifference,OscillatorFrequency,Pll_DontUse,&sysDiv))
            {
                if(ConfigureClock(OscSource_HibernationOsc , xtal , Pll_DontUse , sysDiv))
                {
                    CurrentFrequency = OscillatorFrequency / (sysDiv + 1);
                    ClockSource      = oC_CLOCK_LLD_ClockSource_External;
                    errorCode        = oC_ErrorCode_None;
                    CallClockConfiguredEvent();
                }
                else
                {
                    errorCode = oC_ErrorCode_ClockConfigurationError;
                }
            }
            else
            {
                errorCode = oC_ErrorCode_FrequencyNotPossible;
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * Configures clock, to more details look at the machine documentation.
 */
//==========================================================================================================================================
static bool ConfigureClock( OscSource_t OscSource , Xtal_t Xtal , Pll_t Pll , SysDiv_t SysDiv )
{
    bool        success   = true;
    oC_RegisterType_(RCC)   rcc;
    oC_RegisterType_(RCC2)  rcc2;

    rcc.Value  = RegisterRCC->Value;
    rcc2.Value = RegisterRCC2->Value;

    /*
     * Bypass the PLL and system clock divider by setting the BYPASS bit and clearing the USESYS
     * bit in the RCC register, thereby configuring the microcontroller to run off a "raw" clock source
     * and allowing for the new PLL configuration to be validated before switching the system clock
     * to the PLL.
     */
    rcc.BYPASS     = 1;
    rcc.USESYSDIV  = 0;

    RegisterRCC->Value = rcc.Value;

    /*
     * Select the crystal value (XTAL) and oscillator source (OSCSRC), and clear the PWRDN bit in
     * RCC/RCC2. Setting the XTAL field automatically pulls valid PLL configuration data for the
     * appropriate crystal, and clearing the PWRDN bit powers and enables the PLL and its output.
     */
    rcc.XTAL   = Xtal;

    if(OscSource < OscSource_HibernationOsc)
    {
        rcc.OSCSRC   = OscSource;
        rcc2.USERCC2 = 1;
    }
    rcc2.OSCSRC2 = OscSource;

    rcc.PWRDN   = 0;
    rcc2.PWRDN2 = 0;

    RegisterRCC->Value  = rcc.Value;
    RegisterRCC2->Value = rcc2.Value;

    /*
     * Select the desired system divider (SYSDIV) in RCC/RCC2 and set the USESYS bit in RCC. The
     * SYSDIV field determines the system frequency for the microcontroller
     */
    if(Pll == Pll_DontUse)
    {
        if(SysDiv <= SysDiv_16)
        {
            rcc.SYSDIV     = SysDiv;
            rcc2.SYSDIV2   = SysDiv;
        }
        else if(SysDiv <= SysDiv_64)
        {
            rcc2.SYSDIV2   = SysDiv;
            rcc2.USERCC2   = 1;
        }
        else
        {
            success = false;
        }
    }
    else if(Pll == Pll_200MHz)
    {
        rcc2.DIV400 = 0;

        if(SysDiv <= SysDiv_2)
        {
            success = false;
        }
        else if(SysDiv <= SysDiv_16)
        {
            rcc.SYSDIV     = SysDiv;
            rcc2.SYSDIV2   = SysDiv;
        }
        else if(SysDiv < SysDiv_64)
        {
            rcc2.SYSDIV2   = SysDiv;
            rcc2.USERCC2   = 1;
        }
        else
        {
            success = false;
        }
    }
    else
    {
        rcc2.DIV400 = 1;
        rcc2.USERCC2 = 1;

        if(SysDiv <= SysDiv_4)
        {
            success = false;
        }
        else
        {
            rcc2.SYSDIV2    = SysDiv >> 1;
            rcc2.SYSDIV2LSB = SysDiv % 2;
        }
    }
    rcc.USESYSDIV = 1;

    RegisterRCC->Value  = rcc.Value;
    RegisterRCC2->Value = rcc2.Value;

    oC_MCS_Delay(16);

    /*
     * Wait for the PLL to lock by polling the PLLLRIS bit in the Raw Interrupt Status (RIS) register.
     */
    if(Pll != Pll_DontUse)
    {
        while(!RegisterRIS->PLLLRIS);

        /*
         * Enable use of the PLL by clearing the BYPASS bit in RCC/RCC2.
         */
        RegisterRCC->BYPASS   = 0;
        RegisterRCC2->BYPASS2 = 0;

        oC_MCS_Delay(16);
    }

    return success;
}

//==========================================================================================================================================
/**
 * Returns the best xtal value for the frequency of the oscillator
 *
 * @param Frequency             Oscillator frequency
 * @param AcceptableDifference  Maximum difference between xtal and real frequency
 *
 * @return xtal value
 */
//==========================================================================================================================================
static Xtal_t GetXtalForFrequency( oC_Frequency_t Frequency , oC_Frequency_t AcceptableDifference )
{
    Xtal_t xtal = 0;

    for( Xtal_t x = Xtal_4MHz ; x <= Xtal_25MHz ; x++ )
    {
        if( oC_ABS( XtalFrequencyArray[x] , Frequency ) <= AcceptableDifference )
        {
            xtal = x;
            break;
        }
    }

    return xtal;
}

//==========================================================================================================================================
/**
 * Finds divisor for the configuration
 *
 * @return true if found
 */
//==========================================================================================================================================
static bool FindDivisor( oC_Frequency_t Frequency , oC_Frequency_t Difference , oC_Frequency_t OscillatorFrequency , Pll_t Pll , SysDiv_t * outSysDiv )
{
    bool            found               = false;
    oC_Frequency_t  frequencyToDivide   = 0;
    SysDiv_t        startSysDiv         = SysDiv_1;
    SysDiv_t        endSysDiv           = SysDiv_64;

    if(Pll == Pll_DontUse)
    {
        frequencyToDivide = OscillatorFrequency;
        startSysDiv       = SysDiv_1;
        endSysDiv         = SysDiv_64;
    }
    else if(Pll == Pll_200MHz)
    {
        frequencyToDivide = oC_MHz(200);
        startSysDiv       = SysDiv_3;
        endSysDiv         = SysDiv_64;
    }
    else if(Pll == Pll_400MHz)
    {
        frequencyToDivide = oC_MHz(400);
        startSysDiv       = SysDiv_5;
        endSysDiv         = SysDiv_128;
    }

    for(SysDiv_t sysDiv = startSysDiv ; sysDiv <= endSysDiv ; sysDiv++ )
    {
        /*
         * This is for skipping one special case. I don't know why, but the 7 divisor in DIV400=1 mode is reserved.
         */
        if(Pll == Pll_400MHz && sysDiv == SysDiv_7)
        {
            continue;
        }
        oC_Frequency_t mainOscFrequency = frequencyToDivide / (sysDiv+1);

        if(oC_ABS(mainOscFrequency,Frequency) <= Difference)
        {
            found       = true;
            *outSysDiv  = sysDiv;
            break;
        }
    }

    return found;
}

//==========================================================================================================================================
/**
 * Calls an event, that clock was configured
 */
//==========================================================================================================================================
static void CallClockConfiguredEvent( void )
{
    if( ClockConfiguredHandler)
    {
        ClockConfiguredHandler(CurrentFrequency);
    }
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
