/** ****************************************************************************************************************************************
 *
 * @file       oc_timer_lld.c
 *
 * @brief      File with source for TIMER LLD functions
 *
 * @author     Patryk Kubiak - (Created on: 16 08 2015 09:22:23)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_timer_lld.h>
#include <oc_mem_lld.h>
#include <oc_clock_lld.h>
#include <oc_gpio_lld.h>
#include <oc_gpio_mslld.h>

#include <oc_math.h>
#include <oc_array.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

#define GPTMTAMR(Channel)               oC_Machine_Register(Channel,GPTMTAMR)
#define GPTMTBMR(Channel)               oC_Machine_Register(Channel,GPTMTBMR)
#define GPTMCTL(Channel)                oC_Machine_Register(Channel,GPTMCTL)
#define GPTMPP(Channel)                 oC_Machine_Register(Channel,GPTMPP)
#define GPTMIMR(Channel)                oC_Machine_Register(Channel,GPTMIMR)
#define GPTMMIS(Channel)                oC_Machine_Register(Channel,GPTMMIS)
#define GPTMICR(Channel)                oC_Machine_Register(Channel,GPTMICR)

#define IsChannelPoweredOn(Channel)     (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)
#define IsChannelCorrect(channel)       (oC_Channel_IsCorrect(TIMER,channel))
#define MODE(Channel)                   Modes[oC_TIMER_LLD_ChannelToChannelIndex(Channel)]


#undef  _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

typedef enum
{
    GPTMTxMR_TxMR_Reserved          = 0 ,
    GPTMTxMR_TxMR_OneShotTimerMode  = 0x1 ,
    GPTMTxMR_TxMR_PeriodicTimerMode = 0x2 ,
    GPTMTxMR_TxMR_CaptureMode       = 0x3
} GPTMTxMR_TxMR_t;

typedef enum
{
    GPTMCTL_TxEVENT_PositiveEdge    = 0 ,
    GPTMCTL_TxEVENT_NegativeEdge    = 1 ,
    GPTMCTL_TxEVENT_BothEdges       = 3 ,
} GPTMCTL_TxEVENT_t;

typedef enum
{
    GPTMCFG_TimerWidth_Full         = 0x0 ,
    GPTMCFG_TimerWidth_FullRtc      = 0x1 ,
    GPTMCFG_TimerWidth_Half         = 0x4
} GPTMCFG_TimerWidth_t;

typedef enum
{
    GPTMTxMR_TxAMS_AlternateMode_CaptureOrCompare = 0 ,
    GPTMTxMR_TxAMS_AlternateMode_Pwm              = 1
} GPTMTxMR_TxAMS_AlternateMode_t;

typedef enum
{
    GPTMTxMR_TxCMR_CaptureMode_EdgeCount = 0 ,
    GPTMTxMR_TxCMR_CaptureMode_EdgeTime  = 1
} GPTMTxMR_TxCMR_CaptureMode_t;

typedef enum
{
    GPTMCTL_TxPWML_PwmOutput_Unaffected = 0 ,
    GPTMCTL_TxPWML_PwmOutput_Inverted   = 1
} GPTMCTL_TxPWML_PwmOutput_t;

typedef enum
{
    GPTMTxMR_TxPWMIE_PwmInterruptEnable_Disabled = 0 ,
    GPTMTxMR_TxPWMIE_PwmInterruptEnable_Enabled  = 1
} GPTMTxMR_TxPWMIE_PwmInterruptEnable_t;

typedef enum
{
    GPTMTxMR_TxPLO_PwmLegacyOperations_Disabled = 0 ,
    GPTMTxMR_TxPLO_PwmLegacyOperations_Enabled  = 1
} GPTMTxMR_TxPLO_PwmLegacyOperations_t;

typedef enum
{
    GPTMCTL_TxEVENT_EventMode_PositiveEdge = 0 ,
    GPTMCTL_TxEVENT_EventMode_NegativeEdge = 1 ,
    GPTMCTL_TxEVENT_EventMode_BothEdges    = 3
} GPTMCTL_TxEVENT_EventMode_t;

#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with global variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


/* Variables */
static bool ModuleEnabledFlag  = false;

/* Arrays */
static oC_TIMER_LLD_EventFlags_t    EventFlagsArray[oC_ModuleChannel_NumberOfElements(TIMER)][2];
static oC_TIMER_LLD_Mode_t          Modes[oC_ModuleChannel_NumberOfElements(TIMER)][2];
static oC_TIMER_LLD_EventHandler_t  EventHandlers[oC_ModuleChannel_NumberOfElements(TIMER)][2];


#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION_________________________________________________________________

static inline bool                          IsModeSelected                  ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static inline bool                          IsTimerEnabled                  ( oC_TIMER_Channel_t Channel );
static inline bool                          IsWideChannel                   ( oC_TIMER_Channel_t Channel );
static inline bool                          CanBeFullWidth                  ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static inline void                          SetCountDirection               ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_CountDirection_t CountDirection );
static inline oC_TIMER_LLD_CountDirection_t GetCountDirection               ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static inline void                          SetTimersMode                   ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxMR_t Mode );
static inline void                          SetInputTrigger                 ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMCTL_TxEVENT_t Trigger );
static inline GPTMCTL_TxEVENT_t             GetInputTrigger                 ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static inline void                          EnableTimer                     ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static inline void                          DisableTimer                    ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static        void                          CallEvent                       ( oC_TIMER_Channel_t Channel , oC_ChannelIndex_t ChannelIndex , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags );
static inline uint32_t                      GetPrescaler                    ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static inline void                          SetTimerWidth                   ( oC_TIMER_Channel_t Channel , GPTMCFG_TimerWidth_t TimerWidth );
static inline GPTMCFG_TimerWidth_t          GetTimerWidth                   ( oC_TIMER_Channel_t Channel );
static inline void                          SetMaxValue                     ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t MaxValue );
static inline uint64_t                      GetMaxValue                     ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer);
static inline void                          SetValue                        ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value );
static inline uint64_t                      GetValue                        ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer);
static inline void                          SetMatchValue                   ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value );
static inline uint64_t                      GetMatchValue                   ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static inline void                          SetAlternateMode                ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxAMS_AlternateMode_t Mode );
static inline void                          SetCaptureMode                  ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxCMR_CaptureMode_t Mode );
static inline void                          SetPwmOutput                    ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMCTL_TxPWML_PwmOutput_t PwmOutput );
static inline GPTMCTL_TxPWML_PwmOutput_t    GetPwmOutput                    ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static        void                          EnableEvents                    ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags );
static        void                          DisableEvents                   ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags );
static        void                          SetPwmInterrupt                 ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxPWMIE_PwmInterruptEnable_t Enabled );
static inline void                          SetPwmLegacyOperations          ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxPLO_PwmLegacyOperations_t Enabled );
static inline oC_ErrorCode_t                InitializeMode                  ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t Mode );
static        oC_TIMER_LLD_Mode_t           GetMode                         ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
static        void                          SetMode                         ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t Mode );
static inline void                          SetEventMode                    ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMCTL_TxEVENT_EventMode_t EventMode );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION_________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , !ModuleEnabledFlag , oC_ErrorCode_ModuleIsTurnedOn))
    {
        oC_TIMER_LLD_ForEachChannel(Channel)
        {
            oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(TIMER,Channel);
            EventHandlers[channelIndex][0]      = NULL;
            EventHandlers[channelIndex][1]      = NULL;
            Modes[channelIndex][0]              = oC_TIMER_LLD_Mode_NotSelected;
            Modes[channelIndex][1]              = oC_TIMER_LLD_Mode_NotSelected;
            EventFlagsArray[channelIndex][0]    = 0;
            EventFlagsArray[channelIndex][1]    = 0;
        }

        ModuleEnabledFlag   = true;
        errorCode           = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;

        oC_TIMER_LLD_ForEachChannel(Channel)
        {
            oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(TIMER,Channel);

            oC_TIMER_LLD_RestoreDefaultStateOnChannel(Channel,oC_TIMER_LLD_SubTimer_Both);

            EventHandlers[channelIndex][0]      = NULL;
            EventHandlers[channelIndex][1]      = NULL;
            Modes[channelIndex][0]              = oC_TIMER_LLD_Mode_NotSelected;
            Modes[channelIndex][1]              = oC_TIMER_LLD_Mode_NotSelected;
            EventFlagsArray[channelIndex][0]    = 0;
            EventFlagsArray[channelIndex][1]    = 0;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsChannelCorrect( oC_TIMER_Channel_t Channel )
{
    return oC_Channel_IsCorrect(TIMER , Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsChannelIndexCorrect( oC_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(TIMER);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsModulePinDefined( oC_TIMER_Pin_t ModulePin )
{
    oC_Pin_t pin = oC_ModulePin_GetPin(ModulePin);
    return oC_GPIO_LLD_IsPinDefined(pin);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsSubTimerCorrect(oC_TIMER_LLD_SubTimer_t SubTimer)
{
    return SubTimer > oC_TIMER_LLD_SubTimer_None && SubTimer <= oC_TIMER_LLD_SubTimer_Both;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_Pin_t oC_TIMER_LLD_GetPinOfModulePin( oC_TIMER_Pin_t ModulePin )
{
    return oC_ModulePin_GetPin(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
const char * oC_TIMER_LLD_GetModulePinName( oC_TIMER_Pin_t ModulePin )
{
    return oC_GPIO_LLD_GetPinName( oC_ModulePin_GetPin(ModulePin) );
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
const char * oC_TIMER_LLD_GetChannelName( oC_TIMER_Channel_t Channel )
{
    return oC_Channel_GetName(Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_TIMER_Channel_t oC_TIMER_LLD_GetChannelOfModulePin( oC_TIMER_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ChannelIndex_t oC_TIMER_LLD_ChannelToChannelIndex( oC_TIMER_Channel_t Channel )
{
    return oC_Channel_ToIndex(TIMER,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_TIMER_Channel_t oC_TIMER_LLD_ChannelIndexToChannel( oC_TIMER_LLD_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex(TIMER,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_SetPower( oC_TIMER_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                     oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,            oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Power == oC_Power_On || Power == oC_Power_Off ,         oC_ErrorCode_PowerStateNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Machine_SetPowerStateForChannel(Channel,Power) ,      oC_ErrorCode_CannotEnableChannel)
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadPower( oC_TIMER_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                     oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,            oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outPower) ,                     oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outPower = oC_Machine_GetPowerStateForChannel(Channel);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_RestoreDefaultStateOnChannel( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    bool stateRestored = false;

    if( ModuleEnabledFlag && oC_Channel_IsCorrect(TIMER,Channel) )
    {
        SetMode(Channel,SubTimer,oC_TIMER_LLD_Mode_NotSelected);
        DisableTimer(Channel,SubTimer);
        DisableEvents(Channel,SubTimer,oC_TIMER_LLD_EventFlags_All);

        if(oC_Machine_SetPowerStateForChannel(Channel,oC_Power_Off))
        {
            stateRestored = true;
        }
    }


    return stateRestored;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TimerStart( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)   , oC_ErrorCode_TIMERSubTimerNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,           oC_ErrorCode_TIMERModeNotSelected )
        )
    {
        EnableTimer(Channel,SubTimer);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TimerStop( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)   , oC_ErrorCode_TIMERSubTimerNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn )
        )
    {
        DisableTimer(Channel,SubTimer);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeFrequency( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_Frequency_t Frequency , oC_Frequency_t PermissibleDifference )
{
    oC_ErrorCode_t errorCode      = oC_ErrorCode_ImplementError;
    oC_Frequency_t clockFrequency = oC_CLOCK_LLD_GetClockFrequency();

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag                        , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel)      , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer) , oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)              , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , !IsTimerEnabled(Channel)                 , oC_ErrorCode_TIMERChannelNotStopped ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer)         , oC_ErrorCode_TIMERModeNotSelected ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (Frequency > 0)                          , oC_ErrorCode_WrongFrequency )
        )
    {
        uint32_t prescaler      = (uint32_t)(clockFrequency / Frequency);
        uint32_t prescalerMax   = IsWideChannel(Channel) ? oC_uint16_MAX : oC_uint8_MAX;

        if(prescaler <= prescalerMax && (prescaler > 0))
        {
            oC_Frequency_t realFrequency = clockFrequency / prescaler;

            if(oC_ABS(realFrequency,Frequency) <= PermissibleDifference)
            {
                if(prescaler == 1)
                {
                    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
                    {
                        oC_Machine_WriteRegister(Channel,  GPTMTAPR , prescaler - 1);
                    }
                    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
                    {
                        oC_Machine_WriteRegister(Channel,  GPTMTBPR , prescaler - 1);
                    }
                    errorCode = oC_ErrorCode_None;
                }
                else if(GetCountDirection(Channel,SubTimer) == oC_TIMER_LLD_CountDirection_Up)
                {
                    /*
                     * When the timer is counting up, it is not possible to configure the prescaler
                     */
                    errorCode = oC_ErrorCode_TIMERCountsDirNotPossible;
                }
                else if(GetTimerWidth(Channel) != GPTMCFG_TimerWidth_Half)
                {
                    errorCode = oC_ErrorCode_TIMERFrequencyNotPossibleInFullMode;
                }
                else
                {
                    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
                    {
                        oC_Machine_WriteRegister(Channel,  GPTMTAPR , prescaler - 1);
                    }
                    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
                    {
                        oC_Machine_WriteRegister(Channel,  GPTMTBPR , prescaler - 1);
                    }
                    errorCode = oC_ErrorCode_None;
                }
            }
            else
            {
                errorCode = oC_ErrorCode_FrequencyNotPossible;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_FrequencyNotPossible;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadFrequency( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_Frequency_t * outFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outFrequency) ,      oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        oC_Frequency_t clockFrequency = oC_CLOCK_LLD_GetClockFrequency();
        uint32_t       prescaler      = GetPrescaler(Channel,SubTimer);

        *outFrequency = clockFrequency / prescaler;
        errorCode     = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeMaximumValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,           oC_ErrorCode_TIMERModeNotSelected )
        )
    {
        if( (Value <= oC_uint16_MAX) || (IsWideChannel(Channel) && (Value <= oC_uint32_MAX)) )
        {
            if(GetMode(Channel,SubTimer) != oC_TIMER_LLD_Mode_RealTimeClock)
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Half);
            }
            errorCode = oC_ErrorCode_None;
        }
        else if ( CanBeFullWidth(Channel,SubTimer) && ( (!IsWideChannel(Channel) && (Value <= oC_uint32_MAX)) || IsWideChannel(Channel) ) )
        {
            if(SubTimer & oC_TIMER_LLD_SubTimer_Both)
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Full);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_TIMERBothSubTimersNeeeded;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_TIMERMaximumValueTooBig;
        }

        if(errorCode == oC_ErrorCode_None)
        {
            SetMaxValue(Channel,SubTimer,Value);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadMaximumValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outValue) ,          oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        *outValue = GetMaxValue(Channel,SubTimer);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeCurrentValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,           oC_ErrorCode_TIMERModeNotSelected )
        )
    {
        if( (Value <= oC_uint16_MAX) || (IsWideChannel(Channel) && (Value <= oC_uint32_MAX)) )
        {
            if(GetMode(Channel,SubTimer) != oC_TIMER_LLD_Mode_RealTimeClock)
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Half);
            }
            errorCode = oC_ErrorCode_None;
        }
        else if ( CanBeFullWidth(Channel,SubTimer) && ( (!IsWideChannel(Channel) && (Value <= oC_uint32_MAX)) || IsWideChannel(Channel) ) )
        {
            if(SubTimer & oC_TIMER_LLD_SubTimer_Both)
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Full);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_TIMERBothSubTimersNeeeded;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_TIMERMaximumValueTooBig;
        }

        if(errorCode == oC_ErrorCode_None)
        {
            SetValue(Channel,SubTimer,Value);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadCurrentValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outValue) ,          oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        *outValue = GetValue(Channel,SubTimer);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,           oC_ErrorCode_TIMERModeNotSelected )
        )
    {
        if( (Value <= oC_uint16_MAX) || (IsWideChannel(Channel) && (Value <= oC_uint32_MAX)) )
        {
            if(GetMode(Channel,SubTimer) != oC_TIMER_LLD_Mode_RealTimeClock)
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Half);
            }
            errorCode = oC_ErrorCode_None;
        }
        else if ( CanBeFullWidth(Channel,SubTimer) && ( (!IsWideChannel(Channel) && (Value <= oC_uint32_MAX)) || IsWideChannel(Channel) ) )
        {
            if(SubTimer & oC_TIMER_LLD_SubTimer_Both)
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Full);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_TIMERBothSubTimersNeeeded;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_TIMERMaximumValueTooBig;
        }

        if(errorCode == oC_ErrorCode_None)
        {
            SetMatchValue(Channel,SubTimer,Value);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outValue) ,          oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        *outValue = GetMatchValue(Channel,SubTimer);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , !IsTimerEnabled(Channel) ,                   oC_ErrorCode_TIMERChannelNotStopped ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn )
        )
    {
        SetMode(Channel,SubTimer,Mode);
        errorCode     = InitializeMode(Channel,SubTimer,Mode);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t * outMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outMode) ,           oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outMode    = GetMode(Channel,SubTimer);
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_CountDirection_t CountDirection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                  oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,         oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,          oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , CountDirection <= oC_TIMER_LLD_CountDirection_Down , oC_ErrorCode_TIMERCountsDirNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) ,         oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , !IsTimerEnabled(Channel) ,                           oC_ErrorCode_TIMERChannelNotStopped ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,                   oC_ErrorCode_TIMERModeNotSelected )
        )
    {
        uint32_t prescaler = GetPrescaler(Channel,SubTimer);

        /* Saved mode of the timer */
        switch(GetMode(Channel,SubTimer))
        {
            /* ****************************************************************************************************************************
             *
             * For most of timer modes each count direction is possible.
             *
             * ****************************************************************************************************************************/
            default:
                if((prescaler > 1) && (CountDirection == oC_TIMER_LLD_CountDirection_Up))
                {
                    errorCode = oC_ErrorCode_TIMERCountsDirNotPossible;
                }
                else
                {
                    /* If it does not matter what the direction is, then just set it to down */
                    if(CountDirection == oC_TIMER_LLD_CountDirection_DoesntMatter)
                    {
                        CountDirection = oC_TIMER_LLD_CountDirection_Down;
                    }

                    errorCode = oC_ErrorCode_None;
                }

                break;

            /* ****************************************************************************************************************************
             *
             * For PWM timer mode it is possible to configure only down counting direction.
             *
             * ****************************************************************************************************************************/
            case oC_TIMER_LLD_Mode_PWM:
                if(CountDirection == oC_TIMER_LLD_CountDirection_Up)
                {
                    errorCode = oC_ErrorCode_TIMERCountsDirNotPossible;
                }
                else
                {
                    CountDirection  = oC_TIMER_LLD_CountDirection_Down;
                    errorCode       = oC_ErrorCode_None;
                }
                break;

            /* ****************************************************************************************************************************
             *
             * For RTC timer mode it is possible to configure only up counting direction.
             *
             * ****************************************************************************************************************************/
            case oC_TIMER_LLD_Mode_RealTimeClock:

                if(CountDirection == oC_TIMER_LLD_CountDirection_Down)
                {
                    errorCode = oC_ErrorCode_TIMERCountsDirNotPossible;
                }
                else
                {
                    CountDirection  = oC_TIMER_LLD_CountDirection_Up;
                    errorCode       = oC_ErrorCode_None;
                }

                break;
        }

        /* If there was not any error, then it can be set to new direction */
        if(errorCode == oC_ErrorCode_None)
        {
            SetCountDirection(Channel,SubTimer,CountDirection);
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_CountDirection_t * outCountDirection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                  oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,         oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,          oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) ,         oC_ErrorCode_ChannelNotPoweredOn ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outCountDirection) ,         oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outCountDirection = GetCountDirection(Channel,SubTimer);
        errorCode          = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Trigger_t Trigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,           oC_ErrorCode_TIMERModeNotSelected )
        )
    {
        /* If trigger is set to None, then no action is needed */
        if(Trigger == oC_TIMER_LLD_Trigger_None)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            switch(GetMode(Channel,SubTimer))
            {
                /* *************************************************************************************************************************
                 *
                 *  If it is input mode, then trigger can be set
                 *
                 **************************************************************************************************************************/
                case oC_TIMER_LLD_Mode_InputEdgeCount:
                case oC_TIMER_LLD_Mode_InputEdgeTime:
                case oC_TIMER_LLD_Mode_PWM:

                    if(Trigger == oC_TIMER_LLD_Trigger_Both)
                    {
                        SetInputTrigger(Channel,SubTimer,GPTMCTL_TxEVENT_BothEdges);
                        errorCode = oC_ErrorCode_None;
                    }
                    else if(Trigger == oC_TIMER_LLD_Trigger_RisingEdge)
                    {
                        SetInputTrigger(Channel,SubTimer,GPTMCTL_TxEVENT_PositiveEdge);
                        errorCode = oC_ErrorCode_None;
                    }
                    else if(Trigger == oC_TIMER_LLD_Trigger_FallingEdge)
                    {
                        SetInputTrigger(Channel,SubTimer,GPTMCTL_TxEVENT_NegativeEdge);
                        errorCode = oC_ErrorCode_None;
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_TIMERTriggerNotCorrect;
                    }

                    break;
                /* *************************************************************************************************************************
                 *
                 *  Only input modes allow to configure trigger
                 *
                 **************************************************************************************************************************/
                default:
                    errorCode = oC_ErrorCode_TIMERModeNotCorrect;
                    break;
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Trigger_t * outTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,  oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,           oC_ErrorCode_TIMERModeNotSelected ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outTrigger) ,        oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        GPTMCTL_TxEVENT_t trigger = GetInputTrigger(Channel,SubTimer);

        errorCode = oC_ErrorCode_None;

        switch(trigger)
        {
            case GPTMCTL_TxEVENT_NegativeEdge: *outTrigger = oC_TIMER_LLD_Trigger_FallingEdge;  break;
            case GPTMCTL_TxEVENT_PositiveEdge: *outTrigger = oC_TIMER_LLD_Trigger_RisingEdge;   break;
            case GPTMCTL_TxEVENT_BothEdges:    *outTrigger = oC_TIMER_LLD_Trigger_Both;         break;
            default: errorCode = oC_ErrorCode_MachineCanBeDamaged; break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeEventHandler( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventHandler_t EventHandler , oC_TIMER_LLD_EventFlags_t EventFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                                                oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,                                       oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,                                        oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) ,                                       oC_ErrorCode_ChannelNotPoweredOn ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(EventHandler) || oC_MEM_LLD_IsFlashAddress(EventHandler) , oC_ErrorCode_WrongAddress)
        )
    {
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
        {
            EventHandlers[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][0] = EventHandler;
        }
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
        {
            EventHandlers[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][1] = EventHandler;
        }
        EnableEvents(Channel,SubTimer,EventFlags);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadEventHandler(oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventHandler_t * outEventHandler , oC_TIMER_LLD_EventFlags_t * outEventFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                                                oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,                                       oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,                                        oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) ,                                       oC_ErrorCode_ChannelNotPoweredOn ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outEventHandler)  ,                                        oC_ErrorCode_OutputAddressNotInRAM ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outEventFlags)  ,                                          oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
        {
            *outEventHandler = EventHandlers[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][0];
            *outEventFlags   = EventFlagsArray[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][0];
        }
        else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
        {
            *outEventHandler = EventHandlers[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][1];
            *outEventFlags   = EventFlagsArray[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][1];
        }
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeStartPwmState( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_PwmState_t State)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                  oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,         oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,          oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) ,         oC_ErrorCode_ChannelNotPoweredOn ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,                   oC_ErrorCode_TIMERModeNotSelected ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , GetMode(Channel,SubTimer) == oC_TIMER_LLD_Mode_PWM , oC_ErrorCode_TIMERModeNotCorrect )
        )
    {
        if(oC_TIMER_LLD_PwmState_Low == State)
        {
            SetPwmOutput(Channel,SubTimer,GPTMCTL_TxPWML_PwmOutput_Inverted);
        }
        else
        {
            SetPwmOutput(Channel,SubTimer,GPTMCTL_TxPWML_PwmOutput_Unaffected);
        }
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadStartPwmState( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_PwmState_t * outState)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                  oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,         oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer)  ,          oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) ,         oC_ErrorCode_ChannelNotPoweredOn ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outState) ,                  oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsModeSelected(Channel,SubTimer) ,                   oC_ErrorCode_TIMERModeNotSelected ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , GetMode(Channel,SubTimer) == oC_TIMER_LLD_Mode_PWM , oC_ErrorCode_TIMERModeNotCorrect )
        )
    {
        if(GPTMCTL_TxPWML_PwmOutput_Inverted == GetPwmOutput(Channel,SubTimer))
        {
            *outState = oC_TIMER_LLD_PwmState_Low;
        }
        else
        {
            *outState = oC_TIMER_LLD_PwmState_High;
        }
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ConnectModulePin(oC_TIMER_Pin_t ModulePin )
{
    oC_ErrorCode_t          errorCode = oC_ErrorCode_ImplementError;
    oC_TIMER_Channel_t      channel   = oC_TIMER_LLD_GetChannelOfModulePin(ModulePin);
    oC_TIMER_LLD_SubTimer_t subTimer;

    if( oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag , oC_ErrorCode_ModuleNotStartedYet ) )
    {
        oC_Pin_t          pin     = oC_ModulePin_GetPin(ModulePin);

        if(
            ErrorCondition( IsChannelCorrect(channel)       , oC_ErrorCode_WrongChannel  ) &&
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(pin)   , oC_ErrorCode_PinNotDefined )
            )
        {
            bool unused = false;

            oC_MCS_EnterCriticalSection();

            if(
                oC_AssignErrorCode(        &errorCode , oC_GPIO_LLD_ArePinsUnused(pin,&unused)      ) &&
                ErrorCondition(            unused     , oC_ErrorCode_PinIsUsed                      ) &&
                oC_AssignErrorCode(        &errorCode , oC_GPIO_LLD_SetPinsUsed(pin)                ) &&
                oC_AssignErrorCode(        &errorCode , oC_TIMER_LLD_ReadSubTimerOfModulePin(ModulePin,&subTimer)) &&
                oC_AssignErrorCodeIfFalse( &errorCode , oC_TIMER_LLD_IsSubTimerCorrect(subTimer)                 , oC_ErrorCode_TIMERSubTimerNotCorrect ) &&
                oC_AssignErrorCodeIfFalse( &errorCode , IsModeSelected(channel,subTimer)                         , oC_ErrorCode_TIMERModeNotSelected    ) &&
                oC_AssignErrorCodeIfFalse( &errorCode , IsChannelPoweredOn(channel)                              , oC_ErrorCode_ChannelNotPoweredOn     ) &&
                oC_AssignErrorCode(        &errorCode , oC_GPIO_MSLLD_ConnectModulePin(ModulePin)   )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_GPIO_LLD_SetPinsUnused(pin);
            }

            oC_MCS_ExitCriticalSection();
        }
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadSubTimerOfModulePin(oC_TIMER_Pin_t PeripheralPin , oC_TIMER_LLD_SubTimer_t * outSubTimer )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_TIMER_Channel_t  channel   = oC_TIMER_LLD_GetChannelOfModulePin(PeripheralPin);

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                                      oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,channel) ,                    oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsModulePinDefined(PeripheralPin) ,         oC_ErrorCode_PeripheralPinNotDefined ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outSubTimer) ,                   oC_ErrorCode_OutputAddressNotInRAM)
        )
    {

        oC_PinFunction_t         pinFunction = oC_ModulePin_GetPinFunction(PeripheralPin);

        /*
         * This is wrong way for recognize sub-timer, but it is the best that I have for now...
         */
        if(pinFunction)
        {
            *outSubTimer = oC_TIMER_LLD_SubTimer_TimerB;
        }
        else
        {
            *outSubTimer = oC_TIMER_LLD_SubTimer_TimerA;
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_SetChannelUsed( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                            oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) ,   oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer) ,     oC_ErrorCode_TIMERSubTimerNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , !oC_TIMER_LLD_IsChannelUsed(Channel,SubTimer), oC_ErrorCode_ChannelIsUsed)
        )
    {
        SetMode(Channel,SubTimer,oC_TIMER_LLD_Mode_Reserved);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_SetChannelUnused( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag ,                          oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Channel_IsCorrect(TIMER,Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsSubTimerCorrect(SubTimer) ,   oC_ErrorCode_TIMERSubTimerNotCorrect)
        )
    {
        SetMode(Channel,SubTimer,oC_TIMER_LLD_Mode_NotSelected);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsChannelUsed( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    return  oC_Channel_IsCorrect(TIMER,Channel) && oC_TIMER_LLD_IsSubTimerCorrect(SubTimer) && (GetMode(Channel,SubTimer) != oC_TIMER_LLD_Mode_NotSelected);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadModulePinsOfPin( oC_Pins_t Pin , oC_TIMER_Pin_t * outPeripheralPinsArray , uint32_t * ArraySize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , oC_GPIO_LLD_IsSinglePin(Pin)  ,                   oC_ErrorCode_NotSinglePin  ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_TIMER_LLD_IsModulePinDefined(Pin) ,            oC_ErrorCode_PinNotDefined ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outPeripheralPinsArray) , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(ArraySize) ,              oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (*ArraySize) < 255 ,                              oC_ErrorCode_ValueTooBig )
        )
    {
        uint8_t outArrayIndex = 0;

        errorCode = oC_ErrorCode_None;

        oC_ModulePin_ForeachDefined(modulePin)
        {
            if(modulePin->Pin == Pin && oC_Channel_IsCorrect(UART,modulePin->Channel))
            {
                if(outArrayIndex < (*ArraySize))
                {
                    outPeripheralPinsArray[outArrayIndex++] = modulePin->ModulePinIndex;
                }
                else
                {
                    errorCode = oC_ErrorCode_OutputArrayToSmall;
                }
            }
        }

        if(outArrayIndex == 0)
        {
            errorCode = oC_ErrorCode_PeripheralPinNotDefined;
        }

        *ArraySize = outArrayIndex;
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if mode is selected
 */
//==========================================================================================================================================
static inline bool IsModeSelected( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer)
{
    oC_TIMER_LLD_Mode_t mode = GetMode(Channel,SubTimer);
    return (mode > oC_TIMER_LLD_Mode_Reserved) &&
           (mode <= oC_TIMER_LLD_Mode_PWM);
}

//==========================================================================================================================================
/**
 * @brief checks if timer is enabled
 */
//==========================================================================================================================================
static inline bool IsTimerEnabled( oC_TIMER_Channel_t Channel )
{
    return GPTMCTL(Channel)->TAEN && GPTMCTL(Channel)->TBEN;
}

//==========================================================================================================================================
/**
 * @brief checks if timer is wide (32/64 bits)
 */
//==========================================================================================================================================
static inline bool IsWideChannel( oC_TIMER_Channel_t Channel )
{
    return GPTMPP(Channel)->SIZE == 1;
}

//==========================================================================================================================================
/**
 * @brief checks if timer can work in full width mode
 */
//==========================================================================================================================================
static inline bool CanBeFullWidth( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_TIMER_LLD_Mode_t mode      = GetMode(Channel,SubTimer);
    uint32_t            prescaler = GetPrescaler(Channel,SubTimer);

    return
            (
            mode == oC_TIMER_LLD_Mode_RealTimeClock ||
            mode == oC_TIMER_LLD_Mode_PeriodicTimer
            ) &&
            (prescaler == 1);

}

//==========================================================================================================================================
/**
 * @brief sets direction of counting
 */
//==========================================================================================================================================
static inline void SetCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_CountDirection_t CountDirection )
{
    if(CountDirection == oC_TIMER_LLD_CountDirection_Up)
    {
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
        {
            GPTMTAMR(Channel)->TACDIR = 1;
        }
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
        {
            GPTMTBMR(Channel)->TBCDIR = 1;
        }
    }
    else
    {
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
        {
            GPTMTAMR(Channel)->TACDIR = 0;
        }
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
        {
            GPTMTBMR(Channel)->TBCDIR = 0;
        }
    }
}

//==========================================================================================================================================
/**
 * @brief returns direction of counting
 */
//==========================================================================================================================================
static inline oC_TIMER_LLD_CountDirection_t GetCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_TIMER_LLD_CountDirection_t countDirection;

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        if(GPTMTAMR(Channel)->TACDIR == 1)
        {
            countDirection = oC_TIMER_LLD_CountDirection_Up;
        }
        else
        {
            countDirection = oC_TIMER_LLD_CountDirection_Down;
        }
    }
    else
    {
        if(GPTMTBMR(Channel)->TBCDIR == 1)
        {
            countDirection = oC_TIMER_LLD_CountDirection_Up;
        }
        else
        {
            countDirection = oC_TIMER_LLD_CountDirection_Down;
        }
    }

    return countDirection;
}

//==========================================================================================================================================
/**
 * @brief sets mode of timer in the GPTMTxMR register
 */
//==========================================================================================================================================
static inline void SetTimersMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxMR_t Mode )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMTAMR(Channel)->TAMR = Mode;
    }
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMTBMR(Channel)->TBMR = Mode;
    }
}

//==========================================================================================================================================
/**
 * @brief sets trigger of input event
 */
//==========================================================================================================================================
static inline void SetInputTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMCTL_TxEVENT_t Trigger )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMCTL(Channel)->TAEVENT = Trigger;
    }
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        GPTMCTL(Channel)->TBEVENT = Trigger;
    }
}

//==========================================================================================================================================
/**
 * @brief returns current input trigger
 */
//==========================================================================================================================================
static inline GPTMCTL_TxEVENT_t GetInputTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        return GPTMCTL(Channel)->TAEVENT;
    }
    else
    {
        return GPTMCTL(Channel)->TBEVENT;
    }
}

//==========================================================================================================================================
/**
 * @brief Enables (turns on and start timer)
 */
//==========================================================================================================================================
static inline void EnableTimer( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    if(oC_Machine_IsChannelPoweredOn(Channel))
    {
        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
        {
            GPTMCTL(Channel)->TAEN = 1;
        }

        if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
        {
            GPTMCTL(Channel)->TBEN = 1;
        }
    }
}

//==========================================================================================================================================
/**
 * @brief disables (turns off and stop timer)
 */
//==========================================================================================================================================
static inline void DisableTimer( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMCTL(Channel)->TAEN = 0;
    }

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        GPTMCTL(Channel)->TBEN = 0;
    }
}

//==========================================================================================================================================
/**
 * @brief calls event handler
 */
//==========================================================================================================================================
static void CallEvent( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_ChannelIndex_t ChannelIndex , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags )
{
    oC_TIMER_LLD_EventHandler_t eventHandler      = NULL;
    oC_TIMER_LLD_EventFlags_t   enabledEventFlags = 0;

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        eventHandler      = EventHandlers[ChannelIndex][0];
        enabledEventFlags = EventFlagsArray[ChannelIndex][0];
    }
    else
    {
        eventHandler      = EventHandlers[ChannelIndex][1];
        enabledEventFlags = EventFlagsArray[ChannelIndex][1];
    }

    if(eventHandler && (EventFlags & enabledEventFlags))
    {
        eventHandler(Channel,SubTimer,EventFlags);
    }
}

//==========================================================================================================================================
/**
 * @brief returns Prescaler of timer
 */
//==========================================================================================================================================
static inline uint32_t GetPrescaler( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
        return oC_Machine_ReadRegister( Channel,  GPTMTAPR ) + 1;
    else
        return oC_Machine_ReadRegister( Channel,  GPTMTBPR ) + 1;
}

//==========================================================================================================================================
/**
 * @brief
 */
//==========================================================================================================================================
static inline void SetTimerWidth( oC_TIMER_Channel_t Channel , GPTMCFG_TimerWidth_t TimerWidth )
{
    oC_Machine_WriteRegister(Channel, GPTMCFG , TimerWidth);
}

//==========================================================================================================================================
/**
 * @brief returns width of timer (half / full)
 */
//==========================================================================================================================================
static inline GPTMCFG_TimerWidth_t GetTimerWidth( oC_TIMER_Channel_t Channel )
{
    return oC_Machine_ReadRegister(Channel, GPTMCFG) & 0x7;
}

//==========================================================================================================================================
/**
 * @brief sets maximum value for the timer
 */
//==========================================================================================================================================
static inline void SetMaxValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t MaxValue )
{
    uint32_t lowWord  = 0;
    uint32_t highWord = 0;

    oC_Bits_SplitU64ToU32(MaxValue , &lowWord , &highWord);

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        oC_Machine_WriteRegister(Channel, GPTMTAILR , lowWord );
    }
    if(oC_Bits_AreBitsSetU32(SubTimer , oC_TIMER_LLD_SubTimer_Both))
    {
        oC_Machine_WriteRegister(Channel, GPTMTBILR , highWord );
    }
    else if (SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        oC_Machine_WriteRegister(Channel, GPTMTBILR , lowWord );
    }
}

//==========================================================================================================================================
/**
 * @brief returns maximum value for the timer
 */
//==========================================================================================================================================
static inline uint64_t GetMaxValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    uint64_t maxValue = 0;
    uint32_t lowWord  = oC_Machine_ReadRegister(Channel, GPTMTAILR );
    uint32_t highWord = oC_Machine_ReadRegister(Channel, GPTMTBILR );

    if(oC_Bits_AreBitsSetU32(SubTimer,oC_TIMER_LLD_SubTimer_Both))
    {
        maxValue = oC_Bits_JoinU32ToU64(lowWord,highWord);
    }
    else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        maxValue = lowWord;
    }
    else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        maxValue = highWord;
    }

    return maxValue;
}

//==========================================================================================================================================
/**
 * @brief sets current value for the timer
 */
//==========================================================================================================================================
static inline void SetValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    uint32_t lowWord  = 0;
    uint32_t highWord = 0;

    oC_Bits_SplitU64ToU32(Value , &lowWord , &highWord);

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        oC_Machine_WriteRegister(Channel,GPTMTAV , lowWord );
    }
    if(oC_Bits_AreBitsSetU32(SubTimer , oC_TIMER_LLD_SubTimer_Both))
    {
        oC_Machine_WriteRegister(Channel, GPTMTBV , highWord );
    }
    else if (SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        oC_Machine_WriteRegister(Channel, GPTMTBV , lowWord );
    }
}

//==========================================================================================================================================
/**
 * @brief returns current value of the timer
 */
//==========================================================================================================================================
static inline uint64_t GetValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    uint64_t maxValue = 0;
    uint32_t lowWord  = oC_Machine_ReadRegister(Channel, GPTMTAR );
    uint32_t highWord = oC_Machine_ReadRegister(Channel, GPTMTBR );

    if(oC_Bits_AreBitsSetU32(SubTimer,oC_TIMER_LLD_SubTimer_Both))
    {
        maxValue = oC_Bits_JoinU32ToU64(lowWord,highWord);
    }
    else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        maxValue = lowWord;
    }
    else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        maxValue = highWord;
    }

    return maxValue;
}

//==========================================================================================================================================
/**
 * @brief sets match value for a timer
 */
//==========================================================================================================================================
static inline void SetMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    uint32_t lowWord  = 0;
    uint32_t highWord = 0;

    oC_Bits_SplitU64ToU32(Value , &lowWord , &highWord);

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        oC_Machine_WriteRegister(Channel, GPTMTAMATCHR , lowWord );
    }
    if(oC_Bits_AreBitsSetU32(SubTimer , oC_TIMER_LLD_SubTimer_Both))
    {
        oC_Machine_WriteRegister(Channel, GPTMTBMATCHR , highWord );
    }
    else if (SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        oC_Machine_WriteRegister(Channel, GPTMTBMATCHR , lowWord );
    }
}

//==========================================================================================================================================
/**
 * @brief returns match value for a timer
 */
//==========================================================================================================================================
static inline uint64_t GetMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    uint64_t maxValue = 0;
    uint32_t lowWord  = oC_Machine_ReadRegister(Channel, GPTMTAMATCHR );
    uint32_t highWord = oC_Machine_ReadRegister(Channel, GPTMTBMATCHR );

    if(oC_Bits_AreBitsSetU32(SubTimer,oC_TIMER_LLD_SubTimer_Both))
    {
        maxValue = oC_Bits_JoinU32ToU64(lowWord,highWord);
    }
    else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        maxValue = lowWord;
    }
    else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        maxValue = highWord;
    }

    return maxValue;
}

//==========================================================================================================================================
/**
 * @brief sets alternate mode
 */
//==========================================================================================================================================
static inline void SetAlternateMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxAMS_AlternateMode_t Mode )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMTAMR(Channel)->TAAMS = Mode;
    }
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        GPTMTBMR(Channel)->TBAMS = Mode;
    }
}

//==========================================================================================================================================
/**
 * @brief sets capture mode
 */
//==========================================================================================================================================
static inline void SetCaptureMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxCMR_CaptureMode_t Mode )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMTAMR(Channel)->TACMR = Mode;
    }
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        GPTMTBMR(Channel)->TBCMR = Mode;
    }
}

//==========================================================================================================================================
/**
 * @brief sets if pwm output should be inverted or not
 */
//==========================================================================================================================================
static inline void SetPwmOutput( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMCTL_TxPWML_PwmOutput_t PwmOutput )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMCTL(Channel)->TAPWML = PwmOutput;
    }
    else
    {
        GPTMCTL(Channel)->TBPWML = PwmOutput;
    }
}

//==========================================================================================================================================
/**
 * @brief reads if pwm output should be inverted or not
 */
//==========================================================================================================================================
static inline GPTMCTL_TxPWML_PwmOutput_t GetPwmOutput( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    GPTMCTL_TxPWML_PwmOutput_t pwmOutput;

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        pwmOutput = GPTMCTL(Channel)->TAPWML;
    }
    else
    {
        pwmOutput = GPTMCTL(Channel)->TBPWML;
    }

    return pwmOutput;
}

//==========================================================================================================================================
/**
 * @brief enables events for timer
 */
//==========================================================================================================================================
static void EnableEvents( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        EventFlagsArray[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][0] = EventFlags;
        oC_Channel_EnableInterrupt(Channel,TimerA);

        if(EventFlags & oC_TIMER_LLD_EventFlags_TimeoutInterrupt)
        {
            GPTMIMR(Channel)->TATOIM = 1;
        }

        if(EventFlags & oC_TIMER_LLD_EventFlags_MatchValueInterrupt)
        {
            GPTMIMR(Channel)->TAMIM  = 1;
        }

        if(
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInLowState  ) ||
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInHighState )

                )
        {
            GPTMIMR(Channel)->CAEIM = 1;
            GPTMIMR(Channel)->CAEIM = 1;
        }
    }
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        EventFlagsArray[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][1] = EventFlags;
        oC_Channel_EnableInterrupt(Channel,TimerB);

        if(EventFlags & oC_TIMER_LLD_EventFlags_TimeoutInterrupt)
        {
            GPTMIMR(Channel)->TBTOIM = 1;
        }

        if(EventFlags & oC_TIMER_LLD_EventFlags_MatchValueInterrupt)
        {
            GPTMIMR(Channel)->TBMIM  = 1;
        }

        if(
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInLowState  ) ||
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInHighState )

                )
        {
            GPTMIMR(Channel)->CBEIM = 1;
            GPTMIMR(Channel)->CBEIM = 1;
        }
    }

    if(
        oC_Bits_AreBitsSetU32(EventFlags , oC_TIMER_LLD_EventFlags_PwmPinInLowState) &&
        oC_Bits_AreBitsSetU32(EventFlags , oC_TIMER_LLD_EventFlags_PwmPinInHighState)
        )
    {
        SetEventMode(Channel,SubTimer,GPTMCTL_TxEVENT_EventMode_BothEdges);
    }
    else if (oC_Bits_AreBitsSetU32(EventFlags , oC_TIMER_LLD_EventFlags_PwmPinInLowState))
    {
        SetEventMode(Channel,SubTimer,GPTMCTL_TxEVENT_EventMode_NegativeEdge);
    }
    else if (oC_Bits_AreBitsSetU32(EventFlags , oC_TIMER_LLD_EventFlags_PwmPinInHighState))
    {
        SetEventMode(Channel,SubTimer,GPTMCTL_TxEVENT_EventMode_PositiveEdge);
    }
}

//==========================================================================================================================================
/**
 * @brief disables events for timer
 */
//==========================================================================================================================================
static void DisableEvents( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        EventFlagsArray[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][0] &= ~EventFlags;
        oC_Channel_EnableInterrupt(Channel,TimerA);

        if(EventFlags & oC_TIMER_LLD_EventFlags_TimeoutInterrupt)
        {
            GPTMIMR(Channel)->TATOIM = 0;
        }

        if(EventFlags & oC_TIMER_LLD_EventFlags_MatchValueInterrupt)
        {
            GPTMIMR(Channel)->TAMIM  = 0;
        }

        if(
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInLowState  ) ||
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInHighState )

                )
        {
            GPTMIMR(Channel)->CAEIM = 0;
            GPTMIMR(Channel)->CAEIM = 0;
        }
    }
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        EventFlagsArray[oC_TIMER_LLD_ChannelToChannelIndex(Channel)][1] &= ~EventFlags;
        oC_Channel_EnableInterrupt(Channel,TimerB);

        if(EventFlags & oC_TIMER_LLD_EventFlags_TimeoutInterrupt)
        {
            GPTMIMR(Channel)->TBTOIM = 0;
        }

        if(EventFlags & oC_TIMER_LLD_EventFlags_MatchValueInterrupt)
        {
            GPTMIMR(Channel)->TBMIM  = 0;
        }

        if(
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInLowState  ) ||
                ( EventFlags & oC_TIMER_LLD_EventFlags_PwmPinInHighState )

                )
        {
            GPTMIMR(Channel)->CBEIM = 0;
            GPTMIMR(Channel)->CBEIM = 0;
        }
    }

}

//==========================================================================================================================================
/**
 * @brief sets pwm interrupt enable or not
 */
//==========================================================================================================================================
static void SetPwmInterrupt( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxPWMIE_PwmInterruptEnable_t Enabled )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMTAMR(Channel)->TAPWMIE = Enabled;
    }
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        GPTMTBMR(Channel)->TBPWMIE = Enabled;
    }
}

//==========================================================================================================================================
/**
 * @brief sets PWM legacy operations mode turned on or not
 */
//==========================================================================================================================================
static inline void SetPwmLegacyOperations( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMTxMR_TxPLO_PwmLegacyOperations_t Enabled )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMTAMR(Channel)->TAPLO = Enabled;
    }

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        GPTMTBMR(Channel)->TBPLO = Enabled;
    }
}


//==========================================================================================================================================
/**
 * @brief initializes timer mode
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t InitializeMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    switch(Mode)
    {
        /* *****************************************************************************************************************************
         *
         *  PWM must be in individual timer width, and can be done with periodic timer mode
         *
         ******************************************************************************************************************************/
        case oC_TIMER_LLD_Mode_PWM:
            SetTimerWidth(Channel , GPTMCFG_TimerWidth_Half);
            SetTimersMode(Channel , SubTimer , GPTMTxMR_TxMR_PeriodicTimerMode);
            SetAlternateMode(Channel , SubTimer , GPTMTxMR_TxAMS_AlternateMode_Pwm);
            SetPwmInterrupt(Channel , SubTimer , GPTMTxMR_TxPWMIE_PwmInterruptEnable_Enabled);
            SetCaptureMode(Channel , SubTimer , GPTMTxMR_TxCMR_CaptureMode_EdgeCount);
            SetPwmLegacyOperations(Channel, SubTimer , GPTMTxMR_TxPLO_PwmLegacyOperations_Enabled);
            SetPwmOutput(Channel, SubTimer , GPTMCTL_TxPWML_PwmOutput_Inverted);
            errorCode = oC_ErrorCode_None;
            break;
        /* *****************************************************************************************************************************
         *
         *  These modes just need to set the periodic timer mode
         *
         ******************************************************************************************************************************/
        case oC_TIMER_LLD_Mode_PeriodicTimer:
            if(
                ((SubTimer & oC_TIMER_LLD_SubTimer_TimerA) == oC_TIMER_LLD_SubTimer_TimerA) ||
                ((SubTimer & oC_TIMER_LLD_SubTimer_TimerB) == oC_TIMER_LLD_SubTimer_TimerB)
                )
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Half);
            }
            else
            {
                SetTimerWidth(Channel , GPTMCFG_TimerWidth_Full);
            }

            SetTimersMode(Channel , SubTimer , GPTMTxMR_TxMR_PeriodicTimerMode);
            SetAlternateMode(Channel , SubTimer , GPTMTxMR_TxAMS_AlternateMode_CaptureOrCompare);
            SetPwmLegacyOperations(Channel , SubTimer, GPTMTxMR_TxPLO_PwmLegacyOperations_Disabled);
            errorCode = oC_ErrorCode_None;
            break;
        /* *****************************************************************************************************************************
         *
         *  In real time clock mode the timers mode selection is not needed, but timer width must be full
         *
         ******************************************************************************************************************************/
        case oC_TIMER_LLD_Mode_RealTimeClock:
            if(oC_Bits_AreBitsSetU32(SubTimer,oC_TIMER_LLD_SubTimer_Both))
            {
                SetTimerWidth(Channel,GPTMCFG_TimerWidth_FullRtc);
                SetAlternateMode(Channel , SubTimer , GPTMTxMR_TxAMS_AlternateMode_CaptureOrCompare);
                SetPwmLegacyOperations(Channel , SubTimer, GPTMTxMR_TxPLO_PwmLegacyOperations_Disabled);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_TIMERBothSubTimersNeeeded;
            }
            break;
        /* *****************************************************************************************************************************
         *
         *  In input edge modes the timers modes must be configured in capture mode
         *
         ******************************************************************************************************************************/
        case oC_TIMER_LLD_Mode_InputEdgeCount:
            SetTimerWidth(Channel , GPTMCFG_TimerWidth_Half);
            SetTimersMode(Channel , SubTimer , GPTMTxMR_TxMR_CaptureMode);
            SetAlternateMode(Channel , SubTimer , GPTMTxMR_TxAMS_AlternateMode_CaptureOrCompare);
            SetPwmLegacyOperations(Channel , SubTimer, GPTMTxMR_TxPLO_PwmLegacyOperations_Disabled);
            SetCaptureMode(Channel , SubTimer , GPTMTxMR_TxCMR_CaptureMode_EdgeCount);
            errorCode = oC_ErrorCode_None;
            break;
        /* *****************************************************************************************************************************
         *
         *  In input edge modes the timers modes must be configured in capture mode
         *
         ******************************************************************************************************************************/
        case oC_TIMER_LLD_Mode_InputEdgeTime:
            SetTimerWidth(Channel , GPTMCFG_TimerWidth_Half);
            SetTimersMode(Channel , SubTimer , GPTMTxMR_TxMR_CaptureMode);
            SetAlternateMode(Channel , SubTimer , GPTMTxMR_TxAMS_AlternateMode_CaptureOrCompare);
            SetPwmLegacyOperations(Channel , SubTimer, GPTMTxMR_TxPLO_PwmLegacyOperations_Disabled);
            SetCaptureMode(Channel , SubTimer , GPTMTxMR_TxCMR_CaptureMode_EdgeTime);
            errorCode = oC_ErrorCode_None;
            break;
        /* *****************************************************************************************************************************
         *
         *  Other modes are not handled (not recognized)
         *
         ******************************************************************************************************************************/
        default:
            errorCode = oC_ErrorCode_TIMERModeNotCorrect;
            break;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns current timer mode
 */
//==========================================================================================================================================
static oC_TIMER_LLD_Mode_t GetMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_TIMER_LLD_Mode_t Mode;

    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        Mode = MODE(Channel)[0];
    }
    else if(SubTimer & oC_TIMER_LLD_SubTimer_TimerB)
    {
        Mode = MODE(Channel)[1];
    }

    return Mode;
}

//==========================================================================================================================================
/**
 * @brief sets, but not initializes timer mode
 */
//==========================================================================================================================================
static void SetMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t Mode )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        MODE(Channel)[0] = Mode;
    }
    else
    {
        MODE(Channel)[1] = Mode;
    }
}

//==========================================================================================================================================
/**
 * @brief sets event modes for the CTL register
 */
//==========================================================================================================================================
static inline void SetEventMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , GPTMCTL_TxEVENT_EventMode_t EventMode )
{
    if(SubTimer & oC_TIMER_LLD_SubTimer_TimerA)
    {
        GPTMCTL(Channel)->TAEVENT = EventMode;
    }
    else
    {
        GPTMCTL(Channel)->TBEVENT = EventMode;
    }
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

#define MODULE_NAME      TIMER
#define INTERRUPT_TYPE   TimerA

#undef INTERRUPT_TYPE
#define INTERRUPT_TYPE   TimerB
#undef INTERRUPT_TYPE


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________
#define MODULE_NAME TIMER
#define INTERRUPT_TYPE_NAME TimerA

oC_Channel_InterruptHandler(Channel)
{
    oC_TIMER_LLD_EventFlags_t EventFlags = 0;
    oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(TIMER,Channel);

    if(GPTMMIS(Channel)->TAMMIS)
    {
        EventFlags |= oC_TIMER_LLD_EventFlags_MatchValueInterrupt;
        GPTMICR(Channel)->TAMCINT = 1;
    }

    if(GPTMMIS(Channel)->TATOMIS)
    {
        EventFlags |= oC_TIMER_LLD_EventFlags_TimeoutInterrupt;
        GPTMICR(Channel)->TATOCINT = 1;
    }

    if(GPTMMIS(Channel)->CAEMIS)
    {
        oC_TIMER_LLD_Mode_t mode = GetMode(Channel,oC_TIMER_LLD_SubTimer_TimerA);
        if(mode == oC_TIMER_LLD_Mode_PWM)
        {
            EventFlags |= oC_TIMER_LLD_EventFlags_PwmPinInHighState;
            EventFlags |= oC_TIMER_LLD_EventFlags_PwmPinInLowState;

        }
        else
        {
            EventFlags |= oC_TIMER_LLD_EventFlags_EdgeDetect;
        }
        GPTMICR(Channel)->CAECINT = 1;
    }


    CallEvent(Channel,channelIndex,oC_TIMER_LLD_SubTimer_TimerA,EventFlags);
    GPTMICR(Channel)->TATOCINT = 1;
}
#undef INTERRUPT_TYPE_NAME

#define INTERRUPT_TYPE_NAME TimerB

oC_Channel_InterruptHandler(Channel)
{
    oC_TIMER_LLD_EventFlags_t EventFlags = 0;
    oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(TIMER,Channel);

    if(GPTMMIS(Channel)->TBMMIS)
    {
        EventFlags |= oC_TIMER_LLD_EventFlags_MatchValueInterrupt;
        GPTMICR(Channel)->TBMCINT = 1;
    }

    if(GPTMMIS(Channel)->TBTOMIS)
    {
        EventFlags |= oC_TIMER_LLD_EventFlags_TimeoutInterrupt;
        GPTMICR(Channel)->TBTOCINT = 1;
    }

    if(GPTMMIS(Channel)->CBEMIS)
    {
        oC_TIMER_LLD_Mode_t mode = GetMode(Channel,oC_TIMER_LLD_SubTimer_TimerB);
        if(mode == oC_TIMER_LLD_Mode_PWM)
        {
            EventFlags |= oC_TIMER_LLD_EventFlags_PwmPinInHighState;
            EventFlags |= oC_TIMER_LLD_EventFlags_PwmPinInLowState;

        }
        else
        {
            EventFlags |= oC_TIMER_LLD_EventFlags_EdgeDetect;
        }

        GPTMICR(Channel)->CBECINT = 1;
    }


    CallEvent(Channel,channelIndex,oC_TIMER_LLD_SubTimer_TimerB,EventFlags);
    GPTMICR(Channel)->TBTOCINT = 1;
}
#undef INTERRUPT_TYPE_NAME

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

