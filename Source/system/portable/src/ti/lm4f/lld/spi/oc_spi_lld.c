/*******************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_spi_lld.c
 *
 *    @brief        The file with sources for the spi low level driver (SPI-LLD)
 *
 *    @author     Krzysztof Chmielewski - (Created on: 7 09 2015 20:30:18)
 *                Kamil Drobienko
 *                Krzysztof Dworzynski
 *
 *    @note       Copyright (C) 2015 Krzysztof Chmielewski <krzysztof.marek.chmielewski@gmail.com>
 *                Copyright (C) 2015 Kamil Drobienko <kamildrobienko@gmail.com>
 *                Copyright (C) 2015 Krzysztof Dworzynski <dworniok@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_spi_lld.h>
#include <oc_machine.h>
#include <oc_bits.h>
#include <oc_gpio_mslld.h>
#include <oc_machine_defs.h>
#include <oc_clock_lld.h>
#include <oc_dma_lld.h>
#include <oc_array.h>
#include <oc_mem_lld.h>
#include <oc_math.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(address)                          (oC_MEM_LLD_IsRamAddress(address))
#define IsRom(address)                          (oC_MEM_LLD_IsFlashAddress(address))
#define IsChannelCorrect(channel)               (oC_Channel_IsCorrect(SPI,channel))
#define IsChannelPoweredOn(channel)             (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)
#define IsChannelUsed(channel)                  (oC_Bits_IsBitSetU32(ChannelUsedFlags,(uint8_t)oC_Channel_ToIndex(SPI,Channel)))
#define IsChannelBusy(channel)                  (SSISR(channel)->BSY == 1)                  // SSI Busy Bit
#define IsRxFifoFull(channel)                   (SSISR(channel)->RFF == 1)                  // SSI Receive FIFO Full
#define IsRxFifoEmpty(channel)                  (SSISR(channel)->RNE == 0)                  // SSI Receive FIFO Not Empty
#define IsTxFifoFull(channel)                   (SSISR(channel)->TNF == 0)                  // SSI Transmit FIFO Not Full
#define IsTxFifoEmpty(channel)                  (SSISR(channel)->TFE == 1)                  // SSI Transmit FIFO Empty

#define SSICR0(Channel)                         oC_Machine_Register(Channel, SSICR0)        /**< SSI Control 0 - Frame type, clock rate, protocol mode, data size */
#define SSICR1(Channel)                         oC_Machine_Register(Channel, SSICR1)        /**< SSI Control 1 - Master and Slave mode */
#define SSIDR(Channel)                          oC_Machine_Register(Channel, SSIDR)         /**< FIFO Services, read/write operations */
#define SSISR(Channel)                          oC_Machine_Register(Channel, SSISR)         /**< FIFO fill status, SSI busy flag */
#define SSICPSR(Channel)                        oC_Machine_Register(Channel, SSICPSR)       /**< SSI Clock Prescale */
#define SSIDMACTL(Channel)                      oC_Machine_Register(Channel, SSIDMACTL)     /**< uDMA control register */
#define SSICC(Channel)                          oC_Machine_Register(Channel, SSICC)         /**< Controls the baud rate source */

#define IsFrameWidthCorrect(FrameWidth)         ((FrameWidth) >= oC_MACHINE_SPI_MINIMUM_FRAME_WIDTH) && ((FrameWidth) <= oC_MACHINE_SPI_MAXIMUM_FRAME_WIDTH) /**< Checking FrameWidth */

#define AreChannelOperationsEnabled(Channel)    (SSICR1(Channel)->SSE == 1)
#define AreChannelOperationsDisabled(Channel)   (SSICR1(Channel)->SSE == 0)

#undef __________________________________________MACROS_SECTION_____________________________________________________________________________

#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool                     ModuleEnabledFlag   = false;
static oC_SPI_LLD_Interrupt_t   RxFullInterrupt     = NULL;
static oC_SPI_LLD_Interrupt_t   TxEmptyInterrupt    = NULL;
static uint32_t                 ChannelUsedFlags    = 0;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes functions
 *
 *  ======================================================================================================================================*/
#define ______________________________________LOCAL_PROTOTYPES_SECTION______________________________________________________________________

static oC_ErrorCode_t CalculateDivisorsAndWriteRegisters   ( oC_SPI_Channel_t Channel, oC_Frequency_t Frequency , oC_Frequency_t AcceptableDifference );

#undef ______________________________________LOCAL_PROTOTYPES_SECTION_______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsChannelCorrect( oC_SPI_Channel_t Channel )
{
    return oC_Channel_IsCorrect( SPI , Channel);
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsChannelIndexCorrect( oC_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(SPI);
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ChannelIndex_t oC_SPI_LLD_ChannelToChannelIndex( oC_SPI_Channel_t Channel )
{
    return oC_Channel_ToIndex(SPI,Channel);
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_SPI_Channel_t oC_SPI_LLD_ChannelIndexToChannel( oC_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex(SPI,Channel);
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_SPI_Channel_t oC_SPI_LLD_GetChannelOfModulePin( oC_SPI_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn))
    {
        errorCode         = oC_ErrorCode_None;
        RxFullInterrupt   = NULL;
        TxEmptyInterrupt  = NULL;
        ChannelUsedFlags  = 0;
        ModuleEnabledFlag = true;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;
        RxFullInterrupt   = NULL;
        TxEmptyInterrupt  = NULL;
        ChannelUsedFlags  = 0;
        errorCode         = oC_ErrorCode_None;

        oC_SPI_LLD_ForEachChannel(channel)
        {
            oC_AssignErrorCode(&errorCode , oC_SPI_LLD_RestoreDefaultStateOnChannel(channel));
        }

    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetDriverInterruptHandlers( oC_SPI_LLD_Interrupt_t RxFull , oC_SPI_LLD_Interrupt_t TxEmpty )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true        , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , RxFullInterrupt   == NULL        , oC_ErrorCode_InterruptHandlerAlreadySet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , TxEmptyInterrupt  == NULL        , oC_ErrorCode_InterruptHandlerAlreadySet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(RxFull)  || IsRom(RxFull)  , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(TxEmpty) || IsRom(TxEmpty) , oC_ErrorCode_WrongAddress)
        )
    {
        RxFullInterrupt   = RxFull;
        TxEmptyInterrupt  = TxEmpty;

        errorCode         = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetPower( oC_SPI_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                        , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                        , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Power == oC_Power_On || Power == oC_Power_Off    , oC_ErrorCode_PowerStateNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Machine_SetPowerStateForChannel(Channel,Power) ,      oC_ErrorCode_CannotEnableChannel)
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadPower( oC_SPI_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outPower)           , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outPower = oC_Machine_GetPowerStateForChannel(Channel);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_DisableOperations( oC_SPI_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)            , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)          , oC_ErrorCode_ChannelNotPoweredOn)
        )
    {
        while(IsChannelBusy(Channel));

        SSICR1(Channel)->SSE = 0;

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_EnableOperations( oC_SPI_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true             , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)             , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)           , oC_ErrorCode_ChannelNotPoweredOn)
        )
    {
        if( AreChannelOperationsEnabled(Channel) )
        {
            errorCode = oC_SPI_LLD_DisableOperations(Channel);

            if( errorCode == oC_ErrorCode_None )
            {
                SSICR0(Channel)->FRF   = 0; /**< These bits set frame format as Freescale */
                SSICR1(Channel)->SLBY6 = 1; /**< This bit sets highest bypass mode for slave */
                SSICC (Channel)->CS    = 0; /**< These bits set system clock as clock source for SPI */

                SSICR1(Channel)->SSE   = 1; /**< This bit sets operations enabled - it's important to set this bit after those above! */
            }
        }
        else
        {
            SSICR0(Channel)->FRF   = 0; /**< These bits set frame format as Freescale */
            SSICR1(Channel)->SLBY6 = 1; /**< This bit sets highest bypass mode for slave */
            SSICC (Channel)->CS    = 0; /**< These bits set system clock as clock source for SPI */

            SSICR1(Channel)->SSE   = 1; /**< This bit sets operations enabled - it's important to set this bit after those above! */

            errorCode = oC_ErrorCode_None;
        }

    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_RestoreDefaultStateOnChannel( oC_SPI_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel))
    {
        oC_Bits_ClearBitU32(&ChannelUsedFlags,oC_SPI_LLD_ChannelToChannelIndex(Channel));

        if(oC_Machine_SetPowerStateForChannel(Channel,oC_Power_Off))
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetBitOrder( oC_SPI_Channel_t Channel , oC_SPI_LLD_BitOrder_t BitOrder )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , BitOrder == oC_SPI_LLD_BitOrder_MSBFirst   , oC_ErrorCode_BitOrderNotSupported) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel)      , oC_ErrorCode_ChannelOperationsNotDisabled)
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadBitOrder( oC_SPI_Channel_t Channel , oC_SPI_LLD_BitOrder_t * outBitOrder )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outBitOrder)                         , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outBitOrder = oC_SPI_LLD_BitOrder_MSBFirst;

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetFrameWidth( oC_SPI_Channel_t Channel , oC_SPI_LLD_FrameWidth_t FrameWidth )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsFrameWidthCorrect(FrameWidth)            , oC_ErrorCode_FrameWidthNotSupported) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel)      , oC_ErrorCode_ChannelOperationsNotDisabled)
        )
    {
        SSICR0(Channel)->DSS = (--FrameWidth);
        errorCode            = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadFrameWidth( oC_SPI_Channel_t Channel , oC_SPI_LLD_FrameWidth_t * outFrameWidth )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outFrameWidth)                       , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outFrameWidth = SSICR0(Channel)->DSS + 1;

        errorCode      = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetClockPhase( oC_SPI_Channel_t Channel , oC_SPI_LLD_Phase_t Phase )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel)      , oC_ErrorCode_ChannelOperationsNotDisabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Phase == oC_SPI_LLD_Phase_FirstEdge ||
                                               Phase == oC_SPI_LLD_Phase_SecondEdge       , oC_ErrorCode_ClockPhaseNotSupported)
        )
    {
        if (oC_SPI_LLD_Phase_FirstEdge == Phase)
        {
            SSICR0(Channel)->SPH = 0;
        }
        else
        {
            SSICR0(Channel)->SPH = 1;
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadClockPhase( oC_SPI_Channel_t Channel , oC_SPI_LLD_Phase_t * outPhase )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outPhase)                            , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        if (0 == SSICR0(Channel)->SPH)
        {
            *outPhase = oC_SPI_LLD_Phase_FirstEdge;
        }
        else
        {
            *outPhase = oC_SPI_LLD_Phase_SecondEdge;
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetClockPolarity( oC_SPI_Channel_t Channel , oC_SPI_LLD_Polarity_t Polarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                         , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                         , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                       , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel)             , oC_ErrorCode_ChannelOperationsNotDisabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Polarity == oC_SPI_LLD_Polarity_HighWhenActive ||
                                               Polarity == oC_SPI_LLD_Polarity_LowWhenActive     , oC_ErrorCode_ClockPolarityNotSupported)
        )
    {
        if (oC_SPI_LLD_Polarity_HighWhenActive == Polarity)
        {
            SSICR0(Channel)->SPO = 1;
        }
        else
        {
            SSICR0(Channel)->SPO = 0;
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadClockPolarity( oC_SPI_Channel_t Channel , oC_SPI_LLD_Polarity_t * outPolarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outPolarity)                         , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        if (0 == SSICR0(Channel)->SPO)
        {
            *outPolarity = oC_SPI_LLD_Polarity_LowWhenActive;
        }
        else
        {
            *outPolarity = oC_SPI_LLD_Polarity_HighWhenActive;
        }

        errorCode    = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetChipSelectPolarity( oC_SPI_Channel_t Channel , oC_SPI_LLD_Polarity_t Polarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                     , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                     , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                   , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel)         , oC_ErrorCode_ChannelOperationsNotDisabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Polarity == oC_SPI_LLD_Polarity_LowWhenActive , oC_ErrorCode_ChipSelectPolarityNotSupported)
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadChipSelectPolarity( oC_SPI_Channel_t Channel , oC_SPI_LLD_Polarity_t * outPolarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outPolarity)                         , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outPolarity = oC_SPI_LLD_Polarity_LowWhenActive;
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetLoopback( oC_SPI_Channel_t Channel , bool Loopback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true             , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)             , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)           , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel) , oC_ErrorCode_ChannelOperationsNotDisabled)
        )
    {
        if(Loopback)
        {
            SSICR1(Channel)->LBM = 1;
        }
        else
        {
            SSICR1(Channel)->LBM = 0;
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadLoopback( oC_SPI_Channel_t Channel , bool * outLoopback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true   , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outLoopback)          , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        *outLoopback   = SSICR1(Channel)->LBM;
        errorCode      = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetFrequency( oC_SPI_Channel_t Channel , oC_Frequency_t Frequency , oC_Frequency_t PermissibleDifference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel)      , oC_ErrorCode_ChannelOperationsNotDisabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Frequency >=0                              , oC_ErrorCode_WrongFrequency)
    )
    {
        oC_SPI_LLD_Mode_t mode;
        errorCode = oC_SPI_LLD_ReadMode(Channel , &mode);

        if (mode == oC_SPI_LLD_Mode_Master)
        {
            errorCode = CalculateDivisorsAndWriteRegisters(Channel , Frequency , PermissibleDifference);
        }
        else
        {
            errorCode = oC_ErrorCode_FunctionNotSupportedInCurrentMode;
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadFrequency( oC_SPI_Channel_t Channel , oC_Frequency_t * outFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                  , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                  , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outFrequency)                        , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        oC_SPI_LLD_Mode_t mode;

        if(oC_AssignErrorCode(&errorCode , oC_SPI_LLD_ReadMode(Channel , &mode)))
        {
            if(mode == oC_SPI_LLD_Mode_Master)
            {
                oC_Frequency_t SystemClockFrequency = oC_CLOCK_LLD_GetClockFrequency();

                *outFrequency = SystemClockFrequency / (SSICPSR(Channel)->CPSDVSR * (SSICR0(Channel)->SCR + 1));
                errorCode     = oC_ErrorCode_None;
            }
            else
            {
                errorCode     = oC_ErrorCode_FunctionNotSupportedInCurrentMode;
            }
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetChannelUsed( oC_SPI_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , !oC_SPI_LLD_IsChannelUsed(Channel), oC_ErrorCode_ChannelIsUsed)
        )
    {
        oC_Bits_SetBitU32(&ChannelUsedFlags,(oC_uint32_t) oC_SPI_LLD_ChannelToChannelIndex(Channel));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetChannelUnused( oC_SPI_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel )
        )
    {
        oC_Bits_ClearBitU32(&ChannelUsedFlags,(oC_uint32_t) oC_SPI_LLD_ChannelToChannelIndex(Channel));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsChannelUsed( oC_SPI_Channel_t Channel )
{
    bool used = false;

    if( (ModuleEnabledFlag == true) && IsChannelCorrect(Channel))
    {
        /*
         * This assertion fails, when some of lm4f machine has more than 32 SPI channels. To save RAM memory, only 1 bit is used for marking,
         * that channel is already used. Flags are stored in the uint32_t type, so it can store only 32 channels. If you want to repair this error,
         * you must change the ChannelUsedFlags type to uint64_t, and then change assertion below to compare with '64' instead of '32'.
         * Moreover, you should update functions #oC_SPI_LLD_SetChannelUnused and oC_SPI_LLD_SetChannelUsed to use
         * #oC_Bits_SetBitU64 and #oC_Bits_ClearBitU64 instead of #oC_Bits_SetBitU32 and #oC_Bits_ClearBitU32.
         * If you do this, it should works.
         */
        oC_STATIC_ASSERT(oC_ModuleChannel_NumberOfElements(SPI) <= 32,"The SPI LLD module is not designed for more, than 32 channels - you need to set ChannelUsedFlags to uint64_t if you want to handle it!");

        used = IsChannelUsed(Channel);
    }

    return used;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsModulePinDefined( oC_SPI_Pin_t ModulePin )
{
    oC_Pin_t pin = oC_ModulePin_GetPin(ModulePin);
    return oC_GPIO_LLD_IsPinDefined(pin);
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetMode( oC_SPI_Channel_t Channel, oC_SPI_LLD_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                      , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                      , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)                    , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsDisabled(Channel)          , oC_ErrorCode_ChannelOperationsNotDisabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Mode == oC_SPI_LLD_Mode_Master              ||
                                               Mode == oC_SPI_LLD_Mode_SlaveDisabledOutput ||
                                               Mode == oC_SPI_LLD_Mode_SlaveEnabledOutput     , oC_ErrorCode_ModeNotSupported)
        )
    {
        SSICR1(Channel)->MS  = (Mode & 1);          /**<  This bit sets master or slave mode */
        SSICR1(Channel)->SOD = ((Mode>>1) & 1) ;    /**<  This bit sets output enabled or disabled */

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadMode( oC_SPI_Channel_t Channel, oC_SPI_LLD_Mode_t * outMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true   , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outMode)              , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        if      ((SSICR1(Channel)->MS == 1) && (SSICR1(Channel)->SOD == 0))
        {
            *outMode = oC_SPI_LLD_Mode_SlaveEnabledOutput;
        }
        else if ((SSICR1(Channel)->MS == 1) && (SSICR1(Channel)->SOD == 1))
        {
            *outMode = oC_SPI_LLD_Mode_SlaveDisabledOutput;
        }
        else
        {
            *outMode = oC_SPI_LLD_Mode_Master;
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadModulePinsOfPin(  oC_Pins_t Pin , oC_SPI_Pin_t * outPeripheralPinsArray , uint32_t * ArraySize)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , oC_GPIO_LLD_IsSinglePin(Pin)  ,                    oC_ErrorCode_NotSinglePin  ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_SPI_LLD_IsModulePinDefined(Pin) ,              oC_ErrorCode_PinNotDefined ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(outPeripheralPinsArray) , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsRamAddress(ArraySize) ,              oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (*ArraySize) < 255 ,                              oC_ErrorCode_ValueTooBig )
        )
    {
        uint8_t outArrayIndex = 0;

        errorCode = oC_ErrorCode_None;

        oC_ModulePin_ForeachDefined(modulePin)
        {
            if(modulePin->Pin == Pin && oC_Channel_IsCorrect(SPI,modulePin->Channel))
            {
                if(outArrayIndex < (*ArraySize))
                {
                    outPeripheralPinsArray[outArrayIndex++] = modulePin->ModulePinIndex;
                }
                else
                {
                    errorCode = oC_ErrorCode_OutputArrayToSmall;
                }
            }
        }

        if(outArrayIndex == 0)
        {
            errorCode = oC_ErrorCode_PeripheralPinNotDefined;//module
        }

        *ArraySize = outArrayIndex;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ConnectModulePin( oC_SPI_Pin_t ModulePin )
{
    oC_ErrorCode_t       errorCode = oC_ErrorCode_ImplementError;
//    oC_SPI_Channel_t channel   = oC_SPI_LLD_GetChannelOfModulePin(ModulePin);
//TODO: implement this
    //    oC_Pins_t pin             = oC_ModulePin_GetPin(ModulePin);
//
//    if(
//            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                        , oC_ErrorCode_ModuleNotStartedYet ) &&
//            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(channel)                        , oC_ErrorCode_WrongChannel ) &&
//            oC_AssignErrorCodeIfFalse(&errorCode , oC_GPIO_LLD_IsPinDefined(ModulePin) , oC_ErrorCode_PeripheralPinNotDefined )
//        )
//    {
//
//        uint8_t   digitalFunction = oC_Machine_GetDigitalFunctionOfPeripheralPin(ModulePin);
//
//        if(oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_ConnectModulePin( pin , digitalFunction )))
//        {
//            errorCode = oC_ErrorCode_None;
//        }
//    }
    errorCode = oC_ErrorCode_None;
    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ConfigureGeneralChipSelectPin( oC_SPI_Channel_t Channel , oC_Pins_t Pin )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    bool                pinUnused = false;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true    , oC_ErrorCode_ModuleNotStartedYet) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)    , oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_GPIO_LLD_IsPinDefined(Pin) , oC_ErrorCode_PinNotDefined)
    )
    {
        if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ArePinsUnused(Pin,&pinUnused)) &&
                oC_AssignErrorCodeIfFalse(&errorCode , pinUnused , oC_ErrorCode_PinIsUsed) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(Pin))
        )
        {
            if(
                    oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pin)) &&
                    oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pin , oC_GPIO_LLD_Mode_Output)) &&
                    oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetCurrent(Pin , oC_GPIO_LLD_Current_Minimum)) &&
                    oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetOutputCircuit(Pin , oC_GPIO_LLD_OutputCircuit_PushPull)) &&
                    oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPull(Pin , oC_GPIO_LLD_Pull_Up)) &&
                    oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pin))
            )
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_GPIO_LLD_SetPinsUnused(Pin);
            }
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_UnconfigureGeneralChipSelectPin( oC_SPI_Channel_t Channel , oC_Pins_t Pin )
{

    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)    , oC_ErrorCode_WrongChannel ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_GPIO_LLD_IsPinDefined(Pin) , oC_ErrorCode_PinNotDefined)
    )
    {
        errorCode = oC_GPIO_LLD_SetPinsUnused(Pin);
    }

    return errorCode;

}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetGeneralChipSelectState( oC_SPI_Channel_t Channel , oC_Pins_t Pin , oC_SPI_LLD_State_t State )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;
    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true                , oC_ErrorCode_ModuleNotStartedYet) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)                , oC_ErrorCode_WrongChannel ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_GPIO_LLD_IsPinDefined(Pin)             , oC_ErrorCode_PinNotDefined) &&
            oC_AssignErrorCodeIfFalse(&errorCode , State == oC_SPI_LLD_State_Active     ||
                                                   State == oC_SPI_LLD_State_NotActive      , oC_ErrorCode_StateNotSupported)
    )
    {
        oC_GPIO_LLD_SetPinsState(Pin , State);

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsDmaSupportPossible( oC_SPI_Channel_t Channel )
{
    bool result = false;
    //TODO: loop for all dma channels
    if(ModuleEnabledFlag && IsChannelCorrect(Channel))
    {
        oC_DMA_Channel_t channelRx = 0;
        oC_DMA_Channel_t channelTx = 0;

        if(
           oC_ErrorOccur(oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade(&channelRx , Channel, oC_Machine_DmaSignalType_RX)) &&
           oC_ErrorOccur(oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade(&channelRx , Channel, oC_Machine_DmaSignalType_TX))
           )
        {
            result = true;
        }
        else if(
                 oC_DMA_LLD_IsChannelSupportedOnDmaChannel(channelRx , Channel , oC_Machine_DmaSignalType_RX) &&
                 oC_DMA_LLD_IsChannelSupportedOnDmaChannel(channelTx , Channel , oC_Machine_DmaSignalType_RX)
                 )
        {
            result = true;
        }
    }

    return result;
}//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_Write( oC_SPI_Channel_t Channel , void * Buffer , oC_UInt_t BufferSize , oC_UInt_t ElementSize , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)            , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)          , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsEnabled(Channel) , oC_ErrorCode_ChannelOperationsNotEnabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(Buffer) || IsRom(Buffer)       , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , BufferSize != 0 && ElementSize != 0  , oC_ErrorCode_SizeNotCorrect)
        )
    {
        void * endBuffer = Buffer + (BufferSize * ElementSize);

        for(;Buffer < endBuffer ; Buffer += ElementSize)
        {
            if( oC_Bits_AreBitsClearU32(IoFlags,oC_IoFlags_WaitForAllElements) && IsTxFifoFull(Channel) )
            {
                break;
            }

            while(IsTxFifoFull(Channel));

            SSIDR(Channel)->DATA = *((uint32_t*)Buffer) & oC_Bits_Mask_U32(0 , 15);
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_Read( oC_SPI_Channel_t Channel , void * outBuffer , oC_UInt_t BufferSize , oC_UInt_t ElementSize , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)            , oC_ErrorCode_WrongChannel) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)          , oC_ErrorCode_ChannelNotPoweredOn) &&
            oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsEnabled(Channel) , oC_ErrorCode_ChannelOperationsNotEnabled) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outBuffer)                     , oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , BufferSize != 0 && ElementSize != 0        , oC_ErrorCode_SizeNotCorrect)
            )
        {
            void * endOutBuffer = outBuffer + (BufferSize * ElementSize);

            for(oC_uint32_t byteIndex = ElementSize , savedData = 0; outBuffer < endOutBuffer ; outBuffer++ , byteIndex++)
            {
                if(byteIndex == ElementSize)
                {
                   if( oC_Bits_AreBitsClearU32(IoFlags,oC_IoFlags_WaitForAllElements) && IsRxFifoEmpty(Channel) )
                   {
                       break;
                   }

                    while(IsRxFifoEmpty(Channel));

                    savedData  = SSIDR(Channel)->DATA;
                    byteIndex  = 0;
                }

                if(byteIndex >= 2)
                {
                    *(oC_uint8_t*)outBuffer = 0;
                }
                else
                {

                    *(oC_uint8_t*)outBuffer = savedData >> (byteIndex * 8);
                }
            }

            errorCode = oC_ErrorCode_None;
        }

        return errorCode;
    }

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_WriteWithDma( oC_SPI_Channel_t Channel , void * Buffer , oC_UInt_t Size , oC_UInt_t ElementSize  , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t       errorCode = oC_ErrorCode_ImplementError;
    oC_DMA_Channel_t channel   = 0;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)            , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)          , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsEnabled(Channel) , oC_ErrorCode_ChannelOperationsNotEnabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(Buffer) || IsRom(Buffer)       , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size != 0                            , oC_ErrorCode_SizeNotCorrect) &&
        oC_AssignErrorCode(&errorCode , oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade(&channel,Channel,oC_Machine_DmaSignalType_TX) )
        )
    {
        oC_DMA_LLD_PeripheralTradeConfig_t dmaConfig;

        dmaConfig.Buffer                        = (void*)Buffer;
        dmaConfig.BufferSize                    = Size;
        dmaConfig.ElementSize                   = ElementSize;
        dmaConfig.PeripheralChannel             = Channel;
        dmaConfig.PeripheralData                = (void*)&SSIDR(Channel)->Value;
        dmaConfig.Priority                      = oC_DMA_LLD_Priority_Medium;
        dmaConfig.SignalType                    = oC_Machine_DmaSignalType_TX;
        dmaConfig.TransferCompleteEventHandler  = NULL;
        dmaConfig.TransmitDirection             = oC_DMA_LLD_Direction_Transmit;

        SSIDMACTL(Channel)->TXDMAE = 1;

        errorCode = oC_DMA_LLD_ConfigurePeripheralTrade(channel,&dmaConfig);
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadWithDma( oC_SPI_Channel_t Channel , void * outBuffer , oC_UInt_t Size , oC_UInt_t ElementSize , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_DMA_Channel_t channel   = 0;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)            , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)          , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , AreChannelOperationsEnabled(Channel) , oC_ErrorCode_ChannelOperationsNotEnabled) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outBuffer)                     , oC_ErrorCode_OutputAddressNotInRAM) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size > 0                             , oC_ErrorCode_SizeNotCorrect) &&
        oC_AssignErrorCode(&errorCode , oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade(&channel,Channel,oC_Machine_DmaSignalType_RX) )
        )
    {
        oC_DMA_LLD_PeripheralTradeConfig_t dmaConfig;

        dmaConfig.Buffer                        = (void*)outBuffer;
        dmaConfig.BufferSize                    = Size;
        dmaConfig.ElementSize                   = ElementSize;
        dmaConfig.PeripheralChannel             = Channel;
        dmaConfig.PeripheralData                = (void*)&SSIDR(Channel)->Value;
        dmaConfig.Priority                      = oC_DMA_LLD_Priority_Medium;
        dmaConfig.SignalType                    = oC_Machine_DmaSignalType_RX;
        dmaConfig.TransferCompleteEventHandler  = NULL;
        dmaConfig.TransmitDirection             = oC_DMA_LLD_Direction_Receive;

        SSIDMACTL(Channel)->RXDMAE = 1;

        errorCode = oC_DMA_LLD_ConfigurePeripheralTrade(channel,&dmaConfig);
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

#define ______________________________________LOCAL_FUNCTIONS_SECTION_______________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
static oC_ErrorCode_t CalculateDivisorsAndWriteRegisters( oC_SPI_Channel_t Channel , oC_Frequency_t Frequency , oC_Frequency_t AcceptableDifference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Frequency_t systemClockFrequency = oC_CLOCK_LLD_GetClockFrequency();

    oC_Frequency_t minimumFrequency     =  oC_MACHINE_SPI_MINIMUM_TRANSMISION_FREQUENCY;
    oC_Frequency_t maximumFrequency     = (oC_MACHINE_SPI_MAXIMUM_TRANSMISION_FREQUENCY <= (systemClockFrequency / 2)) ?
                    oC_MACHINE_SPI_MAXIMUM_TRANSMISION_FREQUENCY  : (systemClockFrequency / 2);


    oC_Frequency_t calculatedFrequency  = oC_Hz(0);

    oC_uint16_t     frequencyRatio       = systemClockFrequency / (Frequency);

    oC_uint8_t CPSDVSR = 2;
    oC_uint8_t SCR     = 0;
    errorCode = oC_ErrorCode_FrequencyNotPossible;

    while(CPSDVSR != 0)
    {
        (bool)(frequencyRatio / CPSDVSR - 1) ?
        ( SCR=0 ) : ( SCR = frequencyRatio / CPSDVSR - 1 );

        calculatedFrequency = systemClockFrequency / CPSDVSR * (1 + SCR);

        if( ( oC_ABS(calculatedFrequency,Frequency) > AcceptableDifference ) || (calculatedFrequency > maximumFrequency) )
        {
            CPSDVSR  += 2;
        }
        else
        {
            if( (calculatedFrequency >= minimumFrequency)  )
            {
                SSICPSR(Channel)->CPSDVSR = CPSDVSR;
                SSICR0(Channel)->SCR      = SCR;

                errorCode = oC_ErrorCode_None;
            }

            break;
        }
    }

    return errorCode;
}

#undef ______________________________________LOCAL_FUNCTIONS_SECTION__________________________________________________________________________
