/** ****************************************************************************************************************************************
 *
 * @file       oc_uart_lld.c
 *
 * @brief      The file with source for UART LLD functions
 *
 * @author     Patryk Kubiak - (Created on: 20 09 2015 10:31:48)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_uart_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_gpio_mslld.h>
#include <oc_array.h>
#include <oc_clock_lld.h>
#include <oc_dma_lld.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <string.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define SOFTWARE_RING_COUNT                     10
#define IsRam(address)                          (oC_LSF_IsRamAddress(address))
#define IsRom(address)                          (oC_LSF_IsRomAddress(address))
#define IsChannelCorrect(channel)               (oC_Channel_IsCorrect(UART,channel))
#define IsChannelPoweredOn(channel)             (oC_Machine_GetPowerStateForChannel(channel) == oC_Power_On)
#define IsChannelUsed(channel)                  (oC_Bits_IsBitSetU32(ChannelUsedFlags,(uint8_t)oC_Machine_ChannelToChannelIndex(UART,channel)))
#define IsChannelBusy(channel)                  (UARTFR(channel)->BUSY == 1)
#define AreOperationsDisabled(channel)          (UARTCTL(channel)->UARTEN == 0)
#define AreOperationsEnabled(channel)           (UARTCTL(channel)->UARTEN == 1)
#define IsRxFifoEmpty(channel)                  (UARTFR(channel)->RXFE == 1)
#define IsTxFifoEmpty(channel)                  (UARTFR(channel)->TXFE == 1)
#define IsRxFifoFull(channel)                   (UARTFR(channel)->RXFF == 1)
#define IsTxFifoFull(channel)                   (UARTFR(channel)->TXFF == 1)

#define UARTCTL(Channel)                        oC_Machine_Register(Channel,UARTCTL)
#define UARTFR(Channel)                         oC_Machine_Register(Channel,UARTFR)
#define UARTLCRH(Channel)                       oC_Machine_Register(Channel,UARTLCRH)
#define UARTDR(Channel)                         oC_Machine_Register(Channel,UARTDR)
#define UARTDMACTL(Channel)                     oC_Machine_Register(Channel,UARTDMACTL)
#define UARTIM(Channel)                         oC_Machine_Register(Channel,UARTIM)
#define UARTICR(Channel)                        oC_Machine_Register(Channel,UARTICR)
#define UARTRIS(Channel)                        oC_Machine_Register(Channel,UARTRIS)
#define UARTIFLS(Channel)                       oC_Machine_Register(Channel,UARTIFLS)
#define UARTCC(Channel)                         oC_Machine_Register(Channel,UARTCC)
#define RCGCUART                                oC_Register(SystemControl,RCGCUART)
#define RxRing(Channel)                         SoftwareRxRings[oC_Channel_ToIndex(UART,Channel)]
#define IsSoftwareRxRingEmpty(Channel)          (RxRing(Channel).Counter == 0)
#define IsSoftwareRxRingFull(Channel)           (RxRing(Channel).Counter == SOFTWARE_RING_COUNT)
#define TxRing(Channel)                         SoftwareTxRings[oC_Channel_ToIndex(UART,Channel)]
#define IsSoftwareTxRingEmpty(Channel)          (TxRing(Channel).Counter == 0)
#define IsSoftwareTxRingFull(Channel)           (TxRing(Channel).Counter == SOFTWARE_RING_COUNT)


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    uint16_t            Buffer[SOFTWARE_RING_COUNT];
    uint16_t            PutIndex;
    uint16_t            GetIndex;
    uint16_t            Counter;
} SoftwareRing_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline void     PutToSoftwareRing  ( SoftwareRing_t * Ring , uint16_t Data );
static inline uint16_t GetFromSoftwareRing( SoftwareRing_t * Ring );
static inline void     ClearSoftwareRing  ( SoftwareRing_t * Ring );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_UART_LLD_Interrupt_t  RxNotEmptyHandler  = NULL;
static oC_UART_LLD_Interrupt_t  TxNotFullHandler   = NULL;
static SoftwareRing_t           SoftwareRxRings[oC_ModuleChannel_NumberOfElements(UART)];
static SoftwareRing_t           SoftwareTxRings[oC_ModuleChannel_NumberOfElements(UART)];
#if oC_ModuleChannel_NumberOfElements(UART) <= 8
static uint8_t                 UsedChannels     = 0;
#elif oC_ModuleChannel_NumberOfElements(UART) <= 16
static uint16_t                UsedChannels     = 0;
#elif oC_ModuleChannel_NumberOfElements(UART) <= 32
static uint32_t                UsedChannels     = 0;
#elif oC_ModuleChannel_NumberOfElements(UART) <= 64
static uint64_t                UsedChannels     = 0;
#else
#   error Number of UART channels is too big (max 64)
#endif



#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelCorrect( oC_UART_Channel_t Channel )
{
    return oC_Channel_IsCorrect(UART,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelIndexCorrect( oC_UART_LLD_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(UART);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_UART_LLD_ChannelIndex_t oC_UART_LLD_ChannelToChannelIndex( oC_UART_Channel_t Channel )
{
    return oC_Channel_ToIndex(UART,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_UART_Channel_t oC_UART_LLD_ChannelIndexToChannel( oC_UART_LLD_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex(UART,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_UART_Channel_t oC_UART_LLD_GetChannelOfModulePin( oC_UART_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_Module_TurnOn(oC_Module_UART_LLD);
        errorCode         = oC_ErrorCode_None;
        RxNotEmptyHandler = NULL;
        TxNotFullHandler  = NULL;
        UsedChannels      = 0;

    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_Module_TurnOff(oC_Module_UART_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetDriverInterruptHandlers( oC_UART_LLD_Interrupt_t RxNotEmpty , oC_UART_LLD_Interrupt_t TxNotFull)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsRam(RxNotEmpty) || IsRom(RxNotEmpty)  , oC_ErrorCode_WrongEventHandlerAddress) &&
            ErrorCondition( RxNotEmptyHandler  == NULL              , oC_ErrorCode_InterruptHandlerAlreadySet)  &&
            ErrorCondition( IsRam(TxNotFull) || IsRom(TxNotFull)    , oC_ErrorCode_WrongEventHandlerAddress) &&
            ErrorCondition( TxNotFullHandler  == NULL               , oC_ErrorCode_InterruptHandlerAlreadySet)
            )
        {
            RxNotEmptyHandler   = RxNotEmpty;
            TxNotFullHandler    = TxNotFull;
            errorCode           = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetPower( oC_UART_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_InterruptPriotity_t uartPriority = 10;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                                                   , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( Power == oC_Power_On || Power == oC_Power_Off                               , oC_ErrorCode_PowerStateNotCorrect) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,Power)                           , oC_ErrorCode_CannotEnableChannel) &&
            ErrorCondition( oC_Channel_EnableInterrupt(Channel,PeripheralInterrupt)                     , oC_ErrorCode_CannotEnableInterrupt) &&
            ErrorCondition( oC_Channel_SetInterruptPriority(Channel,PeripheralInterrupt, uartPriority)  , oC_ErrorCode_UARTInterruptConfigureError)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadPower( oC_UART_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsRam(outPower)           , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outPower = oC_Machine_GetPowerStateForChannel(Channel);
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_DisableOperations( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            UARTCTL(Channel)->UARTEN = 0;
            UARTCTL(Channel)->RXE    = 0;
            UARTCTL(Channel)->TXE    = 0;

            /* Waiting for end of transmissions */
            while(IsChannelBusy(Channel));

            UARTLCRH(Channel)->FEN   = 0;

            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_EnableOperations( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn)
        )
        {
            UARTCTL(Channel)->RXE       = 1;
            UARTCTL(Channel)->TXE       = 1;
            UARTCTL(Channel)->UARTEN    = 1;
            UARTIM(Channel)->RXIM       = 1;
            UARTIFLS(Channel)->RXIFLSEL = 0;
            UARTIFLS(Channel)->TXIFLSEL = 0;

            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_RestoreDefaultStateOnChannel( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                                , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,oC_Power_Off) , oC_ErrorCode_CannotRestoreDefaultState)
            )
        {

            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetWordLength( oC_UART_Channel_t Channel , oC_UART_LLD_WordLength_t WordLength )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel                 ) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn          ) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled )
            )
        {
            /*
             * This assertion fails, when someone will add, or change the #oC_UART_LLD_WordLength_t type.
             * Note, that values for word length are very important in lm4f machine, and when it is changed,
             * this function must be re-implemented.
             *
             *          Values for the WLEN in lm4f machine:
             *          Value | Word Length
             *          ------|-----------------------------
             *            0   | 5 bits
             *            1   | 6 bits
             *            2   | 7 bits
             *            3   | 8 bits
             *
             */
            oC_STATIC_ASSERT(
                       oC_UART_LLD_WordLength_5Bits == 0 &&
                       oC_UART_LLD_WordLength_6Bits == 1 &&
                       oC_UART_LLD_WordLength_7Bits == 2 &&
                       oC_UART_LLD_WordLength_8Bits == 3 ,
                       "WordLength values in the lm4f must have exactly values in range <0,3>! Please check function oC_UART_LLD_Set/ReadWordLength"
                       );

            UARTLCRH(Channel)->WLEN = WordLength;
            errorCode               = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadWordLength( oC_UART_Channel_t Channel , oC_UART_LLD_WordLength_t * outWordLength )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outWordLength)        , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            /*
             * This assertion fails, when someone will add, or change the #oC_UART_LLD_WordLength_t type.
             * Note, that values for word length are very important in lm4f machine, and when it is changed,
             * this function must be re-implemented.
             *
             *          Values for the WLEN in lm4f machine:
             *          Value | Word Length
             *          ------|-----------------------------
             *            0   | 5 bits
             *            1   | 6 bits
             *            2   | 7 bits
             *            3   | 8 bits
             *
             */
            oC_STATIC_ASSERT(
                       oC_UART_LLD_WordLength_5Bits == 0 &&
                       oC_UART_LLD_WordLength_6Bits == 1 &&
                       oC_UART_LLD_WordLength_7Bits == 2 &&
                       oC_UART_LLD_WordLength_8Bits == 3 ,
                       "WordLength values in the lm4f must have exactly values in range <0,3>! Please check function oC_UART_LLD_Set/ReadWordLength"
                       );

            *outWordLength = UARTLCRH(Channel)->WLEN;
            errorCode      = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetBitRate( oC_UART_Channel_t Channel , uint32_t BitRate )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled) &&
            ErrorCondition( BitRate > 0                     , oC_ErrorCode_BitRateNotCorrect)
            )
        {
        oC_Frequency_t      currentClockFrequency = oC_CLOCK_LLD_GetClockFrequency();
        float               brdFloat              = 0;
        uint8_t             clockDivisor          = 8;
        static const float  maxBrd                = (float)(0x10000);

#define CountBrd( clkFreq , clkDiv , BaudRate )   ((float)((clkFreq) / ( ((float)(clkDiv)) * ((float)(BaudRate)) )))

        brdFloat = CountBrd(currentClockFrequency,clockDivisor,BitRate);

        /* If baud rate divisor is too big, try to re-count it using grower clock divisor */
        if(brdFloat > maxBrd )
        {
            clockDivisor = 16;
            brdFloat     = CountBrd(currentClockFrequency,clockDivisor,BitRate);
        }

        if(brdFloat <= maxBrd )
        {
            oC_UInt_t integerBrdPart = (oC_UInt_t)brdFloat;
            float fractionalBrdPart  = brdFloat - (float)integerBrdPart;
            /* Write integer part */
            oC_Machine_WriteRegister(Channel, UARTIBRD , integerBrdPart );

            /* Write fractional part - the magic is from machine documentation, page 854 */
            oC_Machine_WriteRegister(Channel, UARTFBRD , (oC_UInt_t) (fractionalBrdPart * 64 + 0.5));

            /* Selecting between 8 and 16 clock divisor */
            UARTCTL(Channel)->HSE = (clockDivisor == 8) ? 1 : 0;

            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_BitRateNotSupported;
        }

#undef CountBrd
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadBitRate( oC_UART_Channel_t Channel , uint32_t * outBitRate )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outBitRate)               , oC_ErrorCode_OutputAddressNotInRAM)
        )
        {
            oC_Frequency_t currentClockFrequency = oC_CLOCK_LLD_GetClockFrequency();
            oC_UInt_t      integerBrdPart        = oC_Machine_ReadRegister(Channel, UARTIBRD);
            oC_UInt_t      fractionalBrdPart     = oC_Machine_ReadRegister(Channel, UARTFBRD);
            float          brd                   = (float)integerBrdPart;
            float          clockDivisor          = (UARTCTL(Channel)->HSE == 1) ? 8 : 16;

            if(oC_AssignErrorCodeIfFalse(&errorCode , integerBrdPart != 0 || fractionalBrdPart != 0 , oC_ErrorCode_BitRateNotSet ))
            {
                /* This magic is from machine documentation from the page 854 */
                brd += (((float)fractionalBrdPart) - 0.5) / 64;

                /* Protect again divide by 0 */
                if(oC_AssignErrorCodeIfFalse(&errorCode , brd != 0 , oC_ErrorCode_MachineCanBeDamaged))
                {
                    /* This magic is from machine documentation from the page 854 */
                    *outBitRate = (uint32_t)(currentClockFrequency / (clockDivisor * brd));

                    errorCode = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetParity( oC_UART_Channel_t Channel , oC_UART_LLD_Parity_t Parity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            if(Parity == oC_UART_LLD_Parity_Odd)
            {
                UARTLCRH(Channel)->PEN = 1;
                UARTLCRH(Channel)->EPS = 0;
            }
            else if(Parity == oC_UART_LLD_Parity_Even)
            {
                UARTLCRH(Channel)->PEN = 1;
                UARTLCRH(Channel)->EPS = 1;
            }
            else
            {
                UARTLCRH(Channel)->PEN = 0;
            }

            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadParity( oC_UART_Channel_t Channel , oC_UART_LLD_Parity_t * outParity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outParity)                , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(UARTLCRH(Channel)->PEN == 1)
            {
                if(UARTLCRH(Channel)->EPS == 0)
                {
                    *outParity = oC_UART_LLD_Parity_Odd;
                }
                else
                {
                    *outParity = oC_UART_LLD_Parity_Even;
                }
            }
            else
            {
                *outParity   = oC_UART_LLD_Parity_None;
            }
            errorCode      = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetStopBit( oC_UART_Channel_t Channel , oC_UART_LLD_StopBit_t StopBit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            if(StopBit == oC_UART_LLD_StopBit_1Bit)
            {
                UARTLCRH(Channel)->STP2 = 0;
            }
            else
            {
                UARTLCRH(Channel)->STP2 = 1;
            }

            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadStopBit( oC_UART_Channel_t Channel , oC_UART_LLD_StopBit_t * outStopBit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outStopBit)               , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(UARTLCRH(Channel)->STP2 == 0)
            {
                *outStopBit = oC_UART_LLD_StopBit_1Bit;
            }
            else
            {
                *outStopBit = oC_UART_LLD_StopBit_2Bits;
            }
            errorCode      = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetBitOrder( oC_UART_Channel_t Channel , oC_UART_LLD_BitOrder_t BitOrder )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                 , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)               , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)            , oC_ErrorCode_ChannelOperationsNotDisabled)  &&
            ErrorCondition( BitOrder == oC_UART_LLD_BitOrder_LSBFirst , oC_ErrorCode_BitOrderNotSupported)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadBitOrder( oC_UART_Channel_t Channel , oC_UART_LLD_BitOrder_t * outBitOrder )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outBitOrder)              , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outBitOrder   = oC_UART_LLD_BitOrder_LSBFirst;
            errorCode      = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetInvert( oC_UART_Channel_t Channel , oC_UART_LLD_Invert_t Invert )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)              , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)           , oC_ErrorCode_ChannelOperationsNotDisabled)  &&
            ErrorCondition( Invert == oC_UART_LLD_Invert_NotInverted , oC_ErrorCode_InvertNotSupported)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadInvert( oC_UART_Channel_t Channel , oC_UART_LLD_Invert_t * outInvert )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outInvert)                , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outInvert     = oC_UART_LLD_Invert_NotInverted;
            errorCode      = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetLoopback( oC_UART_Channel_t Channel , bool Loopback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            if(Loopback)
            {
                UARTCTL(Channel)->LBE = 1;
            }
            else
            {
                UARTCTL(Channel)->LBE = 0;
            }

            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadLoopback( oC_UART_Channel_t Channel , bool * outLoopback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outLoopback)              , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outLoopback   = UARTCTL(Channel)->LBE == 1;
            errorCode      = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ConnectModulePin( oC_UART_Pin_t ModulePin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_UART_Channel_t channel = oC_ModulePin_GetChannel(ModulePin);
        oC_Pin_t          pin     = oC_ModulePin_GetPin(ModulePin);

        if(
            ErrorCondition( IsChannelCorrect(channel)       , oC_ErrorCode_WrongChannel  ) &&
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(pin)   , oC_ErrorCode_PinNotDefined )
            )
        {
            bool unused = false;

            oC_MCS_EnterCriticalSection();

            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ArePinsUnused(pin,&unused)      ) &&
                ErrorCondition(        unused , oC_ErrorCode_PinIsUsed                              ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(pin)                ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_ConnectModulePin(ModulePin)   )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_GPIO_LLD_SetPinsUnused(pin);
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_DisconnectModulePin( oC_UART_Pin_t ModulePin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_UART_Channel_t channel = oC_ModulePin_GetChannel(ModulePin);
        oC_Pin_t          pin     = oC_ModulePin_GetPin(ModulePin);

        if(
            ErrorCondition( IsChannelCorrect(channel)       , oC_ErrorCode_WrongChannel  ) &&
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(pin)   , oC_ErrorCode_PinNotDefined )
            )
        {
            errorCode = oC_ErrorCode_NotImplemented;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetChannelUsed( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if( ErrorCondition(IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) )
        {
            UsedChannels |= (1<<oC_Channel_ToIndex(UART,Channel));
            errorCode     = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetChannelUnused( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if( ErrorCondition(IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) )
        {
            UsedChannels &= ~(1<<oC_Channel_ToIndex(UART,Channel));
            errorCode     = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelUsed( oC_UART_Channel_t Channel )
{
    bool used = false;

    if(oC_Module_IsTurnedOn(oC_Module_UART_LLD))
    {
        if(IsChannelCorrect(Channel))
        {
            used = (UsedChannels & (1<<oC_Channel_ToIndex(UART,Channel))) == (1<<oC_Channel_ToIndex(UART,Channel));
        }
    }

    return used;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ClearRxFifo( oC_UART_Channel_t Channel )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsRxFifoEmpty( oC_UART_Channel_t Channel )
{
    return IsSoftwareRxRingEmpty(Channel);
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsTxFifoFull( oC_UART_Channel_t Channel )
{
    return IsSoftwareTxRingFull(Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_UART_LLD_PutData( oC_UART_Channel_t Channel , char Data )
{
    oC_MCS_EnterCriticalSection();
    UARTIM(Channel)->TXIM    = 1;
    if(IsSoftwareTxRingEmpty(Channel) && IsTxFifoEmpty(Channel))
    {
        UARTDR(Channel)->DATA = Data;
    }
    else
    {
        PutToSoftwareRing(&TxRing(Channel),Data);
    }
    oC_MCS_ExitCriticalSection();
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
char oC_UART_LLD_GetData( oC_UART_Channel_t Channel )
{
    return GetFromSoftwareRing(&RxRing(Channel));
}
//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadModulePinsOfPin( oC_Pin_t Pin , oC_UART_Pin_t * outModulePinsArray , uint32_t * ArraySize , oC_UART_PinFunction_t PinFunction )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {

        if(
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(Pin)                          , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(ArraySize)                                       , oC_ErrorCode_OutputAddressNotInRAM) &&
            ErrorCondition(outModulePinsArray == NULL || IsRam(outModulePinsArray) , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            uint32_t foundPins = 0;

            errorCode = oC_ErrorCode_None;

            oC_ModulePin_ForeachDefined(modulePin)
            {
                if(modulePin->Pin == Pin && oC_Channel_IsCorrect(UART,modulePin->Channel) && modulePin->PinFunction == PinFunction)
                {
                    if(outModulePinsArray != NULL)
                    {
                        if(foundPins < *ArraySize)
                        {
                            outModulePinsArray[foundPins] = modulePin->ModulePinIndex;
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_OutputArrayToSmall;
                        }
                    }
                    foundPins++;
                }
            }

            *ArraySize = foundPins;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_Write( oC_UART_Channel_t Channel , const char * Buffer , oC_UInt_t Size , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
                        ErrorCondition( IsChannelCorrect(Channel)        , oC_ErrorCode_WrongChannel) &&
                        ErrorCondition( IsRam(Buffer) || IsRom(Buffer)   , oC_ErrorCode_WrongAddress) &&
                        ErrorCondition( Size > 0                         , oC_ErrorCode_SizeNotCorrect) &&
                        ErrorCondition( IsChannelPoweredOn(Channel)      , oC_ErrorCode_ChannelNotPoweredOn) &&
                        ErrorCondition( AreOperationsEnabled(Channel)    , oC_ErrorCode_ChannelOperationsNotEnabled)
        )
        {
            UARTDMACTL(Channel)->TXDMAE = 0;

            for(oC_UInt_t sendIndex = 0 ; sendIndex < Size ; sendIndex++ )
            {
                if( oC_Bits_AreBitsClearU32(IoFlags,oC_IoFlags_WaitForAllElements) && IsTxFifoFull(Channel) )
                {
                    break;
                }
                while(IsTxFifoFull(Channel));

                UARTDR(Channel)->DATA = (uint8_t)Buffer[sendIndex];
            }

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_Read( oC_UART_Channel_t Channel , char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)        , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)      , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outBuffer)                 , oC_ErrorCode_OutputAddressNotInRAM) &&
            ErrorCondition( Size > 0                         , oC_ErrorCode_SizeNotCorrect) &&
            ErrorCondition( AreOperationsEnabled(Channel)    , oC_ErrorCode_ChannelOperationsNotEnabled)
        )
        {
            UARTDMACTL(Channel)->RXDMAE = 0;

            errorCode = oC_ErrorCode_None;

            for(oC_UInt_t sendIndex = 0 ; sendIndex < Size ; sendIndex++ )
            {
                if( oC_Bits_AreBitsClearU32(IoFlags,oC_IoFlags_WaitForAllElements) && IsRxFifoEmpty(Channel) )
                {
                    if(sendIndex == 0)
                    {
                        errorCode = oC_ErrorCode_NoneElementReceived;
                    }
                    break;
                }
                while(IsRxFifoEmpty(Channel));

                outBuffer[sendIndex] = UARTDR(Channel)->DATA;

                if((IoFlags & oC_IoFlags_ReadOneLine) && (outBuffer[sendIndex] == '\n' || outBuffer[sendIndex] == '\r'))
                {
                    break;
                }
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_WriteWithDma( oC_UART_Channel_t Channel , const char * Buffer , oC_UInt_t Size )
{
    oC_ErrorCode_t       errorCode = oC_ErrorCode_ImplementError;
    oC_DMA_Channel_t channel   = 0;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)            , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)          , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(Buffer) || IsRom(Buffer)       , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size != 0                            , oC_ErrorCode_SizeNotCorrect) &&
        oC_AssignErrorCode(&errorCode , oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade(&channel,Channel,oC_Machine_DmaSignalType_TX) ) &&
        ErrorCondition( AreOperationsEnabled(Channel)    , oC_ErrorCode_ChannelOperationsNotEnabled)
        )
    {
        oC_DMA_LLD_PeripheralTradeConfig_t dmaConfig;

        dmaConfig.Buffer                        = (void*)Buffer;
        dmaConfig.BufferSize                    = Size;
        dmaConfig.ElementSize                   = sizeof(char);
        dmaConfig.PeripheralChannel             = Channel;
        dmaConfig.PeripheralData                = (void*)&UARTDR(Channel)->Value;
        dmaConfig.Priority                      = oC_DMA_LLD_Priority_Medium;
        dmaConfig.SignalType                    = oC_Machine_DmaSignalType_TX;
        dmaConfig.TransferCompleteEventHandler  = NULL;
        dmaConfig.TransmitDirection             = oC_DMA_LLD_Direction_Transmit;

        UARTDMACTL(Channel)->TXDMAE             = 1;

        errorCode = oC_DMA_LLD_ConfigurePeripheralTrade(channel,&dmaConfig);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadWithDma( oC_UART_Channel_t Channel , char * outBuffer , oC_UInt_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_DMA_Channel_t channel   = 0;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelCorrect(Channel)            , oC_ErrorCode_WrongChannel) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsChannelPoweredOn(Channel)          , oC_ErrorCode_ChannelNotPoweredOn) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRam(outBuffer)                     , oC_ErrorCode_OutputAddressNotInRAM) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size > 0                             , oC_ErrorCode_SizeNotCorrect) &&
        oC_AssignErrorCode(&errorCode , oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade(&channel,Channel,oC_Machine_DmaSignalType_RX) )&&
        ErrorCondition( AreOperationsEnabled(Channel)    , oC_ErrorCode_ChannelOperationsNotEnabled)
        )
    {
        oC_DMA_LLD_PeripheralTradeConfig_t dmaConfig;

        dmaConfig.Buffer                        = (void*)outBuffer;
        dmaConfig.BufferSize                    = Size;
        dmaConfig.ElementSize                   = sizeof(char);
        dmaConfig.PeripheralChannel             = Channel;
        dmaConfig.PeripheralData                = (void*)&UARTDR(Channel)->Value;
        dmaConfig.Priority                      = oC_DMA_LLD_Priority_Medium;
        dmaConfig.SignalType                    = oC_Machine_DmaSignalType_RX;
        dmaConfig.TransferCompleteEventHandler  = NULL;
        dmaConfig.TransmitDirection             = oC_DMA_LLD_Direction_Receive;

        UARTDMACTL(Channel)->RXDMAE             = 1;

        errorCode = oC_DMA_LLD_ConfigurePeripheralTrade(channel,&dmaConfig);
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


//==========================================================================================================================================
/**
 * @brief puts data to software ring
 */
//==========================================================================================================================================
static inline void PutToSoftwareRing( SoftwareRing_t * Ring , uint16_t Data )
{
    if(Ring->Counter >= SOFTWARE_RING_COUNT)
    {
        /* If ring is full, take the oldest data */
        GetFromSoftwareRing(Ring);
    }

    Ring->Buffer[Ring->PutIndex++] = Data;
    Ring->Counter++;

    if(Ring->PutIndex >= SOFTWARE_RING_COUNT)
    {
        Ring->PutIndex = 0;
    }
}

//==========================================================================================================================================
/**
 * @brief gets data from software ring
 */
//==========================================================================================================================================
static inline uint16_t GetFromSoftwareRing( SoftwareRing_t * Ring )
{
    uint16_t data = 0xFFFF;

    if(Ring->Counter > 0)
    {
        Ring->Counter--;
        data = Ring->Buffer[Ring->GetIndex++];
        if(Ring->GetIndex >= SOFTWARE_RING_COUNT)
        {
            Ring->GetIndex = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief clears data in software ring
 */
//==========================================================================================================================================
static inline void ClearSoftwareRing( SoftwareRing_t * Ring )
{
    memset(Ring,0,sizeof(SoftwareRing_t));
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

#define MODULE_NAME         UART
#define INTERRUPT_TYPE_NAME PeripheralInterrupt

//==========================================================================================================================================
/**
 * @brief common handler for UART interrupts
 */
//==========================================================================================================================================
oC_Channel_InterruptHandler(Channel)
{
    if(UARTRIS(Channel)->TXRIS == 1)
    {
        while(IsSoftwareTxRingEmpty(Channel) == false && (IsTxFifoFull(Channel) == false))
        {
            UARTDR(Channel)->DATA = GetFromSoftwareRing(&TxRing(Channel));
        }

        if(TxNotFullHandler)
        {
            TxNotFullHandler(Channel);
        }

        if(IsSoftwareTxRingEmpty(Channel))
        {
            UARTIM(Channel)->TXIM    = 0;
        }
    }

    if(UARTRIS(Channel)->RXRIS == 1)
    {
        while(IsRxFifoEmpty(Channel) == false)
        {
            PutToSoftwareRing(&RxRing(Channel),UARTDR(Channel)->DATA);
        }

        if(RxNotEmptyHandler)
        {
            RxNotEmptyHandler(Channel);
        }
    }

    //clear all interrupt
    oC_Machine_WriteRegister(Channel,UARTICR,0x17F2);
}
#undef INTERRUPT_TYPE_NAME

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________


