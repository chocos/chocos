/** ****************************************************************************************************************************************
 *
 * @brief      The file with source of functions for MCS module (ARM Cortex M7)
 *
 * @file       oc_mcs.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_mcs.h>
#include <oc_lsf.h>
#include <oc_interrupts.h>
#include <oc_array.h>
#include <oc_bits.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(Address)                      (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define IsCorrect(Address)                  oC_LSF_IsCorrectAddress(Address)
#define ALIGN_ADDRESS(Address,Alignment)    ((void*)((((oC_UInt_t)Address) + Alignment - 1) & ~(Alignment-1)))
#define IsAligned(Address,Alignment)        ( ALIGN_ADDRESS(Address,Alignment) == Address )


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * The type for storing context of the machine (for threads). It consist of the machine registers, that should be saved when the context is
 * switched.
 *
 * @note Order of these fields is very important and should be as in the machine.
 */
//==========================================================================================================================================
typedef volatile struct
{
    /* Software Registers - registers switched by the software*/
    oC_UInt_t       R4;
    oC_UInt_t       R5;
    oC_UInt_t       R6;
    oC_UInt_t       R7;
    oC_UInt_t       R8;
    oC_UInt_t       R9;
    oC_UInt_t       R10;
    oC_UInt_t       R11;
    /* Hardware Registers - registers switched by the hardware */
    oC_UInt_t       R0;
    oC_UInt_t       R1;
    oC_UInt_t       R2;
    oC_UInt_t       R3;
    oC_UInt_t       R12;
    oC_UInt_t       LR;
    oC_UInt_t       PC;
    oC_UInt_t       xPSR;
} Context_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    oC_Access_t     User;
    oC_Access_t     Privileged;
} AccessPermissions_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief pointer to the current stack (currently executed stack)
 */
//==========================================================================================================================================
static oC_Stack_t CurrentStack = NULL;
//==========================================================================================================================================
/**
 * @brief pointer to the next stack (will be executed with next context switching)
 */
//==========================================================================================================================================
static oC_Stack_t NextStack    = NULL;

//==========================================================================================================================================
/**
 * @brief pointer of the system stack
 */
//==========================================================================================================================================
static oC_Stack_t SystemStack  = NULL;
//==========================================================================================================================================
/**
 * @brief Stores next stack handler
 */
//==========================================================================================================================================
static oC_FindNextStackHandler_t GlobalFindNextStackHandler = NULL;
//==========================================================================================================================================
/**
 * @brief global variable with critical section nesting counter
 */
//==========================================================================================================================================
int16_t oC_MCS_CriticalSectionCounter = 0;

//==========================================================================================================================================
/**
 * @brief possible sizes of a memory region
 */
//==========================================================================================================================================
static const oC_UInt_t PossibleMemoryRegionSizes[] = {
                B(0) ,
                B(0) ,
                B(0) ,
                B(0) ,
                B(32) ,
                B(64) ,
                B(128) ,
                B(256) ,
                B(512) ,
                kB(1) ,
                kB(2) ,
                kB(4) ,
                kB(8) ,
                kB(16) ,
                kB(32) ,
                kB(64) ,
                kB(128) ,
                kB(256) ,
                kB(512) ,
                MB(1) ,
                MB(2) ,
                MB(4) ,
                MB(8) ,
                MB(16) ,
                MB(32) ,
                MB(64) ,
                MB(128) ,
                MB(256) ,
                MB(512) ,
                GB(1) ,
};

//==========================================================================================================================================
/**
 * List of possible access permissions (DONT EDIT THIS ARRAY!!!)
 *
 * The index of the array is the value to write to the MPU RASR register
 */
//==========================================================================================================================================
static const AccessPermissions_t PossibleAccessPermissions[] = {
                { .User = oC_Access_None      , .Privileged = oC_Access_None        } ,
                { .User = oC_Access_None      , .Privileged = oC_Access_RW          } ,
                { .User = oC_Access_R         , .Privileged = oC_Access_RW          } ,
                { .User = oC_Access_RW        , .Privileged = oC_Access_RW          } ,
                { .User = oC_Access_None      , .Privileged = oC_Access_None        } ,
                { .User = oC_Access_None      , .Privileged = oC_Access_R           } ,
                { .User = oC_Access_R         , .Privileged = oC_Access_R           } ,
                { .User = oC_Access_R         , .Privileged = oC_Access_R           } ,
};

#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline void            TriggerPendSV         ( void );
static        bool            IsStackCorrect        ( oC_Stack_t Stack );
static        oC_UInt_t       FindMemoryRegionSize  ( oC_UInt_t Size , bool AlignSize , uint8_t * outSizeId );
static        uint8_t         FindAccessPermissions ( oC_Access_t User , oC_Access_t Privileged );
static        void            SetPowerForMpu        ( oC_Power_t Power );
static        oC_Power_t      GetPowerOfMpu         ( void );


#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_HANDLERS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * Handler of SysTick interrupt. It increments system tick counter and checks if stack should be switched
 */
//==========================================================================================================================================
oC_InterruptHandler(System,SysTick)
{
    /* When this assertion fails, it means, that module was not initialized yet */
    oC_ASSERT(CurrentStack != NULL);

    /* Searching next stack if required and possible */
    if(GlobalFindNextStackHandler != NULL)
    {
        GlobalFindNextStackHandler();
    }

    /* When this assertion fails, it means, that the stack is overflowed */
    oC_ASSERT(CurrentStack->StackPointer >= CurrentStack->Buffer);

    /* If next stack is set */
    if(NextStack != NULL)
    {
        TriggerPendSV();
    }
}

//==========================================================================================================================================
/**
 * PendSV interrupt handler. It switches current stack
 */
//==========================================================================================================================================
__attribute__((naked)) oC_InterruptHandler(System,PendSV)
{
    oC_UInt_t r;

    __asm volatile (
          "MOV r12,    %1           \n\t" /* Move the pointer to the r12, because it will be overwritten */
          "MRS r1,     psp          \n\t" /* Move Process stack pointer (PSP) to the first available register (r12) */
          "STMDB r1!,  {r4-r11}     \n\t" /* Store software registers (registers, that are not switched by hardware) */
          "STR r1,     [r12, #0]    \n\t" /* Save the stack pointer to the memory */
                : "=r" (r)
                : "r"  (&CurrentStack->StackPointer)
    );

    __asm volatile (
          "MOV   r12,  %1          \n\t"
          "LDR   r1,   [%1, #0]    \n\t"
          "LDMFD r1!,  {r4-r11}    \n\t" /* Load software registers (registers, that are not switched by hardware) */
          "MSR   psp, r1           \n\t" /* Update PSP to new stack pointer */
          "STR   r1,   [r12, #0]   \n\t"
                    : "=r" (r)
                    : "r" (&NextStack->StackPointer)
   );

        CurrentStack   = NextStack;
        NextStack      = NULL;
   __asm volatile (
         "bx lr                 \n\t" /* This is a naked function, so it must be branched manually */
   );
}

#undef  _________________________________________INTERRUPTS_HANDLERS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/* This defines number of bits needed for priority storing. It is needed for CMSIS */
#define __NVIC_PRIO_BITS    oC_MACHINE_PRIO_BITS
#define __ICACHE_PRESENT   1
#define __DCACHE_PRESENT   1
#define __MPU_PRESENT      1

#include <core_cm7.h>

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_InitializeModule( void )
{
    /* This assertion fails, when process stack size is configured too small in linker configuration file */
    oC_ASSERT(oC_LSF_GetProcessStackSize() >= oC_MCS_GetMinimumStackBufferSize(0));

    /* Initialization of the system stack */
    SystemStack                 = oC_LSF_GetProcessStackStart();
    SystemStack->Buffer         = oC_LSF_GetProcessStackStart() + oC_MCS_AlignSize(sizeof(oC_StackData_t), oC_MCS_MEMORY_ALIGNMENT);
    SystemStack->BufferSize     = oC_LSF_GetProcessStackSize()  - oC_MCS_AlignSize(sizeof(oC_StackData_t), oC_MCS_MEMORY_ALIGNMENT);
    SystemStack->StackPointer   = oC_MCS_GetCurrentProcessStackPointer();

    /* At the start current stack is the system stack */
    CurrentStack = SystemStack;

    /* Enable I-Cache */
    SCB_EnableICache();

    /* Enable D-Cache */
    SCB_EnableDCache();

    return true;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void * oC_MCS_GetHardFaultReason( void )
{
    if(SCB->CFSR & (1<<7))
    {
        return (void*)SCB->MMFAR;
    }
    else
    {
        return (void*)SCB->BFAR;
    }
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void oC_MCS_EnableAllPossibleEvents( void )
{
    SCB->CCR |= SCB_CCR_DIV_0_TRP_Msk;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
const char * oC_MCS_GetHardFaultReasonDescription( void )
{
    const char * reason = "HARD Fault: unknown reason";

    if(SCB->HFSR & SCB_HFSR_VECTTBL_Msk)
    {
        reason = "BusFault on vector table read";
    }
    else if(SCB->HFSR & SCB_HFSR_FORCED_Msk)
    {
        uint32_t cfsr = SCB->CFSR;

        static const char * reasons[32] = {
                        [0]  = "the processor attempted an instruction fetch from a location that does not permit execution" ,
                        [1]  = "the processor attempted a load or store at a location that does not permit the operation."   ,
                        [2]  = NULL ,
                        [3]  = "unstack for an exception return has caused one or more access violations." ,
                        [4]  = "stacking for an exception entry has caused one or more access violations." ,
                        [5]  = NULL ,
                        [6]  = NULL ,
                        [7]  = NULL ,
                        [8]  = "instruction bus error." ,
                        [9]  = "a data bus error has occurred, and the PC value stacked for the exception return points to the instruction that caused the fault." ,
                        [10] = "a data bus error has occurred, but the return address in the stack frame is not related to the instruction that caused the error." ,
                        [11] = "unstack for an exception return has caused one or more BusFaults." ,
                        [12] = "stacking for an exception entry has caused one or more BusFaults." ,
                        [13] = NULL ,
                        [14] = NULL ,
                        [15] = NULL ,
                        [16] = "the processor has attempted to execute an undefined instruction." ,
                        [17] = "Illegal use of EPSR: probably trying to branch to address with 0 in the least significant bit" ,
                        [18] = "the processor has attempted an illegal load of EXC_RETURN to the PC, as a result of an invalid context, or an invalid EXC_RETURN value." ,
                        [19] = "the processor has attempted to access a coprocessor." ,
                        [20] = NULL ,
                        [21] = NULL ,
                        [22] = NULL ,
                        [23] = NULL ,
                        [24] = "the processor has made an unaligned memory access." ,
                        [25] = "Attempt to divide by 0" ,
                        [26] = NULL ,
                        [27] = NULL ,
                        [28] = NULL ,
                        [29] = NULL ,
                        [30] = NULL ,
                        [31] = NULL
        };

        for(int i = 0; i < 32; i++)
        {
            if(reasons[i] != NULL && (cfsr & (1<<i)))
            {
                reason = reasons[i];
                break;
            }
        }
    }

    return reason;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void oC_MCS_ClearHardFaultStatus( void )
{
    SCB->HFSR = SCB_HFSR_FORCED_Msk | SCB_HFSR_VECTTBL_Msk;

    SCB->CFSR = 0;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void oC_MCS_EnableInterrupts( void )
{
    __set_PRIMASK(0);
    __set_BASEPRI(0);
    __asm("cpsie   i");
    __asm volatile( "dsb" );
    __asm volatile( "isb" );
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes: In fact it does not disable interrupts - it just set possible interrupt priority to maximum. It is for correctly
 * handling kernel and context switching
 */
//==========================================================================================================================================
void oC_MCS_DisableInterrupts( void )
{
    __set_PRIMASK(1);
    __set_BASEPRI(0xFF);
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_AreInterruptsEnabled( void )
{
    uint32_t basepri = __get_BASEPRI();

    return basepri == 0;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_EnableInterrupt( IRQn_Type InterruptNumber )
{
    if(InterruptNumber >= 0)
    {
        NVIC_EnableIRQ(InterruptNumber);
        return true;
    }
    else
    {
        return false;
    }
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_DisableInterrupt( IRQn_Type InterruptNumber )
{
    if(InterruptNumber >= 0)
    {
        NVIC_DisableIRQ(InterruptNumber);
        return true;
    }
    else
    {
        return false;
    }
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_IsInterruptEnabled( IRQn_Type InterruptNumber )
{
    return (NVIC->ISER[(uint32_t)((int32_t)InterruptNumber) >> 5] & (uint32_t)(1 << ((uint32_t)((int32_t)InterruptNumber) & (uint32_t)0x1F))) != 0;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_SetInterruptPriority( IRQn_Type InterruptNumber , oC_InterruptPriotity_t Priority )
{
    NVIC_SetPriority(InterruptNumber , Priority);
    return true;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_InterruptPriotity_t oC_MCS_GetInterruptPriority( IRQn_Type InterruptNumber )
{
    return NVIC_GetPriority(InterruptNumber);
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void oC_MCS_Reboot( void )
{
    NVIC_SystemReset();
    while(1);
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_InitializeStack(
                oC_Stack_t *            outStack ,
                void *                  Buffer ,
                oC_Int_t                BufferSize ,
                oC_ContextHandler_t     ContextHandler  ,
                void *                  HandlerParameter ,
                oC_ContextExitHandler_t ExitHandler
                )
{
    bool success = false;

    if(
        BufferSize > oC_MCS_GetMinimumStackBufferSize(0) &&
        IsRam(Buffer)             &&
        IsRam(outStack)           &&
        oC_LSF_IsCorrectAddress(ContextHandler) &&
        oC_LSF_IsCorrectAddress(ExitHandler)
        )
    {
        /* ============================================
         *
         * --------------- [ Buffer ] -----------------
         *
         *  This part of buffer is free stack
         *
         * --------------- [context] ------------------
         *
         *   This part of buffer is initialized at the
         *   start as machine context
         *
         * ---------------- [stack] -------------------
         *   This part of buffer is for storing stack
         *   data
         * -------------- [ bufferEnd ] ---------------
         *
         * ============================================ */
        void *      bufferEnd       = &(((uint8_t*)Buffer)[BufferSize]);
        oC_Stack_t  stack           = oC_MCS_AlignStackPointer(bufferEnd - sizeof(oC_StackData_t) - sizeof(uint32_t));
        Context_t * context         = oC_MCS_AlignStackPointer(stack - sizeof(Context_t));

        if((((void*)stack) > Buffer) && (((void*)context) > Buffer))
        {
            /* Initializing registers of machine (in first start of this, it will be read by the hardware) */
            context->PC     = (oC_UInt_t)ContextHandler;            /* Program Counter set to the context handler function */
            context->xPSR   = oC_MCS_xPSR_INITIAL_VALUE;            /* Initial value for the xPSR */
            context->LR     = (oC_UInt_t)ExitHandler;               /* Link Register (address of return) */
            context->R0     = (oC_UInt_t)HandlerParameter;          /* Parameters pointer will be given to the r0 register */
            context->R1     = 0;
            context->R2     = 0;
            context->R3     = 0;
            context->R4     = 0;
            context->R5     = 0;
            context->R6     = 0;
            context->R7     = 0;
            context->R8     = 0;
            context->R9     = 0;
            context->R10    = 0;
            context->R11    = 0;
            context->R12    = 0;

            stack->StackPointer = (void*)context;
            stack->Buffer       = Buffer;
            stack->BufferSize   = BufferSize;

            *outStack           = stack;
            success             = true;

        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void * oC_MCS_GetCurrentProcessStackPointer( void )
{
    return (void*) __get_PSP();
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void * oC_MCS_GetCurrentMainStackPointer( void )
{
    return (void*) __get_MSP();
}
//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_Int_t oC_MCS_GetStackSize( oC_Stack_t Stack )
{
    oC_Int_t stackSize = 0;

    if(Stack == NULL)
    {
        Stack = CurrentStack;
    }

    if(IsStackCorrect(Stack))
    {
        stackSize = Stack->BufferSize;
    }

    return stackSize;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_Int_t oC_MCS_GetFreeStackSize( oC_Stack_t Stack )
{
    oC_Int_t freeStackSize          = 0;

    if(Stack == NULL)
    {
        Stack = CurrentStack;
    }

    if(IsRam(Stack))
    {
        void *   currentStackPointer    = NULL;

        if(CurrentStack == Stack)
        {
            currentStackPointer = oC_MCS_GetCurrentProcessStackPointer();
        }
        else
        {
            currentStackPointer = Stack->StackPointer;
        }

        if(currentStackPointer >= ((void*)Stack->Buffer))
        {
            freeStackSize = (oC_Int_t)(currentStackPointer - ((void*)Stack->Buffer));
        }
        else
        {
            freeStackSize -= (oC_Int_t)(((void*)Stack->Buffer) - currentStackPointer);
        }
    }

    return freeStackSize;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_Stack_t oC_MCS_GetCurrentStack( void )
{
    return CurrentStack;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_Int_t oC_MCS_GetMinimumStackBufferSize( oC_Int_t StackSize )
{
    return oC_MCS_AlignSize(sizeof(Context_t),      oC_MCS_STACK_MEMORY_ALIGNMENT) +
           oC_MCS_AlignSize(sizeof(oC_StackData_t), oC_MCS_MEMORY_ALIGNMENT) +
           oC_MCS_AlignSize(StackSize             , oC_MCS_STACK_MEMORY_ALIGNMENT);
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_SetNextStack( oC_Stack_t Stack )
{
    bool stackSwitched = false;

    if(IsStackCorrect(Stack))
    {
        oC_MCS_EnterCriticalSection();
        NextStack           = Stack;
        oC_MCS_ExitCriticalSection();

        stackSwitched       = true;
    }

    return stackSwitched;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_ConfigureSystemTimer( oC_UInt_t Prescaler , oC_FindNextStackHandler_t FindNextStackHandler )
{
    bool configured = false;

    if(Prescaler > 0 && oC_LSF_IsCorrectAddress(FindNextStackHandler))
    {
        if(SysTick_Config(Prescaler) == 0)
        {
            NVIC_SetPriority( PendSV_IRQn  , oC_InterruptPriority_Minimum - 1);
            NVIC_SetPriority( SysTick_IRQn , oC_InterruptPriority_Maximum + 1);

            GlobalFindNextStackHandler = FindNextStackHandler;

            configured = true;
        }
    }

    return configured;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_Stack_t oC_MCS_GetSystemStack( void )
{
    return SystemStack;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_ReturnToSystemStack( void )
{
    return oC_MCS_SetNextStack(SystemStack);
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void oC_MCS_Delay( register oC_UInt_t Cycles )
{
    Cycles /= 1; // 1 clock cycles per loop
    if(Cycles>0)
    {
        __asm volatile(
                        "mov r3, %[Cycles] \n\t"
                        "loop: \n\t"
                        "subs r3, #1 \n\t"
                        "bne loop \n\t"
                        :
                        : [Cycles] "r" (Cycles)
        );
    }
}


//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MCS_ConfigureMemoryRegion( const oC_MCS_MemoryRegionConfig_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(IsCorrect(Config)                                            , oC_ErrorCode_WrongAddress           )
     && ErrorCondition(IsCorrect(Config->BaseAddress)                               , oC_ErrorCode_WrongAddress           )
     && ErrorCondition(Config->Size > 0                                             , oC_ErrorCode_SizeNotCorrect         )
     && ErrorCondition(Config->Size <= UINT32_MAX                                   , oC_ErrorCode_SizeNotCorrect         )
     && ErrorCondition(Config->RegionNumber < oC_MCS_GetMaximumNumberOfRegions()    , oC_ErrorCode_RegionNumberNotCorrect )
        )
    {
        oC_UInt_t       foundRegionSize     = 0;
        uint8_t         accessPermissions   = 0;
        uint8_t         disableExecution    = oC_Bits_AreBitsSetU32(Config->UserAccess | Config->PrivilegedAccess , oC_Access_Execute) ? 0 : 1;
        uint8_t         sizeId              = 0;

        oC_Procedure_Begin
        {
            foundRegionSize     = FindMemoryRegionSize((oC_UInt_t)Config->Size,Config->AlignSize,&sizeId);
            accessPermissions   = FindAccessPermissions(Config->UserAccess,Config->PrivilegedAccess);

            oC_Procedure_ExitIfFalse( foundRegionSize   >= Config->Size              , oC_ErrorCode_SizeNotCorrect);
            oC_Procedure_ExitIfFalse( accessPermissions <  0xFF                      , oC_ErrorCode_AccessPermissionsNotPossible);
            oC_Procedure_ExitIfFalse( IsAligned(Config->BaseAddress,foundRegionSize) , oC_ErrorCode_AddressNotAligned);
            oC_Procedure_ExitIfFalse(
                                     ! (oC_Bits_AreBitsClearU32(Config->UserAccess,       oC_Access_R)
                                     && oC_Bits_AreBitsSetU32(  Config->UserAccess,       oC_Access_X) ) ,
                                      oC_ErrorCode_AccessPermissionsNotCorrect);
            oC_Procedure_ExitIfFalse(
                                     ! (oC_Bits_AreBitsClearU32(Config->PrivilegedAccess, oC_Access_R)
                                     && oC_Bits_AreBitsSetU32(  Config->PrivilegedAccess, oC_Access_X) ) ,
                                      oC_ErrorCode_AccessPermissionsNotCorrect);

            SetPowerForMpu(oC_Power_Off);
            MPU->RNR        = Config->RegionNumber;
            MPU->RBAR       = (uint32_t)Config->BaseAddress;
            MPU->RASR       = ((uint32_t)disableExecution                     << MPU_RASR_XN_Pos)   |
                              ((uint32_t)accessPermissions                    << MPU_RASR_AP_Pos)   |
                              ((uint32_t)0x00                                 << MPU_RASR_TEX_Pos)  |
                              (((uint32_t)Config->Shareable  ? 1 : 0)         << MPU_RASR_S_Pos)    |
                              (((uint32_t)Config->Cacheable  ? 1 : 0)         << MPU_RASR_C_Pos)    |
                              (((uint32_t)Config->Bufforable ? 1 : 0)         << MPU_RASR_B_Pos)    |
                              ((uint32_t)0x00                                 << MPU_RASR_SRD_Pos)  |
                              ((uint32_t)sizeId                               << MPU_RASR_SIZE_Pos) |
                              ((uint32_t)Config->Power == oC_Power_On ? 1 : 0 << MPU_RASR_ENABLE_Pos);

            SetPowerForMpu(oC_Power_On);

            errorCode = oC_ErrorCode_None;
        }
        oC_Procedure_End;

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
uint32_t oC_MCS_GetMaximumNumberOfRegions( void )
{
    return (MPU->TYPE & MPU_TYPE_DREGION_Msk) >> MPU_TYPE_DREGION_Pos;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
uint32_t oC_MCS_GetNumberOfRegions( void )
{
    uint32_t    numberOfRegions    = 0;
    uint32_t    maxNumberOfRegions = oC_MCS_GetMaximumNumberOfRegions();
    oC_Power_t  savedPowerState    = oC_Power_Off;

    oC_MCS_EnterCriticalSection();

    savedPowerState = GetPowerOfMpu();

    SetPowerForMpu(oC_Power_Off);

    for(uint32_t i = 0 ; i < maxNumberOfRegions ; i++)
    {
        MPU->RNR = i;

        if(oC_Bits_AreBitsSetU32(MPU->RASR, MPU_RASR_ENABLE_Msk))
        {
            numberOfRegions++;
        }
    }

    SetPowerForMpu(savedPowerState);

    oC_MCS_ExitCriticalSection();

    return numberOfRegions;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_ReadFreeRegionNumber( uint32_t * outFreeRegionNumber )
{
    bool found = false;

    if(IsRam(outFreeRegionNumber))
    {
        uint32_t    maxNumberOfRegions = oC_MCS_GetMaximumNumberOfRegions();
        oC_Power_t  savedPowerState    = oC_Power_Off;

        oC_MCS_EnterCriticalSection();

        savedPowerState = GetPowerOfMpu();

        SetPowerForMpu(oC_Power_Off);

        for(uint32_t i = 0 ; i < maxNumberOfRegions ; i++)
        {
            MPU->RNR = i;

            if(oC_Bits_AreBitsClearU32(MPU->RASR, MPU_RASR_ENABLE_Msk))
            {
                *outFreeRegionNumber = i;
                found = true;
                break;
            }
        }

        SetPowerForMpu(savedPowerState);

        oC_MCS_ExitCriticalSection();
    }

    return found;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_SetMemoryAccessMode( oC_MCS_MemoryAccessMode_t Mode )
{
    bool success = false;

    if(Mode == oC_MCS_MemoryAccessMode_Privileged)
    {
        MPU->CTRL  |= MPU_CTRL_PRIVDEFENA_Msk;
        success     = true;
    }
    else if(Mode == oC_MCS_MemoryAccessMode_User)
    {
        MPU->CTRL  &= ~MPU_CTRL_PRIVDEFENA_Msk;
        success     = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
oC_MCS_MemoryAccessMode_t oC_MCS_GetMemoryAccessMode( void )
{
    return    ((MPU->CTRL & MPU_CTRL_PRIVDEFENA_Msk) != 0)
           || ((MPU->CTRL & MPU_CTRL_ENABLE_Msk)     == 0)
                    ? oC_MCS_MemoryAccessMode_Privileged : oC_MCS_MemoryAccessMode_User;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void oC_MCS_PrintToDebugger( const char * Message )
{
    while(*Message)
    {
        ITM_SendChar(*Message++);
    }
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
bool oC_MCS_IsDebuggerConnected( void )
{
    return (CoreDebug->DHCSR & CoreDebug_DHCSR_C_DEBUGEN_Msk) != 0;
}

//==========================================================================================================================================
/**
 * @note ARM Cortex M7 notes:
 */
//==========================================================================================================================================
void oC_MCS_HALT( const char* Message )
{
    oC_MCS_PrintToDebugger(Message);
    while(!oC_MCS_IsDebuggerConnected());
    __BKPT(1);
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * Triggers PendSV interrupt
 */
//==========================================================================================================================================
static inline void TriggerPendSV(void)
{
    SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
}

//==========================================================================================================================================
/**
 * Checks if stack is correct (if stack pointer is correct address and if stack pointer is correct)
 */
//==========================================================================================================================================
static bool IsStackCorrect( oC_Stack_t Stack )
{
    return  IsRam(Stack) &&
           (oC_MCS_IsStackPointerAligned(Stack->StackPointer)) &&
           (Stack->StackPointer <  (Stack->Buffer + Stack->BufferSize)) &&
           (Stack->StackPointer >= (Stack->Buffer));
}

//==========================================================================================================================================
/**
 * @brief searches for possible memory region size
 *
 * The function searches possible size of the memory region in the global array.
 *
 * @param Size          Size to find in the global array
 * @param AlignSize     If true, the function will return the nearest size
 * @param outSizeId     ID of the size that has been found (value to write in the register)
 *
 * @return 0 if not found, otherwise possible memory region size
 */
//==========================================================================================================================================
static oC_UInt_t FindMemoryRegionSize( oC_UInt_t Size , bool AlignSize , uint8_t * outSizeId )
{
    oC_UInt_t     foundSize = 0;
    uint8_t       sizeId    = 0;

    /* Searching for the size */
    oC_ARRAY_FOREACH_IN_ARRAY(PossibleMemoryRegionSizes,regionSize)
    {
        if((*regionSize) > 0)
        {
            if((*regionSize) == Size)
            {
                foundSize = *regionSize;
                break;
            }
            else if(AlignSize)
            {
                foundSize = *regionSize;

                if(foundSize > Size)
                {
                    break;
                }
            }
        }
        sizeId++;
    }

    if(foundSize > 0)
    {
        *outSizeId = sizeId;
    }

    return foundSize;
}

//==========================================================================================================================================
/**
 * @brief searches for possible access permissions value to the MPU RASR register
 *
 * @return 0xFF if not found, otherwise value to the MPU RASR register
 */
//==========================================================================================================================================
static uint8_t FindAccessPermissions( oC_Access_t User , oC_Access_t Privileged )
{
    uint8_t foundAccessPermissions = 0xFF;
    uint8_t arraySize              = oC_ARRAY_SIZE(PossibleAccessPermissions);

    /* Only RW flags are stored in the array */
    User        &= oC_Access_RW;
    Privileged  &= oC_Access_RW;

    for(uint8_t i = 0; i < arraySize ; i++)
    {
        if(User == PossibleAccessPermissions[i].User && Privileged == PossibleAccessPermissions[i].Privileged)
        {
            foundAccessPermissions = i;
        }
    }

    return foundAccessPermissions;
}

//==========================================================================================================================================
/**
 * @brief sets power (on/off) for the MPU module
 */
//==========================================================================================================================================
static void SetPowerForMpu( oC_Power_t Power )
{
    if(Power == oC_Power_On)
    {
        /* Enable the MPU */
        MPU->CTRL  |= MPU_CTRL_ENABLE_Msk;

        /* Enable fault exceptions */
        SCB->SHCSR |= SCB_SHCSR_MEMFAULTENA_Msk;
    }
    else
    {
        /* Disable fault exceptions */
        SCB->SHCSR &= ~SCB_SHCSR_MEMFAULTENA_Msk;

        /* Disable the MPU */
        MPU->CTRL  &= ~MPU_CTRL_ENABLE_Msk;
    }
}

//==========================================================================================================================================
/**
 * @brief returns power state for the MPU
 */
//==========================================================================================================================================
static oC_Power_t GetPowerOfMpu( void )
{
    return oC_Bits_AreBitsSetU32(MPU->CTRL , MPU_CTRL_ENABLE_Msk) ? oC_Power_On : oC_Power_Off;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
