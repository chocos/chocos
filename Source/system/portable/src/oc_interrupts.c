/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_interrupts.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_interrupts.h>
#include <oc_null.h>
#include <oc_lsf.h>
#include <oc_array.h>

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

/* Pointer from linker */
extern const char __main_stack_end;

/* Stores unexpected interrupt handler pointer - interface variable */
oC_InterruptHandler_t oC_UnexpectedInterruptHandler = NULL;

/* Prototypes for special interrupt handlers */
oC_ResetInterruptHandlerPrototype;
oC_DefaultInterruptHandlerPrototype;

/* Creates weak prototypes for each interrupt */
#define CREATE_INTERRUPT_WEAK_PROTOTYPE( BASE_NAME , INTERRUPT_TYPE , INTERRUPT_NUMBER , VECTOR_NUMBER )     \
        oC_InterruptHandlerWeakPrototype( BASE_NAME , INTERRUPT_TYPE );
oC_MACHINE_INTERRUPTS_LIST(CREATE_INTERRUPT_WEAK_PROTOTYPE)
#undef CREATE_INTERRUPT_WEAK_PROROTYPE

/* Interrupts vector array */
void (*const vectors[])(void) __attribute__ ((section (".vectors"))) =
{
   (void (*)(void))&__main_stack_end,
   oC_ResetInterruptHandlerName,
#define PUSH_INTERRUPT_TO_VECTOR( BASE_NAME , INTERRUPT_TYPE , INTERRUPT_NUMBER , VECTOR_NUMBER , ...)       \
    [VECTOR_NUMBER] = oc_InterruptHandlerName(BASE_NAME , INTERRUPT_TYPE) ,
   oC_MACHINE_INTERRUPTS_LIST(PUSH_INTERRUPT_TO_VECTOR)
#undef PUSH_INTERRUPT_TO_VECTOR
};

const oC_InterruptData_t oC_InterruptData[oC_InterruptIndex_NumberOfElements] = {
#define PUSH_INTERRUPT_TO_ARRAY( BASE_NAME , INTERRUPT_TYPE , INTERRUPT_NUMBER , VECTOR_NUMBER , ...)       \
    [VECTOR_NUMBER] = { .BaseAddress    = oC_InterruptBaseAddress_(BASE_NAME,INTERRUPT_TYPE) , \
                        .Type           = oC_InterruptType_(INTERRUPT_TYPE) ,\
                        .InterruptIndex = oC_InterruptIndex_(BASE_NAME , INTERRUPT_TYPE) ,\
                        .InterruptNumber= oC_InterruptNumber_(BASE_NAME , INTERRUPT_TYPE)\
    } ,
   oC_MACHINE_INTERRUPTS_LIST(PUSH_INTERRUPT_TO_ARRAY)
#undef PUSH_INTERRUPT_TO_ARRAY
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief returns data pointer for interrupt
 *
 * The function returns interrupt data pointer from the #oC_InterruptData array.
 *
 * @param BaseAddress           Base address to match
 * @param InterruptType         Type of interrupt to find
 *
 * @return pointer to the array entry or NULL if not found
 */
//==========================================================================================================================================
const oC_InterruptData_t * oC_Interrupt_GetData( oC_InterruptBaseAddress_t BaseAddress , oC_InterruptType_t InterruptType )
{
    const oC_InterruptData_t * data = NULL;

    oC_ARRAY_FOREACH_IN_ARRAY(oC_InterruptData,interruptData)
    {
        if(interruptData->BaseAddress == BaseAddress && interruptData->Type == InterruptType)
        {
            data = interruptData;
            break;
        }
    }

    return data;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupt handlers
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPT_HANDLERS_SECTION_________________________________________________________________

//==========================================================================================================================================
/**
 * Default handler of interrupts. It will be called, when the interrupt handler is not set.
 */
//==========================================================================================================================================
oC_DefaultInterruptHandler
{
    // Checking if pointer is correct, and calling unexpected interrupt handler
    if(oC_LSF_IsCorrectAddress(oC_UnexpectedInterruptHandler))
    {
        oC_UnexpectedInterruptHandler();
    }
}

#undef  _________________________________________INTERRUPT_HANDLERS_SECTION_________________________________________________________________
