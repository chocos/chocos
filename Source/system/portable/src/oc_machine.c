/** ****************************************************************************************************************************************
 *
 * @brief      The file with source for machine module
 *
 * @file       oc_machine.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_machine.h>
#include <oc_assert.h>

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * Array with DMA channel assignments
 */
//==========================================================================================================================================
const oC_Machine_DmaChannelAssignment_t oC_DmaChannelAssignments[oC_Machine_DmaChannelAssignmentIndex_NumberOfElements] = {
#define ADD_DMA_CHANNEL_ASSIGNMENT( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC )               \
     oC_Machine_DmaChannelAssignment_( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC ) ,
    oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST(ADD_DMA_CHANNEL_ASSIGNMENT)
#undef ADD_DMA_CHANNEL_ASSIGNMENT
};

//==========================================================================================================================================
/**
 * Verification of the registers sizes
 */
//==========================================================================================================================================
#define MAKE_BIT(BIT_NAME , SIZE )
#define MAKE_REGISTER(REGISTER_NAME)            oC_STATIC_ASSERT( oC_RegisterSize_(REGISTER_NAME) == (sizeof(oC_UInt_t) * 8) , "Number of bits defined in the register " #REGISTER_NAME " is not correct!");

oC_REGISTERS_LIST(MAKE_REGISTER)

#undef MAKE_BIT
#undef MAKE_REGISTER


#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________
