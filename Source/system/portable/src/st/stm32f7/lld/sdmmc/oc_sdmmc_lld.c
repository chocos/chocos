/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_lld.c
 *
 * @brief      The file with source for SDMMC LLD functions
 *
 * @file       oc_sdmmc_lld.c
 *
 * @author     Patryk Kubiak - (Created on: 2017-08-07 - 22:44:51)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_sdmmc_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <oc_debug.h>
#include <oc_gpio_lld.h>
#include <oc_gpio_mslld.h>
#include <oc_saipll.h>
#include <oc_datapackage.h>

#ifdef oC_SDMMC_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

// Set this definition to `true` if you want to enable debug logs from the LLD, otherwise set it to `false`
#define DEBUGLOGS_ENABLED                   true
// There is a possibility, that a thread will be killed before reading response for a command. In such situation module can be blocked
// as the response is not received, but despite of it, someone tries to send a new one. To prevent this, we provide this flag, that allows
// for disabling checking of the state before sending new command. If it is set to `false`, only warning to debuglogs will be printed
#define CHECK_COMMAND_STATE_BEFORE_SEND_NEW true
#define SDMMC_SOURCE_CLK_FREQUENCY          oC_MHz(48)
#define INIT_MAX_CLK_FREQUENCY              oC_kHz(400)
#define SIZE_OF_RX_SOFTWARE_BUFFER          1024
#define IsRam(Address)                      oC_LSF_IsRamAddress(Address)
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define IsChannelCorrect(Channel)           oC_Channel_IsCorrect(SDMMC,Channel)
#define IsChannelPoweredOn(Channel)         (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)
#define ChannelIndex(Channel)               oC_Channel_ToIndex(SDMMC,Channel)
#define NUMBER_OF_CHANNELS                  oC_ModuleChannel_NumberOfElements(SDMMC)
#define MAX_COMMAND_INDEX                   0x3F
#define MAX_RESPONSE_TYPE                   oC_SDMMC_LLD_ResponseType_NumberOfElements
#define SDMMC_STA(Channel)                  oC_Channel_Register(Channel,SDMMC_STA)
#define SDMMC_ICR(Channel)                  oC_Channel_Register(Channel,SDMMC_ICR)
#define SDMMC_CMD(Channel)                  oC_Channel_Register(Channel,SDMMC_CMD)
#define SDMMC_ARG(Channel)                  oC_Channel_Register(Channel,SDMMC_ARG)
#define SDMMC_CLKCR(Channel)                oC_Channel_Register(Channel,SDMMC_CLKCR)
#define SDMMC_DCTRL(Channel)                oC_Channel_Register(Channel,SDMMC_DCTRL)
#define SDMMC_DLEN(Channel)                 oC_Channel_Register(Channel,SDMMC_DLEN)
#define SDMMC_DTIMER(Channel)               oC_Channel_Register(Channel,SDMMC_DTIMER)
#define SDMMC_RESPCMD(Channel)              oC_Channel_Register(Channel,SDMMC_RESPCMD)
#define SDMMC_RESP1(Channel)                oC_Channel_Register(Channel,SDMMC_RESP1)
#define SDMMC_RESP2(Channel)                oC_Channel_Register(Channel,SDMMC_RESP2)
#define SDMMC_RESP3(Channel)                oC_Channel_Register(Channel,SDMMC_RESP3)
#define SDMMC_RESP4(Channel)                oC_Channel_Register(Channel,SDMMC_RESP4)
#define SDMMC_MASK(Channel)                 oC_Channel_Register(Channel,SDMMC_MASK)
#define SDMMC_POWER(Channel)                oC_Channel_Register(Channel,SDMMC_POWER)
#define SDMMC_FIFOCNT(Channel)              oC_Channel_Register(Channel,SDMMC_FIFOCNT)
#define SDMMC_FIFO(Channel)                 oC_Channel_Register(Channel,SDMMC_FIFO)
#define SDMMC_DTIMER(Channel)               oC_Channel_Register(Channel,SDMMC_DTIMER)
#define RCC_DCKCFGR2                        oC_Register(RCC,RCC_DCKCFGR2)
#if DEBUGLOGS_ENABLED
#   define debuglog( LogType, ... )         kdebuglog( LogType, "SDMMC-LLD: " __VA_ARGS__ )
#else
#   define debuglog( LogType, ... )
#endif

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores command transfer state
 */
//==========================================================================================================================================
typedef enum
{
    CommandState_NotReady,                                                      //!< Module is not ready to send new commands
    CommandState_ReadyToSendCommand,                                            //!< Module is ready to send new command
    CommandState_WaitsForCommandResponse,                                       //!< Module is waiting for response for previous command
    CommandState_WaitsForCommandResponseReceiving,                              //!< Module is waiting for user to receive response of last command
} CommandState_t;

//==========================================================================================================================================
/**
 * @brief state for data transfer
 */
//==========================================================================================================================================
typedef enum
{
    DataTransferState_NotReady,                 //!< Module is not ready to transfer data
    DataTransferState_DataReceiving,            //!< The channel waits for a data from the card
    DataTransferState_DataSending,              //!< The module waits for data to send
} DataTransferState_t;

#undef  _________________________________________TYPES______________________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_SDMMC_LLD_InterruptHandler_t      GlobalInterruptHandler                                              = NULL;
static oC_SDMMC_LLD_InterruptSource_t       GlobalInterruptSource                                               = oC_SDMMC_LLD_InterruptSource_None;
static CommandState_t                       GlobalCommandStates[NUMBER_OF_CHANNELS]                             = { 0 };
static DataTransferState_t                  GlobalTransferStates[NUMBER_OF_CHANNELS]                            = { 0 };
static oC_MemorySize_t                      GlobalSectorSizes[NUMBER_OF_CHANNELS]                               = { 0 };
static oC_Frequency_t                       GlobalClkFrequency[NUMBER_OF_CHANNELS]                              = { 0 };
static uint8_t                              RxBuffer[NUMBER_OF_CHANNELS][SIZE_OF_RX_SOFTWARE_BUFFER]            = { { 0 } };
static oC_DataPackage_t                     RxDataPackage[NUMBER_OF_CHANNELS]                                   = { { 0 } };
static oC_DataPackage_t*                    TxDataPackage[NUMBER_OF_CHANNELS]                                   = { NULL };

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES___________________________________________________________________________

static inline bool              AreAllInterruptSourcesSupported     ( oC_SDMMC_LLD_InterruptSource_t Source );
static inline bool              IsResponseTypeSupported             ( oC_SDMMC_LLD_ResponseType_t ResponseType );
static inline bool              IsSectorSizeSupported               ( oC_MemorySize_t SectorSize );
static inline bool              IsWideBusSupported                  ( oC_SDMMC_LLD_WideBus_t WideBus );
static        void              CallInterruptHandler                ( oC_SDMMC_Channel_t Channel, oC_SDMMC_LLD_InterruptSource_t InterruptSource );
static        void              EnableInterrupts                    ( oC_SDMMC_Channel_t Channel );
static        oC_ErrorCode_t    ConfigureClock                      ( const oC_SDMMC_LLD_Config_t * Config, oC_RegisterType_SDMMC_CLKCR_t * outClkCr );
static inline oC_ErrorCode_t    SetBlockSize                        ( oC_RegisterType_SDMMC_DCTRL_t* outRegister, oC_MemorySize_t BlockSize );

#undef  _________________________________________LOCAL_PROTOTYPES___________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SDMMC_LLD_IsChannelCorrect( oC_SDMMC_Channel_t Channel )
{
    return oC_Channel_IsCorrect(SDMMC,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SDMMC_LLD_IsChannelIndexCorrect( oC_SDMMC_LLD_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(SDMMC);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SDMMC_LLD_ChannelIndex_t oC_SDMMC_LLD_ChannelToChannelIndex( oC_SDMMC_Channel_t Channel )
{
    return oC_Channel_ToIndex(SDMMC,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SDMMC_Channel_t oC_SDMMC_LLD_ChannelIndexToChannel( oC_SDMMC_LLD_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex(SDMMC,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SDMMC_Channel_t oC_SDMMC_LLD_GetChannelOfModulePin( oC_SDMMC_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        oC_Module_TurnOn(oC_Module_SDMMC_LLD);
        GlobalInterruptHandler  = NULL;
        GlobalInterruptSource   = oC_SDMMC_LLD_InterruptSource_None;
        errorCode               = oC_ErrorCode_None;

        oC_SDMMC_LLD_ForEachChannel(channel)
        {
            GlobalCommandStates [ ChannelIndex(channel) ] = CommandState_ReadyToSendCommand;
            GlobalTransferStates[ ChannelIndex(channel) ] = DataTransferState_NotReady;
            GlobalSectorSizes   [ ChannelIndex(channel) ] = 0;
            GlobalClkFrequency  [ ChannelIndex(channel) ] = 0;

            memset(RxBuffer[ ChannelIndex(channel) ], 0, SIZE_OF_RX_SOFTWARE_BUFFER);

            oC_DataPackage_Initialize(
                            &RxDataPackage[ ChannelIndex(channel) ],
                            RxBuffer[ ChannelIndex(channel) ],
                            SIZE_OF_RX_SOFTWARE_BUFFER,
                            1,
                            false);
            TxDataPackage[ ChannelIndex(channel) ] = NULL;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        oC_Module_TurnOff(oC_Module_SDMMC_LLD);
        GlobalInterruptHandler  = NULL;
        GlobalInterruptSource   = oC_SDMMC_LLD_InterruptSource_None;
        errorCode               = oC_ErrorCode_None;

        oC_SDMMC_LLD_ForEachChannel( channel )
        {
            oC_Channel_DisableInterrupt( channel, PeripheralInterrupt );

            GlobalCommandStates [ ChannelIndex(channel) ] = CommandState_NotReady;
            GlobalTransferStates[ ChannelIndex(channel) ] = DataTransferState_NotReady;
            GlobalSectorSizes   [ ChannelIndex(channel) ] = 0;
            GlobalClkFrequency  [ ChannelIndex(channel) ] = 0;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_SetPower( oC_SDMMC_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                               , oC_ErrorCode_WrongChannel             )
         && ErrorCondition( Power == oC_Power_On || Power == oC_Power_Off           , oC_ErrorCode_PowerStateNotCorrect     )
         && ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,Power)       , oC_ErrorCode_CannotEnableChannel      )
            )
        {
            if ( ErrorCondition( oC_Channel_EnableInterrupt(Channel,PeripheralInterrupt) , oC_ErrorCode_CannotEnableInterrupt    ) )
            {
                oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(SDMMC,Channel);

                oC_ASSERT(channelIndex < oC_ModuleChannel_NumberOfElements(SDMMC));

                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfErrorOccur("Cannot disable power for SDMMC channel", oC_Machine_SetPowerStateForChannel(Channel,Power));
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_ReadPower( oC_SDMMC_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel           )
         && ErrorCondition( IsRam(outPower)           , oC_ErrorCode_OutputAddressNotInRAM  )
            )
        {
            *outPower = oC_Machine_GetPowerStateForChannel(Channel);
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_SetInterruptHandler( oC_SDMMC_LLD_InterruptHandler_t Handler, oC_SDMMC_LLD_InterruptSource_t EnabledSources )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        if(
            ErrorCondition( GlobalInterruptHandler == NULL                      , oC_ErrorCode_InterruptHandlerAlreadySet   )
         && ErrorCondition( IsRam(Handler) || IsRom(Handler)                    , oC_ErrorCode_WrongEventHandlerAddress     )
         && ErrorCondition( EnabledSources != oC_SDMMC_LLD_InterruptSource_None , oC_ErrorCode_IncorrectMask                )
         && ErrorCondition( AreAllInterruptSourcesSupported(EnabledSources)     , oC_ErrorCode_NotSupportedOnTargetMachine  )
            )
        {
            GlobalInterruptHandler  = Handler;
            GlobalInterruptSource   = EnabledSources;
            errorCode               = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_Configure( const oC_SDMMC_LLD_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if(
            ErrorCondition( IsRam(Config) || IsRom(Config)                          , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( IsChannelCorrect( Config->Channel )                     , oC_ErrorCode_WrongChannel             )
         && ErrorCondition( Config->SectorSize > 0                                  , oC_ErrorCode_SizeNotCorrect           )
         && ErrorCondition( IsSectorSizeSupported(Config->SectorSize)               , oC_ErrorCode_SizeNotSupported         )
         && ErrorCondition( Config->WideBus < oC_SDMMC_LLD_WideBus_NumberOfElements , oC_ErrorCode_WideBusNotCorrect        )
         && ErrorCondition( IsWideBusSupported( Config->WideBus )                   , oC_ErrorCode_WideBusNotSupported      )
            )
        {
            if( ErrorCode( oC_SDMMC_LLD_SetPower( Config->Channel, oC_Power_On ) ) )
            {
                oC_RegisterType_SDMMC_CLKCR_t clkcr = { .Value = 0 };
                if ( ErrorCode( ConfigureClock( Config, &clkcr ) ) )
                {
                    GlobalSectorSizes[ ChannelIndex(Config->Channel) ] = Config->SectorSize;
                    GlobalCommandStates[ ChannelIndex(Config->Channel) ] = CommandState_ReadyToSendCommand;
                    EnableInterrupts( Config->Channel );

                    clkcr.WIDBUS    = Config->WideBus == oC_SDMMC_LLD_WideBus_1Bit ? 0 :
                                      Config->WideBus == oC_SDMMC_LLD_WideBus_4Bit ? 1 :
                                      Config->WideBus == oC_SDMMC_LLD_WideBus_8Bit ? 2 : 0;

                    SDMMC_CLKCR( Config->Channel )->Value   = clkcr.Value;
                    SDMMC_POWER( Config->Channel )->PWRCTRL = 0x3; // 0x3 enables power for the card, 0x00 disables the power
                    SDMMC_DTIMER( Config->Channel )->Value = 0x10000;
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    oC_SaveIfErrorOccur("Cannot disable SDMMC channel power", oC_SDMMC_LLD_SetPower(Config->Channel, oC_Power_Off));
                }
            }
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_Unconfigure( oC_SDMMC_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        oC_MCS_EnterCriticalSection();
        volatile oC_RegisterType_SDMMC_STA_t* sta = SDMMC_STA(Channel);
        (void)sta;
        if(
            ErrorCondition( IsChannelCorrect(Channel)                                   , oC_ErrorCode_WrongChannel             )
         && ErrorCondition( IsChannelPoweredOn(Channel)                                 , oC_ErrorCode_ChannelNotConfigured     )
         && ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel, oC_Power_Off)   , oC_ErrorCode_CannotDisableChannel     )
            )
        {
            GlobalCommandStates[ ChannelIndex(Channel) ] = CommandState_NotReady;
            errorCode = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_ConfigurePin( oC_SDMMC_Channel_t Channel , oC_Pin_t Pin , oC_SDMMC_PinFunction_t PinFunction )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                           , oC_ErrorCode_WrongChannel             )
         && ErrorCondition( oC_GPIO_LLD_IsSinglePin(Pin)                        , oC_ErrorCode_NotSinglePin             )
         && ErrorCondition( oC_GPIO_LLD_IsPinDefined(Pin)                       , oC_ErrorCode_PinNotDefined            )
         && ErrorCondition( PinFunction < oC_SDMMC_PinFunction_NumberOfElements , oC_ErrorCode_PinFunctionNotCorrect    )
            )
        {
            bool unused = false;

            errorCode = oC_ErrorCode_NoneOfModulePinAssigned;

            oC_ModulePin_ForeachDefined(modulePin)
            {
                if(
                    modulePin->Pin == Pin
                 && modulePin->PinFunction == PinFunction
                 && modulePin->Channel == (oC_Channel_t)Channel
                    )
                {
                    oC_MCS_EnterCriticalSection();

                    if(
                        ErrorCode     ( oC_GPIO_LLD_ArePinsUnused(Pin,&unused)                                                  )
                     && ErrorCondition(        unused , oC_ErrorCode_PinIsUsed                                                  )
                     && ErrorCode     ( oC_GPIO_LLD_SetPinsUsed(Pin)                                                            )
                     && ErrorCode     ( oC_GPIO_MSLLD_ConnectModulePin(modulePin->ModulePinIndex)                               )
                     && ErrorCode     ( oC_GPIO_LLD_SetSpeed(Pin, oC_GPIO_LLD_Speed_Maximum)                                    )
                     && ErrorCode     ( oC_GPIO_LLD_SetOutputCircuit(Pin, oC_GPIO_LLD_OutputCircuit_PushPull)                   )
                     && ErrorCode     ( oC_GPIO_LLD_SetPull(Pin, oC_GPIO_LLD_Pull_Up)                                         )
                        )
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                    else
                    {
                        debuglog( oC_LogType_Error, "Cannot configure module pin: %R", errorCode );
                        oC_GPIO_LLD_SetPinsUnused(Pin);
                    }

                    oC_MCS_ExitCriticalSection();
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SDMMC_LLD_IsPinAvailable( oC_SDMMC_Channel_t Channel , oC_Pin_t Pin , oC_SDMMC_PinFunction_t PinFunction )
{
    bool pinAvailable = false;
    bool pinIsUsed    = false;

    if(oC_Module_IsTurnedOn(oC_Module_SPI_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if(
            oC_SaveIfFalse( "Channel"       , IsChannelCorrect(Channel)                             , oC_ErrorCode_WrongChannel            )
         && oC_SaveIfFalse( "Pin"           , oC_GPIO_LLD_IsSinglePin(Pin)                          , oC_ErrorCode_NotSinglePin            )
         && oC_SaveIfFalse( "Pin"           , oC_GPIO_LLD_IsPinDefined(Pin)                         , oC_ErrorCode_PinNotDefined           )
         && oC_SaveIfErrorOccur( "Pin"      , oC_GPIO_LLD_CheckIsPinUsed(Pin,&pinIsUsed)                                                   )
         && oC_SaveIfFalse( "Pin"           , pinIsUsed == false                                    , oC_ErrorCode_PinIsUsed               )
         && oC_SaveIfFalse( "PinFunction"   , PinFunction < oC_SDMMC_PinFunction_NumberOfElements   , oC_ErrorCode_PinFunctionNotCorrect   )
            )
        {
            pinAvailable = PinFunction == 0;
            oC_ModulePin_ForeachDefined(modulePin)
            {
                if(
                    modulePin->Pin == Pin
                 && modulePin->PinFunction == PinFunction
                 && modulePin->Channel == (oC_Channel_t)Channel
                    )
                {
                    pinAvailable = true;
                    break;
                }
            }
        }

        oC_MCS_ExitCriticalSection();
    }

    return pinAvailable;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_UnconfigurePin( oC_Pin_t Pin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();

        if( ErrorCode( oC_GPIO_MSLLD_DisconnectPin(Pin,0) ) )
        {
            errorCode = oC_GPIO_LLD_SetPinsUnused(Pin);
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_SetWideBus( oC_SDMMC_Channel_t Channel, oC_SDMMC_LLD_WideBus_t WideBus )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if(
            ErrorCondition( IsChannelCorrect( Channel )                     , oC_ErrorCode_WrongChannel             )
         && ErrorCondition( IsChannelPoweredOn(Channel)                     , oC_ErrorCode_ChannelNotConfigured     )
         && ErrorCondition( WideBus < oC_SDMMC_LLD_WideBus_NumberOfElements , oC_ErrorCode_WideBusNotCorrect        )
         && ErrorCondition( IsWideBusSupported( WideBus )                   , oC_ErrorCode_WideBusNotSupported      )
            )
        {
                oC_RegisterType_SDMMC_CLKCR_t clkcr = { .Value = SDMMC_CLKCR( Channel )->Value };

                clkcr.WIDBUS    = WideBus == oC_SDMMC_LLD_WideBus_1Bit ? 0 :
                                  WideBus == oC_SDMMC_LLD_WideBus_4Bit ? 1 :
                                  WideBus == oC_SDMMC_LLD_WideBus_8Bit ? 2 : 0;

                SDMMC_CLKCR( Channel )->Value   = clkcr.Value;
                debuglog( oC_LogType_Track, "Wide bus has been set to %d\n", WideBus );
                errorCode = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_SendCommand( oC_SDMMC_Channel_t Channel, const oC_SDMMC_LLD_Command_t * Command )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();

        if(
            ErrorCondition( IsChannelCorrect( Channel )                     , oC_ErrorCode_WrongChannel                     )
         && ErrorCondition( IsChannelPoweredOn( Channel )                   , oC_ErrorCode_ChannelNotConfigured             )
         && ErrorCondition( IsRam( Command ) || IsRom( Command )            , oC_ErrorCode_WrongAddress                     )
         && ErrorCondition( Command->CommandIndex <= MAX_COMMAND_INDEX      , oC_ErrorCode_CommandNotCorrect                )
         && ErrorCondition( Command->ResponseType <= MAX_RESPONSE_TYPE      , oC_ErrorCode_ResponseTypeNotCorrect           )
         && ErrorCondition( IsResponseTypeSupported(Command->ResponseType)  , oC_ErrorCode_ResponseTypeNotSupported         )
         && ErrorCondition( SDMMC_STA(Channel)->CMDACT == 0                 , oC_ErrorCode_CommandTransmissionInProgress    )
#if CHECK_COMMAND_STATE_BEFORE_SEND_NEW
         && ErrorCondition( GlobalCommandStates[ ChannelIndex(Channel) ] == CommandState_ReadyToSendCommand
                         || SDMMC_STA(Channel)->CTIMEOUT == 1               , oC_ErrorCode_ResponseNotReceived              )
#endif
            )
        {
            oC_RegisterType_SDMMC_CMD_t cmd = { .Value = 0 };

#if CHECK_COMMAND_STATE_BEFORE_SEND_NEW == false
            if( GlobalCommandStates[ ChannelIndex(Channel) ] != CommandState_ReadyToSendCommand )
            {
                debuglog( oC_LogType_Warning, "Channel %s is not ready to send new command, but we do it: %d", oC_Channel_GetName(Channel)
                          , GlobalCommandStates[ ChannelIndex(Channel) ] );
            }
#endif

            cmd.CMDINDEX    = Command->CommandIndex;
            cmd.WAITRESP    = Command->ResponseType == oC_SDMMC_LLD_ResponseType_None           ? 0 :
                              Command->ResponseType == oC_SDMMC_LLD_ResponseType_ShortResponse  ? 1 :
                              Command->ResponseType == oC_SDMMC_LLD_ResponseType_LongResponse   ? 3 : 0;

            // If this bit is set, the command to be sent is a suspend command (to be used only with SDIO card).
            cmd.SDIOSuspend = Command->SuspendCommand ? 1 : 0;

            // CPSM Waits for ends of data transfer (CmdPend internal signal). If this bit is set, the CPSM waits for the end of data
            // transfer before it starts sending a command. This feature is available only with Stream data transfer mode SDMMC_DCTRL[2] = 1
            cmd.WAITPEND    = 0;

            // If this bit is set, the CPSM is enabled.
            cmd.CPSMEN      = 1;

            // If this bit is set, the CPSM disables command timeout and waits for an interrupt request.
            cmd.WAITINT     = 0;

            SDMMC_ARG(Channel)->Value = Command->Argument;
            SDMMC_CMD(Channel)->Value = cmd.Value;

            // Block the module until response is not received by an user
            GlobalCommandStates[ ChannelIndex(Channel) ] = Command->ResponseType == oC_SDMMC_LLD_ResponseType_None ?
                                                                CommandState_ReadyToSendCommand :
                                                                CommandState_WaitsForCommandResponse;

            errorCode = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_ReadCommandResponse( oC_SDMMC_Channel_t Channel , oC_SDMMC_LLD_CommandResponse_t * outResponse )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();

        if(
            ErrorCondition( IsChannelCorrect( Channel )                 , oC_ErrorCode_WrongChannel             )
         && ErrorCondition( IsChannelPoweredOn( Channel )               , oC_ErrorCode_ChannelNotConfigured     )
         && ErrorCondition( IsRam(outResponse)                          , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( GlobalCommandStates[ ChannelIndex(Channel) ] == CommandState_WaitsForCommandResponseReceiving
                         || SDMMC_STA(Channel)->CMDREND == 1            , oC_ErrorCode_ResponseNotReady         )
            )
        {
            outResponse->CommandIndex = SDMMC_RESPCMD(Channel)->RESPCMD;
            outResponse->Response[0]  = SDMMC_RESP1(Channel)->Value;
            outResponse->Response[1]  = SDMMC_RESP2(Channel)->Value;
            outResponse->Response[2]  = SDMMC_RESP3(Channel)->Value;
            outResponse->Response[3]  = SDMMC_RESP4(Channel)->Value;

            GlobalCommandStates[ ChannelIndex(Channel) ] = CommandState_ReadyToSendCommand;

            errorCode = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 * timeout is not implemented yet, it is hardcoded to the maximum
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_PrepareForTransfer( oC_SDMMC_Channel_t Channel, const oC_SDMMC_LLD_TransferConfig_t* TransferConfig )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();

        if (
            ErrorCondition( IsChannelCorrect(Channel)                                                           , oC_ErrorCode_WrongChannel                 )
         && ErrorCondition( IsRom(TransferConfig) || IsRam(TransferConfig)                                      , oC_ErrorCode_WrongAddress                 )
         && ErrorCondition( TransferConfig->Timeout  > 0                                                        , oC_ErrorCode_TimeNotCorrect               )
         && ErrorCondition( TransferConfig->DataSize > 0                                                        , oC_ErrorCode_SizeNotCorrect               )
         && ErrorCondition( TransferConfig->DataSize <= 0x1FFFFFF                                               , oC_ErrorCode_SizeNotSupported             )
         && ErrorCondition( TransferConfig->BlockSize > 0                                                       , oC_ErrorCode_BlockSizeNotCorrect          )
         && ErrorCondition( TransferConfig->TransferDirection < oC_SDMMC_LLD_TransferDirection_NumberOfElements , oC_ErrorCode_TransferDirectionNotCorrect  )
         && ErrorCondition( TransferConfig->TransferMode      < oC_SDMMC_LLD_TransferMode_NumberOfElements      , oC_ErrorCode_TransferModeNotCorrect       )
            )
        {
            oC_RegisterType_SDMMC_DCTRL_t dctrl = { .Value = 0 };

            if ( ErrorCode( SetBlockSize(&dctrl, TransferConfig->BlockSize) ) )
            {
                dctrl.DTEN = 1;
                dctrl.DTDIR = TransferConfig->TransferDirection == oC_SDMMC_LLD_TransferDirection_Read ? 1 : 0;
                dctrl.DTMODE = TransferConfig->TransferMode == oC_SDMMC_LLD_TransferMode_Stream ? 1 : 0;
                dctrl.DMAEN = TransferConfig->UseDma ? 1 : 0;
                dctrl.DBLOCKSIZE = 0x9;

                SDMMC_DTIMER(Channel)->Value = 0xFFFFFFFF; // TODO: Calculate real timeout
                SDMMC_DLEN(Channel)->Value  = TransferConfig->DataSize;
                SDMMC_DCTRL(Channel)->Value = dctrl.Value;
                EnableInterrupts(Channel);

                oC_DataPackage_Clear(&RxDataPackage[ ChannelIndex(Channel) ]);

                GlobalTransferStates[ ChannelIndex(Channel) ] = TransferConfig->TransferDirection == oC_SDMMC_LLD_TransferDirection_Read ?
                                                                    DataTransferState_DataReceiving : DataTransferState_DataSending;

                debuglog(oC_LogType_Track, "Prepared for transfer, DTIMER: 0x%08X, DLEN: 0x%08X DCTRL: 0x%08X\n",
                         SDMMC_DTIMER(Channel)->Value,
                         SDMMC_DLEN(Channel)->Value,
                         SDMMC_DCTRL(Channel)->Value
                         );

                CallInterruptHandler(Channel, oC_SDMMC_LLD_InterruptSource_TransmissionRestarted);

                errorCode = oC_ErrorCode_None;
            }
            else
            {
                debuglog(oC_LogType_Error, "Block Size of %d is not supported\n", TransferConfig->BlockSize);
            }

        }

        oC_MCS_ExitCriticalSection();
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_WriteDataPackage( oC_SDMMC_Channel_t Channel, oC_DataPackage_t * DataPackage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();

        oC_ChannelIndex_t channelIndex = ChannelIndex(Channel);
        if(
            ErrorCondition( IsChannelCorrect(Channel)           , oC_ErrorCode_WrongChannel                 )
         && ErrorCondition( IsChannelPoweredOn(Channel)         , oC_ErrorCode_ChannelNotConfigured         )
         && ErrorCondition( IsRam(DataPackage)                  , oC_ErrorCode_AddressNotInRam              )
         && ErrorCondition( DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
                                                                , oC_ErrorCode_DataPackageNotInitialized    )
         && ErrorCondition( !oC_DataPackage_IsEmpty(DataPackage), oC_ErrorCode_DataPackageIsEmpty           )
         && ErrorCondition( IsRam(DataPackage->ReadBuffer)
                         || IsRom(DataPackage->ReadBuffer)      , oC_ErrorCode_WrongAddress                 )
         && ErrorCondition( GlobalTransferStates[channelIndex]
                            == DataTransferState_DataSending    , oC_ErrorCode_NotSupportedInTheCurrentState )
            )
        {
//            errorCode = oC_ErrorCode_NoSpaceAvailable;

            while( DataPackage->DataLength > 0 && SDMMC_STA(Channel)->TXFIFOF == 0 )
            {
                SDMMC_FIFO(Channel)->Value = oC_DataPackage_GetDataU32(DataPackage);
                errorCode = oC_ErrorCode_None;
            }

            if ( TxDataPackage[channelIndex] != DataPackage )
            {
                TxDataPackage[channelIndex] = DataPackage;
                errorCode = oC_ErrorCode_None;
            }
            EnableInterrupts(Channel);
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_LLD_ReadDataPackage( oC_SDMMC_Channel_t Channel, oC_DataPackage_t * DataPackage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();

        oC_ChannelIndex_t channelIndex = ChannelIndex(Channel);

        if(
            ErrorCondition( IsChannelCorrect(Channel)           , oC_ErrorCode_WrongChannel                     )
         && ErrorCondition( IsChannelPoweredOn(Channel)         , oC_ErrorCode_ChannelNotConfigured             )
         && ErrorCondition( IsRam(DataPackage)                  , oC_ErrorCode_AddressNotInRam                  )
         && ErrorCondition( DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
                                                                , oC_ErrorCode_DataPackageNotInitialized        )
         && ErrorCondition( !oC_DataPackage_IsFull(DataPackage) , oC_ErrorCode_DataPackageIsFull                )
         && ErrorCondition( DataPackage->ReadOnly == false      , oC_ErrorCode_DataPackageIsReadOnly            )
         && ErrorCondition( IsRam(DataPackage->ReadBuffer)      , oC_ErrorCode_AddressNotInRam                  )
         && ErrorCondition( GlobalTransferStates[channelIndex]
                            == DataTransferState_DataReceiving, oC_ErrorCode_NotSupportedInTheCurrentState )
            )
        {
            errorCode = oC_ErrorCode_NoDataToReceive;

            oC_DataPackage_t* rxSoftwareDataPackage = &RxDataPackage[ channelIndex ];
            if ( DataPackage != rxSoftwareDataPackage )
            {
                while ( !oC_DataPackage_IsEmpty(rxSoftwareDataPackage)
                     && !oC_DataPackage_PutDataU8(DataPackage,  oC_DataPackage_GetDataU8(rxSoftwareDataPackage) ) )
                {
                    errorCode = oC_ErrorCode_None;
                }
            }

            while( SDMMC_STA(Channel)->RXFIFOE == 0 && !oC_DataPackage_PutDataU32( DataPackage, SDMMC_FIFO(Channel)->Value ) )
            {
                errorCode = oC_ErrorCode_None;
            }

            EnableInterrupts(Channel);
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief returns true if all of interrupt sources listed in the `Source` variable are supported
 */
//==========================================================================================================================================
static inline bool AreAllInterruptSourcesSupported( oC_SDMMC_LLD_InterruptSource_t Source )
{
    static const oC_SDMMC_LLD_InterruptSource_t supportedSources = oC_SDMMC_LLD_InterruptSource_CommandResponseReceived
                                                                 | oC_SDMMC_LLD_InterruptSource_CommandSent
                                                                 | oC_SDMMC_LLD_InterruptSource_DataReadyToReceive
                                                                 | oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData
                                                                 | oC_SDMMC_LLD_InterruptSource_TransmissionError
                                                                 | oC_SDMMC_LLD_InterruptSource_CommandTransmissionError
                                                                 | oC_SDMMC_LLD_InterruptSource_TransmissionRestarted
                                                                 | oC_SDMMC_LLD_InterruptSource_TransmissionFinished
                                                                 ;
    return (Source & (~supportedSources)) == 0;
}

//==========================================================================================================================================
/**
 * @brief returns true if response type is supported by the target
 */
//==========================================================================================================================================
static inline bool IsResponseTypeSupported( oC_SDMMC_LLD_ResponseType_t ResponseType )
{
    return ResponseType == oC_SDMMC_LLD_ResponseType_None
        || ResponseType == oC_SDMMC_LLD_ResponseType_LongResponse
        || ResponseType == oC_SDMMC_LLD_ResponseType_ShortResponse;
}

//==========================================================================================================================================
/**
 * @brief returns true if sector size is supported
 */
//==========================================================================================================================================
static inline bool IsSectorSizeSupported( oC_MemorySize_t SectorSize )
{
    bool supported = false;

    for( oC_MemorySize_t i = 0; i < 15 && !supported; i++ )
    {
        supported = SectorSize == (1 << i);
    }

    return supported;
}

//==========================================================================================================================================
/**
 * @brief returns true if wide bus is supported
 */
//==========================================================================================================================================
static inline bool IsWideBusSupported( oC_SDMMC_LLD_WideBus_t WideBus )
{
    return WideBus == oC_SDMMC_LLD_WideBus_1Bit
        || WideBus == oC_SDMMC_LLD_WideBus_4Bit
        || WideBus == oC_SDMMC_LLD_WideBus_8Bit;
}

//==========================================================================================================================================
/**
 * @brief calls interrupt handler if it is not NULL
 */
//==========================================================================================================================================
static void CallInterruptHandler( oC_SDMMC_Channel_t Channel, oC_SDMMC_LLD_InterruptSource_t InterruptSource )
{
    if( GlobalInterruptHandler != NULL )
    {
        InterruptSource ^= ~GlobalInterruptSource;
        if( InterruptSource != oC_SDMMC_LLD_InterruptSource_None )
        {
            GlobalInterruptHandler( Channel, InterruptSource );
        }
    }
    else
    {
        debuglog( oC_LogType_Error, "Interrupt has not been handled - handler is not set!" );
    }
}

//==========================================================================================================================================
/**
 * @brief enables needed interrupts on the given channel
 */
//==========================================================================================================================================
static void EnableInterrupts( oC_SDMMC_Channel_t Channel )
{
    if( GlobalInterruptHandler == NULL )
    {
        debuglog( oC_LogType_Warning, "interrupt handler is not set" );
    }
    if( GlobalInterruptSource & ( oC_SDMMC_LLD_InterruptSource_CommandResponseReceived | oC_SDMMC_LLD_InterruptSource_CommandSent ) )
    {
        SDMMC_MASK(Channel)->CMDRENDIE = 1;
        SDMMC_MASK(Channel)->CCRCFAILIE = 1;
    }
    if( GlobalInterruptSource & oC_SDMMC_LLD_InterruptSource_CommandSent )
    {
        SDMMC_MASK(Channel)->CMDSENTIE = 1;
    }
    if( GlobalInterruptSource & oC_SDMMC_LLD_InterruptSource_CommandTransmissionError )
    {
        SDMMC_MASK(Channel)->CTIMEOUTIE = 1;
        SDMMC_MASK(Channel)->CCRCFAILIE = 1;
    }
    if( GlobalInterruptSource & oC_SDMMC_LLD_InterruptSource_TransmissionError )
    {
        SDMMC_MASK(Channel)->DTIMEOUTIE = 1;
        SDMMC_MASK(Channel)->DCRCFAILIE = 1;
    }
    if( GlobalInterruptSource & oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData )
    {
        SDMMC_MASK(Channel)->TXFIFOHEIE = 1;
        SDMMC_MASK(Channel)->TXFIFOEIE  = 1;
        SDMMC_MASK(Channel)->DATAENDIE  = 1;
    }
    if( GlobalInterruptSource & oC_SDMMC_LLD_InterruptSource_DataReadyToReceive )
    {
        SDMMC_MASK(Channel)->RXFIFOHFIE = 1;
        SDMMC_MASK(Channel)->RXFIFOFIE = 1;
        SDMMC_MASK(Channel)->DATAENDIE = 1;
    }
    if( GlobalInterruptSource & oC_SDMMC_LLD_InterruptSource_TransmissionFinished )
    {
        SDMMC_MASK(Channel)->DATAENDIE = 1;
    }
    SDMMC_MASK(Channel)->SDIOITIE = 1;
}

//==========================================================================================================================================
/**
 * @brief searches for clock divisor
 */
//==========================================================================================================================================
static bool FindClockDivisor( oC_Frequency_t MaxFrequency, uint8_t * outDivisor )
{
    bool found = false;

    for( uint16_t CLKDIV = 0; CLKDIV <= 0xFF; CLKDIV++ )
    {
        oC_Frequency_t realFrequency = SDMMC_SOURCE_CLK_FREQUENCY / ((oC_Frequency_t)( CLKDIV + 2 ));
        if( realFrequency <= MaxFrequency )
        {
            *outDivisor = CLKDIV;
            found       = true;
            break;
        }
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief sets SDMMC lock frequency
 */
//==========================================================================================================================================
oC_ErrorCode_t SetClockFrequency( oC_Frequency_t Frequency, oC_RegisterType_SDMMC_CLKCR_t * outClkCr )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    uint8_t        CLKDIV    = 0;

    if( ErrorCondition( FindClockDivisor( Frequency, &CLKDIV ), oC_ErrorCode_FrequencyNotPossible ) )
    {
        outClkCr->Value     = 0;
        outClkCr->CLKDIV    = CLKDIV;
        outClkCr->CLKEN     = 1;

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures SDMMC clock
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureClock( const oC_SDMMC_LLD_Config_t * Config, oC_RegisterType_SDMMC_CLKCR_t * outClkCr )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCode( oC_SaiPll_Configure( oC_SaiPll_OutputLine_PLL48CLK, SDMMC_SOURCE_CLK_FREQUENCY, INIT_MAX_CLK_FREQUENCY, NULL) )
     && ErrorCode( SetClockFrequency( INIT_MAX_CLK_FREQUENCY, outClkCr ) )
        )
    {
        // Set PLL48CLK as source for SDMMC
        RCC_DCKCFGR2->SDMMCSEL = 0;
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets the block size in DCTRL register
 *
 * @param outRegister           register to write with the block size
 * @param BlockSize             size of the block memory to convert
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetBlockSize( oC_RegisterType_SDMMC_DCTRL_t* outRegister, oC_MemorySize_t BlockSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_BlockSizeNotSupported;

    for( uint8_t dblockSize = 0; dblockSize < 0xF; dblockSize++ )
    {
        oC_MemorySize_t blockSize = 1<<dblockSize;
        if ( BlockSize == blockSize )
        {
            outRegister->DBLOCKSIZE = dblockSize;
            errorCode = oC_ErrorCode_None;
            break;
        }
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_________________________________________________________________________________

//==========================================================================================================================================
/**
 *              INTERRUPT HANDLER FOR SPI Peripheral Interrupt
 * */
//==========================================================================================================================================
#define MODULE_NAME              SDMMC
#define INTERRUPT_TYPE_NAME      PeripheralInterrupt

oC_Channel_InterruptHandler(Channel)
{
    oC_SDMMC_LLD_InterruptSource_t  interruptSource = oC_SDMMC_LLD_InterruptSource_None;
    oC_RegisterType_SDMMC_STA_t     sta             = { .Value = SDMMC_STA(Channel)->Value };
    oC_RegisterType_SDMMC_MASK_t    mask            = { .Value = SDMMC_MASK(Channel)->Value };
    oC_ChannelIndex_t channelIndex = ChannelIndex(Channel);
    (void)mask;
    debuglog(oC_LogType_Track, "IRQ: STA = 0x%08X\n", sta.Value);
    // if CMDSENT is set, command has been sent (no response required)
    if( sta.CMDSENT == 1 )
    {
        interruptSource |= oC_SDMMC_LLD_InterruptSource_CommandSent;
        GlobalCommandStates[ channelIndex ] = CommandState_ReadyToSendCommand;

        SDMMC_ICR(Channel)->CMDSENTC = 1;
    }

    // if CTIMEOUT is set, command response timeout occurred
    // if CCRCFAIL is set, command response received (CRC check failed)
    if( sta.CTIMEOUT == 1 || sta.CCRCFAIL == 1)
    {
        interruptSource |= oC_SDMMC_LLD_InterruptSource_CommandTransmissionError
                         | oC_SDMMC_LLD_InterruptSource_CommandSent;
        GlobalCommandStates[ channelIndex ] = CommandState_ReadyToSendCommand;

        SDMMC_ICR(Channel)->CTIMEOUTC = 1;
        SDMMC_ICR(Channel)->CCRCFAILC = 1;
    }

    // if CMDREND is set, command response has been received (CRC check passed)
    // CRC can be failed for example for ACMD41, for R3:
    //      The ACMD41 response is R3 and in the specification, the place for CRC is just reserved '1111111',
    //      that means that response R3 has no CRC check!!! Thats the reason why SDIO peripheral sets the CRC error flag.
    // Source:
    //      https://community.st.com/s/question/0D50X00009XkaGDSAZ/sdio-command-crc-error-on-acmd41
    if( sta.CMDREND == 1 || sta.CCRCFAIL == 1 )
    {
        interruptSource |= oC_SDMMC_LLD_InterruptSource_CommandSent
                        | oC_SDMMC_LLD_InterruptSource_CommandResponseReceived;

        GlobalCommandStates[ channelIndex ] = CommandState_WaitsForCommandResponseReceiving;

        SDMMC_ICR(Channel)->CMDRENDC = 1;
    }

    // if DTIMEOUT is set, data timeout occurred
    // if DCRCFAIL is set, data block sent/received (CRC check failed)
    if( sta.DTIMEOUT == 1 || sta.DCRCFAIL == 1)
    {
        interruptSource |= oC_SDMMC_LLD_InterruptSource_TransmissionError
                         | oC_SDMMC_LLD_InterruptSource_CommandSent;

        SDMMC_ICR(Channel)->DTIMEOUTC = 1;
        SDMMC_ICR(Channel)->DCRCFAILC = 1;
    }

    if ( GlobalTransferStates[channelIndex] == DataTransferState_DataReceiving )
    {
        if ( sta.DATAEND == 1 )
        {
            interruptSource |= oC_SDMMC_LLD_InterruptSource_TransmissionFinished;
            SDMMC_ICR(Channel)->DATAENDC = 1;
        }
        // if Rx FIFO is half full or DATAEND has been received
        if( sta.RXFIFOHF == 1 || sta.DATAEND == 1 )
        {
            interruptSource |= oC_SDMMC_LLD_InterruptSource_DataReadyToReceive;
        }
    }
    else if ( GlobalTransferStates[channelIndex] == DataTransferState_DataSending )
    {
        oC_DataPackage_t* dataPackage = TxDataPackage[channelIndex];
        if ( dataPackage != NULL && dataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER )
        {
            while( dataPackage->DataLength >= sizeof(uint32_t) && SDMMC_STA(Channel)->TXFIFOF == 0 )
            {
                SDMMC_FIFO(Channel)->Value = oC_DataPackage_GetDataU32(dataPackage);
            }
        }
        if( SDMMC_STA(Channel)->DATAEND == 1 )
        {
            SDMMC_ICR(Channel)->DATAENDC = 1;
            if ( dataPackage == NULL || dataPackage->DataLength == 0 )
            {
                interruptSource |= oC_SDMMC_LLD_InterruptSource_TransmissionFinished;
                TxDataPackage[channelIndex] = NULL;
            }
        }
    }


    SDMMC_ICR(Channel)->SDIOITC = 1;

    CallInterruptHandler( Channel, interruptSource );

    // If all the data has been sent, disable interrupt
    if ( SDMMC_STA(Channel)->TXFIFOE == 1 || SDMMC_STA(Channel)->TXFIFOHE == 1 )
    {
        SDMMC_MASK(Channel)->TXFIFOHEIE = 0;
        SDMMC_MASK(Channel)->TXFIFOEIE  = 0;
    }

    // If user did not clear the FIFO, read it to the software buffer
    if( SDMMC_STA(Channel)->RXFIFOHF == 1 || SDMMC_STA(Channel)->RXFIFOF == 1 )
    {
        oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
        while( SDMMC_STA(Channel)->RXFIFOHF == 1
            && ErrorCode( oC_SDMMC_LLD_ReadDataPackage(Channel, &RxDataPackage[ channelIndex ])) );

        // If the RX FIFO is still not read completely, we have to disable interrupt
        if ( SDMMC_STA(Channel)->RXFIFOHF == 1 )
        {
            debuglog(oC_LogType_Error, "RX FIFO is not read completely: %R\n", errorCode);
            SDMMC_MASK(Channel)->RXFIFOHFIE = 0;
        }
    }
}

#undef  _________________________________________INTERRUPTS_________________________________________________________________________________

#endif
