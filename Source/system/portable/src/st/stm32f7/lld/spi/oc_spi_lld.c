/** ****************************************************************************************************************************************
 *
 * @file       oc_spi_lld.c
 *
 * @brief      The file with source for SPI LLD functions
 *
 * @file       oc_spi_lld.c
 *
 * @author     Patryk Kubiak - (Created on: 2017-07-20 - 00:42:25)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_spi_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <oc_clock_lld.h>
#include <oc_math.h>
#include <oc_debug.h>
#include <oc_gpio_lld.h>
#include <oc_gpio_mslld.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(Address)                      oC_LSF_IsRamAddress(Address)
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define IsChannelCorrect(Channel)           oC_Channel_IsCorrect(SPI,Channel)
#define IsChannelPoweredOn(Channel)         (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)

#define SPIx_CR1(Channel)                   oC_Channel_Register(Channel,SPIx_CR1)
#define SPIx_CR2(Channel)                   oC_Channel_Register(Channel,SPIx_CR2)
#define SPIx_SR(Channel)                    oC_Channel_Register(Channel,SPIx_SR)
#define SPIx_DR(Channel)                    oC_Channel_Register(Channel,SPIx_DR)

#define MIN_BAUD_RATE_PRESC                 2
#define MAX_BAUD_RATE_PRESC                 256

#define SOFTWARE_RING_COUNT                     255
#define RxRing(Channel)                         SoftwareRxRings[oC_Channel_ToIndex(SPI,Channel)]
#define IsSoftwareRxRingEmpty(Channel)          (RxRing(Channel).Counter == 0)
#define IsSoftwareRxRingFull(Channel)           (RxRing(Channel).Counter == SOFTWARE_RING_COUNT)
#define TxRing(Channel)                         SoftwareTxRings[oC_Channel_ToIndex(SPI,Channel)]
#define IsSoftwareTxRingEmpty(Channel)          (TxRing(Channel).Counter == 0)
#define IsSoftwareTxRingFull(Channel)           (TxRing(Channel).Counter == SOFTWARE_RING_COUNT)

#define NUMBER_OF_CHANNELS                  oC_ModuleChannel_NumberOfElements(SPI)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_SPI_LLD_Data_t       Buffer[SOFTWARE_RING_COUNT];
    uint16_t                PutIndex;
    uint16_t                GetIndex;
    uint16_t                Counter;
} SoftwareRing_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static uint8_t                  FindBaudRateControl         ( oC_Frequency_t SpiFrequency , oC_Frequency_t Tolerance , oC_BaudRate_t * outRealBaudRate );
static inline void              PutToSoftwareRing           ( SoftwareRing_t * Ring , oC_SPI_LLD_Data_t Data );
static inline oC_SPI_LLD_Data_t GetFromSoftwareRing         ( SoftwareRing_t * Ring );
static inline void              ClearSoftwareRing           ( SoftwareRing_t * Ring );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static SoftwareRing_t                SoftwareRxRings[NUMBER_OF_CHANNELS];
static SoftwareRing_t                SoftwareTxRings[NUMBER_OF_CHANNELS];
static oC_BaudRate_t                 ConfiguredBaudRates[NUMBER_OF_CHANNELS];
static oC_SPI_LLD_InterruptHandler_t Handler = NULL;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsChannelCorrect( oC_SPI_Channel_t Channel )
{
    return oC_Channel_IsCorrect(SPI,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsChannelIndexCorrect( oC_SPI_LLD_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(SPI);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SPI_LLD_ChannelIndex_t oC_SPI_LLD_ChannelToChannelIndex( oC_SPI_Channel_t Channel )
{
    return oC_Channel_ToIndex(SPI,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SPI_Channel_t oC_SPI_LLD_ChannelIndexToChannel( oC_SPI_LLD_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex(SPI,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SPI_Channel_t oC_SPI_LLD_GetChannelOfModulePin( oC_SPI_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_SPI_LLD))
    {
        oC_Module_TurnOn(oC_Module_SPI_LLD);
        errorCode = oC_ErrorCode_None;

        Handler = NULL;

        ClearSoftwareRing(SoftwareTxRings);
        ClearSoftwareRing(SoftwareRxRings);

        memset(ConfiguredBaudRates,0,sizeof(ConfiguredBaudRates));
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI_LLD))
    {
        oC_Module_TurnOff(oC_Module_SPI_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetInterruptHandler( oC_SPI_LLD_InterruptHandler_t InterruptHandler )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI_LLD))
    {
        if( ErrorCondition( IsRam(InterruptHandler) || IsRom(InterruptHandler) , oC_ErrorCode_WrongEventHandlerAddress ) )
        {
            Handler     = InterruptHandler;
            errorCode   = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_SetPower( oC_SPI_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                               , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( Power == oC_Power_On || Power == oC_Power_Off           , oC_ErrorCode_PowerStateNotCorrect) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,Power)       , oC_ErrorCode_CannotEnableChannel)  &&
            ErrorCondition( oC_Channel_EnableInterrupt(Channel,PeripheralInterrupt) , oC_ErrorCode_CannotEnableInterrupt)
            )
        {
            oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(SPI,Channel);

            oC_ASSERT(channelIndex < oC_ModuleChannel_NumberOfElements(SPI));


            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ReadPower( oC_SPI_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsRam(outPower)           , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outPower = oC_Machine_GetPowerStateForChannel(Channel);
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_Configure(
                oC_SPI_Channel_t            Channel ,
                oC_SPI_LLD_Mode_t           Mode ,
                oC_BaudRate_t               BaudRate ,
                oC_BaudRate_t               Tolerance,
                oC_SPI_LLD_ClockPhase_t     ClockPhase,
                oC_SPI_LLD_ClockPolarity_t  ClockPolarity ,
                oC_SPI_LLD_FrameFormat_t    FrameFormat ,
                oC_SPI_LLD_DataSize_t       DataSize
                )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();
    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                           , oC_ErrorCode_WrongChannel                 )
         && ErrorCondition( IsChannelPoweredOn(Channel)                         , oC_ErrorCode_ChannelNotPoweredOn          )
         && ErrorCondition( ClockPhase == oC_SPI_LLD_ClockPhase_FirstTransition
                         || ClockPhase == oC_SPI_LLD_ClockPhase_FirstTransition , oC_ErrorCode_ClockPhaseNotSupported       )
         && ErrorCondition( ClockPolarity == oC_SPI_LLD_ClockPolarity_HighActive
                         || ClockPolarity == oC_SPI_LLD_ClockPolarity_LowActive , oC_ErrorCode_ClockPolarityNotSupported    )
         && ErrorCondition( FrameFormat == oC_SPI_LLD_FrameFormat_LSB
                         || FrameFormat == oC_SPI_LLD_FrameFormat_MSB           , oC_ErrorCode_FrameFormatNotCorrect        )
         && ErrorCondition( DataSize >= 4 && DataSize <= 16                     , oC_ErrorCode_DataSizeNotSupported         )
         && ErrorCondition( Tolerance >= 0                                      , oC_ErrorCode_ToleranceNotCorrect          )
            )
        {
            oC_Frequency_t sysclk        = oC_CLOCK_LLD_GetClockFrequency();
            oC_Frequency_t pclkFrequency = sysclk; /* We assume, that the AHB prescaller is 1  */
            oC_Frequency_t tolerance     = oC_Frequency_FromBaudRate(Tolerance);
            oC_Frequency_t maxFrequency  = (pclkFrequency / MIN_BAUD_RATE_PRESC) + tolerance;
            oC_Frequency_t minFrequency  = (pclkFrequency / MAX_BAUD_RATE_PRESC) - tolerance;
            oC_Frequency_t spiFrequency  = oC_Frequency_FromBaudRate(BaudRate);
            uint16_t       prescaler     = (uint16_t)(pclkFrequency / spiFrequency);
            uint8_t        baudRateCtrl  = FindBaudRateControl(spiFrequency,tolerance,&ConfiguredBaudRates[ oC_Channel_ToIndex(SPI,Channel)]);


            if(
                ErrorCondition( spiFrequency > minFrequency && spiFrequency < maxFrequency              , oC_ErrorCode_BaudRateNotSupported     )
             && ErrorCondition( prescaler >= MIN_BAUD_RATE_PRESC && prescaler <= MAX_BAUD_RATE_PRESC    , oC_ErrorCode_BaudRateNotSupported     )
             && ErrorCondition( baudRateCtrl <= 0x7                                                     , oC_ErrorCode_FrequencyNotPossible     )
             && ErrorCondition( oC_Channel_EnableInterrupt(Channel,PeripheralInterrupt)                 , oC_ErrorCode_CannotEnableInterrupt    )
                )
            {
                oC_RegisterType_SPIx_CR1_t     cr1     = { .Value = 0 };
                oC_RegisterType_SPIx_CR2_t     cr2     = { .Value = 0 };

                cr1.MSTR     = Mode == oC_SPI_LLD_Mode_Master ? 1 : 0;
                cr1.BR       = baudRateCtrl;
                cr1.CPHA     = ClockPhase    == oC_SPI_LLD_ClockPhase_SecondTransition ? 1 : 0;
                cr1.CPOL     = ClockPolarity == oC_SPI_LLD_ClockPolarity_LowActive     ? 1 : 0;
                cr1.LSBFIRST = FrameFormat   == oC_SPI_LLD_FrameFormat_LSB             ? 1 : 0;

                cr2.DS       = DataSize - 1;
                cr2.RXNEIE   = 1;
                cr2.ERRIE    = 1;
                cr2.SSOE     = 1;
                cr2.TXEIE    = 1;

                if(Mode == oC_SPI_LLD_Mode_Master)
                {
                    cr1.SPE = 0;
                }
                else
                {
                    cr1.SPE = 1;
                }

                SPIx_CR2(Channel)->Value = cr2.Value;
                SPIx_CR1(Channel)->Value = cr1.Value;

                errorCode = oC_ErrorCode_None;
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_RestoreDefaultStateOnChannel( oC_SPI_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        if( ErrorCondition( IsChannelCorrect(Channel), oC_ErrorCode_WrongChannel ) )
        {
            oC_MCS_EnterCriticalSection();
            ClearSoftwareRing( &RxRing(Channel) );
            ClearSoftwareRing( &TxRing(Channel) );

            SPIx_CR1(Channel)->SPE = 0;

            if( ErrorCondition( oC_Machine_SetPowerStateForChannel( Channel , oC_Power_Off ), oC_ErrorCode_CannotDisableChannel ) )
            {
                errorCode = oC_ErrorCode_None;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ConfigurePin( oC_SPI_Channel_t Channel , oC_Pin_t Pin , oC_SPI_PinFunction_t PinFunction )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)    , oC_ErrorCode_WrongChannel    )
         && ErrorCondition( oC_GPIO_LLD_IsPinDefined(Pin), oC_ErrorCode_PinNotDefined   )
            )
        {
            bool unused = false;

            errorCode = oC_ErrorCode_NoneOfModulePinAssigned;

            oC_ModulePin_ForeachDefined(modulePin)
            {
                if(
                    modulePin->Pin == Pin
                 && modulePin->PinFunction == PinFunction
                 && modulePin->Channel == (oC_Channel_t)Channel
                    )
                {
                    oC_MCS_EnterCriticalSection();

                    if(
                        ErrorCode     ( oC_GPIO_LLD_ArePinsUnused(Pin,&unused)                                                  )
                     && ErrorCondition(        unused , oC_ErrorCode_PinIsUsed                                                  )
                     && ErrorCode     ( oC_GPIO_LLD_SetPinsUsed(Pin)                                                            )
                     && ErrorCode     ( oC_GPIO_MSLLD_ConnectModulePin(modulePin->ModulePinIndex)                               )
                     && ErrorCode     ( oC_GPIO_LLD_SetSpeed(Pin, oC_GPIO_LLD_Speed_Maximum)                                    )
                     && ErrorCode     ( oC_GPIO_LLD_SetOutputCircuit(Pin, oC_GPIO_LLD_OutputCircuit_PushPull)                   )
                     && ErrorCode     ( oC_GPIO_LLD_SetPull(Pin, oC_GPIO_LLD_Pull_Down)                                         )
                        )
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                    else
                    {
                        kdebuglog( oC_LogType_Error, "SPI-LLD: Cannot configure module pin: %R", errorCode );
                        oC_GPIO_LLD_SetPinsUnused(Pin);
                    }

                    oC_MCS_ExitCriticalSection();
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_UnconfigurePin( oC_Pin_t Pin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD) )
    {
        if( ErrorCode( oC_GPIO_MSLLD_DisconnectPin(Pin,0) ) )
        {
            errorCode = oC_GPIO_LLD_SetPinsUnused(Pin);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsPinAvailble( oC_SPI_Channel_t Channel , oC_Pin_t Pin , oC_SPI_PinFunction_t PinFunction )
{
    bool pinAvailable = false;
    bool pinIsUsed    = false;

    if(oC_Module_IsTurnedOn(oC_Module_SPI_LLD))
    {
        if(
            oC_SaveIfFalse( "Channel"       , IsChannelCorrect(Channel)     , oC_ErrorCode_WrongChannel      )
         && oC_SaveIfFalse( "Pin"           , oC_GPIO_LLD_IsPinDefined(Pin) , oC_ErrorCode_PinNotDefined     )
         && oC_SaveIfErrorOccur( "Pin"      , oC_GPIO_LLD_CheckIsPinUsed(Pin,&pinIsUsed)                     )
         && oC_SaveIfFalse( "Pin"           , pinIsUsed == false            , oC_ErrorCode_PinIsUsed         )
            )
        {
            oC_ModulePin_ForeachDefined(modulePin)
            {
                if(
                    modulePin->Pin == Pin
                 && modulePin->PinFunction == PinFunction
                 && modulePin->Channel == (oC_Channel_t)Channel
                    )
                {
                    pinAvailable = true;
                    break;
                }
            }
        }
    }

    return pinAvailable;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsRxEmpty( oC_SPI_Channel_t Channel )
{
    bool empty = true;

    if( oC_Module_IsTurnedOn(oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();
        empty = IsSoftwareRxRingEmpty(Channel);
        oC_MCS_ExitCriticalSection();
    }

    return empty;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsTxFull ( oC_SPI_Channel_t Channel )
{
    bool full = true;

    if( oC_Module_IsTurnedOn(oC_Module_SPI_LLD) )
    {
        oC_MCS_EnterCriticalSection();
        full = IsSoftwareTxRingFull(Channel);
        oC_MCS_ExitCriticalSection();
    }

    return full;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SPI_LLD_Data_t oC_SPI_LLD_GetReceivedData( oC_SPI_Channel_t Channel )
{
    oC_SPI_LLD_Data_t data = 0;

    if( oC_Module_IsTurnedOn(oC_Module_SPI_LLD) )
    {
        data = GetFromSoftwareRing( &RxRing(Channel) );
    }

    return data;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_SPI_LLD_PutDataToTransmit( oC_SPI_Channel_t Channel , oC_SPI_LLD_Data_t Data )
{
    oC_MCS_EnterCriticalSection();

    if( oC_Module_IsTurnedOn( oC_Module_SPI_LLD ) )
    {
        PutToSoftwareRing( &TxRing(Channel), Data );

        SPIx_CR2(Channel)->TXEIE    = 1;
        SPIx_CR1(Channel)->SPE      = 1;
    }

    oC_MCS_ExitCriticalSection();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SPI_LLD_IsBusy( oC_SPI_Channel_t Channel )
{
    bool busy = false;

    if( oC_Module_IsTurnedOn( oC_Module_SPI_LLD ) )
    {
        busy = SPIx_SR(Channel)->BSY == 1;
    }

    return busy;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_BaudRate_t oC_SPI_LLD_GetConfiguredBaudRate( oC_SPI_Channel_t Channel )
{
    oC_BaudRate_t baudRate = 0;

    if( oC_Module_IsTurnedOn(oC_Module_SPI_LLD) && oC_Channel_IsCorrect(SPI,Channel) )
    {
        baudRate = ConfiguredBaudRates[oC_Channel_ToIndex(SPI,Channel)];
    }

    return baudRate;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LLD_ClearRxFifo( oC_SPI_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel         )
         && ErrorCondition( IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn  )
            )
        {
            oC_MCS_EnterCriticalSection();

            ClearSoftwareRing(&RxRing(Channel));
            errorCode = oC_ErrorCode_None;

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief searches for prescaler
 */
//==========================================================================================================================================
static uint8_t FindBaudRateControl( oC_Frequency_t SpiFrequency , oC_Frequency_t Tolerance , oC_BaudRate_t * outRealBaudRate )
{
    uint8_t         baudRateControl = 0xFF;
    oC_Frequency_t  sysclk          = oC_CLOCK_LLD_GetClockFrequency();
    oC_Frequency_t  pclkFrequency   = sysclk;
    static const uint16_t prescalers[] = {
                    2 ,
                    4 ,
                    8 ,
                    16 ,
                    32 ,
                    64 ,
                    128 ,
                    256
    };

    for( int i = 0; i < oC_ARRAY_SIZE(prescalers); i++ )
    {
        oC_Frequency_t realFrequency = pclkFrequency / ((oC_Frequency_t)(prescalers[i]));
        if( oC_ABS(realFrequency,SpiFrequency) <= Tolerance )
        {
            *outRealBaudRate = oC_Frequency_ToBaudRate(realFrequency);
            baudRateControl  = i;
            break;
        }
    }

    return baudRateControl;
}


//==========================================================================================================================================
/**
 * @brief puts data to software ring
 */
//==========================================================================================================================================
static inline void PutToSoftwareRing( SoftwareRing_t * Ring , oC_SPI_LLD_Data_t Data )
{
    if(Ring->Counter >= SOFTWARE_RING_COUNT)
    {
        /* If ring is full, take the oldest data */
        oC_SPI_LLD_Data_t droppedData = GetFromSoftwareRing(Ring);

        kdebuglog( oC_LogType_Warning, "SPI-LLD: software ring is full - dropping oldest value (0x%X)", droppedData );
    }

    Ring->Buffer[Ring->PutIndex++] = Data;
    Ring->Counter++;

    if(Ring->PutIndex >= SOFTWARE_RING_COUNT)
    {
        Ring->PutIndex = 0;
    }
}

//==========================================================================================================================================
/**
 * @brief gets data from software ring
 */
//==========================================================================================================================================
static inline oC_SPI_LLD_Data_t GetFromSoftwareRing( SoftwareRing_t * Ring )
{
    oC_SPI_LLD_Data_t data = 0x00;

    if(Ring->Counter > 0)
    {
        Ring->Counter--;
        data = Ring->Buffer[Ring->GetIndex++];
        if(Ring->GetIndex >= SOFTWARE_RING_COUNT)
        {
            Ring->GetIndex = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief clears data in software ring
 */
//==========================================================================================================================================
static inline void ClearSoftwareRing( SoftwareRing_t * Ring )
{
    memset(Ring,0,sizeof(SoftwareRing_t));
}

//==========================================================================================================================================
/**
 * @brief calls an interrupt handler
 */
//==========================================================================================================================================
static inline void CallInterruptHandler( oC_SPI_Channel_t Channel , oC_SPI_LLD_InterruptSource_t Source )
{
    if( Handler != NULL )
    {
        if( IsRam(Handler) || IsRom(Handler) )
        {
            Handler(Channel,Source);
        }
        else
        {
            kdebuglog( oC_LogType_Error , "SPI LLD: Wrong interrupt handler (%p)", Handler );
        }
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

//==========================================================================================================================================
/**
 *              INTERRUPT HANDLER FOR SPI Peripheral Interrupt
 * */
//==========================================================================================================================================
#define MODULE_NAME              SPI
#define INTERRUPT_TYPE_NAME      PeripheralInterrupt

oC_Channel_InterruptHandler(Channel)
{
    oC_SPI_LLD_InterruptSource_t source     = oC_SPI_LLD_InterruptSource_None;
    oC_RegisterType_SPIx_SR_t  * sr         = SPIx_SR(Channel);
    oC_SPI_LLD_DataSize_t        dataSize   = SPIx_CR2(Channel)->DS + 1;

    if(dataSize > 8)
    {
        while( sr->RXNE == 1 )
        {
            uint32_t data = (oC_SPI_LLD_Data_t)SPIx_DR(Channel)->Value;
            PutToSoftwareRing( &RxRing(Channel) , data );
        }
    }
    else
    {
        while( sr->RXNE == 1 )
        {
            uint32_t data = (oC_SPI_LLD_Data_t)SPIx_DR(Channel)->Value;
            PutToSoftwareRing( &RxRing(Channel) , ( data & 0x00FF ) >> 0 );
            PutToSoftwareRing( &RxRing(Channel) , ( data & 0xFF00 ) >> 8 );
        }
    }

    if(IsSoftwareTxRingEmpty(Channel))
    {
        SPIx_CR2(Channel)->TXEIE = 0;
    }
    else
    {
        if(dataSize > 8)
        {
            while( SPIx_SR(Channel)->TXE == 1 && IsSoftwareTxRingEmpty(Channel) == false)
            {
                SPIx_DR(Channel)->Value = (uint32_t)GetFromSoftwareRing(&TxRing(Channel));
            }
        }
        else
        {
            while( SPIx_SR(Channel)->TXE == 1 && IsSoftwareTxRingEmpty(Channel) == false)
            {
                uint16_t value = GetFromSoftwareRing(&TxRing(Channel)) | (GetFromSoftwareRing(&TxRing(Channel)) << 8);

                SPIx_DR(Channel)->Value = (uint32_t)value;
            }
        }
    }

    if( IsSoftwareRxRingEmpty(Channel) == false )
    {
        source |= oC_SPI_LLD_InterruptSource_RxFifoNotEmpty;
    }

    if( IsSoftwareTxRingFull(Channel) == false )
    {
        source |= oC_SPI_LLD_InterruptSource_TxFifoNotFull;
    }

    CallInterruptHandler(Channel,source);
}

#undef MODULE_NAME
#undef INTERRUPT_TYPE_NAME

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________


