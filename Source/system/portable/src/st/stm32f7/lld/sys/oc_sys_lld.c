/** ****************************************************************************************************************************************
 *
 * @file       oc_sys_lld.c
 *
 * @brief      The file with source of interface functions for the SYS-LLD module on stm32f7120 machine family
 *
 * @author     Patryk Kubiak - (Created on: 30 06 2015 11:45:00)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_sys_lld.h>
#include <oc_mem_lld.h>
#include <oc_clock_lld.h>
#include <oc_compiler.h>
#include <oc_mcs.h>
#include <oc_lsf.h>
#include <oc_machine.h>
#include <oc_interrupts.h>
#include <oc_module.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________



#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

#define PWR_CR1                     oC_Register(PWR,PWR_CR1)
#define IsRam(Address)              (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)              oC_LSF_IsRomAddress(Address)

#undef  _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_SYS_LLD_EventInterrupt_t EventInterruptHandler = NULL;
static oC_SYS_LLD_EventFlags_t     EnabledEventsFlags    = 0;
static oC_Frequency_t              SystemTimerFrequency  = 0;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void EnableEventsInterrupts  ( oC_SYS_LLD_EventFlags_t EventFlags );
static void CallEventHandler        ( oC_SYS_LLD_EventFlags_t EventFlags , void * MemoryAddress , const char * Details );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCITONS_SECTION________________________________________________________________
//! @addtogroup SYS-LLD
//! @{


//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_SYS_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(oC_MCS_InitializeModule())
        {
            oC_Module_TurnOn(oC_Module_SYS_LLD);

            /* The setting oC_UnexpectedInterruptHandler is very important - without it, the oc_interrupt.c file is not used, and then
             * the interrupt vector is not included to the output file. DO NOT REMOVE IT!! */
            EventInterruptHandler           = NULL;
            EnabledEventsFlags              = 0;
            SystemTimerFrequency            = 0;
            oC_UnexpectedInterruptHandler   = NULL;
            errorCode                       = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotInitializeModule;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SYS_LLD))
    {
        oC_MCS_EnterCriticalSection();

        EventInterruptHandler  = NULL;
        EnabledEventsFlags     = 0;
        SystemTimerFrequency   = 0;
        oC_Module_TurnOff(oC_Module_SYS_LLD);

        oC_MCS_ExitCriticalSection();
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_SetEventInterruptHandler(oC_SYS_LLD_EventInterrupt_t EventHandler , oC_SYS_LLD_EventFlags_t EventsFlags)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SYS_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , IsRam(EventHandler) || IsRom(EventHandler) , oC_ErrorCode_WrongEventHandlerAddress) &&
            oC_AssignErrorCodeIfFalse(&errorCode , EventInterruptHandler == NULL              , oC_ErrorCode_InterruptHandlerAlreadySet)
            )
        {
            EventInterruptHandler   = EventHandler;
            EnabledEventsFlags      = EventsFlags;

            EnableEventsInterrupts(EventsFlags);

            errorCode               = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SYS_LLD_IsMachineSupportMultithreadMode( void )
{
    return true;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_SYS_LLD_TurnOnInterrupts(void)
{
    oC_MCS_EnableInterrupts();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_SYS_LLD_TurnOffInterrupts(void)
{
    oC_MCS_DisableInterrupts();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_SYS_LLD_EnterCriticalSection(void)
{
    oC_MCS_EnterCriticalSection();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_SYS_LLD_ExitCriticalSection(void)
{
    oC_MCS_ExitCriticalSection();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SYS_LLD_AreInterruptsEnabled(void)
{
    return oC_MCS_AreInterruptsEnabled();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_ConfigureSystemTimer( oC_Frequency_t Frequency , oC_SYS_LLD_SysTickIncrementHandler_t Interrupt )
{
    oC_ErrorCode_t errorCode      = oC_ErrorCode_ImplementError;
    oC_Frequency_t clockFrequency = oC_CLOCK_LLD_GetClockFrequency();

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_SYS_LLD) &&
        oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsRom(Interrupt) || IsRam(Interrupt)             , oC_ErrorCode_WrongEventHandlerAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Frequency > 0 && (Frequency <= clockFrequency)   , oC_ErrorCode_WrongFrequency)
        )
    {
        uint32_t prescaler = clockFrequency / Frequency;

        oC_MCS_EnterCriticalSection();

        if(oC_MCS_ConfigureSystemTimer(prescaler,Interrupt))
        {
            SystemTimerFrequency    = clockFrequency / prescaler;
            errorCode               = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_FrequencyNotPossible;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_ReadSystemTimerFrequency( oC_Frequency_t * outFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SYS_LLD))
    {
        if(oC_AssignErrorCodeIfFalse(&errorCode , SystemTimerFrequency != 0 , oC_ErrorCode_SystemClockNotConfigured))
        {
            *outFrequency   = SystemTimerFrequency;
            errorCode       = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_SetNextContext( oC_SYS_LLD_Context_t * Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SYS_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if(oC_MCS_SetNextStack(Context))
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ContextNotCorrect;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Int_t oC_SYS_LLD_GetContextStackSize(oC_SYS_LLD_Context_t * Context)
{
    oC_Int_t stackSize = 0;

    if(oC_Module_IsTurnedOn(oC_Module_SYS_LLD))
    {
        stackSize = oC_MCS_GetStackSize(Context);
    }

    return stackSize;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Int_t oC_SYS_LLD_GetContextFreeStackSize(oC_SYS_LLD_Context_t * Context)
{
    oC_Int_t freeStackSize = 0;

    if(oC_Module_IsTurnedOn(oC_Module_SYS_LLD))
    {
        freeStackSize = oC_MCS_GetFreeStackSize(Context);
    }

    return freeStackSize;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_ReturnToSystemContext( void )
{
    return oC_MCS_ReturnToSystemStack();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SYS_LLD_Context_t * oC_SYS_LLD_GetCurrentContext(void)
{
    return oC_MCS_GetCurrentStack();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_SYS_LLD_Context_t * oC_SYS_LLD_GetSystemContext(void)
{
    return oC_MCS_GetSystemStack();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_InitializeContext( oC_SYS_LLD_Context_t ** Context , oC_Int_t StackSize , oC_SYS_LLD_ContextHandler_t ContextHandler , void * Parameters , oC_SYS_LLD_ContextExitHandler_t ExitHandler )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SYS_LLD))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , IsRam(Context)                               , oC_ErrorCode_OutputAddressNotInRAM     ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsRam(Context)     || IsRom(ContextHandler)  , oC_ErrorCode_WrongEventHandlerAddress  ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsRam(ExitHandler) || IsRom(ExitHandler)     , oC_ErrorCode_WrongEventHandlerAddress  ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , StackSize > 0                                , oC_ErrorCode_WrongStackSize)
            )
        {
            if(oC_MCS_InitializeStack((oC_Stack_t*)Context,*Context,StackSize,ContextHandler,Parameters,ExitHandler))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_CannotInitializeStack;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Int_t oC_SYS_LLD_GetMinimumContextSize( oC_Int_t StackSize )
{
    return oC_MCS_GetMinimumStackBufferSize(StackSize);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
const char * oC_SYS_LLD_GetMachineName( void )
{
    return oC_TO_STRING(oC_MACHINE);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
const char * oC_SYS_LLD_GetMachineFamilyName( void )
{
    return oC_TO_STRING(oC_MACHINE_FAMILY);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
const char * oC_SYS_LLD_GetMachineProducentName( void )
{
    return "ST";
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_SYS_LLD_Reset( void )
{
    oC_MCS_Reboot();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_SYS_LLD_IsStackPushDecrementPointer( void )
{
    return oC_MCS_IsStackPushDecrementPointer();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SYS_LLD_ReadPowerState( float * outVcc )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , oC_LSF_IsRamAddress(outVcc) , oC_ErrorCode_OutputAddressNotInRAM))
    {
        *outVcc   = 3.3;
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void * oC_SYS_LLD_GetLastProcessStackPointer( void )
{
    return oC_MCS_GetCurrentProcessStackPointer();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_SYS_LLD_PrintToDebugger( const char * Message )
{
    if( IsRam(Message) || IsRom(Message) )
    {
        oC_MCS_PrintToDebugger(Message);
    }
}

#undef  _________________________________________INTERFACE_FUNCITONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief enables interrupts from events list
 */
//==========================================================================================================================================
static void EnableEventsInterrupts( oC_SYS_LLD_EventFlags_t EventFlags )
{
    if(EventFlags & oC_SYS_LLD_EventFlags_HardFault)
    {
        oC_MCS_EnableAllPossibleEvents();
    }
}

//==========================================================================================================================================
/**
 * @brief calls event handler according to enabled flags
 */
//==========================================================================================================================================
static void CallEventHandler( oC_SYS_LLD_EventFlags_t EventFlags , void * MemoryAddress , const char * Details )
{
    if(IsRom(EventInterruptHandler) || IsRam(EventInterruptHandler))
    {
        if(EnabledEventsFlags & EventFlags)
        {
            EventInterruptHandler(EventFlags,oC_MCS_GetCurrentStack(), MemoryAddress, Details);
        }
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interrupts handlers
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief hard fault interrupt handler
 */
//==========================================================================================================================================
oC_InterruptHandler(System,HardFault)
{
    CallEventHandler( oC_SYS_LLD_EventFlags_HardFault , oC_MCS_GetHardFaultReason(), oC_MCS_GetHardFaultReasonDescription());

    /* Interrupt handler is not set, but an hard fault occurs */
    oC_ASSERT( EventInterruptHandler != NULL );

    oC_MCS_ClearHardFaultStatus();
}

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________
