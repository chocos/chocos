/** ****************************************************************************************************************************************
 *
 * @file       oc_clock_lld.c
 *
 * @brief      The file with interface functions for the CLOCK-LLD module.
 *
 * @author     Patryk Kubiak - (Created on: 25 06 2015 17:41:45)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_clock_lld.h>
#include <oc_mem_lld.h>
#include <oc_math.h>
#include <oc_module.h>
#include <oc_mcs.h>
#include <oc_lsf.h>
#include <oc_sys_lld.h>
#include <oc_interrupts.h>

/** ========================================================================================================================================
 *
 *              The section with local definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define RCC_APB1ENR                 oC_Register(RCC,RCC_APB1ENR)
#define RCC_CR                      oC_Register(RCC,RCC_CR)
#define RCC_CFGR                    oC_Register(RCC,RCC_CFGR)
#define RCC_PLLCFGR                 oC_Register(RCC,RCC_PLLCFGR)
#define RCC_CSR                     oC_Register(RCC,RCC_CSR)
#define PWR_CR1                     oC_Register(PWR,PWR_CR1)
#define PWR_CSR1                    oC_Register(PWR,PWR_CSR1)
#define FLASH_ACR                   oC_Register(FLASH,FLASH_ACR)
#define MAX_FREQUENCY               oC_MACHINE_MAXIMUM_FREQUENCY
#define HIBERNATION_FREQUENCY       oC_MACHINE_HIBERNATION_OSCILLATOR_FREQUENCY

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(Address)              (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)              oC_LSF_IsRomAddress(Address)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    MainClockSource_HSI = 0,
    MainClockSource_HSE = 1 ,
    MainClockSource_PLL = 2
} MainClockSource_t;

typedef enum
{
    PllSource_HSI = 0 ,
    PllSource_HSE = 1
} PllSource_t;

typedef struct
{
    bool                UsePll;
    uint8_t             PllM:6;
    uint16_t            PllN:9;
    uint8_t             PllP:2;
    uint8_t             Hpre:4;
    oC_Frequency_t      ResultFrequency;
    oC_Frequency_t      ResultDifference;
    oC_Frequency_t      TargetFrequency;
    oC_Frequency_t      PermissibleDifference;
    oC_Frequency_t      OscillatorFrequency;
} ClockConfiguration_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void             SetPowerForInternalOscillator       ( oC_Power_t Power );
static void             SetPowerForExternalOscillator       ( oC_Power_t Power );
static void             SetPowerForHibernationOscillator    ( oC_Power_t Power );
static void             SetPowerForPll                      ( oC_Power_t Power );
static void             SetPowerForOverDrive                ( oC_Power_t Power );
static void             SetFlashLatency                     ( uint8_t WaitStates );
static oC_ErrorCode_t   FindFlashLatency                    ( oC_Frequency_t ClockFrequency , uint8_t * outWaitStates );
static void             SetMainClockSource                  ( MainClockSource_t MainClockSource );
static void             SetPllSource                        ( PllSource_t PllSource );
static bool             CountClockConfiguration             ( ClockConfiguration_t * ClockConfiguration );
static bool             FindHpre                            ( oC_Frequency_t EntryFrequency , ClockConfiguration_t * ClockConfiguration );
static void             ConfigurePll                        ( ClockConfiguration_t * ClockConfiguration );
static void             SetHpre                             ( uint8_t Hpre );
static void             ClockConfigured                     ( oC_Frequency_t RealFrequency , oC_Frequency_t OscillatorFrequency , oC_CLOCK_LLD_ClockSource_t ClockSource );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_Frequency_t               CurrentFrequency            = oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY;
static oC_Frequency_t               CurrentOscillatorFrequency  = oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY;
static oC_CLOCK_LLD_ClockSource_t   CurrentClockSource          = oC_CLOCK_LLD_ClockSource_Internal;
static oC_CLOCK_LLD_Interrupt_t     ClockConfiguredHandler      = NULL;
static bool                         UseEthernet                 = false;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup CLOCK-LLD
//! @{


//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        /* ************************* NOTE ******************************
         * ClockSource and ClockFrequency is specially not initialized *
         * to not provide wrong informations about state of clock      *
         * configuration                                               *
         * *************************************************************/

        ClockConfiguredHandler = NULL;
        UseEthernet            = false;

        /* Enable power interface clock */
        RCC_APB1ENR->PWR_EN = 1;
        oC_MCS_Delay(10);

        /* This MUST be at the end of module initialization */
        oC_Module_TurnOn(oC_Module_CLOCK_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        /* This should be at the start of module release */
        oC_Module_TurnOff(oC_Module_CLOCK_LLD);

        /* Disable power interface clock */
        RCC_APB1ENR->PWR_EN = 0;
        oC_MCS_Delay(10);

        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_CLOCK_LLD_ClockSource_t oC_CLOCK_LLD_GetClockSource( void )
{
    return CurrentClockSource;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Frequency_t oC_CLOCK_LLD_GetClockFrequency( void )
{
    return CurrentFrequency;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Frequency_t oC_CLOCK_LLD_GetOscillatorFrequency( void )
{
    return CurrentOscillatorFrequency;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Frequency_t oC_CLOCK_LLD_GetMaximumClockFrequency( void )
{
    return MAX_FREQUENCY;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_SetClockConfiguredInterrupt( oC_CLOCK_LLD_Interrupt_t Interrupt )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , ClockConfiguredHandler == NULL       , oC_ErrorCode_InterruptHandlerAlreadySet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsRom(Interrupt) || IsRam(Interrupt) , oC_ErrorCode_WrongEventHandlerAddress )
            )
        {
            oC_MCS_EnterCriticalSection();
            ClockConfiguredHandler  = Interrupt;
            errorCode               = oC_ErrorCode_None;
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_CLOCK_LLD_DelayForMicroseconds( oC_UInt_t Microseconds )
{
    oC_UInt_t numberOfCyclesPerMicrosecond = CurrentFrequency/1000000UL;

    oC_MCS_Delay(numberOfCyclesPerMicrosecond*Microseconds);

    return true;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_ConfigureInternalClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        uint8_t                 latencyWaitStates   = 0;
        bool                    increasingFrequency = TargetFrequency >= CurrentFrequency;
        ClockConfiguration_t    clockConfig         = {
                        .ResultDifference           = PermissibleDifference + 1 ,
                        .TargetFrequency            = TargetFrequency ,
                        .PermissibleDifference      = PermissibleDifference ,
                        .OscillatorFrequency        = oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY
        };

        if(
            oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency > 0                   , oC_ErrorCode_WrongFrequency      ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency <= MAX_FREQUENCY      , oC_ErrorCode_FrequencyNotPossible) &&
            oC_AssignErrorCodeIfFalse(&errorCode , CountClockConfiguration(&clockConfig) , oC_ErrorCode_FrequencyNotPossible) &&
            oC_AssignErrorCode(       &errorCode , FindFlashLatency(clockConfig.ResultFrequency , &latencyWaitStates))
            )
        {
            oC_MCS_EnterCriticalSection();

            SetPowerForInternalOscillator(oC_Power_On);
            SetPowerForPll(oC_Power_Off);

            if(clockConfig.UsePll)
            {
                SetPllSource(PllSource_HSI);
                ConfigurePll(&clockConfig);
                SetPowerForPll(oC_Power_On);

                SetPowerForOverDrive(oC_Power_On);

                /* ****************** NOTE ******************
                 * In case of increasing system clock       *
                 * frequency, it is recommended to set flash*
                 * latency before selecting clock source    *
                 * ******************************************/
                if(increasingFrequency)
                {
                    SetFlashLatency(latencyWaitStates);
                }

                SetHpre(clockConfig.Hpre);

                SetMainClockSource(MainClockSource_PLL);
            }
            else
            {
                /* ****************** NOTE ******************
                 * In case of increasing system clock       *
                 * frequency, it is recommended to set flash*
                 * latency before selecting clock source    *
                 * ******************************************/
                if(increasingFrequency)
                {
                    SetFlashLatency(latencyWaitStates);
                }

                SetHpre(clockConfig.Hpre);

                SetMainClockSource(MainClockSource_HSI);
            }

            /* ****************** NOTE ******************
             * In case of increasing system clock       *
             * frequency, it is recommended to set flash*
             * latency after selecting clock source     *
             * ******************************************/
            if(!increasingFrequency)
            {
                SetFlashLatency(latencyWaitStates);
            }

            ClockConfigured(clockConfig.ResultFrequency , clockConfig.OscillatorFrequency , oC_CLOCK_LLD_ClockSource_Internal);

            errorCode = oC_ErrorCode_None;

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_ConfigureExternalClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference , oC_Frequency_t OscillatorFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        uint8_t                 latencyWaitStates   = 0;
        bool                    increasingFrequency = TargetFrequency >= CurrentFrequency;
        ClockConfiguration_t    clockConfig         = {
                        .ResultDifference       = PermissibleDifference + 1 ,
                        .TargetFrequency        = TargetFrequency,
                        .PermissibleDifference  = PermissibleDifference,
                        .OscillatorFrequency    = OscillatorFrequency
        };

        if(
            oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency > 0                          , oC_ErrorCode_WrongFrequency      ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , OscillatorFrequency > 0                      , oC_ErrorCode_WrongFrequency      ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency <= MAX_FREQUENCY             , oC_ErrorCode_FrequencyNotPossible) &&
            oC_AssignErrorCodeIfFalse(&errorCode , CountClockConfiguration(&clockConfig)        , oC_ErrorCode_FrequencyNotPossible) &&
            oC_AssignErrorCode(       &errorCode , FindFlashLatency(clockConfig.ResultFrequency , &latencyWaitStates))
            )
        {
            oC_MCS_EnterCriticalSection();

            SetPowerForExternalOscillator(oC_Power_On);
            SetPowerForPll(oC_Power_Off);

            if(clockConfig.UsePll)
            {
                SetPllSource(PllSource_HSE);
                ConfigurePll(&clockConfig);

                /* ****************** NOTE ******************
                 * Turning on power for external oscillator *
                 * must be done after configuring PLL       *
                 * according to the documentation so it     *
                 * cannot be done before this condition     *
                 * block
                 * ******************************************/
                SetPowerForExternalOscillator(oC_Power_On);
                SetPowerForPll(oC_Power_On);

                SetPowerForOverDrive(oC_Power_On);

                /* ****************** NOTE ******************
                 * In case of increasing system clock       *
                 * frequency, it is recommended to set flash*
                 * latency before selecting clock source    *
                 * ******************************************/
                if(increasingFrequency)
                {
                    SetFlashLatency(latencyWaitStates);
                }

                SetHpre(clockConfig.Hpre);

                SetMainClockSource(MainClockSource_PLL);
            }
            else
            {
                SetPowerForExternalOscillator(oC_Power_On);

                /* ****************** NOTE ******************
                 * In case of increasing system clock       *
                 * frequency, it is recommended to set flash*
                 * latency before selecting clock source    *
                 * ******************************************/
                if(increasingFrequency)
                {
                    SetFlashLatency(latencyWaitStates);
                }

                SetHpre(clockConfig.Hpre);

                SetMainClockSource(MainClockSource_HSE);
            }

            /* ****************** NOTE ******************
             * In case of increasing system clock       *
             * frequency, it is recommended to set flash*
             * latency after selecting clock source     *
             * ******************************************/
            if(!increasingFrequency)
            {
                SetFlashLatency(latencyWaitStates);
            }

            ClockConfigured(clockConfig.ResultFrequency , clockConfig.OscillatorFrequency , oC_CLOCK_LLD_ClockSource_External);

            oC_MCS_ExitCriticalSection();

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: not implemented - it's only turn on power for hibernation clock
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CLOCK_LLD_ConfigureHibernationClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference , oC_Frequency_t OscillatorFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_CLOCK_LLD))
    {
        if(TargetFrequency == 0)
        {
            TargetFrequency = HIBERNATION_FREQUENCY;
        }

        if(oC_AssignErrorCodeIfFalse(&errorCode , TargetFrequency != HIBERNATION_FREQUENCY , oC_ErrorCode_WrongFrequency ))
        {
            SetPowerForHibernationOscillator(oC_Power_On);
            errorCode = oC_ErrorCode_NotImplemented;
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * Turns on/off power for internal oscillator
 */
//==========================================================================================================================================
static void SetPowerForInternalOscillator( oC_Power_t Power )
{
    if(Power == oC_Power_On)
    {
        RCC_CR->HSION = 1;

        while(!RCC_CR->HSIRDY);
    }
    else
    {
        RCC_CR->HSION = 0;

        while(RCC_CR->HSIRDY);
    }
}

//==========================================================================================================================================
/**
 * Turns on/off power for external oscillator
 */
//==========================================================================================================================================
static void SetPowerForExternalOscillator( oC_Power_t Power )
{
    if(Power == oC_Power_On)
    {
        RCC_CR->HSEON = 0;

        while(RCC_CR->HSERDY);

        RCC_CR->HSEBYP= 0;
        RCC_CR->HSEON = 1;

        while(!RCC_CR->HSERDY);
    }
    else
    {
        RCC_CR->HSEON = 0;

        while(RCC_CR->HSERDY);
    }
}

//==========================================================================================================================================
/**
 * Turns on/off power for hibernation oscillator
 */
//==========================================================================================================================================
static void SetPowerForHibernationOscillator( oC_Power_t Power )
{
    if(Power == oC_Power_On)
    {
        RCC_CSR->LSION = 1;

        while(!RCC_CSR->LSIRDY);
    }
    else
    {
        RCC_CSR->LSION = 0;

        while(RCC_CSR->LSIRDY);
    }
}

//==========================================================================================================================================
/**
 * Turns on power for PLL
 */
//==========================================================================================================================================
static void SetPowerForPll( oC_Power_t Power )
{
    if(Power == oC_Power_On)
    {
        RCC_CR->PLLON = 1;

        while(!RCC_CR->PLLRDY);
    }
    else
    {
        RCC_CR->PLLON = 0;

        while(RCC_CR->PLLRDY);
    }
}

//==========================================================================================================================================
/**
 * Turns on power for OverDrive
 */
//==========================================================================================================================================
static void SetPowerForOverDrive( oC_Power_t Power )
{
    if(Power == oC_Power_On)
    {
        PWR_CR1->ODEN = 1;

        while(!PWR_CSR1->ODRDY);
    }
    else
    {
        PWR_CR1->ODEN = 0;

        while(PWR_CSR1->ODRDY);
    }
}

//==========================================================================================================================================
/**
 * Sets flash latency
 */
//==========================================================================================================================================
static void SetFlashLatency( uint8_t WaitStates )
{
    FLASH_ACR->LATENCY = WaitStates;

    /* FLASH Latency cannot be programmed */
    oC_ASSERT(FLASH_ACR->LATENCY == WaitStates);
}

//==========================================================================================================================================
/**
 * Searching for flash latency according to new clock frequency and current voltage state (VDD)
 */
//==========================================================================================================================================
static oC_ErrorCode_t FindFlashLatency( oC_Frequency_t ClockFrequency , uint8_t * outWaitStates )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    float          vdd       = 0;

    if(oC_AssignErrorCode(&errorCode , oC_SYS_LLD_ReadPowerState(&vdd)))
    {
        typedef struct
        {
            uint8_t         WaitStates;
            float           VddMin;
            float           VddMax;
            oC_Frequency_t  SystemClockMin;
            oC_Frequency_t  SystemClockMax;
        } FlashLatencyCapabilities_t;

        /* ****************************************************
         *  The array with definitions of wait states for     *
         *  different conditions - the VDD voltage and        *
         *  clock frequency is important here. For more       *
         *  informations look at the documentation at 76 p.   *
         * ****************************************************/
        static const FlashLatencyCapabilities_t latencyArray[] = {
                        /* 2.7 - 3.6 V */
                        { .WaitStates = 0 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = 0        , .SystemClockMax = MHz(30)  } ,
                        { .WaitStates = 1 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = MHz(30)  , .SystemClockMax = MHz(60)  } ,
                        { .WaitStates = 2 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = MHz(60)  , .SystemClockMax = MHz(90)  } ,
                        { .WaitStates = 3 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = MHz(90)  , .SystemClockMax = MHz(120) } ,
                        { .WaitStates = 4 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = MHz(120) , .SystemClockMax = MHz(150) } ,
                        { .WaitStates = 5 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = MHz(150) , .SystemClockMax = MHz(180) } ,
                        { .WaitStates = 6 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = MHz(180) , .SystemClockMax = MHz(210) } ,
                        { .WaitStates = 7 , .VddMin = 2.7 , .VddMax = 3.6 , .SystemClockMin = MHz(210) , .SystemClockMax = MHz(216) } ,

                        /* 2.4 - 2.7 V */
                        { .WaitStates = 0 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = 0        , .SystemClockMax = MHz(24)  } ,
                        { .WaitStates = 1 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(24)  , .SystemClockMax = MHz(48)  } ,
                        { .WaitStates = 2 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(48)  , .SystemClockMax = MHz(72)  } ,
                        { .WaitStates = 3 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(72)  , .SystemClockMax = MHz(96) } ,
                        { .WaitStates = 4 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(96)  , .SystemClockMax = MHz(120) } ,
                        { .WaitStates = 5 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(120) , .SystemClockMax = MHz(144) } ,
                        { .WaitStates = 6 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(144) , .SystemClockMax = MHz(168) } ,
                        { .WaitStates = 7 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(168) , .SystemClockMax = MHz(192) } ,
                        { .WaitStates = 8 , .VddMin = 2.4 , .VddMax = 2.7 , .SystemClockMin = MHz(192) , .SystemClockMax = MHz(216) } ,

                        /* 2.1 - 2.4 V */
                        { .WaitStates = 0 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = 0        , .SystemClockMax = MHz(22)  } ,
                        { .WaitStates = 1 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(22)  , .SystemClockMax = MHz(44)  } ,
                        { .WaitStates = 2 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(44)  , .SystemClockMax = MHz(66)  } ,
                        { .WaitStates = 3 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(66)  , .SystemClockMax = MHz(88) } ,
                        { .WaitStates = 4 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(88)  , .SystemClockMax = MHz(110) } ,
                        { .WaitStates = 5 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(110) , .SystemClockMax = MHz(132) } ,
                        { .WaitStates = 6 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(132) , .SystemClockMax = MHz(154) } ,
                        { .WaitStates = 7 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(154) , .SystemClockMax = MHz(176) } ,
                        { .WaitStates = 8 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(176) , .SystemClockMax = MHz(198) } ,
                        { .WaitStates = 9 , .VddMin = 2.1 , .VddMax = 2.4 , .SystemClockMin = MHz(198) , .SystemClockMax = MHz(216) } ,

                        /* 1.8 - 2.1 V */
                        { .WaitStates = 0 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = 0        , .SystemClockMax = MHz(20)  } ,
                        { .WaitStates = 1 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(20)  , .SystemClockMax = MHz(40)  } ,
                        { .WaitStates = 2 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(40)  , .SystemClockMax = MHz(60)  } ,
                        { .WaitStates = 3 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(60)  , .SystemClockMax = MHz(80) } ,
                        { .WaitStates = 4 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(80)  , .SystemClockMax = MHz(100) } ,
                        { .WaitStates = 5 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(100) , .SystemClockMax = MHz(120) } ,
                        { .WaitStates = 6 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(120) , .SystemClockMax = MHz(140) } ,
                        { .WaitStates = 7 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(140) , .SystemClockMax = MHz(160) } ,
                        { .WaitStates = 8 , .VddMin = 1.8 , .VddMax = 2.1 , .SystemClockMin = MHz(160) , .SystemClockMax = MHz(180) }
        };

        errorCode = oC_ErrorCode_VoltageNotCorrect;

        oC_ARRAY_FOREACH_IN_ARRAY(latencyArray,latency)
        {
            if(vdd >= latency->VddMin && vdd < latency->VddMax)
            {
                if(ClockFrequency > latency->SystemClockMin && ClockFrequency < (latency->SystemClockMax + MHz(1)))
                {
                    *outWaitStates  = latency->WaitStates;
                    errorCode       = oC_ErrorCode_None;
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Selects source of main clock
 */
//==========================================================================================================================================
static void SetMainClockSource( MainClockSource_t MainClockSource )
{
    RCC_CFGR->SW = MainClockSource;

    while(RCC_CFGR->SWS != MainClockSource);
}

//==========================================================================================================================================
/**
 * Selects source for PLL
 */
//==========================================================================================================================================
static void SetPllSource( PllSource_t PllSource )
{
    RCC_PLLCFGR->PLLSRC = PllSource;
}

//==========================================================================================================================================
/**
 * Prepares clock configuration parameters
 */
//==========================================================================================================================================
static bool CountClockConfiguration( ClockConfiguration_t * ClockConfiguration )
{
    bool configurationPossible = false;

    /* *************************************
     * At the start we are trying to use   *
     * PLL to receive the best frequency.  *
     * *************************************/
    for(uint8_t PLLM = 2 ; PLLM < 64 ; PLLM++)
    {
        oC_Frequency_t pllInputFrequency = ClockConfiguration->OscillatorFrequency / ((oC_Frequency_t)(PLLM));

        if(pllInputFrequency >= oC_MHz(1) && pllInputFrequency <= oC_MHz(2))
        {
            for(uint16_t PLLN = 0; PLLN < 511 ; PLLN++)
            {
                oC_Frequency_t pllOutputFrequency = pllInputFrequency * ( (oC_Frequency_t)(PLLN) );

                if(pllOutputFrequency >= oC_MHz(192) && pllOutputFrequency < oC_MHz(433))
                {
                    for(uint8_t PLLP = 0 ; PLLP < 4 ; PLLP++)
                    {
                        oC_Frequency_t pllClkFrequency = pllOutputFrequency / ((oC_Frequency_t)((PLLP+1)<<1));

                        if(FindHpre(pllClkFrequency , ClockConfiguration))
                        {
                            ClockConfiguration->UsePll = true;
                            ClockConfiguration->PllM   = PLLM;
                            ClockConfiguration->PllN   = PLLN;
                            ClockConfiguration->PllP   = PLLP;

                            configurationPossible      = true;
                        }
                    }
                }
            }
        }
    }

    /* *************************************
     * If it is possible, it is better to  *
     * use oscillator without PLL          *
     * *************************************/
    if(FindHpre(ClockConfiguration->OscillatorFrequency , ClockConfiguration))
    {
        ClockConfiguration->UsePll = false;
        ClockConfiguration->PllM   = 0;
        ClockConfiguration->PllN   = 0;
        ClockConfiguration->PllP   = 0;

        configurationPossible      = true;
    }


    return configurationPossible;
}

//==========================================================================================================================================
/**
 * Searching for HPRE prescaler value
 */
//==========================================================================================================================================
static bool FindHpre( oC_Frequency_t EntryFrequency , ClockConfiguration_t * ClockConfiguration )
{
    bool            foundHpre       = false;
    const uint16_t  hpreDivisors[]  = { 1 , 1 , 1 , 1 , 1 , 1 , 1 , 1 , 2 , 4 , 8 , 16 , 64 , 128 , 256 , 512 };

    for(uint8_t HPRE = 0x0 ; HPRE <= 0xF ;HPRE++)
    {
        oC_Frequency_t mainClockFrequency = EntryFrequency / ((oC_Frequency_t)(hpreDivisors[HPRE]));

        if((UseEthernet && mainClockFrequency >= MHz(25)) || !UseEthernet)
        {
            if(!foundHpre || oC_ABS(mainClockFrequency,ClockConfiguration->TargetFrequency) <= ClockConfiguration->ResultDifference)
            {
                ClockConfiguration->ResultDifference= oC_ABS(mainClockFrequency,ClockConfiguration->TargetFrequency);

                if(ClockConfiguration->ResultDifference <= ClockConfiguration->PermissibleDifference)
                {
                    ClockConfiguration->Hpre            = HPRE;
                    ClockConfiguration->ResultFrequency = mainClockFrequency;
                    foundHpre                           = true;
                    break;
                }
            }
        }
    }

    return foundHpre;
}

//==========================================================================================================================================
/**
 * Configures PLL
 */
//==========================================================================================================================================
static void ConfigurePll( ClockConfiguration_t * ClockConfiguration )
{
    RCC_PLLCFGR->PLLM = ClockConfiguration->PllM;
    RCC_PLLCFGR->PLLN = ClockConfiguration->PllN;
    RCC_PLLCFGR->PLLP = ClockConfiguration->PllP;
}

//==========================================================================================================================================
/**
 * Configures prescaler (HPRE)
 */
//==========================================================================================================================================
static void SetHpre( uint8_t Hpre )
{
    RCC_CFGR->HPRE = Hpre;
}

//==========================================================================================================================================
/**
 * Should be called after configuration of clock
 */
//==========================================================================================================================================
static void ClockConfigured( oC_Frequency_t RealFrequency , oC_Frequency_t OscillatorFrequency , oC_CLOCK_LLD_ClockSource_t ClockSource )
{
    oC_MCS_EnterCriticalSection();

    CurrentClockSource          = ClockSource;
    CurrentFrequency            = RealFrequency;
    CurrentOscillatorFrequency  = OscillatorFrequency;

    if(IsRom(ClockConfiguredHandler) || IsRam(ClockConfiguredHandler))
    {
        ClockConfiguredHandler(RealFrequency);
    }

    oC_MCS_ExitCriticalSection();
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
