/** ****************************************************************************************************************************************
 *
 * @file       oC_FMC_lld.c
 *
 * @brief      The file with source for FMC LLD functions
 *
 * @file       oC_FMC_lld.c
 *
 * @author     Patryk Kubiak - (Created on: 2016-04-02 - 16:12:03)
 *
 * @copyright  Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_fmc_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <oc_cfg.h>
#include <oc_array.h>
#include <oc_gpio_mslld.h>
#include <oc_math.h>
#include <oc_clock_lld.h>
#include <oc_string.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(Address)                      (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define IsExternal(Address)                 oC_LSF_IsExternalAddress(Address)
#define IsAddressCorrect(Address)           IsRam(Address) || IsRom(Address)
#define IsChannelPoweredOn()                (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)
#define IsBankUsed(Bank)                    ((BanksUsage & ((Bank) & Bank_MainBankMask)) != 0)
#define GetNumberOfPinsInStructure(PinsType)    ( (sizeof(PinsType)) / sizeof(oC_Pin_t) )
#define SetBankUsed(Bank)                   BanksUsage |= (Bank)
#define SetBankUnused(Bank)                 BanksUsage &= ~(Bank)
#define FMC_Channel                         oC_Channel_FMC
#define MAX_MEMORY_SIZE                     oC_MemorySize_MiB(512)
#define MAX_SDRAM_SIZE                      oC_MemorySize_MiB(512)
#define FMC_BCR1                            oC_Register(FMC,FMC_BCR1)
#define FMC_BCR2                            oC_Register(FMC,FMC_BCR2)
#define FMC_BCR3                            oC_Register(FMC,FMC_BCR3)
#define FMC_BCR4                            oC_Register(FMC,FMC_BCR4)
#define FMC_BTR1                            oC_Register(FMC,FMC_BTR1)
#define FMC_BTR2                            oC_Register(FMC,FMC_BTR2)
#define FMC_BTR3                            oC_Register(FMC,FMC_BTR3)
#define FMC_BTR4                            oC_Register(FMC,FMC_BTR4)
#define FMC_BWTR1                           oC_Register(FMC,FMC_BWTR1)
#define FMC_BWTR2                           oC_Register(FMC,FMC_BWTR2)
#define FMC_BWTR3                           oC_Register(FMC,FMC_BWTR3)
#define FMC_BWTR4                           oC_Register(FMC,FMC_BWTR4)
#define FMC_PCR                             oC_Register(FMC,FMC_PCR)
#define FMC_SR                              oC_Register(FMC,FMC_SR)
#define FMC_PMEM                            oC_Register(FMC,FMC_PMEM)
#define FMC_PATT                            oC_Register(FMC,FMC_PATT)
#define FMC_ECCR                            oC_Register(FMC,FMC_ECCR)
#define FMC_SDCR1                           oC_Register(FMC,FMC_SDCR1)
#define FMC_SDCR2                           oC_Register(FMC,FMC_SDCR2)
#define FMC_SDTR1                           oC_Register(FMC,FMC_SDTR1)
#define FMC_SDTR2                           oC_Register(FMC,FMC_SDTR2)
#define FMC_SDCMR                           oC_Register(FMC,FMC_SDCMR)
#define FMC_SDRTR                           oC_Register(FMC,FMC_SDRTR)
#define FMC_SDSR                            oC_Register(FMC,FMC_SDSR)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Stores STM bank number
 */
//==========================================================================================================================================
typedef enum
{
    Bank_SubBankMask            = 0xF ,                            //!< Mask for SUB banks numbers
    Bank_SubBank1               = (1<<0) ,                         //!< SUB Bank number 1
    Bank_SubBank2               = (1<<1) ,                         //!< SUB Bank number 2
    Bank_SubBank3               = (1<<2) ,                         //!< SUB Bank number 3
    Bank_SubBank4               = (1<<3) ,                         //!< SUB Bank number 4
    Bank_MainBankMask           = 0xF0 ,                           //!< SUB Mask for MAIN banks numbers
    Bank_MainBank1              = (1<<4) ,                         //!< MAIN Bank number 1
    Bank_MainBank3              = (1<<5) ,                         //!< MAIN Bank number 3
    Bank_MainBank5              = (1<<6) ,                         //!< MAIN Bank number 5
    Bank_MainBank6              = (1<<7) ,                         //!< MAIN Bank number 6
    Bank_1_NOR_PSRAM1           = Bank_MainBank1 | Bank_SubBank1  ,//!< MAIN Bank 1 SUB Bank 1 NOR_PSRAM
    Bank_1_NOR_PSRAM2           = Bank_MainBank1 | Bank_SubBank2  ,//!< MAIN Bank 1 SUB Bank 2 NOR_PSRAM
    Bank_1_NOR_PSRAM3           = Bank_MainBank1 | Bank_SubBank3  ,//!< MAIN Bank 1 SUB Bank 3 NOR_PSRAM
    Bank_1_NOR_PSRAM4           = Bank_MainBank1 | Bank_SubBank4  ,//!< MAIN Bank 1 SUB Bank 4 NOR_PSRAM
    Bank_3_NAND_FlashMemory1    = Bank_MainBank3 | Bank_SubBank1  ,//!< MAIN Bank 3 SUB Bank 1 NAND_FlashMemory
    Bank_3_NAND_FlashMemory2    = Bank_MainBank3 | Bank_SubBank2  ,//!< MAIN Bank 3 SUB Bank 2 NAND_FlashMemory
    Bank_3_NAND_FlashMemory3    = Bank_MainBank3 | Bank_SubBank3  ,//!< MAIN Bank 3 SUB Bank 3 NAND_FlashMemory
    Bank_3_NAND_FlashMemory4    = Bank_MainBank3 | Bank_SubBank4  ,//!< MAIN Bank 3 SUB Bank 4 NAND_FlashMemory
    Bank_5_SDRAM1               = Bank_MainBank5 | Bank_SubBank1  ,//!< MAIN Bank 5 SUB Bank 1 SDRAM
    Bank_5_SDRAM2               = Bank_MainBank5 | Bank_SubBank2  ,//!< MAIN Bank 5 SUB Bank 2 SDRAM
    Bank_5_SDRAM3               = Bank_MainBank5 | Bank_SubBank3 , //!< MAIN Bank 5 SUB Bank 3 SDRAM
    Bank_5_SDRAM4               = Bank_MainBank5 | Bank_SubBank4 , //!< MAIN Bank 5 SUB Bank 4 SDRAM
    Bank_6_SDRAM1               = Bank_MainBank6 | Bank_SubBank1 , //!< MAIN Bank 6 SUB Bank 1 SDRAM
    Bank_6_SDRAM2               = Bank_MainBank6 | Bank_SubBank2 , //!< MAIN Bank 6 SUB Bank 2 SDRAM
    Bank_6_SDRAM3               = Bank_MainBank6 | Bank_SubBank3 , //!< MAIN Bank 6 SUB Bank 3 SDRAM
    Bank_6_SDRAM4               = Bank_MainBank6 | Bank_SubBank4 , //!< MAIN Bank 6 SUB Bank 4 SDRAM
} Bank_t;

//==========================================================================================================================================
/**
 * @brief stores DATA about the bank
 */
//==========================================================================================================================================
typedef struct
{
    void *                      StartAddress;   //!< Start address of the bank
    void *                      EndAddress;     //!< End address of the bank
    oC_UInt_t                   Size;           //!< Size of the bank
    Bank_t                      Bank;           //!< Bank number mask
    oC_FMC_LLD_MemoryType_t     MemoryType;     //!< Memory type that bank is designed for
} BankData_t;

//==========================================================================================================================================
/**
 * @brief stores SDRAM command to send
 */
//==========================================================================================================================================
typedef enum
{
    CommandMode_NormalMode ,
    CommandMode_ClockConfigurationEnable ,
    CommandMode_AllBankPrecharge ,
    CommandMode_AutoRefresh ,
    CommandMode_LoadModeRegister ,
    CommandMode_SelfRefresh ,
    CommandMode_PowerDown
} CommandMode_t;

//==========================================================================================================================================
/**
 * @brief stores data for SDRAM commands
 */
//==========================================================================================================================================
typedef struct
{
    CommandMode_t               CommandMode;             //!< Command to send
    uint32_t                    AutoRefreshNumber;       //!< Number of auto-refresh
    uint32_t                    ModeRegisterDefinition;  //!< MDR data
} SDRAMCommand_t;

//==========================================================================================================================================
/**
 * @brief stores variables required for SDRAM configuration
 */
//==========================================================================================================================================
typedef struct
{
    oC_RegisterType_FMC_SDCR1_t SDCR;
    oC_RegisterType_FMC_SDTR1_t SDTR;
} SDRAMConfigVariables_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static Bank_t           BanksUsage  = 0;
static const BankData_t BankDatas[] = {
    /* =========================== BANK 1 ============================== */
    {
        .Bank         = Bank_1_NOR_PSRAM1 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NOR_Flash | oC_FMC_LLD_MemoryType_PSRAM  ,
        .StartAddress = (void*)0x60000000 ,
        .EndAddress   = (void*)0x64000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_1_NOR_PSRAM2 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NOR_Flash | oC_FMC_LLD_MemoryType_PSRAM  ,
        .StartAddress = (void*)0x64000000 ,
        .EndAddress   = (void*)0x68000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_1_NOR_PSRAM3 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NOR_Flash | oC_FMC_LLD_MemoryType_PSRAM  ,
        .StartAddress = (void*)0x68000000 ,
        .EndAddress   = (void*)0x6C000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_1_NOR_PSRAM4 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NOR_Flash | oC_FMC_LLD_MemoryType_PSRAM  ,
        .StartAddress = (void*)0x6C000000 ,
        .EndAddress   = (void*)0x70000000 ,
        .Size         = MB(64)
    } ,
    /* =========================== BANK 3 ============================== */
    {
        .Bank         = Bank_3_NAND_FlashMemory1 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NAND_Flash ,
        .StartAddress = (void*)0x80000000 ,
        .EndAddress   = (void*)0x84000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_3_NAND_FlashMemory2 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NAND_Flash ,
        .StartAddress = (void*)0x84000000 ,
        .EndAddress   = (void*)0x88000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_3_NAND_FlashMemory3 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NAND_Flash ,
        .StartAddress = (void*)0x88000000 ,
        .EndAddress   = (void*)0x8C000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_3_NAND_FlashMemory4 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_NAND_Flash ,
        .StartAddress = (void*)0x8C000000 ,
        .EndAddress   = (void*)0x90000000 ,
        .Size         = MB(64)
    } ,
    /* =========================== BANK 5 ============================== */
    {
        .Bank         = Bank_5_SDRAM1 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xC0000000 ,
        .EndAddress   = (void*)0xC4000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_5_SDRAM2 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xC4000000 ,
        .EndAddress   = (void*)0xC8000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_5_SDRAM3 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xC8000000 ,
        .EndAddress   = (void*)0xCC000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_5_SDRAM4 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xCC000000 ,
        .EndAddress   = (void*)0xD0000000 ,
        .Size         = MB(64)
    } ,
    /* =========================== BANK 6 ============================== */
    {
        .Bank         = Bank_6_SDRAM1 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xD0000000 ,
        .EndAddress   = (void*)0xD4000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_6_SDRAM2 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xD4000000 ,
        .EndAddress   = (void*)0xD8000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_6_SDRAM3 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xD8000000 ,
        .EndAddress   = (void*)0xDC000000 ,
        .Size         = MB(64)
    } ,
    {
        .Bank         = Bank_6_SDRAM4 ,
        .MemoryType   = oC_FMC_LLD_MemoryType_SDRAM ,
        .StartAddress = (void*)0xDC000000 ,
        .EndAddress   = (void*)0xE0000000 ,
        .Size         = MB(64)
    } ,
};


#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool      IsProtectionCorrect         ( oC_FMC_LLD_Protection_t   Protection    );
static inline bool      IsDataBusWidthCorrect       ( oC_FMC_LLD_DataBusWidth_t DataBusWidth  );
static inline bool      IsDataBusWidthSupported     ( oC_FMC_LLD_DataBusWidth_t DataBusWidth );
static oC_ErrorCode_t   ConnectSDRAMModulePins      ( const oC_FMC_LLD_SDRAM_Pins_t *   Pins , oC_FMC_LLD_Result_t * Result );
static oC_ErrorCode_t   DisconnectModulePins        ( const oC_Pin_t * Pins , oC_FMC_LLD_PinUsage_t * PinsUsage , oC_UInt_t NumberOfPins );
static bool             FindFreeBanks               ( oC_FMC_LLD_MemoryType_t MemoryType , oC_MemorySize_t MemorySize , Bank_t * outBanks , uint8_t ** outAddress );
static oC_ErrorCode_t   ConfigureSDRAM              ( const oC_FMC_LLD_SDRAM_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , SDRAMConfigVariables_t * outVariables , oC_FMC_LLD_Result_t * outResult );
static oC_ErrorCode_t   UnconfigureSDRAM            ( const oC_FMC_LLD_SDRAM_Config_t *  Config );
static oC_ErrorCode_t   ConnectModulePin            ( oC_Pin_t Pin , oC_PinFunction_t PinFunction , oC_FMC_LLD_PinUsage_t * PinUsage);
static oC_ErrorCode_t   DisconnectPin               ( oC_Pin_t Pin );
static uint32_t         TimeToCycles                ( oC_Time_t Time , oC_Frequency_t Frequency );
static bool             SetTimeInCycles             ( uint32_t * outCycles , oC_Time_t Time , oC_Frequency_t Frequency , uint32_t Min , uint32_t Max , uint32_t Default );
static bool             SendSDRAMCommand            ( oC_Time_t * Timeout , SDRAMCommand_t * Command , Bank_t Banks );
static oC_ErrorCode_t   CountSDRAMConfigVariables   ( const oC_FMC_LLD_SDRAM_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , SDRAMConfigVariables_t * outVariables , oC_FMC_LLD_Result_t * outResult );
static void             CountSDRAMRequiredPins      ( const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_FMC_LLD))
    {
        oC_Module_TurnOn(oC_Module_FMC_LLD);
        BanksUsage          = 0;
        errorCode           = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC_LLD))
    {
        oC_Module_TurnOff(oC_Module_FMC_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_ConfigureSDRAM( const oC_FMC_LLD_SDRAM_Config_t * Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    oC_ErrorCode_t          errorCode           = oC_ErrorCode_ImplementError;
    SDRAMConfigVariables_t  configVariables;

    memset(&configVariables,0,sizeof(configVariables));

    if(oC_Module_TurnOnVerification(&errorCode,oC_Module_FMC_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if(
            ErrorCondition( IsAddressCorrect(Config)                             , oC_ErrorCode_WrongConfigAddress           )
         && ErrorCondition( IsAddressCorrect(ChipInfo)                           , oC_ErrorCode_WrongAddress                 )
         && ErrorCondition( IsRam(outResult)                                     , oC_ErrorCode_OutputAddressNotInRAM        )
         && ErrorCondition( ChipInfo->MemoryType == oC_FMC_LLD_MemoryType_SDRAM  , oC_ErrorCode_MemoryTypeNotCorrect         )
            )
        {
            if(
                oC_AssignErrorCode(&errorCode , CountSDRAMConfigVariables( Config        , ChipInfo, &configVariables , outResult ))
             && oC_AssignErrorCode(&errorCode , ConnectSDRAMModulePins   ( &Config->Pins , outResult                              ))
             && oC_AssignErrorCode(&errorCode , ConfigureSDRAM           ( Config        , ChipInfo, &configVariables , outResult ))
                )
            {
                SetBankUsed(outResult->ConfiguredBanks);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                memset(outResult,0,sizeof(oC_FMC_LLD_Result_t));
            }
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_ConfigureNORFlash( const oC_FMC_LLD_NORFlash_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_ConfigureNANDFlash(const oC_FMC_LLD_NANDFlash_Config_t * Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_ConfigurePSRAM(    const oC_FMC_LLD_PSRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_UnconfigureSDRAM( const oC_FMC_LLD_SDRAM_Config_t * Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC_LLD))
    {
        if(
            ErrorCondition( IsRam(Config) || IsRom(Config) , oC_ErrorCode_WrongConfigAddress        ) &&
            ErrorCondition( IsRam(outResult)               , oC_ErrorCode_OutputAddressNotInRAM     )
            )
        {
            oC_MCS_EnterCriticalSection();

            if(
                oC_AssignErrorCode(&errorCode , DisconnectModulePins(Config->Pins.PinsArray,outResult->PinsUsage,GetNumberOfPinsInStructure(oC_FMC_LLD_SDRAM_Pins_t))  ) &&
                oC_AssignErrorCode(&errorCode , UnconfigureSDRAM(Config)                                                                                            )
                )
            {
                SetBankUnused(outResult->ConfiguredBanks);
                errorCode = oC_ErrorCode_None;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_UnconfigureNORFlash( const oC_FMC_LLD_NORFlash_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_UnconfigureNANDFlash(const oC_FMC_LLD_NANDFlash_Config_t * Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_UnconfigurePSRAM(    const oC_FMC_LLD_PSRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_SendSDRAMCommand( oC_FMC_LLD_Result_t * Result , oC_Time_t * Timeout , oC_FMC_LLD_SDRAM_Command_t Command , const oC_FMC_LLD_SDRAM_CommandData_t * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC_LLD ) &&
        ErrorCondition( IsRam(Result) , oC_ErrorCode_WrongAddress )
        )
    {
        bool           sendCommand = false;
        SDRAMCommand_t command     = {0};

        switch(Command)
        {
            case oC_FMC_LLD_SDRAM_Command_EnableClock:
                command.CommandMode             = CommandMode_ClockConfigurationEnable;
                command.AutoRefreshNumber       = 1;
                command.ModeRegisterDefinition  = 0;
                sendCommand                     = true;
                errorCode                       = oC_ErrorCode_None;
                break;
            case oC_FMC_LLD_SDRAM_Command_Inhibit         :
                sendCommand                     = false;
                errorCode   = oC_ErrorCode_None;
                break;
            case oC_FMC_LLD_SDRAM_Command_Nop             :
                sendCommand = false;
                errorCode   = oC_ErrorCode_None;
                break;
            case oC_FMC_LLD_SDRAM_Command_LoadModeRegister:
                if(ErrorCondition(IsRam(Data) || IsRom(Data) , oC_ErrorCode_WrongAddress))
                {
                    command.CommandMode             = CommandMode_LoadModeRegister;
                    command.AutoRefreshNumber       = 1;
                    command.ModeRegisterDefinition  = Data->LoadModeRegister.MRD;
                    sendCommand                     = true;
                    errorCode   = oC_ErrorCode_None;
                }
                break;
            case oC_FMC_LLD_SDRAM_Command_Active:
                errorCode = oC_ErrorCode_NotImplemented;
                break;
            case oC_FMC_LLD_SDRAM_Command_Read:
                errorCode = oC_ErrorCode_NotImplemented;
                break;
            case oC_FMC_LLD_SDRAM_Command_Write:
                errorCode = oC_ErrorCode_NotImplemented;
                break;
            case oC_FMC_LLD_SDRAM_Command_Precharge:
                if(ErrorCondition(IsRam(Data) || IsRom(Data) , oC_ErrorCode_WrongAddress))
                {
                    command.CommandMode             = CommandMode_AllBankPrecharge;
                    command.AutoRefreshNumber       = 1;
                    command.ModeRegisterDefinition  = 0;
                    sendCommand                     = true;
                    errorCode                       = oC_ErrorCode_None;
                }
                break;
            case oC_FMC_LLD_SDRAM_Command_BurstTerminate:
                errorCode = oC_ErrorCode_NotImplemented;
                break;
            case oC_FMC_LLD_SDRAM_Command_AutoRefresh:
                if(ErrorCondition(IsRam(Data) || IsRom(Data) , oC_ErrorCode_WrongAddress))
                {
                    command.CommandMode             = CommandMode_AutoRefresh;
                    command.AutoRefreshNumber       = Data->AutoRefresh.NumberOfAutoRefresh;
                    command.ModeRegisterDefinition  = 0;
                    sendCommand                     = true;
                    errorCode                       = oC_ErrorCode_None;
                }
                break;
            case oC_FMC_LLD_SDRAM_Command_SelfRefresh:
                if(ErrorCondition(IsRam(Data) || IsRom(Data) , oC_ErrorCode_WrongAddress))
                {
                    command.CommandMode             = CommandMode_SelfRefresh;
                    command.AutoRefreshNumber       = Data->AutoRefresh.NumberOfAutoRefresh;
                    command.ModeRegisterDefinition  = 0;
                    sendCommand                     = true;
                    errorCode                       = oC_ErrorCode_None;
                }
                break;
            default:
                errorCode   = oC_ErrorCode_CommandNotCorrect;
                break;
        }

        if(!oC_ErrorOccur(errorCode) && sendCommand == true)
        {
            if(ErrorCondition(SendSDRAMCommand(Timeout,&command,Result->ConfiguredBanks) , oC_ErrorCode_Timeout))
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_FinishSDRAMInitialization( const oC_FMC_LLD_SDRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode,oC_Module_FMC_LLD))
    {
        if(
            ErrorCondition( IsAddressCorrect(Config)                                , oC_ErrorCode_WrongConfigAddress         )
         && ErrorCondition( IsAddressCorrect(ChipInfo)                              , oC_ErrorCode_WrongAddress               )
         && ErrorCondition( IsRam(Result)                                           , oC_ErrorCode_OutputAddressNotInRAM      )
         && ErrorCondition( IsExternal(Result->MemoryStart)                         , oC_ErrorCode_MemoryNotConfigured        )
         && ErrorCondition( Result->DataBusWidth > 0 && Result->DataBusWidth <= 4   , oC_ErrorCode_MemoryNotConfigured        )
         && ErrorCondition( Result->ConfiguredFrequency > 0                         , oC_ErrorCode_MemoryNotConfigured        )
         && ErrorCondition( ChipInfo->MemoryType == oC_FMC_LLD_MemoryType_SDRAM     , oC_ErrorCode_MemoryTypeNotCorrect       )
         && ErrorCondition( ChipInfo->SDRAM.NumberOfRowAddressBits >= 11            , oC_ErrorCode_RowBitsNumberNotSupported  )
         && ErrorCondition( ChipInfo->SDRAM.NumberOfRowAddressBits <= 13            , oC_ErrorCode_RowBitsNumberNotSupported  )
         && ErrorCondition( ChipInfo->SDRAM.AutoRefreshPeriod > 0                   , oC_ErrorCode_AutoRefreshPeriodNotCorrect)
         && ErrorCondition( ChipInfo->SDRAM.MaximumClockFrequency > 0               , oC_ErrorCode_WrongFrequency             )
            )
        {
            /* Counting maximum number of rows, that is possible for the chip */
            uint32_t maximumNumberOfRows = oC_Bits_Mask_U32(0,ChipInfo->SDRAM.NumberOfRowAddressBits);

            /* Counting number of used rows according to the configured data bus width */
            maximumNumberOfRows /= ChipInfo->SDRAM.DataBusWidth / Result->DataBusWidth;

            /* Counting refresh rate in number of SDCLK clock cycles between the refresh cycles */
            uint32_t count = (uint32_t)((ChipInfo->SDRAM.AutoRefreshPeriod / ((double)maximumNumberOfRows)) * Result->ConfiguredFrequency);

            if(count <= oC_Bits_Mask_U32(0,12))
            {
                FMC_SDRTR->COUNT = count;
                errorCode        = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_FrequencyNotPossible;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_FinishNORFlashInitialization( const oC_FMC_LLD_NORFlash_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_FinishNANDFlashInitialization( const oC_FMC_LLD_NANDFlash_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_LLD_FinishPSRAMInitialization( const oC_FMC_LLD_PSRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result )
{
    return oC_ErrorCode_NotImplemented;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if protection type is correct
 */
//==========================================================================================================================================
static inline bool IsProtectionCorrect( oC_FMC_LLD_Protection_t Protection )
{
    return Protection == 0 ||
           Protection <= (oC_FMC_LLD_Protection_AllowExecute | oC_FMC_LLD_Protection_AllowRead | oC_FMC_LLD_Protection_AllowWrite);
}

//==========================================================================================================================================
/**
 * @brief checks if data bus width is correct
 */
//==========================================================================================================================================
static inline bool IsDataBusWidthCorrect( oC_FMC_LLD_DataBusWidth_t DataBusWidth )
{
    oC_STATIC_ASSERT(oC_FMC_LLD_DataBusWidth_8Bits  == 1 , "data bus width must be equal to the number of used bytes!");
    oC_STATIC_ASSERT(oC_FMC_LLD_DataBusWidth_16Bits == 2 , "data bus width must be equal to the number of used bytes!");
    oC_STATIC_ASSERT(oC_FMC_LLD_DataBusWidth_32Bits == 4 , "data bus width must be equal to the number of used bytes!");
    oC_STATIC_ASSERT(oC_FMC_LLD_DataBusWidth_64Bits == 8 , "data bus width must be equal to the number of used bytes!");

    return DataBusWidth == oC_FMC_LLD_DataBusWidth_8Bits
        || DataBusWidth == oC_FMC_LLD_DataBusWidth_16Bits
        || DataBusWidth == oC_FMC_LLD_DataBusWidth_32Bits
        || DataBusWidth == oC_FMC_LLD_DataBusWidth_64Bits;
}

//==========================================================================================================================================
/**
 * @brief checks if data bus width is supported
 */
//==========================================================================================================================================
static inline bool IsDataBusWidthSupported( oC_FMC_LLD_DataBusWidth_t DataBusWidth )
{
    return DataBusWidth == oC_FMC_LLD_DataBusWidth_8Bits
        || DataBusWidth == oC_FMC_LLD_DataBusWidth_16Bits
        || DataBusWidth == oC_FMC_LLD_DataBusWidth_32Bits;
}


//==========================================================================================================================================
/**
 * @brief connects module pins for SDRAM
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectSDRAMModulePins( const oC_FMC_LLD_SDRAM_Pins_t *   Pins , oC_FMC_LLD_Result_t * Result )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Procedure_Begin
    {
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->SDCLK    , oC_FMC_PinFunction_FMC_SDCLK   , &Result->SDCLK         ) );

        oC_Procedure_ExitIfError(ConnectModulePin( Pins->SDCKE[0] , oC_FMC_PinFunction_FMC_SDCKE0  , &Result->SDCKE[0]      ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->SDCKE[1] , oC_FMC_PinFunction_FMC_SDCKE1  , &Result->SDCKE[1]      ) );


        oC_Procedure_ExitIfError(ConnectModulePin( Pins->SDNE[0]  , oC_FMC_PinFunction_FMC_SDNE0   , &Result->SDNE[0]       ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->SDNE[1]  , oC_FMC_PinFunction_FMC_SDNE1   , &Result->SDNE[1]       ) );


        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 0]    , oC_FMC_PinFunction_FMC_A0      , &Result->A[ 0]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 1]    , oC_FMC_PinFunction_FMC_A1      , &Result->A[ 1]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 2]    , oC_FMC_PinFunction_FMC_A2      , &Result->A[ 2]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 3]    , oC_FMC_PinFunction_FMC_A3      , &Result->A[ 3]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 4]    , oC_FMC_PinFunction_FMC_A4      , &Result->A[ 4]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 5]    , oC_FMC_PinFunction_FMC_A5      , &Result->A[ 5]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 6]    , oC_FMC_PinFunction_FMC_A6      , &Result->A[ 6]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 7]    , oC_FMC_PinFunction_FMC_A7      , &Result->A[ 7]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 8]    , oC_FMC_PinFunction_FMC_A8      , &Result->A[ 8]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[ 9]    , oC_FMC_PinFunction_FMC_A9      , &Result->A[ 9]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[10]    , oC_FMC_PinFunction_FMC_A10     , &Result->A[10]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[11]    , oC_FMC_PinFunction_FMC_A11     , &Result->A[11]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->A[12]    , oC_FMC_PinFunction_FMC_A12     , &Result->A[12]         ) );


        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 0]    , oC_FMC_PinFunction_FMC_D0      , &Result->D[ 0]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 1]    , oC_FMC_PinFunction_FMC_D1      , &Result->D[ 1]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 2]    , oC_FMC_PinFunction_FMC_D2      , &Result->D[ 2]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 3]    , oC_FMC_PinFunction_FMC_D3      , &Result->D[ 3]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 4]    , oC_FMC_PinFunction_FMC_D4      , &Result->D[ 4]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 5]    , oC_FMC_PinFunction_FMC_D5      , &Result->D[ 5]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 6]    , oC_FMC_PinFunction_FMC_D6      , &Result->D[ 6]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 7]    , oC_FMC_PinFunction_FMC_D7      , &Result->D[ 7]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 8]    , oC_FMC_PinFunction_FMC_D8      , &Result->D[ 8]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[ 9]    , oC_FMC_PinFunction_FMC_D9      , &Result->D[ 9]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[10]    , oC_FMC_PinFunction_FMC_D10     , &Result->D[10]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[11]    , oC_FMC_PinFunction_FMC_D11     , &Result->D[11]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[12]    , oC_FMC_PinFunction_FMC_D12     , &Result->D[12]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[13]    , oC_FMC_PinFunction_FMC_D13     , &Result->D[13]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[14]    , oC_FMC_PinFunction_FMC_D14     , &Result->D[14]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[15]    , oC_FMC_PinFunction_FMC_D15     , &Result->D[15]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[16]    , oC_FMC_PinFunction_FMC_D16     , &Result->D[16]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[17]    , oC_FMC_PinFunction_FMC_D17     , &Result->D[17]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[18]    , oC_FMC_PinFunction_FMC_D18     , &Result->D[18]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[19]    , oC_FMC_PinFunction_FMC_D19     , &Result->D[19]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[20]    , oC_FMC_PinFunction_FMC_D20     , &Result->D[20]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[21]    , oC_FMC_PinFunction_FMC_D21     , &Result->D[21]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[22]    , oC_FMC_PinFunction_FMC_D22     , &Result->D[22]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[23]    , oC_FMC_PinFunction_FMC_D23     , &Result->D[23]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[24]    , oC_FMC_PinFunction_FMC_D24     , &Result->D[24]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[25]    , oC_FMC_PinFunction_FMC_D25     , &Result->D[25]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[26]    , oC_FMC_PinFunction_FMC_D26     , &Result->D[26]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[27]    , oC_FMC_PinFunction_FMC_D27     , &Result->D[27]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[28]    , oC_FMC_PinFunction_FMC_D28     , &Result->D[28]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[29]    , oC_FMC_PinFunction_FMC_D29     , &Result->D[29]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[30]    , oC_FMC_PinFunction_FMC_D30     , &Result->D[30]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->D[31]    , oC_FMC_PinFunction_FMC_D31     , &Result->D[31]         ) );

        oC_Procedure_ExitIfError(ConnectModulePin( Pins->BA[0]    , oC_FMC_PinFunction_FMC_BA0     , &Result->BA[0]         ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->BA[1]    , oC_FMC_PinFunction_FMC_BA1     , &Result->BA[1]         ) );

        oC_Procedure_ExitIfError(ConnectModulePin( Pins->NRAS     , oC_FMC_PinFunction_FMC_SDNRAS  , &Result->NRAS          ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->NCAS     , oC_FMC_PinFunction_FMC_SDNCAS  , &Result->NCAS          ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->SDNWE    , oC_FMC_PinFunction_FMC_SDNWE   , &Result->SDNWE         ) );

        oC_Procedure_ExitIfError(ConnectModulePin( Pins->NBL[0]   , oC_FMC_PinFunction_FMC_NBL0    , &Result->NBL[0]        ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->NBL[1]   , oC_FMC_PinFunction_FMC_NBL1    , &Result->NBL[1]        ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->NBL[2]   , oC_FMC_PinFunction_FMC_NBL2    , &Result->NBL[2]        ) );
        oC_Procedure_ExitIfError(ConnectModulePin( Pins->NBL[3]   , oC_FMC_PinFunction_FMC_NBL3    , &Result->NBL[3]        ) );

        errorCode = oC_ErrorCode_None;
    }
    oC_Procedure_End;

    if(oC_ErrorOccur(errorCode))
    {
        DisconnectModulePins(Pins->PinsArray,Result->PinsUsage,GetNumberOfPinsInStructure(oC_FMC_LLD_SDRAM_Pins_t));
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disconnects module pins
 */
//==========================================================================================================================================
static oC_ErrorCode_t DisconnectModulePins( const oC_Pin_t * Pins , oC_FMC_LLD_PinUsage_t * PinsUsage , oC_UInt_t NumberOfPins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;

    for(uint32_t pinIndex = 0; pinIndex < NumberOfPins ; pinIndex++)
    {
        if(PinsUsage[pinIndex] == oC_FMC_LLD_PinUsage_Used)
        {
            oC_AssignErrorCode(&errorCode,DisconnectPin(Pins[pinIndex]));
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief searches for not used banks required by the configuration
 */
//==========================================================================================================================================
static bool FindFreeBanks( oC_FMC_LLD_MemoryType_t MemoryType , oC_MemorySize_t MemorySize , Bank_t * outBanks , uint8_t ** outAddress )
{
    Bank_t     banks         = 0;
    BankData_t foundBankData = {0};

    oC_ARRAY_FOREACH_IN_ARRAY(BankDatas,bankData)
    {
        if(IsBankUsed(bankData->Bank) == false)
        {
            if(bankData->MemoryType == MemoryType)
            {
                /* If this is the first bank that has been found */
                if(foundBankData.Bank == 0)
                {
                    foundBankData.Bank          = bankData->Bank;
                    foundBankData.StartAddress  = bankData->StartAddress;
                    foundBankData.EndAddress    = bankData->EndAddress;
                    foundBankData.MemoryType    = bankData->MemoryType;
                    foundBankData.Size          = bankData->Size;
                }
                /* If some bank was found before */
                else
                {
                    if(bankData->StartAddress == foundBankData.EndAddress)
                    {
                        foundBankData.Bank       |= bankData->Bank;
                        foundBankData.EndAddress  = bankData->EndAddress;
                        foundBankData.Size       += bankData->Size;
                    }
                    else if(bankData->EndAddress == foundBankData.StartAddress)
                    {
                        foundBankData.Bank       |= bankData->Bank;
                        foundBankData.StartAddress= bankData->StartAddress;
                        foundBankData.Size       += bankData->Size;
                    }
                }

                if(foundBankData.Size >= MemorySize)
                {
                    break;
                }
            }
        }
    }

    if(foundBankData.Size >= MemorySize)
    {
        banks       = foundBankData.Bank;
        *outBanks   = banks;
        *outAddress = foundBankData.StartAddress;
    }

    return banks != 0;
}

//==========================================================================================================================================
/**
 * @brief removes configuration of the SDRAM
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigureSDRAM( const oC_FMC_LLD_SDRAM_Config_t *  Config )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief connects module pin
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectModulePin( oC_Pin_t Pin , oC_PinFunction_t PinFunction , oC_FMC_LLD_PinUsage_t * PinUsage )
{
    oC_ErrorCode_t        errorCode = oC_ErrorCode_ImplementError;
    oC_ModulePinIndex_t   modulePin = 0;
    bool                  pinUsed   = false;

    if(Pin == oC_Pin_NotUsed && (*PinUsage) != oC_FMC_LLD_PinUsage_Required)
    {
        errorCode = oC_ErrorCode_None;
    }
    else if(ErrorCondition(oC_GPIO_LLD_IsPinDefined(Pin), oC_ErrorCode_PinNotDefined                                    )
         && oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUsed(Pin , &pinUsed)                                  )
         && ErrorCondition(pinUsed == false             , oC_ErrorCode_PinIsUsed                                        )
         && oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_FindModulePin(Pin , oC_Channel_FMC , PinFunction , &modulePin))
         && oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_ConnectModulePin(modulePin)                                   )
         && oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(Pin)                                                )
            )
    {
        *PinUsage = oC_FMC_LLD_PinUsage_Used;
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disconnects pin
 */
//==========================================================================================================================================
static oC_ErrorCode_t DisconnectPin( oC_Pin_t Pin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_SetAlternateNumber(Pin,0) )
     && oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUnused(Pin)          )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief converts time to cycles according to the Frequency
 */
//==========================================================================================================================================
static uint32_t TimeToCycles( oC_Time_t Time , oC_Frequency_t Frequency )
{
    return (uint32_t)(Time / oC_Frequency_ToTime(Frequency));
}

//==========================================================================================================================================
/**
 * @brief sets time in cycles
 */
//==========================================================================================================================================
static bool SetTimeInCycles( uint32_t * outCycles , oC_Time_t Time , oC_Frequency_t Frequency , uint32_t Min , uint32_t Max , uint32_t Default )
{
    bool     success = false;
    uint32_t cycles  = TimeToCycles(Time,Frequency);

    if(Time == 0)
    {
        *outCycles = Default;
        success    = true;
    }
    else if(cycles >= Min && cycles <= Max)
    {
        *outCycles = cycles;
        success    = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief sends SDRAM command
 */
//==========================================================================================================================================
static bool SendSDRAMCommand( oC_Time_t * Timeout , SDRAMCommand_t * Command , Bank_t Banks )
{
    oC_RegisterType_FMC_SDCMR_t SDCMR = { .Value = FMC_SDCMR->Value };

    SDCMR.MRD   = Command->ModeRegisterDefinition;
    SDCMR.NRFS  = Command->AutoRefreshNumber;
    SDCMR.CTB1  = (Banks & Bank_MainBank5) ? 1 : 0;
    SDCMR.CTB2  = (Banks & Bank_MainBank6) ? 1 : 0;
    SDCMR.MODE  = Command->CommandMode;

    FMC_SDCMR->Value = SDCMR.Value;

    while(FMC_SDSR->BUSY && (*Timeout) > 0)
    {
        oC_CLOCK_LLD_DelayForMicroseconds(10);
        *Timeout = *Timeout - oC_Time_ToMicroseconds(10);
    }

    return FMC_SDSR->BUSY == 0;
}

//==========================================================================================================================================
/**
 * @brief counts variables required by the SDRAM configuration procedure
 */
//==========================================================================================================================================
static oC_ErrorCode_t CountSDRAMConfigVariables( const oC_FMC_LLD_SDRAM_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , SDRAMConfigVariables_t * outVariables , oC_FMC_LLD_Result_t * outResult )
{
    oC_ErrorCode_t errorCode                        = oC_ErrorCode_ImplementError;
    oC_Frequency_t currentFrequency                 = oC_CLOCK_LLD_GetClockFrequency();

    outResult->DataBusWidth             = (Config->DataBusWidth == 0) ? ChipInfo->SDRAM.DataBusWidth : Config->DataBusWidth;
    outResult->DirectAccessProtection   = Config->Protection != oC_FMC_LLD_Protection_Default ? Config->Protection :
                                                                                                oC_FMC_LLD_Protection_AllowRead |
                                                                                                oC_FMC_LLD_Protection_AllowWrite;
    outVariables->SDCR.SDCLK            = ((currentFrequency/2) <= ChipInfo->SDRAM.MaximumClockFrequency) ? 2 :
                                          ((currentFrequency/3) <= ChipInfo->SDRAM.MaximumClockFrequency) ? 3 : 0;

    if(
        ErrorCondition( IsDataBusWidthCorrect  ( outResult->DataBusWidth)                         , oC_ErrorCode_DataBusWidthNotCorrect       )
     && ErrorCondition( IsDataBusWidthSupported( outResult->DataBusWidth)                         , oC_ErrorCode_DataBusWidthNotSupported     )
     && ErrorCondition( IsDataBusWidthCorrect  ( ChipInfo->SDRAM.DataBusWidth)                    , oC_ErrorCode_DataBusWidthNotCorrect       )
     && ErrorCondition( ChipInfo->SDRAM.Size > 0                                                  , oC_ErrorCode_SizeNotCorrect               )
     && ErrorCondition( ChipInfo->SDRAM.NumberOfRowAddressBits    >= 11                           , oC_ErrorCode_RowBitsNumberNotSupported    )
     && ErrorCondition( ChipInfo->SDRAM.NumberOfRowAddressBits    <= 13                           , oC_ErrorCode_RowBitsNumberNotSupported    )
     && ErrorCondition( ChipInfo->SDRAM.NumberOfColumnAddressBits >=  8                           , oC_ErrorCode_ColumnBitsNumberNotSupported )
     && ErrorCondition( ChipInfo->SDRAM.NumberOfColumnAddressBits <= 11                           , oC_ErrorCode_ColumnBitsNumberNotSupported )
     && ErrorCondition( ChipInfo->SDRAM.NumberOfBanks == 4 || ChipInfo->SDRAM.NumberOfBanks == 2  , oC_ErrorCode_NumberOfBanksNotSupported    )
     && ErrorCondition( IsProtectionCorrect(outResult->DirectAccessProtection)                    , oC_ErrorCode_ProtectionNotCorrect         )
     && ErrorCondition( currentFrequency > 0                                                      , oC_ErrorCode_ClockConfigurationError      )
     && ErrorCondition( ChipInfo->SDRAM.MaximumClockFrequency > 0                                 , oC_ErrorCode_WrongFrequency               )
     && ErrorCondition( ChipInfo->SDRAM.CasLatency > 0                                            , oC_ErrorCode_CasLatencyNotCorrect         )
     && ErrorCondition( outVariables->SDCR.SDCLK > 0                                              , oC_ErrorCode_FrequencyNotPossible         )
        )
    {
        oC_Time_t memoryClockPeriod                = 0;
        oC_Time_t mainClockPeriod                  = oC_Frequency_ToTime(currentFrequency);
        uint32_t  activeToReadWriteDelayInCycles   = 0;
        uint32_t  prechargeDelayInCycles           = 0;
        uint32_t  writeRecoveryDelayInCycles       = 0;
        uint32_t  refreshToActivateDelayInCycles   = 0;
        uint32_t  minimumSelfRefreshPeriodInCycles = 0;
        uint32_t  exitSelfRefreshDelayInCycles     = 0;


        /* If this assertion fails, then the function for verification data bus width does not work correctly (the 0 value is not allowed) */
        oC_ASSERT( outResult->DataBusWidth != 0 );

        outResult->ConfiguredFrequency  = currentFrequency / ((oC_Frequency_t)outVariables->SDCR.SDCLK);

        /* Memory clock period is required for counting most of parameters */
        memoryClockPeriod      = oC_Frequency_ToTime(outResult->ConfiguredFrequency);

        /* Size of the used memory can be different than chip size if we do not use all DATA pins */
        outResult->MemorySize  = ChipInfo->SDRAM.Size;
        outResult->MemorySize /= ChipInfo->SDRAM.DataBusWidth / outResult->DataBusWidth;


        /* Prepare SDCR register value */
        outVariables->SDCR.NC       = ChipInfo->SDRAM.NumberOfColumnAddressBits -  8;
        outVariables->SDCR.NR       = ChipInfo->SDRAM.NumberOfRowAddressBits    - 11;
        outVariables->SDCR.NB       = ChipInfo->SDRAM.NumberOfBanks == 4 ? 1 : 0;
        outVariables->SDCR.MWID     = outResult->DataBusWidth == oC_FMC_LLD_DataBusWidth_8Bits  ? 0 :
                                      outResult->DataBusWidth == oC_FMC_LLD_DataBusWidth_16Bits ? 1 :
                                      outResult->DataBusWidth == oC_FMC_LLD_DataBusWidth_32Bits ? 2 : 3;
        outVariables->SDCR.CAS      = ( memoryClockPeriod * 2 ) >= ChipInfo->SDRAM.CasLatency ? 1 :
                                      ( memoryClockPeriod * 3 ) >= ChipInfo->SDRAM.CasLatency ? 2 :
                                      ( memoryClockPeriod * 4 ) >= ChipInfo->SDRAM.CasLatency ? 3 : 0;
        outVariables->SDCR.WP       = outResult->DirectAccessProtection & oC_FMC_LLD_Protection_AllowWrite ? 0 : 1;
        outVariables->SDCR.RBURST   = ChipInfo->SDRAM.Advanced.UseBurstRead ? 1 : 0;
        outVariables->SDCR.RPIPE    = ( mainClockPeriod * 0 ) >= ChipInfo->SDRAM.Advanced.ReadPipeDelay ? 0 :
                                      ( mainClockPeriod * 2 ) >= ChipInfo->SDRAM.Advanced.ReadPipeDelay ? 1 :
                                      ( mainClockPeriod * 3 ) >= ChipInfo->SDRAM.Advanced.ReadPipeDelay ? 2 : 3;

        CountSDRAMRequiredPins(ChipInfo,outResult);


        if(
            ErrorCondition( outResult->MemorySize >  0               , oC_ErrorCode_SizeNotCorrect              )
         && ErrorCondition( outResult->MemorySize <= MAX_SDRAM_SIZE  , oC_ErrorCode_SizeTooBig                  )
         && ErrorCondition( outVariables->SDCR.CAS > 0               , oC_ErrorCode_CasLatencyNotSupported      )
         && ErrorCondition( outVariables->SDCR.RPIPE < 3             , oC_ErrorCode_ReadPipeDelayNotPossible    )
         && ErrorCondition( outVariables->SDCR.MWID  < 3             , oC_ErrorCode_DataBusWidthNotCorrect      )

         && ErrorCondition( ChipInfo->SDRAM.CyclesToDelayAfterLoadMode >= 1 && ChipInfo->SDRAM.CyclesToDelayAfterLoadMode <= 16                                    , oC_ErrorCode_DelayNotPossible )
         && ErrorCondition( SetTimeInCycles(&activeToReadWriteDelayInCycles     , ChipInfo->SDRAM.ActiveToReadWriteDelay   , outResult->ConfiguredFrequency,1,16,1), oC_ErrorCode_DelayNotPossible )
         && ErrorCondition( SetTimeInCycles(&prechargeDelayInCycles             , ChipInfo->SDRAM.PrechargeDelay           , outResult->ConfiguredFrequency,1,16,1), oC_ErrorCode_DelayNotPossible )
         && ErrorCondition( SetTimeInCycles(&writeRecoveryDelayInCycles         , ChipInfo->SDRAM.WriteRecoveryDelay       , outResult->ConfiguredFrequency,1,16,1), oC_ErrorCode_DelayNotPossible )
         && ErrorCondition( SetTimeInCycles(&refreshToActivateDelayInCycles     , ChipInfo->SDRAM.RefreshToActivateDelay   , outResult->ConfiguredFrequency,1,16,1), oC_ErrorCode_DelayNotPossible )
         && ErrorCondition( SetTimeInCycles(&minimumSelfRefreshPeriodInCycles   , ChipInfo->SDRAM.MinimumSelfRefreshPeriod , outResult->ConfiguredFrequency,1,16,1), oC_ErrorCode_DelayNotPossible )
         && ErrorCondition( SetTimeInCycles(&exitSelfRefreshDelayInCycles       , ChipInfo->SDRAM.ExitSelfRefreshDelay     , outResult->ConfiguredFrequency,1,16,1), oC_ErrorCode_DelayNotPossible )

         && ErrorCondition( FindFreeBanks( ChipInfo->MemoryType,
                                           outResult->MemorySize,
                                           (Bank_t*)&outResult->ConfiguredBanks,
                                           &outResult->MemoryStart)  , oC_ErrorCode_NoFreeBankAvailable         )
            )
        {
            outVariables->SDTR.TRCD         = activeToReadWriteDelayInCycles   - 1;
            outVariables->SDTR.TRP          = prechargeDelayInCycles           - 1;
            outVariables->SDTR.TWR          = writeRecoveryDelayInCycles       - 1;
            outVariables->SDTR.TRC          = refreshToActivateDelayInCycles   - 1;
            outVariables->SDTR.TRAS         = minimumSelfRefreshPeriodInCycles - 1;
            outVariables->SDTR.TXSR         = exitSelfRefreshDelayInCycles     - 1;
            outVariables->SDTR.TMRD         = ChipInfo->SDRAM.CyclesToDelayAfterLoadMode - 1;

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief counts pins required by the SDRAM configuration
 */
//==========================================================================================================================================
static void CountSDRAMRequiredPins( const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult )
{
    uint32_t numberOfDataBits = outResult->DataBusWidth * 8;

    oC_ASSERT( IsDataBusWidthSupported(outResult->DataBusWidth) );
    oC_ASSERT( ChipInfo->SDRAM.NumberOfRowAddressBits >= ChipInfo->SDRAM.NumberOfColumnAddressBits );

    outResult->SDCLK    = oC_FMC_LLD_PinUsage_Required;
    outResult->SDNWE    = oC_FMC_LLD_PinUsage_Required;
    outResult->NCAS     = oC_FMC_LLD_PinUsage_Required;
    outResult->NRAS     = oC_FMC_LLD_PinUsage_Required;

    for(uint8_t bitIndex = 0; bitIndex < ChipInfo->SDRAM.NumberOfRowAddressBits; bitIndex++)
    {
        outResult->A[bitIndex] = oC_FMC_LLD_PinUsage_Required;
    }

    for(uint8_t bitIndex = 0; bitIndex < numberOfDataBits ; bitIndex++)
    {
        outResult->D[bitIndex] = oC_FMC_LLD_PinUsage_Required;
    }

    outResult->BA[0] = oC_FMC_LLD_PinUsage_Required;
    outResult->BA[1] = ChipInfo->SDRAM.NumberOfBanks == 4 ? oC_FMC_LLD_PinUsage_Required : oC_FMC_LLD_PinUsage_NotUsed;

    outResult->NBL[0] = oC_FMC_LLD_PinUsage_Optional;
    outResult->NBL[1] = oC_FMC_LLD_PinUsage_Optional;
    outResult->NBL[2] = oC_FMC_LLD_PinUsage_Optional;
    outResult->NBL[3] = oC_FMC_LLD_PinUsage_Optional;

    outResult->SDCKE[0] = oC_FMC_LLD_PinUsage_Optional;
    outResult->SDCKE[1] = oC_FMC_LLD_PinUsage_Optional;

    outResult->SDNE[0] = oC_FMC_LLD_PinUsage_Required;
    outResult->SDNE[1] = oC_FMC_LLD_PinUsage_Optional;
}

//==========================================================================================================================================
/**
 * @brief confiures SDRAM
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureSDRAM(const oC_FMC_LLD_SDRAM_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , SDRAMConfigVariables_t * outVariables , oC_FMC_LLD_Result_t * outResult )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( outVariables->SDCR.SDCLK == 2 || outVariables->SDCR.SDCLK        , oC_ErrorCode_MachineSpecificValueNotCorrect )
     && ErrorCondition( outResult->ConfiguredBanks & ( Bank_MainBank5 | Bank_MainBank6)  , oC_ErrorCode_MachineSpecificValueNotCorrect )
     && ErrorCondition( oC_Machine_SetPowerStateForChannel(FMC_Channel,oC_Power_On)      , oC_ErrorCode_CannotEnableChannel            )
        )
    {
        if(outResult->ConfiguredBanks & Bank_MainBank5)
        {
            FMC_SDCR1->Value = outVariables->SDCR.Value;
            FMC_SDTR1->Value = outVariables->SDTR.Value;

            oC_ASSERT( FMC_SDCR1->Value == outVariables->SDCR.Value );
            oC_ASSERT( FMC_SDTR1->Value == outVariables->SDTR.Value );
        }
        if(outResult->ConfiguredBanks & Bank_MainBank6)
        {
            FMC_SDCR2->Value = outVariables->SDCR.Value;
            FMC_SDTR2->Value = outVariables->SDTR.Value;

            oC_ASSERT( FMC_SDCR2->Value == outVariables->SDCR.Value );
            oC_ASSERT( FMC_SDTR2->Value == outVariables->SDTR.Value );
        }
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

