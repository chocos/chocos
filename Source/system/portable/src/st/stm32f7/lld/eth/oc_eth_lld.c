/** ****************************************************************************************************************************************
 *
 * @file       oc_eth_lld.c
 *
 * @brief      The file with source for ETH LLD functions
 *
 * @file       oc_eth_lld.c
 *
 * @author     Patryk Kubiak - (Created on: 2016-07-28 - 21:37:41)
 *
 * @copyright  Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_eth_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <oc_channels.h>
#include <oc_ba.h>
#include <oc_gpio_lld.h>
#include <oc_gpio_mslld.h>
#include <oc_clock_lld.h>
#include <string.h>
#include <oc_registers.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define DESCRIPTOR_LIST_SIZE                5
#define TIMEOUT                             ms(200)
#define MAX_PHY_ADDRESS                     0x1F
#define MAX_PHY_REGISTER_ADDRESS            0x1F
#define MAX_PHY_REGISTER_DATA_VALUE         0xFFFF
#define ETH_Channel                         oC_Channel_ETH
#define IsRam(Address)                      (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define IsDma(Address)                      oC_LSF_IsDmaAddress(Address)
#define ETH_MACCR                           oC_Register(ETHERNET_MAC,ETH_MACCR)
#define ETH_MACFFR                          oC_Register(ETHERNET_MAC,ETH_MACFFR)
#define ETH_MACHTHR                         oC_Register(ETHERNET_MAC,ETH_MACHTHR)
#define ETH_MACHTLR                         oC_Register(ETHERNET_MAC,ETH_MACHTLR)
#define ETH_MACMIIAR                        oC_Register(ETHERNET_MAC,ETH_MACMIIAR)
#define ETH_MACMIIDR                        oC_Register(ETHERNET_MAC,ETH_MACMIIDR)
#define ETH_MACFCR                          oC_Register(ETHERNET_MAC,ETH_MACFCR)
#define ETH_MACVLANTR                       oC_Register(ETHERNET_MAC,ETH_MACVLANTR)
#define ETH_MACRWUFFR                       oC_Register(ETHERNET_MAC,ETH_MACRWUFFR)
#define ETH_MACPMTCSR                       oC_Register(ETHERNET_MAC,ETH_MACPMTCSR)
#define ETH_MACDBGR                         oC_Register(ETHERNET_MAC,ETH_MACDBGR)
#define ETH_MACSR                           oC_Register(ETHERNET_MAC,ETH_MACSR)
#define ETH_MACIMR                          oC_Register(ETHERNET_MAC,ETH_MACIMR)
#define ETH_MACA0HR                         oC_Register(ETHERNET_MAC,ETH_MACA0HR)
#define ETH_MACA0LR                         oC_Register(ETHERNET_MAC,ETH_MACA0LR)
#define ETH_MACA1HR                         oC_Register(ETHERNET_MAC,ETH_MACA1HR)
#define ETH_MACA1LR                         oC_Register(ETHERNET_MAC,ETH_MACA1LR)
#define ETH_MACA2HR                         oC_Register(ETHERNET_MAC,ETH_MACA2HR)
#define ETH_MACA2LR                         oC_Register(ETHERNET_MAC,ETH_MACA2LR)
#define ETH_MACA3HR                         oC_Register(ETHERNET_MAC,ETH_MACA3HR)
#define ETH_MACA3LR                         oC_Register(ETHERNET_MAC,ETH_MACA3LR)
#define ETH_MMCCR                           oC_Register(ETHERNET_MAC,ETH_MMCCR)
#define ETH_MMCRIR                          oC_Register(ETHERNET_MAC,ETH_MMCRIR)
#define ETH_MMCTIR                          oC_Register(ETHERNET_MAC,ETH_MMCTIR)
#define ETH_MMCRIMR                         oC_Register(ETHERNET_MAC,ETH_MMCRIMR    )
#define ETH_MMCTIMR                         oC_Register(ETHERNET_MAC,ETH_MMCTIMR    )
#define ETH_MMCTGFSCCR                      oC_Register(ETHERNET_MAC,ETH_MMCTGFSCCR )
#define ETH_MMCTGFMSCCR                     oC_Register(ETHERNET_MAC,ETH_MMCTGFMSCCR)
#define ETH_MMCTGFCR                        oC_Register(ETHERNET_MAC,ETH_MMCTGFCR   )
#define ETH_MMCRFCECR                       oC_Register(ETHERNET_MAC,ETH_MMCRFCECR  )
#define ETH_MMCRFAECR                       oC_Register(ETHERNET_MAC,ETH_MMCRFAECR  )
#define ETH_MMCRGUFCR                       oC_Register(ETHERNET_MAC,ETH_MMCRGUFCR  )
#define ETH_PTPTSCR                         oC_Register(ETHERNET_MAC,ETH_PTPTSCR    )
#define ETH_PTPSSIR                         oC_Register(ETHERNET_MAC,ETH_PTPSSIR    )
#define ETH_PTPTSHR                         oC_Register(ETHERNET_MAC,ETH_PTPTSHR    )
#define ETH_PTPTSLR                         oC_Register(ETHERNET_MAC,ETH_PTPTSLR    )
#define ETH_PTPTSHUR                        oC_Register(ETHERNET_MAC,ETH_PTPTSHUR   )
#define ETH_PTPTSLIR                        oC_Register(ETHERNET_MAC,ETH_PTPTSLIR   )
#define ETH_PTPTSAR                         oC_Register(ETHERNET_MAC,ETH_PTPTSAR    )
#define ETH_PTPTTHR                         oC_Register(ETHERNET_MAC,ETH_PTPTTHR    )
#define ETH_PTPTTLR                         oC_Register(ETHERNET_MAC,ETH_PTPTTLR    )
#define ETH_PTPTSSR                         oC_Register(ETHERNET_MAC,ETH_PTPTSSR    )
#define ETH_DMABMR                          oC_Register(ETHERNET_MAC,ETH_DMABMR     )
#define ETH_DMATPDR                         oC_Register(ETHERNET_MAC,ETH_DMATPDR    )
#define ETH_DMARPDR                         oC_Register(ETHERNET_MAC,ETH_DMARPDR    )
#define ETH_DMARDLAR                        oC_Register(ETHERNET_MAC,ETH_DMARDLAR   )
#define ETH_DMATDLAR                        oC_Register(ETHERNET_MAC,ETH_DMATDLAR   )
#define ETH_DMASR                           oC_Register(ETHERNET_MAC,ETH_DMASR      )
#define ETH_DMAOMR                          oC_Register(ETHERNET_MAC,ETH_DMAOMR     )
#define ETH_DMAIER                          oC_Register(ETHERNET_MAC,ETH_DMAIER     )
#define ETH_DMAMFBOCR                       oC_Register(ETHERNET_MAC,ETH_DMAMFBOCR  )
#define ETH_DMARSWTR                        oC_Register(ETHERNET_MAC,ETH_DMARSWTR   )
#define ETH_DMACHTDR                        oC_Register(ETHERNET_MAC,ETH_DMACHTDR   )
#define ETH_DMACHRDR                        oC_Register(ETHERNET_MAC,ETH_DMACHRDR   )
#define ETH_DMACHTBAR                       oC_Register(ETHERNET_MAC,ETH_DMACHTBAR  )
#define ETH_DMACHRBAR                       oC_Register(ETHERNET_MAC,ETH_DMACHRBAR  )
#define SYSCFG_PMC                          oC_Register(SYSCFG,SYSCFG_PMC)
#define RCC_AHB1ENR                         oC_Register(RCC,RCC_AHB1ENR)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_Frequency_t  Min;
    oC_Frequency_t  Max;
} ClockRange_t;

typedef enum
{
    ReceiveProcessState_Stopped                                 = 0 ,
    ReceiveProcessState_FetchingReceiveTransferDescriptor       = 0x01 ,
    ReceiveProcessState_WaitingForReceivePacket                 = 0x03 ,
    ReceiveProcessState_SuspendedReceiveDescriptorUnavailable   = 0x04 ,
    ReceiveProcessState_ClosingReceiveDescriptor                = 0x05 ,
    ReceiveProcessState_TransferingReceivedPacketToHostMemory   = 0x07 ,
} ReceiveProcessState_t;

typedef enum
{
    TransmitProcessState_Stopped                                = 0 ,
    TransmitProcessState_FetchingTransmitTransferDescriptor     = 0x01 ,
    TransmitProcessState_WaitingForStatus                       = 0x02 ,
    TransmitProcessState_ReadingDataFromHostToFifo              = 0x03 ,
    TransmitProcessState_SuspendedTransmitDescriptorUnavailable = 0x06 ,
    TransmitProcessState_ClosingTransmitDescriptor              = 0x07 ,
} TransmitProcessState_t;

typedef struct
{
    uint8_t                 TransmissionFinished:1;
    uint8_t                 TransmissionStopped:1;
    uint8_t                 TransmitBufferUnavailable:1;
    uint8_t                 TransmitJabberTimeout:1;
    uint8_t                 ReceiveOverflow:1;
    uint8_t                 TransmitUnderflow:1;
    uint8_t                 ReceiveFinished:1;
    uint8_t                 ReceiveBufferUnavailable:1;
    uint8_t                 ReceiveStopped:1;
    uint8_t                 ReceiveWatchdogTimeout:1;
    uint8_t                 DataTransferredToFifo:1;
    uint8_t                 Reserved:2;
    uint8_t                 FatalBusError:1;
    uint8_t                 FirstDataReceived:1;
    uint8_t                 AbnormalInterruptSummary:1;
    uint8_t                 NormalInterruptSummary:1;
    ReceiveProcessState_t   ReceiveProcessState:3;
    TransmitProcessState_t  TransmitProcessState:3;
    uint8_t                 ErrorDuringTransferDataByTx:1;
    uint8_t                 ErrorDuringReadTransfer:1;
    uint8_t                 ErrorDuringDescriptorAccess:1;
    uint8_t                 Reserved2:1;
    uint8_t                 MmcStatus:1;
    uint8_t                 PmtStatus:1;
    uint8_t                 TimeStampTriggerStatus:1;
    uint8_t                 Reserved3:2;
} DmaStatus_t;

struct Descriptor_t
{
    union
    {
        struct
        {
            uint32_t    DB:1;
            uint32_t    UF:1;
            uint32_t    ED:1;
            uint32_t    CC:4;
            uint32_t    VF:1;
            uint32_t    EC:1;
            uint32_t    LCO:1;
            uint32_t    NC:1;
            uint32_t    LCA:1;
            uint32_t    IPE:1;
            uint32_t    FF:1;
            uint32_t    JT:1;
            uint32_t    ES:1;
            uint32_t    IHE:1;
            uint32_t    TTSS:1;
            uint32_t    Reserved:2;
            uint32_t    TCH:1;
            uint32_t    TER:1;
            uint32_t    CIC:2;
            uint32_t    Reserved24:1;
            uint32_t    TTSSE:1;
            uint32_t    DP:1;
            uint32_t    DC:1;
            uint32_t    FS:1;
            uint32_t    LS:1;
            uint32_t    IC:1;
            uint32_t    OWN:1;
        } Tx0;

        struct
        {
            uint32_t    PCEESA:1;
            uint32_t    CE:1;
            uint32_t    DBE:1;
            uint32_t    RE:1;
            uint32_t    RWT:1;
            uint32_t    FT:1;
            uint32_t    LCO:1;
            uint32_t    IPHCETSV:1;
            uint32_t    LS:1;
            uint32_t    FS:1;
            uint32_t    VLAN:1;
            uint32_t    OE:1;
            uint32_t    LE:1;
            uint32_t    SAF:1;
            uint32_t    DE:1;
            uint32_t    ES:1;
            uint32_t    FL:14;
            uint32_t    AFM:1;
            uint32_t    OWN:1;
        } Rx0;
        uint32_t    TDES0;
        uint32_t    RDES0;
    };

    union
    {
        struct
        {
            uint32_t    Buffer1ByteCount:13;
            uint32_t    Reserved1315:3;
            uint32_t    Buffer2ByteCount:13;
            uint32_t    Reserved3129:3;
        } Tx1;
        struct
        {
            uint32_t    RBS:13;
            uint32_t    Reserved1313:1;
            uint32_t    RCH:1;
            uint32_t    RER:1;
            uint32_t    RBS2:13;
            uint32_t    Reserved2930:2;
            uint32_t    DIC:1;
        } Rx1;
        uint32_t    TDES1;
        uint32_t    RDES1;
    };

    union
    {
        oC_ETH_LLD_Frame_t*     Frame;
        uint32_t                TimestampLow;
        uint32_t                TDES2;
    };

    union
    {
        uint32_t                TimestampHigh;
        struct Descriptor_t*    NextDescriptorAddress;
        uint32_t                Buffer2Address;
        uint32_t                TDES3;
    };

    uint32_t    TDES4;
    uint32_t    TDES5;
    uint32_t    TDES6;
    uint32_t    TDES7;
} ;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static oC_ErrorCode_t ConnectPins                       ( const oC_ETH_LLD_Config_t * Config , oC_ETH_LLD_PHY_CommunicationInterface_t CommunicationInterface , oC_ETH_LLD_Result_t * outResult );
static oC_ErrorCode_t DisconnectPins                    ( oC_Pin_t * ConnectedPins , uint16_t ArraySize );
static oC_ErrorCode_t ConnectPin                        ( oC_Pin_t Pin , oC_PinFunction_t PinFunction , oC_Pin_t * outConnectedPinReference );
static oC_ErrorCode_t DisconnectPin                     ( oC_Pin_t Pin );
static oC_ErrorCode_t EnableClock                       ( oC_ETH_LLD_PHY_CommunicationInterface_t CommunicationInterface );
static oC_ErrorCode_t DisableClock                      ( void );
static oC_ErrorCode_t CheckPhyCommunicationInterface    ( oC_ETH_LLD_PHY_CommunicationInterface_t CommunicationInterface );

static oC_ErrorCode_t PerformMacRegisterAccessTest      ( oC_Diag_t * Diag , void * Context );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_ETH_LLD_PHY_CommunicationInterface_t ConfiguredPhyCommunicationInterface = oC_ETH_LLD_PHY_CommunicationInterface_None;
static uint32_t                                NumberOfConfiguredPhys              = 0;
static oC_ETH_LLD_Descriptor_t                 TxDescriptor                        = NULL;
static oC_ETH_LLD_Descriptor_t                 RxDescriptor                        = NULL;
static oC_ETH_LLD_InterruptFunction_t          InterruptHandler                    = NULL;
static DmaStatus_t*                            DmaStatus                           = (DmaStatus_t*)&ETH_DMASR->Value;

static const oC_Diag_SupportedDiagData_t       SupportedDiagsArray[]               = {
                { .Name = "MAC register access test" , .PerformFunction = PerformMacRegisterAccessTest } ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_ETH_LLD))
    {
        oC_Module_TurnOn(oC_Module_ETH_LLD);

        ConfiguredPhyCommunicationInterface = oC_ETH_LLD_PHY_CommunicationInterface_None;
        NumberOfConfiguredPhys              = 0;
        TxDescriptor                        = NULL;
        RxDescriptor                        = NULL;
        InterruptHandler                    = NULL;
        errorCode                           = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH_LLD))
    {
        oC_Module_TurnOff(oC_Module_ETH_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_SetInterruptHandler( oC_ETH_LLD_InterruptFunction_t Function )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Function) || IsRom(Function) , oC_ErrorCode_WrongAddress               )
         && ErrorCondition( InterruptHandler == NULL           , oC_ErrorCode_InterruptHandlerAlreadySet )
            )
        {
            InterruptHandler = Function;
            errorCode        = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_ETH_LLD_IsAutoPadGenerationSupported( void )
{
    return true;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_ETH_LLD_IsAutoCalculateCrcSupported( void )
{
    return true;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_InitializeMac( const oC_ETH_LLD_Config_t * Config , const oC_ETH_LLD_PHY_ChipInfo_t * ChipInfo , oC_ETH_LLD_Result_t * outResult )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Config)   || IsRom(Config)   , oC_ErrorCode_WrongConfigAddress     )
         && ErrorCondition( IsRam(ChipInfo) || IsRom(ChipInfo) , oC_ErrorCode_WrongAddress           )
         && ErrorCondition( IsRam(outResult)                   , oC_ErrorCode_OutputAddressNotInRAM  )
         && ErrorCode( CheckPhyCommunicationInterface( ChipInfo->CommunicationInterface              ))
         && ErrorCode( ConnectPins( Config, ChipInfo->CommunicationInterface, outResult              ))
         && ErrorCode( EnableClock( ChipInfo->CommunicationInterface                                 ))
            )
        {
            outResult->RxData.Alignment         = 16;
            outResult->RxData.DescriptorSize    = sizeof(struct Descriptor_t) + sizeof(oC_ETH_LLD_Frame_t);
            outResult->RxData.RingSize          = DESCRIPTOR_LIST_SIZE;
            outResult->TxData.Alignment         = 16;
            outResult->TxData.DescriptorSize    = sizeof(struct Descriptor_t) + sizeof(oC_ETH_LLD_Frame_t);
            outResult->TxData.RingSize          = DESCRIPTOR_LIST_SIZE;
            outResult->NumberOfMacAddresses     = 4;
            ConfiguredPhyCommunicationInterface = ChipInfo->CommunicationInterface;

            //==============================================================================================================================
            /*                                                                                                                            *
             *                                     MAC Initialization in registers                                                        *
             *                                                                                                                            */
            //==============================================================================================================================
            oC_RegisterType_ETH_MACCR_t maccr   = { .Value = ETH_MACCR->Value };

            maccr.WD    = 0; /* Watchdog enabled */
            maccr.JD    = 0; /* Jabber enabled */
            maccr.IFG   = 0; /* Interframe gap 96 bit times */
            maccr.CSD   = 0; /* Carrier Sense enabled */
            maccr.FES   = Config->BaudRate == oC_BaudRate_MBd(100) ? 1 : 0; /* Fast Ethernet Speed */
            maccr.ROD   = 0; /* Receive own disabled */
            maccr.LM    = 0; /* Loopback mode disabled */
            maccr.DM    = Config->OperationMode == oC_ETH_LLD_OperationMode_FullDuplex ? 1 : 0; /* Duplex Mode */
            maccr.IPCO  = 0; /* IPv4 checksum offload (0 disabled - 1 enabled) */
            maccr.RD    = 0; /* Retry disabled */
            maccr.APCS  = 0; /* Automatic pad/CRC stripping - pass all incoming frames unmodified */
            maccr.BL    = 0; /* Back-off limit - 10 */
            maccr.DC    = 0; /* Deferral check disabled */

            ETH_MACCR->Value = maccr.Value;

            /* Wait until the operation is complete and write it again */
            maccr.Value      = ETH_MACCR->Value;
            oC_MCS_Delay(50);
            ETH_MACCR->Value = maccr.Value;

            /* MACFFR configuration */
            oC_RegisterType_ETH_MACFFR_t macffr = { .Value = ETH_MACFFR->Value };

            macffr.RA   = 0;    /* Receive all - also frames that are not for us (not for our MAC address) (0-disabled, 1 - enabled)*/
            macffr.SAF  = 0;    /* Source address filter disabled */
            macffr.PCF  = 0x1;  /* Prevent control frames */
            macffr.BFD  = 0;    /* Broadcast frames enabled */
            macffr.DAIF = 0;    /* Destination address filter not inverted */
            macffr.PM   = 0;    /* Promiscuous mode - receive all frames regardless of their destination or source address */
            macffr.PAM  = 1;    /* All multicast frames passed */

            /* Wait until the operation is complete and write it again */
            macffr.Value      = ETH_MACFFR->Value;
            oC_MCS_Delay(50);
            ETH_MACFFR->Value = macffr.Value;

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_ReleaseMac( const oC_ETH_LLD_Config_t * Config , const oC_ETH_LLD_PHY_ChipInfo_t * ChipInfo , oC_ETH_LLD_Result_t * Result )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Config) || IsRom(Config) , oC_ErrorCode_WrongConfigAddress            )
         && ErrorCondition( IsRam(ChipInfo)                , oC_ErrorCode_WrongAddress                  )
         && ErrorCondition( IsRam(Result)                  , oC_ErrorCode_WrongAddress                  )
         && oC_AssignErrorCode( &errorCode , DisconnectPins(Result->ConnectedPins, oC_ARRAY_SIZE(Result->ConnectedPins) )   )
         && oC_AssignErrorCode( &errorCode , DisableClock()                                                                 )
            )
        {
            memset(Result,0,sizeof(oC_ETH_LLD_Result_t));

            TxDescriptor = NULL;
            RxDescriptor = NULL;

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_ReadPhyRegister( oC_ETH_LLD_PHY_Address_t PhyAddress , oC_ETH_LLD_PHY_RegisterAddress_t RegisterAddress , uint32_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( oC_Machine_IsChannelPoweredOn(ETH_Channel)      , oC_ErrorCode_MacNotInitialized            )
         && ErrorCondition( PhyAddress      <= MAX_PHY_ADDRESS              , oC_ErrorCode_PhyAddressNotCorrect         )
         && ErrorCondition( RegisterAddress <= MAX_PHY_REGISTER_ADDRESS     , oC_ErrorCode_RegisterAddressNotCorrect    )
         && ErrorCondition( IsRam(outValue)                                 , oC_ErrorCode_OutputAddressNotInRAM        )
            )
        {
            oC_MCS_EnterCriticalSection();

            oC_RegisterType_ETH_MACMIIAR_t macMiiAr = { .Value = ETH_MACMIIAR->Value };
            oC_Time_t                      time     = 0;

            macMiiAr.MB = 1;
            macMiiAr.MW = 0;
            macMiiAr.MR = RegisterAddress;
            macMiiAr.PA = PhyAddress;

            ETH_MACMIIAR->Value = macMiiAr.Value;

            while(ETH_MACMIIAR->MB == 1 && time < TIMEOUT)
            {
                time += ms(1);
                oC_CLOCK_LLD_DelayForMicroseconds(1000);
            }

            if(ETH_MACMIIAR->MB == 0)
            {
                *outValue = ETH_MACMIIDR->Value;
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_Timeout;
            }

            oC_MCS_ExitCriticalSection();

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_WritePhyRegister( oC_ETH_LLD_PHY_Address_t PhyAddress , oC_ETH_LLD_PHY_RegisterAddress_t RegisterAddress , uint32_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( oC_Machine_IsChannelPoweredOn(ETH_Channel)      , oC_ErrorCode_MacNotInitialized            )
         && ErrorCondition( PhyAddress      <= MAX_PHY_ADDRESS              , oC_ErrorCode_PhyAddressNotCorrect         )
         && ErrorCondition( RegisterAddress <= MAX_PHY_REGISTER_ADDRESS     , oC_ErrorCode_RegisterAddressNotCorrect    )
         && ErrorCondition( Value <= MAX_PHY_REGISTER_DATA_VALUE            , oC_ErrorCode_ValueTooBig                  )
            )
        {
            oC_MCS_EnterCriticalSection();

            oC_RegisterType_ETH_MACMIIAR_t macMiiAr = { .Value = ETH_MACMIIAR->Value };
            oC_Time_t                      time     = 0;

            macMiiAr.MB = 1;
            macMiiAr.MW = 1;
            macMiiAr.MR = RegisterAddress;
            macMiiAr.PA = PhyAddress;

            ETH_MACMIIDR->Value = Value;
            ETH_MACMIIAR->Value = macMiiAr.Value;

            while(ETH_MACMIIAR->MB == 1 && time < TIMEOUT)
            {
                time += ms(1);
                oC_CLOCK_LLD_DelayForMicroseconds(1000);
            }

            if(ETH_MACMIIAR->MB == 0)
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_Timeout;
            }

            oC_MCS_ExitCriticalSection();

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_SetMacLoopback( bool Enabled )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH_LLD))
    {
        if(ErrorCondition( oC_Machine_IsChannelPoweredOn(ETH_Channel) , oC_ErrorCode_MacNotInitialized ))
        {
            ETH_MACCR->LM   = Enabled ? 1 : 0;
            errorCode       = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_InitializeDescriptors( oC_ETH_LLD_Result_t * Result , const oC_ETH_LLD_Config_t * Config , oC_ETH_LLD_Descriptor_t Tx , oC_ETH_LLD_Descriptor_t Rx )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Config) || IsRom(Config)      , oC_ErrorCode_WrongConfigAddress       )
         && ErrorCondition( IsDma(Tx)                           , oC_ErrorCode_NotDmaAddress            )
         && ErrorCondition( IsDma(Rx)                           , oC_ErrorCode_NotDmaAddress            )
         && ErrorCondition( IsRam(Result)                       , oC_ErrorCode_OutputAddressNotInRAM    )
            )
        {
            oC_ETH_LLD_Frame_t* txFramesArray = (oC_ETH_LLD_Frame_t*)&Tx[Result->TxData.RingSize];
            oC_ETH_LLD_Frame_t* rxFramesArray = (oC_ETH_LLD_Frame_t*)&Rx[Result->RxData.RingSize];

            TxDescriptor        = Tx;
            RxDescriptor        = Rx;

            oC_STATIC_ASSERT( (sizeof(struct Descriptor_t) % (sizeof(uint32_t) * 4 )) == 0 , "Size of descriptor is not multiple of 4 words!" );

            /* Setting address of Rx and Tx descriptors list */
            ETH_DMATDLAR->Value  = (uint32_t)TxDescriptor;
            ETH_DMARDLAR->Value  = (uint32_t)RxDescriptor;

            for( uint32_t i = 0 ; i < DESCRIPTOR_LIST_SIZE ; i++ )
            {
                Tx[i].Tx0.OWN = 0;
                Tx[i].Tx0.TCH = 1;
                Rx[i].Rx0.OWN = 1;
                Rx[i].Rx1.RCH = 1;
                Rx[i].Rx1.DIC = 0;

                Tx[i].Tx1.Buffer1ByteCount = 1524;
                Rx[i].Rx1.RBS              = 1524;

                Tx[i].Frame = &txFramesArray[i];
                Rx[i].Frame = &rxFramesArray[i];

                if(i < (DESCRIPTOR_LIST_SIZE - 1))
                {
                    Tx[i].NextDescriptorAddress = &Tx[i+1];
                    Rx[i].NextDescriptorAddress = &Rx[i+1];

                }
                else
                {
                    Tx[i].NextDescriptorAddress = &Tx[0];
                    Rx[i].NextDescriptorAddress = &Rx[0];
                }
            }

            Result->TxData.NextDescriptor   = &Tx[0];
            Result->TxData.SegmentStarted   = false;
            Result->RxData.NextDescriptor   = &Rx[0];
            Result->RxData.SegmentStarted   = false;
            errorCode                       = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_InitializeDma( oC_ETH_LLD_Result_t * Result , const oC_ETH_LLD_Config_t * Config , oC_ETH_LLD_Descriptor_t Tx , oC_ETH_LLD_Descriptor_t Rx )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Result)                       , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( IsRam(Config) || IsRom(Config)      , oC_ErrorCode_WrongAddress          )
         && ErrorCondition( IsDma(Tx) && IsDma(Rx)              , oC_ErrorCode_OutputAddressNotInRAM )
            )
        {
            oC_RegisterType_ETH_DMABMR_t dmabmr     = { .Value = ETH_DMABMR->Value };
            oC_RegisterType_ETH_DMAOMR_t dmaomr     = { .Value = ETH_DMAOMR->Value };

            dmaomr.DTCEFD = 0;  /* Dropping of TCP/IP checksum error frames enable */
            dmaomr.RSF    = 1;  /* Receive store and forward */
            dmaomr.DFRF   = 0;  /* Enable flushing of received frames */
            dmaomr.TSF    = 1;  /* Transmit store and forward enable */
            dmaomr.TTC    = 0;  /* Transmit threshold control - 64Bytes */
            dmaomr.FEF    = 0;  /* Forward Error Frames - disable */
            dmaomr.FUGF   = 0;  /* Forward undersized good frames - disable */
            dmaomr.RTC    = 0;  /* Receive threshold control - 64Bytes */
            dmaomr.OSF    = 1;  /* Operate on second frame to transmit even before of status of the first frame */

            ETH_DMAOMR->Value = dmaomr.Value;

            /* Wait until the operation is complete and write it again */
            dmaomr.Value = ETH_DMAOMR->Value;
            oC_MCS_Delay(50);
            ETH_DMAOMR->Value = dmaomr.Value;

            dmabmr.AAB    = 1;  /* Address-Aligned beats */
            dmabmr.FB     = 1;  /* Fixed burst */
            dmabmr.RDP    = 32; /* Rx DMA PBL - Maximum number of beats to be transferred in one RxDMA transaction */
            dmabmr.PBL    = 32; /* Tx DMA PBL - Maximum number of betas to be transferred in one TxDMA transaction */
            dmabmr.EDFE   = 0;  /* Enhanced descriptor format disabled TODO: make sure, that it can be disabled */
            dmabmr.DSL    = 0;  /* Descriptor skip length - number of words to skip between to unchained descriptors */
            dmabmr.DA     = 0;  /* DMA Arbitration - priority given as ETH_DMABMR->PM */
            dmabmr.PM     = 0;  /* Rx Tx priority ratio - 1:1 */
            dmabmr.USP    = 1;  /* Use separate PBL - enabled. This means, that PBL is separately configured for Rx and Tx in PBL and RDP fields */

            ETH_DMABMR->Value = dmabmr.Value;

            /* Wait until the operation is complete and write it again */
            dmabmr.Value      = ETH_DMABMR->Value;
            oC_MCS_Delay(50);
            ETH_DMABMR->Value = dmabmr.Value;

            /* Enable Rx interrupt */
            ETH_DMAIER->NISE  = 1; /* Normal Interrupt Summary Enabled */
            ETH_DMAIER->RIE   = 1; /* Receive Interrupt Enabled */
            ETH_DMAIER->TIE   = 1; /* Transmit Interrupt Enabled */

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_SendFrame( oC_ETH_LLD_Result_t * Result , const oC_ETH_LLD_MacAddress_t Source, const oC_ETH_LLD_MacAddress_t Destination, const void * Data , uint16_t Size , oC_ETH_LLD_FrameSegment_t FrameSegment , uint16_t EtherType )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Source)      || IsRom(Source)                          , oC_ErrorCode_WrongAddress                         )
         && ErrorCondition( IsRam(Destination) || IsRom(Destination)                     , oC_ErrorCode_WrongAddress                         )
         && ErrorCondition( IsRam(Data)        || IsRom(Data)                            , oC_ErrorCode_WrongAddress                         )
         && ErrorCondition( IsRam(Result)                                                , oC_ErrorCode_OutputAddressNotInRAM                )
         && ErrorCondition( Size > 0                                                     , oC_ErrorCode_SizeNotCorrect                       )
         && ErrorCondition( Size <= oC_ETH_LLD_ETHERNET_MTU                              , oC_ErrorCode_FrameWidthNotSupported               )
         && ErrorCondition( IsDma(TxDescriptor)                                          , oC_ErrorCode_DescriptorListNotInitialized         )
         && ErrorCondition( IsDma(Result->TxData.NextDescriptor)                         , oC_ErrorCode_InternalDataAreDamaged               )
         && ErrorCondition( FrameSegment < oC_ETH_LLD_FrameSegment_NumberOfFrameSegments , oC_ErrorCode_SegmentTypeNotSupported              )
         && ErrorCondition( EtherType > 0                                                , oC_ErrorCode_FrameTypeNotSupported                )
            )
        {
            oC_MCS_EnterCriticalSection();
            oC_ETH_LLD_Descriptor_t descriptor = Result->TxData.NextDescriptor->Tx0.OWN == 0 ? Result->TxData.NextDescriptor : NULL;

            if(
                ErrorCondition( descriptor != NULL                                                                      , oC_ErrorCode_NoFreeSlots  )
             && ErrorCondition( Result->TxData.SegmentStarted == false || FrameSegment > oC_ETH_LLD_FrameSegment_First , oC_ErrorCode_ModuleIsBusy )
                )
            {
                uint8_t *       destination = (uint8_t*)descriptor->Frame->Data;
                const uint8_t * source      = Data;

                /* Cleaning the frame */
                memset( descriptor->Frame , 0 , sizeof(oC_ETH_LLD_Frame_t) );

                if(FrameSegment & oC_ETH_LLD_FrameSegment_First)
                {
                    memcpy( descriptor->Frame->SourceAddress      , Source        , sizeof(oC_ETH_LLD_MacAddress_t));
                    memcpy( descriptor->Frame->DestinationAddress , Destination   , sizeof(oC_ETH_LLD_MacAddress_t));

                    descriptor->Frame->DataLengthEtherType    = EtherType;
                    descriptor->Tx1.Buffer1ByteCount          = Size + 14;

                }
                else
                {
                    descriptor->Tx1.Buffer1ByteCount = Size;
                    destination = (uint8_t*)descriptor->Frame;
                }

                /* This is workaround for a problem with copying data from the external ram to the internal RAM memory.         *
                 * When the 'memcpy' is used for this operation it causes a hard fault. I am not sure why, probably it can be   *
                 * related with memory accessing time. It happens in the receive frame function, but probably it will occur     *
                 * also here. Just in case I'm adding it here also.                                                             */
                for(uint32_t offset = 0 ; offset < Size ; offset ++)
                {
                    destination[offset] = source[offset];
                }

                descriptor->Tx0.CIC = 0x0;

                if(FrameSegment == oC_ETH_LLD_FrameSegment_First)
                {
                    descriptor->Tx0.LS = 0;
                    descriptor->Tx0.FS = 1;
                }
                else if(FrameSegment == oC_ETH_LLD_FrameSegment_Last)
                {
                    descriptor->Tx0.LS = 1;
                    descriptor->Tx0.FS = 0;
                }
                else
                {
                    descriptor->Tx0.LS = 1;
                    descriptor->Tx0.FS = 1;
                }

                descriptor->Tx0.OWN              = 1;

                descriptor->Tx0.DC               = 0;
                descriptor->Tx0.DP               = 0;
                Result->TxData.NextDescriptor    = Result->TxData.NextDescriptor->NextDescriptorAddress;

                /* If the transfer is suspended */
                if(DmaStatus->TransmitProcessState == TransmitProcessState_SuspendedTransmitDescriptorUnavailable)
                {
                    ETH_DMASR->TBUS    = 1;         /* Clear status */
                    ETH_DMATPDR->Value = 0xFFFF;    /* Resume transmission */
                }

                Result->TxData.NextDescriptor   = descriptor->NextDescriptorAddress;
                errorCode                       = oC_ErrorCode_None;
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_ReceiveFrame( oC_ETH_LLD_Result_t * Result , oC_ETH_LLD_MacAddress_t outSource, oC_ETH_LLD_MacAddress_t outDestination, void * Data , uint16_t * Size , oC_ETH_LLD_FrameSegment_t * outFrameSegment , uint16_t * outEtherType )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Result)                                 , oC_ErrorCode_OutputAddressNotInRAM        )
         && ErrorCondition( IsRam(outDestination)                         , oC_ErrorCode_OutputAddressNotInRAM        )
         && ErrorCondition( IsRam(Data)                                   , oC_ErrorCode_OutputAddressNotInRAM        )
         && ErrorCondition( IsRam(Size)                                   , oC_ErrorCode_OutputAddressNotInRAM        )
         && ErrorCondition( (*Size) > 0                                   , oC_ErrorCode_SizeNotCorrect               )
         && ErrorCondition( IsRam(outFrameSegment)                        , oC_ErrorCode_OutputAddressNotInRAM        )
         && ErrorCondition( IsRam(outEtherType)                           , oC_ErrorCode_OutputAddressNotInRAM        )
         && ErrorCondition( IsDma(RxDescriptor)                           , oC_ErrorCode_DescriptorListNotInitialized )
         && ErrorCondition( IsDma(Result->RxData.NextDescriptor)          , oC_ErrorCode_InternalDataAreDamaged       )
         && ErrorCondition( IsRam(Result->RxData.NextDescriptor->Frame) ||
                            IsDma(Result->RxData.NextDescriptor->Frame)   , oC_ErrorCode_InternalDataAreDamaged       )
            )
        {
            oC_ETH_LLD_Descriptor_t descriptor = Result->RxData.NextDescriptor;
            uint16_t                dataSize   = descriptor->Rx0.FL - sizeof(uint32_t);

            oC_MCS_EnterCriticalSection();
            if(
                ErrorCondition( descriptor->Rx0.OWN == 0       , oC_ErrorCode_DataNotAvailable     )
             && ErrorCondition( dataSize  <= (*Size)           , oC_ErrorCode_OutputBufferTooSmall )
                )
            {
                uint8_t *       destination = Data;
                const uint8_t * source      = (const uint8_t*)descriptor->Frame->Data;

                *Size               = dataSize;
                descriptor->Rx0.OWN = 1;
                *outEtherType       = descriptor->Frame->DataLengthEtherType;

                memcpy(outSource        , descriptor->Frame->SourceAddress        , sizeof(oC_ETH_LLD_MacAddress_t) );
                memcpy(outDestination   , descriptor->Frame->DestinationAddress   , sizeof(oC_ETH_LLD_MacAddress_t) );

                /* This is workaround for a problem with copying data from the ram (also internal) to the external RAM memory.  *
                 * When the 'memcpy' is used for this operation it causes a hard fault. I am not sure why, probably it can be   *
                 * related with memory accessing time                                                                           */
                for(uint32_t offset = 0 ; offset < dataSize ; offset ++)
                {
                    destination[offset] = source[offset];
                }

                if(Result->RxData.SegmentStarted == false && descriptor->Rx0.FS == 1 && descriptor->Rx0.LS == 1)
                {
                    *outFrameSegment   = oC_ETH_LLD_FrameSegment_Single;
                    errorCode       = oC_ErrorCode_None;
                }
                else if(Result->RxData.SegmentStarted == false && descriptor->Rx0.FS == 1 && descriptor->Rx0.LS == 0)
                {
                    Result->RxData.SegmentStarted   = true;
                    *outFrameSegment                   = oC_ETH_LLD_FrameSegment_First;
                    errorCode                       = oC_ErrorCode_None;
                }
                else if(Result->RxData.SegmentStarted == true && descriptor->Rx0.FS == 0 && descriptor->Rx0.LS == 1)
                {
                    Result->RxData.SegmentStarted   = false;
                    *outFrameSegment                   = oC_ETH_LLD_FrameSegment_Last;
                    errorCode                       = oC_ErrorCode_None;
                }
                else if(Result->RxData.SegmentStarted == true && descriptor->Rx0.FS == 0 && descriptor->Rx0.LS == 0)
                {
                    *outFrameSegment   = oC_ETH_LLD_FrameSegment_Middle;
                    errorCode       = oC_ErrorCode_None;
                }
                else
                {
                    errorCode       = oC_ErrorCode_InternalDataAreDamaged;
                }

                Result->RxData.NextDescriptor = (void*)descriptor->NextDescriptorAddress;

                /* If the transfer is suspended */
                if(DmaStatus->ReceiveProcessState == ReceiveProcessState_SuspendedReceiveDescriptorUnavailable)
                {
                    ETH_DMASR->RBUS    = 1;         /* Clear status */
                    ETH_DMATPDR->Value = 0xFFFF;    /* Resume transmission */
                }
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_Start( const oC_ETH_LLD_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Config) || IsRom(Config)                                                              , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( Config->BaudRate == oC_BaudRate_MBd(10) || Config->BaudRate == oC_BaudRate_MBd(100)         , oC_ErrorCode_BaudRateNotSupported     )
         && ErrorCondition(    Config->OperationMode == oC_ETH_LLD_OperationMode_FullDuplex
                            || Config->OperationMode == oC_ETH_LLD_OperationMode_HalfDuplex                             , oC_ErrorCode_OperationModeNotCorrect  )
         && ErrorCondition( oC_Channel_EnableInterruptFunction(oC_Channel_ETH, oC_InterruptType_PeripheralInterrupt)    , oC_ErrorCode_CannotEnableInterrupt    )
            )
        {
            oC_RegisterType_ETH_MACCR_t  maccr  = { .Value = ETH_MACCR->Value  };
            oC_RegisterType_ETH_MACFFR_t macffr = { .Value = ETH_MACFFR->Value };
            oC_RegisterType_ETH_DMAOMR_t dmaomr = { .Value = ETH_DMAOMR->Value };

            maccr.TE    = 1;

            ETH_MACCR->Value = maccr.Value;
            maccr.Value      = ETH_MACCR->Value;
            oC_MCS_Delay(50);
            ETH_MACCR->Value = maccr.Value;

            maccr.RE    = 1;

            ETH_MACCR->Value = maccr.Value;
            maccr.Value      = ETH_MACCR->Value;
            oC_MCS_Delay(50);
            ETH_MACCR->Value = maccr.Value;

            dmaomr.FTF  = 1;

            ETH_DMAOMR->Value = dmaomr.Value;
            dmaomr.Value      = ETH_DMAOMR->Value;
            oC_MCS_Delay(50);
            ETH_DMAOMR->Value = dmaomr.Value;

            dmaomr.ST   = 1;

            ETH_DMAOMR->Value   = dmaomr.Value;
            dmaomr.Value      = ETH_DMAOMR->Value;
            oC_MCS_Delay(50);
            ETH_DMAOMR->Value = dmaomr.Value;

            dmaomr.SR   = 1;

            ETH_DMAOMR->Value   = dmaomr.Value;
            dmaomr.Value      = ETH_DMAOMR->Value;
            oC_MCS_Delay(50);
            ETH_DMAOMR->Value = dmaomr.Value;

            macffr.RA   = 1;

            ETH_MACFFR->Value   = macffr.Value;
            macffr.Value      = ETH_MACFFR->Value;
            oC_MCS_Delay(50);
            ETH_MACFFR->Value = macffr.Value;

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_SetMacAddress( oC_ETH_LLD_Result_t * Result , uint32_t MacAddressIndex, const oC_ETH_LLD_MacAddress_t Address )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification( &errorCode , oC_Module_ETH_LLD ))
    {
        if(
            ErrorCondition( IsRam(Result)  || IsRom(Result)              , oC_ErrorCode_WrongAddress         )
         && ErrorCondition( IsRam(Address) || IsRom(Address)             , oC_ErrorCode_WrongAddress         )
         && ErrorCondition( MacAddressIndex >= 0 && MacAddressIndex < 4  , oC_ErrorCode_WrongMacAddressIndex )
            )
        {
            switch(MacAddressIndex)
            {
                case 0:
                    ETH_MACA0LR->MACA0LR   = (((uint32_t)Address[0]) <<  0)
                                           | (((uint32_t)Address[1]) <<  8)
                                           | (((uint32_t)Address[2]) << 16)
                                           | (((uint32_t)Address[3]) << 24);
                    ETH_MACA0HR->MACA0H    = (((uint32_t)Address[4]) <<  0)
                                           | (((uint32_t)Address[5]) <<  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                case 1:
                    ETH_MACA1LR->MACA1LR   = (((uint32_t)Address[0]) <<  0)
                                           | (((uint32_t)Address[1]) <<  8)
                                           | (((uint32_t)Address[2]) << 16)
                                           | (((uint32_t)Address[3]) << 24);
                    ETH_MACA1HR->MACA1H    = (((uint32_t)Address[4]) <<  0)
                                           | (((uint32_t)Address[5]) <<  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                case 2:
                    ETH_MACA2LR->MACA2LR   = (((uint32_t)Address[0]) <<  0)
                                           | (((uint32_t)Address[1]) <<  8)
                                           | (((uint32_t)Address[2]) << 16)
                                           | (((uint32_t)Address[3]) << 24);
                    ETH_MACA2HR->MACA2H    = (((uint32_t)Address[4]) <<  0)
                                           | (((uint32_t)Address[5]) <<  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                case 3:
                    ETH_MACA3LR->MACA3LR   = (((uint32_t)Address[0]) <<  0)
                                           | (((uint32_t)Address[1]) <<  8)
                                           | (((uint32_t)Address[2]) << 16)
                                           | (((uint32_t)Address[3]) << 24);
                    ETH_MACA3HR->MACA3H    = (((uint32_t)Address[4]) <<  0)
                                           | (((uint32_t)Address[5]) <<  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                default:
                    errorCode = oC_ErrorCode_WrongMacAddressIndex;
                    break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_ReadMacAddress( oC_ETH_LLD_Result_t * Result , uint32_t MacAddressIndex, oC_ETH_LLD_MacAddress_t outAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification( &errorCode , oC_Module_ETH_LLD ))
    {
        if(
            ErrorCondition( IsRam(Result)  || IsRom(Result)              , oC_ErrorCode_WrongAddress         )
         && ErrorCondition( IsRam(outAddress)                            , oC_ErrorCode_WrongAddress         )
         && ErrorCondition( MacAddressIndex >= 0 && MacAddressIndex < 4  , oC_ErrorCode_WrongMacAddressIndex )
            )
        {
            switch(MacAddressIndex)
            {
                case 0:
                    outAddress[0] = (uint8_t)(ETH_MACA0LR->MACA0LR >>  0);
                    outAddress[1] = (uint8_t)(ETH_MACA0LR->MACA0LR >>  8);
                    outAddress[2] = (uint8_t)(ETH_MACA0LR->MACA0LR >> 16);
                    outAddress[3] = (uint8_t)(ETH_MACA0LR->MACA0LR >> 24);
                    outAddress[4] = (uint8_t)(ETH_MACA0HR->MACA0H  >>  0);
                    outAddress[5] = (uint8_t)(ETH_MACA0HR->MACA0H  >>  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                case 1:
                    outAddress[0] = (uint8_t)(ETH_MACA1LR->MACA1LR >>  0);
                    outAddress[1] = (uint8_t)(ETH_MACA1LR->MACA1LR >>  8);
                    outAddress[2] = (uint8_t)(ETH_MACA1LR->MACA1LR >> 16);
                    outAddress[3] = (uint8_t)(ETH_MACA1LR->MACA1LR >> 24);
                    outAddress[4] = (uint8_t)(ETH_MACA1HR->MACA1H  >>  0);
                    outAddress[5] = (uint8_t)(ETH_MACA1HR->MACA1H  >>  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                case 2:
                    outAddress[0] = (uint8_t)(ETH_MACA2LR->MACA2LR >>  0);
                    outAddress[1] = (uint8_t)(ETH_MACA2LR->MACA2LR >>  8);
                    outAddress[2] = (uint8_t)(ETH_MACA2LR->MACA2LR >> 16);
                    outAddress[3] = (uint8_t)(ETH_MACA2LR->MACA2LR >> 24);
                    outAddress[4] = (uint8_t)(ETH_MACA2HR->MACA2H  >>  0);
                    outAddress[5] = (uint8_t)(ETH_MACA2HR->MACA2H  >>  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                case 3:
                    outAddress[0] = (uint8_t)(ETH_MACA3LR->MACA3LR >>  0);
                    outAddress[1] = (uint8_t)(ETH_MACA3LR->MACA3LR >>  8);
                    outAddress[2] = (uint8_t)(ETH_MACA3LR->MACA3LR >> 16);
                    outAddress[3] = (uint8_t)(ETH_MACA3LR->MACA3LR >> 24);
                    outAddress[4] = (uint8_t)(ETH_MACA3HR->MACA3H  >>  0);
                    outAddress[5] = (uint8_t)(ETH_MACA3HR->MACA3H  >>  8);
                    errorCode = oC_ErrorCode_None;
                    break;
                default:
                    errorCode = oC_ErrorCode_WrongMacAddressIndex;
                    break;
            }
        }
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_ETH_LLD_IsTransmitQueueFull( oC_ETH_LLD_Result_t * Result )
{
    bool full = false;

    if(oC_Module_IsTurnedOn(oC_Module_ETH_LLD))
    {
        oC_MCS_EnterCriticalSection();
        full =  Result->TxData.NextDescriptor->Tx0.OWN == 1;
        oC_MCS_ExitCriticalSection();

    }

    return full;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_ETH_LLD_IsDataReadyToReceive( oC_ETH_LLD_Result_t * Result )
{
    bool full = false;

    if(oC_Module_IsTurnedOn(oC_Module_ETH_LLD))
    {
        oC_MCS_EnterCriticalSection();
        full = Result->RxData.NextDescriptor->Rx0.OWN == 0;
        oC_MCS_ExitCriticalSection();

    }

    return full;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_LLD_PerformDiagnostics( oC_ETH_LLD_Result_t * Result , oC_Diag_t * Diag , uint32_t * NumberOfDiags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH_LLD))
    {
        if(
            ErrorCondition( IsRam(Result)                                                                                           , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( IsRam(Diag) || Diag == NULL                                                                             , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( IsRam(NumberOfDiags)                                                                                    , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( Diag == NULL || Diag->PrintFunction == NULL || IsRam(Diag->PrintFunction) || IsRom(Diag->PrintFunction) , oC_ErrorCode_WrongAddress          )
            )
        {
            if(Diag != NULL)
            {
                errorCode      = oC_Diag_PerformDiagnostics(SupportedDiagsArray,Diag,*NumberOfDiags,"ETH-LLD",Result);
                *NumberOfDiags = oC_Diag_GetNumberOfSupportedDiagnostics( SupportedDiagsArray );
            }
            else
            {
                *NumberOfDiags = oC_Diag_GetNumberOfSupportedDiagnostics( SupportedDiagsArray );
                errorCode      = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** =======================================s=================================================================================================
 *
 *              The section with sources of local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief connects all pins that are required by the interface
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectPins( const oC_ETH_LLD_Config_t * Config , oC_ETH_LLD_PHY_CommunicationInterface_t CommunicationInterface , oC_ETH_LLD_Result_t * outResult )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    uint32_t       pinIndex  = 0;

    memset(outResult->ConnectedPins,0,sizeof(outResult->ConnectedPins));

    if(CommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_MII)
    {
        if(
            oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.Mdc           , oC_ETH_PinFunction_ETH_MDC          , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.Mdio          , oC_ETH_PinFunction_ETH_MDIO         , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.Col           , oC_ETH_PinFunction_ETH_MII_COL      , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.Crs           , oC_ETH_PinFunction_ETH_MII_CRS      , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.RxClk         , oC_ETH_PinFunction_ETH_MII_RX_CLK   , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.RxD0          , oC_ETH_PinFunction_ETH_MII_RXD0     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.RxD1          , oC_ETH_PinFunction_ETH_MII_RXD1     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.RxD2          , oC_ETH_PinFunction_ETH_MII_RXD2     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.RxD3          , oC_ETH_PinFunction_ETH_MII_RXD3     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.RxDv          , oC_ETH_PinFunction_ETH_MII_RX_DV    , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.RxEr          , oC_ETH_PinFunction_ETH_MII_RX_ER    , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.TxClk         , oC_ETH_PinFunction_ETH_MII_TX_CLK   , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.TxD0          , oC_ETH_PinFunction_ETH_MII_TXD0     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.TxD1          , oC_ETH_PinFunction_ETH_MII_TXD1     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.TxD2          , oC_ETH_PinFunction_ETH_MII_TXD2     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.TxD3          , oC_ETH_PinFunction_ETH_MII_TXD3     , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Mii.TxEn          , oC_ETH_PinFunction_ETH_MII_TX_EN    , &outResult->ConnectedPins[pinIndex++] ) )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            oC_SaveIfErrorOccur("ETH-LLD: Disconnect error - " , DisconnectPins(outResult->ConnectedPins, oC_ARRAY_SIZE(outResult->ConnectedPins)));
        }
    }
    else if(CommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_RMII)
    {
        if(
            oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.Mdc          , oC_ETH_PinFunction_ETH_MDC          , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.Mdio         , oC_ETH_PinFunction_ETH_MDIO         , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.RcsDv        , oC_ETH_PinFunction_ETH_RMII_CRS_DV  , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.RefClk       , oC_ETH_PinFunction_ETH_RMII_REF_CLK , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.RxD0         , oC_ETH_PinFunction_ETH_RMII_RXD0    , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.RxD1         , oC_ETH_PinFunction_ETH_RMII_RXD1    , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.TxD0         , oC_ETH_PinFunction_ETH_RMII_TXD0    , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.TxD1         , oC_ETH_PinFunction_ETH_RMII_TXD1    , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.TxEn         , oC_ETH_PinFunction_ETH_RMII_TX_EN   , &outResult->ConnectedPins[pinIndex++] ) )
         && oC_AssignErrorCode(&errorCode , ConnectPin( Config->Rmii.RxEr         , oC_ETH_PinFunction_ETH_RMII_RX_ER   , &outResult->ConnectedPins[pinIndex++] ) )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            oC_SaveIfErrorOccur("ETH-LLD: Disconnect error - " , DisconnectPins(outResult->ConnectedPins, oC_ARRAY_SIZE(outResult->ConnectedPins)));
        }
    }
    else
    {
        errorCode = oC_ErrorCode_CommunicationInterfaceNotCorrect;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disconnects all pins connected by the #ConnectPins
 */
//==========================================================================================================================================
static oC_ErrorCode_t DisconnectPins( oC_Pin_t * ConnectedPins , uint16_t ArraySize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;

    for(uint16_t pinIndex = 0; pinIndex < ArraySize ; pinIndex++)
    {
        if(ConnectedPins[pinIndex] != oC_Pin_NotUsed)
        {
            oC_AssignErrorCode(&errorCode, DisconnectPin(ConnectedPins[pinIndex]));
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finds module pin and connects it
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectPin( oC_Pin_t Pin , oC_PinFunction_t PinFunction , oC_Pin_t * outConnectedPinReference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( Pin != oC_Pin_NotUsed           , oC_ErrorCode_PinNotSet        )
     && ErrorCondition( oC_GPIO_LLD_IsPinDefined(Pin)   , oC_ErrorCode_PinNotDefined    )
        )
    {
        bool                pinUsed   = false;
        oC_ModulePinIndex_t modulePin = 0;

        oC_MCS_EnterCriticalSection();

        if(
            oC_AssignErrorCode( &errorCode , oC_GPIO_MSLLD_FindModulePin( Pin, ETH_Channel, PinFunction, &modulePin )  )
         && oC_AssignErrorCode( &errorCode , oC_GPIO_LLD_CheckIsPinUsed( Pin, &pinUsed )                               )
         && ErrorCondition( pinUsed == false , oC_ErrorCode_PinIsUsed )
         && oC_AssignErrorCode( &errorCode , oC_GPIO_MSLLD_ConnectModulePin( modulePin )                               )
         && oC_AssignErrorCode( &errorCode , oC_GPIO_LLD_SetPinsUsed( Pin )                                            )
            )
        {
            *outConnectedPinReference = Pin;
            errorCode                 = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disconnects pin
 */
//==========================================================================================================================================
static oC_ErrorCode_t DisconnectPin( oC_Pin_t Pin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_DisconnectPin(Pin,0)      )
     && oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUnused(Pin)          )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief enables all required clocks for the Ethernet work
 */
//==========================================================================================================================================
static oC_ErrorCode_t EnableClock( oC_ETH_LLD_PHY_CommunicationInterface_t CommunicationInterface )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(
        ErrorCondition( oC_Machine_SetPowerStateForChannel(ETH_Channel,oC_Power_On) , oC_ErrorCode_CannotEnableChannel )
        )
    {
        /* Enable Tx/Rx clocks (MAC is enabled by the enabling the ETH channel) */
        RCC_AHB1ENR->ETHMACRXEN = 1;
        RCC_AHB1ENR->ETHMACTXEN = 1;

        if(
            CommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_MII
         || CommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_RMII
            )
        {
            oC_RegisterType_SYSCFG_PMC_t pmc;
            oC_Time_t                    time = 0;
            oC_RegisterType_ETH_DMABMR_t dmabmr;

            pmc.Value           = SYSCFG_PMC->Value;
            pmc.MII_RMII_SEL    = CommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_RMII;
            dmabmr.Value        = ETH_DMABMR->Value;
            dmabmr.SR           = 1;
            SYSCFG_PMC->Value   = pmc.Value;
            ETH_DMABMR->Value   = dmabmr.Value;

            while(ETH_DMABMR->SR == 1 && time < TIMEOUT)
            {
                time += ms(1);
                oC_CLOCK_LLD_DelayForMicroseconds(1000);
            }

            if(ETH_DMABMR->SR == 0)
            {
                /* These values are from machine documentation. Don't change it's order! */
                static const ClockRange_t ranges[] = {
                    { .Min = MHz(60)  , .Max = MHz(100) },
                    { .Min = MHz(100) , .Max = MHz(150) },
                    { .Min = MHz(20)  , .Max = MHz(35)  },
                    { .Min = MHz(35)  , .Max = MHz(60)  },
                    { .Min = MHz(150) , .Max = oC_MACHINE_MAXIMUM_FREQUENCY  },
                };
                oC_Frequency_t clockFrequency = oC_CLOCK_LLD_GetClockFrequency();
                bool           foundRange     = false;

                for(uint8_t rangeIndex = 0; rangeIndex < oC_ARRAY_SIZE(ranges); rangeIndex++)
                {
                    if(clockFrequency >= ranges[rangeIndex].Min && clockFrequency <= ranges[rangeIndex].Max)
                    {
                        ETH_MACMIIAR->CR = rangeIndex;
                        foundRange       = true;
                        break;
                    }
                }

                if(foundRange)
                {
                    NumberOfConfiguredPhys++;
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    errorCode = oC_ErrorCode_ClockConfigurationError;
                }
            }
            else
            {
                oC_SaveError("ETH-LLD: Cannot enable MII/RMII interface - " , errorCode);
                errorCode = oC_ErrorCode_Timeout;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_CommunicationInterfaceNotCorrect;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disables ethernet clock
 */
//==========================================================================================================================================
static oC_ErrorCode_t DisableClock( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(ErrorCondition( NumberOfConfiguredPhys > 0 , oC_ErrorCode_ChannelNotConfigured ))
    {
        NumberOfConfiguredPhys--;

        if(NumberOfConfiguredPhys == 0)
        {
            /* Disable Tx/Rx clocks */
            RCC_AHB1ENR->ETHMACRXEN = 0;
            RCC_AHB1ENR->ETHMACTXEN = 0;

            if(ErrorCondition(oC_Machine_SetPowerStateForChannel(ETH_Channel,oC_Power_Off) , oC_ErrorCode_CannotDisableChannel ))
            {
                errorCode = oC_ErrorCode_None;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }
    }


    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the given PHY communication interface is correct
 */
//==========================================================================================================================================
static oC_ErrorCode_t CheckPhyCommunicationInterface( oC_ETH_LLD_PHY_CommunicationInterface_t CommunicationInterface )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ConfiguredPhyCommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_None)
    {
        if(
            CommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_MII
         || CommunicationInterface == oC_ETH_LLD_PHY_CommunicationInterface_RMII
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CommunicationInterfaceNotCorrect;
        }
    }
    else
    {
        if(CommunicationInterface == ConfiguredPhyCommunicationInterface)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_OtherCommunicationInterfaceInUse;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief performs MAC register access test
 */
//==========================================================================================================================================
static oC_ErrorCode_t PerformMacRegisterAccessTest( oC_Diag_t * Diag , void * Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsRam(Context) , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        oC_MCS_EnterCriticalSection();

        oC_Procedure_Begin
        {
            ETH_MACCR->LM = 1;

            oC_MCS_Delay(10);

            if(!ErrorCondition( ETH_MACCR->LM == 1 , oC_ErrorCode_CannotAccessRegister ))
            {
                Diag->ResultDescription = "Cannot access ETH_MACCR register! ETH_MACCR->LM != 1";
                break;
            }

            ETH_MACCR->LM = 0;

            oC_MCS_Delay(10);

            if(!ErrorCondition( ETH_MACCR->LM == 0 , oC_ErrorCode_CannotAccessRegister ))
            {
                Diag->ResultDescription = "Cannot access ETH_MACCR register! ETH_MACCR->LM != 0";
                break;
            }

            errorCode = oC_ErrorCode_None;
        }
        oC_Procedure_End;



        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

//==========================================================================================================================================
/**
 * Interrupt handler for Peripheral Interrupt of the ETH module
 */
//==========================================================================================================================================
oC_InterruptHandler(ETHERNET_MAC,PeripheralInterrupt)
{
    oC_ETH_LLD_InterruptSource_t interruptSource = 0;

    if(InterruptHandler)
    {

        if(DmaStatus->ReceiveFinished)
        {
            interruptSource |=  oC_ETH_LLD_InterruptSource_DataReceived;
        }
        if(DmaStatus->TransmissionFinished)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_TransmissionSlotsAvailable;
        }

        if(DmaStatus->TransmissionStopped)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_TransmitProcessStopped | oC_ETH_LLD_InterruptSource_TransmitError;
        }

        if(DmaStatus->ReceiveStopped)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_ReceiveProcessStopped | oC_ETH_LLD_InterruptSource_ReceiveError;
        }

        if(DmaStatus->ReceiveWatchdogTimeout)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_ReceiveTimeout | oC_ETH_LLD_InterruptSource_ReceiveError;
        }

        if(DmaStatus->TransmitJabberTimeout)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_TransmitJabberTimeout | oC_ETH_LLD_InterruptSource_TransmitError;
        }

        if(DmaStatus->ReceiveOverflow)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_ReceiveOverflowError | oC_ETH_LLD_InterruptSource_ReceiveError;
        }

        if(DmaStatus->TransmitUnderflow)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_TransmitUnderflowError | oC_ETH_LLD_InterruptSource_TransmitError;
        }

        if(DmaStatus->TransmitBufferUnavailable)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_TransmitDescriptorError | oC_ETH_LLD_InterruptSource_TransmitError;

            ETH_DMASR->TBUS    = 1;         /* Clear status */
            ETH_DMATPDR->Value = 0xFFFF;    /* Resume transmission */
        }

        if(DmaStatus->ReceiveBufferUnavailable)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_ReceiveDescriptorError | oC_ETH_LLD_InterruptSource_ReceiveError;
        }

        if(DmaStatus->FatalBusError)
        {
            interruptSource |= oC_ETH_LLD_InterruptSource_FatalBusError;
        }

        InterruptHandler(interruptSource);

        /* Clearing interrupt status */
        ETH_DMASR->Value = 0x1FFFF;
    }
}

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

