/** ****************************************************************************************************************************************
 *
 * @file       oc_lcdtft_lld.c
 *
 * @brief      The file with source for LCDTFT LLD functions
 *
 * @file       oc_lcdtft_lld.c
 *
 * @author     Patryk Kubiak - (Created on: 2016-02-06 - 11:43:03)
 *
 * @copyright  Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_lcdtft_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <oc_saipll.h>
#include <oc_gpio_lld.h>
#include <oc_gpio_mslld.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsRam(Address)                      (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define LTDC_Channel                        oC_Channel_LCD
#define LTDC_Layer                          1
#define IsLayerCorrect(Layer)               ( (Layer) < 2 && (Layer) >= 0 )
#define IsPolarityCorrect(Polarity)         ( (Polarity) == oC_LCDTFT_LLD_Polarity_ActiveLow || (Polarity) == oC_LCDTFT_LLD_Polarity_ActiveHigh )
#define IsChannelPoweredOn()                (oC_Machine_GetPowerStateForChannel(LTDC_Channel) == oC_Power_On)
#define SetPolarity(Bit , Polarity)         Bit = (Polarity == oC_LCDTFT_LLD_Polarity_ActiveHigh) ? 1 : 0
#define GetPolarity(Bit)                    ((Bit) == 1) ? oC_LCDTFT_LLD_Polarity_ActiveHigh : oC_LCDTFT_LLD_Polarity_ActiveHigh
#define AreOperationsEnabled()              (LTDC_GCR->LTDCEN == 1)
#define AreOperationsDisabled()             (LTDC_GCR->LTDCEN == 0)
#define LTDC_GCR                            oC_Register(LCD_TFT , LTDC_GCR      )
#define LTDC_SSCR                           oC_Register(LCD_TFT , LTDC_SSCR     )
#define LTDC_SRCR                           oC_Register(LCD_TFT , LTDC_SRCR     )
#define LTDC_BCCR                           oC_Register(LCD_TFT , LTDC_BCCR     )
#define LTDC_IER                            oC_Register(LCD_TFT , LTDC_IER      )
#define LTDC_TWCR                           oC_Register(LCD_TFT , LTDC_TWCR     )
#define LTDC_BPCR                           oC_Register(LCD_TFT , LTDC_BPCR     )
#define LTDC_AWCR                           oC_Register(LCD_TFT , LTDC_AWCR     )
#define LTDC_L1CFBAR                        oC_Register(LCD_TFT , LTDC_L1CFBAR  )
#define LTDC_L1CFBLR                        oC_Register(LCD_TFT , LTDC_L1CFBLR  )
#define LTDC_L1CFBLNR                       oC_Register(LCD_TFT , LTDC_L1CFBLNR )
#define LTDC_L1CR                           oC_Register(LCD_TFT , LTDC_L1CR     )
#define LTDC_L1PFCR                         oC_Register(LCD_TFT , LTDC_L1PFCR   )
#define LTDC_L1WHPCR                        oC_Register(LCD_TFT , LTDC_L1WHPCR  )
#define LTDC_L1WVPCR                        oC_Register(LCD_TFT , LTDC_L1WVPCR  )
#define LTDC_L1DCCR                         oC_Register(LCD_TFT , LTDC_L1DCCR   )
#define LTDC_L1CACR                         oC_Register(LCD_TFT , LTDC_L1CACR   )
#define LTDC_L1BFCR                         oC_Register(LCD_TFT , LTDC_L1BFCR   )
#define RCC_CR                              oC_Register(RCC     , RCC_CR)
#define RCC_PLLSAICFGR                      oC_Register(RCC     , RCC_PLLSAICFGR)
#define RCC_DCKCFGR1                        oC_Register(RCC     , RCC_DCKCFGR1)
#define RCC_APB2RSTR                        oC_Register(RCC     , RCC_APB2RSTR)
#define MAX_WIDTH                           1024
#define MAX_HEIGHT                          768
#define MAX_HSW                             0xFFF
#define MAX_VSW                             0x7FF
#define MAX_AHBP                            0xFFF
#define MAX_AVBP                            0x7FF
#define MAX_AAH                             0xFFF
#define MAX_AAW                             0x7FF
#define MAX_TOTALH                          0xFFF
#define MAX_TOTALW                          0x7FF
#define MAX_COLOR_VALUE(PixelSize)          ( \
                                                    ((PixelSize) == 1 ) ? 0x000000FF :   \
                                                    ((PixelSize) == 2 ) ? 0x0000FFFF :   \
                                                    ((PixelSize) == 3 ) ? 0x00FFFFFF :   \
                                                    ((PixelSize) == 4 ) ? 0xFFFFFFFF : 0 \
                                            )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool                         FindModulePin        ( oC_Pin_t Pin , oC_LCDTFT_PinFunction_t Function , oC_LCDTFT_Pin_t * outModulePin );
static oC_ErrorCode_t               ConnectModulePin     ( oC_LCDTFT_Pin_t ModulePin );
static oC_ErrorCode_t               ConnectPin           ( oC_Pin_t Pin , oC_GPIO_LLD_Mode_t PinMode );
static oC_ColorFormat_t             GetPixelFormat       ( void );
static oC_UInt_t                    GetPixelSize         ( void );
static oC_ErrorCode_t               SetPixelFormat       ( oC_ColorFormat_t PixelFormat );
static oC_ErrorCode_t               SetTimingParameters  ( const oC_LCDTFT_LLD_TimingParameters_t * TimingParameters );
static oC_ErrorCode_t               ReadTimingParameters (       oC_LCDTFT_LLD_TimingParameters_t * outTimingParameters );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_Frequency_t                ConfiguredFrequency    = 0;
static oC_Pixel_ResolutionUInt_t     ConfiguredWidth        = 0;
static oC_Pixel_ResolutionUInt_t     ConfiguredHeight       = 0;
static oC_Pin_t                      ConfiguredDispPin      = 0;
static oC_Pin_t                      ConfiguredBlCtrlPin    = 0;
static oC_ColorFormat_t              ConfiguredColorFormat  = oC_ColorFormat_Unknown;
static const oC_LCDTFT_PinFunction_t PinFunctions  [oC_LCDTFT_LLD_PinIndex_NumberOfPins] = {
                            [ oC_LCDTFT_LLD_PinIndex_R0 ]      = oC_LCDTFT_PinFunction_LCD_R0 ,
                            [ oC_LCDTFT_LLD_PinIndex_R1 ]      = oC_LCDTFT_PinFunction_LCD_R1 ,
                            [ oC_LCDTFT_LLD_PinIndex_R2 ]      = oC_LCDTFT_PinFunction_LCD_R2 ,
                            [ oC_LCDTFT_LLD_PinIndex_R3 ]      = oC_LCDTFT_PinFunction_LCD_R3 ,
                            [ oC_LCDTFT_LLD_PinIndex_R4 ]      = oC_LCDTFT_PinFunction_LCD_R4 ,
                            [ oC_LCDTFT_LLD_PinIndex_R5 ]      = oC_LCDTFT_PinFunction_LCD_R5 ,
                            [ oC_LCDTFT_LLD_PinIndex_R6 ]      = oC_LCDTFT_PinFunction_LCD_R6 ,
                            [ oC_LCDTFT_LLD_PinIndex_R7 ]      = oC_LCDTFT_PinFunction_LCD_R7 ,
                            [ oC_LCDTFT_LLD_PinIndex_G0 ]      = oC_LCDTFT_PinFunction_LCD_G0 ,
                            [ oC_LCDTFT_LLD_PinIndex_G1 ]      = oC_LCDTFT_PinFunction_LCD_G1 ,
                            [ oC_LCDTFT_LLD_PinIndex_G2 ]      = oC_LCDTFT_PinFunction_LCD_G2 ,
                            [ oC_LCDTFT_LLD_PinIndex_G3 ]      = oC_LCDTFT_PinFunction_LCD_G3 ,
                            [ oC_LCDTFT_LLD_PinIndex_G4 ]      = oC_LCDTFT_PinFunction_LCD_G4 ,
                            [ oC_LCDTFT_LLD_PinIndex_G5 ]      = oC_LCDTFT_PinFunction_LCD_G5 ,
                            [ oC_LCDTFT_LLD_PinIndex_G6 ]      = oC_LCDTFT_PinFunction_LCD_G6 ,
                            [ oC_LCDTFT_LLD_PinIndex_G7 ]      = oC_LCDTFT_PinFunction_LCD_G7 ,
                            [ oC_LCDTFT_LLD_PinIndex_B0 ]      = oC_LCDTFT_PinFunction_LCD_B0 ,
                            [ oC_LCDTFT_LLD_PinIndex_B1 ]      = oC_LCDTFT_PinFunction_LCD_B1 ,
                            [ oC_LCDTFT_LLD_PinIndex_B2 ]      = oC_LCDTFT_PinFunction_LCD_B2 ,
                            [ oC_LCDTFT_LLD_PinIndex_B3 ]      = oC_LCDTFT_PinFunction_LCD_B3 ,
                            [ oC_LCDTFT_LLD_PinIndex_B4 ]      = oC_LCDTFT_PinFunction_LCD_B4 ,
                            [ oC_LCDTFT_LLD_PinIndex_B5 ]      = oC_LCDTFT_PinFunction_LCD_B5 ,
                            [ oC_LCDTFT_LLD_PinIndex_B6 ]      = oC_LCDTFT_PinFunction_LCD_B6 ,
                            [ oC_LCDTFT_LLD_PinIndex_B7 ]      = oC_LCDTFT_PinFunction_LCD_B7 ,
                            [ oC_LCDTFT_LLD_PinIndex_VSYNC ]   = oC_LCDTFT_PinFunction_LCD_VSYNC ,
                            [ oC_LCDTFT_LLD_PinIndex_HSYNC ]   = oC_LCDTFT_PinFunction_LCD_HSYNC ,
                            [ oC_LCDTFT_LLD_PinIndex_CLK ]     = oC_LCDTFT_PinFunction_LCD_CLK ,
                            [ oC_LCDTFT_LLD_PinIndex_DE ]      = oC_LCDTFT_PinFunction_LCD_DE ,
                            [ oC_LCDTFT_LLD_PinIndex_DISP ]    = 0 ,
                            [ oC_LCDTFT_LLD_PinIndex_BL_CTRL ] = 0 ,
                            [ oC_LCDTFT_LLD_PinIndex_LED_K ]   = 0 ,
                            [ oC_LCDTFT_LLD_PinIndex_LED_A ]   = 0 ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        oC_Module_TurnOn(oC_Module_LCDTFT_LLD);
        ConfiguredFrequency           = 0;
        ConfiguredHeight              = 0;
        ConfiguredWidth               = 0;
        ConfiguredDispPin             = 0;
        ConfiguredBlCtrlPin           = 0;
        ConfiguredColorFormat         = 0;
        errorCode                     = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        oC_Module_TurnOff(oC_Module_LCDTFT_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetPower( oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( Power == oC_Power_On || Power == oC_Power_Off           , oC_ErrorCode_PowerStateNotCorrect) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(LTDC_Channel,Power)  , oC_ErrorCode_CannotEnableChannel)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadPower( oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(ErrorCondition( IsRam(outPower) , oC_ErrorCode_OutputAddressNotInRAM))
        {
            *outPower = oC_Machine_GetPowerStateForChannel(LTDC_Channel);
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_EnableOperations( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()     , oC_ErrorCode_ChannelNotPoweredOn ) &&
            ErrorCondition( ConfiguredBlCtrlPin != 0 , oC_ErrorCode_ChannelNotConfigured) &&
            ErrorCondition( ConfiguredDispPin   != 0 , oC_ErrorCode_ChannelNotConfigured) &&
            ErrorCondition( ConfiguredFrequency != 0 , oC_ErrorCode_ChannelNotConfigured) &&
            ErrorCondition( ConfiguredHeight    != 0 , oC_ErrorCode_ChannelNotConfigured) &&
            ErrorCondition( ConfiguredWidth     != 0 , oC_ErrorCode_ChannelNotConfigured)
            )
        {
            LTDC_IER->TERRIE = 1;
            LTDC_IER->FUIE   = 1;
            LTDC_SRCR->IMR   = 1;
            LTDC_L1CR->LEN   = 1;
            LTDC_GCR->LTDCEN = 1;

            oC_GPIO_LLD_SetPinsState(ConfiguredBlCtrlPin , oC_GPIO_LLD_PinsState_AllHigh);
            oC_GPIO_LLD_SetPinsState(ConfiguredDispPin   , oC_GPIO_LLD_PinsState_AllHigh);

            errorCode = oC_ErrorCode_None;

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_DisableOperations( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(ErrorCondition( IsChannelPoweredOn() , oC_ErrorCode_ChannelNotPoweredOn))
        {
            LTDC_GCR->LTDCEN = 0;
            errorCode        = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetFrequency( oC_Frequency_t Frequency , oC_Frequency_t PermissibleDifference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( Frequency > 0               , oC_ErrorCode_WrongFrequency ) &&
            ErrorCondition( PermissibleDifference >= 0  , oC_ErrorCode_WrongFrequency )
            )
        {
            errorCode = oC_SaiPll_Configure( oC_SaiPll_OutputLine_LTDC , Frequency , PermissibleDifference , &ConfiguredFrequency );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadFrequency( oC_Frequency_t * outFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsRam(outFrequency)      , oC_ErrorCode_OutputAddressNotInRAM ) &&
            ErrorCondition( ConfiguredFrequency != 0 , oC_ErrorCode_ChannelNotConfigured )
            )
        {
            *outFrequency = ConfiguredFrequency;
            errorCode     = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetPixelClockPolarity( oC_LCDTFT_LLD_PixelClockPolarity_t PixelPolarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()    , oC_ErrorCode_ChannelNotPoweredOn          ) &&
            ErrorCondition( AreOperationsDisabled() , oC_ErrorCode_ChannelOperationsNotDisabled )
            )
        {
            switch(PixelPolarity)
            {
                case oC_LCDTFT_LLD_PixelClockPolarity_InputPixelClock:
                    LTDC_GCR->PCPOL = 0;
                    errorCode       = oC_ErrorCode_None;
                    break;
                case oC_LCDTFT_LLD_PixelClockPolarity_InvertedInputPixelClock:
                    LTDC_GCR->PCPOL = 1;
                    errorCode       = oC_ErrorCode_None;
                    break;
                default:
                    errorCode = oC_ErrorCode_PolarityNotCorrect;
                    break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadPixelClockPolarity( oC_LCDTFT_LLD_PixelClockPolarity_t * outPixelPolarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()    , oC_ErrorCode_ChannelNotPoweredOn ) &&
            ErrorCondition( IsRam(outPixelPolarity) , oC_ErrorCode_OutputAddressNotInRAM )
            )
        {
            if(LTDC_GCR->PCPOL == 0)
            {
                *outPixelPolarity = oC_LCDTFT_LLD_PixelClockPolarity_InputPixelClock;
            }
            else
            {
                *outPixelPolarity = oC_LCDTFT_LLD_PixelClockPolarity_InvertedInputPixelClock;
            }
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetPolarities( oC_LCDTFT_LLD_Polarity_t HSyncPolarity , oC_LCDTFT_LLD_Polarity_t VSyncPolarity , oC_LCDTFT_LLD_Polarity_t DataEnablePolarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()                    , oC_ErrorCode_ChannelNotPoweredOn          ) &&
            ErrorCondition( AreOperationsDisabled()                 , oC_ErrorCode_ChannelOperationsNotDisabled ) &&
            ErrorCondition( IsPolarityCorrect(HSyncPolarity)        , oC_ErrorCode_PolarityNotCorrect           ) &&
            ErrorCondition( IsPolarityCorrect(VSyncPolarity)        , oC_ErrorCode_PolarityNotCorrect           ) &&
            ErrorCondition( IsPolarityCorrect(DataEnablePolarity)   , oC_ErrorCode_PolarityNotCorrect           )
            )
        {
            SetPolarity( LTDC_GCR->HSPOL , HSyncPolarity      );
            SetPolarity( LTDC_GCR->VSPOL , VSyncPolarity      );
            SetPolarity( LTDC_GCR->DEPOL , DataEnablePolarity );
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadPolarities( oC_LCDTFT_LLD_Polarity_t * outHSyncPolarity , oC_LCDTFT_LLD_Polarity_t * outVSyncPolarity , oC_LCDTFT_LLD_Polarity_t * outDataEnablePolarity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()            , oC_ErrorCode_ChannelNotPoweredOn   ) &&
            ErrorCondition( IsRam(outHSyncPolarity)         , oC_ErrorCode_OutputAddressNotInRAM ) &&
            ErrorCondition( IsRam(outVSyncPolarity)         , oC_ErrorCode_OutputAddressNotInRAM ) &&
            ErrorCondition( IsRam(outDataEnablePolarity)    , oC_ErrorCode_OutputAddressNotInRAM )
            )
        {
            *outHSyncPolarity       = GetPolarity(LTDC_GCR->HSPOL);
            *outVSyncPolarity       = GetPolarity(LTDC_GCR->VSPOL);
            *outDataEnablePolarity  = GetPolarity(LTDC_GCR->DEPOL);
            errorCode               = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetResolution(  oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()        , oC_ErrorCode_ChannelNotPoweredOn              ) &&
            ErrorCondition( AreOperationsDisabled()     , oC_ErrorCode_ChannelOperationsNotDisabled     ) &&
            ErrorCondition( Width  <= MAX_WIDTH         , oC_ErrorCode_WidthTooBig                      ) &&
            ErrorCondition( Height <= MAX_HEIGHT        , oC_ErrorCode_HeightTooBig                     )
            )
        {
            ConfiguredWidth     = Width;
            ConfiguredHeight    = Height;
            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadResolution( oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsRam(outWidth)             , oC_ErrorCode_OutputAddressNotInRAM    ) &&
            ErrorCondition( IsRam(outHeight)            , oC_ErrorCode_OutputAddressNotInRAM    ) &&
            ErrorCondition( IsChannelPoweredOn()        , oC_ErrorCode_ChannelNotPoweredOn      ) &&
            ErrorCondition( ConfiguredWidth  > 0        , oC_ErrorCode_ChannelNotConfigured     ) &&
            ErrorCondition( ConfiguredHeight > 0        , oC_ErrorCode_ChannelNotConfigured     )
            )
        {
            *outWidth   = ConfiguredWidth;
            *outHeight  = ConfiguredHeight;
            errorCode   = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_RestoreDefaultState( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        RCC_APB2RSTR->LTDCRST = 1;
        errorCode             = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ConnectPins( const oC_LCDTFT_LLD_Pins_t * Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsRam(Pins) || IsRom(Pins) , oC_ErrorCode_WrongAddress          ) &&
            ErrorCondition( IsChannelPoweredOn()       , oC_ErrorCode_ChannelNotPoweredOn   )
            )
        {
#define IS_PIN_REQUIRED(PinIndex)               \
    ( (PinIndex >= oC_LCDTFT_LLD_PinIndex_R0 && PinIndex <= oC_LCDTFT_LLD_PinIndex_R7) ? (PinIndex - oC_LCDTFT_LLD_PinIndex_R0) < requiredRed   :\
      (PinIndex >= oC_LCDTFT_LLD_PinIndex_G0 && PinIndex <= oC_LCDTFT_LLD_PinIndex_G7) ? (PinIndex - oC_LCDTFT_LLD_PinIndex_G0) < requiredGreen :\
      (PinIndex >= oC_LCDTFT_LLD_PinIndex_B0 && PinIndex <= oC_LCDTFT_LLD_PinIndex_B7) ? (PinIndex - oC_LCDTFT_LLD_PinIndex_B0) < requiredBlue  : true\
                    )
            oC_ColorFormat_t   pixelFormat   = GetPixelFormat();
            uint8_t            requiredRed   = oC_ColorFormat_GetNoRedBits(pixelFormat);
            uint8_t            requiredGreen = oC_ColorFormat_GetNoGreenBits(pixelFormat);
            uint8_t            requiredBlue  = oC_ColorFormat_GetNoBlueBits(pixelFormat);

            errorCode = (pixelFormat != oC_ColorFormat_Unknown) ? oC_ErrorCode_None : oC_ErrorCode_PixelFormatNotConfigured;

            for(oC_LCDTFT_LLD_PinIndex_t pinIndex = 0 ; !oC_ErrorOccur(errorCode ) && pinIndex < oC_LCDTFT_LLD_PinIndex_NumberOfPins ; pinIndex++ )
            {
                if(PinFunctions[pinIndex])
                {
                    if(oC_GPIO_LLD_IsPinDefined(Pins->PinsArray[pinIndex]))
                    {
                        oC_LCDTFT_Pin_t modulePin = 0;

                        if(FindModulePin(Pins->PinsArray[pinIndex],PinFunctions[pinIndex],&modulePin))
                        {
                            errorCode = ConnectModulePin(modulePin);
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_NoneOfModulePinAssigned;
                        }
                    }
                    else if(IS_PIN_REQUIRED(pinIndex))
                    {
                        errorCode = oC_ErrorCode_PinNotDefined;
                    }
                }
            }

            if(
                !oC_ErrorOccur(errorCode) &&
                oC_AssignErrorCode( &errorCode , ConnectPin(Pins->PinsArray[oC_LCDTFT_LLD_PinIndex_DISP]   , oC_GPIO_LLD_Mode_Output ) ) &&
                oC_AssignErrorCode( &errorCode , ConnectPin(Pins->PinsArray[oC_LCDTFT_LLD_PinIndex_BL_CTRL], oC_GPIO_LLD_Mode_Output ) )
                )
            {
                ConfiguredDispPin   = Pins->PinsArray[oC_LCDTFT_LLD_PinIndex_DISP];
                ConfiguredBlCtrlPin = Pins->PinsArray[oC_LCDTFT_LLD_PinIndex_BL_CTRL];
                errorCode           = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfErrorOccur("LCDTFT-LLD - Disconnecting pins error: " , oC_LCDTFT_LLD_DisconnectPins(Pins));
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_DisconnectPins( const oC_LCDTFT_LLD_Pins_t * Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsRam(Pins) || IsRom(Pins) , oC_ErrorCode_WrongAddress )
            )
        {
            errorCode = oC_ErrorCode_None;

            for(oC_LCDTFT_LLD_PinIndex_t pinIndex = 0 ; pinIndex < oC_LCDTFT_LLD_PinIndex_NumberOfPins ; pinIndex++ )
            {
                if(Pins->PinsArray[pinIndex] != oC_Pin_NotUsed)
                {
                    oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUnused(Pins->PinsArray[pinIndex])         );
                    oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_DisconnectPin( Pins->PinsArray[pinIndex] , 0 ) );
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetColormapBuffer( const void * Buffer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        oC_UInt_t    pixelSize          = GetPixelSize();
        oC_UInt_t    requiredBufferSize = pixelSize * ConfiguredWidth * ConfiguredHeight;
        const void * bufferEnd          = (const void * )(((oC_UInt_t)Buffer) + requiredBufferSize - 1);

        if(
            ErrorCondition( IsRam(Buffer) || IsRom(Buffer)             , oC_ErrorCode_WrongAddress                 ) &&
            ErrorCondition( GetPixelFormat() != oC_ColorFormat_Unknown , oC_ErrorCode_PixelFormatNotConfigured     ) &&
            ErrorCondition( pixelSize > 0 && pixelSize <= 4            , oC_ErrorCode_MachineCanBeDamaged          ) &&
            ErrorCondition( ConfiguredWidth > 0                        , oC_ErrorCode_ResolutionNotSet             ) &&
            ErrorCondition( ConfiguredHeight> 0                        , oC_ErrorCode_ResolutionNotSet             ) &&
            ErrorCondition( IsRam(bufferEnd) || IsRom(bufferEnd)       , oC_ErrorCode_WrongBufferEndAddress        )
            )
        {
            LTDC_L1CFBAR->Value = (uint32_t)Buffer;
            LTDC_SRCR->IMR      = 1;
            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetPixelFormat( oC_ColorFormat_t PixelFormat )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition(IsChannelPoweredOn()    , oC_ErrorCode_ChannelNotPoweredOn            ) &&
            ErrorCondition(AreOperationsDisabled() , oC_ErrorCode_ChannelOperationsNotDisabled   )
            )
        {
            errorCode = SetPixelFormat( PixelFormat );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadPixelFormat( oC_ColorFormat_t * outPixelFormat )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition(IsChannelPoweredOn()  , oC_ErrorCode_ChannelNotPoweredOn   ) &&
            ErrorCondition(IsRam(outPixelFormat) , oC_ErrorCode_OutputAddressNotInRAM )
            )
        {
            *outPixelFormat = GetPixelFormat();
            errorCode       = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetTimingParameters(  const oC_LCDTFT_LLD_TimingParameters_t * TimingParameters )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsRam(TimingParameters) || IsRom(TimingParameters) , oC_ErrorCode_WrongAddress                  ) &&
            ErrorCondition( IsChannelPoweredOn()                               , oC_ErrorCode_ChannelNotPoweredOn           ) &&
            ErrorCondition( AreOperationsDisabled()                            , oC_ErrorCode_ChannelOperationsNotDisabled  ) &&
            ErrorCondition( ConfiguredWidth > 0                                , oC_ErrorCode_ResolutionNotSet              ) &&
            ErrorCondition( ConfiguredHeight> 0                                , oC_ErrorCode_ResolutionNotSet              )
            )
        {
            errorCode = SetTimingParameters( TimingParameters );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadTimingParameters( oC_LCDTFT_LLD_TimingParameters_t * outTimingParameters )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsRam(outTimingParameters)  , oC_ErrorCode_OutputAddressNotInRAM  ) &&
            ErrorCondition( IsChannelPoweredOn()        , oC_ErrorCode_ChannelNotPoweredOn    )
            )
        {
            errorCode = ReadTimingParameters( outTimingParameters );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_SetBackgroundColor( oC_Color_t Color , oC_ColorFormat_t ColorFormat )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()                    , oC_ErrorCode_ChannelNotPoweredOn              ) &&
            ErrorCondition( AreOperationsDisabled()                 , oC_ErrorCode_ChannelOperationsNotDisabled     ) &&
            ErrorCondition( oC_Color_IsCorrect(Color)               , oC_ErrorCode_ColorNotCorrect                  ) &&
            ErrorCondition( ColorFormat != oC_ColorFormat_Unknown   , oC_ErrorCode_ColorFormatNotSupported          )
            )
        {
            oC_Color_t converted = oC_Color_ConvertToFormat(Color,ColorFormat,oC_ColorFormat_RGB888);
            LTDC_BCCR->Value   = (uint32_t)converted;
            LTDC_L1DCCR->Value = (uint32_t)converted;
            LTDC_L1CACR->Value = 0xFF;
            errorCode          = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_LLD_ReadBackgroundColor( oC_Color_t * outColor , oC_ColorFormat_t ColorFormat )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT_LLD))
    {
        if(
            ErrorCondition( IsChannelPoweredOn()                    , oC_ErrorCode_ChannelNotPoweredOn              ) &&
            ErrorCondition( IsRam(outColor)                         , oC_ErrorCode_OutputAddressNotInRAM            ) &&
            ErrorCondition( ColorFormat != oC_ColorFormat_Unknown   , oC_ErrorCode_ColorFormatNotSupported          )
            )
        {
            *outColor  = oC_Color_ConvertToFormat((oC_Color_t)LTDC_BCCR->Value,oC_ColorFormat_RGB888,ColorFormat);
            errorCode  = oC_ErrorCode_None;
        }
    }

    return errorCode;
}


#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * Searches for module pin
 */
//==========================================================================================================================================
static bool FindModulePin( oC_Pin_t Pin , oC_LCDTFT_PinFunction_t Function , oC_LCDTFT_Pin_t * outModulePin )
{
    bool success = false;

    oC_ModulePin_ForeachDefined(modulePin)
    {
        if((modulePin->Channel == LTDC_Channel) && modulePin->PinFunction == Function && modulePin->Pin == Pin)
        {
            *outModulePin = modulePin->ModulePinIndex;
            success       = true;
            break;
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * Connects module pin
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectModulePin( oC_LCDTFT_Pin_t ModulePin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Pin_t       pin       = oC_ModulePin_GetPin(ModulePin);
    bool           unused    = false;
    if(
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ArePinsUnused(pin,&unused)      ) &&
        ErrorCondition(    unused     , oC_ErrorCode_PinIsUsed) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(pin)                ) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_ConnectModulePin(ModulePin)   )
        )
    {
        errorCode = oC_ErrorCode_None;
    }
    return errorCode;
}
//==========================================================================================================================================
/**
 * Connects GPIO pin
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectPin( oC_Pin_t Pin , oC_GPIO_LLD_Mode_t PinMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    bool           unused    = false;

    if(
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ArePinsUnused(Pin,&unused)  ) &&
        ErrorCondition(    unused     , oC_ErrorCode_PinIsUsed                  ) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(Pin)            ) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pin)     ) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pin , PinMode)      ) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pin)    )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Returns configured pixel format
 */
//==========================================================================================================================================
static oC_ColorFormat_t GetPixelFormat( void )
{
    /* This is not read from register, because it always returns 0 */
    return ConfiguredColorFormat;
}

//==========================================================================================================================================
/**
 * Returns size of pixel (0 if not configured)
 */
//==========================================================================================================================================
static oC_UInt_t GetPixelSize( void )
{
    oC_ColorFormat_t pixelFormat = GetPixelFormat();
    return oC_Color_GetFormatSize(pixelFormat);
}

//==========================================================================================================================================
/**
 * Configures pixel format
 */
//==========================================================================================================================================
static oC_ErrorCode_t SetPixelFormat( oC_ColorFormat_t PixelFormat )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(PixelFormat)
    {
        case oC_ColorFormat_RGB888: LTDC_L1PFCR->PF = 1; errorCode = oC_ErrorCode_None; ConfiguredColorFormat = PixelFormat; break;
        case oC_ColorFormat_RGB565: LTDC_L1PFCR->PF = 2; errorCode = oC_ErrorCode_None; ConfiguredColorFormat = PixelFormat; break;
        default: errorCode = oC_ErrorCode_ColorFormatNotSupported; break;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Sets timing parameters
 */
//==========================================================================================================================================
static oC_ErrorCode_t SetTimingParameters( const oC_LCDTFT_LLD_TimingParameters_t * TimingParameters )
{
    oC_ErrorCode_t errorCode     = oC_ErrorCode_ImplementError;
    uint32_t       TOTAL_HEIGHT;
    uint32_t       TOTAL_WIDTH;
    uint32_t       ACTIVE_HEIGHT;
    uint32_t       ACTIVE_WIDTH;
    oC_UInt_t      pixelSize     = GetPixelSize();

    ACTIVE_WIDTH = TimingParameters->HSYNC.PulseWidth + ConfiguredWidth  + TimingParameters->HSYNC.BackPorch - 1;
    ACTIVE_HEIGHT= TimingParameters->VSYNC.PulseWidth + ConfiguredHeight + TimingParameters->VSYNC.BackPorch - 1;

    TOTAL_WIDTH  = TimingParameters->HSYNC.PulseWidth + TimingParameters->HSYNC.BackPorch + ACTIVE_WIDTH + TimingParameters->HSYNC.FrontPorch;
    TOTAL_HEIGHT = TimingParameters->VSYNC.PulseWidth + TimingParameters->VSYNC.BackPorch + ACTIVE_HEIGHT+ TimingParameters->VSYNC.FrontPorch;

    if(
        ErrorCondition( TimingParameters->HSYNC.PulseWidth <= MAX_HSW    , oC_ErrorCode_TimingParametersNotSupported ) &&
        ErrorCondition( TimingParameters->VSYNC.PulseWidth <= MAX_VSW    , oC_ErrorCode_TimingParametersNotSupported ) &&
        ErrorCondition( TimingParameters->HSYNC.BackPorch  <= MAX_AHBP   , oC_ErrorCode_TimingParametersNotSupported ) &&
        ErrorCondition( TimingParameters->VSYNC.BackPorch  <= MAX_AVBP   , oC_ErrorCode_TimingParametersNotSupported ) &&
        ErrorCondition( TimingParameters->HSYNC.PulseWidth  > 0          , oC_ErrorCode_TimingParametersNotFilled    ) &&
        ErrorCondition( TimingParameters->VSYNC.PulseWidth  > 0          , oC_ErrorCode_TimingParametersNotFilled    ) &&
        ErrorCondition( TimingParameters->HSYNC.BackPorch   > 0          , oC_ErrorCode_TimingParametersNotFilled    ) &&
        ErrorCondition( TimingParameters->VSYNC.BackPorch   > 0          , oC_ErrorCode_TimingParametersNotFilled    ) &&
        ErrorCondition( ACTIVE_HEIGHT                      <= MAX_AAH    , oC_ErrorCode_TimingParametersNotSupported ) &&
        ErrorCondition( ACTIVE_WIDTH                       <= MAX_AAW    , oC_ErrorCode_TimingParametersNotSupported ) &&
        ErrorCondition( TOTAL_HEIGHT                       <= MAX_TOTALH , oC_ErrorCode_TimingParametersNotSupported ) &&
        ErrorCondition( TOTAL_WIDTH                        <= MAX_TOTALW , oC_ErrorCode_TimingParametersNotSupported )
        )
    {
        LTDC_L1CR->LEN   = 1;

        SetPixelFormat(ConfiguredColorFormat);

        LTDC_SSCR->HSW = TimingParameters->HSYNC.PulseWidth - 1;
        LTDC_SSCR->VSH = TimingParameters->VSYNC.PulseWidth - 1;

        LTDC_BPCR->AHBP =  TimingParameters->HSYNC.BackPorch - 1;
        LTDC_BPCR->AVBP =  TimingParameters->VSYNC.BackPorch - 1;

        LTDC_AWCR->AAH  =  ACTIVE_HEIGHT;
        LTDC_AWCR->AAW  =  ACTIVE_WIDTH;

        LTDC_TWCR->TOTALH =  TOTAL_HEIGHT;
        LTDC_TWCR->TOTALW =  TOTAL_WIDTH;

        LTDC_L1WHPCR->Value = TimingParameters->HSYNC.BackPorch | ((TimingParameters->HSYNC.BackPorch + ConfiguredWidth - 1) << 16);

        LTDC_L1WVPCR->WVSPPOS = TimingParameters->VSYNC.BackPorch + ConfiguredHeight - 1;
        LTDC_L1WVPCR->WVSTPOS = TimingParameters->VSYNC.BackPorch;

        LTDC_L1CFBLR->Value = ((ConfiguredWidth * pixelSize) << 16) | ((ConfiguredWidth * pixelSize) + 3);

        LTDC_L1CFBLNR->Value = ConfiguredHeight;

        LTDC_L1BFCR->BF1 = 4;
        LTDC_L1BFCR->BF2 = 5;

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Reads timing parameters
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadTimingParameters( oC_LCDTFT_LLD_TimingParameters_t * outTimingParameters )
{
    return oC_ErrorCode_NotImplemented;
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

