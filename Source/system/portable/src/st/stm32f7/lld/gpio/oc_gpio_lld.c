/** ****************************************************************************************************************************************
 *
 * @file       oc_gpio_lld.c
 *
 * @brief      File with source for stm32f7120h5qr machine for GPIO LLD module
 *
 * @author     Patryk Kubiak - (Created on: 15 08 2015 15:10:29)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_gpio_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_lld.h>
#include <oc_module.h>
#include <oc_mcs.h>
#include <oc_lsf.h>

/** ========================================================================================================================================
 *
 *              The section with local macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

#define PORT(Pins)               oC_Pin_GetPort(Pins)
#define PORT_INDEX(Pins)         oC_Channel_ToIndex(GPIO , PORT(Pins))
#define PINS(Pins)               oC_Pin_GetPinsInPort(Pins)
#define IsPoweredOn(Pins)        oC_Machine_IsChannelPoweredOn(PORT(Pins))
#define GPIOx_MODER(Pins)        oC_Machine_Register(PORT(Pins),GPIOx_MODER)
#define GPIOx_OTYPER(Pins)       oC_Machine_Register(PORT(Pins),GPIOx_OTYPER)
#define GPIOx_PUPDR(Pins)        oC_Machine_Register(PORT(Pins),GPIOx_PUPDR)
#define GPIOx_IDR(Pins)          oC_Machine_Register(PORT(Pins),GPIOx_IDR)
#define GPIOx_ODR(Pins)          oC_Machine_Register(PORT(Pins),GPIOx_ODR)
#define GPIOx_OSPEEDR(Pins)      oC_Machine_Register(PORT(Pins),GPIOx_OSPEEDR)
#define GPIOx_LCKR(Pins)         oC_Machine_Register(PORT(Pins),GPIOx_LCKR)
#define GPIOx_AFRL(Pins)         oC_Machine_Register(PORT(Pins),GPIOx_AFRL)
#define GPIOx_AFRH(Pins)         oC_Machine_Register(PORT(Pins),GPIOx_AFRH)
#define SYSCFG_EXTICR1           oC_Register(SYSCFG , SYSCFG_EXTICR1)
#define SYSCFG_EXTICR2           oC_Register(SYSCFG , SYSCFG_EXTICR2)
#define SYSCFG_EXTICR3           oC_Register(SYSCFG , SYSCFG_EXTICR3)
#define SYSCFG_EXTICR4           oC_Register(SYSCFG , SYSCFG_EXTICR4)
#define EXTI_IMR                 oC_Register(EXTI   , EXTI_IMR)
#define EXTI_EMR                 oC_Register(EXTI   , EXTI_EMR)
#define EXTI_RTSR                oC_Register(EXTI   , EXTI_RTSR)
#define EXTI_FTSR                oC_Register(EXTI   , EXTI_FTSR)
#define EXTI_PR                  oC_Register(EXTI   , EXTI_PR)
#define IsRam(Address)           (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)           oC_LSF_IsRomAddress(Address)
#define IsExtiUsed(ExtiLine)     (ExtiUsedBits &   (1<<ExtiLine))
#define SetExtiUsed(ExtiLine)     ExtiUsedBits |=  (1<<ExtiLine)
#define SetExtiUnused(ExtiLine)   ExtiUsedBits &= ~(1<<ExtiLine)

#undef  _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with redefinitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________REDEFINITIONS_SECTION______________________________________________________________________

oC_LLD_ConnectToMainDriver(GPIO,IsPortCorrect);
oC_LLD_ConnectToMainDriver(GPIO,IsPortIndexCorrect);
oC_LLD_ConnectToMainDriver(GPIO,GetPinsMaskOfPins);
oC_LLD_ConnectToMainDriver(GPIO,GetPortOfPins);
oC_LLD_ConnectToMainDriver(GPIO,PortToPortIndex);
oC_LLD_ConnectToMainDriver(GPIO,PortIndexToPort);
oC_LLD_ConnectToMainDriver(GPIO,IsPinDefined);
oC_LLD_ConnectToMainDriver(GPIO,ArePinsDefined);
oC_LLD_ConnectToMainDriver(GPIO,ArePinsCorrect);
oC_LLD_ConnectToMainDriver(GPIO,IsSinglePin);
oC_LLD_ConnectToMainDriver(GPIO,GetPinsFor);
oC_LLD_ConnectToMainDriver(GPIO,GetPortName);
oC_LLD_ConnectToMainDriver(GPIO,GetPinName);
oC_LLD_ConnectToMainDriver(GPIO,SetPower);
oC_LLD_ConnectToMainDriver(GPIO,ReadPower);
oC_LLD_ConnectToMainDriver(GPIO,CheckIsPinUsed);
oC_LLD_ConnectToMainDriver(GPIO,ArePinsUnused);
oC_LLD_ConnectToMainDriver(GPIO,WriteData);
oC_LLD_ConnectToMainDriver(GPIO,ReadData);
oC_LLD_ConnectToMainDriver(GPIO,ReadDataReference);
oC_LLD_ConnectToMainDriver(GPIO,GetHighStatePins);
oC_LLD_ConnectToMainDriver(GPIO,GetLowStatePins);
oC_LLD_ConnectToMainDriver(GPIO,IsPinsState);
oC_LLD_ConnectToMainDriver(GPIO,SetPinsState);
oC_LLD_ConnectToMainDriver(GPIO,TogglePinsState);
oC_LLD_ConnectToMainDriver(GPIO,GetPinName);

#undef  _________________________________________REDEFINITIONS_SECTION______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void             SetConfigurationBitsInRegister( oC_Register_t * outRegister , oC_PinsInPort_t PinsInPort , uint8_t Value );
static uint8_t          GetConfigurationBitsInRegister( oC_Register_t      Register , oC_PinsInPort_t PinsInPort );
static oC_ErrorCode_t   ConnectPinsToExti             ( oC_Pins_t Pins );
static oC_ErrorCode_t   DisconnectPinsFromExti        ( oC_Pins_t Pins );
static oC_Port_t        GetPortOfExtiLine             ( uint8_t ExtiLine );
static bool             SetExtiLineInterruptState     ( uint8_t ExtiLine , bool Enabled );
static bool             SetInterruptsStateForPins     ( oC_Pins_t Pins , bool Enabled );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

static oC_PinsInPort_t          UsedPinsArray[oC_ModuleChannel_NumberOfElements(GPIO)];
static oC_GPIO_LLD_Interrupt_t  InterruptHandler = NULL;
static uint16_t                 ExtiUsedBits     = 0;

#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_Module_TurnOn(oC_Module_GPIO_LLD);

        oC_GPIO_LLD_ForEachPort(port)
        {
            UsedPinsArray[PORT_INDEX(port)] = 0;
        }

        InterruptHandler = NULL;
        ExtiUsedBits     = 0;
        errorCode        = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_Module_TurnOff(oC_Module_GPIO_LLD);

        InterruptHandler  = NULL;
        errorCode         = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPortCorrect( oC_Port_t Port )
{
    return oC_Channel_IsCorrect(GPIO , Port);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPortIndexCorrect( oC_PortIndex_t PortIndex )
{
    return PortIndex < oC_ModuleChannel_NumberOfElements(GPIO);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_PinsMask_t oC_GPIO_LLD_GetPinsMaskOfPins( oC_Pins_t Pins )
{
    return PINS(Pins);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Port_t oC_GPIO_LLD_GetPortOfPins( oC_Pins_t Pins )
{
    return PORT(Pins);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_PortIndex_t oC_GPIO_LLD_PortToPortIndex( oC_Port_t Port )
{
    return oC_Channel_ToIndex(GPIO,Port);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Port_t oC_GPIO_LLD_PortIndexToPort( oC_PortIndex_t PortIndex )
{
    return oC_Channel_FromIndex(GPIO,PortIndex);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPinDefined( oC_Pin_t Pin )
{
    bool defined = false;

    oC_Pin_ForeachDefined(pin)
    {
        if(pin->Pin == Pin)
        {
            defined = true;
            break;
        }
    }

    return defined;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_ArePinsDefined( oC_Pins_t Pins )
{
    bool            allDefined = false;
    oC_Port_t       port       = PORT(Pins);
    oC_PinsInPort_t pins       = PINS(Pins);

    if(oC_Channel_IsCorrect(GPIO,port))
    {
        allDefined = true;

        for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH && allDefined; bitIndex++ )
        {
            if(pins & (1<<bitIndex))
            {
                allDefined = oC_GPIO_LLD_IsPinDefined(oC_Pins_ToSinglePin(Pins,bitIndex));
            }
        }
    }

    return allDefined;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_ArePinsCorrect( oC_Pins_t Pins )
{
    return oC_Channel_IsCorrect(GPIO,PORT(Pins));
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsSinglePin( oC_Pins_t Pins )
{
    bool            isSinglePin     = false;
    oC_PinsInPort_t pins            = PINS(Pins);

    for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH ; bitIndex++)
    {
        if(pins & (1<<bitIndex))
        {
            isSinglePin = oC_Pins_ToSinglePin(Pins,bitIndex) == Pins;
            break;
        }
    }

    return isSinglePin;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Pins_t oC_GPIO_LLD_GetPinsFor( oC_Port_t Port , oC_PinsMask_t Pins )
{
    return oC_Pin_Make(Port,Pins);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
const char * oC_GPIO_LLD_GetPortName( oC_Port_t Port )
{
    return oC_Channel_GetName(Port);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
const char * oC_GPIO_LLD_GetPinName( oC_Pin_t Pin )
{
    const char * name = "<INCORRECT_PIN>";
    oC_Port_t    port = PORT(Pin);

    if(oC_Channel_IsCorrect(GPIO,port))
    {
        name = "<UNDEFINED_PIN>";

        oC_Pin_ForeachDefined(pinData)
        {
            if(pinData->Pin == Pin)
            {
                name = pinData->Name;
                break;
            }
        }
    }

    return name;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetDriverInterruptHandler( oC_GPIO_LLD_Interrupt_t Handler )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( IsRam(Handler) || IsRom(Handler), oC_ErrorCode_WrongEventHandlerAddress) &&
            ErrorCondition( InterruptHandler == NULL,         oC_ErrorCode_InterruptHandlerAlreadySet)
            )
        {
            InterruptHandler = Handler;
            errorCode        = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_BeginConfiguration( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins)                                    , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0                                                     , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(PORT(Pins),oC_Power_On)          , oC_ErrorCode_CannotEnableChannel) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(oC_Channel_SYSCFG , oC_Power_On) , oC_ErrorCode_CannotEnableChannel)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: nothing to do in this architecture
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_FinishConfiguration( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins), oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                  oC_ErrorCode_PinNotDefined)
            )
        {
            errorCode  = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPower( oC_Pins_t Pins , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),               oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( Power  == oC_Power_On || Power == oC_Power_Off, oC_ErrorCode_PowerStateNotCorrect)
            )
        {
            if(oC_Machine_SetPowerStateForChannel(PORT(Pins) , Power ) && oC_Machine_SetPowerStateForChannel(oC_Channel_SYSCFG , oC_Power_On))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_CannotEnableChannel;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadPower( oC_Pins_t Pins , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),   oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                    oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outPower),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_Power_t powerState = oC_Machine_GetPowerStateForChannel(PORT(Pins));

            if (powerState == oC_Power_NotHandled)
            {
                *outPower = oC_Power_On;
                errorCode = oC_ErrorCode_None;
            }
            else if (powerState == oC_Power_On || powerState == oC_Power_Off)
            {
                *outPower = powerState;
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPinProtected( oC_Pins_t Pins )
{
    bool pinProtected = false;

    if(oC_Module_IsTurnedOn(oC_Module_GPIO_LLD) && IsPoweredOn(Pins))
    {
        pinProtected = GPIOx_LCKR(Pins)->Value & PINS(Pins);
    }

    return pinProtected;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_UnlockProtection( oC_Pins_t Pins , oC_GPIO_LLD_Protection_t Protection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),                           oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                            oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                                          oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( Protection == oC_GPIO_LLD_Protection_UnlockProtectedPins,   oC_ErrorCode_PinNeedUnlock)
            )
        {
            oC_MCS_EnterCriticalSection();
            volatile uint32_t oldValue = GPIOx_LCKR(Pins)->Value;
            GPIOx_LCKR(Pins)->Value = (1<<16) | ~PINS(Pins) | oldValue;
            GPIOx_LCKR(Pins)->Value = (0<<16) | ~PINS(Pins) | oldValue;
            GPIOx_LCKR(Pins)->Value = (1<<16) | ~PINS(Pins) | oldValue;

            volatile uint32_t Unused(value) = GPIOx_LCKR(Pins)->Value;

            errorCode = oC_ErrorCode_None;

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_LockProtection( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),                           oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                            oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                                          oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();
            volatile uint32_t oldValue = GPIOx_LCKR(Pins)->Value;
            GPIOx_LCKR(Pins)->Value = (1<<16) | PINS(Pins) | oldValue;
            GPIOx_LCKR(Pins)->Value = (0<<16) | PINS(Pins) | oldValue;
            GPIOx_LCKR(Pins)->Value = (1<<16) | PINS(Pins) | oldValue;

            volatile uint32_t Unused(value) = GPIOx_LCKR(Pins)->Value;

            errorCode = oC_ErrorCode_None;

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_CheckIsPinUnlocked( oC_Pins_t Pins , bool * outPinUnlocked )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),                           oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                                            oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                                          oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outPinUnlocked),                                      oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();
            *outPinUnlocked = !(GPIOx_LCKR(Pins)->Value & PINS(Pins));
            errorCode       = oC_ErrorCode_None;
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetSpeed( oC_Pins_t Pins , oC_GPIO_LLD_Speed_t Speed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();

            errorCode = oC_ErrorCode_None;

            switch(Speed)
            {
                case oC_GPIO_LLD_Speed_Minimum:
                    SetConfigurationBitsInRegister(&GPIOx_OSPEEDR(Pins)->Value , PINS(Pins) , 0);
                    break;
                case oC_GPIO_LLD_Speed_Medium:
                case oC_GPIO_LLD_Speed_Default:
                    SetConfigurationBitsInRegister(&GPIOx_OSPEEDR(Pins)->Value , PINS(Pins) , 0x1);
                    break;
                case oC_GPIO_LLD_Speed_Maximum:
                    SetConfigurationBitsInRegister(&GPIOx_OSPEEDR(Pins)->Value , PINS(Pins) , 0x3);
                    break;
                default:
                    errorCode = oC_ErrorCode_SpeedNotCorrect;
                    break;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadSpeed( oC_Pins_t Pins , oC_GPIO_LLD_Speed_t * outSpeed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outSpeed),                   oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();

            uint8_t value = GetConfigurationBitsInRegister(GPIOx_OSPEEDR(Pins)->Value , PINS(Pins));
            errorCode     = oC_ErrorCode_None;

            switch(value)
            {
                case 0: *outSpeed = oC_GPIO_LLD_Speed_Minimum;   break;
                case 1: *outSpeed = oC_GPIO_LLD_Speed_Medium;    break;
                case 2: *outSpeed = oC_GPIO_LLD_Speed_Medium;    break;
                case 3: *outSpeed = oC_GPIO_LLD_Speed_Maximum;   break;
                default:
                    errorCode = oC_ErrorCode_MachineCanBeDamaged;
                    break;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: not handled in the machine
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetCurrent( oC_Pins_t Pins , oC_GPIO_LLD_Current_t Current )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: not handled in the machine
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadCurrent( oC_Pins_t Pins , oC_GPIO_LLD_Current_t * outCurrent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outCurrent),                 oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outCurrent = oC_GPIO_LLD_Current_Maximum;
            errorCode   = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetMode( oC_Pins_t Pins , oC_GPIO_LLD_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();

            errorCode = oC_ErrorCode_None;

            switch(Mode)
            {
                case oC_GPIO_LLD_Mode_Input:
                    SetConfigurationBitsInRegister(&GPIOx_MODER(Pins)->Value , PINS(Pins) , 0);
                    break;
                case oC_GPIO_LLD_Mode_Output:
                case oC_GPIO_LLD_Mode_Default:
                    SetConfigurationBitsInRegister(&GPIOx_MODER(Pins)->Value , PINS(Pins) , 0x1);
                    break;
                case oC_GPIO_LLD_Mode_Alternate:
                    SetConfigurationBitsInRegister(&GPIOx_MODER(Pins)->Value , PINS(Pins) , 0x2);
                    break;
                default:
                    errorCode = oC_ErrorCode_ModeNotCorrect;
                    break;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadMode( oC_Pins_t Pins , oC_GPIO_LLD_Mode_t * outMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outMode),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();

            uint8_t value = GetConfigurationBitsInRegister(GPIOx_MODER(Pins)->Value , PINS(Pins));
            errorCode     = oC_ErrorCode_None;

            switch(value)
            {
                case 0: *outMode = oC_GPIO_LLD_Mode_Input;      break;
                case 1: *outMode = oC_GPIO_LLD_Mode_Output;     break;
                case 2: *outMode = oC_GPIO_LLD_Mode_Alternate;  break;
                case 3: *outMode = oC_GPIO_LLD_Mode_Alternate;  break;
                default:
                    errorCode = oC_ErrorCode_MachineCanBeDamaged;
                    break;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPull( oC_Pins_t Pins , oC_GPIO_LLD_Pull_t Pull )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();

            errorCode = oC_ErrorCode_None;

            switch(Pull)
            {
                case oC_GPIO_LLD_Pull_Down:
                    SetConfigurationBitsInRegister(&GPIOx_PUPDR(Pins)->Value , PINS(Pins) , 0x2);
                    break;
                case oC_GPIO_LLD_Pull_Up:
                    SetConfigurationBitsInRegister(&GPIOx_PUPDR(Pins)->Value , PINS(Pins) , 0x1);
                    break;
                case oC_GPIO_LLD_Mode_Default:
                    SetConfigurationBitsInRegister(&GPIOx_PUPDR(Pins)->Value , PINS(Pins) , 0);
                    break;
                default:
                    errorCode = oC_ErrorCode_PullNotCorrect;
                    break;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadPull( oC_Pins_t Pins , oC_GPIO_LLD_Pull_t * outPull )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outPull),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();

            uint8_t value = GetConfigurationBitsInRegister(GPIOx_PUPDR(Pins)->Value , PINS(Pins));
            errorCode     = oC_ErrorCode_None;

            switch(value)
            {
                case 0: *outPull = oC_GPIO_LLD_Pull_Default;    break;
                case 1: *outPull = oC_GPIO_LLD_Pull_Up;         break;
                case 2: *outPull = oC_GPIO_LLD_Pull_Down;       break;
                default:
                    errorCode = oC_ErrorCode_MachineCanBeDamaged;
                    break;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetOutputCircuit( oC_Pins_t Pins , oC_GPIO_LLD_OutputCircuit_t OutputCircuit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            oC_MCS_EnterCriticalSection();

            if(OutputCircuit == oC_GPIO_LLD_OutputCircuit_OpenDrain)
            {
                GPIOx_OTYPER(Pins)->Value |= PINS(Pins);
                errorCode                  = oC_ErrorCode_None;
            }
            else if (OutputCircuit == oC_GPIO_LLD_OutputCircuit_PushPull || OutputCircuit == oC_GPIO_LLD_OutputCircuit_Default)
            {
                GPIOx_OTYPER(Pins)->Value &= ~(PINS(Pins));
                errorCode                  = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_OutputCircuitNotCorrect;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadOutputCircuit( oC_Pins_t Pins , oC_GPIO_LLD_OutputCircuit_t * outOutputCircuit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outOutputCircuit),           oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_MCS_EnterCriticalSection();

            oC_PinsInPort_t pinsInPort  = PINS(Pins);
            uint32_t value              = GPIOx_OTYPER(Pins)->Value & pinsInPort;

            if(value == pinsInPort)
            {
                errorCode           = oC_ErrorCode_None;
                *outOutputCircuit   = oC_GPIO_LLD_OutputCircuit_OpenDrain;
            }
            else if(value == 0)
            {
                errorCode           = oC_ErrorCode_None;
                *outOutputCircuit   = oC_GPIO_LLD_OutputCircuit_PushPull;
            }
            else
            {
                errorCode = oC_ErrorCode_NotAllPinsInTheSameState;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetInterruptTrigger( oC_Pins_t Pins , oC_GPIO_LLD_IntTrigger_t InterruptTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins), oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                  oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            if(InterruptTrigger == oC_GPIO_LLD_IntTrigger_Off)
            {
                if(ErrorCondition(SetInterruptsStateForPins(Pins,false) , oC_ErrorCode_CannotDisableInterrupt))
                {
                    errorCode = DisconnectPinsFromExti(Pins);
                }
            }
            else if(oC_AssignErrorCode(&errorCode , ConnectPinsToExti(Pins)))
            {
                oC_PinsInPort_t pinsInPort = oC_Pin_GetPinsInPort(Pins);

                if(InterruptTrigger & oC_GPIO_LLD_IntTrigger_FallingEdge)
                {
                    EXTI_IMR->Value |= ((uint32_t)pinsInPort);
                    EXTI_FTSR->Value |= ((uint32_t)pinsInPort);

                    if(ErrorCondition(SetInterruptsStateForPins(Pins,true) , oC_ErrorCode_CannotEnableInterrupt))
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                }
                if(InterruptTrigger & oC_GPIO_LLD_IntTrigger_RisingEdge)
                {
                    EXTI_IMR->Value |= ((uint32_t)pinsInPort);
                    EXTI_RTSR->Value |= ((uint32_t)pinsInPort);

                    if(ErrorCondition(SetInterruptsStateForPins(Pins,true) , oC_ErrorCode_CannotEnableInterrupt))
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                }

                if(InterruptTrigger & oC_GPIO_LLD_IntTrigger_HighLevel)
                {
                    errorCode = oC_ErrorCode_NotSupportedOnTargetMachine;
                }

                if(InterruptTrigger & oC_GPIO_LLD_IntTrigger_LowLevel)
                {
                    errorCode = oC_ErrorCode_NotSupportedOnTargetMachine;
                }
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadInterruptTrigger( oC_Pins_t Pins , oC_GPIO_LLD_IntTrigger_t * outInterruptTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outInterruptTrigger),        oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            errorCode = oC_ErrorCode_NotImplemented;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPinsUsed( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel    ) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined   )
            )
        {
            oC_Bits_SetBitsU16(&UsedPinsArray[PORT_INDEX(Pins)],PINS(Pins));
            errorCode = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_SetPinsUnused( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined)
            )
        {
            oC_Bits_ClearBitsU16(&UsedPinsArray[PORT_INDEX(Pins)],PINS(Pins));
            DisconnectPinsFromExti(Pins);

            errorCode = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_CheckIsPinUsed( oC_Pins_t Pins , bool * outPinUsed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( oC_GPIO_LLD_IsSinglePin(Pins),     oC_ErrorCode_NotSinglePin) &&
            ErrorCondition( IsRam(outPinUsed),                 oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(oC_Bits_AreBitsSetU16(UsedPinsArray[PORT_INDEX(Pins)],PINS(Pins)))
            {
                *outPinUsed = true;
                errorCode   = oC_ErrorCode_None;
            }
            else if(oC_Bits_AreBitsClearU16(UsedPinsArray[PORT_INDEX(Pins)],PINS(Pins)))
            {
                *outPinUsed = false;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_NotAllPinsInTheSameState;
            }
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ArePinsUnused( oC_Pins_t Pins , bool * outPinsUnused )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outPinsUnused),              oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(oC_Bits_AreBitsSetU16(UsedPinsArray[PORT_INDEX(Pins)],PINS(Pins)))
            {
                *outPinsUnused  = false;
                errorCode       = oC_ErrorCode_None;
            }
            else if(oC_Bits_AreBitsClearU16(UsedPinsArray[PORT_INDEX(Pins)],PINS(Pins)))
            {
                *outPinsUnused  = true;
                errorCode       = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_NotAllPinsInTheSameState;
            }
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//=========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_WriteData( oC_Pins_t Pins , oC_PinsMask_t Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined)
            )
        {
            oC_PinsInPort_t pinsInPort  = PINS(Pins);
            GPIOx_ODR(Pins)->Value      = (GPIOx_ODR(Pins)->Value & ~pinsInPort) | (GPIOx_ODR(Pins)->Value & pinsInPort & Data);
            errorCode                   = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadData( oC_Pins_t Pins , oC_PinsMask_t * outData )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outData),                    oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outData = GPIOx_IDR(Pins)->Value & PINS(Pins);
            errorCode= oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_LLD_ReadDataReference( oC_Pins_t Pins , oC_UInt_t ** outDataReference )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins),  oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsPoweredOn(Pins),                 oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( PINS(Pins) != 0,                   oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(outDataReference),           oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outDataReference = (oC_UInt_t*)&GPIOx_IDR(Pins)->Value;
            errorCode         = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Pins_t oC_GPIO_LLD_GetHighStatePins( oC_Pins_t Pins )
{
    return oC_Pin_Make(PORT(Pins),(GPIOx_IDR(Pins)->Value & PINS(Pins)));
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Pins_t oC_GPIO_LLD_GetLowStatePins( oC_Pins_t Pins )
{
    return oC_Pin_Make(PORT(Pins),(~GPIOx_IDR(Pins)->Value & PINS(Pins)));
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_GPIO_LLD_IsPinsState( oC_Pins_t Pins , oC_GPIO_LLD_PinsState_t ExpectedPinsState )
{
    uint16_t pins = PINS(Pins);
    uint16_t pinsState = GPIOx_IDR(Pins)->Value;
    return oC_Bits_AreBitsSetU16(pinsState & pins,pins & ExpectedPinsState);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_GPIO_LLD_PinsState_t oC_GPIO_LLD_GetPinsState( oC_Pins_t Pins )
{
    uint16_t pins = PINS(Pins);
    uint16_t pinsState = GPIOx_IDR(Pins)->Value;

    return (oC_GPIO_LLD_PinsState_t)pinsState & pins;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_GPIO_LLD_SetPinsState( oC_Pins_t Pins , oC_GPIO_LLD_PinsState_t PinsState )
{
    oC_Bits_SetValueU32((uint32_t*)&GPIOx_ODR(Pins)->Value, PinsState & PINS(Pins) , 0 , 16);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_GPIO_LLD_TogglePinsState( oC_Pins_t Pins )
{
    GPIOx_ODR(Pins)->Value = (GPIOx_ODR(Pins)->Value & (~PINS(Pins))) | ((GPIOx_ODR(Pins)->Value ^ PINS(Pins)) & PINS(Pins));
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with machine specific functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________MSLLD_INTERFACE_FUNCTIONS_SECTION__________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_MSLLD_SetAlternateNumber( oC_Pin_t Pin , oC_PinAlternateNumber_t Function )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition(oC_GPIO_LLD_IsSinglePin(Pin)          , oC_ErrorCode_NotSinglePin) &&
            ErrorCondition(oC_GPIO_LLD_IsPinDefined(Pin)         , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsPoweredOn(Pin),                      oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition(Function <= oC_PinAlternateNumberMask , oC_ErrorCode_ValueTooBig)
            )
        {
            oC_PinsInPort_t pinsInPort = oC_Pin_GetPinsInPort(Pin);
            uint8_t         bitIndex   = oC_Bits_GetBitNumberU32(pinsInPort);
            oC_Register_t * regPointer = (bitIndex < 8) ? &GPIOx_AFRL(Pin)->Value : &GPIOx_AFRH(Pin)->Value;

            bitIndex = (bitIndex >= 8) ? bitIndex - 8 : bitIndex;

            oC_MCS_EnterCriticalSection();
            oC_Bits_SetValueU32((uint32_t*)regPointer , Function , bitIndex * 4 , (bitIndex * 4) + 3);
            oC_MCS_ExitCriticalSection();

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_MSLLD_ConnectPin( oC_Pin_t Pin , oC_PinAlternateNumber_t Function )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        oC_MCS_EnterCriticalSection();
        if(
            ErrorCondition(oC_GPIO_LLD_IsSinglePin(Pin)                                 , oC_ErrorCode_NotSinglePin) &&
            ErrorCondition(oC_GPIO_LLD_IsPinDefined(Pin)                                , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition(Function <= oC_PinAlternateNumberMask                        , oC_ErrorCode_ValueTooBig)
            )
        {

            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pin)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pin , oC_GPIO_LLD_Mode_Alternate)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetSpeed(Pin , oC_GPIO_LLD_Speed_Maximum)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPull(Pin , oC_GPIO_LLD_Pull_Up)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_SetAlternateNumber(Pin , Function     )) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pin))
                )
            {
                errorCode = oC_ErrorCode_None;
            }

        }
        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_MSLLD_DisconnectPin( oC_Pin_t Pin , oC_PinAlternateNumber_t Function )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition(oC_GPIO_LLD_IsSinglePin(Pin)          , oC_ErrorCode_NotSinglePin) &&
            ErrorCondition(oC_GPIO_LLD_IsPinDefined(Pin)         , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition(Function <= oC_PinAlternateNumberMask , oC_ErrorCode_ValueTooBig)
            )
        {
            oC_MCS_EnterCriticalSection();
            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pin)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pin , oC_GPIO_LLD_Mode_Default)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_SetAlternateNumber(Pin , 0)) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pin))
                )
            {
                errorCode = oC_ErrorCode_None;
            }
            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_MSLLD_FindModulePin( oC_Pin_t Pin , oC_Channel_t Channel , oC_PinFunction_t PinFunction , oC_ModulePinIndex_t * outModulePinIndex )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO_LLD))
    {
        if(
            ErrorCondition(oC_GPIO_LLD_IsSinglePin(Pin)          , oC_ErrorCode_NotSinglePin            ) &&
            ErrorCondition(oC_GPIO_LLD_IsPinDefined(Pin)         , oC_ErrorCode_PinNotDefined           ) &&
            ErrorCondition(IsRam(outModulePinIndex)              , oC_ErrorCode_OutputAddressNotInRAM   )
            )
        {
            errorCode = oC_ErrorCode_NoneOfModulePinAssigned;

            oC_ModulePin_ForeachDefined(pinDefinition)
            {
                if(pinDefinition->Pin == Pin && pinDefinition->PinFunction == PinFunction && pinDefinition->Channel == Channel)
                {
                    *outModulePinIndex = pinDefinition->ModulePinIndex;
                    errorCode          = oC_ErrorCode_None;
                    break;
                }
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________MSLLD_INTERFACE_FUNCTIONS_SECTION__________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief function for setting bits in GPIO configuration registers
 *
 * The function is for setting bits in most of GPIO configuration registers. It is for each one, that is in format:
 *
 * |  Pin15  |  Pin14  |  Pin13  |  Pin12  |  Pin11  |  Pin10  |  Pin9   |  Pin8   |  Pin7   |  Pin6   |  Pin5   |  Pin4   |  Pin3   |  Pin2   |  Pin1   |  Pin0   |
 * | 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |
 *
 * @param outRegister       Pointer to the register to write
 * @param PinsInPort        Pins mask to write
 * @param Value             Value in range [0-3]
 */
//==========================================================================================================================================
static void SetConfigurationBitsInRegister( oC_Register_t * outRegister , oC_PinsInPort_t PinsInPort , uint8_t Value )
{
    const uint8_t fieldWidth = 2;

    for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH ; bitIndex++ )
    {
        if(PinsInPort & (1<<bitIndex))
        {
            oC_Bits_SetValueU32((uint32_t*)outRegister,Value,bitIndex*fieldWidth,(bitIndex+1)*fieldWidth-1);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief returns configuration bits in GPIO register
 *
 * The function is for reading configuration bits in most of GPIO configuration registers. It is for each one, that is in format:
 *
 * |  Pin15  |  Pin14  |  Pin13  |  Pin12  |  Pin11  |  Pin10  |  Pin9   |  Pin8   |  Pin7   |  Pin6   |  Pin5   |  Pin4   |  Pin3   |  Pin2   |  Pin1   |  Pin0   |
 * | 31 | 30 | 29 | 28 | 27 | 26 | 25 | 24 | 23 | 22 | 21 | 20 | 19 | 18 | 17 | 16 | 15 | 14 | 13 | 12 | 11 | 10 |  9 |  8 |  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |
 *
 * The function returns the first matched value in pins list from the lowest bit.
 *
 * @param Register      Register to read bits
 * @param PinsInPort    Pins mask to read
 *
 * @return Value in range [0-3]
 */
//==========================================================================================================================================
static uint8_t GetConfigurationBitsInRegister( oC_Register_t Register , oC_PinsInPort_t PinsInPort )
{
    uint8_t       value      = 0;
    const uint8_t fieldWidth = 2;

    for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH ; bitIndex++ )
    {
        if(PinsInPort & (1<<bitIndex))
        {
            value = oC_Bits_GetValueU32((uint32_t)Register,bitIndex*fieldWidth,(bitIndex+1)*fieldWidth-1);
            break;
        }
    }

    return value;
}

//==========================================================================================================================================
/**
 * @brief connects pins to EXTI
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectPinsToExti( oC_Pins_t Pins )
{
    oC_ErrorCode_t  errorCode  = oC_ErrorCode_None;
    oC_PinsInPort_t pinsInPort = oC_Pin_GetPinsInPort(Pins);

    oC_MCS_EnterCriticalSection();

    for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH ; bitIndex++ )
    {
        if(pinsInPort & (1<<bitIndex))
        {
            if(ErrorCondition(!IsExtiUsed(bitIndex) , oC_ErrorCode_ExtiLineIsUsed))
            {
                SetExtiUsed(bitIndex);

                if(bitIndex <= 3)
                {
                    oC_Bits_SetValueU32((uint32_t*)&SYSCFG_EXTICR1->Value , oC_Channel_ToIndex(GPIO,PORT(Pins)) , bitIndex * 4 , bitIndex * 4 + 3 );
                }
                else if(bitIndex <= 7)
                {
                    oC_Bits_SetValueU32((uint32_t*)&SYSCFG_EXTICR2->Value , oC_Channel_ToIndex(GPIO,PORT(Pins)) , (bitIndex - 4) * 4 , (bitIndex - 4) * 4 + 3 );
                }
                else if(bitIndex <= 11)
                {
                    oC_Bits_SetValueU32((uint32_t*)&SYSCFG_EXTICR3->Value , oC_Channel_ToIndex(GPIO,PORT(Pins)) , (bitIndex - 8) * 4 , (bitIndex - 8) * 4 + 3 );
                }
                else
                {
                    oC_Bits_SetValueU32((uint32_t*)&SYSCFG_EXTICR4->Value , oC_Channel_ToIndex(GPIO,PORT(Pins)) , (bitIndex - 12) * 4 , (bitIndex - 12) * 4 + 3 );
                }
            }
        }
    }

    oC_MCS_ExitCriticalSection();
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disconnects pins to EXTI
 */
//==========================================================================================================================================
static oC_ErrorCode_t DisconnectPinsFromExti( oC_Pins_t Pins )
{
    oC_ErrorCode_t  errorCode  = oC_ErrorCode_None;
    oC_PinsInPort_t pinsInPort = oC_Pin_GetPinsInPort(Pins);

    for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH ; bitIndex++ )
    {
        if(pinsInPort & (1<<bitIndex))
        {
            uint8_t         startBit      = 0;
            uint8_t         endBit        = 0;
            oC_Register_t * SYSCFG_EXTICR = NULL;

            if(bitIndex <= 3)
            {
                startBit        = bitIndex * 4;
                endBit          = bitIndex * 4 + 3;
                SYSCFG_EXTICR   = &SYSCFG_EXTICR1->Value;
            }
            else if(bitIndex <= 7)
            {
                startBit        = (bitIndex - 4) * 4;
                endBit          = (bitIndex - 4) * 4 + 3;
                SYSCFG_EXTICR   = &SYSCFG_EXTICR2->Value;
            }
            else if(bitIndex <= 11)
            {
                startBit        = (bitIndex - 8) * 4;
                endBit          = (bitIndex - 8) * 4 + 3;
                SYSCFG_EXTICR   = &SYSCFG_EXTICR3->Value;
            }
            else
            {
                startBit        = (bitIndex - 12) * 4;
                endBit          = (bitIndex - 12) * 4 + 3;
                SYSCFG_EXTICR   = &SYSCFG_EXTICR4->Value;
            }

            oC_ChannelIndex_t channelIndex  = oC_Bits_GetValueU32(*SYSCFG_EXTICR,startBit,endBit);

            if(channelIndex == oC_Channel_ToIndex(GPIO,PORT(Pins)))
            {
                SetExtiUnused(bitIndex);
                oC_Bits_SetValueU32((uint32_t*)SYSCFG_EXTICR , oC_ModuleChannel_NumberOfElements(GPIO) , startBit , endBit );
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns GPIO port connected to the EXTI line
 */
//==========================================================================================================================================
static oC_Port_t GetPortOfExtiLine( uint8_t ExtiLine )
{
    uint8_t         startBit      = 0;
    uint8_t         endBit        = 0;
    oC_Register_t * SYSCFG_EXTICR = NULL;

    if(ExtiLine <= 3)
    {
        startBit        = ExtiLine * 4;
        endBit          = ExtiLine * 4 + 3;
        SYSCFG_EXTICR   = &SYSCFG_EXTICR1->Value;
    }
    else if(ExtiLine <= 7)
    {
        startBit        = (ExtiLine - 4) * 4;
        endBit          = (ExtiLine - 4) * 4 + 3;
        SYSCFG_EXTICR   = &SYSCFG_EXTICR2->Value;
    }
    else if(ExtiLine <= 11)
    {
        startBit        = (ExtiLine - 8) * 4;
        endBit          = (ExtiLine - 8) * 4 + 3;
        SYSCFG_EXTICR   = &SYSCFG_EXTICR3->Value;
    }
    else
    {
        startBit        = (ExtiLine - 12) * 4;
        endBit          = (ExtiLine - 12) * 4 + 3;
        SYSCFG_EXTICR   = &SYSCFG_EXTICR4->Value;
    }

    oC_ChannelIndex_t channelIndex  = oC_Bits_GetValueU32(*SYSCFG_EXTICR,startBit,endBit);

    return oC_Channel_FromIndex(GPIO,channelIndex);
}

//==========================================================================================================================================
/**
 * @brief enables / disables EXTI line interrupt
 */
//==========================================================================================================================================
static bool SetExtiLineInterruptState( uint8_t ExtiLine , bool Enabled )
{
    static const IRQn_Type  irqns[]     = {
                    EXTI0EXTI_IRQn ,
                    EXTI1EXTI_IRQn ,
                    EXTI2EXTI_IRQn ,
                    EXTI3EXTI_IRQn ,
                    EXTI4EXTI_IRQn ,
                    EXTI5_9EXTI_IRQn ,
                    EXTI5_9EXTI_IRQn ,
                    EXTI5_9EXTI_IRQn ,
                    EXTI5_9EXTI_IRQn ,
                    EXTI5_9EXTI_IRQn ,
                    EXTI10_15EXTI_IRQn ,
                    EXTI10_15EXTI_IRQn ,
                    EXTI10_15EXTI_IRQn ,
                    EXTI10_15EXTI_IRQn ,
                    EXTI10_15EXTI_IRQn ,
                    EXTI10_15EXTI_IRQn
    };

    oC_ASSERT( ExtiLine < oC_ARRAY_SIZE(irqns) );

    return Enabled ? oC_MCS_EnableInterrupt(irqns[ExtiLine]) : oC_MCS_DisableInterrupt(irqns[ExtiLine]);
}

//==========================================================================================================================================
/**
 * @brief enables interrupts for pins
 */
//==========================================================================================================================================
static bool SetInterruptsStateForPins( oC_Pins_t Pins , bool Enabled )
{
    oC_PinsInPort_t pinsInPort = oC_Pin_GetPinsInPort(Pins);
    oC_Port_t       port       = oC_Pin_GetPort(Pins);
    bool            allEnabled = true;

    for(uint8_t bitIndex = 0; bitIndex < oC_PORT_WIDTH ; bitIndex++ )
    {
        if(pinsInPort & (1<<bitIndex))
        {
            oC_Port_t extiPort = GetPortOfExtiLine(bitIndex);

            if(port == extiPort)
            {
                allEnabled = allEnabled && SetExtiLineInterruptState(bitIndex , Enabled);
            }
            else if (Enabled)
            {
                allEnabled = false;
            }
        }
    }

    return allEnabled;
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Interrupt handler for EXTI line 0
 */
//==========================================================================================================================================
oC_InterruptHandler(EXTI,EXTI0)
{
    oC_Port_t port = GetPortOfExtiLine(0);

    if((IsRam(InterruptHandler) || IsRom(InterruptHandler)) && oC_Channel_IsCorrect(GPIO,port))
    {
        InterruptHandler( oC_Pin_Make( port , EXTI_PR->Value ) );
    }

    EXTI_PR->Value |= (1<<0);
}

//==========================================================================================================================================
/**
 * @brief Interrupt handler for EXTI line 1
 */
//==========================================================================================================================================
oC_InterruptHandler(EXTI,EXTI1)
{
    oC_Port_t port = GetPortOfExtiLine(1);

    if((IsRam(InterruptHandler) || IsRom(InterruptHandler)) && oC_Channel_IsCorrect(GPIO,port))
    {
        InterruptHandler( oC_Pin_Make( port , EXTI_PR->Value ) );
    }

    EXTI_PR->Value |= (1<<1);
}

//==========================================================================================================================================
/**
 * @brief Interrupt handler for EXTI line 2
 */
//==========================================================================================================================================
oC_InterruptHandler(EXTI,EXTI2)
{
    oC_Port_t port = GetPortOfExtiLine(2);

    if((IsRam(InterruptHandler) || IsRom(InterruptHandler)) && oC_Channel_IsCorrect(GPIO,port))
    {
        InterruptHandler( oC_Pin_Make( port , EXTI_PR->Value ) );
    }

    EXTI_PR->Value |= (1<<2);
}

//==========================================================================================================================================
/**
 * @brief Interrupt handler for EXTI line 3
 */
//==========================================================================================================================================
oC_InterruptHandler(EXTI,EXTI3)
{
    oC_Port_t port = GetPortOfExtiLine(3);

    if((IsRam(InterruptHandler) || IsRom(InterruptHandler)) && oC_Channel_IsCorrect(GPIO,port))
    {
        InterruptHandler( oC_Pin_Make( port , EXTI_PR->Value ) );
    }

    EXTI_PR->Value |= (1<<3);
}

//==========================================================================================================================================
/**
 * @brief Interrupt handler for EXTI line 4
 */
//==========================================================================================================================================
oC_InterruptHandler(EXTI,EXTI4)
{
    oC_Port_t port = GetPortOfExtiLine(4);

    if((IsRam(InterruptHandler) || IsRom(InterruptHandler)) && oC_Channel_IsCorrect(GPIO,port))
    {
        InterruptHandler( oC_Pin_Make( port , EXTI_PR->Value ) );
    }

    EXTI_PR->Value |= (1<<4);
}

//==========================================================================================================================================
/**
 * @brief Interrupt handler for EXTI lines 5 to 9
 */
//==========================================================================================================================================
oC_InterruptHandler(EXTI,EXTI5_9)
{
    if((IsRam(InterruptHandler) || IsRom(InterruptHandler)))
    {
        for(uint8_t extiLine = 5; extiLine < 10 ; extiLine++)
        {
            oC_Port_t port = GetPortOfExtiLine(extiLine);

            if(oC_Channel_IsCorrect(GPIO,port) && oC_Bits_IsBitSetU32(EXTI_PR->Value,extiLine))
            {
                InterruptHandler( oC_Pin_Make( port , 1<<extiLine ) );
            }

        }

    }

    EXTI_PR->Value |= oC_Bits_Mask_U32(5,9);
}

//==========================================================================================================================================
/**
 * @brief Interrupt handler for EXTI lines 10 to 15
 */
//==========================================================================================================================================
oC_InterruptHandler(EXTI,EXTI10_15)
{
    if((IsRam(InterruptHandler) || IsRom(InterruptHandler)))
    {
        for(uint8_t extiLine = 10; extiLine < 16 ; extiLine++)
        {
            oC_Port_t port = GetPortOfExtiLine(extiLine);

            if(oC_Channel_IsCorrect(GPIO,port) && oC_Bits_IsBitSetU32(EXTI_PR->Value,extiLine))
            {
                InterruptHandler( oC_Pin_Make( port , 1<<extiLine ) );
            }
        }

    }

    EXTI_PR->Value |= oC_Bits_Mask_U32(10,15);
}

#undef  _________________________________________INTERRUPTS_SECTION________________________________________________________________________

