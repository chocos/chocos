/** ****************************************************************************************************************************************
 *
 * @file       oc_uart_lld.c
 *
 * @brief      The file with source for UART LLD functions
 *
 * @author     Patryk Kubiak - (Created on: 20 09 2015 10:31:48)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_uart_lld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_gpio_mslld.h>
#include <oc_array.h>
#include <oc_clock_lld.h>
#include <oc_dma_lld.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <string.h>
#include <oc_compiler.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define USE_INTERRUPTS_FOR_TX               false
#define SOFTWARE_RING_COUNT                 255
#define IsRam(Address)                      (oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address))
#define IsRom(Address)                      oC_LSF_IsRomAddress(Address)
#define IsChannelCorrect(Channel)           oC_Channel_IsCorrect(UART,Channel)
#define IsChannelPoweredOn(Channel)         (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)
#define AreOperationsDisabled(Channel)      (USARTx_CR1(Channel)->UE == 0)
#define AreOperationsEnabled(Channel)       (USARTx_CR1(Channel)->UE == 1)
#define USARTx_CR1(Channel)                 oC_Machine_Register(Channel , USARTx_CR1)
#define USARTx_CR2(Channel)                 oC_Machine_Register(Channel , USARTx_CR2)
#define USARTx_CR3(Channel)                 oC_Machine_Register(Channel , USARTx_CR3)
#define USARTx_BRR(Channel)                 oC_Machine_Register(Channel , USARTx_BRR)
#define USARTx_TDR(Channel)                 oC_Machine_Register(Channel , USARTx_TDR)
#define USARTx_RDR(Channel)                 oC_Machine_Register(Channel , USARTx_RDR)
#define USARTx_ISR(Channel)                 oC_Machine_Register(Channel , USARTx_ISR)
#define USARTx_ICR(Channel)                 oC_Machine_Register(Channel , USARTx_ICR)
#define TransmitDataMovedToFifo(Channel)    (USARTx_ISR(Channel)->TXE == 1)
#define ReceiveDataAvailable(Channel)       (USARTx_ISR(Channel)->RXNE == 1)
#define RCC_DCKCFGR2                        oC_Register(RCC,RCC_DCKCFGR2)
#define RxRing(Channel)                     SoftwareRxRings[oC_Channel_ToIndex(UART,Channel)]
#define IsSoftwareRxRingEmpty(Channel)      (RxRing(Channel).Counter == 0)
#define IsSoftwareRxRingFull(Channel)       (RxRing(Channel).Counter == SOFTWARE_RING_COUNT)
#define TxRing(Channel)                     SoftwareTxRings[oC_Channel_ToIndex(UART,Channel)]
#define IsSoftwareTxRingEmpty(Channel)      (TxRing(Channel).Counter == 0)
#define IsSoftwareTxRingFull(Channel)       (TxRing(Channel).Counter == SOFTWARE_RING_COUNT)


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    uint16_t            Buffer[SOFTWARE_RING_COUNT];
    uint16_t            PutIndex;
    uint16_t            GetIndex;
    uint16_t            Counter;
} SoftwareRing_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline void     PutToSoftwareRing  ( SoftwareRing_t * Ring , uint16_t Data );
static inline uint16_t GetFromSoftwareRing( SoftwareRing_t * Ring );
static inline void     ClearSoftwareRing  ( SoftwareRing_t * Ring );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_UART_LLD_Interrupt_t RxNotEmptyHandler  = NULL;
static oC_UART_LLD_Interrupt_t TxNotFullHandler   = NULL;
static SoftwareRing_t          SoftwareRxRings[oC_ModuleChannel_NumberOfElements(UART)];
static SoftwareRing_t          SoftwareTxRings[oC_ModuleChannel_NumberOfElements(UART)];
#if oC_ModuleChannel_NumberOfElements(UART) <= 8
static uint8_t                 UsedChannels     = 0;
#elif oC_ModuleChannel_NumberOfElements(UART) <= 16
static uint16_t                UsedChannels     = 0;
#elif oC_ModuleChannel_NumberOfElements(UART) <= 32
static uint32_t                UsedChannels     = 0;
#elif oC_ModuleChannel_NumberOfElements(UART) <= 64
static uint64_t                UsedChannels     = 0;
#else
#   error Number of UART channels is too big (max 64)
#endif

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelCorrect( oC_UART_Channel_t Channel )
{
    return oC_Channel_IsCorrect(UART,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelIndexCorrect( oC_UART_LLD_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(UART);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_UART_LLD_ChannelIndex_t oC_UART_LLD_ChannelToChannelIndex( oC_UART_Channel_t Channel )
{
    return oC_Channel_ToIndex(UART,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_UART_Channel_t oC_UART_LLD_ChannelIndexToChannel( oC_UART_LLD_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex(UART,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_UART_Channel_t oC_UART_LLD_GetChannelOfModulePin( oC_UART_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_Module_TurnOn(oC_Module_UART_LLD);
        errorCode           = oC_ErrorCode_None;
        RxNotEmptyHandler   = NULL;
        TxNotFullHandler    = NULL;
        UsedChannels        = 0;

        memset(SoftwareRxRings,0,sizeof(SoftwareRxRings));
        memset(SoftwareTxRings,0,sizeof(SoftwareTxRings));
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_Module_TurnOff(oC_Module_UART_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetDriverInterruptHandlers( oC_UART_LLD_Interrupt_t RxNotEmpty , oC_UART_LLD_Interrupt_t TxNotFull)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsRam(RxNotEmpty) || IsRom(RxNotEmpty)  , oC_ErrorCode_WrongEventHandlerAddress) &&
            ErrorCondition( RxNotEmptyHandler  == NULL              , oC_ErrorCode_InterruptHandlerAlreadySet)  &&
            ErrorCondition( IsRam(TxNotFull) || IsRom(TxNotFull)    , oC_ErrorCode_WrongEventHandlerAddress) &&
            ErrorCondition( TxNotFullHandler  == NULL               , oC_ErrorCode_InterruptHandlerAlreadySet)
            )
        {
            RxNotEmptyHandler   = RxNotEmpty;
            TxNotFullHandler    = TxNotFull;
            errorCode           = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetPower( oC_UART_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                               , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( Power == oC_Power_On || Power == oC_Power_Off           , oC_ErrorCode_PowerStateNotCorrect) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,Power)       , oC_ErrorCode_CannotEnableChannel)  &&
            ErrorCondition( oC_Channel_EnableInterrupt(Channel,PeripheralInterrupt) , oC_ErrorCode_CannotEnableInterrupt)
            )
        {
            oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(UART,Channel);

            oC_ASSERT(channelIndex < oC_ModuleChannel_NumberOfElements(UART));

            RCC_DCKCFGR2->Value &= ~(0x3<<(channelIndex*2));
            RCC_DCKCFGR2->Value |=   0x1<<(channelIndex*2);

            oC_Channel_SetInterruptPriority(Channel,PeripheralInterrupt,oC_InterruptPriority_Maximum);

            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadPower( oC_UART_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsRam(outPower)           , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outPower = oC_Machine_GetPowerStateForChannel(Channel);
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_DisableOperations( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            USARTx_CR1(Channel)->UE = 0;
            errorCode               = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_EnableOperations( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn)
            )
        {
            USARTx_CR1(Channel)->UE     = 1;
            USARTx_CR1(Channel)->TE     = 1;
            USARTx_CR1(Channel)->RE     = 1;
            USARTx_CR1(Channel)->RXNEIE = 1;
            errorCode                   = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_RestoreDefaultStateOnChannel( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)                                , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,oC_Power_Off) , oC_ErrorCode_CannotRestoreDefaultState)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetWordLength( oC_UART_Channel_t Channel , oC_UART_LLD_WordLength_t WordLength )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            switch(WordLength)
            {
                case oC_UART_LLD_WordLength_5Bits:
                    errorCode = oC_ErrorCode_NotSupportedOnTargetMachine;
                    break;
                case oC_UART_LLD_WordLength_6Bits:
                    errorCode = oC_ErrorCode_NotSupportedOnTargetMachine;
                    break;
                case oC_UART_LLD_WordLength_7Bits:
                    USARTx_CR1(Channel)->M0 = 0;
                    USARTx_CR1(Channel)->M1 = 1;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_WordLength_8Bits:
                    USARTx_CR1(Channel)->M0 = 0;
                    USARTx_CR1(Channel)->M1 = 0;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_WordLength_9Bits:
                    USARTx_CR1(Channel)->M0 = 1;
                    USARTx_CR1(Channel)->M1 = 0;

                    /* ***************************************************************** **
                     * Data in UART transmit/receive functions are given by char (uint8),
                     * Thanks to that transmission functions is quite simple. We will keep
                     * this until it is not very important. If you need 9bits transmission,
                     * please tell us with explanation, and we will try to fix this problem.
                     * ***************************************************************** */
                    errorCode = oC_ErrorCode_ProhibitedByArchitecture;
                    break;
                default:
                    errorCode = oC_ErrorCode_WordLengthNotCorrect;
                    break;
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadWordLength( oC_UART_Channel_t Channel , oC_UART_LLD_WordLength_t * outWordLength )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)   , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel) , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outWordLength)        , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if( USARTx_CR1(Channel)->M0 == 0 && USARTx_CR1(Channel)->M1 == 0 )
            {
                *outWordLength  = oC_UART_LLD_WordLength_8Bits;
                errorCode       = oC_ErrorCode_None;
            }
            else if( USARTx_CR1(Channel)->M0 == 0 && USARTx_CR1(Channel)->M1 == 1 )
            {
                *outWordLength  = oC_UART_LLD_WordLength_7Bits;
                errorCode       = oC_ErrorCode_None;
            }
            else if( USARTx_CR1(Channel)->M0 == 1 && USARTx_CR1(Channel)->M1 == 0 )
            {
                *outWordLength  = oC_UART_LLD_WordLength_9Bits;
                errorCode       = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetBitRate( oC_UART_Channel_t Channel , uint32_t BitRate )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled) &&
            ErrorCondition( BitRate > 0                     , oC_ErrorCode_BitRateNotCorrect)
            )
        {
            oC_Frequency_t clockFrequency   = oC_CLOCK_LLD_GetClockFrequency();
            uint32_t       usartDiv         = (clockFrequency / BitRate);

            if(usartDiv <= UINT16_MAX)
            {
                USARTx_CR1(Channel)->OVER8  = 0;
                USARTx_BRR(Channel)->BRR    = usartDiv;
                errorCode                   = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_BitRateNotSupported;
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadBitRate( oC_UART_Channel_t Channel , uint32_t * outBitRate )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outBitRate)               , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_Frequency_t clockFrequency   = oC_CLOCK_LLD_GetClockFrequency();
            uint32_t       usartDiv         = USARTx_BRR(Channel)->BRR;

            if(usartDiv > 0)
            {
                *outBitRate = clockFrequency / usartDiv;
            }
            else
            {
                *outBitRate = 0;
            }

            errorCode   = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetParity( oC_UART_Channel_t Channel , oC_UART_LLD_Parity_t Parity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            switch(Parity)
            {
                case oC_UART_LLD_Parity_None:
                    USARTx_CR1(Channel)->PCE = 0;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_Parity_Even:
                    USARTx_CR1(Channel)->PCE = 1;
                    USARTx_CR1(Channel)->PS  = 0;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_Parity_Odd:
                    USARTx_CR1(Channel)->PCE = 1;
                    USARTx_CR1(Channel)->PS  = 1;
                    errorCode = oC_ErrorCode_None;
                    break;
                default:
                    errorCode = oC_ErrorCode_ParityNotCorrect;
                    break;
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadParity( oC_UART_Channel_t Channel , oC_UART_LLD_Parity_t * outParity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outParity)                , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(USARTx_CR1(Channel)->PCE == 0)
            {
                *outParity = oC_UART_LLD_Parity_None;
                errorCode  = oC_ErrorCode_None;
            }
            else if(USARTx_CR1(Channel)->PS == 0)
            {
                *outParity = oC_UART_LLD_Parity_Even;
                errorCode  = oC_ErrorCode_None;
            }
            else if(USARTx_CR1(Channel)->PS == 1)
            {
                *outParity = oC_UART_LLD_Parity_Odd;
                errorCode  = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetStopBit( oC_UART_Channel_t Channel , oC_UART_LLD_StopBit_t StopBit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            switch(StopBit)
            {
                case oC_UART_LLD_StopBit_0p5Bit:
                    USARTx_CR2(Channel)->STOP = 1;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_StopBit_1Bit:
                    USARTx_CR2(Channel)->STOP = 0;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_StopBit_1p5Bits:
                    USARTx_CR2(Channel)->STOP = 3;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_StopBit_2Bits:
                    USARTx_CR2(Channel)->STOP = 2;
                    errorCode = oC_ErrorCode_None;
                    break;
                default:
                    errorCode = oC_ErrorCode_StopBitNotSupported;
                    break;
            }
        }
    }

    oC_MCS_ExitCriticalSection();


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadStopBit( oC_UART_Channel_t Channel , oC_UART_LLD_StopBit_t * outStopBit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outStopBit)               , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(USARTx_CR2(Channel)->STOP == 1)
            {
                *outStopBit = oC_UART_LLD_StopBit_0p5Bit;
                errorCode   = oC_ErrorCode_None;
            }
            else if(USARTx_CR2(Channel)->STOP == 0)
            {
                *outStopBit = oC_UART_LLD_StopBit_1Bit;
                errorCode   = oC_ErrorCode_None;
            }
            else if(USARTx_CR2(Channel)->STOP == 3)
            {
                *outStopBit = oC_UART_LLD_StopBit_1p5Bits;
                errorCode   = oC_ErrorCode_None;
            }
            else if(USARTx_CR2(Channel)->STOP == 2)
            {
                *outStopBit = oC_UART_LLD_StopBit_2Bits;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetBitOrder( oC_UART_Channel_t Channel , oC_UART_LLD_BitOrder_t BitOrder )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            switch(BitOrder)
            {
                case oC_UART_LLD_BitOrder_LSBFirst:
                    USARTx_CR2(Channel)->MSBFIRST = 0;
                    errorCode = oC_ErrorCode_None;
                    break;
                case oC_UART_LLD_BitOrder_MSBFirst:
                    USARTx_CR2(Channel)->MSBFIRST = 1;
                    errorCode = oC_ErrorCode_None;
                    break;
                default:
                    errorCode = oC_ErrorCode_BitOrderNotSupported;
                    break;
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadBitOrder( oC_UART_Channel_t Channel , oC_UART_LLD_BitOrder_t * outBitOrder )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outBitOrder)              , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(USARTx_CR2(Channel)->MSBFIRST == 0)
            {
                *outBitOrder= oC_UART_LLD_BitOrder_LSBFirst;
                errorCode   = oC_ErrorCode_None;
            }
            else if(USARTx_CR2(Channel)->MSBFIRST == 1)
            {
                *outBitOrder= oC_UART_LLD_BitOrder_MSBFirst;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetInvert( oC_UART_Channel_t Channel , oC_UART_LLD_Invert_t Invert )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            if(Invert == oC_UART_LLD_Invert_Inverted)
            {
                USARTx_CR2(Channel)->TXINV  = 1;
                USARTx_CR2(Channel)->RXINV  = 1;
                errorCode = oC_ErrorCode_None;
            }
            else if(Invert == oC_UART_LLD_Invert_NotInverted)
            {
                USARTx_CR2(Channel)->TXINV  = 0;
                USARTx_CR2(Channel)->RXINV  = 0;
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_InvertNotSupported;
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadInvert( oC_UART_Channel_t Channel , oC_UART_LLD_Invert_t * outInvert )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outInvert)                , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            if(USARTx_CR2(Channel)->TXINV == 0 && USARTx_CR2(Channel)->RXINV == 0)
            {
                *outInvert  = oC_UART_LLD_Invert_NotInverted;
                errorCode   = oC_ErrorCode_None;
            }
            else if(USARTx_CR2(Channel)->TXINV == 1 && USARTx_CR2(Channel)->RXINV == 1)
            {
                *outInvert  = oC_UART_LLD_Invert_Inverted;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_MachineCanBeDamaged;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetLoopback( oC_UART_Channel_t Channel , bool Loopback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsDisabled(Channel)  , oC_ErrorCode_ChannelOperationsNotDisabled)
            )
        {
            if(!Loopback)
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_LoopbackNotSupported;
            }
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadLoopback( oC_UART_Channel_t Channel , bool * outLoopback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)       , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsChannelPoweredOn(Channel)     , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( IsRam(outLoopback)              , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            *outLoopback = false;
            errorCode    = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ConnectModulePin( oC_UART_Pin_t ModulePin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_UART_Channel_t channel = oC_ModulePin_GetChannel(ModulePin);
        oC_Pin_t          pin     = oC_ModulePin_GetPin(ModulePin);

        if(
            ErrorCondition( IsChannelCorrect(channel)       , oC_ErrorCode_WrongChannel  ) &&
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(pin)   , oC_ErrorCode_PinNotDefined )
            )
        {
            bool unused = false;

            oC_MCS_EnterCriticalSection();

            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ArePinsUnused(pin,&unused)      ) &&
                ErrorCondition(        unused , oC_ErrorCode_PinIsUsed                              ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(pin)                ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_ConnectModulePin(ModulePin)   )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_GPIO_LLD_SetPinsUnused(pin);
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_DisconnectModulePin( oC_UART_Pin_t ModulePin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_UART_Channel_t channel = oC_ModulePin_GetChannel(ModulePin);
        oC_Pin_t          pin     = oC_ModulePin_GetPin(ModulePin);

        if(
            ErrorCondition( IsChannelCorrect(channel)       , oC_ErrorCode_WrongChannel  ) &&
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(pin)   , oC_ErrorCode_PinNotDefined )
            )
        {
            oC_MCS_EnterCriticalSection();

            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUnused(pin)                 ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_DisconnectModulePin(ModulePin)   )
                )
            {
                errorCode = oC_ErrorCode_None;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetChannelUsed( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if( ErrorCondition(IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) )
        {
            UsedChannels |= (1<<oC_Channel_ToIndex(UART,Channel));
            errorCode     = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_SetChannelUnused( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        oC_MCS_EnterCriticalSection();

        if( ErrorCondition(IsChannelCorrect(Channel) , oC_ErrorCode_WrongChannel) )
        {
            UsedChannels &= ~(1<<oC_Channel_ToIndex(UART,Channel));
            errorCode     = oC_ErrorCode_None;
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsChannelUsed( oC_UART_Channel_t Channel )
{
    bool used = false;

    if(oC_Module_IsTurnedOn(oC_Module_UART_LLD))
    {
        if(IsChannelCorrect(Channel))
        {
            used = (UsedChannels & (1<<oC_Channel_ToIndex(UART,Channel))) == (1<<oC_Channel_ToIndex(UART,Channel));
        }
    }

    return used;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadModulePinsOfPin( oC_Pin_t Pin , oC_UART_Pin_t * outModulePinsArray , uint32_t * ArraySize , oC_UART_PinFunction_t PinFunction )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {

        if(
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(Pin)                          , oC_ErrorCode_PinNotDefined) &&
            ErrorCondition( IsRam(ArraySize)                                       , oC_ErrorCode_OutputAddressNotInRAM) &&
            ErrorCondition(outModulePinsArray == NULL || IsRam(outModulePinsArray) , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            uint32_t foundPins = 0;

            errorCode = oC_ErrorCode_None;

            oC_ModulePin_ForeachDefined(modulePin)
            {
                if(modulePin->Pin == Pin && oC_Channel_IsCorrect(UART,modulePin->Channel) && modulePin->PinFunction == PinFunction)
                {
                    if(outModulePinsArray != NULL)
                    {
                        if(foundPins < *ArraySize)
                        {
                            outModulePinsArray[foundPins] = modulePin->ModulePinIndex;
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_OutputArrayToSmall;
                        }
                    }
                    foundPins++;
                }
            }

            *ArraySize = foundPins;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ClearRxFifo( oC_UART_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Channel_IsCorrect(UART,Channel) , oC_ErrorCode_WrongChannel ))
    {
        ClearSoftwareRing(&RxRing(Channel));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsTxFifoFull( oC_UART_Channel_t Channel )
{
#if USE_INTERRUPTS_FOR_TX
    return IsSoftwareTxRingFull(Channel);
#else
    return false;
#endif
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_UART_LLD_IsRxFifoEmpty( oC_UART_Channel_t Channel )
{
    return IsSoftwareRxRingEmpty(Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
void oC_UART_LLD_PutData( oC_UART_Channel_t Channel , char Data )
{
#if USE_INTERRUPTS_FOR_TX
    oC_MCS_EnterCriticalSection();
    USARTx_CR1(Channel)->TCIE  = 1;

    if(IsSoftwareTxRingEmpty(Channel) && TransmitDataMovedToFifo(Channel))
    {
        USARTx_TDR(Channel)->TDR = Data;
    }
    else
    {
        PutToSoftwareRing(&TxRing(Channel),Data);
    }
    oC_MCS_ExitCriticalSection();
#else
    while(TransmitDataMovedToFifo(Channel) != true);
    USARTx_TDR(Channel)->TDR = Data;
#endif
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
char oC_UART_LLD_GetData( oC_UART_Channel_t Channel )
{
    return (char)GetFromSoftwareRing(&RxRing(Channel));
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_WriteWithDma( oC_UART_Channel_t Channel , const char * Buffer , oC_UInt_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)        , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsRam(Buffer) || IsRom(Buffer)   , oC_ErrorCode_WrongAddress) &&
            ErrorCondition( Size > 0                         , oC_ErrorCode_SizeNotCorrect) &&
            ErrorCondition( IsChannelPoweredOn(Channel)      , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsEnabled(Channel)    , oC_ErrorCode_ChannelOperationsNotEnabled)
            )
        {
            errorCode = oC_ErrorCode_NotImplemented;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_LLD_ReadWithDma( oC_UART_Channel_t Channel , char * outBuffer , oC_UInt_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_UART_LLD))
    {
        if(
            ErrorCondition( IsChannelCorrect(Channel)        , oC_ErrorCode_WrongChannel) &&
            ErrorCondition( IsRam(outBuffer)                 , oC_ErrorCode_OutputAddressNotInRAM) &&
            ErrorCondition( Size > 0                         , oC_ErrorCode_SizeNotCorrect) &&
            ErrorCondition( IsChannelPoweredOn(Channel)      , oC_ErrorCode_ChannelNotPoweredOn) &&
            ErrorCondition( AreOperationsEnabled(Channel)    , oC_ErrorCode_ChannelOperationsNotEnabled)
            )
        {
            errorCode = oC_ErrorCode_NotImplemented;
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief puts data to software ring
 */
//==========================================================================================================================================
static inline void PutToSoftwareRing( SoftwareRing_t * Ring , uint16_t Data )
{
    if(Ring->Counter >= SOFTWARE_RING_COUNT)
    {
        /* If ring is full, take the oldest data */
        GetFromSoftwareRing(Ring);
    }

    Ring->Buffer[Ring->PutIndex++] = Data;
    Ring->Counter++;

    if(Ring->PutIndex >= SOFTWARE_RING_COUNT)
    {
        Ring->PutIndex = 0;
    }
}

//==========================================================================================================================================
/**
 * @brief gets data from software ring
 */
//==========================================================================================================================================
static inline uint16_t GetFromSoftwareRing( SoftwareRing_t * Ring )
{
    uint16_t data = 0xFFFF;

    if(Ring->Counter > 0)
    {
        Ring->Counter--;
        data = Ring->Buffer[Ring->GetIndex++];
        if(Ring->GetIndex >= SOFTWARE_RING_COUNT)
        {
            Ring->GetIndex = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief clears data in software ring
 */
//==========================================================================================================================================
static inline void ClearSoftwareRing( SoftwareRing_t * Ring )
{
    memset(Ring,0,sizeof(SoftwareRing_t));
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

#define MODULE_NAME         UART
#define INTERRUPT_TYPE_NAME PeripheralInterrupt

//==========================================================================================================================================
/**
 * @brief common handler for UART interrupts
 */
//==========================================================================================================================================
oC_Channel_InterruptHandler(Channel)
{
    if(USARTx_ISR(Channel)->TC == 1)
    {
        while(IsSoftwareTxRingEmpty(Channel) == false && (TransmitDataMovedToFifo(Channel)))
        {
            USARTx_TDR(Channel)->TDR = GetFromSoftwareRing(&TxRing(Channel));
        }

        if(TxNotFullHandler)
        {
            TxNotFullHandler(Channel);
        }

        if(IsSoftwareTxRingEmpty(Channel))
        {
            USARTx_CR1(Channel)->TCIE  = 0;
        }
    }

    if(USARTx_ISR(Channel)->RXNE == 1)
    {
        while(ReceiveDataAvailable(Channel))
        {
            PutToSoftwareRing(&RxRing(Channel),USARTx_RDR(Channel)->RDR);
        }

        if(RxNotEmptyHandler)
        {
            RxNotEmptyHandler(Channel);
        }
    }

    USARTx_ICR(Channel)->Value = 0x21B5F;
}

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

