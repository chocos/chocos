/** ****************************************************************************************************************************************
 *
 * @file       oc_dma_lld.c
 *
 * @brief      The file with source code for DMA-LLD
 *
 * @author     Patryk Kubiak - (Created on: 28 08 2015 15:52:50)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#include <oc_dma_lld.h>
#include <oc_mem_lld.h>
#include <oc_math.h>
#include <oc_array.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION___________________________________________________________________

static bool                         ModuleEnabledFlag = false;
static bool                         ChannelUsedArray[oC_ModuleChannel_NumberOfElements(DMA)];

#undef  _________________________________________LOCAL_VARIABLES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;



    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsChannelAvailable( oC_DMA_Channel_t Channel )
{
    return ChannelUsedArray[oC_Channel_ToIndex(DMA,Channel)] == false;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
void oC_DMA_LLD_RestoreDefaultStateOnChannel( oC_DMA_Channel_t Channel )
{

}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsChannelSupportedOnDmaChannel( oC_DMA_Channel_t DmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType )
{
    bool registerMapSupported = false;


    return registerMapSupported;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsSoftwareTradeSupportedOnDmaChannel( oC_DMA_Channel_t Channel )
{
    bool softwareTradeSupported = false;

    return softwareTradeSupported;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_DoesDmaHasAccessToAddress( oC_DMA_Channel_t Channel , const void * Address )
{
    return oC_MEM_LLD_IsRamAddress(Address);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_ConfigureSoftwareTrade( oC_DMA_Channel_t Channel , const oC_DMA_LLD_SoftwareTradeConfig_t * Config )
{
    oC_ErrorCode_t              errorCode               = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_ConfigurePeripheralTrade( oC_DMA_Channel_t Channel , const oC_DMA_LLD_PeripheralTradeConfig_t * Config )
{
    oC_ErrorCode_t            errorCode            = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
bool oC_DMA_LLD_IsTransferCompleteOnChannel( oC_DMA_Channel_t Channel )
{
    oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(DMA,Channel);

    return ModuleEnabledFlag && oC_Channel_IsCorrect(DMA,Channel) && (ChannelUsedArray[channelIndex] == false);
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_ReadChannelUsedReference( oC_DMA_Channel_t Channel , bool ** outChannelUsedFlag )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;



    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **lm4f** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade( oC_DMA_Channel_t * outDmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag , oC_ErrorCode_ModuleNotStartedYet)
        )
    {
        errorCode = oC_ErrorCode_NoChannelAvailable;

        oC_ARRAY_FOREACH_IN_ARRAY(oC_DmaChannelAssignments,DmaChannelAssignment)
        {
            if(
                (oC_Machine_DmaChannelAssignment_GetChannel(       *DmaChannelAssignment) == Channel )  &&
                (oC_Machine_DmaChannelAssignment_GetDmaSignalType( *DmaChannelAssignment) == SignalType       )
                )
            {
                *outDmaChannel  = oC_Machine_DmaChannelAssignment_GetDmaChannel(*DmaChannelAssignment);
                errorCode       = oC_ErrorCode_None;
                oC_ARRAY_FOREACH_BREAK(oC_DmaChannelAssignments);
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_INTERRUPTS_SECTION___________________________________________________________________


#undef  _________________________________________LOCAL_INTERRUPTS_SECTION___________________________________________________________________
