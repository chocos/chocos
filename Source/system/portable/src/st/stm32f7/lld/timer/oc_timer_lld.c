/** ****************************************************************************************************************************************
 *
 * @file       oc_timer_lld.c
 *
 * @brief      File with source for TIMER LLD functions
 *
 * @author     Patryk Kubiak - (Created on: 16 08 2015 09:22:23)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_timer_lld.h>
#include <oc_mem_lld.h>
#include <oc_clock_lld.h>
#include <oc_gpio_lld.h>
#include <oc_gpio_mslld.h>

#include <oc_math.h>
#include <oc_array.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

#undef  _________________________________________LOCAL_MACROS_SECTION_______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with global variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________



#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION_________________________________________________________________

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION_________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsChannelCorrect( oC_TIMER_Channel_t Channel )
{
    return oC_Channel_IsCorrect(TIMER , Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsChannelIndexCorrect( oC_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(TIMER);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsModulePinDefined( oC_TIMER_Pin_t ModulePin )
{
    oC_Pin_t pin = oC_ModulePin_GetPin(ModulePin);
    return oC_GPIO_LLD_IsPinDefined(pin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsSubTimerCorrect(oC_TIMER_LLD_SubTimer_t SubTimer)
{
    return SubTimer > oC_TIMER_LLD_SubTimer_None && SubTimer <= oC_TIMER_LLD_SubTimer_Both;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_Pins_t oC_TIMER_LLD_GetPinOfModulePin( oC_TIMER_Pin_t ModulePin )
{
    return oC_ModulePin_GetPin(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
const char * oC_TIMER_LLD_GetModulePinName( oC_TIMER_Pin_t ModulePin )
{
    return oC_GPIO_LLD_GetPinName( oC_ModulePin_GetPin(ModulePin) );
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
const char * oC_TIMER_LLD_GetChannelName( oC_TIMER_Channel_t Channel )
{
    return oC_Channel_GetName( Channel );
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_TIMER_Channel_t oC_TIMER_LLD_GetChannelOfModulePin( oC_TIMER_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ChannelIndex_t oC_TIMER_LLD_ChannelToChannelIndex( oC_TIMER_Channel_t Channel )
{
    return oC_Channel_ToIndex(TIMER,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_TIMER_Channel_t oC_TIMER_LLD_ChannelIndexToChannel( oC_ChannelIndex_t ChannelIndex )
{
    return oC_Channel_FromIndex(TIMER,ChannelIndex);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_SetPower( oC_TIMER_Channel_t Channel , oC_Power_t Power )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadPower( oC_TIMER_Channel_t Channel , oC_Power_t * outPower )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_RestoreDefaultStateOnChannel( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    bool stateRestored = false;

    return stateRestored;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TimerStart( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_TimerStop( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeFrequency( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_Frequency_t Frequency , oC_Frequency_t PermissibleDifference )
{
    oC_ErrorCode_t errorCode      = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadFrequency( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_Frequency_t * outFrequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeMaximumValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadMaximumValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeCurrentValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadCurrentValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t * outMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_CountDirection_t CountDirection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_CountDirection_t * outCountDirection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Trigger_t Trigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Trigger_t * outTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeEventHandler( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventHandler_t EventHandler , oC_TIMER_LLD_EventFlags_t EventFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadEventHandler(oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventHandler_t * outEventHandler , oC_TIMER_LLD_EventFlags_t * outEventFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ChangeStartPwmState( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_PwmState_t State)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadStartPwmState( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_PwmState_t * outState)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ConnectModulePin(oC_TIMER_Pin_t PeripheralPin )
{
    oC_ErrorCode_t          errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadSubTimerOfPeripheralPin(oC_TIMER_Pin_t PeripheralPin , oC_TIMER_LLD_SubTimer_t * outSubTimer )
{
    oC_ErrorCode_t         errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_SetChannelUsed( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_SetChannelUnused( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_TIMER_LLD_IsChannelUsed( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer )
{
    return false;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_LLD_ReadModulePinsOfPin( oC_Pins_t Pin , oC_TIMER_Pin_t * outPeripheralPinsArray , uint32_t * ArraySize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________


#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

