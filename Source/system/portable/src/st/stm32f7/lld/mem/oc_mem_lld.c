/** ****************************************************************************************************************************************
 *
 * @file       oc_mem_lld.c
 *
 * @brief      The file with sources for the memory low level driver (MEM-LLD)
 *
 * @author     Patryk Kubiak - (Created on: 10 06 2015 19:29:17)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_mem_lld.h>
#include <oc_interrupts.h>
#include <oc_lsf.h>
#include <oc_module.h>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

static oC_MEM_LLD_Interrupt_t BusFaultInterruptHandler          = NULL;
static oC_MEM_LLD_Interrupt_t MemoryFaultInterruptHandler       = NULL;
static bool                   BusFaultInterruptEnabledFlag      = false;
static bool                   MemoryFaultInterruptEnabledFlag   = false;

#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup MEM-LLD
//! @{



//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: initializes faults interrupts, and interrupts handlers pointers.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_MEM_LLD))
    {
        oC_MCS_EnterCriticalSection();
        BusFaultInterruptHandler        = NULL;
        MemoryFaultInterruptHandler     = NULL;
        BusFaultInterruptEnabledFlag    = false;
        MemoryFaultInterruptEnabledFlag = false;

        oC_Module_TurnOn(oC_Module_MEM_LLD);
        oC_MCS_ExitCriticalSection();

        errorCode                       = oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_ModuleIsTurnedOn;
    }
    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: turns off interrupts
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_MEM_LLD))
    {
        oC_MCS_EnterCriticalSection();
        oC_Module_TurnOff(oC_Module_MEM_LLD);
        MemoryFaultInterruptEnabledFlag = false;
        BusFaultInterruptEnabledFlag    = false;

        if(
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MCS_DisableInterrupt(BusFault_IRQn)         , oC_ErrorCode_CannotTurnOffInterruptInMachineModule) &&
            oC_AssignErrorCodeIfFalse(&errorCode , oC_MCS_DisableInterrupt(MemoryManagement_IRQn) , oC_ErrorCode_CannotTurnOffInterruptInMachineModule)
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_MCS_ExitCriticalSection();
    }
    else
    {
        errorCode = oC_ErrorCode_ModuleNotStartedYet;
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetRamStartAddress( void )
{
    return oC_LSF_GetRamStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetRamEndAddress( void )
{
    return oC_LSF_GetRamEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetRamSize( void )
{
    return oC_LSF_GetRamSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetDmaRamStartAddress( void )
{
    return oC_LSF_GetDmaStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetDmaRamEndAddress( void )
{
    return oC_LSF_GetDmaEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetDmaRamSize( void )
{
    return oC_LSF_GetDmaSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetFlashStartAddress( void )
{
    return oC_LSF_GetRomStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetFlashEndAddress( void )
{
    return oC_LSF_GetRomEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetFlashSize( void )
{
    return oC_LSF_GetRomSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetHeapStartAddress( void )
{
    return oC_LSF_GetHeapStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetHeapEndAddress( void )
{
    return oC_LSF_GetHeapEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetHeapSize( void )
{
    return oC_LSF_GetHeapSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetStackStartAddress( void )
{
    return oC_LSF_GetStackStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetStackEndAddress( void )
{
    return oC_LSF_GetStackEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetStackSize( void )
{
    return oC_LSF_GetStackSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetDataStartAddress( void )
{
    return oC_LSF_GetDataStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetDataEndAddress( void )
{
    return oC_LSF_GetDataEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetDataSize( void )
{
    return oC_LSF_GetDataSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetBssStartAddress( void )
{
    return oC_LSF_GetBssStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetBssEndAddress( void )
{
    return oC_LSF_GetBssEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetBssSize( void )
{
    return oC_LSF_GetBssSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetUsedFlashStartAddress( void )
{
    return oC_LSF_GetTextStart();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
void * oC_MEM_LLD_GetUsedFlashEndAddress( void )
{
    return oC_LSF_GetTextEnd();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:redefinition from the machine module. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetUsedFlashSize( void )
{
    return oC_LSF_GetTextSize();
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: it is not read from the linkage script. Works also when the module is turned off.
 */
//==========================================================================================================================================
oC_MEM_LLD_Size_t oC_MEM_LLD_GetAlignmentSize( void )
{
    return oC_MCS_MEMORY_ALIGNMENT;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsRamAddress( const void * Address )
{
    return oC_LSF_IsRamAddress(Address) || oC_LSF_IsExternalAddress(Address) || oC_LSF_IsDmaAddress(Address );
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsExternalAddress( const void * Address )
{
    return oC_LSF_IsExternalAddress(Address);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsDmaRamAddress( const void * Address )
{
    return oC_LSF_IsDmaAddress(Address);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsFlashAddress( const void * Address )
{
    return oC_LSF_IsRomAddress(Address);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsUsedFlashAddress( const void * Address )
{
    return (Address > oC_LSF_GetTextStart()) && (Address < oC_LSF_GetTextEnd());
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsHeapAddress( const void * Address )
{
    return (Address >= oC_LSF_GetHeapStart()) && (Address < oC_LSF_GetHeapEnd());
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: address must be in RAM or text, and interrupt handler is not already set.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_SetMemoryFaultInterrupt( oC_MEM_LLD_Interrupt_t Interrupt )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_MEM_LLD_IsRamAddress(Interrupt) || oC_MEM_LLD_IsUsedFlashAddress(Interrupt))
    {
        if(
             oC_Module_TurnOnVerification(  &errorCode , oC_Module_MEM_LLD) &&
             oC_AssignErrorCodeIfFalse(     &errorCode , MemoryFaultInterruptHandler == NULL , oC_ErrorCode_InterruptHandlerAlreadySet)
                )
        {
            /* Must not turn off interrupts, because there is no possible to turn it on, when
             * the handler is not set.                                                        */
            MemoryFaultInterruptHandler     = Interrupt;
            errorCode                       = oC_ErrorCode_None;
        }
    }
    else
    {
        errorCode = oC_ErrorCode_WrongAddress;
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: address must be in RAM or text, and interrupt handler is not already set.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_SetBusFaultInterrupt( oC_MEM_LLD_Interrupt_t Interrupt )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_MEM_LLD_IsRamAddress(Interrupt) || oC_MEM_LLD_IsUsedFlashAddress(Interrupt))
    {
        if(
             oC_Module_TurnOnVerification(  &errorCode , oC_Module_MEM_LLD) &&
             oC_AssignErrorCodeIfFalse(     &errorCode , BusFaultInterruptHandler == NULL    , oC_ErrorCode_InterruptHandlerAlreadySet)
                )
        {
            /* Must not turn off interrupts, because there is no possible to turn it on, when
             * the handler is not set.                                                        */
            BusFaultInterruptHandler        = Interrupt;
            errorCode                       = oC_ErrorCode_None;
        }
    }
    else
    {
        errorCode = oC_ErrorCode_WrongAddress;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_TurnOnMemoryFaultInterrupt( void )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_TurnOffMemoryFaultInterrupt( void )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_TurnOnBusFaultInterrupt( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(   &errorCode , oC_Module_MEM_LLD) &&
        oC_AssignErrorCodeIfFalse(      &errorCode , BusFaultInterruptHandler != NULL    , oC_ErrorCode_InterruptHandlerNotSet)
           )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes: works also when the module is turned off
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_TurnOffBusFaultInterrupt( void )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsMemoryFaultInterruptTurnedOn( void )
{
    bool interruptTurnedOn = true;

    return interruptTurnedOn;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_MEM_LLD_IsBusFaultInterruptTurnedOn( void )
{
    bool interruptTurnedOn = true;

    return interruptTurnedOn;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MEM_LLD_ConfigureMemoryRegion( oC_MEM_LLD_MemoryRegionConfig_t * Config )
{
    oC_ErrorCode_t                  errorCode           = oC_ErrorCode_ImplementError;
    oC_MCS_MemoryRegionConfig_t     mcsConfig;

    if(
        oC_Module_TurnOnVerification(   &errorCode , oC_Module_MEM_LLD) &&
        ErrorCondition( oC_LSF_IsCorrectAddress(Config) , oC_ErrorCode_WrongAddress)
           )
    {
        mcsConfig.AlignSize         = false;
        mcsConfig.BaseAddress       = Config->BaseAddress;
        mcsConfig.Bufforable        = Config->Bufforable;
        mcsConfig.Cacheable         = Config->Cachable;
        mcsConfig.Power             = Config->Power;
        mcsConfig.PrivilegedAccess  = Config->PrivilegedAccess;
        mcsConfig.UserAccess        = Config->UserAccess;
        mcsConfig.RegionNumber      = Config->RegionNumber;
        mcsConfig.Shareable         = false;
        mcsConfig.Size              = Config->Size;

        if(Config->FindFreeRegion == false || ErrorCondition(oC_MCS_ReadFreeRegionNumber(&mcsConfig.RegionNumber) , oC_ErrorCode_NoChannelAvailable))
        {
            if(oC_AssignErrorCode(&errorCode , oC_MCS_ConfigureMemoryRegion(&mcsConfig)))
            {
                if(  Config->FindFreeRegion == true
                  && ErrorCondition( oC_LSF_IsRamAddress(Config) || oC_LSF_IsExternalAddress(Config) , oC_ErrorCode_OutputAddressNotInRAM )
                     )
                {
                    Config->RegionNumber = mcsConfig.RegionNumber;
                }
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_MEM_LLD_SetMemoryAccessMode( oC_MEM_LLD_MemoryAccessMode_t Mode )
{
    return oC_MCS_SetMemoryAccessMode(Mode);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_MEM_LLD_MemoryAccessMode_t oC_MEM_LLD_GetMemoryAccessMode( void )
{
    return oC_MCS_GetMemoryAccessMode();
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

//==========================================================================================================================================
/**
 * Interrupt handler for the BusFault event
 */
//==========================================================================================================================================
oC_InterruptHandler(System, BusFault)
{
    if(BusFaultInterruptHandler && BusFaultInterruptEnabledFlag)
    {
        BusFaultInterruptHandler();
    }
}

//==========================================================================================================================================
/**
 * Interrupt handler for MemoryManagement event
 */
//==========================================================================================================================================
oC_InterruptHandler(System, MemoryManagement)
{
    if(MemoryFaultInterruptHandler && MemoryFaultInterruptEnabledFlag)
    {
        MemoryFaultInterruptHandler();
    }
}

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________
