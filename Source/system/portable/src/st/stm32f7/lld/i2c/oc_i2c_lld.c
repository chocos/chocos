/** ****************************************************************************************************************************************
 *
 * @file       oc_i2c_lld.c
 *
 * @brief      The file with source for I2C LLD functions
 *
 * @file       oc_i2c_lld.c
 *
 * @author     Patryk Kubiak - (Created on: 2017-02-17 - 19:39:55)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_i2c_lld.h>

#ifdef oC_I2C_LLD_AVAILABLE
#include <oc_gpio_lld.h>
#include <oc_gpio_mslld.h>
#include <oc_mem_lld.h>
#include <oc_bits.h>
#include <oc_module.h>
#include <oc_lsf.h>
#include <oc_stdtypes.h>
#include <oc_clock_lld.h>
#include <string.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define SOFTWARE_RING_COUNT                     255
#define RxRing(Channel)                         SoftwareRxRings[oC_Channel_ToIndex(I2C,Channel)]
#define IsSoftwareRxRingEmpty(Channel)          (RxRing(Channel).Counter == 0)
#define IsSoftwareRxRingFull(Channel)           (RxRing(Channel).Counter == SOFTWARE_RING_COUNT)
#define TxRing(Channel)                         SoftwareTxRings[oC_Channel_ToIndex(I2C,Channel)]
#define IsSoftwareTxRingEmpty(Channel)          (TxRing(Channel).Counter == 0)
#define IsSoftwareTxRingFull(Channel)           (TxRing(Channel).Counter == SOFTWARE_RING_COUNT)
#define IsRam(Address)                          oC_LSF_IsRamAddress(Address)
#define IsRom(Address)                          oC_LSF_IsRomAddress(Address)
#define IsChannelCorrect(Channel)               oC_Channel_IsCorrect(I2C,Channel)
#define IsChannelPoweredOn(Channel)             (oC_Machine_GetPowerStateForChannel(Channel) == oC_Power_On)
#define IsModeCorrect(Mode)                     (oC_I2C_LLD_Mode_Master == (Mode) || oC_I2C_LLD_Mode_Slave == (Mode))
#define IsSpeedModeCorrect(SpeedMode)           (oC_I2C_LLD_SpeedMode_Normal     == (SpeedMode) \
                                              || oC_I2C_LLD_SpeedMode_Fast       == (SpeedMode) \
                                              || oC_I2C_LLD_SpeedMode_FastPlus   == (SpeedMode) \
                                              || oC_I2C_LLD_SpeedMode_HighSpeed  == (SpeedMode) \
                                              || oC_I2C_LLD_SpeedMode_UltraFast  == (SpeedMode) )
#define IsSpeedModeSupported(SpeedMode)         (oC_I2C_LLD_SpeedMode_Normal     == (SpeedMode) \
                                              || oC_I2C_LLD_SpeedMode_Fast       == (SpeedMode) \
                                              || oC_I2C_LLD_SpeedMode_FastPlus   == (SpeedMode) )
#define IsAddressModeCorrect(AddressMode)       (oC_I2C_LLD_AddressMode_7Bits    == (AddressMode) \
                                              || oC_I2C_LLD_AddressMode_10Bits   == (AddressMode) )
#define IsPrescalerSupported(Prescaler)         ((Prescaler) <= 0xF )
#define AreOperationsDisabled(Channel)          (I2C_CR1(Channel)->PE == 0)
#define AreOperationsEnabled(Channel)           (I2C_CR1(Channel)->PE == 1)
#define IsAddressCorrect(Channel,Address)       ((Is10BitsAddressMode(Channel)) ? ((Address) <= 0x3FF) : ((Address) <= 0xFF))
#define RCC_DCKCFGR2                            oC_Register(RCC,RCC_DCKCFGR2)
#define I2C_CR1(Channel)                        oC_Channel_Register(Channel,I2C_CR1)
#define I2C_CR2(Channel)                        oC_Channel_Register(Channel,I2C_CR2)
#define I2C_OAR1(Channel)                       oC_Channel_Register(Channel,I2C_OAR1)
#define I2C_OAR2(Channel)                       oC_Channel_Register(Channel,I2C_OAR2)
#define I2C_TIMINGR(Channel)                    oC_Channel_Register(Channel,I2C_TIMINGR)
#define I2C_TIMEOUTR(Channel)                   oC_Channel_Register(Channel,I2C_TIMEOUTR)
#define I2C_ISR(Channel)                        oC_Channel_Register(Channel,I2C_ISR)
#define I2C_ICR(Channel)                        oC_Channel_Register(Channel,I2C_ICR)
#define I2C_PECR(Channel)                       oC_Channel_Register(Channel,I2C_PECR)
#define I2C_RXDR(Channel)                       oC_Channel_Register(Channel,I2C_RXDR)
#define I2C_TXDR(Channel)                       oC_Channel_Register(Channel,I2C_TXDR)
#define IsRxFifoEmpty(Channel)                  (I2C_ISR(Channel)->RXNE == 0)
#define Is10BitsAddressMode(Channel)            (I2C_CR2(Channel)->ADD10 == 1)
#define Is7BitsAddressMode(Channel)             (I2C_CR2(Channel)->ADD10 == 0)
#define IsChannelUsed(Channel)                  (SavedConfiguration(Channel).Used == true)
#define NUMBER_OF_CHANNELS                      oC_ModuleChannel_NumberOfElements(I2C)

#define IsAcknowledgeReceived(Channel)          (I2C_ISR(Channel)->NACKF == 0)
#define IsTxFifoEmpty(Channel)                  (I2C_ISR(Channel)->TXIS == 1)
#define IsTxFifoFull(Channel)                   (I2C_ISR(Channel)->TXIS == 0)
#define IsTransferComplete(Channel)             (I2C_ISR(Channel)->TC == 1)
#define IsTransferReloadBitSet(Channel)         (I2C_ISR(Channel)->TCR == 1)
#define SavedConfiguration(Channel)             SavedConfigurations[oC_Channel_ToIndex(I2C,Channel)]
#define IsMasterMode(Channel)                   (SavedConfiguration(Channel).Mode == oC_I2C_LLD_Mode_Master)
#define ResetChannelState(Channel)              I2C_CR1(Channel)->PE = 0
#define StartPeripheral(Channel)                I2C_CR1(Channel)->PE = 1

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief type for storing configuration of a channel
 */
//==========================================================================================================================================
typedef struct
{
    bool                            Used;
    oC_I2C_LLD_Mode_t               Mode;
    oC_I2C_LLD_SpeedMode_t          SpeedMode;
    oC_I2C_LLD_AddressMode_t        AddressMode;
    oC_MemorySize_t                 LeftTransmissionSize;
    bool                            TransmissionStarted;
    bool                            StartedForRead;
    oC_I2C_LLD_InterruptSource_t    EnabledInterrupts;
    bool                            EarlyStopDetected;
    bool                            AcknowledgeNotReceived;
} SavedConfiguration_t;

typedef struct
{
    uint8_t             Buffer[SOFTWARE_RING_COUNT];
    uint16_t            PutIndex;
    uint16_t            GetIndex;
    uint16_t            Counter;
} SoftwareRing_t;


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool                 AreAllInterruptsSupported   ( bool MasterMode, oC_I2C_LLD_InterruptSource_t Interrupts );
static void                 ReadDefaultTimingValues     ( oC_I2C_LLD_SpeedMode_t SpeedMode, oC_Frequency_t * ClockFrequency, oC_Time_t * DataHoldTime, oC_Time_t * DataSetupTime, oC_Time_t * ClockLowTime, oC_Time_t * ClockHighTime );
static oC_ErrorCode_t       ConfigureGeneral            ( oC_I2C_Channel_t Channel, oC_I2C_LLD_InterruptSource_t Interrupts, bool GeneralCall, bool ClockStretching );
static oC_ErrorCode_t       ConfigureClock              ( oC_I2C_Channel_t Channel, oC_Frequency_t Frequency, oC_Time_t DataHoldTime, oC_Time_t DataSetupTime, oC_Time_t ClockLowTime, oC_Time_t ClockHighTime );
static inline void          PutToSoftwareRing           ( SoftwareRing_t * Ring , uint8_t Data );
static inline uint8_t       GetFromSoftwareRing         ( SoftwareRing_t * Ring );
static inline void          ClearSoftwareRing           ( SoftwareRing_t * Ring );
static void                 CallInterruptHandler        ( oC_I2C_Channel_t Channel , oC_I2C_LLD_InterruptSource_t Active , oC_I2C_LLD_InterruptSource_t Inactive  );

oC_Channel_InterruptHandlerPrototype(PeripheralInterrupt,Channel);

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_I2C_LLD_InterruptHandler_t InterruptHandler                        = NULL;
static SavedConfiguration_t          SavedConfigurations[NUMBER_OF_CHANNELS];
static SoftwareRing_t                SoftwareRxRings[NUMBER_OF_CHANNELS];
static SoftwareRing_t                SoftwareTxRings[NUMBER_OF_CHANNELS];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_I2C_LLD_IsChannelCorrect( oC_I2C_Channel_t Channel )
{
    return oC_Channel_IsCorrect(I2C,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_I2C_LLD_IsChannelIndexCorrect( oC_I2C_LLD_ChannelIndex_t ChannelIndex )
{
    return ChannelIndex < oC_ModuleChannel_NumberOfElements(I2C);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_I2C_LLD_ChannelIndex_t oC_I2C_LLD_ChannelToChannelIndex( oC_I2C_Channel_t Channel )
{
    return oC_Channel_ToIndex(I2C,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_I2C_Channel_t oC_I2C_LLD_ChannelIndexToChannel( oC_I2C_LLD_ChannelIndex_t Channel )
{
    return oC_Channel_FromIndex(I2C,Channel);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_I2C_Channel_t oC_I2C_LLD_GetChannelOfModulePin( oC_I2C_Pin_t ModulePin )
{
    return oC_ModulePin_GetChannel(ModulePin);
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_TurnOnDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_I2C_LLD))
    {
        oC_Module_TurnOn(oC_Module_I2C_LLD);

        memset(SavedConfigurations,0,sizeof(SoftwareRxRings));
        memset(SoftwareRxRings,0,sizeof(SoftwareRxRings));
        memset(SoftwareTxRings,0,sizeof(SoftwareTxRings));
        errorCode           = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_TurnOffDriver( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C_LLD))
    {
        oC_Module_TurnOff(oC_Module_I2C_LLD);
        errorCode = oC_ErrorCode_None;
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
bool oC_I2C_LLD_IsChannelUsed( oC_I2C_Channel_t Channel )
{
    bool used = false;

    if(IsChannelCorrect(Channel))
    {
        used = IsChannelUsed(Channel);
    }

    return used;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_ConfigureAsMaster(
    oC_I2C_Channel_t                Channel ,
    oC_I2C_LLD_SpeedMode_t          SpeedMode ,
    oC_I2C_LLD_AddressMode_t        AddressMode ,
    oC_Time_t                       DataHoldTime ,
    oC_Time_t                       DataSetupTime ,
    oC_Time_t                       ClockLowTime ,
    oC_Time_t                       ClockHighTime ,
    oC_Frequency_t                  ClockFrequency ,
    oC_I2C_LLD_InterruptSource_t    EnabledInterrupts
    )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                           , oC_ErrorCode_WrongChannel             )
     && ErrorCondition( IsChannelUsed(Channel) == false                     , oC_ErrorCode_ChannelIsUsed            )
     && ErrorCondition( IsSpeedModeCorrect(SpeedMode)                       , oC_ErrorCode_SpeedModeNotCorrect      )
     && ErrorCondition( IsSpeedModeSupported(SpeedMode)                     , oC_ErrorCode_SpeedModeNotSupported    )
     && ErrorCondition( IsAddressModeCorrect(AddressMode)                   , oC_ErrorCode_AddressModeNotCorrect    )
     && ErrorCondition( ClockFrequency >= 0                                 , oC_ErrorCode_WrongFrequency           )
     && ErrorCondition( ClockFrequency <= SpeedMode                         , oC_ErrorCode_WrongFrequency           )
     && ErrorCondition( DataHoldTime  >= 0                                  , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( DataSetupTime >= 0                                  , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( ClockLowTime  >= 0                                  , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( ClockHighTime >= 0                                  , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( AreAllInterruptsSupported(true,EnabledInterrupts)   , oC_ErrorCode_InterruptNotSupported    )
        )
    {
        ReadDefaultTimingValues(SpeedMode,&ClockFrequency,&DataHoldTime,&DataSetupTime,&ClockLowTime,&ClockHighTime);

        if(
            ErrorCode( ConfigureGeneral( Channel, EnabledInterrupts, false, true                                            ))
         && ErrorCode( ConfigureClock  ( Channel, ClockFrequency, DataHoldTime, DataSetupTime, ClockLowTime, ClockHighTime  ))
            )
        {
            SavedConfiguration(Channel).AddressMode = AddressMode;
            SavedConfiguration(Channel).SpeedMode   = SpeedMode;
            SavedConfiguration(Channel).Mode        = oC_I2C_LLD_Mode_Master;
            SavedConfiguration(Channel).Used        = true;

            StartPeripheral(Channel);

            errorCode = oC_ErrorCode_None;
        }
        else
        {
            ResetChannelState(Channel);
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_ConfigureAsSlave(
    oC_I2C_Channel_t                Channel ,
    oC_I2C_LLD_SpeedMode_t          SpeedMode ,
    oC_I2C_LLD_AddressMode_t        AddressMode ,
    oC_I2C_LLD_Address_t            OwnAddress ,
    bool                            GeneralCallEnabled ,
    bool                            ClockStretchingEnabled,
    oC_I2C_LLD_InterruptSource_t    EnabledInterrupts
    )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                           , oC_ErrorCode_WrongChannel             )
     && ErrorCondition( IsChannelUsed(Channel) == false                     , oC_ErrorCode_ChannelIsUsed            )
     && ErrorCondition( IsSpeedModeCorrect(SpeedMode)                       , oC_ErrorCode_SpeedModeNotCorrect      )
     && ErrorCondition( IsSpeedModeSupported(SpeedMode)                     , oC_ErrorCode_SpeedModeNotSupported    )
     && ErrorCondition( IsAddressModeCorrect(AddressMode)                   , oC_ErrorCode_AddressModeNotCorrect    )
     && ErrorCondition( OwnAddress <= AddressMode                           , oC_ErrorCode_AddressNotCorrect        )
     && ErrorCondition( AreAllInterruptsSupported(false,EnabledInterrupts)  , oC_ErrorCode_InterruptNotSupported    )
        )
    {
        if( ErrorCode( ConfigureGeneral( Channel, EnabledInterrupts, GeneralCallEnabled, ClockStretchingEnabled) ) )
        {
            SavedConfiguration(Channel).AddressMode = AddressMode;
            SavedConfiguration(Channel).SpeedMode   = SpeedMode;
            SavedConfiguration(Channel).Mode        = oC_I2C_LLD_Mode_Slave;
            SavedConfiguration(Channel).Used        = true;

            I2C_OAR1(Channel)->OA1EN   = 1;
            I2C_OAR1(Channel)->OA1MODE = AddressMode == oC_I2C_LLD_AddressMode_10Bits ? 1 : 0;
            I2C_OAR1(Channel)->OA1     = OwnAddress;

            StartPeripheral(Channel);

            errorCode = oC_ErrorCode_None;
        }
        else
        {
            ResetChannelState(Channel);
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_UnconfigureChannel( oC_I2C_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)           , oC_ErrorCode_WrongChannel             )
     && ErrorCondition( IsChannelUsed(Channel) == true      , oC_ErrorCode_ChannelNotConfigured     )
        )
    {
        ResetChannelState(Channel);
        SavedConfiguration(Channel).Used = false;
        ClearSoftwareRing(&RxRing(Channel));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_StartAsMaster( oC_I2C_Channel_t Channel, bool StartForRead, oC_I2C_LLD_Address_t SlaveAddress, oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                   , oC_ErrorCode_WrongChannel             )
     && ErrorCondition( IsChannelUsed(Channel) == true              , oC_ErrorCode_ChannelNotConfigured     )
     && ErrorCondition( IsChannelPoweredOn(Channel)                 , oC_ErrorCode_ChannelNotPoweredOn      )
     && ErrorCondition( IsAddressCorrect(Channel,SlaveAddress)      , oC_ErrorCode_AddressNotCorrect        )
     && ErrorCondition( IsMasterMode(Channel)                       , oC_ErrorCode_NotMasterMode            )
     && ErrorCondition( Size > 0                                    , oC_ErrorCode_SizeNotCorrect           )
        )
    {
        /* Clearing or setting R/W bit */
        SlaveAddress = StartForRead ? (SlaveAddress | (1<<0)) : (SlaveAddress & (~(1<<0)));

        I2C_CR2(Channel)->SADD   = SlaveAddress;
        I2C_CR2(Channel)->RDWRN  = StartForRead ? 1 : 0;
        I2C_CR2(Channel)->NBYTES = Size > 0xFF ? 0xFF : Size ;
        I2C_CR2(Channel)->RELOAD = Size > 0xFF ? 0x01 : 0x00 ;
        I2C_CR2(Channel)->AUTOEND= 0;
        I2C_ICR(Channel)->STOPCF = 1;
        I2C_ICR(Channel)->NACKCF = 1;

        SavedConfiguration(Channel).LeftTransmissionSize    = Size;
        SavedConfiguration(Channel).TransmissionStarted     = true;
        SavedConfiguration(Channel).StartedForRead          = StartForRead;
        SavedConfiguration(Channel).EarlyStopDetected       = false;
        SavedConfiguration(Channel).AcknowledgeNotReceived  = false;

        if(StartForRead)
        {
            CallInterruptHandler(Channel,0,oC_I2C_LLD_InterruptSource_DataReadyToReceive);
            ClearSoftwareRing(&RxRing(Channel));
        }
        else
        {
            CallInterruptHandler(Channel,oC_I2C_LLD_InterruptSource_ReadyToSendNewData,0);
            ClearSoftwareRing(&TxRing(Channel));
        }

        /* As we don't have data to send now, we cannot START the transmission yet - we have to wait for    *
         * a first master write call                                                                        */
        if(StartForRead)
        {
            oC_RegisterType_I2C_ISR_t * isr = I2C_ISR(Channel);

            while(isr->TC == 0 && I2C_CR2(Channel)->START == 1);

            I2C_CR2(Channel)->START  = 1;
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_StopAsMaster ( oC_I2C_Channel_t Channel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                   , oC_ErrorCode_WrongChannel             )
     && ErrorCondition( IsChannelUsed(Channel) == true              , oC_ErrorCode_ChannelNotConfigured     )
     && ErrorCondition( IsChannelPoweredOn(Channel)                 , oC_ErrorCode_ChannelNotPoweredOn      )
     && ErrorCondition( IsMasterMode(Channel)                       , oC_ErrorCode_NotMasterMode            )
        )
    {
        oC_MCS_EnterCriticalSection();

        oC_RegisterType_I2C_ISR_t * isr = I2C_ISR(Channel);

        ClearSoftwareRing(&TxRing(Channel));
        ClearSoftwareRing(&RxRing(Channel));
        CallInterruptHandler(
                        Channel,
                        oC_I2C_LLD_InterruptSource_ReadyToSendNewData,
                        oC_I2C_LLD_InterruptSource_DataReadyToReceive | oC_I2C_LLD_InterruptSource_TransferErrors
                        );

        if(isr->STOPF == 0 && SavedConfiguration(Channel).EarlyStopDetected == false)
        {
            I2C_CR2(Channel)->STOP  = 1;
        }
        else
        {
            SavedConfiguration(Channel).EarlyStopDetected = true;
        }

        CallInterruptHandler(Channel,oC_I2C_LLD_InterruptSource_TransferComplete,0);

        if(isr->NACKF == 1 || SavedConfiguration(Channel).AcknowledgeNotReceived)
        {
            errorCode = oC_ErrorCode_AcknowledgeNotReceived;
        }
        else if(SavedConfiguration(Channel).EarlyStopDetected)
        {
            errorCode = oC_ErrorCode_EarlyStopDetected;
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }

        SavedConfiguration(Channel).TransmissionStarted     = false;
        SavedConfiguration(Channel).EarlyStopDetected       = false;
        SavedConfiguration(Channel).AcknowledgeNotReceived  = false;

        I2C_ICR(Channel)->NACKCF = 1;
        I2C_ICR(Channel)->STOPCF = 1;

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_WriteAsMaster( oC_I2C_Channel_t Channel, const uint8_t * Data, oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                                   , oC_ErrorCode_WrongChannel                 )
     && ErrorCondition( IsChannelUsed(Channel) == true                              , oC_ErrorCode_ChannelNotConfigured         )
     && ErrorCondition( IsChannelPoweredOn(Channel)                                 , oC_ErrorCode_ChannelNotPoweredOn          )
     && ErrorCondition( IsMasterMode(Channel)                                       , oC_ErrorCode_NotMasterMode                )
     && ErrorCondition( IsRam(Data) || IsRom(Data)                                  , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( IsRam(Size)                                                 , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( (*Size) > 0                                                 , oC_ErrorCode_SizeNotCorrect               )
     && ErrorCondition( SavedConfiguration(Channel).TransmissionStarted             , oC_ErrorCode_TransmissionNotStarted       )
     && ErrorCondition( SavedConfiguration(Channel).StartedForRead == false         , oC_ErrorCode_TransmissionStartedForRead   )
     && ErrorCondition( SavedConfiguration(Channel).LeftTransmissionSize >= (*Size) , oC_ErrorCode_SizeTooBig                   )
        )
    {
        oC_MCS_EnterCriticalSection();

        oC_MemorySize_t sentBytes   = 0;
        oC_MemorySize_t bufferSize  = *Size;

        errorCode = oC_ErrorCode_TransmitFifoIsFull;

        for(; sentBytes < bufferSize && IsSoftwareTxRingFull(Channel) == false; sentBytes++ )
        {
            PutToSoftwareRing(&TxRing(Channel), Data[sentBytes]);
            errorCode   = oC_ErrorCode_None;
        }

        if( I2C_CR2(Channel)->START == 0
        &&  ( TxRing(Channel).Counter == SavedConfiguration(Channel).LeftTransmissionSize
           || IsSoftwareTxRingFull(Channel)
              )
           )
        {
            /* Flushing the TX DATA register */
            I2C_ISR(Channel)->TXE   = 1;

            I2C_CR2(Channel)->START = 1;
        }

        if(IsSoftwareTxRingFull(Channel))
        {
            CallInterruptHandler(Channel,0,oC_I2C_LLD_InterruptSource_ReadyToSendNewData);
        }

        oC_MCS_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_ReadAsMaster( oC_I2C_Channel_t Channel, uint8_t *    outData, oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                           , oC_ErrorCode_WrongChannel                 )
     && ErrorCondition( IsChannelUsed(Channel) == true                      , oC_ErrorCode_ChannelNotConfigured         )
     && ErrorCondition( IsChannelPoweredOn(Channel)                         , oC_ErrorCode_ChannelNotPoweredOn          )
     && ErrorCondition( IsMasterMode(Channel)                               , oC_ErrorCode_NotMasterMode                )
     && ErrorCondition( IsRam(outData)                                      , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( IsRam(Size)                                         , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( (*Size) > 0                                         , oC_ErrorCode_SizeNotCorrect               )
     && ErrorCondition( SavedConfiguration(Channel).TransmissionStarted     , oC_ErrorCode_TransmissionNotStarted       )
     && ErrorCondition( SavedConfiguration(Channel).StartedForRead == true  , oC_ErrorCode_TransmissionStartedForWrite  )
        )
    {
        oC_MemorySize_t receivedBytes   = 0;
        oC_MemorySize_t bufferSize      = *Size;

        errorCode = oC_ErrorCode_NoDataToReceive;

        for(receivedBytes = 0; receivedBytes < bufferSize && IsSoftwareRxRingEmpty(Channel) == false; receivedBytes++)
        {
            outData[receivedBytes] = GetFromSoftwareRing(&RxRing(Channel));

            errorCode = oC_ErrorCode_None;
        }
        *Size = receivedBytes;

        if(IsSoftwareRxRingEmpty(Channel))
        {
            CallInterruptHandler(Channel,0,oC_I2C_LLD_InterruptSource_DataReadyToReceive);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_WriteAsSlave( oC_I2C_Channel_t Channel, const uint8_t * Data, oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                           , oC_ErrorCode_WrongChannel                 )
     && ErrorCondition( IsChannelUsed(Channel) == true                      , oC_ErrorCode_ChannelNotConfigured         )
     && ErrorCondition( IsChannelPoweredOn(Channel)                         , oC_ErrorCode_ChannelNotPoweredOn          )
     && ErrorCondition( IsMasterMode(Channel) == false                      , oC_ErrorCode_MasterMode                   )
     && ErrorCondition( IsRam(Data) || IsRom(Data)                          , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( IsRam(Size)                                         , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( (*Size) > 0                                         , oC_ErrorCode_SizeNotCorrect               )
        )
    {
        if(I2C_CR1(Channel)->NOSTRETCH == 0)
        {
            if(
                ErrorCondition( I2C_ISR(Channel)->ADDR == 1 , oC_ErrorCode_NoRequestFromMaster          )
             && ErrorCondition( I2C_ISR(Channel)->DIR  == 0 , oC_ErrorCode_MasterAskedAboutDataWrite    )
                )
            {
                uint32_t        timeout    = 0;
                oC_MemorySize_t sentBytes  = 0;
                oC_MemorySize_t bufferSize = *Size;

                I2C_ICR(Channel)->ADDRCF = 1;

                errorCode = oC_ErrorCode_MasterChangedHisMind;

                for(sentBytes = 0; sentBytes < bufferSize; sentBytes++)
                {
                    timeout = 0xFF;

                    while(I2C_ISR(Channel)->TXIS == 0)
                    {
                        oC_MCS_Delay(10);
                        timeout--;
                        if(timeout == 0)
                        {
                            break;
                        }
                    }

                    I2C_TXDR(Channel)->TXDATA = Data[sentBytes];
                    errorCode                 = oC_ErrorCode_None;
                }

                *Size = sentBytes;
            }
        }
        else
        {
            uint32_t        timeout    = 0;
            oC_MemorySize_t sentBytes  = 0;
            oC_MemorySize_t bufferSize = *Size;

            errorCode = oC_ErrorCode_MasterChangedHisMind;

            for(sentBytes = 0; sentBytes < bufferSize; sentBytes++)
            {
                timeout = 0xFF;

                while(I2C_ISR(Channel)->TXIS == 0)
                {
                    oC_MCS_Delay(10);
                    timeout--;
                    if(timeout == 0)
                    {
                        break;
                    }
                }

                I2C_TXDR(Channel)->TXDATA = Data[sentBytes];
                errorCode                 = oC_ErrorCode_None;
            }

            *Size = sentBytes;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_ReadAsSlave( oC_I2C_Channel_t Channel, uint8_t *    outData, oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C_LLD)
     && ErrorCondition( IsChannelCorrect(Channel)                           , oC_ErrorCode_WrongChannel                 )
     && ErrorCondition( IsChannelUsed(Channel) == true                      , oC_ErrorCode_ChannelNotConfigured         )
     && ErrorCondition( IsChannelPoweredOn(Channel)                         , oC_ErrorCode_ChannelNotPoweredOn          )
     && ErrorCondition( IsMasterMode(Channel) == false                      , oC_ErrorCode_MasterMode                   )
     && ErrorCondition( IsRam(outData)                                      , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( IsRam(Size)                                         , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( (*Size) > 0                                         , oC_ErrorCode_SizeNotCorrect               )
        )
    {
        if(I2C_CR1(Channel)->NOSTRETCH == 0)
        {
            if(
                ErrorCondition( I2C_ISR(Channel)->ADDR == 1 , oC_ErrorCode_NoRequestFromMaster          )
             && ErrorCondition( I2C_ISR(Channel)->DIR  == 1 , oC_ErrorCode_MasterAskedAboutDataRead     )
                )
            {
                I2C_ICR(Channel)->ADDRCF = 1;

                oC_MemorySize_t receivedBytes = 0;
                oC_MemorySize_t bufferSize    = 0;

                errorCode = oC_ErrorCode_MasterChangedHisMind;

                for(receivedBytes = 0; receivedBytes < bufferSize; receivedBytes++)
                {
                    if(I2C_ISR(Channel)->RXNE == 0 && IsSoftwareRxRingEmpty(Channel))
                    {
                        break;
                    }

                    if(IsSoftwareRxRingEmpty(Channel) == false)
                    {
                        outData[receivedBytes] = GetFromSoftwareRing(&RxRing(Channel));
                    }
                    else
                    {
                        outData[receivedBytes] = I2C_RXDR(Channel)->RXDATA;
                    }
                    errorCode = oC_ErrorCode_None;
                }
                *Size = receivedBytes;
            }
        }
        else
        {
            oC_MemorySize_t receivedBytes = 0;
            oC_MemorySize_t bufferSize    = 0;

            errorCode = oC_ErrorCode_MasterChangedHisMind;

            for(receivedBytes = 0; receivedBytes < bufferSize; receivedBytes++)
            {
                if(I2C_ISR(Channel)->RXNE == 0 && IsSoftwareRxRingEmpty(Channel))
                {
                    break;
                }

                if(IsSoftwareRxRingEmpty(Channel) == false)
                {
                    outData[receivedBytes] = GetFromSoftwareRing(&RxRing(Channel));
                }
                else
                {
                    outData[receivedBytes] = I2C_RXDR(Channel)->RXDATA;
                }
                errorCode = oC_ErrorCode_None;
            }
            *Size = receivedBytes;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_ReadModulePinsOfPin( oC_Pin_t Pin , oC_I2C_Pin_t * outModulePinsArray , uint32_t * ArraySize , oC_I2C_PinFunction_t PinFunction )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C_LLD))
    {
        if(
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(Pin)                           , oC_ErrorCode_PinNotDefined         ) &&
            ErrorCondition( IsRam(ArraySize)                                        , oC_ErrorCode_OutputAddressNotInRAM ) &&
            ErrorCondition( outModulePinsArray == NULL || IsRam(outModulePinsArray) , oC_ErrorCode_OutputAddressNotInRAM )
            )
        {
            uint32_t foundPins = 0;

            errorCode = oC_ErrorCode_None;

            oC_ModulePin_ForeachDefined(modulePin)
            {
                if(modulePin->Pin == Pin && oC_Channel_IsCorrect(I2C,modulePin->Channel) && modulePin->PinFunction == PinFunction)
                {
                    if(outModulePinsArray != NULL)
                    {
                        if(foundPins < *ArraySize)
                        {
                            outModulePinsArray[foundPins] = modulePin->ModulePinIndex;
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_OutputArrayToSmall;
                        }
                    }
                    foundPins++;
                }
            }

            *ArraySize = foundPins;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_SetInterruptHandler( oC_I2C_LLD_InterruptHandler_t Handler )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_MCS_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C_LLD))
    {
        if(
            ErrorCondition( IsRam(Handler) || IsRom(Handler), oC_ErrorCode_WrongAddress                 )
         && ErrorCondition( InterruptHandler == NULL        , oC_ErrorCode_InterruptHandlerAlreadySet   )
            )
        {
            InterruptHandler = Handler;
            errorCode        = oC_ErrorCode_None;
        }
    }

    oC_MCS_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_ConnectModulePin( oC_I2C_Pin_t ModulePin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C_LLD))
    {
        oC_I2C_Channel_t channel = oC_ModulePin_GetChannel(ModulePin);
        oC_Pin_t         pin     = oC_ModulePin_GetPin(ModulePin);

        if(
            ErrorCondition( IsChannelCorrect(channel)       , oC_ErrorCode_WrongChannel  ) &&
            ErrorCondition( oC_GPIO_LLD_IsPinDefined(pin)   , oC_ErrorCode_PinNotDefined )
            )
        {
            bool unused = false;

            oC_MCS_EnterCriticalSection();

            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ArePinsUnused(pin,&unused)                                    ) &&
                ErrorCondition(        unused , oC_ErrorCode_PinIsUsed                                                    ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(pin)                                              ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_ConnectModulePin(ModulePin)                                 ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetSpeed(pin, oC_GPIO_LLD_Speed_Maximum)                      ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetOutputCircuit(pin, oC_GPIO_LLD_OutputCircuit_OpenDrain)    )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_GPIO_LLD_SetPinsUnused(pin);
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @note
 * **stm32f7** notes:
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_LLD_DisconnectModulePin( oC_I2C_Pin_t ModulePin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C_LLD))
    {
        oC_I2C_Channel_t channel = oC_ModulePin_GetChannel(ModulePin);
        oC_Pin_t         pin     = oC_ModulePin_GetPin(ModulePin);

        if(
            ErrorCondition( IsChannelCorrect(channel)       , oC_ErrorCode_WrongChannel  )
         && ErrorCondition( oC_GPIO_LLD_IsPinDefined(pin)   , oC_ErrorCode_PinNotDefined )
            )
        {
            oC_MCS_EnterCriticalSection();

            if(
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUnused(pin)                 )
             && oC_AssignErrorCode(&errorCode , oC_GPIO_MSLLD_DisconnectModulePin(ModulePin)   )
                )
            {
                errorCode = oC_ErrorCode_None;
            }

            oC_MCS_ExitCriticalSection();
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if all given interrupts are supported
 */
//==========================================================================================================================================
static bool AreAllInterruptsSupported( bool MasterMode, oC_I2C_LLD_InterruptSource_t Interrupts )
{
    bool                            allSupported        = false;
    oC_I2C_LLD_InterruptSource_t    supportedSources    = oC_I2C_LLD_InterruptSource_TransferComplete
                                                        | oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived
                                                        | oC_I2C_LLD_InterruptSource_ArbitrationLost
                                                        | oC_I2C_LLD_InterruptSource_BusError
                                                        | oC_I2C_LLD_InterruptSource_EarlyStopDetected
                                                        | oC_I2C_LLD_InterruptSource_DataReadyToReceive
                                                        | oC_I2C_LLD_InterruptSource_ReadyToSendNewData
                                                        | oC_I2C_LLD_InterruptSource_Overrun
                                                        | oC_I2C_LLD_InterruptSource_Underrun
                                                        | oC_I2C_LLD_InterruptSource_Timeout
                                                        | oC_I2C_LLD_InterruptSource_PecError
                                                        | oC_I2C_LLD_InterruptSource_SmbusAlert
                                                        | oC_I2C_LLD_InterruptSource_NoMoreDataToSend;

    if(MasterMode == false)
    {
        supportedSources |= oC_I2C_LLD_InterruptSource_AddressMatched;
    }

    allSupported = (Interrupts & (~supportedSources)) == 0;

    return allSupported;
}

//==========================================================================================================================================
/**
 * @brief reads default timing values if they are not given
 */
//==========================================================================================================================================
static void ReadDefaultTimingValues( oC_I2C_LLD_SpeedMode_t SpeedMode, oC_Frequency_t * ClockFrequency, oC_Time_t * DataHoldTime, oC_Time_t * DataSetupTime, oC_Time_t * ClockLowTime, oC_Time_t * ClockHighTime )
{
    if((*ClockFrequency) == 0)
    {
        *ClockFrequency = SpeedMode;
    }

    oC_Time_t t_scl = oC_Frequency_ToTime(*ClockFrequency);

    if((*ClockLowTime) == 0 && (*ClockHighTime) == 0)
    {
        *ClockLowTime  = t_scl / 2;
        *ClockHighTime = t_scl / 2;
    }
    else if((*ClockLowTime) == 0)
    {
        *ClockLowTime = t_scl - *ClockHighTime;
    }
    else if((*ClockHighTime) == 0)
    {
        *ClockHighTime = t_scl - *ClockLowTime;
    }

    if(*DataHoldTime == 0)
    {
        *DataHoldTime  = ns(7);
    }
    if(*DataSetupTime == 0)
    {
        *DataSetupTime = ns(10);
    }
}

//==========================================================================================================================================
/**
 * @brief configures I2C general to work (enables peripheral, RCC, etc)
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureGeneral( oC_I2C_Channel_t Channel, oC_I2C_LLD_InterruptSource_t Interrupts, bool GeneralCall, bool ClockStretching )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Machine_SetPowerStateForChannel(Channel,oC_Power_On)     , oC_ErrorCode_CannotEnableChannel      )
     && ErrorCondition( oC_Channel_EnableInterrupt(Channel,PeripheralInterrupt)     , oC_ErrorCode_CannotEnableInterrupt    )
        )
    {
        I2C_CR1(Channel)->PE        = 0;

        I2C_CR1(Channel)->GCEN      = GeneralCall     ? 1 : 0;
        I2C_CR1(Channel)->NOSTRETCH = ClockStretching ? 0 : 1;
        I2C_CR1(Channel)->ANFOFF    = 0;
        I2C_CR1(Channel)->DNF       = 0;

        /* Interrupt Sources */
        I2C_CR1(Channel)->ERRIE     = 1;
        I2C_CR1(Channel)->TCIE      = 0;
        I2C_CR1(Channel)->STOPIE    = 0;
        I2C_CR1(Channel)->NACKIE    = (Interrupts & oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived   ) != 0 ? 1 : 0;
        I2C_CR1(Channel)->ADDRIE    = (Interrupts & oC_I2C_LLD_InterruptSource_AddressMatched           ) != 0 ? 1 : 0;
        I2C_CR1(Channel)->RXIE      = (Interrupts & oC_I2C_LLD_InterruptSource_DataReadyToReceive       ) != 0 ? 1 : 0;
        I2C_CR1(Channel)->TXIE      = (Interrupts & oC_I2C_LLD_InterruptSource_ReadyToSendNewData       ) != 0 ? 1 : 0;

        SavedConfiguration(Channel).EnabledInterrupts = Interrupts;

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures master I2C clock to work
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureClock( oC_I2C_Channel_t Channel, oC_Frequency_t Frequency, oC_Time_t DataHoldTime, oC_Time_t DataSetupTime, oC_Time_t ClockLowTime, oC_Time_t ClockHighTime )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    oC_Time_t      t_scldel         = DataSetupTime;
    oC_Time_t      t_sdadel         = DataHoldTime;
    oC_Time_t      t_sclh           = ClockLowTime;
    oC_Time_t      t_scll           = ClockHighTime;
    oC_Frequency_t hsiFrequency     = oC_MHz(16);
    oC_Frequency_t i2cclkFrequency  = hsiFrequency;
    oC_Time_t      t_i2cclk         = oC_Frequency_ToTime(i2cclkFrequency);
    oC_Frequency_t realSclFrequency = oC_Frequency_FromTime(t_sclh + t_sclh);

    if(
        ErrorCondition( realSclFrequency <= Frequency , oC_ErrorCode_TimeNotCorrect )
        )
    {
        oC_Time_t      t_presc              = 0;
        uint32_t       PRESC                = 0;
        uint32_t       SCLDEL               = 0;
        uint32_t       SDADEL               = 0;
        uint32_t       SCLH                 = 0;
        uint32_t       SCLL                 = 0;
        bool           configurationFound   = false;

        for(PRESC = 0; PRESC <= 0xF; PRESC++)
        {
            t_presc = ((oC_Time_t)(PRESC + 1)) * t_i2cclk;

            if(t_presc == 0)
            {
                continue;
            }

            SCLDEL = (uint32_t)((t_scldel / t_presc) - 1);
            SDADEL = (uint32_t)(t_sdadel  / t_presc);
            SCLH   = (uint32_t)((t_sclh   / t_presc) - 1);
            SCLL   = (uint32_t)((t_scll   / t_presc) - 1);

            if(
                SCLDEL <= 0xF
             && SDADEL <= 0xF
             && SCLH   <= 0xFF
             && SCLL   <= 0xFF
                )
            {
                configurationFound = true;
                break;
            }
        }

        if(ErrorCondition( configurationFound, oC_ErrorCode_FrequencyNotPossible ))
        {
            uint8_t rccBitShift = 16;

            rccBitShift += ((oC_Channel_ToIndex(I2C,Channel)) * 2);

            /* Set I2CCLK source as HSI */
            RCC_DCKCFGR2->Value &= ~(0x3 << rccBitShift);
            RCC_DCKCFGR2->Value |=   0x2 << rccBitShift ;

            I2C_TIMINGR(Channel)->PRESC  = PRESC;
            I2C_TIMINGR(Channel)->SCLDEL = SCLDEL;
            I2C_TIMINGR(Channel)->SDADEL = SDADEL;
            I2C_TIMINGR(Channel)->SCLH   = SCLH;
            I2C_TIMINGR(Channel)->SCLL   = SCLL;

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief puts data to software ring
 */
//==========================================================================================================================================
static inline void PutToSoftwareRing( SoftwareRing_t * Ring , uint8_t Data )
{
    if(Ring->Counter >= SOFTWARE_RING_COUNT)
    {
        /* If ring is full, take the oldest data */
        GetFromSoftwareRing(Ring);
    }

    Ring->Buffer[Ring->PutIndex++] = Data;
    Ring->Counter++;

    if(Ring->PutIndex >= SOFTWARE_RING_COUNT)
    {
        Ring->PutIndex = 0;
    }
}

//==========================================================================================================================================
/**
 * @brief gets data from software ring
 */
//==========================================================================================================================================
static inline uint8_t GetFromSoftwareRing( SoftwareRing_t * Ring )
{
    uint8_t data = 0x00;

    if(Ring->Counter > 0)
    {
        Ring->Counter--;
        data = Ring->Buffer[Ring->GetIndex++];
        if(Ring->GetIndex >= SOFTWARE_RING_COUNT)
        {
            Ring->GetIndex = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief clears data in software ring
 */
//==========================================================================================================================================
static inline void ClearSoftwareRing( SoftwareRing_t * Ring )
{
    memset(Ring,0,sizeof(SoftwareRing_t));
}

//==========================================================================================================================================
/**
 * @brief calls interrupt handler if it is needed
 */
//==========================================================================================================================================
static void CallInterruptHandler( oC_I2C_Channel_t Channel , oC_I2C_LLD_InterruptSource_t Active , oC_I2C_LLD_InterruptSource_t Inactive  )
{
    if(InterruptHandler != NULL)
    {
        InterruptHandler(Channel,Active,Inactive);
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

//==========================================================================================================================================
/**
 *              INTERRUPT HANDLER FOR I2C Peripheral Interrupt
 * */
//==========================================================================================================================================
#define MODULE_NAME              I2C
#define INTERRUPT_TYPE_NAME      PeripheralInterrupt

oC_Channel_InterruptHandler(Channel)
{
    oC_RegisterType_I2C_ISR_t *  isr      = I2C_ISR(Channel);
    oC_RegisterType_I2C_ICR_t *  icr      = I2C_ICR(Channel);
    oC_I2C_LLD_InterruptSource_t active   = 0;
    oC_I2C_LLD_InterruptSource_t inactive = 0;

    if(I2C_ISR(Channel)->RXNE == 1 || I2C_ISR(Channel)->TC == 1)
    {
        uint8_t receivedData = I2C_RXDR(Channel)->RXDATA;
        PutToSoftwareRing(&RxRing(Channel), receivedData);
        SavedConfiguration(Channel).LeftTransmissionSize--;

        active |= oC_I2C_LLD_InterruptSource_DataReadyToReceive;
    }

    if(isr->TXE == 1 || isr->TXIS == 1)
    {
        if(IsSoftwareTxRingEmpty(Channel) == false)
        {
            I2C_TXDR(Channel)->TXDATA = GetFromSoftwareRing(&TxRing(Channel));

            if(SavedConfiguration(Channel).LeftTransmissionSize > 0)
            {
                SavedConfiguration(Channel).LeftTransmissionSize--;
            }
        }
    }

    if(IsSoftwareTxRingEmpty(Channel))
    {
        active |= oC_I2C_LLD_InterruptSource_NoMoreDataToSend;
    }
    else
    {
        inactive |= oC_I2C_LLD_InterruptSource_NoMoreDataToSend;
    }

    if(isr->STOPF == 1)
    {
        if(SavedConfiguration(Channel).TransmissionStarted)
        {
            SavedConfiguration(Channel).EarlyStopDetected = true;
            active |= oC_I2C_LLD_InterruptSource_EarlyStopDetected;
        }
    }

    if(isr->NACKF == 1)
    {
        SavedConfiguration(Channel).AcknowledgeNotReceived = true;
        icr->NACKCF = 1;
        active |= oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived;
    }

    if(isr->ADDR == 1)
    {
        icr->ADDRCF = 1;
        active |= oC_I2C_LLD_InterruptSource_AddressMatched;
    }
    if(isr->TC == 1)
    {
        active |= oC_I2C_LLD_InterruptSource_TransferComplete;
    }
    if(isr->TCR == 1)
    {
        if(SavedConfiguration(Channel).LeftTransmissionSize > 0)
        {
            if(SavedConfiguration(Channel).LeftTransmissionSize > 0xFF)
            {
                I2C_CR2(Channel)->NBYTES = 0xFF;
                I2C_CR2(Channel)->RELOAD = 1;
            }
            else
            {
                I2C_CR2(Channel)->NBYTES = SavedConfiguration(Channel).LeftTransmissionSize;
                I2C_CR2(Channel)->RELOAD = 0;
            }
        }
    }

    if(SavedConfiguration(Channel).LeftTransmissionSize == 0)
    {
        active |= oC_I2C_LLD_InterruptSource_TransferComplete;
    }

    if(isr->BERR == 1)
    {
        icr->BERRCF = 1;
        active |= oC_I2C_LLD_InterruptSource_BusError;
    }

    if(isr->ARLO == 1)
    {
        icr->ARLOCF = 1;
        active |= oC_I2C_LLD_InterruptSource_ArbitrationLost;
    }
    if(isr->ALERT == 1)
    {
        icr->ALERTCF = 1;
        active |= oC_I2C_LLD_InterruptSource_SmbusAlert;
    }
    if(isr->TIMEOUT == 1)
    {
        icr->TIMEOUTCF = 1;
        active |= oC_I2C_LLD_InterruptSource_Timeout;
    }
    if(isr->OVR == 1)
    {
        icr->OVRCF = 1;
        active |= oC_I2C_LLD_InterruptSource_Overrun | oC_I2C_LLD_InterruptSource_Underrun;
    }

    CallInterruptHandler(Channel,active,inactive);
}

#undef MODULE_NAME
#undef INTERRUPT_TYPE_NAME

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________



#endif /* oC_I2C_LLD_AVAILABLE */
