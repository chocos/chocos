/** ****************************************************************************************************************************************
 *
 * @brief      The file with source for the pins module
 *
 * @file       oc_pins.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_pins.h>

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

const oC_DefinedPinData_t oC_DefinedPins[oC_PinIndex_NumberOfElements] = {
#define MAKE_PIN(BASE_ADDRESS_NAME , PIN_NAME , BIT_INDEX , PIN_NUMBER)             { .Pin = oC_Pin_(PIN_NAME), .Name = #PIN_NAME } ,
    oC_PINS_LIST(MAKE_PIN)
#undef MAKE_PIN
};

const oC_ModulePinData_t oC_ModulePinsData[oC_ModulePinIndex_NumberOfElements] = {
#define MAKE_MODULE_PIN( PIN_NAME , CHANNEL_NAME  , PIN_FUNCTION   , ALTERNATE_FUNCTION )       \
                { .Pin = oC_Pin_(PIN_NAME) , .Channel = oC_Channel_(CHANNEL_NAME) , .PinFunction = oC_PinFunctionIndex_(PIN_FUNCTION) , .AlternateNumber = ALTERNATE_FUNCTION , .ModulePinIndex = oC_ModulePinIndex_(CHANNEL_NAME,PIN_FUNCTION,PIN_NAME,ALTERNATE_FUNCTION)},
#define MAKE_MODULE(MODULE_NAME)            oC_MODULE_PINS_(MODULE_NAME)(MAKE_MODULE_PIN)
                oC_MODULES_PINS_LIST(MAKE_MODULE)
#undef MAKE_MODULE
#undef MAKE_MODULE_PIN
};
#define MAKE_MODULE_PIN_FUNCTION( PIN_FUNCTION )       \
                oC_TO_STRING(PIN_FUNCTION) ,
#define MAKE_MODULE(MODULE_NAME)            oC_MODULE_PIN_FUNCTIONS_(MODULE_NAME)(MAKE_MODULE_PIN_FUNCTION)
const char * oC_PinFunctionsNames[oC_PinFunctionIndex_NumberOfElements] = {
    oC_MODULES_PINS_LIST(MAKE_MODULE)
};
#undef MAKE_MODULE
#undef MAKE_MODULE_PIN_FUNCTION

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

