/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_channels.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_channels.h>
#include <stdbool.h>
#include <stddef.h>
#include <oc_assert.h>
#include <oc_mcs.h>

const oC_ChannelData_t oC_ChannelsData[] = {
#define MAKE_CHANNEL(CHANNEL_NAME,BASE_ADDRESS_NAME,REGISTER_MAP_NAME,...)          \
    [oC_Channel_(CHANNEL_NAME)] = { \
        .BaseAddress        = oC_BaseAddress_(BASE_ADDRESS_NAME),\
        .PowerBaseAddress   = oC_PowerBaseAddress_(BASE_ADDRESS_NAME),\
        .PowerOffset        = oC_PowerOffset_(BASE_ADDRESS_NAME),\
        .PowerBitIndex      = oC_PowerBit_(BASE_ADDRESS_NAME),\
        .RegisterMap        = oC_RegisterMap_(REGISTER_MAP_NAME),\
        .ChannelName        = #CHANNEL_NAME ,\
    },
    oC_MODULES_LIST(oC_Make_ModuleWithChannels)
#undef MAKE_CHANNEL
};

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//==========================================================================================================================================
/**
 * @brief set interrupt priority according to channel and type
 *
 * The function is for setting interrupt priority according to dynamic channel and interrupt type.
 *
 * @see oC_Channel_SetInterruptPriority
 *
 * @param Channel               Channel of the interrupt to enable
 * @param InterruptPriority     Interrupt priority
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Channel_SetInterruptPriorityFunction( oC_Channel_t Channel , oC_InterruptType_t InterruptType , oC_InterruptPriotity_t Priority )
{
    bool                        success       = false;
    const oC_InterruptData_t *  interruptData = oC_Interrupt_GetData(oC_Channel_GetBaseAddress(Channel),InterruptType);

    if(interruptData != NULL)
    {
        success =  oC_MCS_SetInterruptPriority( interruptData->InterruptNumber , Priority );
    }

    return success;
}
//==========================================================================================================================================
/**
 * @brief enables interrupt according to channel and type
 *
 * The function is for enabling interrupt according to dynamic channel and interrupt type.
 *
 * @see oC_Channel_EnableInterrupt
 *
 * @param Channel           Channel of the interrupt to enable
 * @param InterruptType     Type of interrupt (peripheral interrupt for example)
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Channel_EnableInterruptFunction( oC_Channel_t Channel , oC_InterruptType_t InterruptType )
{
    bool                        success       = false;
    const oC_InterruptData_t *  interruptData = oC_Interrupt_GetData(oC_Channel_GetBaseAddress(Channel),InterruptType);

    if(interruptData != NULL)
    {
        success = oC_MCS_EnableInterrupt(interruptData->InterruptNumber);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief disables interrupt according to channel and type
 *
 * The function is for disabling interrupt according to dynamic channel and interrupt type.
 *
 * @see oC_Channel_DisableInterrupt
 *
 * @param Channel           Channel of the interrupt to disable
 * @param InterruptType     Type of interrupt (peripheral interrupt for example)
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Channel_DisableInterruptFunction( oC_Channel_t Channel , oC_InterruptType_t InterruptType )
{
    bool                        success       = false;
    const oC_InterruptData_t *  interruptData = oC_Interrupt_GetData(oC_Channel_GetBaseAddress(Channel),InterruptType);

    if(interruptData != NULL)
    {
        success = oC_MCS_DisableInterrupt(interruptData->InterruptNumber);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief checks if interrupt is enabled according to channel and type
 *
 * The function is for checking if interrupt is enabled according to dynamic channel and interrupt type.
 *
 * @see oC_Channel_EnableInterrupt
 *
 * @param Channel           Channel of the interrupt
 * @param InterruptType     Type of interrupt (peripheral interrupt for example)
 *
 * @return true if interrupt enabled
 */
//==========================================================================================================================================
bool oC_Channel_IsInterruptEnabledFunction( oC_Channel_t Channel , oC_InterruptType_t InterruptType )
{
    bool                        success       = false;
    const oC_InterruptData_t *  interruptData = oC_Interrupt_GetData(oC_Channel_GetBaseAddress(Channel),InterruptType);

    if(interruptData != NULL)
    {
        success = oC_MCS_IsInterruptEnabled(interruptData->InterruptNumber);
    }

    return success;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
