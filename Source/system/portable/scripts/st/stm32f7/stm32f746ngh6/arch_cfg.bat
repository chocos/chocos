::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/12/14
::	Description:	Script for setting paths
::================================================================================================================================
@echo off

:: Architecture of the uC
::   	For example for STM32 it is ARM 
SET MACHINE_ARCHITECTURE=ARM

:: Core architecture family 
::		For example for STM32f7 it is Cortex_M7 
:: Before you will set it, make sure, that MCS directory for this architecture exist
SET MACHINE_CORE_ARCHITECTURE_FAMILY=Cortex_M7