::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	Script for connection with the programmer for the machine
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=%cd%/../../../../../../..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Checking paths
::----------------------------------------------------------
IF NOT EXIST %OPENOCD_BIN_DIR%     GOTO OpenOcdBinNotExists
IF NOT EXIST %OPENOCD_SCRIPTS_DIR% GOTO OpenOcdScriptsNotExists

::----------------------------------------------------------
:: connect to the programmer
::----------------------------------------------------------
SET SAVED_PATH=%CD%
cd %OPENOCD_SCRIPTS_DIR%
call %OPENOCD_BIN_DIR%/openocd.exe -f board/stm32f7discovery.cfg -c "init; halt"
GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Scripts for setting paths not exists!
echo =          file: '%SET_PATHS_FILE_PATH%'
echo =
echo ==========================================================================
GOTO EOF

:OpenOcdBinNotExists
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Openocd dir not exists!
echo =          dir: '%OPENOCD_BIN_DIR%
echo =
echo ==========================================================================
GOTO EOF

:OpenOcdScriptsNotExists
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Openocd dir not exists!
echo =          dir: '%OPENOCD_SCRIPTS_DIR%
echo =
echo ==========================================================================
GOTO EOF

:EOF
pause
exit

