#!/bin/sh
#================================================================================================================================
#	Author:			Patryk Kubiak
#	Date:			2016/07/04
#	Description:	Script for connection with the programmer for the machine
#================================================================================================================================


#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR!                                                     ="
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR" = "" ]; then PROJECT_DIR=$PWD/../../../../../../..; fi
CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh
SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH

#=====================================================================================================
#   OPENOCD PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $OPENOCD_BIN_DIR          ]; then PathDoesNotExist $OPENOCD_BIN_DIR;      fi
if [ ! -e $OPENOCD_SCRIPTS_DIR      ]; then PathDoesNotExist $OPENOCD_SCRIPTS_DIR;  fi

#----------------------------------------------------------
# connect to the programmer
#----------------------------------------------------------
SAVED_PATH=$PWD
cd $OPENOCD_SCRIPTS_DIR
openocd -f board/stm32f7discovery.cfg -c "init; halt" -l error
cd $SAVED_PATH
