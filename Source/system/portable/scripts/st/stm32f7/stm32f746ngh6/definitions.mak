############################################################################################################################################
##
##	Author:          Patryk Kubiak
##	Date:            2015/04/27
##	Description:     makefile with definitions specific for the machine
##
############################################################################################################################################

##============================================================================================================================
##											
##				PREPARATION TOOLS				
##											
##============================================================================================================================
CC                          = arm-none-eabi-gcc
AR                          = arm-none-eabi-ar
AS                          = arm-none-eabi-gcc -x assembler-with-cpp
LD                          = arm-none-eabi-g++
OBJCOPY                     = arm-none-eabi-objcopy
OBJDUMP                     = arm-none-eabi-objdump
SIZE                        = arm-none-eabi-size

##============================================================================================================================
##											
##				PREPARATION FLAGS				
##											
##============================================================================================================================
CPUCONFIG_CFLAGS            =-mcpu=cortex-m7 -mthumb -mno-unaligned-access -DGCC_ARMCM7
CPUCONFIG_AFLAGS            =-mcpu=cortex-m7 -mthumb -mno-unaligned-access -DGCC_ARMCM7
CPUCONFIG_LDFLAGS           =-mcpu=cortex-m7 -mthumb -mno-unaligned-access 

##============================================================================================================================
##											
##				MACHINE DEFINITIONS				
##											
##============================================================================================================================
MACHINE_PRODUCENT           = st
MACHINE_FAMILY              = stm32f7
MACHINE_NAME                = stm32f746ngh6
MACHINE_WORD_SIZE           = 4
MACHINE_ENDIANNESS          = LITTLE
