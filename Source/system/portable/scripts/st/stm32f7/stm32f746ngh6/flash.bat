::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	Script for flashing machine
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=%cd%/../../../../../../..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Checking paths
::----------------------------------------------------------
IF NOT EXIST %ARM_NONE_EABI_GCC_BIN_DIR% GOTO ArmNoneEabiGccNotExists
IF NOT EXIST %MACHINE_GDB_FILE_PATH% GOTO GdbFileNotExists
IF NOT EXIST %PROJECT_ELF_FILE_PATH% GOTO ProjectNotCompiled

::----------------------------------------------------------
:: Flash the micro
::----------------------------------------------------------
cd %ARM_NONE_EABI_GCC_BIN_DIR%

:Loop
echo ---=== Flashing the machine ===---
arm-none-eabi-gdb --quiet --batch -x %MACHINE_GDB_FILE_PATH% %PROJECT_ELF_FILE_PATH%
pause
GOTO Loop
GOTO EOF

:ArmNoneEabiGccNotExists
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			arm-none-eabi-gcc dir not exists!
echo =          dir: '%ARM_NONE_EABI_GCC_BIN_DIR%'
echo =
echo ==========================================================================
GOTO EOF

:GdbFileNotExists
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			gdb file not exists!
echo =          file: '%MACHINE_GDB_FILE_PATH%'
echo =
echo ==========================================================================
GOTO EOF

:ProjectNotCompiled
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Project must be compiled first!
echo =
echo ==========================================================================
GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Scripts for setting paths not exists!
echo =          file: '%SET_PATHS_FILE_PATH%'
echo =
echo ==========================================================================
GOTO EOF

:OpenOcdBinNotExists
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Openocd dir not exists!
echo =          dir: '%OPENOCD_BIN_DIR%
echo =
echo ==========================================================================
GOTO EOF

:EOF
exit

