#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/06
#   Description:    Script for setting paths
#================================================================================================================================

# Architecture of the uC
#   	For example for STM32 it is ARM 
export MACHINE_ARCHITECTURE=ARM

# Core architecture family 
#		For example for STM32f7 it is Cortex_M7 
# Before you will set it, make sure, that MCS directory for this architecture exist
export MACHINE_CORE_ARCHITECTURE_FAMILY=Cortex_M7