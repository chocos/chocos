#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/05
#   Description:    Script for flashing the machine
#================================================================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR!                                                     ="
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

ArchitectureNotSelected()
{
    SetForegroundColor "red"
    echo "=========================================================================="
    echo "=	                                                                    "
    echo "= 	ERROR!                                                              "
    echo "= 			Target architecture is not selected                 "
    echo "= 			run select_arch.bat first                           "
    echo "=                                                                         "
    echo "=========================================================================="
    ResetAllAttributes
    exit 1
}

ProjectNotCompiled()
{
    SetForegroundColor "red"
    echo "=========================================================================="
    echo "=	                                                                    "
    echo "= 	ERROR!                                                              "
    echo "= 			Project must be compiled first!                     "
    echo "=                                                                         "
    echo "=========================================================================="
    ResetAllAttributes
    exit 1
}

#=====================================================================================================
#	HELPER FUNCTIONS
#=====================================================================================================
IsMachineSelected()
{
    if [ "$MACHINE_SELECTED" = "true" ]; then
        return 0;
    else
        return 1;
    fi
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR"         = "" ]; then PROJECT_DIR=$PWD/..;                          fi
if [ "$SET_PATHS_FILE_PATH" = "" ]; then SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh;fi
if [ "$CONSOLE_FILE_PATH"   = "" ]; then CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh;    fi

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#    ARCHITECTURE SELECTED VERIFICATION
#=====================================================================================================
if ! IsMachineSelected ; then ArchitectureNotSelected; fi

#=====================================================================================================
#   TOOLS PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $ARM_NONE_EABI_GCC_BIN_DIR    ]; then PathDoesNotExist $ARM_NONE_EABI_GCC_BIN_DIR;   fi
if [ ! -e $MACHINE_GDB_FILE_PATH        ]; then PathDoesNotExist $MACHINE_GDB_FILE_PATH;       fi
if [ ! -e $PROJECT_ELF_FILE_PATH        ]; then ProjectNotCompiled;                            fi


#=====================================================================================================
#   TOOLS PATHS VERIFICATIONS
#=====================================================================================================
SAVED_PATH=$PWD
ANSWER_YN="Y"
cd $ARM_NONE_EABI_GCC_BIN_DIR

#while [ ! "$ANSWER_YN" = "n" ]; do
    echo "---=== Flashing the machine ===---"
    arm-none-eabi-gdb --quiet --batch -x $MACHINE_GDB_FILE_PATH $PROJECT_ELF_FILE_PATH
    #read -p "Continue? [Y/n]: " ANSWER_YN
#done

#=====================================================================================================
#   RETURN TO THE OLD PATH
#=====================================================================================================
cd $SAVED_PATH
