::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	Build script for portable space
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Check architecture
::----------------------------------------------------------
IF NOT DEFINED MACHINE_SELECTED GOTO ArchitectureNotSelected
IF "%MACHINE_SELECTED%"=="FALSE" GOTO ArchitectureNotSelected

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=%cd:\=/%/../../..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Default values
::----------------------------------------------------------
SET TARGET=build_portable

::----------------------------------------------------------
:: Check parameters
::----------------------------------------------------------
IF NOT "%1"=="" IF NOT "%1"=="all" IF NOT "%1"=="clean" GOTO Usage
IF "%1"=="clean" SET TARGET=clean_portable

::----------------------------------------------------------
:: Compile
::----------------------------------------------------------
make.exe -f portable.mak %TARGET%

GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Scripts for setting paths not exists!                         =
echo =          file: '%SET_PATHS_FILE_PATH%'                                 =
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:ArchitectureNotSelected
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target architecture is not selected
echo = 			run select_arch.bat first
echo =
echo ==========================================================================
GOTO EOF

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 [TARGET]
echo =				
echo = 			where TARGET is:
echo = 					all
echo = 					clean
echo ==========================================================================

:EOF