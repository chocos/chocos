/** ****************************************************************************************************************************************
 *
 * @file       oc_machines_list.h
 *
 * @brief      The file contains the list of machines
 *
 * @author     Patryk Kubiak - (Created on: 26 kwi 2015 17:32:30) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_MACHINES_LIST_H_
#define SYSTEM_PORTABLE_INC_OC_MACHINES_LIST_H_

#include <oc_1word.h>

#define oC_FAMILY_MASK                                  0xF000
#define oC_CORTEX_MASK                                  0x0F00
#define oC_PROCESSOR_MASK                               0x00FF

#define oC_DEFINE_FAMILY( INDEX )                       ( (INDEX) << 12 )
#define oC_DEFINE_CORTEX( INDEX )                       ( (INDEX) <<  8 )
#define oC_DEFINE_MACHINE( FAMILY , CORTEX , INDEX )    ( (FAMILY) | (CORTEX) | (INDEX) )

#define oC_GET_MACHINE_FAMILY( MACHINE )                ( (MACHINE) & oC_FAMILY_MASK )
#define oC_GET_MACHINE_CORTEX( MACHINE )                ( (MACHINE) & oC_CORTEX_MASK )
#define oC_GET_MACHINE_PROCESSOR( MACHINE )             ( (MACHINE) & oC_PROCESSOR_MASK )

#define oC_GET_MACHINE_ID(NAME)                         oC_1WORD_FROM_2(MACHINE_ , NAME)
#define oC_GET_FAMILY_ID(NAME)                          oC_1WORD_FROM_2(FAMILY_  , NAME)
#define oC_GET_CORTEX_ID(NAME)                          oC_1WORD_FROM_2(CORTEX_  , NAME)

#define CORTEX_ARM_Cortex_M4                                   oC_DEFINE_CORTEX( 0x04 )
#define CORTEX_ARM_Cortex_M7                                   oC_DEFINE_CORTEX( 0x07 )

#define FAMILY_LM4F                                            oC_DEFINE_FAMILY( 0x01 )
#define FAMILY_STM32F7                                         oC_DEFINE_FAMILY( 0x02 )

#define MACHINE_LM4F120H5QR                                     oC_DEFINE_MACHINE( LM4F    , ARM_Cortex_M4 , 0x01 )
#define MACHINE_STM32F746NGH6                                   oC_DEFINE_MACHINE( STM32F7 , ARM_Cortex_M7 , 0x02 )

#endif /* SYSTEM_PORTABLE_INC_OC_MACHINES_LIST_H_ */
