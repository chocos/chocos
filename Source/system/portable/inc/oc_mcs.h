/** ****************************************************************************************************************************************
 *
 * @brief      Contains machine core specific functions
 *
 * @file       oc_mcs.h
 *
 * @author     Patryk Kubiak 
 *
 * @copyright  Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @par
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @par
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @par
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup MCS MCS - Machine Core Specific Module
 * @ingroup PortableSpace
 * @brief Module with machine core specific definitions
 *
 * This module is part of the @subpage PortableSpace
 *
 * The module **MCS** (Machine Core Specific) contains interface, that is specific for machine core. This is to not duplicate a code, that is
 * common for all machines, that are based on the same core, for example uC of **TI** from the **LM4F** family are based on **ARM Cortex M4**, and the
 * uC from **ST** in **STM32F4** family are also based on the same core. We don't want to keep it separately so here is the module, that is designed
 * for all machines based on the same core.
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_PORTABLE_INC_MCS_OC_MCS_H_
#define SYSTEM_PORTABLE_INC_MCS_OC_MCS_H_

#include <oc_interrupts_defs.h>
#include <oc_assert.h>
#include <oc_stdtypes.h>
#include <stdbool.h>
#include <oc_mcs_defs.h>
#include <stddef.h>
#include <oc_memory.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup MCS
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns true if stack push decrements SP
 *
 * The macro returns true if operation stack push decrement stack pointer. This can be also used in "hash conditions".
 *
 * @code{.c}
   #if oC_MCS_IsStackPushDecrementPointer()
       // Do something, the stack push operation decrements pointer
   #else
       // Do something other, the stack push operation increments pointer
   #endif
   @endcode
 */
//==========================================================================================================================================
#define oC_MCS_IsStackPushDecrementPointer()                        oC_MCS_STACK_PUSH_DECREMENTS_POINTER

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief align pointer to the stack alignment
 *
 * The macro is for alignment of stack pointers
 *
 * @see oC_MCS_AlignPointer, oC_MCS_AlignSize
 */
//==========================================================================================================================================
#define oC_MCS_AlignStackPointer(Pointer)                           ((void*)oC_MCS_AlignSize((void*)Pointer,oC_MCS_STACK_MEMORY_ALIGNMENT))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief align pointer to the machine alignment
 *
 * The macro is for alignment of pointer
 *
 * @see oC_MCS_AlignStackPointer, oC_MCS_AlignSize
 */
//==========================================================================================================================================
#define oC_MCS_AlignPointer(Pointer)                                ((void*)oC_MCS_AlignSize((void*)Pointer,oC_MCS_MEMORY_ALIGNMENT))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief align size to the machine alignment
 *
 * The macro is for alignment of size
 *
 * @see oC_MCS_AlignStackPointer, oC_MCS_AlignPointer
 */
//==========================================================================================================================================
#define oC_MCS_AlignSize(Size,Alignment)                            ((((oC_UInt_t)(Size)) + (Alignment) - 1) & ~((Alignment)-1))

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if stack pointer is aligned to the machine stack memory alignment
 */
//==========================================================================================================================================
#define oC_MCS_IsStackPointerAligned(Pointer)                       (oC_MCS_AlignStackPointer(Pointer) == ((void*)(Pointer)))

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if pointer is aligned to the machine memory alignment
 */
//==========================================================================================================================================
#define oC_MCS_IsPointerAligned(Pointer)                            (oC_MCS_AlignPointer(Pointer) == Pointer)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup MCS
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for the CMSIS library, that contains definitions of interrupts
 *
 * This type must be defined for the CMSIS library. It is created according to definitions from the oc_interrupts_defs.h file.
 */
//==========================================================================================================================================
typedef enum
{
/* This definition is for removing text "System" from the IRQn enumerator name */
#define System
/* Definition for creating IRQn type enumerator name */
#define IRQn_(NAME,TYPE)                                                                oC_1WORD_FROM_3(TYPE , NAME , _IRQn)
/* Definition for creation of interrupt number enumerator in IRQn_Type */
#define CREATE_INTERRUPT_NUMBER( BASE_NAME , INTERRUPT_TYPE , INTERRUPT_NUMBER , ...)   IRQn_(BASE_NAME , INTERRUPT_TYPE) = INTERRUPT_NUMBER ,

    /* This creates enumerator values from definitions in interrupts defs file */
    oC_MACHINE_INTERRUPTS_LIST(CREATE_INTERRUPT_NUMBER)

/* Removes all definitions that are not needed anymore */
#undef CREATE_INTERRUPT_NUMBER
#undef System
} IRQn_Type;

//==========================================================================================================================================
/**
 * @brief stores priority of interrupts
 *
 * The type is for storing priority of interrupts. Note, that you can store with it values from #oC_InterruptPriority_Minimum to #oC_InterruptPriority_Maximum.
 * The highest priority has #oC_InterruptPriority_Maximum value.
 */
//==========================================================================================================================================
typedef enum
{
    oC_InterruptPriority_Error      = 0xFFFFFF ,                           //!< Value for functions, that returns priority to provide error handling. See #oC_MCS_GetInterruptPriority for example
    oC_InterruptPriority_Minimum    = oC_MINIMUM_INTERRUPT_PRIORITY ,      //!< The lowest value for priority
    oC_InterruptPriority_Maximum    = oC_MAXIMUM_INTERRUPT_PRIORITY        //!< The most important interrupt priority, that is possible in the machine.
} oC_InterruptPriotity_t;

//==========================================================================================================================================
/**
 * @brief stores handler of function, that handles context
 *
 * The function is for handling RTOS context. It will be called, as new thread, with separate stack.
 *
 * @param Parameter         Optional parameter, that can be given for function
 */
//==========================================================================================================================================
typedef void (*oC_ContextHandler_t)( void * ContextParameter );

//==========================================================================================================================================
/**
 * @brief stores handler of function called, when context handling is finished
 *
 * The function is called, when the #oC_ContextHandler_t function finishes its work.
 */
//==========================================================================================================================================
typedef void (*oC_ContextExitHandler_t)(void);

//==========================================================================================================================================
/**
 * @brief stores stack data
 *
 * The type is for storing informations about stack.
 */
//==========================================================================================================================================
typedef struct
{
    void *      StackPointer;           /**< Current stack pointer */
    void *      Buffer;                 /**< Pointer to start of the stack (minimum address in stack) */
    oC_Int_t    BufferSize;             /**< Size of the stack buffer */
} oC_StackData_t;

//==========================================================================================================================================
/**
 * @brief configuration structure for the memory region
 *
 * The type is for storing configuration of the memory region for MPU
 */
//==========================================================================================================================================
typedef struct
{
    const void *        BaseAddress;          //!< Base address of the region - this must point to the start of the region. Only architecture supported values are allowed
    oC_MemorySize_t     Size;                 //!< Size of the region
    bool                AlignSize;            //!< True if the MCS should find the size nearest to the value given as the #Size field
    oC_Power_t          Power;                //!< Power state for the region (enabled or disabled)
    oC_Access_t         UserAccess;           //!< Definition of access for user space
    oC_Access_t         PrivilegedAccess;     //!< Definition of access for privileged space (portable or core)
    bool                Shareable;            //!< For a shareable memory region that is implemented, the memory system provides data synchronization between bus masters in a system with multiple bus masters, for example, a processor with a DMA controller. Strongly-ordered memory is always shareable. If multiple bus masters can access a non-shareable memory region, software must ensure data coherency between the bus masters.
    bool                Cacheable;            //!< True if the region should be cacheable - If you set a region to be cacheable: When you load from that region, the cache is searched. If the item is found, it is loaded from the cache. If the item is not found, a complete cache line including the required address is loaded. Some other cache line is evicted from the cache, unless there is an unused cache line available. When you save to that region, the cache is searched. If the item is found, the save is made to the cache. If the item is not found, the save is made to memory.
    bool                Bufforable;           //!< True if the region should be bufforable - Bufferable means whether a write to the address can be buffered.  When you do a write to a memory (e.g. a STR instruction) the write gets placed into a buffer in the processor.  The buffer continually drains the writes to the memory system - allowing the core to continue with the next instructions.  Processors often have multiple buffers, for example one between the pipeline and L1 data cache, another between the L1 and L2 caches, another for main memory....
    uint32_t            RegionNumber;         //!< Number of the region to configure. It is just ID index between 0 and #oC_MCS_GetMaximumNumberOfRegions
} oC_MCS_MemoryRegionConfig_t;

//==========================================================================================================================================
/**
 * @brief stores memory access mode
 *
 * The type is for storing mode of the memory access controlling
 */
//==========================================================================================================================================
typedef enum
{
    oC_MCS_MemoryAccessMode_User ,      //!< Memory controller is configured to allow for memory access only for regions that can be accessible by a user
    oC_MCS_MemoryAccessMode_Privileged  //!< Memory controller is configured to allow for memory access for regions that can be accessible by a privileged tasks
} oC_MCS_MemoryAccessMode_t;

//==========================================================================================================================================
/**
 * @brief type for storing stack reference
 *
 * @see oC_StackData_t
 */
//==========================================================================================================================================
typedef oC_StackData_t * oC_Stack_t;

//==========================================================================================================================================
/**
 * @brief stores pointer to function for searching next stack
 *
 * The function is called, when changing current stack is required.
 */
//==========================================================================================================================================
typedef void (*oC_FindNextStackHandler_t)( void );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}



/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
//! @addtogroup MCS
//! @{

//==========================================================================================================================================
/**
 * @brief initializes module to work
 *
 * The function initializes module to work. There is no function, that deinitializes it, and does not prevents again reinitialization
 *
 * @return true if success
 */
//==========================================================================================================================================
extern bool oC_MCS_InitializeModule( void );

//==========================================================================================================================================
/**
 * @brief returns address that cause a hard fault
 */
//==========================================================================================================================================
extern void * oC_MCS_GetHardFaultReason( void );

//==========================================================================================================================================
/**
 * @brief enables all hard fault and other events handled by the processor
 */
//==========================================================================================================================================
extern void oC_MCS_EnableAllPossibleEvents( void );

//==========================================================================================================================================
/**
 * @brief returns description of the hard fault
 */
//==========================================================================================================================================
extern const char * oC_MCS_GetHardFaultReasonDescription( void );

//==========================================================================================================================================
/**
 * @brief clears hard fault reason register
 */
//==========================================================================================================================================
extern void oC_MCS_ClearHardFaultStatus( void );

//==========================================================================================================================================
/**
 * @brief Globally enables interrupts (always)
 *
 * The function is for enabling interrupts. It is not critical section - when this function is called, interrupts are always enabled.
 *
 * @warning This function should not be used in most of cases! Use #oC_MCS_ExitCriticalSection instead of it!
 *
 * @see oC_MCS_DisableInterrupts, oC_MCS_AreInterruptsEnabled, oC_MCS_EnterCriticalSection , oC_MCS_ExitCriticalSection , oC_MCS_IsCriticalSectionActive
 */
//==========================================================================================================================================
extern void oC_MCS_EnableInterrupts( void );
//==========================================================================================================================================
/**
 * @brief Globally disables interrupts (always)
 *
 * The functions is for disabling interrupts. It is not critical section - when this function is called, interrupts are always disabled
 *
 * @warning This function should not be used in most of cases! Use #oC_MCS_EnterCriticalSection instead of it!
 *
 * @see oC_MCS_DisableInterrupts, oC_MCS_AreInterruptsEnabled, oC_MCS_EnterCriticalSection , oC_MCS_ExitCriticalSection , oC_MCS_IsCriticalSectionActive
 */
//==========================================================================================================================================
extern void oC_MCS_DisableInterrupts( void );

//==========================================================================================================================================
/**
 * @brief Checks if interrupts are enabled in HW
 *
 * The function checks if interrupts are already enabled, by checking flag in a hardware.
 *
 * @return true if interrupts are enabled
 *
 * @see oC_MCS_DisableInterrupts, oC_MCS_AreInterruptsEnabled, oC_MCS_EnterCriticalSection , oC_MCS_ExitCriticalSection , oC_MCS_IsCriticalSectionActive
 */
//==========================================================================================================================================
extern bool oC_MCS_AreInterruptsEnabled( void );

//==========================================================================================================================================
/**
 * @brief enables interrupt with specified number
 *
 * The function is for enabling interrupt with specified number.
 *
 * @param InterruptNumber       IRQ number to enable
 *
 * @return true if interrupt is correctly enabled
 *
 * @see oC_MCS_DisableInterrupt , oC_MCS_IsInterruptEnabled
 */
//==========================================================================================================================================
extern bool oC_MCS_EnableInterrupt( IRQn_Type InterruptNumber );

//==========================================================================================================================================
/**
 * @brief disables interrupt with specified number
 *
 * The function is for disabling interrupt with specified number.
 *
 * @param InterruptNumber       IRQ number to disable
 *
 * @return true if interrupt is correctly disabled
 *
 * @see oC_MCS_EnableInterrupt , oC_MCS_IsInterruptEnabled
 */
//==========================================================================================================================================
extern bool oC_MCS_DisableInterrupt( IRQn_Type InterruptNumber );

//==========================================================================================================================================
/**
 * @brief checks if interrupt is enabled
 *
 * Checks if an interrupt with specified number was enabled.
 *
 * @param InterruptNumber       IRQ number
 *
 * @return true if interrupt is enabled
 *
 * @see oC_MCS_EnableInterrupt, oC_MCS_DisableInterrupt
 */
//==========================================================================================================================================
extern bool oC_MCS_IsInterruptEnabled( IRQn_Type InterruptNumber );

//==========================================================================================================================================
/**
 * @brief sets interrupt priority
 *
 * Change priority of interrupt specified by InterruptNumber. Priority can be value between #oC_InterruptPriority_Minimum and #oC_InterruptPriority_Maximum.
 * The maximum value is the highest priority.
 *
 * @param InterruptNumber       IRQ number
 * @param Priority              Priority of interrupt, see #oC_InterruptPriority_t for details
 *
 * @return true priority was set
 *
 * @see oC_MCS_EnableInterrupt, oC_MCS_DisableInterrupt , oC_MCS_IsInterruptEnabled, oC_MCS_GetInterruptPriority
 */
//==========================================================================================================================================
extern bool oC_MCS_SetInterruptPriority( IRQn_Type InterruptNumber , oC_InterruptPriotity_t Priority );

//==========================================================================================================================================
/**
 * @brief returns interrupt priority
 *
 * Returns priority of interrupt specified by InterruptNumber. If error, it returns #oC_InterruptPriority_Error
 *
 * @param InterruptNumber       IRQ number
 *
 * @return priority of interrupt or #oC_InterruptPriority_Error if error
 *
 * @see oC_MCS_EnableInterrupt, oC_MCS_DisableInterrupt , oC_MCS_IsInterruptEnabled, oC_MCS_SetInterruptPriority
 */
//==========================================================================================================================================
extern oC_InterruptPriotity_t oC_MCS_GetInterruptPriority( IRQn_Type InterruptNumber );

//==========================================================================================================================================
/**
 * @brief Software reboots of machine
 *
 * The function reboots the machine using software bits. It should never exit. If it does, it means, that it is not implemented yet for the
 * given machine core.
 *
 * @warning This function should never ends!
 */
//==========================================================================================================================================
extern void oC_MCS_Reboot( void );

//==========================================================================================================================================
/**
 * @brief initializes stack for the system
 *
 * The function is initializing the stack for threads. It prepares machine general registers, such as stack pointers, returns pointers,
 * etc to work with new thread. When the stack is switched, then the machine reads registers states from the stack. In this case from the
 * stack, that was initialized by this function. Returns true if success
 *
 * @param outStack              [out] Stack to initialize
 * @param Buffer                Buffer for storing stack
 * @param BufferSize            Size of the buffer, cannot be smaller than value returned by function #oC_MCS_GetMinimumStackBufferSize
 * @param ContextHandler        Handler of the function that should be called during this context execution
 * @param HandlerParameter      Parameter to give to the context handler
 * @param ExitHandler           Handler of the function that should be called when the task is return
 *
 * @return true if success
 *
 * @see oC_MCS_SetNextStack , oC_MCS_GetFreeStackSize , oC_MCS_GetMinimumStackBufferSize
 */
//==========================================================================================================================================
extern bool oC_MCS_InitializeStack(
                oC_Stack_t *            outStack ,
                void *                  Buffer ,
                oC_Int_t                BufferSize ,
                oC_ContextHandler_t     ContextHandler  ,
                void *                  HandlerParameter ,
                oC_ContextExitHandler_t ExitHandler
                );

//==========================================================================================================================================
/**
 * @brief returns current value of PSP
 *
 * The function returns current process stack pointer
 *
 * @see oC_MCS_GetCurrentMainStackPointer , oC_MCS_GetFreeStackSize
 */
//==========================================================================================================================================
extern void * oC_MCS_GetCurrentProcessStackPointer( void );

//==========================================================================================================================================
/**
 * @brief returns current value of MSP
 *
 * The function returns current main stack pointer
 *
 * @see oC_MCS_GetCurrentProcessStackPointer , oC_MCS_GetFreeStackSize
 */
//==========================================================================================================================================
extern void * oC_MCS_GetCurrentMainStackPointer( void );

//==========================================================================================================================================
/**
 * @brief returns size of stack
 *
 * The function returns stack size of the stack
 *
 * @param Stack     Initialized stack. Set it to NULL if you want current stack
 *
 * @return size of memory on stack.
 */
//==========================================================================================================================================
extern oC_Int_t oC_MCS_GetStackSize( oC_Stack_t Stack );

//==========================================================================================================================================
/**
 * @brief returns number of free stack
 *
 * The function is for checking how much stack is free. When Stack is set to NULL, then it will check current stack.
 *
 * @param Stack             Initialized stack. Set it to NULL if you want current stack
 *
 * @return size of free memory on stack. Note, that this can be also smaller than 0
 *
 * @see oC_MCS_GetCurrentProcessStackPointer
 */
//==========================================================================================================================================
extern oC_Int_t oC_MCS_GetFreeStackSize( oC_Stack_t Stack );

//==========================================================================================================================================
/**
 * @brief returns current stack
 *
 * This returns stack of current thread.
 *
 * @see oC_MCS_GetCurrentProcessStackPointer
 */
//==========================================================================================================================================
extern oC_Stack_t oC_MCS_GetCurrentStack( void );

//==========================================================================================================================================
/**
 * @brief returns minimum stack buffer size
 *
 *
 * Returns minimum size of the buffer for storing stack
 *
 * @param StackSize     Required stack size
 */
//==========================================================================================================================================
extern oC_Int_t oC_MCS_GetMinimumStackBufferSize( oC_Int_t StackSize );

//==========================================================================================================================================
/**
 * @brief sets next stack for context switching
 *
 * The function sets next stack for kernel. The stack will be changed in the next context switching interrupt.
 *
 * @warning Remember, that stack should be initialized by #oC_MCS_InitializeStack function before this function, and that the system timer should
 * be configured first by using function #oC_MCS_ConfigureSystemTimer
 *
 * @param Stack     Initialized stack

 * @return true if success
 *
 * @see oC_MCS_GetCurrentStack, oC_MCS_InitializeStack, oC_MCS_ConfigureSystemTimer
 *
 * @code{.c}

   void MyThread( void * Argument )
   {
       while(1);
   }

   void MyThreadEnds( void )
   {
       printf("My thread does not exist anymore!");
   }

   oC_Stack_t stack         = NULL;
   oC_Int_t   bufferSize    = oC_MCS_GetMinimumStackBufferSize( 512 );
   void *     buffer        = malloc( bufferSize );

   oC_MCS_InitializeStack( &stack , buffer , bufferSize , MyThread , NULL , MyThreadEnds );

   // When context will be switched, the MyThread function will be called.
   oC_MCS_SetNextStack( stack );
   @endcode
 */
//==========================================================================================================================================
extern bool oC_MCS_SetNextStack( oC_Stack_t Stack );

//==========================================================================================================================================
/**
 * @brief configures system timer
 *
 * The function is for configuring system timer - special timer, that is designed for RTOS. The timer is periodically calling the special
 * interrupt (usually called SysTick), that is incrementing system tick counter, and call changing context interrupt if it is required.
 *
 * @param Prescaler                 Prescaler from system clock to system timer
 * @param FindNextStackHandler      Handler to call, when the changing current stack is required
 *
 * @return true if success
 *
 * @see oC_MCS_SetNextStack
 */
//==========================================================================================================================================
extern bool oC_MCS_ConfigureSystemTimer( oC_UInt_t Prescaler , oC_FindNextStackHandler_t FindNextStackHandler );

//==========================================================================================================================================
/**
 * @brief returns pointer to the system stack
 */
//==========================================================================================================================================
extern oC_Stack_t oC_MCS_GetSystemStack( void );

//==========================================================================================================================================
/**
 * @brief sets next stack as system stack
 *
 * @return true if success
 *
 * @see oC_MCS_SetNextStack
 */
//==========================================================================================================================================
extern bool oC_MCS_ReturnToSystemStack( void );

//==========================================================================================================================================
/**
 * @brief delays operations for cycles
 *
 * The function is for creating delays for the number of cycles that is given as the Cycles argument.
 *
 * @param Cycles            Number of cycles to delay
 */
//==========================================================================================================================================
extern void oC_MCS_Delay( register oC_UInt_t Cycles );

//==========================================================================================================================================
/**
 * @brief configures memory region
 *
 * The function is for configuration of the memory region. It allows to enable it, configure protection of the sector, etc. See #oC_MCS_MemoryRegionConfig_t
 * for more info.
 *
 * @param Config        Pointer to the configuration structure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MCS_ConfigureMemoryRegion( const oC_MCS_MemoryRegionConfig_t * Config );

//==========================================================================================================================================
/**
 * @brief returns maximum number of regions handled by the machine
 */
//==========================================================================================================================================
extern uint32_t oC_MCS_GetMaximumNumberOfRegions( void );

//==========================================================================================================================================
/**
 * @brief returns number of regions that are currently configured
 */
//==========================================================================================================================================
extern uint32_t oC_MCS_GetNumberOfRegions( void );

//==========================================================================================================================================
/**
 * @brief reads number of a free region
 *
 * @param outFreeRegionNumber       Pointer to the variable, where the region number should be stored
 *
 * @return true if found
 */
//==========================================================================================================================================
extern bool oC_MCS_ReadFreeRegionNumber( uint32_t * outFreeRegionNumber );

//==========================================================================================================================================
/**
 * @brief sets the memory access mode
 *
 * The function is for select between modes privileged and unprivileged. Thanks to that some memory regions can be protected against
 * not privileged user access.
 *
 * @param Mode          Access mode to set
 *
 * @return true if success
 */
//==========================================================================================================================================
extern bool oC_MCS_SetMemoryAccessMode( oC_MCS_MemoryAccessMode_t Mode );

//==========================================================================================================================================
/**
 * @brief reads memory access mode
 *
 * The function returns memory access mode, that is currently set.
 *
 * @see oC_MCS_SetMemoryAccessMode
 */
//==========================================================================================================================================
extern oC_MCS_MemoryAccessMode_t oC_MCS_GetMemoryAccessMode( void );

//==========================================================================================================================================
/**
 * @brief prints a message to debugger
 */
//==========================================================================================================================================
extern void oC_MCS_PrintToDebugger( const char * Message );

//==========================================================================================================================================
/**
 * @brief true if debugger is connected
 */
//==========================================================================================================================================
extern bool oC_MCS_IsDebuggerConnected( void );

//==========================================================================================================================================
/**
 * @brief halts the execution
 */
//==========================================================================================================================================
extern void oC_MCS_HALT( const char* Message );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with global variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________

extern int16_t oC_MCS_CriticalSectionCounter;

#undef  _________________________________________GLOBAL_VARIABLES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with static inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Increment or decrement counter for critical sections
 *
 * The function changes value of critical section counter. The #ValueToAdd will be added to the current value of counter. The function returns
 * true, when the critical counter is equal to 0.
 *
 * @param ValueToAdd
 *
 * @return true if counter is equal to 0
 *
 * @code{.c}
   if(oC_MCS_ChangeCriticalSectionCounter(0))
   {
       // Interrupts should be enabled!
   }
   @endcode
 *
 * @see oC_MCS_EnterCriticalSection , oC_MCS_ExitCriticalSection , oC_MCS_IsCriticalSectionActive
 */
//==========================================================================================================================================
static inline bool oC_MCS_ChangeCriticalSectionCounter( int8_t ValueToAdd )
{
    static const int16_t criticalCounterMax     = INT16_MAX - INT8_MAX;

    oC_MCS_CriticalSectionCounter += ValueToAdd;

    /* This assertion fails, when the critical counter was decremented too much time */
    oC_ASSERT(oC_MCS_CriticalSectionCounter >= 0);

    /* This assertion fails, when the critical counter achieve maximum safe value */
    oC_ASSERT(oC_MCS_CriticalSectionCounter < criticalCounterMax);

    return oC_MCS_CriticalSectionCounter == 0;
}

//==========================================================================================================================================
/**
 * @brief Enters to critical section
 *
 * The function begins critical section. It increments critical section counter and disable interrupts.
 *
 * @code

   oC_EnterCriticalSection();

   // Do very important things, that cannot be stopped. This should be as short as possible

   oC_ExitCriticalSection();

   @endcode
 *
 * @see oC_MCS_EnterCriticalSection , oC_MCS_ExitCriticalSection , oC_MCS_IsCriticalSectionActive
 */
//==========================================================================================================================================
static inline void oC_MCS_EnterCriticalSection( void )
{
    oC_MCS_DisableInterrupts();

    /* Add 1 to the critical section counter */
    oC_MCS_ChangeCriticalSectionCounter(1);
}

//==========================================================================================================================================
/**
 * @brief Exits from critical section
 *
 * The function ends critical section. It decrements counter and enables interrupts, when counter is equal to 0.
 *
 * @return true if interrupts are enabled
 *
 * @code{.c}

   oC_EnterCriticalSection();

   // Do very important things, that cannot be stopped. This should be as short as possible

   oC_ExitCriticalSection();

   @endcode

 * @see oC_MCS_EnterCriticalSection , oC_MCS_ExitCriticalSection , oC_MCS_IsCriticalSectionActive
 */
//==========================================================================================================================================
static inline bool oC_MCS_ExitCriticalSection( void )
{
    bool interruptsEnabled = false;

    if(oC_MCS_ChangeCriticalSectionCounter(-1))
    {
        oC_MCS_EnableInterrupts();
    }

    return interruptsEnabled;
}

//==========================================================================================================================================
/**
 * @brief checks if critical section is active
 *
 * The function is for checking if critical section is currently active (interrupts should be disabled then, but it is not checked in this
 * function)
 *
 * @return true if critical section is active
 *
 * @code{.c}

   oC_EnterCriticalSection();

   // Do very important things, that cannot be stopped. This should be as short as possible

   oC_ExitCriticalSection();

   if(oC_MCS_IsCriticalSectionActive())
   {
       // this means, that some other function begins critical section also
   }

   @endcode
 *
 * @see oC_MCS_EnterCriticalSection , oC_MCS_ExitCriticalSection , oC_MCS_IsCriticalSectionActive
 */
//==========================================================================================================================================
static inline bool oC_MCS_IsCriticalSectionActive( void )
{
    return oC_MCS_ChangeCriticalSectionCounter(0) == false;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#endif /* SYSTEM_PORTABLE_INC_MCS_ARM_ARM_CORTEX_M7_OC_MCS_H_ */
