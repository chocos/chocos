/** ****************************************************************************************************************************************
 *
 * @brief      Contains interface of the pins module
 *
 * @file       oc_pins.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup PinsModule Pins Module
 * @ingroup PortableSpace
 * @brief module with interface for handling machine pins
 *
 * @defgroup Channels Channels Module
 * @ingroup PortableSpace
 * @brief Module for managing machine module channels
 *
 * The module contains interface for managing machine module channels, for example SPI, UART, TIMER and more...
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_PINS_H_
#define SYSTEM_PORTABLE_INC_OC_PINS_H_

#include <oc_1word.h>
#include <oc_pins_defs.h>
#include <oc_pinsmap.h>
#include <oc_channels.h>
#include <oc_ba.h>
#include <oc_bits.h>
#include <oc_pinsmap.h>
#include <oc_array.h>

#if !oC_Channel_IsModuleDefined(GPIO)
#error GPIO module is not defined
#else

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup PinsModule
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if module pins are defined for a module
 *
 * The macro is for checking if module pins are defined for the given module. Example of usage:
 * @code{.c}
   #if oC_ModulePin_IsModuleDefined(UART) == false
       #error pins for module UART are not defined!
   #endif
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_IsModuleDefined(MODULE_NAME)                      defined(oC_MODULE_PINS_##MODULE_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if module pin functions are defined for a module
 *
 * The macro is for checking if module pin functions are defined for the given module. Example of usage:
 * @code{.c}
   #if oC_ModulePinFunctions_IsModuleDefined(UART) == false
       #error pin functions for module UART are not defined!
   #endif
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePinFunctions_IsModuleDefined(MODULE_NAME)             defined(oC_MODULE_PIN_FUNCTIONS_##MODULE_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns pin by name
 *
 * The macro is for receiving pin by using its name from the oc_pins_defs.h file.
 *
 * Example of usage:
 * @code{.c}
   oC_Pin_t led_pin = oC_Pin_(PF4);
   @endcode

   @see oC_Pin_t
 */
//==========================================================================================================================================
#define oC_Pin_(PIN_NAME,...)                                           oC_1WORD_FROM_2(oC_Pin_ , PIN_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns index of pin by name
 */
//==========================================================================================================================================
#define oC_PinIndex_(PIN_NAME,...)                                      oC_1WORD_FROM_2(oC_PinIndex_ , PIN_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns index of module pin
 */
//==========================================================================================================================================
#define oC_ModulePinIndex_(CHANNEL_NAME,PIN_FUNCTION,PIN_NAME,DIGITAL_VALUE)    oC_1WORD_FROM_8( oC_ModulePinIndex_ , CHANNEL_NAME , _ , PIN_FUNCTION , _ , PIN_NAME , _ , DIGITAL_VALUE )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns global index of pin function
 */
//==========================================================================================================================================
#define oC_PinFunctionIndex_(PIN_FUNCTION)                              oC_1WORD_FROM_2(oC_PinFunctionIndex_ , PIN_FUNCTION)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns function of module pin
 *
 * Returns a function of a module pin.
 */
//==========================================================================================================================================
#define oC_ModulePinFunction_(MODULE_NAME,PIN_FUNCTION)                 oC_1WORD_FROM_4(oC_,MODULE_NAME,_PinFunction_ , PIN_FUNCTION )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns module pin
 *
 * @code{.c}
   oC_ModulePinType_(GPIO) gpio_pin = oC_ModulePin_(GPIO,UART0,Rx);
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_(MODULE_NAME,CHANNEL_NAME,PIN_FUNCTION,PIN_NAME,DIGITAL_VALUE)       oC_1WORD_FROM_8(oC_ , MODULE_NAME, _Pin_ , CHANNEL_NAME , PIN_FUNCTION,PIN_NAME,_,DIGITAL_VALUE)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of type for storing module pin
 *
 * The macro is for creating a name of module pin type. For example:
 * @code{.c}
   oC_ModulePinType_(UART) uart_pin = oC_ModulePin_(UART,UART0,Rx);
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePinType_(MODULE_NAME)                                  oC_1WORD_FROM_3(oC_,MODULE_NAME,_Pin_t)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns mask for all bits of pins field set
 *
 * The macro returns mask with set all bits that are reserved for storing pins in #oC_Pin_t type. For example:
 * @code{.c}
   oC_Pin_t        pin          = oC_Pin_PF4;
   oC_PinsInPort_t pinsInPort   = oC_PinsInPortMask & pin;
   @endcode
 */
//==========================================================================================================================================
#define oC_PinsInPortMask                                   ( (1<<(oC_PORT_WIDTH))                  - 1 )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Mask for module pin function
 *
 * The macro returns mask for pin function (all bits set that are needed for storing pin function)
 */
//==========================================================================================================================================
#define oC_PinFunctionMask                                  ( (1<<(oC_MODULE_PIN_FUNCTION_WIDTH))    - 1 )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for alternate number for module pins
 *
 * The macro returns mask for alternate number for module pins.
 */
//==========================================================================================================================================
#define oC_PinAlternateNumberMask                           ( (1<<(oC_ALTERNATE_NUMBER_FIELD_WIDTH)) - 1 )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of pins in port field in #oC_Pin_t type
 */
//==========================================================================================================================================
#define oC_Pin_PinsInPortMaskWidth                          (oC_PORT_WIDTH)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of channel field in #oC_Pin_t type
 */
//==========================================================================================================================================
#define oC_Pin_ChannelMaskWidth                             (oC_CHANNEL_MASK_WIDTH)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of #oC_Pin_t type.
 */
//==========================================================================================================================================
#define oC_Pin_MaskWidth                                    ((oC_Pin_PinsInPortMaskWidth) + (oC_Pin_ChannelMaskWidth))

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns start bit (the lowest bit number) for pins in port field in #oC_Pin_t type
 */
//==========================================================================================================================================
#define oC_Pin_PinsInPortMaskStartBit                       (0)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Start bit for channel bit field
 */
//==========================================================================================================================================
#define oC_Pin_ChannelMaskStartBit                          ((oC_Pin_PinsInPortMaskWidth) + (oC_Pin_PinsInPortMaskStartBit))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Returns mask of pins in port in #oC_Pin_t type
 */
//==========================================================================================================================================
#define oC_Pin_PinsInPortMask                               ((oC_PinsInPortMask)    << (oC_Pin_PinsInPortMaskStartBit))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns channel mask in #oC_Pin_t type
 */
//==========================================================================================================================================
#define oC_Pin_ChannelMask                                  ((oC_ChannelMask)       << (oC_Pin_ChannelMaskStartBit) )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns mask for #oC_Pin_t type
 */
//==========================================================================================================================================
#define oC_Pin_Mask                                         ( (1<<(oC_Pin_MaskWidth)) - 1 )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief not for user usage
 *
 * Makes pin functions for module
 *
 * @note require MAKE_PIN_FUNCTION definition
 */
//==========================================================================================================================================
#define oC_Make_PinFunctionsForModule(MODULE_NAME)          oC_MODULE_PIN_FUNCTIONS_(MODULE_NAME)(MAKE_PIN_FUNCTION)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief not for user usage
 *
 * Makes pin function for module in pin function enum type
 */
//==========================================================================================================================================
#define oC_Make_PinFunctionForModule(PIN_FUNCTION,...)      oC_ModulePinFunction_(MODULE_NAME,PIN_FUNCTION) = oC_PinFunctionIndex_(PIN_FUNCTION) ,
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief not for user usage
 *
 * Makes module pin type for module
 */
//==========================================================================================================================================
#define oC_Make_ModulePinForModule( PIN_NAME , CHANNEL_NAME  , PIN_FUNCTION   , ALTERNATE_FUNCTION , ... ) \
    oC_ModulePin_(MODULE_NAME,CHANNEL_NAME,PIN_FUNCTION,PIN_NAME,ALTERNATE_FUNCTION) = oC_ModulePinIndex_(CHANNEL_NAME,PIN_FUNCTION,PIN_NAME,ALTERNATE_FUNCTION),

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns pins in port (#oC_PinsInPort_t type) from #oC_Pin_t
 *
 * The macro reads pins in port from the #oC_Pin_t type. Here is an example of usage:
 * @code{.c}
   oC_Pin_t ledPin      = oC_Pin_(PF1);
   oC_PinsInPort_t pins = oC_Pin_GetPinsInPort(ledPin);
   // now pins = (1<<1);
   @endcode
 */
//==========================================================================================================================================
#define oC_Pin_GetPinsInPort(Pin)                           ( ((Pin) & (oC_Pin_PinsInPortMask)) >> oC_Pin_PinsInPortMaskStartBit )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns GPIO channel assigned to the pin
 *
 * The macro returns channel of GPIO assigned to the pin from #oC_Pin_t type. Example of usage:
 * @code{.c}
   oC_Pin_t     ledPin = oC_Pin_(PF1);
   oC_Channel_t channel= oC_Pin_GetChannel(ledPin);
   // now channel = oC_Channel_PORTF;
   @endcode
 */
//==========================================================================================================================================
#define oC_Pin_GetChannel(Pin)                              ( ((Pin) & (oC_Pin_ChannelMask))    >> oC_Pin_ChannelMaskStartBit )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns port of pin
 *
 * The macro returns port of pin.
 *
 * @see oC_Pin_GetChannel
 */
//==========================================================================================================================================
#define oC_Pin_GetPort(Pin)                                 oC_Pin_GetChannel(Pin)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief makes #oC_Pin_t type from port and pins in port
 *
 * The macro is for creating #oC_Pin_t type (or #oC_Pins_t type) from port and pins. Example of usage:
 * @code{.c}
   oC_PinsInPort_t  pinsInPort = 0xFF;
   oC_Port_t        port       = oC_Channel_PORTF;
   oC_Pins_t        pins       = oC_Pin_Make(port,pinsInPort);
   @endcode
 */
//==========================================================================================================================================
#define oC_Pin_Make(Port,PinsInPort)                            \
    oC_Bits_ValueWithMask( PinsInPort , oC_Pin_PinsInPortMaskStartBit,  oC_Pin_PinsInPortMask) |  \
    oC_Bits_ValueWithMask( Port       , oC_Pin_ChannelMaskStartBit,     oC_Pin_ChannelMask)    \


//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief converts multipins to single pin
 *
 * @param Pins              Connected pins from the same port
 * @param PinIndex          Index of pin to get from the pins
 *
 * Example of usage:
 * @code{.c}
   oC_Pins_t pins = oC_Pin_PF4 | oC_Pin_PF3;
   oC_Pin_t  pin  = oC_Pins_ToSinglePin(pins,4); // now pin = oC_Pin_PF4;
   @endcode
 */
//==========================================================================================================================================
#define oC_Pins_ToSinglePin(Pins,PinIndex)                       ( ((Pins) & ((1<<PinIndex)<<oC_Pin_PinsInPortMaskStartBit)) | ((Pins) & oC_Pin_ChannelMask) )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns #oC_ModulePinData_t structure pointer
 *
 * The macro returns data structure for the module pin.
 */
//==========================================================================================================================================
#define oC_ModulePin_GetData(ModulePin)                          (&oC_ModulePinsData[ModulePin])
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns pin of module pin
 *
 * The macro is to read pin from a module pin type. Example of usage:
 * @code{.c}
   oC_ModulePinType_(UART) uartPin = oC_ModulePin_(UART,UART0,Rx);
   oC_Pin_t                pin     = oC_ModulePin_GetPin(uartPin);
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_GetPin(ModulePin)                              oC_ModulePin_GetData(ModulePin)->Pin
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns function of module pin
 *
 * The macro returns function of module pin (for example Rx/Tx for UART).
 *
 * Here is an example:
 * @code{.c}
   oC_ModulePinType_(UART) uartPin     = oC_ModulePin_(UART,UART0,Rx);
   oC_PinFunction_t        pinFunction = oC_ModulePin_GetPinFunction(uartPin);
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_GetPinFunction(ModulePin)                      oC_ModulePin_GetData(ModulePin)->PinFunction
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns channel from module pin type
 *
 * The macro returns channel from module pin type. Example:
 * @code{.c}
   oC_ModulePinType_(UART) uartPin = oC_ModulePin_(UART,UART0,Rx);
   oC_Channel_t            channel = oC_ModulePin_GetChannel(uartPin);
   // channel = oC_Channel_UART0 now
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_GetChannel(ModulePin)                          oC_ModulePin_GetData(ModulePin)->Channel

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns alternate number from module pin type
 *
 * The macro returns alternate number from module pin type. Example:
 * @code{.c}
   oC_PinAlternateNumber_t pinAlternateNumber = oC_ModulePin_GetAlternateNumber( uartPin );
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_GetAlternateNumber(ModulePin)                  oC_ModulePin_GetData(ModulePin)->AlternateNumber

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of the pin function
 *
 * The macro returns name of the pin function in user-friendly format
 */
//==========================================================================================================================================
#define oC_PinFunction_GetName(PinFunction)                         oC_PinFunctionsNames[PinFunction]

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief macro for definition of module pin type
 *
 * The macro defines module pin type for the module defined in oc_channels_defs.h file. Requires MODULE_NAME definition. Example of usage:
 * @code{.c}
   #define MODULE_NAME          UART
   oC_ModulePinFunction_DefineType;
   #undef MODULE_NAME
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePinFunction_DefineType     \
    typedef enum \
    { \
        oC_MODULE_PIN_FUNCTIONS_(MODULE_NAME)(oC_Make_PinFunctionForModule) \
        oC_Make_PinFunctionForModule(NumberOfElements)\
    } oC_ModulePinFunction_(MODULE_NAME,t)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief defines module pin type
 *
 * The macro is to define module pin type. Requires definition of MODULE_NAME. Example of usage:
 * @code{.c}
   #define MODULE_NAME      UART
   oC_ModulePinFunction_DefineType;
   oC_ModulePin_DefineType;
   #undef MODULE_NAME

   oC_ModulePinType_(UART) uartPin = oC_ModulePin_(UART,UART0,Rx);

   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_DefineType       \
    typedef enum    \
    { \
        oC_MODULE_PINS_(MODULE_NAME)(oC_Make_ModulePinForModule) \
    } oC_ModulePinType_(MODULE_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each defined pin
 *
 * The macro is for creating loop for each pin that is defined in the machine. Example of usage:
 * @code{.c}
   oC_Pin_ForeachDefined(pinData)
   {
       oC_Channel_t    channel  = oC_Pin_GetChannel(pinData->Pin);
       oC_PinsInPort_t pinsMask = oC_Pin_GetPinsInPort(pinData->Pin);
       printf("found pin %#x in port %s. Pin is named %s\n\r" , pinsMask , oC_Channel_GetName(channel) , pinData->Name );
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_Pin_ForeachDefined(PinVariableName)          oC_ARRAY_FOREACH_IN_ARRAY(oC_DefinedPins , PinVariableName)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each module pin (for all modules)
 *
 * The macro is for creating loop for each module pin that is defined in the machine. Example of usage:
 * @code{.c}
   oC_ModulePin_ForeachDefined(pin)
   {
       if(pin->Channel == oC_UART_Channel_UART0)
       {
           oC_UART_Pin_t * uartPin = pin;
           break;
       }
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_ModulePin_ForeachDefined(PinVariableName)    oC_ARRAY_FOREACH_IN_ARRAY(oC_ModulePinsData , PinVariableName)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup PinsModule
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing machine pins
 *
 * The type is for storing pins. List of pins is defined in oc_pins_defs.h file in format `oC_Pin_[PIN_NAME]`. Note, that you can join more pins in this
 * type if they are from the same port. Example of usage:
 * @code{.c}
   oC_Pin_t pin = oC_Pin_PF4 | oC_Pin_PF3 | oC_Pin_PF0;
   @endcode
 */
//==========================================================================================================================================
typedef enum
{
    oC_Pin_NotUsed = 0 ,
#define MAKE_CFG_PIN(USER_PIN_NAME , PIN_NAME)      oC_Pin_(USER_PIN_NAME) = oC_Pin_(PIN_NAME) ,
#define MAKE_PIN(BASE_ADDRESS_NAME , PIN_NAME , BIT_INDEX , PIN_NUMBER) \
    oC_Pin_(PIN_NAME) = oC_Pin_Make(oC_Channel_(BASE_ADDRESS_NAME) , 1 << BIT_INDEX) ,

    oC_PINS_LIST(MAKE_PIN)
    oC_CFG_PINS(MAKE_CFG_PIN)
#undef MAKE_PIN
#undef MAKE_CFG_PIN
} oC_Pin_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief index of pin in #oC_DefinedPins array
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_PIN(BASE_ADDRESS_NAME , PIN_NAME , BIT_INDEX , PIN_NUMBER) \
    oC_PinIndex_(PIN_NAME) ,
    oC_PINS_LIST(MAKE_PIN)
    oC_PinIndex_NumberOfElements
#undef MAKE_PIN
} oC_PinIndex_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief index of module pin in #oC_ModulePinsData array.
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_MODULE_PIN( PIN_NAME , CHANNEL_NAME  , PIN_FUNCTION   , ALTERNATE_FUNCTION )       \
                oC_ModulePinIndex_(CHANNEL_NAME,PIN_FUNCTION,PIN_NAME,ALTERNATE_FUNCTION) ,
#define MAKE_MODULE(MODULE_NAME)            oC_MODULE_PINS_(MODULE_NAME)(MAKE_MODULE_PIN)
    oC_MODULES_PINS_LIST(MAKE_MODULE)
#undef MAKE_MODULE
#undef MAKE_MODULE_PIN
    oC_ModulePinIndex_NumberOfElements
} oC_ModulePinIndex_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief index of module pin in #oC_ModulePinsData array.
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_MODULE_PIN_FUNCTION( PIN_FUNCTION )       \
                oC_PinFunctionIndex_(PIN_FUNCTION) ,
#define MAKE_MODULE(MODULE_NAME)            oC_MODULE_PIN_FUNCTIONS_(MODULE_NAME)(MAKE_MODULE_PIN_FUNCTION)
    oC_MODULES_PINS_LIST(MAKE_MODULE)
#undef MAKE_MODULE
#undef MAKE_MODULE_PIN_FUNCTION
    oC_PinFunctionIndex_NumberOfElements
} oC_PinFunctionIndex_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing more than 1 pin.
 *
 * The type is designed for storing more than one pin from the same port. It is equal to the #oC_Pin_t type, but for clearance it should be
 * used, when it is required to store not single pin.
 */
//==========================================================================================================================================
typedef oC_Pin_t oC_Pins_t;

//==========================================================================================================================================
/**
 * @brief stores port of GPIO pins
 *
 * The type is for storing port of GPIO pins. It is equal to the #oC_Channel_t type.
 */
//==========================================================================================================================================
typedef oC_Channel_t oC_Port_t;

//==========================================================================================================================================
/**
 * @brief stores port index
 *
 * Type for storing index of port in GPIO.
 *
 * @see oC_ChannelIndex_t
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_PortIndex_t;

#if oC_PORT_WIDTH <= 8
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pins in port
 *
 * The type is for storing pins of port. Example of usage:
 * @code{.c}
   oC_Pin_t         pin         = oC_Pin_PF4;
   oC_PinsInPort_t  pinsInPort  = oC_Pin_GetPinsInPort(pin);
   // now pinsInPort = (1<<4)
   @endcode
 */
//==========================================================================================================================================
typedef uint8_t oC_PinsInPort_t;
#elif oC_PORT_WIDTH <= 16
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pins in port
 *
 * The type is for storing pins of port. Example of usage:
 * @code{.c}
   oC_Pin_t         pin         = oC_Pin_PF4;
   oC_PinsInPort_t  pinsInPort  = oC_Pin_GetPinsInPort(pin);
   // now pinsInPort = (1<<4)
   @endcode
 */
//==========================================================================================================================================
typedef uint16_t oC_PinsInPort_t;
#elif oC_PORT_WIDTH <= 32
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pins in port
 *
 * The type is for storing pins of port. Example of usage:
 * @code{.c}
   oC_Pin_t         pin         = oC_Pin_PF4;
   oC_PinsInPort_t  pinsInPort  = oC_Pin_GetPinsInPort(pin);
   // now pinsInPort = (1<<4)
   @endcode
 */
//==========================================================================================================================================
typedef uint32_t oC_PinsInPort_t;
#elif oC_PORT_WIDTH <= 64
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pins in port
 *
 * The type is for storing pins of port. Example of usage:
 * @code{.c}
   oC_Pin_t         pin         = oC_Pin_PF4;
   oC_PinsInPort_t  pinsInPort  = oC_Pin_GetPinsInPort(pin);
   // now pinsInPort = (1<<4)
   @endcode
 */
//==========================================================================================================================================
typedef uint64_t oC_PinsInPort_t;
#else
#   error oC_PORT_WIDTH is too big! (max 64)
#endif

#if oC_ALTERNATE_NUMBER_FIELD_WIDTH <= 8
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing alternate number for pins
 *
 * The type is for storing alternate number of module pins. It is a special value, that is very often used in machines for connecting
 * GPIO pin to the specific module function, for example as UART Rx.
 */
//==========================================================================================================================================
typedef uint8_t oC_PinAlternateNumber_t;
#elif oC_ALTERNATE_NUMBER_FIELD_WIDTH <= 16
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing alternate number for pins
 *
 * The type is for storing alternate number of module pins. It is a special value, that is very often used in machines for connecting
 * GPIO pin to the specific module function, for example as UART Rx.
 */
//==========================================================================================================================================
typedef uint16_t oC_PinAlternateNumber_t;
#elif oC_ALTERNATE_NUMBER_FIELD_WIDTH <= 32
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing alternate number for pins
 *
 * The type is for storing alternate number of module pins. It is a special value, that is very often used in machines for connecting
 * GPIO pin to the specific module function, for example as UART Rx.
 */
//==========================================================================================================================================
typedef uint32_t oC_PinAlternateNumber_t;
#elif oC_ALTERNATE_NUMBER_FIELD_WIDTH <= 64
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing alternate number for pins
 *
 * The type is for storing alternate number of module pins. It is a special value, that is very often used in machines for connecting
 * GPIO pin to the specific module function, for example as UART Rx.
 */
//==========================================================================================================================================
typedef uint64_t oC_PinAlternateNumber_t;
#else
#   error oC_ALTERNATE_NUMBER_FIELD_WIDTH is too big! (max 64)
#endif

#if oC_MODULE_PIN_FUNCTION_WIDTH <= 8
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pin function number
 */
//==========================================================================================================================================
typedef uint8_t oC_PinFunction_t;
#elif oC_MODULE_PIN_FUNCTION_WIDTH <= 16
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pin function number
 */
//==========================================================================================================================================
typedef uint16_t oC_PinFunction_t;
#elif oC_MODULE_PIN_FUNCTION_WIDTH <= 32
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pin function number
 */
//==========================================================================================================================================
typedef uint32_t oC_PinFunction_t;
#elif oC_MODULE_PIN_FUNCTION_WIDTH <= 64
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pin function number
 */
//==========================================================================================================================================
typedef uint64_t oC_PinFunction_t;
#else
#   error oC_MODULE_PIN_FUNCTION_WIDTH is too big! (max 64)
#endif

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing pins bits
 *
 * @note This is legacy type
 *
 * The type is the same type as #oC_PinsInPort_t.
 */
//==========================================================================================================================================
typedef oC_PinsInPort_t oC_PinsMask_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing data of pin
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pin_t        Pin;        //!< Defined pin
    const char *    Name;       //!< Name of defined pin
} oC_DefinedPinData_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing data of module pin
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pin_t                    Pin;                //!< Pin of the module pin
    oC_PinAlternateNumber_t     AlternateNumber;    //!< Number for alternate function selection
    oC_Channel_t                Channel;            //!< Channel connected to the module pin
    oC_PinFunction_t            PinFunction;        //!< Function of the pin (Rx/Tx,etc)
    oC_ModulePinIndex_t         ModulePinIndex;     //!< Index of the module pin
} oC_ModulePinData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief array with pins, that are defined in the machine
 *
 * This array contains all pins that are defined in the machine
 */
//==========================================================================================================================================
extern const oC_DefinedPinData_t oC_DefinedPins[oC_PinIndex_NumberOfElements];

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief array with data for module pins
 *
 * This is an array with data for module pins.
 */
//==========================================================================================================================================
extern const oC_ModulePinData_t oC_ModulePinsData[oC_ModulePinIndex_NumberOfElements];

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief array with names of pin functions
 */
//==========================================================================================================================================
extern const char * oC_PinFunctionsNames[oC_PinFunctionIndex_NumberOfElements];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


#endif /* SYSTEM_PORTABLE_INC_OC_PINS_H_ */
#endif
