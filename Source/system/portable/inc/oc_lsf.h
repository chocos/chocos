/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for LSF module
 *
 * @file       oc_lsf.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup LSF (LSF) - Linker Specific Functions
 * @ingroup PortableSpace
 * @brief Linker Specific Functions
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_LSF_H_
#define SYSTEM_PORTABLE_INC_OC_LSF_H_

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup LSF
//! @{

#define oC_LSF_IsNotNull(Address)               ( ((void*)(Address)) != NULL )
#define oC_LSF_IsExternalAddress(Address)       ((((void*)(Address)) >= oC_LSF_GetExternalStart()) && (((void*)(Address)) < oC_LSF_GetExternalEnd()) && oC_LSF_IsNotNull(Address))
#define oC_LSF_IsRomAddress(Address)            ((((void*)(Address)) >= oC_LSF_GetRomStart())      && (((void*)(Address)) < oC_LSF_GetRomEnd())      && oC_LSF_IsNotNull(Address))
#define oC_LSF_IsRamAddress(Address)            ((((void*)(Address)) >= oC_LSF_GetRamStart())      && (((void*)(Address)) < oC_LSF_GetRamEnd())      && oC_LSF_IsNotNull(Address))
#define oC_LSF_IsDmaAddress(Address)            ((((void*)(Address)) >= oC_LSF_GetDmaStart())      && (((void*)(Address)) < oC_LSF_GetDmaEnd())      && oC_LSF_IsNotNull(Address))

#define oC_LSF_IsCorrectAddress(Address)        ((oC_LSF_IsRamAddress(Address) || oC_LSF_IsRomAddress(Address)) || oC_LSF_IsExternalAddress(Address) || oC_LSF_IsDmaAddress(Address))

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}
//!

/** ========================================================================================================================================
 *
 *              The section with linker variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LINKER_VARIABLES_SECTION___________________________________________________________________

extern const char __rom_start;
extern const char __rom_end;
extern const char __rom_size;
extern const char __ram_start;
extern const char __ram_end;
extern const char __ram_size;
extern const char __data_start;
extern const char __data_end;
extern const char __data_size;
extern const char __bss_start;
extern const char __bss_end;
extern const char __bss_size;
extern const char __stack_start;
extern const char __stack_end;
extern const char __stack_size;
extern const char __text_start;
extern const char __text_end;
extern const char __text_size;
extern const char __heap_start;
extern const char __heap_end;
extern const char __heap_size;
extern const char __main_stack_start;
extern const char __main_stack_end;
extern const char __main_stack_size;
extern const char __ext_start;
extern const char __ext_end;
extern const char __ext_size;
extern const char __dma_start;
extern const char __dma_end;
extern const char __dma_size;
extern const char __process_stack_start;
extern const char __process_stack_end;
extern const char __process_stack_size;

#undef  _________________________________________LINKER_VARIABLES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


//==========================================================================================================================================
/**
 * @brief returns rom start address
 *
 * Returns address from linkage
 *
 * @return address of the start of rom
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetRomStart( void )
{
    return (void*)&__rom_start;
}

//==========================================================================================================================================
/**
 * @brief returns rom end address
 *
 * Returns address from linkage
 *
 * @return address of the of rom
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetRomEnd( void )
{
    return (void*)&__rom_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetRomSize( void )
{
    return (oC_UInt_t)&__rom_size;
}

//==========================================================================================================================================
/**
 * @brief returns ram start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetRamStart( void )
{
    return (void*)&__ram_start;
}

//==========================================================================================================================================
/**
 * @brief returns ram end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetRamEnd( void )
{
    return (void*)&__ram_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetRamSize( void )
{
    return (oC_UInt_t)&__ram_size;
}

//==========================================================================================================================================
/**
 * @brief returns data start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetDataStart( void )
{
    return (void*)&__data_start;
}

//==========================================================================================================================================
/**
 * @brief returns data end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetDataEnd( void )
{
    return (void*)&__data_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetDataSize( void )
{
    return (oC_UInt_t)&__data_size;
}

//==========================================================================================================================================
/**
 * @brief returns bss start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetBssStart( void )
{
    return (void*)&__bss_start;
}

//==========================================================================================================================================
/**
 * @brief returns bss end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetBssEnd( void )
{
    return (void*)&__bss_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetBssSize( void )
{
    return (oC_UInt_t)&__bss_size;
}

//==========================================================================================================================================
/**
 * @brief returns text start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetTextStart( void )
{
    return (void*)&__text_start;
}

//==========================================================================================================================================
/**
 * @brief returns text end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetTextEnd( void )
{
    return (void*)&__text_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetTextSize( void )
{
    return (oC_UInt_t)&__text_size;
}

//==========================================================================================================================================
/**
 * @brief returns heap start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetHeapStart( void )
{
    return (void*)&__heap_start;
}

//==========================================================================================================================================
/**
 * @brief returns heap end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetHeapEnd( void )
{
    return (void*)&__heap_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetHeapSize( void )
{
    return (oC_UInt_t)&__heap_size;
}

//==========================================================================================================================================
/**
 * @brief returns stack start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetStackStart( void )
{
    return (void*)&__stack_start;
}

//==========================================================================================================================================
/**
 * @brief returns stack end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetStackEnd( void )
{
    return (void*)&__stack_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetStackSize( void )
{
    return (oC_UInt_t)&__stack_size;
}

//==========================================================================================================================================
/**
 * @brief returns stack start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetProcessStackStart( void )
{
    return (void*)&__process_stack_start;
}

//==========================================================================================================================================
/**
 * @brief returns stack end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetProcessStackEnd( void )
{
    return (void*)&__process_stack_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetProcessStackSize( void )
{
    return (oC_UInt_t)&__process_stack_size;
}

//==========================================================================================================================================
/**
 * @brief returns stack start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetMainStackStart( void )
{
    return (void*)&__main_stack_start;
}

//==========================================================================================================================================
/**
 * @brief returns stack end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetMainStackEnd( void )
{
    return (void*)&__main_stack_end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetMainStackSize( void )
{
    return (oC_UInt_t)&__main_stack_size;
}

//==========================================================================================================================================
/**
 * @brief returns stack start address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetExternalStart( void )
{
    void * start = (void*)&__ext_start;
    return start;
}

//==========================================================================================================================================
/**
 * @brief returns stack end address
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetExternalEnd( void )
{
    void * end = (void*)&__ext_end;
    return end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetExternalSize( void )
{
    oC_UInt_t size = (oC_UInt_t)&__ext_size;
    return size;
}

//==========================================================================================================================================
/**
 * @brief returns start address of DMA section
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetDmaStart( void )
{
    void * start = (void*)&__dma_start;
    return start;
}

//==========================================================================================================================================
/**
 * @brief returns end address of DMA section
 *
 * Returns address from linkage
 *
 * @return address of the point
 */
//==========================================================================================================================================
static inline void * oC_LSF_GetDmaEnd( void )
{
    void * end = (void*)&__dma_end;
    return end;
}

//==========================================================================================================================================
/**
 * @brief returns size of the section in linkage
 *
 * Returns size of the section in linkage
 *
 * @return size of the section
 */
//==========================================================================================================================================
static inline oC_UInt_t oC_LSF_GetDmaSize( void )
{
    oC_UInt_t size = (oC_UInt_t)&__dma_size;
    return size;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#endif /* SYSTEM_PORTABLE_INC_OC_LSF_H_ */
