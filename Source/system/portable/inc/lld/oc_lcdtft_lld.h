/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the LCDTFT driver
 *
 * @file       oc_lcdtft_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 2016-02-06 - 11:43:03)
 *
 * @copyright  Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup LCDTFT-LLD LCDTFT 
 * @ingroup LowLevelDrivers
 * @brief Handles LCDTFT transmissions
 *
 ******************************************************************************************************************************************/


#ifndef _OC_LCDTFT_LLD_H
#define _OC_LCDTFT_LLD_H

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>
#include <oc_color.h>
#include <oc_pixel.h>

#if oC_Channel_IsModuleDefined(LCDTFT) == true && oC_ModulePinFunctions_IsModuleDefined(LCDTFT) == true && oC_ModulePin_IsModuleDefined(LCDTFT) == true
#define oC_LCDTFT_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup LCDTFT-LLD
//! @{
#define MODULE_NAME LCDTFT


//==========================================================================================================================================
/**
 * @enum oC_LCDTFT_Channel_t
 * @brief channel of the LCDTFT
 *
 * The type stores channel of the LCDTFT
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_LCDTFT_LLD_PinFunction_t
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_LCDTFT_Pin_t
 * @brief pin of the LCDTFT peripheral
 *
 * The type stores peripheral pin, where the LCDTFT channel is connected.
 */
//==========================================================================================================================================
oC_ModulePin_DefineType;
//==========================================================================================================================================
/**
 * @brief stores index of channel in the LCDTFT
 *
 * The type stores index of the channel in the LCDTFT array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_LCDTFT_LLD_ChannelIndex_t;

typedef enum
{
    oC_LCDTFT_LLD_PixelClockPolarity_InputPixelClock ,
    oC_LCDTFT_LLD_PixelClockPolarity_InvertedInputPixelClock
} oC_LCDTFT_LLD_PixelClockPolarity_t;

typedef enum
{
    oC_LCDTFT_LLD_Polarity_ActiveLow ,
    oC_LCDTFT_LLD_Polarity_ActiveHigh
} oC_LCDTFT_LLD_Polarity_t;

typedef enum
{
    oC_LCDTFT_LLD_PinIndex_R0 ,
    oC_LCDTFT_LLD_PinIndex_R1 ,
    oC_LCDTFT_LLD_PinIndex_R2 ,
    oC_LCDTFT_LLD_PinIndex_R3 ,
    oC_LCDTFT_LLD_PinIndex_R4 ,
    oC_LCDTFT_LLD_PinIndex_R5 ,
    oC_LCDTFT_LLD_PinIndex_R6 ,
    oC_LCDTFT_LLD_PinIndex_R7 ,
    oC_LCDTFT_LLD_PinIndex_G0 ,
    oC_LCDTFT_LLD_PinIndex_G1 ,
    oC_LCDTFT_LLD_PinIndex_G2 ,
    oC_LCDTFT_LLD_PinIndex_G3 ,
    oC_LCDTFT_LLD_PinIndex_G4 ,
    oC_LCDTFT_LLD_PinIndex_G5 ,
    oC_LCDTFT_LLD_PinIndex_G6 ,
    oC_LCDTFT_LLD_PinIndex_G7 ,
    oC_LCDTFT_LLD_PinIndex_B0 ,
    oC_LCDTFT_LLD_PinIndex_B1 ,
    oC_LCDTFT_LLD_PinIndex_B2 ,
    oC_LCDTFT_LLD_PinIndex_B3 ,
    oC_LCDTFT_LLD_PinIndex_B4 ,
    oC_LCDTFT_LLD_PinIndex_B5 ,
    oC_LCDTFT_LLD_PinIndex_B6 ,
    oC_LCDTFT_LLD_PinIndex_B7 ,
    oC_LCDTFT_LLD_PinIndex_VSYNC ,
    oC_LCDTFT_LLD_PinIndex_HSYNC ,
    oC_LCDTFT_LLD_PinIndex_CLK ,
    oC_LCDTFT_LLD_PinIndex_DE ,
    oC_LCDTFT_LLD_PinIndex_DISP ,
    oC_LCDTFT_LLD_PinIndex_BL_CTRL ,
    oC_LCDTFT_LLD_PinIndex_LED_K ,
    oC_LCDTFT_LLD_PinIndex_LED_A ,
    oC_LCDTFT_LLD_PinIndex_NumberOfPins ,
} oC_LCDTFT_LLD_PinIndex_t;


typedef union
{
    struct
    {
        oC_Pin_t                        R[8];
        oC_Pin_t                        G[8];
        oC_Pin_t                        B[8];
        oC_Pin_t                        VSYNC;
        oC_Pin_t                        HSYNC;
        oC_Pin_t                        CLK;
        oC_Pin_t                        DE;
        oC_Pin_t                        DISP;
        oC_Pin_t                        BL_CTRL;
        oC_Pin_t                        LED_K;
        oC_Pin_t                        LED_A;
    };
    oC_Pin_t    PinsArray[oC_LCDTFT_LLD_PinIndex_NumberOfPins];
} oC_LCDTFT_LLD_Pins_t;

typedef struct
{
    uint32_t    PulseWidth;
    uint32_t    BackPorch;
    uint32_t    FrontPorch;
} oC_LCDTFT_LLD_SyncParameters_t;

typedef struct
{
    oC_LCDTFT_LLD_SyncParameters_t  HSYNC;
    oC_LCDTFT_LLD_SyncParameters_t  VSYNC;
} oC_LCDTFT_LLD_TimingParameters_t;

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** =========================   ===============================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup LCDTFT-LLD
//! @{

extern oC_ErrorCode_t oC_LCDTFT_LLD_TurnOnDriver( void );
extern oC_ErrorCode_t oC_LCDTFT_LLD_TurnOffDriver( void );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetPower( oC_Power_t Power );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadPower( oC_Power_t * outPower );
extern oC_ErrorCode_t oC_LCDTFT_LLD_EnableOperations( void );
extern oC_ErrorCode_t oC_LCDTFT_LLD_DisableOperations( void );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetFrequency( oC_Frequency_t Frequency , oC_Frequency_t PermissibleDifference );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadFrequency( oC_Frequency_t * outFrequency );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetPixelClockPolarity( oC_LCDTFT_LLD_PixelClockPolarity_t PixelPolarity );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadPixelClockPolarity( oC_LCDTFT_LLD_PixelClockPolarity_t * outPixelPolarity );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetPolarities( oC_LCDTFT_LLD_Polarity_t HSyncPolarity , oC_LCDTFT_LLD_Polarity_t VSyncPolarity , oC_LCDTFT_LLD_Polarity_t DataEnablePolarity );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadPolarities( oC_LCDTFT_LLD_Polarity_t * outHSyncPolarity , oC_LCDTFT_LLD_Polarity_t * outVSyncPolarity , oC_LCDTFT_LLD_Polarity_t * outDataEnablePolarity );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetResolution( oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadResolution( oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight );
extern oC_ErrorCode_t oC_LCDTFT_LLD_RestoreDefaultState( void );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ConnectPins( const oC_LCDTFT_LLD_Pins_t * Pins );
extern oC_ErrorCode_t oC_LCDTFT_LLD_DisconnectPins( const oC_LCDTFT_LLD_Pins_t * Pins );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetColormapBuffer( const void * Buffer );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetPixelFormat( oC_ColorFormat_t PixelFormat );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadPixelFormat( oC_ColorFormat_t * outPixelFormat );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetTimingParameters(  const oC_LCDTFT_LLD_TimingParameters_t * TimingParameters );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadTimingParameters(       oC_LCDTFT_LLD_TimingParameters_t * outTimingParameters );
extern oC_ErrorCode_t oC_LCDTFT_LLD_SetBackgroundColor( oC_Color_t Color , oC_ColorFormat_t ColorFormat );
extern oC_ErrorCode_t oC_LCDTFT_LLD_ReadBackgroundColor( oC_Color_t * outColor , oC_ColorFormat_t ColorFormat );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* _OC_LCDTFT_LLD_H */
#endif
