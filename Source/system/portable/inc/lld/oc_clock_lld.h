/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the CLOCK driver
 *
 * @file       oc_clock_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 30 04 2015 19:19:52)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup CLOCK-LLD Clock 
 * @ingroup LowLevelDrivers
 *
 * @brief System clock related operations
 *
 * @par
 * The driver is for operations that are based on the system clock, such as configuring it as internal/external, simple delaying operations
 * by using special loops, and so on. This layer also provide an special interrupt, that occurs, when the system clock is reconfigured, what
 * can be used for example for reconfiguring drivers that are clock related, such as #SPI or #UART.
 *
 * @par
 * The module must be turned on before usage by using the function #oC_CLOCK_LLD_TurnOnDriver. When it is not needed anymore, the #oC_CLOCK_LLD_TurnOffDriver
 * function can be used for turning it off. The default state will be restored (clock will be configured to work in default state).
 *
 * @par
 * The clock can be configured to work with internal or external oscillator by functions #oC_CLOCK_LLD_ConfigureInternalClock and #oC_CLOCK_LLD_ConfigureExternalClock.
 * There is also a possibility to configure extra external hibernation oscillator by function #oC_CLOCK_LLD_ConfigureHibernationClock.
 * Every time, when the clock is configured, the special interrupt named **Clock Configured Interrupt** occurs. The handler of this interrupt
 * can be set by function #oC_CLOCK_LLD_SetClockConfiguredInterrupt, but it can be done only once after enabling of the module.
 *
 * @par
 * The module provide also a function for delay operations. It is called #oC_CLOCK_LLD_DelayForMicroseconds and it allow to block a CPU for
 * the time given in microseconds.
 *
 ******************************************************************************************************************************************/
#ifndef INC_LLD_OC_CLOCK_LLD_H_
#define INC_LLD_OC_CLOCK_LLD_H_

#include <oc_machine.h>
#include <oc_frequency.h>
#include <oc_errors.h>
#include <stdbool.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup CLOCK-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief type for storing source of the system clock
 *
 * The type is for storing source of the system clock.
 */
//==========================================================================================================================================
typedef enum
{
    oC_CLOCK_LLD_ClockSource_Internal ,     //!< an internal clock source is used
    oC_CLOCK_LLD_ClockSource_External       //!< an external source of the clock is used
} oC_CLOCK_LLD_ClockSource_t;

//==========================================================================================================================================
/**
 * @brief type for storing interrupts pointers
 *
 * @param Frequency     New frequency, that was configured
 *
 * The type is for storing pointers to interrupts for the **CLOCK-LLD** module.
 */
//==========================================================================================================================================
typedef void (*oC_CLOCK_LLD_Interrupt_t)( oC_Frequency_t Frequency );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with driver interface function prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup CLOCK-LLD
//! @{


//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_CLOCK_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_CLOCK_LLD_TurnOffDriver( void );

//==========================================================================================================================================
/**
 * @brief returns source of the system clock
 *
 * The function returns actual configured clock source.
 *
 * @return source of the system clock
 */
//==========================================================================================================================================
extern oC_CLOCK_LLD_ClockSource_t oC_CLOCK_LLD_GetClockSource( void );

//==========================================================================================================================================
/**
 * @brief returns frequency of the system clock
 *
 * The function is for reading actual clock frequency configuration
 *
 * @return configured frequency of the system clock
 */
//==========================================================================================================================================
extern oC_Frequency_t oC_CLOCK_LLD_GetClockFrequency( void );

//==========================================================================================================================================
/**
 * @brief returns frequency of the oscillator
 *
 * @return configured oscillator frequency
 */
//==========================================================================================================================================
extern oC_Frequency_t oC_CLOCK_LLD_GetOscillatorFrequency( void );

//==========================================================================================================================================
/**
 * @brief returns maximum frequency permissible for the machine
 *
 * The function returns maximum frequency that is possible for the given machine.
 */
//==========================================================================================================================================
extern oC_Frequency_t oC_CLOCK_LLD_GetMaximumClockFrequency( void );

//==========================================================================================================================================
/**
 * @brief configures an interrupt for clock configured event
 *
 * The function is for setting a function handler for an interrupt that occurs, when the system clock is configured. The handler is called
 * every time, when some module will change the clock configuration. It can be useful for some frequency related modules, such as #UART and
 * #SPI, that use a system clock for its internal transmission clocks.
 *
 * @note The interrupt can be set only once after turning on the module! When it is needed to reconfigure it, the module must be restarted.
 *
 * @param Interrupt     Pointer to the interrupt handler function
 *
 * @return code of error, or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_CLOCK_LLD_SetClockConfiguredInterrupt( oC_CLOCK_LLD_Interrupt_t Interrupt );

//==========================================================================================================================================
/**
 * @brief perform a delay for us
 *
 * The function is for performing a delay in microseconds. It is important, that this function should be implemented as simple as possible
 * (for example by using a loop) to provide shortest time that is possible. Note, that when this function is used, there is no guarantee, that
 * some other system task will not interrupt this process, so there is a possible that this delay will stay for more time that expected. It
 * is recommended only for short delays, that can not be achieved by using other delay functions.
 *
 * @note It uses clock frequency to count number of cycles, that should be delayed.
 *
 * @param Microseconds      Number of microseconds to delay
 *
 * @return true if success, false if this function is not implemented already in the target machine.
 */
//==========================================================================================================================================
extern bool oC_CLOCK_LLD_DelayForMicroseconds( oC_UInt_t Microseconds );

//==========================================================================================================================================
/**
 * @brief configures system clock in internal mode
 *
 * The function configures the system clock to work with internal oscillator with given frequency.
 *
 * @param TargetFrequency           The frequency to achieve for the system clock.
 * @param PermissibleDifference     Maximum difference between real frequency and the wanted frequency that is acceptable by the user
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_CLOCK_LLD_ConfigureInternalClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference );

//==========================================================================================================================================
/**
 * @brief configures system clock to work in external mode
 *
 * @param TargetFrequency           The frequency to achieve for the system clock.
 * @param PermissibleDifference     Maximum difference between real frequency and the wanted frequency that is acceptable by the user
 * @param OscillatorFrequency       The frequency of the external oscillator that is connected to the machine
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_CLOCK_LLD_ConfigureExternalClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference , oC_Frequency_t OscillatorFrequency );

//==========================================================================================================================================
/**
 * @brief configures system clock to work in hibernation mode
 *
 * @param TargetFrequency           The frequency to achieve for the system clock.
 * @param PermissibleDifference     Maximum difference between real frequency and the wanted frequency that is acceptable by the user
 * @param OscillatorFrequency       The frequency of the external oscillator connected to the correct pins in the machine
 *
 * @return code of error
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_CLOCK_LLD_ConfigureHibernationClock( oC_Frequency_t TargetFrequency , oC_Frequency_t PermissibleDifference , oC_Frequency_t OscillatorFrequency );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_LLD_OC_CLOCK_LLD_H_ */
