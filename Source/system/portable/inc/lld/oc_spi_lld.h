/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the SPI driver
 *
 * @file       oc_spi_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 2017-07-20 - 00:42:25)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SPI-LLD SPI 
 * @ingroup LowLevelDrivers
 * @brief Handles SPI transmissions
 *
 ******************************************************************************************************************************************/


#ifndef _OC_SPI_LLD_H
#define _OC_SPI_LLD_H

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>

#if oC_Channel_IsModuleDefined(SPI) && oC_ModulePinFunctions_IsModuleDefined(SPI) && oC_ModulePin_IsModuleDefined(SPI)
#define oC_SPI_LLD_AVAILABLE

#if oC_Channel_IsModuleDefined(SPI) == false
#error SPI module channels are not defined
#elif oC_ModulePinFunctions_IsModuleDefined(SPI) == false
#error  SPI module pin functions are not defined
#elif oC_ModulePin_IsModuleDefined(SPI) == false
#error  SPI module pins are not defined
#else

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup SPI-LLD
//! @{
#define MODULE_NAME SPI


//==========================================================================================================================================
/**
 * @enum oC_SPI_Channel_t
 * @brief channel of the SPI
 *
 * The type stores channel of the SPI
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_SPI_LLD_PinFunction_t
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_SPI_Pin_t
 * @brief pin of the SPI peripheral
 *
 * The type stores peripheral pin, where the SPI channel is connected.
 */
//==========================================================================================================================================
oC_ModulePin_DefineType;
//==========================================================================================================================================
/**
 * @brief stores index of channel in the SPI
 *
 * The type stores index of the channel in the SPI array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_SPI_LLD_ChannelIndex_t;

//==========================================================================================================================================
/**
 * @brief stores mode of transmission
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_LLD_Mode_Master ,//!< Driver works as master
    oC_SPI_LLD_Mode_Slave   //!< Driver works as slave
} oC_SPI_LLD_Mode_t;

//==========================================================================================================================================
/**
 * @brief stores frame format
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_LLD_FrameFormat_MSB ,    //!< MSB is first
    oC_SPI_LLD_FrameFormat_LSB ,    //!< LSB is first
} oC_SPI_LLD_FrameFormat_t;

//==========================================================================================================================================
/**
 * @brief stores clock polarity
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_LLD_ClockPolarity_HighActive ,   //!< Clock is active when CLK is 1
    oC_SPI_LLD_ClockPolarity_LowActive ,    //!< Clock is active when CLK is 0
} oC_SPI_LLD_ClockPolarity_t;

//==========================================================================================================================================
/**
 * @brief stores clock phase
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_LLD_ClockPhase_FirstTransition ,  //!< The first clock transition is the first data capture edge
    oC_SPI_LLD_ClockPhase_SecondTransition , //!< The second clock transition is the first data capture edge
} oC_SPI_LLD_ClockPhase_t;

//==========================================================================================================================================
/**
 * @brief stores source of interrupt
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_LLD_InterruptSource_None                 = 0 ,       //!< None of known interrupt source occurred
    oC_SPI_LLD_InterruptSource_TxFifoNotFull        = (1<<0) ,  //!< Tx FIFO is empty
    oC_SPI_LLD_InterruptSource_RxFifoNotEmpty       = (1<<1) ,  //!< Rx FIFO is not empty
    oC_SPI_LLD_InterruptSource_TransmissionError    = (1<<2) ,  //!< There was an error during transmission
} oC_SPI_LLD_InterruptSource_t;

//==========================================================================================================================================
/**
 * @brief stores size of data in SPI transmission
 */
//==========================================================================================================================================
typedef uint8_t oC_SPI_LLD_DataSize_t;

//==========================================================================================================================================
/**
 * @brief stores SPI data
 */
//==========================================================================================================================================
typedef uint32_t oC_SPI_LLD_Data_t;

//==========================================================================================================================================
/**
 * @brief stores pointer to the interrupt handler
 *
 * The type is for storing pointer to the interrupt handler.
 *
 * @param Channel           Channel of SPI that caused the event
 * @param InterruptSource   Source of the interrupt
 */
//==========================================================================================================================================
typedef void (*oC_SPI_LLD_InterruptHandler_t)( oC_SPI_Channel_t Channel, oC_SPI_LLD_InterruptSource_t InterruptSource );

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each SPI channel
 *
 * The macro is for creating a loop for each channel defined in the machine. As 'index' there is a variable that stores the channel ID.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * // example of loop that restores default states on each SPI channel defined in the machine
 * oC_SPI_LLD_ForEachChannel(Channel , ChannelIndex)
 * {
 *      // this will be executed for each channel
 *      oC_SPI_LLD_RestoreDefaultStateOnChannel( Channel );
 * }
 * ~~~~~~~~~~~~
 *
 */
//==========================================================================================================================================
#define oC_SPI_LLD_ForEachChannel( Channel , ChannelIndex )           oC_Channel_Foreach( SPI , Channel )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup SPI-LLD
//! @{


//==========================================================================================================================================
/**
 * @brief checks if the SPI channel is correct
 *
 * Checks if the SPI channel is correct.
 *
 * @param Channel       Channel of the SPI to check
 *
 * @return true if channel is correct
 */
//==========================================================================================================================================
extern bool oC_SPI_LLD_IsChannelCorrect( oC_SPI_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief check if the channel index is correct
 *
 * Checks if the SPI channel index is correct.
 *
 * @param ChannelIndex  Index of the channel in module
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_SPI_LLD_IsChannelIndexCorrect( oC_SPI_LLD_ChannelIndex_t ChannelIndex );
//==========================================================================================================================================
/**
 * @brief converts channel to channel index
 *
 * The function converts channel to index of the channel in the SPI module
 *
 * @param Channel       Channel of the SPI
 *
 * @return index of the channel in the SPI
 */
//==========================================================================================================================================
extern oC_SPI_LLD_ChannelIndex_t oC_SPI_LLD_ChannelToChannelIndex( oC_SPI_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief converts channel index to channel
 *
 * The function converts index of the channel to channel in the SPI module
 *
 * @param Channel       Channel of the SPI
 *
 * @return index of the channel in the SPI
 */
//==========================================================================================================================================
extern oC_SPI_Channel_t oC_SPI_LLD_ChannelIndexToChannel( oC_SPI_LLD_ChannelIndex_t Channel );
//==========================================================================================================================================
/**
 * @brief returns channel of peripheral pin
 *
 * The function reads channel of peripheral pin. It not checks if the channel is correct, use #oC_SPI_LLD_IsChannelCorrect function to check
 * if returned channel is correct.
 *
 * @param ModulePin         special pin that is connected to SPI channel
 *
 * @return channel of SPI (it can be not correct)
 */
//==========================================================================================================================================
extern oC_SPI_Channel_t oC_SPI_LLD_GetChannelOfModulePin( oC_SPI_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_TurnOffDriver( void );

//==========================================================================================================================================
/**
 * @brief sets SPI interrupt handler
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_SetInterruptHandler( oC_SPI_LLD_InterruptHandler_t InterruptHandler );

//==========================================================================================================================================
/**
 * @brief allow to turn on/off register map
 *
 * The function is for manage power state on the SPI channel. It allow to turn on/off register map. Note, that if register map is turned off
 * then none of operations can be performed on the channel. It is required to turn on the power before any configurations on the channel.
 *
 * @param Channel   one of available channels in the machine
 * @param Power     power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_SetPower( oC_SPI_Channel_t Channel , oC_Power_t Power );
//==========================================================================================================================================
/**
 * @brief read current power state
 *
 * The function is for reading power state of the register map on the selected channel. Channel should be powered on before configuration
 * and any other operations on it. Powering off the channel allow to save power. Each channel should be powered off when the driver is powered
 * off.
 *
 * @param Channel   one of available channels in the machine
 * @param outPower  destination for the power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_ReadPower( oC_SPI_Channel_t Channel , oC_Power_t * outPower );

extern oC_ErrorCode_t oC_SPI_LLD_Configure(
                oC_SPI_Channel_t            Channel ,
                oC_SPI_LLD_Mode_t           Mode ,
                oC_BaudRate_t               BaudRate ,
                oC_BaudRate_t               Tolerance,
                oC_SPI_LLD_ClockPhase_t     ClockPhase,
                oC_SPI_LLD_ClockPolarity_t  ClockPolarity ,
                oC_SPI_LLD_FrameFormat_t    FrameFormat ,
                oC_SPI_LLD_DataSize_t       DataSize
                );

//==========================================================================================================================================
/**
 * @brief restore default state on channel
 *
 * The function is for restoring default state on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_RestoreDefaultStateOnChannel( oC_SPI_Channel_t Channel );

//==========================================================================================================================================
/**
 * @brief configures the given pin with the given SPI pin function
 *
 * The function is for configuration of the SPI pin.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param Pin           Pin to connect the SPI with
 * @param PinFunction   SPI pin function to connect the pin
 *
 * @return  code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_ConfigurePin( oC_SPI_Channel_t Channel , oC_Pin_t Pin , oC_SPI_PinFunction_t PinFunction );

//==========================================================================================================================================
/**
 * @brief unconfigures SPI pin
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SPI_LLD_UnconfigurePin( oC_Pin_t Pin );

extern bool oC_SPI_LLD_IsPinAvailble( oC_SPI_Channel_t Channel , oC_Pin_t Pin , oC_SPI_PinFunction_t PinFunction );

extern bool oC_SPI_LLD_IsRxEmpty( oC_SPI_Channel_t Channel );
extern bool oC_SPI_LLD_IsTxFull ( oC_SPI_Channel_t Channel );

extern oC_ErrorCode_t oC_SPI_LLD_ClearRxFifo( oC_SPI_Channel_t Channel );

extern oC_SPI_LLD_Data_t oC_SPI_LLD_GetReceivedData( oC_SPI_Channel_t Channel );
extern void oC_SPI_LLD_PutDataToTransmit( oC_SPI_Channel_t Channel , oC_SPI_LLD_Data_t Data );
extern bool oC_SPI_LLD_IsBusy( oC_SPI_Channel_t Channel );
extern oC_BaudRate_t oC_SPI_LLD_GetConfiguredBaudRate( oC_SPI_Channel_t Channel );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* _OC_SPI_LLD_H */
#endif
#endif
