/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the FMC driver
 *
 * @file       oc_fmc_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 2016-04-02 - 16:12:03)
 *
 * @copyright  Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup FMC-LLD FMC 
 * @ingroup LowLevelDrivers
 * @brief Handles FMC transmissions
 *
 ******************************************************************************************************************************************/


#ifndef _OC_FMC_LLD_H
#define _OC_FMC_LLD_H

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>
#include <oc_memory.h>
#include <oc_time.h>

#if oC_Channel_IsModuleDefined(FMC) && oC_ModulePinFunctions_IsModuleDefined(FMC) && oC_ModulePin_IsModuleDefined(FMC)

#define oC_FMC_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup FMC-LLD
//! @{
#define MODULE_NAME FMC

//==========================================================================================================================================
/**
 * @enum oC_FMC_LLD_PinFunction_t
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_FMC_Pin_t
 * @brief pin of the FMC peripheral
 *
 * The type stores peripheral pin, where the FMC channel is connected.
 */
//==========================================================================================================================================
oC_ModulePin_DefineType;

//==========================================================================================================================================
/**
 * @brief type of the flexible memory
 *
 * This is the mask to store the type of the memory. If the #oC_FMC_LLD_MemoryType_RAM bit is set, then this is the RAM type memory, if the
 * #oC_FMC_LLD_MemoryType_Flash is set, then it is FLASH type.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_LLD_MemoryType_RAM           = (1<<6) ,                             //!< If this flag is set, it is the RAM memory type
    oC_FMC_LLD_MemoryType_Flash         = (1<<7) ,                             //!< If this flag is set, it is the ROM memory type
    oC_FMC_LLD_MemoryType_SDRAM         = (0x1) | oC_FMC_LLD_MemoryType_RAM ,  //!< SDRAM memory type
    oC_FMC_LLD_MemoryType_PSRAM         = (0x2) | oC_FMC_LLD_MemoryType_RAM ,  //!< PSRAM memory type
    oC_FMC_LLD_MemoryType_NAND_Flash    = (0x3) | oC_FMC_LLD_MemoryType_Flash ,//!< NAND flash memory type
    oC_FMC_LLD_MemoryType_NOR_Flash     = (0x4) | oC_FMC_LLD_MemoryType_Flash ,//!< NOR flash memory type
} oC_FMC_LLD_MemoryType_t;

//==========================================================================================================================================
/**
 * @brief stores the width of the bus in bytes
 *
 * This stores the width of the bus in bytes.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_LLD_DataBusWidth_8Bits   = 1,    //!< 8 bits data bus width
    oC_FMC_LLD_DataBusWidth_16Bits  = 2,    //!< 16 bits data bus width
    oC_FMC_LLD_DataBusWidth_32Bits  = 4,    //!< 32 bits data bus width
    oC_FMC_LLD_DataBusWidth_64Bits  = 8,    //!< 64 bits data bus width
} oC_FMC_LLD_DataBusWidth_t;

//==========================================================================================================================================
/**
 * @brief protection of the memory
 *
 * The type is for storing mask of protection that should be configured for memory addresses. Bits can be joined by the | separator
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_LLD_Protection_Default       = 0      ,  //!< Default protection
    oC_FMC_LLD_Protection_AllowWrite    = (1<<0) ,  //!< Allow write for the addresses
    oC_FMC_LLD_Protection_AllowRead     = (1<<1) ,  //!< Allow for read
    oC_FMC_LLD_Protection_AllowExecute  = (1<<2) ,  //!< Allow for execution
} oC_FMC_LLD_Protection_t;

//==========================================================================================================================================
/**
 * @brief Mask for storing CAS latency
 *
 * Stores possibilities of CAS Latency. The type is a mask, where each bit is reserved for one latency.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_LLD_SDRAM_CasLatency_0 = (1<<0) ,//!< CAS Latency 0 is possible
    oC_FMC_LLD_SDRAM_CasLatency_1 = (1<<1) ,//!< CAS Latency 1 is possible
    oC_FMC_LLD_SDRAM_CasLatency_2 = (1<<2) ,//!< CAS Latency 2 is possible
    oC_FMC_LLD_SDRAM_CasLatency_3 = (1<<3) ,//!< CAS Latency 3 is possible
    oC_FMC_LLD_SDRAM_CasLatency_4 = (1<<4) ,//!< CAS Latency 4 is possible
    oC_FMC_LLD_SDRAM_CasLatency_5 = (1<<5) ,//!< CAS Latency 5 is possible
    oC_FMC_LLD_SDRAM_CasLatency_6 = (1<<6) ,//!< CAS Latency 6 is possible
    oC_FMC_LLD_SDRAM_CasLatency_7 = (1<<7) ,//!< CAS Latency 7 is possible
} oC_FMC_LLD_SDRAM_CasLatency_t;

//==========================================================================================================================================
/**
 * @brief Mask for storing length of the burst
 *
 * The type is for storing possible length of the burst.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_LLD_SDRAM_BurstLength_1              = (1<<0),   //!< Burst length of 1 byte is possible
    oC_FMC_LLD_SDRAM_BurstLength_2              = (1<<1),   //!< Burst length of 2 bytes is possible
    oC_FMC_LLD_SDRAM_BurstLength_4              = (1<<2),   //!< Burst length of 4 bytes is possible
    oC_FMC_LLD_SDRAM_BurstLength_8              = (1<<3),   //!< Burst length of 8 bytes is possible
    oC_FMC_LLD_SDRAM_BurstLength_FullPage       = (1<<4),   //!< Burst length of full page is possible
} oC_FMC_LLD_SDRAM_BurstLength_t;

//==========================================================================================================================================
/**
 * @brief stores usage of pin
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_LLD_PinUsage_NotUsed , //!< Pin is not used
    oC_FMC_LLD_PinUsage_Optional ,//!< Pin is only optional
    oC_FMC_LLD_PinUsage_Required ,//!< Pin is required to use
    oC_FMC_LLD_PinUsage_Used      //!< Pin is used
} oC_FMC_LLD_PinUsage_t;

//==========================================================================================================================================
/**
 * @brief Type with pins for NAND flash
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_NANDFlash_Pins_t;

//==========================================================================================================================================
/**
 * @brief Type with pins for NOR flash
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_NORFlash_Pins_t;

//==========================================================================================================================================
/**
 * @brief Type with pins for the SDRAM
 *
 * The type is for storing pins required by the SDRAM
 */
//==========================================================================================================================================
typedef struct
{
    union
    {
        oC_Pin_t    PinsArray[1];
        struct
        {
            oC_Pin_t        SDCLK;
            oC_Pin_t        SDCKE[2];
            oC_Pin_t        SDNE[2];
            oC_Pin_t        A[13];
            oC_Pin_t        D[32];
            oC_Pin_t        BA[4];
            oC_Pin_t        NRAS;
            oC_Pin_t        NCAS;
            oC_Pin_t        SDNWE;
            oC_Pin_t        NBL[4];
        };
    };
} oC_FMC_LLD_SDRAM_Pins_t;

//==========================================================================================================================================
/**
 * @brief Type with pins for PSRAM
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_PSRAM_Pins_t;

//==========================================================================================================================================
/**
 * @brief stores parameters of the NAND Flash chip
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_NANDFlash_ChipParameters_t;

//==========================================================================================================================================
/**
 * @brief stores parameters of the NOR Flash chip
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_NORFlash_ChipParameters_t;

//==========================================================================================================================================
/**
 * @brief stores informations about SDRAM chip
 *
 * The structure stores informations about the SDRAM chips. If your chip is not defined in the FMC module, you should fill this structure
 * and give it to the #oC_FMC_Configure function.
 *
 * @warning
 * The module handles only chips with banks with the same sizes.
 */
//==========================================================================================================================================
typedef struct
{
    oC_MemorySize_t                 Size;                               //!< Size of the SDRAM (in bytes)
    oC_MemorySize_t                 BankSize;                           //!< Size of each bank in the SDRAM
    uint32_t                        NumberOfBanks;                      //!< Number of banks in the chip
    oC_FMC_LLD_DataBusWidth_t       DataBusWidth;                       //!< Width of the bus
    oC_FMC_LLD_SDRAM_CasLatency_t   CasLatencyMask;                     //!< Mask with possible latency of CAS
    oC_Time_t                       CasLatency;                         //!< CAS Latency
    oC_FMC_LLD_SDRAM_BurstLength_t  BurstLengthMask;                    //!< Mask with possible lengths of burst
    bool                            AutoPrechargePossible;              //!< True if the chip supports auto precharge mode
    bool                            AutoRefreshPossible;                //!< True if the chip supports auto refresh mode (auto-refresh memory during normal mode)
    bool                            SelfPrechargePossible;              //!< True if the chip supports self refresh mode (auto-refresh memory during power-down of the master microcontroller)
    oC_Time_t                       AutoRefreshPeriod;                  //!< Period of the AutoRefresh (if possible)
    oC_Frequency_t                  MaximumClockFrequency;              //!< Maximum clock frequency
    uint8_t                         NumberOfRowAddressBits;             //!< Number of bits of a row address
    uint8_t                         NumberOfColumnAddressBits;          //!< Number of bits of a column address
    oC_Time_t                       ActiveToReadWriteDelay;             //!< Delay between the Active command and a Read/Write command
    oC_Time_t                       PrechargeDelay;                     //!< Delay between the Precharge command and another command
    oC_Time_t                       WriteRecoveryDelay;                 //!< Delay between the Write command and a the Precharge command
    oC_Time_t                       RefreshToActivateDelay;             //!< Minimum delay between the Refresh command and the Activate command as well as the delay between two consecutive Refresh commands
    oC_Time_t                       MinimumSelfRefreshPeriod;           //!< Minimum period between self refresh commands
    oC_Time_t                       ExitSelfRefreshDelay;               //!< Delay from releasing the Self-Refresh command to issuing the Activate command
    uint32_t                        CyclesToDelayAfterLoadMode;         //!< Delay from the Load Mode Register command and an Activate or Refresh command in number of memory clock cycles
    struct
    {
        oC_Time_t                   ReadPipeDelay;              //!< Delay for reading data after CAS latency
        bool                        UseBurstRead;               //!< Enable burst read mode. If it is true, then even single read requests are always managed as burst
    } Advanced;
} oC_FMC_LLD_SDRAM_ChipParameters_t;


//==========================================================================================================================================
/**
 * @brief stores parameters of the PSRAM chip
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_PSRAM_ChipParameters_t;

//==========================================================================================================================================
/**
 * @brief stores informations about the chip
 *
 * The type is for storing informations specific for the selected chip. This is the union, so only one of the memory type fields can be filled
 * at once.
 */
//==========================================================================================================================================
typedef struct
{
    oC_FMC_LLD_MemoryType_t         MemoryType;     //!< Type of the chip memory
    const char *                    Name;           //!< String with name of the chip
    union
    {
        oC_FMC_LLD_SDRAM_ChipParameters_t     SDRAM;
    };
} oC_FMC_LLD_ChipParameters_t;


//==========================================================================================================================================
/**
 * @brief stores configuration for the NAND flash memory
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_NANDFlash_Config_t;

//==========================================================================================================================================
/**
 * @brief stores configuration for the NOR flash memory
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_NORFlash_Config_t;

//==========================================================================================================================================
/**
 * @brief stores configuration for the SDRAM memory
 *
 * The type is for storing configuration of the external SDRAM.
 */
//==========================================================================================================================================
typedef struct
{
    oC_FMC_LLD_DataBusWidth_t   DataBusWidth;       //!< Use it  to force change data bus width (to other than default)
    oC_FMC_LLD_Protection_t     Protection;         //!< Access for the memory configured for the SDRAM
    oC_FMC_LLD_SDRAM_Pins_t     Pins;               //!< Pins where the SDRAM is connected
    oC_Time_t                   Timeout;            //!< Maximum time for configuration
} oC_FMC_LLD_SDRAM_Config_t;

//==========================================================================================================================================
/**
 * @brief stores configuration for the PSRAM memory
 *
 * The type is for storing configuration of the external PSRAM.
 */
//==========================================================================================================================================
typedef struct
{

} oC_FMC_LLD_PSRAM_Config_t;

typedef uint32_t oC_FMC_LLD_Bank_t;

//==========================================================================================================================================
/**
 * @brief stores result of configuration
 *
 * The type is for storing result of memory configuration.
 */
//==========================================================================================================================================
typedef struct
{
    union
    {
        oC_FMC_LLD_PinUsage_t           PinsUsage[1];
        // //////////////////////////////////////////////////////////////////////////
        //                               SDRAM
        // //////////////////////////////////////////////////////////////////////////
        struct
        {
            /*            IMPORTANT!!!!                 */
            /*                                          */
            /*     Pins usage must be at the start      */
            /*     of the structure and order must      */
            /*     be the same like in the SDRAM        */
            /*     pins structure!!!                    */
            oC_FMC_LLD_PinUsage_t       SDCLK;
            oC_FMC_LLD_PinUsage_t       SDCKE[2];
            oC_FMC_LLD_PinUsage_t       SDNE[2];
            oC_FMC_LLD_PinUsage_t       A[13];
            oC_FMC_LLD_PinUsage_t       D[32];
            oC_FMC_LLD_PinUsage_t       BA[4];
            oC_FMC_LLD_PinUsage_t       NRAS;
            oC_FMC_LLD_PinUsage_t       NCAS;
            oC_FMC_LLD_PinUsage_t       SDNWE;
            oC_FMC_LLD_PinUsage_t       NBL[4];
        };
    };
    oC_Frequency_t              ConfiguredFrequency;            //!< Configured frequency of the clock
    uint8_t *                   MemoryStart;                    //!< Start pointer to the memory
    oC_MemorySize_t             MemorySize;                     //!< Size of configured sections
    oC_FMC_LLD_Bank_t           ConfiguredBanks;                //!< Mask with banks configured for the memory
    oC_FMC_LLD_Protection_t     DirectAccessProtection;         //!< Configured protection of memory (rwe)
    oC_FMC_LLD_DataBusWidth_t   DataBusWidth;                   //!< Configured data bus width
} oC_FMC_LLD_Result_t;

//==========================================================================================================================================
/**
 * @brief commands for SDRAM
 *
 * The type stores command that can be send to the SDRAM. It can be useful in the SDRAM initialization procedure. Look at the #oC_FMC_SendSDRAMCommand
 * function for more informations.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_LLD_SDRAM_Command_EnableClock ,     //!< Enables the clock (provides a stable CLK signal)
    oC_FMC_LLD_SDRAM_Command_Inhibit ,         //!< The COMMAND INHIBIT function prevents new commands from being executed by the device, regardless of whether the CLK signal is enabled. The device is effectively deselected. Operations already in progress are not affected.
    oC_FMC_LLD_SDRAM_Command_Nop ,             //!< Simple no operation command
    oC_FMC_LLD_SDRAM_Command_LoadModeRegister ,//!< Configuration command for loading modes registers
    oC_FMC_LLD_SDRAM_Command_Active ,          //!< The command used for activate a row in a bank for a subsequent access
    oC_FMC_LLD_SDRAM_Command_Read ,            //!< Read a burst to an active row
    oC_FMC_LLD_SDRAM_Command_Write ,           //!< Write a burst to an active row
    oC_FMC_LLD_SDRAM_Command_Precharge ,       //!< Deactivate a row in a bank (or in all banks)
    oC_FMC_LLD_SDRAM_Command_BurstTerminate ,  //!< Terminates a burst (either fixed length or continuous page burst)
    oC_FMC_LLD_SDRAM_Command_AutoRefresh ,     //!< Refreshes a SDRAM. It could be called each time, when the refresh is required
    oC_FMC_LLD_SDRAM_Command_SelfRefresh ,     //!< Refreshes a ram, when the system is powered-down (it does not require a CLOCK)
} oC_FMC_LLD_SDRAM_Command_t;

//==========================================================================================================================================
/**
 * @brief stores address of external memory
 */
//==========================================================================================================================================
typedef uint64_t oC_FMC_LLD_Address_t;

//==========================================================================================================================================
/**
 * @brief stores DATA for the SDRAM commands
 */
//==========================================================================================================================================
typedef union
{
    struct
    {
        oC_FMC_LLD_Address_t        RowAddress;
        oC_FMC_LLD_Address_t        BankAddress;
        bool                        Reserved;
    } Active;

    struct
    {
        oC_FMC_LLD_Address_t        ColumnAddress;
        oC_FMC_LLD_Address_t        BankAddress;
        bool                        EnableAutoPrecharge;
    } Read;

    struct
    {
        oC_FMC_LLD_Address_t        ColumnAddress;
        oC_FMC_LLD_Address_t        BankAddress;
        bool                        EnableAutoPrecharge;
    } Write;

    struct
    {
        oC_FMC_LLD_Address_t        Reserved;
        oC_FMC_LLD_Address_t        BankAddress;
        bool                        AllBanks;
    } Precharge;

    struct
    {
        union
        {
            oC_FMC_LLD_Address_t        MRD;
            struct
            {
#ifdef LITTLE_ENDIAN
                oC_FMC_LLD_Address_t    BurstLength:3;
                oC_FMC_LLD_Address_t    BurstType:1;
                oC_FMC_LLD_Address_t    CasLatency:3;
                oC_FMC_LLD_Address_t    OperatingMode:2;
                oC_FMC_LLD_Address_t    WriteBurstMode:1;
                oC_FMC_LLD_Address_t    Reserved:55;
#else
#   error LoadModeRegister is not defined for BIG_ENDIAN
#endif
            };
        };
        oC_FMC_LLD_Address_t        BankAddress;
        bool                        AllBanks;
    } LoadModeRegister;

    struct
    {
        oC_FMC_LLD_Address_t        NumberOfAutoRefresh;
        oC_FMC_LLD_Address_t        Reserved;
        bool                        Reserved2;
    } AutoRefresh;

    struct
    {
        oC_FMC_LLD_Address_t        Address;
        oC_FMC_LLD_Address_t        BankAddress;
        bool                        A10;
    } Common;
} oC_FMC_LLD_SDRAM_CommandData_t;

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup FMC-LLD
//! @{


//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_FMC_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_FMC_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief
 * @param Config
 * @param ChipInfo
 * @param outResult
 * @return
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_FMC_LLD_ConfigureSDRAM(    const oC_FMC_LLD_SDRAM_Config_t *    Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_ConfigureNORFlash( const oC_FMC_LLD_NORFlash_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_ConfigureNANDFlash(const oC_FMC_LLD_NANDFlash_Config_t * Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_ConfigurePSRAM(    const oC_FMC_LLD_PSRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_UnconfigureSDRAM(    const oC_FMC_LLD_SDRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_UnconfigureNORFlash( const oC_FMC_LLD_NORFlash_Config_t *  Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_UnconfigureNANDFlash(const oC_FMC_LLD_NANDFlash_Config_t * Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_UnconfigurePSRAM(    const oC_FMC_LLD_PSRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * outResult );
extern oC_ErrorCode_t oC_FMC_LLD_SendSDRAMCommand( oC_FMC_LLD_Result_t * Result , oC_Time_t * Timeout , oC_FMC_LLD_SDRAM_Command_t Command , const oC_FMC_LLD_SDRAM_CommandData_t * Data );
extern oC_ErrorCode_t oC_FMC_LLD_FinishSDRAMInitialization( const oC_FMC_LLD_SDRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result );
extern oC_ErrorCode_t oC_FMC_LLD_FinishNORFlashInitialization( const oC_FMC_LLD_NORFlash_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result );
extern oC_ErrorCode_t oC_FMC_LLD_FinishNANDFlashInitialization( const oC_FMC_LLD_NANDFlash_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result );
extern oC_ErrorCode_t oC_FMC_LLD_FinishPSRAMInitialization( const oC_FMC_LLD_PSRAM_Config_t *     Config , const oC_FMC_LLD_ChipParameters_t * ChipInfo , oC_FMC_LLD_Result_t * Result );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* _OC_FMC_LLD_H */
#endif
