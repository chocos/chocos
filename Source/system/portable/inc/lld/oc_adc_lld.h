/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the ADC driver
 *
 * @file       oc_adc_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 3 maj 2015 13:21:16) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ADC-LLD Analog to Digital Converter 
 * @ingroup LowLevelDrivers
 * @brief Driver for handling convert from analog to digital signals
 *
 ******************************************************************************************************************************************/


#ifndef INC_LLD_OC_ADC_LLD_H_
#define INC_LLD_OC_ADC_LLD_H_

#include <oc_machine.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup ADC-LLD
//! @{




#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup ADC-LLD
//! @{




#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_LLD_OC_ADC_LLD_H_ */
