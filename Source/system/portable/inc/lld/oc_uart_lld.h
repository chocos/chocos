/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the UART driver
 *
 * @file       oc_gpio_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 26 04 2015 21:57:54)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup UART-LLD UART 
 * @ingroup LowLevelDrivers
 * @brief Handles UART transmissions
 *
 ******************************************************************************************************************************************/


#ifndef UARTTEM_PORTABLE_INC_LLD_OC_UART_LLD_H_
#define UARTTEM_PORTABLE_INC_LLD_OC_UART_LLD_H_

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>

#if oC_Channel_IsModuleDefined(UART) == false
#error UART module channels are not defined
#elif oC_ModulePinFunctions_IsModuleDefined(UART) == false
#error  UART module pin functions are not defined
#elif oC_ModulePin_IsModuleDefined(UART) == false
#error  UART module pins are not defined
#else


/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup UART-LLD
//! @{
#define MODULE_NAME UART

//==========================================================================================================================================
/**
 * @enum oC_UART_Channel_t
 * @brief channel of the UART
 *
 * The type stores channel of the UART
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_UART_LLD_PinFunction_t
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_UART_Pin_t
 * @brief pin of the UART peripheral
 *
 * The type stores peripheral pin, where the UART channel is connected.
 */
//==========================================================================================================================================
oC_ModulePin_DefineType;
//==========================================================================================================================================
/**
 * @brief stores index of channel in the UART
 *
 * The type stores index of the channel in the UART array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_UART_LLD_ChannelIndex_t;

//==========================================================================================================================================
/**
 * @brief Store length of the word
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_LLD_WordLength_5Bits ,  //!< transmission length is 5 bits
    oC_UART_LLD_WordLength_6Bits ,  //!< transmission length is 6 bits
    oC_UART_LLD_WordLength_7Bits ,  //!< transmission length is 7 bits
    oC_UART_LLD_WordLength_8Bits ,  //!< transmission length is 8 bits
    oC_UART_LLD_WordLength_9Bits    //!< transmission length is 9 bits
} oC_UART_LLD_WordLength_t;

//==========================================================================================================================================
/**
 * @brief Parity checks
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_LLD_Parity_None ,//!< none parity checking
    oC_UART_LLD_Parity_Odd , //!< odd parity checking
    oC_UART_LLD_Parity_Even  //!< even parity checking
} oC_UART_LLD_Parity_t;

//==========================================================================================================================================
/**
 * @brief How long should be stop bit
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_LLD_StopBit_0p5Bit ,   //!< stop bit length set to 0.5 bit
    oC_UART_LLD_StopBit_1Bit ,   //!< stop bit length set to 1 bit
    oC_UART_LLD_StopBit_1p5Bits ,//!< stop bit length set to 1.5 bit
    oC_UART_LLD_StopBit_2Bits    //!< stop bit length set to 2 bits
} oC_UART_LLD_StopBit_t;

//==========================================================================================================================================
/**
 * @brief Order of bits in frame
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_LLD_BitOrder_LSBFirst ,//!< low significant bit first
    oC_UART_LLD_BitOrder_MSBFirst  //!< most significant bit first
} oC_UART_LLD_BitOrder_t;

//==========================================================================================================================================
/**
 * @brief If uart should be inverted
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_LLD_Invert_NotInverted ,//!< not inverted signals
    oC_UART_LLD_Invert_Inverted     //!< signals will be inverted
} oC_UART_LLD_Invert_t;

//==========================================================================================================================================
/**
 * @brief interrupt pointer
 *
 * The type is for storing pointer to the interrupt handler function.
 *
 * @param Channel       Channel of the UART, that caused the interrupt
 */
//==========================================================================================================================================
typedef void (*oC_UART_LLD_Interrupt_t)( oC_UART_Channel_t Channel );

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each uart channel
 *
 * The macro is for creating a loop for each channel defined in the machine. As 'index' there is a variable that stores the channel ID.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * // example of loop that restores default states on each timer channel defined in the machine
 * oC_UART_LLD_ForEachChannel(Channel , ChannelIndex)
 * {
 *      // this will be executed for each channel
 *      oC_UART_LLD_RestoreDefaultStateOnChannel( Channel );
 * }
 * ~~~~~~~~~~~~
 *
 */
//==========================================================================================================================================
#define oC_UART_LLD_ForEachChannel( Channel , ChannelIndex )           oC_Machine_ForEachChannel( UART , Channel , ChannelIndex )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup UART-LLD
//! @{


//==========================================================================================================================================
/**
 * @brief checks if the UART channel is correct
 *
 * Checks if the UART channel is correct.
 *
 * @param Channel       Channel of the UART to check
 *
 * @return true if channel is correct
 */
//==========================================================================================================================================
extern bool oC_UART_LLD_IsChannelCorrect( oC_UART_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief check if the channel index is correct
 *
 * Checks if the UART channel index is correct.
 *
 * @param ChannelIndex  Index of the channel in module
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_UART_LLD_IsChannelIndexCorrect( oC_UART_LLD_ChannelIndex_t ChannelIndex );
//==========================================================================================================================================
/**
 * @brief converts channel to channel index
 *
 * The function converts channel to index of the channel in the UART module
 *
 * @param Channel       Channel of the UART
 *
 * @return index of the channel in the UART
 */
//==========================================================================================================================================
extern oC_UART_LLD_ChannelIndex_t oC_UART_LLD_ChannelToChannelIndex( oC_UART_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief converts channel index to channel
 *
 * The function converts index of the channel to channel in the UART module
 *
 * @param Channel       Channel of the UART
 *
 * @return index of the channel in the UART
 */
//==========================================================================================================================================
extern oC_UART_Channel_t oC_UART_LLD_ChannelIndexToChannel( oC_UART_LLD_ChannelIndex_t Channel );
//==========================================================================================================================================
/**
 * @brief returns channel of peripheral pin
 *
 * The function reads channel of peripheral pin. It not checks if the channel is correct, use #oC_UART_LLD_IsChannelCorrect function to check
 * if returned channel is correct.
 *
 * @param ModulePin         special pin that is connected to UART channel
 *
 * @return channel of UART (it can be not correct)
 */
//==========================================================================================================================================
extern oC_UART_Channel_t oC_UART_LLD_GetChannelOfModulePin( oC_UART_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief sets handler for driver interrupts
 *
 * The function configures interrupt handler for the driver. It can be called only once after turning on driver. The LLD protects before reconfiguring
 * it. To change interrupt handler it is required to restart the LLD driver.
 *
 * @param RxNotEmpty    Rx data available interrupt handler
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetDriverInterruptHandlers( oC_UART_LLD_Interrupt_t RxNotEmpty , oC_UART_LLD_Interrupt_t TxNotFull);
//==========================================================================================================================================
/**
 * @brief allow to turn on/off register map
 *
 * The function is for manage power state on the UART channel. It allow to turn on/off register map. Note, that if register map is turned off
 * then none of operations can be performed on the channel. It is required to turn on the power before any configurations on the channel.
 *
 * @param Channel   one of available channels in the machine
 * @param Power     power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetPower( oC_UART_Channel_t Channel , oC_Power_t Power );
//==========================================================================================================================================
/**
 * @brief read current power state
 *
 * The function is for reading power state of the register map on the selected channel. Channel should be powered on before configuration
 * and any other operations on it. Powering off the channel allow to save power. Each channel should be powered off when the driver is powered
 * off.
 *
 * @param Channel   one of available channels in the machine
 * @param outPower  destination for the power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadPower( oC_UART_Channel_t Channel , oC_Power_t * outPower );
//==========================================================================================================================================
/**
 * @brief disables operations on channel
 *
 * The function allow to disable any operations on the channel. The channel must be disabled before any configurations on it.
 *
 * @param Channel   one of available channels in the machine
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_DisableOperations( oC_UART_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief enables operations on channel
 *
 * The function allow to enable operations on UART channel, when it is already configured. Use this when configuration is successfully finished.
 * The channel must be disabled before any configurations on it.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_EnableOperations( oC_UART_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief restore default state on channel
 *
 * The function is for restoring default state on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_RestoreDefaultStateOnChannel( oC_UART_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief sets word length
 *
 * The function is for configuring word length on the selected channel.
 *
 * @warning
 * It is required to call #oC_UART_LLD_SetPower function to turn on power on register map and #oC_UART_LLD_DisableOperations to disable
 * operations on the UART channel before any configuration
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param WordLength    parameter to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetWordLength( oC_UART_Channel_t Channel , oC_UART_LLD_WordLength_t WordLength );
//==========================================================================================================================================
/**
 * @brief reads word length
 *
 * The function is for reading configuration of word length on the selected channel.
 *
 * @param Channel           one of the channels (register map) that is available in the machine for this peripheral.
 * @param outWordLength     destination for the parameter current state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadWordLength( oC_UART_Channel_t Channel , oC_UART_LLD_WordLength_t * outWordLength );
//==========================================================================================================================================
/**
 * @brief sets bit rate
 *
 * The function is for configuring bit rate on the selected channel.
 *
 * @warning
 * It is required to call #oC_UART_LLD_SetPower function to turn on power on register map and #oC_UART_LLD_DisableOperations to disable
 * operations on the UART channel before any configuration
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param BitRate       parameter to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetBitRate( oC_UART_Channel_t Channel , uint32_t BitRate );
//==========================================================================================================================================
/**
 * @brief reads bit rate
 *
 * The function is for reading configuration bit rate on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param outBitRate    destination for the parameter state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadBitRate( oC_UART_Channel_t Channel , uint32_t * outBitRate );
//==========================================================================================================================================
/**
 * @brief sets parity checking
 *
 * The function is for configuring parity checking on the selected channel.
 *
 * @warning
 * It is required to call #oC_UART_LLD_SetPower function to turn on power on register map and #oC_UART_LLD_DisableOperations to disable
 * operations on the UART channel before any configuration
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param Parity        parameter to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetParity( oC_UART_Channel_t Channel , oC_UART_LLD_Parity_t Parity );
//==========================================================================================================================================
/**
 * @brief reads parity checking state
 *
 * The function is for reading configuration of parity checking on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param outParity     destination for the parity state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadParity( oC_UART_Channel_t Channel , oC_UART_LLD_Parity_t * outParity );
//==========================================================================================================================================
/**
 * @brief sets stop bit
 *
 * The function is for configuring stop bit on the selected channel.
 *
 * @warning
 * It is required to call #oC_UART_LLD_SetPower function to turn on power on register map and #oC_UART_LLD_DisableOperations to disable
 * operations on the UART channel before any configuration
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param StopBit       parameter to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetStopBit( oC_UART_Channel_t Channel , oC_UART_LLD_StopBit_t StopBit );
//==========================================================================================================================================
/**
 * @brief read stop bit
 *
 * The function is for reading configuration of stop bit on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param outStopBit    destination for the stop bit
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadStopBit( oC_UART_Channel_t Channel , oC_UART_LLD_StopBit_t * outStopBit );
//==========================================================================================================================================
/**
 * @brief sets bit order
 *
 * The function is for configuring bit order on the selected channel.
 *
 * @warning
 * It is required to call #oC_UART_LLD_SetPower function to turn on power on register map and #oC_UART_LLD_DisableOperations to disable
 * operations on the UART channel before any configuration
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param BitOrder      parameter to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetBitOrder( oC_UART_Channel_t Channel , oC_UART_LLD_BitOrder_t BitOrder );
//==========================================================================================================================================
/**
 * @brief reads bit order
 *
 * The function is for reading configuration of bit order on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param outBitOrder   destination for the parameter state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadBitOrder( oC_UART_Channel_t Channel , oC_UART_LLD_BitOrder_t * outBitOrder );
//==========================================================================================================================================
/**
 * @brief sets invert
 *
 * The function is for configuring inverting signal on the selected channel.
 *
 * @warning
 * It is required to call #oC_UART_LLD_SetPower function to turn on power on register map and #oC_UART_LLD_DisableOperations to disable
 * operations on the UART channel before any configuration
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param Invert        parameter to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetInvert( oC_UART_Channel_t Channel , oC_UART_LLD_Invert_t Invert );
//==========================================================================================================================================
/**
 * @brief reads invert
 *
 * The function is for reading configuration of inverting signal on the selected channel.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param outInvert     destination for the parameter state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadInvert( oC_UART_Channel_t Channel , oC_UART_LLD_Invert_t * outInvert );
//==========================================================================================================================================
/**
 * @brief sets loop-back mode
 *
 * The function is for setting a special loop-back mode, where the input pin is internal connected to the output. In this mode any message that
 * will be send to the output should be received on input. It can be useful for diagnostic.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param Loopback      set it to true to turn on mode or to false to turn it off
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetLoopback( oC_UART_Channel_t Channel , bool Loopback );
//==========================================================================================================================================
/**
 * @brief sets loop-back mode
 *
 * The function is for reading a special loop-back mode, where the input pin is internal connected to the output. In this mode any message that
 * will be send to the output should be received on input. It can be useful for diagnostic.
 *
 * @param Channel       one of the channels (register map) that is available in the machine for this peripheral.
 * @param outLoopback   destination to state of loop-back mode
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadLoopback( oC_UART_Channel_t Channel , bool * outLoopback );

//==========================================================================================================================================
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ConnectModulePin( oC_UART_Pin_t ModulePin );

//==========================================================================================================================================
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_DisconnectModulePin( oC_UART_Pin_t ModulePin );

//==========================================================================================================================================
//==========================================================================================================================================

extern oC_ErrorCode_t oC_UART_LLD_SetChannelUsed( oC_UART_Channel_t Channel );

//==========================================================================================================================================
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_SetChannelUnused( oC_UART_Channel_t Channel );

//==========================================================================================================================================
//==========================================================================================================================================
extern bool oC_UART_LLD_IsChannelUsed( oC_UART_Channel_t Channel );

//==========================================================================================================================================
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadModulePinsOfPin( oC_Pin_t Pin , oC_UART_Pin_t * outModulePinsArray , uint32_t * ArraySize , oC_UART_PinFunction_t PinFunction );

//==========================================================================================================================================
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ClearRxFifo( oC_UART_Channel_t Channel );

//==========================================================================================================================================
//==========================================================================================================================================
extern bool oC_UART_LLD_IsTxFifoFull( oC_UART_Channel_t Channel );

//==========================================================================================================================================
//==========================================================================================================================================
extern bool oC_UART_LLD_IsRxFifoEmpty( oC_UART_Channel_t Channel );

//==========================================================================================================================================
//==========================================================================================================================================
extern void oC_UART_LLD_PutData( oC_UART_Channel_t Channel , char Data );

//==========================================================================================================================================
//==========================================================================================================================================
extern char oC_UART_LLD_GetData( oC_UART_Channel_t Channel );

//==========================================================================================================================================
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_WriteWithDma( oC_UART_Channel_t Channel , const char * Buffer , oC_UInt_t Size );

//==========================================================================================================================================
//==========================================================================================================================================
extern oC_ErrorCode_t oC_UART_LLD_ReadWithDma( oC_UART_Channel_t Channel , char * outBuffer , oC_UInt_t Size );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* UART module defined */
#endif /* UARTTEM_PORTABLE_INC_LLD_OC_UART_LLD_H_ */
