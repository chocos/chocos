/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the GPIO driver
 *
 * @file       oc_gpio_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 26 04 2015 21:57:54)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup GPIO-LLD General Purpose Input-Output 
 * @ingroup LowLevelDrivers
 * @brief Input/Output basic operations managing
 *
 * @par
 * The driver is for managing and configuring all operations that handles basic operations that are based on the ports/pins of the machine.
 * It supports configuration, handle pins usage (it stores informations about each pin that was used in the system), provide an interface
 * to get printable pin and more.
 *
 * @par
 * All operations are based on basic definitions of the port: #oC_Port_t and #oC_Pins_t. The first type stores information about the port
 * from which the pin come. The second type is for storing pin or more pins from the same port. To get port from the pins variable use
 * #oC_GPIO_LLD_GetPortOfPins function. Note, that you can assign more pins from one port in one variable by using 'OR' operations, such in
 * the example below:
 *
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * ~~~~~~~~~~~~
 * @par
 * It provide also an interface for verification if the channel, port or pin is correct. You can use the following functions for it:
 * - @link oC_GPIO_LLD_IsChannelCorrect @endlink
 * - @link oC_GPIO_LLD_IsChannelIndexCorrect @endlink
 * - @link oC_GPIO_LLD_IsPinDefined @endlink
 * - @link oC_GPIO_LLD_ArePinsDefined @endlink
 * - @link oC_GPIO_LLD_ArePinsCorrect @endlink
 * - @link oC_GPIO_LLD_IsSinglePin @endlink
 *
 * @par
 * The driver also provides an interface for reading names of port or pins. There are two functions for it: #oC_GPIO_LLD_GetPortName, #oC_GPIO_LLD_GetPinName.
 * This feature can be useful for example in case, when the configuration of GPIO should be printed to the STDOUT.
 *
 * @par
 * There are also functions, that helps to configure the driver to work, such as #oC_GPIO_LLD_SetDriverInterruptHandler. The function helps
 * to configure an interrupt handler, that is common for all driver events. Each time, when there will be some interrupt on some port,
 * this function will be called with source of the interrupt passed in function arguments.
 *
 * @par
 * Main purpose of this module is configuration of pins. There are few functions that helps to achieve it, but there must be used properly.
 * Most of operations that changes port configurations require additional operations before any changes, such as turning on power, disable
 * pin works, etc. Because of that, there are additional functions: #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration that
 * should be called before and after any changes. It is required to call it only once before and after configuration. After it is possible
 * to configure more parameters without second call of #oC_GPIO_LLD_BeginConfiguration function for the same port until the #oC_GPIO_LLD_FinishConfiguration
 * is called.
 *
 * @note
 * Not all functions require calls of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration. To get to know if some function
 * need it, check its documentation.
 *
 * @par
 * Generally most of functions that sets some configuration parameter require to preparation and finishing and read functions does not.
 *
 * @par
 * The driver provide an interface for marking pins as used or not. In the LLD layer this is only an additional feature, and it is protection
 * against reconfiguration, because none of the driver functions checks it. The driver, that use this module should also use an interface for
 * marking the pin as used for protection. To get more information look at function: #oC_GPIO_LLD_SetPinUsed, #oC_GPIO_LLD_SetPinUnused and
 * #oC_GPIO_LLD_CheckIsPinUsed.
 *
 * @par
 * If you want to set a state on pin(s), you have few functions that makes it more comfortable. The #oC_GPIO_LLD_WriteData function is for
 * situation, when you are using more pins in a port, and you need to set different states for each one. For example:
 *
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_AllInPORTA;
 *
 * oC_GPIO_LLD_WriteData( pins , 0xAA ); // now pins on this port are set to 0xAA value
 * oC_GPIO_LLD_WriteData( pins , 0x0C ); // now pins on this port are set to 0x0C value
 * oC_GPIO_LLD_SetPinsState( pins , oC_GPIO_LLD_PinsState_AllHigh ); // pins on this port are set to 0xFF
 * oC_GPIO_LLD_SetPinsState( pins , oC_GPIO_LLD_PinsState_AllLow ); // pins on this port are set to 0x00
 *
 * oC_GPIO_LLD_SetPinsState( oC_Pins_PA2 , oC_GPIO_LLD_PinsState_AllHigh ); // only PA2 from the PORTA is set to high
 * oC_GPIO_LLD_TogglePinsState( pins ); // only PA2 from the PORTA is set to low, the other pins in this port are set to high
 *
 * ~~~~~~~~~~~~
 *
 * @par
 * In the same way there is a possible to read value from pins in the given port by using #oC_GPIO_LLD_ReadData function. Note, that these
 * functions reads and writes only value for pins that are set in the Pins argument, and that it also checks if arguments that are given
 * to the function are correct. It is safe way to manage pins data, but it is recommended for time critical operations to use #oC_GPIO_LLD_SetPinsState,
 * #oC_GPIO_LLD_GetLowStatePins and #oC_GPIO_LLD_GetHighStatePins functions, which should be little faster, and can be more comfortable in
 * few cases. For example, when you expect, that all pins that are defined in the variable are set to high state, you can use #oC_GPIO_LLD_IsPinsState
 * function, such in an example below:
 *
 * ~~~~~~~~~~~~{.c}
 * if ( oC_GPIO_LLD_IsPinsState( oC_Pins_PA2 , oC_GPIO_LLD_PinsState_AllHigh ) )
 * {
 *      // PA2 is set to high state
 * }
 * else
 * {
 *      // PA2 is set to low state
 * }
 * ~~~~~~~~~~~~
 *
 * @warning
 * The function #oC_GPIO_LLD_IsPinsState is fast, but it not checks if the given argument is correct. It should be used only when it is certain
 * that pins are correct.
 *
 ******************************************************************************************************************************************/


#ifndef GPIOTEM_PORTABLE_INC_LLD_OC_GPIO_LLD_H_
#define GPIOTEM_PORTABLE_INC_LLD_OC_GPIO_LLD_H_

#include <oc_machine.h>
#include <oc_errors.h>
#include <stdbool.h>

#if oC_Channel_IsModuleDefined(GPIO) == false && !defined(DOXYGEN)
#error GPIO module is not defined
#else

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup GPIO-LLD
//! @{
#define MODULE_NAME GPIO

//==========================================================================================================================================
/**
 * @enum oC_GPIO_LLD_Channel_t
 * @brief port of the pin
 *
 * The type stores port for the pin
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @brief unlocking special pins
 *
 * The type is for protection for configure special pins, like JTAG and NMI pins
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_Protection_DontUnlockProtectedPins ,    /**< Do not configure special pins */
    oC_GPIO_LLD_Protection_UnlockProtectedPins          /**< Configure special pins */
} oC_GPIO_LLD_Protection_t;

//==========================================================================================================================================
/**
 * @brief speed of pins
 *
 * The type represents maximum speed, that GPIO pins can be switched. When you do not need to very often switch you pin, you can use #oC_GPIO_Speed_Minimum,
 * and thanks to that, you can save some current.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_Speed_Default ,             /**< Default value for oC_GPIO_Speed_t */
    oC_GPIO_LLD_Speed_Minimum ,             /**< Pin works with minimum speed */
    oC_GPIO_LLD_Speed_Medium ,              /**< Pin works with medium speed */
    oC_GPIO_LLD_Speed_Maximum ,             /**< Pin works with maximum speed */
    oC_GPIO_LLD_Speed_NumberOfElements      /**< Number of elements in oC_GPIO_Speed_t enum */
} oC_GPIO_LLD_Speed_t;

//==========================================================================================================================================
/**
 * @brief output current
 *
 * Type represents maximum output current for GPIO pin.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_Current_Default ,        /**< Pin works with default current (not changed) */
    oC_GPIO_LLD_Current_Minimum ,        /**< Pin works with minimum current */
    oC_GPIO_LLD_Current_Medium ,         /**< Pin works with medium current */
    oC_GPIO_LLD_Current_Maximum          /**< Pin works with maximum current */
} oC_GPIO_LLD_Current_t;

//==========================================================================================================================================
/**
 * @brief select In/Out mode
 *
 * Type represents mode of works for GPIO pin.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_Mode_Default ,       /**< Pin works in default mode (not changed) */
    oC_GPIO_LLD_Mode_Input ,         /**< Pin works as input */
    oC_GPIO_LLD_Mode_Output ,        /**< Pin works as output */
    oC_GPIO_LLD_Mode_Alternate       /**< Pin works in alternative mode */
} oC_GPIO_LLD_Mode_t;

//==========================================================================================================================================
/**
 * @brief pull-up/pull-down in input mode
 *
 * Type represents GPIO pull mode, when the pin work as input.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_Pull_Default ,       /**< Pin works with default pull mode (not changed) */
    oC_GPIO_LLD_Pull_Up ,            /**< Pull-up */
    oC_GPIO_LLD_Pull_Down            /**< Pull-down */
} oC_GPIO_LLD_Pull_t;

//==========================================================================================================================================
/**
 * @brief output circuit - open drain/push pull
 *
 * Type represents GPIO output circuit mode ( Open Drain / PushPull )
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_OutputCircuit_Default ,
    oC_GPIO_LLD_OutputCircuit_OpenDrain ,   /**< open drain mode */
    oC_GPIO_LLD_OutputCircuit_PushPull      /**< push-pull mode */
} oC_GPIO_LLD_OutputCircuit_t;


//==========================================================================================================================================
/**
 * @brief   interrupt trigger source
 *
 * Type represents GPIO interrupt trigger source.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_IntTrigger_Default ,                        /**< Pin works in default interrupt mode (not changed) */
    oC_GPIO_LLD_IntTrigger_Off          = 0 ,               /**< Interrupts on this pin are turned off */
    oC_GPIO_LLD_IntTrigger_RisingEdge   = (1<<0),           /**< Interrupt on rising edge */
    oC_GPIO_LLD_IntTrigger_FallingEdge  = (1<<1),           /**< Interrupt on falling edge */
    oC_GPIO_LLD_IntTrigger_BothEdges    = (1<<0) | (1<<1),  /**< Interrupt on both edges */
    oC_GPIO_LLD_IntTrigger_HighLevel    = (1<<2),           /**< Interrupt on high level */
    oC_GPIO_LLD_IntTrigger_LowLevel     = (1<<3),           /**< Interrupt on low level */
    oC_GPIO_LLD_IntTrigger_BothLevels   = (1<<2) | (1<<3)   /**< Interrupt on both levels */
} oC_GPIO_LLD_IntTrigger_t;

//==========================================================================================================================================
/**
 * @brief type for storing state of pins
 *
 * The type is for storing state of pins. This type is similar to the #oC_PinsMask_t type, and every bit in the variable is reserved for some
 * pins.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_LLD_PinsState_AllLow  = 0,                      //!< all pins are set to low state
    oC_GPIO_LLD_PinsState_AllHigh = oC_Pin_PinsInPortMask   //!< all pins are set to high state
} oC_GPIO_LLD_PinsState_t;

//==========================================================================================================================================
/**
 * @brief GPIO interrupt pointer
 *
 * This is the type of function to store pointer to the interrupt function.
 *
 * @param Pins                  pin(s) that caused this interrupt
 *
 */
//==========================================================================================================================================
typedef void (*oC_GPIO_LLD_Interrupt_t)( oC_Pins_t Pins );

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup GPIO-LLD
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief macro to create a loop for each port
 *
 * This is special macro that creates a loop for each port that is defined in the machine. It is some kind of **for**. Inside the loop you
 * can use both: Port and PortIndex variables, that are set in each iteration for new values.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * // This loop turns on each port in the machine
 * oC_GPIO_LLD_ForEachPort( Port )
 * {
 *      oC_Pins_t pins = oC_GPIO_LLD_GetPinsFor( Port , oC_PinsMask_All );
 *      oC_GPIO_LLD_SetPower( pins , oC_Power_On );
 * }
 * ~~~~~~~~~~~~
 *
 * @param Port
 *
 */
//==========================================================================================================================================
#define oC_GPIO_LLD_ForEachPort( PortVariable )         oC_Channel_Foreach(GPIO , PortVariable )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns mask of important bits for pins
 *
 * Example of usage:
 * @code{.c}
   oC_Pins_t pins = oC_Pin_PF3 | oC_Pin_PF2;
   oC_Pins_t mask = oC_GPIO_LLD_GetImportantBitsMaskForPins(pins);

   // now mask will have set all bits that are important in the variable set
   @endcode
 */
//==========================================================================================================================================
#define oC_GPIO_LLD_GetImportantBitsMaskForPins( Pins )              ( oC_Pin_ChannelMask | (oC_Pin_PinsInPortMask & Pins ) )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup GPIO-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief checks if the GPIO port is correct
 *
 * The function checks if the port is correct
 *
 * @param Port       Port of the GPIO (port)
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_IsPortCorrect( oC_Port_t Port );
//==========================================================================================================================================
/**
 * @brief checks if the GPIO port index is correct
 *
 * The function checks if the given port index is correct in the GPIO module.
 *
 * @param PortIndex  Index of the channel in the module
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_IsPortIndexCorrect( oC_PortIndex_t PortIndex );
//==========================================================================================================================================
/**
 * @brief returns pins mask of pins
 *
 * The function returns pins mask from the #oC_Pins_t type. The pins mask is a mask with bits of port, that are used in this variable.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 *
 * oC_PinsMask_t pinsMask = oC_GPIO_LLD_GetPinsMaskOfPins(pins); // pinsMask = 0b00001100
 *
 * ~~~~~~~~~~~~
 *
 * @param Pins      Variable with one or more pins
 *
 * @return bit mask
 */
//==========================================================================================================================================
extern oC_PinsMask_t oC_GPIO_LLD_GetPinsMaskOfPins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief returns port of pins
 *
 * The function returns port of pins that are stored in the pins variable.
 *
 * @param Pins      Variable with one or more pins
 *
 * @return port of pins
 */
//==========================================================================================================================================
extern oC_Port_t oC_GPIO_LLD_GetPortOfPins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief converts port to port index
 *
 * The function converts port to index of the port in the GPIO module
 *
 * @param Port       Port of the GPIO (port)
 *
 * @return index of the port in the GPIO
 */
//==========================================================================================================================================
extern oC_PortIndex_t oC_GPIO_LLD_PortToPortIndex( oC_Port_t Port );
//==========================================================================================================================================
/**
 * @brief converts port index to port
 *
 * The function converts index of the port to port in the GPIO module
 *
 * @param Channel       Channel of the GPIO (port)
 *
 * @return index of the port in the GPIO
 */
//==========================================================================================================================================
extern oC_Port_t oC_GPIO_LLD_PortIndexToPort( oC_PortIndex_t PortIndex );
//==========================================================================================================================================
/**
 * @brief checks if the pin is defined (only one pin)
 *
 * The function checks if the given pin is defined in the machine definition list for this processor.
 *
 * @warning this function is correct only for single selection pin link. It return false if the pin link will contain more than one pin.
 *
 * @param Pin           Pin to check if is defined
 *
 * @return true if defined.
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_IsPinDefined( oC_Pin_t Pin );
//==========================================================================================================================================
/**
 * @brief checks if pins are defined
 *
 * The function checks if all pins that are stored in the Pins argument are defined. It is important, that each pin should be from the same
 * port. If any of pins will be from different port, then the function will return, that pins are not defined.
 *
 * @param Pins      variable with pins from the same port
 *
 * @return true if each pin stored in the Pins argument is defined
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_ArePinsDefined( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if pins are correct
 *
 * The function checks if pins are correct, but not checks if each pin in the variable is defined.
 *
 * @param Pins      One or more pins from the same port
 *
 * @return true if pins are correct
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_ArePinsCorrect( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if there is only one pin in pins variable
 *
 * The function checks if in the Pins variable is stored only one pin.
 *
 * @param Pins      Variable with pins from the same port
 *
 * @return true if there is only one pin
 *
 * @code{.c}
   oC_Pins_t pins = oC_Pin_PF1 | oC_Pin_PF2;
   oC_Pin_t  pin  = oC_Pin_PF1;

   // this is not true!!!
   if(oC_GPIO_LLD_IsSinglePin( pins ) )
   {
   }

   // This is TRUE
   if(oC_GPIO_LLD_IsSinglePin(pin))
   {
   }
   @endcode
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_IsSinglePin( oC_Pins_t Pins );

//==========================================================================================================================================
/**
 * @brief connect port and pins to one variable
 *
 * The function is for connecting pins definitions and port to the one, special type #oC_Pins_t that stores this both informations.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Port_t port = oC_Port_PORTA;
 * oC_Pins_Mask_t pinsMask = oC_Pins_Mask_All;
 * oC_Pins_t pins = oC_GPIO_LLD_GetPinsFor( port , pinsMask );
 * ~~~~~~~~~~~~
 *
 * @param Port      Port of the GPIO
 * @param Pins      Mask of pins that should be included to the #oC_Pins_t
 *
 * @return connected port and pins definitions
 */
//==========================================================================================================================================
extern oC_Pins_t oC_GPIO_LLD_GetPinsFor( oC_Port_t Port , oC_PinsMask_t Pins );

//==========================================================================================================================================
/**
 * @brief returns name of the port
 *
 * The function is for reading printable name of the given port. If the port is not correct, then it should return "unknown" string.
 *
 * @param Port      Variable with a port value
 *
 * @return constant string with name of a port
 */
//==========================================================================================================================================
extern const char * oC_GPIO_LLD_GetPortName( oC_Port_t Port );
//==========================================================================================================================================
/**
 * @brief returns name of the pin
 *
 * The function is for reading printable name of the given pin, or "unknown" if the pin is not correct. Note, that it works only for
 * single pin and will not return correct value for more than one pin.
 *
 * @param Pin   Variable with single pin
 *
 * @return constant string with name of a pin
 */
//==========================================================================================================================================
extern const char * oC_GPIO_LLD_GetPinName( oC_Pin_t Pin );
//==========================================================================================================================================
/**
 * @brief sets driver interrupt function handler
 *
 * This function is for configuring the interrupt function handler, that should be called, when there is an interrupt in some of ports. The
 * handler should be set one for all driver. There will be argument in the #oC_GPIO_LLD_Interrupt_t type, that inform what kind of interrupt
 * occurs.
 *
 * @warning The function protects before reconfiguration of this - if the interrupt handler is already set, it will not allow to set it again.
 * In this case the driver must be reset by turning off and turning on again.
 *
 * @param InterruptHandler      Pointer to the function to call, when the interrupt occurs
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetDriverInterruptHandler( oC_GPIO_LLD_Interrupt_t Handler );
//==========================================================================================================================================
/**
 * @brief prepares port to configuration
 *
 * The function is for preparation of the port for configuration operations. It must be called every time before any changes on a port, such
 * as changing mode, speed and so on.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * oC_GPIO_LLD_BeginConfiguration( pins );
 * oC_GPIO_LLD_SetMode(pins , oC_GPIO_LLD_Mode_Output );
 * oC_GPIO_LLD_FinishConfiguration( pins );
 * ~~~~~~~~~~~~
 *
 * @param Pins      Variable with single pin or more pins from the same port
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_BeginConfiguration( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief finishes configuration of port
 *
 * The function is for finishing a configuration of port. It should be called every time after any changes on a port, such as changing mode,
 * speed and so on.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * oC_GPIO_LLD_BeginConfiguration( pins );
 * oC_GPIO_LLD_SetMode(pins , oC_GPIO_LLD_Mode_Output );
 * oC_GPIO_LLD_FinishConfiguration( pins );
 * ~~~~~~~~~~~~
 *
 * @param Pins      Variable with single pin or more pins from the same port
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_FinishConfiguration( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief sets power for a port
 *
 * The function configures power for the port that is given in the Pins argument. This function should power up all registers that are needed
 * to configure GPIO pins.
 *
 * @note this function does not require calling of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and
 * after usage
 *
 * @param Pins      variable with single or more pins from the same port
 * @param Power     power to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetPower( oC_Pins_t Pins , oC_Power_t Power );
//==========================================================================================================================================
/**
 * @brief reads power state in a port
 *
 * The function reads power state in a port.
 *
 * @note this function does not require calling of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and
 * after usage
 *
 * @param Pins      variable with single or more pins from the same port
 * @param outPower  destination for the power state variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadPower( oC_Pins_t Pins , oC_Power_t * outPower );
//==========================================================================================================================================
/**
 * @brief checks if the pin(s) is/are protected
 *
 * The function checks if pins that are given in the Pins argument are protected. This means, that there are special pins, that must be unlocked
 * before usage. Look at #oC_GPIO_LLD_UnlockProtection function.
 *
 * @param Pins      variable with single or more pins from the same port
 *
 * @return true if pin is protected or if the given pin is not correct
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_IsPinProtected( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief unlocking protection from the pin(s)
 *
 * The function is for unlocking protection of special pins. To ensure, that it is not called by accident it is necessary to set the Protection
 * argument to #oC_GPIO_LLD_Protection_UnlockProtectedPins value. Otherwise it will not unlock protection and returns an error.
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins          variable with single or more pins from the same port
 * @param Protection    special value to ensure protection. It should be set to the #oC_GPIO_LLD_Protection_UnlockProtectedPins value
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_UnlockProtection( oC_Pins_t Pins , oC_GPIO_LLD_Protection_t Protection );
//==========================================================================================================================================
/**
 * @brief locking protection for pin(s)
 *
 * The function is for locking special pins. All special pins should be locked by default after system starts. To unlock pin use #oC_GPIO_LLD_UnlockProtection
 * function.
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins          variable with single or more pins from the same port
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_LockProtection( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if special pin is unlocked
 *
 * The function is for checking if special pin is already unlocked. To unlock pin use #oC_GPIO_LLD_UnlockProtection function.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outPinUnlocked    destination for the flag if pin is unlocked
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_CheckIsPinUnlocked( oC_Pins_t Pins , bool * outPinUnlocked );
//==========================================================================================================================================
/**
 * @brief configures speed of pin(s)
 *
 * The function is for configuration of pin(s) speed. It is parameter, that defines maximum speed of pin state changing. In most of cases,
 * more speed choice means more current consumption.
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Speed             speed to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetSpeed( oC_Pins_t Pins , oC_GPIO_LLD_Speed_t Speed );
//==========================================================================================================================================
/**
 * @brief reads configured speed
 *
 * The function is for reading current configuration of speed that is set on the port. It is parameter, that defines maximum speed of pin
 * state changing. In most of cases, more speed choice means more current consumption.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outSpeed          destination for the speed variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadSpeed( oC_Pins_t Pins , oC_GPIO_LLD_Speed_t * outSpeed );
//==========================================================================================================================================
/**
 * @brief configures current of pin(s)
 *
 * The function is for configuration of the current on selected port and pin(s). This parameter is for output mode, and it defines strong
 * of the current source. If it is set to #oC_GPIO_LLD_Current_Minimum, it will be in low energy mode.
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Current           current limit to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetCurrent( oC_Pins_t Pins , oC_GPIO_LLD_Current_t Current );
//==========================================================================================================================================
/**
 * @brief reads configured current
 *
 * The function is for reading configuration of current that is set on the port. This parameter is for output mode, and it defines strong
 * of the current source. If it is set to #oC_GPIO_LLD_Current_Minimum, it will be in low energy mode.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outCurrent        destination for current variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadCurrent( oC_Pins_t Pins , oC_GPIO_LLD_Current_t * outCurrent );
//==========================================================================================================================================
/**
 * @brief configures mode of pin(s)
 *
 * The function is for configuration of pins mode. This parameter helps to choose if it is output or input mode.
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Mode              mode to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetMode( oC_Pins_t Pins , oC_GPIO_LLD_Mode_t Mode );
//==========================================================================================================================================
/**
 * @brief reads mode configuration
 *
 * The function is for reading pin mode. This parameter helps to choose if it is output or input mode.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outMode           destination for the mode variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadMode( oC_Pins_t Pins , oC_GPIO_LLD_Mode_t * outMode );
//==========================================================================================================================================
/**
 * @brief configures pull for pin(s)
 *
 * The function is for configuration of pull for pins. This parameter is for input mode, and defines default state for pin(s).
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Pull              pull to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetPull( oC_Pins_t Pins , oC_GPIO_LLD_Pull_t Pull );
//==========================================================================================================================================
/**
 * @brief reads pull configuration
 *
 * The function is for reading pull configuration. This parameter is for input mode, and defines default state for pin(s).
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outPull           destination for the pull variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadPull( oC_Pins_t Pins , oC_GPIO_LLD_Pull_t * outPull );
//==========================================================================================================================================
/**
 * @brief configures output circuit
 *
 * The function if for configuration of output circuit for pin(s). This parameter is for output mode, and helps to select between open drain
 * and push-pull mode.
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param OutputCircuit     value to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetOutputCircuit( oC_Pins_t Pins , oC_GPIO_LLD_OutputCircuit_t OutputCircuit );
//==========================================================================================================================================
/**
 * @brief reads output circuit configuration
 *
 * The function is for reading configuration of the output circuit parameter. This parameter is for output mode, and helps to select between
 * open drain and push-pull mode.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outOutputCircuit  destination for the returned value
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadOutputCircuit( oC_Pins_t Pins , oC_GPIO_LLD_OutputCircuit_t * outOutputCircuit );
//==========================================================================================================================================
/**
 * @brief configures interrupt for pin(s)
 *
 * The function is for configuration of the interrupt for pins in GPIO input mode. The parameter defines trigger source for interrupts in
 * input mode. The trigger can be also set to #oC_GPIO_LLD_IntTrigger_Off, and in this case it will disable any interrupts on this pin(s).
 *
 * @note this function require usage of #oC_GPIO_LLD_BeginConfiguration and #oC_GPIO_LLD_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param InterruptTrigger  value to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetInterruptTrigger( oC_Pins_t Pins , oC_GPIO_LLD_IntTrigger_t InterruptTrigger );
//==========================================================================================================================================
/**
 * @brief reads interrupt trigger
 *
 * The function is for reading configuration of interrupt trigger. The parameter defines trigger source for interrupts in input mode. The
 * trigger can be also set to #oC_GPIO_LLD_IntTrigger_Off, and in this case it will disable any interrupts on this pin(s).
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outInterruptTrigger   destination for the returned value
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadInterruptTrigger( oC_Pins_t Pins , oC_GPIO_LLD_IntTrigger_t * outInterruptTrigger );
//==========================================================================================================================================
/**
 * @brief marks pin as used
 *
 * The function is for marking that the pin is already in use. It is for protection before reconfiguration in the future. Note, that none
 * of **GPIO-LLD** functions checks if pin is used during configuration - on this layer it is only a feature. The main driver should provide
 * an interface to protect pins when it is necessary. To check if pin is used see #oC_GPIO_LLD_CheckIsPinUsed.
 *
 * @param Pins                  variable with single or more pins from the same port
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetPinsUsed( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief marks pin as unused
 *
 * The function is for marking that the pin is already not in use. It is for protection before reconfiguration in the future. Note, that none
 * of **GPIO-LLD** functions checks if pin is used during configuration - on this layer it is only a feature. The main driver should provide
 * an interface to protect pins when it is necessary. To check if pin is used see #oC_GPIO_LLD_CheckIsPinUsed.
 *
 * @param Pins                  variable with single or more pins from the same port
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_SetPinsUnused( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if the pin is used
 *
 * The function is for checking if the pin is in use. It is for protection before reconfiguration in the future. Note,
 * that none of **GPIO-LLD** functions checks if pin is used during configuration - on this layer it is only a feature. The main driver
 * should provide an interface to protect pins when it is necessary. To set pin as used see #oC_GPIO_LLD_SetPinUsed function.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outPinUsed            destination for the output variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_CheckIsPinUsed( oC_Pins_t Pins , bool * outPinUsed );
//==========================================================================================================================================
/**
 * @brief checks if all of pins are not used
 *
 * The function is for checking if the pin (or pins) are not in use. It is for protection before reconfiguration in the future. Note,
 * that none of **GPIO-LLD** functions checks if pin is used during configuration - on this layer it is only a feature. The main driver
 * should provide an interface to protect pins when it is necessary. To set pin as used see #oC_GPIO_LLD_SetPinUsed function.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outPinsUnused         destination for the output variable
 *
 * @see oC_GPIO_LLD_CheckIsPinUsed
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ArePinsUnused( oC_Pins_t Pins , bool * outPinsUnused );
//==========================================================================================================================================
/**
 * @brief write data in port
 *
 * The function is for writing data on the selected pins. This function checks if arguments are correct, and it is useful for example, when
 * all port is configured and used, and there is a need to write data on it, because it sets both - high and low states on each pin from the
 * Pins argument. Note, that if you need to set all pins to the same state, the #oC_GPIO_LLD_SetPinsState function will be more useful and
 * faster.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param Data                  Data to write on port
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_WriteData( oC_Pins_t Pins , oC_PinsMask_t Data );
//==========================================================================================================================================
/**
 * @brief reads data from port
 *
 * The function is for reading data from the selected pins. This function checks if arguments are correct, and it is useful for example, when
 * all port is configured and used, and there is a need to read data from it (not pins state). Note, that there are also functions #oC_GPIO_LLD_GetHighStatePins,
 * #oC_GPIO_LLD_GetLowStatePins and #oC_GPIO_LLD_IsPinsState that should be little faster.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outData               Destination for the return data
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_LLD_ReadData( oC_Pins_t Pins , oC_PinsMask_t * outData );
//==========================================================================================================================================
/**
 * @brief reads reference to the GPIO data
 *
 * This function allows to read the pointer to the DATA of GPIO. It can be useful for speeding up operations based on the writing or reading
 * state of a GPIO port.
 *
 * Example of usage:
 * @code{.c}
 * // (with assumption, that it was correctly configured before)
 *
 * oC_PinsMask_t * keysPortData = NULL;
 * oC_PinsMask_t * ledsPortData = NULL;
 *
 * oC_GPIO_LLD_ReadDataReference( ledPins  , &ledsPortData );
 * oC_GPIO_LLD_ReadDataReference( keysPins , &keysPortData );
 *
 * while(1)
 * {
 *      // If at least one of keys is pressed
 *      if(*keysPortData)
 *      {
 *          // Turn on LEDs
 *          *ledsPortData = 0xff;
 *      }
 *      else
 *      {
 *          // Turn off LEDs if none of keys is turned on
 *          *ledsPortData = 0;
 *      }
 * }
 *
 * @endcode
 *
 * @param Pins
 * @param outDataReference
 * @return
 */
//==========================================================================================================================================
// TODO: Change to Input / Output
extern oC_ErrorCode_t oC_GPIO_LLD_ReadDataReference( oC_Pins_t Pins , oC_UInt_t ** outDataReference );
//==========================================================================================================================================
/**
 * @brief returns pins that are set to high state
 *
 * The function is for reading state of pins without arguments checking. It is little faster then the #oC_GPIO_LLD_ReadData function, and
 * can be more comfortable in use, but there is a risk, that if the Pins argument is not set correctly, then it can cause the hard fault on
 * the machine. This function should be used only when the pins argument is correct.
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_LLD_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * if ( oC_GPIO_LLD_GetHighStatePins( pins ) == oC_Pins_PA2 )
 * {
 *      // only the oC_Pins_PA2 pin is set to high!
 * }
 * ~~~~~~~~~~~~
 * @warning
 * This function returns list of pins, that are set to high state. If you want to simply check if all pins that are defined in the Pins argument
 * are set to the given state - low or high, use #oC_GPIO_LLD_IsPinsState function.
 *
 * @param Pins                  variable with single or more pins from the same port
 *
 * @return pins that set from the Pins argument
 */
//==========================================================================================================================================
extern oC_Pins_t oC_GPIO_LLD_GetHighStatePins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief returns pins that are set to low state
 *
 * The function is for reading state of pins without arguments checking. It is little faster then the #oC_GPIO_LLD_ReadData function, and
 * can be more comfortable in use, but there is a risk, that if the Pins argument is not set correctly, then it can cause the hard fault on
 * the machine. This function should be used only when the pins argument is correct.
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_LLD_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * if ( oC_GPIO_LLD_GetLowStatePins( pins ) == oC_Pins_PA2 )
 * {
 *      // only the oC_Pins_PA2 pin is set to low!
 * }
 * ~~~~~~~~~~~~
 * @warning
 * This function returns list of pins, that are set to low state. If you want to simply check if all pins that are defined in the Pins argument
 * are set to the given state - low or high, use #oC_GPIO_LLD_IsPinsState function.
 *
 *
 * @param Pins                  variable with single or more pins from the same port
 *
 * @return pins that set from the Pins argument
 */
//==========================================================================================================================================
extern oC_Pins_t oC_GPIO_LLD_GetLowStatePins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if all pins are set to state
 *
 * The function checks if all pins that are set in the Pins argument are set to the selected state.
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_LLD_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * if ( oC_GPIO_LLD_IsPinsState( pins , oC_GPIO_LLD_PinsState_AllHigh ) )
 * {
 *      // oC_Pins_PA2 and oC_Pins_PA3 pins are set to high state
 * }
 * else if ( oC_GPIO_LLD_IsPinsState( pins , oC_GPIO_LLD_PinsState_AllLow ) )
 * {
 *      // oC_Pins_PA2 and oC_Pins_PA3 pins are set to low state
 * }
 * else
 * {
 *      // pins are not in the same state
 * }
 * ~~~~~~~~~~~~
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param ExpectedPinsState     expected state of pins
 *
 * @return true if all pins are in the expected state
 */
//==========================================================================================================================================
extern bool oC_GPIO_LLD_IsPinsState( oC_Pins_t Pins , oC_GPIO_LLD_PinsState_t ExpectedPinsState );

//==========================================================================================================================================
/**
 * @brief getter for pins state
 *
 * @param Pins                  variable with single or more pins from the same port
 *
 * @return state of the pins with bits set for pins that are in high state
 */
//==========================================================================================================================================
extern oC_GPIO_LLD_PinsState_t oC_GPIO_LLD_GetPinsState( oC_Pins_t Pins );

//==========================================================================================================================================
/**
 * @brief sets pins to the selected state
 *
 * The function is for setting a state for all pins that are defined in the Pins argument.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 *
 * oC_GPIO_LLD_SetPinsState( pins , oC_GPIO_LLD_PinsState_AllHigh );
 * ~~~~~~~~~~~~
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_LLD_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param PinsState             state of pins to set
 */
//==========================================================================================================================================
extern void oC_GPIO_LLD_SetPinsState( oC_Pins_t Pins , oC_GPIO_LLD_PinsState_t PinsState );
//==========================================================================================================================================
/**
 * @brief toggles pins state
 *
 * The function toggles each pins state to opposite. This function is for make basic operation faster, so it not checks arguments.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 *
 * oC_GPIO_LLD_SetPinsState( pins , oC_GPIO_LLD_PinsState_AllHigh );
 * // pins oC_Pins_PA2 and oC_Pins_PA3 are now in high state
 * oC_GPIO_LLD_TogglePinsState( pins );
 * // pins oC_Pins_PA2 and oC_Pins_PA3 are now in low state
 * oC_GPIO_LLD_SetPinsState( oC_Pins_PA2 , oC_GPIO_LLD_PinsState_AllHigh );
 * // pin oC_Pins_PA2 is in high state, but oC_Pins_PA3 is in low state
 * oC_GPIO_LLD_TogglePinsState( pins );
 * // pin oC_Pins_PA2 is in low state, but oC_Pins_PA3 is in high state
 * ~~~~~~~~~~~~
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_LLD_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * @param Pins                  variable with single or more pins from the same port
 */
//==========================================================================================================================================
extern void oC_GPIO_LLD_TogglePinsState( oC_Pins_t Pins );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* GPIO module is not defined */
#endif /* GPIOTEM_PORTABLE_INC_LLD_OC_GPIO_LLD_H_ */
