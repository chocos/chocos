/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the MEM driver
 *
 * @file       oc_mem_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 27 04 2015 17:07:50)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup MEM-LLD Memory 
 * @ingroup LowLevelDrivers
 * @brief Managing memory operations
 *
 * @par
 * The driver is for supporting operations based on a machine memory. It should be turned on before usage by function #oC_MEM_LLD_TurnOnDriver,
 * and it should be turned off by #oC_MEM_LLD_TurnOffDriver when it is not needed anymore. The driver provide an interface for reading
 * machine limits addresses, memory section sizes, and configuring interrupts that are memory related.
 *
 * @par
 * There are following kind of interrupts:
 * - Memory fault interrupt - is an exception that occurs because of a memory protection related fault, including access violation and no match.
 * - Bus fault interrupt - memory related fault, that occurs while bus transactions.
 *
 * @par
 * The driver allow to set interrupt handlers for these interrupts, but only once. When the handler is set, it is not possible to set it
 * again until restart of the module. To set interrupt handler pointer use #oC_MEM_LLD_SetMemoryFaultInterrupt and #oC_MEM_LLD_SetBusFaultInterrupt
 * functions. When the handlers are set, it is possible to turn on these interrupts using functions #oC_MEM_LLD_TurnOnBusFaultInterrupt and
 * #oC_MEM_LLD_TurnOnMemoryFaultInterrupt. You can always check if interrupts are correctly configured and turned on, using #oC_MEM_LLD_IsBusFaultInterruptTurnedOn
 * and #oC_MEM_LLD_IsMemoryFaultInterruptTurnedOn functions.
 *
 * Example of usage:
 * @code{.c}
 * static void MemoryFaultInterrupt(void)
 * {
 *    // This occurs, when there was some fault in memory protection
 * }
 *
 * void main(void)
 * {
 *     oC_MEM_LLD_TurnOnDriver();
 *     oC_MEM_LLD_SetMemoryFaultInterrupt(MemoryFaultInterrupt);
 *     oC_MEM_LLD_TurnOnMemoryFaultInterrupt();
 * }
 * @endcode
 *
 * @par
 * Using **MEM-LLD** module, you can get physical addresses of machine memory sections, such as flash, ram, used flash, data, stack, etc.
 * You can also read size of these sections. It is not required to turn on the module to use these functions. Sometimes it is needed to keep
 * memory alignment. The function #oC_MEM_LLD_GetAlignmentSize and macro #oC_MEM_LLD_MEMORY_ALIGNMENT returns number of bytes, that the memory
 * is aligned in the machine.
 *
 * @see oC_MEM_LLD_GetFlashStartAddress, oC_MEM_LLD_GetFlashEndAddress, oC_MEM_LLD_GetRamStartAddress, oC_MEM_LLD_GetRamEndAddress, oC_MEM_LLD_GetRamSize, oC_MEM_LLD_GetFlashSize, oC_MEM_LLD_GetHeapStartAddress, oC_MEM_LLD_GetHeapEndAddress, oC_MEM_LLD_GetHeapSize
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_LLD_OC_MEM_LLD_H_
#define SYSTEM_PORTABLE_INC_LLD_OC_MEM_LLD_H_

#include <oc_machine.h>
#include <oc_errors.h>
#include <stdbool.h>
#include <oc_mcs.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________
//! @addtogroup MEM-LLD
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief number of bytes in memory alignment
 *
 * The macro returns number of bytes that the memory is aligned.
 */
//==========================================================================================================================================
#define oC_MEM_LLD_MEMORY_ALIGNMENT             oC_MCS_MEMORY_ALIGNMENT

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief number of bytes in alignment of the stack
 */
//==========================================================================================================================================
#define oC_MEM_LLD_STACK_ALIGNMENT             oC_MCS_STACK_MEMORY_ALIGNMENT

//==========================================================================================================================================
/**
 * @brief stores configuration of the memory region
 *
 * The type is for storing configuration of the memory region.
 */
//==========================================================================================================================================
typedef struct
{
    const void *        BaseAddress;      //!< Base address of the region - this must point to the start of the region. Only architecture supported values are allowed
    oC_MemorySize_t     Size;             //!< Size of the region
    oC_Access_t         PrivilegedAccess; //!< Definition of access for privileged space (portable or core)
    oC_Access_t         UserAccess;       //!< Definition of access for user space
    oC_Power_t          Power;            //!< Power state for the region (enabled or disabled)
    bool                Cachable;         //!< True if the region should be cacheable - If you set a region to be cacheable: When you load from that region, the cache is searched. If the item is found, it is loaded from the cache. If the item is not found, a complete cache line including the required address is loaded. Some other cache line is evicted from the cache, unless there is an unused cache line available. When you save to that region, the cache is searched. If the item is found, the save is made to the cache. If the item is not found, the save is made to memory.
    bool                Bufforable;       //!< True if the region should be bufforable - Bufferable means whether a write to the address can be buffered.  When you do a write to a memory (e.g. a STR instruction) the write gets placed into a buffer in the processor.  The buffer continually drains the writes to the memory system - allowing the core to continue with the next instructions.  Processors often have multiple buffers, for example one between the pipeline and L1 data cache, another between the L1 and L2 caches, another for main memory....
    uint32_t            RegionNumber;     //!< (optional) Number of the region to configure. It is just ID index between 0 and #oC_MCS_GetMaximumNumberOfRegions
    bool                FindFreeRegion;   //!< If true, the #RegionNumber field is ignored and filled with the new value after configuration
} oC_MEM_LLD_MemoryRegionConfig_t;

//==========================================================================================================================================
/**
 * @brief stores access memory mode
 *
 * The type is for storing memory access more (if it is privileged or not)
 */
//==========================================================================================================================================
typedef enum
{
    oC_MEM_LLD_MemoryAccessMode_User       = oC_MCS_MemoryAccessMode_User ,    //!< Memory controller is configured to allow for memory access only for regions that can be accessible by a user
    oC_MEM_LLD_MemoryAccessMode_Privileged = oC_MCS_MemoryAccessMode_Privileged//!< Memory controller is configured to allow for memory access for regions that can be accessible by a privileged tasks
} oC_MEM_LLD_MemoryAccessMode_t;

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types for MEM-LLD driver
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @addtogroup MEM-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief type for storing size of memory
 */
//==========================================================================================================================================
typedef oC_UInt_t oC_MEM_LLD_Size_t;

//==========================================================================================================================================
/**
 * @brief type for storing memory interrupt pointers
 *
 * The type stores pointers for interrupt function handlers.
 *
 */
//==========================================================================================================================================
typedef void (*oC_MEM_LLD_Interrupt_t)(void);

#undef  _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface function prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup MEM-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on.
 *
 * @return code of error, or `oC_ErrorCode_None` if there is no error. Moreover it return `oC_ErrorCode_ModuleIsTurnedOn` when the module
 * was turned on before.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return code of error, or `oC_ErrorCode_None` if there is no error. Moreover it return `oC_ErrorCode_ModuleNotStartedYet` when the module
 * was not turned on.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the RAM
 *
 * The function is for reading the start address of the machine RAM. Note, that it is not reserved memory, and any try to write at this address
 * may cause memory or hard fault.
 *
 * @return address to start of the RAM section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetRamStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns end address of the RAM
 *
 * The function is for reading the end address of the machine RAM. Note, that it is not reserved memory, and any try to write at this address
 * may cause memory or hard fault.
 *
 * @return address to end of the RAM section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetRamEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the ram
 *
 * The function returns size in bytes of the RAM memory. Note, that it is size of the all RAM - both used, and not.
 *
 * @return size of the RAM in bytes
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetRamSize( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the DMA RAM
 *
 * The function is for reading the start address of the machine DMA RAM. Note, that it is not reserved memory, and any try to write at this address
 * may cause memory or hard fault.
 *
 * @return address to start of the RAM section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetDmaRamStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns end address of the DMA RAM
 *
 * The function is for reading the end address of the machine RAM. Note, that it is not reserved memory, and any try to write at this address
 * may cause memory or hard fault.
 *
 * @return address to end of the RAM section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetDmaRamEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the DMA ram
 *
 * The function returns size in bytes of the DMA RAM memory. Note, that it is size of the all RAM - both used, and not.
 *
 * @return size of the RAM in bytes
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetDmaRamSize( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the flash
 *
 * The function returns begin point address of the flash in the machine. Note, that it is start of the all flash - used and not.
 *
 * @return start address of the flash
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetFlashStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns end address of the flash
 *
 * The function returns finish point address of the flash in the machine. Note, that it is end of the all flash - used and not.
 *
 * @return end address of the flash
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetFlashEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the flash section
 *
 * The function returns size of the all flash of machine (both used and not).
 *
 * @return size of the flash in bytes
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetFlashSize( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the heap
 *
 * The function returns start address of the heap section in the machine.
 *
 * @return start heap address
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetHeapStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns end address of the heap
 *
 * The function returns end address of the heap section in the machine.
 *
 * @return end address of the heap
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetHeapEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the heap
 *
 * The function returns size of the heap section memory, that is in bytes.
 *
 * @return size of the heap
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetHeapSize( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the stack
 *
 * The function returns start address of the stack.
 *
 * @return the start address of the stack
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetStackStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns end address of the stack
 *
 * The function returns end address of the stack.
 *
 * @return end address of the stack
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetStackEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the stack
 *
 * The function returns size in bytes of the stack memory section.
 *
 * @return size of the stack
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetStackSize( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the data section
 *
 * The function returns address of the data section. This sections stores all ram data that was not dynamically allocated.
 *
 * @return start address of the data section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetDataStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns end address of the data section
 *
 * The function returns address of the data section. This sections stores all ram data that was not dynamically allocated.
 *
 * @return end address of the data section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetDataEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the data section
 *
 * The function returns size in bytes of the section in RAM that stores all data, that was not dynamically allocated in the machine.
 *
 * @return size of the data section
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetDataSize( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the Bss section
 *
 * The function returns address of the data section. This sections stores all ram data that was not dynamically allocated.
 *
 * @return start address of the data section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetBssStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns end address of the Bss section
 *
 * The function returns address of the data section. This sections stores all ram data that was not dynamically allocated.
 *
 * @return end address of the data section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetBssEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the Bss section
 *
 * The function returns size in bytes of the section in RAM that stores all data, that was not dynamically allocated in the machine.
 *
 * @return size of the data section
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetBssSize( void );
//==========================================================================================================================================
/**
 * @brief returns start address of the used flash section
 *
 * The function returns start address of the section in the flash where is stored program code.
 *
 * @return start address of the used flash section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetUsedFlashStartAddress( void );
//==========================================================================================================================================
/**
 * @brief returns used flash end address
 *
 * The function returns end address of the section in the flash that is stored used flash.
 *
 * @return end address of the used flash section
 */
//==========================================================================================================================================
extern void * oC_MEM_LLD_GetUsedFlashEndAddress( void );
//==========================================================================================================================================
/**
 * @brief returns size of the used flash section
 *
 * The function is for reading from linkage size of the section in the flash, that is used by system.
 *
 * @return size of the used flash section
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetUsedFlashSize( void );

//==========================================================================================================================================
/**
 * @brief returns size of alignment
 *
 * The function returns number of bytes, that the machine align the memory.
 *
 * @return size of alignment
 */
//==========================================================================================================================================
extern oC_MEM_LLD_Size_t oC_MEM_LLD_GetAlignmentSize( void );

//==========================================================================================================================================
/**
 * @brief checks if the pointer is in ram section
 *
 * @param Address   address to check
 *
 * @return true if address is in the ram
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsRamAddress( const void * Address );
//==========================================================================================================================================
/**
 * @brief checks if the pointer is in flash section
 *
 * @param Address   address to check
 *
 * @return true if address is in the flash
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsFlashAddress( const void * Address );
//==========================================================================================================================================
/**
 * @brief checks if the pointer is in used flash section
 *
 * @param Address   address to check
 *Change
 * @return true if address is in the used flash
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsUsedFlashAddress( const void * Address );

//==========================================================================================================================================
/**
 * @brief checks if the pointer is in heap section
 *
 * @param Address   address to check
 *
 * @return true if address is in the heap section
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsHeapAddress( const void * Address );

//==========================================================================================================================================
/**
 * @brief checks if the pointer is in external section
 *
 * @param Address   address to check
 *
 * @return true if address is in the external section
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsExternalAddress( const void * Address );

//==========================================================================================================================================
/**
 * @brief checks if the pointer is in DMA RAM section
 *
 * @param Address   address to check
 *
 * @return true if address is in the DMA RAM section
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsDmaRamAddress( const void * Address );

//==========================================================================================================================================
/**
 * @brief sets interrupt handler for memory fault
 *
 * The function is for setting an interrupt, that occurs, when some of memory section, that is protected is trying was accessed.
 * The module must be already turned on, before call of this function.
 *
 * @param Interrupt     The function to call, when the interrupt occurs
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_SetMemoryFaultInterrupt( oC_MEM_LLD_Interrupt_t Interrupt );
//==========================================================================================================================================
/**
 * @brief sets interrupt handler for bus fault
 *
 * @param Interrupt     The function to call, when the interrupt occurs
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_SetBusFaultInterrupt( oC_MEM_LLD_Interrupt_t Interrupt );
//==========================================================================================================================================
/**
 * @brief turns on memory fault interrupt
 *
 * The function is turning on memory fault interrupt. It is important, that this should not be called when the driver is turned on.
 *
 * @note The interrupt handler must be set before call of this function!
 *
 * @see oC_MEM_LLD_SetMemoryFaultInterrupt
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_TurnOnMemoryFaultInterrupt( void );
//==========================================================================================================================================
/**
 * @brief turns off memory fault interrupt
 *
 * The function is turning off memory fault interrupt. It should work also when the module is turned off
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_TurnOffMemoryFaultInterrupt( void );
//==========================================================================================================================================
/**
 * @brief turns on bus fault interrupt
 *
 * The function is turning on bus fault interrupt. It is important, that this should not be called when the driver is turned on.
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_TurnOnBusFaultInterrupt( void );
//==========================================================================================================================================
/**
 * @brief turns off bus fault interrupt
 *
 * The function is turning off bus fault interrupt. It should work also when the module is turned off
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_TurnOffBusFaultInterrupt( void );

//==========================================================================================================================================
/**
 * @brief checks if memory fault interrupt is turned on
 *
 * The function checks if the memory fault interrupt is turned on. If this function returns true, it means, that the interrupt handler is set,
 * internal mask is set, and the physical interrupt in the machine is also turned on. Note, that the module must not be enabled to call this function.
 *
 * @return bool - true if the interrupt is configured and turned on
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsMemoryFaultInterruptTurnedOn( void );

//==========================================================================================================================================
/**
 * @brief checks if bus fault interrupt is turned on
 *
 * The function checks if the bus fault interrupt is turned on. If this function returns true, it means, that the interrupt handler is set,
 * internal mask is set, and the physical interrupt in the machine is also turned on. Note, that the module must not be enabled to call this function.
 *
 * @return bool - true if the interrupt is configured and turned on
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_IsBusFaultInterruptTurnedOn( void );

//==========================================================================================================================================
/**
 * @brief configures memory region
 *
 * The function configures memory region.
 *
 * @param Config        Pointer to the configuration
 *
 * @return code of error or #oC_ErrorCode_None if success
 *
 * @see oC_MEM_LLD_MemoryRegionConfig_t
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_MEM_LLD_ConfigureMemoryRegion( oC_MEM_LLD_MemoryRegionConfig_t * Config );

//==========================================================================================================================================
/**
 * @brief Change current memory access mode
 *
 * The function is for changing currently used memory access mode.
 *
 * @param Mode      User or Privileged access mode, for more see #oC_MEM_LLD_MemoryAccessMode_t
 *
 * @return true if success
 */
//==========================================================================================================================================
extern bool oC_MEM_LLD_SetMemoryAccessMode( oC_MEM_LLD_MemoryAccessMode_t Mode );

//==========================================================================================================================================
/**
 * @brief returns currently configured memory access mode
 * @return currently set memory access mode
 */
//==========================================================================================================================================
extern oC_MEM_LLD_MemoryAccessMode_t oC_MEM_LLD_GetMemoryAccessMode( void );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_LLD_OC_MEM_LLD_H_ */
