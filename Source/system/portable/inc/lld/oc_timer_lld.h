/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the TIMER driver
 *
 * @file       oc_timer_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 1 05 2015 15:47:23)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup TIMER-LLD Timers 
 * @ingroup LowLevelDrivers
 * @brief Managing timers operations
 *
 * @par
 * The driver is for managing timers of the machine. Timers are divided for channels that can work independently of the others. The channel
 * variable is stored in the #oC_TIMER_LLD_Channel_t type, that is generated using definitions from the @link Machine @endlink module. General
 * rule of timer work is that it automatically increment or decrement special counter with the frequency that is selected during configuration.
 * It is counting to the configured value, and then it starts from the beginning. There are some predefined interrupts, that can occurs, when
 * the timer value achieve some specified value, or when some other event occur. Moreover most of timers are equipped with additional special
 * functionality, like generation PWM signal, counting edges from an input pin, or measuring time between 2 edges of input pin. This module
 * is for supporting most popular features, that timers modules are equipped. It includes:
 * - periodic interrupts
 * - time measurement
 * - input edge counting
 * - input edge time measurement
 * - PWM signal generation
 *
 * Example of PWM configuration:
 * @code{.c}
   oC_ErrorCode_t errorCode = oC_ErrorCode_None;

   if ( oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_TurnOnDriver()) )
   {
       oC_TIMER_LLD_PeripheralPin_t RedPwmPin;
       oC_TIMER_LLD_Channel_t       RedPwmChannel;
       oC_TIMER_LLD_SubTimer_t      RedPwmSubTimer;

       if(
          oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ReadPeripheralPinsOfPin(    RedPin ,    &RedPwmPin , &RedArraySize)) &&
          oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ReadSubTimerOfPeripheralPin(RedPwmPin,  &RedPwmSubTimer))
          )
       {
           RedPwmChannel = oC_TIMER_LLD_GetChannelOfPeripheralPin(RedPwmPin);

           if(
               oC_AssignErrorCodeIfFalse(&errorCode ,!oC_TIMER_LLD_IsChannelUsed(RedPwmChannel,RedPwmSubTimer) , oC_ErrorCode_ChannelIsUsed ) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_SetChannelUsed(RedPwmChannel ,      RedPwmSubTimer )) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_SetPower(RedPwmChannel ,            oC_Power_On)) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_TimerStop(RedPwmChannel ,           RedPwmSubTimer)) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_ChangeMode(RedPwmChannel ,          RedPwmSubTimer, oC_TIMER_LLD_Mode_PWM )) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_ChangeFrequency(RedPwmChannel ,     RedPwmSubTimer, oC_MHz(80) , oC_MHz(10))) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_ChangeMaximumValue(RedPwmChannel ,  RedPwmSubTimer, 255)) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_ChangeMatchValue(RedPwmChannel ,    RedPwmSubTimer, 0)) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_ConnectPeripheralPin(RedPwmPin)     ) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_ChangeStartPwmState(RedPwmChannel,  RedPwmSubTimer , oC_TIMER_LLD_PwmState_Low)) &&
               oC_AssignErrorCode(       &errorCode , oC_TIMER_LLD_TimerStart(RedPwmChannel ,          RedPwmSubTimer))
             )
           {
               errorCode = oC_ErrorCode_None;
           }
       }
   }

 * @endcode
 *
 ******************************************************************************************************************************************/
#ifndef INC_LLD_OC_TIMER_LLD_H_
#define INC_LLD_OC_TIMER_LLD_H_

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>

#if oC_Channel_IsModuleDefined(TIMER) == false
#error TIMER module channels are not defined
#elif oC_ModulePinFunctions_IsModuleDefined(TIMER) == false
#error  TIMER module pin functions are not defined
#elif oC_ModulePin_IsModuleDefined(TIMER) == false
#error  TIMER module pins are not defined
#else

/** ========================================================================================================================================
 *
 *              The section with types for the module
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup TIMER-LLD
//! @{
#define MODULE_NAME TIMER

//==========================================================================================================================================
/**
 * @enum oC_TIMER_Channel_t
 * @brief type with channels of timers
 *
 * The type stores timer channels. The 'channel' is timer ID. Each channel can work independent of the others.
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_TIMER_PinFunction_t
 *
 * @brief type for storing pin functions
 *
 * The type is for storing types of functionality, that the pin is created for.
 */
//==========================================================================================================================================
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_TIMER_Pin_t
 *
 * @brief type for storing timer pins.
 *
 * The type is an enum, that is for storing pins that are assigned for timers.
 */
//==========================================================================================================================================
oC_ModulePin_DefineType;

//==========================================================================================================================================
/**
 * @brief stores index of channel in the TIMER
 *
 * The type stores index of the channel in the TIMER array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_TIMER_LLD_ChannelIndex_t;

//==========================================================================================================================================
/**
 * @brief type for storing direction of timer counting
 *
 * The type is for storing direction of timer counting.
 */
//==========================================================================================================================================
typedef enum
{
    oC_TIMER_LLD_CountDirection_DoesntMatter ,//!< It doesn't matter what direction the timer will count.
    oC_TIMER_LLD_CountDirection_Up ,          //!< timer counts from 0 to maximum value
    oC_TIMER_LLD_CountDirection_Down          //!< timer counts from maximum value to 0
} oC_TIMER_LLD_CountDirection_t;

//==========================================================================================================================================
/**
 * @brief timer usage reason
 *
 * The type is for storing information about reason, that user need a timer.
 */
//==========================================================================================================================================
typedef enum
{
    oC_TIMER_LLD_Mode_NotSelected ,         //!< Mode not selected
    oC_TIMER_LLD_Mode_Reserved ,            //!< Channel is reserved
    oC_TIMER_LLD_Mode_PeriodicTimer ,       //!< periodic timer
    oC_TIMER_LLD_Mode_InputEdgeCount ,      //!< counting of input pin edges
    oC_TIMER_LLD_Mode_InputEdgeTime ,       //!< counting time of input pin edges
    oC_TIMER_LLD_Mode_RealTimeClock ,       //!< work with real time clock
    oC_TIMER_LLD_Mode_PWM                   //!< generation of PWM signal
} oC_TIMER_LLD_Mode_t;

//==========================================================================================================================================
/**
 * @brief triggering edge
 *
 * The type is for storing trigger edge information
 */
//==========================================================================================================================================
typedef enum
{
    oC_TIMER_LLD_Trigger_None ,             //!< none of trigger is used
    oC_TIMER_LLD_Trigger_RisingEdge ,       //!< rising edge will be an event
    oC_TIMER_LLD_Trigger_FallingEdge ,      //!< falling edge will be an event
    oC_TIMER_LLD_Trigger_Both               //!< both edges will be an event
} oC_TIMER_LLD_Trigger_t;

//==========================================================================================================================================
/**
 * @brief state of PWM pin
 *
 * The type is for storing state of the PWM output signal
 */
//==========================================================================================================================================
typedef enum
{
    oC_TIMER_LLD_PwmState_Low ,     //!< state is low
    oC_TIMER_LLD_PwmState_High      //!< state is high
} oC_TIMER_LLD_PwmState_t;

//==========================================================================================================================================
/**
 * @brief stores flags of timer events
 *
 * The type is for storing flags of events. Flags can be joined
 */
//==========================================================================================================================================
typedef enum
{
    oC_TIMER_LLD_EventFlags_MatchValueInterrupt             = (1<<0) ,      //!< current value is equal to the match value
    oC_TIMER_LLD_EventFlags_TimeoutInterrupt                = (1<<1) ,      //!< current value is equal to the maximum value, when counting up, or when current is 0 when counting down
    oC_TIMER_LLD_EventFlags_MaximumCountOfEdgesOccurs       = (1<<2) ,      //!< when in input count mode, the event occurs, when maximum count of edges occurs.
    oC_TIMER_LLD_EventFlags_EdgeDetect                      = (1<<3) ,      //!< the wanted edge is detected
    oC_TIMER_LLD_EventFlags_PwmPinInHighState               = (1<<4) ,      //!< PWM pin is in high state
    oC_TIMER_LLD_EventFlags_PwmPinInLowState                = (1<<5) ,      //!< PWM pin is in low state
    oC_TIMER_LLD_EventFlags_All                             = 0xFFFFFFFFUL ,//!< all events enabled
} oC_TIMER_LLD_EventFlags_t;

//==========================================================================================================================================
/**
 * @brief stores sub-timers
 *
 * The type is for storing sub-timers of timers, for example: TimerA,TimerB for Timer1. Note, that sub-timers can be join.
 */
//==========================================================================================================================================
typedef enum
{
    oC_TIMER_LLD_SubTimer_None   = 0 ,                                                        //!< none of sub-timer is used
    oC_TIMER_LLD_SubTimer_TimerA = (1<<0) ,                                                   //!< Timer A is used
    oC_TIMER_LLD_SubTimer_TimerB = (1<<1) ,                                                   //!< Timer B is used
    oC_TIMER_LLD_SubTimer_Both   = oC_TIMER_LLD_SubTimer_TimerA | oC_TIMER_LLD_SubTimer_TimerB//!< Both timers (Timer A and Timer B) are used
} oC_TIMER_LLD_SubTimer_t;

//==========================================================================================================================================
/**
 * @brief event handler for timers module
 *
 * The type is for storing pointer to the event handler, that should be called when an event occurs. Execution of the event should
 * be as short as possible.
 *
 * @param Channel               Channel of the timer (ex. Timer0) that cause the event
 * @param SubTimer              SubTimer of the channel (ex. TimerA) that cause the event
 * @param EventFlags            List of flags, that cause the event
 *
 */
//==========================================================================================================================================
typedef void (*oC_TIMER_LLD_EventHandler_t)(oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags );

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup TIMER-LLD
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each timer channel
 *
 * The macro is for creating a loop for each channel defined in the machine. As 'index' there is a variable that stores the channel ID.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * // example of loop that restores default states on each timer channel defined in the machine
 * oC_TIMER_LLD_ForEachChannel(Channel)
 * {
 *      // this will be executed for each channel
 *      oC_TIMER_LLD_RestoreDefaultStateOnChannel( Channel );
 * }
 * ~~~~~~~~~~~~
 *
 */
//==========================================================================================================================================
#define oC_TIMER_LLD_ForEachChannel( Channel )              oC_Channel_Foreach( TIMER , Channel )


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup TIMER-LLD
//! @{
//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief checks if channel is correct
 *
 * The function is for checking if a timer channel is correct.
 *
 * @param Channel       channel of timer
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_TIMER_LLD_IsChannelCorrect( oC_TIMER_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief checks if channel index is correct
 *
 * The function is for checking if the index of channel is correct
 *
 * @param ChannelIndex  index of a timer channel
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_TIMER_LLD_IsChannelIndexCorrect( oC_ChannelIndex_t ChannelIndex );
//==========================================================================================================================================
/**
 * @brief checks if pin is defined for the timer module
 *
 * The function is for checking if the peripheral pin is defined for the timer module.
 *
 * @param ModulePin     the pin to check
 *
 * @return true if it is defined
 */
//==========================================================================================================================================
extern bool oC_TIMER_LLD_IsModulePinDefined( oC_TIMER_Pin_t ModulePin );

//==========================================================================================================================================
/**
 * @brief checks if sub-timer is correct
 *
 * The function checks if the sub-timer is correct
 *
 * @param SubTimer      sub-timer of the channel (ex. TimerA)
 *
 * @return true if it is correct
 */
//==========================================================================================================================================
extern bool oC_TIMER_LLD_IsSubTimerCorrect(oC_TIMER_LLD_SubTimer_t SubTimer);
//==========================================================================================================================================
/**
 * @brief returns machine pin of peripheral pin
 *
 * The function is for reading pin from peripheral pin
 *
 * @param ModulePin     timer peripheral pin
 *
 * @return pin if defined or 0 if not
 */
//==========================================================================================================================================
extern oC_Pin_t oC_TIMER_LLD_GetPinOfModulePin( oC_TIMER_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief returns printable name of the peripheral pin
 *
 * The function returns printable name of peripheral pin. If the given pin is not correct or not defined, then it will return
 * "unknown" string.
 *
 * @param ModulePin     pin that is defined for the timer driver
 *
 * @return constant string with name of the peripheral pin
 */
//==========================================================================================================================================
extern const char * oC_TIMER_LLD_GetModulePinName( oC_TIMER_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief returns printable name of the timer channel
 *
 * The function returns printable name of a timer channel. If the channel is not correct or not defined, then it will return 'unknown' string.
 *
 * @param Channel       channel of timer
 *
 * @return constant string with name of the channel
 */
//==========================================================================================================================================
extern const char * oC_TIMER_LLD_GetChannelName( oC_TIMER_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief returns channel that is assigned to the peripheral pin
 *
 * Each peripheral pin is assigned to the channel. The function is for reading timer channel, that the peripheral pin is assigned.
 *
 * @param PeripheralPin     pin that is defined for the timer driver
 *
 * @return channel of timer or oC_TIMER_LLD_ChannelIndex_NumberOfElements if peripheral pin is not correct
 */
//==========================================================================================================================================
extern oC_TIMER_Channel_t oC_TIMER_LLD_GetChannelOfModulePin( oC_TIMER_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief converts timer channel to channel index
 *
 * The function is for converting a channel to channel index.
 *
 * @param Channel       channel of timer
 *
 * @return index of timer channel
 */
//==========================================================================================================================================
extern oC_ChannelIndex_t oC_TIMER_LLD_ChannelToChannelIndex( oC_TIMER_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief converts timer channel index to timer channel
 *
 * The function is for converting a channel index to channel.
 *
 * @param ChannelIndex  index of timer channel
 *
 * @return channel of timer
 */
//==========================================================================================================================================
extern oC_TIMER_Channel_t oC_TIMER_LLD_ChannelIndexToChannel( oC_ChannelIndex_t ChannelIndex );

//==========================================================================================================================================
/**
 * @brief turns on power for the timer
 *
 * The function is for turning on the power for the timer. It should be called before any timer configuration.
 *
 * @param Channel                   Channel of the timer (ex. Timer0)
 * @param Power                     Power state to set (on/off)
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_SetPower( oC_TIMER_Channel_t Channel , oC_Power_t Power );
//==========================================================================================================================================
/**
 * @brief reads power state for the timer
 *
 * The function reads power state for the timer.
 *
 * @param Channel                   Channel of the timer (ex. Timer0)
 * @param outPower                  Destination for the power state
 *
 * @return code of error
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadPower( oC_TIMER_Channel_t Channel , oC_Power_t * outPower );
//==========================================================================================================================================
/**
 * @brief restores default state on the selected channel
 *
 * The function restores default state on the selected channel. The channel after this function execution should be stopped, turned off, set
 * as unused and generally ready to reconfigure.
 *
 * @param Channel           channel of timer
 *
 * @return true if success
 */
//==========================================================================================================================================
extern bool oC_TIMER_LLD_RestoreDefaultStateOnChannel( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer);
//==========================================================================================================================================
/**
 * @brief starts timer
 *
 * The function is for starting a timer on selected channel.
 *
 * @param Channel                   Channel of the timer (ex. Timer0)
 * @param SubTimer                  SubTimer of the channel (ex. TimerA)
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_TimerStart( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer);
//==========================================================================================================================================
/**
 * @brief stops timer
 *
 * The function is for stopping a timer on selected channel.
 *
 * @param Channel                   Channel of the timer (ex. Timer0)
 * @param SubTimer                  SubTimer of the channel (ex. TimerA)
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_TimerStop( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer);
//==========================================================================================================================================
/**
 * @brief changes frequency
 *
 * The function is for changing the frequency of a timer channel (frequency of counter increment)
 *
 * @note The module must be enabled before use of this function, and the timer should be stopped before
 *
 * @param Channel                   Channel of the timer (ex. Timer0)
 * @param SubTimer                  SubTimer of the channel (ex. TimerA)
 * @param Frequency                 Frequency to set
 * @param PermissibleDifference     Maximum difference between real frequency and frequency that you want to set)
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeFrequency( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_Frequency_t Frequency , oC_Frequency_t PermissibleDifference );
//==========================================================================================================================================
/**
 * @brief reads channel frequency
 *
 * The function is for reading frequency of a timer channel (frequency of counter increment)
 *
 * @note The module must be enabled before use of this function, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param outFrequency          destination for the frequency variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadFrequency( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_Frequency_t * outFrequency );
//==========================================================================================================================================
/**
 * @brief changes maximum value configuration
 *
 * The function is for changing maximum value for timer channel.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param Value                 value to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeMaximumValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value );
//==========================================================================================================================================
/**
 * @brief reads channel maximum value
 *
 * The function is for maximum value of a timer channel.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param outValue              destination for the read variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadMaximumValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue );
//==========================================================================================================================================
/**
 * @brief changes current value configuration
 *
 * The function is for changing current value for timer channel.
 *
 * @param Channel           channel of the timer
 * @param Mode              mode of the timer
 * @param Value             value to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeCurrentValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value );
//==========================================================================================================================================
/**
 * @brief reads channel current value
 *
 * The function is for reading current value of a timer channel.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param outValue              destination for the read variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadCurrentValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue );
//==========================================================================================================================================
/**
 * @brief changes match value configuration
 *
 * The function is for changing match value for timer channel.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param Value                 value to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t Value );
//==========================================================================================================================================
/**
 * @brief reads channel match value
 *
 * The function is for reading match value of a timer channel.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param outValue              destination for the read variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadMatchValue( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , uint64_t * outValue );
//==========================================================================================================================================
/**
 * @brief configures timer mode
 *
 * The function configures mode of timer.
 *
 * @note The module must be enabled before use of this function, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param Mode                  One of available timer modes
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t Mode );
//==========================================================================================================================================
/**
 * @brief reads configured timer mode
 *
 * The function reads configured mode of timer
 *
 * @note The module must be enabled before use of this function, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param outMode               Destination for the timer mode
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadMode( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Mode_t * outMode );
//==========================================================================================================================================
/**
 * @brief changes count direction configuration
 *
 * The function is for changing count direction for timer channel.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param CountDirection        Direction of counting (up / down)
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_CountDirection_t CountDirection );
//==========================================================================================================================================
/**
 * @brief reads channel count direction
 *
 * The function is for reading count direction of a timer channel.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected, and the timer should be stopped before
 *
 * @param Channel               Channel of the timer (ex. Timer0)
 * @param SubTimer              SubTimer of the channel (ex. TimerA)
 * @param outCountDirection     destination for the read variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadCountDirection( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer, oC_TIMER_LLD_CountDirection_t * outCountDirection );
//==========================================================================================================================================
/**
 * @brief changes trigger configuration
 *
 * The function is for changing trigger for timer channel. For PWM mode it is choosing of edge (positive/negative) to trigger an interrupt,
 * and for input modes (for example input count mode) it is for choosing what edge should increment a timer/counter.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected, and the timer should be stopped before
 *
 * @param Channel           Channel of the timer (ex. Timer0)
 * @param SubTimer          SubTimer of the channel (ex. TimerA)
 * @param Trigger           Trigger of pin to call event
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Trigger_t Trigger );
//==========================================================================================================================================
/**
 * @brief reads trigger configuration
 *
 * The function is for read trigger for timer channel. For PWM mode it is choosing of edge (positive/negative) to trigger an interrupt,
 * and for input modes (for example input count mode) it is for choosing what edge should increment a timer/counter.
 *
 * @param Channel           Channel of the timer (ex. Timer0)
 * @param SubTimer          SubTimer of the channel (ex. TimerA)
 * @param outTrigger        Destination for the trigger
 *
 * @return code of error
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadTrigger( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_Trigger_t * outTrigger );
//==========================================================================================================================================
/**
 * @brief configures timer event handler
 *
 * The function is for configuration event handler for a timer. Events that are listed in the EventFlags, will be turned on.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected
 *
 * @param Channel           Channel of the timer (ex. Timer0)
 * @param SubTimer          SubTimer of the channel (ex. TimerA)
 * @param EventHandler      Pointer to the function, that should be called, when one of events occurs
 * @param EventFlags        List of event flags, that should be turned on
 *
 * @return code of error or #oC_ErrorCode_None if success
 *
 * @code{.c}
 * /// This function is called, when some event occurs.
 * /// For description of parameters look at #oC_TIMER_LLD_EventHandler_t type description
 * void ExampleEventHandler(oC_TIMER_LLD_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags )
 * {
 *
 * }
 *
 * void main(void)
 * {
 *      oC_TIMER_LLD_Channel_t Channel = oC_TIMER_LLD_Channel_Timer0;
 *      oC_TIMER_LLD_SubTimer_t SubTimer = oC_TIMER_LLD_SubTimer_TimerA;
 *
 *      oC_TIMER_LLD_ChangeEventHandler( Channel , SubTimer , ExampleEventHandler , oC_TIMER_LLD_EventFlags_All );
 * }
 * @endcode
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeEventHandler( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventHandler_t EventHandler , oC_TIMER_LLD_EventFlags_t EventFlags );
//==========================================================================================================================================
/**
 * @brief reads configuration of timer event handler
 *
 * The function reads event handler configuration of the sub-timer.
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected
 *
 * @param Channel           Channel of the timer (ex. Timer0)
 * @param SubTimer          SubTimer of the channel (ex. TimerA)
 * @param outEventHandler   Destination for the event handler function pointer
 * @param outEventFlags     Destination for the enabled event flags
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadEventHandler(oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventHandler_t * outEventHandler , oC_TIMER_LLD_EventFlags_t * outEventFlags );
//==========================================================================================================================================
/**
 * @brief Changes start PWM state
 *
 * The function is for changing the start PWM pin state (the first state of the pin in each cycle).
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected to the #oC_TIMER_LLD_Mode_PWM before use
 * of this function.
 *
 * @param Channel           channel of the timer
 * @param SubTimer          SubTimer of the channel
 * @param State             state to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ChangeStartPwmState( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer, oC_TIMER_LLD_PwmState_t State);
//==========================================================================================================================================
/**
 * @brief read start PWM state
 *
 * The function is for read the start PWM state (the first state of the pin in each cycle).
 *
 * @note The module must be enabled, channel should be powered on and the mode should be selected to the #oC_TIMER_LLD_Mode_PWM before use
 * of this function.
 *
 * @param Channel           channel of the timer
 * @param SubTimer          SubTimer of the channel
 * @param outState          destination for the read variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadStartPwmState( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_PwmState_t * outState);
//==========================================================================================================================================
/**
 * @brief connects pin to the timer
 *
 * The function is for connecting of peripheral pin to the channel of the timer. The channel and sub-timer is read from the peripheral pin.
 *
 * @param PeripheralPin         Pin designed for a timer channel
 *
 * @return code of error or #oC_ErrorCode_None if success
 *
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ConnectModulePin(oC_TIMER_Pin_t PeripheralPin );
//==========================================================================================================================================
/**
 * @brief reads sub-timer assigned to a peripheral pin
 *
 * The function is for reading sub-timer assigned to the peripheral pin.
 *
 * @param PeripheralPin         Pin designed for a timer channel
 * @param outSubTimer           Destination address for the sub-timer variable (must be in the RAM section)
 *
 * @return code of error or #oC_ErrorCode_None if success
 *
 * @code{.c}
 * oC_TIMER_LLD_PeripheralPin_t pin = oC_TIMER_LLD_PeripheralPin_Timer0A_PB6;
 * oC_TIMER_LLD_SubTimer_t subTimer = oC_TIMER_LLD_SubTimer_None;
 * oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
 *
 * errorCode = oC_TIMER_LLD_ReadSubTimerOfPeripheralPin( pin , &subTimer );
 *
 * if(errorCode == oC_ErrorCode_None)
 * {
 *    // sub timer is now correctly filled
 * }
 *
 * @endcode
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadSubTimerOfModulePin(oC_TIMER_Pin_t PeripheralPin , oC_TIMER_LLD_SubTimer_t * outSubTimer );
//==========================================================================================================================================
/**
 * @brief marks timer channel as used
 *
 * The function is for marking a timer channel as used. It is for prevent to reuse it for the others without restoring default configuration
 * on the channel. To check if the channel is actually used, use #oC_TIMER_LLD_IsChannelUsed function. Note, that functions from the LLD
 * layer does not prevent before reuse the same channel of the timer. It is only a feature, and the drivers should prevents by itself by reading
 * a used state before any configurations. To set a channel to unused use #oC_TIMER_LLD_SetChannelUnused function.
 *
 * @param Channel           channel of the timer
 * @param SubTimer          SubTimer of the channel
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_SetChannelUsed( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
//==========================================================================================================================================
/**
 * @brief marks timer channel as unused
 *
 * The function is for marking a timer channel as unused. It is for prevent to reuse it for the others without restoring default configuration
 * on the channel. To check if the channel is actually used, use #oC_TIMER_LLD_IsChannelUsed function. Note, that functions from the LLD
 * layer does not prevent before reuse the same channel of the timer. It is only a feature, and the drivers should prevents by itself by reading
 * a used state before any configurations. To set a channel to used use #oC_TIMER_LLD_SetChannelUsed function.
 *
 * @param Channel           channel of the timer
 * @param SubTimer          SubTimer of the channel
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_SetChannelUnused( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
//==========================================================================================================================================
/**
 * @brief checks if timer channel is already used
 *
 * The function is for checking if a timer channel is already in use. Each driver, that uses the **TIMER-LLD** module, should firstly check
 * if the channel is not in use, and then mark it as used by #oC_TIMER_LLD_SetChannelUsed function before any configurations. It prevents
 * to using it by other drivers.
 *
 * @param Channel           channel of the timer
 * @param SubTimer          SubTimer of the channel
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern bool oC_TIMER_LLD_IsChannelUsed( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer );
//==========================================================================================================================================
/**
 * @brief reads list of peripheral pins for the selected pin
 *
 * The function reads list of peripheral pins that are connected to the selected pin.
 *
 * @param Pin                       Pin to find peripheral pins
 * @param outPeripheralPinsArray    Pointer to the array where the peripheral pins should be returned
 * @param ArraySize                 Pointer to the variable with size of the array. It will be also used to return size of matched pins
 *
 * @return code of error (`oC_ErrorCode_OutputArrayToSmall` if the outPeripheralPinsArray is too small)
 *
 * @code{.c}
 *
 * oC_TIMER_LLD_PeripheralPin_t peripheralPinsArray[10];
 * uint32_t arraySize = 10;
 * oC_TIMER_LLD_ReadPeripheralPinsOfPin( oC_Pin_PA2 , peripheralPinsArray , &arraySize );
 *
 * forif(uint32_t index = 0, index < arraySize , index++) // The forif is defined in the oc_forif.h file
 * {
 *      // peripheralPinsArray[index] is not filled peripheral pin
 * }
 * else
 * {
 *      // None of pins matches oC_Pin_PA2
 * }
 * @endcode
 *
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_TIMER_LLD_ReadModulePinsOfPin( oC_Pin_t Pin , oC_TIMER_Pin_t * outPeripheralPinsArray , uint32_t * ArraySize );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* TIMER module defined */
#endif /* INC_LLD_OC_TIMER_LLD_H_ */
