/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the ETH driver
 *
 * @file       oc_eth_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 2016-07-28 - 21:37:41)
 *
 * @copyright  Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ETH-LLD ETH 
 * @ingroup LowLevelDrivers
 * @brief Handles ETH transmissions
 *
 ******************************************************************************************************************************************/


#ifndef _OC_ETH_LLD_H
#define _OC_ETH_LLD_H

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>
#include <oc_pins.h>
#include <oc_channels.h>
#include <oc_baudrate.h>
#include <oc_diag.h>

#if oC_Channel_IsModuleDefined(ETH) && oC_ModulePinFunctions_IsModuleDefined(ETH) && oC_ModulePin_IsModuleDefined(ETH)
#define oC_ETH_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define oC_ETH_LLD_ETHERNET_MTU         1500
#define oC_ETH_LLD_MAC_ADDRESS_LENGTH   6

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup ETH-LLD
//! @{
#define MODULE_NAME ETH


//==========================================================================================================================================
/**
 * @enum oC_ETH_Channel_t
 * @brief channel of the ETH
 *
 * The type stores channel of the ETH
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_ETH_LLD_PinFunction_t
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_ETH_Pin_t
 * @brief pin of the ETH peripheral
 *
 * The type stores peripheral pin, where the ETH channel is connected.
 */
//==========================================================================================================================================
oC_ModulePin_DefineType;
//==========================================================================================================================================
/**
 * @brief stores index of channel in the ETH
 *
 * The type stores index of the channel in the ETH array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_ETH_LLD_ChannelIndex_t;

//==========================================================================================================================================
/**
 * @brief stores type of the segment (for packets transmission)
 */
//==========================================================================================================================================
typedef enum
{
    oC_ETH_LLD_FrameSegment_First       = (1<<0) ,              //!< This is the first segment of the packet
    oC_ETH_LLD_FrameSegment_Last        = (1<<1) ,              //!< This is the last segment of the packet
    oC_ETH_LLD_FrameSegment_Single      = (1<<0) | (1<<1),      //!< This is single segment (not divided)
    oC_ETH_LLD_FrameSegment_Middle      = 0,                    //!< This is middle segment of the packet
    oC_ETH_LLD_FrameSegment_NumberOfFrameSegments = 4,          //!< Number of elements in this type
} oC_ETH_LLD_FrameSegment_t;

//==========================================================================================================================================
/**
 * @brief stores communication with PHY interface
 *
 * The type is for storing a method to communicate with the PHY.
 */
//==========================================================================================================================================
typedef enum
{
    oC_ETH_LLD_PHY_CommunicationInterface_None ,    //!< Communication interface not filled
    oC_ETH_LLD_PHY_CommunicationInterface_MII ,     //!< Media Independent Interface (MII)
    oC_ETH_LLD_PHY_CommunicationInterface_RMII      //!< Reduced Media Independent Interface (RMII)
} oC_ETH_LLD_PHY_CommunicationInterface_t;

//==========================================================================================================================================
/**
 * @brief stores protocol of the media access
 *
 * The type is for storing a method to access the media.
 */
//==========================================================================================================================================
typedef enum
{
    oC_ETH_LLD_AccessProtocol_CSMA_CD , //!< Carrier sense multiple access with collision detection. Frequently used protocol in the Ethernet
    oC_ETH_LLD_AccessProtocol_CSMA_CA , //!< Carrier sense multiple access with collision avoidance. Frequently used protocol in WiFi.
    oC_ETH_LLD_AccessProtocol_ALOHA   , //!< The first protocol in the history. It was used in the radio network on Hawaii, and ... Probably only there
} oC_ETH_LLD_AccessProtocol_t;

//==========================================================================================================================================
/**
 * @brief stores operation mode
 *
 * The type is for storing operation mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_ETH_LLD_OperationMode_HalfDuplex ,   //!< Data can be transferred only in one direction at the same time
    oC_ETH_LLD_OperationMode_FullDuplex ,   //!< Data can be transferred in both directions at the same time
} oC_ETH_LLD_OperationMode_t;

//==========================================================================================================================================
/**
 * @brief stores external PHY chip info
 *
 * The type is for storing informations specific for the selected chip
 */
//==========================================================================================================================================
typedef struct
{
    oC_ETH_LLD_PHY_CommunicationInterface_t         CommunicationInterface;     //!< Interface to communicate with the PHY
    oC_BaudRate_t                                   PossibleBaudRates[5];       //!< List of possible baud rates
} oC_ETH_LLD_PHY_ChipInfo_t;

//==========================================================================================================================================
/**
 * @brief stores address of the PHY
 */
//==========================================================================================================================================
typedef uint16_t oC_ETH_LLD_PHY_Address_t;

//==========================================================================================================================================
/**
 * @brief stores address of a PHY register
 */
//==========================================================================================================================================
typedef uint16_t oC_ETH_LLD_PHY_RegisterAddress_t;

//==========================================================================================================================================
/**
 * @brief stores configuration of the ETH driver
 */
//==========================================================================================================================================
typedef struct
{
    bool                        AutoGeneratePad;            //!< Padding will be automatically generated. (Check #oC_ETH_LLD_IsAutoPadGenerationSupported for more)
    bool                        AutoCalculateCrc;           //!< CRC will be automatically calculated (Check #oC_ETH_LLD_IsAutoCalculateCrcSupported for more)
    oC_ETH_LLD_PHY_Address_t    PhyAddress;                 //!< Address of the external PHY (leave it 0 if there is only 1 PHY connected to the board)
    oC_BaudRate_t               BaudRate;                   //!< Sets baud rate. Leave it as 0 if you want to the fastest possible
    oC_ETH_LLD_OperationMode_t  OperationMode;              //!< This is the operation mode of the Ethernet - you can choose between Half and Full duplex mode here.
    union
    {
        //==================================================================================================================================
        /**
         *  @brief List of pins required for RMII interface
         */
        //==================================================================================================================================
        struct
        {
            oC_Pin_t    Mdc;
            oC_Pin_t    Mdio;
            oC_Pin_t    RefClk;
            oC_Pin_t    RcsDv;
            oC_Pin_t    RxD0;
            oC_Pin_t    RxD1;
            oC_Pin_t    TxEn;
            oC_Pin_t    TxD0;
            oC_Pin_t    TxD1;
            oC_Pin_t    RxEr;
        } Rmii;

        //==================================================================================================================================
        /**
         *  @brief List of pins required for MII interface
         */
        //==================================================================================================================================
        struct
        {
            oC_Pin_t    Mdc;
            oC_Pin_t    Mdio;
            oC_Pin_t    Crs;
            oC_Pin_t    Col;
            oC_Pin_t    RxClk;
            oC_Pin_t    RxDv;
            oC_Pin_t    RxD0;
            oC_Pin_t    RxD1;
            oC_Pin_t    RxD2;
            oC_Pin_t    RxD3;
            oC_Pin_t    RxEr;
            oC_Pin_t    TxEn;
            oC_Pin_t    TxD0;
            oC_Pin_t    TxD1;
            oC_Pin_t    TxD2;
            oC_Pin_t    TxD3;
            oC_Pin_t    TxClk;
        } Mii;
        //==================================================================================================================================
        /**
         * @brief array with defined pins (only for help, don't fill it)
         */
        //==================================================================================================================================
        oC_Pin_t    Pins[17];
    };

} oC_ETH_LLD_Config_t;

//==========================================================================================================================================
/**
 * @brief type for storing ethernet data
 */
//==========================================================================================================================================
typedef uint8_t oC_ETH_LLD_Payload_t[oC_ETH_LLD_ETHERNET_MTU];

//==========================================================================================================================================
/**
 * @brief type for storing MAC address
 */
//==========================================================================================================================================
typedef uint8_t oC_ETH_LLD_MacAddress_t[oC_ETH_LLD_MAC_ADDRESS_LENGTH];


//==========================================================================================================================================
/**
 * @brief Ethernet 802.3 frame
 */
//==========================================================================================================================================
typedef struct
{
#if 0
    uint8_t                 Preamble[7];                //!< Constant preamble (0x55 0x55 0x55 0x55 0x55 0x55 0x55)
    uint8_t                 StartOfFrameDelimiter;      //!< Delimiter for recognition of start of frame (always 0xD5)
#endif
    oC_ETH_LLD_MacAddress_t DestinationAddress;         //!< MAC destination address
    oC_ETH_LLD_MacAddress_t SourceAddress;              //!< MAC source address
    uint16_t                DataLengthEtherType;        //!< If value smaller than 1500, it is a length of the frame, if it is greater than 1536 it is type of the frame (Ethernet II)
    oC_ETH_LLD_Payload_t    Data;                       //!< Data of the frame (Payload)
    uint32_t                CRC;                        //!< CRC
} oC_ETH_LLD_Frame_t;

//==========================================================================================================================================
/**
 * @brief stores DMA descriptor data
 */
//==========================================================================================================================================
typedef struct Descriptor_t * oC_ETH_LLD_Descriptor_t;

//==========================================================================================================================================
/**
 * @brief stores source of Ethernet interrupts
 */
//==========================================================================================================================================
typedef enum
{
    oC_ETH_LLD_InterruptSource_DataReceived                    = (1<<0)  , //!< oC_ETH_LLD_InterruptSource_DataReceived
    oC_ETH_LLD_InterruptSource_ReceiveError                    = (1<<1)  , //!< oC_ETH_LLD_InterruptSource_DataReceived
    oC_ETH_LLD_InterruptSource_TransmitError                   = (1<<2)  , //!< oC_ETH_LLD_InterruptSource_DataReceived
    oC_ETH_LLD_InterruptSource_TransmissionSlotsAvailable      = (1<<3)  , //!< One more transmission slot is available
    oC_ETH_LLD_InterruptSource_FatalBusError                   = (1<<4)  , //!< Fatal BUS error interrupt
    oC_ETH_LLD_InterruptSource_ReceiveTimeout                  = (1<<5)  , //!< Maximum time for receiving frame expired
    oC_ETH_LLD_InterruptSource_ReceiveProcessStopped           = (1<<6)  , //!< Receive process stopped
    oC_ETH_LLD_InterruptSource_ReceiveDescriptorError          = (1<<7)  , //!< Receive descriptors error
    oC_ETH_LLD_InterruptSource_TransmitJabberTimeout           = (1<<8)  , //!< Transmit Jabber timeout
    oC_ETH_LLD_InterruptSource_TransmitDescriptorError         = (1<<9)  , //!< Transmit descriptors error
    oC_ETH_LLD_InterruptSource_TransmitProcessStopped          = (1<<10) , //!< Transmit process stopped
    oC_ETH_LLD_InterruptSource_ReceiveOverflowError            = (1<<11) , //!< Receive overflow error
    oC_ETH_LLD_InterruptSource_TransmitUnderflowError          = (1<<12) , //!< Transmit underflow error
} oC_ETH_LLD_InterruptSource_t;

//==========================================================================================================================================
/**
 * @brief Stores pointer to the function that should be called when an interrupt occurs
 */
//==========================================================================================================================================
typedef void (*oC_ETH_LLD_InterruptFunction_t)( oC_ETH_LLD_InterruptSource_t Source );

//==========================================================================================================================================
/**
 * @brief stores resultant data of the ETH LLD
 *
 * The type is for storing result informations about the configuration that are required for work of the LLD layer of the ETH
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pin_t            ConnectedPins[17];
    uint32_t            NumberOfMacAddresses;
    struct
    {
        oC_MemorySize_t             DescriptorSize;
        uint32_t                    RingSize;
        oC_MemorySize_t             Alignment;
        oC_ETH_LLD_Descriptor_t     NextDescriptor;
        uint32_t                    SegmentStarted;
    } TxData, RxData;
} oC_ETH_LLD_Result_t;

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup ETH-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_TurnOffDriver( void );

//==========================================================================================================================================
/**
 * @brief sets handler for the module interrupt
 *
 * The function is designed for setting pointer to the function, that handles interrupts of the module. If the pointer is set, it cannot be
 * changed until restart of the module.
 *
 * @param Function      Function to call when the interrupt occur
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                   | Description
 * ----------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None                | Operation success
 *  oC_ErrorCode_NotImplement        | Function is already not implemented
 *  oC_ErrorCode_ImplementError      | There was unexpected error in implementation
 *  oC_ErrorCode_ModuleNotStartedYet | #oC_ETH_LLD_TurnOn was not called yet
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_SetInterruptHandler( oC_ETH_LLD_InterruptFunction_t Function );

//==========================================================================================================================================
/**
 * @brief returns true if machine supports auto pad generation
 *
 * To meet the IEEE 802.3 standard requirements, the minimum data field size has to be 46 bytes. In case of smaller data packet, it has to be
 * filled to meet this requirement. Some of machine ETH modules can do it automatically, but if some does not, this function will return false.
 *
 * @return true if auto pad generation is supported in the target machine
 */
//==========================================================================================================================================
extern bool oC_ETH_LLD_IsAutoPadGenerationSupported( void );

//==========================================================================================================================================
/**
 * @brief returns true if machine supports auto calculation of CRC
 *
 * The Ethernet frame contains field of CRC that can be counted automatically before sending the frame. This function returns true if this
 * mechanism is supported in the target machine.
 */
//==========================================================================================================================================
extern bool oC_ETH_LLD_IsAutoCalculateCrcSupported( void );

//==========================================================================================================================================
/**
 * @brief initializes MAC to work
 *
 * The function initializes MAC to work. It connects PINs, enable clocks, etc.
 *
 * @param Config        Configuration of the module
 * @param ChipInfo      Structure with parameters of the Chip
 * @param outResult     Destination pointer for a result of the configuration
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_InitializeMac( const oC_ETH_LLD_Config_t * Config , const oC_ETH_LLD_PHY_ChipInfo_t * ChipInfo , oC_ETH_LLD_Result_t * outResult );

//==========================================================================================================================================
/**
 * @brief releases MAC
 *
 * Releases MAC when it is not needed anymore. MAC should be initialized by the #oC_ETH_LLD_InitializeMac function before.
 *
 * @param Config        Configuration of the module
 * @param ChipInfo      Structure with parameters of the Chip
 * @param Result        Result of the MAC initialization from the #oC_ETH_LLD_InitializeMac
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_ReleaseMac( const oC_ETH_LLD_Config_t * Config , const oC_ETH_LLD_PHY_ChipInfo_t * ChipInfo , oC_ETH_LLD_Result_t * Result );

//==========================================================================================================================================
/**
 * @brief reads PHY register
 *
 * The function is for reading external PHY register.
 *
 * @param PhyAddress        external PHY address
 * @param RegisterAddress   Address of the register
 * @param outValue          Destination for the register value
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_ReadPhyRegister( oC_ETH_LLD_PHY_Address_t PhyAddress , oC_ETH_LLD_PHY_RegisterAddress_t RegisterAddress , uint32_t * outValue );

//==========================================================================================================================================
/**
 * @brief writes PHY register
 *
 * The function is for writing external PHY register.
 *
 * @param PhyAddress        external PHY address
 * @param RegisterAddress   Address of the register
 * @param Value             Value to write
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_WritePhyRegister( oC_ETH_LLD_PHY_Address_t PhyAddress , oC_ETH_LLD_PHY_RegisterAddress_t RegisterAddress , uint32_t Value );

//==========================================================================================================================================
/**
 * @brief enables/disables MAC loopback
 *
 * The function is for enabling and disabling MAC loopback mode. If the MAC loopback is enabled, all data are automatically transmitted from
 * the Tx to the Rx at the (R)MII.
 *
 * @param Enabled       Flag if the loopback mode should be enabled
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_SetMacLoopback( bool Enabled );

//==========================================================================================================================================
/**
 * @brief initializes DMA descriptors
 *
 * The function sets DMA descriptors pointers. You should call #oC_ETH_LLD_InitializeMac function before to get size of descriptors to allocate.
 *
 * @param Result        Result of MAC Initialization from function #oC_ETH_LLD_InitializeMac
 * @param Config        LLD configuration of the ETH module
 * @param Tx            Allocated Tx descriptors list
 * @param Rx            Allocated Rx descriptors list
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_InitializeDescriptors( oC_ETH_LLD_Result_t * Result , const oC_ETH_LLD_Config_t * Config , oC_ETH_LLD_Descriptor_t Tx , oC_ETH_LLD_Descriptor_t Rx );

//==========================================================================================================================================
/**
 * @brief initializes DMA module to work
 *
 * The function is for initialization of the DMA for transfers Rx and Tx to the MAC.
 *
 * @param Result        Result of MAC Initialization from function #oC_ETH_LLD_InitializeMac
 * @param Config        LLD configuration of the ETH module
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_InitializeDma( oC_ETH_LLD_Result_t * Result , const oC_ETH_LLD_Config_t * Config , oC_ETH_LLD_Descriptor_t Tx , oC_ETH_LLD_Descriptor_t Rx );

//==========================================================================================================================================
/**
 * @brief starts the MAC module
 *
 * The function is for starting handle of transactions between PHY and MAC. It should be called at the end of the configuration.
 *
 * @param Config        LLD configuration of the ETH module
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_Start( const oC_ETH_LLD_Config_t * Config );
// TODO: Interface for sending a pause frame

// TODO: WoL support

//==========================================================================================================================================
/**
 * @brief sends data via Ethernet
 *
 * The function sends data to the given address. The ETH LLD driver has to be initialized by the #oC_ETH_LLD_InitializeMac function first, and
 * descriptors has to be also initialized by function named #oC_ETH_LLD_InitializeDescriptors.
 *
 * @param Result            Result of MAC Initialization from function #oC_ETH_LLD_InitializeMac
 * @param Source            Source MAC address to use for the frame
 * @param Destination       Destination MAC address
 * @param Data              Data to send (max size is 1500)
 * @param Size              Pointer to size of the data to send (max 1500)
 * @param FrameSegment      Segment of the frame (start/last/middle/single) to send
 * @param EtherType         Ethernet II frame type (or size lower than 1500 bytes if it is Ethernet I)
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_SendFrame( oC_ETH_LLD_Result_t * Result , const oC_ETH_LLD_MacAddress_t Source, const oC_ETH_LLD_MacAddress_t Destination, const void * Data , uint16_t Size , oC_ETH_LLD_FrameSegment_t FrameSegment , uint16_t EtherType );

//==========================================================================================================================================
/**
 * @brief receives data via Ethernet when if it is available
 *
 * The function receives data from Ethernet. The ETH LLD driver has to be initialized by the #oC_ETH_LLD_InitializeMac function first, and
 * descriptors has to be also initialized by function named #oC_ETH_LLD_InitializeDescriptors.
 *
 * @param Result            Result of MAC Initialization from function #oC_ETH_LLD_InitializeMac
 * @param outSource         Pointer for Source MAC address
 * @param outDestination    Pointer for Destination MAC address
 * @param Data              Pointer to the frame DATA (probably packet)
 * @param Size              Size of the memory given as `Data` argument on input. On output size of the received data
 * @param outFrameSegment   Destination pointer for the frame segment (last/single,first,middle,etc)
 * @param outEtherType      Type of the ethernet frame
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_ReceiveFrame( oC_ETH_LLD_Result_t * Result , oC_ETH_LLD_MacAddress_t outSource, oC_ETH_LLD_MacAddress_t outDestination, void * Data , uint16_t * Size , oC_ETH_LLD_FrameSegment_t * outFrameSegment , uint16_t * outEtherType );

//==========================================================================================================================================
/**
 * @brief sets MAC address
 *
 * The function is for setting MAC address of the module.The number of available MAC addresses you can read from the #oC_ETH_LLD_Result_t structure
 * that should be filled during MAC initialization.
 *
 * @param Result            Result of MAC Initialization from function #oC_ETH_LLD_InitializeMac
 * @param MacAddressIndex   Index of the MAC address to set
 * @param Address           MAC address (pointer to the array)
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_SetMacAddress( oC_ETH_LLD_Result_t * Result , uint32_t MacAddressIndex, const oC_ETH_LLD_MacAddress_t Address );

//==========================================================================================================================================
/**
 * @brief reads MAC address
 *
 * The function is for reading MAC address of the module.The number of available MAC addresses you can read from the #oC_ETH_LLD_Result_t structure
 * that should be filled during MAC initialization.
 *
 * @param Result            Result of MAC Initialization from function #oC_ETH_LLD_InitializeMac
 * @param MacAddressIndex   Index of the MAC address to set
 * @param Address           Destination for the MAC address (pointer to the array)
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_ReadMacAddress( oC_ETH_LLD_Result_t * Result , uint32_t MacAddressIndex, oC_ETH_LLD_MacAddress_t outAddress );

//==========================================================================================================================================
/**
 * @brief returns true if transmit queue is FULL
 */
//==========================================================================================================================================
extern bool oC_ETH_LLD_IsTransmitQueueFull( oC_ETH_LLD_Result_t * Result );

//==========================================================================================================================================
/**
 * @brief returns true if data is ready to read
 */
//==========================================================================================================================================
extern bool oC_ETH_LLD_IsDataReadyToReceive( oC_ETH_LLD_Result_t * Result );

//==========================================================================================================================================
/**
 * @brief performs diagnostics on LLD layer
 *
 * The function is for performing diagnostics of the LLD layer.
 *
 * @param Result            Result of MAC Initialization from function #oC_ETH_LLD_InitializeMac
 * @param Diag              Pointer to the Diagnostics array, or #NULL if you need number of supported diagnostics
 * @param NumberOfDiags     Pointer to the size of the diagnostics array on input, on output number of supported diagnostics
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_ETH_LLD_PerformDiagnostics( oC_ETH_LLD_Result_t * Result , oC_Diag_t * Diag , uint32_t * NumberOfDiags );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* _OC_ETH_LLD_H */
#endif
