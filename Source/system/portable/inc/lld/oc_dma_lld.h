/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the DMA driver
 *
 * @file       oc_dma_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 3 05 2015 13:18:38)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup DMA-LLD DMA Driver
 * @ingroup LowLevelDrivers
 *
 * @brief Direct Memory Access Portable Layer
 *
 * @par
 * The driver is for handle DMA operations. It helps to configure it to transfer data between 2 pointers, from peripheral to pointer and from
 * pointer to peripheral. Driver must be turned on before usage by calling function #oC_DMA_LLD_TurnOnDriver and turned off by calling function
 * #oC_DMA_LLD_TurnOffDriver. After power on the driver each DMA channel is set to unused (as available). After configuration of channel
 * it is marked as not available until it will stop data transfer. To check if channel is available use function #oC_DMA_LLD_IsChannelAvailable.
 *
 * @par
 * Each DMA channel has assigned its destination. To check if a channel supports configuration use #oC_DMA_LLD_IsSoftwareTradeSupportedOnChannel
 * and #oC_DMA_LLD_IsSoftwareTradeSupportedOnChannel functions. When you do not need a DMA channel anymore, use #oC_DMA_LLD_RestoreDefaultStateOnChannel function
 * to restore default state on it.
 *
 ******************************************************************************************************************************************/

#ifndef INC_LLD_OC_DMA_LLD_H_
#define INC_LLD_OC_DMA_LLD_H_

#include <oc_machine.h>
#include <oc_errors.h>

#if oC_Channel_IsModuleDefined(DMA) == false
#error DMA module is not defined
#else

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup DMA-LLD
//! @{
#define MODULE_NAME DMA

//==========================================================================================================================================
/**
 * @enum oC_DMA_Channel_t
 * @brief type for storing channel of the DMA
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @brief type with priority for the channel
 *
 * The type is for storing priority of the channel. Note that each priority that is not supported on the machine should be treated as medium
 */
//==========================================================================================================================================
typedef enum
{
    oC_DMA_LLD_Priority_Low ,   //!< low priority
    oC_DMA_LLD_Priority_Medium ,//!< medium priority
    oC_DMA_LLD_Priority_High    //!< high priority
} oC_DMA_LLD_Priority_t;

//==========================================================================================================================================
/**
 * @brief type of DMA interrupt
 *
 * The type is for storing interrupt handler.
 *
 * @param Channel   Channel that cause interrupt
 */
//==========================================================================================================================================
typedef void (*oC_DMA_LLD_EventHandler_t)(oC_DMA_Channel_t Channel);

//==========================================================================================================================================
/**
 * @brief size of each element in the transfer
 */
//==========================================================================================================================================
typedef enum
{
    oC_DMA_LLD_ElementSize_Byte         = 0 ,//!< Each element is 1 byte length
    oC_DMA_LLD_ElementSize_HalfWord     = 1 ,//!< Each element is 2 bytes length
    oC_DMA_LLD_ElementSize_Word         = 2  //!< Each element is 4 bytes length
} oC_DMA_LLD_ElementSize_t;

//==========================================================================================================================================
/**
 * @brief direction of peripheral transmission
 */
//==========================================================================================================================================
typedef enum
{
    oC_DMA_LLD_Direction_Transmit ,//!< Data is transmit to peripheral
    oC_DMA_LLD_Direction_Receive   //!< Data is transmit from peripheral
} oC_DMA_LLD_Direction_t;

//==========================================================================================================================================
/**
 * @brief configuration structure for software trade
 */
//==========================================================================================================================================
typedef struct
{
    const void *                Source;                         //!< source address from where data should be transfered
    void *                      Destination;                    //!< destination address where data should be copied
    oC_DMA_LLD_EventHandler_t   TransferCompleteEventHandler;   //!< address of function that should be called when the transfer is complete
    oC_UInt_t                   BufferSize;                     //!< size of Source/Destination buffers (must be the same)
    oC_DMA_LLD_Priority_t       Priority;                       //!< priority of this transfer
    oC_DMA_LLD_ElementSize_t    ElementSize;                    //!< size of each element in transfer
} oC_DMA_LLD_SoftwareTradeConfig_t;

//==========================================================================================================================================
/**
 * @brief configuration structure for peripheral trade
 */
//==========================================================================================================================================
typedef struct
{
    void *                      Buffer;                         //!< buffer address for data copy (source or destination)
    oC_DMA_LLD_EventHandler_t   TransferCompleteEventHandler;   //!< address of function that should be called when the transfer is complete
    oC_Channel_t                PeripheralChannel;              //!< peripheral channel to copy (for example UART0)
    void *                      PeripheralData;                 //!< reference to the peripheral data
    oC_UInt_t                   BufferSize;                     //!< size of buffer
    oC_DMA_LLD_Priority_t       Priority;                       //!< priority of this transfer
    oC_DMA_LLD_ElementSize_t    ElementSize;                    //!< size of each element in transfer
    oC_DMA_LLD_Direction_t      TransmitDirection;              //!< direction of transmit (from or to peripheral)
    oC_Machine_DmaSignalType_t  SignalType;                     //!< type of peripheral signal to configure transmission
} oC_DMA_LLD_PeripheralTradeConfig_t;

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup DMA-LLD
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief loop for each DMA channel
 *
 * This is the loop for each channel, that is defined for the DMA in the machine. It is like 'for' but each iteration in this loop is for different
 * DMA channel. It can be useful for example in case of searching available DMA channels.
 *
 * @param Channel       Name of the variable that stores current DMA channel
 * @param ChannelIndex  Name of the variable that stores index in DMA of actual channel
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 *
 * char source[100];
 * char destination[100];
 *
 * oC_DMA_LLD_ForeachChannel( Channel , ChannelIndex )
 * {
 *      if (oC_DMA_LLD_IsChannelAvailable( Channel ) )
 *      {
 *          oC_DMA_LLD_ConfigureSoftwareTrade( Channel , source , destination , sizeof(char) , 100 );
 *          break;
 *      }
 * }
 * ~~~~~~~~~~~~
 */
//==========================================================================================================================================
#define oC_DMA_LLD_ForeachChannel(Channel)          oC_Channel_Foreach(DMA , Channel)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}



/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup DMA-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_DMA_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_DMA_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief checks if channel is available
 *
 * The function is for checking if the DMA channel is available. To make channel available call #oC_DMA_LLD_RestoreDefaultStateOnChannel function.
 * Each channel is available after turning on the driver.
 *
 * @param Channel   DMA channel to check
 *
 * @return true if channel is available
 */
//==========================================================================================================================================
extern bool oC_DMA_LLD_IsChannelAvailable( oC_DMA_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief restores default state on channel
 *
 * The function is for restoring default state on the channel. It will break any transmission that is in progress and turn off this DMA channel.
 *
 * @param Channel       DMA channel
 */
//==========================================================================================================================================
extern void oC_DMA_LLD_RestoreDefaultStateOnChannel( oC_DMA_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief checks if channel is assigned for register map
 *
 * The function is for checking if the given DMA channel supports the given register map.
 *
 * @param Channel           DMA channel
 * @param RegisterMapIndex  Register map
 * @param SignalType        Signal type
 *
 * @return true if channel can be configured to read/write data from/to register map
 */
//==========================================================================================================================================
extern bool oC_DMA_LLD_IsChannelSupportedOnDmaChannel( oC_DMA_Channel_t DmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType );
//==========================================================================================================================================
/**
 * @brief checks if channel supports software DMA trading
 *
 * The function is for checking if the given channel supports transmission between 2 software addresses.
 *
 * @param Channel       DMA channel
 *
 * @return true if it is possible
 */
//==========================================================================================================================================
extern bool oC_DMA_LLD_IsSoftwareTradeSupportedOnDmaChannel( oC_DMA_Channel_t DmaChannel );
//==========================================================================================================================================
/**
 * @brief checks if the channel has access to the address
 *
 * Checks if the DMA channel can use address that is given as parameter
 *
 * @param Channel       DMA channel
 * @param Address       Memory address
 *
 * @return true if DMA has access to the address
 */
//==========================================================================================================================================
extern bool oC_DMA_LLD_DoesDmaHasAccessToAddress( oC_DMA_Channel_t Channel , const void * Address );
//==========================================================================================================================================
/**
 * @brief configure channel to software trade
 *
 * The function is for configuring DMA channel to copy data between 2 memory addresses.
 *
 * @param Channel       DMA channel
 * @param Config        Configuration structure with arguments required for the software trading mode.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_DMA_LLD_ConfigureSoftwareTrade( oC_DMA_Channel_t Channel , const oC_DMA_LLD_SoftwareTradeConfig_t * Config );
//==========================================================================================================================================
/**
 * @brief configures channel to trade with register map
 *
 * The function is for configuration of DMA channel to work with some machine register map.
 *
 * @param Channel       DMA channel
 * @param Config        Configuration structure with arguments required for the mode.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_DMA_LLD_ConfigurePeripheralTrade( oC_DMA_Channel_t Channel , const oC_DMA_LLD_PeripheralTradeConfig_t * Config );
//==========================================================================================================================================
/**
 * @brief checks if transfer on the given channel is complete
 *
 * The function is for checking if the transfer on the given channel is complete.
 *
 * @param Channel       DMA channel
 *
 * @return true if complete
 */
//==========================================================================================================================================
extern bool oC_DMA_LLD_IsTransferCompleteOnChannel( oC_DMA_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief reads reference to the flag of channel is used
 *
 * The function reads reference to the channel used flag. It can be useful, when some module is waiting for the channel.
 *
 * @param Channel                   DMA channel
 * @param outTransferCompleteFlag   Destination for the channel used flag pointer
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_DMA_LLD_ReadChannelUsedReference( oC_DMA_Channel_t Channel , bool ** outChannelUsedFlag );
//==========================================================================================================================================
/**
 * @brief search free channel for peripheral trade
 *
 * Searches free channel for peripheral trade
 *
 * @param outChannel                destination for DMA channel
 * @param RegisterMapIndex          Peripheral register map
 * @param SignalType                DMA - signal type
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_DMA_LLD_FindFreeDmaChannelForPeripheralTrade( oC_DMA_Channel_t * outDmaChannel , oC_Channel_t Channel , oC_Machine_DmaSignalType_t SignalType );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* DMA DEFINED */
#endif /* INC_LLD_OC_DMA_LLD_H_ */
