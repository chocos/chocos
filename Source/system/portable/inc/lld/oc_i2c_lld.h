/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the I2C driver
 *
 * @file       oc_i2c_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 2017-02-17 - 19:39:55)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup I2C-LLD I2C 
 * @ingroup LowLevelDrivers
 * @brief Handles I2C transmissions
 *
 ******************************************************************************************************************************************/


#ifndef _OC_I2C_LLD_H
#define _OC_I2C_LLD_H

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>
#include <oc_baudrate.h>

#if oC_Channel_IsModuleDefined(I2C) && oC_ModulePinFunctions_IsModuleDefined(I2C) && oC_ModulePin_IsModuleDefined(I2C)
#define oC_I2C_LLD_AVAILABLE

#if oC_Channel_IsModuleDefined(I2C) == false
#error I2C module channels are not defined
#elif oC_ModulePinFunctions_IsModuleDefined(I2C) == false
#error  I2C module pin functions are not defined
#elif oC_ModulePin_IsModuleDefined(I2C) == false
#error  I2C module pins are not defined
#else

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup I2C-LLD
//! @{
#define MODULE_NAME I2C


//==========================================================================================================================================
/**
 * @enum oC_I2C_Channel_t
 * @brief channel of the I2C
 *
 * The type stores channel of the I2C
 */
//==========================================================================================================================================
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_I2C_LLD_PinFunction_t
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @enum oC_I2C_Pin_t
 * @brief pin of the I2C peripheral
 *
 * The type stores peripheral pin, where the I2C channel is connected.
 */
//==========================================================================================================================================
oC_ModulePin_DefineType;
//==========================================================================================================================================
/**
 * @brief stores index of channel in the I2C
 *
 * The type stores index of the channel in the I2C array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_I2C_LLD_ChannelIndex_t;

//==========================================================================================================================================
/**
 * @brief stores I2C address
 */
//==========================================================================================================================================
typedef uint16_t oC_I2C_LLD_Address_t;

//==========================================================================================================================================
/**
 * @brief stores I2C Speed Mode
 *
 * Value of the speed mode stores maximum frequency of the mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_LLD_SpeedMode_Normal    = kBd(100) , //!< Max 100 kbps
    oC_I2C_LLD_SpeedMode_Fast      = kBd(400) , //!< Max 400 kbps
    oC_I2C_LLD_SpeedMode_FastPlus  = MBd(1)   , //!< Max 1 Mbps
    oC_I2C_LLD_SpeedMode_HighSpeed = kBd(3400), //!< Max 3.4 Mbps
    oC_I2C_LLD_SpeedMode_UltraFast = MBd(5)   , //!< Max 5 Mbps
} oC_I2C_LLD_SpeedMode_t;

//==========================================================================================================================================
/**
 * @brief stores addressing mode
 *
 * Value of the address mode stores maximum address that can be stored in the mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_LLD_AddressMode_7Bits   = 0x0FF, //!< address is 7 bits length, but on the bits 1..7, bit 0 is R/W
    oC_I2C_LLD_AddressMode_10Bits  = 0x3FF, //!< address is 10 bits length
} oC_I2C_LLD_AddressMode_t;

//==========================================================================================================================================
/**
 * @brief stores I2C mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_LLD_Mode_Master , //!< Master Mode
    oC_I2C_LLD_Mode_Slave  , //!< Slave Mode
} oC_I2C_LLD_Mode_t;

//==========================================================================================================================================
/**
 * @brief stores interrupt source / events
 *
 * The type is for storing interrupt sources and I2C LLD events. It is passed to the LLD configuration function to select interrupts
 * that should be enabled and later to an interrupt handler to inform which events are active and which are not.
 *
 * @warning
 * If you implement new I2C LLD driver (for new platform, that we don't support yet), remember to provide all interrupt sources, that
 * the main driver request for. If handle of some interrupt is not possible in the target, you have to find a way to simulate this event
 * by using global variables etc.
 *
 * The LLD driver has to inform by using interrupt handler, which events are currently active and which are not. This does not apply to the
 * 'Error Events', which are in the mask named #oC_I2C_LLD_InterruptSource_TransferErrors - if some error occured, the main driver has to
 * clear the status by ifself.
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_LLD_InterruptSource_TransferComplete                = 0x80000000 ,       //!< All data has been sent/received, transmission is finished without error
    oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived          = 0x40000000 ,       //!< The slave did not answer with the ACK flag - probably device with the given address does not exist
    oC_I2C_LLD_InterruptSource_ArbitrationLost                 = 0x20000000 ,       //!< Arbitration lost error (handling of this error in LLD is not very important for the workflow)
    oC_I2C_LLD_InterruptSource_BusError                        = 0x10000000 ,       //!< Bus error (handling of this error in LLD also is not very important for the workflow)
    oC_I2C_LLD_InterruptSource_EarlyStopDetected               = 0x08000000 ,       //!< Transmission has been stopped early - probably not all data (or none of them) was sent.
    oC_I2C_LLD_InterruptSource_DataReadyToReceive              = 0x04000000 ,       //!< Some data in the RX buffer waits to be read by the main driver. Handling of this event is very important to provide work of the I2C driver. Remember to call interrupt handler with this event inactive, when there is no more data to receive
    oC_I2C_LLD_InterruptSource_ReadyToSendNewData              = 0x02000000 ,       //!< The LLD is ready to get new data for sending. Handling of this event is also very important. Just like in the previous event, you have to call interrupt handler with this event inactive, when LLD TX buffer is full.
    oC_I2C_LLD_InterruptSource_AddressMatched                  = 0x01000000 ,       //!< (slave only) LLD should activate this event if own address has been matched by the master
    oC_I2C_LLD_InterruptSource_Overrun                         = 0x00800000 ,       //!< Overrun error (handling of this error in LLD is not very important for the workflow)
    oC_I2C_LLD_InterruptSource_Underrun                        = 0x00400000 ,       //!< Underrun error (handling of this error in LLD is not very important for the workflow)
    oC_I2C_LLD_InterruptSource_Timeout                         = 0x00200000 ,       //!< Timeout error (handling of this error in LLD is not very important for the workflow)
    oC_I2C_LLD_InterruptSource_PecError                        = 0x00100000 ,       //!< PEC error (handling of this error in LLD is not very important for the workflow)
    oC_I2C_LLD_InterruptSource_SmbusAlert                      = 0x00080000 ,       //!< SMBus error (handling of this error in LLD is not very important for the workflow)
    oC_I2C_LLD_InterruptSource_NoMoreDataToSend                = 0x00040000 ,       //!< The event should be activated, when LLD sent all data from the TX buffer. It is very important, specially for handling I2C registers, that LLD should call interrupt handler with this event inactive, when TX buffer is not empty

    oC_I2C_LLD_InterruptSource_TransferErrors                  = oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived  //!< The mask with all possible transmission errors bits set
                                                               | oC_I2C_LLD_InterruptSource_ArbitrationLost
                                                               | oC_I2C_LLD_InterruptSource_BusError
                                                               | oC_I2C_LLD_InterruptSource_EarlyStopDetected
                                                               | oC_I2C_LLD_InterruptSource_PecError
                                                               | oC_I2C_LLD_InterruptSource_Overrun
                                                               | oC_I2C_LLD_InterruptSource_Underrun
                                                               | oC_I2C_LLD_InterruptSource_Timeout ,
} oC_I2C_LLD_InterruptSource_t;

//==========================================================================================================================================
/**
 * @brief stores pointer to the interrupt handler
 *
 * This is the type for storing pointer to the interrupt handler function. LLD use it to inform main driver about pending events.
 *
 * @param Channel           Channel of I2C where the event occurs
 * @param ActiveEvents      List of new events about which LLD wants to inform
 * @param InactiveEvents    List of events that are no more active (does not apply to Transfer Errors). Please look at #oC_I2C_LLD_InterruptSource_t for more
 *
 * Look to #oC_I2C_LLD_InterruptSource_t for more information about events.
 */
//==========================================================================================================================================
typedef void (*oC_I2C_LLD_InterruptHandler_t)( oC_I2C_Channel_t Channel , oC_I2C_LLD_InterruptSource_t ActiveEvents , oC_I2C_LLD_InterruptSource_t InactiveEvents );

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each I2C channel
 *
 * The macro is for creating a loop for each channel defined in the machine. As 'index' there is a variable that stores the channel ID.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * // example of loop that restores default states on each I2C channel defined in the machine
 * oC_I2C_LLD_ForEachChannel(Channel , ChannelIndex)
 * {
 *      // this will be executed for each channel
 *      oC_I2C_LLD_RestoreDefaultStateOnChannel( Channel );
 * }
 * ~~~~~~~~~~~~
 *
 */
//==========================================================================================================================================
#define oC_I2C_LLD_ForEachChannel( Channel , ChannelIndex )           oC_Channel_Foreach( I2C , Channel )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup I2C-LLD
//! @{


//==========================================================================================================================================
/**
 * @brief checks if the I2C channel is correct
 *
 * Checks if the I2C channel is correct.
 *
 * @param Channel       Channel of the I2C to check
 *
 * @return true if channel is correct
 */
//==========================================================================================================================================
extern bool oC_I2C_LLD_IsChannelCorrect( oC_I2C_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief check if the channel index is correct
 *
 * Checks if the I2C channel index is correct.
 *
 * @param ChannelIndex  Index of the channel in module
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_I2C_LLD_IsChannelIndexCorrect( oC_I2C_LLD_ChannelIndex_t ChannelIndex );
//==========================================================================================================================================
/**
 * @brief converts channel to channel index
 *
 * The function converts channel to index of the channel in the I2C module
 *
 * @param Channel       Channel of the I2C
 *
 * @return index of the channel in the I2C
 */
//==========================================================================================================================================
extern oC_I2C_LLD_ChannelIndex_t oC_I2C_LLD_ChannelToChannelIndex( oC_I2C_Channel_t Channel );

//==========================================================================================================================================
/**
 * @brief reads array of modules pins
 *
 * The function reads list of module pins for the given pin. Thanks to that, you can find a free I2C channel that is connected with the given pins.
 *
 * @param Pin                   I2C pin to find
 * @param outModulePinsArray    Destination for the I2C module pins (or #NULL if only `ArraySize` is required)
 * @param ArraySize             Size of the output array
 * @param PinFunction           I2C pin function to find module pins
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *  oC_ErrorCode_PinNotDefined    | The given `Pin` is not correct
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_I2C_LLD_ReadModulePinsOfPin( oC_Pin_t Pin , oC_I2C_Pin_t * outModulePinsArray , uint32_t * ArraySize , oC_I2C_PinFunction_t PinFunction );

//==========================================================================================================================================
/**
 * @brief converts channel index to channel
 *
 * The function converts index of the channel to channel in the I2C module
 *
 * @param Channel       Channel of the I2C
 *
 * @return index of the channel in the I2C
 */
//==========================================================================================================================================
extern oC_I2C_Channel_t oC_I2C_LLD_ChannelIndexToChannel( oC_I2C_LLD_ChannelIndex_t Channel );
//==========================================================================================================================================
/**
 * @brief returns channel of peripheral pin
 *
 * The function reads channel of peripheral pin. It not checks if the channel is correct, use #oC_I2C_LLD_IsChannelCorrect function to check
 * if returned channel is correct.
 *
 * @param ModulePin         special pin that is connected to I2C channel
 *
 * @return channel of I2C (it can be not correct)
 */
//==========================================================================================================================================
extern oC_I2C_Channel_t oC_I2C_LLD_GetChannelOfModulePin( oC_I2C_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_I2C_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                | Description
 * -------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None             | Operation success
 *  oC_ErrorCode_NotImplement     | Function is already not implemented
 *  oC_ErrorCode_ImplementError   | There was unexpected error in implementation
 *                                |
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_I2C_LLD_TurnOffDriver( void );

//==========================================================================================================================================
/**
 * @brief checks if the channel is used
 *
 * The function should check if the given I2C channel is used. Actually it is not important to implement this function in new architectures,
 * as the main driver already controls usage of the channels, but for additional protection it can be done also in LLD.
 *
 * @note
 * Remember, that if you don't plan to implement this, it should return false
 *
 * @param Channel           Channel of the I2C to check
 *
 * @return true if channel is used
 */
//==========================================================================================================================================
extern bool oC_I2C_LLD_IsChannelUsed( oC_I2C_Channel_t Channel );

//==========================================================================================================================================
/**
 * @brief configures I2C channel to work as a master
 *
 *
 *
 * @param Channel
 * @param SpeedMode
 * @param AddressMode
 * @param DataHoldTime
 * @param DataSetupTime
 * @param ClockLowTime
 * @param ClockHighTime
 * @param ClockFrequency
 * @param EnabledInterrupts
 * @return
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_I2C_LLD_ConfigureAsMaster(
    oC_I2C_Channel_t                Channel ,
    oC_I2C_LLD_SpeedMode_t          SpeedMode ,
    oC_I2C_LLD_AddressMode_t        AddressMode ,
    oC_Time_t                       DataHoldTime ,
    oC_Time_t                       DataSetupTime ,
    oC_Time_t                       ClockLowTime ,
    oC_Time_t                       ClockHighTime ,
    oC_Frequency_t                  ClockFrequency ,
    oC_I2C_LLD_InterruptSource_t    EnabledInterrupts
    );

extern oC_ErrorCode_t oC_I2C_LLD_ConfigureAsSlave(
    oC_I2C_Channel_t                Channel ,
    oC_I2C_LLD_SpeedMode_t          SpeedMode ,
    oC_I2C_LLD_AddressMode_t        AddressMode ,
    oC_I2C_LLD_Address_t            OwnAddress ,
    bool                            GeneralCallEnabled ,
    bool                            ClockStretchingEnabled,
    oC_I2C_LLD_InterruptSource_t    EnabledInterrupts
    );

extern oC_ErrorCode_t oC_I2C_LLD_UnconfigureChannel( oC_I2C_Channel_t Channel );

extern oC_ErrorCode_t oC_I2C_LLD_StartAsMaster( oC_I2C_Channel_t Channel, bool StartForRead, oC_I2C_LLD_Address_t SlaveAddress, oC_MemorySize_t Size );
extern oC_ErrorCode_t oC_I2C_LLD_StopAsMaster ( oC_I2C_Channel_t Channel );
extern oC_ErrorCode_t oC_I2C_LLD_WriteAsMaster( oC_I2C_Channel_t Channel, const uint8_t * Data, oC_MemorySize_t * Size );
extern oC_ErrorCode_t oC_I2C_LLD_ReadAsMaster ( oC_I2C_Channel_t Channel, uint8_t *    outData, oC_MemorySize_t * Size );
extern oC_ErrorCode_t oC_I2C_LLD_WriteAsSlave ( oC_I2C_Channel_t Channel, const uint8_t * Data, oC_MemorySize_t * Size );
extern oC_ErrorCode_t oC_I2C_LLD_ReadAsSlave  ( oC_I2C_Channel_t Channel, uint8_t *    outData, oC_MemorySize_t * Size );

//==========================================================================================================================================
/**
 * @brief sets interrupt handler
 *
 * The function is for setting pointer to the function that should be called when an interrupt occurs. It can be called only once after module
 * enable. If the interrupt handler is already set, the function will return an error
 *
 * @param InterruptHandler          Pointer to the function that should be called when the interrupt occurs
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None                       | Operation success
 *  oC_ErrorCode_NotImplement               | Function is already not implemented
 *  oC_ErrorCode_ImplementError             | There was unexpected error in implementation
 *  oC_ErrorCode_WrongAddress               | Address of the handler is not correct
 *  oC_ErrorCode_InterruptHandlerAlreadySet | The handler for interrupts is already set
 *  oC_ErrorCode_ModuleNotStartedYet        | The module is not turned on
 *
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_I2C_LLD_SetInterruptHandler( oC_I2C_LLD_InterruptHandler_t Handler );

//==========================================================================================================================================
/**
 * @brief connects module pin into the I2C channel
 *
 * The function allows for connecting the given I2C module pin into the I2C channel.
 *
 * @param ModulePin     I2C module pin to connect
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *
 *      Error Code                      | Description
 * -------------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None                   | Operation success
 *  oC_ErrorCode_NotImplement           | Function is already not implemented
 *  oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  oC_ErrorCode_ModuleNotStartedYet    | The module has not been turned on
 *  oC_ErrorCode_PinNotDefined          | The given bin is not defined
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_I2C_LLD_ConnectModulePin( oC_I2C_Pin_t ModulePin );

//==========================================================================================================================================
/**
 * @brief disconnects module pin into the I2C channel
 *
 * The function allows for disconnecting the given I2C module pin from the I2C channel.
 *
 * @param ModulePin     I2C module pin to disconnect
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *
 *      Error Code                      | Description
 * -------------------------------------|-----------------------------------------------------
 *  oC_ErrorCode_None                   | Operation success
 *  oC_ErrorCode_NotImplement           | Function is already not implemented
 *  oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  oC_ErrorCode_ModuleNotStartedYet    | The module has not been turned on
 *  oC_ErrorCode_PinNotDefined          | The given bin is not defined
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_I2C_LLD_DisconnectModulePin( oC_I2C_Pin_t ModulePin );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* _OC_I2C_LLD_H */
#endif
#endif
