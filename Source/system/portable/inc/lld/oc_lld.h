/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for LLD layer
 *
 * @file       oc_lld.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_LLD_OC_LLD_H_
#define SYSTEM_PORTABLE_INC_LLD_OC_LLD_H_

#include <oc_1word.h>
#include <oc_compiler.h>

#define oC_LLD_ConnectToMainDriver(DRIVER_NAME,FunctionSufix)    oC_FUNCTION_REDEFINITION( oC_1WORD_FROM_4(oC_,DRIVER_NAME,_,FunctionSufix), oC_1WORD_FROM_4(oC_,DRIVER_NAME,_LLD_,FunctionSufix))

#endif /* SYSTEM_PORTABLE_INC_LLD_OC_LLD_H_ */
