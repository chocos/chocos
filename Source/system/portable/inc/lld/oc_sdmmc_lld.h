/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the SDMMC driver
 *
 * @file       oc_sdmmc_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 2017-08-07 - 22:44:51)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SDMMC-LLD SDMMC 
 * @ingroup LowLevelDrivers
 * @ingroup SDMMC
 *
 * @brief Handles SD/MMC cards transmissions in target machine
 *
 * @par
 * The driver is for handling SD/MMC card communication. It should be turned on before usage by function #oC_SDMMC_LLD_TurnOnDriver,
 * and it should be turned off by #oC_SDMMC_LLD_TurnOffDriver when it is not needed anymore. The driver provide an interface for accessing
 * SD/MMC cards by using embedded hardware peripheral of the target.
 *
 * @par
 * After enabling of the module interrupts should be configured by the function #oC_SDMMC_LLD_SetInterruptHandler. To get to know which interrupts
 * you should enable, please check the type #oC_SDMMC_LLD_InterruptSource_t. The handler given to the function will be called every time, when
 * one of the enabled interrupts occurs.
 *
 * @note
 * The module does not check if the card is inserted, so you have to control it on your own. Otherwise, behavior of the module is unexpected.
 *
 * @par
 * To send a command to the card, you have to use function #oC_SDMMC_LLD_SendCommand. You can do it in pulling mode or by using interrupts.
 * Here you have example of sending a command in pulling mode:
 * @code{.c}
   // The function sends a command in pulling mode
   static oC_ErrorCode_t SendCommand( oC_SDMMC_Channel_t Channel, oC_SDMMC_LLD_CommandIndex_t CommandIndex , uint32_t Argument )
   {
       oC_ErrorCode_t         errorCode = oC_ErrorCode_ImplementError;
       oC_SDMMC_LLD_Command_t command;

       command.CommandIndex     = CommandIndex;
       command.Argument         = Argument;
       command.SuspendCommand   = false;
       command.ResponseType     = oC_SDMMC_LLD_ResponseType_None;

       // wait for previous transmission finish
       while( oC_SDMMC_LLD_SendCommand( Channel, NULL ) == oC_ErrorCode_CommandTransmissionInProgress );

       // Sending the package
       if( ErrorCode( oC_SDMMC_LLD_SendCommand( Channel, &command ) ) )
       {
          errorCode = oC_ErrorCode_None;
       }

       return errorCode;
   }
   @endcode
 * And here you have an example by using interrupts mode:
 * @code{.c}
   static oC_Semaphore_t CommandSentSemaphore = NULL;

   static void InterruptHandler( oC_SDMMC_Channel_t Channel, oC_SDMMC_LLD_InterruptSource_t InterruptSource )
   {
       if( InterruptSource & oC_SDMMC_LLD_InterruptSource_CommandSent )
       {
           oC_Semaphore_Give( &CommandSentSemaphore );
       }
   }

   void main( void )
   {
       printf( "Turning on the SDMMC module: %R\n", oC_SDMMC_LLD_TurnOnDriver() );
       printf( "Setting interrupt handler: %R\n", oC_SDMMC_LLD_SetInterruptHandler( InterruptHandler, oC_SDMMC_LLD_InterruptSource_CommandSent ) );

       CommandSentSemaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary, 1, getcurallocator(), 0 );

       // Here also should be a driver configuration, connecting pins and searching for a SDMMC channel
       ...

       printf( "Semaphore created: %p\n", CommandSentSemaphore );
       printf( "Sending a command: %R\n", SendCommand( Channel, 20, 0, s(5) ) );
   }

   ...

   // The function sends a command in interrupts mode
   static oC_ErrorCode_t SendCommand( oC_SDMMC_Channel_t Channel, oC_SDMMC_LLD_CommandIndex_t CommandIndex , uint32_t Argument, oC_Time_t Timeout )
   {
       oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

       // Wait for previous transmission finish
       if( ErrorCondition( oC_Semaphore_Take( CommandSentSemaphore, Timeout ), oC_ErrorCode_Timeout ) )
       {
           oC_SDMMC_LLD_Command_t command;

           command.CommandIndex     = CommandIndex;
           command.Argument         = Argument;
           command.SuspendCommand   = false;
           command.ResponseType     = oC_SDMMC_LLD_ResponseType_None;

           // Sending the package
           if( ErrorCode( oC_SDMMC_LLD_SendCommand( Channel, &command ) ) )
           {
              errorCode = oC_ErrorCode_None;
           }
       }

       return errorCode;
   }
   @endcode
 *
 * @par
 * In the same way as sending a command you can use #oC_SDMMC_LLD_ReadCommandResponse and #oC_SDMMC_LLD_InterruptSource_CommandResponseReceived
 * to receive a response for the sent command. More details you can find in the function description.
 *
 * @par
 * Transfer of data from and to a card can be done with #oC_SDMMC_LLD_WriteDataPackage and #oC_SDMMC_LLD_ReadDataPackage. Both functions can
 * work in interrupts mode or in pulling mode. For more information please read the functions description
 *
 ******************************************************************************************************************************************/


#ifndef _OC_SDMMC_LLD_H
#define _OC_SDMMC_LLD_H

#include <oc_machine.h>
#include <oc_errors.h>
#include <oc_frequency.h>
#include <oc_datapackage.h>

#if (oC_Channel_IsModuleDefined(SDMMC) && oC_ModulePinFunctions_IsModuleDefined(SDMMC) && oC_ModulePin_IsModuleDefined(SDMMC)) || defined(DOXYGEN)
#define oC_SDMMC_LLD_AVAILABLE

#if oC_Channel_IsModuleDefined(SDMMC) == false && !defined(DOXYGEN)
#error SDMMC module channels are not defined
#elif oC_ModulePinFunctions_IsModuleDefined(SDMMC) == false && !defined(DOXYGEN)
#error  SDMMC module pin functions are not defined
#elif oC_ModulePin_IsModuleDefined(SDMMC) == false && !defined(DOXYGEN)
#error  SDMMC module pins are not defined
#else

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup SDMMC-LLD
//! @{
#define MODULE_NAME SDMMC

//==========================================================================================================================================
/**
 * @brief channel of the SDMMC
 *
 * The type stores channel of the SDMMC
 */
//==========================================================================================================================================
#if defined(DOXYGEN)
typedef uint32_t oC_SDMMC_Channel_t;
#endif
oC_ModuleChannel_DefineType;

//==========================================================================================================================================
/**
 * @brief function of peripheral pin
 *
 * The type stores functions of the peripheral pins
 */
//==========================================================================================================================================
#if defined(DOXYGEN)
typedef uint32_t oC_SDMMC_LLD_PinFunction_t;
#endif
oC_ModulePinFunction_DefineType;

//==========================================================================================================================================
/**
 * @brief pin of the SDMMC peripheral
 *
 * The type stores peripheral pin, where the SDMMC channel is connected.
 */
//==========================================================================================================================================
#if defined(DOXYGEN)
typedef uint32_t oC_SDMMC_Pin_t;
#endif
oC_ModulePin_DefineType;

//==========================================================================================================================================
/**
 * @brief stores index of channel in the SDMMC
 *
 * The type stores index of the channel in the SDMMC array
 */
//==========================================================================================================================================
typedef oC_ChannelIndex_t oC_SDMMC_LLD_ChannelIndex_t;

//==========================================================================================================================================
/**
 * @brief stores index of a SDMMC command
 *
 * The type is for storing SDMMC command index
 */
//==========================================================================================================================================
typedef uint8_t oC_SDMMC_LLD_CommandIndex_t;

//==========================================================================================================================================
/**
 * @brief stores command response type
 *
 * The type is for storing type of response for commands. In `SDMMC` you can expect short, long or no response.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_LLD_ResponseType_None ,          //!< No response expected - the module does not wait for the response
    oC_SDMMC_LLD_ResponseType_ShortResponse , //!< Short response expected - only 1 word of data is expected
    oC_SDMMC_LLD_ResponseType_LongResponse ,  //!< Long response expected - the module waits for 4 words of data
    oC_SDMMC_LLD_ResponseType_NumberOfElements//!< Special definition for storing number of defined response types. It has to be always at the end of the list
} oC_SDMMC_LLD_ResponseType_t;

//==========================================================================================================================================
/**
 * @brief stores source of interrupts
 *
 * The type is for storing source flags of interrupt. Flags can be joined, each flags is for different interrupt source.
 */
//==========================================================================================================================================
typedef enum
{
   oC_SDMMC_LLD_InterruptSource_None                        = 0      , //!< None interrupt source
   oC_SDMMC_LLD_InterruptSource_CommandSent                 = (1<<0) , //!< Interrupt is fired when the command has been sent
   oC_SDMMC_LLD_InterruptSource_CommandResponseReceived     = (1<<1) , //!< Interrupt is fired when the response for the command has been received
   oC_SDMMC_LLD_InterruptSource_DataReadyToReceive          = (1<<2) , //!< Interrupt is fired when Rx FIFO is not empty - some data waits to be read
   oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData          = (1<<3) , //!< Interrupt is fired when Tx FIFO is not full no more
   oC_SDMMC_LLD_InterruptSource_TransmissionError           = (1<<4) , //!< Interrupt is fired in case of data transmission error
   oC_SDMMC_LLD_InterruptSource_CommandTransmissionError    = (1<<5) , //!< Interrupt is fired in case of command transmission error
   oC_SDMMC_LLD_InterruptSource_TransmissionRestarted       = (1<<6) , //!< Interrupt is fired when the driver should clear transmission data, because a new one has been started
   oC_SDMMC_LLD_InterruptSource_TransmissionFinished        = (1<<7) , //!< Interrupt is fired when the transmission is finished
} oC_SDMMC_LLD_InterruptSource_t;

//==========================================================================================================================================
/**
 * @brief Stores pointer to the interrupt handler
 *
 * The type is for storing pointer to the function, that should be called when a SDMMC interrupt occurs.
 *
 * @param Channel           `SDMMC` channel affected by the interrupt
 * @param InterruptSource   Variable with interrupt source flags
 */
//==========================================================================================================================================
typedef void (*oC_SDMMC_LLD_InterruptHandler_t)( oC_SDMMC_Channel_t Channel , oC_SDMMC_LLD_InterruptSource_t InterruptSource );

//==========================================================================================================================================
/**
 * @brief stores SDMMC command
 *
 * The type is for storing `SDMMC` command to send
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_LLD_CommandIndex_t     CommandIndex;   //!< Index of the command
    uint32_t                        Argument;       //!< Argument for the command
    bool                            SuspendCommand; //!< If it is set, the command to be sent is a suspend command (to be used only with SDIO card)
    oC_SDMMC_LLD_ResponseType_t     ResponseType;   //!< Type of the expected response for the command
} oC_SDMMC_LLD_Command_t;

//==========================================================================================================================================
/**
 * @brief stores response for the SDMMC command
 *
 * The type is for storing response of the command
 */
//==========================================================================================================================================
typedef struct
{
     oC_SDMMC_LLD_CommandIndex_t    CommandIndex;   //!< Index of the command
     uint32_t                       Response[4];    //!< Array with data of the response
} oC_SDMMC_LLD_CommandResponse_t;

//==========================================================================================================================================
/**
 * @brief stores transmission mode
 *
 * The type is for storing wide of the bus - transmission mode.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_LLD_WideBus_1Bit ,             //!< Only D0 is used - default bus mode
    oC_SDMMC_LLD_WideBus_4Bit ,             //!< D[0:3] is used
    oC_SDMMC_LLD_WideBus_8Bit ,             //!< D[0:7] is used
    oC_SDMMC_LLD_WideBus_NumberOfElements   //!< Number of elements defined in the type
} oC_SDMMC_LLD_WideBus_t;

//==========================================================================================================================================
/**
 * @brief Enumerator for specifying the direction of data transfer in SD/MMC communication
 *
 * This enumerator is used to specify the direction of data transfer when communicating with an SD/MMC card.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_LLD_TransferDirection_Read,        //!< Indicates that data is being read from the card.
    oC_SDMMC_LLD_TransferDirection_Write,       //!< Indicates that data is being written to the card.
    oC_SDMMC_LLD_TransferDirection_NumberOfElements
} oC_SDMMC_LLD_TransferDirection_t;

//==========================================================================================================================================
/**
 *
 * @brief Enumerator for specifying the mode of data transfer in SD/MMC communication
 *
 * This enumerator is used to specify the mode of data transfer when communicating with an SD/MMC card.
 *
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_LLD_TransferMode_BlockDataTransfer,    //!< Indicates that data is transferred in blocks.
    oC_SDMMC_LLD_TransferMode_Stream,               //!< Stream or SDIO multibyte data transfer
    oC_SDMMC_LLD_TransferMode_NumberOfElements      //!< Indicates that data is transferred in a stream or SDIO multibyte format.
} oC_SDMMC_LLD_TransferMode_t;

//==========================================================================================================================================
/**
 * @brief Stores SDMMC LLD configuration
 *
 * The type is for storing SDMMC LLD configuration.
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_LLD_WideBus_t  WideBus;    //!< Wide of the bus - transmission mode
    oC_MemorySize_t         SectorSize; //!< Size of the sector (block) in bytes
    oC_SDMMC_Channel_t      Channel;    //!< Channel of the SDMMC
} oC_SDMMC_LLD_Config_t;

//==========================================================================================================================================
/**
 * @brief Structure holding the configuration for SD/MMC data transfer
 */
//==========================================================================================================================================
typedef struct
{
    oC_Time_t                           Timeout;            //!< Timeout for the data transfer operation
    oC_MemorySize_t                     DataSize;           //!< Size of the data to be transferred
    oC_MemorySize_t                     BlockSize;          //!< Size of the block
    oC_SDMMC_LLD_TransferDirection_t    TransferDirection;  //!< Direction of data transfer (read/write)
    oC_SDMMC_LLD_TransferMode_t         TransferMode;       //!< Mode of data transfer (block/stream)
    bool                                UseDma;             //!< Flag indicating whether DMA should be used for data transfer
} oC_SDMMC_LLD_TransferConfig_t;

#undef MODULE_NAME
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each SDMMC channel
 *
 * The macro is for creating a loop for each channel defined in the machine. As 'index' there is a variable that stores the channel ID.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * // example of loop that restores default states on each SDMMC channel defined in the machine
 * oC_SDMMC_LLD_ForEachChannel(Channel , ChannelIndex)
 * {
 *      // this will be executed for each channel
 *      oC_SDMMC_LLD_RestoreDefaultStateOnChannel( Channel );
 * }
 * ~~~~~~~~~~~~
 *
 */
//==========================================================================================================================================
#define oC_SDMMC_LLD_ForEachChannel( Channel )           oC_Channel_Foreach( SDMMC , Channel )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup SDMMC-LLD
//! @{


//==========================================================================================================================================
/**
 * @brief checks if the SDMMC channel is correct
 *
 * Checks if the SDMMC channel is correct.
 *
 * @param Channel       Channel of the SDMMC to check
 *
 * @return true if channel is correct
 */
//==========================================================================================================================================
extern bool oC_SDMMC_LLD_IsChannelCorrect( oC_SDMMC_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief check if the channel index is correct
 *
 * Checks if the SDMMC channel index is correct.
 *
 * @param ChannelIndex  Index of the channel in module
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern bool oC_SDMMC_LLD_IsChannelIndexCorrect( oC_SDMMC_LLD_ChannelIndex_t ChannelIndex );
//==========================================================================================================================================
/**
 * @brief converts channel to channel index
 *
 * The function converts channel to index of the channel in the SDMMC module
 *
 * @param Channel       Channel of the SDMMC
 *
 * @return index of the channel in the SDMMC
 */
//==========================================================================================================================================
extern oC_SDMMC_LLD_ChannelIndex_t oC_SDMMC_LLD_ChannelToChannelIndex( oC_SDMMC_Channel_t Channel );
//==========================================================================================================================================
/**
 * @brief converts channel index to channel
 *
 * The function converts index of the channel to channel in the SDMMC module
 *
 * @param Channel       Channel of the SDMMC
 *
 * @return index of the channel in the SDMMC
 */
//==========================================================================================================================================
extern oC_SDMMC_Channel_t oC_SDMMC_LLD_ChannelIndexToChannel( oC_SDMMC_LLD_ChannelIndex_t Channel );
//==========================================================================================================================================
/**
 * @brief returns channel of peripheral pin
 *
 * The function reads channel of peripheral pin. It not checks if the channel is correct, use #oC_SDMMC_LLD_IsChannelCorrect function to check
 * if returned channel is correct.
 *
 * @param ModulePin         special pin that is connected to SDMMC channel
 *
 * @return channel of SDMMC (it can be not correct)
 */
//==========================================================================================================================================
extern oC_SDMMC_Channel_t oC_SDMMC_LLD_GetChannelOfModulePin( oC_SDMMC_Pin_t ModulePin );
//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | Function is already not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleIsTurnedOn       | The given module has been already started
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | Function is already not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | The given module has not started yet
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief allow to turn on/off register map
 *
 * The function is for manage power state on the SDMMC channel. It allow to turn on/off register map. Note, that if register map is turned off
 * then none of operations can be performed on the channel. It is required to turn on the power before any configurations on the channel.
 *
 * @param Channel   one of available `SDMMC` channels in the machine
 * @param Power     power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | Function is already not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel           | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_PowerStateNotCorrect   | The given `Power` is not #oC_Power_On or #oC_Power_Off
 *  #oC_ErrorCode_CannotEnableChannel    | The given channel cannot be enabled (reason is unknown)
 *  #oC_ErrorCode_CannotEnableInterrupt  | Cannot enable channel interrupt
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_SetPower( oC_SDMMC_Channel_t Channel , oC_Power_t Power );
//==========================================================================================================================================
/**
 * @brief read current power state
 *
 * The function is for reading power state of the register map on the selected channel. Channel should be powered on before configuration
 * and any other operations on it. Powering off the channel allow to save power. Each channel should be powered off when the driver is powered
 * off.
 *
 * @param Channel   one of available `SDMMC` channels in the machine
 * @param outPower  destination for the power state
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                      | Description
 * -------------------------------------|-----------------------------------------------------
 *  #oC_ErrorCode_None                  | Operation success
 *  #oC_ErrorCode_NotImplemented        | Function is already not implemented
 *  #oC_ErrorCode_ImplementError        | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet   | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel          | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_OutputAddressNotInRAM | `outPower` does not point to the RAM section
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_ReadPower( oC_SDMMC_Channel_t Channel , oC_Power_t * outPower );

//==========================================================================================================================================
/**
 * @brief Sets address of the interrupt handler
 *
 * The function is responsible for setting handler of the interrupt. It should be called once after enabling the module. When the interrupt
 * handler is set, the module calls it with set of flags that represents sources of the interrupt. The module has to enabled before usage of
 * the function.
 *
 * @param Handler               Pointer to the function to call, when an interrupt occurs
 * @param EnabledSources        Flags with interrupts that should be enabled
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                            | Description
 * -------------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                        | Operation success
 *  #oC_ErrorCode_NotImplemented              | Function is already not implemented
 *  #oC_ErrorCode_ImplementError              | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet         | The given module has not started yet
 *  #oC_ErrorCode_WrongEventHandlerAddress    | Address of the handler is not correct
 *  #oC_ErrorCode_InterruptHandlerAlreadySet  | Interrupt handler has been already set. To change the handler you have to restart the module.
 *  #oC_ErrorCode_UnknownInterruptSource      | At least one of the interrupt source flags is unknown on the target
 *  #oC_ErrorCode_NotSupportedOnTargetMachine | At least one of the interrupt source flags is not supported and cannot be enabled
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_SetInterruptHandler( oC_SDMMC_LLD_InterruptHandler_t Handler, oC_SDMMC_LLD_InterruptSource_t EnabledSources );
//==========================================================================================================================================
/**
 * @brief configures SDMMC LLD channel
 *
 * The function is for configuration of `SDMMC LLD`. The module has to be turned on with the function #oC_SDMMC_LLD_TurnOnDriver. Remember
 * that after or before the configuration it is also required to call #oC_SDMMC_LLD_ConfigurePin to configure all module pins. Without the pins
 * configuration, the module will not work. The function is implemented in the critical section.
 *
 * @warning
 * `SDDMC LLD` does not handle reservation of channels, so it has to be done in the `SDMMC` main driver.
 *
 * To restore default state on the channel when configuration is not needed anymore, please use #oC_SDMMC_LLD_Unconfigure
 *
 * @param Config                Pointer to the structure with configuration of the driver
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongAddress              | Address of the configuration structure is not correct
 *  #oC_ErrorCode_SizeNotCorrect            | `Config->SectorSize` is not correct (it cannot be 0!)
 *  #oC_ErrorCode_SizeNotSupported          | `Config->SectorSize` is not supported by the target
 *  #oC_ErrorCode_WrongChannel              | The given `Config->Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_WideBusNotCorrect         | The given `Config->WideBus` is not correct (it stores value bigger than MAX)
 *  #oC_ErrorCode_WideBusNotSupported       | The given `Config->WideBus` is correct, but not supported by the target
 *
 * @note
 * More error codes can be returned by function #oC_SDMMC_LLD_SetPower
 *
 * @see oC_SDMMC_LLD_SetPower, oC_SDMMC_LLD_Unconfigure, oC_SDMMC_LLD_ConfigurePin
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_Configure( const oC_SDMMC_LLD_Config_t * Config );

//==========================================================================================================================================
/**
 * @brief unconfigures SDMMC LLD
 *
 * The function is for reverting configuration of `SDMMC LLD` previously done with #oC_SDMMC_LLD_Configure. The module has to be turned
 * on with the function #oC_SDMMC_LLD_TurnOnDriver. Remember that after the unconfiguration it is also required to call
 * #oC_SDMMC_LLD_UnconfigurePin to disconnect all module pins from the `SDMMC`. The function is implemented in critical section.
 *
 * @param Channel   one of available `SDMMC` channels in the machine
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel              | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_ChannelNotConfigured      | The given `Channel` has not been configured by #oC_SDMMC_LLD_Configure before
 *  #oC_ErrorCode_CannotDisableChannel      | Cannot restore default state on the given channel
 *
 * @note
 * More error codes can be returned by function #oC_SDMMC_LLD_SetPower
 *
 * @see oC_SDMMC_LLD_SetPower, oC_SDMMC_LLD_Configure, oC_SDMMC_LLD_UnconfigurePin
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_Unconfigure( oC_SDMMC_Channel_t Channel );

//==========================================================================================================================================
/**
 * @brief configures the given pin to work with the SDMMC driver
 *
 * The function is for configuration of a `GPIO` pin to work with the `SDMMC` driver on the given `Channel` and with the given `PinFunction`.
 * Function #oC_SDMMC_LLD_IsPinAvailable should be used for finding a channel for the configuration. Remember, that the module has to be turned
 * on by function #oC_SDMMC_LLD_TurnOnDriver and that it is also required to configure the channel with #oC_SDMMC_LLD_Configure. The function
 * is implemented in critical section.
 *
 * @note
 * The function also marks the given `GPIO` pin as used!
 *
 * @param Channel           one of available `SDMMC` channels in the machine
 * @param Pin               `GPIO` pin to configure
 * @param PinFunction       `SDMMC` pin function to use for the configuration
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel              | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_NotSinglePin              | The given `Pin` is not single pin (definition contains more than 1 `GPIO` pin)
 *  #oC_ErrorCode_PinNotDefined             | The given `Pin` is not defined in the target machine
 *  #oC_ErrorCode_PinIsUsed                 | The given `Pin` has been already used before and cannot be reconfigured
 *  #oC_ErrorCode_NoneOfModulePinAssigned   | None of module pins in the target has been assigned to the given parameters
 *  #oC_ErrorCode_PinFunctionNotCorrect     | The given `PinFunction` is not correct `SDMMC` pin function
 *
 * More error codes can be returned by: #oC_GPIO_LLD_ArePinsUnused, #oC_GPIO_LLD_SetPinsUsed, #oC_GPIO_LLD_SetSpeed,
 * #oC_GPIO_LLD_SetOutputCircuit, #oC_GPIO_LLD_SetPull
 *
 *  @see oC_SDMMC_LLD_UnconfigurePin, oC_SDMMC_LLD_IsPinAvailable
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_ConfigurePin( oC_SDMMC_Channel_t Channel , oC_Pin_t Pin , oC_SDMMC_PinFunction_t PinFunction );

//==========================================================================================================================================
/**
 * @brief Checks if the given pin can work with the given function for the SDMMC driver
 *
 * The function is for checking if the given `Pin` can work with `PinFunction` for the `SDMMC Channel`. It should be used before #oC_SDMMC_LLD_ConfigurePin
 * and #oC_SDMMC_LLD_Configure to find a `SDMMC` channel for configuration. The function is implemented in critical section.
 *
 * @warning
 * The `SDMMC LLD` module has to be enabled by #oC_SDMMC_LLD_TurnOnDriver before usage of the function, otherwise it always returns `false`!
 *
 * @param Channel           one of available `SDMMC` channels in the machine
 * @param Pin               `GPIO` pin to configure
 * @param PinFunction       `SDMMC` pin function to use for the configuration
 *
 * @return true if pin is available and can be configured as `SDMMC` pin
 *
 * @see oC_SDMMC_LLD_ConfigurePin, oC_SDMMC_LLD_Configure
 */
//==========================================================================================================================================
extern bool oC_SDMMC_LLD_IsPinAvailable( oC_SDMMC_Channel_t Channel , oC_Pin_t Pin , oC_SDMMC_PinFunction_t PinFunction );

//==========================================================================================================================================
/**
 * @brief disconnects pin to stop working with the `SDMMC` driver
 *
 * The function is for unconfiguration of the pin previously configured with the function #oC_SDMMC_LLD_ConfigurePin. The module has to be
 * turned on by function #oC_SDMMC_LLD_TurnOnDriver. The function is implemented in critical section.
 *
 * @warning
 * The function does not check if the pin was configured with the function #oC_SDMMC_LLD_ConfigurePin or if it is connected to the other module,
 * so there is a risk, that the function can unconfigures pin from the other module.
 *
 * @param Pin               `GPIO` pin to unconfigure
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel              | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_NotSinglePin              | The given `Pin` is not single pin (definition contains more than 1 `GPIO` pin)
 *  #oC_ErrorCode_PinNotDefined             | The given `Pin` is not defined in the target machine
 *  #oC_ErrorCode_PinIsNotUsed              | The given `Pin` is not used (it wasn't configured yet)
 *
 * More error codes can be returned by functions #oC_GPIO_LLD_BeginConfiguration, #oC_GPIO_LLD_SetMode, #oC_GPIO_LLD_FinishConfiguration, #oC_GPIO_LLD_SetPinsUnused
 *
 * @see oC_SDMMC_LLD_ConfigurePin, oC_SDMMC_LLD_Configure
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_UnconfigurePin( oC_Pin_t Pin );

//==========================================================================================================================================
/**
 * @brief sets transmission mode
 *
 * The function configures wide bus (transmission mode) parameter.
 *
 * By default the card is communicating on 1-Byte mode, so it is required to change the transmission mode before initialization to 1-Byte and
 * to other modes after the initialization is finished
 *
 * @param Channel           one of available `SDMMC` channels in the machine
 * @param WideBus           wide bus to configure
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel              | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_WideBusNotCorrect         | The given `WideBus` is not correct (it stores value bigger than MAX)
 *  #oC_ErrorCode_WideBusNotSupported       | The given `WideBus` is correct, but not supported by the target
 *  #oC_ErrorCode_ChannelNotConfigured      | The given `Channel` has not been configured by using #oC_SDMMC_LLD_Configure function
 *
 * @see oC_SDMMC_LLD_WideBus_t
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_SetWideBus( oC_SDMMC_Channel_t Channel, oC_SDMMC_LLD_WideBus_t WideBus );

//==========================================================================================================================================
/**
 * @brief sends SDMMC command
 *
 * The function is for sending a command with `SDMMC` interface to the card. The function does not wait for a response, but only initiates the command
 * transmission.
 *
 * It uses interrupt source flags to inform the user about events. The user should expect the following interrupts:
 *
 *  Interrupt flag                                         | Interrupt description
 * --------------------------------------------------------|----------------------------------------------------------------------------------------
 *  #oC_SDMMC_LLD_InterruptSource_CommandSent              | Fired when the `SDMMC` command sending is finished (response is not ready yet)
 *  #oC_SDMMC_LLD_InterruptSource_CommandResponseReceived  | Fired when the response for a command can be read by #oC_SDMMC_LLD_ReadCommandResponse
 *  #oC_SDMMC_LLD_InterruptSource_CommandTransmissionError | Fired when the command cannot be send
 *
 * @note
 * Remember, that if you want to use interrupts mode, the handler has to be configured before by #oC_SDMMC_LLD_SetInterruptHandler
 * with #oC_SDMMC_LLD_InterruptSource_CommandSent and #oC_SDMMC_LLD_InterruptSource_CommandResponseReceived flags set, otherwise you are not
 * informed about finish of the transmission or response availability. If you cannot use interrupts, you can try to work in pulling mode by
 * requesting a response with function #oC_SDMMC_LLD_ReadCommandResponse
 *
 * The `SDMMC LLD` module has to be enabled by using function #oC_SDMMC_LLD_TurnOnDriver.
 *
 * @param Channel           one of available `SDMMC` channels in the machine
 * @param Command           Pointer to the structure with data of the command to send
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                              | Description
 * ---------------------------------------------|-------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                          | Operation success
 *  #oC_ErrorCode_NotImplemented                | Function is already not implemented
 *  #oC_ErrorCode_ImplementError                | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet           | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel                  | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_ChannelNotConfigured          | The given `Channel` has not been configured by #oC_SDMMC_LLD_Configure
 *  #oC_ErrorCode_WrongAddress                  | `Command` points to incorrect address
 *  #oC_ErrorCode_CommandNotCorrect             | `CommandIndex` in `Command` structure is too big
 *  #oC_ErrorCode_CommandTransmissionInProgress | Previous command transmission has not finished yet, please try again later
 *  #oC_ErrorCode_ResponseNotReceived           | Response for previous command has not been received yet
 *  #oC_ErrorCode_ResponseTypeNotCorrect        | `Command->ResponseType` is not correct
 *  #oC_ErrorCode_ResponseTypeNotSupported      | `Command->ResponseType` is not supported by the target
 *
 * @see oC_SDMMC_LLD_SetInterruptHandler, oC_SDMMC_LLD_ReadCommandResponse
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_SendCommand( oC_SDMMC_Channel_t Channel, const oC_SDMMC_LLD_Command_t * Command );

//==========================================================================================================================================
/**
 * @brief reads command response
 *
 * The function is for reading response for the command last sent by #oC_SDMMC_LLD_SendCommand. In **interrupt mode** it should be called
 * after interrupt with #oC_SDMMC_LLD_InterruptSource_CommandResponseReceived flag set.
 *
 * @param Channel           one of available `SDMMC` channels in the machine
 * @param outResponse       Pointer to the structure where command response should be stored
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel              | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_ChannelNotConfigured      | The given `Channel` has not been configured by #oC_SDMMC_LLD_Configure
 *  #oC_ErrorCode_OutputAddressNotInRAM     | `outResponse` does not point to the RAM section (address is not correct)
 *  #oC_ErrorCode_CommandNotSent            | No command has been sent by #oC_SDMMC_LLD_SendCommand or response has been already received
 *  #oC_ErrorCode_ResponseNotReady          | Response for a command is not ready yet
 *
 * @see oC_SDMMC_LLD_SendCommand
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_ReadCommandResponse( oC_SDMMC_Channel_t Channel , oC_SDMMC_LLD_CommandResponse_t * outResponse );

//==========================================================================================================================================
/**
 * @brief prepares the SD/MMC module for transfer of data
 *
 *
 * The function is responsible for preparation for a data transfer and should be called before #oC_SDMMC_LLD_ReadDataPackage or #oC_SDMMC_LLD_WriteDataPackage.
 *
 * @param Channel                       one of available `SDMMC` channels in the machine
 * @param TransferConfig                Pointer to the structure holding the configuration for data transfer
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                            | Description
 * -------------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                        | Operation success
 *  #oC_ErrorCode_NotImplemented              | Function is already not implemented
 *  #oC_ErrorCode_ImplementError              | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet         | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel                | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_WrongAddress                | `TransferConfig` address does not point to the ROM/RAM section
 *  #oC_ErrorCode_TimeNotCorrect              | `TransferConfig->Timeout` is below or equal to 0
 *  #oC_ErrorCode_SizeNotCorrect              | `TransferConfig->DataSize` is not given
 *  #oC_ErrorCode_SizeNotSupported            | `TransferConfig->DataSize` is too big
 *  #oC_ErrorCode_BlockSizeNotCorrect         | `TransferConfig->BlockSize` is not supported by the target
 *  #oC_ErrorCode_TransferDirectionNotCorrect | `TransferConfig->TransferDirection` is not valid
 *  #oC_ErrorCode_TransferModeNotCorrect      | `TransferConfig->TransferMode` is not valid
 *
 *  @see oC_SDMMC_LLD_SetInterruptHandler, oC_SDMMC_LLD_ReadDataPackage, oC_SDMMC_LLD_WriteDataPackage
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_PrepareForTransfer( oC_SDMMC_Channel_t Channel, const oC_SDMMC_LLD_TransferConfig_t* TransferConfig );

//==========================================================================================================================================
/**
 * @brief writes data to the FIFO
 *
 * The function is for writing data to the transmit FIFO (FIFO, software ring or DMA, depends on the implementation). The function reads from
 * the package and stores for transmission as many data as possible and updates the `DataPackage` state. The module has to be
 * turned on by function #oC_SDMMC_LLD_TurnOnDriver. The function is implemented in critical section.
 *
 * It uses interrupts to inform if more data can be put to the FIFO. The user should expect the following interrupts:
 *
 *  Interrupt flag                                         | Interrupt description
 * --------------------------------------------------------|----------------------------------------------------------------------------------------
 *  #oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData       | Fired when the module is ready to send new data after be full
 *  #oC_SDMMC_LLD_InterruptSource_TransmissionError        | Fired when some transmission error occurred
 *
 * @note
 * Remember, that if you want to use interrupts mode, the handler has to be configured before by #oC_SDMMC_LLD_SetInterruptHandler
 * with #oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData flag set, otherwise you are not informed about the events. If you cannot use interrupts,
 * you can try to write data package in pulling mode by calling #oC_SDMMC_LLD_WriteDataPackage in the loop and waiting for `DataPackage` to be empty
 *
 * @warning
 * The function does not return an error if the FIFO is full
 *
 * @param Channel           One of available `SDMMC` channels in the machine
 * @param DataPackage       Initialized package with data to write
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel              | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_AddressNotInRam           | `DataPackage` address does not point to the RAM section
 *  #oC_ErrorCode_DataPackageNotInitialized | `DataPackage` is not initialized or buffer address is not correct
 *  #oC_ErrorCode_DataPackageIsEmpty        | `DataPackage` is empty (there is no more data to read)
 *
 *  @see oC_SDMMC_LLD_SetInterruptHandler, oC_SDMMC_LLD_ReadDataPackage
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_WriteDataPackage( oC_SDMMC_Channel_t Channel, oC_DataPackage_t * DataPackage );

//==========================================================================================================================================
/**
 * @brief reads data from the FIFO
 *
 * The function is for reading data from the transmit FIFO (FIFO, software ring or DMA, depends on the implementation). The function reads from
 * the FIFO and stores to the package as many data as possible and updates the `DataPackage` state. The module has to be
 * turned on by function #oC_SDMMC_LLD_TurnOnDriver. The function is implemented in critical section.
 *
 * It uses interrupts to inform about new data to receive from FIFO. The user should expect the following interrupts:
 *
 *  Interrupt flag                                         | Interrupt description
 * --------------------------------------------------------|----------------------------------------------------------------------------------------
 *  #oC_SDMMC_LLD_InterruptSource_DataReadyToReceive       | Fired when data has been received and are ready to read
 *  #oC_SDMMC_LLD_InterruptSource_TransmissionError        | Fired when some transmission error occurred
 *
 * @note
 * Remember, that if you want to use interrupts mode, the handler has to be configured before by #oC_SDMMC_LLD_SetInterruptHandler
 * with #oC_SDMMC_LLD_InterruptSource_DataReadyToReceive flag set, otherwise you are not informed about the events. If you cannot use interrupts,
 * you can try to read data package in pulling mode by calling #oC_SDMMC_LLD_ReadDataPackage in the loop and waiting for `DataPackage` to be full
 *
 * @warning
 * In case of interrupts mode, you should call this function from the interrupt handler
 *
 * @param Channel           One of available `SDMMC` channels in the machine
 * @param DataPackage       Initialized package for received data (it cannot be read only)
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_NotImplemented            | Function is already not implemented
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet       | The given module has not started yet
 *  #oC_ErrorCode_WrongChannel              | The given `Channel` is not correct `SDMMC` channel
 *  #oC_ErrorCode_AddressNotInRam           | `DataPackage` address does not point to the RAM section
 *  #oC_ErrorCode_DataPackageNotInitialized | `DataPackage` is not initialized or buffer address is not correct
 *  #oC_ErrorCode_DataPackageIsFull         | `DataPackage` is full
 *  #oC_ErrorCode_DataPackageIsReadOnly     | `DataPackage` is initialized as read only
 *
 *  @see oC_SDMMC_LLD_SetInterruptHandler, oC_SDMMC_LLD_WriteDataPackage
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SDMMC_LLD_ReadDataPackage( oC_SDMMC_Channel_t Channel, oC_DataPackage_t * DataPackage );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}


#endif /* _OC_SDMMC_LLD_H */
#endif
#endif
