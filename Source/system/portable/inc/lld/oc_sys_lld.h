/** ****************************************************************************************************************************************
 *
 * @brief      The file with LLD interface for the SYS driver
 *
 * @file       oc_sys_lld.h
 *
 * @author     Patryk Kubiak - (Created on: 26 04 2015 21:55:10)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SYS-LLD System 
 * @ingroup LowLevelDrivers
 * @brief Manage system operations
 *
 * @par
 * The driver is for supporting operations that are specific for the system handling, such as switching context, turning on and off interrupts,
 * configuring system tick and so on.
 *
 * @par
 * The module must be turned on before usage by the function #oC_SYS_LLD_TurnOnDriver. When it is not needed anymore, the #oC_SYS_LLD_TurnOffDriver
 * allow to turn it off. It will restore the system task.
 *
 * @par
 * It support also an event system, that can be configured by #oC_SYS_LLD_SetEventInterruptHandler once after enabling the module. The SYS
 * allow for switching context. The context is the stack that was allocated for the thread for example. The size of required allocation can
 * be read by function #oC_SYS_LLD_GetMinimumContextSize. When memory for a process stack is allocated with correct size it must be initialized.
 * For initialization you will need to set pointer to the function, that will be executed during context handling. There is not any `kill`
 * thread function. If thread want to destroy by itself, it should just change current context and wait. On the other hand, when some other
 * thread want to kill a thread, there is no need to inform this module - if other thread want to kill, so other thread is currently active.
 *
 * @par
 * The module does not support any queue for tasks. It just help by simple way for task switching. The real task manager is implemented in the
 * kernel, not portable space, and it is common for all machines.
 *
 * @par
 * The first thing to use the **SYS-LLD** as a context switcher, is configuration of the system timer. It can be done by #oC_SYS_LLD_ConfigureSystemTimer.
 * It allow to set frequency of calling the SysTick handler function. Note, that this function should be called every time, when the system
 * clock is reconfigured - otherwise this frequency will be not updated. The SysTick handler function is the special function that allow to
 * select next task to execute. If it will return true, the context will be switched immediately. To select the next context, use the function
 * #oC_SYS_LLD_SetNextContext.
 *
 * Some example pseudo-code (do not use it - it is only for explanation):
 * @code{.c}
 *
 * //
 * //   This is the context handler, that will blink the led
 * //
 * void BlinkLed(void * Parameter)
 * {
 *      while(1)
 *      {
 *         TurnOnLed();
 *         oC_CLOCK_DelayForMicroseconds(500);
 *         TurnOffLed();
 *         oC_CLOCK_DelayForMicroseconds(500);
 *      }
 * }
 *
 * //
 * // Handler for context exit event
 * //
 * void BlinkLedStopped()
 * {
 *    // Some error occurs, and the BlinkLed function has been stopped!
 *    while(1);
 * }
 *
 * //
 * // Handler for the SysTick event. When it will return true, the context will be switched.
 * //
 * bool IncrementSystemTimer(void)
 * {
 *    return true;
 * }
 *
 * void main()
 * {
 *      // This will enable the SYS driver
 *      oC_SYS_LLD_TurnOnDriver();
 *
 *      // This will configure the timer to call the IncrementSystemTimer function periodically with frequency 10 kHz
 *      oC_SYS_LLD_ConfigureSystemTimer( oC_kHz(10) , IncrementSystemTimer );
 *
 *      // Allocation the stack for this context. We will reserve 128 bytes for execution of the BlinkLed function.
 *      oC_SYS_LLD_Context_t * context = malloc(oC_SYS_LLD_GetMinimumContextSize(128));
 *
 *      // Allocation of context by setting function to execute, size of the stack and exit handler function.
 *      if(oC_SYS_LLD_InitializeContext( context , 128 , BlinkLed , NULL , BlinkLedStopped))
 *      {
 *          // Setting the context as the next context to switched, when it will be possible (with the next call of the SysTick handler, that will return true).
 *          oC_SYS_LLD_SetNextContext( context );
 *      }
 *
 * }
 * @endcode
 *
 * @par
 * The **SYS-LLD** module supports controlling of stack overflow. It is controlled with frequency of system timer. So, when the system
 * timer is configured to work with 1 kHz frequency, the stack is checked each 1 ms. Size of the context stack free memory can be checked by
 * the function #oC_SYS_LLD_GetContextFreeStackSize.
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_PORTABLE_INC_LLD_OC_SYS_LLD_H_
#define SYSTEM_PORTABLE_INC_LLD_OC_SYS_LLD_H_

#include <oc_machine.h>
#include <oc_frequency.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup SYS-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief type for storing context of the machine
 *
 * This is the type for storing context of the machine. It should be dynamically allocated with size from the function
 * #oC_SYS_LLD_GetContextStackSize. The start of this structure pointer is at the start of the stack. It must be initialized by
 * #oC_SYS_LLD_InitializeContext function before usage. When it is done, you can change the context of the machine (call other task)
 * by using the function #oC_SYS_LLD_SetNextContext.
 */
//==========================================================================================================================================
typedef void oC_SYS_LLD_Context_t;

//==========================================================================================================================================
/**
 * @brief event flags handled by the module.
 *
 * The type stores flags for module events. Event handler, and interrupts selection can be done by #oC_SYS_LLD_SetEventInterruptHandler
 * function.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SYS_LLD_EventFlags_HardFault             = (1<<0),       //!< Hard fault, reason is unknown
    oC_SYS_LLD_EventFlags_DivideBy0             = (1<<1),       //!< Hard fault, divide by 0 detected
    oC_SYS_LLD_EventFlags_UnalignedLoadOrStore  = (1<<2),       //!< Loading or storing data to memory occurs with not aligned address
    oC_SYS_LLD_EventFlags_UndefinedInstruction  = (1<<3),       //!< Undefined instruction detected
    oC_SYS_LLD_EventFlags_UsageFault            = (1<<4),       //!< Usage fault
    oC_SYS_LLD_EventFlags_ProcessStackOverflow  = (1<<5),       //!< The process is not correct, or the process stack was overflowed
} oC_SYS_LLD_EventFlags_t;

//==========================================================================================================================================
/**
 * @brief type for storing interrupt pointers
 *
 * The type is for storing interrupt handler function pointer. The function that is set to this pointer should be as short as possible, and it
 * should be included, that this is executed in an interrupt.
 *
 */
//==========================================================================================================================================
typedef void (*oC_SYS_LLD_Interrupt_t)(void);

//==========================================================================================================================================
/**
 * @brief stores SysTick interrupt handler
 *
 * The type is for storing special function - SysTick handler. It is the function, that is called periodically as interrupt for inform the
 * system about need the increment of the system tick counter. The handler should also change the current system task if it is needed.
 */
//==========================================================================================================================================
typedef void (*oC_SYS_LLD_SysTickIncrementHandler_t)(void);

//==========================================================================================================================================
/**
 * @brief stores event handler function
 *
 * The type is for storing handler of events. It is a function that will be called, when one of enabled events by #oC_SYS_LLD_SetEventInterruptHandler
 * function will occur.
 *
 * @param EventFlags        Flags of active events (can be more than one)
 * @param Context           Context of the event
 *
 */
//==========================================================================================================================================
typedef void (*oC_SYS_LLD_EventInterrupt_t)( oC_SYS_LLD_EventFlags_t EventFlags , oC_SYS_LLD_Context_t * Context , void * BusAddress , const char * Details );

//==========================================================================================================================================
/**
 * @brief stores pointer to context handler function
 *
 * The type stores pointer to the function, that handle context
 *
 * @param Parameter     User parameter for the function
 */
//==========================================================================================================================================
typedef void (*oC_SYS_LLD_ContextHandler_t)( void * Parameter );

//==========================================================================================================================================
/**
 * @brief pointer to the context exit handler
 *
 * The type stores pointer to the function, that is called, when the context handler finish work.
 */
//==========================================================================================================================================
typedef void (*oC_SYS_LLD_ContextExitHandler_t)(void);

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup SYS-LLD
//! @{

//==========================================================================================================================================
/**
 * @brief initializes the driver to work
 *
 * The function is for initializing the low level driver. It will be called every time when the driver will turned on. There is no need to
 * protect again returning because the main driver should protect it by itself.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_TurnOnDriver( void );
//==========================================================================================================================================
/**
 * @brief release the driver
 *
 * The function is for releasing resources needed for the driver. The main driver should call it every time, when it is turned off. This
 * function should restore default states in all resources, that are handled and was initialized by the driver. It must not protect by itself
 * again turning off the driver when it is not turned on.
 *
 * @note When the module is turned off, this function restores the system context.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_TurnOffDriver( void );
//==========================================================================================================================================
/**
 * @brief sets event interrupt handler
 *
 * The function is for configuration of the event interrupts for the module. For information what events are handled, look at the #oC_SYS_LLD_EventFlags_t
 * type. The function configures and turns on each interrupt.
 *
 * @param EventHandler      Pointer to the function that should be called, when event occurs
 * @param EventsFlags       Events, that should be enabled
 *
 * @note Interrupt handler can be set only once. To set interrupt handler again, the SYS module must be restarted.
 *
 * @return code of error, or #oC_ErrorCode_None if there is no error.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_SetEventInterruptHandler(oC_SYS_LLD_EventInterrupt_t EventHandler , oC_SYS_LLD_EventFlags_t EventsFlags);
//==========================================================================================================================================
/**
 * @brief checks if the machine supports multi-thread mode
 *
 * The function checks if the machine supports multi-thread mode. It returns false if it is not supported or not implemented already.
 *
 * @return true if the machine supports multi-thread mode and it is implemented.
 */
//==========================================================================================================================================
extern bool oC_SYS_LLD_IsMachineSupportMultithreadMode( void );
//==========================================================================================================================================
/**
 * @brief turns on interrupt
 *
 * The function is for turning on interrupts. It not protect before recursive calling, each call of this function turns on interrupts. This
 * function can be used also when module is turned off.
 */
//==========================================================================================================================================
extern void oC_SYS_LLD_TurnOnInterrupts(void);
//==========================================================================================================================================
/**
 * @brief turns off interrupt
 *
 * The function is for turning off interrupts. It not protect before recursive calling, each call of this function turns off interrupts.This
 * function can be used also when module is turned off.
 */
//==========================================================================================================================================
extern void oC_SYS_LLD_TurnOffInterrupts(void);
//==========================================================================================================================================
/**
 * @brief enters to critical section
 *
 * The function enters to critical section.
 */
//==========================================================================================================================================
extern void oC_SYS_LLD_EnterCriticalSection(void);
//==========================================================================================================================================
/**
 * @brief exits from critical section
 *
 * The function enters to critical section.
 */
//==========================================================================================================================================
extern void oC_SYS_LLD_ExitCriticalSection(void);
//==========================================================================================================================================
/**
 * @brief checks if interrupts are turned on
 */
//==========================================================================================================================================
extern bool oC_SYS_LLD_AreInterruptsEnabled(void);
//==========================================================================================================================================
/**
 * @brief configures system timer
 *
 * The function is for configuration of the system timer that should call the Interrupt with the given Frequency.
 *
 * @param Frequency     Frequency of switching context event
 * @param Interrupt     SysTick increment interrupt handler
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_ConfigureSystemTimer( oC_Frequency_t Frequency , oC_SYS_LLD_SysTickIncrementHandler_t Interrupt);
//==========================================================================================================================================
/**
 * @brief reads configured system frequency
 *
 * The function is for reading frequency of the system timer.
 *
 * @param outFrequency  a destination for the variable where the frequency should be saved.
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_ReadSystemTimerFrequency( oC_Frequency_t * outFrequency );
//==========================================================================================================================================
/**
 * @brief configures next machine context
 *
 * The function is for setting next context of the machine. It will save the pointer for later - the context will be switched with the first
 * call of context switching interrupt. The context is the process stack. It should be allocated and initialized by #oC_SYS_LLD_InitializeContext
 * function before usage.
 *
 * @warning It is recommended to turn off interrupts before call of this function.
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_SetNextContext(oC_SYS_LLD_Context_t * Context);

//==========================================================================================================================================
/**
 * @brief returns size of the stack for context
 *
 * It returns size of the stack, that is allocated for the context. Note, that this size does not contain an extra size that is additionally
 * required and used for handling the context. All of this size is used only for handling the process stack.
 *
 * @param Context       Initialized context pointer
 *
 * @return size of the context, or 0 if error
 */
//==========================================================================================================================================
extern oC_Int_t oC_SYS_LLD_GetContextStackSize(oC_SYS_LLD_Context_t * Context);

//==========================================================================================================================================
/**
 * @brief returns size of free stack in the context
 *
 * The function returns size of the free memory on the context stack.
 *
 * @param Context       Pointer to the context initialized before by #oC_SYS_LLD_InitializeContext
 *
 * @return size or 0 if error
 */
//==========================================================================================================================================
extern oC_Int_t oC_SYS_LLD_GetContextFreeStackSize(oC_SYS_LLD_Context_t * Context);

//==========================================================================================================================================
/**
 * @brief switches to the system context
 *
 * The function returns to the system context - the context, that was active, when the SYS LLD module was turned on.
 *
 * @return code of error
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_ReturnToSystemContext( void );

//==========================================================================================================================================
/**
 * @brief returns pointer to the current context
 */
//==========================================================================================================================================
extern oC_SYS_LLD_Context_t * oC_SYS_LLD_GetCurrentContext(void);

//==========================================================================================================================================
/**
 * @brief returns pointer to the system context
 */
//==========================================================================================================================================
extern oC_SYS_LLD_Context_t * oC_SYS_LLD_GetSystemContext(void);

//==========================================================================================================================================
/**
 * @brief initializes stack of process
 *
 * The function is for initialization of a context stack. Note, that the context must be allocated and initialized before. The minimum size
 * of the stack for a context can be read by the function #oC_SYS_LLD_GetMinimumContextSize.
 *
 * Here some example of usage:
 * @code{.c}
 *
 * void ContextHandler( void * )
 * {
 *      while(1);
 * }
 *
 * void ExitHandler(void)
 * {
 *    while(1);
 * }
 *
 * oC_UInt_t stackSize = 512;
 * oC_SYS_LLD_Context_t * context = malloc( oC_SYS_LLD_GetMinimumContextSize(stackSize) );
 *
 * oC_SYS_LLD_InitializeContext(context , stackSize, ContextHandler , NULL , ContextHandler , ExitHandler);
 *
 * oC_SYS_LLD_SetNextContext(context);
 *
 * while(1);
 * @endcode
 *
 * @param Context           Allocated memory to initialize. It will be used as a context stack.
 * @param StackSize         Size of the stack. It is not equal to the allocation size (this size should be smaller)
 * @param ContextHandler    The function that should be called, while this context is active
 * @param Parameters        Optional parameter for the ContextHandler function
 * @param ExitHandler       The function that should be called, when the context handler will finish.
 *
 *
 * @return code of error, or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_InitializeContext(oC_SYS_LLD_Context_t ** Context , oC_Int_t StackSize , oC_SYS_LLD_ContextHandler_t ContextHandler , void * Parameters , oC_SYS_LLD_ContextExitHandler_t ExitHandler );
//==========================================================================================================================================
/**
 * @brief returns minimum size to allocate for context
 *
 * The function returns the minimum size of allocation that is needed to handle the context switching.
 *
 * @param StackSize     The minimum stack size, that you will need for the task
 *
 * @return size of the context structure
 */
//==========================================================================================================================================
extern oC_Int_t oC_SYS_LLD_GetMinimumContextSize(oC_Int_t StackSize);
//==========================================================================================================================================
/**
 * @brief returns printable name of the machine
 *
 * The function is for reading name of the target machine.
 *
 * @return constant string with machine name
 */
//==========================================================================================================================================
extern const char * oC_SYS_LLD_GetMachineName( void );
//==========================================================================================================================================
/**
 * @brief returns printable name of the machine family
 *
 * The function is for reading name of the target machine family.
 *
 * @return constant string with machine family name.
 */
//==========================================================================================================================================
extern const char * oC_SYS_LLD_GetMachineFamilyName( void );
//==========================================================================================================================================
/**
 * @brief returns printable name of the machine producent
 *
 * The function is for reading name of the target machine company.
 *
 * @return constant string with machine company name.
 */
//==========================================================================================================================================
extern const char * oC_SYS_LLD_GetMachineProducentName( void );
//==========================================================================================================================================
/**
 * @brief software reset of the machine
 *
 * This function never returns.
 *
 * @return code of error
 */
//==========================================================================================================================================
extern void oC_SYS_LLD_Reset( void );
//==========================================================================================================================================
/**
 * @brief flag if stack push is decrement pointer
 *
 * The function is for checking if stack push command is decrement or increment pointer on this machine.
 *
 * @return true if after push, the address is decremented
 */
//==========================================================================================================================================
extern bool oC_SYS_LLD_IsStackPushDecrementPointer( void );
//==========================================================================================================================================
/**
 * @brief reads power state
 *
 * The function reads current power state (the VCC of the CPU)
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_SYS_LLD_ReadPowerState( float * outVcc );

//==========================================================================================================================================
/**
 * @brief returns stack pointer of last executed process
 */
//==========================================================================================================================================
extern void * oC_SYS_LLD_GetLastProcessStackPointer( void );

//==========================================================================================================================================
/**
 * @brief prints message by using debugger
 */
//==========================================================================================================================================
extern void oC_SYS_LLD_PrintToDebugger( const char * Message );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_LLD_OC_SYS_LLD_H_ */
