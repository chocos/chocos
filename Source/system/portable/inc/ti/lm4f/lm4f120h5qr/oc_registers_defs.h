/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions of bits in registers
 *
 * @file       oc_machine_regs.h
 *
 * @author     Kamil Drobienko
 *
 * @note       Copyright (C) 2016 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_REGISTERS_DEFS_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_REGISTERS_DEFS_H_

#include <oc_1word.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of the register bits definition
 */
//==========================================================================================================================================
#define oC_REGISTER_(REGISTER_NAME)         oC_1WORD_FROM_2(oC_REGISTER_ , REGISTER_NAME)

/** @hideinitializer ****************************************************************************************************************************************
 * The section with system control definitions
 */
#define _________________________________________SECTION_____________________________________________________________________
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DID0(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DID1(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PBORCTL(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RIS(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0           , 1 )\
    MAKE_BIT( BOR1RIS              , 1 )\
    MAKE_BIT( _RESERVED2           , 1 )\
    MAKE_BIT( MOFRIS               , 1 )\
    MAKE_BIT( _RESERVED4_5         , 2 )\
    MAKE_BIT( PLLLRIS              , 1 )\
    MAKE_BIT( USBPLLLRIS           , 1 )\
    MAKE_BIT( MOSCPUPRIS           , 1 )\
    MAKE_BIT( _RESERVED9           , 1 )\
    MAKE_BIT( VDDARIS              , 1 )\
    MAKE_BIT( BOR0RIS              , 1 )\
    MAKE_BIT( _RESERVED12_31       , 20 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_IMC(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_MISC(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RESC(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC(MAKE_BIT)     \
    MAKE_BIT( MOSCDIS               , 1 )\
    MAKE_BIT( _RESERVED1_3          , 3 )\
    MAKE_BIT( OSCSRC                , 2 )\
    MAKE_BIT( XTAL                  , 5 )\
    MAKE_BIT( BYPASS                , 1 )\
    MAKE_BIT( _RESERVED12           , 1 )\
    MAKE_BIT( PWRDN                 , 1 )\
    MAKE_BIT( _RESERVED14_21        , 8 )\
    MAKE_BIT( USESYSDIV             , 1 )\
    MAKE_BIT( SYSDIV                , 4 )\
    MAKE_BIT( ACG                   , 1 )\
    MAKE_BIT( _RESERVED28_31        , 4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOHBCTL(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC2(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_3         , 4 )\
    MAKE_BIT( OSCSRC2              , 3 )\
    MAKE_BIT( _RESERVED7_10        , 4 )\
    MAKE_BIT( BYPASS2              , 1 )\
    MAKE_BIT( _RESERVED12          , 1 )\
    MAKE_BIT( PWRDN2               , 1 )\
    MAKE_BIT( USBPWRDN             , 1 )\
    MAKE_BIT( _RESERVED15_21       , 7 )\
    MAKE_BIT( SYSDIV2LSB           , 1 )\
    MAKE_BIT( SYSDIV2              , 6 )\
    MAKE_BIT( _RESERVED29          , 1 )\
    MAKE_BIT( DIV400               , 1 )\
    MAKE_BIT( USERCC2              , 1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_MOSCCTL(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DSLPCLKCFG(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSPROP(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PIOSCCAL(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PIOSCSTAT(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PLLFREQ0(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PLLFREQ1(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PLLSTAT(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPWD(MAKE_BIT)     \
    MAKE_BIT( _RESERVED0_31        ,  32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPTIMER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPGPIO( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPDMA( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPHIB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPSSI( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPI2C( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPUSB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPCAN( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPADC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPACMP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPPWM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPQEI( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PPEEPROM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRWD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRTIMER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRGPIO( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRDMA( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRHIB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRSSI( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRI2C( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRUSB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRCAN( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRADC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRACMP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SREEPROM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRWTIMER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCWD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCTIMER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCGPIO( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCDMA( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCHIB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCUART( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCSSI( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCI2C( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCUSB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCCAN( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCADC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCACMP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCEEPROM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGCWTIMER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGCWD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGCHIB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGCI2C( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGCUSB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGCACMP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGCEEPROM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCGCWD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCGCHIB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCGCEEPROM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRWD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRTIMER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRGPIO( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRDMA( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRHIB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRSSI( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRI2C( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRUSB( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRCAN( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRADC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRACMP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PREEPROM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PRWTIMER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC8( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRCR0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRCR1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SRCR2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGC0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGC1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCGC2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGC0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGC1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SCGC2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCGC0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCGC1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCGC2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DC9( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_NVMSTAT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\
/* END OF SECTION */
#undef  _________________________________________SECTION_____________________________________________________________________

/** @hideinitializer ****************************************************************************************************************************************
 * The section with GPIO definitions
 */
#define _________________________________________GPIO_SECTION_______________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIODATA( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIODIR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOIBE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOIEV( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOIM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIORIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOMIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOICR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOAFSEL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIODR2R( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIODR4R( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIODR8R( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOODR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPUR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPDR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOSLR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIODEN( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOLOCK( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOCR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOAMSEL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOADCCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIODMACTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPeriphID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPCellID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPCellID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPCellID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOPCellID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

/* END OF SECTION */
#undef  _________________________________________GPIO_SECTION_______________________________________________________________________________

/** @hideinitializer ****************************************************************************************************************************************
 * The section with DMA definitions
 */
#define _________________________________________DMA_SECTION________________________________________________________________________________


//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMASRCENDP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMADSTENDP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACHCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMASTAT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACFG( MAKE_BIT ) \
    MAKE_BIT( MASTEN         , 1 )\
    MAKE_BIT( _RESERVED1_31 , 31 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACTLBASE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAALTBASE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAWAITSTAT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMASWREQ( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAUSEBURSTSET( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAUSEBURSTCLR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAREQMASKSET( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAREQMASKCLR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAENASET( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAENACLR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAALTSET( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAALTCLR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPRIOSET( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPRIOCLR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAERRCLR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACHASGN( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACHIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACHMAP0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACHMAP1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACHMAP2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMACHMAP3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPeriphID4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPeriphID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPeriphID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPeriphID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPeriphID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPCellID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPCellID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPCellID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMAPCellID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________DMA_SECTION________________________________________________________________________________


/** @hideinitializer ****************************************************************************************************************************************
 * The section with definitions for timers
 */
#define _________________________________________TIMER_SECTION____________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMCFG( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAMR( MAKE_BIT ) \
    MAKE_BIT( TAMR    , 2 )\
    MAKE_BIT( TACMR   , 1 )\
    MAKE_BIT( TAAMS   , 1 )\
    MAKE_BIT( TACDIR  , 1 )\
    MAKE_BIT( TAMIE   , 1 )\
    MAKE_BIT( TAWOT   , 1 )\
    MAKE_BIT( TASNAPS , 1 )\
    MAKE_BIT( TAILD   , 1 )\
    MAKE_BIT( TAPWMIE , 1 )\
    MAKE_BIT( TAMRSU  , 1 )\
    MAKE_BIT( TAPLO   , 1 )\
    MAKE_BIT( _RESERVED12_31 , 20 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBMR( MAKE_BIT ) \
    MAKE_BIT( TBMR    , 2 )\
    MAKE_BIT( TBCMR   , 1 )\
    MAKE_BIT( TBAMS   , 1 )\
    MAKE_BIT( TBCDIR  , 1 )\
    MAKE_BIT( TBMIE   , 1 )\
    MAKE_BIT( TBWOT   , 1 )\
    MAKE_BIT( TBSNAPS , 1 )\
    MAKE_BIT( TBILD   , 1 )\
    MAKE_BIT( TBPWMIE , 1 )\
    MAKE_BIT( TBMRSU  , 1 )\
    MAKE_BIT( TBPLO   , 1 )\
    MAKE_BIT( _RESERVED12_31 , 20 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMCTL( MAKE_BIT ) \
    MAKE_BIT( TAEN              , 1 )\
    MAKE_BIT( TASTALL           , 1 )\
    MAKE_BIT( TAEVENT           , 2 )\
    MAKE_BIT( RTCEN             , 1 )\
    MAKE_BIT( TAOTE             , 1 )\
    MAKE_BIT( TAPWML            , 1 )\
    MAKE_BIT( _RESERVED7_7    , 1 )\
    MAKE_BIT( TBEN              , 1 )\
    MAKE_BIT( TBSTALL           , 1 )\
    MAKE_BIT( TBEVENT           , 2 )\
    MAKE_BIT( _RESERVED12_12  , 1 )\
    MAKE_BIT( TBOTE             , 1 )\
    MAKE_BIT( TBPWML            , 1 )\
    MAKE_BIT( _RESERVED15_31  , 17 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMSYNC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMIMR( MAKE_BIT ) \
    MAKE_BIT( TATOIM           , 1 )\
    MAKE_BIT( CAMIM            , 1 )\
    MAKE_BIT( CAEIM            , 1 )\
    MAKE_BIT( RTCIM            , 1 )\
    MAKE_BIT( TAMIM            , 1 )\
    MAKE_BIT( _RESERVED5_7   , 3 )\
    MAKE_BIT( TBTOIM           , 1 )\
    MAKE_BIT( CBMIM            , 1 )\
    MAKE_BIT( CBEIM            , 1 )\
    MAKE_BIT( TBMIM            , 1 )\
    MAKE_BIT( _RESERVED12_15 , 4 )\
    MAKE_BIT( WUEIM            , 1 )\
    MAKE_BIT( _RESERVED17_31 , 15 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMMIS( MAKE_BIT ) \
    MAKE_BIT( TATOMIS          , 1 )\
    MAKE_BIT( CAMMIS           , 1 )\
    MAKE_BIT( CAEMIS           , 1 )\
    MAKE_BIT( RTCMIS           , 1 )\
    MAKE_BIT( TAMMIS           , 1 )\
    MAKE_BIT( _RESERVED5_7   , 3 )\
    MAKE_BIT( TBTOMIS          , 1 )\
    MAKE_BIT( CBMMIS           , 1 )\
    MAKE_BIT( CBEMIS           , 1 )\
    MAKE_BIT( TBMMIS           , 1 )\
    MAKE_BIT( _RESERVED12_15 , 4 )\
    MAKE_BIT( WUEMIS           , 1 )\
    MAKE_BIT( _RESERVED17_31 , 15 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMICR( MAKE_BIT ) \
    MAKE_BIT( TATOCINT         , 1 )\
    MAKE_BIT( CAMCINT          , 1 )\
    MAKE_BIT( CAECINT          , 1 )\
    MAKE_BIT( RTCCINT          , 1 )\
    MAKE_BIT( TAMCINT          , 1 )\
    MAKE_BIT( _RESERVED5_7   , 3 )\
    MAKE_BIT( TBTOCINT         , 1 )\
    MAKE_BIT( CBMCINT          , 1 )\
    MAKE_BIT( CBECINT          , 1 )\
    MAKE_BIT( TBMCINT          , 1 )\
    MAKE_BIT( _RESERVED12_15 , 4 )\
    MAKE_BIT( WUECINT          , 1 )\
    MAKE_BIT( _RESERVED17_31 , 15 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAILR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBILR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAMATCHR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBMATCHR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAPR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBPR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAPMR( MAKE_BIT ) \
    MAKE_BIT( TAPSMR            , 8 )\
    MAKE_BIT( TAPSMRH           , 8 )\
    MAKE_BIT( _RESERVED16_31  , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBPMR( MAKE_BIT ) \
    MAKE_BIT( TBPSMR            , 8 )\
    MAKE_BIT( TBPSMRH           , 8 )\
    MAKE_BIT( _RESERVED16_31  , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAV( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBV( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMRTCPD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAPS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBPS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTAPV( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMTBPV( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPTMPP( MAKE_BIT ) \
    MAKE_BIT( SIZE            , 4 )\
    MAKE_BIT( _RESERVED4_31 , 28 )\


/* END OF SECTION */
#undef  _________________________________________TIMER_SECTION____________________________________________________________________________

/** @hideinitializer ****************************************************************************************************************************************
 * The section watchdog timers
 */
#define _________________________________________WATCHDOG_TIMERS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTLOAD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTVALUE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTMIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTTEST( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTLOCK( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPeriphID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPCellID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPCellID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPCellID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WDTPCellID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________WATCHDOG_TIMERS_SECTION____________________________________________________________________


/** @hideinitializer ****************************************************************************************************************************************
 * The section with ADC definitions
 */
#define _________________________________________ADC_SECTION________________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCACTSS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCIM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCISC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCOSTAT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCEMUX( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCUSTAT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSPRI( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSPC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCPSSI( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSAC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCISC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSMUX0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSCTL0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFIFO0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFSTAT0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSOP0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSDC0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSMUX1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSCTL1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFIFO1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFSTAT1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSOP1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSDC1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSMUX2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSCTL2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFIFO2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFSTAT2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSOP2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSDC2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSMUX3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSCTL3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFIFO3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSFSTAT3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSOP3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCSSDC3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCRIC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCTL7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCDCCMP7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCPP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCPC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADCCC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________ADC_SECTION________________________________________________________________________________


/** @hideinitializer ****************************************************************************************************************************************
 * The section with UART definitions
 */
#define _________________________________________UART_SECTION_______________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTDR( MAKE_BIT ) \
    MAKE_BIT( DATA                  , 8 )\
    MAKE_BIT( FE                    , 1 )\
    MAKE_BIT( PE                    , 1 )\
    MAKE_BIT( BE                    , 1 )\
    MAKE_BIT( OE                    , 1 )\
    MAKE_BIT( _RESERVED12_31       , 20 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTFR( MAKE_BIT ) \
    MAKE_BIT( CTS                   , 1 )\
    MAKE_BIT( _RESERVED1_2         , 2 )\
    MAKE_BIT( BUSY                  , 1 )\
    MAKE_BIT( RXFE                  , 1 )\
    MAKE_BIT( TXFF                  , 1 )\
    MAKE_BIT( RXFF                  , 1 )\
    MAKE_BIT( TXFE                  , 1 )\
    MAKE_BIT( _RESERVED8_31        , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTILPR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTIBRD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTFBRD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTLCRH( MAKE_BIT ) \
    MAKE_BIT( BRK            , 1 )\
    MAKE_BIT( PEN            , 1 )\
    MAKE_BIT( EPS            , 1 )\
    MAKE_BIT( STP2           , 1 )\
    MAKE_BIT( FEN            , 1 )\
    MAKE_BIT( WLEN           , 2 )\
    MAKE_BIT( SPS            , 1 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTCTL( MAKE_BIT ) \
    MAKE_BIT( UARTEN            , 1 )\
    MAKE_BIT( SIREN             , 1 )\
    MAKE_BIT( SIRLP             , 1 )\
    MAKE_BIT( SMART             , 1 )\
    MAKE_BIT( EOT               , 1 )\
    MAKE_BIT( HSE               , 1 )\
    MAKE_BIT( _RESERVED6_6     , 1 )\
    MAKE_BIT( LBE               , 1 )\
    MAKE_BIT( TXE               , 1 )\
    MAKE_BIT( RXE               , 1 )\
    MAKE_BIT( _RESERVED10_10   , 1 )\
    MAKE_BIT( RTS               , 1 )\
    MAKE_BIT( _RESERVED12_13   , 2 )\
    MAKE_BIT( RTSEN             , 1 )\
    MAKE_BIT( CTSEN             , 1 )\
    MAKE_BIT( _RESERVED16_31   , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTIFLS( MAKE_BIT ) \
    MAKE_BIT( TXIFLSEL      , 3  ) \
    MAKE_BIT( RXIFLSEL      , 3  ) \
    MAKE_BIT( _RESERVED6_31 , 26 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTIM( MAKE_BIT )\
    MAKE_BIT( _RESERVED0_0      , 1 )\
    MAKE_BIT( CTSISM            , 1 )\
    MAKE_BIT( _RESERVED2_3      , 2 )\
    MAKE_BIT( RXIM              , 1 )\
    MAKE_BIT( TXIM              , 1 )\
    MAKE_BIT( RTIM              , 1 )\
    MAKE_BIT( FEIM              , 1 )\
    MAKE_BIT( PEIM              , 1 )\
    MAKE_BIT( BEIM              , 1 )\
    MAKE_BIT( OEIM              , 1 )\
    MAKE_BIT( _RESERVED11_11    , 1 )\
    MAKE_BIT( BITM              , 1 )\
    MAKE_BIT( _RESERVED13_31    , 19 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_0      , 1 )\
    MAKE_BIT( CTSRIS            , 1 )\
    MAKE_BIT( _RESERVED2_3      , 2 )\
    MAKE_BIT( RXRIS             , 1 )\
    MAKE_BIT( TXRIS             , 1 )\
    MAKE_BIT( RTRIS             , 1 )\
    MAKE_BIT( FERIS             , 1 )\
    MAKE_BIT( PERIS             , 1 )\
    MAKE_BIT( BERIS             , 1 )\
    MAKE_BIT( OERIS             , 1 )\
    MAKE_BIT( _RESERVED11_11    , 1 )\
    MAKE_BIT( BIRIS             , 1 )\
    MAKE_BIT( _RESERVED13_31    , 19 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTMIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTICR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_0      , 1 )\
    MAKE_BIT( CTSMIC            , 1 )\
    MAKE_BIT( _RESERVED2_3      , 2 )\
    MAKE_BIT( RXIC              , 1 )\
    MAKE_BIT( TXIC              , 1 )\
    MAKE_BIT( RTIC              , 1 )\
    MAKE_BIT( FEIC              , 1 )\
    MAKE_BIT( PEIC              , 1 )\
    MAKE_BIT( BEIC              , 1 )\
    MAKE_BIT( OEIC              , 1 )\
    MAKE_BIT( _RESERVED11_11    , 1 )\
    MAKE_BIT( BITC              , 1 )\
    MAKE_BIT( _RESERVED13_31    , 19 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTDMACTL( MAKE_BIT ) \
    MAKE_BIT( RXDMAE            , 1 )\
    MAKE_BIT( TXDMAE            , 1 )\
    MAKE_BIT( DMAERR            , 1 )\
    MAKE_BIT( _RESERVED3_31 , 29 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UART9BITADDR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UART9BITAMASK( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTCC( MAKE_BIT ) \
    MAKE_BIT( CS            , 4  )\
    MAKE_BIT( _RESERVED4_31 , 28 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPeriphID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPCellID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPCellID1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPCellID2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_UARTPCellID3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________UART_SECTION_______________________________________________________________________________


/** @hideinitializer ****************************************************************************************************************************************
 * The section with SPI definitions
 */
#define _________________________________________SPI_SECTION________________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSICR0( MAKE_BIT ) \
    MAKE_BIT( DSS , 4 )\
    MAKE_BIT( FRF , 2 )\
    MAKE_BIT( SPO , 1 )\
    MAKE_BIT( SPH , 1 )\
    MAKE_BIT( SCR , 8 )\
    MAKE_BIT( _RESERVED16_31 , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSICR1( MAKE_BIT ) \
    MAKE_BIT( LBM , 1 )\
    MAKE_BIT( SSE , 1 )\
    MAKE_BIT( MS , 1 )\
    MAKE_BIT( SOD , 1 )\
    MAKE_BIT( EOT , 1 )\
    MAKE_BIT( SLBY6 , 1 )\
    MAKE_BIT( _RESERVED6_31 , 26)\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIDR( MAKE_BIT ) \
    MAKE_BIT( DATA , 16 )\
    MAKE_BIT( _RESERVED16_31 , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSISR( MAKE_BIT ) \
    MAKE_BIT( TFE , 1 )\
    MAKE_BIT( TNF , 1 )\
    MAKE_BIT( RNE , 1 )\
    MAKE_BIT( RFF , 1 )\
    MAKE_BIT( BSY , 1 )\
    MAKE_BIT( _RESERVED5_15 , 27 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSICPSR( MAKE_BIT ) \
    MAKE_BIT( CPSDVSR , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIIM( MAKE_BIT ) \
    MAKE_BIT( RORIM , 1 )\
    MAKE_BIT( RTIM , 1 )\
    MAKE_BIT( RXIM , 1 )\
    MAKE_BIT( TXIM , 1 )\
    MAKE_BIT( _RESERVED4_31 , 28 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIRIS( MAKE_BIT ) \
    MAKE_BIT( RORRIS , 1 )\
    MAKE_BIT( RTRIS , 1 )\
    MAKE_BIT( RXRIS , 1 )\
    MAKE_BIT( TXRIS , 1 )\
    MAKE_BIT( _RESERVED4_31 , 28 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIMIS( MAKE_BIT ) \
    MAKE_BIT( RORMIS , 1 )\
    MAKE_BIT( RTMIS , 1 )\
    MAKE_BIT( RXMIS , 1 )\
    MAKE_BIT( TXMIS , 1 )\
    MAKE_BIT( _RESERVED4_31 , 28 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIICR( MAKE_BIT ) \
    MAKE_BIT( RORIC , 1 )\
    MAKE_BIT( RTIC , 1 )\
    MAKE_BIT( _RESERVED2_31 , 30 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIDMACTL( MAKE_BIT ) \
    MAKE_BIT( TXDMAE , 1 )\
    MAKE_BIT( RXDMAE , 1 )\
    MAKE_BIT( _RESERVED2_31 , 30 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSICC( MAKE_BIT ) \
    MAKE_BIT( CS , 4 )\
    MAKE_BIT( _RESERVED4_31 , 28 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID4( MAKE_BIT ) \
    MAKE_BIT( PID4 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID5( MAKE_BIT ) \
    MAKE_BIT( PID5 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID6( MAKE_BIT ) \
    MAKE_BIT( PID6 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID7( MAKE_BIT ) \
    MAKE_BIT( PID7 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID0( MAKE_BIT ) \
    MAKE_BIT( PID0 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID1( MAKE_BIT ) \
    MAKE_BIT( PID1 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID2( MAKE_BIT ) \
    MAKE_BIT( PID2 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPeriphID3( MAKE_BIT ) \
    MAKE_BIT( PID3 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPCellID0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPCellID1( MAKE_BIT ) \
    MAKE_BIT( CID1 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPCellID2( MAKE_BIT ) \
    MAKE_BIT( CID2 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SSIPCellID3( MAKE_BIT ) \
    MAKE_BIT( PID3 , 8 )\
    MAKE_BIT( _RESERVED8_31 , 24 )\


/* END OF SECTION */
#undef  _________________________________________SPI_SECTION________________________________________________________________________________

/** @hideinitializer ****************************************************************************************************************************************
 * The section with I2C definitions
 */
#define _________________________________________I2C_SECTION________________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMSA( MAKE_BIT ) \
    MAKE_BIT( PC2           , 2 )\
    MAKE_BIT( _RESERVED0_31 , 30 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMCS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMDR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMTPR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMIMR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMMIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMICR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMCR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMCLKOCNT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMBMON( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CMCR2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSOAR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSCSR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSDR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSIMR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSMIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSICR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSOAR2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CSACKCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CPP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2CPC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________I2C_SECTION________________________________________________________________________________

/** @hideinitializer ****************************************************************************************************************************************
 * The section with CAN definitions
 */
#define _________________________________________CAN_SECTION________________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANCTL.
CAN Control
 *
 *     Access:      RW
 *     Reset value: 0x0000.0001
 *     Page of description: 1025

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANSTS.
CAN Status
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1027

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANSTS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANERR.
CAN Error Counter
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1030

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANERR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANBIT.
CAN Bit Timing
 *
 *     Access:      RW
 *     Reset value: 0x0000.2301
 *     Page of description: 1031

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANBIT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANINT.
CAN Interrupt
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1032

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANINT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANTST.
CAN Test
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1033

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANTST( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANBRPE.
CAN Baud Rate Prescaler Extension
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1035

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANBRPE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1CRQ.
CAN IF1 Command Request
 *
 *     Access:      RW
 *     Reset value: 0x0000.0001
 *     Page of description: 1036

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1CRQ( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1CMSK.
CAN IF1 Command Mask
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1037

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1CMSK( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1MSK1.
CAN IF1 Mask 1
 *
 *     Access:      RW
 *     Reset value: 0x0000.FFFF
 *     Page of description: 1040

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1MSK1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1MSK2.
CAN IF1 Mask 2
 *
 *     Access:      RW
 *     Reset value: 0x0000.FFFF
 *     Page of description: 1041

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1MSK2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1ARB1.
CAN IF1 Arbitration 1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1043

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1ARB1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1ARB2.
CAN IF1 Arbitration 2
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1044

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1ARB2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1MCTL.
CAN IF1 Message Control
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1046

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1MCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1DA1.
CAN IF1 Data A1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1DA1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1DA2.
CAN IF1 Data A2
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1DA2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1DB1.
CAN IF1 Data B1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1DB1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF1DB2.
CAN IF1 Data B2
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF1DB2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2CRQ.
CAN IF2 Command Request
 *
 *     Access:      RW
 *     Reset value: 0x0000.0001
 *     Page of description: 1036

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2CRQ( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2CMSK.
CAN IF2 Command Mask
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1037

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2CMSK( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2MSK1.
CAN IF2 Mask 1
 *
 *     Access:      RW
 *     Reset value: 0x0000.FFFF
 *     Page of description: 1040

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2MSK1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2MSK2.
CAN IF2 Mask 2
 *
 *     Access:      RW
 *     Reset value: 0x0000.FFFF
 *     Page of description: 1041

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2MSK2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2ARB1.
CAN IF2 Arbitration 1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1043

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2ARB1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2ARB2.
CAN IF2 Arbitration 2
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1044

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2ARB2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2MCTL.
CAN IF2 Message Control
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1046

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2MCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2DA1.
CAN IF2 Data A1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2DA1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2DA2.
CAN IF2 Data A2
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2DA2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2DB1.
CAN IF2 Data B1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2DB1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANIF2DB2.
CAN IF2 Data B2
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1049

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANIF2DB2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANTXRQ1.
CAN Transmission Request 1
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1050

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANTXRQ1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANTXRQ2.
CAN Transmission Request 2
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1050

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANTXRQ2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANNWDA1.
CAN New Data 1
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1051

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANNWDA1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANNWDA2.
CAN New Data 2
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1051

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANNWDA2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANMSG1INT.
CAN Message 1 Interrupt Pending
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1052

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANMSG1INT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANMSG2INT.
CAN Message 2 Interrupt Pending
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1052

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANMSG2INT( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANMSG1VAL.
CAN Message 1 Valid
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1053

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANMSG1VAL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register CANMSG2VAL.
CAN Message 2 Valid
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1053

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_CANMSG2VAL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________CAN_SECTION________________________________________________________________________________

/** @hideinitializer ****************************************************************************************************************************************
 * The section with USB definitions
 */
#define _________________________________________USB_SECTION________________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFADDR.
USB Device Functional Address
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1066

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFADDR( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBPOWER.
USB Power
 *
 *     Access:      RW
 *     Reset value: 0x20
 *     Page of description: 1067

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBPOWER( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXIS.
USB Transmit Interrupt Status
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1069

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXIS.
USB Receive Interrupt Status
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1071

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXIE.
USB Transmit Interrupt Enable
 *
 *     Access:      RW
 *     Reset value: 0xFFFF
 *     Page of description: 1072

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXIE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXIE.
USB Receive Interrupt Enable
 *
 *     Access:      RW
 *     Reset value: 0xFFFE
 *     Page of description: 1074

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXIE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBIS.
USB General Interrupt Status
 *
 *     Access:      RO
 *     Reset value: 0x00
 *     Page of description: 1075

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBIE.
USB Interrupt Enable
 *
 *     Access:      RW
 *     Reset value: 0x06
 *     Page of description: 1076

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBIE( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFRAME.
USB Frame Value
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1078

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFRAME( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBEPIDX.
USB Endpoint Index
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1079

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBEPIDX( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTEST.
USB Test Mode
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1080

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTEST( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO0.
USB FIFO Endpoint 0
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO1.
USB FIFO Endpoint 1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO2.
USB FIFO Endpoint 2
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO3.
USB FIFO Endpoint 3
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO4.
USB FIFO Endpoint 4
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO5.
USB FIFO Endpoint 5
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO6.
USB FIFO Endpoint 6
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFIFO7.
USB FIFO Endpoint 7
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1081

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFIFO7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXFIFOSZ.
USB Transmit Dynamic FIFO Sizing
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1082

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXFIFOSZ( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXFIFOSZ.
USB Receive Dynamic FIFO Sizing
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1082

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXFIFOSZ( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXFIFOADD.
USB Transmit FIFO Start Address
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1083

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXFIFOADD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXFIFOADD.
USB Receive FIFO Start Address
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1083

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXFIFOADD( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBCONTIM.
USB Connect Timing
 *
 *     Access:      RW
 *     Reset value: 0x5C
 *     Page of description: 1084

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBCONTIM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBFSEOF.
USB Full-Speed Last Transaction to End of Frame Timing
 *
 *     Access:      RW
 *     Reset value: 0x77
 *     Page of description: 1085

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBFSEOF( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBCSRL0.
USB Control and Status Endpoint 0 Low
 *
 *     Access:      W1C
 *     Reset value: 0x00
 *     Page of description: 1088

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBCSRL0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBCSRH0.
USB Control and Status Endpoint 0 High
 *
 *     Access:      W1C
 *     Reset value: 0x00
 *     Page of description: 1090

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBCSRH0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBCOUNT0.
USB Receive Byte Count Endpoint 0
 *
 *     Access:      RO
 *     Reset value: 0x00
 *     Page of description: 1091

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBCOUNT0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXMAXP1.
USB Maximum Transmit Data Endpoint 1
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1087

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXMAXP1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRL1.
USB Transmit Control and Status Endpoint 1 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1092

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRL1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRH1.
USB Transmit Control and Status Endpoint 1 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1094

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRH1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXMAXP1.
USB Maximum Receive Data Endpoint 1
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1096

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXMAXP1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRL1.
USB Receive Control and Status Endpoint 1 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1097

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRL1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRH1.
USB Receive Control and Status Endpoint 1 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1100

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRH1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCOUNT1.
USB Receive Byte Count Endpoint 1
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1102

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCOUNT1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXMAXP2.
USB Maximum Transmit Data Endpoint 2
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1087

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXMAXP2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRL2.
USB Transmit Control and Status Endpoint 2 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1092

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRL2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRH2.
USB Transmit Control and Status Endpoint 2 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1094

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRH2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXMAXP2.
USB Maximum Receive Data Endpoint 2
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1096

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXMAXP2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRL2.
USB Receive Control and Status Endpoint 2 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1097

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRL2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRH2.
USB Receive Control and Status Endpoint 2 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1100

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRH2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCOUNT2.
USB Receive Byte Count Endpoint 2
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1102

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCOUNT2( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXMAXP3.
USB Maximum Transmit Data Endpoint 3
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1087

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXMAXP3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRL3.
USB Transmit Control and Status Endpoint 3 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1092

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRL3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRH3.
USB Transmit Control and Status Endpoint 3 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1094

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRH3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXMAXP3.
USB Maximum Receive Data Endpoint 3
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1096

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXMAXP3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRL3.
USB Receive Control and Status Endpoint 3 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1097

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRL3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRH3.
USB Receive Control and Status Endpoint 3 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1100

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRH3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCOUNT3.
USB Receive Byte Count Endpoint 3
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1102

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCOUNT3( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXMAXP4.
USB Maximum Transmit Data Endpoint 4
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1087

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXMAXP4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRL4.
USB Transmit Control and Status Endpoint 4 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1092

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRL4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRH4.
USB Transmit Control and Status Endpoint 4 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1094

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRH4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXMAXP4.
USB Maximum Receive Data Endpoint 4
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1096

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXMAXP4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRL4.
USB Receive Control and Status Endpoint 4 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1097

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRL4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRH4.
USB Receive Control and Status Endpoint 4 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1100

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRH4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCOUNT4.
USB Receive Byte Count Endpoint 4
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1102

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCOUNT4( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXMAXP5.
USB Maximum Transmit Data Endpoint 5
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1087

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXMAXP5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRL5.
USB Transmit Control and Status Endpoint 5 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1092

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRL5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRH5.
USB Transmit Control and Status Endpoint 5 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1094

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRH5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXMAXP5.
USB Maximum Receive Data Endpoint 5
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1096

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXMAXP5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRL5.
USB Receive Control and Status Endpoint 5 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1097

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRL5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRH5.
USB Receive Control and Status Endpoint 5 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1100

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRH5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCOUNT5.
USB Receive Byte Count Endpoint 5
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1102

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCOUNT5( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXMAXP6.
USB Maximum Transmit Data Endpoint 6
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1087

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXMAXP6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRL6.
USB Transmit Control and Status Endpoint 6 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1092

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRL6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRH6.
USB Transmit Control and Status Endpoint 6 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1094

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRH6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXMAXP6.
USB Maximum Receive Data Endpoint 6
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1096

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXMAXP6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRL6.
USB Receive Control and Status Endpoint 6 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1097

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRL6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRH6.
USB Receive Control and Status Endpoint 6 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1100

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRH6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCOUNT6.
USB Receive Byte Count Endpoint 6
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1102

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCOUNT6( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXMAXP7.
USB Maximum Transmit Data Endpoint 7
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1087

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXMAXP7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRL7.
USB Transmit Control and Status Endpoint 7 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1092

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRL7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXCSRH7.
USB Transmit Control and Status Endpoint 7 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1094

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXCSRH7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXMAXP7.
USB Maximum Receive Data Endpoint 7
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1096

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXMAXP7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRL7.
USB Receive Control and Status Endpoint 7 Low
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1097

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRL7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCSRH7.
USB Receive Control and Status Endpoint 7 High
 *
 *     Access:      RW
 *     Reset value: 0x00
 *     Page of description: 1100

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCSRH7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXCOUNT7.
USB Receive Byte Count Endpoint 7
 *
 *     Access:      RO
 *     Reset value: 0x0000
 *     Page of description: 1102

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXCOUNT7( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBRXDPKTBUFDIS.
USB Receive Double Packet Buffer Disable
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1103

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBRXDPKTBUFDIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBTXDPKTBUFDIS.
USB Transmit Double Packet Buffer Disable
 *
 *     Access:      RW
 *     Reset value: 0x0000
 *     Page of description: 1104

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBTXDPKTBUFDIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBDRRIS.
USB Device RESUME Raw Interrupt Status
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1105

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBDRRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBDRIM.
USB Device RESUME Interrupt Mask
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1106

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBDRIM( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBDRISC.
USB Device RESUME Interrupt Status and Clear
 *
 *     Access:      W1C
 *     Reset value: 0x0000.0000
 *     Page of description: 1107

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBDRISC( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBDMASEL.
USB DMA Select
 *
 *     Access:      RW
 *     Reset value: 0x0033.2211
 *     Page of description: 1108

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBDMASEL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register USBPP.
USB Peripheral Properties
 *
 *     Access:      RO
 *     Reset value: 0x0000.1050
 *     Page of description: 1110

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_USBPP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________USB_SECTION________________________________________________________________________________


/** @hideinitializer ****************************************************************************************************************************************
 * The section with ANALOG COMPARATOR definitions
 */
#define _________________________________________ANALOG_COMPARATORS_SECTION_________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACMIS.
Analog Comparator Masked Interrupt Status
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1118

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACMIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACRIS.
Analog Comparator Raw Interrupt Status
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1119

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACRIS( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACINTEN.
Analog Comparator Interrupt Enable
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1120

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACINTEN( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACREFCTL.
Analog Comparator Reference Voltage Control
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1121

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACREFCTL( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACSTAT0.
Analog Comparator Status 0
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1122

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACSTAT0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACCTL0.
Analog Comparator Control 0
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1123

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACCTL0( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACSTAT1.
Analog Comparator Status 1
 *
 *     Access:      RO
 *     Reset value: 0x0000.0000
 *     Page of description: 1122

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACSTAT1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACCTL1.
Analog Comparator Control 1
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: 1123

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACCTL1( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register ACMPPP.
Analog Comparator Peripheral Properties
 *
 *     Access:      RO
 *     Reset value: 0x0003.0003
 *     Page of description: 1125

To add new bit definition use MAKE_BIT( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_REGISTER_ACMPPP( MAKE_BIT ) \
    MAKE_BIT( _RESERVED0_31 , 32 )\


/* END OF SECTION */
#undef  _________________________________________ANALOG_COMPARATORS_SECTION_________________________________________________________________



#define _________________________________________LIST_OF_REGISTERS_SECTION_________________________________________________________________
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of registers in this file
 *
 * You can add register to list by using MAKE_REGISTER macro:
 * @code{.c} MAKE_REGISTER( REGISTER_NAME )\ @endcode
 *
 * Where
 *          REGISTER_NAME           - name of the register definition
 *
 * Remember, that if you will add register, you must also define a list of bits for it, that must be named as:
 * oC_REGISTER_[REGISTER_NAME]
 *
 */
//==========================================================================================================================================
#define oC_REGISTERS_LIST(MAKE_REGISTER) \
    MAKE_REGISTER( DID0      ) \
    MAKE_REGISTER( DID1      ) \
    MAKE_REGISTER( PBORCTL   ) \
    MAKE_REGISTER( RIS       ) \
    MAKE_REGISTER( IMC       ) \
    MAKE_REGISTER( MISC      ) \
    MAKE_REGISTER( RESC      ) \
    MAKE_REGISTER( RCC       ) \
    MAKE_REGISTER( GPIOHBCTL ) \
    MAKE_REGISTER( RCC2      ) \
    MAKE_REGISTER( MOSCCTL   ) \
    MAKE_REGISTER( DSLPCLKCFG) \
    MAKE_REGISTER( SYSPROP   ) \
    MAKE_REGISTER( PIOSCCAL  ) \
    MAKE_REGISTER( PIOSCSTAT ) \
    MAKE_REGISTER( PLLFREQ0  ) \
    MAKE_REGISTER( PLLFREQ1  ) \
    MAKE_REGISTER( PLLSTAT   ) \
    MAKE_REGISTER( PPWD      ) \
    MAKE_REGISTER( PPTIMER   ) \
    MAKE_REGISTER( PPGPIO    ) \
    MAKE_REGISTER( PPDMA     ) \
    MAKE_REGISTER( PPHIB     ) \
    MAKE_REGISTER( PPSSI     ) \
    MAKE_REGISTER( PPI2C     ) \
    MAKE_REGISTER( PPUSB     ) \
    MAKE_REGISTER( PPCAN     ) \
    MAKE_REGISTER( PPADC     ) \
    MAKE_REGISTER( PPACMP    ) \
    MAKE_REGISTER( PPPWM     ) \
    MAKE_REGISTER( PPQEI     ) \
    MAKE_REGISTER( PPEEPROM  ) \
    MAKE_REGISTER( SRWD      ) \
    MAKE_REGISTER( SRTIMER   ) \
    MAKE_REGISTER( SRGPIO    ) \
    MAKE_REGISTER( SRDMA     ) \
    MAKE_REGISTER( SRHIB     ) \
    MAKE_REGISTER( SRSSI     ) \
    MAKE_REGISTER( SRI2C     ) \
    MAKE_REGISTER( SRUSB     ) \
    MAKE_REGISTER( SRCAN     ) \
    MAKE_REGISTER( SRADC     ) \
    MAKE_REGISTER( SRACMP    ) \
    MAKE_REGISTER( SREEPROM  ) \
    MAKE_REGISTER( SRWTIMER  ) \
    MAKE_REGISTER( RCGCWD    ) \
    MAKE_REGISTER( RCGCTIMER ) \
    MAKE_REGISTER( RCGCGPIO  ) \
    MAKE_REGISTER( RCGCDMA   ) \
    MAKE_REGISTER( RCGCHIB   ) \
    MAKE_REGISTER( RCGCUART  ) \
    MAKE_REGISTER( RCGCSSI   ) \
    MAKE_REGISTER( RCGCI2C   ) \
    MAKE_REGISTER( RCGCUSB   ) \
    MAKE_REGISTER( RCGCCAN   ) \
    MAKE_REGISTER( RCGCADC   ) \
    MAKE_REGISTER( RCGCACMP  ) \
    MAKE_REGISTER( RCGCEEPROM ) \
    MAKE_REGISTER( RCGCWTIMER ) \
    MAKE_REGISTER( SCGCWD    ) \
    MAKE_REGISTER( SCGCHIB   ) \
    MAKE_REGISTER( SCGCI2C   ) \
    MAKE_REGISTER( SCGCUSB   ) \
    MAKE_REGISTER( SCGCACMP  ) \
    MAKE_REGISTER( SCGCEEPROM ) \
    MAKE_REGISTER( DCGCWD    ) \
    MAKE_REGISTER( DCGCHIB   ) \
    MAKE_REGISTER( DCGCEEPROM ) \
    MAKE_REGISTER( PRWD      ) \
    MAKE_REGISTER( PRTIMER   ) \
    MAKE_REGISTER( PRGPIO    ) \
    MAKE_REGISTER( PRDMA     ) \
    MAKE_REGISTER( PRHIB     ) \
    MAKE_REGISTER( PRSSI     ) \
    MAKE_REGISTER( PRI2C     ) \
    MAKE_REGISTER( PRUSB     ) \
    MAKE_REGISTER( PRCAN     ) \
    MAKE_REGISTER( PRADC     ) \
    MAKE_REGISTER( PRACMP    ) \
    MAKE_REGISTER( PREEPROM  ) \
    MAKE_REGISTER( PRWTIMER  ) \
    MAKE_REGISTER( DC0       ) \
    MAKE_REGISTER( DC1       ) \
    MAKE_REGISTER( DC2       ) \
    MAKE_REGISTER( DC3       ) \
    MAKE_REGISTER( DC4       ) \
    MAKE_REGISTER( DC5       ) \
    MAKE_REGISTER( DC6       ) \
    MAKE_REGISTER( DC7       ) \
    MAKE_REGISTER( DC8       ) \
    MAKE_REGISTER( SRCR0     ) \
    MAKE_REGISTER( SRCR1     ) \
    MAKE_REGISTER( SRCR2     ) \
    MAKE_REGISTER( RCGC0     ) \
    MAKE_REGISTER( RCGC1     ) \
    MAKE_REGISTER( RCGC2     ) \
    MAKE_REGISTER( SCGC0     ) \
    MAKE_REGISTER( SCGC1     ) \
    MAKE_REGISTER( SCGC2     ) \
    MAKE_REGISTER( DCGC0     ) \
    MAKE_REGISTER( DCGC1     ) \
    MAKE_REGISTER( DCGC2     ) \
    MAKE_REGISTER( DC9       ) \
    MAKE_REGISTER( NVMSTAT   ) \
    MAKE_REGISTER( GPIODATA ) \
    MAKE_REGISTER( GPIODIR ) \
    MAKE_REGISTER( GPIOIS ) \
    MAKE_REGISTER( GPIOIBE ) \
    MAKE_REGISTER( GPIOIEV ) \
    MAKE_REGISTER( GPIOIM ) \
    MAKE_REGISTER( GPIORIS ) \
    MAKE_REGISTER( GPIOMIS ) \
    MAKE_REGISTER( GPIOICR ) \
    MAKE_REGISTER( GPIOAFSEL ) \
    MAKE_REGISTER( GPIODR2R ) \
    MAKE_REGISTER( GPIODR4R ) \
    MAKE_REGISTER( GPIODR8R ) \
    MAKE_REGISTER( GPIOODR ) \
    MAKE_REGISTER( GPIOPUR ) \
    MAKE_REGISTER( GPIOPDR ) \
    MAKE_REGISTER( GPIOSLR ) \
    MAKE_REGISTER( GPIODEN ) \
    MAKE_REGISTER( GPIOLOCK ) \
    MAKE_REGISTER( GPIOCR ) \
    MAKE_REGISTER( GPIOAMSEL ) \
    MAKE_REGISTER( GPIOPCTL ) \
    MAKE_REGISTER( GPIOADCCTL ) \
    MAKE_REGISTER( GPIODMACTL ) \
    MAKE_REGISTER( GPIOPeriphID4 ) \
    MAKE_REGISTER( GPIOPeriphID5 ) \
    MAKE_REGISTER( GPIOPeriphID6 ) \
    MAKE_REGISTER( GPIOPeriphID7 ) \
    MAKE_REGISTER( GPIOPeriphID0 ) \
    MAKE_REGISTER( GPIOPeriphID1 ) \
    MAKE_REGISTER( GPIOPeriphID2 ) \
    MAKE_REGISTER( GPIOPeriphID3 ) \
    MAKE_REGISTER( GPIOPCellID0 ) \
    MAKE_REGISTER( GPIOPCellID1 ) \
    MAKE_REGISTER( GPIOPCellID2 ) \
    MAKE_REGISTER( GPIOPCellID3 ) \
	MAKE_REGISTER( DMASTAT        ) \
    MAKE_REGISTER( DMACFG         ) \
    MAKE_REGISTER( DMACTLBASE     ) \
    MAKE_REGISTER( DMAALTBASE     ) \
    MAKE_REGISTER( DMAWAITSTAT    ) \
    MAKE_REGISTER( DMASWREQ       ) \
    MAKE_REGISTER( DMAUSEBURSTSET ) \
    MAKE_REGISTER( DMAUSEBURSTCLR ) \
    MAKE_REGISTER( DMAREQMASKSET  ) \
    MAKE_REGISTER( DMAREQMASKCLR  ) \
    MAKE_REGISTER( DMAENASET      ) \
    MAKE_REGISTER( DMAENACLR      ) \
    MAKE_REGISTER( DMAALTSET      ) \
    MAKE_REGISTER( DMAALTCLR      ) \
    MAKE_REGISTER( DMAPRIOSET     ) \
    MAKE_REGISTER( DMAPRIOCLR     ) \
    MAKE_REGISTER( DMAERRCLR      ) \
    MAKE_REGISTER( DMACHASGN      ) \
    MAKE_REGISTER( DMACHIS        ) \
    MAKE_REGISTER( DMACHMAP0      ) \
    MAKE_REGISTER( DMACHMAP1      ) \
    MAKE_REGISTER( DMACHMAP2      ) \
    MAKE_REGISTER( DMACHMAP3      ) \
    MAKE_REGISTER( DMAPeriphID4   ) \
    MAKE_REGISTER( DMAPeriphID0   ) \
    MAKE_REGISTER( DMAPeriphID1   ) \
    MAKE_REGISTER( DMAPeriphID2   ) \
    MAKE_REGISTER( DMAPeriphID3   ) \
    MAKE_REGISTER( DMAPCellID0    ) \
    MAKE_REGISTER( DMAPCellID1    ) \
    MAKE_REGISTER( DMAPCellID2    ) \
    MAKE_REGISTER( DMAPCellID3    ) \
    MAKE_REGISTER( GPTMCFG      ) \
    MAKE_REGISTER( GPTMTAMR     ) \
    MAKE_REGISTER( GPTMTBMR     ) \
    MAKE_REGISTER( GPTMCTL      ) \
    MAKE_REGISTER( GPTMSYNC     ) \
    MAKE_REGISTER( GPTMIMR      ) \
    MAKE_REGISTER( GPTMRIS      ) \
    MAKE_REGISTER( GPTMMIS      ) \
    MAKE_REGISTER( GPTMICR      ) \
    MAKE_REGISTER( GPTMTAILR    ) \
    MAKE_REGISTER( GPTMTBILR    ) \
    MAKE_REGISTER( GPTMTAMATCHR ) \
    MAKE_REGISTER( GPTMTBMATCHR ) \
    MAKE_REGISTER( GPTMTAPR     ) \
    MAKE_REGISTER( GPTMTBPR     ) \
    MAKE_REGISTER( GPTMTAPMR    ) \
    MAKE_REGISTER( GPTMTBPMR    ) \
    MAKE_REGISTER( GPTMTAR      ) \
    MAKE_REGISTER( GPTMTBR      ) \
    MAKE_REGISTER( GPTMTAV      ) \
    MAKE_REGISTER( GPTMTBV      ) \
    MAKE_REGISTER( GPTMRTCPD    ) \
    MAKE_REGISTER( GPTMTAPS     ) \
    MAKE_REGISTER( GPTMTBPS     ) \
    MAKE_REGISTER( GPTMTAPV     ) \
    MAKE_REGISTER( GPTMTBPV     ) \
    MAKE_REGISTER( GPTMPP       ) \
    MAKE_REGISTER( WDTLOAD      ) \
    MAKE_REGISTER( WDTVALUE     ) \
    MAKE_REGISTER( WDTCTL       ) \
    MAKE_REGISTER( WDTRIS       ) \
    MAKE_REGISTER( WDTMIS       ) \
    MAKE_REGISTER( WDTTEST      ) \
    MAKE_REGISTER( WDTLOCK      ) \
    MAKE_REGISTER( WDTPeriphID4 ) \
    MAKE_REGISTER( WDTPeriphID5 ) \
    MAKE_REGISTER( WDTPeriphID6 ) \
    MAKE_REGISTER( WDTPeriphID7 ) \
    MAKE_REGISTER( WDTPeriphID0 ) \
    MAKE_REGISTER( WDTPeriphID1 ) \
    MAKE_REGISTER( WDTPeriphID2 ) \
    MAKE_REGISTER( WDTPeriphID3 ) \
    MAKE_REGISTER( WDTPCellID0  ) \
    MAKE_REGISTER( WDTPCellID1  ) \
    MAKE_REGISTER( WDTPCellID2  ) \
    MAKE_REGISTER( WDTPCellID3  ) \
    MAKE_REGISTER( ADCACTSS     ) \
    MAKE_REGISTER( ADCRIS       ) \
    MAKE_REGISTER( ADCIM        ) \
    MAKE_REGISTER( ADCISC       ) \
    MAKE_REGISTER( ADCOSTAT     ) \
    MAKE_REGISTER( ADCEMUX      ) \
    MAKE_REGISTER( ADCUSTAT     ) \
    MAKE_REGISTER( ADCSSPRI     ) \
    MAKE_REGISTER( ADCSPC       ) \
    MAKE_REGISTER( ADCPSSI      ) \
    MAKE_REGISTER( ADCSAC       ) \
    MAKE_REGISTER( ADCDCISC     ) \
    MAKE_REGISTER( ADCCTL       ) \
    MAKE_REGISTER( ADCSSMUX0    ) \
    MAKE_REGISTER( ADCSSCTL0    ) \
    MAKE_REGISTER( ADCSSFIFO0   ) \
    MAKE_REGISTER( ADCSSFSTAT0  ) \
    MAKE_REGISTER( ADCSSOP0     ) \
    MAKE_REGISTER( ADCSSDC0     ) \
    MAKE_REGISTER( ADCSSMUX1    ) \
    MAKE_REGISTER( ADCSSCTL1    ) \
    MAKE_REGISTER( ADCSSFIFO1   ) \
    MAKE_REGISTER( ADCSSFSTAT1  ) \
    MAKE_REGISTER( ADCSSOP1     ) \
    MAKE_REGISTER( ADCSSDC1     ) \
    MAKE_REGISTER( ADCSSMUX2    ) \
    MAKE_REGISTER( ADCSSCTL2    ) \
    MAKE_REGISTER( ADCSSFIFO2   ) \
    MAKE_REGISTER( ADCSSFSTAT2  ) \
    MAKE_REGISTER( ADCSSOP2     ) \
    MAKE_REGISTER( ADCSSDC2     ) \
    MAKE_REGISTER( ADCSSMUX3    ) \
    MAKE_REGISTER( ADCSSCTL3    ) \
    MAKE_REGISTER( ADCSSFIFO3   ) \
    MAKE_REGISTER( ADCSSFSTAT3  ) \
    MAKE_REGISTER( ADCSSOP3     ) \
    MAKE_REGISTER( ADCSSDC3     ) \
    MAKE_REGISTER( ADCDCRIC     ) \
    MAKE_REGISTER( ADCDCCTL0    ) \
    MAKE_REGISTER( ADCDCCTL1    ) \
    MAKE_REGISTER( ADCDCCTL2    ) \
    MAKE_REGISTER( ADCDCCTL3    ) \
    MAKE_REGISTER( ADCDCCTL4    ) \
    MAKE_REGISTER( ADCDCCTL5    ) \
    MAKE_REGISTER( ADCDCCTL6    ) \
    MAKE_REGISTER( ADCDCCTL7    ) \
    MAKE_REGISTER( ADCDCCMP0    ) \
    MAKE_REGISTER( ADCDCCMP1    ) \
    MAKE_REGISTER( ADCDCCMP2    ) \
    MAKE_REGISTER( ADCDCCMP3    ) \
    MAKE_REGISTER( ADCDCCMP4    ) \
    MAKE_REGISTER( ADCDCCMP5    ) \
    MAKE_REGISTER( ADCDCCMP6    ) \
    MAKE_REGISTER( ADCDCCMP7    ) \
    MAKE_REGISTER( ADCPP        ) \
    MAKE_REGISTER( ADCPC        ) \
    MAKE_REGISTER( ADCCC        ) \
    MAKE_REGISTER( UARTDR        ) \
    MAKE_REGISTER( UARTFR        ) \
    MAKE_REGISTER( UARTILPR      ) \
    MAKE_REGISTER( UARTIBRD      ) \
    MAKE_REGISTER( UARTFBRD      ) \
    MAKE_REGISTER( UARTLCRH      ) \
    MAKE_REGISTER( UARTCTL       ) \
    MAKE_REGISTER( UARTIFLS      ) \
    MAKE_REGISTER( UARTIM        ) \
    MAKE_REGISTER( UARTRIS       ) \
    MAKE_REGISTER( UARTMIS       ) \
    MAKE_REGISTER( UARTICR       ) \
    MAKE_REGISTER( UARTDMACTL    ) \
    MAKE_REGISTER( UART9BITADDR  ) \
    MAKE_REGISTER( UART9BITAMASK ) \
    MAKE_REGISTER( UARTPP        ) \
    MAKE_REGISTER( UARTCC        ) \
    MAKE_REGISTER( UARTPeriphID4 ) \
    MAKE_REGISTER( UARTPeriphID5 ) \
    MAKE_REGISTER( UARTPeriphID6 ) \
    MAKE_REGISTER( UARTPeriphID7 ) \
    MAKE_REGISTER( UARTPeriphID0 ) \
    MAKE_REGISTER( UARTPeriphID1 ) \
    MAKE_REGISTER( UARTPeriphID2 ) \
    MAKE_REGISTER( UARTPeriphID3 ) \
    MAKE_REGISTER( UARTPCellID0  ) \
    MAKE_REGISTER( UARTPCellID1  ) \
    MAKE_REGISTER( UARTPCellID2  ) \
    MAKE_REGISTER( UARTPCellID3  ) \
    MAKE_REGISTER( SSICR0        ) \
    MAKE_REGISTER( SSICR1        ) \
    MAKE_REGISTER( SSIDR         ) \
    MAKE_REGISTER( SSISR         ) \
    MAKE_REGISTER( SSICPSR       ) \
    MAKE_REGISTER( SSIIM         ) \
    MAKE_REGISTER( SSIRIS        ) \
    MAKE_REGISTER( SSIMIS        ) \
    MAKE_REGISTER( SSIICR        ) \
    MAKE_REGISTER( SSIDMACTL     ) \
    MAKE_REGISTER( SSICC         ) \
    MAKE_REGISTER( SSIPeriphID4  ) \
    MAKE_REGISTER( SSIPeriphID5  ) \
    MAKE_REGISTER( SSIPeriphID6  ) \
    MAKE_REGISTER( SSIPeriphID7  ) \
    MAKE_REGISTER( SSIPeriphID0  ) \
    MAKE_REGISTER( SSIPeriphID1  ) \
    MAKE_REGISTER( SSIPeriphID2  ) \
    MAKE_REGISTER( SSIPeriphID3  ) \
    MAKE_REGISTER( SSIPCellID0   ) \
    MAKE_REGISTER( SSIPCellID1   ) \
    MAKE_REGISTER( SSIPCellID2   ) \
    MAKE_REGISTER( SSIPCellID3   ) \
    MAKE_REGISTER( I2CMSA        ) \
    MAKE_REGISTER( I2CMCS        ) \
    MAKE_REGISTER( I2CMDR        ) \
    MAKE_REGISTER( I2CMTPR       ) \
    MAKE_REGISTER( I2CMIMR       ) \
    MAKE_REGISTER( I2CMRIS       ) \
    MAKE_REGISTER( I2CMMIS       ) \
    MAKE_REGISTER( I2CMICR       ) \
    MAKE_REGISTER( I2CMCR        ) \
    MAKE_REGISTER( I2CMCLKOCNT   ) \
    MAKE_REGISTER( I2CMBMON      ) \
    MAKE_REGISTER( I2CMCR2       ) \
    MAKE_REGISTER( I2CSOAR       ) \
    MAKE_REGISTER( I2CSCSR       ) \
    MAKE_REGISTER( I2CSDR        ) \
    MAKE_REGISTER( I2CSIMR       ) \
    MAKE_REGISTER( I2CSRIS       ) \
    MAKE_REGISTER( I2CSMIS       ) \
    MAKE_REGISTER( I2CSICR       ) \
    MAKE_REGISTER( I2CSOAR2      ) \
    MAKE_REGISTER( I2CSACKCTL    ) \
    MAKE_REGISTER( I2CPP         ) \
    MAKE_REGISTER( I2CPC         ) \
    MAKE_REGISTER( CANCTL        ) \
    MAKE_REGISTER( CANSTS        ) \
    MAKE_REGISTER( CANERR        ) \
    MAKE_REGISTER( CANBIT        ) \
    MAKE_REGISTER( CANINT        ) \
    MAKE_REGISTER( CANTST        ) \
    MAKE_REGISTER( CANBRPE       ) \
    MAKE_REGISTER( CANIF1CRQ     ) \
    MAKE_REGISTER( CANIF1CMSK    ) \
    MAKE_REGISTER( CANIF1MSK1    ) \
    MAKE_REGISTER( CANIF1MSK2    ) \
    MAKE_REGISTER( CANIF1ARB1    ) \
    MAKE_REGISTER( CANIF1ARB2    ) \
    MAKE_REGISTER( CANIF1MCTL    ) \
    MAKE_REGISTER( CANIF1DA1     ) \
    MAKE_REGISTER( CANIF1DA2     ) \
    MAKE_REGISTER( CANIF1DB1     ) \
    MAKE_REGISTER( CANIF1DB2     ) \
    MAKE_REGISTER( CANIF2CRQ     ) \
    MAKE_REGISTER( CANIF2CMSK    ) \
    MAKE_REGISTER( CANIF2MSK1    ) \
    MAKE_REGISTER( CANIF2MSK2    ) \
    MAKE_REGISTER( CANIF2ARB1    ) \
    MAKE_REGISTER( CANIF2ARB2    ) \
    MAKE_REGISTER( CANIF2MCTL    ) \
    MAKE_REGISTER( CANIF2DA1     ) \
    MAKE_REGISTER( CANIF2DA2     ) \
    MAKE_REGISTER( CANIF2DB1     ) \
    MAKE_REGISTER( CANIF2DB2     ) \
    MAKE_REGISTER( CANTXRQ1      ) \
    MAKE_REGISTER( CANTXRQ2      ) \
    MAKE_REGISTER( CANNWDA1      ) \
    MAKE_REGISTER( CANNWDA2      ) \
    MAKE_REGISTER( CANMSG1INT    ) \
    MAKE_REGISTER( CANMSG2INT    ) \
    MAKE_REGISTER( CANMSG1VAL    ) \
    MAKE_REGISTER( CANMSG2VAL    ) \
	MAKE_REGISTER( USBFADDR        ) \
    MAKE_REGISTER( USBPOWER        ) \
    MAKE_REGISTER( USBTXIS         ) \
    MAKE_REGISTER( USBRXIS         ) \
    MAKE_REGISTER( USBTXIE         ) \
    MAKE_REGISTER( USBRXIE         ) \
    MAKE_REGISTER( USBIS           ) \
    MAKE_REGISTER( USBIE           ) \
    MAKE_REGISTER( USBFRAME        ) \
    MAKE_REGISTER( USBEPIDX        ) \
    MAKE_REGISTER( USBTEST         ) \
    MAKE_REGISTER( USBFIFO0        ) \
    MAKE_REGISTER( USBFIFO1        ) \
    MAKE_REGISTER( USBFIFO2        ) \
    MAKE_REGISTER( USBFIFO3        ) \
    MAKE_REGISTER( USBFIFO4        ) \
    MAKE_REGISTER( USBFIFO5        ) \
    MAKE_REGISTER( USBFIFO6        ) \
    MAKE_REGISTER( USBFIFO7        ) \
    MAKE_REGISTER( USBTXFIFOSZ     ) \
    MAKE_REGISTER( USBRXFIFOSZ     ) \
    MAKE_REGISTER( USBTXFIFOADD    ) \
    MAKE_REGISTER( USBRXFIFOADD    ) \
    MAKE_REGISTER( USBCONTIM       ) \
    MAKE_REGISTER( USBFSEOF        ) \
    MAKE_REGISTER( USBCSRL0        ) \
    MAKE_REGISTER( USBCSRH0        ) \
    MAKE_REGISTER( USBCOUNT0       ) \
    MAKE_REGISTER( USBTXMAXP1      ) \
    MAKE_REGISTER( USBTXCSRL1      ) \
    MAKE_REGISTER( USBTXCSRH1      ) \
    MAKE_REGISTER( USBRXMAXP1      ) \
    MAKE_REGISTER( USBRXCSRL1      ) \
    MAKE_REGISTER( USBRXCSRH1      ) \
    MAKE_REGISTER( USBRXCOUNT1     ) \
    MAKE_REGISTER( USBTXMAXP2      ) \
    MAKE_REGISTER( USBTXCSRL2      ) \
    MAKE_REGISTER( USBTXCSRH2      ) \
    MAKE_REGISTER( USBRXMAXP2      ) \
    MAKE_REGISTER( USBRXCSRL2      ) \
    MAKE_REGISTER( USBRXCSRH2      ) \
    MAKE_REGISTER( USBRXCOUNT2     ) \
    MAKE_REGISTER( USBTXMAXP3      ) \
    MAKE_REGISTER( USBTXCSRL3      ) \
    MAKE_REGISTER( USBTXCSRH3      ) \
    MAKE_REGISTER( USBRXMAXP3      ) \
    MAKE_REGISTER( USBRXCSRL3      ) \
    MAKE_REGISTER( USBRXCSRH3      ) \
    MAKE_REGISTER( USBRXCOUNT3     ) \
    MAKE_REGISTER( USBTXMAXP4      ) \
    MAKE_REGISTER( USBTXCSRL4      ) \
    MAKE_REGISTER( USBTXCSRH4      ) \
    MAKE_REGISTER( USBRXMAXP4      ) \
    MAKE_REGISTER( USBRXCSRL4      ) \
    MAKE_REGISTER( USBRXCSRH4      ) \
    MAKE_REGISTER( USBRXCOUNT4     ) \
    MAKE_REGISTER( USBTXMAXP5      ) \
    MAKE_REGISTER( USBTXCSRL5      ) \
    MAKE_REGISTER( USBTXCSRH5      ) \
    MAKE_REGISTER( USBRXMAXP5      ) \
    MAKE_REGISTER( USBRXCSRL5      ) \
    MAKE_REGISTER( USBRXCSRH5      ) \
    MAKE_REGISTER( USBRXCOUNT5     ) \
    MAKE_REGISTER( USBTXMAXP6      ) \
    MAKE_REGISTER( USBTXCSRL6      ) \
    MAKE_REGISTER( USBTXCSRH6      ) \
    MAKE_REGISTER( USBRXMAXP6      ) \
    MAKE_REGISTER( USBRXCSRL6      ) \
    MAKE_REGISTER( USBRXCSRH6      ) \
    MAKE_REGISTER( USBRXCOUNT6     ) \
    MAKE_REGISTER( USBTXMAXP7      ) \
    MAKE_REGISTER( USBTXCSRL7      ) \
    MAKE_REGISTER( USBTXCSRH7      ) \
    MAKE_REGISTER( USBRXMAXP7      ) \
    MAKE_REGISTER( USBRXCSRL7      ) \
    MAKE_REGISTER( USBRXCSRH7      ) \
    MAKE_REGISTER( USBRXCOUNT7     ) \
    MAKE_REGISTER( USBRXDPKTBUFDIS ) \
    MAKE_REGISTER( USBTXDPKTBUFDIS ) \
    MAKE_REGISTER( USBDRRIS        ) \
    MAKE_REGISTER( USBDRIM         ) \
    MAKE_REGISTER( USBDRISC        ) \
    MAKE_REGISTER( USBDMASEL       ) \
    MAKE_REGISTER( USBPP           ) \
    MAKE_REGISTER( ACMIS           ) \
    MAKE_REGISTER( ACRIS           ) \
    MAKE_REGISTER( ACINTEN         ) \
    MAKE_REGISTER( ACREFCTL        ) \
    MAKE_REGISTER( ACSTAT0         ) \
    MAKE_REGISTER( ACCTL0          ) \
    MAKE_REGISTER( ACSTAT1         ) \
    MAKE_REGISTER( ACCTL1          ) \
    MAKE_REGISTER( ACMPPP          ) \

/* END OF SECTION */
#undef _________________________________________LIST_OF_REGISTERS_SECTION_________________________________________________________________



#endif /* SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_REGISTERS_DEFS_H_ */
