/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions for interrupts module
 *
 * @file       oc_interrupts_defs.h
 *
 * @author     Kamil Drobienko
 *
 * @note       Copyright (C) 2016 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_INTERRUPTS_DEFS_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_INTERRUPTS_DEFS_H_

#include <oc_1word.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits needed for storing interrupt priority
 *
 * This definition stores number of bits, that are used for storing priority of interrupts
 */
//==========================================================================================================================================
#define oC_MACHINE_PRIO_BITS                        3

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Name of default interrupt handler function
 */
//==========================================================================================================================================
#define oC_DEFAULT_INTERRUPT_HANDLER_NAME                   oC_DefaultInterrupt

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Maximum value for interrupt priority
 */
//==========================================================================================================================================
#define oC_MAXIMUM_INTERRUPT_PRIORITY               0
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Minimum value for interrupt priority
 */
//==========================================================================================================================================
#define oC_MINIMUM_INTERRUPT_PRIORITY               0xFF

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief types of interrupts
 *
 * Each interrupt should be connected with some base address or defined as "System" interrupt. To distinguish interrupts connected to the same
 * base address, there is a interrupt type.
 *
 * This definition is a list of interrupt types needed for the the machine.
 *
 * To add new interrupt type, use:
 * @code{.c} ADD_INTERRUPT_TYPE( INTERRUPT_NAME )\ @endcode
 *
 * Where:
 *              INTERRUPT_NAME      - any word as name of the interrupt type
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERRUPTS_TYPES_LIST(ADD_INTERRUPT_TYPE)     \
    ADD_INTERRUPT_TYPE( NonMaskableInterrupt     )\
    ADD_INTERRUPT_TYPE( HardFault                )\
    ADD_INTERRUPT_TYPE( MemoryManagement         )\
    ADD_INTERRUPT_TYPE( BusFault                 )\
    ADD_INTERRUPT_TYPE( UsageFault               )\
    ADD_INTERRUPT_TYPE( SVCall                   )\
    ADD_INTERRUPT_TYPE( DebugMonitor             )\
    ADD_INTERRUPT_TYPE( PendSV                   )\
    ADD_INTERRUPT_TYPE( SysTick                  )\
    ADD_INTERRUPT_TYPE( PeripheralInterrupt      )\
    ADD_INTERRUPT_TYPE( ADCSequence0             )\
    ADD_INTERRUPT_TYPE( ADCSequence1             )\
    ADD_INTERRUPT_TYPE( ADCSequence2             )\
    ADD_INTERRUPT_TYPE( ADCSequence3             )\
    ADD_INTERRUPT_TYPE( TimerA                   )\
    ADD_INTERRUPT_TYPE( TimerB                   )\
    ADD_INTERRUPT_TYPE( SystemControl            )\
    ADD_INTERRUPT_TYPE( FlashMemCtlAndE2ROMCtl   )\
    ADD_INTERRUPT_TYPE( HibernationModule        )\
    ADD_INTERRUPT_TYPE( USB                      )\
    ADD_INTERRUPT_TYPE( Software                 )\
    ADD_INTERRUPT_TYPE( Error                    )\
    ADD_INTERRUPT_TYPE( SystemException          )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of interrupts
 *
 * Each interrupt should be connected with some base address or defined as "System" interrupt. To distinguish interrupts connected to the same
 * base address, there is a interrupt type.
 *
 * This definition is a list of interrupts in the machine.
 *
 * To add new interrupt use:
 * @code{.c} ADD_INTERRUPT( BASE_NAME , INTERRUPT_TYPE_NAME , INTERRUPT_NUMBER , VECTOR_INDEX )\ @endcode
 *
 * Where:
 *              BASE_NAME               - Name of base address, that the interrupt is connected to. The base address with this name must exist in the oc_ba_defs.h file
 *              INTERRUPT_TYPE_NAME     - Name of type from #oC_MACHINE_INTERRUPTS_TYPES_LIST definition. Helps to distinguish more interrupts on the same base address
 *              INTERRUPT_NUMBER        - Any number of interrupt (needed for CMSIS)
 *              VECTOR_INDEX            - Index of this interrupt in interrupt vector
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERRUPTS_LIST(ADD_INTERRUPT) \
    ADD_INTERRUPT( System     , NonMaskableInterrupt        , -14 ,   2  )\
    ADD_INTERRUPT( System     , HardFault                   , -13 ,   3  )\
    ADD_INTERRUPT( System     , MemoryManagement            , -12 ,   4  )\
    ADD_INTERRUPT( System     , BusFault                    , -11 ,   5  )\
    ADD_INTERRUPT( System     , UsageFault                  , -10 ,   6  )\
    ADD_INTERRUPT( System     , SVCall                      , -5  ,  11  )\
    ADD_INTERRUPT( System     , DebugMonitor                , -4  ,  12  )\
    ADD_INTERRUPT( System     , PendSV                      , -2  ,  14  )\
    ADD_INTERRUPT( System     , SysTick                     , -1  ,  15  )\
    ADD_INTERRUPT( PORTA      , PeripheralInterrupt         , 0   ,  16  )\
    ADD_INTERRUPT( PORTB      , PeripheralInterrupt         , 1   ,  17  )\
    ADD_INTERRUPT( PORTC      , PeripheralInterrupt         , 2   ,  18  )\
    ADD_INTERRUPT( PORTD      , PeripheralInterrupt         , 3   ,  19  )\
    ADD_INTERRUPT( PORTE      , PeripheralInterrupt         , 4   ,  20  )\
    ADD_INTERRUPT( UART0      , PeripheralInterrupt         , 5   ,  21  )\
    ADD_INTERRUPT( UART1      , PeripheralInterrupt         , 6   ,  22  )\
    ADD_INTERRUPT( SSI0       , PeripheralInterrupt         , 7   ,  23  )\
    ADD_INTERRUPT( I2C0       , PeripheralInterrupt         , 8   ,  24  )\
    ADD_INTERRUPT( ADC0       , ADCSequence0                , 14  ,  30  )\
    ADD_INTERRUPT( ADC0       , ADCSequence1                , 15  ,  31  )\
    ADD_INTERRUPT( ADC0       , ADCSequence2                , 16  ,  32  )\
    ADD_INTERRUPT( ADC0       , ADCSequence3                , 17  ,  33  )\
    ADD_INTERRUPT( WDT0       , PeripheralInterrupt         , 18  ,  34  )\
    ADD_INTERRUPT( WDT1       , PeripheralInterrupt         , 18  ,  34  )\
    ADD_INTERRUPT( Timer0     , TimerA                      , 19  ,  35  )\
    ADD_INTERRUPT( Timer0     , TimerB                      , 20  ,  36  )\
    ADD_INTERRUPT( Timer1     , TimerA                      , 21  ,  37  )\
    ADD_INTERRUPT( Timer1     , TimerB                      , 22  ,  38  )\
    ADD_INTERRUPT( Timer2     , TimerA                      , 23  ,  39  )\
    ADD_INTERRUPT( Timer2     , TimerB                      , 24  ,  40  )\
    ADD_INTERRUPT( ACOM0      , PeripheralInterrupt         , 25  ,  41  )\
    ADD_INTERRUPT( System     , SystemControl               , 28  ,  44  )\
    ADD_INTERRUPT( System     , FlashMemCtlAndE2ROMCtl      , 29  ,  45  )\
    ADD_INTERRUPT( PORTF      , PeripheralInterrupt         , 30  ,  46  )\
    ADD_INTERRUPT( UART2      , PeripheralInterrupt         , 33  ,  49  )\
    ADD_INTERRUPT( SSI1       , PeripheralInterrupt         , 34  ,  50  )\
    ADD_INTERRUPT( Timer3     , TimerA                      , 35  ,  51  )\
    ADD_INTERRUPT( Timer3     , TimerB                      , 36  ,  52  )\
    ADD_INTERRUPT( I2C1       , PeripheralInterrupt         , 37  ,  53  )\
    ADD_INTERRUPT( CAN0       , PeripheralInterrupt         , 39  ,  55  )\
    ADD_INTERRUPT( System     , HibernationModule           , 43  ,  59  )\
    ADD_INTERRUPT( USB0       , PeripheralInterrupt         , 44  ,  60  )\
    ADD_INTERRUPT( uDMA0      , Software                    , 46  ,  62  )\
    ADD_INTERRUPT( uDMA0      , Error                       , 47  ,  63  )\
    ADD_INTERRUPT( ADC1       , ADCSequence0                , 48  ,  64  )\
    ADD_INTERRUPT( ADC1       , ADCSequence1                , 49  ,  65  )\
    ADD_INTERRUPT( ADC1       , ADCSequence2                , 50  ,  66  )\
    ADD_INTERRUPT( ADC1       , ADCSequence3                , 51  ,  67  )\
    ADD_INTERRUPT( SSI2       , PeripheralInterrupt         , 57  ,  73  )\
    ADD_INTERRUPT( SSI3       , PeripheralInterrupt         , 58  ,  74  )\
    ADD_INTERRUPT( UART3      , PeripheralInterrupt         , 59  ,  75  )\
    ADD_INTERRUPT( UART4      , PeripheralInterrupt         , 60  ,  76  )\
    ADD_INTERRUPT( UART5      , PeripheralInterrupt         , 61  ,  77  )\
    ADD_INTERRUPT( UART6      , PeripheralInterrupt         , 62  ,  78  )\
    ADD_INTERRUPT( UART7      , PeripheralInterrupt         , 63  ,  79  )\
    ADD_INTERRUPT( I2C2       , PeripheralInterrupt         , 68  ,  84  )\
    ADD_INTERRUPT( I2C3       , PeripheralInterrupt         , 69  ,  85  )\
    ADD_INTERRUPT( Timer4     , TimerA                      , 70  ,  86  )\
    ADD_INTERRUPT( Timer4     , TimerB                      , 71  ,  87  )\
    ADD_INTERRUPT( Timer5     , TimerA                      , 92  , 108  )\
    ADD_INTERRUPT( Timer5     , TimerB                      , 93  , 109  )\
    ADD_INTERRUPT( WideTimer0 , TimerA                      , 94  , 110  )\
    ADD_INTERRUPT( WideTimer0 , TimerB                      , 95  , 111  )\
    ADD_INTERRUPT( WideTimer1 , TimerA                      , 96  , 112  )\
    ADD_INTERRUPT( WideTimer1 , TimerB                      , 97  , 113  )\
    ADD_INTERRUPT( WideTimer2 , TimerA                      , 98  , 114  )\
    ADD_INTERRUPT( WideTimer2 , TimerB                      , 99  , 115  )\
    ADD_INTERRUPT( WideTimer3 , TimerA                      , 100 , 116  )\
    ADD_INTERRUPT( WideTimer3 , TimerB                      , 101 , 117  )\
    ADD_INTERRUPT( WideTimer4 , TimerA                      , 102 , 118  )\
    ADD_INTERRUPT( WideTimer4 , TimerB                      , 103 , 119  )\
    ADD_INTERRUPT( WideTimer5 , TimerA                      , 104 , 120  )\
    ADD_INTERRUPT( WideTimer5 , TimerB                      , 105 , 121  )\
    ADD_INTERRUPT( System     , SystemException             , 106 , 122  )\


#endif /* SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_REGISTERS_DEFS_H_ */
