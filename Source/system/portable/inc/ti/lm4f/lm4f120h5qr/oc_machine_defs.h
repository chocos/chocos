/** @hideinitializer ****************************************************************************************************************************************
 *
 * @file       oc_machine_defs.h
 *
 * @brief      Contains definitions for the uC
 *
 * @author     Patryk Kubiak - (Created on: 11 04 2015 20:08:10)
 *             Kamil Drobienko
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *                                Kamil Drobienko <kamildrobienko@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup MDefs Machine Definitions (MDefs)
 * @{
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_LM4FH5QR_OC_MACHINE_DEFS_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_LM4FH5QR_OC_MACHINE_DEFS_H_

#include <oc_machines_list.h>
#include <oc_frequency.h>

/** @hideinitializer ****************************************************************************************************************************************
 * The section CPU definitions
 */
#define _________________________________________CPU_DEFINITIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Definition that contains name of the machine
 */
//==========================================================================================================================================
#define oC_MACHINE                  LM4F120H5QR

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Family of the machine
 */
//==========================================================================================================================================
#define oC_MACHINE_FAMILY           LM4F

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Name of the CPU CORTEX
 */
//==========================================================================================================================================
#define oC_MACHINE_CORTEX           ARM_Cortex_M4

//==========================================================================================================================================
/**
 * @hideinitializer
 * Number of bits to keep NVIC priority
 */
//==========================================================================================================================================
#define oC_MACHINE_PRIO_BITS        3

//==========================================================================================================================================
/**
 * @hideinitializer
 * Number of bytes, that memory is aligned
 */
//==========================================================================================================================================
#define oC_MACHINE_MEMORY_ALIGNMENT_BYTES      4

//==========================================================================================================================================
 /**
 * @hideinitializer
 * Number of bytes, that memory is aligned
 */
//==========================================================================================================================================
#define oC_MACHINE_STACK_TOP_ALIGNMENT_BYTES      8

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the precision internal oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_PRECISION_INTERNAL_OSCILLATOR_FREQUENCY  oC_MHz(16)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the internal oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY            oC_kHz(30)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the hibernation oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_HIBERNATION_OSCILLATOR_FREQUENCY         oC_Hz(32768)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Maximum possible frequency for system clock
 */
//==========================================================================================================================================
#define oC_MACHINE_MAXIMUM_FREQUENCY                        oC_MHz(80)


//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns default frequency of the machine clock
 */
//==========================================================================================================================================
#define oC_Machine_DefaultFrequency                     oC_MACHINE_PRECISION_INTERNAL_OSCILLATOR_FREQUENCY
//==========================================================================================================================================
/**
 * @hideinitializer
 * Initial value for the xPSR register
 */
//==========================================================================================================================================
#define oC_MACHINE_xPSR_INITIAL_VALUE                       0x01000000

//==========================================================================================================================================
/**
 * @hideinitializer
 * Flag if stack push command in this machine is decrementing address
 */
//==========================================================================================================================================
#define oC_MACHINE_STACK_PUSH_IS_DECREMENTING_ADDRESS         true

/* END OF SECTION */
#undef  _________________________________________CPU_DEFINITIONS_SECTION____________________________________________________________________

#define oC_MACHINE_GPIO_LOCK_KEY                (0x4C4F434BUL)
#define oC_Machine_GetDigitalFunctionOfPeripheralPin( PeripheralPin )               ( PeripheralPin & 0x7 )

/** ========================================================================================================================================
 *
 *              The section with DMA definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DMA_SECTION________________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * The definition contain types of DMA signals for #oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST
 *
 * To add signal use:
 *              ADD_SIGNAL( NAME )\
 *                          NAME - name of the signal to add
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_SIGNAL_TYPE_LIST(ADD_SIGNAL) \
    ADD_SIGNAL( EP1_RX ) \
    ADD_SIGNAL( EP1_TX ) \
    ADD_SIGNAL( EP2_RX ) \
    ADD_SIGNAL( EP2_TX )\
    ADD_SIGNAL( EP3_RX ) \
    ADD_SIGNAL( EP3_TX ) \
    ADD_SIGNAL( RX ) \
    ADD_SIGNAL( TX ) \
    ADD_SIGNAL( Default ) \
    ADD_SIGNAL( SS0 ) \
    ADD_SIGNAL( SS1 ) \
    ADD_SIGNAL( SS2 ) \
    ADD_SIGNAL( SS3 ) \
    ADD_SIGNAL( A ) \
    ADD_SIGNAL( B ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits needed for storing DMA signal type (length of oC_MACHINE_DMA_SIGNAL_TYPE_LIST)
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_SIGNAL_TYPE_WIDTH                4

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits needed for encoding value for DMA channel assignments
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_ENCODING_VALUE_WIDTH             4

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * The definition contains assignments of DMA channels.
 *
 * To add channel assignment use:
 *
 *          ADD_CHANNEL_ASSIGNMENT( DMA_REGISTER_MAP_NAME , REGISTER_MAP_NAME , SIGNAL_TYPE , TYPE , ENC )\
 *
 *                      where:
 *                                  DMA_CHANNEL           - name of DMA channel
 *                                  CHANNEL_NAME          - channel that is assigned to this (from UART for example)
 *                                  SIGNAL_TYPE           - type of the signal from #oC_MACHINE_DMA_SIGNAL_TYPE_LIST definition
 *                                  TYPE                  - indicate if particular peripheral uses a single request (S), burst request (B)
 *                                                          or both (SB)
 *                                  ENC                   - special value for register
 */
//==========================================================================================================================================

#define oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST(ADD_CHANNEL_ASSIGNMENT)   \
	ADD_CHANNEL_ASSIGNMENT( uDMA0  , USB0        , EP1_RX  , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , USB0        , EP1_TX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , USB0        , EP2_RX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , USB0        , EP2_TX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , USB0        , EP3_RX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , USB0        , EP3_TX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , UART0       , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , UART0       , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , SSI0        , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , SSI0        , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , ADC0        , SS0     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , ADC0        , SS1     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , ADC0        , SS2     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , ADC0        , SS3     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , Timer0      , A       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Timer0      , B       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Timer1      , A       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Timer1      , B       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , UART1       , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , UART1       , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , SSI1        , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , SSI1        , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , UART2       , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , UART2       , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Timer3      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Timer3      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , Timer2      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , Timer2      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , Timer2      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , Timer2      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , UART1       , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , UART1       , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , SSI1        , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , SSI1        , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , UART2       , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , UART2       , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , Timer2      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , Timer2      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , Timer1      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Timer1      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , ADC1        , SS0     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , ADC1        , SS1     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , ADC1        , SS2     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , ADC1        , SS3     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , UART5       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , UART5       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , UART6       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , UART6       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , SSI2        , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , SSI2        , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , SSI3        , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , SSI3        , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , UART3       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , UART3       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , UART4       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , UART4       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , UART7       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , UART7       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , Timer4      , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , Timer4      , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , PORTA       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , PORTB       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , PORTC       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , PORTD       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , Timer5      , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , Timer5      , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , WideTimer0  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , WideTimer0  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , WideTimer1  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , WideTimer1  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , PORTE       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , PORTF       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , WideTimer2  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , WideTimer2  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , PORTB       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , WideTimer3  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , WideTimer3  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , WideTimer4  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , WideTimer4  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , WideTimer5  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , WideTimer5  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 4)\


#undef  _________________________________________DMA_SECTION________________________________________________________________________________


#define _________________________________________SPI_SECTION________________________________________________________________________________

#define oC_MACHINE_SPI_MINIMUM_TRANSMISION_FREQUENCY  oC_MHz(2)
#define oC_MACHINE_SPI_MAXIMUM_TRANSMISION_FREQUENCY  oC_MHz(25)

#define oC_MACHINE_SPI_MINIMUM_FRAME_WIDTH            4
#define oC_MACHINE_SPI_MAXIMUM_FRAME_WIDTH            16

#undef _________________________________________SPI_SECTION________________________________________________________________________________

#endif /* SYSTEM_PORTABLE_TI_LM4F_LM4FH5QR_OC_MACHINE_DEFS_H_ */
