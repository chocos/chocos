/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions for Registers Maps module
 *
 * @file       oc_rmaps_defs.h
 *
 * @author     Kamil Drobienko
 *
 * @note       Copyright (C) 2016 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_MACHINE_RMAPSDEFS_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_MACHINE_RMAPSDEFS_H_


#include <oc_1word.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief macro for getting register map definition name
 *
 * The macro returns the name of the register map definition from this file. Even if it looks stupid, it is needed for other macros.
 *
 * For example:
 * @code{.c}
   #if defined(oC_REGISTER_MAP_(FLASH))
       // oC_REGISTER_MAP_FLASH is defined
   #endif
   @endcode
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_(REGISTER_MAP_NAME)                 oC_1WORD_FROM_2(oC_REGISTER_MAP_ , REGISTER_MAP_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in SYSTEM CONTROL register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_SYSTEM_CONTROL(MAKE_REGISTER)   \
    MAKE_REGISTER( DID0                            , 0x000UL , R ) \
    MAKE_REGISTER( DID1                            , 0x004UL , R ) \
    MAKE_REGISTER( PBORCTL                         , 0x030UL , RW ) \
    MAKE_REGISTER( RIS                             , 0x050UL , R ) \
    MAKE_REGISTER( IMC                             , 0x054UL , RW ) \
    MAKE_REGISTER( MISC                            , 0x058UL , R ) \
    MAKE_REGISTER( RESC                            , 0x05CUL , RW ) \
    MAKE_REGISTER( RCC                             , 0x060UL , RW ) \
    MAKE_REGISTER( GPIOHBCTL                       , 0x06CUL , RW ) \
    MAKE_REGISTER( RCC2                            , 0x070UL , RW ) \
    MAKE_REGISTER( MOSCCTL                         , 0x07CUL , RW ) \
    MAKE_REGISTER( DSLPCLKCFG                      , 0x144UL , RW ) \
    MAKE_REGISTER( SYSPROP                         , 0x14CUL , R ) \
    MAKE_REGISTER( PIOSCCAL                        , 0x150UL , RW ) \
    MAKE_REGISTER( PIOSCSTAT                       , 0x154UL , R ) \
    MAKE_REGISTER( PLLFREQ0                        , 0x160UL , R ) \
    MAKE_REGISTER( PLLFREQ1                        , 0x164UL , R ) \
    MAKE_REGISTER( PLLSTAT                         , 0x168UL , R ) \
    MAKE_REGISTER( PPWD                            , 0x300UL , R ) \
    MAKE_REGISTER( PPTIMER                         , 0x304UL , R ) \
    MAKE_REGISTER( PPGPIO                          , 0x308UL , R ) \
    MAKE_REGISTER( PPDMA                           , 0x30CUL , R ) \
    MAKE_REGISTER( PPHIB                           , 0x314UL , R ) \
    MAKE_REGISTER( PPSSI                           , 0x31CUL , R ) \
    MAKE_REGISTER( PPI2C                           , 0x320UL , R ) \
    MAKE_REGISTER( PPUSB                           , 0x328UL , R ) \
    MAKE_REGISTER( PPCAN                           , 0x334UL , R ) \
    MAKE_REGISTER( PPADC                           , 0x338UL , R ) \
    MAKE_REGISTER( PPACMP                          , 0x33CUL , R ) \
    MAKE_REGISTER( PPPWM                           , 0x340UL , R ) \
    MAKE_REGISTER( PPQEI                           , 0x344UL , R ) \
    MAKE_REGISTER( PPEEPROM                        , 0x358UL , R ) \
    MAKE_REGISTER( SRWD                            , 0x500UL , RW ) \
    MAKE_REGISTER( SRTIMER                         , 0x504UL , RW ) \
    MAKE_REGISTER( SRGPIO                          , 0x508UL , RW ) \
    MAKE_REGISTER( SRDMA                           , 0x50CUL , RW ) \
    MAKE_REGISTER( SRHIB                           , 0x514UL , RW ) \
    MAKE_REGISTER( SRSSI                           , 0x51CUL , RW ) \
    MAKE_REGISTER( SRI2C                           , 0x520UL , RW ) \
    MAKE_REGISTER( SRUSB                           , 0x528UL , RW ) \
    MAKE_REGISTER( SRCAN                           , 0x534UL , RW ) \
    MAKE_REGISTER( SRADC                           , 0x538UL , RW ) \
    MAKE_REGISTER( SRACMP                          , 0x53CUL , RW ) \
    MAKE_REGISTER( SREEPROM                        , 0x558UL , RW ) \
    MAKE_REGISTER( SRWTIMER                        , 0x55CUL , RW ) \
    MAKE_REGISTER( RCGCWD                          , 0x600UL , RW ) \
    MAKE_REGISTER( RCGCTIMER                       , 0x604UL , RW ) \
    MAKE_REGISTER( RCGCGPIO                        , 0x608UL , RW ) \
    MAKE_REGISTER( RCGCDMA                         , 0x60CUL , RW ) \
    MAKE_REGISTER( RCGCHIB                         , 0x614UL , RW ) \
    MAKE_REGISTER( RCGCUART                        , 0x618UL , RW ) \
    MAKE_REGISTER( RCGCSSI                         , 0x61CUL , RW ) \
    MAKE_REGISTER( RCGCI2C                         , 0x620UL , RW ) \
    MAKE_REGISTER( RCGCUSB                         , 0x628UL , RW ) \
    MAKE_REGISTER( RCGCCAN                         , 0x634UL , RW ) \
    MAKE_REGISTER( RCGCADC                         , 0x638UL , RW ) \
    MAKE_REGISTER( RCGCACMP                        , 0x63CUL , RW ) \
    MAKE_REGISTER( RCGCEEPROM                      , 0x658UL , RW ) \
    MAKE_REGISTER( RCGCWTIMER                      , 0x65CUL , RW ) \
    MAKE_REGISTER( SCGCWD                          , 0x700UL , RW ) \
    MAKE_REGISTER( SCGCHIB                         , 0x714UL , RW ) \
    MAKE_REGISTER( SCGCI2C                         , 0x720UL , RW ) \
    MAKE_REGISTER( SCGCUSB                         , 0x728UL , RW ) \
    MAKE_REGISTER( SCGCACMP                        , 0x73CUL , RW ) \
    MAKE_REGISTER( SCGCEEPROM                      , 0x758UL , RW ) \
    MAKE_REGISTER( DCGCWD                          , 0x800UL , RW ) \
    MAKE_REGISTER( DCGCHIB                         , 0x814UL , RW ) \
    MAKE_REGISTER( DCGCEEPROM                      , 0x858UL , RW ) \
    MAKE_REGISTER( PRWD                            , 0xA00UL , R ) \
    MAKE_REGISTER( PRTIMER                         , 0xA04UL , R ) \
    MAKE_REGISTER( PRGPIO                          , 0xA08UL , R ) \
    MAKE_REGISTER( PRDMA                           , 0xA0CUL , R ) \
    MAKE_REGISTER( PRHIB                           , 0xA14UL , R ) \
    MAKE_REGISTER( PRSSI                           , 0xA1CUL , R ) \
    MAKE_REGISTER( PRI2C                           , 0xA20UL , R ) \
    MAKE_REGISTER( PRUSB                           , 0xA28UL , R ) \
    MAKE_REGISTER( PRCAN                           , 0xA34UL , R ) \
    MAKE_REGISTER( PRADC                           , 0xA38UL , R ) \
    MAKE_REGISTER( PRACMP                          , 0xA3CUL , R ) \
    MAKE_REGISTER( PREEPROM                        , 0xA58UL , R ) \
    MAKE_REGISTER( PRWTIMER                        , 0xA5CUL , R ) \
    MAKE_REGISTER( DC0                             , 0x008UL , R ) \
    MAKE_REGISTER( DC1                             , 0x010UL , R ) \
    MAKE_REGISTER( DC2                             , 0x014UL , R ) \
    MAKE_REGISTER( DC3                             , 0x018UL , R ) \
    MAKE_REGISTER( DC4                             , 0x01CUL , R ) \
    MAKE_REGISTER( DC5                             , 0x020UL , R ) \
    MAKE_REGISTER( DC6                             , 0x024UL , R ) \
    MAKE_REGISTER( DC7                             , 0x028UL , R ) \
    MAKE_REGISTER( DC8                             , 0x02CUL , R ) \
    MAKE_REGISTER( SRCR0                           , 0x040UL , R ) \
    MAKE_REGISTER( SRCR1                           , 0x044UL , R ) \
    MAKE_REGISTER( SRCR2                           , 0x048UL , R ) \
    MAKE_REGISTER( RCGC0                           , 0x100UL , R ) \
    MAKE_REGISTER( RCGC1                           , 0x104UL , R ) \
    MAKE_REGISTER( RCGC2                           , 0x108UL , R ) \
    MAKE_REGISTER( SCGC0                           , 0x110UL , R ) \
    MAKE_REGISTER( SCGC1                           , 0x114UL , R ) \
    MAKE_REGISTER( SCGC2                           , 0x118UL , R ) \
    MAKE_REGISTER( DCGC0                           , 0x120UL , R ) \
    MAKE_REGISTER( DCGC1                           , 0x124UL , R ) \
    MAKE_REGISTER( DCGC2                           , 0x128UL , R ) \
    MAKE_REGISTER( DC9                             , 0x190UL , R ) \
    MAKE_REGISTER( NVMSTAT                         , 0x1A0UL , R ) \


//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in GPIO register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_GPIO(MAKE_REGISTER) \
    MAKE_REGISTER( GPIODATA             , 0x3FCUL , RW ) \
    MAKE_REGISTER( GPIODIR              , 0x400UL , RW ) \
    MAKE_REGISTER( GPIOIS               , 0x404UL , RW ) \
    MAKE_REGISTER( GPIOIBE              , 0x408UL , RW ) \
    MAKE_REGISTER( GPIOIEV              , 0x40CUL , RW ) \
    MAKE_REGISTER( GPIOIM               , 0x410UL , RW ) \
    MAKE_REGISTER( GPIORIS              , 0x414UL , R ) \
    MAKE_REGISTER( GPIOMIS              , 0x418UL , R ) \
    MAKE_REGISTER( GPIOICR              , 0x41CUL , RW ) \
    MAKE_REGISTER( GPIOAFSEL            , 0x420UL , RW ) \
    MAKE_REGISTER( GPIODR2R             , 0x500UL , RW ) \
    MAKE_REGISTER( GPIODR4R             , 0x504UL , RW ) \
    MAKE_REGISTER( GPIODR8R             , 0x508UL , RW ) \
    MAKE_REGISTER( GPIOODR              , 0x50CUL , RW ) \
    MAKE_REGISTER( GPIOPUR              , 0x510UL , RW ) \
    MAKE_REGISTER( GPIOPDR              , 0x514UL , RW ) \
    MAKE_REGISTER( GPIOSLR              , 0x518UL , RW ) \
    MAKE_REGISTER( GPIODEN              , 0x51CUL , RW ) \
    MAKE_REGISTER( GPIOLOCK             , 0x520UL , RW ) \
    MAKE_REGISTER( GPIOCR               , 0x524UL , R ) \
    MAKE_REGISTER( GPIOAMSEL            , 0x528UL , RW ) \
    MAKE_REGISTER( GPIOPCTL             , 0x52CUL , RW ) \
    MAKE_REGISTER( GPIOADCCTL           , 0x530UL , RW ) \
    MAKE_REGISTER( GPIODMACTL           , 0x534UL , RW ) \
    MAKE_REGISTER( GPIOPeriphID4        , 0xFD0UL , R ) \
    MAKE_REGISTER( GPIOPeriphID5        , 0xFD4UL , R ) \
    MAKE_REGISTER( GPIOPeriphID6        , 0xFD8UL , R ) \
    MAKE_REGISTER( GPIOPeriphID7        , 0xFDCUL , R ) \
    MAKE_REGISTER( GPIOPeriphID0        , 0xFE0UL , R ) \
    MAKE_REGISTER( GPIOPeriphID1        , 0xFE4UL , R ) \
    MAKE_REGISTER( GPIOPeriphID2        , 0xFE8UL , R ) \
    MAKE_REGISTER( GPIOPeriphID3        , 0xFECUL , R ) \
    MAKE_REGISTER( GPIOPCellID0         , 0xFF0UL , R ) \
    MAKE_REGISTER( GPIOPCellID1         , 0xFF4UL , R ) \
    MAKE_REGISTER( GPIOPCellID2         , 0xFF8UL , R ) \
    MAKE_REGISTER( GPIOPCellID3         , 0xFFCUL , R ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in DMA register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_DMA(MAKE_REGISTER) \
    MAKE_REGISTER( DMASTAT                         , 0x000UL , R ) \
    MAKE_REGISTER( DMACFG                          , 0x004UL , W ) \
    MAKE_REGISTER( DMACTLBASE                      , 0x008UL , RW ) \
    MAKE_REGISTER( DMAALTBASE                      , 0x00CUL , R ) \
    MAKE_REGISTER( DMAWAITSTAT                     , 0x010UL , R ) \
    MAKE_REGISTER( DMASWREQ                        , 0x014UL , W ) \
    MAKE_REGISTER( DMAUSEBURSTSET                  , 0x018UL , RW ) \
    MAKE_REGISTER( DMAUSEBURSTCLR                  , 0x01CUL , W ) \
    MAKE_REGISTER( DMAREQMASKSET                   , 0x020UL , RW ) \
    MAKE_REGISTER( DMAREQMASKCLR                   , 0x024UL , W ) \
    MAKE_REGISTER( DMAENASET                       , 0x028UL , RW ) \
    MAKE_REGISTER( DMAENACLR                       , 0x02CUL , W ) \
    MAKE_REGISTER( DMAALTSET                       , 0x030UL , RW ) \
    MAKE_REGISTER( DMAALTCLR                       , 0x034UL , W ) \
    MAKE_REGISTER( DMAPRIOSET                      , 0x038UL , RW ) \
    MAKE_REGISTER( DMAPRIOCLR                      , 0x03CUL , W ) \
    MAKE_REGISTER( DMAERRCLR                       , 0x04CUL , RW ) \
    MAKE_REGISTER( DMACHASGN                       , 0x500UL , RW ) \
    MAKE_REGISTER( DMACHIS                         , 0x504UL , RW ) \
    MAKE_REGISTER( DMACHMAP0                       , 0x510UL , RW ) \
    MAKE_REGISTER( DMACHMAP1                       , 0x514UL , RW ) \
    MAKE_REGISTER( DMACHMAP2                       , 0x518UL , RW ) \
    MAKE_REGISTER( DMACHMAP3                       , 0x51CUL , RW ) \
    MAKE_REGISTER( DMAPeriphID4                    , 0xFD0UL , R ) \
    MAKE_REGISTER( DMAPeriphID0                    , 0xFE0UL , R ) \
    MAKE_REGISTER( DMAPeriphID1                    , 0xFE4UL , R ) \
    MAKE_REGISTER( DMAPeriphID2                    , 0xFE8UL , R ) \
    MAKE_REGISTER( DMAPeriphID3                    , 0xFECUL , R ) \
    MAKE_REGISTER( DMAPCellID0                     , 0xFF0UL , R ) \
    MAKE_REGISTER( DMAPCellID1                     , 0xFF4UL , R ) \
    MAKE_REGISTER( DMAPCellID2                     , 0xFF8UL , R ) \
    MAKE_REGISTER( DMAPCellID3                     , 0xFFCUL , R ) \


//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in TIMER register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_TIMER(MAKE_REGISTER) \
    MAKE_REGISTER( GPTMCFG                         , 0x000UL , RW  ) \
    MAKE_REGISTER( GPTMTAMR                        , 0x004UL , RW  ) \
    MAKE_REGISTER( GPTMTBMR                        , 0x008UL , RW  ) \
    MAKE_REGISTER( GPTMCTL                         , 0x00CUL , RW  ) \
    MAKE_REGISTER( GPTMSYNC                        , 0x010UL , RW  ) \
    MAKE_REGISTER( GPTMIMR                         , 0x018UL , RW  ) \
    MAKE_REGISTER( GPTMRIS                         , 0x01CUL , R  ) \
    MAKE_REGISTER( GPTMMIS                         , 0x020UL , R  ) \
    MAKE_REGISTER( GPTMICR                         , 0x024UL , RW ) \
    MAKE_REGISTER( GPTMTAILR                       , 0x028UL , RW  ) \
    MAKE_REGISTER( GPTMTBILR                       , 0x02CUL , RW  ) \
    MAKE_REGISTER( GPTMTAMATCHR                    , 0x030UL , RW  ) \
    MAKE_REGISTER( GPTMTBMATCHR                    , 0x034UL , RW  ) \
    MAKE_REGISTER( GPTMTAPR                        , 0x038UL , RW  ) \
    MAKE_REGISTER( GPTMTBPR                        , 0x03CUL , RW  ) \
    MAKE_REGISTER( GPTMTAPMR                       , 0x040UL , RW  ) \
    MAKE_REGISTER( GPTMTBPMR                       , 0x044UL , RW  ) \
    MAKE_REGISTER( GPTMTAR                         , 0x048UL , R  ) \
    MAKE_REGISTER( GPTMTBR                         , 0x04CUL , R  ) \
    MAKE_REGISTER( GPTMTAV                         , 0x050UL , RW  ) \
    MAKE_REGISTER( GPTMTBV                         , 0x054UL , RW  ) \
    MAKE_REGISTER( GPTMRTCPD                       , 0x058UL , R  ) \
    MAKE_REGISTER( GPTMTAPS                        , 0x05CUL , R  ) \
    MAKE_REGISTER( GPTMTBPS                        , 0x060UL , R  ) \
    MAKE_REGISTER( GPTMTAPV                        , 0x064UL , R  ) \
    MAKE_REGISTER( GPTMTBPV                        , 0x068UL , R  ) \
    MAKE_REGISTER( GPTMPP                          , 0xFC0UL , R  ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in Watchdog Timers (WDGTIMER) register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_WDGTIMER(MAKE_REGISTER) \
    MAKE_REGISTER( WDTLOAD                         , 0x000UL , RW ) \
    MAKE_REGISTER( WDTVALUE                        , 0x004UL , R ) \
    MAKE_REGISTER( WDTCTL                          , 0x008UL , RW ) \
    MAKE_REGISTER( WDTRIS                          , 0x010UL , R ) \
    MAKE_REGISTER( WDTMIS                          , 0x014UL , R ) \
    MAKE_REGISTER( WDTTEST                         , 0x418UL , RW ) \
    MAKE_REGISTER( WDTLOCK                         , 0xC00UL , RW ) \
    MAKE_REGISTER( WDTPeriphID4                    , 0xFD0UL , R ) \
    MAKE_REGISTER( WDTPeriphID5                    , 0xFD4UL , R ) \
    MAKE_REGISTER( WDTPeriphID6                    , 0xFD8UL , R ) \
    MAKE_REGISTER( WDTPeriphID7                    , 0xFDCUL , R ) \
    MAKE_REGISTER( WDTPeriphID0                    , 0xFE0UL , R ) \
    MAKE_REGISTER( WDTPeriphID1                    , 0xFE4UL , R ) \
    MAKE_REGISTER( WDTPeriphID2                    , 0xFE8UL , R ) \
    MAKE_REGISTER( WDTPeriphID3                    , 0xFECUL , R ) \
    MAKE_REGISTER( WDTPCellID0                     , 0xFF0UL , R ) \
    MAKE_REGISTER( WDTPCellID1                     , 0xFF4UL , R ) \
    MAKE_REGISTER( WDTPCellID2                     , 0xFF8UL , R ) \
    MAKE_REGISTER( WDTPCellID3                     , 0xFFCUL , R ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in ADC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_ADC(MAKE_REGISTER) \
    MAKE_REGISTER( ADCACTSS                        , 0x000UL , RW ) \
    MAKE_REGISTER( ADCRIS                          , 0x004UL , R ) \
    MAKE_REGISTER( ADCIM                           , 0x008UL , RW ) \
    MAKE_REGISTER( ADCISC                          , 0x00CUL , R ) \
    MAKE_REGISTER( ADCOSTAT                        , 0x010UL , R ) \
    MAKE_REGISTER( ADCEMUX                         , 0x014UL , RW ) \
    MAKE_REGISTER( ADCUSTAT                        , 0x018UL , R ) \
    MAKE_REGISTER( ADCSSPRI                        , 0x020UL , RW ) \
    MAKE_REGISTER( ADCSPC                          , 0x024UL , RW ) \
    MAKE_REGISTER( ADCPSSI                         , 0x028UL , RW ) \
    MAKE_REGISTER( ADCSAC                          , 0x030UL , RW ) \
    MAKE_REGISTER( ADCDCISC                        , 0x034UL , R ) \
    MAKE_REGISTER( ADCCTL                          , 0x038UL , RW ) \
    MAKE_REGISTER( ADCSSMUX0                       , 0x040UL , RW ) \
    MAKE_REGISTER( ADCSSCTL0                       , 0x044UL , RW ) \
    MAKE_REGISTER( ADCSSFIFO0                      , 0x048UL , R ) \
    MAKE_REGISTER( ADCSSFSTAT0                     , 0x04CUL , R ) \
    MAKE_REGISTER( ADCSSOP0                        , 0x050UL , RW ) \
    MAKE_REGISTER( ADCSSDC0                        , 0x054UL , RW ) \
    MAKE_REGISTER( ADCSSMUX1                       , 0x060UL , RW ) \
    MAKE_REGISTER( ADCSSCTL1                       , 0x064UL , RW ) \
    MAKE_REGISTER( ADCSSFIFO1                      , 0x068UL , R ) \
    MAKE_REGISTER( ADCSSFSTAT1                     , 0x06CUL , R ) \
    MAKE_REGISTER( ADCSSOP1                        , 0x070UL , RW ) \
    MAKE_REGISTER( ADCSSDC1                        , 0x074UL , RW ) \
    MAKE_REGISTER( ADCSSMUX2                       , 0x080UL , RW ) \
    MAKE_REGISTER( ADCSSCTL2                       , 0x084UL , RW ) \
    MAKE_REGISTER( ADCSSFIFO2                      , 0x088UL , R ) \
    MAKE_REGISTER( ADCSSFSTAT2                     , 0x08CUL , R ) \
    MAKE_REGISTER( ADCSSOP2                        , 0x090UL , RW ) \
    MAKE_REGISTER( ADCSSDC2                        , 0x094UL , RW ) \
    MAKE_REGISTER( ADCSSMUX3                       , 0x0A0UL , RW ) \
    MAKE_REGISTER( ADCSSCTL3                       , 0x0A4UL , RW ) \
    MAKE_REGISTER( ADCSSFIFO3                      , 0x0A8UL , R ) \
    MAKE_REGISTER( ADCSSFSTAT3                     , 0x0ACUL , R ) \
    MAKE_REGISTER( ADCSSOP3                        , 0x0B0UL , RW ) \
    MAKE_REGISTER( ADCSSDC3                        , 0x0B4UL , RW ) \
    MAKE_REGISTER( ADCDCRIC                        , 0xD00UL , W ) \
    MAKE_REGISTER( ADCDCCTL0                       , 0xE00UL , RW ) \
    MAKE_REGISTER( ADCDCCTL1                       , 0xE04UL , RW ) \
    MAKE_REGISTER( ADCDCCTL2                       , 0xE08UL , RW ) \
    MAKE_REGISTER( ADCDCCTL3                       , 0xE0CUL , RW ) \
    MAKE_REGISTER( ADCDCCTL4                       , 0xE10UL , RW ) \
    MAKE_REGISTER( ADCDCCTL5                       , 0xE14UL , RW ) \
    MAKE_REGISTER( ADCDCCTL6                       , 0xE18UL , RW ) \
    MAKE_REGISTER( ADCDCCTL7                       , 0xE1CUL , RW ) \
    MAKE_REGISTER( ADCDCCMP0                       , 0xE40UL , RW ) \
    MAKE_REGISTER( ADCDCCMP1                       , 0xE44UL , RW ) \
    MAKE_REGISTER( ADCDCCMP2                       , 0xE48UL , RW ) \
    MAKE_REGISTER( ADCDCCMP3                       , 0xE4CUL , RW ) \
    MAKE_REGISTER( ADCDCCMP4                       , 0xE50UL , RW ) \
    MAKE_REGISTER( ADCDCCMP5                       , 0xE54UL , RW ) \
    MAKE_REGISTER( ADCDCCMP6                       , 0xE58UL , RW ) \
    MAKE_REGISTER( ADCDCCMP7                       , 0xE5CUL , RW ) \
    MAKE_REGISTER( ADCPP                           , 0xFC0UL , R ) \
    MAKE_REGISTER( ADCPC                           , 0xFC4UL , RW ) \
    MAKE_REGISTER( ADCCC                           , 0xFC8UL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in UART register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_UART(MAKE_REGISTER) \
    MAKE_REGISTER( UARTDR                          , 0x000UL , RW ) \
    MAKE_REGISTER( UARTFR                          , 0x018UL , R ) \
    MAKE_REGISTER( UARTILPR                        , 0x020UL , RW ) \
    MAKE_REGISTER( UARTIBRD                        , 0x024UL , RW ) \
    MAKE_REGISTER( UARTFBRD                        , 0x028UL , RW ) \
    MAKE_REGISTER( UARTLCRH                        , 0x02CUL , RW ) \
    MAKE_REGISTER( UARTCTL                         , 0x030UL , RW ) \
    MAKE_REGISTER( UARTIFLS                        , 0x034UL , RW ) \
    MAKE_REGISTER( UARTIM                          , 0x038UL , RW ) \
    MAKE_REGISTER( UARTRIS                         , 0x03CUL , R ) \
    MAKE_REGISTER( UARTMIS                         , 0x040UL , R ) \
    MAKE_REGISTER( UARTICR                         , 0x044UL , RW) \
    MAKE_REGISTER( UARTDMACTL                      , 0x048UL , RW ) \
    MAKE_REGISTER( UART9BITADDR                    , 0x0A4UL , RW ) \
    MAKE_REGISTER( UART9BITAMASK                   , 0x0A8UL , RW ) \
    MAKE_REGISTER( UARTPP                          , 0xFC0UL , R ) \
    MAKE_REGISTER( UARTCC                          , 0xFC8UL , RW ) \
    MAKE_REGISTER( UARTPeriphID4                   , 0xFD0UL , R ) \
    MAKE_REGISTER( UARTPeriphID5                   , 0xFD4UL , R ) \
    MAKE_REGISTER( UARTPeriphID6                   , 0xFD8UL , R ) \
    MAKE_REGISTER( UARTPeriphID7                   , 0xFDCUL , R ) \
    MAKE_REGISTER( UARTPeriphID0                   , 0xFE0UL , R ) \
    MAKE_REGISTER( UARTPeriphID1                   , 0xFE4UL , R ) \
    MAKE_REGISTER( UARTPeriphID2                   , 0xFE8UL , R ) \
    MAKE_REGISTER( UARTPeriphID3                   , 0xFECUL , R ) \
    MAKE_REGISTER( UARTPCellID0                    , 0xFF0UL , R ) \
    MAKE_REGISTER( UARTPCellID1                    , 0xFF4UL , R ) \
    MAKE_REGISTER( UARTPCellID2                    , 0xFF8UL , R ) \
    MAKE_REGISTER( UARTPCellID3                    , 0xFFCUL , R ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in SPI register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_SPI(MAKE_REGISTER) \
    MAKE_REGISTER( SSICR0                          , 0x000UL , RW  ) \
    MAKE_REGISTER( SSICR1                          , 0x004UL , RW  ) \
    MAKE_REGISTER( SSIDR                           , 0x008UL , RW  ) \
    MAKE_REGISTER( SSISR                           , 0x00CUL , R  ) \
    MAKE_REGISTER( SSICPSR                         , 0x010UL , RW  ) \
    MAKE_REGISTER( SSIIM                           , 0x014UL , RW  ) \
    MAKE_REGISTER( SSIRIS                          , 0x018UL , R  ) \
    MAKE_REGISTER( SSIMIS                          , 0x01CUL , R  ) \
    MAKE_REGISTER( SSIICR                          , 0x020UL , RW ) \
    MAKE_REGISTER( SSIDMACTL                       , 0x024UL , RW  ) \
    MAKE_REGISTER( SSICC                           , 0xFC8UL , RW  ) \
    MAKE_REGISTER( SSIPeriphID4                    , 0xFD0UL , R  ) \
    MAKE_REGISTER( SSIPeriphID5                    , 0xFD4UL , R  ) \
    MAKE_REGISTER( SSIPeriphID6                    , 0xFD8UL , R  ) \
    MAKE_REGISTER( SSIPeriphID7                    , 0xFDCUL , R  ) \
    MAKE_REGISTER( SSIPeriphID0                    , 0xFE0UL , R  ) \
    MAKE_REGISTER( SSIPeriphID1                    , 0xFE4UL , R  ) \
    MAKE_REGISTER( SSIPeriphID2                    , 0xFE8UL , R  ) \
    MAKE_REGISTER( SSIPeriphID3                    , 0xFECUL , R  ) \
    MAKE_REGISTER( SSIPCellID0                     , 0xFF0UL , R  ) \
    MAKE_REGISTER( SSIPCellID1                     , 0xFF4UL , R  ) \
    MAKE_REGISTER( SSIPCellID2                     , 0xFF8UL , R  ) \
    MAKE_REGISTER( SSIPCellID3                     , 0xFFCUL , R  ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in I2C register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_I2C(MAKE_REGISTER) \
    MAKE_REGISTER( I2CMSA                          , 0x000UL , RW ) \
    MAKE_REGISTER( I2CMCS                          , 0x004UL , RW ) \
    MAKE_REGISTER( I2CMDR                          , 0x008UL , RW ) \
    MAKE_REGISTER( I2CMTPR                         , 0x00CUL , RW ) \
    MAKE_REGISTER( I2CMIMR                         , 0x010UL , RW ) \
    MAKE_REGISTER( I2CMRIS                         , 0x014UL , R ) \
    MAKE_REGISTER( I2CMMIS                         , 0x018UL , R ) \
    MAKE_REGISTER( I2CMICR                         , 0x01CUL , W ) \
    MAKE_REGISTER( I2CMCR                          , 0x020UL , RW ) \
    MAKE_REGISTER( I2CMCLKOCNT                     , 0x024UL , RW ) \
    MAKE_REGISTER( I2CMBMON                        , 0x02CUL , R ) \
    MAKE_REGISTER( I2CMCR2                         , 0x038UL , R ) \
    MAKE_REGISTER( I2CSOAR                         , 0x800UL , RW ) \
    MAKE_REGISTER( I2CSCSR                         , 0x804UL , R ) \
    MAKE_REGISTER( I2CSDR                          , 0x808UL , RW ) \
    MAKE_REGISTER( I2CSIMR                         , 0x80CUL , RW ) \
    MAKE_REGISTER( I2CSRIS                         , 0x810UL , R ) \
    MAKE_REGISTER( I2CSMIS                         , 0x814UL , R ) \
    MAKE_REGISTER( I2CSICR                         , 0x818UL , W ) \
    MAKE_REGISTER( I2CSOAR2                        , 0x81CUL , RW ) \
    MAKE_REGISTER( I2CSACKCTL                      , 0x820UL , RW ) \
    MAKE_REGISTER( I2CPP                           , 0xFC0UL , R ) \
    MAKE_REGISTER( I2CPC                           , 0xFC4UL , R ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in CAN register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_CAN(MAKE_REGISTER) \
    MAKE_REGISTER( CANCTL                          , 0x000UL , RW ) \
    MAKE_REGISTER( CANSTS                          , 0x004UL , RW ) \
    MAKE_REGISTER( CANERR                          , 0x008UL , R ) \
    MAKE_REGISTER( CANBIT                          , 0x00CUL , RW ) \
    MAKE_REGISTER( CANINT                          , 0x010UL , R ) \
    MAKE_REGISTER( CANTST                          , 0x014UL , RW ) \
    MAKE_REGISTER( CANBRPE                         , 0x018UL , RW ) \
    MAKE_REGISTER( CANIF1CRQ                       , 0x020UL , RW ) \
    MAKE_REGISTER( CANIF1CMSK                      , 0x024UL , RW ) \
    MAKE_REGISTER( CANIF1MSK1                      , 0x028UL , RW ) \
    MAKE_REGISTER( CANIF1MSK2                      , 0x02CUL , RW ) \
    MAKE_REGISTER( CANIF1ARB1                      , 0x030UL , RW ) \
    MAKE_REGISTER( CANIF1ARB2                      , 0x034UL , RW ) \
    MAKE_REGISTER( CANIF1MCTL                      , 0x038UL , RW ) \
    MAKE_REGISTER( CANIF1DA1                       , 0x03CUL , RW ) \
    MAKE_REGISTER( CANIF1DA2                       , 0x040UL , RW ) \
    MAKE_REGISTER( CANIF1DB1                       , 0x044UL , RW ) \
    MAKE_REGISTER( CANIF1DB2                       , 0x048UL , RW ) \
    MAKE_REGISTER( CANIF2CRQ                       , 0x080UL , RW ) \
    MAKE_REGISTER( CANIF2CMSK                      , 0x084UL , RW ) \
    MAKE_REGISTER( CANIF2MSK1                      , 0x088UL , RW ) \
    MAKE_REGISTER( CANIF2MSK2                      , 0x08CUL , RW ) \
    MAKE_REGISTER( CANIF2ARB1                      , 0x090UL , RW ) \
    MAKE_REGISTER( CANIF2ARB2                      , 0x094UL , RW ) \
    MAKE_REGISTER( CANIF2MCTL                      , 0x098UL , RW ) \
    MAKE_REGISTER( CANIF2DA1                       , 0x09CUL , RW ) \
    MAKE_REGISTER( CANIF2DA2                       , 0x0A0UL , RW ) \
    MAKE_REGISTER( CANIF2DB1                       , 0x0A4UL , RW ) \
    MAKE_REGISTER( CANIF2DB2                       , 0x0A8UL , RW ) \
    MAKE_REGISTER( CANTXRQ1                        , 0x100UL , R ) \
    MAKE_REGISTER( CANTXRQ2                        , 0x104UL , R ) \
    MAKE_REGISTER( CANNWDA1                        , 0x120UL , R ) \
    MAKE_REGISTER( CANNWDA2                        , 0x124UL , R ) \
    MAKE_REGISTER( CANMSG1INT                      , 0x140UL , R ) \
    MAKE_REGISTER( CANMSG2INT                      , 0x144UL , R ) \
    MAKE_REGISTER( CANMSG1VAL                      , 0x160UL , R ) \
    MAKE_REGISTER( CANMSG2VAL                      , 0x164UL , R ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in USB register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_USB(MAKE_REGISTER) \
    MAKE_REGISTER( USBFADDR                        , 0x000UL , RW ) \
    MAKE_REGISTER( USBPOWER                        , 0x001UL , RW ) \
    MAKE_REGISTER( USBTXIS                         , 0x002UL , R ) \
    MAKE_REGISTER( USBRXIS                         , 0x004UL , R ) \
    MAKE_REGISTER( USBTXIE                         , 0x006UL , RW ) \
    MAKE_REGISTER( USBRXIE                         , 0x008UL , RW ) \
    MAKE_REGISTER( USBIS                           , 0x00AUL , R ) \
    MAKE_REGISTER( USBIE                           , 0x00BUL , RW ) \
    MAKE_REGISTER( USBFRAME                        , 0x00CUL , R ) \
    MAKE_REGISTER( USBEPIDX                        , 0x00EUL , RW ) \
    MAKE_REGISTER( USBTEST                         , 0x00FUL , RW ) \
    MAKE_REGISTER( USBFIFO0                        , 0x020UL , RW ) \
    MAKE_REGISTER( USBFIFO1                        , 0x024UL , RW ) \
    MAKE_REGISTER( USBFIFO2                        , 0x028UL , RW ) \
    MAKE_REGISTER( USBFIFO3                        , 0x02CUL , RW ) \
    MAKE_REGISTER( USBFIFO4                        , 0x030UL , RW ) \
    MAKE_REGISTER( USBFIFO5                        , 0x034UL , RW ) \
    MAKE_REGISTER( USBFIFO6                        , 0x038UL , RW ) \
    MAKE_REGISTER( USBFIFO7                        , 0x03CUL , RW ) \
    MAKE_REGISTER( USBTXFIFOSZ                     , 0x062UL , RW ) \
    MAKE_REGISTER( USBRXFIFOSZ                     , 0x063UL , RW ) \
    MAKE_REGISTER( USBTXFIFOADD                    , 0x064UL , RW ) \
    MAKE_REGISTER( USBRXFIFOADD                    , 0x066UL , RW ) \
    MAKE_REGISTER( USBCONTIM                       , 0x07AUL , RW ) \
    MAKE_REGISTER( USBFSEOF                        , 0x07DUL , RW ) \
    MAKE_REGISTER( USBCSRL0                        , 0x102UL , RW) \
    MAKE_REGISTER( USBCSRH0                        , 0x103UL , RW) \
    MAKE_REGISTER( USBCOUNT0                       , 0x108UL , R ) \
    MAKE_REGISTER( USBTXMAXP1                      , 0x110UL , RW ) \
    MAKE_REGISTER( USBTXCSRL1                      , 0x112UL , RW ) \
    MAKE_REGISTER( USBTXCSRH1                      , 0x113UL , RW ) \
    MAKE_REGISTER( USBRXMAXP1                      , 0x114UL , RW ) \
    MAKE_REGISTER( USBRXCSRL1                      , 0x116UL , RW ) \
    MAKE_REGISTER( USBRXCSRH1                      , 0x117UL , RW ) \
    MAKE_REGISTER( USBRXCOUNT1                     , 0x118UL , R ) \
    MAKE_REGISTER( USBTXMAXP2                      , 0x120UL , RW ) \
    MAKE_REGISTER( USBTXCSRL2                      , 0x122UL , RW ) \
    MAKE_REGISTER( USBTXCSRH2                      , 0x123UL , RW ) \
    MAKE_REGISTER( USBRXMAXP2                      , 0x124UL , RW ) \
    MAKE_REGISTER( USBRXCSRL2                      , 0x126UL , RW ) \
    MAKE_REGISTER( USBRXCSRH2                      , 0x127UL , RW ) \
    MAKE_REGISTER( USBRXCOUNT2                     , 0x128UL , R ) \
    MAKE_REGISTER( USBTXMAXP3                      , 0x130UL , RW ) \
    MAKE_REGISTER( USBTXCSRL3                      , 0x132UL , RW ) \
    MAKE_REGISTER( USBTXCSRH3                      , 0x133UL , RW ) \
    MAKE_REGISTER( USBRXMAXP3                      , 0x134UL , RW ) \
    MAKE_REGISTER( USBRXCSRL3                      , 0x136UL , RW ) \
    MAKE_REGISTER( USBRXCSRH3                      , 0x137UL , RW ) \
    MAKE_REGISTER( USBRXCOUNT3                     , 0x138UL , R ) \
    MAKE_REGISTER( USBTXMAXP4                      , 0x140UL , RW ) \
    MAKE_REGISTER( USBTXCSRL4                      , 0x142UL , RW ) \
    MAKE_REGISTER( USBTXCSRH4                      , 0x143UL , RW ) \
    MAKE_REGISTER( USBRXMAXP4                      , 0x144UL , RW ) \
    MAKE_REGISTER( USBRXCSRL4                      , 0x146UL , RW ) \
    MAKE_REGISTER( USBRXCSRH4                      , 0x147UL , RW ) \
    MAKE_REGISTER( USBRXCOUNT4                     , 0x148UL , R ) \
    MAKE_REGISTER( USBTXMAXP5                      , 0x150UL , RW ) \
    MAKE_REGISTER( USBTXCSRL5                      , 0x152UL , RW ) \
    MAKE_REGISTER( USBTXCSRH5                      , 0x153UL , RW ) \
    MAKE_REGISTER( USBRXMAXP5                      , 0x154UL , RW ) \
    MAKE_REGISTER( USBRXCSRL5                      , 0x156UL , RW ) \
    MAKE_REGISTER( USBRXCSRH5                      , 0x157UL , RW ) \
    MAKE_REGISTER( USBRXCOUNT5                     , 0x158UL , R ) \
    MAKE_REGISTER( USBTXMAXP6                      , 0x160UL , RW ) \
    MAKE_REGISTER( USBTXCSRL6                      , 0x162UL , RW ) \
    MAKE_REGISTER( USBTXCSRH6                      , 0x163UL , RW ) \
    MAKE_REGISTER( USBRXMAXP6                      , 0x164UL , RW ) \
    MAKE_REGISTER( USBRXCSRL6                      , 0x166UL , RW ) \
    MAKE_REGISTER( USBRXCSRH6                      , 0x167UL , RW ) \
    MAKE_REGISTER( USBRXCOUNT6                     , 0x168UL , R ) \
    MAKE_REGISTER( USBTXMAXP7                      , 0x170UL , RW ) \
    MAKE_REGISTER( USBTXCSRL7                      , 0x172UL , RW ) \
    MAKE_REGISTER( USBTXCSRH7                      , 0x173UL , RW ) \
    MAKE_REGISTER( USBRXMAXP7                      , 0x174UL , RW ) \
    MAKE_REGISTER( USBRXCSRL7                      , 0x176UL , RW ) \
    MAKE_REGISTER( USBRXCSRH7                      , 0x177UL , RW ) \
    MAKE_REGISTER( USBRXCOUNT7                     , 0x178UL , R ) \
    MAKE_REGISTER( USBRXDPKTBUFDIS                 , 0x340UL , RW ) \
    MAKE_REGISTER( USBTXDPKTBUFDIS                 , 0x342UL , RW ) \
    MAKE_REGISTER( USBDRRIS                        , 0x410UL , R ) \
    MAKE_REGISTER( USBDRIM                         , 0x414UL , RW ) \
    MAKE_REGISTER( USBDRISC                        , 0x418UL , RW) \
    MAKE_REGISTER( USBDMASEL                       , 0x450UL , RW ) \
    MAKE_REGISTER( USBPP                           , 0xFC0UL , R ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in analog comparators (ACOM) register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_ACOM(MAKE_REGISTER) \
    MAKE_REGISTER( ACMIS                           , 0x000UL , R ) \
    MAKE_REGISTER( ACRIS                           , 0x004UL , R ) \
    MAKE_REGISTER( ACINTEN                         , 0x008UL , RW ) \
    MAKE_REGISTER( ACREFCTL                        , 0x010UL , RW ) \
    MAKE_REGISTER( ACSTAT0                         , 0x020UL , R ) \
    MAKE_REGISTER( ACCTL0                          , 0x024UL , RW ) \
    MAKE_REGISTER( ACSTAT1                         , 0x040UL , R ) \
    MAKE_REGISTER( ACCTL1                          , 0x044UL , RW ) \
    MAKE_REGISTER( ACMPPP                          , 0xFC0UL , R ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of register maps in this file
 *
 * This definition contains all register maps names. If you want to add new register map, you must add it also to this list.
 *
 * To add new register map use:
 * @code
       MAKE_REGISTER_MAP( REGISTER_MAP_NAME )\
   @endcode
 * Where:
 *          REGISTER_MAP_NAME   - name of the register map. The definition with list of registers for this register map should be compatible
 *                                with macro #oC_REGISTER_MAP_
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_LIST(MAKE_REGISTER_MAP) \
    MAKE_REGISTER_MAP(SYSTEM_CONTROL) \
    MAKE_REGISTER_MAP(GPIO) \
    MAKE_REGISTER_MAP(DMA) \
    MAKE_REGISTER_MAP(TIMER) \
    MAKE_REGISTER_MAP(WDGTIMER) \
    MAKE_REGISTER_MAP(ADC) \
    MAKE_REGISTER_MAP(UART) \
    MAKE_REGISTER_MAP(SPI) \
    MAKE_REGISTER_MAP(I2C) \
    MAKE_REGISTER_MAP(CAN) \
    MAKE_REGISTER_MAP(USB) \
    MAKE_REGISTER_MAP(ACOM) \

#endif /* SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_MACHINE_RMAPSDEFS_H_ */
