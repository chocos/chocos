/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the PORT driver
 *
 * @file       oc_pins_defs.h
 *
 * @author     Kamil Drobienko
 *
 * @note       Copyright (C) 2016 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_PINS_DEFS_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_PINS_DEFS_H_

#include <oc_1word.h>

#define oC_PORT_WIDTH                                8ULL
#define oC_ALTERNATE_NUMBER_FIELD_WIDTH              4ULL
#define oC_MODULE_PIN_FUNCTION_WIDTH                 4ULL

#define oC_MODULE_PIN_FUNCTIONS_(MODULE_NAME)        oC_1WORD_FROM_2(oC_MODULE_PIN_FUNCTIONS_ , MODULE_NAME)
#define oC_MODULE_PINS_(MODULE_NAME)                 oC_1WORD_FROM_2(oC_MODULE_PINS_ , MODULE_NAME)


#define oC_PINS_LIST(MAKE_PIN)\
    MAKE_PIN( PORTA , PA0 , 0 , 17 )\
    MAKE_PIN( PORTA , PA1 , 1 , 18 )\
    MAKE_PIN( PORTA , PA2 , 2 , 19 )\
    MAKE_PIN( PORTA , PA3 , 3 , 20 )\
    MAKE_PIN( PORTA , PA4 , 4 , 21 )\
    MAKE_PIN( PORTA , PA5 , 5 , 22 )\
    MAKE_PIN( PORTA , PA6 , 6 , 23 )\
    MAKE_PIN( PORTA , PA7 , 7 , 24 )\
    MAKE_PIN( PORTB , PB0 , 0 , 45 )\
    MAKE_PIN( PORTB , PB1 , 1 , 46 )\
    MAKE_PIN( PORTB , PB2 , 2 , 47 )\
    MAKE_PIN( PORTB , PB3 , 3 , 48 )\
    MAKE_PIN( PORTB , PB4 , 4 , 58 )\
    MAKE_PIN( PORTB , PB5 , 5 , 57 )\
    MAKE_PIN( PORTB , PB6 , 6 , 1 )\
    MAKE_PIN( PORTB , PB7 , 7 , 4 )\
    MAKE_PIN( PORTC , PC0 , 0 , 52 )\
    MAKE_PIN( PORTC , PC1 , 1 , 51 )\
    MAKE_PIN( PORTC , PC2 , 2 , 50 )\
    MAKE_PIN( PORTC , PC3 , 3 , 49 )\
    MAKE_PIN( PORTC , PC4 , 4 , 16 )\
    MAKE_PIN( PORTC , PC5 , 5 , 15 )\
    MAKE_PIN( PORTC , PC6 , 6 , 14 )\
    MAKE_PIN( PORTC , PC7 , 7 , 13 )\
    MAKE_PIN( PORTD , PD0 , 0 , 61 )\
    MAKE_PIN( PORTD , PD1 , 1 , 62 )\
    MAKE_PIN( PORTD , PD2 , 2 , 63 )\
    MAKE_PIN( PORTD , PD3 , 3 , 64 )\
    MAKE_PIN( PORTD , PD4 , 4 , 43 )\
    MAKE_PIN( PORTD , PD5 , 5 , 44 )\
    MAKE_PIN( PORTD , PD6 , 6 , 53 )\
    MAKE_PIN( PORTD , PD7 , 7 , 10 )\
    MAKE_PIN( PORTE , PE0 , 0 , 9 )\
    MAKE_PIN( PORTE , PE1 , 1 , 8 )\
    MAKE_PIN( PORTE , PE2 , 2 , 7 )\
    MAKE_PIN( PORTE , PE3 , 3 , 6 )\
    MAKE_PIN( PORTE , PE4 , 4 , 59 )\
    MAKE_PIN( PORTE , PE5 , 5 , 60 )\
    MAKE_PIN( PORTF , PF0 , 0 , 28 )\
    MAKE_PIN( PORTF , PF1 , 1 , 29 )\
    MAKE_PIN( PORTF , PF2 , 2 , 30 )\
    MAKE_PIN( PORTF , PF3 , 3 , 31 )\
	MAKE_PIN( PORTF , PF4 , 4 , 5 )\



#define oC_MODULE_PIN_FUNCTIONS_UART(MAKE_PIN_FUNCTION) \
    MAKE_PIN_FUNCTION(Rx)\
    MAKE_PIN_FUNCTION(Tx)\

#define oC_MODULE_PINS_UART(MAKE_PIN) \
    MAKE_PIN( PA0 , UART0 , Rx , 1 ) \
    MAKE_PIN( PA1 , UART0 , Tx , 1 ) \
    MAKE_PIN( PB0 , UART1 , Rx , 1 ) \
    MAKE_PIN( PB1 , UART1 , Tx , 1 ) \
    MAKE_PIN( PC4 , UART1 , Rx , 2 ) \
    MAKE_PIN( PC5 , UART1 , Tx , 2 ) \
    MAKE_PIN( PC4 , UART4 , Rx , 1 ) \
    MAKE_PIN( PC5 , UART4 , Tx , 1 ) \
    MAKE_PIN( PC6 , UART3 , Rx , 1 ) \
    MAKE_PIN( PC7 , UART3 , Tx , 1 ) \
    MAKE_PIN( PD4 , UART6 , Rx , 1 ) \
    MAKE_PIN( PD5 , UART6 , Tx , 1 ) \
    MAKE_PIN( PD6 , UART2 , Rx , 1 ) \
    MAKE_PIN( PD7 , UART2 , Tx , 1 ) \
    MAKE_PIN( PE0 , UART7 , Rx , 1 ) \
    MAKE_PIN( PE1 , UART7 , Tx , 1 ) \
    MAKE_PIN( PE4 , UART5 , Rx , 1 ) \
    MAKE_PIN( PE5 , UART5 , Tx , 1 ) \


#define oC_MODULE_PIN_FUNCTIONS_SPI(MAKE_PIN_FUNCTION) \
    MAKE_PIN_FUNCTION(MOSI)\
    MAKE_PIN_FUNCTION(MISO)\
    MAKE_PIN_FUNCTION(CS)\
    MAKE_PIN_FUNCTION(CLK)\

#define oC_MODULE_PINS_SPI(MAKE_PIN) \
    MAKE_PIN( PA2 , SSI0 , CLK  , 2 ) \
    MAKE_PIN( PA3 , SSI0 , CS   , 2 ) \
    MAKE_PIN( PA4 , SSI0 , MISO , 2 ) \
    MAKE_PIN( PA5 , SSI0 , MOSI , 2 ) \
    MAKE_PIN( PD0 , SSI1 , CLK  , 2 ) \
    MAKE_PIN( PD1 , SSI1 , CS   , 2 ) \
    MAKE_PIN( PD2 , SSI1 , MISO , 2 ) \
    MAKE_PIN( PD3 , SSI1 , MOSI , 2 ) \
    MAKE_PIN( PF0 , SSI1 , MISO , 2 ) \
    MAKE_PIN( PF1 , SSI1 , MOSI , 2 ) \
    MAKE_PIN( PF2 , SSI1 , CLK  , 2 ) \
    MAKE_PIN( PF3 , SSI1 , CS   , 2 ) \
    MAKE_PIN( PB4 , SSI2 , CLK  , 2 ) \
    MAKE_PIN( PB5 , SSI2 , CS   , 2 ) \
    MAKE_PIN( PB6 , SSI2 , MISO , 2 ) \
    MAKE_PIN( PB7 , SSI2 , MOSI , 2 ) \
    MAKE_PIN( PD0 , SSI3 , CLK  , 1 ) \
    MAKE_PIN( PD1 , SSI3 , CS   , 1 ) \
    MAKE_PIN( PD2 , SSI3 , MISO , 1 ) \
    MAKE_PIN( PD3 , SSI3 , MOSI , 1 ) \


#define oC_MODULE_PIN_FUNCTIONS_TIMER(MAKE_PIN_FUNCTION) \
    MAKE_PIN_FUNCTION( A ) \
    MAKE_PIN_FUNCTION( B ) \

#define oC_MODULE_PINS_TIMER(MAKE_PIN) \
    MAKE_PIN( PB0 , Timer2 , A , 7 ) \
    MAKE_PIN( PB1 , Timer2 , B , 7 ) \
    MAKE_PIN( PB2 , Timer3 , A , 7 ) \
    MAKE_PIN( PB3 , Timer3 , B , 7 ) \
    MAKE_PIN( PB4 , Timer1 , A , 7 ) \
    MAKE_PIN( PB5 , Timer1 , B , 7 ) \
    MAKE_PIN( PB6 , Timer0 , A , 7 ) \
    MAKE_PIN( PB7 , Timer0 , B , 7 ) \
    MAKE_PIN( PC0 , Timer4 , A , 7 ) \
    MAKE_PIN( PC1 , Timer4 , B , 7 ) \
    MAKE_PIN( PC2 , Timer5 , A , 7 ) \
    MAKE_PIN( PC3 , Timer5 , B , 7 ) \
    MAKE_PIN( PC4 , WideTimer0 , A , 7 ) \
    MAKE_PIN( PC5 , WideTimer0 , B , 7 ) \
    MAKE_PIN( PC6 , WideTimer1 , A , 7 ) \
    MAKE_PIN( PC7 , WideTimer1 , B , 7 ) \
    MAKE_PIN( PD0 , WideTimer2 , A , 7 ) \
    MAKE_PIN( PD1 , WideTimer2 , B , 7 ) \
    MAKE_PIN( PD2 , WideTimer3 , A , 7 ) \
    MAKE_PIN( PD3 , WideTimer3 , B , 7 ) \
    MAKE_PIN( PD4 , WideTimer4 , A , 7 ) \
    MAKE_PIN( PD5 , WideTimer4 , B , 7 ) \
    MAKE_PIN( PD6 , WideTimer5 , A , 7 ) \
    MAKE_PIN( PD7 , WideTimer5 , B , 7 ) \
    MAKE_PIN( PF0 , Timer0 , A , 7 ) \
    MAKE_PIN( PF1 , Timer0 , B , 7 ) \
    MAKE_PIN( PF2 , Timer1 , A , 7 ) \
    MAKE_PIN( PF3 , Timer1 , B , 7 ) \
    MAKE_PIN( PF4 , Timer2 , A , 7 ) \

#define oC_MODULES_PINS_LIST(MAKE_MODULE)   \
    MAKE_MODULE(UART)  \
    MAKE_MODULE(SPI)   \
    MAKE_MODULE(TIMER)    \

#endif /* SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_PINS_DEFS_H_ */
