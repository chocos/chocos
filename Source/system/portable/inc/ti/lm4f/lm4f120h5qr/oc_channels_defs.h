/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_machine_chdef.h
 *
 * @author     Kamil Drobienko
 *
 * @note       Copyright (C) 2016 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_CHANNELS_DEFS_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_CHANNELS_DEFS_H_

#include <oc_1word.h>

#define oC_MODULE_CHANNELS_(MODULE_NAME)        oC_1WORD_FROM_2(oC_MODULE_CHANNELS_ , MODULE_NAME )

#define oC_CHANNEL_MASK_WIDTH                   9ULL

// par1 name par2 base address param3 rmapsdefs

#define oC_MODULE_CHANNELS_GPIO(MAKE_CHANNEL)    \
    MAKE_CHANNEL( PORTA , PORTA , GPIO ) \
    MAKE_CHANNEL( PORTB , PORTB , GPIO ) \
    MAKE_CHANNEL( PORTC , PORTC , GPIO ) \
    MAKE_CHANNEL( PORTD , PORTD , GPIO ) \
    MAKE_CHANNEL( PORTE , PORTE , GPIO ) \
    MAKE_CHANNEL( PORTF , PORTF , GPIO ) \


#define oC_MODULE_CHANNELS_DMA(MAKE_CHANNEL) \
    MAKE_CHANNEL( uDMA0  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA1  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA2  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA3  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA4  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA5  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA6  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA7  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA8  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA9  , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA10 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA11 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA12 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA13 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA14 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA15 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA16 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA17 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA18 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA19 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA20 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA21 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA22 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA23 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA24 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA25 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA26 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA27 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA28 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA29 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA30 , uDMA0, DMA ) \
    MAKE_CHANNEL( uDMA31 , uDMA0, DMA ) \


#define oC_MODULE_CHANNELS_TIMER(MAKE_CHANNEL)    \
    MAKE_CHANNEL( Timer0      , Timer0      , TIMER ) \
    MAKE_CHANNEL( Timer1      , Timer1      , TIMER ) \
    MAKE_CHANNEL( Timer2      , Timer2      , TIMER ) \
    MAKE_CHANNEL( Timer3      , Timer3      , TIMER ) \
    MAKE_CHANNEL( Timer4      , Timer4      , TIMER ) \
    MAKE_CHANNEL( Timer5      , Timer5      , TIMER ) \
    MAKE_CHANNEL( WideTimer0  , WideTimer0  , TIMER ) \
    MAKE_CHANNEL( WideTimer1  , WideTimer1  , TIMER ) \
    MAKE_CHANNEL( WideTimer2  , WideTimer2  , TIMER ) \
    MAKE_CHANNEL( WideTimer3  , WideTimer3  , TIMER ) \
    MAKE_CHANNEL( WideTimer4  , WideTimer4  , TIMER ) \
    MAKE_CHANNEL( WideTimer5  , WideTimer5  , TIMER ) \


#define oC_MODULE_CHANNELS_WDG(MAKE_CHANNEL)    \
    MAKE_CHANNEL( WDT0 , WDT0 , WDGTIMER ) \
    MAKE_CHANNEL( WDT1 , WDT1 , WDGTIMER ) \


#define oC_MODULE_CHANNELS_ADC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( ADC0 , ADC0 , ADC ) \
    MAKE_CHANNEL( ADC1 , ADC1 , ADC ) \


#define oC_MODULE_CHANNELS_UART(MAKE_CHANNEL)    \
    MAKE_CHANNEL( UART0 , UART0 , UART ) \
    MAKE_CHANNEL( UART1 , UART1 , UART ) \
    MAKE_CHANNEL( UART2 , UART2 , UART ) \
    MAKE_CHANNEL( UART3 , UART3 , UART ) \
    MAKE_CHANNEL( UART4 , UART4 , UART ) \
    MAKE_CHANNEL( UART5 , UART5 , UART ) \
    MAKE_CHANNEL( UART6 , UART6 , UART ) \
    MAKE_CHANNEL( UART7 , UART7 , UART ) \


#define oC_MODULE_CHANNELS_SPI(MAKE_CHANNEL)    \
    MAKE_CHANNEL( SSI0 , SSI0 , SPI ) \
    MAKE_CHANNEL( SSI1 , SSI1 , SPI ) \
    MAKE_CHANNEL( SSI2 , SSI2 , SPI ) \
    MAKE_CHANNEL( SSI3 , SSI3 , SPI ) \


#define oC_MODULE_CHANNELS_I2C(MAKE_CHANNEL)    \
    MAKE_CHANNEL( I2C0 , I2C0 , I2C ) \
    MAKE_CHANNEL( I2C1 , I2C1 , I2C ) \
    MAKE_CHANNEL( I2C2 , I2C2 , I2C ) \
    MAKE_CHANNEL( I2C3 , I2C3 , I2C ) \


#define oC_MODULE_CHANNELS_CAN(MAKE_CHANNEL)    \
    MAKE_CHANNEL( CAN0 , CAN0 , CAN ) \


#define oC_MODULE_CHANNELS_USB(MAKE_CHANNEL)    \
    MAKE_CHANNEL( USB0 , USB0 , USB ) \


#define oC_MODULE_CHANNELS_ACOM(MAKE_CHANNEL)    \
    MAKE_CHANNEL( ACOM0 , ACOM0 , ACOM ) \

#define oC_MODULES_LIST(MAKE_MODULE_CHANNELS) \
    MAKE_MODULE_CHANNELS( GPIO ) \
    MAKE_MODULE_CHANNELS( DMA ) \
    MAKE_MODULE_CHANNELS( TIMER ) \
    MAKE_MODULE_CHANNELS( WDG ) \
    MAKE_MODULE_CHANNELS( ADC ) \
    MAKE_MODULE_CHANNELS( UART ) \
    MAKE_MODULE_CHANNELS( SPI ) \
    MAKE_MODULE_CHANNELS( I2C ) \
    MAKE_MODULE_CHANNELS( CAN ) \
    MAKE_MODULE_CHANNELS( USB ) \
    MAKE_MODULE_CHANNELS( ACOM ) \


#endif /* SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_BA_DEFS_H_ */
