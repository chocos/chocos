/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions of base addresses for machine
 *
 * @file       oc_ba_defs.h
 *
 * @author     Kamil Drobienko
 *
 * @note       Copyright (C) 2016 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_BA_DEFS_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_BA_DEFS_H_

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits for storing base address
 *
 * The definition stores number of bits that are required for storing base address.
 */
//==========================================================================================================================================
#define oC_BASE_ADDRESS_WIDTH           32

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of base addresses
 *
 * The definition contains list of base addresses. To add new base address to list:
 *
 * @code{.c}
       MAKE_BASE_ADDRESS( BA_NAME , ADDRESS , POWER_BASE_NAME,POWER_OFFSET_NAME,POWER_BIT_INDEX )\
   @endcode
 * Where:
 *          BA_NAME             - Name of a base address
 *          ADDRESS             - Address in memory for the start of the register map (base address)
 *          POWER_BASE_NAME     - Name of base from this list, that allow to enable power for the base (Use 'None' if not used)
 *          POWER_OFFSET_NAME   - Name of offset, that should be added to base address to receive full address of register for enabling power
 *                                for the register map. This should be defined in oc_rmaps_defs.h file (Use 'None' if not used)
 *          POWER_BIT_INDEX     - Index of bit in a power register to set for enabling this register map (Use 'None' if not used)
 */
//==========================================================================================================================================
#define oC_MACHINE_BA_LIST(MAKE_BASE_ADDRESS)   \
    MAKE_BASE_ADDRESS( SystemControl    , 0x400FE000UL , None          , None       , 0 ) \
    MAKE_BASE_ADDRESS( PORTA            , 0x40058000UL , SystemControl , RCGCGPIO   , 0 ) \
    MAKE_BASE_ADDRESS( PORTB            , 0x40059000UL , SystemControl , RCGCGPIO   , 1 ) \
    MAKE_BASE_ADDRESS( PORTC            , 0x4005A000UL , SystemControl , RCGCGPIO   , 2 ) \
    MAKE_BASE_ADDRESS( PORTD            , 0x4005B000UL , SystemControl , RCGCGPIO   , 3 ) \
    MAKE_BASE_ADDRESS( PORTE            , 0x4005C000UL , SystemControl , RCGCGPIO   , 4 ) \
    MAKE_BASE_ADDRESS( PORTF            , 0x4005D000UL , SystemControl , RCGCGPIO   , 5 ) \
    MAKE_BASE_ADDRESS( uDMA0            , 0x400FF000UL , SystemControl , RCGCDMA    , 0 ) \
    MAKE_BASE_ADDRESS( Timer0           , 0x40030000UL , SystemControl , RCGCTIMER  , 0 , SIZE16or32 )\
    MAKE_BASE_ADDRESS( Timer1           , 0x40031000UL , SystemControl , RCGCTIMER  , 1 , SIZE16or32 )\
    MAKE_BASE_ADDRESS( Timer2           , 0x40032000UL , SystemControl , RCGCTIMER  , 2 , SIZE16or32 )\
    MAKE_BASE_ADDRESS( Timer3           , 0x40033000UL , SystemControl , RCGCTIMER  , 3 , SIZE16or32 )\
    MAKE_BASE_ADDRESS( Timer4           , 0x40034000UL , SystemControl , RCGCTIMER  , 4 , SIZE16or32 )\
    MAKE_BASE_ADDRESS( Timer5           , 0x40035000UL , SystemControl , RCGCTIMER  , 5 , SIZE16or32 )\
    MAKE_BASE_ADDRESS( WideTimer0       , 0x40036000UL , SystemControl , RCGCWTIMER , 0 , SIZE32or64 )\
    MAKE_BASE_ADDRESS( WideTimer1       , 0x40037000UL , SystemControl , RCGCWTIMER , 1 , SIZE32or64 )\
    MAKE_BASE_ADDRESS( WideTimer2       , 0x4004C000UL , SystemControl , RCGCWTIMER , 2 , SIZE32or64 )\
    MAKE_BASE_ADDRESS( WideTimer3       , 0x4004D000UL , SystemControl , RCGCWTIMER , 3 , SIZE32or64 )\
    MAKE_BASE_ADDRESS( WideTimer4       , 0x4004E000UL , SystemControl , RCGCWTIMER , 4 , SIZE32or64 )\
    MAKE_BASE_ADDRESS( WideTimer5       , 0x4004F000UL , SystemControl , RCGCWTIMER , 5 , SIZE32or64 )\
    MAKE_BASE_ADDRESS( WDT0             , 0x40000000UL , SystemControl , RCGCWD     , 0 ) \
    MAKE_BASE_ADDRESS( WDT1             , 0x40001000UL , SystemControl , RCGCWD     , 1 ) \
	MAKE_BASE_ADDRESS( ADC0             , 0x40038000UL , SystemControl , RCGCADC    , 0 ) \
    MAKE_BASE_ADDRESS( ADC1             , 0x40039000UL , SystemControl , RCGCADC    , 1 ) \
    MAKE_BASE_ADDRESS( UART0            , 0x4000C000UL , SystemControl , RCGCUART   , 0 ) \
    MAKE_BASE_ADDRESS( UART1            , 0x4000D000UL , SystemControl , RCGCUART   , 1 ) \
    MAKE_BASE_ADDRESS( UART2            , 0x4000E000UL , SystemControl , RCGCUART   , 2 ) \
    MAKE_BASE_ADDRESS( UART3            , 0x4000F000UL , SystemControl , RCGCUART   , 3 ) \
    MAKE_BASE_ADDRESS( UART4            , 0x40010000UL , SystemControl , RCGCUART   , 4 ) \
    MAKE_BASE_ADDRESS( UART5            , 0x40011000UL , SystemControl , RCGCUART   , 5 ) \
    MAKE_BASE_ADDRESS( UART6            , 0x40012000UL , SystemControl , RCGCUART   , 6 ) \
    MAKE_BASE_ADDRESS( UART7            , 0x40013000UL , SystemControl , RCGCUART   , 7 ) \
    MAKE_BASE_ADDRESS( SSI0             , 0x40008000UL , SystemControl , RCGCSSI    , 0 ) \
    MAKE_BASE_ADDRESS( SSI1             , 0x40009000UL , SystemControl , RCGCSSI    , 1 ) \
    MAKE_BASE_ADDRESS( SSI2             , 0x4000A000UL , SystemControl , RCGCSSI    , 2 ) \
    MAKE_BASE_ADDRESS( SSI3             , 0x4000B000UL , SystemControl , RCGCSSI    , 3 ) \
    MAKE_BASE_ADDRESS( I2C0             , 0x40020000UL , SystemControl , RCGCI2C    , 0 ) \
    MAKE_BASE_ADDRESS( I2C1             , 0x40021000UL , SystemControl , RCGCI2C    , 1 ) \
    MAKE_BASE_ADDRESS( I2C2             , 0x40022000UL , SystemControl , RCGCI2C    , 2 ) \
    MAKE_BASE_ADDRESS( I2C3             , 0x40023000UL , SystemControl , RCGCI2C    , 3 ) \
    MAKE_BASE_ADDRESS( CAN0             , 0x40040000UL , SystemControl , RCGCCAN    , 0 ) \
    MAKE_BASE_ADDRESS( USB0             , 0x40050000UL , SystemControl , RCGCUSB    , 0 ) \
    MAKE_BASE_ADDRESS( ACOM0            , 0x4003C000UL , SystemControl , RCGCACMP   , 0 ) \


#endif /* SYSTEM_PORTABLE_INC_TI_LM4F_LM4F120H5QR_OC_BA_DEFS_H_ */
