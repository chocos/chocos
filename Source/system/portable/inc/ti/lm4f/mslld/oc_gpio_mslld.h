/** ****************************************************************************************************************************************
 *
 * @file       oc_gpio_mslld.h
 *
 * @brief      The file with interface of machine specific LLD for GPIO LLD
 *
 * @author     Patryk Kubiak - (Created on: 15 08 2015 20:07:50)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_PORTABLE_INC_TI_LM4F_MSLLD_OC_GPIO_MSLLD_H_
#define SYSTEM_PORTABLE_INC_TI_LM4F_MSLLD_OC_GPIO_MSLLD_H_

#include <oc_gpio_lld.h>

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

//==========================================================================================================================================
/**
 * @brief selection of digital function of special pins
 *
 * This is the function specific for the lm4f family. It allows to set function of the pin, for example for SPI.
 *
 * @see oC_Machine_PeripheralPin , oC_Machine_GetDigitalFunctionOfPeripheralPin , oC_Machine_GetPinOfPeripheralPin
 *
 * @param Pins              Pins to configure
 * @param DigitalFunction   Value for the GPIOPCTL register
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_MSLLD_SetAlternateNumber( oC_Pin_t Pin , oC_PinAlternateNumber_t Function );

//==========================================================================================================================================
/**
 * @brief connects and configures peripheral pin to the peripheral
 *
 * This is the function specific for the lm4f family. It allows to connect peripheral pin, for example for SPI.
 *
 * @param Pin               Pin to connect
 * @param DigitalFunction   Digital function for SetPinDigitalFunction function
 *
 * @return code of error
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_MSLLD_ConnectPin( oC_Pin_t Pin , oC_PinAlternateNumber_t Function );


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief connects module pin to specific pin function
 *
 * The function connects module pin to the specific pin function, that is stored in the module pin. See also #oC_GPIO_MSLLD_ConnectPin.
 *
 * @param ModulePin         Module pin defined by using macros from the oc_pins.h file.
 */
//==========================================================================================================================================
#define oC_GPIO_MSLLD_ConnectModulePin( ModulePin )         oC_GPIO_MSLLD_ConnectPin( oC_ModulePin_GetPin(ModulePin) , oC_ModulePin_GetAlternateNumber(ModulePin) )
//==========================================================================================================================================
/**
 * @brief disconnects module pin from specific pin function
 *
 * The function disconnects module pin to the specific pin function, that is stored in the module pin. See also #oC_GPIO_MSLLD_ConnectPin.
 *
 * @param ModulePin         Module pin defined by using macros from the oc_pins.h file.
 */
//==========================================================================================================================================
#define oC_GPIO_MSLLD_DisconnectModulePin( ModulePin )      oC_GPIO_MSLLD_DisconnectPin( oC_ModulePin_GetPin(ModulePin) , oC_ModulePin_GetAlternateNumber(ModulePin) )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


#endif /* SYSTEM_PORTABLE_INC_TI_LM4F_MSLLD_OC_GPIO_MSLLD_H_ */
