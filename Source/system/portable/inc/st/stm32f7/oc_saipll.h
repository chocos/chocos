/** ****************************************************************************************************************************************
 *
 * @brief      File with interface for SAI PLL module
 *
 * @file       oc_saipll.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup STM32F7-SAIPLL Module for configuration of the SAI PLL
 * @ingroup PortableSpace
 * @brief Module for configuration of the PLL SAI for STM32F7 family
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_SAIPLL_H_
#define SYSTEM_PORTABLE_INC_OC_SAIPLL_H_

#include <oc_stdtypes.h>
#include <oc_frequency.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup STM32F7-SAIPLL
//! @{

//==========================================================================================================================================
/**
 * @brief stores selection of the output line
 *
 * The type is for storing SAI PLL output line (look to the STM32F7 documentation for a CLOCK TREE)
 */
//==========================================================================================================================================
typedef enum
{
    oC_SaiPll_OutputLine_LTDC ,                                                      //!< Output of the PLL connected to the LTDC module (LTDC Clock)
    oC_SaiPll_OutputLine_PLL48CLK ,                                                  //!< Output of the PLL connected to the PLL48CLK
    oC_SaiPll_OutputLine_SAICLK ,                                                    //!< Output of the PLL connected to the SAI
    oC_SaiPll_OutputLine_NumberOfElements ,                                          //!< Number of main elements (maximum index) in the type. All lines that has number smaller than this value can be configured separately
    oC_SaiPll_OutputLine_IndexMask           = 0xF ,                                 //!< This is the mask to get index of the line from the type
    oC_SaiPll_OutputLine_SAI1CLK             = (1<<4) | oC_SaiPll_OutputLine_SAICLK ,//!< Additional definition for SAI1 CLK line. It has the same index as SAICLK, because it cannot be configured to the different frequency than SAI2
    oC_SaiPll_OutputLine_SAI2CLK             = (1<<5) | oC_SaiPll_OutputLine_SAICLK ,//!< Additional definition for SAI2 CLK line. It has the same index as SAICLK, because it cannot be configured to the different frequency than SAI1
} oC_SaiPll_OutputLine_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
//! @addtogroup STM32F7-SAIPLL
//! @{

extern oC_ErrorCode_t oC_SaiPll_Configure( oC_SaiPll_OutputLine_t OutputLine , oC_Frequency_t Frequency , oC_Frequency_t PermissibleDifference , oC_Frequency_t * outRealFrequency );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_OC_SAIPLL_H_ */
