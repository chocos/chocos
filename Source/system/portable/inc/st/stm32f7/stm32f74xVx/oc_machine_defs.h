/** @hideinitializer ****************************************************************************************************************************************
 *
 * @file       oc_machine_defs.h
 *
 * @brief      Contains definitions for the uC
 *
 * @author     Patryk Kubiak - (Created on: 11 kwi 2015 20:08:10) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup MDefs Machine Definitions (MDefs)
 * @ingroup PortableSpace
 * @{
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_PORTABLE_ST_STM32F7_STM32F746NGH6_OC_MACHINE_DEFS_H_
#define SYSTEM_PORTABLE_ST_STM32F7_STM32F746NGH6_OC_MACHINE_DEFS_H_

#include <oc_machines_list.h>
#include <oc_frequency.h>

/** @hideinitializer ****************************************************************************************************************************************
 * The section CPU definitions
 */
#define _________________________________________CPU_DEFINITIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Definition that contains name of the machine
 */
//==========================================================================================================================================
#define oC_MACHINE                  STM32F746NGH6

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Family of the machine
 */
//==========================================================================================================================================
#define oC_MACHINE_FAMILY           STM32F7

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Name of the CPU CORTEX
 */
//==========================================================================================================================================
#define oC_MACHINE_CORTEX           ARM_Cortex_M7

//==========================================================================================================================================
/**
 * @hideinitializer
 * Number of bits to keep NVIC priority
 */
//==========================================================================================================================================
#define oC_MACHINE_PRIO_BITS        3

//==========================================================================================================================================
/**
 * @hideinitializer
 * Number of bytes, that memory is aligned
 */
//==========================================================================================================================================
#define oC_MACHINE_MEMORY_ALIGNMENT_BYTES      4

//==========================================================================================================================================
/**
 * @hideinitializer
 * Number of bytes, that memory is aligned
 */
//==========================================================================================================================================
#define oC_MACHINE_STACK_TOP_ALIGNMENT_BYTES      8

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the precision internal oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_PRECISION_INTERNAL_OSCILLATOR_FREQUENCY  oC_MHz(16)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the internal oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY            oC_kHz(30)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the hibernation oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_HIBERNATION_OSCILLATOR_FREQUENCY            oC_Hz(32768)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Initial value for the xPSR register
 */
//==========================================================================================================================================
#define oC_MACHINE_xPSR_INITIAL_VALUE                          0x01000000

//==========================================================================================================================================
/**
 * @hideinitializer
 * Flag if stack push command in this machine is decrementing address
 */
//==========================================================================================================================================
#define oC_MACHINE_STACK_PUSH_IS_DECREMENTING_ADDRESS          true

/* END OF SECTION */
#undef  _________________________________________CPU_DEFINITIONS_SECTION____________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with interrupts definitions
 */
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief definition of name for default interrupt handler
 *
 * This definition contains name of the function that should be called, when some of the interrupt is not defined, but occurs.
 */
//==========================================================================================================================================
#define oC_MACHINE_DEFAULT_INTERRUPT_HANDLER_NAME           _DefaultInterruptHandler

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief definition with types of interrupts
 *
 * This definition contains types of interrupts, that exists in the system. Each type must has unique name, that will be used in the #oC_MACHINE_INTERRUPTS_LIST
 * definition.
 *
 * To add new interrupt type:
 *          ADD_INTERRUPT_TYPE( TYPE_NAME )\
 *
 *                      where:
 *                                  TYPE_NAME      - any name of the interrupt type, that match \w+ rules
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERRUPTS_TYPES_LIST(ADD_INTERRUPT_TYPE)     \
    ADD_INTERRUPT_TYPE( NonMaskableInterrupt     )\
    ADD_INTERRUPT_TYPE( HardFault                )\
    ADD_INTERRUPT_TYPE( MemoryManagement         )\
    ADD_INTERRUPT_TYPE( BusFault                 )\
    ADD_INTERRUPT_TYPE( UsageFault               )\
    ADD_INTERRUPT_TYPE( SVCall                   )\
    ADD_INTERRUPT_TYPE( DebugMonitor             )\
    ADD_INTERRUPT_TYPE( PendSV                   )\
    ADD_INTERRUPT_TYPE( SysTick                  )\
    ADD_INTERRUPT_TYPE( PeripheralInterrupt      )\
    ADD_INTERRUPT_TYPE( ADCSequence0             )\
    ADD_INTERRUPT_TYPE( ADCSequence1             )\
    ADD_INTERRUPT_TYPE( ADCSequence2             )\
    ADD_INTERRUPT_TYPE( ADCSequence3             )\
    ADD_INTERRUPT_TYPE( TimerA                   )\
    ADD_INTERRUPT_TYPE( TimerB                   )\
    ADD_INTERRUPT_TYPE( SystemControl            )\
    ADD_INTERRUPT_TYPE( FlashMemCtlAndE2ROMCtl   )\
    ADD_INTERRUPT_TYPE( HibernationModule        )\
    ADD_INTERRUPT_TYPE( USB                      )\
    ADD_INTERRUPT_TYPE( Software                 )\
    ADD_INTERRUPT_TYPE( Error                    )\
    ADD_INTERRUPT_TYPE( SystemException          )\
    ADD_INTERRUPT_TYPE( WatchdogInterrupt        )\
    ADD_INTERRUPT_TYPE( PVD                      )\
    ADD_INTERRUPT_TYPE( TimeStamp                )\
    ADD_INTERRUPT_TYPE( RccGlobalInterrupt       )\
    ADD_INTERRUPT_TYPE( Stream0                  )\
    ADD_INTERRUPT_TYPE( Stream1                  )\
    ADD_INTERRUPT_TYPE( Stream2                  )\
    ADD_INTERRUPT_TYPE( Stream3                  )\
    ADD_INTERRUPT_TYPE( Stream4                  )\
    ADD_INTERRUPT_TYPE( Stream5                  )\
    ADD_INTERRUPT_TYPE( Stream6                  )\
    ADD_INTERRUPT_TYPE( Stream7                  )\
    ADD_INTERRUPT_TYPE( Tx                       )\
    ADD_INTERRUPT_TYPE( Rx0                      )\
    ADD_INTERRUPT_TYPE( Rx1                      )\
    ADD_INTERRUPT_TYPE( Sce                      )\
    ADD_INTERRUPT_TYPE( BreakInterrupt           )\
    ADD_INTERRUPT_TYPE( UpdateInterrupt          )\
    ADD_INTERRUPT_TYPE( TriggerInterrupt         )\
    ADD_INTERRUPT_TYPE( CaptureCompareInterrupt  )\
    ADD_INTERRUPT_TYPE( RtcAlarms                )\
    ADD_INTERRUPT_TYPE( OnTheGoInterrupt         )\
    ADD_INTERRUPT_TYPE( Fsmc                     )\
    ADD_INTERRUPT_TYPE( WakeUp                   )\
    ADD_INTERRUPT_TYPE( OnTheGoFsInterrupt       )\
    ADD_INTERRUPT_TYPE( OnTheGoHsEp1OutInterrupt )\
    ADD_INTERRUPT_TYPE( OnTheGoHsEp1InInterrupt  )\
    ADD_INTERRUPT_TYPE( OnTheGoHsWakeUpInterrupt )\
    ADD_INTERRUPT_TYPE( OnTheGoHsInterrupt       )\
    ADD_INTERRUPT_TYPE( SPDIFRX                  )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Definition of interrupts
 *
 * This definition contains list of interrupts that are handled in the system. To add new interrupt use
 *
 *          ADD_INTERRUPT( REGISTER_MAP_NAME , INTERRUPT_TYPE , INTERRUPT_NUMBER , VECTOR_NUMBER, ATTRIBUTE )\
 *
 *                  where:
 *                          REGISTER_MAP_NAME   - name of the register map that the interrupt is assigned or SYSTEM_INT if not used
 *                          INTERRUPT_TYPE      - name for the interrupt type from the #oC_MACHINE_INTERRUPTS_TYPES_LIST definition
 *                          INTERRUPT_NUMBER    - number of the interrupt. It can be negative value, values can be repeated, but only
 *                                                in case of different register map name.
 *                          VECTOR_NUMBER       - index in the interrupt vector array
 *                          ATTRIBUTE           - special attribute of the interrupt prototype (interrupt/naked)
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERRUPTS_LIST(ADD_INTERRUPT) \
    ADD_INTERRUPT( SYSTEM_INT , NonMaskableInterrupt        , -14 ,   2 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , HardFault                   , -13 ,   3 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , MemoryManagement            , -12 ,   4 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , BusFault                    , -11 ,   5 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , UsageFault                  , -10 ,   6 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , SVCall                      , -5  ,  11 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , DebugMonitor                , -4  ,  12 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , PendSV                      , -2  ,  14 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , SysTick                     , -1  ,  15 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , WatchdogInterrupt           ,  0  ,  16 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , PVD                         ,  1  ,  17 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , TimeStamp                   ,  2  ,  18 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , RccGlobalInterrupt          ,  3  ,  19 , interrupt )\
    ADD_INTERRUPT( EXTI0      , PeripheralInterrupt         ,  4  ,  20 , interrupt )\
    ADD_INTERRUPT( EXTI1      , PeripheralInterrupt         ,  5  ,  21 , interrupt )\
    ADD_INTERRUPT( EXTI2      , PeripheralInterrupt         ,  6  ,  22 , interrupt )\
    ADD_INTERRUPT( EXTI3      , PeripheralInterrupt         ,  7  ,  23 , interrupt )\
    ADD_INTERRUPT( EXTI4      , PeripheralInterrupt         ,  8  ,  24 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream0                     ,  9  ,  25 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream1                     , 10  ,  26 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream2                     , 11  ,  27 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream3                     , 12  ,  28 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream4                     , 13  ,  29 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream5                     , 14  ,  30 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream6                     , 15  ,  31 , interrupt )\
    ADD_INTERRUPT( ADC        , PeripheralInterrupt         , 16  ,  32 , interrupt )\
    ADD_INTERRUPT( CAN1       , Tx                          , 17  ,  33 , interrupt )\
    ADD_INTERRUPT( CAN1       , Rx0                         , 18  ,  34 , interrupt )\
    ADD_INTERRUPT( CAN1       , Rx1                         , 19  ,  35 , interrupt )\
    ADD_INTERRUPT( CAN1       , Sce                         , 20  ,  36 , interrupt )\
    ADD_INTERRUPT( EXTI5      , PeripheralInterrupt         , 21  ,  37 , interrupt )\
    ADD_INTERRUPT( Timer1     , BreakInterrupt              , 22  ,  38 , interrupt )\
    ADD_INTERRUPT( Timer1     , UpdateInterrupt             , 23  ,  39 , interrupt )\
    ADD_INTERRUPT( Timer1     , TriggerInterrupt            , 24  ,  40 , interrupt )\
    ADD_INTERRUPT( Timer1     , CaptureCompareInterrupt     , 25  ,  41 , interrupt )\
    ADD_INTERRUPT( Timer2     , PeripheralInterrupt         , 26  ,  42 , interrupt )\
    ADD_INTERRUPT( Timer3     , PeripheralInterrupt         , 27  ,  43 , interrupt )\
    ADD_INTERRUPT( Timer4     , PeripheralInterrupt         , 28  ,  44 , interrupt )\
    ADD_INTERRUPT( I2C1       , PeripheralInterrupt         , 29  ,  45 , interrupt )\
    ADD_INTERRUPT( I2C1       , Error                       , 30  ,  46 , interrupt )\
    ADD_INTERRUPT( I2C2       , PeripheralInterrupt         , 31  ,  47 , interrupt )\
    ADD_INTERRUPT( I2C2       , Error                       , 32  ,  48 , interrupt )\
    ADD_INTERRUPT( SPI1       , PeripheralInterrupt         , 33  ,  49 , interrupt )\
    ADD_INTERRUPT( SPI2       , PeripheralInterrupt         , 34  ,  50 , interrupt )\
    ADD_INTERRUPT( USART1     , PeripheralInterrupt         , 35  ,  51 , interrupt )\
    ADD_INTERRUPT( USART2     , PeripheralInterrupt         , 36  ,  52 , interrupt )\
    ADD_INTERRUPT( USART3     , PeripheralInterrupt         , 37  ,  53 , interrupt )\
    ADD_INTERRUPT( EXTI10     , PeripheralInterrupt         , 38  ,  54 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , RtcAlarms                   , 39  ,  55 , interrupt )\
    ADD_INTERRUPT( USB        , OnTheGoInterrupt            , 40  ,  56 , interrupt )\
    ADD_INTERRUPT( Timer8     , BreakInterrupt              , 41  ,  57 , interrupt )\
    ADD_INTERRUPT( Timer8     , UpdateInterrupt             , 42  ,  58 , interrupt )\
    ADD_INTERRUPT( Timer8     , TriggerInterrupt            , 43  ,  59 , interrupt )\
    ADD_INTERRUPT( Timer8     , CaptureCompareInterrupt     , 44  ,  60 , interrupt )\
    ADD_INTERRUPT( DMA1       , Stream7                     , 45  ,  61 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , Fsmc                        , 46  ,  62 , interrupt )\
    ADD_INTERRUPT( SDMMMC1    , PeripheralInterrupt         , 47  ,  63 , interrupt )\
    ADD_INTERRUPT( Timer5     , PeripheralInterrupt         , 48  ,  64 , interrupt )\
    ADD_INTERRUPT( SPI3       , PeripheralInterrupt         , 49  ,  65 , interrupt )\
    ADD_INTERRUPT( UART4      , PeripheralInterrupt         , 50  ,  66 , interrupt )\
    ADD_INTERRUPT( UART5      , PeripheralInterrupt         , 51  ,  67 , interrupt )\
    ADD_INTERRUPT( Timer6     , PeripheralInterrupt         , 52  ,  68 , interrupt )\
    ADD_INTERRUPT( Timer7     , PeripheralInterrupt         , 53  ,  69 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream0                     , 54  ,  70 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream1                     , 55  ,  71 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream2                     , 56  ,  72 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream3                     , 57  ,  73 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream4                     , 58  ,  74 , interrupt )\
    ADD_INTERRUPT( ETH        , PeripheralInterrupt         , 59  ,  75 , interrupt )\
    ADD_INTERRUPT( ETH        , WakeUp                      , 60  ,  76 , interrupt )\
    ADD_INTERRUPT( CAN2       , Tx                          , 61  ,  77 , interrupt )\
    ADD_INTERRUPT( CAN2       , Rx0                         , 62  ,  78 , interrupt )\
    ADD_INTERRUPT( CAN2       , Rx1                         , 63  ,  79 , interrupt )\
    ADD_INTERRUPT( CAN2       , Sce                         , 64  ,  80 , interrupt )\
    ADD_INTERRUPT( USB        , OnTheGoFsInterrupt          , 65  ,  81 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream5                     , 66  ,  82 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream6                     , 67  ,  83 , interrupt )\
    ADD_INTERRUPT( DMA2       , Stream7                     , 68  ,  84 , interrupt )\
    ADD_INTERRUPT( USART6     , PeripheralInterrupt         , 69  ,  85 , interrupt )\
    ADD_INTERRUPT( I2C3       , PeripheralInterrupt         , 70  ,  86 , interrupt )\
    ADD_INTERRUPT( I2C3       , Error                       , 71  ,  87 , interrupt )\
    ADD_INTERRUPT( USB        , OnTheGoHsEp1OutInterrupt    , 72  ,  88 , interrupt )\
    ADD_INTERRUPT( USB        , OnTheGoHsEp1InInterrupt     , 73  ,  89 , interrupt )\
    ADD_INTERRUPT( USB        , OnTheGoHsWakeUpInterrupt    , 74  ,  90 , interrupt )\
    ADD_INTERRUPT( USB        , OnTheGoHsInterrupt          , 75  ,  91 , interrupt )\
    ADD_INTERRUPT( DCMI       , PeripheralInterrupt         , 76  ,  92 , interrupt )\
    ADD_INTERRUPT( CRYP       , PeripheralInterrupt         , 77  ,  93 , interrupt )\
    ADD_INTERRUPT( UART7      , PeripheralInterrupt         , 78  ,  94 , interrupt )\
    ADD_INTERRUPT( UART8      , PeripheralInterrupt         , 79  ,  95 , interrupt )\
    ADD_INTERRUPT( SPI4       , PeripheralInterrupt         , 80  ,  96 , interrupt )\
    ADD_INTERRUPT( SPI5       , PeripheralInterrupt         , 81  ,  97 , interrupt )\
    ADD_INTERRUPT( SPI6       , PeripheralInterrupt         , 82  ,  98 , interrupt )\
    ADD_INTERRUPT( SAI1       , PeripheralInterrupt         , 83  ,  99 , interrupt )\
    ADD_INTERRUPT( LCDTFT     , PeripheralInterrupt         , 84  , 100 , interrupt )\
    ADD_INTERRUPT( LCDTFT     , Error                       , 85  , 101 , interrupt )\
    ADD_INTERRUPT( DMA2D      , PeripheralInterrupt         , 86  , 102 , interrupt )\
    ADD_INTERRUPT( SAI2       , PeripheralInterrupt         , 87  , 103 , interrupt )\
    ADD_INTERRUPT( QuadSpi    , PeripheralInterrupt         , 88  , 104 , interrupt )\
    ADD_INTERRUPT( LPTimer1   , PeripheralInterrupt         , 89  , 105 , interrupt )\
    ADD_INTERRUPT( HDMI       , PeripheralInterrupt         , 90  , 106 , interrupt )\
    ADD_INTERRUPT( I2C4       , PeripheralInterrupt         , 91  , 107 , interrupt )\
    ADD_INTERRUPT( I2C4       , Error                       , 92  , 108 , interrupt )\
    ADD_INTERRUPT( SYSTEM_INT , SPDIFRX                     , 93  , 109 , interrupt )\

/* END OF SECTION */
#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with pins definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________PINS_SECTION_______________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief number of bits in port
 *
 * This definition contains number of bits, that the port contains pins in maximum. This can be only set to one of the values: 8 , 16 , 32 , 64
 */
//==========================================================================================================================================
#define oC_MACHINE_PORT_WIDTH                   16

//==========================================================================================================================================
/** @hideinitializer
 * This is the list that contains definitions of pins and ports.
 *
 * To add port use format:
 *          ADD_PIN( REGISTER_MAP_NAME , PIN_NAME , BIT_INDEX , PIN_NUMBER )
 *
 *                  where:
 *                          REGISTER_MAP_NAME   - name of the register map address from the #oC_MACHINE_GPIO_REGISTERS_MAPS_LIST definition
 *                          PIN_NAME            - name of the pin (PA0 for example)
 *                          BIT_INDEX           - index of the bit in the port for this pin
 *                          PIN_NUMBER          - number of pin in the hardware
 */
//==========================================================================================================================================
#define oC_MACHINE_PINS(ADD_PIN)\
        ADD_PIN( PORTE , PE2  ,  2 , 1 )\
        ADD_PIN( PORTE , PE3  ,  3 , 2 )\
        ADD_PIN( PORTE , PE4  ,  4 , 3 )\
        ADD_PIN( PORTE , PE5  ,  5 , 4 )\
        ADD_PIN( PORTE , PE6  ,  6 , 5 )\
        ADD_PIN( PORTC , PC0  ,  0 , 15 )\
        ADD_PIN( PORTC , PC1  ,  1 , 16 )\
        ADD_PIN( PORTC , PC2  ,  2 , 17 )\
        ADD_PIN( PORTC , PC3  ,  3 , 18 )\
        ADD_PIN( PORTA , PA1  ,  1 , 23 )\
        ADD_PIN( PORTA , PA2  ,  2 , 24 )\
        ADD_PIN( PORTA , PA3  ,  3 , 25 )\
        ADD_PIN( PORTA , PA4  ,  4 , 28 )\
        ADD_PIN( PORTA , PA5  ,  5 , 29 )\
        ADD_PIN( PORTA , PA6  ,  6 , 30 )\
        ADD_PIN( PORTA , PA7  ,  7 , 31 )\
        ADD_PIN( PORTC , PC4  ,  4 , 32 )\
        ADD_PIN( PORTC , PC5  ,  5 , 33 )\
        ADD_PIN( PORTB , PB0  ,  0 , 34 )\
        ADD_PIN( PORTB , PB1  ,  1 , 35 )\
        ADD_PIN( PORTB , PB2  ,  2 , 36 )\
        ADD_PIN( PORTE , PE7  ,  7 , 37 )\
        ADD_PIN( PORTE , PE8  ,  8 , 38 )\
        ADD_PIN( PORTE , PE9  ,  9 , 39 )\
        ADD_PIN( PORTE , PE10 , 10 , 40 )\
        ADD_PIN( PORTE , PE11 , 11 , 41 )\
        ADD_PIN( PORTE , PE12 , 12 , 42 )\
        ADD_PIN( PORTE , PE13 , 13 , 43 )\
        ADD_PIN( PORTE , PE14 , 14 , 44 )\
        ADD_PIN( PORTE , PE15 , 15 , 45 )\
        ADD_PIN( PORTB , PB10 , 10 , 46 )\
        ADD_PIN( PORTB , PB11 , 11 , 47 )\
        ADD_PIN( PORTB , PB12 , 12 , 51 )\
        ADD_PIN( PORTB , PB13 , 13 , 52 )\
        ADD_PIN( PORTB , PB14 , 14 , 53 )\
        ADD_PIN( PORTB , PB15 , 15 , 54 )\
        ADD_PIN( PORTD , PD8  ,  8 , 55 )\
        ADD_PIN( PORTD , PD9  ,  9 , 56 )\
        ADD_PIN( PORTD , PD10 , 10 , 57 )\
        ADD_PIN( PORTD , PD11 , 11 , 58 )\
        ADD_PIN( PORTD , PD12 , 12 , 59 )\
        ADD_PIN( PORTD , PD13 , 13 , 60 )\
        ADD_PIN( PORTD , PD14 , 14 , 61 )\
        ADD_PIN( PORTD , PD15 , 15 , 62 )\
        ADD_PIN( PORTC , PC6  ,  6 , 63 )\
        ADD_PIN( PORTC , PC7  ,  7 , 64 )\
        ADD_PIN( PORTC , PC8  ,  8 , 65 )\
        ADD_PIN( PORTC , PC9  ,  9 , 66 )\
        ADD_PIN( PORTA , PA8  ,  8 , 67 )\
        ADD_PIN( PORTA , PA9  ,  9 , 68 )\
        ADD_PIN( PORTA , PA10 , 10 , 69 )\
        ADD_PIN( PORTA , PA11 , 11 , 70 )\
        ADD_PIN( PORTA , PA12 , 12 , 71 )\
        ADD_PIN( PORTA , PA13 , 13 , 72 )\
        ADD_PIN( PORTA , PA14 , 14 , 76 )\
        ADD_PIN( PORTA , PA15 , 15 , 77 )\
        ADD_PIN( PORTC , PC10 , 10 , 78 )\
        ADD_PIN( PORTC , PC11 , 11 , 79 )\
        ADD_PIN( PORTC , PC12 , 12 , 80 )\
        ADD_PIN( PORTD , PD0  ,  0 , 81 )\
        ADD_PIN( PORTD , PD1  ,  1 , 82 )\
        ADD_PIN( PORTD , PD2  ,  2 , 83 )\
        ADD_PIN( PORTD , PD3  ,  3 , 84 )\
        ADD_PIN( PORTD , PD4  ,  4 , 85 )\
        ADD_PIN( PORTD , PD5  ,  5 , 86 )\
        ADD_PIN( PORTD , PD6  ,  6 , 87 )\
        ADD_PIN( PORTD , PD7  ,  7 , 88 )\
        ADD_PIN( PORTB , PB3  ,  3 , 89 )\
        ADD_PIN( PORTB , PB4  ,  4 , 90 )\
        ADD_PIN( PORTB , PB5  ,  5 , 91 )\
        ADD_PIN( PORTB , PB6  ,  6 , 92 )\
        ADD_PIN( PORTB , PB7  ,  7 , 93 )\
        ADD_PIN( PORTB , PB8  ,  8 , 95 )\
        ADD_PIN( PORTB , PB9  ,  9 , 96 )\
        ADD_PIN( PORTE , PE0  ,  0 , 97 )\
        ADD_PIN( PORTE , PE1  ,  1 , 98 )\


//==========================================================================================================================================
/**
 * @hideinitializer
 * List of pin functions for the UART
 *
 * To add pin function:
 *              ADD_PIN_FUNCTION( TYPE_NAME )\
 *
 *                     where:
 *                              TYPE_NAME   - any \w+ name of the pin type
 */
//==========================================================================================================================================
#define oC_MACHINE_UART_PIN_FUNCTIONS(ADD_PIN_FUNCTION) \
    ADD_PIN_FUNCTION(Rx) \
    ADD_PIN_FUNCTION(Tx) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * List of pins for UART module
 *
 * To add SPI pin:
 *              ADD_PIN( PIN_NAME , REGISTER_MAP_NAME , PIN_FUNCTION , DIGITAL_FUNCTION )\
 *
 *                      where:
 *                                  PIN_NAME            - name of the pin from #oC_MACHINE_PINS
 *                                  REGISTER_MAP_NAME   - name of the register map from #oC_MACHINE_UART_REGISTERS_MAPS_LIST
 *                                  PIN_FUNCTION        - function of the pin from #oC_MACHINE_UART_PIN_FUNCTIONS
 *                                  DIGITAL_FUNCTION    - digital function value to set in the special alternative GPIO register
 */
//==========================================================================================================================================
#define oC_MACHINE_UART_PERIPHERAL_PINS(ADD_PIN) \
    ADD_PIN( PA0 , UART0 , Rx , 1 ) \
    ADD_PIN( PA1 , UART0 , Tx , 1 ) \
    ADD_PIN( PB0 , UART1 , Rx , 1 ) \
    ADD_PIN( PB1 , UART1 , Tx , 1 ) \
    ADD_PIN( PC4 , UART1 , Rx , 2 ) \
    ADD_PIN( PC5 , UART1 , Tx , 2 ) \
    ADD_PIN( PC4 , UART4 , Rx , 1 ) \
    ADD_PIN( PC5 , UART5 , Tx , 1 ) \
    ADD_PIN( PC6 , UART3 , Rx , 1 ) \
    ADD_PIN( PC7 , UART3 , Tx , 1 ) \
    ADD_PIN( PD4 , UART6 , Rx , 1 ) \
    ADD_PIN( PD5 , UART6 , Tx , 1 ) \
    ADD_PIN( PD6 , UART2 , Rx , 1 ) \
    ADD_PIN( PD7 , UART2 , Tx , 1 ) \
    ADD_PIN( PE0 , UART7 , Rx , 1 ) \
    ADD_PIN( PE1 , UART7 , Tx , 1 ) \
    ADD_PIN( PE4 , UART5 , Rx , 1 ) \
    ADD_PIN( PE5 , UART5 , Tx , 1 ) \


//==========================================================================================================================================
/**
 * @hideinitializer
 * List of pin functions for the SPI
 *
 * To add pin function:
 *              ADD_PIN_FUNCTION( TYPE_NAME )\
 *
 *                     where:
 *                              TYPE_NAME   - any \w+ name of the pin type
 */
//==========================================================================================================================================
#define oC_MACHINE_TIMER_PIN_FUNCTIONS(ADD_PIN_FUNCTION) \
    ADD_PIN_FUNCTION(A) \
    ADD_PIN_FUNCTION(B) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * List of pins for SPI module
 *
 * To add SPI pin:
 *              ADD_PIN( PIN_NAME , REGISTER_MAP_NAME , PIN_FUNCTION , DIGITAL_FUNCTION )\
 *
 *                      where:
 *                                  PIN_NAME            - name of the pin from #oC_MACHINE_PINS
 *                                  REGISTER_MAP_NAME   - name of the register map from #oC_MACHINE_SPI_REGISTERS_MAPS_LIST
 *                                  PIN_FUNCTION        - function of the pin from #oC_MACHINE_SPI_PIN_FUNCTIONS
 *                                  DIGITAL_FUNCTION    - digital function value to set in the special alternative GPIO register
 */
//==========================================================================================================================================
#define oC_MACHINE_TIMER_PERIPHERAL_PINS(ADD_PIN) \
    ADD_PIN( PB0 , Timer2 , A , 7 ) \
    ADD_PIN( PB1 , Timer2 , B , 7 ) \
    ADD_PIN( PB2 , Timer3 , A , 7 ) \
    ADD_PIN( PB3 , Timer3 , B , 7 ) \
    ADD_PIN( PB4 , Timer1 , A , 7 ) \
    ADD_PIN( PB5 , Timer1 , B , 7 ) \
    ADD_PIN( PB6 , Timer0 , A , 7 ) \
    ADD_PIN( PB7 , Timer0 , B , 7 ) \
    ADD_PIN( PC0 , Timer4 , A , 7 ) \
    ADD_PIN( PC1 , Timer4 , B , 7 ) \
    ADD_PIN( PC2 , Timer5 , A , 7 ) \
    ADD_PIN( PC3 , Timer5 , B , 7 ) \
    ADD_PIN( PC4 , WideTimer0 , A , 7 ) \
    ADD_PIN( PC5 , WideTimer0 , B , 7 ) \
    ADD_PIN( PC6 , WideTimer1 , A , 7 ) \
    ADD_PIN( PC7 , WideTimer1 , B , 7 ) \
    ADD_PIN( PD0 , WideTimer2 , A , 7 ) \
    ADD_PIN( PD1 , WideTimer2 , B , 7 ) \
    ADD_PIN( PD2 , WideTimer3 , A , 7 ) \
    ADD_PIN( PD3 , WideTimer3 , B , 7 ) \
    ADD_PIN( PD4 , WideTimer4 , A , 7 ) \
    ADD_PIN( PD5 , WideTimer4 , B , 7 ) \
    ADD_PIN( PD6 , WideTimer5 , A , 7 ) \
    ADD_PIN( PD7 , WideTimer5 , B , 7 ) \
    ADD_PIN( PF0 , Timer0 , A , 7 ) \
    ADD_PIN( PF1 , Timer0 , B , 7 ) \
    ADD_PIN( PF2 , Timer1 , A , 7 ) \
    ADD_PIN( PF3 , Timer1 , B , 7 ) \
    ADD_PIN( PF4 , Timer2 , A , 7 ) \


//==========================================================================================================================================
/**
 * @hideinitializer
 * List of pin functions for the SPI
 *
 * To add pin function:
 *              ADD_PIN_FUNCTION( TYPE_NAME )\
 *
 *                     where:
 *                              TYPE_NAME   - any \w+ name of the pin type
 */
//==========================================================================================================================================
#define oC_MACHINE_SPI_PIN_FUNCTIONS(ADD_PIN_FUNCTION) \
    ADD_PIN_FUNCTION(Clk) \
    ADD_PIN_FUNCTION(Fss) \
    ADD_PIN_FUNCTION(Rx) \
    ADD_PIN_FUNCTION(Tx) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * List of pins for SPI module
 *
 * To add SPI pin:
 *              ADD_PIN( PIN_NAME , REGISTER_MAP_NAME , PIN_FUNCTION , DIGITAL_FUNCTION )\
 *
 *                      where:
 *                                  PIN_NAME            - name of the pin from #oC_MACHINE_PINS
 *                                  REGISTER_MAP_NAME   - name of the register map from #oC_MACHINE_SPI_REGISTERS_MAPS_LIST
 *                                  PIN_FUNCTION        - function of the pin from #oC_MACHINE_SPI_PIN_FUNCTIONS
 *                                  DIGITAL_FUNCTION    - digital function value to set in the special alternative GPIO register
 */
//==========================================================================================================================================
#define oC_MACHINE_SPI_PERIPHERAL_PINS(ADD_PIN) \
    ADD_PIN( PA2 , SSI0 , Clk , 2 ) \
    ADD_PIN( PA3 , SSI0 , Fss , 2 ) \
    ADD_PIN( PA4 , SSI0 , Rx  , 2 ) \
    ADD_PIN( PA5 , SSI0 , Tx  , 2 ) \
    ADD_PIN( PD0 , SSI1 , Clk , 2 ) \
    ADD_PIN( PD1 , SSI1 , Fss , 2 ) \
    ADD_PIN( PD2 , SSI1 , Rx  , 2 ) \
    ADD_PIN( PD3 , SSI1 , Tx  , 2 ) \
    ADD_PIN( PF0 , SSI1 , Rx  , 2 ) \
    ADD_PIN( PF1 , SSI1 , Tx  , 2 ) \
    ADD_PIN( PF2 , SSI1 , Clk , 2 ) \
    ADD_PIN( PF3 , SSI1 , Fss , 2 ) \
    ADD_PIN( PB4 , SSI2 , Clk , 2 ) \
    ADD_PIN( PB5 , SSI2 , Fss , 2 ) \
    ADD_PIN( PB6 , SSI2 , Rx  , 2 ) \
    ADD_PIN( PB7 , SSI2 , Tx  , 2 ) \
    ADD_PIN( PD0 , SSI3 , Clk , 1 ) \
    ADD_PIN( PD1 , SSI3 , Fss , 1 ) \
    ADD_PIN( PD2 , SSI3 , Rx  , 1 ) \
    ADD_PIN( PD3 , SSI3 , Tx  , 1 ) \

#undef  _________________________________________PINS_SECTION_______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with QuadSPI
 *
 *  ======================================================================================================================================*/
#define _________________________________________Quad_SPI_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for QuadSPI ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_QuadSPI_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( QuadSPI , 0xA0001000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for QuadSPI.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_QuadSPI_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________Quad_SPI_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with FMC
 *
 *  ======================================================================================================================================*/
#define _________________________________________FMC_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for FMC ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_FMC_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( FMC , 0xA0000000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for FMC.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_FMC_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________FMC_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with RNG
 *
 *  ======================================================================================================================================*/
#define _________________________________________RNG_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for RNG ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_RNG_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( RNG , 0x50060800UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for RNG.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_RNG_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________RNG_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with HASH
 *
 *  ======================================================================================================================================*/
#define _________________________________________HASH_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for HASH ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_HASH_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( HASH , 0x50060400UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for HASH.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_HASH_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________HASH_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with CRYPT
 *
 *  ======================================================================================================================================*/
#define _________________________________________CRYPT_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for CRYPT ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_CRYPT_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( CRYPT , 0x50060000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for CRYPT.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_CRYPT_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________CRYPT_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with DCMI
 *
 *  ======================================================================================================================================*/
#define _________________________________________DCMI_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for DCMI ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_DCMI_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( DCMI , 0x50050000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for DCMI.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_DCMI_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________DCMI_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with USB
 *
 *  ======================================================================================================================================*/
#define _________________________________________USB_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for USB ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_USB_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( USB_FS , 0x50000000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( USB_HS , 0x40040000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for USB.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_USB_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________USB_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with DMA2D
 *
 *  ======================================================================================================================================*/
#define _________________________________________DMA2D_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for DMA2D ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA2D_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( DMA2D , 0x4002B000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for DMA2D.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA2D_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________DMA2D_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with Ethernet
 *
 *  ======================================================================================================================================*/
#define _________________________________________Ethernet_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for Ethernet ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_Ethernet_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( Ethernet , 0x40028000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for Ethernet.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_Ethernet_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________Ethernet_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with DMA
 *
 *  ======================================================================================================================================*/
#define _________________________________________DMA_SECTION___________________________________________________________________________


//==========================================================================================================================================
/**
 * @hideinitializer
 * The definition contain types of DMA signals for #oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST
 *
 * To add signal use:
 *              ADD_SIGNAL( NAME )\
 *                          NAME - name of the signal to add
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_SIGNAL_TYPE_LIST(ADD_SIGNAL) \
    ADD_SIGNAL( EP1_RX ) \
    ADD_SIGNAL( EP1_TX ) \
    ADD_SIGNAL( EP2_RX ) \
    ADD_SIGNAL( EP2_TX )\
    ADD_SIGNAL( EP3_RX ) \
    ADD_SIGNAL( EP3_TX ) \
    ADD_SIGNAL( RX ) \
    ADD_SIGNAL( TX ) \
    ADD_SIGNAL( Default ) \
    ADD_SIGNAL( SS0 ) \
    ADD_SIGNAL( SS1 ) \
    ADD_SIGNAL( SS2 ) \
    ADD_SIGNAL( SS3 ) \
    ADD_SIGNAL( A ) \
    ADD_SIGNAL( B ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * The definition contains assignments of DMA channels.
 *
 * To add channel assignment use:
 *
 *          ADD_CHANNEL_ASSIGNMENT( DMA_REGISTER_MAP_NAME , REGISTER_MAP_NAME , SIGNAL_TYPE , TYPE , ENC )\
 *
 *                      where:
 *                                  DMA_REGISTER_MAP_NAME - name of the register map channel from the #oC_MACHINE_DMA_REGISTERS_MAPS_LIST definition
 *                                  REGISTER_MAP_NAME     - any name of the register map that the channel is assigned
 *                                  SIGNAL_TYPE           - type of the signal from #oC_MACHINE_DMA_SIGNAL_TYPE_LIST definition
 *                                  TYPE                  - indicate if particular peripheral uses a single request (S), burst request (B)
 *                                                          or both (SB)
 *                                  ENC                   - encoding for the respective DMACHMAPn bit field
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST(ADD_CHANNEL_ASSIGNMENT)   \
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , USB0        , EP1_RX  , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , USB0        , EP1_TX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , USB0        , EP2_RX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , USB0        , EP2_TX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , USB0        , EP3_RX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , USB0        , EP3_TX  ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , UART0       , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , UART0       , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , SSI0        , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , SSI0        , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , ADC0        , SS0     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , ADC0        , SS1     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , ADC0        , SS2     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , ADC0        , SS3     ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , Timer0      , A       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Timer0      , B       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Timer1      , A       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Timer1      , B       ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , UART1       , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , UART1       , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , SSI1        , RX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , SSI1        , TX      , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 0)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , UART2       , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , UART2       , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Timer3      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Timer3      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , Timer2      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , Timer2      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , Timer2      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , Timer2      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , UART1       , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , UART1       , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , SSI1        , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , SSI1        , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , UART2       , RX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , UART2       , TX      , SB , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , Timer2      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , Timer2      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , Timer1      , A       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Timer1      , B       ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , ADC1        , SS0     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , ADC1        , SS1     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , ADC1        , SS2     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , ADC1        , SS3     ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 1)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , UART5       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , UART5       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , UART6       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , UART6       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , SSI2        , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , SSI2        , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , SSI3        , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , SSI3        , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , UART3       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , UART3       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , UART4       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , UART4       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , UART7       , RX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , UART7       , TX      , SB , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 2)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , Timer4      , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , Timer4      , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , PORTA       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , PORTB       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , PORTC       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , PORTD       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , Timer5      , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , Timer5      , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , WideTimer0  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , WideTimer0  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , WideTimer1  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , WideTimer1  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , PORTE       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , PORTF       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , WideTimer2  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , WideTimer2  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , PORTB       , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , WideTimer3  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , WideTimer3  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , WideTimer4  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , WideTimer4  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , WideTimer5  , A       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , WideTimer5  , B       ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 3)\
    ADD_CHANNEL_ASSIGNMENT( uDMA0  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA1  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA2  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA3  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA4  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA5  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA6  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA7  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA8  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA9  , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA10 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA11 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA12 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA13 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA14 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA15 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA16 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA17 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA18 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA19 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA20 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA21 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA22 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA23 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA24 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA25 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA26 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA27 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA28 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA29 , Software    , Default ,  B , 4)\
    ADD_CHANNEL_ASSIGNMENT( uDMA30 , Software    , Default ,  B , 4)\

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for DMA ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( DMA1 , 0x40026000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( DMA2 , 0x40026400UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for DMA.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________DMA_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with RCC
 *
 *  ======================================================================================================================================*/
#define _________________________________________RCC_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for RCC ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_RCC_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( BKPSRAM , 0x40024000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( RCC     , 0x40023800UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for RCC.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_RCC_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________RCC_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with CRC
 *
 *  ======================================================================================================================================*/
#define _________________________________________CRC_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for CRC ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_CRC_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( CRC , 0x40023000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for CRC.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_CRC_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________CRC_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with GPIO
 *
 *  ======================================================================================================================================*/
#define _________________________________________GPIO_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for GPIO ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_GPIO_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \

#if 0
    ADD_REGISTER_MAP( PORTA , 0x40020000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTB , 0x40020400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTC , 0x40020800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTD , 0x40020C00UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTE , 0x40021000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTF , 0x40021400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTG , 0x40021800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTH , 0x40021C00UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTI , 0x40022000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTJ , 0x40022400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( PORTK , 0x40022800UL , NONE , NONE , 0 ) \

#endif

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for GPIO.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_GPIO_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________GPIO_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with LCDTFT
 *
 *  ======================================================================================================================================*/
#define _________________________________________LCDTFT_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for LCDTFT ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_LCDTFT_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( LCDTFT , 0x40016800UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for LCDTFT.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_LCDTFT_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________LCDTFT_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with SAI
 *
 *  ======================================================================================================================================*/
#define _________________________________________SAI_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for SAI ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_SAI_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( SAI1 , 0x40015800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( SAI2 , 0x40015C00UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for SAI.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_SAI_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________SAI_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with SPI
 *
 *  ======================================================================================================================================*/
#define _________________________________________SPI_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for SPI ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_SPI_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( SPI1 , 0x40013000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( SPI2 , 0x40003800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( SPI3 , 0x40003C00UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( SPI4 , 0x40013400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( SPI5 , 0x40015000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( SPI6 , 0x40015400UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for SPI.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_SPI_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________SPI_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with ACTIM
 *
 *  ======================================================================================================================================*/
#define _________________________________________ACTIM_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for ACTIM ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_ACTIM_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( TIM1 , 0x40010000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM8 , 0x40010400UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for ACTIM.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_ACTIM_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________ACTIM_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with GPTIM1
 *
 *  ======================================================================================================================================*/
#define _________________________________________GPTIM1_SECTION___________________________________________________________________________


//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for GPTIM1 ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_GPTIM1_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( TIM2  , 0x40000000UL  , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM3  , 0x40000400UL  , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM4  , 0x40000800UL  , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM5  , 0x40000C00UL  , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for GPTIM1.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_GPTIM1_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________GPTIM1_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with GPTIM2
 *
 *  ======================================================================================================================================*/
#define _________________________________________GPTIM2_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for GPTIM2 ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_GPTIM2_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( TIM10 , 0x40014400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM11 , 0x40014800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM13 , 0x40001C00UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM14 , 0x40002000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for GPTIM2.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_GPTIM2_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________GPTIM2_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with GPTIM3
 *
 *  ======================================================================================================================================*/
#define _________________________________________GPTIM3_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for GPTIM3 ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_GPTIM3_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( TIM9  , 0x40014000UL  , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM12 , 0x40001800UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for GPTIM3.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_GPTIM3_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________GPTIM3_SECTION___________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with BTIM
 *
 *  ======================================================================================================================================*/
#define _________________________________________BTIM_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for BTIM ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_BTIM_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( TIM6 , 0x40001000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( TIM7 , 0x40001400UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for BTIM.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_BTIM_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________BTIM_SECTION___________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with EXTI
 *
 *  ======================================================================================================================================*/
#define _________________________________________EXTI_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for EXTI ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_EXTI_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( EXTI , 0x40013C00UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for EXTI.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_EXTI_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________EXTI_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with SYSCFG
 *
 *  ======================================================================================================================================*/
#define _________________________________________SYSCFG_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for SYSCFG ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_SYSCFG_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( SYSCFG , 0x40013800UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for SYSCFG.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_SYSCFG_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________SYSCFG_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with SDMMC
 *
 *  ======================================================================================================================================*/
#define _________________________________________SDMMC_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for SDMMC ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_SDMMC_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( SDMMC1 , 0x40012C00UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for SDMMC.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_SDMMC_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________SDMMC_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with ADC
 *
 *  ======================================================================================================================================*/
#define _________________________________________ADC_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for ADC ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_ADC_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( ADC , 0x40012000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for ADC.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_ADC_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________ADC_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with USART
 *
 *  ======================================================================================================================================*/
#define _________________________________________USART_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for USART ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_USART_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( USART , 0xA0001000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for USART.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_USART_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________USART_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with UART
 *
 *  ======================================================================================================================================*/
#define _________________________________________UART_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for UART ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_UART_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( UART1  , 0x40011000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( USART2 , 0x40004400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( USART3 , 0x40004800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( UART4  , 0x40004C00UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( UART5  , 0x40005000UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( UART6  , 0x40011400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( UART7  , 0x40007800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( UART8  , 0x40007C00UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for UART.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_UART_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________UART_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with DAC
 *
 *  ======================================================================================================================================*/
#define _________________________________________DAC_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for DAC ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_DAC_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( DAC , 0x40007400UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for DAC.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_DAC_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________DAC_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with PWR
 *
 *  ======================================================================================================================================*/
#define _________________________________________PWR_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for PWR ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_PWR_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( PWR , 0x40007000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for PWR.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_PWR_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________PWR_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with HDMI
 *
 *  ======================================================================================================================================*/
#define _________________________________________HDMI_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for HDMI ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_HDMI_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( HDMI , 0x40006C00UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for HDMI.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_HDMI_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________HDMI_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with CAN
 *
 *  ======================================================================================================================================*/
#define _________________________________________CAN_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for CAN ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_CAN_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( CAN1 , 0x40006400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( CAN2 , 0x40006800UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for CAN.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_CAN_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________CAN_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with I2C
 *
 *  ======================================================================================================================================*/
#define _________________________________________I2C_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for I2C ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_I2C_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( I2C1 , 0x40005400UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( I2C2 , 0x40005800UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( I2C3 , 0x40005C00UL , NONE , NONE , 0 ) \
    ADD_REGISTER_MAP( I2C4 , 0x40006000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for I2C.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_I2C_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________I2C_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with SPDIFRX
 *
 *  ======================================================================================================================================*/
#define _________________________________________SPDIFRX_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for SPDIFRX ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_SPDIFRX_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( SPDIFRX , 0x40004000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for SPDIFRX.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_SPDIFRX_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________SPDIFRX_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with IWDG
 *
 *  ======================================================================================================================================*/
#define _________________________________________IWDG_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for IWDG ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_IWDG_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( IWDG , 0x40003000UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for IWDG.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_IWDG_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________IWDG_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with WWDG
 *
 *  ======================================================================================================================================*/
#define _________________________________________WWDG_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for WWDG ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_WWDG_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( WWDG , 0x40002C00UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for WWDG.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_WWDG_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________WWDG_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with RTC
 *
 *  ======================================================================================================================================*/
#define _________________________________________RTC_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for RTC ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_RTC_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( RTC , 0x40002800UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for RTC.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_RTC_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________RTC_SECTION___________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with LPTIM
 *
 *  ======================================================================================================================================*/
#define _________________________________________LPTIM_SECTION___________________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of registers maps list for LPTIM ports
 *
 * To add register map:
 *
 *      ADD_REGISTER_MAP( MAP_NAME , ADDRESS , POWER_MAP , POWER_REGISTER , POWER_BIT )\
 *
 *             where:
 *                      MAP_NAME        - any \w register map name
 *                      ADDRESS         - address of the register map
 *                      POWER_MAP       - the name of the register map where is the power register (NONE if not used)
 *                      POWER_REGISTER  - register to turn on power (NONE if not used)
 *                      POWER_BIT       - bit in power register
 */
//==========================================================================================================================================
#define oC_MACHINE_LPTIM_REGISTERS_MAPS_LIST(ADD_REGISTER_MAP) \
    ADD_REGISTER_MAP( LPTIM1 , 0x40002400UL , NONE , NONE , 0 ) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the register map for LPTIM.
 *
 * To add register use:
 *
 *              ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 *                              where:
 *                                          REGISTER_NAME   - any \w+ register nam
 *                                          OFFSET          - offset to register map base
 *                                          ACCESS          - RO / WO / RW / W1C
 */
//==========================================================================================================================================
#define oC_MACHINE_LPTIM_REGISTER_MAP( ADD_REGISTER ) \
    ADD_REGISTER( DEFAULT               , 0x000UL , RW , oC_MACHINE_REGISTER_DEFAULT) \

#undef  _________________________________________LPTIM_SECTION___________________________________________________________________________

/** @hideinitializer ****************************************************************************************************************************************
 * The section with list of modules definitions
 */
#define _________________________________________LIST_OF_MODULES_SECTION____________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * List of modules in the uC
 *
 * To add module:
 *
 *      ADD_MODULE( MODULE_NAME , REGISTERS_MAPS_LIST , REGISTER_MAP ) \
 *
 *              where:
 *                          MODULE_NAME             - any \w module name
 *                          REGISTERS_MAPS_LIST     - list of registers maps, where each element is added in format: ADD( RMAP_NAME , ADDRESS , ...)
 *                          REGISTER_MAP            - list of registers in format ADD_REGISTER( REGISTER_NAME , OFFSET , ACCESS , REGISTER_BITS )
 *
 */
//==========================================================================================================================================
#define oC_MACHINE_MODULES_LIST(ADD_MODULE) \
    ADD_MODULE( QuadSPI        , oC_MACHINE_QuadSPI_REGISTERS_MAPS_LIST    , oC_MACHINE_QuadSPI_REGISTER_MAP) \
    ADD_MODULE( FMC            , oC_MACHINE_FMC_REGISTERS_MAPS_LIST        , oC_MACHINE_FMC_REGISTER_MAP) \
    ADD_MODULE( RNG            , oC_MACHINE_RNG_REGISTERS_MAPS_LIST        , oC_MACHINE_RNG_REGISTER_MAP) \
    ADD_MODULE( HASH           , oC_MACHINE_HASH_REGISTERS_MAPS_LIST       , oC_MACHINE_HASH_REGISTER_MAP) \
    ADD_MODULE( CRYP           , oC_MACHINE_CRYP_REGISTERS_MAPS_LIST       , oC_MACHINE_CRYP_REGISTER_MAP) \
    ADD_MODULE( DCMI           , oC_MACHINE_DCMI_REGISTERS_MAPS_LIST       , oC_MACHINE_DCMI_REGISTER_MAP) \
    ADD_MODULE( USB            , oC_MACHINE_USB_REGISTERS_MAPS_LIST        , oC_MACHINE_USB_REGISTER_MAP) \
    ADD_MODULE( DMA2D          , oC_MACHINE_DMA2D_REGISTERS_MAPS_LIST      , oC_MACHINE_DMA2D_REGISTER_MAP) \
    ADD_MODULE( Ethernet       , oC_MACHINE_Ethernet_REGISTERS_MAPS_LIST   , oC_MACHINE_Ethernet_REGISTER_MAP) \
    ADD_MODULE( DMA            , oC_MACHINE_DMA_REGISTERS_MAPS_LIST        , oC_MACHINE_DMA_REGISTER_MAP) \
    ADD_MODULE( RCC            , oC_MACHINE_RCC_REGISTERS_MAPS_LIST        , oC_MACHINE_RCC_REGISTER_MAP) \
    ADD_MODULE( CRC            , oC_MACHINE_CRC_REGISTERS_MAPS_LIST        , oC_MACHINE_CRC_REGISTER_MAP) \
    ADD_MODULE( GPIO           , oC_MACHINE_GPIO_REGISTERS_MAPS_LIST       , oC_MACHINE_GPIO_REGISTER_MAP) \
    ADD_MODULE( LCDTFT         , oC_MACHINE_LCDTFT_REGISTERS_MAPS_LIST     , oC_MACHINE_LCDTFT_REGISTER_MAP) \
    ADD_MODULE( SAI            , oC_MACHINE_SAI_REGISTERS_MAPS_LIST        , oC_MACHINE_SAI_REGISTER_MAP) \
    ADD_MODULE( SPI            , oC_MACHINE_SPI_REGISTERS_MAPS_LIST        , oC_MACHINE_SPI_REGISTER_MAP) \
    ADD_MODULE( ACTIM          , oC_MACHINE_ACTIM_REGISTERS_MAPS_LIST      , oC_MACHINE_ACTIM_REGISTER_MAP) \
    ADD_MODULE( GPTIM1         , oC_MACHINE_GPTIM1_REGISTERS_MAPS_LIST     , oC_MACHINE_GPTIM1_REGISTER_MAP) \
    ADD_MODULE( GPTIM2         , oC_MACHINE_GPTIM2_REGISTERS_MAPS_LIST     , oC_MACHINE_GPTIM2_REGISTER_MAP) \
    ADD_MODULE( GPTIM3         , oC_MACHINE_GPTIM3_REGISTERS_MAPS_LIST     , oC_MACHINE_GPTIM3_REGISTER_MAP) \
    ADD_MODULE( BTIM           , oC_MACHINE_BTIM_REGISTERS_MAPS_LIST       , oC_MACHINE_BTIM_REGISTER_MAP) \
    ADD_MODULE( EXTI           , oC_MACHINE_EXTI_REGISTERS_MAPS_LIST       , oC_MACHINE_EXTI_REGISTER_MAP) \
    ADD_MODULE( SYSCFG         , oC_MACHINE_SYSCFG_REGISTERS_MAPS_LIST     , oC_MACHINE_SYSCFG_REGISTER_MAP) \
    ADD_MODULE( SDMMC          , oC_MACHINE_SDMMC_REGISTERS_MAPS_LIST      , oC_MACHINE_SDMMC_REGISTER_MAP) \
    ADD_MODULE( ADC            , oC_MACHINE_ADC_REGISTERS_MAPS_LIST        , oC_MACHINE_ADC_REGISTER_MAP) \
    ADD_MODULE( USART          , oC_MACHINE_USART_REGISTERS_MAPS_LIST      , oC_MACHINE_USART_REGISTER_MAP) \
    ADD_MODULE( UART           , oC_MACHINE_UART_REGISTERS_MAPS_LIST       , oC_MACHINE_USART_REGISTER_MAP) \
    ADD_MODULE( DAC            , oC_MACHINE_DAC_REGISTERS_MAPS_LIST        , oC_MACHINE_DAC_REGISTER_MAP) \
    ADD_MODULE( PWR            , oC_MACHINE_PWR_REGISTERS_MAPS_LIST        , oC_MACHINE_PWR_REGISTER_MAP) \
    ADD_MODULE( HDMI           , oC_MACHINE_HDMI_REGISTERS_MAPS_LIST       , oC_MACHINE_HDMI_REGISTER_MAP) \
    ADD_MODULE( CAN            , oC_MACHINE_CAN_REGISTERS_MAPS_LIST        , oC_MACHINE_CAN_REGISTER_MAP) \
    ADD_MODULE( I2C            , oC_MACHINE_I2C_REGISTERS_MAPS_LIST        , oC_MACHINE_I2C_REGISTER_MAP) \
    ADD_MODULE( SPDIFRX        , oC_MACHINE_SPDIFRX_REGISTERS_MAPS_LIST    , oC_MACHINE_SPDIFRX_REGISTER_MAP) \
    ADD_MODULE( IWDG           , oC_MACHINE_IWDG_REGISTERS_MAPS_LIST       , oC_MACHINE_IWDG_REGISTER_MAP) \
    ADD_MODULE( WWDG           , oC_MACHINE_WWDG_REGISTERS_MAPS_LIST       , oC_MACHINE_WWDG_REGISTER_MAP) \
    ADD_MODULE( RTC            , oC_MACHINE_RTC_REGISTERS_MAPS_LIST        , oC_MACHINE_RTC_REGISTER_MAP) \
    ADD_MODULE( LPTIM          , oC_MACHINE_LPTIM_REGISTERS_MAPS_LIST      , oC_MACHINE_LPTIM_REGISTER_MAP) \

//==========================================================================================================================================
/** @hideinitializer
 * Definition of the default register.
 *
 *     Access:      RW
 *     Reset value: 0x0000.0000
 *     Page of description: -
 *
 *  To add new bit definition use ADD_BITS( BITS_NAME , NUMBER_OF_BITS )\
 */
//==========================================================================================================================================
#define oC_MACHINE_REGISTER_DEFAULT( ADD_BITS ) \
    ADD_BITS( Reserved , 31 )\

/** @} */
/* END OF SECTION */
#undef  _________________________________________LIST_OF_MODULES_SECTION____________________________________________________________________

#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_OC_MACHINE_DEFS_H_ */
