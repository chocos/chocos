/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions for Registers Maps module
 *
 * @file       oc_rmaps_defs.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_MACHINE_RMAPSDEFS_H_
#define SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_MACHINE_RMAPSDEFS_H_

#include <oc_1word.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief macro for getting register map definition name
 *
 * The macro returns the name of the register map definition from this file. Even if it looks stupid, it is needed for other macros.
 *
 * For example:
 * @code{.c}
   #if defined(oC_REGISTER_MAP_(FLASH))
       // oC_REGISTER_MAP_FLASH is defined
   #endif
   @endcode
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_(REGISTER_MAP_NAME)                 oC_1WORD_FROM_2(oC_REGISTER_MAP_ , REGISTER_MAP_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in FLASH register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_FLASH(MAKE_REGISTER)   \
    MAKE_REGISTER(FLASH_ACR          , 0x000UL , RW ) \
    MAKE_REGISTER(FLASH_KEYR         , 0x004UL , W ) \
    MAKE_REGISTER(FLASH_OPTKEYR      , 0x008UL , W ) \
    MAKE_REGISTER(FLASH_SR           , 0x00CUL , RW ) \
    MAKE_REGISTER(FLASH_CR           , 0x010UL , RW ) \
    MAKE_REGISTER(FLASH_OPTCR        , 0x014UL , RW ) \
    MAKE_REGISTER(FLASH_OPTCR1       , 0x018UL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in PWR register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_PWR(MAKE_REGISTER) \
    MAKE_REGISTER(PWR_CR1            , 0x000UL , RW ) \
    MAKE_REGISTER(PWR_CSR1           , 0x004UL , RW ) \
    MAKE_REGISTER(PWR_CR2            , 0x008UL , RW ) \
    MAKE_REGISTER(PWR_CSR2           , 0x00CUL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in RCC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_RCC(MAKE_REGISTER) \
    MAKE_REGISTER(RCC_CR             , 0x000UL , RW ) \
    MAKE_REGISTER(RCC_PLLCFGR        , 0x004UL , RW ) \
    MAKE_REGISTER(RCC_CFGR           , 0x008UL , RW ) \
    MAKE_REGISTER(RCC_CIR            , 0x00CUL , RW ) \
    MAKE_REGISTER(RCC_AHB1RSTR       , 0x010UL , RW ) \
    MAKE_REGISTER(RCC_AHB2RSTR       , 0x014UL , RW ) \
    MAKE_REGISTER(RCC_AHB3RSTR       , 0x018UL , RW ) \
    MAKE_REGISTER(RCC_APB1RSTR       , 0x020UL , RW ) \
    MAKE_REGISTER(RCC_APB2RSTR       , 0x024UL , RW ) \
    MAKE_REGISTER(RCC_AHB1ENR        , 0x030UL , RW ) \
    MAKE_REGISTER(RCC_AHB2ENR        , 0x034UL , RW ) \
    MAKE_REGISTER(RCC_AHB3ENR        , 0x038UL , RW ) \
    MAKE_REGISTER(RCC_APB1ENR        , 0x040UL , RW ) \
    MAKE_REGISTER(RCC_APB2ENR        , 0x044UL , RW ) \
    MAKE_REGISTER(RCC_AHB1LPENR      , 0x050UL , RW ) \
    MAKE_REGISTER(RCC_AHB2LPENR      , 0x054UL , RW ) \
    MAKE_REGISTER(RCC_AHB3LPENR      , 0x058UL , RW ) \
    MAKE_REGISTER(RCC_APB1LPENR      , 0x060UL , RW ) \
    MAKE_REGISTER(RCC_APB2LPENR      , 0x064UL , RW ) \
    MAKE_REGISTER(RCC_BDCR           , 0x070UL , RW ) \
    MAKE_REGISTER(RCC_CSR            , 0x074UL , RW ) \
    MAKE_REGISTER(RCC_SSCRG          , 0x080UL , RW ) \
    MAKE_REGISTER(RCC_PLLI2SCFGR     , 0x084UL , RW ) \
    MAKE_REGISTER(RCC_PLLSAICFGR     , 0x088UL , RW ) \
    MAKE_REGISTER(RCC_DCKCFGR1       , 0x08CUL , RW ) \
    MAKE_REGISTER(RCC_DCKCFGR2       , 0x090UL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in GPIO register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_GPIO(MAKE_REGISTER) \
    MAKE_REGISTER(GPIOA_MODER        , 0x000UL , RW ) \
    MAKE_REGISTER(GPIOB_MODER        , 0x000UL , RW ) \
    MAKE_REGISTER(GPIOx_MODER        , 0x000UL , RW ) \
    MAKE_REGISTER(GPIOx_OTYPER       , 0x004UL , RW ) \
    MAKE_REGISTER(GPIOA_OSPEEDR      , 0x008UL , RW ) \
    MAKE_REGISTER(GPIOB_OSPEEDR      , 0x008UL , RW ) \
    MAKE_REGISTER(GPIOx_OSPEEDR      , 0x008UL , RW ) \
    MAKE_REGISTER(GPIOx_PUPDR        , 0x00CUL , RW ) \
    MAKE_REGISTER(GPIOx_IDR          , 0x010UL , RW ) \
    MAKE_REGISTER(GPIOx_ODR          , 0x014UL , RW ) \
    MAKE_REGISTER(GPIOx_BSRR         , 0x018UL , RW ) \
    MAKE_REGISTER(GPIOx_LCKR         , 0x01CUL , RW ) \
    MAKE_REGISTER(GPIOx_AFRL         , 0x020UL , RW ) \
    MAKE_REGISTER(GPIOx_AFRH         , 0x024UL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in SYSCFG register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_SYSCFG(MAKE_REGISTER) \
    MAKE_REGISTER(SYSCFG_MEMRMP      , 0x000UL , RW ) \
    MAKE_REGISTER(SYSCFG_PMC         , 0x004UL , RW ) \
    MAKE_REGISTER(SYSCFG_EXTICR1     , 0x008UL , RW ) \
    MAKE_REGISTER(SYSCFG_EXTICR2     , 0x00CUL , RW ) \
    MAKE_REGISTER(SYSCFG_EXTICR3     , 0x010UL , RW ) \
    MAKE_REGISTER(SYSCFG_EXTICR4     , 0x014UL , RW ) \
    MAKE_REGISTER(SYSCFG_CMPCR       , 0x020UL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in DMA register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_DMA(MAKE_REGISTER) \
    MAKE_REGISTER(DMA_LISR           , 0x000UL , RW ) \
    MAKE_REGISTER(DMA_HISR           , 0x004UL , RW ) \
    MAKE_REGISTER(DMA_LIFCR          , 0x008UL , RW ) \
    MAKE_REGISTER(DMA_HIFCR          , 0x00CUL , RW ) \
    MAKE_REGISTER(DMA_S0CR           , 0x010UL , RW ) \
    MAKE_REGISTER(DMA_S0NDTR         , 0x014UL , RW ) \
    MAKE_REGISTER(DMA_S0PAR          , 0x018UL , RW ) \
    MAKE_REGISTER(DMA_S0M0AR         , 0x01CUL , RW ) \
    MAKE_REGISTER(DMA_S0M1AR         , 0x020UL , RW ) \
    MAKE_REGISTER(DMA_S0FCR          , 0x024UL , RW ) \
    MAKE_REGISTER(DMA_S1CR           , 0x028UL , RW ) \
    MAKE_REGISTER(DMA_S1NDTR         , 0x02CUL , RW ) \
    MAKE_REGISTER(DMA_S1PAR          , 0x030UL , RW ) \
    MAKE_REGISTER(DMA_S1M0AR         , 0x034UL , RW ) \
    MAKE_REGISTER(DMA_S1M1AR         , 0x038UL , RW ) \
    MAKE_REGISTER(DMA_S1FCR          , 0x03CUL , RW ) \
    MAKE_REGISTER(DMA_S2CR           , 0x040UL , RW ) \
    MAKE_REGISTER(DMA_S2NDTR         , 0x044UL , RW ) \
    MAKE_REGISTER(DMA_S2PAR          , 0x048UL , RW ) \
    MAKE_REGISTER(DMA_S2M0AR         , 0x04CUL , RW ) \
    MAKE_REGISTER(DMA_S2M1AR         , 0x050UL , RW ) \
    MAKE_REGISTER(DMA_S2FCR          , 0x054UL , RW ) \
    MAKE_REGISTER(DMA_S3CR           , 0x058UL , RW ) \
    MAKE_REGISTER(DMA_S3NDTR         , 0x05CUL , RW ) \
    MAKE_REGISTER(DMA_S3PAR          , 0x060UL , RW ) \
    MAKE_REGISTER(DMA_S3M0AR         , 0x064UL , RW ) \
    MAKE_REGISTER(DMA_S3M1AR         , 0x068UL , RW ) \
    MAKE_REGISTER(DMA_S3FCR          , 0x06CUL , RW ) \
    MAKE_REGISTER(DMA_S4CR           , 0x070UL , RW ) \
    MAKE_REGISTER(DMA_S4NDTR         , 0x074UL , RW ) \
    MAKE_REGISTER(DMA_S4PAR          , 0x078UL , RW ) \
    MAKE_REGISTER(DMA_S4M0AR         , 0x07CUL , RW ) \
    MAKE_REGISTER(DMA_S4M1AR         , 0x080UL , RW ) \
    MAKE_REGISTER(DMA_S4FCR          , 0x084UL , RW ) \
    MAKE_REGISTER(DMA_S5CR           , 0x088UL , RW ) \
    MAKE_REGISTER(DMA_S5NDTR         , 0x08CUL , RW ) \
    MAKE_REGISTER(DMA_S5PAR          , 0x090UL , RW ) \
    MAKE_REGISTER(DMA_S5M0AR         , 0x094UL , RW ) \
    MAKE_REGISTER(DMA_S5M1AR         , 0x098UL , RW ) \
    MAKE_REGISTER(DMA_S5FCR          , 0x09CUL , RW ) \
    MAKE_REGISTER(DMA_S6CR           , 0x0A0UL , RW ) \
    MAKE_REGISTER(DMA_S6NDTR         , 0x0A4UL , RW ) \
    MAKE_REGISTER(DMA_S6PAR          , 0x0A8UL , RW ) \
    MAKE_REGISTER(DMA_S6M0AR         , 0x0ACUL , RW ) \
    MAKE_REGISTER(DMA_S6M1AR         , 0x0B0UL , RW ) \
    MAKE_REGISTER(DMA_S6FCR          , 0x0B4UL , RW ) \
    MAKE_REGISTER(DMA_S7CR           , 0x0B8UL , RW ) \
    MAKE_REGISTER(DMA_S7NDTR         , 0x0BCUL , RW ) \
    MAKE_REGISTER(DMA_S7PAR          , 0x0C0UL , RW ) \
    MAKE_REGISTER(DMA_S7M0AR         , 0x0C4UL , RW ) \
    MAKE_REGISTER(DMA_S7M1AR         , 0x0C8UL , RW ) \
    MAKE_REGISTER(DMA_S7FCR          , 0x0CCUL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in DMA2D register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_DMA2D(MAKE_REGISTER) \
    MAKE_REGISTER(DMA2D_CR           , 0x000UL , RW ) \
    MAKE_REGISTER(DMA2D_ISR          , 0x0004L , RW ) \
    MAKE_REGISTER(DMA2D_IFCR         , 0x0008L , RW ) \
    MAKE_REGISTER(DMA2D_FGMAR        , 0x000CL , RW ) \
    MAKE_REGISTER(DMA2D_FGOR         , 0x0010L , RW ) \
    MAKE_REGISTER(DMA2D_BGOR         , 0x0018L , RW ) \
    MAKE_REGISTER(DMA2D_FGPFCCR      , 0x001CL , RW ) \
    MAKE_REGISTER(DMA2D_FGCOLR       , 0x0020L , RW ) \
    MAKE_REGISTER(DMA2D_BGPFCCR      , 0x0024L , RW ) \
    MAKE_REGISTER(DMA2D_BGCOLR       , 0x0028L , RW ) \
    MAKE_REGISTER(DMA2D_FGCMAR       , 0x002CL , RW ) \
    MAKE_REGISTER(DMA2D_BGCMAR       , 0x0030L , RW ) \
    MAKE_REGISTER(DMA2D_OPFCCR       , 0x0034L , RW ) \
    MAKE_REGISTER(DMA2D_OCOLR        , 0x0038L , RW ) \
    MAKE_REGISTER(DMA2D_OMAR         , 0x003CL , RW ) \
    MAKE_REGISTER(DMA2D_OOR          , 0x0040L , RW ) \
    MAKE_REGISTER(DMA2D_NLR          , 0x0044L , RW ) \
    MAKE_REGISTER(DMA2D_LWR          , 0x0048L , RW ) \
    MAKE_REGISTER(DMA2D_AMTCR        , 0x004CL , RW ) \
    MAKE_REGISTER(DMA2D_FGCLUT       , 0x0400L , RW ) \
    MAKE_REGISTER(DMA2D_BGCLUT       , 0x0800L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in EXTI register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_EXTI(MAKE_REGISTER) \
    MAKE_REGISTER(EXTI_IMR           , 0x0000L , RW ) \
    MAKE_REGISTER(EXTI_EMR           , 0x0004L , RW ) \
    MAKE_REGISTER(EXTI_RTSR          , 0x0008L , RW ) \
    MAKE_REGISTER(EXTI_FTSR          , 0x000CL , RW ) \
    MAKE_REGISTER(EXTI_SWIER         , 0x0010L , RW ) \
    MAKE_REGISTER(EXTI_PR            , 0x0014L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in CRC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_CRC(MAKE_REGISTER) \
    MAKE_REGISTER(CRC_DR             , 0x0000L , RW ) \
    MAKE_REGISTER(CRC_IDR            , 0x0004L , RW ) \
    MAKE_REGISTER(CRC_CR             , 0x0008L , RW ) \
    MAKE_REGISTER(CRC_INIT           , 0x0010L , RW ) \
    MAKE_REGISTER(CRC_POL            , 0x0014L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in FMC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_FMC(MAKE_REGISTER) \
    MAKE_REGISTER(FMC_BCR1           , 0x0000L , RW ) \
    MAKE_REGISTER(FMC_BCR2           , 0x0008L , RW ) \
    MAKE_REGISTER(FMC_BCR3           , 0x0010L , RW ) \
    MAKE_REGISTER(FMC_BCR4           , 0x0018L , RW ) \
    MAKE_REGISTER(FMC_BTR1           , 0x0004L , RW ) \
    MAKE_REGISTER(FMC_BTR2           , 0x000CL , RW ) \
    MAKE_REGISTER(FMC_BTR3           , 0x0014L , RW ) \
    MAKE_REGISTER(FMC_BTR4           , 0x001CL , RW ) \
    MAKE_REGISTER(FMC_BWTR1          , 0x0104L , RW ) \
    MAKE_REGISTER(FMC_BWTR2          , 0x010CL , RW ) \
    MAKE_REGISTER(FMC_BWTR3          , 0x0114L , RW ) \
    MAKE_REGISTER(FMC_BWTR4          , 0x011CL , RW ) \
    MAKE_REGISTER(FMC_PCR            , 0x0080L , RW ) \
    MAKE_REGISTER(FMC_SR             , 0x0084L , RW ) \
    MAKE_REGISTER(FMC_PMEM           , 0x0088L , RW ) \
    MAKE_REGISTER(FMC_PATT           , 0x008CL , RW ) \
    MAKE_REGISTER(FMC_ECCR           , 0x0094L , RW ) \
    MAKE_REGISTER(FMC_SDCR1          , 0x0140L , RW ) \
    MAKE_REGISTER(FMC_SDCR2          , 0x0144L , RW ) \
    MAKE_REGISTER(FMC_SDTR1          , 0x0148L , RW ) \
    MAKE_REGISTER(FMC_SDTR2          , 0x014CL , RW ) \
    MAKE_REGISTER(FMC_SDCMR          , 0x0150L , RW ) \
    MAKE_REGISTER(FMC_SDRTR          , 0x0154L , RW ) \
    MAKE_REGISTER(FMC_SDSR           , 0x0158L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in QUADSPI register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_QUADSPI(MAKE_REGISTER) \
    MAKE_REGISTER(QUADSPI_CR         , 0x0000L , RW ) \
    MAKE_REGISTER(QUADSPI_DCR        , 0x0004L , RW ) \
    MAKE_REGISTER(QUADSPI_SR         , 0x0008L , RW ) \
    MAKE_REGISTER(QUADSPI_FCR        , 0x000CL , RW ) \
    MAKE_REGISTER(QUADSPI_DLR        , 0x0010L , RW ) \
    MAKE_REGISTER(QUADSPI_CCR        , 0x0014L , RW ) \
    MAKE_REGISTER(QUADSPI_AR         , 0x0018L , RW ) \
    MAKE_REGISTER(QUADSPI_ABR        , 0x001CL , RW ) \
    MAKE_REGISTER(QUADSPI_DR         , 0x0020L , RW ) \
    MAKE_REGISTER(QUADSPI_PSMKR      , 0x0024L , RW ) \
    MAKE_REGISTER(QUADSPI_PSMAR      , 0x0028L , RW ) \
    MAKE_REGISTER(QUADSPI_PIR        , 0x002CL , RW ) \
    MAKE_REGISTER(QUADSPI_LPTR       , 0x0030L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in ADC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_ADC(MAKE_REGISTER) \
    MAKE_REGISTER(ADC_SR             , 0x0000L , RW ) \
    MAKE_REGISTER(ADC_CR1            , 0x0004L , RW ) \
    MAKE_REGISTER(ADC_CR2            , 0x0008L , RW ) \
    MAKE_REGISTER(ADC_SMPR1          , 0x000CL , RW ) \
    MAKE_REGISTER(ADC_SMPR2          , 0x0010L , RW ) \
    MAKE_REGISTER(ADC_JOFR1          , 0x0014L , RW ) \
    MAKE_REGISTER(ADC_JOFR2          , 0x0018L , RW ) \
    MAKE_REGISTER(ADC_JOFR3          , 0x001CL , RW ) \
    MAKE_REGISTER(ADC_JOFR4          , 0x0020L , RW ) \
    MAKE_REGISTER(ADC_HTR            , 0x0024L , RW ) \
    MAKE_REGISTER(ADC_LTR            , 0x0028L , RW ) \
    MAKE_REGISTER(ADC_SQR1           , 0x002CL , RW ) \
    MAKE_REGISTER(ADC_SQR2           , 0x0030L , RW ) \
    MAKE_REGISTER(ADC_SQR3           , 0x0034L , RW ) \
    MAKE_REGISTER(ADC_JSQR           , 0x0038L , RW ) \
    MAKE_REGISTER(ADC_JDR1           , 0x003CL , RW ) \
    MAKE_REGISTER(ADC_JDR2           , 0x0040L , RW ) \
    MAKE_REGISTER(ADC_JDR3           , 0x0044L , RW ) \
    MAKE_REGISTER(ADC_JDR4           , 0x0048L , RW ) \
    MAKE_REGISTER(ADC_DR             , 0x004CL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in ADCCOMMON register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_ADCCOMMON(MAKE_REGISTER) \
    MAKE_REGISTER(ADC_CSR            , 0x000CL , RW ) \
    MAKE_REGISTER(ADC_CCR            , 0x004CL , RW ) \
    MAKE_REGISTER(ADC_CDR            , 0x008CL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in DAC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_DAC(MAKE_REGISTER) \
    MAKE_REGISTER(DAC_CR             , 0x0000L , RW ) \
    MAKE_REGISTER(DAC_SWTRIGR        , 0x0004L , RW ) \
    MAKE_REGISTER(DAC_DHR12R1        , 0x0008L , RW ) \
    MAKE_REGISTER(DAC_DHR12L1        , 0x000CL , RW ) \
    MAKE_REGISTER(DAC_DHR8R1         , 0x0010L , RW ) \
    MAKE_REGISTER(DAC_DHR12R2        , 0x0014L , RW ) \
    MAKE_REGISTER(DAC_DHR12L2        , 0x0018L , RW ) \
    MAKE_REGISTER(DAC_DHR8R2         , 0x001CL , RW ) \
    MAKE_REGISTER(DAC_DHR12RD        , 0x0020L , RW ) \
    MAKE_REGISTER(DAC_DHR12LD        , 0x0024L , RW ) \
    MAKE_REGISTER(DAC_DHR8RD         , 0x0028L , RW ) \
    MAKE_REGISTER(DAC_DOR1           , 0x002CL , RW ) \
    MAKE_REGISTER(DAC_DOR2           , 0x0030L , RW ) \
    MAKE_REGISTER(DAC_SR             , 0x0034L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in DCMI register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_DCMI(MAKE_REGISTER) \
    MAKE_REGISTER(DCMI_CR            , 0x0000L , RW ) \
    MAKE_REGISTER(DCMI_SR            , 0x0004L , RW ) \
    MAKE_REGISTER(DCMI_RIS           , 0x0008L , RW ) \
    MAKE_REGISTER(DCMI_IER           , 0x000CL , RW ) \
    MAKE_REGISTER(DCMI_MIS           , 0x0010L , RW ) \
    MAKE_REGISTER(DCMI_ICR           , 0x0014L , RW ) \
    MAKE_REGISTER(DCMI_ESCR          , 0x0018L , RW ) \
    MAKE_REGISTER(DCMI_ESUR          , 0x001CL , RW ) \
    MAKE_REGISTER(DCMI_CWSTRT        , 0x0020L , RW ) \
    MAKE_REGISTER(DCMI_CWSIZE        , 0x0024L , RW ) \
    MAKE_REGISTER(DCMI_DR            , 0x0028L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in LTDC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_LTDC(MAKE_REGISTER) \
    MAKE_REGISTER(LTDC_SSCR          , 0x0008L , RW ) \
    MAKE_REGISTER(LTDC_BPCR          , 0x000CL , RW ) \
    MAKE_REGISTER(LTDC_AWCR          , 0x0010L , RW ) \
    MAKE_REGISTER(LTDC_TWCR          , 0x0014L , RW ) \
    MAKE_REGISTER(LTDC_GCR           , 0x0018L , RW ) \
    MAKE_REGISTER(LTDC_SRCR          , 0x0024L , RW ) \
    MAKE_REGISTER(LTDC_BCCR          , 0x002CL , RW ) \
    MAKE_REGISTER(LTDC_IER           , 0x0034L , RW ) \
    MAKE_REGISTER(LTDC_ISR           , 0x0038L , RW ) \
    MAKE_REGISTER(LTDC_ICR           , 0x003CL , RW ) \
    MAKE_REGISTER(LTDC_LIPCR         , 0x0040L , RW ) \
    MAKE_REGISTER(LTDC_CPSR          , 0x0044L , RW ) \
    MAKE_REGISTER(LTDC_CDSR          , 0x0048L , RW ) \
    MAKE_REGISTER(LTDC_L1CR          , 0x0084L , RW ) \
    MAKE_REGISTER(LTDC_L1WHPCR       , 0x0088L , RW ) \
    MAKE_REGISTER(LTDC_L1WVPCR       , 0x008CL , RW ) \
    MAKE_REGISTER(LTDC_L1CKCR        , 0x0090L , RW ) \
    MAKE_REGISTER(LTDC_L1PFCR        , 0x0094L , RW ) \
    MAKE_REGISTER(LTDC_L1CACR        , 0x0098L , RW ) \
    MAKE_REGISTER(LTDC_L1DCCR        , 0x009CL , RW ) \
    MAKE_REGISTER(LTDC_L1BFCR        , 0x00A0L , RW ) \
    MAKE_REGISTER(LTDC_L1CFBAR       , 0x00ACL , RW ) \
    MAKE_REGISTER(LTDC_L1CFBLR       , 0x00B0L , RW ) \
    MAKE_REGISTER(LTDC_L1CFBLNR      , 0x00B4L , RW ) \
    MAKE_REGISTER(LTDC_L1CLUTWR      , 0x00C4L , RW ) \
    MAKE_REGISTER(LTDC_L2CR          , 0x0104L , RW ) \
    MAKE_REGISTER(LTDC_L2WHPCR       , 0x0108L , RW ) \
    MAKE_REGISTER(LTDC_L2WVPCR       , 0x010CL , RW ) \
    MAKE_REGISTER(LTDC_L2CKCR        , 0x0110L , RW ) \
    MAKE_REGISTER(LTDC_L2PFCR        , 0x0114L , RW ) \
    MAKE_REGISTER(LTDC_L2CACR        , 0x0118L , RW ) \
    MAKE_REGISTER(LTDC_L2DCCR        , 0x011CL , RW ) \
    MAKE_REGISTER(LTDC_L2BFCR        , 0x0120L , RW ) \
    MAKE_REGISTER(LTDC_L2CFBAR       , 0x012CL , RW ) \
    MAKE_REGISTER(LTDC_L2CFBLR       , 0x0130L , RW ) \
    MAKE_REGISTER(LTDC_L2CFBLNR      , 0x0134L , RW ) \
    MAKE_REGISTER(LTDC_L2CLUTWR      , 0x0144L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in RNG register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_RNG(MAKE_REGISTER) \
    MAKE_REGISTER(RNG_CR             , 0x0000L , RW ) \
    MAKE_REGISTER(RNG_SR             , 0x0004L , RW ) \
    MAKE_REGISTER(RNG_DR             , 0x0008L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in CRYP register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_CRYP(MAKE_REGISTER) \
    MAKE_REGISTER(CRYP_CR            , 0x0000L , RW ) \
    MAKE_REGISTER(CRYP_SR            , 0x0004L , RW ) \
    MAKE_REGISTER(CRYP_DIN           , 0x0008L , RW ) \
    MAKE_REGISTER(CRYP_DOUT          , 0x000CL , RW ) \
    MAKE_REGISTER(CRYP_DMACR         , 0x0010L , RW ) \
    MAKE_REGISTER(CRYP_IMSCR         , 0x0014L , RW ) \
    MAKE_REGISTER(CRYP_RISR          , 0x0018L , RW ) \
    MAKE_REGISTER(CRYP_MISR          , 0x001CL , RW ) \
    MAKE_REGISTER(CRYP_K0LR          , 0x0020L , RW ) \
    MAKE_REGISTER(CRYP_K0RR          , 0x0024L , RW ) \
    MAKE_REGISTER(CRYP_K1LR          , 0x0028L , RW ) \
    MAKE_REGISTER(CRYP_K1RR          , 0x002CL , RW ) \
    MAKE_REGISTER(CRYP_K2LR          , 0x0030L , RW ) \
    MAKE_REGISTER(CRYP_K2RR          , 0x0034L , RW ) \
    MAKE_REGISTER(CRYP_K3LR          , 0x0038L , RW ) \
    MAKE_REGISTER(CRYP_K3RR          , 0x003CL , RW ) \
    MAKE_REGISTER(CRYP_IV0LR         , 0x0040L , RW ) \
    MAKE_REGISTER(CRYP_IV0RR         , 0x0044L , RW ) \
    MAKE_REGISTER(CRYP_IV1LR         , 0x0048L , RW ) \
    MAKE_REGISTER(CRYP_IV1RR         , 0x004CL , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCMR     , 0x0050L , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCM1R    , 0x0054L , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCM2R    , 0x0058L , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCM3R    , 0x005CL , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCM4R    , 0x0060L , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCM5R    , 0x0064L , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCM6R    , 0x0068L , RW ) \
    MAKE_REGISTER(CRYP_CSGCMCCM7R    , 0x006CL , RW ) \
    MAKE_REGISTER(CRYP_CSGCM0R       , 0x0070L , RW ) \
    MAKE_REGISTER(CRYP_CSGCM1R       , 0x0074L , RW ) \
    MAKE_REGISTER(CRYP_CSGCM2R       , 0x0078L , RW ) \
    MAKE_REGISTER(CRYP_CSGCM3R       , 0x007CL , RW ) \
    MAKE_REGISTER(CRYP_CSGCM4R       , 0x0080L , RW ) \
    MAKE_REGISTER(CRYP_CSGCM5R       , 0x0084L , RW ) \
    MAKE_REGISTER(CRYP_CSGCM6R       , 0x0088L , RW ) \
    MAKE_REGISTER(CRYP_CSGCM7R       , 0x008CL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in HASH register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_HASH(MAKE_REGISTER) \
    MAKE_REGISTER(HASH_CR            , 0x0000L , RW ) \
    MAKE_REGISTER(HASH_DIN           , 0x0004L , RW ) \
    MAKE_REGISTER(HASH_STR           , 0x0008L , RW ) \
    MAKE_REGISTER(HASH_HR0           , 0x000CL , RW ) \
    MAKE_REGISTER(HASH_HR1           , 0x0010L , RW ) \
    MAKE_REGISTER(HASH_HR2           , 0x0014L , RW ) \
    MAKE_REGISTER(HASH_HR3           , 0x0018L , RW ) \
    MAKE_REGISTER(HASH_HR4           , 0x001CL , RW ) \
    MAKE_REGISTER(HASH_IMR           , 0x0020L , RW ) \
    MAKE_REGISTER(HASH_SR            , 0x0024L , RW ) \
    MAKE_REGISTER(HASH_CSR0          , 0x00F8L , RW ) \
    MAKE_REGISTER(HASH_CSR1          , 0x00FCL , RW ) \
    MAKE_REGISTER(HASH_CSR2          , 0x0100L , RW ) \
    MAKE_REGISTER(HASH_CSR3          , 0x0104L , RW ) \
    MAKE_REGISTER(HASH_CSR4          , 0x0108L , RW ) \
    MAKE_REGISTER(HASH_CSR5          , 0x010CL , RW ) \
    MAKE_REGISTER(HASH_CSR6          , 0x0110L , RW ) \
    MAKE_REGISTER(HASH_CSR7          , 0x0114L , RW ) \
    MAKE_REGISTER(HASH_CSR8          , 0x0118L , RW ) \
    MAKE_REGISTER(HASH_CSR9          , 0x011CL , RW ) \
    MAKE_REGISTER(HASH_CSR10         , 0x0120L , RW ) \
    MAKE_REGISTER(HASH_CSR11         , 0x0124L , RW ) \
    MAKE_REGISTER(HASH_CSR12         , 0x0128L , RW ) \
    MAKE_REGISTER(HASH_CSR13         , 0x012CL , RW ) \
    MAKE_REGISTER(HASH_CSR14         , 0x0130L , RW ) \
    MAKE_REGISTER(HASH_CSR15         , 0x0134L , RW ) \
    MAKE_REGISTER(HASH_CSR16         , 0x0138L , RW ) \
    MAKE_REGISTER(HASH_CSR17         , 0x013CL , RW ) \
    MAKE_REGISTER(HASH_CSR18         , 0x0140L , RW ) \
    MAKE_REGISTER(HASH_CSR19         , 0x0144L , RW ) \
    MAKE_REGISTER(HASH_CSR20         , 0x0148L , RW ) \
    MAKE_REGISTER(HASH_CSR21         , 0x014CL , RW ) \
    MAKE_REGISTER(HASH_CSR22         , 0x0150L , RW ) \
    MAKE_REGISTER(HASH_CSR23         , 0x0154L , RW ) \
    MAKE_REGISTER(HASH_CSR24         , 0x0158L , RW ) \
    MAKE_REGISTER(HASH_CSR25         , 0x015CL , RW ) \
    MAKE_REGISTER(HASH_CSR26         , 0x0160L , RW ) \
    MAKE_REGISTER(HASH_CSR27         , 0x0164L , RW ) \
    MAKE_REGISTER(HASH_CSR28         , 0x0168L , RW ) \
    MAKE_REGISTER(HASH_CSR29         , 0x016CL , RW ) \
    MAKE_REGISTER(HASH_CSR30         , 0x0170L , RW ) \
    MAKE_REGISTER(HASH_CSR31         , 0x0174L , RW ) \
    MAKE_REGISTER(HASH_CSR32         , 0x0178L , RW ) \
    MAKE_REGISTER(HASH_CSR33         , 0x017CL , RW ) \
    MAKE_REGISTER(HASH_CSR34         , 0x0180L , RW ) \
    MAKE_REGISTER(HASH_CSR35         , 0x0184L , RW ) \
    MAKE_REGISTER(HASH_CSR36         , 0x0188L , RW ) \
    MAKE_REGISTER(HASH_CSR37         , 0x018CL , RW ) \
    MAKE_REGISTER(HASH_CSR38         , 0x0190L , RW ) \
    MAKE_REGISTER(HASH_CSR39         , 0x0194L , RW ) \
    MAKE_REGISTER(HASH_CSR40         , 0x0198L , RW ) \
    MAKE_REGISTER(HASH_CSR41         , 0x019CL , RW ) \
    MAKE_REGISTER(HASH_CSR42         , 0x01A0L , RW ) \
    MAKE_REGISTER(HASH_CSR43         , 0x01A4L , RW ) \
    MAKE_REGISTER(HASH_CSR44         , 0x01A8L , RW ) \
    MAKE_REGISTER(HASH_CSR45         , 0x01ACL , RW ) \
    MAKE_REGISTER(HASH_CSR46         , 0x01B0L , RW ) \
    MAKE_REGISTER(HASH_CSR47         , 0x01B4L , RW ) \
    MAKE_REGISTER(HASH_CSR48         , 0x01B8L , RW ) \
    MAKE_REGISTER(HASH_CSR49         , 0x01BCL , RW ) \
    MAKE_REGISTER(HASH_CSR50         , 0x01C0L , RW ) \
    MAKE_REGISTER(HASH_CSR51         , 0x01C4L , RW ) \
    MAKE_REGISTER(HASH_CSR52         , 0x01C8L , RW ) \
    MAKE_REGISTER(HASH_CSR53         , 0x01CCL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in TIMx register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_TIMx(MAKE_REGISTER) \
    MAKE_REGISTER(TIMx_CR1           , 0x0000L , RW ) \
    MAKE_REGISTER(TIMx_CR2           , 0x0004L , RW ) \
    MAKE_REGISTER(TIMx_SMCR          , 0x0008L , RW ) \
    MAKE_REGISTER(TIMx_DIER          , 0x000CL , RW ) \
    MAKE_REGISTER(TIMx_SR            , 0x0010L , RW ) \
    MAKE_REGISTER(TIMx_EGR           , 0x0014L , RW ) \
    MAKE_REGISTER(TIMx_CCMR1         , 0x0018L , RW ) \
    MAKE_REGISTER(TIMx_CCMR2         , 0x001CL , RW ) \
    MAKE_REGISTER(TIMx_CCER          , 0x0020L , RW ) \
    MAKE_REGISTER(TIMx_CNT           , 0x0024L , RW ) \
    MAKE_REGISTER(TIMx_PSC           , 0x0028L , RW ) \
    MAKE_REGISTER(TIMx_ARR           , 0x002CL , RW ) \
    MAKE_REGISTER(TIMx_RCR           , 0x0030L , RW ) \
    MAKE_REGISTER(TIMx_CCR1          , 0x0034L , RW ) \
    MAKE_REGISTER(TIMx_CCR2          , 0x0038L , RW ) \
    MAKE_REGISTER(TIMx_CCR3          , 0x003CL , RW ) \
    MAKE_REGISTER(TIMx_CCR4          , 0x0040L , RW ) \
    MAKE_REGISTER(TIMx_BDTR          , 0x0044L , RW ) \
    MAKE_REGISTER(TIMx_DCR           , 0x0048L , RW ) \
    MAKE_REGISTER(TIMx_DMAR          , 0x004CL , RW ) \
    MAKE_REGISTER(TIMx_OR            , 0x0050L , RW ) \
    MAKE_REGISTER(TIMx_CCMR3         , 0x0054L , RW ) \
    MAKE_REGISTER(TIMx_CCR5          , 0x0058L , RW ) \
    MAKE_REGISTER(TIMx_CCR6          , 0x005CL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in LPTIMx register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_LPTIMx(MAKE_REGISTER) \
    MAKE_REGISTER(LPTIMx_ISR         , 0x0000L , RW ) \
    MAKE_REGISTER(LPTIMx_ICR         , 0x0004L , RW ) \
    MAKE_REGISTER(LPTIMx_IER         , 0x0008L , RW ) \
    MAKE_REGISTER(LPTIMx_CFGR        , 0x000CL , RW ) \
    MAKE_REGISTER(LPTIMx_CR          , 0x0010L , RW ) \
    MAKE_REGISTER(LPTIMx_CMP         , 0x0014L , RW ) \
    MAKE_REGISTER(LPTIMx_ARR         , 0x0018L , RW ) \
    MAKE_REGISTER(LPTIMx_CNT         , 0x001CL , RW ) \
    MAKE_REGISTER(LPTIMx_OR          , 0x0020L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in IWDG register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_IWDG(MAKE_REGISTER) \
    MAKE_REGISTER(IWDG_KR            , 0x0000L , RW ) \
    MAKE_REGISTER(IWDG_PR            , 0x0004L , RW ) \
    MAKE_REGISTER(IWDG_RLR           , 0x0008L , RW ) \
    MAKE_REGISTER(IWDG_SR            , 0x000CL , RW ) \
    MAKE_REGISTER(IWDG_WINR          , 0x0010L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in WWDG register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_WWDG(MAKE_REGISTER) \
    MAKE_REGISTER(WWDG_CR            , 0x0000L , RW ) \
    MAKE_REGISTER(WWDG_CFR           , 0x0004L , RW ) \
    MAKE_REGISTER(WWDG_SR            , 0x0008L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in RTC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_RTC(MAKE_REGISTER) \
    MAKE_REGISTER(RTC_TR             , 0x0000L , RW ) \
    MAKE_REGISTER(RTC_DR             , 0x0004L , RW ) \
    MAKE_REGISTER(RTC_CR             , 0x0008L , RW ) \
    MAKE_REGISTER(RTC_ISR            , 0x000CL , RW ) \
    MAKE_REGISTER(RTC_PRER           , 0x0010L , RW ) \
    MAKE_REGISTER(RTC_WUTR           , 0x0014L , RW ) \
    MAKE_REGISTER(RTC_ALRMAR         , 0x001CL , RW ) \
    MAKE_REGISTER(RTC_ALRMBR         , 0x0020L , RW ) \
    MAKE_REGISTER(RTC_WPR            , 0x0024L , RW ) \
    MAKE_REGISTER(RTC_SSR            , 0x0028L , RW ) \
    MAKE_REGISTER(RTC_SHIFTR         , 0x002CL , RW ) \
    MAKE_REGISTER(RTC_TSTR           , 0x0030L , RW ) \
    MAKE_REGISTER(RTC_TSDR           , 0x0034L , RW ) \
    MAKE_REGISTER(RTC_TSSSR          , 0x0038L , RW ) \
    MAKE_REGISTER(RTC_CALR           , 0x003CL , RW ) \
    MAKE_REGISTER(RTC_TAMPCR         , 0x0040L , RW ) \
    MAKE_REGISTER(RTC_ALRMASSR       , 0x0044L , RW ) \
    MAKE_REGISTER(RTC_ALRMBSSR       , 0x0048L , RW ) \
    MAKE_REGISTER(RTC_OR             , 0x004CL , RW ) \
    MAKE_REGISTER(RTC_BKP0R          , 0x0050L , RW ) \
    MAKE_REGISTER(RTC_BKP1R          , 0x0054L , RW ) \
    MAKE_REGISTER(RTC_BKP2R          , 0x0058L , RW ) \
    MAKE_REGISTER(RTC_BKP3R          , 0x005CL , RW ) \
    MAKE_REGISTER(RTC_BKP4R          , 0x0060L , RW ) \
    MAKE_REGISTER(RTC_BKP5R          , 0x0064L , RW ) \
    MAKE_REGISTER(RTC_BKP6R          , 0x0068L , RW ) \
    MAKE_REGISTER(RTC_BKP7R          , 0x006CL , RW ) \
    MAKE_REGISTER(RTC_BKP8R          , 0x0070L , RW ) \
    MAKE_REGISTER(RTC_BKP9R          , 0x0074L , RW ) \
    MAKE_REGISTER(RTC_BKP10R         , 0x0078L , RW ) \
    MAKE_REGISTER(RTC_BKP11R         , 0x007CL , RW ) \
    MAKE_REGISTER(RTC_BKP12R         , 0x0080L , RW ) \
    MAKE_REGISTER(RTC_BKP13R         , 0x0084L , RW ) \
    MAKE_REGISTER(RTC_BKP14R         , 0x0088L , RW ) \
    MAKE_REGISTER(RTC_BKP15R         , 0x008CL , RW ) \
    MAKE_REGISTER(RTC_BKP16R         , 0x0090L , RW ) \
    MAKE_REGISTER(RTC_BKP17R         , 0x0094L , RW ) \
    MAKE_REGISTER(RTC_BKP18R         , 0x0098L , RW ) \
    MAKE_REGISTER(RTC_BKP19R         , 0x009CL , RW ) \
    MAKE_REGISTER(RTC_BKP20R         , 0x00A0L , RW ) \
    MAKE_REGISTER(RTC_BKP21R         , 0x00A4L , RW ) \
    MAKE_REGISTER(RTC_BKP22R         , 0x00A8L , RW ) \
    MAKE_REGISTER(RTC_BKP23R         , 0x00ACL , RW ) \
    MAKE_REGISTER(RTC_BKP24R         , 0x00B0L , RW ) \
    MAKE_REGISTER(RTC_BKP25R         , 0x00B4L , RW ) \
    MAKE_REGISTER(RTC_BKP26R         , 0x00B8L , RW ) \
    MAKE_REGISTER(RTC_BKP27R         , 0x00BCL , RW ) \
    MAKE_REGISTER(RTC_BKP28R         , 0x00C0L , RW ) \
    MAKE_REGISTER(RTC_BKP29R         , 0x00C4L , RW ) \
    MAKE_REGISTER(RTC_BKP30R         , 0x00C8L , RW ) \
    MAKE_REGISTER(RTC_BKP31R         , 0x00CCL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in I2C register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_I2C(MAKE_REGISTER) \
    MAKE_REGISTER(I2C_CR1            , 0x0000L , RW ) \
    MAKE_REGISTER(I2C_CR2            , 0x0004L , RW ) \
    MAKE_REGISTER(I2C_OAR1           , 0x0008L , RW ) \
    MAKE_REGISTER(I2C_OAR2           , 0x000CL , RW ) \
    MAKE_REGISTER(I2C_TIMINGR        , 0x0010L , RW ) \
    MAKE_REGISTER(I2C_TIMEOUTR       , 0x0014L , RW ) \
    MAKE_REGISTER(I2C_ISR            , 0x0018L , RW ) \
    MAKE_REGISTER(I2C_ICR            , 0x001CL , RW ) \
    MAKE_REGISTER(I2C_PECR           , 0x0020L , RW ) \
    MAKE_REGISTER(I2C_RXDR           , 0x0024L , RW ) \
    MAKE_REGISTER(I2C_TXDR           , 0x0028L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in USART register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_USART(MAKE_REGISTER) \
    MAKE_REGISTER(USARTx_CR1         , 0x0000L , RW ) \
    MAKE_REGISTER(USARTx_CR2         , 0x0004L , RW ) \
    MAKE_REGISTER(USARTx_CR3         , 0x0008L , RW ) \
    MAKE_REGISTER(USARTx_BRR         , 0x000CL , RW ) \
    MAKE_REGISTER(USARTx_GTPR        , 0x0010L , RW ) \
    MAKE_REGISTER(USARTx_RTOR        , 0x0014L , RW ) \
    MAKE_REGISTER(USARTx_RQR         , 0x0018L , RW ) \
    MAKE_REGISTER(USARTx_ISR         , 0x001CL , RW ) \
    MAKE_REGISTER(USARTx_ICR         , 0x0020L , RW ) \
    MAKE_REGISTER(USARTx_RDR         , 0x0024L , RW ) \
    MAKE_REGISTER(USARTx_TDR         , 0x0028L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in SPI register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_SPI(MAKE_REGISTER) \
    MAKE_REGISTER(SPIx_CR1           , 0x0000L , RW ) \
    MAKE_REGISTER(SPIx_CR2           , 0x0004L , RW ) \
    MAKE_REGISTER(SPIx_SR            , 0x0008L , RW ) \
    MAKE_REGISTER(SPIx_DR            , 0x000CL , RW ) \
    MAKE_REGISTER(SPIx_CRCPR         , 0x0010L , RW ) \
    MAKE_REGISTER(SPIx_RXCRCR        , 0x0014L , RW ) \
    MAKE_REGISTER(SPIx_TXCRCR        , 0x0018L , RW ) \
    MAKE_REGISTER(SPIx_I2SCFGR       , 0x001CL , RW ) \
    MAKE_REGISTER(SPIx_I2SPR         , 0x0020L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in SAI register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_SAI(MAKE_REGISTER) \
    MAKE_REGISTER(SAI_GCR            , 0x0000L , RW ) \
    MAKE_REGISTER(SAI_xCR1           , 0x0004L , RW ) \
    MAKE_REGISTER(SAI_xCR2           , 0x0008L , RW ) \
    MAKE_REGISTER(SAI_xFRCR          , 0x000CL , RW ) \
    MAKE_REGISTER(SAI_xSLOTR         , 0x0010L , RW ) \
    MAKE_REGISTER(SAI_xIM            , 0x0014L , RW ) \
    MAKE_REGISTER(SAI_xSR            , 0x0018L , RW ) \
    MAKE_REGISTER(SAI_xCLRFR         , 0x001CL , RW ) \
    MAKE_REGISTER(SAI_xDR            , 0x0020L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in SPDIFRX register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_SPDIFRX(MAKE_REGISTER) \
    MAKE_REGISTER(SPDIFRX_CR         , 0x0000L , RW ) \
    MAKE_REGISTER(SPDIFRX_IMR        , 0x0004L , RW ) \
    MAKE_REGISTER(SPDIFRX_SR         , 0x0008L , RW ) \
    MAKE_REGISTER(SPDIFRX_IFCR       , 0x000CL , RW ) \
    MAKE_REGISTER(SPDIFRX_DR         , 0x0010L , RW ) \
    MAKE_REGISTER(SPDIFRX_CSR        , 0x0014L , RW ) \
    MAKE_REGISTER(SPDIFRX_DIR        , 0x0018L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in SDMMC register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_SDMMC(MAKE_REGISTER) \
    MAKE_REGISTER(SDMMC_POWER        , 0x0000L , RW ) \
    MAKE_REGISTER(SDMMC_CLKCR        , 0x0004L , RW ) \
    MAKE_REGISTER(SDMMC_ARG          , 0x0008L , RW ) \
    MAKE_REGISTER(SDMMC_CMD          , 0x000CL , RW ) \
    MAKE_REGISTER(SDMMC_RESPCMD      , 0x0010L , RW ) \
    MAKE_REGISTER(SDMMC_RESP1        , 0x0014L , RW ) \
    MAKE_REGISTER(SDMMC_RESP2        , 0x0018L , RW ) \
    MAKE_REGISTER(SDMMC_RESP3        , 0x001CL , RW ) \
    MAKE_REGISTER(SDMMC_RESP4        , 0x0020L , RW ) \
    MAKE_REGISTER(SDMMC_DTIMER       , 0x0024L , RW ) \
    MAKE_REGISTER(SDMMC_DLEN         , 0x0028L , RW ) \
    MAKE_REGISTER(SDMMC_DCTRL        , 0x002CL , RW ) \
    MAKE_REGISTER(SDMMC_DCOUNT       , 0x0030L , RW ) \
    MAKE_REGISTER(SDMMC_STA          , 0x0034L , RW ) \
    MAKE_REGISTER(SDMMC_ICR          , 0x0038L , RW ) \
    MAKE_REGISTER(SDMMC_MASK         , 0x003CL , RW ) \
    MAKE_REGISTER(SDMMC_FIFOCNT      , 0x0048L , RW ) \
    MAKE_REGISTER(SDMMC_FIFO         , 0x0080L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in CAN register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_CAN(MAKE_REGISTER) \
    MAKE_REGISTER(CAN_MCR            , 0x0000L , RW ) \
    MAKE_REGISTER(CAN_MSR            , 0x0004L , RW ) \
    MAKE_REGISTER(CAN_TSR            , 0x0008L , RW ) \
    MAKE_REGISTER(CAN_RF0R           , 0x000CL , RW ) \
    MAKE_REGISTER(CAN_RF1R           , 0x0010L , RW ) \
    MAKE_REGISTER(CAN_IER            , 0x0014L , RW ) \
    MAKE_REGISTER(CAN_ESR            , 0x0018L , RW ) \
    MAKE_REGISTER(CAN_BTR            , 0x001CL , RW ) \
    MAKE_REGISTER(CAN_TI0R           , 0x0180L , RW ) \
    MAKE_REGISTER(CAN_TDT0R          , 0x0184L , RW ) \
    MAKE_REGISTER(CAN_TDL0R          , 0x0188L , RW ) \
    MAKE_REGISTER(CAN_TDH0R          , 0x018CL , RW ) \
    MAKE_REGISTER(CAN_TI1R           , 0x0190L , RW ) \
    MAKE_REGISTER(CAN_TDT1R          , 0x0194L , RW ) \
    MAKE_REGISTER(CAN_TDL1R          , 0x0198L , RW ) \
    MAKE_REGISTER(CAN_TDH1R          , 0x019CL , RW ) \
    MAKE_REGISTER(CAN_TI2R           , 0x01A0L , RW ) \
    MAKE_REGISTER(CAN_TDT2R          , 0x01A4L , RW ) \
    MAKE_REGISTER(CAN_TDL2R          , 0x01A8L , RW ) \
    MAKE_REGISTER(CAN_TDH2R          , 0x01ACL , RW ) \
    MAKE_REGISTER(CAN_RI0R           , 0x01B0L , RW ) \
    MAKE_REGISTER(CAN_RDT0R          , 0x01B4L , RW ) \
    MAKE_REGISTER(CAN_RDL0R          , 0x01B8L , RW ) \
    MAKE_REGISTER(CAN_RDH0R          , 0x01BCL , RW ) \
    MAKE_REGISTER(CAN_RI1R           , 0x01C0L , RW ) \
    MAKE_REGISTER(CAN_RDT1R          , 0x01C4L , RW ) \
    MAKE_REGISTER(CAN_RDL1R          , 0x01C8L , RW ) \
    MAKE_REGISTER(CAN_RDH1R          , 0x01CCL , RW ) \
    MAKE_REGISTER(CAN_FMR            , 0x0200L , RW ) \
    MAKE_REGISTER(CAN_FM1R           , 0x0204L , RW ) \
    MAKE_REGISTER(CAN_FS1R           , 0x020CL , RW ) \
    MAKE_REGISTER(CAN_FFA1R          , 0x0214L , RW ) \
    MAKE_REGISTER(CAN_FA1R           , 0x021CL , RW ) \
    MAKE_REGISTER(CAN_F0R1           , 0x0240L , RW ) \
    MAKE_REGISTER(CAN_F0R2           , 0x0244L , RW ) \
    MAKE_REGISTER(CAN_F1R1           , 0x0248L , RW ) \
    MAKE_REGISTER(CAN_F1R2           , 0x024CL , RW ) \
    MAKE_REGISTER(CAN_F2R1           , 0x0250L , RW ) \
    MAKE_REGISTER(CAN_F2R2           , 0x0254L , RW ) \
    MAKE_REGISTER(CAN_F3R1           , 0x0258L , RW ) \
    MAKE_REGISTER(CAN_F3R2           , 0x025CL , RW ) \
    MAKE_REGISTER(CAN_F4R1           , 0x0260L , RW ) \
    MAKE_REGISTER(CAN_F4R2           , 0x0264L , RW ) \
    MAKE_REGISTER(CAN_F5R1           , 0x0268L , RW ) \
    MAKE_REGISTER(CAN_F5R2           , 0x026CL , RW ) \
    MAKE_REGISTER(CAN_F6R1           , 0x0270L , RW ) \
    MAKE_REGISTER(CAN_F6R2           , 0x0274L , RW ) \
    MAKE_REGISTER(CAN_F7R1           , 0x0278L , RW ) \
    MAKE_REGISTER(CAN_F7R2           , 0x027CL , RW ) \
    MAKE_REGISTER(CAN_F8R1           , 0x0280L , RW ) \
    MAKE_REGISTER(CAN_F8R2           , 0x0284L , RW ) \
    MAKE_REGISTER(CAN_F9R1           , 0x0288L , RW ) \
    MAKE_REGISTER(CAN_F9R2           , 0x028CL , RW ) \
    MAKE_REGISTER(CAN_F10R1          , 0x0290L , RW ) \
    MAKE_REGISTER(CAN_F10R2          , 0x0294L , RW ) \
    MAKE_REGISTER(CAN_F11R1          , 0x0298L , RW ) \
    MAKE_REGISTER(CAN_F11R2          , 0x029CL , RW ) \
    MAKE_REGISTER(CAN_F12R1          , 0x02A0L , RW ) \
    MAKE_REGISTER(CAN_F12R2          , 0x02A4L , RW ) \
    MAKE_REGISTER(CAN_F13R1          , 0x02A8L , RW ) \
    MAKE_REGISTER(CAN_F13R2          , 0x02ACL , RW ) \
    MAKE_REGISTER(CAN_F14R1          , 0x02B0L , RW ) \
    MAKE_REGISTER(CAN_F14R2          , 0x02B4L , RW ) \
    MAKE_REGISTER(CAN_F15R1          , 0x02B8L , RW ) \
    MAKE_REGISTER(CAN_F15R2          , 0x02BCL , RW ) \
    MAKE_REGISTER(CAN_F16R1          , 0x02C0L , RW ) \
    MAKE_REGISTER(CAN_F16R2          , 0x02C4L , RW ) \
    MAKE_REGISTER(CAN_F17R1          , 0x02C8L , RW ) \
    MAKE_REGISTER(CAN_F17R2          , 0x02CCL , RW ) \
    MAKE_REGISTER(CAN_F18R1          , 0x02D0L , RW ) \
    MAKE_REGISTER(CAN_F18R2          , 0x02D4L , RW ) \
    MAKE_REGISTER(CAN_F19R1          , 0x02D8L , RW ) \
    MAKE_REGISTER(CAN_F19R2          , 0x02DCL , RW ) \
    MAKE_REGISTER(CAN_F20R1          , 0x02E0L , RW ) \
    MAKE_REGISTER(CAN_F20R2          , 0x02E4L , RW ) \
    MAKE_REGISTER(CAN_F21R1          , 0x02E8L , RW ) \
    MAKE_REGISTER(CAN_F21R2          , 0x02ECL , RW ) \
    MAKE_REGISTER(CAN_F22R1          , 0x02F0L , RW ) \
    MAKE_REGISTER(CAN_F22R2          , 0x02F4L , RW ) \
    MAKE_REGISTER(CAN_F23R1          , 0x02F8L , RW ) \
    MAKE_REGISTER(CAN_F23R2          , 0x02FCL , RW ) \
    MAKE_REGISTER(CAN_F24R1          , 0x0300L , RW ) \
    MAKE_REGISTER(CAN_F24R2          , 0x0304L , RW ) \
    MAKE_REGISTER(CAN_F25R1          , 0x0308L , RW ) \
    MAKE_REGISTER(CAN_F25R2          , 0x030CL , RW ) \
    MAKE_REGISTER(CAN_F26R1          , 0x0310L , RW ) \
    MAKE_REGISTER(CAN_F26R2          , 0x0314L , RW ) \
    MAKE_REGISTER(CAN_F27R1          , 0x0318L , RW ) \
    MAKE_REGISTER(CAN_F27R2          , 0x031CL , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in OTG register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_OTG(MAKE_REGISTER) \
    MAKE_REGISTER(OTG_GOTGCTL        , 0x0000L , RW ) \
    MAKE_REGISTER(OTG_GOTGINT        , 0x0004L , RW ) \
    MAKE_REGISTER(OTG_GAHBCFG        , 0x0008L , RW ) \
    MAKE_REGISTER(OTG_GUSBCFG        , 0x000CL , RW ) \
    MAKE_REGISTER(OTG_GRSTCTL        , 0x0010L , RW ) \
    MAKE_REGISTER(OTG_GINTSTS        , 0x0014L , RW ) \
    MAKE_REGISTER(OTG_GINTMSK        , 0x0018L , RW ) \
    MAKE_REGISTER(OTG_GRXSTSR        , 0x001CL , RW ) \
    MAKE_REGISTER(OTG_GRXSTSPR       , 0x0020L , RW ) \
    MAKE_REGISTER(OTG_GRXFSIZ        , 0x0024L , RW ) \
    MAKE_REGISTER(OTG_HNPTXFSIZ      , 0x0028L , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF0       , 0x0028L , RW ) \
    MAKE_REGISTER(OTG_HNPTXSTS       , 0x002CL , RW ) \
    MAKE_REGISTER(OTG_GI2CCTL        , 0x0030L , RW ) \
    MAKE_REGISTER(OTG_GCCFG          , 0x0038L , RW ) \
    MAKE_REGISTER(OTG_CID            , 0x003CL , RW ) \
    MAKE_REGISTER(OTG_GLPMCFG        , 0x0054L , RW ) \
    MAKE_REGISTER(OTG_HPTXFSIZ       , 0x0100L , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF1       , 0x0104L , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF2       , 0x0108L , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF3       , 0x010CL , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF4       , 0x0110L , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF5       , 0x0204L , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF6       , 0x0208L , RW ) \
    MAKE_REGISTER(OTG_DIEPTXF7       , 0x0244L , RW ) \
    MAKE_REGISTER(OTG_HCFG           , 0x0400L , RW ) \
    MAKE_REGISTER(OTG_HFIR           , 0x0404L , RW ) \
    MAKE_REGISTER(OTG_HFNUM          , 0x0408L , RW ) \
    MAKE_REGISTER(OTG_HPTXSTS        , 0x0410L , RW ) \
    MAKE_REGISTER(OTG_HAINT          , 0x0414L , RW ) \
    MAKE_REGISTER(OTG_HAINTMSK       , 0x0418L , RW ) \
    MAKE_REGISTER(OTG_HPRT           , 0x0440L , RW ) \
    MAKE_REGISTER(OTG_HCSPLT0        , 0x0504L , RW ) \
    MAKE_REGISTER(OTG_HCCHAR0        , 0x0500L , RW ) \
    MAKE_REGISTER(OTG_HCINT0         , 0x0508L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ0        , 0x0510L , RW ) \
    MAKE_REGISTER(OTG_HCUNTMSK0      , 0x050CL , RW ) \
    MAKE_REGISTER(OTG_HCDMA0         , 0x0514L , RW ) \
    MAKE_REGISTER(OTG_HCCHAR1        , 0x0520L , RW ) \
    MAKE_REGISTER(OTG_HCINT1         , 0x0528L , RW ) \
    MAKE_REGISTER(OTG_HCINTMSK1      , 0x052CL , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ1        , 0x0530L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ2        , 0x0534L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ3        , 0x0538L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ4        , 0x053CL , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ5        , 0x0540L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ6        , 0x0544L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ7        , 0x0548L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ8        , 0x054CL , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ9        , 0x0550L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ10       , 0x0554L , RW ) \
    MAKE_REGISTER(OTG_HCCHAR11       , 0x0660L , RW ) \
    MAKE_REGISTER(OTG_HCINTMSK11     , 0x066CL , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ11       , 0x0670L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ12       , 0x0674L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ13       , 0x0678L , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ14       , 0x067CL , RW ) \
    MAKE_REGISTER(OTG_HCCHAR15       , 0x06E0L , RW ) \
    MAKE_REGISTER(OTG_HCSPLT15       , 0x06E4L , RW ) \
    MAKE_REGISTER(OTG_HCINTMSK15     , 0x06ECL , RW ) \
    MAKE_REGISTER(OTG_HCTSIZ15       , 0x06F0L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in ETH register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_ETH(MAKE_REGISTER) \
    MAKE_REGISTER(ETH_MACCR          , 0x0000L , RW ) \
    MAKE_REGISTER(ETH_MACFFR         , 0x0004L , RW ) \
    MAKE_REGISTER(ETH_MACHTHR        , 0x0008L , RW ) \
    MAKE_REGISTER(ETH_MACHTLR        , 0x000CL , RW ) \
    MAKE_REGISTER(ETH_MACMIIAR       , 0x0010L , RW ) \
    MAKE_REGISTER(ETH_MACMIIDR       , 0x0014L , RW ) \
    MAKE_REGISTER(ETH_MACFCR         , 0x0018L , RW ) \
    MAKE_REGISTER(ETH_MACVLANTR      , 0x001CL , RW ) \
    MAKE_REGISTER(ETH_MACRWUFFR      , 0x0028L , RW ) \
    MAKE_REGISTER(ETH_MACPMTCSR      , 0x002CL , RW ) \
    MAKE_REGISTER(ETH_MACDBGR        , 0x0034L , RW ) \
    MAKE_REGISTER(ETH_MACSR          , 0x0038L , RW ) \
    MAKE_REGISTER(ETH_MACIMR         , 0x003CL , RW ) \
    MAKE_REGISTER(ETH_MACA0HR        , 0x0040L , RW ) \
    MAKE_REGISTER(ETH_MACA0LR        , 0x0044L , RW ) \
    MAKE_REGISTER(ETH_MACA1HR        , 0x0048L , RW ) \
    MAKE_REGISTER(ETH_MACA1LR        , 0x004CL , RW ) \
    MAKE_REGISTER(ETH_MACA2HR        , 0x0050L , RW ) \
    MAKE_REGISTER(ETH_MACA2LR        , 0x0054L , RW ) \
    MAKE_REGISTER(ETH_MACA3HR        , 0x0058L , RW ) \
    MAKE_REGISTER(ETH_MACA3LR        , 0x005CL , RW ) \
    MAKE_REGISTER(ETH_MMCCR          , 0x0100L , RW ) \
    MAKE_REGISTER(ETH_MMCRIR         , 0x0104L , RW ) \
    MAKE_REGISTER(ETH_MMCTIR         , 0x0108L , RW ) \
    MAKE_REGISTER(ETH_MMCRIMR        , 0x010CL , RW ) \
    MAKE_REGISTER(ETH_MMCTIMR        , 0x0110L , RW ) \
    MAKE_REGISTER(ETH_MMCTGFSCCR     , 0x014CL , RW ) \
    MAKE_REGISTER(ETH_MMCTGFMSCCR    , 0x0150L , RW ) \
    MAKE_REGISTER(ETH_MMCTGFCR       , 0x0168L , RW ) \
    MAKE_REGISTER(ETH_MMCRFCECR      , 0x0194L , RW ) \
    MAKE_REGISTER(ETH_MMCRFAECR      , 0x0198L , RW ) \
    MAKE_REGISTER(ETH_MMCRGUFCR      , 0x01C4L , RW ) \
    MAKE_REGISTER(ETH_PTPTSCR        , 0x0700L , RW ) \
    MAKE_REGISTER(ETH_PTPSSIR        , 0x0704L , RW ) \
    MAKE_REGISTER(ETH_PTPTSHR        , 0x0708L , RW ) \
    MAKE_REGISTER(ETH_PTPTSLR        , 0x070CL , RW ) \
    MAKE_REGISTER(ETH_PTPTSHUR       , 0x0710L , RW ) \
    MAKE_REGISTER(ETH_PTPTSLIR       , 0x0714L , RW ) \
    MAKE_REGISTER(ETH_PTPTSAR        , 0x0718L , RW ) \
    MAKE_REGISTER(ETH_PTPTTHR        , 0x071CL , RW ) \
    MAKE_REGISTER(ETH_PTPTTLR        , 0x0720L , RW ) \
    MAKE_REGISTER(ETH_PTPTSSR        , 0x0728L , RW ) \
    MAKE_REGISTER(ETH_DMABMR         , 0x1000L , RW ) \
    MAKE_REGISTER(ETH_DMATPDR        , 0x1004L , RW ) \
    MAKE_REGISTER(ETH_DMARPDR        , 0x1008L , RW ) \
    MAKE_REGISTER(ETH_DMARDLAR       , 0x100CL , RW ) \
    MAKE_REGISTER(ETH_DMATDLAR       , 0x1010L , RW ) \
    MAKE_REGISTER(ETH_DMASR          , 0x1014L , RW ) \
    MAKE_REGISTER(ETH_DMAOMR         , 0x1018L , RW ) \
    MAKE_REGISTER(ETH_DMAIER         , 0x101CL , RW ) \
    MAKE_REGISTER(ETH_DMAMFBOCR      , 0x1020L , RW ) \
    MAKE_REGISTER(ETH_DMARSWTR       , 0x1024L , RW ) \
    MAKE_REGISTER(ETH_DMACHTDR       , 0x1048L , RW ) \
    MAKE_REGISTER(ETH_DMACHRDR       , 0x104CL , RW ) \
    MAKE_REGISTER(ETH_DMACHTBAR      , 0x1050L , RW ) \
    MAKE_REGISTER(ETH_DMACHRBAR      , 0x1054L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief List of registers in HDMI register map
 *
 * This definition contains list of registers in the register map.
 *
 * To add register use
   @code
     MAKE_REGISTER( REGISTER_NAME , OFFSET , ACCESS ) \
   @endcode
 *
 * Where:
 *           REGISTER_NAME      - name of register
 *           OFFSET             - Offset from the base address of register map
 *           ACCESS             - R/W/RW
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_HDMI(MAKE_REGISTER) \
    MAKE_REGISTER(CEC_CR             , 0x0000L , RW ) \
    MAKE_REGISTER(CEC_CFGR           , 0x0004L , RW ) \
    MAKE_REGISTER(CEC_TXDR           , 0x0008L , RW ) \
    MAKE_REGISTER(CEC_RXDR           , 0x000CL , RW ) \
    MAKE_REGISTER(CEC_ISR            , 0x0010L , RW ) \
    MAKE_REGISTER(CEC_IER            , 0x0014L , RW ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of register maps in this file
 *
 * This definition contains all register maps names. If you want to add new register map, you must add it also to this list.
 *
 * To add new register map use:
 * @code
       MAKE_REGISTER_MAP( REGISTER_MAP_NAME )\
   @endcode
 * Where:
 *          REGISTER_MAP_NAME   - name of the register map. The definition with list of registers for this register map should be compatible
 *                                with macro #oC_REGISTER_MAP_
 */
//==========================================================================================================================================
#define oC_REGISTER_MAP_LIST(MAKE_REGISTER_MAP) \
    MAKE_REGISTER_MAP(FLASH) \
    MAKE_REGISTER_MAP(PWR) \
    MAKE_REGISTER_MAP(RCC) \
    MAKE_REGISTER_MAP(GPIO) \
    MAKE_REGISTER_MAP(SYSCFG) \
    MAKE_REGISTER_MAP(DMA) \
    MAKE_REGISTER_MAP(DMA2D) \
    MAKE_REGISTER_MAP(EXTI) \
    MAKE_REGISTER_MAP(CRC) \
    MAKE_REGISTER_MAP(FMC) \
    MAKE_REGISTER_MAP(QUADSPI) \
    MAKE_REGISTER_MAP(ADC) \
    MAKE_REGISTER_MAP(ADCCOMMON) \
    MAKE_REGISTER_MAP(DAC) \
    MAKE_REGISTER_MAP(DCMI) \
    MAKE_REGISTER_MAP(LTDC) \
    MAKE_REGISTER_MAP(RNG) \
    MAKE_REGISTER_MAP(CRYP) \
    MAKE_REGISTER_MAP(HASH) \
    MAKE_REGISTER_MAP(TIMx) \
    MAKE_REGISTER_MAP(LPTIMx) \
    MAKE_REGISTER_MAP(IWDG) \
    MAKE_REGISTER_MAP(WWDG) \
    MAKE_REGISTER_MAP(RTC) \
    MAKE_REGISTER_MAP(I2C) \
    MAKE_REGISTER_MAP(USART) \
    MAKE_REGISTER_MAP(SPI) \
    MAKE_REGISTER_MAP(SAI) \
    MAKE_REGISTER_MAP(SPDIFRX) \
    MAKE_REGISTER_MAP(SDMMC) \
    MAKE_REGISTER_MAP(CAN) \
    MAKE_REGISTER_MAP(OTG) \
    MAKE_REGISTER_MAP(ETH) \
    MAKE_REGISTER_MAP(HDMI) \

#endif /* SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_MACHINE_RMAPSDEFS_H_ */
