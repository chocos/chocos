/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the PORT driver
 *
 * @file       oc_pins_defs.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_PINS_DEFS_H_
#define SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_PINS_DEFS_H_

#include <oc_1word.h>

#define oC_PORT_WIDTH                       16ULL
#define oC_ALTERNATE_NUMBER_FIELD_WIDTH      4ULL
#define oC_MODULE_PIN_FUNCTION_WIDTH         4ULL

//==========================================================================================================================================
/** @hideinitializer
 * This is the list that contains definitions of pins and ports.
 *
 * To add port use format:
 *          MAKE_PIN( BASE_ADDRESS_NAME , PIN_NAME , BIT_INDEX , PIN_NUMBER )
 *
 *                  where:
 *                          REGISTER_MAP_NAME   - name of the register map address from the #oC_MACHINE_PORT_REGISTERS_MAPS_LIST definition
 *                          PIN_NAME            - name of the pin (PA0 for example)
 *                          BIT_INDEX           - index of the bit in the port for this pin
 *                          PIN_NUMBER          - number of pin in the hardware
 */
//==========================================================================================================================================
#define oC_PINS_LIST(MAKE_PIN)\
        /* The A Row */                  \
        MAKE_PIN( PORTE , PE4  ,  4 , A1  )\
        MAKE_PIN( PORTE , PE3  ,  3 , A2  )\
        MAKE_PIN( PORTE , PE2  ,  2 , A3  )\
        MAKE_PIN( PORTG , PG14 , 14 , A4  )\
        MAKE_PIN( PORTE , PE1  ,  1 , A5  )\
        MAKE_PIN( PORTE , PE0  ,  0 , A6  )\
        MAKE_PIN( PORTB , PB8  ,  8 , A7  )\
        MAKE_PIN( PORTB , PB5  ,  5 , A8  )\
        MAKE_PIN( PORTB , PB4  ,  4 , A9  )\
        MAKE_PIN( PORTB , PB3  ,  3 , A10 )\
        MAKE_PIN( PORTD , PD7  ,  7 , A11 )\
        MAKE_PIN( PORTC , PC12 , 12 , A12 )\
        MAKE_PIN( PORTA , PA15 , 15 , A13 )\
        MAKE_PIN( PORTA , PA14 , 14 , A14 )\
        MAKE_PIN( PORTA , PA13 , 13 , A15 )\
        /* The B Row */                   \
        MAKE_PIN( PORTE , PE5  ,  5 , B1  )\
        MAKE_PIN( PORTE , PE6  ,  6 , B2  )\
        MAKE_PIN( PORTG , PG13 , 13 , B3  )\
        MAKE_PIN( PORTB , PB9  ,  9 , B4  )\
        MAKE_PIN( PORTB , PB7  ,  7 , B5  )\
        MAKE_PIN( PORTB , PB6  ,  6 , B6  )\
        MAKE_PIN( PORTG , PG15 , 15 , B7  )\
        MAKE_PIN( PORTG , PG11 , 11 , B8  )\
        MAKE_PIN( PORTJ , PJ13 , 13 , B9  )\
        MAKE_PIN( PORTJ , PJ12 , 12 , B10 )\
        MAKE_PIN( PORTD , PD6  ,  6 , B11 )\
        MAKE_PIN( PORTD , PD0  ,  0 , B12 )\
        MAKE_PIN( PORTC , PC11 , 11 , B13 )\
        MAKE_PIN( PORTC , PC10 , 10 , B14 )\
        MAKE_PIN( PORTA , PA12 , 12 , B15 )\
        /* The C Row */                   \
        MAKE_PIN( PORTI , PI8  ,  8 , C2  )\
        MAKE_PIN( PORTI , PI4  ,  4 , C3  )\
        MAKE_PIN( PORTK , PK7  ,  7 , C4  )\
        MAKE_PIN( PORTK , PK6  ,  6 , C5  )\
        MAKE_PIN( PORTK , PK5  ,  5 , C6  )\
        MAKE_PIN( PORTG , PG12 , 12 , C7  )\
        MAKE_PIN( PORTG , PG10 , 10 , C8  )\
        MAKE_PIN( PORTJ , PJ14 , 14 , C9  )\
        MAKE_PIN( PORTD , PD5  ,  5 , C10 )\
        MAKE_PIN( PORTD , PD3  ,  3 , C11 )\
        MAKE_PIN( PORTD , PD1  ,  1 , C12 )\
        MAKE_PIN( PORTI , PI3  ,  3 , C13 )\
        MAKE_PIN( PORTI , PI2  ,  2 , C14 )\
        MAKE_PIN( PORTA , PA11 , 11 , C15 )\
        /* The D Row */                   \
        MAKE_PIN( PORTC , PC13 , 13 , D1  )\
        MAKE_PIN( PORTF , PF0  ,  0 , D2  )\
        MAKE_PIN( PORTI , PI5  ,  5 , D3  )\
        MAKE_PIN( PORTI , PI7  ,  7 , D4  )\
        MAKE_PIN( PORTI , PI10 , 10 , D5  )\
        MAKE_PIN( PORTI , PI6  ,  6 , D6  )\
        MAKE_PIN( PORTK , PK4  ,  4 , D7  )\
        MAKE_PIN( PORTK , PK3  ,  3 , D8  )\
        MAKE_PIN( PORTG , PG9  ,  9 , D9  )\
        MAKE_PIN( PORTJ , PJ15 , 15 , D10 )\
        MAKE_PIN( PORTD , PD4  ,  4 , D11 )\
        MAKE_PIN( PORTD , PD2  ,  2 , D12 )\
        MAKE_PIN( PORTH , PH15 , 15 , D13 )\
        MAKE_PIN( PORTI , PI1  ,  1 , D14 )\
        MAKE_PIN( PORTA , PA10 , 10 , D15 )\
        /* The E Row */                   \
        MAKE_PIN( PORTC , PC14 , 14 , E1  )\
        MAKE_PIN( PORTF , PF1  ,  1 , E2  )\
        MAKE_PIN( PORTI , PI12 , 12 , E3  )\
        MAKE_PIN( PORTI , PI9  ,  9 , E4  )\
        MAKE_PIN( PORTH , PH13 , 13 , E12 )\
        MAKE_PIN( PORTH , PH14 , 14 , E13 )\
        MAKE_PIN( PORTI , PI0  ,  0 , E14 )\
        MAKE_PIN( PORTA , PA9  ,  9 , E15 )\
        /* The F Row */                   \
        MAKE_PIN( PORTC , PC15 , 15 , F1  )\
        MAKE_PIN( PORTI , PI11 , 11 , F3  )\
        MAKE_PIN( PORTK , PK1  ,  1 , F12 )\
        MAKE_PIN( PORTK , PK2  ,  2 , F13 )\
        MAKE_PIN( PORTC , PC9  ,  9 , F14 )\
        MAKE_PIN( PORTA , PA8  ,  8 , F15 )\
        /* The G Row */                   \
        MAKE_PIN( PORTH , PH0  ,  0 , G1  )\
        MAKE_PIN( PORTF , PF2  ,  2 , G2  )\
        MAKE_PIN( PORTI , PI13 , 13 , G3  )\
        MAKE_PIN( PORTI , PI15 , 15 , G4  )\
        MAKE_PIN( PORTJ , PJ11 , 11 , G12 )\
        MAKE_PIN( PORTK , PK0  ,  0 , G13 )\
        MAKE_PIN( PORTC , PC8  ,  8 , G14 )\
        MAKE_PIN( PORTC , PC7  ,  7 , G15 )\
        /* The H Row */                   \
        MAKE_PIN( PORTH , PH1  ,  1 , H1  )\
        MAKE_PIN( PORTF , PF3  ,  3 , H2  )\
        MAKE_PIN( PORTI , PI14 , 14 , H3  )\
        MAKE_PIN( PORTH , PH4  ,  4 , H4  )\
        MAKE_PIN( PORTJ , PJ8  ,  8 , H12 )\
        MAKE_PIN( PORTJ , PJ10 , 10 , H13 )\
        MAKE_PIN( PORTG , PG8  ,  8 , H14 )\
        MAKE_PIN( PORTC , PC6  ,  6 , H15 )\
        /* The J Row */                   \
        MAKE_PIN( PORTF , PF4  ,  4 , J2  )\
        MAKE_PIN( PORTH , PH5  ,  5 , J3  )\
        MAKE_PIN( PORTH , PH3  ,  3 , J4  )\
        MAKE_PIN( PORTJ , PJ7  ,  7 , J12 )\
        MAKE_PIN( PORTJ , PJ9  ,  9 , J13 )\
        MAKE_PIN( PORTG , PG7  ,  7 , J14 )\
        MAKE_PIN( PORTG , PG6  ,  6 , J15 )\
        /* The K Row */                   \
        MAKE_PIN( PORTF , PF7  ,  7 , K1  )\
        MAKE_PIN( PORTF , PF6  ,  6 , K2  )\
        MAKE_PIN( PORTF , PF5  ,  5 , K3  )\
        MAKE_PIN( PORTH , PH2  ,  2 , K4  )\
        MAKE_PIN( PORTJ , PJ6  ,  6 , K12 )\
        MAKE_PIN( PORTD , PD15 , 15 , K13 )\
        MAKE_PIN( PORTB , PB13 , 13 , K14 )\
        MAKE_PIN( PORTD , PD10 , 10 , K15 )\
        /* The L Row */                   \
        MAKE_PIN( PORTF , PF10 , 10 , L1  )\
        MAKE_PIN( PORTF , PF9  ,  9 , L2  )\
        MAKE_PIN( PORTF , PF8  ,  8 , L3  )\
        MAKE_PIN( PORTC , PC3  ,  3 , L4  )\
        MAKE_PIN( PORTD , PD14 , 14 , L12 )\
        MAKE_PIN( PORTB , PB12 , 12 , L13 )\
        MAKE_PIN( PORTD , PD9  ,  9 , L14 )\
        MAKE_PIN( PORTD , PD8  ,  8 , L15 )\
        /* The M Row */                   \
        MAKE_PIN( PORTC , PC0  ,  0 , M2  )\
        MAKE_PIN( PORTC , PC1  ,  1 , M3  )\
        MAKE_PIN( PORTC , PC2  ,  2 , M4  )\
        MAKE_PIN( PORTB , PB2  ,  2 , M5  )\
        MAKE_PIN( PORTF , PF12 , 12 , M6  )\
        MAKE_PIN( PORTG , PG1  ,  1 , M7  )\
        MAKE_PIN( PORTF , PF15 , 15 , M8  )\
        MAKE_PIN( PORTJ , PJ4  ,  4 , M9  )\
        MAKE_PIN( PORTD , PD12 , 12 , M10 )\
        MAKE_PIN( PORTD , PD13 , 13 , M11 )\
        MAKE_PIN( PORTG , PG3  ,  3 , M12 )\
        MAKE_PIN( PORTG , PG2  ,  2 , M13 )\
        MAKE_PIN( PORTJ , PJ5  ,  5 , M14 )\
        MAKE_PIN( PORTH , PH12 , 12 , M15 )\
        /* The N Row */                   \
        MAKE_PIN( PORTA , PA1  ,  1 , N2  )\
        MAKE_PIN( PORTA , PA0  ,  0 , N3  )\
        MAKE_PIN( PORTA , PA4  ,  4 , N4  )\
        MAKE_PIN( PORTC , PC4  ,  4 , N5  )\
        MAKE_PIN( PORTF , PF13 , 13 , N6  )\
        MAKE_PIN( PORTG , PG0  ,  0 , N7  )\
        MAKE_PIN( PORTJ , PJ3  ,  3 , N8  )\
        MAKE_PIN( PORTE , PE8  ,  8 , N9  )\
        MAKE_PIN( PORTD , PD11 , 11 , N10 )\
        MAKE_PIN( PORTG , PG5  ,  5 , N11 )\
        MAKE_PIN( PORTG , PG4  ,  4 , N12 )\
        MAKE_PIN( PORTH , PH7  ,  7 , N13 )\
        MAKE_PIN( PORTH , PH9  ,  9 , N14 )\
        MAKE_PIN( PORTH , PH11 , 11 , N15 )\
        /* The P Row */                   \
        MAKE_PIN( PORTA , PA2  ,  2 , P2  )\
        MAKE_PIN( PORTA , PA6  ,  6 , P3  )\
        MAKE_PIN( PORTA , PA5  ,  5 , P4  )\
        MAKE_PIN( PORTC , PC5  ,  5 , P5  )\
        MAKE_PIN( PORTF , PF14 , 14 , P6  )\
        MAKE_PIN( PORTJ , PJ2  ,  2 , P7  )\
        MAKE_PIN( PORTF , PF11 , 11 , P8  )\
        MAKE_PIN( PORTE , PE9  ,  9 , P9  )\
        MAKE_PIN( PORTE , PE11 , 11 , P10 )\
        MAKE_PIN( PORTE , PE14 , 14 , P11 )\
        MAKE_PIN( PORTB , PB10 , 10 , P12 )\
        MAKE_PIN( PORTH , PH6  ,  6 , P13 )\
        MAKE_PIN( PORTH , PH8  ,  8 , P14 )\
        MAKE_PIN( PORTH , PH10 , 10 , P15 )\
        /* The R Row */                   \
        MAKE_PIN( PORTA , PA3  ,  3 , R2  )\
        MAKE_PIN( PORTA , PA7  ,  7 , R3  )\
        MAKE_PIN( PORTB , PB1  ,  1 , R4  )\
        MAKE_PIN( PORTB , PB0  ,  0 , R5  )\
        MAKE_PIN( PORTJ , PJ0  ,  0 , R6  )\
        MAKE_PIN( PORTJ , PJ1  ,  1 , R7  )\
        MAKE_PIN( PORTE , PE7  ,  7 , R8  )\
        MAKE_PIN( PORTE , PE10 , 10 , R9  )\
        MAKE_PIN( PORTE , PE12 , 12 , R10 )\
        MAKE_PIN( PORTE , PE15 , 15 , R11 )\
        MAKE_PIN( PORTE , PE13 , 13 , R12 )\
        MAKE_PIN( PORTB , PB11 , 11 , R13 )\
        MAKE_PIN( PORTB , PB14 , 14 , R14 )\
        MAKE_PIN( PORTB , PB15 , 15 , R15 )\

#define oC_MODULE_PIN_FUNCTIONS_(MODULE_NAME)        oC_1WORD_FROM_2(oC_MODULE_PIN_FUNCTIONS_ , MODULE_NAME)

#define oC_MODULE_PINS_(MODULE_NAME)                 oC_1WORD_FROM_2(oC_MODULE_PINS_ , MODULE_NAME)

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        PINS OF THE LCDTFT MODULE
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define oC_MODULE_PIN_FUNCTIONS_LCDTFT(MAKE_PIN_FUNCTION)         \
    MAKE_PIN_FUNCTION(LCD_R0)\
    MAKE_PIN_FUNCTION(LCD_R1)\
    MAKE_PIN_FUNCTION(LCD_R2)\
    MAKE_PIN_FUNCTION(LCD_R3)\
    MAKE_PIN_FUNCTION(LCD_R4)\
    MAKE_PIN_FUNCTION(LCD_R5)\
    MAKE_PIN_FUNCTION(LCD_R6)\
    MAKE_PIN_FUNCTION(LCD_R7)\
    MAKE_PIN_FUNCTION(LCD_G0)\
    MAKE_PIN_FUNCTION(LCD_G1)\
    MAKE_PIN_FUNCTION(LCD_G2)\
    MAKE_PIN_FUNCTION(LCD_G3)\
    MAKE_PIN_FUNCTION(LCD_G4)\
    MAKE_PIN_FUNCTION(LCD_G5)\
    MAKE_PIN_FUNCTION(LCD_G6)\
    MAKE_PIN_FUNCTION(LCD_G7)\
    MAKE_PIN_FUNCTION(LCD_B0)\
    MAKE_PIN_FUNCTION(LCD_B1)\
    MAKE_PIN_FUNCTION(LCD_B2)\
    MAKE_PIN_FUNCTION(LCD_B3)\
    MAKE_PIN_FUNCTION(LCD_B4)\
    MAKE_PIN_FUNCTION(LCD_B5)\
    MAKE_PIN_FUNCTION(LCD_B6)\
    MAKE_PIN_FUNCTION(LCD_B7)\
    MAKE_PIN_FUNCTION(LCD_VSYNC)\
    MAKE_PIN_FUNCTION(LCD_HSYNC)\
    MAKE_PIN_FUNCTION(LCD_CLK)\
    MAKE_PIN_FUNCTION(LCD_DE)\

#define oC_MODULE_PINS_LCDTFT(MAKE_PIN)       \
    MAKE_PIN( PA1  , LCD , LCD_R2    , 14) \
    MAKE_PIN( PA2  , LCD , LCD_R1    , 14) \
    MAKE_PIN( PA3  , LCD , LCD_B5    , 14) \
    MAKE_PIN( PA4  , LCD , LCD_VSYNC , 14) \
    MAKE_PIN( PA5  , LCD , LCD_R4    , 14) \
    MAKE_PIN( PA6  , LCD , LCD_G2    , 14) \
    MAKE_PIN( PA8  , LCD , LCD_R6    , 14) \
    MAKE_PIN( PA11 , LCD , LCD_R4    , 14) \
    MAKE_PIN( PA12 , LCD , LCD_R5    , 14) \
    MAKE_PIN( PB8  , LCD , LCD_B6    , 14) \
    MAKE_PIN( PB9  , LCD , LCD_B7    , 14) \
    MAKE_PIN( PB10 , LCD , LCD_G4    , 14) \
    MAKE_PIN( PB11 , LCD , LCD_G5    , 14) \
    MAKE_PIN( PC0  , LCD , LCD_R5    , 14) \
    MAKE_PIN( PC6  , LCD , LCD_HSYNC , 14) \
    MAKE_PIN( PC7  , LCD , LCD_G6    , 14) \
    MAKE_PIN( PC10 , LCD , LCD_R2    , 14) \
    MAKE_PIN( PD3  , LCD , LCD_G7    , 14) \
    MAKE_PIN( PD6  , LCD , LCD_B2    , 14) \
    MAKE_PIN( PD10 , LCD , LCD_B3    , 14) \
    MAKE_PIN( PE4  , LCD , LCD_B0    , 14) \
    MAKE_PIN( PE5  , LCD , LCD_G0    , 14) \
    MAKE_PIN( PE6  , LCD , LCD_G1    , 14) \
    MAKE_PIN( PE11 , LCD , LCD_G3    , 14) \
    MAKE_PIN( PE12 , LCD , LCD_B4    , 14) \
    MAKE_PIN( PE13 , LCD , LCD_DE    , 14) \
    MAKE_PIN( PE14 , LCD , LCD_CLK   , 14) \
    MAKE_PIN( PE15 , LCD , LCD_R7    , 14) \
    MAKE_PIN( PF10 , LCD , LCD_DE    , 14) \
    MAKE_PIN( PG6  , LCD , LCD_R7    , 14) \
    MAKE_PIN( PG7  , LCD , LCD_CLK   , 14) \
    MAKE_PIN( PG10 , LCD , LCD_B2    , 14) \
    MAKE_PIN( PG11 , LCD , LCD_B3    , 14) \
    MAKE_PIN( PG12 , LCD , LCD_B4    , 14) \
    MAKE_PIN( PG13 , LCD , LCD_R0    , 14) \
    MAKE_PIN( PG14 , LCD , LCD_B0    , 14) \
    MAKE_PIN( PH2  , LCD , LCD_R0    , 14) \
    MAKE_PIN( PH3  , LCD , LCD_R1    , 14) \
    MAKE_PIN( PH8  , LCD , LCD_R2    , 14) \
    MAKE_PIN( PH9  , LCD , LCD_R3    , 14) \
    MAKE_PIN( PH10 , LCD , LCD_R4    , 14) \
    MAKE_PIN( PH11 , LCD , LCD_R5    , 14) \
    MAKE_PIN( PH12 , LCD , LCD_R6    , 14) \
    MAKE_PIN( PH13 , LCD , LCD_G2    , 14) \
    MAKE_PIN( PH14 , LCD , LCD_G3    , 14) \
    MAKE_PIN( PH15 , LCD , LCD_G4    , 14) \
    MAKE_PIN( PI0  , LCD , LCD_G5    , 14) \
    MAKE_PIN( PI1  , LCD , LCD_G6    , 14) \
    MAKE_PIN( PI2  , LCD , LCD_G7    , 14) \
    MAKE_PIN( PI4  , LCD , LCD_B4    , 14) \
    MAKE_PIN( PI5  , LCD , LCD_B5    , 14) \
    MAKE_PIN( PI6  , LCD , LCD_B6    , 14) \
    MAKE_PIN( PI7  , LCD , LCD_B7    , 14) \
    MAKE_PIN( PI9  , LCD , LCD_VSYNC , 14) \
    MAKE_PIN( PI10 , LCD , LCD_HSYNC , 14) \
    MAKE_PIN( PI12 , LCD , LCD_HSYNC , 14) \
    MAKE_PIN( PI13 , LCD , LCD_VSYNC , 14) \
    MAKE_PIN( PI14 , LCD , LCD_CLK   , 14) \
    MAKE_PIN( PI15 , LCD , LCD_R0    , 14) \
    MAKE_PIN( PJ0  , LCD , LCD_R1    , 14) \
    MAKE_PIN( PJ1  , LCD , LCD_R2    , 14) \
    MAKE_PIN( PJ2  , LCD , LCD_R3    , 14) \
    MAKE_PIN( PJ3  , LCD , LCD_R4    , 14) \
    MAKE_PIN( PJ4  , LCD , LCD_R5    , 14) \
    MAKE_PIN( PJ5  , LCD , LCD_R6    , 14) \
    MAKE_PIN( PJ6  , LCD , LCD_R7    , 14) \
    MAKE_PIN( PJ7  , LCD , LCD_G0    , 14) \
    MAKE_PIN( PJ8  , LCD , LCD_G1    , 14) \
    MAKE_PIN( PJ9  , LCD , LCD_G2    , 14) \
    MAKE_PIN( PJ10 , LCD , LCD_G3    , 14) \
    MAKE_PIN( PJ11 , LCD , LCD_G4    , 14) \
    MAKE_PIN( PJ12 , LCD , LCD_B0    , 14) \
    MAKE_PIN( PJ13 , LCD , LCD_B1    , 14) \
    MAKE_PIN( PJ14 , LCD , LCD_B2    , 14) \
    MAKE_PIN( PJ15 , LCD , LCD_B3    , 14) \
    MAKE_PIN( PK0  , LCD , LCD_G5    , 14) \
    MAKE_PIN( PK1  , LCD , LCD_G6    , 14) \
    MAKE_PIN( PK2  , LCD , LCD_G7    , 14) \
    MAKE_PIN( PK3  , LCD , LCD_B4    , 14) \
    MAKE_PIN( PK4  , LCD , LCD_B5    , 14) \
    MAKE_PIN( PK5  , LCD , LCD_B6    , 14) \
    MAKE_PIN( PK6  , LCD , LCD_B7    , 14) \
    MAKE_PIN( PK7  , LCD , LCD_DE    , 14) \

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        PINS OF THE UART MODULE
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define oC_MODULE_PIN_FUNCTIONS_UART(MAKE_PIN_FUNCTION)         \
    MAKE_PIN_FUNCTION(Rx)\
    MAKE_PIN_FUNCTION(Tx)\
    MAKE_PIN_FUNCTION(CTS)\
    MAKE_PIN_FUNCTION(RTS)\
    MAKE_PIN_FUNCTION(CK)\

#define oC_MODULE_PINS_UART(MAKE_PIN)       \
    MAKE_PIN( PA0  , UART1 , CTS , 7) \
    MAKE_PIN( PA1  , UART1 , RTS , 7) \
    MAKE_PIN( PA2  , UART1 , Tx  , 7) \
    MAKE_PIN( PA3  , UART1 , Rx  , 7) \
    MAKE_PIN( PA4  , UART1 , CK  , 7) \
    MAKE_PIN( PA8  , UART0 , CK  , 7) \
    MAKE_PIN( PA9  , UART0 , Tx  , 7) \
    MAKE_PIN( PA10 , UART0 , Rx  , 7) \
    MAKE_PIN( PA11 , UART0 , CTS , 7) \
    MAKE_PIN( PA12 , UART0 , RTS , 7) \
    MAKE_PIN( PB6  , UART0 , Tx  , 7) \
    MAKE_PIN( PB7  , UART0 , Rx  , 7) \
    MAKE_PIN( PB10 , UART2 , Tx  , 7) \
    MAKE_PIN( PB11 , UART2 , Rx  , 7) \
    MAKE_PIN( PB12 , UART2 , CK  , 7) \
    MAKE_PIN( PB13 , UART2 , CTS , 7) \
    MAKE_PIN( PB14 , UART2 , RTS , 7) \
    MAKE_PIN( PC8  , UART4 , RTS , 7) \
    MAKE_PIN( PC9  , UART4 , CTS , 7) \
    MAKE_PIN( PC10 , UART2 , Tx  , 7) \
    MAKE_PIN( PC11 , UART2 , Rx  , 7) \
    MAKE_PIN( PC12 , UART2 , CK  , 7) \
    MAKE_PIN( PD3  , UART1 , CTS , 7) \
    MAKE_PIN( PD4  , UART1 , RTS , 7) \
    MAKE_PIN( PD5  , UART1 , Tx  , 7) \
    MAKE_PIN( PD6  , UART1 , Rx  , 7) \
    MAKE_PIN( PD7  , UART1 , CK  , 7) \
    MAKE_PIN( PD8  , UART2 , Tx  , 7) \
    MAKE_PIN( PD9  , UART2 , Rx  , 7) \
    MAKE_PIN( PD10 , UART2 , CK  , 7) \
    MAKE_PIN( PD11 , UART2 , CTS , 7) \
    MAKE_PIN( PD12 , UART2 , RTS , 7) \
    MAKE_PIN( PA0  , UART3 , Tx  , 8) \
    MAKE_PIN( PA1  , UART3 , Rx  , 8) \
    MAKE_PIN( PA15 , UART3 , RTS , 8) \
    MAKE_PIN( PB0  , UART3 , CTS , 8) \
    MAKE_PIN( PC6  , UART5 , Tx  , 8) \
    MAKE_PIN( PC7  , UART5 , Rx  , 8) \
    MAKE_PIN( PC8  , UART5 , CK  , 8) \
    MAKE_PIN( PC10 , UART3 , Tx  , 8) \
    MAKE_PIN( PC11 , UART3 , Rx  , 8) \
    MAKE_PIN( PC12 , UART4 , Tx  , 8) \
    MAKE_PIN( PD2  , UART4 , Rx  , 8) \
    MAKE_PIN( PD14 , UART7 , CTS , 8) \
    MAKE_PIN( PD15 , UART7 , RTS , 8) \
    MAKE_PIN( PE0  , UART7 , Rx  , 8) \
    MAKE_PIN( PE1  , UART7 , Tx  , 8) \
    MAKE_PIN( PE7  , UART6 , Rx  , 8) \
    MAKE_PIN( PE8  , UART6 , Tx  , 8) \
    MAKE_PIN( PE9  , UART6 , RTS , 8) \
    MAKE_PIN( PE10 , UART6 , CTS , 8) \
    MAKE_PIN( PF6  , UART6 , Rx  , 8) \
    MAKE_PIN( PF7  , UART6 , Tx  , 8) \
    MAKE_PIN( PF8  , UART6 , RTS , 8) \
    MAKE_PIN( PF9  , UART6 , CTS , 8) \
    MAKE_PIN( PG7  , UART5 , CK  , 8) \
    MAKE_PIN( PG8  , UART5 , RTS , 8) \
    MAKE_PIN( PG9  , UART5 , Rx  , 8) \
    MAKE_PIN( PG12 , UART5 , RTS , 8) \
    MAKE_PIN( PG13 , UART5 , CTS , 8) \
    MAKE_PIN( PG14 , UART5 , Tx  , 8) \
    MAKE_PIN( PG15 , UART5 , CTS , 8) \

#define oC_MODULE_PIN_FUNCTIONS_SPI(MAKE_PIN_FUNCTION)         \
    MAKE_PIN_FUNCTION(MOSI)\
    MAKE_PIN_FUNCTION(MISO)\
    MAKE_PIN_FUNCTION(CS)\
    MAKE_PIN_FUNCTION(CLK)\
    MAKE_PIN_FUNCTION(NSS)\

#define oC_MODULE_PINS_SPI(MAKE_PIN)       \
    MAKE_PIN( PA4 , SPI0 , NSS , 5) \
    MAKE_PIN( PA4 , SPI2 , NSS , 6) \
    MAKE_PIN( PA5 , SPI0 , CLK , 5) \
    MAKE_PIN( PA6 , SPI0 , MISO, 5) \
    MAKE_PIN( PA7 , SPI0 , MOSI, 5) \
    MAKE_PIN( PA9 , SPI1 , CLK , 5) \
    MAKE_PIN( PA15, SPI0 , NSS , 5) \
    MAKE_PIN( PA15, SPI2 , NSS , 6) \
    MAKE_PIN( PB2 , SPI2 , MOSI, 7) \
    MAKE_PIN( PB3 , SPI0 , CLK , 5) \
    MAKE_PIN( PB3 , SPI2 , CLK , 6) \
    MAKE_PIN( PB4 , SPI0 , MISO, 5) \
    MAKE_PIN( PB4 , SPI2 , MISO, 6) \
    MAKE_PIN( PB4 , SPI1 , NSS , 7) \
    MAKE_PIN( PB5 , SPI0 , MOSI, 5) \
    MAKE_PIN( PB5 , SPI2 , MOSI, 6) \
    MAKE_PIN( PB9 , SPI1 , NSS , 5) \
    MAKE_PIN( PB10, SPI1 , CLK , 5) \
    MAKE_PIN( PB12, SPI1 , NSS , 5) \
    MAKE_PIN( PB13, SPI1 , CLK , 5) \
    MAKE_PIN( PB14, SPI1 , MISO, 5) \
    MAKE_PIN( PB15, SPI1 , MOSI, 5) \
    MAKE_PIN( PC1 , SPI1 , MOSI, 5) \
    MAKE_PIN( PC2 , SPI1 , MISO, 5) \
    MAKE_PIN( PC3 , SPI1 , MOSI, 5) \
    MAKE_PIN( PC10, SPI2 , CLK , 6) \
    MAKE_PIN( PC11, SPI2 , MISO, 6) \
    MAKE_PIN( PC12, SPI2 , MOSI, 6) \
    MAKE_PIN( PD3 , SPI1 , CLK , 5) \
    MAKE_PIN( PD6 , SPI2 , MOSI, 5) \
    MAKE_PIN( PE2 , SPI3 , CLK , 5) \
    MAKE_PIN( PE4 , SPI3 , NSS , 5) \
    MAKE_PIN( PE5 , SPI3 , MISO, 5) \
    MAKE_PIN( PE6 , SPI3 , MOSI, 5) \
    MAKE_PIN( PE11, SPI3 , NSS , 5) \
    MAKE_PIN( PE12, SPI3 , CLK , 5) \
    MAKE_PIN( PE13, SPI3 , MISO, 5) \
    MAKE_PIN( PE14, SPI3 , MOSI, 5) \
    MAKE_PIN( PF6 , SPI4 , NSS , 5) \
    MAKE_PIN( PF7 , SPI4 , CLK , 5) \
    MAKE_PIN( PF8 , SPI4 , MISO, 5) \
    MAKE_PIN( PF9 , SPI4 , MOSI, 5) \
    MAKE_PIN( PF11, SPI4 , MOSI, 5) \
    MAKE_PIN( PG8 , SPI5 , NSS , 5) \
    MAKE_PIN( PG12, SPI5 , MISO, 5) \
    MAKE_PIN( PG13, SPI5 , CLK , 5) \
    MAKE_PIN( PG14, SPI5 , MOSI, 5) \
    MAKE_PIN( PH5 , SPI4 , NSS , 5) \
    MAKE_PIN( PH6 , SPI4 , CLK , 5) \
    MAKE_PIN( PH7 , SPI4 , MISO, 5) \
    MAKE_PIN( PI0 , SPI1 , NSS , 5) \
    MAKE_PIN( PI1 , SPI1 , CLK , 5) \
    MAKE_PIN( PI2 , SPI1 , MISO, 5) \
    MAKE_PIN( PI3 , SPI1 , MOSI, 5) \

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        PINS OF THE TIMER MODULE
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define oC_MODULE_PIN_FUNCTIONS_TIMER(MAKE_PIN_FUNCTION)    \
    MAKE_PIN_FUNCTION(CH1) \
    MAKE_PIN_FUNCTION(CH2) \
    MAKE_PIN_FUNCTION(CH3) \
    MAKE_PIN_FUNCTION(CH4) \
    MAKE_PIN_FUNCTION(CH1N) \
    MAKE_PIN_FUNCTION(CH2N) \
    MAKE_PIN_FUNCTION(CH3N) \
    MAKE_PIN_FUNCTION(ETR) \
    MAKE_PIN_FUNCTION(BKIN) \
    MAKE_PIN_FUNCTION(BKIN2) \
    MAKE_PIN_FUNCTION(IN1) \
    MAKE_PIN_FUNCTION(IN2) \
    MAKE_PIN_FUNCTION(OUT) \

#define oC_MODULE_PINS_TIMER(MAKE_PIN)       \
    MAKE_PIN( PA0 , Timer1  , CH1   , 1) \
    MAKE_PIN( PA0 , Timer1  , ETR   , 1) \
    MAKE_PIN( PA1 , Timer1  , CH2   , 1) \
    MAKE_PIN( PA2 , Timer1  , CH3   , 1) \
    MAKE_PIN( PA3 , Timer1  , CH4   , 1) \
    MAKE_PIN( PA5 , Timer1  , CH1   , 1) \
    MAKE_PIN( PA5 , Timer1  , ETR   , 1) \
    MAKE_PIN( PA6 , Timer0  , BKIN  , 1) \
    MAKE_PIN( PA7 , Timer0  , CH1N  , 1) \
    MAKE_PIN( PA8 , Timer0  , CH1   , 1) \
    MAKE_PIN( PA9 , Timer0  , CH2   , 1) \
    MAKE_PIN( PA10, Timer0  , CH3   , 1) \
    MAKE_PIN( PA11, Timer0  , CH4   , 1) \
    MAKE_PIN( PA12, Timer0  , ETR   , 1) \
    MAKE_PIN( PA15, Timer1  , CH1   , 1) \
    MAKE_PIN( PA15, Timer1  , ETR   , 1) \
    MAKE_PIN( PB0 , Timer0  , CH2N  , 1) \
    MAKE_PIN( PB1 , Timer0  , CH3N  , 1) \
    MAKE_PIN( PB3 , Timer1  , CH2   , 1) \
    MAKE_PIN( PB10, Timer1  , CH3   , 1) \
    MAKE_PIN( PB11, Timer1  , CH4   , 1) \
    MAKE_PIN( PB12, Timer0  , BKIN  , 1) \
    MAKE_PIN( PB13, Timer0  , CH1N  , 1) \
    MAKE_PIN( PB14, Timer0  , CH2N  , 1) \
    MAKE_PIN( PB15, Timer0  , CH3N  , 1) \
    MAKE_PIN( PE6 , Timer0  , BKIN2 , 1) \
    MAKE_PIN( PE7 , Timer0  , ETR   , 1) \
    MAKE_PIN( PE8 , Timer0  , CH1N  , 1) \
    MAKE_PIN( PE9 , Timer0  , CH1   , 1) \
    MAKE_PIN( PE10, Timer0  , CH2N  , 1) \
    MAKE_PIN( PE11, Timer0  , CH2   , 1) \
    MAKE_PIN( PE12, Timer0  , CH3N  , 1) \
    MAKE_PIN( PE13, Timer0  , CH3   , 1) \
    MAKE_PIN( PE14, Timer0  , CH4   , 1) \
    MAKE_PIN( PE15, Timer0  , BKIN  , 1) \
    MAKE_PIN( PA0 , Timer4  , CH1   , 2) \
    MAKE_PIN( PA1 , Timer4  , CH2   , 2) \
    MAKE_PIN( PA2 , Timer4  , CH3   , 2) \
    MAKE_PIN( PA3 , Timer4  , CH4   , 2) \
    MAKE_PIN( PA6 , Timer3  , CH1   , 2) \
    MAKE_PIN( PA7 , Timer3  , CH2   , 2) \
    MAKE_PIN( PB0 , Timer3  , CH3   , 2) \
    MAKE_PIN( PB1 , Timer3  , CH4   , 2) \
    MAKE_PIN( PB4 , Timer3  , CH1   , 2) \
    MAKE_PIN( PB5 , Timer3  , CH2   , 2) \
    MAKE_PIN( PB6 , Timer3  , CH1   , 2) \
    MAKE_PIN( PB7 , Timer3  , CH2   , 2) \
    MAKE_PIN( PB8 , Timer3  , CH3   , 2) \
    MAKE_PIN( PB9 , Timer3  , CH4   , 2) \
    MAKE_PIN( PC6 , Timer7  , CH1   , 2) \
    MAKE_PIN( PC7 , Timer7  , CH2   , 2) \
    MAKE_PIN( PC8 , Timer7  , CH3   , 2) \
    MAKE_PIN( PC9 , Timer7  , CH4   , 2) \
    MAKE_PIN( PD2 , Timer3  , ETR   , 2) \
    MAKE_PIN( PD12, Timer3  , CH1   , 2) \
    MAKE_PIN( PD13, Timer3  , CH2   , 2) \
    MAKE_PIN( PD14, Timer3  , CH3   , 2) \
    MAKE_PIN( PD15, Timer3  , CH4   , 2) \
    MAKE_PIN( PE0 , Timer3  , ETR   , 2) \
    MAKE_PIN( PH10, Timer4  , CH1   , 2) \
    MAKE_PIN( PH11, Timer4  , CH2   , 2) \
    MAKE_PIN( PH12, Timer4  , CH3   , 2) \
    MAKE_PIN( PI0 , Timer4  , CH4   , 2) \
    MAKE_PIN( PA0 , Timer7  , ETR   , 3) \
    MAKE_PIN( PA2 , Timer8  , CH1   , 3) \
    MAKE_PIN( PA3 , Timer8  , CH2   , 3) \
    MAKE_PIN( PA5 , Timer7  , CH1N  , 3) \
    MAKE_PIN( PA6 , Timer7  , BKIN  , 3) \
    MAKE_PIN( PA7 , Timer7  , CH1N  , 3) \
    MAKE_PIN( PA8 , Timer7  , BKIN2 , 3) \
    MAKE_PIN( PB0 , Timer7  , CH2N  , 3) \
    MAKE_PIN( PB1 , Timer7  , CH3N  , 3) \
    MAKE_PIN( PB8 , Timer9  , CH1   , 3) \
    MAKE_PIN( PB9 , Timer10 , CH1   , 3) \
    MAKE_PIN( PB14, Timer7  , CH2N  , 3) \
    MAKE_PIN( PB15, Timer7  , CH3N  , 3) \
    MAKE_PIN( PC6 , Timer7  , CH1   , 3) \
    MAKE_PIN( PC7 , Timer7  , CH2   , 3) \
    MAKE_PIN( PC8 , Timer7  , CH3   , 3) \
    MAKE_PIN( PC9 , Timer7  , CH4   , 3) \
    MAKE_PIN( PD12, Timer14 , IN1   , 3) \
    MAKE_PIN( PD13, Timer14 , OUT   , 3) \
    MAKE_PIN( PE0 , Timer14 , ETR   , 3) \
    MAKE_PIN( PE1 , Timer14 , IN2   , 3) \
    MAKE_PIN( PE5 , Timer8  , CH1   , 3) \
    MAKE_PIN( PE6 , Timer8  , CH2   , 3) \
    MAKE_PIN( PF6 , Timer9  , CH1   , 3) \
    MAKE_PIN( PF7 , Timer10 , CH1   , 3) \
    MAKE_PIN( PG12, Timer14 , IN1   , 3) \
    MAKE_PIN( PG13, Timer14 , OUT   , 3) \
    MAKE_PIN( PG14, Timer14 , ETR   , 3) \
    MAKE_PIN( PH3 , Timer14 , IN2   , 3) \
    MAKE_PIN( PH13, Timer7  , CH1N  , 3) \
    MAKE_PIN( PH14, Timer7  , CH2N  , 3) \
    MAKE_PIN( PH15, Timer7  , CH3N  , 3) \
    MAKE_PIN( PI1 , Timer7  , BKIN2 , 3) \
    MAKE_PIN( PI2 , Timer7  , CH4   , 3) \
    MAKE_PIN( PI3 , Timer7  , ETR   , 3) \
    MAKE_PIN( PI4 , Timer7  , BKIN  , 3) \
    MAKE_PIN( PI5 , Timer7  , CH1   , 3) \
    MAKE_PIN( PI6 , Timer7  , CH2   , 3) \
    MAKE_PIN( PI7 , Timer7  , CH3   , 3) \

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        PINS OF THE FMC MODULE
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define oC_MODULE_PIN_FUNCTIONS_FMC(MAKE_PIN_FUNCTION)         \
    MAKE_PIN_FUNCTION(FMC_SDNWE)\
    MAKE_PIN_FUNCTION(FMC_SDCKE0)\
    MAKE_PIN_FUNCTION(FMC_SDCKE1)\
    MAKE_PIN_FUNCTION(FMC_SDNE0)\
    MAKE_PIN_FUNCTION(FMC_SDNE1)\
    MAKE_PIN_FUNCTION(FMC_SDNRAS)\
    MAKE_PIN_FUNCTION(FMC_SDNCAS)\
    MAKE_PIN_FUNCTION(FMC_NE1)\
    MAKE_PIN_FUNCTION(FMC_NL)\
    MAKE_PIN_FUNCTION(FMC_SDCLK)\
    MAKE_PIN_FUNCTION(FMC_CLK)\
    MAKE_PIN_FUNCTION(FMC_CLE)\
    MAKE_PIN_FUNCTION(FMC_ALE)\
    MAKE_PIN_FUNCTION(FMC_D0)\
    MAKE_PIN_FUNCTION(FMC_D1)\
    MAKE_PIN_FUNCTION(FMC_D2)\
    MAKE_PIN_FUNCTION(FMC_D3)\
    MAKE_PIN_FUNCTION(FMC_D4)\
    MAKE_PIN_FUNCTION(FMC_D5)\
    MAKE_PIN_FUNCTION(FMC_D6)\
    MAKE_PIN_FUNCTION(FMC_D7)\
    MAKE_PIN_FUNCTION(FMC_D8)\
    MAKE_PIN_FUNCTION(FMC_D9)\
    MAKE_PIN_FUNCTION(FMC_D10)\
    MAKE_PIN_FUNCTION(FMC_D11)\
    MAKE_PIN_FUNCTION(FMC_D12)\
    MAKE_PIN_FUNCTION(FMC_D13)\
    MAKE_PIN_FUNCTION(FMC_D14)\
    MAKE_PIN_FUNCTION(FMC_D15)\
    MAKE_PIN_FUNCTION(FMC_D16)\
    MAKE_PIN_FUNCTION(FMC_D17)\
    MAKE_PIN_FUNCTION(FMC_D18)\
    MAKE_PIN_FUNCTION(FMC_D19)\
    MAKE_PIN_FUNCTION(FMC_D20)\
    MAKE_PIN_FUNCTION(FMC_D21)\
    MAKE_PIN_FUNCTION(FMC_D22)\
    MAKE_PIN_FUNCTION(FMC_D23)\
    MAKE_PIN_FUNCTION(FMC_D24)\
    MAKE_PIN_FUNCTION(FMC_D25)\
    MAKE_PIN_FUNCTION(FMC_D26)\
    MAKE_PIN_FUNCTION(FMC_D27)\
    MAKE_PIN_FUNCTION(FMC_D28)\
    MAKE_PIN_FUNCTION(FMC_D29)\
    MAKE_PIN_FUNCTION(FMC_D30)\
    MAKE_PIN_FUNCTION(FMC_D31)\
    MAKE_PIN_FUNCTION(FMC_NOE)\
    MAKE_PIN_FUNCTION(FMC_NWE)\
    MAKE_PIN_FUNCTION(FMC_NCE)\
    MAKE_PIN_FUNCTION(FMC_NWAIT)\
    MAKE_PIN_FUNCTION(FMC_A0)\
    MAKE_PIN_FUNCTION(FMC_A1)\
    MAKE_PIN_FUNCTION(FMC_A2)\
    MAKE_PIN_FUNCTION(FMC_A3)\
    MAKE_PIN_FUNCTION(FMC_A4)\
    MAKE_PIN_FUNCTION(FMC_A5)\
    MAKE_PIN_FUNCTION(FMC_A6)\
    MAKE_PIN_FUNCTION(FMC_A7)\
    MAKE_PIN_FUNCTION(FMC_A8)\
    MAKE_PIN_FUNCTION(FMC_A9)\
    MAKE_PIN_FUNCTION(FMC_A10)\
    MAKE_PIN_FUNCTION(FMC_A11)\
    MAKE_PIN_FUNCTION(FMC_A12)\
    MAKE_PIN_FUNCTION(FMC_A13)\
    MAKE_PIN_FUNCTION(FMC_A14)\
    MAKE_PIN_FUNCTION(FMC_A15)\
    MAKE_PIN_FUNCTION(FMC_A16)\
    MAKE_PIN_FUNCTION(FMC_A17)\
    MAKE_PIN_FUNCTION(FMC_A18)\
    MAKE_PIN_FUNCTION(FMC_A19)\
    MAKE_PIN_FUNCTION(FMC_A20)\
    MAKE_PIN_FUNCTION(FMC_A21)\
    MAKE_PIN_FUNCTION(FMC_A22)\
    MAKE_PIN_FUNCTION(FMC_A23)\
    MAKE_PIN_FUNCTION(FMC_A24)\
    MAKE_PIN_FUNCTION(FMC_A25)\
    MAKE_PIN_FUNCTION(FMC_NBL0)\
    MAKE_PIN_FUNCTION(FMC_NBL1)\
    MAKE_PIN_FUNCTION(FMC_BA0)\
    MAKE_PIN_FUNCTION(FMC_BA1)\
    MAKE_PIN_FUNCTION(FMC_INT)\
    MAKE_PIN_FUNCTION(FMC_NE2)\
    MAKE_PIN_FUNCTION(FMC_NE3)\
    MAKE_PIN_FUNCTION(FMC_NE4)\
    MAKE_PIN_FUNCTION(FMC_NBL2)\
    MAKE_PIN_FUNCTION(FMC_NBL3)\

#define oC_MODULE_PINS_FMC(MAKE_PIN)       \
    MAKE_PIN( PA7 , FMC , FMC_SDNWE  , 12 ) \
    MAKE_PIN( PB5 , FMC , FMC_SDCKE1 , 12 ) \
    MAKE_PIN( PB6 , FMC , FMC_SDNE1  , 12 ) \
    MAKE_PIN( PB7 , FMC , FMC_NL     , 12 ) \
    MAKE_PIN( PC0 , FMC , FMC_SDNWE  , 12 ) \
    MAKE_PIN( PC2 , FMC , FMC_SDNE0  , 12 ) \
    MAKE_PIN( PC3 , FMC , FMC_SDCKE0 , 12 ) \
    MAKE_PIN( PC4 , FMC , FMC_SDNE0  , 12 ) \
    MAKE_PIN( PC5 , FMC , FMC_SDCKE0 , 12 ) \
    MAKE_PIN( PD0 , FMC , FMC_D2     , 12 ) \
    MAKE_PIN( PD1 , FMC , FMC_D3     , 12 ) \
    MAKE_PIN( PD3 , FMC , FMC_CLK    , 12 ) \
    MAKE_PIN( PD4 , FMC , FMC_NOE    , 12 ) \
    MAKE_PIN( PD5 , FMC , FMC_NWE    , 12 ) \
    MAKE_PIN( PD6 , FMC , FMC_NWAIT  , 12 ) \
    MAKE_PIN( PD7 , FMC , FMC_NE1    , 12 ) \
    MAKE_PIN( PD8 , FMC , FMC_D13    , 12 ) \
    MAKE_PIN( PD9 , FMC , FMC_D14    , 12 ) \
    MAKE_PIN( PD10, FMC , FMC_D15    , 12 ) \
    MAKE_PIN( PD11, FMC , FMC_A16    , 12 ) \
    MAKE_PIN( PD11, FMC , FMC_CLE    , 12 ) \
    MAKE_PIN( PD12, FMC , FMC_A17    , 12 ) \
    MAKE_PIN( PD12, FMC , FMC_ALE    , 12 ) \
    MAKE_PIN( PD13, FMC , FMC_A18    , 12 ) \
    MAKE_PIN( PD14, FMC , FMC_D0     , 12 ) \
    MAKE_PIN( PD15, FMC , FMC_D1     , 12 ) \
    MAKE_PIN( PE0 , FMC , FMC_NBL0   , 12 ) \
    MAKE_PIN( PE1 , FMC , FMC_NBL1   , 12 ) \
    MAKE_PIN( PE2 , FMC , FMC_A23    , 12 ) \
    MAKE_PIN( PE3 , FMC , FMC_A19    , 12 ) \
    MAKE_PIN( PE4 , FMC , FMC_A20    , 12 ) \
    MAKE_PIN( PE5 , FMC , FMC_A21    , 12 ) \
    MAKE_PIN( PE6 , FMC , FMC_A22    , 12 ) \
    MAKE_PIN( PE7 , FMC , FMC_D4     , 12 ) \
    MAKE_PIN( PE8 , FMC , FMC_D5     , 12 ) \
    MAKE_PIN( PE9 , FMC , FMC_D6     , 12 ) \
    MAKE_PIN( PE10, FMC , FMC_D7     , 12 ) \
    MAKE_PIN( PE11, FMC , FMC_D8     , 12 ) \
    MAKE_PIN( PE12, FMC , FMC_D9     , 12 ) \
    MAKE_PIN( PE13, FMC , FMC_D10    , 12 ) \
    MAKE_PIN( PE14, FMC , FMC_D11    , 12 ) \
    MAKE_PIN( PE15, FMC , FMC_D12    , 12 ) \
    MAKE_PIN( PF0 , FMC , FMC_A0     , 12 ) \
    MAKE_PIN( PF1 , FMC , FMC_A1     , 12 ) \
    MAKE_PIN( PF2 , FMC , FMC_A2     , 12 ) \
    MAKE_PIN( PF3 , FMC , FMC_A3     , 12 ) \
    MAKE_PIN( PF4 , FMC , FMC_A4     , 12 ) \
    MAKE_PIN( PF5 , FMC , FMC_A5     , 12 ) \
    MAKE_PIN( PF11, FMC , FMC_SDNRAS , 12 ) \
    MAKE_PIN( PF12, FMC , FMC_A6     , 12 ) \
    MAKE_PIN( PF13, FMC , FMC_A7     , 12 ) \
    MAKE_PIN( PF14, FMC , FMC_A8     , 12 ) \
    MAKE_PIN( PF15, FMC , FMC_A9     , 12 ) \
    MAKE_PIN( PG0 , FMC , FMC_A10    , 12 ) \
    MAKE_PIN( PG1 , FMC , FMC_A11    , 12 ) \
    MAKE_PIN( PG2 , FMC , FMC_A12    , 12 ) \
    MAKE_PIN( PG3 , FMC , FMC_A13    , 12 ) \
    MAKE_PIN( PG4 , FMC , FMC_A14    , 12 ) \
    MAKE_PIN( PG4 , FMC , FMC_BA0    , 12 ) \
    MAKE_PIN( PG5 , FMC , FMC_A15    , 12 ) \
    MAKE_PIN( PG5 , FMC , FMC_BA1    , 12 ) \
    MAKE_PIN( PG7 , FMC , FMC_INT    , 12 ) \
    MAKE_PIN( PG8 , FMC , FMC_SDCLK  , 12 ) \
    MAKE_PIN( PG9 , FMC , FMC_NE2    , 12 ) \
    MAKE_PIN( PG9 , FMC , FMC_NCE    , 12 ) \
    MAKE_PIN( PG10, FMC , FMC_NE3    , 12 ) \
    MAKE_PIN( PG12, FMC , FMC_NE4    , 12 ) \
    MAKE_PIN( PG13, FMC , FMC_A24    , 12 ) \
    MAKE_PIN( PG14, FMC , FMC_A25    , 12 ) \
    MAKE_PIN( PG15, FMC , FMC_SDNCAS , 12 ) \
    MAKE_PIN( PH2 , FMC , FMC_SDCKE0 , 12 ) \
    MAKE_PIN( PH3 , FMC , FMC_SDNE0  , 12 ) \
    MAKE_PIN( PH5 , FMC , FMC_SDNWE  , 12 ) \
    MAKE_PIN( PH6 , FMC , FMC_SDNE1  , 12 ) \
    MAKE_PIN( PH7 , FMC , FMC_SDCKE1 , 12 ) \
    MAKE_PIN( PH8 , FMC , FMC_D16    , 12 ) \
    MAKE_PIN( PH9 , FMC , FMC_D17    , 12 ) \
    MAKE_PIN( PH10, FMC , FMC_D18    , 12 ) \
    MAKE_PIN( PH11, FMC , FMC_D19    , 12 ) \
    MAKE_PIN( PH12, FMC , FMC_D20    , 12 ) \
    MAKE_PIN( PH13, FMC , FMC_D21    , 12 ) \
    MAKE_PIN( PH14, FMC , FMC_D22    , 12 ) \
    MAKE_PIN( PH15, FMC , FMC_D23    , 12 ) \
    MAKE_PIN( PI0 , FMC , FMC_D24    , 12 ) \
    MAKE_PIN( PI1 , FMC , FMC_D25    , 12 ) \
    MAKE_PIN( PI2 , FMC , FMC_D26    , 12 ) \
    MAKE_PIN( PI3 , FMC , FMC_D27    , 12 ) \
    MAKE_PIN( PI4 , FMC , FMC_NBL2   , 12 ) \
    MAKE_PIN( PI5 , FMC , FMC_NBL3   , 12 ) \
    MAKE_PIN( PI6 , FMC , FMC_D28    , 12 ) \
    MAKE_PIN( PI7 , FMC , FMC_D29    , 12 ) \
    MAKE_PIN( PI9 , FMC , FMC_D30    , 12 ) \
    MAKE_PIN( PI10, FMC , FMC_D31    , 12 ) \

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        PINS OF THE ETH MODULE
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//==========================================================================================================================================
/**
 * Pin functions for the ETH module.
 */
//==========================================================================================================================================
#define oC_MODULE_PIN_FUNCTIONS_ETH(MAKE_PIN_FUNCTION)         \
    MAKE_PIN_FUNCTION(ETH_MII_CRS)\
    MAKE_PIN_FUNCTION(ETH_MII_COL)\
    MAKE_PIN_FUNCTION(ETH_MII_RX_CLK)\
    MAKE_PIN_FUNCTION(ETH_MII_RX_DV)\
    MAKE_PIN_FUNCTION(ETH_MII_RXD0)\
    MAKE_PIN_FUNCTION(ETH_MII_RXD1)\
    MAKE_PIN_FUNCTION(ETH_MII_RXD2)\
    MAKE_PIN_FUNCTION(ETH_MII_RXD3)\
    MAKE_PIN_FUNCTION(ETH_MII_RX_ER)\
    MAKE_PIN_FUNCTION(ETH_MII_TX_EN)\
    MAKE_PIN_FUNCTION(ETH_MII_TXD0)\
    MAKE_PIN_FUNCTION(ETH_MII_TXD1)\
    MAKE_PIN_FUNCTION(ETH_MII_TXD2)\
    MAKE_PIN_FUNCTION(ETH_MII_TXD3)\
    MAKE_PIN_FUNCTION(ETH_MII_TX_CLK)\
    MAKE_PIN_FUNCTION(ETH_RMII_REF_CLK)\
    MAKE_PIN_FUNCTION(ETH_RMII_CRS_DV)\
    MAKE_PIN_FUNCTION(ETH_RMII_TX_EN)\
    MAKE_PIN_FUNCTION(ETH_RMII_RX_ER)\
    MAKE_PIN_FUNCTION(ETH_RMII_RXD0)\
    MAKE_PIN_FUNCTION(ETH_RMII_RXD1)\
    MAKE_PIN_FUNCTION(ETH_RMII_TXD0)\
    MAKE_PIN_FUNCTION(ETH_RMII_TXD1)\
    MAKE_PIN_FUNCTION(ETH_MDIO)\
    MAKE_PIN_FUNCTION(ETH_MDC)\
    MAKE_PIN_FUNCTION(ETH_PPS_OUT)\

//==========================================================================================================================================
/**
 * Pins of the ETH module.
 */
//==========================================================================================================================================
#define oC_MODULE_PINS_ETH(MAKE_PIN)       \
    MAKE_PIN( PA0  , ETH , ETH_MII_CRS           , 11 ) \
    MAKE_PIN( PA1  , ETH , ETH_MII_RX_CLK        , 11 ) \
    MAKE_PIN( PA1  , ETH , ETH_RMII_REF_CLK      , 11 ) \
    MAKE_PIN( PA2  , ETH , ETH_MDIO              , 11 ) \
    MAKE_PIN( PA3  , ETH , ETH_MII_COL           , 11 ) \
    MAKE_PIN( PA7  , ETH , ETH_MII_RX_DV         , 11 ) \
    MAKE_PIN( PA7  , ETH , ETH_RMII_CRS_DV       , 11 ) \
    MAKE_PIN( PB0  , ETH , ETH_MII_RXD2          , 11 ) \
    MAKE_PIN( PB1  , ETH , ETH_MII_RXD3          , 11 ) \
    MAKE_PIN( PB5  , ETH , ETH_PPS_OUT           , 11 ) \
    MAKE_PIN( PB8  , ETH , ETH_MII_TXD3          , 11 ) \
    MAKE_PIN( PB10 , ETH , ETH_MII_RX_ER         , 11 ) \
    MAKE_PIN( PB11 , ETH , ETH_MII_TX_EN         , 11 ) \
    MAKE_PIN( PB11 , ETH , ETH_RMII_TX_EN        , 11 ) \
    MAKE_PIN( PB12 , ETH , ETH_MII_TXD0          , 11 ) \
    MAKE_PIN( PB12 , ETH , ETH_RMII_TXD0         , 11 ) \
    MAKE_PIN( PB13 , ETH , ETH_MII_TXD1          , 11 ) \
    MAKE_PIN( PB13 , ETH , ETH_RMII_TXD1         , 11 ) \
    MAKE_PIN( PC1  , ETH , ETH_MDC               , 11 ) \
    MAKE_PIN( PC2  , ETH , ETH_MII_TXD2          , 11 ) \
    MAKE_PIN( PC3  , ETH , ETH_MII_TX_CLK        , 11 ) \
    MAKE_PIN( PC4  , ETH , ETH_MII_RXD0          , 11 ) \
    MAKE_PIN( PC4  , ETH , ETH_RMII_RXD0         , 11 ) \
    MAKE_PIN( PC5  , ETH , ETH_MII_RXD1          , 11 ) \
    MAKE_PIN( PC5  , ETH , ETH_RMII_RXD1         , 11 ) \
    MAKE_PIN( PE2  , ETH , ETH_MII_TXD3          , 11 ) \
    MAKE_PIN( PG8  , ETH , ETH_PPS_OUT           , 11 ) \
    MAKE_PIN( PG11 , ETH , ETH_MII_TX_EN         , 11 ) \
    MAKE_PIN( PG11 , ETH , ETH_RMII_TX_EN        , 11 ) \
    MAKE_PIN( PG13 , ETH , ETH_MII_TXD0          , 11 ) \
    MAKE_PIN( PG13 , ETH , ETH_RMII_TXD0         , 11 ) \
    MAKE_PIN( PG14 , ETH , ETH_MII_TXD1          , 11 ) \
    MAKE_PIN( PG14 , ETH , ETH_RMII_TXD1         , 11 ) \
    MAKE_PIN( PH2  , ETH , ETH_MII_CRS           , 11 ) \
    MAKE_PIN( PH3  , ETH , ETH_MII_COL           , 11 ) \
    MAKE_PIN( PH6  , ETH , ETH_MII_RXD2          , 11 ) \
    MAKE_PIN( PH7  , ETH , ETH_MII_RXD3          , 11 ) \
    MAKE_PIN( PI10 , ETH , ETH_MII_RX_ER         , 11 ) \
    MAKE_PIN( PG2  , ETH , ETH_RMII_RX_ER        , 11 ) \

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        PINS OF THE I2C MODULE
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define oC_MODULE_PIN_FUNCTIONS_I2C(MAKE_PIN_FUNCTION)         \
    MAKE_PIN_FUNCTION(SCL)\
    MAKE_PIN_FUNCTION(SMBA)\
    MAKE_PIN_FUNCTION(SDA)\

#define oC_MODULE_PINS_I2C(MAKE_PIN)       \
    MAKE_PIN( PA8  , I2C2 , SCL  , 4) \
    MAKE_PIN( PA9  , I2C2 , SMBA , 4) \
    MAKE_PIN( PB5  , I2C0 , SMBA , 4) \
    MAKE_PIN( PB6  , I2C0 , SCL  , 4) \
    MAKE_PIN( PB7  , I2C0 , SDA  , 4) \
    MAKE_PIN( PB8  , I2C0 , SCL  , 4) \
    MAKE_PIN( PB9  , I2C0 , SDA  , 4) \
    MAKE_PIN( PB10 , I2C1 , SCL  , 4) \
    MAKE_PIN( PB11 , I2C1 , SDA  , 4) \
    MAKE_PIN( PB12 , I2C1 , SMBA , 4) \
    MAKE_PIN( PC9  , I2C2 , SDA  , 4) \
    MAKE_PIN( PD11 , I2C3 , SMBA , 4) \
    MAKE_PIN( PD12 , I2C3 , SCL  , 4) \
    MAKE_PIN( PD13 , I2C3 , SDA  , 4) \
    MAKE_PIN( PF0  , I2C1 , SDA  , 4) \
    MAKE_PIN( PF1  , I2C1 , SCL  , 4) \
    MAKE_PIN( PF2  , I2C1 , SMBA , 4) \
    MAKE_PIN( PF13 , I2C3 , SMBA , 4) \
    MAKE_PIN( PF14 , I2C3 , SCL  , 4) \
    MAKE_PIN( PF15 , I2C3 , SDA  , 4) \
    MAKE_PIN( PH4  , I2C1 , SCL  , 4) \
    MAKE_PIN( PH5  , I2C1 , SDA  , 4) \
    MAKE_PIN( PH6  , I2C1 , SMBA , 4) \
    MAKE_PIN( PH7  , I2C2 , SCL  , 4) \
    MAKE_PIN( PH8  , I2C2 , SDA  , 4) \
    MAKE_PIN( PH9  , I2C2 , SMBA , 4) \
    MAKE_PIN( PH10 , I2C3 , SMBA , 4) \
    MAKE_PIN( PH11 , I2C3 , SCL  , 4) \
    MAKE_PIN( PH12 , I2C3 , SDA  , 4) \

// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        PINS OF THE SDMMC MODULE
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define oC_MODULE_PIN_FUNCTIONS_SDMMC(MAKE_PIN_FUNCTION)         \
    MAKE_PIN_FUNCTION(SDMMC_D0)\
    MAKE_PIN_FUNCTION(SDMMC_D1)\
    MAKE_PIN_FUNCTION(SDMMC_D2)\
    MAKE_PIN_FUNCTION(SDMMC_D3)\
    MAKE_PIN_FUNCTION(SDMMC_D4)\
    MAKE_PIN_FUNCTION(SDMMC_D5)\
    MAKE_PIN_FUNCTION(SDMMC_D6)\
    MAKE_PIN_FUNCTION(SDMMC_D7)\
    MAKE_PIN_FUNCTION(SDMMC_CK)\
    MAKE_PIN_FUNCTION(SDMMC_CMD)\

#define oC_MODULE_PINS_SDMMC(MAKE_PIN)       \
    MAKE_PIN( PB8  , SDMMC , SDMMC_D4 , 12 ) \
    MAKE_PIN( PB9  , SDMMC , SDMMC_D5 , 12 ) \
    MAKE_PIN( PC6  , SDMMC , SDMMC_D6 , 12 ) \
    MAKE_PIN( PC7  , SDMMC , SDMMC_D7 , 12 ) \
    MAKE_PIN( PC8  , SDMMC , SDMMC_D0 , 12 ) \
    MAKE_PIN( PC9  , SDMMC , SDMMC_D1 , 12 ) \
    MAKE_PIN( PC10 , SDMMC , SDMMC_D2 , 12 ) \
    MAKE_PIN( PC11 , SDMMC , SDMMC_D3 , 12 ) \
    MAKE_PIN( PC12 , SDMMC , SDMMC_CK , 12 ) \
    MAKE_PIN( PD2  , SDMMC , SDMMC_CMD, 12 ) \


// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 *                                                        LIST OF MODULES
 */
// /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


#define oC_MODULES_PINS_LIST(MAKE_MODULE)       \
    MAKE_MODULE(TIMER) \
    MAKE_MODULE(UART) \
    MAKE_MODULE(SPI) \
    MAKE_MODULE(LCDTFT) \
    MAKE_MODULE(FMC) \
    MAKE_MODULE(ETH) \
    MAKE_MODULE(I2C) \
    MAKE_MODULE(SDMMC) \

#endif /* SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_PINS_DEFS_H_ */
