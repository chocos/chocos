/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_machine_chdef.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_CHANNELS_DEFS_H_
#define SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_CHANNELS_DEFS_H_

#include <oc_1word.h>

#define oC_MODULE_CHANNELS_(MODULE_NAME)        oC_1WORD_FROM_2(oC_MODULE_CHANNELS_ , MODULE_NAME )

#define oC_CHANNEL_MASK_WIDTH                   9ULL

#define oC_MODULE_CHANNELS_FLASH(MAKE_CHANNEL)    \
    MAKE_CHANNEL( FLASH , FLASH , FLASH ) \

#define oC_MODULE_CHANNELS_PWR(MAKE_CHANNEL)    \
    MAKE_CHANNEL( PWR , PWR , PWR ) \

#define oC_MODULE_CHANNELS_RCC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( RCC , RCC , RCC ) \

#define oC_MODULE_CHANNELS_SYSCFG(MAKE_CHANNEL)    \
    MAKE_CHANNEL( SYSCFG , SYSCFG , SYSCFG ) \

#define oC_MODULE_CHANNELS_CRC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( CRC , CRC , CRC ) \

#define oC_MODULE_CHANNELS_FMC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( FMC , FMC , FMC ) \

#define oC_MODULE_CHANNELS_QUADSPI(MAKE_CHANNEL)    \
    MAKE_CHANNEL( QUADSPI , QUADSPI , QUADSPI ) \

#define oC_MODULE_CHANNELS_ADC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( ADC0 , ADC1 , ADC ) \
    MAKE_CHANNEL( ADC1 , ADC2 , ADC ) \
    MAKE_CHANNEL( ADC2 , ADC3 , ADC ) \

#define oC_MODULE_CHANNELS_DAC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( DAC , DAC , DAC ) \

#define oC_MODULE_CHANNELS_GPIO(MAKE_CHANNEL)    \
    MAKE_CHANNEL( PORTA , GPIOA , GPIO ) \
    MAKE_CHANNEL( PORTB , GPIOB , GPIO ) \
    MAKE_CHANNEL( PORTC , GPIOC , GPIO ) \
    MAKE_CHANNEL( PORTD , GPIOD , GPIO ) \
    MAKE_CHANNEL( PORTE , GPIOE , GPIO ) \
    MAKE_CHANNEL( PORTF , GPIOF , GPIO ) \
    MAKE_CHANNEL( PORTG , GPIOG , GPIO ) \
    MAKE_CHANNEL( PORTH , GPIOH , GPIO ) \
    MAKE_CHANNEL( PORTI , GPIOI , GPIO ) \
    MAKE_CHANNEL( PORTJ , GPIOJ , GPIO ) \
    MAKE_CHANNEL( PORTK , GPIOK , GPIO ) \

#define oC_MODULE_CHANNELS_DMA(MAKE_CHANNEL) \
    MAKE_CHANNEL( DMA0  , DMA1 , DMA , 0 , 0 ) \
    MAKE_CHANNEL( DMA1  , DMA1 , DMA , 1 , 0 ) \
    MAKE_CHANNEL( DMA2  , DMA1 , DMA , 2 , 0 ) \
    MAKE_CHANNEL( DMA3  , DMA1 , DMA , 3 , 0 ) \
    MAKE_CHANNEL( DMA4  , DMA1 , DMA , 4 , 0 ) \
    MAKE_CHANNEL( DMA5  , DMA1 , DMA , 5 , 0 ) \
    MAKE_CHANNEL( DMA6  , DMA1 , DMA , 6 , 0 ) \
    MAKE_CHANNEL( DMA7  , DMA1 , DMA , 7 , 0 ) \
    MAKE_CHANNEL( DMA8  , DMA1 , DMA , 0 , 1 ) \
    MAKE_CHANNEL( DMA9  , DMA1 , DMA , 1 , 1 ) \
    MAKE_CHANNEL( DMA10 , DMA1 , DMA , 2 , 1 ) \
    MAKE_CHANNEL( DMA11 , DMA1 , DMA , 3 , 1 ) \
    MAKE_CHANNEL( DMA12 , DMA1 , DMA , 4 , 1 ) \
    MAKE_CHANNEL( DMA13 , DMA1 , DMA , 5 , 1 ) \
    MAKE_CHANNEL( DMA14 , DMA1 , DMA , 6 , 1 ) \
    MAKE_CHANNEL( DMA15 , DMA1 , DMA , 7 , 1 ) \
    MAKE_CHANNEL( DMA16 , DMA1 , DMA , 0 , 2 ) \
    MAKE_CHANNEL( DMA17 , DMA1 , DMA , 1 , 2 ) \
    MAKE_CHANNEL( DMA18 , DMA1 , DMA , 2 , 2 ) \
    MAKE_CHANNEL( DMA19 , DMA1 , DMA , 3 , 2 ) \
    MAKE_CHANNEL( DMA20 , DMA1 , DMA , 4 , 2 ) \
    MAKE_CHANNEL( DMA21 , DMA1 , DMA , 5 , 2 ) \
    MAKE_CHANNEL( DMA22 , DMA1 , DMA , 6 , 2 ) \
    MAKE_CHANNEL( DMA23 , DMA1 , DMA , 7 , 2 ) \
    MAKE_CHANNEL( DMA24 , DMA1 , DMA , 0 , 3 ) \
    MAKE_CHANNEL( DMA25 , DMA1 , DMA , 1 , 3 ) \
    MAKE_CHANNEL( DMA26 , DMA1 , DMA , 2 , 3 ) \
    MAKE_CHANNEL( DMA27 , DMA1 , DMA , 3 , 3 ) \
    MAKE_CHANNEL( DMA28 , DMA1 , DMA , 4 , 3 ) \
    MAKE_CHANNEL( DMA29 , DMA1 , DMA , 5 , 3 ) \
    MAKE_CHANNEL( DMA30 , DMA1 , DMA , 6 , 3 ) \
    MAKE_CHANNEL( DMA31 , DMA1 , DMA , 7 , 3 ) \
    MAKE_CHANNEL( DMA32 , DMA1 , DMA , 0 , 4 ) \
    MAKE_CHANNEL( DMA33 , DMA1 , DMA , 1 , 4 ) \
    MAKE_CHANNEL( DMA34 , DMA1 , DMA , 2 , 4 ) \
    MAKE_CHANNEL( DMA35 , DMA1 , DMA , 3 , 4 ) \
    MAKE_CHANNEL( DMA36 , DMA1 , DMA , 4 , 4 ) \
    MAKE_CHANNEL( DMA37 , DMA1 , DMA , 5 , 4 ) \
    MAKE_CHANNEL( DMA38 , DMA1 , DMA , 6 , 4 ) \
    MAKE_CHANNEL( DMA39 , DMA1 , DMA , 7 , 4 ) \
    MAKE_CHANNEL( DMA40 , DMA1 , DMA , 0 , 5 ) \
    MAKE_CHANNEL( DMA41 , DMA1 , DMA , 1 , 5 ) \
    MAKE_CHANNEL( DMA42 , DMA1 , DMA , 2 , 5 ) \
    MAKE_CHANNEL( DMA43 , DMA1 , DMA , 3 , 5 ) \
    MAKE_CHANNEL( DMA44 , DMA1 , DMA , 4 , 5 ) \
    MAKE_CHANNEL( DMA45 , DMA1 , DMA , 5 , 5 ) \
    MAKE_CHANNEL( DMA46 , DMA1 , DMA , 6 , 5 ) \
    MAKE_CHANNEL( DMA47 , DMA1 , DMA , 7 , 5 ) \
    MAKE_CHANNEL( DMA48 , DMA1 , DMA , 0 , 6 ) \
    MAKE_CHANNEL( DMA49 , DMA1 , DMA , 1 , 6 ) \
    MAKE_CHANNEL( DMA50 , DMA1 , DMA , 2 , 6 ) \
    MAKE_CHANNEL( DMA51 , DMA1 , DMA , 3 , 6 ) \
    MAKE_CHANNEL( DMA52 , DMA1 , DMA , 4 , 6 ) \
    MAKE_CHANNEL( DMA53 , DMA1 , DMA , 5 , 6 ) \
    MAKE_CHANNEL( DMA54 , DMA1 , DMA , 6 , 6 ) \
    MAKE_CHANNEL( DMA55 , DMA1 , DMA , 7 , 6 ) \
    MAKE_CHANNEL( DMA56 , DMA1 , DMA , 0 , 7 ) \
    MAKE_CHANNEL( DMA57 , DMA1 , DMA , 1 , 7 ) \
    MAKE_CHANNEL( DMA58 , DMA1 , DMA , 2 , 7 ) \
    MAKE_CHANNEL( DMA59 , DMA1 , DMA , 3 , 7 ) \
    MAKE_CHANNEL( DMA60 , DMA1 , DMA , 4 , 7 ) \
    MAKE_CHANNEL( DMA61 , DMA1 , DMA , 5 , 7 ) \
    MAKE_CHANNEL( DMA62 , DMA1 , DMA , 6 , 7 ) \
    MAKE_CHANNEL( DMA63 , DMA1 , DMA , 7 , 7 ) \
    MAKE_CHANNEL( DMA64 , DMA2 , DMA , 0 , 0 ) \
    MAKE_CHANNEL( DMA65 , DMA2 , DMA , 1 , 0 ) \
    MAKE_CHANNEL( DMA66 , DMA2 , DMA , 2 , 0 ) \
    MAKE_CHANNEL( DMA67 , DMA2 , DMA , 3 , 0 ) \
    MAKE_CHANNEL( DMA68 , DMA2 , DMA , 4 , 0 ) \
    MAKE_CHANNEL( DMA69 , DMA2 , DMA , 5 , 0 ) \
    MAKE_CHANNEL( DMA70 , DMA2 , DMA , 6 , 0 ) \
    MAKE_CHANNEL( DMA71 , DMA2 , DMA , 7 , 0 ) \
    MAKE_CHANNEL( DMA72 , DMA2 , DMA , 0 , 1 ) \
    MAKE_CHANNEL( DMA73 , DMA2 , DMA , 1 , 1 ) \
    MAKE_CHANNEL( DMA74 , DMA2 , DMA , 2 , 1 ) \
    MAKE_CHANNEL( DMA75 , DMA2 , DMA , 3 , 1 ) \
    MAKE_CHANNEL( DMA76 , DMA2 , DMA , 4 , 1 ) \
    MAKE_CHANNEL( DMA77 , DMA2 , DMA , 5 , 1 ) \
    MAKE_CHANNEL( DMA78 , DMA2 , DMA , 6 , 1 ) \
    MAKE_CHANNEL( DMA79 , DMA2 , DMA , 7 , 1 ) \
    MAKE_CHANNEL( DMA80 , DMA2 , DMA , 0 , 2 ) \
    MAKE_CHANNEL( DMA81 , DMA2 , DMA , 1 , 2 ) \
    MAKE_CHANNEL( DMA82 , DMA2 , DMA , 2 , 2 ) \
    MAKE_CHANNEL( DMA83 , DMA2 , DMA , 3 , 2 ) \
    MAKE_CHANNEL( DMA84 , DMA2 , DMA , 4 , 2 ) \
    MAKE_CHANNEL( DMA85 , DMA2 , DMA , 5 , 2 ) \
    MAKE_CHANNEL( DMA86 , DMA2 , DMA , 6 , 2 ) \
    MAKE_CHANNEL( DMA87 , DMA2 , DMA , 7 , 2 ) \
    MAKE_CHANNEL( DMA88 , DMA2 , DMA , 0 , 3 ) \
    MAKE_CHANNEL( DMA89 , DMA2 , DMA , 1 , 3 ) \
    MAKE_CHANNEL( DMA90 , DMA2 , DMA , 2 , 3 ) \
    MAKE_CHANNEL( DMA91 , DMA2 , DMA , 3 , 3 ) \
    MAKE_CHANNEL( DMA92 , DMA2 , DMA , 4 , 3 ) \
    MAKE_CHANNEL( DMA93 , DMA2 , DMA , 5 , 3 ) \
    MAKE_CHANNEL( DMA94 , DMA2 , DMA , 6 , 3 ) \
    MAKE_CHANNEL( DMA95 , DMA2 , DMA , 7 , 3 ) \
    MAKE_CHANNEL( DMA96 , DMA2 , DMA , 0 , 4 ) \
    MAKE_CHANNEL( DMA97 , DMA2 , DMA , 1 , 4 ) \
    MAKE_CHANNEL( DMA98 , DMA2 , DMA , 2 , 4 ) \
    MAKE_CHANNEL( DMA99 , DMA2 , DMA , 3 , 4 ) \
    MAKE_CHANNEL( DMA100, DMA2 , DMA , 4 , 4 ) \
    MAKE_CHANNEL( DMA101, DMA2 , DMA , 5 , 4 ) \
    MAKE_CHANNEL( DMA102, DMA2 , DMA , 6 , 4 ) \
    MAKE_CHANNEL( DMA103, DMA2 , DMA , 7 , 4 ) \
    MAKE_CHANNEL( DMA104, DMA2 , DMA , 0 , 5 ) \
    MAKE_CHANNEL( DMA105, DMA2 , DMA , 1 , 5 ) \
    MAKE_CHANNEL( DMA106, DMA2 , DMA , 2 , 5 ) \
    MAKE_CHANNEL( DMA107, DMA2 , DMA , 3 , 5 ) \
    MAKE_CHANNEL( DMA108, DMA2 , DMA , 4 , 5 ) \
    MAKE_CHANNEL( DMA109, DMA2 , DMA , 5 , 5 ) \
    MAKE_CHANNEL( DMA110, DMA2 , DMA , 6 , 5 ) \
    MAKE_CHANNEL( DMA111, DMA2 , DMA , 7 , 5 ) \
    MAKE_CHANNEL( DMA112, DMA2 , DMA , 0 , 6 ) \
    MAKE_CHANNEL( DMA113, DMA2 , DMA , 1 , 6 ) \
    MAKE_CHANNEL( DMA114, DMA2 , DMA , 2 , 6 ) \
    MAKE_CHANNEL( DMA115, DMA2 , DMA , 3 , 6 ) \
    MAKE_CHANNEL( DMA116, DMA2 , DMA , 4 , 6 ) \
    MAKE_CHANNEL( DMA117, DMA2 , DMA , 5 , 6 ) \
    MAKE_CHANNEL( DMA118, DMA2 , DMA , 6 , 6 ) \
    MAKE_CHANNEL( DMA119, DMA2 , DMA , 7 , 6 ) \
    MAKE_CHANNEL( DMA120, DMA2 , DMA , 0 , 7 ) \
    MAKE_CHANNEL( DMA121, DMA2 , DMA , 1 , 7 ) \
    MAKE_CHANNEL( DMA122, DMA2 , DMA , 2 , 7 ) \
    MAKE_CHANNEL( DMA123, DMA2 , DMA , 3 , 7 ) \
    MAKE_CHANNEL( DMA124, DMA2 , DMA , 4 , 7 ) \
    MAKE_CHANNEL( DMA125, DMA2 , DMA , 5 , 7 ) \
    MAKE_CHANNEL( DMA126, DMA2 , DMA , 6 , 7 ) \
    MAKE_CHANNEL( DMA127, DMA2 , DMA , 7 , 7 ) \

#define oC_MODULE_CHANNELS_DMA2D(MAKE_CHANNEL)  \
    MAKE_CHANNEL( DMA2D, DMA2D , DMA2D ) \

#define oC_MODULE_CHANNELS_EXTI(MAKE_CHANNEL)  \
    MAKE_CHANNEL( EXTI , EXTI , EXTI ) \

#define oC_MODULE_CHANNELS_DCMI(MAKE_CHANNEL)    \
    MAKE_CHANNEL( DCMI , DCMI , DCMI ) \

#define oC_MODULE_CHANNELS_LCDTFT(MAKE_CHANNEL)    \
    MAKE_CHANNEL( LCD , LCD_TFT , LTDC ) \

#define oC_MODULE_CHANNELS_RNG(MAKE_CHANNEL)    \
    MAKE_CHANNEL( RNG , RNG , RNG ) \

#define oC_MODULE_CHANNELS_CRYP(MAKE_CHANNEL)    \
    MAKE_CHANNEL( CRYP , CRYP , CRYP ) \

#define oC_MODULE_CHANNELS_HASH(MAKE_CHANNEL)    \
    MAKE_CHANNEL( HASH , HASH , HASH ) \

#define oC_MODULE_CHANNELS_TIMER(MAKE_CHANNEL)    \
    MAKE_CHANNEL( Timer0  , TIM1  , TIMx ) \
    MAKE_CHANNEL( Timer1  , TIM2  , TIMx ) \
    MAKE_CHANNEL( Timer2  , TIM3  , TIMx ) \
    MAKE_CHANNEL( Timer3  , TIM4  , TIMx ) \
    MAKE_CHANNEL( Timer4  , TIM5  , TIMx ) \
    MAKE_CHANNEL( Timer5  , TIM6  , TIMx ) \
    MAKE_CHANNEL( Timer6  , TIM7  , TIMx ) \
    MAKE_CHANNEL( Timer7  , TIM8  , TIMx ) \
    MAKE_CHANNEL( Timer8  , TIM9  , TIMx ) \
    MAKE_CHANNEL( Timer9  , TIM10 , TIMx ) \
    MAKE_CHANNEL( Timer10 , TIM11 , TIMx ) \
    MAKE_CHANNEL( Timer11 , TIM12 , TIMx ) \
    MAKE_CHANNEL( Timer12 , TIM13 , TIMx ) \
    MAKE_CHANNEL( Timer13 , TIM14 , TIMx ) \
    MAKE_CHANNEL( Timer14 , LPTIM1, LPTIMx ) \

#define oC_MODULE_CHANNELS_WDG(MAKE_CHANNEL)    \
    MAKE_CHANNEL( IWDG , IWDG , IWDG ) \
    MAKE_CHANNEL( WWDG , WWDG , WWDG ) \

#define oC_MODULE_CHANNELS_RTC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( RTC , RTC_BKP , RTC ) \

#define oC_MODULE_CHANNELS_I2C(MAKE_CHANNEL)    \
    MAKE_CHANNEL( I2C0 , I2C1 , I2C ) \
    MAKE_CHANNEL( I2C1 , I2C2 , I2C ) \
    MAKE_CHANNEL( I2C2 , I2C3 , I2C ) \
    MAKE_CHANNEL( I2C3 , I2C4 , I2C ) \

#define oC_MODULE_CHANNELS_UART(MAKE_CHANNEL)    \
    MAKE_CHANNEL( UART0 , USART1 , USART ) \
    MAKE_CHANNEL( UART1 , USART2 , USART ) \
    MAKE_CHANNEL( UART2 , USART3 , USART ) \
    MAKE_CHANNEL( UART3 , UART4  , USART ) \
    MAKE_CHANNEL( UART4 , UART5  , USART ) \
    MAKE_CHANNEL( UART5 , USART6 , USART ) \
    MAKE_CHANNEL( UART6 , UART7  , USART ) \
    MAKE_CHANNEL( UART7 , UART8  , USART ) \

#define oC_MODULE_CHANNELS_SPI(MAKE_CHANNEL)    \
    MAKE_CHANNEL( SPI0 , SPI1 , SPI ) \
    MAKE_CHANNEL( SPI1 , SPI2 , SPI ) \
    MAKE_CHANNEL( SPI2 , SPI3 , SPI ) \
    MAKE_CHANNEL( SPI3 , SPI4 , SPI ) \
    MAKE_CHANNEL( SPI4 , SPI5 , SPI ) \
    MAKE_CHANNEL( SPI5 , SPI6 , SPI ) \

#define oC_MODULE_CHANNELS_SAI(MAKE_CHANNEL)    \
    MAKE_CHANNEL( SAI0 , SAI1 , SAI ) \
    MAKE_CHANNEL( SAI1 , SAI2 , SAI ) \

#define oC_MODULE_CHANNELS_SPDIFRX(MAKE_CHANNEL)    \
    MAKE_CHANNEL( SPDIFRX , SPDIFRX , SPDIFRX ) \

#define oC_MODULE_CHANNELS_SDMMC(MAKE_CHANNEL)    \
    MAKE_CHANNEL( SDMMC , SDMMC1 , SDMMC ) \

#define oC_MODULE_CHANNELS_CAN(MAKE_CHANNEL)    \
    MAKE_CHANNEL( CAN0 , CAN1 , CAN ) \
    MAKE_CHANNEL( CAN1 , CAN2 , CAN ) \

#define oC_MODULE_CHANNELS_USB(MAKE_CHANNEL)    \
    MAKE_CHANNEL( USB_FS , USB_OTG_FS , OTG ) \
    MAKE_CHANNEL( USB_HS , USB_OTG_HS , OTG ) \

#define oC_MODULE_CHANNELS_ETH(MAKE_CHANNEL)    \
    MAKE_CHANNEL( ETH , ETHERNET_MAC , ETH ) \

#define oC_MODULE_CHANNELS_HDMI(MAKE_CHANNEL)    \
    MAKE_CHANNEL( HDMI , HDMI_CEC , HDMI ) \

#define oC_MODULES_LIST(MAKE_MODULE_CHANNELS)       \
    MAKE_MODULE_CHANNELS( FLASH ) \
    MAKE_MODULE_CHANNELS( PWR ) \
    MAKE_MODULE_CHANNELS( RCC ) \
    MAKE_MODULE_CHANNELS( SYSCFG ) \
    MAKE_MODULE_CHANNELS( CRC ) \
    MAKE_MODULE_CHANNELS( FMC ) \
    MAKE_MODULE_CHANNELS( QUADSPI ) \
    MAKE_MODULE_CHANNELS( ADC ) \
    MAKE_MODULE_CHANNELS( DAC ) \
    MAKE_MODULE_CHANNELS( GPIO ) \
    MAKE_MODULE_CHANNELS( DMA ) \
    MAKE_MODULE_CHANNELS( DMA2D ) \
    MAKE_MODULE_CHANNELS( EXTI ) \
    MAKE_MODULE_CHANNELS( DCMI ) \
    MAKE_MODULE_CHANNELS( LCDTFT ) \
    MAKE_MODULE_CHANNELS( RNG ) \
    MAKE_MODULE_CHANNELS( CRYP ) \
    MAKE_MODULE_CHANNELS( HASH ) \
    MAKE_MODULE_CHANNELS( TIMER ) \
    MAKE_MODULE_CHANNELS( WDG ) \
    MAKE_MODULE_CHANNELS( RTC ) \
    MAKE_MODULE_CHANNELS( I2C ) \
    MAKE_MODULE_CHANNELS( UART ) \
    MAKE_MODULE_CHANNELS( SPI ) \
    MAKE_MODULE_CHANNELS( SAI ) \
    MAKE_MODULE_CHANNELS( SDMMC ) \
    MAKE_MODULE_CHANNELS( CAN ) \
    MAKE_MODULE_CHANNELS( USB ) \
    MAKE_MODULE_CHANNELS( ETH ) \
    MAKE_MODULE_CHANNELS( HDMI ) \

#endif /* SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_CHANNELS_DEFS_H_ */
