/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions of bits in registers
 *
 * @file       oc_machine_regs.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_REGISTERS_DEFS_H_
#define SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_REGISTERS_DEFS_H_

#include <oc_1word.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of the register bits definition
 */
//==========================================================================================================================================
#define oC_REGISTER_(REGISTER_NAME)         oC_1WORD_FROM_2(oC_REGISTER_ , REGISTER_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FLASH_ACR(MAKE_BIT)     \
    MAKE_BIT( LATENCY        ,  4 ) \
    MAKE_BIT( _RESERVED4_7   ,  4 ) \
    MAKE_BIT( PRFTEN         ,  1 ) \
    MAKE_BIT( ARTEN          ,  1 ) \
    MAKE_BIT( _RESERVED10_10 ,  1 ) \
    MAKE_BIT( ARTRST         ,  1 ) \
    MAKE_BIT( _RESERVED12_31 , 20 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FLASH_KEYR(MAKE_BIT)     \
    MAKE_BIT( FKEYR          , 32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FLASH_OPTKEYR(MAKE_BIT)     \
    MAKE_BIT( OPTKEYR        , 32 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range oU*f reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FLASH_SR(MAKE_BIT)     \
    MAKE_BIT( EOP            ,  1 ) \
    MAKE_BIT( OPERR          ,  1 ) \
    MAKE_BIT( _RESERVED2_3   ,  2 ) \
    MAKE_BIT( WRPERR         ,  1 ) \
    MAKE_BIT( PGAERR         ,  1 ) \
    MAKE_BIT( PGPERR         ,  1 ) \
    MAKE_BIT( ERSERR         ,  1 ) \
    MAKE_BIT( _RESERVED8_15  ,  8 ) \
    MAKE_BIT( BSY            ,  1 ) \
    MAKE_BIT( _RESERVED17_31 , 15 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FLASH_CR(MAKE_BIT)     \
    MAKE_BIT( PG             ,  1 ) \
    MAKE_BIT( SER            ,  1 ) \
    MAKE_BIT( MER            ,  1 ) \
    MAKE_BIT( SNB            ,  4 ) \
    MAKE_BIT( _RESERVED7     ,  1 ) \
    MAKE_BIT( PSIZE          ,  2 ) \
    MAKE_BIT( _RESERVED10_15 ,  6 ) \
    MAKE_BIT( STRT           ,  1 ) \
    MAKE_BIT( _RESERVED17_23 ,  7 ) \
    MAKE_BIT( EOPIE          ,  1 ) \
    MAKE_BIT( ERRIE          ,  1 ) \
    MAKE_BIT( _RESERVED26_30 ,  5 ) \
    MAKE_BIT( LOCK           ,  1 ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FLASH_OPTCR(MAKE_BIT) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FLASH_OPTCR1(MAKE_BIT) \
    MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PWR_CR1(MAKE_BIT) \
   MAKE_BIT( LPDS           , 1 )\
   MAKE_BIT( PDDS           , 1 )\
   MAKE_BIT( _RESERVED2_2   , 1 )\
   MAKE_BIT( CSBF           , 1 )\
   MAKE_BIT( PVDE           , 1 )\
   MAKE_BIT( PLS            , 3 )\
   MAKE_BIT( DBP            , 1 )\
   MAKE_BIT( FPDS           , 1 )\
   MAKE_BIT( LPUDS          , 1 )\
   MAKE_BIT( MRUDS          , 1 )\
   MAKE_BIT( _RESERVED12_12 , 1 )\
   MAKE_BIT( ADCDC1         , 1 )\
   MAKE_BIT( VOS            , 2 )\
   MAKE_BIT( ODEN           , 1 )\
   MAKE_BIT( ODESWEN        , 1 )\
   MAKE_BIT( UDEN           , 2 )\
   MAKE_BIT( _RESERVED20_31 , 12 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remember to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PWR_CSR1(MAKE_BIT) \
   MAKE_BIT( WUIF           , 1 )\
   MAKE_BIT( SBF            , 1 )\
   MAKE_BIT( PVDO           , 1 )\
   MAKE_BIT( BRR            , 1 )\
   MAKE_BIT( _RESERVED4_8   , 5 )\
   MAKE_BIT( BRE            , 1 )\
   MAKE_BIT( _RESERVED10_13 , 4 )\
   MAKE_BIT( VOSRDY         , 1 )\
   MAKE_BIT( _RESERVED15_15 , 1 )\
   MAKE_BIT( ODRDY          , 1 )\
   MAKE_BIT( ODSWRDY        , 1 )\
   MAKE_BIT( UDRDY          , 2 )\
   MAKE_BIT( _RESERVED20_31 , 12 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PWR_CR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_PWR_CSR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_CR(MAKE_BIT) \
   MAKE_BIT( HSION                  , 1 )\
   MAKE_BIT( HSIRDY                 , 1 )\
   MAKE_BIT( _RESERVED2_2           , 1 )\
   MAKE_BIT( HSITRIM                , 5 )\
   MAKE_BIT( HSICAL                 , 8 )\
   MAKE_BIT( HSEON                  , 1 )\
   MAKE_BIT( HSERDY                 , 1 )\
   MAKE_BIT( HSEBYP                 , 1 )\
   MAKE_BIT( CSSON                  , 1 )\
   MAKE_BIT( _RESERVED20_23         , 4 )\
   MAKE_BIT( PLLON                  , 1 )\
   MAKE_BIT( PLLRDY                 , 1 )\
   MAKE_BIT( PLLI2SON               , 1 )\
   MAKE_BIT( PLLI2SRDY              , 1 )\
   MAKE_BIT( PLLSAION               , 1 )\
   MAKE_BIT( PLLSAIRDY              , 1 )\
   MAKE_BIT( _RESERVED30_31         , 2 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_PLLCFGR(MAKE_BIT) \
   MAKE_BIT( PLLM                       , 6 )\
   MAKE_BIT( PLLN                       , 9 )\
   MAKE_BIT( _RESERVED15_15             , 1 )\
   MAKE_BIT( PLLP                       , 2 )\
   MAKE_BIT( _RESERVED18_21             , 4 )\
   MAKE_BIT( PLLSRC                     , 1 )\
   MAKE_BIT( _RESERVED23_23             , 1 )\
   MAKE_BIT( PLLQ                       , 4 )\
   MAKE_BIT( _RESERVED28_31             , 4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_CFGR(MAKE_BIT) \
   MAKE_BIT( SW                 , 2 )\
   MAKE_BIT( SWS                , 2 )\
   MAKE_BIT( HPRE               , 4 )\
   MAKE_BIT( _RESERVED8_9       , 2 )\
   MAKE_BIT( PPRE               , 3 )\
   MAKE_BIT( PPRE2              , 3 )\
   MAKE_BIT( RTCPRE             , 5 )\
   MAKE_BIT( MCO1               , 2 )\
   MAKE_BIT( I2SSCR             , 1 )\
   MAKE_BIT( MCO1_PRE           , 3 )\
   MAKE_BIT( MCO2_PRE           , 3 )\
   MAKE_BIT( MCO2               , 2 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_CIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB1RSTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB2RSTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB3RSTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_APB1RSTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_APB2RSTR(MAKE_BIT) \
   MAKE_BIT( TIM1RST            , 1 )\
   MAKE_BIT( TIM8RST            , 1 )\
   MAKE_BIT( _RESERVED2_3       , 2 )\
   MAKE_BIT( USART1RST          , 1 )\
   MAKE_BIT( USART6RST          , 1 )\
   MAKE_BIT( _RESERVED6_7       , 2 )\
   MAKE_BIT( ADCRST             , 1 )\
   MAKE_BIT( _RESERVED9_10      , 2 )\
   MAKE_BIT( SDMMC1RST          , 1 )\
   MAKE_BIT( SPI1RST            , 1 )\
   MAKE_BIT( SPI4RST            , 1 )\
   MAKE_BIT( SYSCFGRST          , 1 )\
   MAKE_BIT( _RESERVED15_15     , 1 )\
   MAKE_BIT( TIM9RST            , 1 )\
   MAKE_BIT( TIM10RST           , 1 )\
   MAKE_BIT( TIM11RST           , 1 )\
   MAKE_BIT( _RESERVED19_19     , 1 )\
   MAKE_BIT( SPI5RST            , 1 )\
   MAKE_BIT( SPI6RST            , 1 )\
   MAKE_BIT( SAI1RST            , 1 )\
   MAKE_BIT( SAI2RST            , 1 )\
   MAKE_BIT( _RESERVED24_25     , 2 )\
   MAKE_BIT( LTDCRST            , 1 )\
   MAKE_BIT( _RESERVED27_31     , 5 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB1ENR(MAKE_BIT) \
   MAKE_BIT( GPIOAEN           , 1 )\
   MAKE_BIT( GPIOBEN           , 1 )\
   MAKE_BIT( GPIOCEN           , 1 )\
   MAKE_BIT( GPIODEN           , 1 )\
   MAKE_BIT( GPIOEEN           , 1 )\
   MAKE_BIT( GPIOFEN           , 1 )\
   MAKE_BIT( GPIOGEN           , 1 )\
   MAKE_BIT( GPIOHEN           , 1 )\
   MAKE_BIT( GPIOIEN           , 1 )\
   MAKE_BIT( GPIOJEN           , 1 )\
   MAKE_BIT( GPIOKEN           , 1 )\
   MAKE_BIT( _RESERVED11_11    , 1 )\
   MAKE_BIT( CRCEN             , 1 )\
   MAKE_BIT( _RESERVED13_17    , 5 )\
   MAKE_BIT( BKPSRAMEN         , 1 )\
   MAKE_BIT( _RESERVED19_19    , 1 )\
   MAKE_BIT( DTCMRAMEN         , 1 )\
   MAKE_BIT( DMA1EN            , 1 )\
   MAKE_BIT( DMA2EN            , 1 )\
   MAKE_BIT( DMA2DEN           , 1 )\
   MAKE_BIT( _RESERVED24_24    , 1 )\
   MAKE_BIT( ETHMACEN          , 1 )\
   MAKE_BIT( ETHMACTXEN        , 1 )\
   MAKE_BIT( ETHMACRXEN        , 1 )\
   MAKE_BIT( ETHMACPTPEN       , 1 )\
   MAKE_BIT( OTGHSEN           , 1 )\
   MAKE_BIT( OTGHSULPIEN       , 1 )\
   MAKE_BIT( _RESERVED31_31    , 1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB2ENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB3ENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_APB1ENR(MAKE_BIT) \
   MAKE_BIT( TIM2_EN            , 1 )\
   MAKE_BIT( TIM3_EN            , 1 )\
   MAKE_BIT( TIM4_EN            , 1 )\
   MAKE_BIT( TIM5_EN            , 1 )\
   MAKE_BIT( TIM6_EN            , 1 )\
   MAKE_BIT( TIM7_EN            , 1 )\
   MAKE_BIT( TIM12_EN           , 1 )\
   MAKE_BIT( TIM13_EN           , 1 )\
   MAKE_BIT( TIM14_EN           , 1 )\
   MAKE_BIT( LPTIM1_EN          , 1 )\
   MAKE_BIT( _Reserved10_10     , 1 )\
   MAKE_BIT( WWDG_EN            , 1 )\
   MAKE_BIT( _Reserved12_13     , 2 )\
   MAKE_BIT( SPI2_EN            , 1 )\
   MAKE_BIT( SPI3_EN            , 1 )\
   MAKE_BIT( SPDIFRX_EN         , 1 )\
   MAKE_BIT( USART2_EN          , 1 )\
   MAKE_BIT( USART3_EN          , 1 )\
   MAKE_BIT( UART4_EN           , 1 )\
   MAKE_BIT( UART5_EN           , 1 )\
   MAKE_BIT( I2C1_EN            , 1 )\
   MAKE_BIT( I2C2_EN            , 1 )\
   MAKE_BIT( I2C3_EN            , 1 )\
   MAKE_BIT( I2C4_EN            , 1 )\
   MAKE_BIT( CAN1_EN            , 1 )\
   MAKE_BIT( CAN2_EN            , 1 )\
   MAKE_BIT( CEC_EN             , 1 )\
   MAKE_BIT( PWR_EN             , 1 )\
   MAKE_BIT( DAC_EN             , 1 )\
   MAKE_BIT( UART7_EN           , 1 )\
   MAKE_BIT( UART8_EN           , 1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_APB2ENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB1LPENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB2LPENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_AHB3LPENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_APB1LPENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_APB2LPENR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_BDCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_CSR(MAKE_BIT) \
   MAKE_BIT( LSION              , 1 )\
   MAKE_BIT( LSIRDY             , 1 )\
   MAKE_BIT( _RESERVED2_23      , 22 )\
   MAKE_BIT( RMVF               , 1 )\
   MAKE_BIT( BORRSTF            , 1 )\
   MAKE_BIT( PINRSTF            , 1 )\
   MAKE_BIT( PORRSTF            , 1 )\
   MAKE_BIT( SFTRSTF            , 1 )\
   MAKE_BIT( IWDGRSTF           , 1 )\
   MAKE_BIT( WWDGRSTF           , 1 )\
   MAKE_BIT( LPWRRSTF           , 1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_SSCRG(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_PLLI2SCFGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_PLLSAICFGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_5               , 6 )\
   MAKE_BIT( PLLSAIN                    , 9 )\
   MAKE_BIT( PLLSAIP                    , 2 )\
   MAKE_BIT( _RESERVED18_23             , 6 )\
   MAKE_BIT( PLLSAIQ                    , 5 )\
   MAKE_BIT( PLLSAIR                    , 3 )\
   MAKE_BIT( _RESERVED31_31             , 1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_DCKCFGR1(MAKE_BIT) \
   MAKE_BIT( PLLI2SDIVQ         , 5 )\
   MAKE_BIT( _RESERVED5_7       , 3 )\
   MAKE_BIT( PLLSAIDIVQ         , 5 )\
   MAKE_BIT( _RESERVED13_15     , 3 )\
   MAKE_BIT( PLLSAIDIVR         , 2 )\
   MAKE_BIT( _RESERVED18_19     , 2 )\
   MAKE_BIT( SAI1SEL            , 2 )\
   MAKE_BIT( SAI2SEL            , 2 )\
   MAKE_BIT( TIMPRE             , 1 )\
   MAKE_BIT( _RESERVED25_31     , 7 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RCC_DCKCFGR2(MAKE_BIT) \
   MAKE_BIT( UART1SEL       ,  2 )\
   MAKE_BIT( UART2SEL       ,  2 )\
   MAKE_BIT( UART3SEL       ,  2 )\
   MAKE_BIT( UART4SEL       ,  2 )\
   MAKE_BIT( UART5SEL       ,  2 )\
   MAKE_BIT( UART6SEL       ,  2 )\
   MAKE_BIT( UART7SEL       ,  2 )\
   MAKE_BIT( UART8SEL       ,  2 )\
   MAKE_BIT( I2C1SEL        ,  2 )\
   MAKE_BIT( I2C2SEL        ,  2 )\
   MAKE_BIT( I2C3SEL        ,  2 )\
   MAKE_BIT( I2C4SEL        ,  2 )\
   MAKE_BIT( LPTIM1SEL      ,  2 )\
   MAKE_BIT( CECSEL         ,  1 )\
   MAKE_BIT( CK48MSEL       ,  1 )\
   MAKE_BIT( SDMMCSEL       ,  1 )\
   MAKE_BIT( _RESERVED29_31 ,  3 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOA_MODER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOB_MODER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_MODER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_OTYPER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOA_OSPEEDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOB_OSPEEDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_OSPEEDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_PUPDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_IDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_ODR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_BSRR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_LCKR(MAKE_BIT) \
   MAKE_BIT( LCK            , 16 )\
   MAKE_BIT( LCKK           ,  1 )\
   MAKE_BIT( _RESERVED17_31 , 15 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_AFRL(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_GPIOx_AFRH(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSCFG_MEMRMP(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSCFG_PMC(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_15 , 16 )\
   MAKE_BIT( ADC1DC2 , 1 )\
   MAKE_BIT( ADC2DC2 , 1 )\
   MAKE_BIT( ADC3DC2 , 1 )\
   MAKE_BIT( _RESERVED19_22 , 4 )\
   MAKE_BIT( MII_RMII_SEL , 1 )\
   MAKE_BIT( _RESERVED24_31 , 8 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSCFG_EXTICR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSCFG_EXTICR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSCFG_EXTICR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSCFG_EXTICR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SYSCFG_CMPCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_LISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_HISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_LIFCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_HIFCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S0CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S0NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S0PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S0M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S0M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S0FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S1CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S1NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S1PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S1M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S1M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S1FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S2CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S2NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S2PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S2M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S2M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S2FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S3CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S3NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S3PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S3M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S3M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S3FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S4CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S4NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S4PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S4M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S4M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S4FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S5CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S5NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S5PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S5M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S5M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S5FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S6CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S6NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S6PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S6M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S6M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S6FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S7CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S7NDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S7PAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S7M0AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S7M1AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA_S7FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_ISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_IFCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_FGMAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_FGOR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_BGOR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_FGPFCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_FGCOLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_BGPFCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_BGCOLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_FGCMAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_BGCMAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_OPFCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_OCOLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_OMAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_OOR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_NLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_LWR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_AMTCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_FGCLUT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DMA2D_BGCLUT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_EXTI_IMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_EXTI_EMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_EXTI_RTSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_EXTI_FTSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_EXTI_SWIER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_EXTI_PR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRC_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRC_IDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRC_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRC_INIT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRC_POL(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BCR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BCR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BCR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BCR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BTR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BTR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BTR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BTR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BWTR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BWTR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BWTR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_BWTR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_PCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_PMEM(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_PATT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_ECCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SDCR1(MAKE_BIT) \
   MAKE_BIT( NC                 , 2  )\
   MAKE_BIT( NR                 , 2  )\
   MAKE_BIT( MWID               , 2  )\
   MAKE_BIT( NB                 , 1  )\
   MAKE_BIT( CAS                , 2  )\
   MAKE_BIT( WP                 , 1  )\
   MAKE_BIT( SDCLK              , 2  )\
   MAKE_BIT( RBURST             , 1  )\
   MAKE_BIT( RPIPE              , 2  )\
   MAKE_BIT( _RESERVED15_31     , 17 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SDCR2(MAKE_BIT) \
   MAKE_BIT( NC                 , 2  )\
   MAKE_BIT( NR                 , 2  )\
   MAKE_BIT( MWID               , 2  )\
   MAKE_BIT( NB                 , 1  )\
   MAKE_BIT( CAS                , 2  )\
   MAKE_BIT( WP                 , 1  )\
   MAKE_BIT( SDCLK              , 2  )\
   MAKE_BIT( RBURST             , 1  )\
   MAKE_BIT( RPIPE              , 2  )\
   MAKE_BIT( _RESERVED15_31     , 17 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SDTR1(MAKE_BIT) \
   MAKE_BIT( TMRD               , 4 )\
   MAKE_BIT( TXSR               , 4 )\
   MAKE_BIT( TRAS               , 4 )\
   MAKE_BIT( TRC                , 4 )\
   MAKE_BIT( TWR                , 4 )\
   MAKE_BIT( TRP                , 4 )\
   MAKE_BIT( TRCD               , 4 )\
   MAKE_BIT( _RESERVED28_31     , 4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SDTR2(MAKE_BIT) \
   MAKE_BIT( TMRD               , 4 )\
   MAKE_BIT( TXSR               , 4 )\
   MAKE_BIT( TRAS               , 4 )\
   MAKE_BIT( TRC                , 4 )\
   MAKE_BIT( TWR                , 4 )\
   MAKE_BIT( TRP                , 4 )\
   MAKE_BIT( TRCD               , 4 )\
   MAKE_BIT( _RESERVED28_31     , 4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SDCMR(MAKE_BIT) \
   MAKE_BIT( MODE               , 3 )\
   MAKE_BIT( CTB2               , 1 )\
   MAKE_BIT( CTB1               , 1 )\
   MAKE_BIT( NRFS               , 4 )\
   MAKE_BIT( MRD                , 13 )\
   MAKE_BIT( _RESERVED22_31     , 10 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SDRTR(MAKE_BIT) \
   MAKE_BIT( CRE                , 1  )\
   MAKE_BIT( COUNT              , 13 )\
   MAKE_BIT( REIE               , 1  )\
   MAKE_BIT( _RESERVED15_31     , 17 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_FMC_SDSR(MAKE_BIT) \
   MAKE_BIT( RE                 , 1 )\
   MAKE_BIT( MODES1             , 2 )\
   MAKE_BIT( MODES2             , 2 )\
   MAKE_BIT( BUSY               , 1 )\
   MAKE_BIT( _RESERVED6_31      , 26 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_DCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_FCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_DLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_CCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_AR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_ABR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_PSMKR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_PSMAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_PIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_QUADSPI_LPTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_CR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_CR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_SMPR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_SMPR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JOFR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JOFR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JOFR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JOFR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_HTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_LTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_SQR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_SQR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_SQR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JSQR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JDR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JDR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JDR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_JDR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_CSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_CCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ADC_CDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_SWTRIGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR12R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR12L1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR8R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR12R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR12L2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR8R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR12RD(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR12LD(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DHR8RD(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DOR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_DOR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DAC_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_RIS(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_IER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_MIS(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_ICR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_ESCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_ESUR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_CWSTRT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_CWSIZE(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_DCMI_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_SSCR(MAKE_BIT) \
   MAKE_BIT( VSH            , 11 )\
   MAKE_BIT( _RESERVED11_15 ,  5 )\
   MAKE_BIT( HSW            , 12 )\
   MAKE_BIT( _RESERVED28_31 ,  4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_BPCR(MAKE_BIT) \
   MAKE_BIT( AVBP           , 11 )\
   MAKE_BIT( _RESERVED11_15 , 5 )\
   MAKE_BIT( AHBP           , 12 )\
   MAKE_BIT( _RESERVED28_31 , 4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_AWCR(MAKE_BIT) \
   MAKE_BIT( AAH            , 11 )\
   MAKE_BIT( _RESERVED11_15 ,  5 )\
   MAKE_BIT( AAW            , 12 )\
   MAKE_BIT( _RESERVED28_31 ,  4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_TWCR(MAKE_BIT) \
   MAKE_BIT( TOTALH             , 11 )\
   MAKE_BIT( _RESERVED11_16     ,  5 )\
   MAKE_BIT( TOTALW             , 12 )\
   MAKE_BIT( _RESERVED28_31     ,  4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remember to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_GCR(MAKE_BIT) \
   MAKE_BIT( LTDCEN             , 1 )\
   MAKE_BIT( _RESERVED1_3       , 3 )\
   MAKE_BIT( DBW                , 3 )\
   MAKE_BIT( _RESERVED7_7       , 1 )\
   MAKE_BIT( DGW                , 3 )\
   MAKE_BIT( _RESERVED11_11     , 1 )\
   MAKE_BIT( DRW                , 3 )\
   MAKE_BIT( _RESERVED15_15     , 1 )\
   MAKE_BIT( DEN                , 1 )\
   MAKE_BIT( _RESERVED17_27     , 11 )\
   MAKE_BIT( PCPOL              , 1 )\
   MAKE_BIT( DEPOL              , 1 )\
   MAKE_BIT( VSPOL              , 1 )\
   MAKE_BIT( HSPOL              , 1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_SRCR(MAKE_BIT) \
   MAKE_BIT( IMR            , 1 )\
   MAKE_BIT( VBR            , 1 )\
   MAKE_BIT( _RESERVED2_31 , 30 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_BCCR(MAKE_BIT) \
   MAKE_BIT( BCBLUE         , 8 )\
   MAKE_BIT( BCGREEN        , 8 )\
   MAKE_BIT( BCRED          , 8 )\
   MAKE_BIT( _RESERVED24_31 , 8 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_IER(MAKE_BIT) \
   MAKE_BIT( LIE            , 1 )\
   MAKE_BIT( FUIE           , 1 )\
   MAKE_BIT( TERRIE         , 1 )\
   MAKE_BIT( RRIE           , 1 )\
   MAKE_BIT( _RESERVED4_31  , 28 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_ISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_ICR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_LIPCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_CPSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_CDSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1CR(MAKE_BIT) \
   MAKE_BIT( LEN                , 1 )\
   MAKE_BIT( COLKEN             , 1 )\
   MAKE_BIT( _RESERVED2_3       , 2 )\
   MAKE_BIT( CLUTEN             , 1 )\
   MAKE_BIT( _RESERVED5_31      , 27 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1WHPCR(MAKE_BIT) \
   MAKE_BIT( WHSTPOS            , 12 )\
   MAKE_BIT( _RESERVED12_15     ,  4 )\
   MAKE_BIT( WHSPPOS            , 12 )\
   MAKE_BIT( _RESERVED28_31     ,  4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1WVPCR(MAKE_BIT) \
   MAKE_BIT( WVSTPOS            , 11 )\
   MAKE_BIT( _RESERVED11_15     ,  5 )\
   MAKE_BIT( WVSPPOS            , 11 )\
   MAKE_BIT( _RESERVED27_31     ,  5 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1CKCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1PFCR(MAKE_BIT) \
   MAKE_BIT( PF             , 3  )\
   MAKE_BIT( _RESERVED3_31  , 29 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1CACR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1DCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1BFCR(MAKE_BIT) \
   MAKE_BIT( BF1            ,  3 )\
   MAKE_BIT( _RESERVED3_7   ,  5 )\
   MAKE_BIT( BF2            ,  3 )\
   MAKE_BIT( _RESERVED11_31 , 21 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1CFBAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1CFBLR(MAKE_BIT) \
   MAKE_BIT( CFBLL              , 13 )\
   MAKE_BIT( _RESERVED13_15     ,  3 )\
   MAKE_BIT( CFBP               , 13 )\
   MAKE_BIT( _RESERVED29_31     ,  3 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1CFBLNR(MAKE_BIT) \
   MAKE_BIT( CFBLNBR        , 11 )\
   MAKE_BIT( _RESERVED11_31 , 21 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L1CLUTWR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2CR(MAKE_BIT) \
   MAKE_BIT( LEN                , 1 )\
   MAKE_BIT( COLKEN             , 1 )\
   MAKE_BIT( _RESERVED2_3       , 2 )\
   MAKE_BIT( CLUTEN             , 1 )\
   MAKE_BIT( _RESERVED5_31      , 27 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2WHPCR(MAKE_BIT) \
   MAKE_BIT( WHSTPOS            , 12 )\
   MAKE_BIT( _RESERVED12_15     ,  4 )\
   MAKE_BIT( WHSPPOS            , 12 )\
   MAKE_BIT( _RESERVED28_31     ,  4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2WVPCR(MAKE_BIT) \
   MAKE_BIT( WVSTPOS            , 11 )\
   MAKE_BIT( _RESERVED11_15     ,  5 )\
   MAKE_BIT( WVSPPOS            , 11 )\
   MAKE_BIT( _RESERVED27_31     ,  5 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2CKCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2PFCR(MAKE_BIT) \
   MAKE_BIT( PF             , 3  )\
   MAKE_BIT( _RESERVED3_31  , 29 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2CACR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2DCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2BFCR(MAKE_BIT) \
   MAKE_BIT( BF1            ,  3 )\
   MAKE_BIT( _RESERVED3_7   ,  5 )\
   MAKE_BIT( BF2            ,  3 )\
   MAKE_BIT( _RESERVED11_31 , 21 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2CFBAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2CFBLR(MAKE_BIT) \
   MAKE_BIT( CFBLL              , 13 )\
   MAKE_BIT( _RESERVED13_15     ,  3 )\
   MAKE_BIT( CFBP               , 13 )\
   MAKE_BIT( _RESERVED29_31     ,  3 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2CFBLNR(MAKE_BIT) \
   MAKE_BIT( CFBLNBR        , 11 )\
   MAKE_BIT( _RESERVED11_31 , 21 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LTDC_L2CLUTWR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RNG_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RNG_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RNG_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_DIN(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_DOUT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_DMACR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_IMSCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_RISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_MISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K0LR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K0RR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K1LR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K1RR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K2LR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K2RR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K3LR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_K3RR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_IV0LR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_IV0RR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_IV1LR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_IV1RR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCM1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCM2R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCM3R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCM4R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCM5R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCM6R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCMCCM7R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM2R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM3R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM4R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM5R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM6R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CRYP_CSGCM7R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_DIN(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_STR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_IMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR5(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR6(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR7(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR8(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR9(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR10(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR11(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR12(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR13(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR14(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR15(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR16(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR17(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR18(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR19(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR20(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR21(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR22(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR23(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR24(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR25(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR26(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR27(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR28(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR29(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR30(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR31(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR32(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR33(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR34(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR35(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR36(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR37(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR38(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR39(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR40(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR41(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR42(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR43(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR44(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR45(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR46(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR47(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR48(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR49(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR50(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR51(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR52(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_CSR53(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR5(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR6(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_HASH_HR7(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_SMCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_DIER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_EGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCMR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCMR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CNT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_PSC(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_ARR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_RCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCR4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_BDTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_DCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_DMAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_OR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCMR3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCR5(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_TIMx_CCR6(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_ISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_ICR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_IER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_CFGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_CMP(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_ARR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_CNT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_LPTIMx_OR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_IWDG_KR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_IWDG_PR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_IWDG_RLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_IWDG_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_IWDG_WINR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WWDG_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WWDG_CFR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_WWDG_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_TR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_ISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_PRER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_WUTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_ALRMAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_ALRMBR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_WPR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_SSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_SHIFTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_TSTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_TSDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_TSSSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_CALR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_TAMPCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_ALRMASSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_ALRMBSSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_OR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP2R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP3R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP4R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP5R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP6R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP7R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP8R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP9R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP10R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP11R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP12R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP13R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP14R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP15R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP16R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP17R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP18R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP19R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP20R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP21R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP22R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP23R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP24R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP25R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP26R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP27R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP28R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP29R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP30R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_RTC_BKP31R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_CR1(MAKE_BIT) \
   MAKE_BIT( PE             , 1 )\
   MAKE_BIT( TXIE           , 1 )\
   MAKE_BIT( RXIE           , 1 )\
   MAKE_BIT( ADDRIE         , 1 )\
   MAKE_BIT( NACKIE         , 1 )\
   MAKE_BIT( STOPIE         , 1 )\
   MAKE_BIT( TCIE           , 1 )\
   MAKE_BIT( ERRIE          , 1 )\
   MAKE_BIT( DNF            , 4 )\
   MAKE_BIT( ANFOFF         , 1 )\
   MAKE_BIT( _RESERVED13_13 , 1 )\
   MAKE_BIT( TXDMAEN        , 1 )\
   MAKE_BIT( RXDMAEN        , 1 )\
   MAKE_BIT( SBC            , 1 )\
   MAKE_BIT( NOSTRETCH      , 1 )\
   MAKE_BIT( _RESERVED18_18 , 1 )\
   MAKE_BIT( GCEN           , 1 )\
   MAKE_BIT( SMBHEN         , 1 )\
   MAKE_BIT( SMBDEN         , 1 )\
   MAKE_BIT( ALERTEN        , 1 )\
   MAKE_BIT( PECEN          , 1 )\
   MAKE_BIT( _RESERVED24_31 , 8 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_CR2(MAKE_BIT) \
   MAKE_BIT( SADD               , 10 )\
   MAKE_BIT( RDWRN              ,  1 )\
   MAKE_BIT( ADD10              ,  1 )\
   MAKE_BIT( HEAD10R            ,  1 )\
   MAKE_BIT( START              ,  1 )\
   MAKE_BIT( STOP               ,  1 )\
   MAKE_BIT( NACK               ,  1 )\
   MAKE_BIT( NBYTES             ,  8 )\
   MAKE_BIT( RELOAD             ,  1 )\
   MAKE_BIT( AUTOEND            ,  1 )\
   MAKE_BIT( PECBYTE            ,  1 )\
   MAKE_BIT( _RESERVED27_31     ,  5 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_OAR1(MAKE_BIT) \
   MAKE_BIT( OA1           , 10 )\
   MAKE_BIT( OA1MODE       ,  1 )\
   MAKE_BIT( _RESERVED11_14,  4 )\
   MAKE_BIT( OA1EN         ,  1 )\
   MAKE_BIT( _RESERVED16_31, 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_OAR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_0   ,  1 )\
   MAKE_BIT( OA2            , 10 )\
   MAKE_BIT( _RESERVED11_14 ,  4 )\
   MAKE_BIT( OA2EN          ,  1 )\
   MAKE_BIT( _RESERVED16_31 , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_TIMINGR(MAKE_BIT) \
   MAKE_BIT( SCLL           ,  8 )\
   MAKE_BIT( SCLH           ,  8 )\
   MAKE_BIT( SDADEL         ,  4 )\
   MAKE_BIT( SCLDEL         ,  4 )\
   MAKE_BIT( _RESERVED24_27 ,  4 )\
   MAKE_BIT( PRESC          ,  4 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_TIMEOUTR(MAKE_BIT) \
   MAKE_BIT( TIMEOUTA       , 12 )\
   MAKE_BIT( TIDLE          ,  1 )\
   MAKE_BIT( _RESERVED13_14 ,  2 )\
   MAKE_BIT( TIMEOUTEN      ,  1 )\
   MAKE_BIT( TIMEOUTB       , 12 )\
   MAKE_BIT( _RESERVED28_30 ,  3 )\
   MAKE_BIT( TEXTEN         ,  1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_ISR(MAKE_BIT) \
   MAKE_BIT( TXE            ,  1 )\
   MAKE_BIT( TXIS           ,  1 )\
   MAKE_BIT( RXNE           ,  1 )\
   MAKE_BIT( ADDR           ,  1 )\
   MAKE_BIT( NACKF          ,  1 )\
   MAKE_BIT( STOPF          ,  1 )\
   MAKE_BIT( TC             ,  1 )\
   MAKE_BIT( TCR            ,  1 )\
   MAKE_BIT( BERR           ,  1 )\
   MAKE_BIT( ARLO           ,  1 )\
   MAKE_BIT( OVR            ,  1 )\
   MAKE_BIT( PECERR         ,  1 )\
   MAKE_BIT( TIMEOUT        ,  1 )\
   MAKE_BIT( ALERT          ,  1 )\
   MAKE_BIT( _RESERVED14_14 ,  1 )\
   MAKE_BIT( BUSY           ,  1 )\
   MAKE_BIT( DIR            ,  1 )\
   MAKE_BIT( ADDCODE        ,  7 )\
   MAKE_BIT( _RESERVED24_31 ,  8 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_ICR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_2   ,  3 )\
   MAKE_BIT( ADDRCF         ,  1 )\
   MAKE_BIT( NACKCF         ,  1 )\
   MAKE_BIT( STOPCF         ,  1 )\
   MAKE_BIT( _RESERVED6_7   ,  2 )\
   MAKE_BIT( BERRCF         ,  1 )\
   MAKE_BIT( ARLOCF         ,  1 )\
   MAKE_BIT( OVRCF          ,  1 )\
   MAKE_BIT( PECCF          ,  1 )\
   MAKE_BIT( TIMEOUTCF      ,  1 )\
   MAKE_BIT( ALERTCF        ,  1 )\
   MAKE_BIT( _RESERVED14_31 , 18 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_PECR(MAKE_BIT) \
   MAKE_BIT( PEC            ,  8 )\
   MAKE_BIT( _RESERVED8_31  , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_RXDR(MAKE_BIT) \
   MAKE_BIT( RXDATA        ,  8 )\
   MAKE_BIT( _RESERVED8_31 , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_I2C_TXDR(MAKE_BIT) \
   MAKE_BIT( TXDATA         ,  8 )\
   MAKE_BIT( _RESERVED0_31  , 24 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_CR1(MAKE_BIT) \
   MAKE_BIT( UE                 , 1 )\
   MAKE_BIT( _RESERVED_1_1      , 1 )\
   MAKE_BIT( RE                 , 1 )\
   MAKE_BIT( TE                 , 1 )\
   MAKE_BIT( IDLEIE             , 1 )\
   MAKE_BIT( RXNEIE             , 1 )\
   MAKE_BIT( TCIE               , 1 )\
   MAKE_BIT( TXEIE              , 1 )\
   MAKE_BIT( PEIE               , 1 )\
   MAKE_BIT( PS                 , 1 )\
   MAKE_BIT( PCE                , 1 )\
   MAKE_BIT( WAKE               , 1 )\
   MAKE_BIT( M0                 , 1 )\
   MAKE_BIT( MME                , 1 )\
   MAKE_BIT( CMIE               , 1 )\
   MAKE_BIT( OVER8              , 1 )\
   MAKE_BIT( DEDT               , 5 )\
   MAKE_BIT( DEAT               , 5 )\
   MAKE_BIT( RTOIE              , 1 )\
   MAKE_BIT( EOBIE              , 1 )\
   MAKE_BIT( M1                 , 1 )\
   MAKE_BIT( _RESERVED_29_31    , 3 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_CR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED_0_3          , 4 )\
   MAKE_BIT( ADDM7                  , 1 )\
   MAKE_BIT( LBDL                   , 1 )\
   MAKE_BIT( LBDIE                  , 1 )\
   MAKE_BIT( _RESERVED_7_7          , 1 )\
   MAKE_BIT( LBCL                   , 1 )\
   MAKE_BIT( CPHA                   , 1 )\
   MAKE_BIT( CPOL                   , 1 )\
   MAKE_BIT( CLKEN                  , 1 )\
   MAKE_BIT( STOP                   , 2 )\
   MAKE_BIT( LINEN                  , 1 )\
   MAKE_BIT( SWAP                   , 1 )\
   MAKE_BIT( RXINV                  , 1 )\
   MAKE_BIT( TXINV                  , 1 )\
   MAKE_BIT( DATAINV                , 1 )\
   MAKE_BIT( MSBFIRST               , 1 )\
   MAKE_BIT( ABREN                  , 1 )\
   MAKE_BIT( ABRMODE                , 2 )\
   MAKE_BIT( RTOEN                  , 1 )\
   MAKE_BIT( ADD                    , 8 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_CR3(MAKE_BIT) \
   MAKE_BIT( EIE            , 1 )\
   MAKE_BIT( IREN           , 1 )\
   MAKE_BIT( IRLP           , 1 )\
   MAKE_BIT( HDSEL          , 1 )\
   MAKE_BIT( NACK           , 1 )\
   MAKE_BIT( SCEN           , 1 )\
   MAKE_BIT( DMAR           , 1 )\
   MAKE_BIT( DMAT           , 1 )\
   MAKE_BIT( RTSE           , 1 )\
   MAKE_BIT( CTSE           , 1 )\
   MAKE_BIT( CTSIE          , 1 )\
   MAKE_BIT( ONE_BIT        , 1 )\
   MAKE_BIT( OVR_DIS        , 1 )\
   MAKE_BIT( DDRE           , 1 )\
   MAKE_BIT( DEM            , 1 )\
   MAKE_BIT( DEP            , 1 )\
   MAKE_BIT( _RESERVED16_16 , 1 )\
   MAKE_BIT( SCARCNT        , 3 )\
   MAKE_BIT( _RESERVED20_31 , 12 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_BRR(MAKE_BIT) \
   MAKE_BIT( BRR                , 16 )\
   MAKE_BIT( _RESERVED_16_31    , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_GTPR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_RTOR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_RQR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_ISR(MAKE_BIT) \
   MAKE_BIT( PE                     , 1 )\
   MAKE_BIT( FE                     , 1 )\
   MAKE_BIT( NF                     , 1 )\
   MAKE_BIT( ORE                    , 1 )\
   MAKE_BIT( IDLE                   , 1 )\
   MAKE_BIT( RXNE                   , 1 )\
   MAKE_BIT( TC                     , 1 )\
   MAKE_BIT( TXE                    , 1 )\
   MAKE_BIT( LBDF                   , 1 )\
   MAKE_BIT( CTSIF                  , 1 )\
   MAKE_BIT( CTS                    , 1 )\
   MAKE_BIT( RTOF                   , 1 )\
   MAKE_BIT( EOBF                   , 1 )\
   MAKE_BIT( _RESERVED_13_13        , 1 )\
   MAKE_BIT( ABRE                   , 1 )\
   MAKE_BIT( ABRF                   , 1 )\
   MAKE_BIT( BUSY                   , 1 )\
   MAKE_BIT( CMF                    , 1 )\
   MAKE_BIT( SBKF                   , 1 )\
   MAKE_BIT( _RESERVED_19_20        , 2 )\
   MAKE_BIT( TEACK                  , 1 )\
   MAKE_BIT( _RESERVED_22_31        , 10 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_ICR(MAKE_BIT) \
   MAKE_BIT( PECF           , 1 )\
   MAKE_BIT( FECF           , 1 )\
   MAKE_BIT( NCF            , 1 )\
   MAKE_BIT( ORECF          , 1 )\
   MAKE_BIT( IDLECF         , 1 )\
   MAKE_BIT( _RESERVED5_5   , 1 )\
   MAKE_BIT( TCCF           , 1 )\
   MAKE_BIT( _RESERVED7_7   , 1 )\
   MAKE_BIT( LBDCF          , 1 )\
   MAKE_BIT( CTSCF          , 1 )\
   MAKE_BIT( _RESERVED10_10 , 1 )\
   MAKE_BIT( RTOCF          , 1 )\
   MAKE_BIT( EOBCF          , 1 )\
   MAKE_BIT( _RESERVED13_16 , 4 )\
   MAKE_BIT( CMCF           , 1 )\
   MAKE_BIT( _RESERVED18_31 , 14 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_RDR(MAKE_BIT) \
   MAKE_BIT( RDR                    ,  9 )\
   MAKE_BIT( _RESERVED_9_31         , 23 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_USARTx_TDR(MAKE_BIT) \
   MAKE_BIT( TDR                    ,  9 )\
   MAKE_BIT( _RESERVED_9_31         , 23 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_CR1(MAKE_BIT) \
   MAKE_BIT( CPHA           ,  1 )\
   MAKE_BIT( CPOL           ,  1 )\
   MAKE_BIT( MSTR           ,  1 )\
   MAKE_BIT( BR             ,  3 )\
   MAKE_BIT( SPE            ,  1 )\
   MAKE_BIT( LSBFIRST       ,  1 )\
   MAKE_BIT( SSI            ,  1 )\
   MAKE_BIT( SSM            ,  1 )\
   MAKE_BIT( RXONLY         ,  1 )\
   MAKE_BIT( CRCL           ,  1 )\
   MAKE_BIT( CRCNEXT        ,  1 )\
   MAKE_BIT( CRCEN          ,  1 )\
   MAKE_BIT( BIDIOE         ,  1 )\
   MAKE_BIT( BIDIMODE       ,  1 )\
   MAKE_BIT( _RESERVED16_31 , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_CR2(MAKE_BIT) \
   MAKE_BIT( RXDMAEN        ,  1 )\
   MAKE_BIT( TXDMAEN        ,  1 )\
   MAKE_BIT( SSOE           ,  1 )\
   MAKE_BIT( NSSP           ,  1 )\
   MAKE_BIT( FRF            ,  1 )\
   MAKE_BIT( ERRIE          ,  1 )\
   MAKE_BIT( RXNEIE         ,  1 )\
   MAKE_BIT( TXEIE          ,  1 )\
   MAKE_BIT( DS             ,  4 )\
   MAKE_BIT( FRXTH          ,  1 )\
   MAKE_BIT( LDMA_RX        ,  1 )\
   MAKE_BIT( LDMA_TX        ,  1 )\
   MAKE_BIT( _RESERVED15_31 , 17 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_SR(MAKE_BIT) \
   MAKE_BIT( RXNE           ,  1 )\
   MAKE_BIT( TXE            ,  1 )\
   MAKE_BIT( CHSIDE         ,  1 )\
   MAKE_BIT( UDR            ,  1 )\
   MAKE_BIT( CRCERR         ,  1 )\
   MAKE_BIT( MODF           ,  1 )\
   MAKE_BIT( OVR            ,  1 )\
   MAKE_BIT( BSY            ,  1 )\
   MAKE_BIT( FRE            ,  1 )\
   MAKE_BIT( FRLVL          ,  2 )\
   MAKE_BIT( FTLVL          ,  2 )\
   MAKE_BIT( _RESERVED13_31 , 19 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_CRCPR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_RXCRCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_TXCRCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_I2SCFGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPIx_I2SPR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_GCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xCR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xCR2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xFRCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xSLOTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xIM(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xCLRFR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SAI_xDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPDIFRX_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPDIFRX_IMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPDIFRX_SR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPDIFRX_IFCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPDIFRX_DR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPDIFRX_CSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SPDIFRX_DIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_POWER(MAKE_BIT) \
   MAKE_BIT( PWRCTRL        ,  2 )\
   MAKE_BIT( _RESERVED2_31  , 30 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_CLKCR(MAKE_BIT) \
   MAKE_BIT( CLKDIV             ,  8 )\
   MAKE_BIT( CLKEN              ,  1 )\
   MAKE_BIT( PWRSAV             ,  1 )\
   MAKE_BIT( BYPASS             ,  1 )\
   MAKE_BIT( WIDBUS             ,  2 )\
   MAKE_BIT( NEDGE              ,  1 )\
   MAKE_BIT( HWFC_EN            ,  1 )\
   MAKE_BIT( _RESERVED_15_31    , 17 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_ARG(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_CMD(MAKE_BIT) \
   MAKE_BIT( CMDINDEX           ,  6 )\
   MAKE_BIT( WAITRESP           ,  2 )\
   MAKE_BIT( WAITINT            ,  1 )\
   MAKE_BIT( WAITPEND           ,  1 )\
   MAKE_BIT( CPSMEN             ,  1 )\
   MAKE_BIT( SDIOSuspend        ,  1 )\
   MAKE_BIT( _RESERVED_12_31    , 20 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_RESPCMD(MAKE_BIT) \
   MAKE_BIT( RESPCMD        ,  6 )\
   MAKE_BIT( _RESERVED6_31  , 26 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_RESP1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_RESP2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_RESP3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_RESP4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_DTIMER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_DLEN(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_DCTRL(MAKE_BIT) \
   MAKE_BIT( DTEN               ,  1 )\
   MAKE_BIT( DTDIR              ,  1 )\
   MAKE_BIT( DTMODE             ,  1 )\
   MAKE_BIT( DMAEN              ,  1 )\
   MAKE_BIT( DBLOCKSIZE         ,  4 )\
   MAKE_BIT( RWSTART            ,  1 )\
   MAKE_BIT( RWSTOP             ,  1 )\
   MAKE_BIT( RWMOD              ,  1 )\
   MAKE_BIT( SDIOEN             ,  1 )\
   MAKE_BIT( _RESERVED12_31     , 20 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_DCOUNT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_STA(MAKE_BIT) \
   MAKE_BIT( CCRCFAIL       , 1 )\
   MAKE_BIT( DCRCFAIL       , 1 )\
   MAKE_BIT( CTIMEOUT       , 1 )\
   MAKE_BIT( DTIMEOUT       , 1 )\
   MAKE_BIT( TCUNDERR       , 1 )\
   MAKE_BIT( RXOVERR        , 1 )\
   MAKE_BIT( CMDREND        , 1 )\
   MAKE_BIT( CMDSENT        , 1 )\
   MAKE_BIT( DATAEND        , 1 )\
   MAKE_BIT( _RESERVED_9_9  , 1 )\
   MAKE_BIT( DBCKEND        , 1 )\
   MAKE_BIT( CMDACT         , 1 )\
   MAKE_BIT( TXACT          , 1 )\
   MAKE_BIT( RXACT          , 1 )\
   MAKE_BIT( TXFIFOHE       , 1 )\
   MAKE_BIT( RXFIFOHF       , 1 )\
   MAKE_BIT( TXFIFOF        , 1 )\
   MAKE_BIT( RXFIFOF        , 1 )\
   MAKE_BIT( TXFIFOE        , 1 )\
   MAKE_BIT( RXFIFOE        , 1 )\
   MAKE_BIT( TXDAVL         , 1 )\
   MAKE_BIT( RXDAVL         , 1 )\
   MAKE_BIT( SDIOIT         , 1 )\
   MAKE_BIT( _RESERVED_23_31, 9 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_ICR(MAKE_BIT) \
   MAKE_BIT( CCRCFAILC          ,  1 )\
   MAKE_BIT( DCRCFAILC          ,  1 )\
   MAKE_BIT( CTIMEOUTC          ,  1 )\
   MAKE_BIT( DTIMEOUTC          ,  1 )\
   MAKE_BIT( TXUNDERRC          ,  1 )\
   MAKE_BIT( RXOVERRC           ,  1 )\
   MAKE_BIT( CMDRENDC           ,  1 )\
   MAKE_BIT( CMDSENTC           ,  1 )\
   MAKE_BIT( DATAENDC           ,  1 )\
   MAKE_BIT( _RESERED_9_9       ,  1 )\
   MAKE_BIT( DBCKENDC           ,  1 )\
   MAKE_BIT( _RESERED_11_21     , 11 )\
   MAKE_BIT( SDIOITC            ,  1 )\
   MAKE_BIT( _RESERED_23_31     ,  9 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_MASK(MAKE_BIT) \
   MAKE_BIT( CCRCFAILIE             , 1 )\
   MAKE_BIT( DCRCFAILIE             , 1 )\
   MAKE_BIT( CTIMEOUTIE             , 1 )\
   MAKE_BIT( DTIMEOUTIE             , 1 )\
   MAKE_BIT( TXUNDERRIE             , 1 )\
   MAKE_BIT( RXOVERRIE              , 1 )\
   MAKE_BIT( CMDRENDIE              , 1 )\
   MAKE_BIT( CMDSENTIE              , 1 )\
   MAKE_BIT( DATAENDIE              , 1 )\
   MAKE_BIT( _RESERVED_9_9          , 1 )\
   MAKE_BIT( DBCKENDIE              , 1 )\
   MAKE_BIT( CMDACTIE               , 1 )\
   MAKE_BIT( TXACTIE                , 1 )\
   MAKE_BIT( RXACTIE                , 1 )\
   MAKE_BIT( TXFIFOHEIE             , 1 )\
   MAKE_BIT( RXFIFOHFIE             , 1 )\
   MAKE_BIT( TXFIFOFIE              , 1 )\
   MAKE_BIT( RXFIFOFIE              , 1 )\
   MAKE_BIT( TXFIFOEIE              , 1 )\
   MAKE_BIT( RXFIFOEIE              , 1 )\
   MAKE_BIT( TXDAVLIE               , 1 )\
   MAKE_BIT( RXDAVLIE               , 1 )\
   MAKE_BIT( SDIOITIE               , 1 )\
   MAKE_BIT( _RESERVED_23_31        , 9 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_FIFOCNT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_SDMMC_FIFO(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_MCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_MSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RF0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RF1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_IER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_ESR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_BTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TI0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDT0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDL0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDH0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TI1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDT1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDL1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDH1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TI2R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDT2R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDL2R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_TDH2R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RI0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RDT0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RDL0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RDH0R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RI1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RDT1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RDL1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_RDH1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_FMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_FM1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_FS1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_FFA1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_FA1R(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F0R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F0R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F1R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F1R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F2R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F2R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F3R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F3R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F4R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F4R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F5R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F5R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F6R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F6R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F7R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F7R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F8R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F8R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F9R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F9R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F10R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F10R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F11R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F11R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F12R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F12R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F13R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F13R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F14R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F14R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F15R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F15R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F16R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F16R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F17R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F17R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F18R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F18R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F19R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F19R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F20R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F20R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F21R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F21R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F22R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F22R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F23R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F23R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F24R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F24R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F25R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F25R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F26R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F26R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F27R1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CAN_F27R2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GOTGCTL(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GOTGINT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GAHBCFG(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GUSBCFG(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GRSTCTL(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GINTSTS(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GINTMSK(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GRXSTSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GRXSTSPR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GRXFSIZ(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HNPTXFSIZ(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HNPTXSTS(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GI2CCTL(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GCCFG(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_CID(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_GLPMCFG(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HPTXFSIZ(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF5(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF6(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_DIEPTXF7(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCFG(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HFIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HFNUM(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HPTXSTS(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HAINT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HAINTMSK(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HPRT(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCSPLT0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCCHAR0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCINT0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCUNTMSK0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCDMA0(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCCHAR1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCINT1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCINTMSK1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ1(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ2(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ3(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ4(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ5(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ6(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ7(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ8(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ9(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ10(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ11(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ12(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ13(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ14(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ15(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ16(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ17(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ18(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ19(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ20(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ21(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ22(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ23(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ24(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ25(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ26(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ27(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ28(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ29(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ30(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ31(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ32(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ33(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ34(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ35(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ36(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ37(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ38(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ39(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ40(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ41(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ42(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ43(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ44(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ45(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ46(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ47(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ48(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ49(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ50(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ51(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ52(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ53(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ54(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ55(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ56(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ57(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ58(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ59(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ60(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ61(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ62(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ63(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ64(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ65(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ66(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ67(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ68(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ69(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ70(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ71(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ72(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ73(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ74(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ75(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCTSIZ76(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCCHAR11(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCINTMSK11(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCCHAR15(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCSPLT15(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_OTG_HCINTMSK15(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_1       , 2 )\
   MAKE_BIT( RE                 , 1 )\
   MAKE_BIT( TE                 , 1 )\
   MAKE_BIT( DC                 , 1 )\
   MAKE_BIT( BL                 , 2 )\
   MAKE_BIT( APCS               , 1 )\
   MAKE_BIT( _RESERVED8_8       , 1 )\
   MAKE_BIT( RD                 , 1 )\
   MAKE_BIT( IPCO               , 1 )\
   MAKE_BIT( DM                 , 1 )\
   MAKE_BIT( LM                 , 1 )\
   MAKE_BIT( ROD                , 1 )\
   MAKE_BIT( FES                , 1 )\
   MAKE_BIT( _RESERVED15_15     , 1 )\
   MAKE_BIT( CSD                , 1 )\
   MAKE_BIT( IFG                , 3 )\
   MAKE_BIT( _RESERVED20_21     , 2 )\
   MAKE_BIT( JD                 , 1 )\
   MAKE_BIT( WD                 , 1 )\
   MAKE_BIT( _RESERVED24_24     , 1 )\
   MAKE_BIT( CSTF               , 1 )\
   MAKE_BIT( _RESERVED26_31     , 6 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACFFR(MAKE_BIT) \
   MAKE_BIT( PM             , 1  )\
   MAKE_BIT( HU             , 1  )\
   MAKE_BIT( HM             , 1  )\
   MAKE_BIT( DAIF           , 1  )\
   MAKE_BIT( PAM            , 1  )\
   MAKE_BIT( BFD            , 1  )\
   MAKE_BIT( PCF            , 2  )\
   MAKE_BIT( SAIF           , 1  )\
   MAKE_BIT( SAF            , 1  )\
   MAKE_BIT( HPF            , 1  )\
   MAKE_BIT( _RESERVED11_30 , 20 )\
   MAKE_BIT( RA             , 1  )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACHTHR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACHTLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACMIIAR(MAKE_BIT) \
   MAKE_BIT( MB             , 1 )\
   MAKE_BIT( MW             , 1 )\
   MAKE_BIT( CR             , 3 )\
   MAKE_BIT( _RESERVED5_5   , 1 )\
   MAKE_BIT( MR             , 5 )\
   MAKE_BIT( PA             , 5 )\
   MAKE_BIT( _RESERVED16_31 , 16 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACMIIDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACFCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACVLANTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACRWUF(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACPMTCSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACDBGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACIMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_2   ,  3 )\
   MAKE_BIT( PMTIM          ,  1 )\
   MAKE_BIT( _RESERVED4_8   ,  5 )\
   MAKE_BIT( TSTIM          ,  1 )\
   MAKE_BIT( _RESERVED10_31 , 22 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA0HR(MAKE_BIT) \
   MAKE_BIT( MACA0H          , 16 )\
   MAKE_BIT( _RESERVED16_30  , 15 )\
   MAKE_BIT( MO              ,  1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA0LR(MAKE_BIT) \
   MAKE_BIT( MACA0LR        , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA1HR(MAKE_BIT) \
   MAKE_BIT( MACA1H          , 16 )\
   MAKE_BIT( _RESERVED16_30  , 15 )\
   MAKE_BIT( MO              ,  1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA1LR(MAKE_BIT) \
   MAKE_BIT( MACA1LR        , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA2HR(MAKE_BIT) \
   MAKE_BIT( MACA2H          , 16 )\
   MAKE_BIT( _RESERVED16_30  , 15 )\
   MAKE_BIT( MO              ,  1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA2LR(MAKE_BIT) \
   MAKE_BIT( MACA2LR        , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA3HR(MAKE_BIT) \
   MAKE_BIT( MACA3H          , 16 )\
   MAKE_BIT( _RESERVED16_30  , 15 )\
   MAKE_BIT( MO              ,  1 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MACA3LR(MAKE_BIT) \
   MAKE_BIT( MACA3LR        , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCRIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_4  , 5  )\
   MAKE_BIT( RFCES         , 1  )\
   MAKE_BIT( RFAES         , 1  )\
   MAKE_BIT( _RESERVED7_16 , 10 )\
   MAKE_BIT( RGUFS         , 1  )\
   MAKE_BIT( _RESERVED18_31, 14 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCCTIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCRIMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCTIMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCTGFSCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCTGFMSCCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCTGFCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCRFCECR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCRFAECR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_MMCRGUFCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTSCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPSSIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTSHR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTSLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTSHUR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTSLIR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTSAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTTHR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTTLR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_PTPTSSR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMABMR(MAKE_BIT) \
   MAKE_BIT( SR                 , 1 )\
   MAKE_BIT( DA                 , 1 )\
   MAKE_BIT( DSL                , 5 )\
   MAKE_BIT( EDFE               , 1 )\
   MAKE_BIT( PBL                , 6 )\
   MAKE_BIT( PM                 , 2 )\
   MAKE_BIT( FB                 , 1 )\
   MAKE_BIT( RDP                , 6 )\
   MAKE_BIT( USP                , 1 )\
   MAKE_BIT( FPM                , 1 )\
   MAKE_BIT( AAB                , 1 )\
   MAKE_BIT( MB                 , 1 )\
   MAKE_BIT( _RESERVED27_31     , 5 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMATPDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMARPDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMARDLAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMATDLAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMASR(MAKE_BIT) \
   MAKE_BIT( TS                 , 1 )\
   MAKE_BIT( TPSS               , 1 )\
   MAKE_BIT( TBUS               , 1 )\
   MAKE_BIT( TJTS               , 1 )\
   MAKE_BIT( ROS                , 1 )\
   MAKE_BIT( TUS                , 1 )\
   MAKE_BIT( RS                 , 1 )\
   MAKE_BIT( RBUS               , 1 )\
   MAKE_BIT( RPSS               , 1 )\
   MAKE_BIT( RWTS               , 1 )\
   MAKE_BIT( ETS                , 1 )\
   MAKE_BIT( _RESERVED11_12     , 2 )\
   MAKE_BIT( FBES               , 1 )\
   MAKE_BIT( ERS                , 1 )\
   MAKE_BIT( AIS                , 1 )\
   MAKE_BIT( NIS                , 1 )\
   MAKE_BIT( RPS                , 3 )\
   MAKE_BIT( TPS                , 3 )\
   MAKE_BIT( EBS                , 3 )\
   MAKE_BIT( _RESERVED26_26     , 1 )\
   MAKE_BIT( MMCS               , 1 )\
   MAKE_BIT( PMTS               , 1 )\
   MAKE_BIT( TSTS               , 1 )\
   MAKE_BIT( _RESERVED30_31     , 2 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMAOMR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_0       , 1 )\
   MAKE_BIT( SR                 , 1 )\
   MAKE_BIT( OSF                , 1 )\
   MAKE_BIT( RTC                , 2 )\
   MAKE_BIT( _RESERVED5_5       , 1 )\
   MAKE_BIT( FUGF               , 1 )\
   MAKE_BIT( FEF                , 1 )\
   MAKE_BIT( _RESERVED8_12      , 5 )\
   MAKE_BIT( ST                 , 1 )\
   MAKE_BIT( TTC                , 3 )\
   MAKE_BIT( _RESERVED17_19     , 3 )\
   MAKE_BIT( FTF                , 1 )\
   MAKE_BIT( TSF                , 1 )\
   MAKE_BIT( _RESERVED22_23     , 2 )\
   MAKE_BIT( DFRF               , 1 )\
   MAKE_BIT( RSF                , 1 )\
   MAKE_BIT( DTCEFD             , 1 )\
   MAKE_BIT( _RESERVED27_31     , 5 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMAIER(MAKE_BIT) \
   MAKE_BIT( TIE            , 1 )\
   MAKE_BIT( TPSIE          , 1 )\
   MAKE_BIT( TBUIE          , 1 )\
   MAKE_BIT( TJTIE          , 1 )\
   MAKE_BIT( ROIE           , 1 )\
   MAKE_BIT( TUIE           , 1 )\
   MAKE_BIT( RIE            , 1 )\
   MAKE_BIT( RBUIE          , 1 )\
   MAKE_BIT( RPSIE          , 1 )\
   MAKE_BIT( RWTIE          , 1 )\
   MAKE_BIT( ETIE           , 1 )\
   MAKE_BIT( _RESERVED11_12 , 2 )\
   MAKE_BIT( FBEIE          , 1 )\
   MAKE_BIT( ERIE           , 1 )\
   MAKE_BIT( AISE           , 1 )\
   MAKE_BIT( NISE           , 1 )\
   MAKE_BIT( _RESERVED17_31 , 15 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMAMFBOCR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMARSWTR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMACHTDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMACHRDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMACHTBAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_ETH_DMACHRBAR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CEC_CR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CEC_CFGR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CEC_TXDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CEC_RXDR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CEC_ISR(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of bits in register
 *
 * This is definition of bits for a register. To add bit use MAKE_BIT macro as in an example below:
 * @code{.c}
   MAKE_BIT( BIT_NAME       , NUMBER_OF_BITS )\
   @endcode
 *
 * Where:
 *          BIT_NAME          - name of bit field
 *          NUMBER_OF_BITS    - number of bits for this bit field
 *
 * Remember, that order of bit fields definition is very important! It depends of machine endianess.
 * For big endian it should be from the bit 0 to the bit 32 (for example). Remeber to add also reserved bit fields as:
 * _RESERVED0_7 (where 0 and 7 is range of reserved bits)
 *
 * @warning Name of register MUST BE UNIQUE!
 */
//==========================================================================================================================================
#define oC_REGISTER_CEC_IER(MAKE_BIT) \
   MAKE_BIT( _RESERVED0_31 , 32 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of registers in this file
 *
 * You can add register to list by using MAKE_REGISTER macro:
 * @code{.c} MAKE_REGISTER( REGISTER_NAME )\ @endcode
 *
 * Where
 *          REGISTER_NAME           - name of the register definition
 *
 * Remember, that if you will add register, you must also define a list of bits for it, that must be named as:
 * oC_REGISTER_[REGISTER_NAME]
 *
 */
//==========================================================================================================================================
#define oC_REGISTERS_LIST(MAKE_REGISTER)    \
    MAKE_REGISTER( FLASH_ACR ) \
    MAKE_REGISTER( FLASH_KEYR ) \
    MAKE_REGISTER( FLASH_OPTKEYR ) \
    MAKE_REGISTER( FLASH_SR ) \
    MAKE_REGISTER( FLASH_CR ) \
    MAKE_REGISTER( FLASH_OPTCR ) \
    MAKE_REGISTER( FLASH_OPTCR1 ) \
    MAKE_REGISTER( PWR_CR1 ) \
    MAKE_REGISTER( PWR_CSR1 ) \
    MAKE_REGISTER( PWR_CR2 ) \
    MAKE_REGISTER( PWR_CSR2 ) \
    MAKE_REGISTER( RCC_CR ) \
    MAKE_REGISTER( RCC_PLLCFGR ) \
    MAKE_REGISTER( RCC_CFGR ) \
    MAKE_REGISTER( RCC_CIR ) \
    MAKE_REGISTER( RCC_AHB1RSTR ) \
    MAKE_REGISTER( RCC_AHB2RSTR ) \
    MAKE_REGISTER( RCC_AHB3RSTR ) \
    MAKE_REGISTER( RCC_APB1RSTR ) \
    MAKE_REGISTER( RCC_APB2RSTR ) \
    MAKE_REGISTER( RCC_AHB1ENR ) \
    MAKE_REGISTER( RCC_AHB2ENR ) \
    MAKE_REGISTER( RCC_AHB3ENR ) \
    MAKE_REGISTER( RCC_APB1ENR ) \
    MAKE_REGISTER( RCC_APB2ENR ) \
    MAKE_REGISTER( RCC_AHB1LPENR ) \
    MAKE_REGISTER( RCC_AHB2LPENR ) \
    MAKE_REGISTER( RCC_AHB3LPENR ) \
    MAKE_REGISTER( RCC_APB1LPENR ) \
    MAKE_REGISTER( RCC_APB2LPENR ) \
    MAKE_REGISTER( RCC_BDCR ) \
    MAKE_REGISTER( RCC_CSR ) \
    MAKE_REGISTER( RCC_SSCRG ) \
    MAKE_REGISTER( RCC_PLLI2SCFGR ) \
    MAKE_REGISTER( RCC_PLLSAICFGR ) \
    MAKE_REGISTER( RCC_DCKCFGR1 ) \
    MAKE_REGISTER( RCC_DCKCFGR2 ) \
    MAKE_REGISTER( GPIOA_MODER ) \
    MAKE_REGISTER( GPIOB_MODER ) \
    MAKE_REGISTER( GPIOx_MODER ) \
    MAKE_REGISTER( GPIOx_OTYPER ) \
    MAKE_REGISTER( GPIOA_OSPEEDR ) \
    MAKE_REGISTER( GPIOB_OSPEEDR ) \
    MAKE_REGISTER( GPIOx_OSPEEDR ) \
    MAKE_REGISTER( GPIOx_PUPDR ) \
    MAKE_REGISTER( GPIOx_IDR ) \
    MAKE_REGISTER( GPIOx_ODR ) \
    MAKE_REGISTER( GPIOx_BSRR ) \
    MAKE_REGISTER( GPIOx_LCKR ) \
    MAKE_REGISTER( GPIOx_AFRL ) \
    MAKE_REGISTER( GPIOx_AFRH ) \
    MAKE_REGISTER( SYSCFG_MEMRMP ) \
    MAKE_REGISTER( SYSCFG_PMC ) \
    MAKE_REGISTER( SYSCFG_EXTICR1 ) \
    MAKE_REGISTER( SYSCFG_EXTICR2 ) \
    MAKE_REGISTER( SYSCFG_EXTICR3 ) \
    MAKE_REGISTER( SYSCFG_EXTICR4 ) \
    MAKE_REGISTER( SYSCFG_CMPCR ) \
    MAKE_REGISTER( DMA_LISR ) \
    MAKE_REGISTER( DMA_HISR ) \
    MAKE_REGISTER( DMA_LIFCR ) \
    MAKE_REGISTER( DMA_HIFCR ) \
    MAKE_REGISTER( DMA_S0CR ) \
    MAKE_REGISTER( DMA_S0NDTR ) \
    MAKE_REGISTER( DMA_S0PAR ) \
    MAKE_REGISTER( DMA_S0M0AR ) \
    MAKE_REGISTER( DMA_S0M1AR ) \
    MAKE_REGISTER( DMA_S0FCR ) \
    MAKE_REGISTER( DMA_S1CR ) \
    MAKE_REGISTER( DMA_S1NDTR ) \
    MAKE_REGISTER( DMA_S1PAR ) \
    MAKE_REGISTER( DMA_S1M0AR ) \
    MAKE_REGISTER( DMA_S1M1AR ) \
    MAKE_REGISTER( DMA_S1FCR ) \
    MAKE_REGISTER( DMA_S2CR ) \
    MAKE_REGISTER( DMA_S2NDTR ) \
    MAKE_REGISTER( DMA_S2PAR ) \
    MAKE_REGISTER( DMA_S2M0AR ) \
    MAKE_REGISTER( DMA_S2M1AR ) \
    MAKE_REGISTER( DMA_S2FCR ) \
    MAKE_REGISTER( DMA_S3CR ) \
    MAKE_REGISTER( DMA_S3NDTR ) \
    MAKE_REGISTER( DMA_S3PAR ) \
    MAKE_REGISTER( DMA_S3M0AR ) \
    MAKE_REGISTER( DMA_S3M1AR ) \
    MAKE_REGISTER( DMA_S3FCR ) \
    MAKE_REGISTER( DMA_S4CR ) \
    MAKE_REGISTER( DMA_S4NDTR ) \
    MAKE_REGISTER( DMA_S4PAR ) \
    MAKE_REGISTER( DMA_S4M0AR ) \
    MAKE_REGISTER( DMA_S4M1AR ) \
    MAKE_REGISTER( DMA_S4FCR ) \
    MAKE_REGISTER( DMA_S5CR ) \
    MAKE_REGISTER( DMA_S5NDTR ) \
    MAKE_REGISTER( DMA_S5PAR ) \
    MAKE_REGISTER( DMA_S5M0AR ) \
    MAKE_REGISTER( DMA_S5M1AR ) \
    MAKE_REGISTER( DMA_S5FCR ) \
    MAKE_REGISTER( DMA_S6CR ) \
    MAKE_REGISTER( DMA_S6NDTR ) \
    MAKE_REGISTER( DMA_S6PAR ) \
    MAKE_REGISTER( DMA_S6M0AR ) \
    MAKE_REGISTER( DMA_S6M1AR ) \
    MAKE_REGISTER( DMA_S6FCR ) \
    MAKE_REGISTER( DMA_S7CR ) \
    MAKE_REGISTER( DMA_S7NDTR ) \
    MAKE_REGISTER( DMA_S7PAR ) \
    MAKE_REGISTER( DMA_S7M0AR ) \
    MAKE_REGISTER( DMA_S7M1AR ) \
    MAKE_REGISTER( DMA_S7FCR ) \
    MAKE_REGISTER( DMA2D_CR ) \
    MAKE_REGISTER( DMA2D_ISR ) \
    MAKE_REGISTER( DMA2D_IFCR ) \
    MAKE_REGISTER( DMA2D_FGMAR ) \
    MAKE_REGISTER( DMA2D_FGOR ) \
    MAKE_REGISTER( DMA2D_BGOR ) \
    MAKE_REGISTER( DMA2D_FGPFCCR ) \
    MAKE_REGISTER( DMA2D_FGCOLR ) \
    MAKE_REGISTER( DMA2D_BGPFCCR ) \
    MAKE_REGISTER( DMA2D_BGCOLR ) \
    MAKE_REGISTER( DMA2D_FGCMAR ) \
    MAKE_REGISTER( DMA2D_BGCMAR ) \
    MAKE_REGISTER( DMA2D_OPFCCR ) \
    MAKE_REGISTER( DMA2D_OCOLR ) \
    MAKE_REGISTER( DMA2D_OMAR ) \
    MAKE_REGISTER( DMA2D_OOR ) \
    MAKE_REGISTER( DMA2D_NLR ) \
    MAKE_REGISTER( DMA2D_LWR ) \
    MAKE_REGISTER( DMA2D_AMTCR ) \
    MAKE_REGISTER( DMA2D_FGCLUT ) \
    MAKE_REGISTER( DMA2D_BGCLUT ) \
    MAKE_REGISTER( EXTI_IMR ) \
    MAKE_REGISTER( EXTI_EMR ) \
    MAKE_REGISTER( EXTI_RTSR ) \
    MAKE_REGISTER( EXTI_FTSR ) \
    MAKE_REGISTER( EXTI_SWIER ) \
    MAKE_REGISTER( EXTI_PR ) \
    MAKE_REGISTER( CRC_DR ) \
    MAKE_REGISTER( CRC_IDR ) \
    MAKE_REGISTER( CRC_CR ) \
    MAKE_REGISTER( CRC_INIT ) \
    MAKE_REGISTER( CRC_POL ) \
    MAKE_REGISTER( FMC_BCR1 ) \
    MAKE_REGISTER( FMC_BCR2 ) \
    MAKE_REGISTER( FMC_BCR3 ) \
    MAKE_REGISTER( FMC_BCR4 ) \
    MAKE_REGISTER( FMC_BTR1 ) \
    MAKE_REGISTER( FMC_BTR2 ) \
    MAKE_REGISTER( FMC_BTR3 ) \
    MAKE_REGISTER( FMC_BTR4 ) \
    MAKE_REGISTER( FMC_BWTR1 ) \
    MAKE_REGISTER( FMC_BWTR2 ) \
    MAKE_REGISTER( FMC_BWTR3 ) \
    MAKE_REGISTER( FMC_BWTR4 ) \
    MAKE_REGISTER( FMC_PCR ) \
    MAKE_REGISTER( FMC_SR ) \
    MAKE_REGISTER( FMC_PMEM ) \
    MAKE_REGISTER( FMC_PATT ) \
    MAKE_REGISTER( FMC_ECCR ) \
    MAKE_REGISTER( FMC_SDCR1 ) \
    MAKE_REGISTER( FMC_SDCR2 ) \
    MAKE_REGISTER( FMC_SDTR1 ) \
    MAKE_REGISTER( FMC_SDTR2 ) \
    MAKE_REGISTER( FMC_SDCMR ) \
    MAKE_REGISTER( FMC_SDRTR ) \
    MAKE_REGISTER( FMC_SDSR ) \
    MAKE_REGISTER( QUADSPI_CR ) \
    MAKE_REGISTER( QUADSPI_DCR ) \
    MAKE_REGISTER( QUADSPI_SR ) \
    MAKE_REGISTER( QUADSPI_FCR ) \
    MAKE_REGISTER( QUADSPI_DLR ) \
    MAKE_REGISTER( QUADSPI_CCR ) \
    MAKE_REGISTER( QUADSPI_AR ) \
    MAKE_REGISTER( QUADSPI_ABR ) \
    MAKE_REGISTER( QUADSPI_DR ) \
    MAKE_REGISTER( QUADSPI_PSMKR ) \
    MAKE_REGISTER( QUADSPI_PSMAR ) \
    MAKE_REGISTER( QUADSPI_PIR ) \
    MAKE_REGISTER( QUADSPI_LPTR ) \
    MAKE_REGISTER( ADC_SR ) \
    MAKE_REGISTER( ADC_CR1 ) \
    MAKE_REGISTER( ADC_CR2 ) \
    MAKE_REGISTER( ADC_SMPR1 ) \
    MAKE_REGISTER( ADC_SMPR2 ) \
    MAKE_REGISTER( ADC_JOFR1 ) \
    MAKE_REGISTER( ADC_JOFR2 ) \
    MAKE_REGISTER( ADC_JOFR3 ) \
    MAKE_REGISTER( ADC_JOFR4 ) \
    MAKE_REGISTER( ADC_HTR ) \
    MAKE_REGISTER( ADC_LTR ) \
    MAKE_REGISTER( ADC_SQR1 ) \
    MAKE_REGISTER( ADC_SQR2 ) \
    MAKE_REGISTER( ADC_SQR3 ) \
    MAKE_REGISTER( ADC_JSQR ) \
    MAKE_REGISTER( ADC_JDR1 ) \
    MAKE_REGISTER( ADC_JDR2 ) \
    MAKE_REGISTER( ADC_JDR3 ) \
    MAKE_REGISTER( ADC_JDR4 ) \
    MAKE_REGISTER( ADC_DR ) \
    MAKE_REGISTER( ADC_CSR ) \
    MAKE_REGISTER( ADC_CCR ) \
    MAKE_REGISTER( ADC_CDR ) \
    MAKE_REGISTER( DAC_CR ) \
    MAKE_REGISTER( DAC_SWTRIGR ) \
    MAKE_REGISTER( DAC_DHR12R1 ) \
    MAKE_REGISTER( DAC_DHR12L1 ) \
    MAKE_REGISTER( DAC_DHR8R1 ) \
    MAKE_REGISTER( DAC_DHR12R2 ) \
    MAKE_REGISTER( DAC_DHR12L2 ) \
    MAKE_REGISTER( DAC_DHR8R2 ) \
    MAKE_REGISTER( DAC_DHR12RD ) \
    MAKE_REGISTER( DAC_DHR12LD ) \
    MAKE_REGISTER( DAC_DHR8RD ) \
    MAKE_REGISTER( DAC_DOR1 ) \
    MAKE_REGISTER( DAC_DOR2 ) \
    MAKE_REGISTER( DAC_SR ) \
    MAKE_REGISTER( DCMI_CR ) \
    MAKE_REGISTER( DCMI_SR ) \
    MAKE_REGISTER( DCMI_RIS ) \
    MAKE_REGISTER( DCMI_IER ) \
    MAKE_REGISTER( DCMI_MIS ) \
    MAKE_REGISTER( DCMI_ICR ) \
    MAKE_REGISTER( DCMI_ESCR ) \
    MAKE_REGISTER( DCMI_ESUR ) \
    MAKE_REGISTER( DCMI_CWSTRT ) \
    MAKE_REGISTER( DCMI_CWSIZE ) \
    MAKE_REGISTER( DCMI_DR ) \
    MAKE_REGISTER( LTDC_SSCR ) \
    MAKE_REGISTER( LTDC_BPCR ) \
    MAKE_REGISTER( LTDC_AWCR ) \
    MAKE_REGISTER( LTDC_TWCR ) \
    MAKE_REGISTER( LTDC_GCR ) \
    MAKE_REGISTER( LTDC_SRCR ) \
    MAKE_REGISTER( LTDC_BCCR ) \
    MAKE_REGISTER( LTDC_IER ) \
    MAKE_REGISTER( LTDC_ISR ) \
    MAKE_REGISTER( LTDC_ICR ) \
    MAKE_REGISTER( LTDC_LIPCR ) \
    MAKE_REGISTER( LTDC_CPSR ) \
    MAKE_REGISTER( LTDC_CDSR ) \
    MAKE_REGISTER( LTDC_L1CR ) \
    MAKE_REGISTER( LTDC_L1WHPCR ) \
    MAKE_REGISTER( LTDC_L1WVPCR ) \
    MAKE_REGISTER( LTDC_L1CKCR ) \
    MAKE_REGISTER( LTDC_L1PFCR ) \
    MAKE_REGISTER( LTDC_L1CACR ) \
    MAKE_REGISTER( LTDC_L1DCCR ) \
    MAKE_REGISTER( LTDC_L1BFCR ) \
    MAKE_REGISTER( LTDC_L1CFBAR ) \
    MAKE_REGISTER( LTDC_L1CFBLR ) \
    MAKE_REGISTER( LTDC_L1CFBLNR ) \
    MAKE_REGISTER( LTDC_L1CLUTWR ) \
    MAKE_REGISTER( LTDC_L2CR ) \
    MAKE_REGISTER( LTDC_L2WHPCR ) \
    MAKE_REGISTER( LTDC_L2WVPCR ) \
    MAKE_REGISTER( LTDC_L2CKCR ) \
    MAKE_REGISTER( LTDC_L2PFCR ) \
    MAKE_REGISTER( LTDC_L2CACR ) \
    MAKE_REGISTER( LTDC_L2DCCR ) \
    MAKE_REGISTER( LTDC_L2BFCR ) \
    MAKE_REGISTER( LTDC_L2CFBAR ) \
    MAKE_REGISTER( LTDC_L2CFBLR ) \
    MAKE_REGISTER( LTDC_L2CFBLNR ) \
    MAKE_REGISTER( LTDC_L2CLUTWR ) \
    MAKE_REGISTER( RNG_CR ) \
    MAKE_REGISTER( RNG_SR ) \
    MAKE_REGISTER( RNG_DR ) \
    MAKE_REGISTER( CRYP_CR ) \
    MAKE_REGISTER( CRYP_SR ) \
    MAKE_REGISTER( CRYP_DIN ) \
    MAKE_REGISTER( CRYP_DOUT ) \
    MAKE_REGISTER( CRYP_DMACR ) \
    MAKE_REGISTER( CRYP_IMSCR ) \
    MAKE_REGISTER( CRYP_RISR ) \
    MAKE_REGISTER( CRYP_MISR ) \
    MAKE_REGISTER( CRYP_K0LR ) \
    MAKE_REGISTER( CRYP_K0RR ) \
    MAKE_REGISTER( CRYP_K1LR ) \
    MAKE_REGISTER( CRYP_K1RR ) \
    MAKE_REGISTER( CRYP_K2LR ) \
    MAKE_REGISTER( CRYP_K2RR ) \
    MAKE_REGISTER( CRYP_K3LR ) \
    MAKE_REGISTER( CRYP_K3RR ) \
    MAKE_REGISTER( CRYP_IV0LR ) \
    MAKE_REGISTER( CRYP_IV0RR ) \
    MAKE_REGISTER( CRYP_IV1LR ) \
    MAKE_REGISTER( CRYP_IV1RR ) \
    MAKE_REGISTER( CRYP_CSGCMCCMR ) \
    MAKE_REGISTER( CRYP_CSGCMCCM1R ) \
    MAKE_REGISTER( CRYP_CSGCMCCM2R ) \
    MAKE_REGISTER( CRYP_CSGCMCCM3R ) \
    MAKE_REGISTER( CRYP_CSGCMCCM4R ) \
    MAKE_REGISTER( CRYP_CSGCMCCM5R ) \
    MAKE_REGISTER( CRYP_CSGCMCCM6R ) \
    MAKE_REGISTER( CRYP_CSGCMCCM7R ) \
    MAKE_REGISTER( CRYP_CSGCM0R ) \
    MAKE_REGISTER( CRYP_CSGCM1R ) \
    MAKE_REGISTER( CRYP_CSGCM2R ) \
    MAKE_REGISTER( CRYP_CSGCM3R ) \
    MAKE_REGISTER( CRYP_CSGCM4R ) \
    MAKE_REGISTER( CRYP_CSGCM5R ) \
    MAKE_REGISTER( CRYP_CSGCM6R ) \
    MAKE_REGISTER( CRYP_CSGCM7R ) \
    MAKE_REGISTER( HASH_CR ) \
    MAKE_REGISTER( HASH_DIN ) \
    MAKE_REGISTER( HASH_STR ) \
    MAKE_REGISTER( HASH_IMR ) \
    MAKE_REGISTER( HASH_SR ) \
    MAKE_REGISTER( HASH_CSR0 ) \
    MAKE_REGISTER( HASH_CSR1 ) \
    MAKE_REGISTER( HASH_CSR2 ) \
    MAKE_REGISTER( HASH_CSR3 ) \
    MAKE_REGISTER( HASH_CSR4 ) \
    MAKE_REGISTER( HASH_CSR5 ) \
    MAKE_REGISTER( HASH_CSR6 ) \
    MAKE_REGISTER( HASH_CSR7 ) \
    MAKE_REGISTER( HASH_CSR8 ) \
    MAKE_REGISTER( HASH_CSR9 ) \
    MAKE_REGISTER( HASH_CSR10 ) \
    MAKE_REGISTER( HASH_CSR11 ) \
    MAKE_REGISTER( HASH_CSR12 ) \
    MAKE_REGISTER( HASH_CSR13 ) \
    MAKE_REGISTER( HASH_CSR14 ) \
    MAKE_REGISTER( HASH_CSR15 ) \
    MAKE_REGISTER( HASH_CSR16 ) \
    MAKE_REGISTER( HASH_CSR17 ) \
    MAKE_REGISTER( HASH_CSR18 ) \
    MAKE_REGISTER( HASH_CSR19 ) \
    MAKE_REGISTER( HASH_CSR20 ) \
    MAKE_REGISTER( HASH_CSR21 ) \
    MAKE_REGISTER( HASH_CSR22 ) \
    MAKE_REGISTER( HASH_CSR23 ) \
    MAKE_REGISTER( HASH_CSR24 ) \
    MAKE_REGISTER( HASH_CSR25 ) \
    MAKE_REGISTER( HASH_CSR26 ) \
    MAKE_REGISTER( HASH_CSR27 ) \
    MAKE_REGISTER( HASH_CSR28 ) \
    MAKE_REGISTER( HASH_CSR29 ) \
    MAKE_REGISTER( HASH_CSR30 ) \
    MAKE_REGISTER( HASH_CSR31 ) \
    MAKE_REGISTER( HASH_CSR32 ) \
    MAKE_REGISTER( HASH_CSR33 ) \
    MAKE_REGISTER( HASH_CSR34 ) \
    MAKE_REGISTER( HASH_CSR35 ) \
    MAKE_REGISTER( HASH_CSR36 ) \
    MAKE_REGISTER( HASH_CSR37 ) \
    MAKE_REGISTER( HASH_CSR38 ) \
    MAKE_REGISTER( HASH_CSR39 ) \
    MAKE_REGISTER( HASH_CSR40 ) \
    MAKE_REGISTER( HASH_CSR41 ) \
    MAKE_REGISTER( HASH_CSR42 ) \
    MAKE_REGISTER( HASH_CSR43 ) \
    MAKE_REGISTER( HASH_CSR44 ) \
    MAKE_REGISTER( HASH_CSR45 ) \
    MAKE_REGISTER( HASH_CSR46 ) \
    MAKE_REGISTER( HASH_CSR47 ) \
    MAKE_REGISTER( HASH_CSR48 ) \
    MAKE_REGISTER( HASH_CSR49 ) \
    MAKE_REGISTER( HASH_CSR50 ) \
    MAKE_REGISTER( HASH_CSR51 ) \
    MAKE_REGISTER( HASH_CSR52 ) \
    MAKE_REGISTER( HASH_CSR53 ) \
    MAKE_REGISTER( HASH_HR0 ) \
    MAKE_REGISTER( HASH_HR1 ) \
    MAKE_REGISTER( HASH_HR2 ) \
    MAKE_REGISTER( HASH_HR3 ) \
    MAKE_REGISTER( HASH_HR4 ) \
    MAKE_REGISTER( HASH_HR5 ) \
    MAKE_REGISTER( HASH_HR6 ) \
    MAKE_REGISTER( HASH_HR7 ) \
    MAKE_REGISTER( TIMx_CR1 ) \
    MAKE_REGISTER( TIMx_CR2 ) \
    MAKE_REGISTER( TIMx_SMCR ) \
    MAKE_REGISTER( TIMx_DIER ) \
    MAKE_REGISTER( TIMx_SR ) \
    MAKE_REGISTER( TIMx_EGR ) \
    MAKE_REGISTER( TIMx_CCMR1 ) \
    MAKE_REGISTER( TIMx_CCMR2 ) \
    MAKE_REGISTER( TIMx_CCER ) \
    MAKE_REGISTER( TIMx_CNT ) \
    MAKE_REGISTER( TIMx_PSC ) \
    MAKE_REGISTER( TIMx_ARR ) \
    MAKE_REGISTER( TIMx_RCR ) \
    MAKE_REGISTER( TIMx_CCR1 ) \
    MAKE_REGISTER( TIMx_CCR2 ) \
    MAKE_REGISTER( TIMx_CCR3 ) \
    MAKE_REGISTER( TIMx_CCR4 ) \
    MAKE_REGISTER( TIMx_BDTR ) \
    MAKE_REGISTER( TIMx_DCR ) \
    MAKE_REGISTER( TIMx_DMAR ) \
    MAKE_REGISTER( TIMx_OR ) \
    MAKE_REGISTER( TIMx_CCMR3 ) \
    MAKE_REGISTER( TIMx_CCR5 ) \
    MAKE_REGISTER( TIMx_CCR6 ) \
    MAKE_REGISTER( LPTIMx_ISR ) \
    MAKE_REGISTER( LPTIMx_ICR ) \
    MAKE_REGISTER( LPTIMx_IER ) \
    MAKE_REGISTER( LPTIMx_CFGR ) \
    MAKE_REGISTER( LPTIMx_CR ) \
    MAKE_REGISTER( LPTIMx_CMP ) \
    MAKE_REGISTER( LPTIMx_ARR ) \
    MAKE_REGISTER( LPTIMx_CNT ) \
    MAKE_REGISTER( LPTIMx_OR ) \
    MAKE_REGISTER( IWDG_KR ) \
    MAKE_REGISTER( IWDG_PR ) \
    MAKE_REGISTER( IWDG_RLR ) \
    MAKE_REGISTER( IWDG_SR ) \
    MAKE_REGISTER( IWDG_WINR ) \
    MAKE_REGISTER( WWDG_CR ) \
    MAKE_REGISTER( WWDG_CFR ) \
    MAKE_REGISTER( WWDG_SR ) \
    MAKE_REGISTER( RTC_TR ) \
    MAKE_REGISTER( RTC_DR ) \
    MAKE_REGISTER( RTC_CR ) \
    MAKE_REGISTER( RTC_ISR ) \
    MAKE_REGISTER( RTC_PRER ) \
    MAKE_REGISTER( RTC_WUTR ) \
    MAKE_REGISTER( RTC_ALRMAR ) \
    MAKE_REGISTER( RTC_ALRMBR ) \
    MAKE_REGISTER( RTC_WPR ) \
    MAKE_REGISTER( RTC_SSR ) \
    MAKE_REGISTER( RTC_SHIFTR ) \
    MAKE_REGISTER( RTC_TSTR ) \
    MAKE_REGISTER( RTC_TSDR ) \
    MAKE_REGISTER( RTC_TSSSR ) \
    MAKE_REGISTER( RTC_CALR ) \
    MAKE_REGISTER( RTC_TAMPCR ) \
    MAKE_REGISTER( RTC_ALRMASSR ) \
    MAKE_REGISTER( RTC_ALRMBSSR ) \
    MAKE_REGISTER( RTC_OR ) \
    MAKE_REGISTER( RTC_BKP0R ) \
    MAKE_REGISTER( RTC_BKP1R ) \
    MAKE_REGISTER( RTC_BKP2R ) \
    MAKE_REGISTER( RTC_BKP3R ) \
    MAKE_REGISTER( RTC_BKP4R ) \
    MAKE_REGISTER( RTC_BKP5R ) \
    MAKE_REGISTER( RTC_BKP6R ) \
    MAKE_REGISTER( RTC_BKP7R ) \
    MAKE_REGISTER( RTC_BKP8R ) \
    MAKE_REGISTER( RTC_BKP9R ) \
    MAKE_REGISTER( RTC_BKP10R ) \
    MAKE_REGISTER( RTC_BKP11R ) \
    MAKE_REGISTER( RTC_BKP12R ) \
    MAKE_REGISTER( RTC_BKP13R ) \
    MAKE_REGISTER( RTC_BKP14R ) \
    MAKE_REGISTER( RTC_BKP15R ) \
    MAKE_REGISTER( RTC_BKP16R ) \
    MAKE_REGISTER( RTC_BKP17R ) \
    MAKE_REGISTER( RTC_BKP18R ) \
    MAKE_REGISTER( RTC_BKP19R ) \
    MAKE_REGISTER( RTC_BKP20R ) \
    MAKE_REGISTER( RTC_BKP21R ) \
    MAKE_REGISTER( RTC_BKP22R ) \
    MAKE_REGISTER( RTC_BKP23R ) \
    MAKE_REGISTER( RTC_BKP24R ) \
    MAKE_REGISTER( RTC_BKP25R ) \
    MAKE_REGISTER( RTC_BKP26R ) \
    MAKE_REGISTER( RTC_BKP27R ) \
    MAKE_REGISTER( RTC_BKP28R ) \
    MAKE_REGISTER( RTC_BKP29R ) \
    MAKE_REGISTER( RTC_BKP30R ) \
    MAKE_REGISTER( RTC_BKP31R ) \
    MAKE_REGISTER( I2C_CR1 ) \
    MAKE_REGISTER( I2C_CR2 ) \
    MAKE_REGISTER( I2C_OAR1 ) \
    MAKE_REGISTER( I2C_OAR2 ) \
    MAKE_REGISTER( I2C_TIMINGR ) \
    MAKE_REGISTER( I2C_TIMEOUTR ) \
    MAKE_REGISTER( I2C_ISR ) \
    MAKE_REGISTER( I2C_ICR ) \
    MAKE_REGISTER( I2C_PECR ) \
    MAKE_REGISTER( I2C_RXDR ) \
    MAKE_REGISTER( I2C_TXDR ) \
    MAKE_REGISTER( USARTx_CR1 ) \
    MAKE_REGISTER( USARTx_CR2 ) \
    MAKE_REGISTER( USARTx_CR3 ) \
    MAKE_REGISTER( USARTx_BRR ) \
    MAKE_REGISTER( USARTx_GTPR ) \
    MAKE_REGISTER( USARTx_RTOR ) \
    MAKE_REGISTER( USARTx_RQR ) \
    MAKE_REGISTER( USARTx_ISR ) \
    MAKE_REGISTER( USARTx_ICR ) \
    MAKE_REGISTER( USARTx_RDR ) \
    MAKE_REGISTER( USARTx_TDR ) \
    MAKE_REGISTER( SPIx_CR1 ) \
    MAKE_REGISTER( SPIx_CR2 ) \
    MAKE_REGISTER( SPIx_SR ) \
    MAKE_REGISTER( SPIx_DR ) \
    MAKE_REGISTER( SPIx_CRCPR ) \
    MAKE_REGISTER( SPIx_RXCRCR ) \
    MAKE_REGISTER( SPIx_TXCRCR ) \
    MAKE_REGISTER( SPIx_I2SCFGR ) \
    MAKE_REGISTER( SPIx_I2SPR ) \
    MAKE_REGISTER( SAI_GCR ) \
    MAKE_REGISTER( SAI_xCR1 ) \
    MAKE_REGISTER( SAI_xCR2 ) \
    MAKE_REGISTER( SAI_xFRCR ) \
    MAKE_REGISTER( SAI_xSLOTR ) \
    MAKE_REGISTER( SAI_xIM ) \
    MAKE_REGISTER( SAI_xSR ) \
    MAKE_REGISTER( SAI_xCLRFR ) \
    MAKE_REGISTER( SAI_xDR ) \
    MAKE_REGISTER( SPDIFRX_CR ) \
    MAKE_REGISTER( SPDIFRX_IMR ) \
    MAKE_REGISTER( SPDIFRX_SR ) \
    MAKE_REGISTER( SPDIFRX_IFCR ) \
    MAKE_REGISTER( SPDIFRX_DR ) \
    MAKE_REGISTER( SPDIFRX_CSR ) \
    MAKE_REGISTER( SPDIFRX_DIR ) \
    MAKE_REGISTER( SDMMC_POWER ) \
    MAKE_REGISTER( SDMMC_CLKCR ) \
    MAKE_REGISTER( SDMMC_ARG ) \
    MAKE_REGISTER( SDMMC_CMD ) \
    MAKE_REGISTER( SDMMC_RESPCMD ) \
    MAKE_REGISTER( SDMMC_RESP1 ) \
    MAKE_REGISTER( SDMMC_RESP2 ) \
    MAKE_REGISTER( SDMMC_RESP3 ) \
    MAKE_REGISTER( SDMMC_RESP4 ) \
    MAKE_REGISTER( SDMMC_DTIMER ) \
    MAKE_REGISTER( SDMMC_DLEN ) \
    MAKE_REGISTER( SDMMC_DCTRL ) \
    MAKE_REGISTER( SDMMC_DCOUNT ) \
    MAKE_REGISTER( SDMMC_STA ) \
    MAKE_REGISTER( SDMMC_ICR ) \
    MAKE_REGISTER( SDMMC_MASK ) \
    MAKE_REGISTER( SDMMC_FIFOCNT ) \
    MAKE_REGISTER( SDMMC_FIFO ) \
    MAKE_REGISTER( CAN_MCR ) \
    MAKE_REGISTER( CAN_MSR ) \
    MAKE_REGISTER( CAN_TSR ) \
    MAKE_REGISTER( CAN_RF0R ) \
    MAKE_REGISTER( CAN_RF1R ) \
    MAKE_REGISTER( CAN_IER ) \
    MAKE_REGISTER( CAN_ESR ) \
    MAKE_REGISTER( CAN_BTR ) \
    MAKE_REGISTER( CAN_TI0R ) \
    MAKE_REGISTER( CAN_TDT0R ) \
    MAKE_REGISTER( CAN_TDL0R ) \
    MAKE_REGISTER( CAN_TDH0R ) \
    MAKE_REGISTER( CAN_TI1R ) \
    MAKE_REGISTER( CAN_TDT1R ) \
    MAKE_REGISTER( CAN_TDL1R ) \
    MAKE_REGISTER( CAN_TDH1R ) \
    MAKE_REGISTER( CAN_TI2R ) \
    MAKE_REGISTER( CAN_TDT2R ) \
    MAKE_REGISTER( CAN_TDL2R ) \
    MAKE_REGISTER( CAN_TDH2R ) \
    MAKE_REGISTER( CAN_RI0R ) \
    MAKE_REGISTER( CAN_RDT0R ) \
    MAKE_REGISTER( CAN_RDL0R ) \
    MAKE_REGISTER( CAN_RDH0R ) \
    MAKE_REGISTER( CAN_RI1R ) \
    MAKE_REGISTER( CAN_RDT1R ) \
    MAKE_REGISTER( CAN_RDL1R ) \
    MAKE_REGISTER( CAN_RDH1R ) \
    MAKE_REGISTER( CAN_FMR ) \
    MAKE_REGISTER( CAN_FM1R ) \
    MAKE_REGISTER( CAN_FS1R ) \
    MAKE_REGISTER( CAN_FFA1R ) \
    MAKE_REGISTER( CAN_FA1R ) \
    MAKE_REGISTER( CAN_F0R1 ) \
    MAKE_REGISTER( CAN_F0R2 ) \
    MAKE_REGISTER( CAN_F1R1 ) \
    MAKE_REGISTER( CAN_F1R2 ) \
    MAKE_REGISTER( CAN_F2R1 ) \
    MAKE_REGISTER( CAN_F2R2 ) \
    MAKE_REGISTER( CAN_F3R1 ) \
    MAKE_REGISTER( CAN_F3R2 ) \
    MAKE_REGISTER( CAN_F4R1 ) \
    MAKE_REGISTER( CAN_F4R2 ) \
    MAKE_REGISTER( CAN_F5R1 ) \
    MAKE_REGISTER( CAN_F5R2 ) \
    MAKE_REGISTER( CAN_F6R1 ) \
    MAKE_REGISTER( CAN_F6R2 ) \
    MAKE_REGISTER( CAN_F7R1 ) \
    MAKE_REGISTER( CAN_F7R2 ) \
    MAKE_REGISTER( CAN_F8R1 ) \
    MAKE_REGISTER( CAN_F8R2 ) \
    MAKE_REGISTER( CAN_F9R1 ) \
    MAKE_REGISTER( CAN_F9R2 ) \
    MAKE_REGISTER( CAN_F10R1 ) \
    MAKE_REGISTER( CAN_F10R2 ) \
    MAKE_REGISTER( CAN_F11R1 ) \
    MAKE_REGISTER( CAN_F11R2 ) \
    MAKE_REGISTER( CAN_F12R1 ) \
    MAKE_REGISTER( CAN_F12R2 ) \
    MAKE_REGISTER( CAN_F13R1 ) \
    MAKE_REGISTER( CAN_F13R2 ) \
    MAKE_REGISTER( CAN_F14R1 ) \
    MAKE_REGISTER( CAN_F14R2 ) \
    MAKE_REGISTER( CAN_F15R1 ) \
    MAKE_REGISTER( CAN_F15R2 ) \
    MAKE_REGISTER( CAN_F16R1 ) \
    MAKE_REGISTER( CAN_F16R2 ) \
    MAKE_REGISTER( CAN_F17R1 ) \
    MAKE_REGISTER( CAN_F17R2 ) \
    MAKE_REGISTER( CAN_F18R1 ) \
    MAKE_REGISTER( CAN_F18R2 ) \
    MAKE_REGISTER( CAN_F19R1 ) \
    MAKE_REGISTER( CAN_F19R2 ) \
    MAKE_REGISTER( CAN_F20R1 ) \
    MAKE_REGISTER( CAN_F20R2 ) \
    MAKE_REGISTER( CAN_F21R1 ) \
    MAKE_REGISTER( CAN_F21R2 ) \
    MAKE_REGISTER( CAN_F22R1 ) \
    MAKE_REGISTER( CAN_F22R2 ) \
    MAKE_REGISTER( CAN_F23R1 ) \
    MAKE_REGISTER( CAN_F23R2 ) \
    MAKE_REGISTER( CAN_F24R1 ) \
    MAKE_REGISTER( CAN_F24R2 ) \
    MAKE_REGISTER( CAN_F25R1 ) \
    MAKE_REGISTER( CAN_F25R2 ) \
    MAKE_REGISTER( CAN_F26R1 ) \
    MAKE_REGISTER( CAN_F26R2 ) \
    MAKE_REGISTER( CAN_F27R1 ) \
    MAKE_REGISTER( CAN_F27R2 ) \
    MAKE_REGISTER( OTG_GOTGCTL ) \
    MAKE_REGISTER( OTG_GOTGINT ) \
    MAKE_REGISTER( OTG_GAHBCFG ) \
    MAKE_REGISTER( OTG_GUSBCFG ) \
    MAKE_REGISTER( OTG_GRSTCTL ) \
    MAKE_REGISTER( OTG_GINTSTS ) \
    MAKE_REGISTER( OTG_GINTMSK ) \
    MAKE_REGISTER( OTG_GRXSTSR ) \
    MAKE_REGISTER( OTG_GRXSTSPR ) \
    MAKE_REGISTER( OTG_GRXFSIZ ) \
    MAKE_REGISTER( OTG_HNPTXFSIZ ) \
    MAKE_REGISTER( OTG_DIEPTXF0 ) \
    MAKE_REGISTER( OTG_HNPTXSTS ) \
    MAKE_REGISTER( OTG_GI2CCTL ) \
    MAKE_REGISTER( OTG_GCCFG ) \
    MAKE_REGISTER( OTG_CID ) \
    MAKE_REGISTER( OTG_GLPMCFG ) \
    MAKE_REGISTER( OTG_HPTXFSIZ ) \
    MAKE_REGISTER( OTG_DIEPTXF1 ) \
    MAKE_REGISTER( OTG_DIEPTXF2 ) \
    MAKE_REGISTER( OTG_DIEPTXF3 ) \
    MAKE_REGISTER( OTG_DIEPTXF4 ) \
    MAKE_REGISTER( OTG_DIEPTXF5 ) \
    MAKE_REGISTER( OTG_DIEPTXF6 ) \
    MAKE_REGISTER( OTG_DIEPTXF7 ) \
    MAKE_REGISTER( OTG_HCFG ) \
    MAKE_REGISTER( OTG_HFIR ) \
    MAKE_REGISTER( OTG_HFNUM ) \
    MAKE_REGISTER( OTG_HPTXSTS ) \
    MAKE_REGISTER( OTG_HAINT ) \
    MAKE_REGISTER( OTG_HAINTMSK ) \
    MAKE_REGISTER( OTG_HPRT ) \
    MAKE_REGISTER( OTG_HCSPLT0 ) \
    MAKE_REGISTER( OTG_HCCHAR0 ) \
    MAKE_REGISTER( OTG_HCINT0 ) \
    MAKE_REGISTER( OTG_HCTSIZ0 ) \
    MAKE_REGISTER( OTG_HCUNTMSK0 ) \
    MAKE_REGISTER( OTG_HCDMA0 ) \
    MAKE_REGISTER( OTG_HCCHAR1 ) \
    MAKE_REGISTER( OTG_HCINT1 ) \
    MAKE_REGISTER( OTG_HCINTMSK1 ) \
    MAKE_REGISTER( OTG_HCTSIZ1 ) \
    MAKE_REGISTER( OTG_HCTSIZ2 ) \
    MAKE_REGISTER( OTG_HCTSIZ3 ) \
    MAKE_REGISTER( OTG_HCTSIZ4 ) \
    MAKE_REGISTER( OTG_HCTSIZ5 ) \
    MAKE_REGISTER( OTG_HCTSIZ6 ) \
    MAKE_REGISTER( OTG_HCTSIZ7 ) \
    MAKE_REGISTER( OTG_HCTSIZ8 ) \
    MAKE_REGISTER( OTG_HCTSIZ9 ) \
    MAKE_REGISTER( OTG_HCTSIZ10 ) \
    MAKE_REGISTER( OTG_HCTSIZ11 ) \
    MAKE_REGISTER( OTG_HCTSIZ12 ) \
    MAKE_REGISTER( OTG_HCTSIZ13 ) \
    MAKE_REGISTER( OTG_HCTSIZ14 ) \
    MAKE_REGISTER( OTG_HCTSIZ15 ) \
    MAKE_REGISTER( OTG_HCTSIZ16 ) \
    MAKE_REGISTER( OTG_HCTSIZ17 ) \
    MAKE_REGISTER( OTG_HCTSIZ18 ) \
    MAKE_REGISTER( OTG_HCTSIZ19 ) \
    MAKE_REGISTER( OTG_HCTSIZ20 ) \
    MAKE_REGISTER( OTG_HCTSIZ21 ) \
    MAKE_REGISTER( OTG_HCTSIZ22 ) \
    MAKE_REGISTER( OTG_HCTSIZ23 ) \
    MAKE_REGISTER( OTG_HCTSIZ24 ) \
    MAKE_REGISTER( OTG_HCTSIZ25 ) \
    MAKE_REGISTER( OTG_HCTSIZ26 ) \
    MAKE_REGISTER( OTG_HCTSIZ27 ) \
    MAKE_REGISTER( OTG_HCTSIZ28 ) \
    MAKE_REGISTER( OTG_HCTSIZ29 ) \
    MAKE_REGISTER( OTG_HCTSIZ30 ) \
    MAKE_REGISTER( OTG_HCTSIZ31 ) \
    MAKE_REGISTER( OTG_HCTSIZ32 ) \
    MAKE_REGISTER( OTG_HCTSIZ33 ) \
    MAKE_REGISTER( OTG_HCTSIZ34 ) \
    MAKE_REGISTER( OTG_HCTSIZ35 ) \
    MAKE_REGISTER( OTG_HCTSIZ36 ) \
    MAKE_REGISTER( OTG_HCTSIZ37 ) \
    MAKE_REGISTER( OTG_HCTSIZ38 ) \
    MAKE_REGISTER( OTG_HCTSIZ39 ) \
    MAKE_REGISTER( OTG_HCTSIZ40 ) \
    MAKE_REGISTER( OTG_HCTSIZ41 ) \
    MAKE_REGISTER( OTG_HCTSIZ42 ) \
    MAKE_REGISTER( OTG_HCTSIZ43 ) \
    MAKE_REGISTER( OTG_HCTSIZ44 ) \
    MAKE_REGISTER( OTG_HCTSIZ45 ) \
    MAKE_REGISTER( OTG_HCTSIZ46 ) \
    MAKE_REGISTER( OTG_HCTSIZ47 ) \
    MAKE_REGISTER( OTG_HCTSIZ48 ) \
    MAKE_REGISTER( OTG_HCTSIZ49 ) \
    MAKE_REGISTER( OTG_HCTSIZ50 ) \
    MAKE_REGISTER( OTG_HCTSIZ51 ) \
    MAKE_REGISTER( OTG_HCTSIZ52 ) \
    MAKE_REGISTER( OTG_HCTSIZ53 ) \
    MAKE_REGISTER( OTG_HCTSIZ54 ) \
    MAKE_REGISTER( OTG_HCTSIZ55 ) \
    MAKE_REGISTER( OTG_HCTSIZ56 ) \
    MAKE_REGISTER( OTG_HCTSIZ57 ) \
    MAKE_REGISTER( OTG_HCTSIZ58 ) \
    MAKE_REGISTER( OTG_HCTSIZ59 ) \
    MAKE_REGISTER( OTG_HCTSIZ60 ) \
    MAKE_REGISTER( OTG_HCTSIZ61 ) \
    MAKE_REGISTER( OTG_HCTSIZ62 ) \
    MAKE_REGISTER( OTG_HCTSIZ63 ) \
    MAKE_REGISTER( OTG_HCTSIZ64 ) \
    MAKE_REGISTER( OTG_HCTSIZ65 ) \
    MAKE_REGISTER( OTG_HCTSIZ66 ) \
    MAKE_REGISTER( OTG_HCTSIZ67 ) \
    MAKE_REGISTER( OTG_HCTSIZ68 ) \
    MAKE_REGISTER( OTG_HCTSIZ69 ) \
    MAKE_REGISTER( OTG_HCTSIZ70 ) \
    MAKE_REGISTER( OTG_HCTSIZ71 ) \
    MAKE_REGISTER( OTG_HCTSIZ72 ) \
    MAKE_REGISTER( OTG_HCTSIZ73 ) \
    MAKE_REGISTER( OTG_HCTSIZ74 ) \
    MAKE_REGISTER( OTG_HCTSIZ75 ) \
    MAKE_REGISTER( OTG_HCTSIZ76 ) \
    MAKE_REGISTER( OTG_HCCHAR11 ) \
    MAKE_REGISTER( OTG_HCINTMSK11 ) \
    MAKE_REGISTER( OTG_HCCHAR15 ) \
    MAKE_REGISTER( OTG_HCSPLT15 ) \
    MAKE_REGISTER( OTG_HCINTMSK15 ) \
    MAKE_REGISTER( ETH_MACCR ) \
    MAKE_REGISTER( ETH_MACFFR ) \
    MAKE_REGISTER( ETH_MACHTHR ) \
    MAKE_REGISTER( ETH_MACHTLR ) \
    MAKE_REGISTER( ETH_MACMIIAR ) \
    MAKE_REGISTER( ETH_MACMIIDR ) \
    MAKE_REGISTER( ETH_MACFCR ) \
    MAKE_REGISTER( ETH_MACVLANTR ) \
    MAKE_REGISTER( ETH_MACRWUF ) \
    MAKE_REGISTER( ETH_MACPMTCSR ) \
    MAKE_REGISTER( ETH_MACDBGR ) \
    MAKE_REGISTER( ETH_MACSR ) \
    MAKE_REGISTER( ETH_MACIMR ) \
    MAKE_REGISTER( ETH_MACA0HR ) \
    MAKE_REGISTER( ETH_MACA0LR ) \
    MAKE_REGISTER( ETH_MACA1HR ) \
    MAKE_REGISTER( ETH_MACA1LR ) \
    MAKE_REGISTER( ETH_MACA2HR ) \
    MAKE_REGISTER( ETH_MACA2LR ) \
    MAKE_REGISTER( ETH_MACA3HR ) \
    MAKE_REGISTER( ETH_MACA3LR ) \
    MAKE_REGISTER( ETH_MMCCR ) \
    MAKE_REGISTER( ETH_MMCRIR ) \
    MAKE_REGISTER( ETH_MMCCTIR ) \
    MAKE_REGISTER( ETH_MMCRIMR ) \
    MAKE_REGISTER( ETH_MMCTIMR ) \
    MAKE_REGISTER( ETH_MMCTGFSCCR ) \
    MAKE_REGISTER( ETH_MMCTGFMSCCR ) \
    MAKE_REGISTER( ETH_MMCTGFCR ) \
    MAKE_REGISTER( ETH_MMCRFCECR ) \
    MAKE_REGISTER( ETH_MMCRFAECR ) \
    MAKE_REGISTER( ETH_MMCRGUFCR ) \
    MAKE_REGISTER( ETH_PTPTSCR ) \
    MAKE_REGISTER( ETH_PTPSSIR ) \
    MAKE_REGISTER( ETH_PTPTSHR ) \
    MAKE_REGISTER( ETH_PTPTSLR ) \
    MAKE_REGISTER( ETH_PTPTSHUR ) \
    MAKE_REGISTER( ETH_PTPTSLIR ) \
    MAKE_REGISTER( ETH_PTPTSAR ) \
    MAKE_REGISTER( ETH_PTPTTHR ) \
    MAKE_REGISTER( ETH_PTPTTLR ) \
    MAKE_REGISTER( ETH_PTPTSSR ) \
    MAKE_REGISTER( ETH_DMABMR ) \
    MAKE_REGISTER( ETH_DMATPDR ) \
    MAKE_REGISTER( ETH_DMARPDR ) \
    MAKE_REGISTER( ETH_DMARDLAR ) \
    MAKE_REGISTER( ETH_DMATDLAR ) \
    MAKE_REGISTER( ETH_DMASR ) \
    MAKE_REGISTER( ETH_DMAOMR ) \
    MAKE_REGISTER( ETH_DMAIER ) \
    MAKE_REGISTER( ETH_DMAMFBOCR ) \
    MAKE_REGISTER( ETH_DMARSWTR ) \
    MAKE_REGISTER( ETH_DMACHTDR ) \
    MAKE_REGISTER( ETH_DMACHRDR ) \
    MAKE_REGISTER( ETH_DMACHTBAR ) \
    MAKE_REGISTER( ETH_DMACHRBAR ) \
    MAKE_REGISTER( CEC_CR ) \
    MAKE_REGISTER( CEC_CFGR ) \
    MAKE_REGISTER( CEC_TXDR ) \
    MAKE_REGISTER( CEC_RXDR ) \
    MAKE_REGISTER( CEC_ISR ) \
    MAKE_REGISTER( CEC_IER ) \

#endif /* SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_REGISTERS_DEFS_H_ */
