/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions of base addresses for machine
 *
 * @file       oc_ba_defs.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_BA_DEFS_H_
#define SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_BA_DEFS_H_

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits for storing base address
 *
 * The definition stores number of bits that are required for storing base address.
 */
//==========================================================================================================================================
#define oC_BASE_ADDRESS_WIDTH           32

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of base addresses
 *
 * The definition contains list of base addresses. To add new base address to list:
 *
 * @code{.c}
       MAKE_BASE_ADDRESS( BA_NAME , ADDRESS , POWER_BASE_NAME,POWER_OFFSET_NAME,POWER_BIT_INDEX )\
   @endcode
 * Where:
 *          BA_NAME             - Name of a base address
 *          ADDRESS             - Address in memory for the start of the register map (base address)
 *          POWER_BASE_NAME     - Name of base from this list, that allow to enable power for the base (Use 'None' if not used)
 *          POWER_OFFSET_NAME   - Name of offset, that should be added to base address to receive full address of register for enabling power
 *                                for the register map. This should be defined in oc_rmaps_defs.h file (Use 'None' if not used)
 *          POWER_BIT_INDEX     - Index of bit in a power register to set for enabling this register map (Use 'None' if not used)
 */
//==========================================================================================================================================
#define oC_MACHINE_BA_LIST(MAKE_BASE_ADDRESS)   \
    MAKE_BASE_ADDRESS( QUADSPI       , 0xA0001000UL , None , None , None) \
    MAKE_BASE_ADDRESS( FMC           , 0xA0000000UL , RCC  , RCC_AHB3ENR , 0) \
    MAKE_BASE_ADDRESS( RNG           , 0x50060800UL , None , None , None) \
    MAKE_BASE_ADDRESS( HASH          , 0x50060400UL , None , None , None) \
    MAKE_BASE_ADDRESS( CRYP          , 0x50060000UL , None , None , None) \
    MAKE_BASE_ADDRESS( DCMI          , 0x50050000UL , None , None , None) \
    MAKE_BASE_ADDRESS( USB_OTG_FS    , 0x50000000UL , None , None , None) \
    MAKE_BASE_ADDRESS( USB_OTG_HS    , 0x40040000UL , None , None , None) \
    MAKE_BASE_ADDRESS( DMA2D         , 0x4002B000UL , None , None , None) \
    MAKE_BASE_ADDRESS( ETHERNET_MAC  , 0x40028000UL , RCC  , RCC_AHB1ENR , 25) \
    MAKE_BASE_ADDRESS( DMA2          , 0x40026400UL , None , None , None) \
    MAKE_BASE_ADDRESS( DMA1          , 0x40026000UL , None , None , None) \
    MAKE_BASE_ADDRESS( BKPSRAM       , 0x40024000UL , None , None , None) \
    MAKE_BASE_ADDRESS( FLASH         , 0x40023C00UL , None , None , None) \
    MAKE_BASE_ADDRESS( RCC           , 0x40023800UL , None , None , None) \
    MAKE_BASE_ADDRESS( CRC           , 0x40023000UL , None , None , None) \
    MAKE_BASE_ADDRESS( GPIOK         , 0x40022800UL , RCC  , RCC_AHB1ENR , 10) \
    MAKE_BASE_ADDRESS( GPIOJ         , 0x40022400UL , RCC  , RCC_AHB1ENR ,  9) \
    MAKE_BASE_ADDRESS( GPIOI         , 0x40022000UL , RCC  , RCC_AHB1ENR ,  8) \
    MAKE_BASE_ADDRESS( GPIOH         , 0x40021C00UL , RCC  , RCC_AHB1ENR ,  7) \
    MAKE_BASE_ADDRESS( GPIOG         , 0x40021800UL , RCC  , RCC_AHB1ENR ,  6) \
    MAKE_BASE_ADDRESS( GPIOF         , 0x40021400UL , RCC  , RCC_AHB1ENR ,  5) \
    MAKE_BASE_ADDRESS( GPIOE         , 0x40021000UL , RCC  , RCC_AHB1ENR ,  4) \
    MAKE_BASE_ADDRESS( GPIOD         , 0x40020C00UL , RCC  , RCC_AHB1ENR ,  3) \
    MAKE_BASE_ADDRESS( GPIOC         , 0x40020800UL , RCC  , RCC_AHB1ENR ,  2) \
    MAKE_BASE_ADDRESS( GPIOB         , 0x40020400UL , RCC  , RCC_AHB1ENR ,  1) \
    MAKE_BASE_ADDRESS( GPIOA         , 0x40020000UL , RCC  , RCC_AHB1ENR ,  0) \
    MAKE_BASE_ADDRESS( LCD_TFT       , 0x40016800UL , RCC  , RCC_APB2ENR , 26) \
    MAKE_BASE_ADDRESS( SAI2          , 0x40015C00UL , None , None , None) \
    MAKE_BASE_ADDRESS( SAI1          , 0x40015800UL , None , None , None) \
    MAKE_BASE_ADDRESS( SPI6          , 0x40015400UL , RCC  , RCC_APB2ENR,  21) \
    MAKE_BASE_ADDRESS( SPI5          , 0x40015000UL , RCC  , RCC_APB2ENR,  20) \
    MAKE_BASE_ADDRESS( TIM11         , 0x40014800UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM10         , 0x40014400UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM9          , 0x40014000UL , None , None , None) \
    MAKE_BASE_ADDRESS( EXTI          , 0x40013C00UL , None , None , None) \
    MAKE_BASE_ADDRESS( SYSCFG        , 0x40013800UL , RCC  , RCC_APB2ENR , 14) \
    MAKE_BASE_ADDRESS( SPI4          , 0x40013400UL , RCC  , RCC_APB2ENR , 13) \
    MAKE_BASE_ADDRESS( SPI1          , 0x40013000UL , RCC  , RCC_APB2ENR , 12) \
    MAKE_BASE_ADDRESS( SDMMC1        , 0x40012C00UL , RCC  , RCC_APB2ENR , 11) \
    MAKE_BASE_ADDRESS( ADC1          , 0x40012000UL , None , None , None) \
    MAKE_BASE_ADDRESS( ADC2          , 0x40012100UL , None , None , None) \
    MAKE_BASE_ADDRESS( ADC3          , 0x40012200UL , None , None , None) \
    MAKE_BASE_ADDRESS( ADC_COMMON    , 0x40012300UL , None , None , None) \
    MAKE_BASE_ADDRESS( USART6        , 0x40011400UL , RCC  , RCC_APB2ENR ,  5) \
    MAKE_BASE_ADDRESS( USART1        , 0x40011000UL , RCC  , RCC_APB2ENR ,  4) \
    MAKE_BASE_ADDRESS( TIM8          , 0x40010400UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM1          , 0x40010000UL , None , None , None) \
    MAKE_BASE_ADDRESS( UART8         , 0x40007C00UL , RCC  , RCC_APB1ENR , 31) \
    MAKE_BASE_ADDRESS( UART7         , 0x40007800UL , RCC  , RCC_APB1ENR , 30) \
    MAKE_BASE_ADDRESS( DAC           , 0x40007400UL , None , None , None) \
    MAKE_BASE_ADDRESS( PWR           , 0x40007000UL , None , None , None) \
    MAKE_BASE_ADDRESS( HDMI_CEC      , 0x40006C00UL , None , None , None) \
    MAKE_BASE_ADDRESS( CAN2          , 0x40006800UL , None , None , None) \
    MAKE_BASE_ADDRESS( CAN1          , 0x40006400UL , None , None , None) \
    MAKE_BASE_ADDRESS( I2C4          , 0x40006000UL , RCC  , RCC_APB1ENR , 24) \
    MAKE_BASE_ADDRESS( I2C3          , 0x40005C00UL , RCC  , RCC_APB1ENR , 23) \
    MAKE_BASE_ADDRESS( I2C2          , 0x40005800UL , RCC  , RCC_APB1ENR , 22) \
    MAKE_BASE_ADDRESS( I2C1          , 0x40005400UL , RCC  , RCC_APB1ENR , 21) \
    MAKE_BASE_ADDRESS( UART5         , 0x40005000UL , RCC  , RCC_APB1ENR , 20) \
    MAKE_BASE_ADDRESS( UART4         , 0x40004C00UL , RCC  , RCC_APB1ENR , 19) \
    MAKE_BASE_ADDRESS( USART3        , 0x40004800UL , RCC  , RCC_APB1ENR , 18) \
    MAKE_BASE_ADDRESS( USART2        , 0x40004400UL , RCC  , RCC_APB1ENR , 17) \
    MAKE_BASE_ADDRESS( SPDIFRX       , 0x40004000UL , None , None , None) \
    MAKE_BASE_ADDRESS( SPI3          , 0x40003C00UL , RCC  , RCC_APB1ENR,  15) \
    MAKE_BASE_ADDRESS( SPI2          , 0x40003800UL , RCC  , RCC_APB1ENR,  14) \
    MAKE_BASE_ADDRESS( IWDG          , 0x40003000UL , None , None , None) \
    MAKE_BASE_ADDRESS( WWDG          , 0x40002C00UL , None , None , None) \
    MAKE_BASE_ADDRESS( RTC_BKP       , 0x40002800UL , None , None , None) \
    MAKE_BASE_ADDRESS( LPTIM1        , 0x40002400UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM14         , 0x40002000UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM13         , 0x40001C00UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM12         , 0x40001800UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM7          , 0x40001400UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM6          , 0x40001000UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM5          , 0x40000C00UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM4          , 0x40000800UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM3          , 0x40000400UL , None , None , None) \
    MAKE_BASE_ADDRESS( TIM2          , 0x40000000UL , None , None , None) \

#endif /* SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_BA_DEFS_H_ */
