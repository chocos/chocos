/** @hideinitializer ****************************************************************************************************************************************
 *
 * @file       oc_machine_defs.h
 *
 * @brief      Contains definitions for the uC
 *
 * @author     Patryk Kubiak - (Created on: 11 kwi 2015 20:08:10) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup MDefs Machine Definitions (MDefs)
 * @ingroup PortableSpace
 * @{
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_PORTABLE_TI_LM4FH5QR_OC_MACHINE_DEFS_H_
#define SYSTEM_PORTABLE_TI_LM4FH5QR_OC_MACHINE_DEFS_H_

#include <oc_machines_list.h>
#include <oc_frequency.h>

/** @hideinitializer ****************************************************************************************************************************************
 * The section CPU definitions
 */
#define _________________________________________CPU_DEFINITIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Definition that contains name of the machine
 */
//==========================================================================================================================================
#define oC_MACHINE                  STM32F746NGH6

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Family of the machine
 */
//==========================================================================================================================================
#define oC_MACHINE_FAMILY           STM32F7

//==========================================================================================================================================
/** @hideinitializer
 * @hideinitializer
 * Name of the CPU CORTEX
 */
//==========================================================================================================================================
#define oC_MACHINE_CORTEX           ARM_Cortex_M7

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the internal oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERNAL_OSCILLATOR_FREQUENCY                oC_MHz(16)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Frequency of the hibernation oscillator
 */
//==========================================================================================================================================
#define oC_MACHINE_HIBERNATION_OSCILLATOR_FREQUENCY            oC_Hz(32768)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Maximum possible frequency for system clock
 */
//==========================================================================================================================================
#define oC_MACHINE_MAXIMUM_FREQUENCY                           oC_MHz(216)

/* END OF SECTION */
#undef  _________________________________________CPU_DEFINITIONS_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with DMA definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DMA_SECTION________________________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * The definition contain types of DMA signals for #oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST
 *
 * To add signal use:
 *              ADD_SIGNAL( NAME )\
 *                          NAME - name of the signal to add
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_SIGNAL_TYPE_LIST(ADD_SIGNAL) \
    ADD_SIGNAL( Rx ) \
    ADD_SIGNAL( Tx ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits needed for storing DMA signal type (length of oC_MACHINE_DMA_SIGNAL_TYPE_LIST)
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_SIGNAL_TYPE_WIDTH                4

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits needed for encoding value for DMA channel assignments
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_ENCODING_VALUE_WIDTH             4

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * The definition contains assignments of DMA channels.
 *
 * To add channel assignment use:
 *
 *          ADD_CHANNEL_ASSIGNMENT( DMA_REGISTER_MAP_NAME , REGISTER_MAP_NAME , SIGNAL_TYPE , TYPE , ENC )\
 *
 *                      where:
 *                                  DMA_CHANNEL           - name of DMA channel
 *                                  CHANNEL_NAME          - channel that is assigned to this (from UART for example)
 *                                  SIGNAL_TYPE           - type of the signal from #oC_MACHINE_DMA_SIGNAL_TYPE_LIST definition
 *                                  TYPE                  - indicate if particular peripheral uses a single request (S), burst request (B)
 *                                                          or both (SB)
 *                                  ENC                   - special value for register
 */
//==========================================================================================================================================
#define oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST(ADD_CHANNEL_ASSIGNMENT)   \
    ADD_CHANNEL_ASSIGNMENT( DMA0   , UART0        , Rx  , SB , 0)\
    ADD_CHANNEL_ASSIGNMENT( DMA1   , UART0        , Tx  ,  B , 0)\

#undef  _________________________________________DMA_SECTION________________________________________________________________________________

#endif /* SYSTEM_PORTABLE_TI_LM4FH5QR_OC_MACHINE_DEFS_H_ */
