/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions for interrupts module
 *
 * @file       oc_interrupts_defs.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_INTERRUPTS_DEFS_H_
#define SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_INTERRUPTS_DEFS_H_

#include <oc_1word.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of bits needed for storing interrupt priority
 *
 * This definition stores number of bits, that are used for storing priority of interrupts
 */
//==========================================================================================================================================
#define oC_MACHINE_PRIO_BITS                        4

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Name of default interrupt handler function
 */
//==========================================================================================================================================
#define oC_DEFAULT_INTERRUPT_HANDLER_NAME                   oC_DefaultInterrupt

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Lowest priority value
 */
//==========================================================================================================================================
#define oC_MAXIMUM_INTERRUPT_PRIORITY               0
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief The highest priority
 */
//==========================================================================================================================================
#define oC_MINIMUM_INTERRUPT_PRIORITY               0xFF

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief types of interrupts
 *
 * Each interrupt should be connected with some base address or defined as "System" interrupt. To distinguish interrupts connected to the same
 * base address, there is a interrupt type.
 *
 * This definition is a list of interrupt types needed for the the machine.
 *
 * To add new interrupt type, use:
 * @code{.c} ADD_INTERRUPT_TYPE( INTERRUPT_NAME )\ @endcode
 *
 * Where:
 *              INTERRUPT_NAME      - any word as name of the interrupt type
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERRUPTS_TYPES_LIST(ADD_INTERRUPT_TYPE)     \
    ADD_INTERRUPT_TYPE( NonMaskableInterrupt     )\
    ADD_INTERRUPT_TYPE( HardFault                )\
    ADD_INTERRUPT_TYPE( MemoryManagement         )\
    ADD_INTERRUPT_TYPE( BusFault                 )\
    ADD_INTERRUPT_TYPE( UsageFault               )\
    ADD_INTERRUPT_TYPE( SVCall                   )\
    ADD_INTERRUPT_TYPE( DebugMonitor             )\
    ADD_INTERRUPT_TYPE( PendSV                   )\
    ADD_INTERRUPT_TYPE( SysTick                  )\
    ADD_INTERRUPT_TYPE( PeripheralInterrupt      )\
    ADD_INTERRUPT_TYPE( ADCSequence0             )\
    ADD_INTERRUPT_TYPE( ADCSequence1             )\
    ADD_INTERRUPT_TYPE( ADCSequence2             )\
    ADD_INTERRUPT_TYPE( ADCSequence3             )\
    ADD_INTERRUPT_TYPE( TimerA                   )\
    ADD_INTERRUPT_TYPE( TimerB                   )\
    ADD_INTERRUPT_TYPE( SystemControl            )\
    ADD_INTERRUPT_TYPE( FlashMemCtlAndE2ROMCtl   )\
    ADD_INTERRUPT_TYPE( HibernationModule        )\
    ADD_INTERRUPT_TYPE( USB                      )\
    ADD_INTERRUPT_TYPE( Software                 )\
    ADD_INTERRUPT_TYPE( Error                    )\
    ADD_INTERRUPT_TYPE( SystemException          )\
    ADD_INTERRUPT_TYPE( WatchdogInterrupt        )\
    ADD_INTERRUPT_TYPE( PVD                      )\
    ADD_INTERRUPT_TYPE( TimeStamp                )\
    ADD_INTERRUPT_TYPE( RccGlobalInterrupt       )\
    ADD_INTERRUPT_TYPE( Stream0                  )\
    ADD_INTERRUPT_TYPE( Stream1                  )\
    ADD_INTERRUPT_TYPE( Stream2                  )\
    ADD_INTERRUPT_TYPE( Stream3                  )\
    ADD_INTERRUPT_TYPE( Stream4                  )\
    ADD_INTERRUPT_TYPE( Stream5                  )\
    ADD_INTERRUPT_TYPE( Stream6                  )\
    ADD_INTERRUPT_TYPE( Stream7                  )\
    ADD_INTERRUPT_TYPE( Tx                       )\
    ADD_INTERRUPT_TYPE( Rx0                      )\
    ADD_INTERRUPT_TYPE( Rx1                      )\
    ADD_INTERRUPT_TYPE( Sce                      )\
    ADD_INTERRUPT_TYPE( BreakInterrupt           )\
    ADD_INTERRUPT_TYPE( UpdateInterrupt          )\
    ADD_INTERRUPT_TYPE( TriggerInterrupt         )\
    ADD_INTERRUPT_TYPE( CaptureCompareInterrupt  )\
    ADD_INTERRUPT_TYPE( RtcAlarms                )\
    ADD_INTERRUPT_TYPE( Fsmc                     )\
    ADD_INTERRUPT_TYPE( WakeUp                   )\
    ADD_INTERRUPT_TYPE( OnTheGoEp1OutInterrupt )\
    ADD_INTERRUPT_TYPE( OnTheGoEp1InInterrupt  )\
    ADD_INTERRUPT_TYPE( OnTheGoWakeUpInterrupt )\
    ADD_INTERRUPT_TYPE( OnTheGoInterrupt       )\
    ADD_INTERRUPT_TYPE( OnTheGoInterruptWakeUp       )\
    ADD_INTERRUPT_TYPE( SPDIFRX                  )\
    ADD_INTERRUPT_TYPE( EXTI0 )\
    ADD_INTERRUPT_TYPE( EXTI1 )\
    ADD_INTERRUPT_TYPE( EXTI2 )\
    ADD_INTERRUPT_TYPE( EXTI3 )\
    ADD_INTERRUPT_TYPE( EXTI4 )\
    ADD_INTERRUPT_TYPE( EXTI5_9 )\
    ADD_INTERRUPT_TYPE( EXTI10_15 )\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of interrupts
 *
 * Each interrupt should be connected with some base address or defined as "System" interrupt. To distinguish interrupts connected to the same
 * base address, there is a interrupt type.
 *
 * This definition is a list of interrupts in the machine.
 *
 * To add new interrupt use:
 * @code{.c} ADD_INTERRUPT( BASE_NAME , INTERRUPT_TYPE_NAME , INTERRUPT_NUMBER , VECTOR_INDEX )\ @endcode
 *
 * Where:
 *              BASE_NAME               - Name of base address, that the interrupt is connected to. The base address with this name must exist in the oc_ba_defs.h file
 *              INTERRUPT_TYPE_NAME     - Name of type from #oC_MACHINE_INTERRUPTS_TYPES_LIST definition. Helps to distinguish more interrupts on the same base address
 *              INTERRUPT_NUMBER        - Any number of interrupt (needed for CMSIS)
 *              VECTOR_INDEX            - Index of this interrupt in interrupt vector
 */
//==========================================================================================================================================
#define oC_MACHINE_INTERRUPTS_LIST(ADD_INTERRUPT) \
    ADD_INTERRUPT( System           , NonMaskableInterrupt        , -14 , 0x008 / 4 )\
    ADD_INTERRUPT( System           , HardFault                   , -13 , 0x00C / 4 )\
    ADD_INTERRUPT( System           , MemoryManagement            , -12 , 0x010 / 4 )\
    ADD_INTERRUPT( System           , BusFault                    , -11 , 0x014 / 4 )\
    ADD_INTERRUPT( System           , UsageFault                  , -10 , 0x018 / 4 )\
    ADD_INTERRUPT( System           , SVCall                      , -5  , 0x02C / 4 )\
    ADD_INTERRUPT( System           , DebugMonitor                , -4  , 0x030 / 4 )\
    ADD_INTERRUPT( System           , PendSV                      , -2  , 0x038 / 4 )\
    ADD_INTERRUPT( System           , SysTick                     , -1  , 0x03C / 4 )\
    ADD_INTERRUPT( System           , WatchdogInterrupt           ,  0  , 0x040 / 4 )\
    ADD_INTERRUPT( System           , PVD                         ,  1  , 0x044 / 4 )\
    ADD_INTERRUPT( System           , TimeStamp                   ,  2  , 0x048 / 4 )\
    ADD_INTERRUPT( EXTI             , EXTI0                       ,  6  , 0x058 / 4 )\
    ADD_INTERRUPT( EXTI             , EXTI1                       ,  7  , 0x05C / 4 )\
    ADD_INTERRUPT( EXTI             , EXTI2                       ,  8  , 0x060 / 4 )\
    ADD_INTERRUPT( EXTI             , EXTI3                       ,  9  , 0x064 / 4 )\
    ADD_INTERRUPT( EXTI             , EXTI4                       , 10  , 0x068 / 4 )\
    ADD_INTERRUPT( DMA1             , Stream0                     , 11  , 0x06C / 4 )\
    ADD_INTERRUPT( DMA1             , Stream1                     , 12  , 0x070 / 4 )\
    ADD_INTERRUPT( DMA1             , Stream2                     , 13  , 0x074 / 4 )\
    ADD_INTERRUPT( DMA1             , Stream3                     , 14  , 0x078 / 4 )\
    ADD_INTERRUPT( DMA1             , Stream4                     , 15  , 0x07C / 4 )\
    ADD_INTERRUPT( DMA1             , Stream5                     , 16  , 0x080 / 4 )\
    ADD_INTERRUPT( DMA1             , Stream6                     , 17  , 0x084 / 4 )\
    ADD_INTERRUPT( ADC_COMMON       , PeripheralInterrupt         , 18  , 0x088 / 4 )\
    ADD_INTERRUPT( CAN1             , Tx                          , 19  , 0x08C / 4 )\
    ADD_INTERRUPT( CAN1             , Rx0                         , 20  , 0x090 / 4 )\
    ADD_INTERRUPT( CAN1             , Rx1                         , 21  , 0x094 / 4 )\
    ADD_INTERRUPT( CAN1             , Sce                         , 22  , 0x098 / 4 )\
    ADD_INTERRUPT( EXTI             , EXTI5_9                     , 23  , 0x09C / 4 )\
    ADD_INTERRUPT( TIM1             , BreakInterrupt              , 24  , 0x0A0 / 4 )\
    ADD_INTERRUPT( TIM1             , UpdateInterrupt             , 25  , 0x0A4 / 4 )\
    ADD_INTERRUPT( TIM1             , TriggerInterrupt            , 26  , 0x0A8 / 4 )\
    ADD_INTERRUPT( TIM1             , CaptureCompareInterrupt     , 27  , 0x0AC / 4 )\
    ADD_INTERRUPT( TIM2             , PeripheralInterrupt         , 28  , 0x0B0 / 4 )\
    ADD_INTERRUPT( TIM3             , PeripheralInterrupt         , 29  , 0x0B4 / 4 )\
    ADD_INTERRUPT( TIM4             , PeripheralInterrupt         , 30  , 0x0B8 / 4 )\
    ADD_INTERRUPT( I2C1             , PeripheralInterrupt         , 31  , 0x0BC / 4 )\
    ADD_INTERRUPT( I2C1             , Error                       , 32  , 0x0C0 / 4 )\
    ADD_INTERRUPT( I2C2             , PeripheralInterrupt         , 33  , 0x0C4 / 4 )\
    ADD_INTERRUPT( I2C2             , Error                       , 34  , 0x0C8 / 4 )\
    ADD_INTERRUPT( SPI1             , PeripheralInterrupt         , 35  , 0x0CC / 4 )\
    ADD_INTERRUPT( SPI2             , PeripheralInterrupt         , 36  , 0x0D0 / 4 )\
    ADD_INTERRUPT( USART1           , PeripheralInterrupt         , 37  , 0x0D4 / 4 )\
    ADD_INTERRUPT( USART2           , PeripheralInterrupt         , 38  , 0x0D8 / 4 )\
    ADD_INTERRUPT( USART3           , PeripheralInterrupt         , 39  , 0x0DC / 4 )\
    ADD_INTERRUPT( EXTI             , EXTI10_15                   , 40  , 0x0E0 / 4 )\
    ADD_INTERRUPT( System           , RtcAlarms                   , 41  , 0x0E4 / 4 )\
    ADD_INTERRUPT( USB_OTG_FS       , OnTheGoInterruptWakeUp      , 42  , 0x0E8 / 4 )\
    ADD_INTERRUPT( TIM8             , BreakInterrupt              , 43  , 0x0EC / 4 )\
    ADD_INTERRUPT( TIM8             , UpdateInterrupt             , 44  , 0x0F0 / 4 )\
    ADD_INTERRUPT( TIM8             , TriggerInterrupt            , 45  , 0x0F4 / 4 )\
    ADD_INTERRUPT( TIM8             , CaptureCompareInterrupt     , 46  , 0x0F8 / 4 )\
    ADD_INTERRUPT( DMA1             , Stream7                     , 47  , 0x0FC / 4 )\
    ADD_INTERRUPT( System           , Fsmc                        , 48  , 0x100 / 4 )\
    ADD_INTERRUPT( SDMMC1           , PeripheralInterrupt         , 49  , 0x104 / 4 )\
    ADD_INTERRUPT( TIM5             , PeripheralInterrupt         , 50  , 0x108 / 4 )\
    ADD_INTERRUPT( SPI3             , PeripheralInterrupt         , 51  , 0x10C / 4 )\
    ADD_INTERRUPT( UART4            , PeripheralInterrupt         , 52  , 0x110 / 4 )\
    ADD_INTERRUPT( UART5            , PeripheralInterrupt         , 53  , 0x114 / 4 )\
    ADD_INTERRUPT( TIM6             , PeripheralInterrupt         , 54  , 0x118 / 4 )\
    ADD_INTERRUPT( TIM7             , PeripheralInterrupt         , 55  , 0x11C / 4 )\
    ADD_INTERRUPT( DMA2             , Stream0                     , 56  , 0x120 / 4 )\
    ADD_INTERRUPT( DMA2             , Stream1                     , 57  , 0x124 / 4 )\
    ADD_INTERRUPT( DMA2             , Stream2                     , 58  , 0x128 / 4 )\
    ADD_INTERRUPT( DMA2             , Stream3                     , 59  , 0x12C / 4 )\
    ADD_INTERRUPT( DMA2             , Stream4                     , 60  , 0x130 / 4 )\
    ADD_INTERRUPT( ETHERNET_MAC     , PeripheralInterrupt         , 61  , 0x134 / 4 )\
    ADD_INTERRUPT( ETHERNET_MAC     , WakeUp                      , 62  , 0x138 / 4 )\
    ADD_INTERRUPT( CAN2             , Tx                          , 63  , 0x13C / 4 )\
    ADD_INTERRUPT( CAN2             , Rx0                         , 64  , 0x140 / 4 )\
    ADD_INTERRUPT( CAN2             , Rx1                         , 65  , 0x144 / 4 )\
    ADD_INTERRUPT( CAN2             , Sce                         , 66  , 0x148 / 4 )\
    ADD_INTERRUPT( USB_OTG_FS       , OnTheGoInterrupt            , 67  , 0x14C / 4 )\
    ADD_INTERRUPT( DMA2             , Stream5                     , 68  , 0x150 / 4 )\
    ADD_INTERRUPT( DMA2             , Stream6                     , 69  , 0x154 / 4 )\
    ADD_INTERRUPT( DMA2             , Stream7                     , 70  , 0x158 / 4 )\
    ADD_INTERRUPT( USART6           , PeripheralInterrupt         , 71  , 0x15C / 4 )\
    ADD_INTERRUPT( I2C3             , PeripheralInterrupt         , 72  , 0x160 / 4 )\
    ADD_INTERRUPT( I2C3             , Error                       , 73  , 0x164 / 4 )\
    ADD_INTERRUPT( USB_OTG_HS       , OnTheGoEp1OutInterrupt      , 74  , 0x168 / 4 )\
    ADD_INTERRUPT( USB_OTG_HS       , OnTheGoEp1InInterrupt       , 75  , 0x16C / 4 )\
    ADD_INTERRUPT( USB_OTG_HS       , OnTheGoWakeUpInterrupt      , 76  , 0x170 / 4 )\
    ADD_INTERRUPT( USB_OTG_HS       , OnTheGoInterrupt            , 77  , 0x174 / 4 )\
    ADD_INTERRUPT( DCMI             , PeripheralInterrupt         , 78  , 0x178 / 4 )\
    ADD_INTERRUPT( CRYP             , PeripheralInterrupt         , 79  , 0x17C / 4 )\
    ADD_INTERRUPT( UART7            , PeripheralInterrupt         , 82  , 0x188 / 4 )\
    ADD_INTERRUPT( UART8            , PeripheralInterrupt         , 83  , 0x18C / 4 )\
    ADD_INTERRUPT( SPI4             , PeripheralInterrupt         , 84  , 0x190 / 4 )\
    ADD_INTERRUPT( SPI5             , PeripheralInterrupt         , 85  , 0x194 / 4 )\
    ADD_INTERRUPT( SPI6             , PeripheralInterrupt         , 86  , 0x198 / 4 )\
    ADD_INTERRUPT( SAI1             , PeripheralInterrupt         , 87  , 0x19C / 4 )\
    ADD_INTERRUPT( LCD_TFT          , PeripheralInterrupt         , 88  , 0x1A0 / 4 )\
    ADD_INTERRUPT( LCD_TFT          , Error                       , 89  , 0x1A4 / 4 )\
    ADD_INTERRUPT( DMA2D            , PeripheralInterrupt         , 90  , 0x1A8 / 4 )\
    ADD_INTERRUPT( SAI2             , PeripheralInterrupt         , 91  , 0x1AC / 4 )\
    ADD_INTERRUPT( QUADSPI          , PeripheralInterrupt         , 92  , 0x1B0 / 4 )\
    ADD_INTERRUPT( LPTIM1           , PeripheralInterrupt         , 93  , 0x1B4 / 4 )\
    ADD_INTERRUPT( HDMI_CEC         , PeripheralInterrupt         , 94  , 0x1B8 / 4 )\
    ADD_INTERRUPT( I2C4             , PeripheralInterrupt         , 95  , 0x1BC / 4 )\
    ADD_INTERRUPT( I2C4             , Error                       , 96  , 0x1C0 / 4 )\
    ADD_INTERRUPT( System           , SPDIFRX                     , 97  , 0x1C4 / 4 )\

#endif /* SYSTEM_PORTABLE_INC_ST_STM32F7_STM32F746NGH6_OC_INTERRUPTS_DEFS_H_ */
