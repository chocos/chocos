/** ****************************************************************************************************************************************
 *
 * @brief      Interface for Machine Base Addresses (BA) module
 *
 * @file       oc_ba.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup BA BA - Base Addresses
 * @ingroup PortableSpace
 * @brief Module for managing base addresses
 *
 * @par
 * It contains definitions of base addresses with all parameters that are connected to it. All types here are created by using definitions from
 * the oc_ba_defs.h file, that should be defined for each machine family. To add new base address or change an existing entry, edit the oc_ba_defs.h file.
 *
 * @par
 * There is a special definition for each base address, that provide information about the **Power Register**. It is special register, that allows
 * to enable power for a part of memory, that is started at the given base address. The system is enabling power by setting a bit in the given Power Register, that
 * is defined as: power base address, power offset and power bit.
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_BA_H_
#define SYSTEM_PORTABLE_INC_OC_BA_H_

#include <oc_1word.h>
#include <oc_ba_defs.h>
#include <oc_rmaps.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup BA
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns base address
 *
 * The macro is for returning base address from base address name. The value is stored in #oC_BaseAddress_t type.
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 */
//==========================================================================================================================================
#define oC_BaseAddress_(BASE_NAME)              oC_1WORD_FROM_2(oC_BaseAddress_ , BASE_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns base address for The Power Register
 *
 * The macro is for returning power base address from base address name. The value is stored in #oC_PowerBaseAddress_t type.
 *
 * The Power Register is a special register, that allows to enable power for part of memory. The power base address is base address of this register.
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 */
//==========================================================================================================================================
#define oC_PowerBaseAddress_(BASE_NAME)         oC_1WORD_FROM_2(oC_PowerBaseAddress_ , BASE_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns offset for The Power Register
 *
 * The macro is for returning offset for base address name. The value is stored in #oC_PowerOffset_t type.
 *
 * The Power Register is a special register, that allows to enable power for part of memory. The power offset is base address of this register.
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 */
//==========================================================================================================================================
#define oC_PowerOffset_(BASE_NAME)              oC_1WORD_FROM_2(oC_PowerOffset_ , BASE_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns power bit in The Power Register
 *
 * The macro is for returning bit index from base address name. The value is stored in #oC_PowerBit_t type.
 *
 * The Power Register is a special register, that allows to enable power for part of memory. The power bit is index of bit in The Power Register, that
 * should be set for enabling the power for the part of memory, where the BASE is situated.
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 */
//==========================================================================================================================================
#define oC_PowerBit_(BASE_NAME)                 oC_1WORD_FROM_2(oC_PowerBit_ , BASE_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Mask for BaseAddress type
 *
 * The macro returns mask with all bits used by #oC_BaseAddress_t type set.
 */
//==========================================================================================================================================
#define oC_BaseAddressMask                      ( ( 1 << (oC_BASE_ADDRESS_WIDTH) ) - 1 )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if power bit is configured in definition file
 */
//==========================================================================================================================================
#define oC_PowerBit_Exist(PowerBit)           ((PowerBit) < 0xFF)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if power register base address is configured in definition file
 */
//==========================================================================================================================================
#define oC_PowerBaseAddress_Exist(Base)         (((oC_BaseAddress_t)(Base)) != oC_BaseAddress_(None))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if power register offset is configured in definition file
 */
//==========================================================================================================================================
#define oC_PowerOffset_Exist(Offset)            (((oC_RegisterOffset_t)(Offset)) != oC_RegisterOffset_(None))

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if power register bit exist for base with name
 */
//==========================================================================================================================================
#define oC_PowerBit_ExistFor(BASE_NAME)         (oC_PowerBit_(BASE_NAME) != 0xFF)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if power register base address exist for base with name
 */
//==========================================================================================================================================
#define oC_PowerBaseAddress_ExistFor(BASE_NAME) (oC_PowerBaseAddress_(BASE_NAME) != oC_BaseAddress_(None))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if power register offset exist for base with name
 */
//==========================================================================================================================================
#define oC_PowerOffset_ExistFor(BASE_NAME)      (oC_PowerOffset_(BASE_NAME) != oC_RegisterOffset_(None))

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup BA
//! @{

//==========================================================================================================================================
/**
 * @brief type for storing base address
 *
 * The type is designed for storing base addresses. It also contains 2 special values:
 * - #oC_BaseAddress_None
 * - #oC_BaseAddress_System
 *
 * If you know base address name from the machine documentation, you can use it to receive value from this type by using macro #oC_BaseAddress_
 */
//==========================================================================================================================================
typedef enum
{
    oC_BaseAddress_None   = 0    ,  //!< Value for setting when none of base address was set
    oC_BaseAddress_System = 0xFF ,  //!< Value for marking system base addresses. It is used for example in interrupts vector
#define MAKE_BASE_ADDRESS(BASE_NAME,BASE_ADDRESS,POWER_BASE_NAME,POWER_OFFSET_NAME,POWER_BIT_INDEX, ...)   oC_BaseAddress_(BASE_NAME) = BASE_ADDRESS ,
    oC_MACHINE_BA_LIST(MAKE_BASE_ADDRESS)
#undef MAKE_BASE_ADDRESS
} oC_BaseAddress_t;

//==========================================================================================================================================
/**
 * @brief stores base address of the Power Register
 *
 * The type is for storing base address of the power register. Value of this type is set from the #oC_BaseAddress_t type.
 *
 * @see oC_PowerBaseAddress_
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_BASE_ADDRESS(BASE_NAME,BASE_ADDRESS,POWER_BASE_NAME,POWER_OFFSET_NAME,POWER_BIT_INDEX, ...)   oC_PowerBaseAddress_(BASE_NAME) = oC_BaseAddress_(POWER_BASE_NAME) ,
    oC_MACHINE_BA_LIST(MAKE_BASE_ADDRESS)
#undef MAKE_BASE_ADDRESS
} oC_PowerBaseAddress_t;

//==========================================================================================================================================
/**
 * @brief stores offset of the Power Register
 *
 * The type is for storing offset of the power register.
 *
 * @see oC_PowerOffset_
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_BASE_ADDRESS(BASE_NAME,BASE_ADDRESS,POWER_BASE_NAME,POWER_OFFSET_NAME,POWER_BIT_INDEX, ...)   oC_PowerOffset_(BASE_NAME) = oC_RegisterOffset_(POWER_OFFSET_NAME) ,
    oC_MACHINE_BA_LIST(MAKE_BASE_ADDRESS)
#undef MAKE_BASE_ADDRESS
} oC_PowerOffset_t;

//==========================================================================================================================================
/**
 * @brief stores number of bit in the Power Register
 *
 * The type is for storing index of bit in the Power Register, that need to be set to enable memory started at the given base address name.
 *
 * @see oC_PowerBit_
 */
//==========================================================================================================================================
typedef enum
{
#define None            0xFF
#define MAKE_BASE_ADDRESS(BASE_NAME,BASE_ADDRESS,POWER_BASE_NAME,POWER_OFFSET_NAME,POWER_BIT_INDEX, ...)   oC_PowerBit_(BASE_NAME) = POWER_BIT_INDEX ,
    oC_MACHINE_BA_LIST(MAKE_BASE_ADDRESS)
#undef MAKE_BASE_ADDRESS
#undef None
} oC_PowerBit_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_OC_BA_H_ */
