/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions for MCS module
 *
 * @file       oc_mcs_defs.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_MCS_ARM_ARM_CORTEX_M7_OC_MCS_DEFS_H_
#define SYSTEM_PORTABLE_INC_MCS_ARM_ARM_CORTEX_M7_OC_MCS_DEFS_H_

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Definition of stack direction
 *
 * This definition should be true, when the stack push operation decrements pointer
 */
//==========================================================================================================================================
#define oC_MCS_STACK_PUSH_DECREMENTS_POINTER        true

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Definition of memory alignment
 *
 * Number of bytes that the memory is aligned
 */
//==========================================================================================================================================
#define oC_MCS_MEMORY_ALIGNMENT                     4

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Definition of stack memory alignment
 *
 * Number of bytes that the stack memory is aligned
 */
//==========================================================================================================================================
#define oC_MCS_STACK_MEMORY_ALIGNMENT               8

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Initial value for the xPSR register
 */
//==========================================================================================================================================
#define oC_MCS_xPSR_INITIAL_VALUE                   0x01000000

#endif /* SYSTEM_PORTABLE_INC_MCS_ARM_ARM_CORTEX_M7_OC_MCS_DEFS_H_ */
