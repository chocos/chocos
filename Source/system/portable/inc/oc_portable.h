/** ****************************************************************************************************************************************
 *
 * @file       oc_portable.h
 *
 * @brief      The file with an interface for the portable layer
 *
 * @author     Patryk Kubiak - (Created on: 29 kwi 2015 18:32:16) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup PortableSpace Portable Space
 * @brief The portable space of the system
 * 
 * @defgroup LowLevelDrivers Low Level Drivers
 * @ingroup PortableSpace
 * @brief The list of low level drivers 
 * 
 ******************************************************************************************************************************************/


#ifndef INC_OC_PORTABLE_H_
#define INC_OC_PORTABLE_H_

#include <oc_mem_lld.h>

/** ========================================================================================================================================
 *
 *              The section with with portable architecture description
 *
 *  ======================================================================================================================================*/
#define _________________________________________ARCHITECTURE_DESCRIPTION_SECTION___________________________________________________________

/**
 * @page PortableSpace Portable Space
 *
 * @par
 * The Portable Space is the layer, that depends of the target machine architecture. It contains definitions of registers, available resources,
 * memory map, operations on machine registers, and everything, that is depended of the target architecture. The main purpose of the system
 * is to make this layer as small as possible. The diagram below represents architecture of this layer:
 *
 * ![Portable Space Architecture](Portable_Architecture.PNG)
 *
 * @subpage LLD
 *
 * @page LLD Low Level Driver (LLD)
 *
 * @section Introduction
 *
 * @par
 * The LLD is a layer of @link PortableSpace Portable Space @endlink that provides interface for machine independent modules for using peripherals,
 * registers, memory, interrupts and more without knowledge about selected device. Each function, type and macro that is defined as part of the
 * **LLD** layer must be defined also in other machines in the same type. If some functionality should be visible only in one machine should
 * be defined as @subpage MSLLD . It is a part of the layer that can be used only in other modules from the **LLD** layer in the same machine.
 * These functions cannot be used outside of the @link PortableSpace Portable Space @endlink and must not be defined for other machines.
 *
 * @par
 * The layer consist of modules, that are created for drivers from the @link CoreSpace Core Space. @endlink Each module is named as his
 * driver equivalent but with **LLD** in name, for example #oC_GPIO_LLD_SetSpeed function is **LLD** equivalent for @link GPIO @endlink driver.
 *
 * @par
 * Each module contain at least following files:
 *
 * File Name                    | Description
 * -----------------------------|----------------------------------------------------
 *  oc_[driver_name]_lld.h      | interface for the driver
 *  oc_[driver_name]_mslld.h    | machine specific interface for the driver
 *  oc_[driver_name]_lld.c      | sources for LLD functions
 *  oc_[driver_name]_mslld.c    | machine specific function sources
 *
 * @par
 * The **oc_[driver_name]_lld.h** file is the main module header. It is common for all machines, and it should contain all common definitions,
 * types, and functions that are required by both of @link CoreSpace The Core Space @endlink and @link PortableSpace The Portable Space. @endlink
 * Each function that is defined in this header, must exists for each machine, also if it will be just empty wrapper. Moreover, functions prototypes
 * in this file should also contain common part of a function description. Note, that in this file cannot be defined any static inline function,
 * because it is common file for each machine, and there is a possibility, that some of architectures will not handle it in the future.
 * Each low level driver should contain at least following functions:
 *
 * @code{.c}
 * oC_ErrorCode_t oC_[DN]_LLD_TurnOnDriver ( void );
 * oC_ErrorCode_t oC_[DN]_LLD_TurnOffDriver( void );
 * @endcode
 *
 * @par
 * The **oc_[driver_name]_mslld.h** file is the family specific header with functions prototypes that exists only in the specific family. It must
 * not exists in other machines so it can be used only in other modules from the same machine. Each type, definition, macro and function prototype
 * that is defined in this file, must contain **MSLLD** prefix in the name. It should include main LLD header. To get more, see @link MSLLD @endlink
 *
 * @par
 * The **oc_[driver_name]_lld.c** file is the family specific file, that contains sources for the LLD interface functions. If some function
 * cannot be written common for all machines in the family, then it should use machine definitions from the @link Machine @endlink module to
 * ensure, that part of code will be executed only on the target machine.
 *
 * @par
 * The **oc_[driver_name]_mslld.c** file is the family specific file, that contains sources of functions, that are used only in the given
 * architecture. There is no need to implement these functions for each machine.
 *
 * @par
 * Bellow you can find a list of available modules in the **LLD** layer:
 *
 * - @subpage MEM-LLD @endlink
 * - @subpage CLOCK-LLD @endlink
 * - @subpage SYS-LLD @endlink
 * - @subpage GPIO-LLD @endlink
 * - @subpage TIMER-LLD @endlink
 * - @subpage DMA-LLD @endlink
 * - @subpage SPI-LLD @endlink
 * - @subpage UART-LLD @endlink
 *
 * @section MSLLD Machine Specific Low Level Driver (MSLLD)
 *
 * This is the part of @link LLD @endlink layer. It contains functions that are machine specific. All types, definitions and functions from
 * this layer should contain **MSLLD** in their names.
 */

#undef  _________________________________________ARCHITECTURE_DESCRIPTION_SECTION___________________________________________________________

#endif /* INC_OC_PORTABLE_H_ */
