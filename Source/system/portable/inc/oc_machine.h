/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the machine module
 *
 * @file       oc_machine.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Machine Machine specific functions module
 * @ingroup PortableSpace
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_MACHINE_H_
#define SYSTEM_PORTABLE_INC_OC_MACHINE_H_

#include <stdbool.h>
#include <oc_stdtypes.h>
#include <oc_rmaps.h>
#include <oc_channels.h>
#include <oc_pins.h>
#include <oc_ba.h>
#include <oc_registers.h>
#include <oc_assert.h>
#include <oc_mcs.h>
#include <oc_machine_defs.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Machine
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of DMA signal type for #oC_Machine_DmaSignalType_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaSignalType_(NAME)                 oC_1WORD_FROM_2(oC_Machine_DmaSignalType_ , NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of DMA channel assignments for #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC , ...)              oC_1WORD_FROM_6(oC_Machine_DmaChannelAssignment_ , DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns index of DMA channel assignments
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignmentIndex_( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC , ...)         oC_1WORD_FROM_6(oC_Machine_DmaChannelAssignmentIndex_ , DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief allows for accessing registers in dynamic mode
 *
 * The macro is for accessing registers in full dynamic mode (base address and register offset is given as variable). Example of usage:
 * @code{.c}
   oC_Channel_t         channel     = oC_Channel_UART0;
   oC_BaseAddress_t     baseAddress = oC_Channel_GetBaseAddress(channel);
   oC_RegisterOffset_t  offset      = oC_RegisterOffset_(UARTDR);
   oC_Machine_RegisterIndirectDynamic(baseAddress,offset) = 0;
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_RegisterIndirectDynamic(BaseAddress , RegisterOffset)      (*((oC_Register_t*)(((void*)BaseAddress)+((oC_UInt_t)RegisterOffset))))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief allows for accessing registers using register offset name
 *
 * The macro is for accessing registers by register offset name. Example of usage:
 * @code{.c}
   oC_Channel_t     channel     = oC_Channel_UART0;
   oC_BaseAddress_t baseAddress = oC_Channel_GetBaseAddress(channel);

   oC_Machine_RegisterIndirectStaticOffset(baseAddress,UARTDR)->Value = 0;
 * @endcode
 */
//==========================================================================================================================================
#define oC_Machine_RegisterIndirectStaticOffset(BaseAddress , REGISTER_NAME)  oC_RegisterByBaseAddress(BaseAddress,REGISTER_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief allows for accessing registers using register offset name and base address name
 *
 * The macro is for accessing registers by register offset name and base address name. Example of usage:
 * @code{.c}
   oC_Machine_RegisterIndirectStatic(UART0,UARTDR)->Value = 0;
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_RegisterIndirectStatic(BASE_NAME,REGISTER_NAME)            oC_Register(BASE_NAME,REGISTER_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief allows for accessing registers using channel and offset name
 *
 * The macro is for accessing registers using channel and register name. Example of usage:
 * @code{.c}
    oC_Channel_t channel = oC_Channel_UART0;
    oC_Machine_Register(channel,UARTDR)->Value = 0;
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_Register(Channel,REGISTER_NAME)                            oC_Machine_RegisterIndirectStaticOffset(oC_Channel_GetBaseAddress(Channel),REGISTER_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns register address according to the channel and register name
 *
 * Example of usage:
 * @code{.c}
   oC_UART_Channel_t channel = oC_Channel_UART0;
   void * registerAddress    = oC_Machine_GetRegisterAddress(channel,UART_DR);
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_GetRegisterAddress(Channel,REGISTER_NAME)                  ((void*)(oC_Channel_GetBaseAddress(Channel) + oC_RegisterOffset_(REGISTER_NAME)))

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if register is writable
 *
 * The macro is for checking if register with the given name is possible for write. Example:
 * @code{.c}
    if(oC_Machine_IsRegisterWritable(UARTDR))
    {
        printf("UARTDR register is writable\n\r");
    }
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_IsRegisterWritable(REGISTER_NAME)                          (oC_RegisterAccess_(REGISTER_NAME) & oC_Access_(W))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if register is readable
 *
 * The macro is for checking if register with the given name is possible for read. Example:
 * @code{.c}
    if(oC_Machine_IsRegisterReadable(UARTDR))
    {
        printf("UARTDR register is readable\n\r");
    }
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_IsRegisterReadable(REGISTER_NAME)                          (oC_RegisterAccess_(REGISTER_NAME) & oC_Access_(R))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief macro for writing registers by using register name
 *
 * The macro allows to write machine register that is given as REGISTER_NAME.
 *
 * Example of usage:
 * @code{.c}
   oC_Channel_t channel = oC_Channel_UART0;

   oC_Machine_WriteRegisterDirectStaticOffset(channel,UARTDR,0xFF);
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_WriteRegisterDirectStaticOffset(Channel,REGISTER_NAME,Value)     oC_Machine_WriteRegisterDirectDynamic(Channel,oC_RegisterOffset_(REGISTER_NAME),Value)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief redefinition of #oC_Machine_WriteRegisterDirectStaticOffset
 */
//==========================================================================================================================================
#define oC_Machine_WriteRegister(Channel,REGISTER_NAME,Value)                       oC_Machine_WriteRegisterDirectStaticOffset(Channel,REGISTER_NAME,Value)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief allows read register by using register name
 *
 * The macro is for reading register using REGISTER NAME
 *
 * Example of usage:
 * @code{.c}
   oC_UInt_t value = oC_Machine_ReadRegisterDirectStaticOffset(oC_Channel_UART0 , UARTDR);
   @endcode
 */
//==========================================================================================================================================
#define oC_Machine_ReadRegisterDirectStaticOffset(Channel,REGISTER_NAME)            oC_Machine_ReadRegisterDirectDynamic(Channel,oC_RegisterOffset_(REGISTER_NAME))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief redefinition of #oC_Machine_ReadRegisterDirectStaticOffset
 */
//==========================================================================================================================================
#define oC_Machine_ReadRegister(Channel,REGISTER_NAME)                              oC_Machine_ReadRegisterDirectStaticOffset(Channel,REGISTER_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask with all bit set for #oC_Machine_DmaSignalType_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaSignalTypeMask                                                ((1<<(oC_MACHINE_DMA_SIGNAL_TYPE_WIDTH))-1)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for #oC_Machine_DmaEncodingValue_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaEncodingValueMask                                             ((1<<oC_MACHINE_DMA_ENCODING_VALUE_WIDTH)-1)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of DMA channel field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaChannelMaskWidth                         (oC_CHANNEL_MASK_WIDTH)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of channel field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_ChannelMaskWidth                            (oC_CHANNEL_MASK_WIDTH)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of DMA Signal Type field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskWidth                      (oC_MACHINE_DMA_SIGNAL_TYPE_WIDTH)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of DMA encoding value field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaEncodingValueMaskWidth                   (oC_MACHINE_DMA_ENCODING_VALUE_WIDTH)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief width of #oC_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_MaskWidth                                   (\
                                                                                     oC_Machine_DmaChannelAssignment_DmaChannelMaskWidth    + \
                                                                                     oC_Machine_DmaChannelAssignment_ChannelMaskWidth       + \
                                                                                     oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskWidth + \
                                                                                     oC_Machine_DmaChannelAssignment_DmaEncodingValueMaskWidth)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief start bit for DMA channel field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaChannelMaskStartBit                      (0)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief start bit for channel field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_ChannelMaskStartBit                         (oC_Machine_DmaChannelAssignment_DmaChannelMaskStartBit     + oC_Machine_DmaChannelAssignment_DmaChannelMaskWidth)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief start bit for DMA signal type field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskStartBit                   (oC_Machine_DmaChannelAssignment_ChannelMaskStartBit        + oC_Machine_DmaChannelAssignment_ChannelMaskWidth)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief start bit for DMA encoding value field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaEncodingValueMaskStartBit                (oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskStartBit  + oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskWidth)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for DMA channel field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaChannelMask                              ((oC_ChannelMask)                   << oC_Machine_DmaChannelAssignment_DmaChannelMaskStartBit)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for channel field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_ChannelMask                                 ((oC_ChannelMask)                   << oC_Machine_DmaChannelAssignment_ChannelMaskStartBit)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for DMA signal type field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaSignalTypeMask                           ((oC_Machine_DmaSignalTypeMask)     << oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskStartBit)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for DMA encoding value field in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_DmaEncodingValueMask                        ((oC_Machine_DmaEncodingValueMask)  << oC_Machine_DmaChannelAssignment_DmaEncodingValueMaskStartBit)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for all bits set in #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignmentMask                                         ((1<<(oC_Machine_DmaChannelAssignment_MaskWidth))-1)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns DMA channel from #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_GetDmaChannel(Assignment)                   (((Assignment) & oC_Machine_DmaChannelAssignment_DmaChannelMask)        >>  (oC_Machine_DmaChannelAssignment_DmaChannelMaskStartBit))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns channel from #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_GetChannel(Assignment)                      (((Assignment) & oC_Machine_DmaChannelAssignment_ChannelMask)           >>  (oC_Machine_DmaChannelAssignment_ChannelMaskStartBit))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns DMA signal type from #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_GetDmaSignalType(Assignment)                (((Assignment) & oC_Machine_DmaChannelAssignment_DmaSignalTypeMask)     >>  (oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskStartBit))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns DMA encoding value from #oC_Machine_DmaChannelAssignment_t type
 */
//==========================================================================================================================================
#define oC_Machine_DmaChannelAssignment_GetDmaEncodingValue(Assignment)             (((Assignment) & oC_Machine_DmaChannelAssignment_DmaEncodingValueMask)  >>  (oC_Machine_DmaChannelAssignment_DmaEncodingValueMaskStartBit))

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Machine
//! @{

#if oC_MACHINE_DMA_ENCODING_VALUE_WIDTH < 8
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing DMA encoding value
 */
//==========================================================================================================================================
typedef uint8_t oC_Machine_DmaEncodingValue_t;
#elif oC_MACHINE_DMA_ENCODING_VALUE_WIDTH < 16
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing DMA encoding value
 */
//==========================================================================================================================================
typedef uint16_t oC_Machine_DmaEncodingValue_t;
#elif oC_MACHINE_DMA_ENCODING_VALUE_WIDTH < 32
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing DMA encoding value
 */
//==========================================================================================================================================
typedef uint32_t oC_Machine_DmaEncodingValue_t;
#elif oC_MACHINE_DMA_ENCODING_VALUE_WIDTH < 64
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief type for storing DMA encoding value
 */
//==========================================================================================================================================
typedef uint64_t oC_Machine_DmaEncodingValue_t;
#else
#   error oC_MACHINE_DMA_ENCODING_VALUE_WIDTH is too big!
#endif

//==========================================================================================================================================
/**
 * @brief type with DMA signal types
 *
 * The type stores types of signals for DMA channels.
 */
//==========================================================================================================================================
typedef enum
{
#define DEFINE_DMA_SIGNAL_TYPE(NAME)                    oC_Machine_DmaSignalType_(NAME) ,
    oC_MACHINE_DMA_SIGNAL_TYPE_LIST(DEFINE_DMA_SIGNAL_TYPE)
#undef DEFINE_DMA_SIGNAL_TYPE
    oC_Machine_DmaSignalType_NumberOfElements
} oC_Machine_DmaSignalType_t;

#if oC_Machine_DmaSignalType_NumberOfElements > oC_Machine_DmaSignalTypeMask
#   error oC_MACHINE_DMA_SIGNAL_TYPE_WIDTH is too small!
#endif

//==========================================================================================================================================
/**
 * @brief type for storing assignment of DMA channels
 *
 * The type stores all informations about DMA channel assignments.
 */
//==========================================================================================================================================
typedef enum
{
#define DEFINE_DMA_CHANNEL_ASSIGNMENT( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC )               \
     oC_Machine_DmaChannelAssignment_( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC ) =  \
    ( (oC_Channel_(DMA_CHANNEL_NAME)            << oC_Machine_DmaChannelAssignment_DmaChannelMaskStartBit )         & oC_Machine_DmaChannelAssignment_DmaChannelMask )      | \
    ( (oC_Channel_(CHANNEL_NAME)                << oC_Machine_DmaChannelAssignment_ChannelMaskStartBit )            & oC_Machine_DmaChannelAssignment_ChannelMask )         | \
    ( (oC_Machine_DmaSignalType_(SIGNAL_TYPE)   << oC_Machine_DmaChannelAssignment_DmaSignalTypeMaskStartBit )      & oC_Machine_DmaChannelAssignment_DmaSignalTypeMask )   | \
    ( (ENC                                      << oC_Machine_DmaChannelAssignment_DmaEncodingValueMaskStartBit )   & oC_Machine_DmaChannelAssignment_DmaEncodingValueMask ),

    oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST(DEFINE_DMA_CHANNEL_ASSIGNMENT)
#undef DEFINE_DMA_CHANNEL_ASSIGNMENT
} oC_Machine_DmaChannelAssignment_t;

#if oC_Machine_DmaChannelAssignment_MaskWidth > 64
#error oC_Machine_DmaChannelAssignment_t type is too big! (it needs more than 64 bits!)
#endif

//==========================================================================================================================================
/**
 * @brief stores index in #oC_DmaChannelAssignments array
 */
//==========================================================================================================================================
typedef enum
{
#define DEFINE_DMA_CHANNEL_ASSIGNMENT( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC )               \
    oC_Machine_DmaChannelAssignmentIndex_( DMA_CHANNEL_NAME , CHANNEL_NAME , SIGNAL_TYPE , TYPE , ENC ) ,
    oC_MACHINE_DMA_CHANNELS_ASSIGNMENTS_LIST(DEFINE_DMA_CHANNEL_ASSIGNMENT)
    oC_Machine_DmaChannelAssignmentIndex_NumberOfElements
#undef DEFINE_DMA_CHANNEL_ASSIGNMENT
} oC_Machine_DmaChannelAssignmentIndex_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief array with DMA channel assignments
 *
 * The array contains channel assignments for DMA.
 */
//==========================================================================================================================================
const oC_Machine_DmaChannelAssignment_t oC_DmaChannelAssignments[oC_Machine_DmaChannelAssignmentIndex_NumberOfElements];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with STATIC INLINE functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @addtogroup Machine
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if power for channel is turn on
 *
 * The function checks if power for channel is turned on.
 *
 * @param Channel           machine channel defined in the oc_channels_defs.h file.
 *
 * @return true if power is turned on and channel can be used
 */
//==========================================================================================================================================
static inline bool oC_Machine_IsPowerRegisterSet( oC_Channel_t Channel )
{
    return oC_Channel_PowerBaseAddressExist(Channel) &&
           oC_Channel_PowerBitIndexExist(Channel) &&
           oC_Channel_PowerOffsetExist(Channel);
}

//==========================================================================================================================================
/**
 * @brief returns power state for channel
 *
 * The function returns current state of power for machine channel.
 *
 * @param Channel           machine channel defined in the oc_channels_defs.h file.
 *
 * @return power state for channel
 */
//==========================================================================================================================================
static inline oC_Power_t oC_Machine_GetPowerStateForChannel( oC_Channel_t Channel )
{
    oC_Power_t power = oC_Power_Invalid;

    if(Channel < oC_Channel_MaximumChannelNumber)
    {
        if(oC_Machine_IsPowerRegisterSet(Channel))
        {
            oC_PowerBaseAddress_t powerBaseAddress = oC_Channel_GetPowerBaseAddress(Channel);
            oC_PowerBit_t         powerBit         = oC_Channel_GetPowerBitIndex(Channel);
            oC_PowerOffset_t      powerOffset      = oC_Channel_GetPowerOffset(Channel);

            if(oC_Machine_RegisterIndirectDynamic(powerBaseAddress,powerOffset) & (1<<powerBit))
            {
                power = oC_Power_On;
            }
            else
            {
                power = oC_Power_Off;
            }
        }
        else
        {
            power = oC_Power_NotHandled;
        }
    }

    return power;
}

//==========================================================================================================================================
/**
 * @brief configures power state for machine channel
 *
 * The function sets power state for channel. Note, that it only enable power for the given channel. If the register map of power register
 * also should be powered on, you must do it by yourself earlier. Otherwise there will be hard fault.
 *
 * @param Channel           machine channel defined in the oc_channels_defs.h file.
 * @param Power             state of power to set (#oC_Power_On or #oC_Power_Off)
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool oC_Machine_SetPowerStateForChannel( oC_Channel_t Channel , oC_Power_t Power )
{
    bool success = false;

    if(Channel < oC_Channel_MaximumChannelNumber)
    {
        if(oC_Machine_IsPowerRegisterSet(Channel))
        {
            oC_PowerBaseAddress_t powerBaseAddress = oC_Channel_GetPowerBaseAddress(Channel);
            oC_PowerBit_t         powerBit         = oC_Channel_GetPowerBitIndex(Channel);
            oC_PowerOffset_t      powerOffset      = oC_Channel_GetPowerOffset(Channel);

            success = true;

            if(Power == oC_Power_On)
            {
                oC_Machine_RegisterIndirectDynamic(powerBaseAddress,powerOffset) |= (1<<powerBit);
            }
            else if(Power == oC_Power_Off)
            {
                oC_Machine_RegisterIndirectDynamic(powerBaseAddress,powerOffset) &= ~(1<<powerBit);
            }
            else
            {
                success = false;
            }
        }
        else
        {
            success = Power == oC_Power_On;
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief checks if channel is powered on
 *
 * The function is for checking if the given machine channel is powered on.
 *
 * @param Channel           machine channel defined in the oc_channels_defs.h file.
 *
 * @return true if channel is powered on
 */
//==========================================================================================================================================
static inline bool oC_Machine_IsChannelPoweredOn( oC_Channel_t Channel )
{
    oC_Power_t powerState = oC_Machine_GetPowerStateForChannel(Channel);
    return powerState == oC_Power_On || powerState == oC_Power_NotHandled;
}

//==========================================================================================================================================
/**
 * @brief returns printable name of channel
 *
 * The function returns string with name of channel.
 *
 * @param Channel           machine channel defined in the oc_channels_defs.h file.
 *
 * @return string with name of channel
 */
//==========================================================================================================================================
static inline const char * oC_Machine_GetChannelName( oC_Channel_t Channel )
{
    return oC_Channel_GetName(Channel);
}

//==========================================================================================================================================
/**
 * @brief writes machine register
 *
 * The function writes register that is given as channel (base address) and register offset.
 *
 * @param Channel                   machine channel defined in the oc_channels_defs.h file.
 * @param RegisterOffset            Offset from a base address defined in oc_registers_defs.h
 * @param Value                     Value to write
 *
 * @return true if success
 */
//==========================================================================================================================================
static inline bool oC_Machine_WriteRegisterDirectDynamic( oC_Channel_t Channel , oC_RegisterOffset_t RegisterOffset , oC_Register_t Value )
{
    bool success = false;

    if(Channel < oC_Channel_MaximumChannelNumber)
    {
        if(oC_Machine_IsChannelPoweredOn(Channel))
        {
            oC_Machine_RegisterIndirectDynamic(oC_Channel_GetBaseAddress(Channel),RegisterOffset) = Value;
            success = true;
        }
        else
        {
            /* Channel is not powered on */
            oC_IMPLEMENT_FAILURE();
        }
    }
    else
    {
        /* Channel is not correct */
        oC_IMPLEMENT_FAILURE();
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads machine register
 *
 * The function reads register that is given as channel (base address) and register offset.
 *
 * @param Channel                   machine channel defined in the oc_channels_defs.h file.
 * @param RegisterOffset            Offset from a base address defined in oc_registers_defs.h
 *
 * @return value from register
 */
//==========================================================================================================================================
static inline oC_Register_t oC_Machine_ReadRegisterDirectDynamic( oC_Channel_t Channel , oC_RegisterOffset_t RegisterOffset )
{
    oC_Register_t value = 0;

    if(Channel < oC_Channel_MaximumChannelNumber)
    {
        if(oC_Machine_IsChannelPoweredOn(Channel))
        {
            value = oC_Machine_RegisterIndirectDynamic(oC_Channel_GetBaseAddress(Channel),RegisterOffset);
        }
        else
        {
            /* Channel is not powered on */
            oC_IMPLEMENT_FAILURE();
        }
    }
    else
    {
        /* Channel is not correct */
        oC_IMPLEMENT_FAILURE();
    }

    return value;
}



#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_OC_MACHINE_H_ */
