/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for Channel module
 *
 * @file       oc_channels.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Channels Channels Module
 * @ingroup PortableSpace
 * @brief Module for managing machine module channels
 *
 * The module contains interface for managing machine module channels, for example SPI, UART, TIMER and more...
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_CHANNELS_H_
#define SYSTEM_PORTABLE_INC_OC_CHANNELS_H_

#include <oc_1word.h>
#include <oc_channels_defs.h>
#include <oc_ba.h>
#include <oc_rmaps.h>
#include <oc_registers.h>
#include <oc_interrupts.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Channels
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief mask for channel type width
 *
 * This definition keep mask for the #oC_Channel_t type. It has all bits taken by this type set. It is created using #oC_CHANNEL_MASK_WIDTH from
 * the oc_channels_defs.h file.
 */
//==========================================================================================================================================
#define oC_ChannelMask                                  ( (1<<(oC_CHANNEL_MASK_WIDTH)) - 1 )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns channel for #oC_Channel_t type
 */
//==========================================================================================================================================
#define oC_Channel_(CHANNEL_NAME,...)                   oC_1WORD_FROM_2(oC_Channel_ , CHANNEL_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns module index of channel with the given name
 */
//==========================================================================================================================================
#define oC_ChannelModule_(CHANNEL_NAME,...)             oC_1WORD_FROM_2(oC_ChannelModule_ , CHANNEL_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns start channel index for module
 *
 * This macro returns start index of channels for the given module. Channels are defined in the #oC_Channel_t type. Each channel has it's own
 * unique number (index) in the global array named #oC_ChannelsData. Thanks to the index, it is possible to read data of the channel, such as
 * register map, base address, power register data, etc.
 *
 * Example of usage:
 * @code{.c}
   oC_Channel_t channel = oC_ChannelStartIndex_(SPI) + 1;

   while(channel < oC_ChannelEndIndex_(SPI))
   {
       printf("Found SPI channel %d\n\r" , channel);
   }
   @endcode
 *
 * @see oC_ChannelEndIndex_ , oC_ModuleChannel_ , oC_Channel_Foreach
 */
//==========================================================================================================================================
#define oC_ChannelStartIndex_(MODULE_NAME,...)          oC_1WORD_FROM_3(oC_Channel_ , MODULE_NAME , _StartIndex )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns end channel index for module
 *
 * This macro returns end index of channels for the given module. Channels are defined in the #oC_Channel_t type. Each channel has it's own
 * unique number (index) in the global array named #oC_ChannelsData. Thanks to the index, it is possible to read data of the channel, such as
 * register map, base address, power register data, etc.
 *
 * @param MODULE_NAME       Name of module of channels
 *
 * Example of usage:
 * @code{.c}
   oC_Channel_t channel = oC_ChannelStartIndex_(SPI) + 1;

   while(channel < oC_ChannelEndIndex_(SPI))
   {
       printf("Found SPI channel %d\n\r" , channel);
   }
   @endcode
 *
 * @see oC_ChannelStartIndex_ , oC_ModuleChannel_ , oC_Channel_Foreach
 */
//==========================================================================================================================================
#define oC_ChannelEndIndex_(MODULE_NAME,...)            oC_1WORD_FROM_3(oC_Channel_ , MODULE_NAME , _EndIndex )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns channel of module
 *
 * This macro returns channel of the module channel type. A module channels should be defined first by using #oC_ModuleChannel_DefineType macro.
 *
 * @param MODULE_NAME       Name of module of channels
 * @param CHANNEL_NAME      Name of a channel from oc_channels_defs.h file
 */
//==========================================================================================================================================
#define oC_ModuleChannel_(MODULE_NAME,CHANNEL_NAME,...) oC_1WORD_FROM_4(oC_ , MODULE_NAME , _Channel_ , CHANNEL_NAME )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of interrupt handler
 *
 * The macro returns name of channel interrupt handler name.
 *
 * @param INTERRUPT_TYPE_NAME       Name of the type from oc_interrupts_defs.h file
 *
 * Example of usage:
 * @code{.c}
   // Define a prototype for local channel interrupt
   oC_Channel_InterruptHandlerPrototype(PeripheralInterrupt,Channel);

   // Saving pointer to the channel interrupt
   oC_ChannelInterruptHandler_t channelInterruptHandler = oC_Channel_InterruptHandlerName(PeripheralInterrupt);

   // Simulating interrupt
   channelInterruptHandler(oC_Channel_SPI0);
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_InterruptHandlerName(INTERRUPT_TYPE_NAME)                            oC_1WORD_FROM_2( INTERRUPT_TYPE_NAME , ChannelHandler )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Prototype for channels interrupt
 *
 * This is a macro for definition of channels interrupt handler prototype.
 *
 * @param INTERRUPT_TYPE_NAME       Name of the type from oc_interrupts_defs.h file
 * @param ChannelVariableName       Name of variable, that will store channel
 *
 * Example of usage:
 * @code{.c}
   // Define a prototype for local channel interrupt
   oC_Channel_InterruptHandlerPrototype(PeripheralInterrupt,Channel);

   // Saving pointer to the channel interrupt
   oC_ChannelInterruptHandler_t channelInterruptHandler = oC_Channel_InterruptHandlerName(PeripheralInterrupt);

   // Simulating interrupt
   channelInterruptHandler(oC_Channel_SPI0);
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_InterruptHandlerPrototype(INTERRUPT_TYPE_NAME,ChannelVariableName)   static void oC_Channel_InterruptHandlerName(INTERRUPT_TYPE_NAME) ( oC_Channel_t ChannelVariableName )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief makes module with channels. Not for user usage.
 */
//==========================================================================================================================================
#define oC_Make_ModuleWithChannels(MODULE_NAME)         oC_MODULE_CHANNELS_(MODULE_NAME)(MAKE_CHANNEL)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief makes module with channels. Not for user usage.
 */
//==========================================================================================================================================
#define oC_Make_ChannelForModule(CHANNEL_NAME,...)      oC_ModuleChannel_(MODULE_NAME,CHANNEL_NAME) = oC_Channel_(CHANNEL_NAME) ,

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief makes handler of interrupt for channel. Not for user usage.
 */
//==========================================================================================================================================
#define oC_Make_InterruptHandlerForChannel(CHANNEL_NAME,BASE_NAME,...)        \
    oC_InterruptHandler(BASE_NAME,INTERRUPT_TYPE_NAME) \
    { \
         oC_Channel_InterruptHandlerName(INTERRUPT_TYPE_NAME)(oC_Channel_(CHANNEL_NAME));\
    } \

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns pointer to channel's data
 *
 * The macro returns pointer to data from the #oC_ChannelsData array for the given channel.
 *
 * @param CHANNEL_NAME          Name of the channel from oc_channels_defs.h file
 *
 * Example of usage:
 * @code{.c}
   printf("Found channel %s\n\r" , oC_ChannelData_(SPI0)->ChannelName);
   @endcode
 */
//==========================================================================================================================================
#define oC_ChannelData_(CHANNEL_NAME)                   (&oC_ChannelsData[oC_Channel_(CHANNEL_NAME)])

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief macro for checking if module is defined
 *
 * The macro checks if the module with the given name is defined. It can be used only in hash conditions. Example of usage:
 * @code{.c}
   #if !oC_Channel_IsModuleDefined(GPIO)
   #    error Module GPIO is not defined!
   #endif
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_IsModuleDefined(MODULE_NAME)         (defined(oC_MODULE_CHANNELS_##MODULE_NAME))

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns data of channel
 *
 * The macro is to get channel's data. The parameter, that is given to it should be variable (channel index). If you want to get data of
 * channel according to the name, you should use #oC_ChannelData_.
 *
 * @param Channel           Variable with channel index
 *
 * Example of usage:
 * @code{.c}
   oC_Channel_t channel = oC_Channel_SPI0;

   printf("Found %s channel\n\r" , oC_Channel_GetData(channel)->ChannelName);
   @endcode
 *
 * @see oC_ChannelData_
 */
//==========================================================================================================================================
#define oC_Channel_GetData(Channel)                     (&oC_ChannelsData[Channel])
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if channel is correct
 *
 * The macro is for checking if the given channel is correct for the given module.
 *
 * @code{.c}
   oC_Channel_t channel = oC_Channel_SPI0;
   if(oC_Channel_IsCorrect(SPI,channel))
   {
       printf("Channel %s is correct!\n\r" , oC_Channel_GetName(channel));
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_IsCorrect(MODULE_NAME,Channel)       ((((oC_Channel_t)(Channel)) > oC_ChannelStartIndex_(MODULE_NAME)) && (((oC_Channel_t)(Channel)) < oC_ChannelEndIndex_(MODULE_NAME)))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns index in module according to channel
 *
 * The macro returns index of channel in the given module. Example of usage:
 * @code{.c}
   oC_Channel_t channel = oC_Channel_SPI0;
   oC_ChannelIndex_t channelIndex = oC_Channel_ToIndex(SPI,channel);
   printf("Channel %s is %d in the module SPI\n\r" , oC_Channel_GetName(channel) , channelIndex);
   @endcode
 * @see oC_Channel_FromIndex
 */
//==========================================================================================================================================
#define oC_Channel_ToIndex(MODULE_NAME,Channel)         (Channel - oC_ChannelStartIndex_(MODULE_NAME) - 1)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns channel according to index in module
 *
 * The macro returns channel from the index in the given module. Example of usage:
 * @code{.c}
   oC_ChannelIndex_t channelIndex = 0;
   oC_Channel_t channel = oC_Channel_FromIndex(SPI,channelIndex);

   printf("SPI channel with index 0 is named %s\n\r" , oC_Channel_GetName(channel));
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_FromIndex(MODULE_NAME,ChannelIndex)  (ChannelIndex + oC_ChannelStartIndex_(MODULE_NAME) + 1)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns base address assigned to the channel
 *
 * The macro returns base address of channel.
 *
 * @code{.c}
   oC_Channel_t     channel     = oC_Channel_SPI0;
   oC_BaseAddress_t baseAddress = oC_Channel_GetBaseAddress(channel);
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_GetBaseAddress(Channel)              oC_Channel_GetData(Channel)->BaseAddress
//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief returns base address of power register
 *
 * The macro returns base address of power register.
 *
 * @code{.c}
   oC_Channel_t     channel = oC_Channel_SPI0;
   oC_BaseAddress_t pba     = oC_Channel_GetPowerBaseAddress(channel);
   @endcode
 *
 * @see oC_PowerBaseAddress_
 */
//==========================================================================================================================================
#define oC_Channel_GetPowerBaseAddress(Channel)         oC_Channel_GetData(Channel)->PowerBaseAddress
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns offset of power register
 *
 * The macro returns offset of power register.
 *
 * @see oC_PowerOffset_
 */
//==========================================================================================================================================
#define oC_Channel_GetPowerOffset(Channel)              oC_Channel_GetData(Channel)->PowerOffset
//=========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief returns index of bit for power register
 *
 * The macro returns index of bit in power register, that should be set to enable power for the register map that is assigned with the channel.
 */
//==========================================================================================================================================
#define oC_Channel_GetPowerBitIndex(Channel)            oC_Channel_GetData(Channel)->PowerBitIndex
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns index of register map
 *
 * The macro returns index of register map connected to the channel.
 */
//==========================================================================================================================================
#define oC_Channel_GetRegisterMap(Channel)              oC_Channel_GetData(Channel)->RegisterMap
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of channel
 *
 * The macro returns printable name of the channel.
 *
 * Example of usage:
 * @code{.c}
   oC_Channel_t channel = oC_Channel_SPI0;
   const char * name    = oC_Channel_GetName(channel);

   printf("Found channel named %s\n\r" , name);
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_GetName(Channel)                     oC_Channel_GetData(Channel)->ChannelName

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if base address of power register exist
 *
 * The macro returns true if base address of the power register is configured for the given channel.
 *
 * @code{.c}
   if(!oC_Channel_PowerBaseAddressExist(channel))
   {
       printf("Power base address is not set for the given channel. Enabling is not possible\n\r");
   }
 */
//==========================================================================================================================================
#define oC_Channel_PowerBaseAddressExist(Channel)       (oC_PowerBaseAddress_Exist( oC_Channel_GetPowerBaseAddress(Channel) ) )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if offset of power register exist
 *
 * The macro returns true if offset of the power register is configured for the given channel.
 *
 * @code{.c}
   if(!oC_Channel_PowerOffsetExist(channel))
   {
       printf("Power offset is not set for the given channel. Enabling is not possible\n\r");
   }
 */
//==========================================================================================================================================
#define oC_Channel_PowerOffsetExist(Channel)            (oC_PowerOffset_Exist(      oC_Channel_GetPowerOffset(Channel) ) )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief checks if bit of power register exist
 *
 * The macro returns true if bit of the power register is configured for the given channel.
 *
 * @code{.c}
   if(!oC_Channel_PowerBitIndexExist(channel))
   {
       printf("Power bit is not set for the given channel. Enabling is not possible\n\r");
   }
 */
//==========================================================================================================================================
#define oC_Channel_PowerBitIndexExist(Channel)          (oC_PowerBit_Exist(    oC_Channel_GetPowerBitIndex(Channel) ) )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief allows to access machine register using channel definition
 *
 * The macro is for accessing machine registers using definitions of channel.
 *
 * @warning The macro does not check if register base map is powered on! You should enable it before by yourself!
 *
 * Example of usage:
 * @code{.c}
   oC_Channel_t channel = oC_Channel_SPI0;
   oC_Channel_Register(channel,SPIDR)->Value = 0xFF;
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_Register(Channel,REGISTER_NAME)      oC_RegisterByBaseAddress( oC_Channel_GetBaseAddress(Channel) , REGISTER_NAME )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of elements in module channel
 *
 * Definition with number of channels in module channel type.
 */
//==========================================================================================================================================
#define oC_ModuleChannel_NumberOfElements(MODULE_NAME)  (oC_ChannelEndIndex_(MODULE_NAME) - oC_ChannelStartIndex_(MODULE_NAME) - 1)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief loop for each module channel
 *
 * Creates a loop for each channel in the module.
 *
 * @param MODULE_NAME       name of module from the oc_channels_defs.h file
 * @param Channel           A channel variable name
 *
 * @code{.c}
   oC_Channel_Foreach(SPI,channel)
   {
       oC_Channel_Register(SPI,SPIDR)->Value = 0xBABA;
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_Foreach(MODULE_NAME,Channel)         for(oC_Channel_t Channel = oC_ChannelStartIndex_(MODULE_NAME) + 1 ; Channel < oC_ChannelEndIndex_(MODULE_NAME) ; Channel++)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief creates common interrupt handler for channel
 *
 * The macro creates interrupt handler for channel (common for each module channels). It require 2 additional definitions before usage:
 * MODULE_NAME with name of the module from oc_channels_defs.h file and INTERRUPT_TYPE_NAME from the oc_interrupts_defs.h file.
 *
 * The function, that is created by the macro is local (static).
 *
 * @param ChannelVariableName       Name of the variable, that stores channel
 *
 * Example of usage:
 * @code{.c}
   #define MODULE_NAME              SPI
   #define INTERRUPT_TYPE_NAME      PeripheralInterrupt

   oC_Channel_InterruptHandler(Channel)
   {
       printf("An interrupt occurs for the %s channel of SPI\n\r" , oC_Channel_GetName(Channel));
   }

   #undef MODULE_NAME
   #undef INTERRUPT_TYPE_NAME
   @endcode
 */
//==========================================================================================================================================
#define oC_Channel_InterruptHandler(ChannelVariableName)   \
    oC_Channel_InterruptHandlerPrototype(INTERRUPT_TYPE_NAME,ChannelVariableName);\
    oC_MODULE_CHANNELS_(MODULE_NAME)(oC_Make_InterruptHandlerForChannel)\
    oC_Channel_InterruptHandlerPrototype(INTERRUPT_TYPE_NAME,ChannelVariableName)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief defines module channel type
 *
 * The macro is for definition of channel type for the module. Require MODULE_NAME definition
 *
 * Example of usage:
 * @code{.c}
   #define MODULE_NAME      SPI

   oC_ModuleChannel_DefineType;

   #undef MODULE_NAME

   void main(void)
   {
       oC_SPI_Channel_t channel = oC_SPI_Channel_SPI0;
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_ModuleChannel_DefineType     \
    typedef enum {\
        oC_MODULE_CHANNELS_(MODULE_NAME)(oC_Make_ChannelForModule) \
    } oC_ModuleChannel_(MODULE_NAME,t)

#define oC_Channel_SetInterruptPriority( Channel , INTERRUPT_TYPE , InterruptPriority )         oC_Channel_SetInterruptPriorityFunction( Channel , oC_InterruptType_(INTERRUPT_TYPE) , InterruptPriority )
#define oC_Channel_EnableInterrupt( Channel , INTERRUPT_TYPE )                                  oC_Channel_EnableInterruptFunction( Channel , oC_InterruptType_(INTERRUPT_TYPE) )
#define oC_Channel_DisableInterrupt( Channel , INTERRUPT_TYPE )                                 oC_Channel_DisableInterruptFunction( Channel , oC_InterruptType_(INTERRUPT_TYPE) )
#define oC_Channel_IsInterruptEnabled( Channel , INTERRUPT_TYPE )                               oC_Channel_IsInterruptEnabledFunction( Channel , oC_InterruptType_(INTERRUPT_TYPE) )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Channels
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief stores machine channel
 *
 * The type is for storing machine channels. This type is generated from definitions in the oc_channels_defs.h file. The channel is an ID in global
 * array with assigned base address and register map. This type is generic for all machine modules (UART,SPI,I2C,GPIO,etc) and each channel in this
 * enum is different.
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_MODULE(MODULE_NAME)                                                    \
        oC_ChannelStartIndex_(MODULE_NAME), \
        oC_MODULE_CHANNELS_(MODULE_NAME)(MAKE_CHANNEL) \
        oC_ChannelEndIndex_(MODULE_NAME),
#define MAKE_CHANNEL(CHANNEL_NAME,BASE_ADDRESS_NAME,REGISTER_MAP_NAME,...)          oC_Channel_(CHANNEL_NAME) ,
    oC_MODULES_LIST(MAKE_MODULE)
    oC_Channel_MaximumChannelNumber ,
    oC_Channel_System                   = oC_Channel_MaximumChannelNumber,
    oC_Channel_Software                 = oC_Channel_MaximumChannelNumber
#undef MAKE_CHANNEL
#undef MAKE_MODULE
} oC_Channel_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief stores index of channel
 *
 * The type is for storing index of a channel in a module. For example, for the SPI1 channel, the channel index will be 1, and for UART1 it
 * will be also 1.
 */
//==========================================================================================================================================
typedef oC_Channel_t oC_ChannelIndex_t;

#if oC_Channel_MaximumChannelNumber > oC_ChannelMask
# error Channel mask width is too small!
#endif

//==========================================================================================================================================
/**
 * @brief stores data for channel
 *
 * The type is for storing all data, that is unique for each channel. It is for global array named #oC_ChannelsData.
 */
//==========================================================================================================================================
typedef struct
{
    oC_BaseAddress_t        BaseAddress;            //!< Start address in memory
    oC_PowerBaseAddress_t   PowerBaseAddress;       //!< Power register base address
    oC_PowerOffset_t        PowerOffset;            //!< Power register offset
    oC_PowerBit_t           PowerBitIndex;          //!< Bit in power register for enabling power for the channel
    oC_RegisterMap_t        RegisterMap;            //!< Index of register map
    const char *            ChannelName;            //!< Name of the given register map
} oC_ChannelData_t;

//==========================================================================================================================================
/**
 * @brief stores index of channel module (UART,SPI,etc)
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_MODULE(MODULE_NAME)        oC_ChannelModule_(MODULE_NAME) ,
    oC_MODULES_LIST(MAKE_MODULE)
#undef MAKE_MODULE
} oC_ChannelModule_t;

//==========================================================================================================================================
/**
 * @brief stores pointer of channel interrupt function
 *
 * The type is for storing special interrupt handler - common for each channel of a module. A parameter #Channel point to the channel, that cause
 * the interrupt.
 *
 * @param Channel       Interrupt source
 *
 * @see oC_Channel_InterruptHandler
 */
//==========================================================================================================================================
typedef void (*oC_ChannelInterruptHandler_t)( oC_Channel_t Channel );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern bool oC_Channel_SetInterruptPriorityFunction(       oC_Channel_t Channel , oC_InterruptType_t InterruptType , oC_InterruptPriotity_t Priority  );
extern bool oC_Channel_EnableInterruptFunction(    oC_Channel_t Channel , oC_InterruptType_t InterruptType );
extern bool oC_Channel_DisableInterruptFunction(   oC_Channel_t Channel , oC_InterruptType_t InterruptType );
extern bool oC_Channel_IsInterruptEnabledFunction( oC_Channel_t Channel , oC_InterruptType_t InterruptType );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________
//! @addtogroup Channels
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief stores data of channels
 *
 * This is a global array, that stores data for each channel. Channels are indexes in the array.
 *
 * @see oC_ChannelData_t
 */
//==========================================================================================================================================
extern const oC_ChannelData_t oC_ChannelsData[];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_OC_CHANNELS_H_ */
