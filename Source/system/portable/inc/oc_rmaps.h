/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for register maps module
 *
 * @file       oc_rmaps.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup RMAPS  RMaps - Register Maps module
 * @ingroup PortableSpace
 * @brief Module for managing register maps definitions
 *
 * @par
 * The module contains interface for accessing register maps data. Types in this module are created using definitions from the oc_rmaps_defs.h file.
 * By using this module you can get information about access (Read/Write) and offset (from base address) of registers in registers maps.
 * The register map is just a group of registers with the same base address - each register in register map should have different offset, that will
 * be added to the base address, but it is not required for this implementation. You can also define more registers with the same offset. The only requirement is
 * to use different names of registers - each register must have different name.
 *
 * @par
 * When you want to port the system to new architecture, you should start by define definitions in the oc_rmaps_defs.h file, that is connected
 * to this interface.
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_RMAPS_H_
#define SYSTEM_PORTABLE_INC_OC_RMAPS_H_

#include <oc_1word.h>
#include <oc_rmaps_defs.h>
#include <oc_memory.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup RMAPS
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Macro for receiving name of register offset type
 *
 * The macro helps to receive name of an enumerator in oC_RegisterOffset_t type.
 *
 * @param REGISTER_NAME         Name of register from the register map definition file.
 */
//==========================================================================================================================================
#define oC_RegisterOffset_(REGISTER_NAME)                       oC_1WORD_FROM_2(oC_RegisterOffset_ , REGISTER_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief Macro for receiving name of register access type
 *
 * The macro helps to receive name of an enumerator in oC_RegisterAccess_t type.
 *
 * @param REGISTER_NAME         Name of register from the register map definition file.
 */
//==========================================================================================================================================
#define oC_RegisterAccess_(REGISTER_NAME)                       oC_1WORD_FROM_2(oC_RegisterAccess_ , REGISTER_NAME)
//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief Macro for receiving name of register map type
 *
 * The macro helps to receive name of an enumerator in oC_RegisterMap_t type.
 *
 * @param REGISTER_MAP_NAME         Name of register map from the register map definition file.
 */
//==========================================================================================================================================
#define oC_RegisterMap_(REGISTER_MAP_NAME)                      oC_1WORD_FROM_2(oC_RegisterMap_ , REGISTER_MAP_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief Special macro for calling registers list from register map definition file with MAKE_REGISTER macro as argument
 *
 * The macro is helper for creating types with using definitions from the definition file. For example:
 * @code{.c}
   #define MAKE_REGISTER(REGISTER_NAME,OFFSET,ACCESS)   #REGISTER_NAME ,

   // The definition of names of registers from the FLASH register map
   const char * FlashRegisterNames[] = {
       oC_Make_RegisterMapWithRegisters(FLASH)
   };
   #undefine MAKE_REGISTER
   @endcode
 */
//==========================================================================================================================================
#define oC_Make_RegisterMapWithRegisters(REGISTER_MAP_NAME)     oC_REGISTER_MAP_(REGISTER_MAP_NAME)(MAKE_REGISTER)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup RMAPS
//! @{

//==========================================================================================================================================
/**
 * @brief Type for storing offset of registers
 *
 * The type is for storing offset of registers. It is created by macros using definitions from the oc_rmaps_defs.h file.
 *
 * This can be used for accessing to registers.
 *
 * @see oC_Register
 */
//==========================================================================================================================================
typedef enum
{
    oC_RegisterOffset_(None) = 0 ,
#define MAKE_REGISTER(REGISTER_NAME , OFFSET , ACCESS)          oC_RegisterOffset_(REGISTER_NAME) = OFFSET ,
    oC_REGISTER_MAP_LIST(oC_Make_RegisterMapWithRegisters)
#undef MAKE_REGISTER
} oC_RegisterOffset_t;

//==========================================================================================================================================
/**
 * @brief Type for storing access of registers
 *
 * The type is for storing access of registers. It is created by macros using definitions from the oc_rmaps_defs.h file.
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_REGISTER(REGISTER_NAME , OFFSET , ACCESS)          oC_RegisterAccess_(REGISTER_NAME) = oC_Access_(ACCESS) ,
    oC_REGISTER_MAP_LIST(oC_Make_RegisterMapWithRegisters)
#undef MAKE_REGISTER
} oC_RegisterAccess_t;

//==========================================================================================================================================
/**
 * @brief Type for storing register map ID
 *
 * The type is created according to definitions in oc_rmaps_defs.h file. It can be used for indexing register maps by their names.
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_REGISTER_MAP(REGISTER_MAP_NAME)    oC_RegisterMap_(REGISTER_MAP_NAME) ,
    oC_REGISTER_MAP_LIST(MAKE_REGISTER_MAP)
#undef MAKE_REGISTER_MAP
    oC_RegisterMap_(NumberOfElements)
} oC_RegisterMap_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}



#endif /* SYSTEM_PORTABLE_INC_OC_RMAPS_H_ */
