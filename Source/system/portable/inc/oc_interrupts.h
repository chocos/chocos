/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface interrupt module
 *
 * @file       oc_interrupts.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Interrupts Interrupts Module
 * @ingroup PortableSpace
 * @brief Module for definitions and types of interrupts
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_INTERRUPTS_H_
#define SYSTEM_PORTABLE_INC_OC_INTERRUPTS_H_

#include <oc_interrupts_defs.h>
#include <oc_ba.h>
#include <oc_compiler.h>
#include <oc_mcs.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Interrupts
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Returns value from #oC_InterruptType_t. It is an index of interrupt type
 */
//==========================================================================================================================================
#define oC_InterruptType_(TYPE_NAME)                    oC_1WORD_FROM_2( oC_InterruptType_ , TYPE_NAME )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Returns value from #oC_InterruptIndex_t. It is an index of interrupt in interrupts vector
 */
//==========================================================================================================================================
#define oC_InterruptIndex_(BASE_NAME,TYPE_NAME)         oC_1WORD_FROM_4( oC_InterruptIndex_ , BASE_NAME , _ , TYPE_NAME )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Returns value from #oC_InterruptNumber_t.
 */
//==========================================================================================================================================
#define oC_InterruptNumber_(BASE_NAME,TYPE_NAME)        oC_1WORD_FROM_4( oC_InterruptNumber_ , BASE_NAME , _ , TYPE_NAME )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Returns value from #oC_InterruptBaseAddress_t
 *
 * Returns value from #oC_InterruptBaseAddress_t (address of base address for register map, that the interrupt is connected to - just base address  from the parameter)
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 * @param TYPE_NAME         Name of the interrupt type
 */
//==========================================================================================================================================
#define oC_InterruptBaseAddress_(BASE_NAME,TYPE_NAME)   oC_1WORD_FROM_4( oC_InterruptBaseAddress_ , BASE_NAME , _ , TYPE_NAME )

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns name of interrupt function
 *
 * The macro returns name of an interrupt handler. It is not interrupt prototype, just the name.
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 * @param TYPE_NAME         Name of the interrupt type
 *
 * Here is some example:
 * @code{.c}
   // Definition of interrupt handler prototype
   extern oC_InterruptHandlerPrototype(SPI0,PeripheralInterrupt);

   // Taking pointer of interrupt handler
   oC_InterruptHandler_t    SPIInterrupt = oC_InterruptHandlerName(SPI0 , PeripheralInterrupt);

   // Checking if the pointer is set to default interrupt handler
   if(SPIInterrupt == oC_DEFAULT_INTERRUPT_HANDLER_NAME)
   {
       // If it is, it does mean, that the interrupt handler is not set
       printf("Handler for SPI0 is not set!\n\r");
   }
   @endcode
 */
//==========================================================================================================================================
#define oc_InterruptHandlerName(BASE_NAME,TYPE_NAME)            oC_1WORD_FROM_4( oC_InterruptHandler_ , BASE_NAME , _ , TYPE_NAME )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief returns handler prototype of interrupt handler
 *
 * The macro returns full prototype for an interrupt function. The type of this prototype should be the same as #oC_InterruptHandler_t type.
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 * @param TYPE_NAME         Name of the interrupt type
 *
 * Some example:
 * @code{.c}
   // Prototype for the interrupt function
   extern oC_InterruptHandlerPrototype(SPI0,PeripheralInterrupt);

   void main(void)
   {
       // Taking pointer of interrupt handler
       oC_InterruptHandler_t    SPIInterrupt = oC_InterruptHandlerName(SPI0 , PeripheralInterrupt);

       printf("Simulating an SPI0 interrupt...\n\r");

       // Simulating SPI0 interrupt
       SPIInterrupt();
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_InterruptHandlerPrototype(BASE_NAME,TYPE_NAME)       void oc_InterruptHandlerName(BASE_NAME , TYPE_NAME) (void)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief creates weak interrupt handler prototype. Not for user usage
 */
//==========================================================================================================================================
#define oC_InterruptHandlerWeakPrototype(BASE_NAME,TYPE_NAME)   oC_InterruptHandlerPrototype(BASE_NAME , TYPE_NAME) __attribute__ ((interrupt, weak, alias(oC_TO_STRING(oC_DEFAULT_INTERRUPT_HANDLER_NAME))))
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Define handler for interrupt
 *
 * This macro should be used for definition of interrupt handler. Look for example below for more details.
 *
 * @param BASE_NAME         Name of base address from the oc_ba_defs.h file
 * @param TYPE_NAME         Name of the interrupt type
 *
 * Example of usage:
 * @code{.c}
   // Definition of interrupt handler for SPI0 base address
   oC_InterruptHandler(SPI0,PeripheralInterrupt)
   {
       // code for handling SPI0 interrupt
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_InterruptHandler(BASE_NAME,TYPE_NAME)                oC_InterruptHandlerPrototype(BASE_NAME , TYPE_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Creates prototype of default interrupt handler
 *
 * The macro creates prototype for default interrupt handler. This handler is called, when an interrupt occurs, but the handler is not set.
 *
 * Example of usage:
 * @code{.c}
   // Assume, that we have SPI0 , SPI1 and SPI2

   // We must define prototypes for these interrupts
   extern oC_InterruptHandlerPrototype(SPI0,PeripheralInterrupt);
   extern oC_InterruptHandlerPrototype(SPI1,PeripheralInterrupt);
   extern oC_InterruptHandlerPrototype(SPI2,PeripheralInterrupt);

   // Definition of default interrupt handler
   extern oC_DefaultInterruptHandler;

   // Handler of interrupt for SPI0
   oC_InterruptHandler(SPI0,PeripheralInterrupt)
   {
       printf("Handler for SPI0 exist!\n\r");
   }

   // Handler of interrupt for SPI1
   oC_InterruptHandler(SPI1,PeripheralInterrupt)
   {
       printf("Handler for SPI1 exist!\n\r");
   }

   // Main entry of function
   void main(void)
   {
       // Taking pointers of interrupts handlers
       oC_InterruptHandler_t    SPI0Interrupt = oC_InterruptHandlerName(SPI0 , PeripheralInterrupt);
       oC_InterruptHandler_t    SPI1Interrupt = oC_InterruptHandlerName(SPI1 , PeripheralInterrupt);
       oC_InterruptHandler_t    SPI2Interrupt = oC_InterruptHandlerName(SPI2 , PeripheralInterrupt);

       // Taking pointers of default interrupt handler
       oC_InterruptHandler_t    DefaultInterrupt = oC_DEFAULT_INTERRUPT_HANDLER_NAME;

       // Simulating SPI0 interrupt - SPI0 handler will be called
       SPI0Interrupt();
       // Simulating SPI1 interrupt - SPI1 handler will be called
       SPI1Interrupt();
       // Simulating SPI2 interrupt - Default interrupt handler will be called
       SPI2Interrupt();

       if(SPI2Interrupt == DefaultInterrupt)
       {
           printf("SPI2 handler is not set!\n\r);
       }
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_DefaultInterruptHandler                              void oC_DEFAULT_INTERRUPT_HANDLER_NAME(void)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Creates special prototype for default interrupt handler. Not for user usage.
 */
//==========================================================================================================================================
#define oC_DefaultInterruptHandlerPrototype                     oC_DefaultInterruptHandler __attribute__ ((interrupt))

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Name of reset handler. Not for user usage.
 */
//==========================================================================================================================================
#define oC_ResetInterruptHandlerName                            Reset_Handler
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Prototype for reset handler. Not for user usage.
 */
//==========================================================================================================================================
#define oC_ResetInterruptHandler                                void oC_ResetInterruptHandlerName( void )
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Prototype of reset handler. Not for user usage.
 */
//==========================================================================================================================================
#define oC_ResetInterruptHandlerPrototype                       oC_ResetInterruptHandler

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Interrupts
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Stores interrupt type
 *
 * The type is for storing interrupt type. Thanks to this type, it is possible to distinguish more interrupts connected to the same register map base address.
 *
 * Example of usage:
 * @code{.c}
   // Taking type of interrupt
   oC_InterruptType_t peripheralInterrupt = oC_InterruptType_(PeripheralInterrupt);
   @endcode
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE(TYPE_NAME)             oC_InterruptType_(TYPE_NAME) ,
    oC_MACHINE_INTERRUPTS_TYPES_LIST(MAKE)
    oC_InterruptType_(NumberOfElements)
#undef MAKE
} oC_InterruptType_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Index of interrupt in the interrupt vector
 *
 * The type is for storing index of interrupt in global interrupt vector array.
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_INTERRUPT_NUMBER(BASE_ADDRESS_NAME  , TYPE_NAME , PRIORITY , NUMBER)           \
     oC_InterruptIndex_(BASE_ADDRESS_NAME , TYPE_NAME) = NUMBER ,
    oC_MACHINE_INTERRUPTS_LIST(MAKE_INTERRUPT_NUMBER)
#undef MAKE_INTERRUPT_NUMBER
    oC_InterruptIndex_NumberOfElements
} oC_InterruptIndex_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Number of interrupt in system
 *
 * The type is for storing number of interrupt in system (Unique number, ID in the machine). This is required for #MCS functions.
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE_INTERRUPT_PRIORITY(BASE_ADDRESS_NAME  , TYPE_NAME , PRIORITY , NUMBER)           \
        oC_InterruptNumber_(BASE_ADDRESS_NAME , TYPE_NAME) = PRIORITY ,
    oC_MACHINE_INTERRUPTS_LIST(MAKE_INTERRUPT_PRIORITY)
#undef MAKE_INTERRUPT_PRIORITY
    oC_InterruptNumber_NumberOfElements
} oC_InterruptNumber_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Base address connected to the interrupt
 *
 * Each interrupt must be defined with connection of base address for register map. If some interrupt is not specific for base address, then it should
 * be defined as system interrupt. In this case, the base address of such interrupt will be set to #oC_BaseAddress_System
 */
//==========================================================================================================================================
typedef enum
{
#define MAKE(BASE_ADDRESS_NAME  , TYPE_NAME , PRIORITY , NUMBER)           \
        oC_InterruptBaseAddress_(BASE_ADDRESS_NAME , TYPE_NAME) = oC_BaseAddress_(BASE_ADDRESS_NAME) ,
    oC_MACHINE_INTERRUPTS_LIST(MAKE)
#undef MAKE
    oC_InterruptBaseAddress_NumberOfElements
} oC_InterruptBaseAddress_t;

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Stores interrupt handler pointer
 *
 * The type is for storing interrupt handler pointers. Here is an example of usage:
 * @code{.c}
       // We must define prototypes for these interrupts
       extern oC_InterruptHandlerPrototype(SPI0,PeripheralInterrupt);
       extern oC_InterruptHandlerPrototype(SPI1,PeripheralInterrupt);
       extern oC_InterruptHandlerPrototype(SPI2,PeripheralInterrupt);

       // Taking pointers of interrupts handlers
       oC_InterruptHandler_t    SPI0Interrupt = oC_InterruptHandlerName(SPI0 , PeripheralInterrupt);
       oC_InterruptHandler_t    SPI1Interrupt = oC_InterruptHandlerName(SPI1 , PeripheralInterrupt);
       oC_InterruptHandler_t    SPI2Interrupt = oC_InterruptHandlerName(SPI2 , PeripheralInterrupt);
   @endcode
 */
//==========================================================================================================================================
typedef void (*oC_InterruptHandler_t)(void);

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief structure with data for interrupts
 *
 * The type stores data for interrupts.
 */
//==========================================================================================================================================
typedef struct
{
    oC_InterruptBaseAddress_t    BaseAddress;
    oC_InterruptType_t           Type;
    oC_InterruptIndex_t          InterruptIndex;
    oC_InterruptNumber_t         InterruptNumber;
} oC_InterruptData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern const oC_InterruptData_t * oC_Interrupt_GetData( oC_InterruptBaseAddress_t BaseAddress , oC_InterruptType_t InterruptType );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________
//! @addtogroup Interrupts
//! @{

//==========================================================================================================================================
/**
 * @brief Handler for unexpected interrupts
 *
 * This variable stores handler for unexpected interrupt handler. This handler will be called, if some interrupt occurs, but the handler is not
 * defined.
 *
 * To use it just set it to correct handler pointer.
 *
 * Example of usage:
 * @code{.c}
   void UnexpectedInterruptHandler(void)
   {
       printf("Unexpected interrupt occurs\n\r");
   }
   @endcode
 */
//==========================================================================================================================================
extern oC_InterruptHandler_t oC_UnexpectedInterruptHandler;

//==========================================================================================================================================
/**
 * @brief Data for interrupts
 */
//==========================================================================================================================================
extern const oC_InterruptData_t oC_InterruptData[oC_InterruptIndex_NumberOfElements];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_OC_INTERRUPTS_H_ */
