/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for registers module
 *
 * @file       oc_registers.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Registers Registers Module
 * @ingroup PortableSpace
 * @brief Module with definitions of registers.
 *
 * The module contains definitions of types for each register. Each register type is a union that consist of register bits definitions and **Value**
 * field.
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PORTABLE_INC_OC_REGISTERS_H_
#define SYSTEM_PORTABLE_INC_OC_REGISTERS_H_

#include <oc_1word.h>
#include <oc_stdtypes.h>
#include <oc_registers_defs.h>
#include <oc_ba.h>
#include <oc_rmaps.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Registers
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief Returns name of type for storing register value.
 *
 * The macro returns name of a structure type for parsing register bits. The type is created using macro and definitions from the oc_registers_defs.h
 * file for each register.
 *
   @code{.c}
   oC_RegisterType_(FLASH_DR) FLASH_DR;
   FLASH_DR.Value = 0xFF;

   if(FLASH_DR.D0 == 1)
   {
      // The D0 bit in the FLASH_DR register is set
   }
   @endcode
 */
//==========================================================================================================================================
#define oC_RegisterType_(REGISTER_NAME)                             oC_1WORD_FROM_3(oC_RegisterType_ , REGISTER_NAME , _t )

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief returns size of register in number of bits
 */
//==========================================================================================================================================
#define oC_RegisterSize_(REGISTER_NAME)                             oC_1WORD_FROM_2(oC_RegisterSize_,REGISTER_NAME)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Access to register with dynamic base address
 *
 * The macro is for accessing registers, when the base address is dynamically counted (for example it is given from user as channel).
 *
 * @param BaseAddress           Base address of register map
 * @param REGISTER_NAME         Name of a register from the oc_registers_defs.h file
 *
 * @code{.c}
   oC_RegisterByBaseAddress( 0x4000000UL , FLASH_DR )->Value = 0xFF;
   @endcode
 */
//==========================================================================================================================================
#define oC_RegisterByBaseAddress(BaseAddress,REGISTER_NAME)         ( (oC_RegisterType_(REGISTER_NAME)*)(BaseAddress + oC_RegisterOffset_(REGISTER_NAME)) )
//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief Access to register via base address name
 *
 * The macro is for accessing registers, when the base address name is known at the compilation time - for example in case of system registers,
 * or modules, when there is only one channel used you can use this macro to read/write register value.
 *
 * @param BASE_NAME                 Name of base address from the oc_ba_defs.h file
 * @param REGISTER_NAME             Name of register from oc_registers_desf.h file
 *
 * @code{.c}
   oC_Register( DMA2D , DMA2D_DR )->Value = 0xFF;
   @endcode
 */
//==========================================================================================================================================
#define oC_Register(BASE_NAME,REGISTER_NAME)                        ( (oC_RegisterType_(REGISTER_NAME)*)(oC_BaseAddress_(BASE_NAME) + oC_RegisterOffset_(REGISTER_NAME)) )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Registers
//! @{

//==========================================================================================================================================
/**
 * @brief type for storing registers
 */
//==========================================================================================================================================
typedef volatile oC_UInt_t oC_Register_t;

//==========================================================================================================================================
/*
 * The lines below are to define types for each register using definitions from oc_registers_defs.h file.
 */
//==========================================================================================================================================
#define MAKE_BIT(BIT_NAME , SIZE )    volatile oC_UInt_t  BIT_NAME:SIZE;
#define MAKE_REGISTER(REGISTER_NAME)            \
    typedef union \
    { \
         struct {\
            oC_REGISTER_(REGISTER_NAME)(MAKE_BIT)\
         };\
         volatile oC_UInt_t         Value;\
    } oC_RegisterType_(REGISTER_NAME);
oC_REGISTERS_LIST(MAKE_REGISTER)
#undef MAKE_BIT
#undef MAKE_REGISTER

//==========================================================================================================================================
/*
* The lines below are to verify, that each register contains exactly 32 bits
*/
//==========================================================================================================================================
#define MAKE_BIT(BIT_NAME , SIZE )              + SIZE
#define MAKE_REGISTER(REGISTER_NAME)            oC_RegisterSize_(REGISTER_NAME) = 0 oC_REGISTER_(REGISTER_NAME)(MAKE_BIT) ,

typedef enum
{
    oC_REGISTERS_LIST(MAKE_REGISTER)
} oC_RegisterSize_t;
#undef MAKE_BIT
#undef MAKE_REGISTER


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

#endif /* SYSTEM_PORTABLE_INC_OC_REGISTERS_H_ */
