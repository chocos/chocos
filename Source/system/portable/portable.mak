############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/04/27
##  Description:    makefile for the portable space
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              PREPARATION VARIABLES               
##                                          
##============================================================================================================================
ifeq ($(PROJECT_DIR),)
    PROJECT_DIR = ../../..
endif

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
SYSTEM_PORTABLE_OPTIMALIZE         = O0
SYSTEM_PORTABLE_WARNING_FLAGS      = -Wall -Werror
SYSTEM_PORTABLE_CSTD               = gnu11
SYSTEM_PORTABLE_DEFINITIONS        = oC_PORTABLE_SPACE

##============================================================================================================================
##                                          
##          INCLUDE MAKEFILES       
##                                          
##============================================================================================================================
include $(GLOBAL_DEFS_MK_FILE_PATH)
include $(LIBRARIES_SPACE_MK_FILE_PATH)

##============================================================================================================================
##                                          
##              EXTENSIONS CONFIGURATION
##                                          
##============================================================================================================================
C_EXT                       = c
OBJ_EXT                     = o

##============================================================================================================================
##                                          
##              PREPARATION OF DIRECTORIES LIST             
##                                          
##============================================================================================================================
SYSTEM_PORTABLE_SOURCES_LLD_SUBDIRS    = $(filter %/, $(wildcard $(SYSTEM_PORTABLE_SOURCES_LLD_DIR)/*/))
SYSTEM_PORTABLE_SOURCES_MSLLD_SUBDIRS  = $(filter %/, $(wildcard $(SYSTEM_PORTABLE_SOURCES_MSLLD_DIR)/*/))
SYSTEM_PORTABLE_SOURCES_DIRS           = $(SYSTEM_PORTABLE_SOURCES_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_PRODUCENT_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_FAMILY_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_MACHINE_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_LLD_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_MSLLD_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_LLD_SUBDIRS) \
                                         $(SYSTEM_PORTABLE_SOURCES_MSLLD_SUBDIRS) \
                                         $(SYSTEM_PORTABLE_SOURCES_MCS_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_MCS_CORE_PRODUCENT_DIR) \
                                         $(SYSTEM_PORTABLE_SOURCES_MCS_CORE_DIR) \

SYSTEM_PORTABLE_INCLUDES_LLD_SUBDIRS   = $(filter %/, $(wildcard $(SYSTEM_PORTABLE_INCLUDES_LLD_DIR)/*/))
SYSTEM_PORTABLE_INCLUDES_MSLLD_SUBDIRS = $(filter %/, $(wildcard $(SYSTEM_PORTABLE_INCLUDES_MSLLD_DIR)/*/))
SYSTEM_PORTABLE_INCLUDES_DIRS          = $(SYSTEM_PORTABLE_INCLUDES_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_PRODUCENT_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_MACHINE_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_LLD_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_MSLLD_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_MCS_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_MCS_CORE_PRODUCENT_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_MCS_CORE_DIR) \
                                         $(SYSTEM_LIBRARIES_INCLUDES_DIRS) \
                                         $(SYSTEM_CONFIG_MACHINE_DIR) \
                                         $(SYSTEM_CONFIG_DIR)

##============================================================================================================================
##                                          
##              PREPARATION OF SOURCE FILES         
##                                          
##============================================================================================================================
SYSTEM_PORTABLE_SOURCES            = $(foreach src,$(foreach subdir, $(SYSTEM_PORTABLE_SOURCES_DIRS), $(wildcard $(subdir)/*.c)),$(subst //,/,$(src)))
SYSTEM_PORTABLE_ASM_SOURCES        =

##============================================================================================================================
##                                          
##              ADD EXTRA DEFINITIONS        
##                                          
##============================================================================================================================
SYSTEM_PORTABLE_DEFINITIONS       += $(BUILD_VERSION_DEFINITIONS) $(ARCHITECTURE_DEFINITIONS)  __POSIX_VISIBLE=200809
                                  
##============================================================================================================================
##                                          
##              PREPARATION OF COMPILER ARGUMENTS   
##                                          
##============================================================================================================================
SYSTEM_PORTABLE_INCLUDES           = $(foreach dir,$(SYSTEM_PORTABLE_INCLUDES_DIRS),-I$(dir))
SYSTEM_PORTABLE_DEFINITIONS_FLAGS  = $(foreach def,$(SYSTEM_PORTABLE_DEFINITIONS),-D$(def))
SYSTEM_PORTABLE_CFLAGS             = -c -g -std=$(SYSTEM_PORTABLE_CSTD) $(SYSTEM_PORTABLE_WARNING_FLAGS) -$(SYSTEM_PORTABLE_OPTIMALIZE) $(CPUCONFIG_CFLAGS) \
                                     -fdata-sections -ffunction-sections $(ADDITIONAL_CFLAGS)
SYSTEM_PORTABLE_AFLAGS             = -c -g -ggdb3 $(CPUCONFIG_AFLAGS)

##============================================================================================================================
##                                          
##              TARGETS 
##                                          
##============================================================================================================================
print_portable_info:
	@echo "=================================================================================================================="
	@echo "                                           Building Portable Space"
	@echo "=================================================================================================================="
	@echo " "
	@echo "Portable includes list:"
	@echo -e " $(foreach inc,$(SYSTEM_PORTABLE_INCLUDES_DIRS), include directory: $(inc)\n)"
	@echo "Portable source dirs:"
	@echo -e " $(foreach inc,$(SYSTEM_PORTABLE_SOURCES_DIRS), source directory: $(inc)\n)"
	@echo "Portable sources list:"
	@echo -e " $(foreach src,$(SYSTEM_PORTABLE_SOURCES), source file: $(src)\n)"
	@echo "Portable asm sources list:"
	@echo -e " $(foreach src,$(SYSTEM_PORTABLE_ASM_SOURCES), source file: $(src)\n)"
	@echo "Creating directories..."
	@$(MKDIR) $(MACHINE_OUTPUT_DIR)
build_portable_dependences:
	@echo "Creating dependeces..."
	@$(MKDEP) -f libraries.mak $(SYSTEM_PORTABLE_INCLUDES) -- $(SYSTEM_PORTABLE_CFLAGS) -- $(SYSTEM_PORTABLE_SOURCES) -o .$(OBJ_EXT)
    
build_portable_objects:
	@echo "Building portable..."
	@$(CC) $(SYSTEM_PORTABLE_SOURCES) $(SYSTEM_PORTABLE_INCLUDES) $(SYSTEM_PORTABLE_CFLAGS) $(SYSTEM_PORTABLE_DEFINITIONS_FLAGS)
	
archieve_portable:
	@echo "Archieve portable space..."
	@$(AR) rcs $(PORTABLE_SPACE_LIB_FILE_PATH) *.o
	@$(RM) *.o

portable_move_stack_analyze_result:
	@echo "Moving stack analysis files to the machine output directory..."
	@$(MV) *.su $(MACHINE_OUTPUT_DIR)
    
portable_status:
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Portable Space building success"
	@echo ""
    
build_portable: print_portable_info clean_portable_output build_portable_objects archieve_portable portable_status 
	
clean_portable_output:	
	@echo "Removing old portable library file..."
	$(RM) -f $(PORTABLE_SPACE_LIB_FILE_PATH)
	
clean_portable:
	@echo "=================================================================================================================="
	@echo "                                           Cleaning Portable Space"
	@echo "=================================================================================================================="
	@echo " "
	$(RM) -f *.o
	$(RM) -f *.a
	$(RM) -f $(PORTABLE_SPACE_LIB_FILE_PATH)
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Portable Space cleaning success"
	@echo ""
    
