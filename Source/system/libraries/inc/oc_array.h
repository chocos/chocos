/** ****************************************************************************************************************************************
 *
 * @file       oc_array.h
 *
 * @brief      Static array definitions
 *
 * @author     Patryk Kubiak - (Created on: 30 mar 2015 20:11:16) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Array Array
 * @ingroup LibrariesSpace
 * @brief The module for handling static arrays
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_ARRAY_H_
#define SYSTEM_CORE_OC_ARRAY_H_

//==========================================================================================================================================
/**
 * @brief returns size of static array
 */
//==========================================================================================================================================
#define oC_ARRAY_SIZE( ARRAY )                                      ( sizeof(ARRAY) / sizeof(ARRAY[0]) )
#define oC_ARRAY_ELEMENT_REFERENCE( ARRAY , INDEX )                 &ARRAY[ INDEX ]
#define oC_ARRAY_LAST_ELEMENT_INDEX( ARRAY )                        (oC_ARRAY_SIZE(ARRAY) - 1)
#define oC_ARRAY_LAST_ELEMENT( ARRAY )                              ARRAY[ oC_ARRAY_LAST_ELEMENT_INDEX(ARRAY) ]
#define oC_ARRAY_LAST_ELEMENT_REFERENCE( ARRAY )                    &oC_ARRAY_ELEMENT_REFERENCE( ARRAY , oC_ARRAY_LAST_ELEMENT_INDEX(ARRAY))
#define oC_ARRAY_IS_INDEX_CORRECT( ARRAY , INDEX )                  ( INDEX < oC_ARRAY_SIZE(ARRAY) )
#define oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(ARRAY,SIZE,ELEMENT)     for( __typeof__(ARRAY[0]) * ELEMENT = ARRAY ; ELEMENT < &ARRAY[SIZE]; ELEMENT++)
#define oC_ARRAY_FOREACH_IN_ARRAY(ARRAY,ELEMENT_NAME)               oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(ARRAY,oC_ARRAY_SIZE(ARRAY),ELEMENT_NAME)
#define oC_ARRAY_FOREACH_BREAK(ARRAY)                               break

#endif /* SYSTEM_CORE_OC_ARRAY_H_ */
