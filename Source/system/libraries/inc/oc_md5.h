/** ****************************************************************************************************************************************
 *
 * @file       oc_md5.h
 *
 * @brief      __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 6 wrz 2015 16:46:59) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Md5 Md5 - The MD5 hash function
 * @ingroup LibrariesSpace
 * @brief The library for MD5 hash function
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_MD5_H_
#define SYSTEM_LIBRARIES_INC_OC_MD5_H_

#include <stdint.h>

/** ========================================================================================================================================
 *
 *              The section with static inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________

static inline const char * md5( const char * String )
{
    return String;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#endif /* SYSTEM_LIBRARIES_INC_OC_MD5_H_ */
