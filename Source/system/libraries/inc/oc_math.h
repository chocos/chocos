/** ****************************************************************************************************************************************
 *
 * @file          oc_math.h
 *
 * @brief      Basic math operations
 *
 * @author     Patryk Kubiak - (Created on: 29 06 2015 19:23:17)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Math Math - Basic math operations
 * @ingroup LibrariesSpace
 * @brief The library for basic math operations
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_MATH_H_
#define SYSTEM_LIBRARIES_INC_OC_MATH_H_

/** ========================================================================================================================================
 *
 *              The section with interface macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_MACROS_SECTION___________________________________________________________________
//! @addtogroup Math
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * Returns absolute difference between A and B values |A-B|
 */
//==========================================================================================================================================
#define oC_ABS(A,B)     ( ((A) > (B)) ? ((A)-(B)) : ((B)-(A)) )

#define oC_MIN(A,B)     ( ((A)>(B)) ? (B) : (A) )
#define oC_MAX(A,B)     ( ((A)>(B)) ? (A) : (B) )

#define abs(C)          oC_ABS(C,0)

#undef  _________________________________________INTERFACE_MACROS_SECTION___________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @addtogroup Math
//! @{

static inline bool oC_IsPowerOf2(oC_UInt_t Count )
{
    return ((Count != 0) && !(Count & (Count - 1)));
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @}

#endif /* SYSTEM_LIBRARIES_INC_OC_MATH_H_ */
