/** ****************************************************************************************************************************************
 *
 * @brief      Interface for standard function pointers
 * 
 * @file          oc_stdfunc.h
 *
 * @author     Patryk Kubiak - (Created on: 20.04.2017 23:58:54) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_POSIX_PTRS_H_
#define SYSTEM_LIBRARIES_INC_OC_POSIX_PTRS_H_

#include <stdint.h>
#include <oc_program_types.h>

#ifdef oC_USER_SPACE
extern oC_SystemCall_t SystemCalls[];
#endif

#define POSIX_FUNCTIONS_LIST(ADD_FUNCTION)      \
    ADD_FUNCTION( vprintf              ) \
    ADD_FUNCTION( printf               ) \
    ADD_FUNCTION( puts                 ) \
    ADD_FUNCTION( putc                 ) \
    ADD_FUNCTION( sprintf_s            ) \
    ADD_FUNCTION( snprintf             ) \
    ADD_FUNCTION( scanf                ) \
    ADD_FUNCTION( sscanf               ) \
    ADD_FUNCTION( oC_GetArgumentAfter  ) \
    ADD_FUNCTION( oC_ArgumentOccur     ) \
    ADD_FUNCTION( oC_GetArgumentNumber ) \
    ADD_FUNCTION( oC_IsMemSetTo        ) \
    ADD_FUNCTION( fopen                ) \
    ADD_FUNCTION( fclose               ) \
    ADD_FUNCTION( fread                ) \
    ADD_FUNCTION( fwrite               ) \
    ADD_FUNCTION( feof                 ) \
    ADD_FUNCTION( fsize                ) \
    ADD_FUNCTION( fgetc                ) \
    ADD_FUNCTION( fgets                ) \
    ADD_FUNCTION( file_exist           ) \
    ADD_FUNCTION( _malloc              ) \
    ADD_FUNCTION( _free                ) \
    ADD_FUNCTION( _rawmalloc           ) \
    ADD_FUNCTION( _rawfree             ) \
    ADD_FUNCTION( _kmalloc             ) \
    ADD_FUNCTION( _kfree               ) \
    ADD_FUNCTION( _krawmalloc          ) \
    ADD_FUNCTION( _krawfree            ) \
    ADD_FUNCTION( _smartalloc          ) \
    ADD_FUNCTION( _smartfree           ) \
    ADD_FUNCTION( _ksmartalloc         ) \
    ADD_FUNCTION( _ksmartfree          ) \
    ADD_FUNCTION( isram                ) \
    ADD_FUNCTION( isrom                ) \
    ADD_FUNCTION( isbufferinram        ) \
    ADD_FUNCTION( isbufferinrom        ) \
    ADD_FUNCTION( isaddresscorrect     ) \
    ADD_FUNCTION( _kdebuglog           ) \

typedef enum
{
#define ADD_FUNCTION(NAME)      oC_Posix_CallId_##NAME ,
    POSIX_FUNCTIONS_LIST(ADD_FUNCTION)
#undef ADD_FUNCTION
    oC_Posix_CallId_NumberOfCalls ,
} oC_Posix_CallId_t;

#ifndef EXTERNAL_FUNC
#   if defined(oC_CORE_SPACE)
#       define EXTERNAL_FUNC(NAME,PROTO)        NAME PROTO
#       define EXTERNAL_PREFIX                  extern
#   elif defined(oC_USER_SPACE)
#       define EXTERNAL_FUNC(NAME,PROTO)        (*NAME) PROTO
#       if defined(oC_PROGRAM_DATA)
#           define EXTERNAL_PREFIX
#       else
#           define EXTERNAL_PREFIX                  extern
#       endif
#   else
#       define EXTERNAL_FUNC(NAME,PROTO)        (*_##NAME) PROTO
#       define EXTERNAL_PREFIX
#   endif
#endif

#endif /* SYSTEM_LIBRARIES_INC_OC_POSIX_PTRS_H_ */
