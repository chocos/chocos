/** ****************************************************************************************************************************************
 *
 * @brief      file with structure operations
 *
 * @file       oc_struct.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_STRUCT_H_
#define SYSTEM_LIBRARIES_INC_OC_STRUCT_H_

#include <string.h>
#include <oc_math.h>

#define oC_AreEqual(S1,S2)                  ( memcmp(&S1,&S2, oC_MIN(sizeof(S1),sizeof(S2))) == 0 )
#define oC_Struct_Copy(S1,S2)               memcpy(&S1,&S2,oC_MIN(sizeof(S1),sizeof(S2)))
#define oC_Struct_Initialize(S,Value)       memset(&S,Value,sizeof(S))
#define oC_Struct_Define(Type,VariableName) Type VariableName;\
                                            memset(&VariableName,0,sizeof(VariableName))

#endif /* SYSTEM_LIBRARIES_INC_OC_STRUCT_H_ */
