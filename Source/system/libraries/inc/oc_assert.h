/** ****************************************************************************************************************************************
 *
 * @file          oc_assert.h
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 22:37:24) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Assert Assertions
 * @ingroup LibrariesSpace
 * @brief The module for handling assertions
 * 
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_OC_ASSERT_H_
#define SYSTEM_CORE_OC_ASSERT_H_

#include <oc_debug_cfg.h>
#include <oc_compiler.h>
#include <assert.h>

extern void oC_MCS_HALT();

#define _STATIC_ASSERT(COND,MSG)
//==========================================================================================================================================
/**
 * @defgroup Assert Assertions
 * @{
 *
 */

/**
 * @def oC_ASSERT( CONDITION )
 *
 * dsdsa
 */

/**
 * @}
 */
//==========================================================================================================================================
#if !defined(CFG_ENABLE_ASSERTIONS)
#   error CFG_ENABLE_ASSERTIONS is not defined in oc_debug_cfg.h
#elif CFG_ENABLE_ASSERTIONS == ON
#   define oC_STATIC_ASSERT( CONDITION , MSG)   _Static_assert( CONDITION , MSG )
#   if !defined( CFG_LEVEL_ASSERTIONS )
#       error CFG_LEVEL_ASSERTIONS is not defined in oc_debug_cfg.h
#   elif CFG_LEVEL_ASSERTIONS == LEVEL(0)
#       define oC_ASSERT( CONDITION )           if ( !(CONDITION) ) { \
		                                                            oC_MCS_HALT("Assertion failed: " #CONDITION); \
		                                                            while(1); \
                                                }
#   elif CFG_LEVEL_ASSERTIONS == LEVEL(1)
#       include <oc_debug.h>
#       define oC_ASSERT( CONDITION )           if ( !(CONDITION) ) kdebuglog(oC_LogType_Error, "%s:%d - Assertion failed: " #CONDITION , oC_FUNCTION , oC_LINE )
#   else
#       error CFG_LEVEL_ASSERTIONS is not correctly defined in oc_debug_cfg.h (maximum level is LEVEL(1))
#   endif

#elif CFG_ENABLE_ASSERTIONS == OFF
#   define oC_ASSERT( CONDITION )
#   define oC_STATIC_ASSERT( CONDITION , MSG)
#else
#   error CFG_ENABLE_ASSERTIONS is not correctly defined in oc_debug_cfg.h (possible only ON / OFF values)
#endif

#if !defined(CFG_ENABLE_IMPLEMENT_FAILURES)
#   error CFG_ENABLE_IMPLEMENT_FAILURES is not defined in oc_debug_cfg.h
#elif CFG_ENABLE_IMPLEMENT_FAILURES == ON
#   define oC_IMPLEMENT_FAILURE()         while(1)
#elif CFG_ENABLE_IMPLEMENT_FAILURES == OFF
#   define oC_IMPLEMENT_FAILURE()
#else
#   error CFG_ENABLE_IMPLEMENT_FAILURES is not correctly defined in oc_debug_cfg.h (possible only ON / OFF values)
#endif

#if !defined(CFG_ENABLE_COMPILE_ASSERTS)
#   error CFG_ENABLE_COMPILE_ASSERTS is not defined in oc_debug_cfg.h
#elif CFG_ENABLE_COMPILE_ASSERTS == ON
#   define oC_COMPILE_ASSERT( CONDITION , MSG ) _Static_assert(CONDITION , MSG)
#elif CFG_ENABLE_COMPILE_ASSERTS == OFF
#   define oC_COMPILE_ASSERT( CONDITION )
#else
#   error CFG_ENABLE_COMPILE_ASSERTS is not correctly defined in oc_debug_cfg.h (only ON / OFF values are possible)
#endif

#endif /* SYSTEM_CORE_OC_ASSERT_H_ */
