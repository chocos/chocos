/** ****************************************************************************************************************************************
 *
 *    @brief      File with definitions of code errors
 *
 *    @author     Patryk Kubiak - (Created on: 9 gru 2014 12:12:45) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Errors Errors
 * @ingroup LibrariesSpace
 * @brief The module for handling errors
 * 
 ******************************************************************************************************************************************/
#ifndef SYSTEM_GEN_OC_ERRORS_H_
#define SYSTEM_GEN_OC_ERRORS_H_

/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <oc_1word.h>
#include <oc_compiler.h>

/*==========================================================================================================================================
//
//     ERROR CODES
//
//========================================================================================================================================*/


/**
 * List of errors in the system. To add new code of error use ADD macro with parameters:
 *  ADD( CODE_NAME , "ERROR DESCRIPTION" )
 *          CODE_NAME - no spaces name
 *          ERROR DESCRIPTION - string with description of the error
 */
#define oC_ERRORS_LIST(ADD) \
    ADD( AllocationError ,                                  "Cannot allocate memory" )\
    ADD( ReleaseError ,                                     "Release memory error" )\
    ADD( TimeoutError ,                                     "Timeout error" )\
    ADD( ReleasingMutexError ,                              "Releasing mutex error" )\
    ADD( VoltageNotSupported,                               "Voltage is not supported" )\
    ADD( ChannelNotConfigured,                              "Channel not configured" )\
    ADD( ExtiLineIsUsed,                                    "Exti line is used" )\
    ADD( ParentProcessNotSet,                               "None of processes contains this thread" )\
    ADD( ThreadNotSet,                                      "Thread not set" )\
    ADD( ProhibitedByArchitecture,                          "Prohibited by architecture" )\
    ADD( PixelFormatNotCorrect,                             "Pixel format not correct" )\
    ADD( PixelFormatNotConfigured,                          "Pixel format not configured" )\
    ADD( WrongLayer,                                        "Wrong layer" )\
    ADD( AddressNotAligned,                                 "Address is not aligned" )\
    ADD( TestFailed,                                        "Test failed" )\
    ADD( SyntaxNotCorrect,                                  "Syntax not correct" )\
    ADD( NoCommandSpecified,                                "No command specified" )\
    ADD( OutputBufferTooSmall,                              "Output buffer is too small" )\
    ADD( WrongModuleIndex,                                  "Wrong module index" )\
    ADD( DaemonThreadAlreadyExists,                         "Daemon thread already exists" )\
    ADD( LoopbackNotSupported,                              "Loopback not supported" )\
    ADD( NotAllSent,                                        "Not all selements sent" )\
    ADD( CannotFindModulePin,                               "Cannot find module pin" )\
    ADD( WrongModulePinChannel,                             "Wrong module pin channel" )\
    ADD( ModulePinIsNotGiven,                               "Module pin is not given" )\
    ADD( NoneOfModulePinAssigned,                           "None of module pin assigned to the given pin" )\
    ADD( DaemonProcessAlreadyExists,                        "Daemon process already exists" )\
    ADD( ChipNotDefined,                                    "Chip is not defined" )\
    ADD( CannotCreateProcess,                               "Cannot create process" )\
    ADD( ArrayNotCorrect,                                   "Array not correct" )\
    ADD( ArraySizeTooBig,                                   "Array size too big" )\
    ADD( CannotCreateThread,                                "Cannot create thread" )\
    ADD( CannotInitializeModule,                            "Cannot initialize module" )\
    ADD( NotHandledByFileSystem,                            "Not handled by file system" )\
    ADD( CannotRunThread,                                   "Cannot run thread" )\
    ADD( NotAllPinsInTheSameState,                          "Not all pins in the same state" )\
    ADD( FileSystemNotCorrect,                              "File System not correct" )\
    ADD( PathAlreadyUsed,                                   "Path already used" )\
    ADD( DriverContextNotSet,                               "Driver context not set" )\
    ADD( StringNotAsExpected,                               "String is not as expected" )\
    ADD( SpecifierNotHandled,                               "Specifier not handled" )\
    ADD( WrongNumberFormat,                                 "Wrong number format" )\
    ADD( NoSuchFile,                                        "No such file" )\
    ADD( FileAlreadyExists,                                 "File already exists" )\
    ADD( FileIsReadOnly,                                    "File is read only" )\
    ADD( CannotCreateFile,                                  "Cannot create file" )\
    ADD( CannotDeleteFile,                                  "Cannot delete file" )\
    ADD( DirectoryNotExists,                                "Directory not exists" )\
    ADD( VoltageNotCorrect,                                 "Voltage is not correct" )\
    ADD( ExistingFileNotCorrect,                            "Existing file is not correct" )\
    ADD( FileIsBusy,                                        "File is busy" )\
    ADD( FileIsEmpty,                                       "File is empty" )\
    ADD( EndOfFile,                                         "End of file" )\
    ADD( FileNotCorrect,                                    "File not correct" )\
    ADD( NoAllBytesRead,                                    "No all bytes read" )\
    ADD( OffsetTooBig,                                      "Offset too big" )\
    ADD( ModeNotCorrect,                                    "Mode not correct" )\
    ADD( DigitBaseNotCorrect,                               "Digit base is not correct" )\
    ADD( StreamTypeNotCorrect ,                             "Stream type is not correct" )\
    ADD( ProcessNotCorrect ,                                "Process is not correct" )\
    ADD( RecursiveDriverUsage,                              "Driver cannot be used in recursive mode" )\
    ADD( ThreadNotCorrect ,                                 "Thread is not correct" )\
    ADD( WidthTooBig,                                       "Width too big" )\
    ADD( HeightTooBig,                                      "Height too big" )\
    ADD( InvertNotSupported,                                "Invert not supported" )\
    ADD( DefaultProgramNotSet,                              "Default program is not set" )\
    ADD( StreamNotCorrect ,                                 "Stream is not correct" )\
    ADD( ContextNotCorrect ,                                "Context is not correct" )\
    ADD( CannotDeleteCurrentThread,                         "Cannot delete current thread" )\
    ADD( CannotDeleteProcess,                               "Cannot delete process" )\
    ADD( ListNotCorrect,                                    "List is not correct" )\
    ADD( BitOrderNotSupported,                              "Bit order is not supported" )\
    ADD( PathNotCorrect,                                    "Path not correct" )\
    ADD( ParityNotSupported,                                "Parity is not supported" )\
    ADD( ParityNotCorrect,                                  "Parity is not correct" )\
    ADD( StopBitNotSupported,                               "Stop bit is not supported" )\
    ADD( Timeout,                                           "Timeout" )\
    ADD( ModuleIsBusy,                                      "Module is busy" )\
    ADD( NoFileSystemMounted,                               "No file system mounted" )\
    ADD( NoSupportedByFileSystem,                           "No supported by file system" )\
    ADD( ObjectNotCorrect,                                  "Object is not correct" )\
    ADD( ObjectNotFoundOnList,                              "Object not found on list" )\
    ADD( SomeDataLost ,                                     "Some data was lost" )\
    ADD( CannotUnblockThread,                               "Cannot unblock thread" )\
    ADD( RootUserNotExist,                                  "Root user not exists" )\
    ADD( CannotRemoveRoot,                                  "Cannot remove root" )\
    ADD( UserInUse,                                         "User already in use" )\
    ADD( UserExists,                                        "User already exists" )\
    ADD( UserNotExists,                                     "User doesn't exist" )\
    ADD( CannotRenameUser,                                  "Cannot rename user" )\
    ADD( InvalidUserName,                                   "Invalid user name")\
    ADD( WrongThread,                                       "Wrong thread" )\
    ADD( NotImplemented  ,                                  "Functionality not implemented" )\
    ADD( ImplementError  ,                                  "Developer has forgot about this path!" )\
    ADD( WrongParameters ,                                  "Given parameters are not correct" )\
    ADD( WrongFrequency ,                                   "Given frequency is not correct" )\
    ADD( WrongUser,                                         "User is not correct" )\
    ADD( WrongAddress ,                                     "Given address is not correct" )\
    ADD( WrongConfigAddress ,                               "Given configuration address is not correct" )\
    ADD( WrongEventHandlerAddress ,                         "The event handler address is not correct" )\
    ADD( FrequencyNotPossible ,                             "Configuration of this frequency is not possible" )\
    ADD( OutputAddressNotInRAM ,                            "The address for output function variable is not pointing to RAM section." )\
    ADD( WrongChannel ,                                     "Given channel is not correct" )\
    ADD( BitRateNotCorrect ,                                "Bit rate is not correct" )\
    ADD( SpeedNotCorrect ,                                  "Speed is not correct" )\
    ADD( BitRateNotSupported ,                              "Bit rate is not supported" )\
    ADD( BitRateNotSet ,                                    "Bit rate is not set" )\
    ADD( ChannelIsUsed ,                                    "Given channel is used" )\
    ADD( ValueTooBig,                                       "Given value is too big" )\
    ADD( MaximumValueNotCorrect,                            "Maximum value is not correct" )\
    ADD( CannotRestoreDefaultState,                         "Cannot restore default module state" )\
    ADD( DriverNotTurnedOn ,                                "The driver is not turned on!" )\
    ADD( DriverNotTurnedOff ,                               "The driver is not turned off!" )\
    ADD( DriverNotRegistered,                               "The given driver is not registered!" )\
    ADD( NotHandledByDriver,                                "The given driver not handle this functionality!" )\
    ADD( FunctionNotExists,                                 "Function not exists!" )\
    ADD( ModuleNotStartedYet,                               "Module has not started yet!" )\
    ADD( NoneElementReceived,                               "None element received!" )\
    ADD( ModuleIsTurnedOn,                                  "Module is already turned on!" )\
    ADD( ModuleIsTestedAlready,                             "Module is already tested!" )\
    ADD( InterruptHandlerNotSet,                            "Handler function for interrupt is not set!" )\
    ADD( InterruptHandlerAlreadySet,                        "Handler function for interrupt is already set!" )\
    ADD( CannotTurnOnInterruptInMachineModule,              "Cannot turn on interrupt in the machine!" )\
    ADD( CannotTurnOffInterruptInMachineModule,             "Cannot turn off interrupt in the machine!" )\
    ADD( MachineHeapError,                                  "Machine heap is not correct!" )\
    ADD( PositionNotCorrect,                                "Position is not correct!" )\
    ADD( MachineCanBeDamaged,                               "Unexpected behavior! Machine can be damaged!" )\
    ADD( WrongStackSize,                                    "The given stack size is not correct!" )\
    ADD( PolarityNotCorrect,                                "Polarity not correct!" )\
    ADD( ChannelOperationsNotDisabled,                      "Channel operations not disabled!" )\
    ADD( ChannelOperationsNotEnabled,                       "Channel operations not enabled!" )\
    ADD( CannotEnableInterrupt,                             "Cannot enable interrupt!" )\
    ADD( CannotDisableInterrupt,                            "Cannot disable interrupt!" )\
    ADD( HeapPointersNotInitialized,                        "Heap pointers are not initialized!" )\
    ADD( ModuleNeedRestart,                                 "Restart of the module is required!" )\
    ADD( ModuleUsedByDifferentModule,                       "Module used by different module!" )\
    ADD( CannotInitializeStack,                             "Cannot initialize stack!" )\
    ADD( ClockConfigurationError,                           "Error while configuration of clock" )\
    ADD( UnsupportedOscillator,                             "The given oscillator is not supported" )\
    ADD( ResolutionNotSet,                                  "Resolution not set" )\
    ADD( SystemClockNotConfigured,                          "The system clock is not configured" )\
    ADD( NotSupportedOnTargetMachine,                       "The feature is not supported on the target machine" )\
    ADD( ChannelNotPoweredOn,                               "The given channel is not turned on!" )\
    ADD( CannotEnableChannel,                               "The given channel cannot be turned on!" )\
    ADD( NotSinglePin,                                      "The given pin is not single pin!" )\
    ADD( PowerStateNotCorrect,                              "The given power state is not correct!" )\
    ADD( PeripheralPinNotDefined,                           "Peripheral pin is not defined" )\
    ADD( PinNotDefined,                                     "Pin is not defined" )\
    ADD( WordLengthNotCorrect,                              "Word length is not correct" )\
    ADD( ColorNotCorrect,                                   "Color is not correct" )\
    ADD( PinIsUsed,                                         "Pin is used" )\
    ADD( PinIsNotUsed,                                      "Pin is not used" )\
    ADD( OutputArrayToSmall,                                "Output array is too small" )\
    ADD( MachineSpecificValueNotCorrect,                    "The machine specific value is not correct!" )\
    ADD( SizeNotCorrect,                                    "The size is not correct!" )\
    ADD( NoChannelAvailable,                                "None of channel is available!" )\
    ADD( CannotAddObjectToList,                             "Cannot add object to list!" )\
    ADD( CannotRemoveObjectFromList,                        "Cannot remove object from list!" )\
    ADD( CannotDeleteObject,                                "Cannot delete object!" )\
    ADD( IndexNotCorrect,                                   "Index is not correct" )\
    ADD( CommandNotCorrect,                                 "Command is not correct!" )\
    ADD( CommandNotHandled,                                 "Command is not handled by the module!" )\
    ADD( DmaModeNotCorrect,                                 "DMA mode is not correct" )\
    ADD( DMAAddressNotHandledByDma,                         "The address is not handled by the DMA!" )\
    ADD( DMAElementSizeNotCorrect,                          "Element size is not correct!" )\
    ADD( DMANumberOfTransfersNotCorrect,                    "Number of transfers is not correct!" )\
    ADD( DMABufferSizeMustBePowerOf2,                       "Buffer size must be power of 2!" )\
    ADD( DMASoftwareTransferNotPossibleOnThisChannel,       "Software transfer is not possible on this channel!" )\
    ADD( DMAPeripheralTransferNotPossibleOnThisChannel,     "This peripheral transfer is not possible on this channel!" )\
    ADD( DMATransmitDirectionNotCorrect,                    "Transmit direction is not correct!" )\
    ADD( PinNeedUnlock,                                     "This pin need to be unlocked before usage" )\
    ADD( GPIOPinNeedUnlock,                                 "This pin need to be unlocked before usage" )\
    ADD( GPIOPinIncorrect ,                                 "Pin is incorrect" )\
    ADD( GPIOPinIsUsed ,                                    "Pin is used" )\
    ADD( GPIOCurrentNotCorrect ,                            "The given GPIO current is not correct" )\
    ADD( GPIOModeNotCorrect ,                               "The given GPIO mode is not correct" )\
    ADD( PullNotCorrect ,                                   "The given pull is not correct" )\
    ADD( OutputCircuitNotCorrect ,                          "The given output circuit mode is not correct" )\
    ADD( GPIOIntTriggerNotCorrect ,                         "The given GPIO interrupt trigger is not correct" )\
    ADD( GPIOModeConfigureError ,                           "Error while setting mode of GPIO pin" )\
    ADD( GPIOOutputCircuitConfigureError ,                  "Error while setting output circuit of GPIO pin" )\
    ADD( GPIOPullConfigureError ,                           "Error while setting pull of GPIO pin" )\
    ADD( GPIOCurrentConfigureError ,                        "Error while setting current of GPIO pin" )\
    ADD( GPIOSpeedConfigureError ,                          "Error while setting speed of GPIO pin" )\
    ADD( GPIOInterruptConfigureError ,                      "Error while setting interrupt of GPIO pin" )\
    ADD( GPIOADDInterruptToListError ,                      "Error while adding interrupt of GPIO pin to list" )\
    ADD( SYSUnknownOscillatorSource ,                       "Unknown oscillator source" )\
    ADD( SYSConfigurationError ,                            "Error while configuration of system clock" )\
    ADD( OSPIDNotExists ,                                   "Process with given ID not exists" )\
    ADD( OSNotAllThreadsCancelled,                          "One or more threads cannot be cancelled" )\
    ADD( OSDuplicatePID ,                                   "Process ID is duplicated" )\
    ADD( OSProcessNotFinished ,                             "Process is not finished" )\
    ADD( OSGlobalVariableRegistrationError ,                "Error while registration of the global variable" )\
    ADD( OSCannotKillChildProcess ,                         "At least one of children processes cannot be killed" )\
    ADD( OSWrongThread ,                                    "Not correct thread reference" )\
    ADD( OSWrongTTY,                                        "Not correct TTY reference" )\
    ADD( TTYConfigurationIncorrect ,                        "Configuration is not correct" )\
    ADD( TTYCannotCreateDLIST ,                             "Cannot create dynamic list" )\
    ADD( TTYCannotCreateMutex,                              "Cant create mutex object" )\
    ADD( UARTNotCorrectChannel,                             "The given channel number is not correct" )\
    ADD( UARTCannotPrepareTRAHN,                            "Error while creating TRANH object" )\
    ADD( UARTPinNotDefined,                                 "Given pin is not defined" )\
    ADD( UARTCantConfigurePin,                              "Cannot configure GPIO pin" )\
    ADD( UARTNotCorrectWordLength,                          "Given word length is not correct" )\
    ADD( UARTWordLengthNotSupported,                        "Given word length is not supported on selected architecture" )\
    ADD( UARTParityNotSupported,                            "Given parity mode is not supported on selected architecture" )\
    ADD( UARTStopBitNotSupported,                           "Given stop bit mode is not supported on selected architecture" )\
    ADD( UARTBitOrderNotSupported,                          "Given bit order mode is not supported on selected architecture" )\
    ADD( UARTInvertNotSupported,                            "Given invert mode is not supported on selected architecture" )\
    ADD( UARTBitRateNotSupported,                           "Given bit-rate is not supported on this architecture" )\
    ADD( UARTInterruptConfigureError,                       "Error while setting interrupt priority for UART channel" )\
    ADD( TIMLockingLockedChannel,                           "Trying to lock locked channel" )\
    ADD( TIMUnlockingUnlockedChannel,                       "Trying to unlock channel which is not locked" )\
    ADD( TIMChannelNotConfigured,                           "The selected channel is not configured" )\
    ADD( TIMMatchCallbackNotCorrect,                        "The match callback function pointer is not correct" )\
    ADD( TIMMatchContextNotCorrect,                         "The match callback context pointer is not correct" )\
    ADD( TIMTimeoutCallbackNotCorrect,                      "The timeout callback function pointer is not correct" )\
    ADD( TIMTimeoutContextNotCorrect,                       "The timeout callback context pointer is not correct" )\
    ADD( TIMNoneOfChannelsAvailable,                        "None of channel available" )\
    ADD( TIMModeNotCorrect,                                 "Timer mode is not correct" )\
    ADD( TIMERCountsDirNotCorrect,                          "Count direction is not correct" )\
    ADD( TIMERSubTimerNotCorrect,                           "SubTimer is not correct" )\
    ADD( TIMERBothSubTimersNeeeded,                         "Both SubTimers are needed" )\
    ADD( TIMERCountsDirNotPossible,                         "Count direction is not possible with this configuration" )\
    ADD( TIMERModeNotCorrect,                               "Mode is not correct" )\
    ADD( TIMERModeNotSelected,                              "Timer mode not selected" )\
    ADD( TIMERTriggerNotCorrect,                            "Input trigger is not correct" )\
    ADD( TIMERChannelNotStopped,                            "Channel is not stopped" )\
    ADD( TIMERFrequencyNotPossibleInFullMode,               "Frequency not possible in full timer mode" )\
    ADD( TIMERMaximumValueTooBig,                           "Maximum value is too big" )\
    ADD( TIMTickSourceNotSupported,                         "The selected TickSource is not supported" )\
    ADD( TIMSizeNotSupported,                               "The given size is not supported" )\
    ADD( TIMTickPeriodNotSupported,                         "The given tick period is not supported" )\
    ADD( TIMSizeNotSupportedOnSelectedChannel,              "The given size is not supported on selected channel" )\
    ADD( TIMMatchValueTooBig,                               "The given match value is too big" )\
    ADD( TIMMaxValueTooBig,                                 "The given max value is too big" )\
    ADD( TIMCantCalculatePrescaler,                         "Error while calculating prescaler" )\
    ADD( TIMTickPeriodTooSmall,                             "The given tick period is too small for this channel" )\
    ADD( TIMTickPeriodTooBig,                               "The given tick period is too big for this channel" )\
    ADD( TIMCountsDirNotSupported,                          "The given counts direction is not supported" )\
    ADD( TIMValueTooBig,                                    "The given timer value is too big for this channel" )\
    ADD( TimeAlreadyTurnedOn,                               "Time module is already turned on" )\
    ADD( TimeAlreadyTurnedOff,                              "Time module is already turned off" )\
    ADD( TimeCannotCreateWakeUpList,                        "Error while creating wake up list" )\
    ADD( TimeCannotCreateConfigEvent,                       "Error while creating configuration event" )\
    ADD( ClockPhaseNotSupported,                            "Given clock phase is not supported on selected architecture" )\
    ADD( ClockPolarityNotSupported,                         "Given clock polarity is not supported on selected architecture" )\
    ADD( ChipSelectPolarityNotSupported,                    "Given chip select polarity is not supported on selected architecture" )\
    ADD( FunctionNotSupportedInCurrentMode,                 "The function is not supported in current mode" )\
    ADD( ModeNotSupported,                                  "Given mode is not supported on selected architecture" )\
    ADD( FrameWidthNotSupported,                            "Given frame width is not supported on selected architecture" )\
    ADD( FrameTypeNotSupported,                             "Given frame type is not supported on selected architecture" )\
    ADD( StateNotSupported,                                 "Given state of Chip Select is not supported" )\
    ADD( PulseWidthTooBig,                                  "Pulse width too big" )\
    ADD( AddressNotInRam,                                   "Address is not in ram" )\
    ADD( WrongBufferEndAddress,                             "Wrong buffer end address" )\
    ADD( ColorFormatNotSupported,                           "Color format not supported" )\
    ADD( ColorFormatNotCorrect,                             "Color format not correct" )\
    ADD( ColorMapNotCorrect,                                "Color map not correct" )\
    ADD( TimingParametersNotSupported,                      "Timing parameters not supported" )\
    ADD( TimingParametersNotFilled,                         "Timing parameters not filled" )\
    ADD( AddressNotInExternalMemory,                        "Address is not stored in external memory" )\
    ADD( HeapMapAlreadyConfigured,                          "Heap map already configured" )\
    ADD( HeapMapNotConfigured,                              "Heap map not configured" )\
    ADD( WritingNotPermitted,                               "Writing not permitted" )\
    ADD( ReadingNotPermitted,                               "Reading not permitted" )\
    ADD( ProtectionNotCorrect,                              "Protection not correct" )\
    ADD( DataBusWidthNotCorrect,                            "Data bus width not correct" )\
    ADD( DataBusWidthNotSupported,                          "Data bus width not supported" )\
    ADD( AccessWidthNotCorrect,                             "Access width not correct" )\
    ADD( SizeTooBig,                                        "Size too big" )\
    ADD( NoFreeBankAvailable,                               "No free bank available" )\
    ADD( MemoryTypeNotCorrect,                              "Memory type is not correct" )\
    ADD( DelayNotPossible,                                  "Delay is not possible" )\
    ADD( RowBitsNumberNotSupported,                         "Row bits number not supported" )\
    ADD( ColumnBitsNumberNotSupported,                      "Column bits number not supported" )\
    ADD( AutoStackMethodNotCorrect,                         "Auto stack method is not correct" )\
    ADD( NumberOfBanksNotCorrect,                           "Number of banks not correct" )\
    ADD( NumberOfBanksNotSupported,                         "Number of banks not supported" )\
    ADD( CasLatencyNotCorrect,                              "CAS Latency not correct" )\
    ADD( CasLatencyNotSupported,                            "CAS Latency not supported" )\
    ADD( DirectAccessNotPossible,                           "Direct access not possible" )\
    ADD( CannotAccessMemory,                                "Cannot access memory" )\
    ADD( AccessPermissionsNotPossible,                      "Access permissions are not possible" )\
    ADD( AccessPermissionsNotCorrect,                       "Access permissions are not correct" )\
    ADD( RegionNumberNotCorrect,                            "Region number is not correct" )\
    ADD( AutoConfigurationsNotAvailable,                    "Auto configurations not available" )\
    ADD( NumberOfRowsNotCorrect,                            "Number of rows is not correct" )\
    ADD( MemoryNotConfigured,                               "Memory not configured" )\
    ADD( AutoRefreshPeriodNotCorrect,                       "Auto refresh period not correct" )\
    ADD( ReadPipeDelayNotPossible,                          "Read pipe delay not possible" )\
    ADD( NotGraphicDriver,                                  "Driver is not graphic driver!" )\
    ADD( AlreadyConfigured,                                 "Already configured" )\
    ADD( NotConfiguredYet,                                  "Not configured yet" )\
    ADD( ObjectProtected,                                   "Object is protected" )\
    ADD( CommunicationInterfaceNotCorrect,                  "Communication interface not correct" )\
    ADD( PinNotSet,                                         "Required pin is not set" )\
    ADD( CannotDisableChannel,                              "Cannot disable channel" )\
    ADD( MacNotInitialized,                                 "MAC not initialized" )\
    ADD( PhyAddressNotCorrect,                              "PHY address not correct" )\
    ADD( RegisterAddressNotCorrect,                         "Register address not correct" )\
    ADD( CannotAccessRegister,                              "Cannot access register" )\
    ADD( OtherCommunicationInterfaceInUse,                  "Other communication interface in use" )\
    ADD( NoMoreConfigurationsPossible,                      "No more configurations possible" )\
    ADD( ChipNameNotDefined,                                "Chip name is not defined" )\
    ADD( DriverTypeNotCorrect,                              "Driver type not correct" )\
    ADD( StringIsEmpty,                                     "String is empty" )\
    ADD( StringIsTooLong,                                   "String is too long" )\
    ADD( CannotGiveSemaphore,                               "Cannot give semaphore" )\
    ADD( FoundOnList,                                       "Already found on the list" )\
    ADD( NoneActiveNetif,                                   "None of network interfaces isn't active" )\
    ADD( IpAddressNotCorrect,                               "IP address is not correct" )\
    ADD( StaticIpNotSet,                                    "Static IP is not set" )\
    ADD( IpAddressEmpty,                                    "IP address is empty" )\
    ADD( AddressTypeNotCorrect,                             "Address type is not correct" )\
    ADD( RoutingTableEntryNotFound,                         "Routing table entry not found" )\
    ADD( NetifNotCorrect,                                   "Netif not correct" )\
    ADD( ProcessAlreadyStarted,                             "Process has started already" )\
    ADD( CannotDeleteThread,                                "Cannot delete thread" )\
    ADD( LinkNotDetected,                                   "Link not detected" )\
    ADD( UnknownHardwareType,                               "Unknown hardware type" )\
    ADD( UnknownAddressType,                                "Unknown address type" )\
    ADD( UnknownHardwareAddress,                            "Unknown hardware address" )\
    ADD( HardwareAddressIsEmpty,                            "Hardware address is empty" )\
    ADD( AlignmentNotCorrect,                               "Alignment not correct" )\
    ADD( DescriptorListNotInitialized,                      "Descriptor list not initialized" )\
    ADD( NoFreeSlots,                                       "No free slots" )\
    ADD( InternalDataAreDamaged,                            "Internal data are damaged (stack has been overwritten?)" )\
    ADD( DataNotAvailable,                                  "Data not available" )\
    ADD( BaudRateNotSupported,                              "Baud rate not supported" )\
    ADD( OperationModeNotCorrect,                           "Operation mode not correct" )\
    ADD( WrongMacAddressIndex,                              "Wrong MAC address index" )\
    ADD( TimeNotCorrect,                                    "Time not correct" )\
    ADD( AllocatorNotCorrect,                               "Allocator not correct" )\
    ADD( ReceiveError,                                      "Receive error" )\
    ADD( UnfinishedPacket,                                  "Unfinished packet" )\
    ADD( MemoryAlreadyAllocated,                            "Memory already allocated" )\
    ADD( MemoryNotAllocated,                                "Memory not allocated" )\
    ADD( UnknownLayer,                                      "Unknown layer" )\
    ADD( NotDmaAddress,                                     "The given address is not correct DMA address" )\
    ADD( CannotConvertHeaderEndianess,                      "Cannot convert header endianess" )\
    ADD( PortNotCorrect,                                    "Port not correct" )\
    ADD( PortNotReserved,                                   "Port not reserved" )\
    ADD( PortNotAvailable,                                  "Port not available" )\
    ADD( PortReservedByDifferentProcess,                    "Port reserved by different process" )\
    ADD( PermissionDenied,                                  "Permission denied" )\
    ADD( PossibleOnlyForRoot,                               "Possible only for root" )\
    ADD( TypeNotCorrect,                                    "Type is not correct" )\
    ADD( PacketNotCorrect,                                  "Packet is not correct" )\
    ADD( CannotFindNetifForTheAddress,                      "Cannot find Netif for the given address" )\
    ADD( ModuleBusy,                                        "Module is busy" )\
    ADD( PortBusy,                                          "Port is busy" )\
    ADD( AddressNotInInternalRam,                           "Address is not in internal RAM" )\
    ADD( SegmentTypeNotSupported,                           "Segment type is not supported" )\
    ADD( HardwareAddressNotFilled,                          "Hardware address not filled" )\
    ADD( SizeNotAligned,                                    "Size is not aligned" )\
    ADD( TypeReservedByDifferentProcess,                    "Type reserved by different process" )\
    ADD( TypeNotReserved,                                   "Type is not reserved" )\
    ADD( TypeBusy,                                          "Type is busy" )\
    ADD( HostNotAvailable,                                  "Host not available" )\
    ADD( NotConnected,                                      "Not connected" )\
    ADD( AlreadyConnected,                                  "Already connected" )\
    ADD( ServerNotStarted,                                  "Server not started" )\
    ADD( DriverImplementedIncorrectly,                      "Driver implemented incorrectly" )\
    ADD( NoSpaceAvailable,                                  "No space available" )\
    ADD( OptionNotFound,                                    "Option not found" )\
    ADD( OptionNotCorrect,                                  "Option not correct" )\
    ADD( OptionNotAdded,                                    "Option not added" )\
    ADD( WindowSizeNotCorrect,                              "Window size not correct" )\
    ADD( ObjectInUse,                                       "Object in use" )\
    ADD( CannotClearOptions,                                "Cannot clear options" )\
    ADD( ConnectionNotRequested,                            "Connection not requested" )\
    ADD( AlreadyRunning,                                    "Already running" )\
    ADD( NotRunning,                                        "Not running" )\
    ADD( ConnectionExpired,                                 "Connection has expired" )\
    ADD( ConnectionFromDifferentServer,                     "Connection from different server" )\
    ADD( ModuleNotCorrect,                                  "Module not correct" )\
    ADD( ModuleAlreadyRegistered ,                          "Module already registered" )\
    ADD( ModuleNotRegistered ,                              "Module not registered" )\
    ADD( PortAlreadyReserved,                               "Port already reserved" )\
    ADD( RequiredModuleNotEnabled,                          "Required module not enabled" )\
    ADD( WrongCommandArgument,                              "Wrong command argument" )\
    ADD( CannotKillProcess,                                 "Cannot kill process" )\
    ADD( ConnectionInProgress,                              "Connection in progress" )\
    ADD( ConnectionHasBeenFinishedTooEarly,                 "Connection has been finished too early" )\
    ADD( ModuleHasNotStartedCorrectly,                      "Module has not started correctly" )\
    ADD( ModuleHasNotBeenStoppedCorrectly,                  "Module has not been stopped correctly" )\
    ADD( ServiceNotStarted,                                 "Service has not started yet" )\
    ADD( ServiceAlreadyStarted,                             "Service already started" )\
    ADD( ProgramNotCorrect,                                 "Program not correct " )\
    ADD( NoDataToSend,                                      "No data to send" )\
    ADD( NoSpaceOnRemoteServer,                             "No space on remote server" )\
    ADD( InterruptsNotEnabled,                              "Interrupts not enabled" )\
    ADD( NoSuchScreen,                                      "No such screen" )\
    ADD( WidthNotCorrect,                                   "Width not correct" )\
    ADD( HeightNotCorrect,                                  "Height not correct" )\
    ADD( FontNotCorrect,                                    "Font not correct" )\
    ADD( DeviceAddressNotCorrect,                           "Device address not correct" )\
    ADD( SpeedModeNotCorrect,                               "Speed mode not correct" )\
    ADD( SpeedModeNotSupported,                             "Speed mode not supported" )\
    ADD( PeripheralNotEnabled,                              "Peripheral not enabled" )\
    ADD( NoDataToReceive,                                   "No data to receive" )\
    ADD( NotMasterMode,                                     "Not master mode" )\
    ADD( MasterMode,                                        "It is master mode" )\
    ADD( AddressModeNotCorrect,                             "Address mode is not correct" )\
    ADD( TransmitFifoIsFull,                                "Transmit fifo is full" )\
    ADD( AddressNotCorrect,                                 "Address not correct" )\
    ADD( InterruptNotSupported,                             "Interrupt not supported" )\
    ADD( AcknowledgeNotReceived,                            "Acknowledge not received" )\
    ADD( ModuleHasHanged,                                   "Module has hanged" )\
    ADD( NotAllDataSent,                                    "Not all data has been sent" )\
    ADD( TransmissionNotStarted,                            "Transmission not started" )\
    ADD( TransmissionStartedForRead,                        "Transmission started for read" )\
    ADD( TransmissionStartedForWrite,                       "Transmission started for write" )\
    ADD( NoRequestFromMaster,                               "No request from master" )\
    ADD( MasterAskedAboutDataWrite,                         "Master asked about data write" )\
    ADD( MasterAskedAboutDataRead,                          "Master asked about data read" )\
    ADD( MasterChangedHisMind,                              "Master has changed his mind" )\
    ADD( RawTransmissionStarted,                            "Raw transmission started" )\
    ADD( RawTransmissionNotStarted,                         "Raw transmission not started" )\
    ADD( BusOwnershipNotTaken,                              "Bus ownership not taken" )\
    ADD( TransmissionNotComplete,                           "Transmission not completed" )\
    ADD( EarlyStopDetected,                                 "Early stop detected" )\
    ADD( ArbitrationLost,                                   "Arbitration lost" )\
    ADD( BusError,                                          "Bus error" )\
    ADD( PecError,                                          "Pec error" )\
    ADD( OverrunError,                                      "Overrun error" )\
    ADD( UnderrunError,                                     "Underrun error" )\
    ADD( IncorrectMask,                                     "Incorrect mask" )\
    ADD( EventNotSupported,                                 "Event not supported" )\
    ADD( UnexpectedRegisterValue,                           "Unexpected register value" )\
    ADD( DriverNotConfiguredYet,                            "Driver not configured yet" )\
    ADD( ChipNotDetected,                                   "Chip not detected" )\
    ADD( UnknownGesture,                                    "Unknown gesture" )\
    ADD( DirectionNotCorrect,                               "Direction is not correct" )\
    ADD( StringLengthNotCorrect,                            "String length is not correct" )\
    ADD( NumberOfElementsNotCorrect,                        "Number of elements is not correct" )\
    ADD( StyleNotCorrect,                                   "Style is not correct" )\
    ADD( NoSuchDefinition,                                  "No such definition" )\
    ADD( IncreaseValueNotCorrect,                           "Increase value not correct" )\
    ADD( CannotRegisterActivity,                            "Cannot register activity" )\
    ADD( WrongWidgetDefinition,                             "Wrong widget definition" )\
    ADD( LayerIndexNotCorrect,                              "Layer index is not correct" )\
    ADD( CannotSwitchColorMapLayer,                         "Cannot switch color map layer" )\
    ADD( NotElfFile,                                        "It is not an ELF file" )\
    ADD( FileNotLoaded,                                     "File not loaded" )\
    ADD( NotExecFile,                                       "It is not an executable file" )\
    ADD( RequiredSectionNotFound,                           "Required section has not been found" )\
    ADD( WrongOffset,                                       "Wrong offset" )\
    ADD( SectionSizeNotCorrect,                             "Section size is not correct" )\
    ADD( NotCBinFile,                                       "It is not an CBin file" )\
    ADD( ProgramHeaderTooSmall,                             "Program header is too small!" )\
    ADD( SectionsDefinitionTooSmall,                        "Sections definition is too small!" )\
    ADD( UnknownSystemCall,                                 "Unknown system call!" )\
    ADD( ProgramNotPrepared,                                "Program not prepared!" )\
    ADD( ArgumentCountNotCorrect,                           "Argument count is not correct" )\
    ADD( FrameFormatNotCorrect,                             "Frame format is not correct" )\
    ADD( ChipIdNotCorrect,                                  "Chip ID is not correct" )\
    ADD( BaudRateNotCorrect,                                "Baud rate is not correct" )\
    ADD( DataSizeNotSupported,                              "Data size is not supported" )\
    ADD( ChannelNotLocked,                                  "Channel is not locked" )\
    ADD( ToleranceNotCorrect,                               "Tolerance is not correct" )\
    ADD( CardNotDetected,                                   "Card has not been detected" )\
    ADD( NotHandledByInterfaceMode,                         "It is not handled in the given interface mode" )\
    ADD( CallbackAlreadySet,                                "Callback already set" )\
    ADD( WrongSectorNumber,                                 "Wrong sector number" )\
    ADD( ObjectAlreadyExist,                                "Object already exist" )\
    ADD( NameNotAvailable,                                  "Name is not available" )\
    ADD( NoSuchPartition,                                   "So such partition" )\
    ADD( PartitionAlreadyExist,                             "Partition already exist" )\
    ADD( WrongPartitionType,                                "Wrong partition type" )\
    ADD( WrongSectorCount,                                  "Wrong sector count" )\
    ADD( StorageNotFound,                                   "Storage has not been found" )\
    ADD( FreeSliceNotFound,                                 "Free slice has not been found" )\
    ADD( LevelNotCorrect,                                   "Level not correct" )\
    ADD( LocationNotFilled,                                 "Location not filled" )\
    ADD( UnknownInterruptSource,                            "Unknown interrupt source" )\
    ADD( PinFunctionNotCorrect,                             "Pin Function is not correct" )\
    ADD( ResponseTypeNotCorrect,                            "Response type is not correct" )\
    ADD( ResponseTypeNotSupported,                          "Response type is not supported" )\
    ADD( CommandNotSent,                                    "Command has not been sent" )\
    ADD( ResponseNotReady,                                  "Response is not ready yet" )\
    ADD( ResponseNotReceived,                               "Response has been not received yet" )\
    ADD( DataPackageNotInitialized,                         "Data package is not initialized" )\
    ADD( DataPackageIsEmpty,                                "Data package is empty" )\
    ADD( DataPackageIsNotEmpty,                             "Data package is not empty" )\
    ADD( DataPackageIsFull,                                 "Data package is full" )\
    ADD( DataPackageIsReadOnly,                             "Data package is read only" )\
    ADD( CommandTransmissionInProgress,                     "Previous command transmission has not finished yet" )\
    ADD( SizeNotSupported,                                  "Size not supported" )\
    ADD( WideBusNotCorrect,                                 "Wide bus is not correct" )\
    ADD( WideBusNotSupported,                               "Wide bus is not supported" )\
    ADD( TooManyOpenFiles,                                  "Too many open files" )\
    ADD( CannotAllocateLfnBuffer,                           "LFN working buffer could not be allocated" )\
    ADD( FileIsLocked,                                      "The operation is rejected according to the file sharing policy" )\
    ADD( NotSupportedByFileSystem,                          "Not supported by the file system" )\
    ADD( SdmmcModeInterfaceNotFound,                        "SDMMC mode interface not found")\
    ADD( SdmmcConfigNotSupported,                           "Given SDMMC configuration is not supported")\
    ADD( CannotSetAddress,                                  "Cannot set address")\
    ADD( InvalidSourceAddress,                              "Invalid source address")\
    ADD( InvalidInitialization,                             "Invalid initialization")\
    ADD( ResponseNotCorrect,                                "Response is not correct")\
    ADD( ResponseNotSupported,                              "Response is not supported")\
    ADD( ClassNotCorrect,                                   "Class is not correct")\
    ADD( ClassNotSupported,                                 "Class is not supported")\
    ADD( InterfaceNotCorrect,                               "Interface is not correct")\
    ADD( OutOfRange,                                        "Out of range")\
    ADD( InvalidBlockLength,                                "Invalid block length")\
    ADD( EraseSequenceError,                                "Erase sequence error")\
    ADD( EraseParameterNotCorrect,                          "Erase parameter not correct")\
    ADD( CardIsLocked,                                      "Card is locked")\
    ADD( LockUnlockFailed,                                  "Lock/Unlock failed")\
    ADD( CommunicationCrcError,                             "Communication CRC Error")\
    ADD( IllegalCommand,                                    "Illegal command")\
    ADD( CardEccFailed,                                     "Card ECC Failed")\
    ADD( UnknownCardError,                                  "Unknown Card Error")\
    ADD( NotEfficientEnough,                                "Not efficient enough")\
    ADD( CIDCSDOverwrite,                                   "CID/CSD Overwrite")\
    ADD( NotEverythingErased,                               "Not everything erased")\
    ADD( ApplicationCommandNotConfirmed,                    "Application command not confirmed")\
    ADD( CommandArgumentCannotBeRead,                       "Command argument cannot be read")\
    ADD( TransferModeNotCorrect,                            "Transfer mode is not correct")\
    ADD( TransferModeNotSupported,                          "Transfer mode is not supported")\
    ADD( ResponseIndexNotCorrect,                           "Response index is not correct")\
    ADD( InterfaceNotSupported,                             "Interface not supported")\
    ADD( InterfaceNotDetected,                              "Interface not detected")\
    ADD( DetectionModeNotCorrect,                           "Detection mode is not correct")\
    ADD( VersionNotCorrect,                                 "Version is not correct")\
    ADD( VersionNotSupported,                               "Version is not supported")\
    ADD( UnitNotSupported,                                  "Unit is not supported")\
    ADD( NoSuchDisk,                                        "No such disk")\
    ADD( SectorSizeNotCorrect,                              "Sector size is not correct")\
    ADD( TransferDirectionNotCorrect,                       "Transfer direction not correct")\
    ADD( BlockSizeNotCorrect,                               "Block size is not correct")\
    ADD( BlockSizeNotSupported,                             "Block size is not supported")\
    ADD( NotSupportedInTheCurrentState,                     "Not supported in the current state")\
    ADD( NotSupportedByInterface,                           "Not supported by the interface")\
    ADD( MissingArgument,                                   "Missing argument")\
    ADD( CannotTakeElement,                                 "Cannot take element")\
    ADD( CannotPushElement,                                 "Cannot push element")\
    ADD( EntryPointNotValid,                                "Entry point is not valid")\
    ADD( GotSectionIsMissing,                               ".got section is missing")\
    ADD( NoProgramHeaderFound,                              "Program header has not been found")\
    ADD( CannotAttachDestructor,                            "Cannot attach destructor")\
    ADD( BssSectionIsMissing,                               "BSS section is missing")\
    ADD( NoSuchMemory,                                      "No such memory")\
    ADD( SyscallsSectionIsMissing,                          "Syscalls section is missing")\
    ADD( ConnectionRoleNotCorrect,                          "Connection Role not correct")\
    ADD( GroupNotCorrect,                                   "Group not correct")\
    ADD( InvalidHttpHeader,                                 "Invalid HTTP header")\
    ADD( HttpVersionNotSupported,                           "HTTP version not supported")\
    ADD( HttpStatusCodeNotSupported,                        "HTTP status code not supported")\
    ADD( HttpPathNotFound,                                  "HTTP path request not found")\
    ADD( TooManyHttpHeaders,                                "Too many HTTP headers")\
    ADD( UnexpectedEndOfHttpHeader,                         "Unexpected end of HTTP header")\
    ADD( HttpHeaderIdNotSupported,                          "HTTP Header ID not supported")\
    ADD( HttpContentTypeNotSupported,                       "HTTP Content Type not supported")\

    // Remember, that if you add new error code to the list, you should also add it into the file oc_libraries.h
    // Without it it, description of the error code is not available in doxygen documentation

/*=========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/
#define _ADD_ERROR_TO_ENUM( CODE_NAME , DESCR )             oC_1WORD_FROM_2( oC_ErrorCode_ , CODE_NAME ) ,

/*==========================================================================================================================================
//
//     TYPES
//
//========================================================================================================================================*/

#ifndef DOXYGEN
typedef enum
{
    oC_ErrorCode_None ,                     //!< None error has occurred, it is used for marking success result
    oC_ERRORS_LIST(_ADD_ERROR_TO_ENUM)
    oC_ErrorCode_NumberOfErrorsDefinitions  //!< Number of defined error codes in the system
} oC_ErrorCode_t;
#endif

/*==========================================================================================================================================
//
//     FUNCTION PROTOTYPES
//
//========================================================================================================================================*/

extern const char * oC_GetErrorString          ( oC_ErrorCode_t ErrorCode );
extern bool         oC_SaveErrorFunction       ( const char * Description    , oC_ErrorCode_t ErrorCode      , const char * Function     , uint32_t      LineNumber );
extern bool         oC_ReadLastError           ( const char ** outDescription, oC_ErrorCode_t * outErrorCode , const char ** outFunction , uint32_t * outLineNumber );
extern void         oC_SetLockSavingErrors     ( bool Lock );

/*==========================================================================================================================================
//
//     INLINE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function assign value for error code destination and return result of error
 *
 * @param outErrorCode      Reference to the error code destination
 * @param ErrorCode         code of error to assign
 *
 * @return true if error code is not error
 *
 * @code
   oC_ErrorCode_t errorCode = oC_ErrorCode_None;

   if (
         oC_AssignErrorCode( &errorCode , oC_SPI_Configure( SPI_Config ) )
      && oC_AssignErrorCode( &errorCode , oC_UART_Configure( UART_Config ) )
      )
   {
      // everything is OK
   }

   return errorCode;
   @endcode
 */
//==========================================================================================================================================
static inline bool oC_AssignErrorCode( oC_ErrorCode_t * outErrorCode , oC_ErrorCode_t ErrorCode )
{
    bool errorOccur = ErrorCode != oC_ErrorCode_None;
    if ( errorOccur )
    {
        outErrorCode[0] = ErrorCode;
    }
    return !errorOccur;
}

//==========================================================================================================================================
/**
 * The function assign an error code, if the Condition is false.
 *
 * @param outErrorCode      Destination for the error code
 * @param Condition         Condition to check
 * @param ErrorCode         Error code to assign if the condition is not true
 *
 * @return false if error occurs
 */
//==========================================================================================================================================
static inline bool oC_AssignErrorCodeIfFalse( oC_ErrorCode_t * outErrorCode , bool Condition , oC_ErrorCode_t ErrorCode )
{
    return oC_AssignErrorCode( outErrorCode , (Condition) ? oC_ErrorCode_None : ErrorCode );
}

//==========================================================================================================================================
/**
 * Checks if some error occurs
 *
 * @param ErrorCode     Code of error to check
 *
 * @return true if error occur
 */
//==========================================================================================================================================
static inline bool oC_ErrorOccur( oC_ErrorCode_t ErrorCode )
{
    return ErrorCode != oC_ErrorCode_None;
}

//==========================================================================================================================================
//==========================================================================================================================================
static inline bool oC_SaveIfErrorOccurFunction( const char * Description , oC_ErrorCode_t ErrorCode , const char * Function, uint32_t LineNumber )
{
    bool success = true;

    if(oC_ErrorOccur(ErrorCode))
    {
        oC_SaveErrorFunction(Description,ErrorCode,Function,LineNumber);
        success = false;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
static inline bool oC_SaveIfFalseFunction( const char * Description , bool Condition , oC_ErrorCode_t ErrorCode , const char * Function, uint32_t LineNumber )
{
    bool success = true;

    if(Condition == false)
    {
        oC_SaveErrorFunction(Description,ErrorCode,Function,LineNumber);
        success = false;
    }

    return success;
}

//==========================================================================================================================================
/**
 * The function assign an error code, if the Condition is false.
 *
 * @param outErrorCode      Destination for the error code
 * @param Condition         Condition to check
 * @param ErrorCode         Error code to assign if the condition is not true
 *
 * @return false if error occurs
 */
//==========================================================================================================================================
static inline bool oC_AssignErrorCodeIfFalseWithSave( oC_ErrorCode_t * outErrorCode , bool Condition , oC_ErrorCode_t ErrorCode , const char * Description, const char * Function , uint32_t LineNumber )
{
    bool errorOccur = oC_AssignErrorCode( outErrorCode , (Condition) ? oC_ErrorCode_None : ErrorCode );

    if(errorOccur)
    {
        oC_SaveErrorFunction(Description,ErrorCode,Function,LineNumber);
    }

    return errorOccur;
}

//==========================================================================================================================================
/**
 * The function assign value for error code destination
 *
 * @param outErrorCode      Reference to the error code destination
 * @param ErrorCode         code of error to assign
 *
 * @return true if error code is not error
 *
 * @code
   oC_ErrorCode_t errorCode = oC_ErrorCode_None;

   if (
         oC_AssignErrorCode( &errorCode , oC_SPI_Configure( SPI_Config ) )
      && oC_AssignErrorCode( &errorCode , oC_UART_Configure( UART_Config ) )
      )
   {
      // everything is OK
   }

   return errorCode;
   @endcode
 */
//==========================================================================================================================================
static inline bool oC_AssignErrorCodeWithSave( oC_ErrorCode_t * outErrorCode , oC_ErrorCode_t ErrorCode , const char * Description, const char * Function , uint32_t LineNumber )
{
    bool errorOccur = ErrorCode != oC_ErrorCode_None;
    if ( errorOccur )
    {
        oC_SaveErrorFunction(Description,ErrorCode,Function,LineNumber);
        outErrorCode[0] = ErrorCode;
    }
    return !errorOccur;
}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define ErrorCondition( Condition , ErrorCode )            oC_AssignErrorCodeIfFalse(&errorCode , Condition , ErrorCode)
#define ErrorCode( ErrorCode )                             oC_AssignErrorCode(&errorCode , ErrorCode )
#define ErrorCodeIgnore( ErrorCode, IgnoredCode )          (oC_AssignErrorCode(&errorCode , ErrorCode ) || errorCode == IgnoredCode)
#define oC_BeginProcedure                                  do
#define oC_EndProcedure                                    while(0);
#define oC_ExitProcedureIfError(Step)                      if(oC_AssignErrorCode(&errorCode,Step) == false) { break; }
#define oC_ExitProcedureIfFalse(Step)                      if(oC_AssignErrorCode(&errorCode,Step) == false) { break; }

#define oC_Procedure_Begin                              do
#define oC_Procedure_End                                while(0);
#define oC_Procedure_ExitIfError(ErrorCode)             if( oC_AssignErrorCode(&errorCode,ErrorCode) == false ) { break; }
#define oC_Procedure_ExitIfFalse(Condition,ErrorCode)   if( oC_AssignErrorCodeIfFalse(&errorCode,Condition,ErrorCode) == false ) { break; }

#define oC_SaveError(String,ErrorCode)                  oC_SaveErrorFunction(String,ErrorCode,oC_FUNCTION,oC_LINE)
#define oC_SaveIfErrorOccur(String,ErrorCode)           oC_SaveIfErrorOccurFunction(String,ErrorCode,oC_FUNCTION,oC_LINE)
#define oC_SaveIfFalse(String,Condition,ErrorCode)      oC_SaveIfFalseFunction(String,Condition,ErrorCode,oC_FUNCTION,oC_LINE)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


#endif /* SYSTEM_GEN_OC_ERRORS_H_ */
