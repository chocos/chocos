/** ****************************************************************************************************************************************
 *
 * @brief      Handles configuration of the Dynamic
 *
 * @file       oc_dynamic_config.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#ifndef Dynamic_LIBRARIES_INC_OC_Dynamic_CFG_H_
#define Dynamic_LIBRARIES_INC_OC_Dynamic_CFG_H_

#include <oc_stdtypes.h>
#include <oc_1word.h>
#include <oc_errors.h>
#include <oc_time.h>
#include <oc_memory.h>
#include <oc_cfg.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * List with definitions of configuration modules. To add new use:
 *      ADD( MODULE_NAME, FRIENDLY_STRING )\
 */
//==========================================================================================================================================
#define oC_CONFIGURATIONS_MODULES_LIST(ADD,DONT_ADD)    \
    ADD( Tcp       , "Tcp" )\
    ADD( ExcHan    , "Exception Handler" )\
    ADD( Telnet    , "Telnet" )\
    ADD( syssprint , "System State Printer" )\
    ADD( ictrl     , "Input Control" )\
    ADD( ictrlman  , "Input Control Manager" )\
    ADD( diskman   , "Disk Manager" )\
    ADD( sdmmc     , "SDMMC card driver")

//==========================================================================================================================================
/**
 * List with configuration variables definitions. To add new use:
 *
 *      ADD( MODULE_NAME, TYPE , VARIABLE_NAME, DEFAULT_VALUE, FRIENDLY_NAME, BRIEF  )\
 *
 */
//==========================================================================================================================================
#define oC_CONFIGURATIONS_LIST(ADD,DONT_ADD)        \
    ADD( Tcp      , oC_MemorySize_t   , StackSize                 , B(1500)   , "Stack Size"                  , "Default size of the stack for TCP server threads "                       )\
    ADD( Tcp      , uint16_t          , WindowSize                , B(1500)   , "Window Size"                 , "Default size of the window for new TCP connections"                      )\
    ADD( Tcp      , uint8_t           , WindowScale               , 0         , "Window Scale"                , "Default scale of the window for new TCP connections"                     )\
    ADD( Tcp      , oC_Time_t         , ConfirmationTimeout       , ms(100)   , "Confirmation Timeout"        , "Maximum time for the confirmation before retransmission of a packet"     )\
    ADD( Tcp      , oC_Time_t         , WaitForConnectionTimeout  , day(1)    , "WfC Timeout"                 , "Maximum time to wait for new connection"                                 )\
    ADD( Tcp      , oC_Time_t         , DeleteConnectionTimeout   , day(1)    , "Delete Connection Timeout"   , "Maximum time for deleting connections"                                   )\
    ADD( Tcp      , oC_Time_t         , DecisionTimeout           ,  s(1)     , "Decision Timeout"            , "Maximum time for making decision about accepting connection"             )\
    ADD( Tcp      , oC_Time_t         , AcceptConnectionTimeout   ,  s(1)     , "Accept Connection Timeout"   , "Maximum time for accepting connections"                                  )\
    ADD( Tcp      , oC_Time_t         , RejectConnectionTimeout   , ms(500)   , "Reject Connection Timeout"   , "Maximum time for rejecting connections"                                  )\
    ADD( Tcp      , oC_Time_t         , ExpirationTimeout         , min(10)   , "Expiration Timeout"          , "Maximum time without receiving packets"                                  )\
    ADD( Tcp      , oC_Time_t         , SendingAcknowledgeTimeout , ms(0)     , "Sending ACK Timeout"         , "Maximum time to wait for sending ACK message"                            )\
    ADD( Tcp      , oC_Time_t         , ReadSegmentTimeout        , ms(500)   , "Read Segment Timeout"        , "Maximum time to wait for an app to read a received segment"              )\
    ADD( Tcp      , oC_Time_t         , SaveSegmentTimeout        , ms(500)   , "Save Segment Timeout"        , "Maximum time to wait for an app to save a received segment"              )\
    ADD( Tcp      , oC_Time_t         , ReceiveTimeout            , ms(500)   , "Receive Timeout"             , "Maximum time to wait for a packet before checking expiration timeout"    )\
    ADD( Tcp      , oC_Time_t         , WaitForFreeSlotTimeout    , ms(500)   , "Free Slot Timeout"           , "Maximum time to wait for a free slot in a TCP server"                    )\
    ADD( Tcp      , uint32_t          , InitialSequenceNumber     ,      0    , "ISN"                         , "Initial Sequence Number for new connections"                             )\
    ADD( Tcp      , oC_MemorySize_t   , PacketSize                , B(1024)   , "Packet Size"                 , "Size of a packet used for sending data"                                  )\
    ADD( Tcp      , oC_Time_t         , TurnTimeout               , s(3)      , "Turn Timeout"                , "Maximum time for turning on or turning off the module"    )\
    ADD( Tcp      , oC_Time_t         , ResendSegmentTime         , ms(100)   , "Segment Resend Time"         , "Time to wait before next try of resending segment"    )\
    ADD( ExcHan   , oC_Time_t         , MemoryEventsLoggingPeriod , min(1)    , "Memory events logging period", "Minimum time to elapse before logging again an event"                    )\
    ADD( ExcHan   , oC_MemorySize_t   , StackSize                 , B(1500)   , "Exception Thread Stack Size" , "Size of the stack for handling exceptions"                    )\
    ADD( Telnet   , uint32_t          , MaxConnections            , 5         , "Max Connections"             , "Maximum number of connections over Telnet"                               )\
    ADD( Telnet   , oC_Time_t         , StartServerTimeout        , s(3)      , "Start Server Timeout"        , "Maximum time for starting the Telnet server"                             )\
    ADD( Telnet   , oC_Time_t         , StopServerTimeout         , s(3)      , "Stop Server Timeout"         , "Maximum time for stopping the Telnet server"                             )\
    ADD( Telnet   , oC_Time_t         , DisconnectTimeout         , ms(500)   , "Disconnect Timeout"          , "Maximum time for disconnecting telnet connection"                        )\
    ADD( Telnet   , oC_Time_t         , NaglePeriod               , ms(50)    , "Nagle Period"                , "Period of buffering according to the Nagle's Algorithm"                  )\
    ADD( Telnet   , oC_Time_t         , NagleMaximumInactiveTime  , ms(500)   , "Max Nagle Inactive Time"     , "Maximum time to wait when Nagle's thread is inactive"                    )\
    ADD( Telnet   , oC_Time_t         , NagleSendingTimeout       , ms(500)   , "Nagle Sending Timeout"       , "Maximum time for TCP transmission"                                       )\
    ADD( Telnet   , oC_MemorySize_t   , NagleBufferSize           , B(1024)   , "Nagle Buffer Size"           , "Size of the buffer allocated for the Nagle's Algorithm"                  )\
    ADD( Telnet   , oC_MemorySize_t   , NagleThreadStackSize      , B(2048)   , "Nagle Thread Stack Size"     , "Size of the stack of the Nagle's Algorithm thread"                       )\
    ADD( Telnet   , uint16_t          , TcpPort                   , 20100     , "TCP Port"                    , "Telnet port the server should listen on"                       )\
    ADD( syssprint, oC_Time_t         , SleepTime                 , s(1)      , "Sleep time"                  , "Time to sleep each time after printing of the system state"              )\
    ADD( syssprint, oC_Time_t         , ClockRefreshTime          , s(1)      , "Clock Refresh Time"          , "Time that has to elapse before the clock will be refreshed"              )\
    ADD( syssprint, oC_Time_t         , CpuLoadRefreshTime        , s(3)      , "CPU Load Refresh Time"       , "Time that has to elapse before the CPU load will be refreshed"           )\
    ADD( syssprint, oC_Time_t         , NetInfoRefreshTime        , s(30)     , "NetInfo Refresh Time"        , "Time that has to elapse before the network information will be refreshed")\
    ADD( syssprint, oC_Time_t         , VersionRefreshTime        , min(10)   , "NetInfo Refresh Time"        , "Time that has to elapse before the version will be refreshed"            )\
    ADD( syssprint, oC_Time_t         , GraphBorderRefreshTime    , min(5)    , "Graph Border Refresh Time"   , "Time that has to elapse before the graph border will be refreshed"       )\
    ADD( syssprint, oC_Time_t         , GraphRefreshTime          , s(15)     , "Graph Refresh Time"          , "Time that has to elapse before the graph will be refreshed"              )\
    ADD( syssprint, oC_Time_t         , RamInfoRefreshTime        , min(1)    , "RamInfo Refresh Time"        , "Time that has to elapse before the RamInfo will be refreshed"            )\
    ADD( syssprint, oC_Time_t         , NewsRefreshTime           , s(30)     , "News Refresh Time"           , "Time that has to elapse before the news will be refreshed"               )\
    ADD( syssprint, bool              , ClockEnabled              , true      , "Clock Enabled"               , "Set it to true, if clock should be enabled"                              )\
    ADD( syssprint, bool              , CpuLoadEnabled            , true      , "CPU Load Enabled"            , "Set it to true, if CPU load should be enabled"                           )\
    ADD( syssprint, bool              , NetInfoEnabled            , true      , "NetInfo Enabled"             , "Set it to true, if NetInfo should be enabled"                            )\
    ADD( syssprint, bool              , VersionEnabled            , true      , "Version Enabled"             , "Set it to true, if Version should be enabled"                            )\
    ADD( syssprint, bool              , GraphEnabled              , false     , "Graph Enabled"               , "Set it to true, if Graph should be enabled"                              )\
    ADD( syssprint, bool              , RamInfoEnabled            , false     , "RamInfo Enabled"             , "Set it to true, if RamInfo should be enabled"                            )\
    ADD( syssprint, bool              , NewsEnabled               , true      , "News Enabled"                , "Set it to true, if News should be enabled"                               )\
    ADD( ictrl    , oC_Time_t         , GettingSamplesTimeout     , s(1)      , "Getting Samples Timeout"     , "Maximum time of getting samples for gestures recognition"                )\
    ADD( ictrl    , oC_Time_t         , GettingSamplesPeriod      , ms(80)    , "Getting Samples Period"      , "Period of getting samples for gestures recognition"                      )\
    ADD( ictrl    , oC_Time_t         , MaxFingerReleaseTime      , ms(80)    , "Max Finger Release Time"     , "Maximum time of finger release inside gesture recognition"               )\
    ADD( ictrl    , oC_Time_t         , MinFingerReleaseTime      , ms(50)    , "Min Finger Release Time"     , "Minimum time of finger release inside gesture recognition"               )\
    ADD( ictrl    , bool              , ForceSoftwareGestDetection, false     , "Force Software G. Detection" , "Forces software gestures detection"                                      )\
    ADD( ictrlman , oC_Time_t         , WaitingForEventTimeout    , min(10)   , "Waiting for Event Timeout"   , "Maximum time to wait for event"                                          )\
    ADD( diskman  , oC_MemorySize_t   , WaitForDiskStackSize      , kB(1)     , "Size of stack for disk wait" , "Size of a stack for threads responsible for waiting for disks"           )\
    ADD( sdmmc    , oC_Time_t         , DiscEjectSleepTime        , s(5)      , "Waiting for disc eject time" , "Time which driver will wait before check if disc is ejected"             )\
    ADD( sdmmc    , oC_Time_t         , TakeMutexTimeout          , s(1)      , "Waiting for Mutex time"      , "Time which driver will wait for Mutex to take"                           )\

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief creates name of the value with the given module name
 */
//==========================================================================================================================================
#define oC_DynamicConfig_Module_( MODULE_NAME )           oC_1WORD_FROM_2( oC_DynamicConfig_Module_, MODULE_NAME )

//==========================================================================================================================================
/**
 * @brief stores configuration module ID
 */
//==========================================================================================================================================
typedef enum
{
#define ADD( MODULE_NAME, FRIENDLY_STRING )     oC_DynamicConfig_Module_( MODULE_NAME ) ,
#define DONT_ADD(...)

    oC_CONFIGURATIONS_MODULES_LIST(ADD,DONT_ADD)

#undef ADD
#undef DONT_ADD
} oC_DynamicConfig_Module_t;

//==========================================================================================================================================
/**
 * @brief stores ID of the configuration variable
 */
//==========================================================================================================================================
typedef enum
{

#define ADD( MODULE_NAME, TYPE , VARIABLE_NAME, DEFAULT_VALUE, FRIENDLY_NAME, BRIEF  )              \
                oC_1WORD_FROM_4( oC_DynamicConfig_VariableId_, MODULE_NAME , _ , VARIABLE_NAME ) ,
#define DONT_ADD(...)

    oC_CONFIGURATIONS_LIST(ADD,DONT_ADD)

    oC_DynamicConfig_VariableId_NumberOfElements
#undef ADD
#undef DONT_ADD

} oC_DynamicConfig_VariableId_t;

//==========================================================================================================================================
/**
 * @brief stores configuration data
 *
 * The type is for storing data about a configuration variable.
 */
//==========================================================================================================================================
typedef struct
{
    oC_DynamicConfig_Module_t           Module;             //!< Module associated with the variable
    const char *                        TypeName;           //!< Name of the type
    oC_MemorySize_t                     TypeSize;           //!< Size of the type
    void *                              ValueReference;     //!< Pointer to the variable that stores the configuration
    const char *                        VariableName;       //!< Name of the variable that stores the configuration
    const char *                        FriendlyName;       //!< Friendly name of the variable
    const char *                        Brief;              //!< Quick description of the variable
} oC_DynamicConfig_VariableDetails_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

#define DYNAMIC_CONFIG_MAKE_VARIABLE_NAME(MODULE_NAME,VARIABLE_NAME)           oC_1WORD_FROM_4(oC_DynamicConfig_, MODULE_NAME, _ , VARIABLE_NAME)

//==========================================================================================================================================
/*                                  Creating variables to store values of configurations                                                  */
//==========================================================================================================================================
#define ADD( MODULE_NAME, TYPE , VARIABLE_NAME, DEFAULT_VALUE, FRIENDLY_NAME, BRIEF  )              \
                    extern TYPE DYNAMIC_CONFIG_MAKE_VARIABLE_NAME(MODULE_NAME, VARIABLE_NAME);
#define DONT_ADD(...)

oC_CONFIGURATIONS_LIST(ADD,DONT_ADD);

#undef ADD
#undef DONT_ADD


//==========================================================================================================================================
/*                                  Array with details of configuration variables                                                         */
//==========================================================================================================================================
extern oC_DynamicConfig_VariableDetails_t oC_DynamicConfig_VariablesDetails[oC_DynamicConfig_VariableId_NumberOfElements];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

#define oC_DynamicConfig_SetValue( MODULE_NAME, VARIABLE_NAME, VALUE )      DYNAMIC_CONFIG_MAKE_VARIABLE_NAME( MODULE_NAME, VARIABLE_NAME )     = VALUE
#define oC_DynamicConfig_GetValue( MODULE_NAME, VARIABLE_NAME )             DYNAMIC_CONFIG_MAKE_VARIABLE_NAME( MODULE_NAME, VARIABLE_NAME )

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________


#endif /* Dynamic_LIBRARIES_INC_OC_Dynamic_CFG_H_ */
