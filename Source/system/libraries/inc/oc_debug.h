/** ****************************************************************************************************************************************
 *
 * @file          oc_debug.h
 *
 * @brief       The file with functions for the debug operation
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 22:15:56) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_OC_DEBUG_H_
#define SYSTEM_CORE_OC_DEBUG_H_

#include <oc_stdtypes.h>
#include <oc_debug_cfg.h>
#include <stdarg.h>
#include <oc_posix_ptrs.h>

typedef enum
{
    oC_LogType_Error        = (1<<0),
    oC_LogType_Warning      = (1<<1),
    oC_LogType_Track        = (1<<2),
    oC_LogType_Info         = (1<<3),
    oC_LogType_GoodNews     = (1<<4),
    oC_LogType_All          = 0xFFFF,
    oC_LogType_Default      = oC_LogType_All,
} oC_LogType_t;

#if !defined(CFG_ENABLE_DEBUG_PRINT)
#   error CFG_ENABLE_DEBUG_PRINT is not defined in oc_debug_cfg.h !
#elif CFG_ENABLE_DEBUG_PRINT == ON
#   ifdef oC_CORE_SPACE
#       define  kdebuglog(LogType , ... )        _kdebuglog( LogType, __FUNCTION__,  __VA_ARGS__ )
#   else
#       define  kdebuglog(LogType , ... )        if(__kdebuglog != NULL) __kdebuglog( LogType, __FUNCTION__,  __VA_ARGS__ )
#   endif
#else
#   ifdef oC_CORE_SPACE
#       define  kdebuglog(LogType , ... )
#   endif
#endif

#ifdef oC_CORE_SPACE


#endif

#endif /* SYSTEM_CORE_OC_DEBUG_H_ */

EXTERNAL_PREFIX void EXTERNAL_FUNC( enkdebuglog         , ( oC_LogType_t LogType )                                                                      );
EXTERNAL_PREFIX void EXTERNAL_FUNC( lockkdebuglog       , ( void )                                                                                      );
EXTERNAL_PREFIX void EXTERNAL_FUNC( unlockkdebuglog     , ( void )                                                                                      );
EXTERNAL_PREFIX void EXTERNAL_FUNC( _kdebuglog          , ( oC_LogType_t LogType , const char * Function, char * Format , ... )                         );
EXTERNAL_PREFIX void EXTERNAL_FUNC( _vkdebuglog         , ( oC_LogType_t LogType , const char * Function, const char * Format , va_list Arguments )     );
EXTERNAL_PREFIX bool EXTERNAL_FUNC( readlastkdebuglog   , ( char * outString , oC_UInt_t Size )                                                         );
EXTERNAL_PREFIX bool EXTERNAL_FUNC( readoldestkdebuglog , ( char * outString, oC_UInt_t Size )                                                          );
EXTERNAL_PREFIX void EXTERNAL_FUNC( savekdebuglog       , ( char * Log )                                                                                );
