/** ****************************************************************************************************************************************
 *
 * @file       oc_frequency.h
 *
 * @brief      The file with frequency definitions
 *
 * @author     Patryk Kubiak - (Created on: 24 kwi 2015 20:43:32) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 ******************************************************************************************************************************************/
#ifndef SYSTEM_LIBRARIES_FREQUENCY_OC_FREQUENCY_H_
#define SYSTEM_LIBRARIES_FREQUENCY_OC_FREQUENCY_H_

/**
 * @defgroup Frequency
 * @ingroup LibrariesSpace
 * @brief Library with types and functions that helps to manage frequency calculations
 * @{
 * @}
 */

#include <stdint.h>
#include <oc_time.h>
#include <oc_baudrate.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Frequency
//! @{

#define oC_GHz(X)           ( (oC_Frequency_t)((X)*1000000000UL) )
#define oC_MHz(X)           ( (oC_Frequency_t)((X)*1000000UL) )
#define oC_kHz(X)           ( (oC_Frequency_t)((X)*1000UL) )
#define oC_Hz(X)            ( (oC_Frequency_t)((X)*1UL) )

#define oC_Frequency_ToTime(Frequency)      ((oC_Time_t) (1/(Frequency)))

#define oC_Frequency_FromTime(Time)         ( (oC_Frequency_t) (1/(Time)) )

#define oC_Frequency_FromBaudRate(BaudRate)         ( (oC_Frequency_t)(BaudRate) )
#define oC_Frequency_ToBaudRate(Frequency)          ( (oC_BaudRate_t)(Frequency) )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Frequency
//! @{

//==========================================================================================================================================
/**
 * @brief type to store frequency
 *
 * The type is for storing frequency
 */
//==========================================================================================================================================
typedef double oC_Frequency_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @addtogroup Frequency
//! @{



#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @}


#endif /* SYSTEM_LIBRARIES_FREQUENCY_OC_FREQUENCY_H_ */
