/** ****************************************************************************************************************************************
 *
 * @file       oc_stdtypes.h
 *
 * @brief      The file contains standard types for the system
 *
 * @author     Patryk Kubiak - (Created on: 8 wrz 2015 21:30:45) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_STDTYPES_H_
#define SYSTEM_LIBRARIES_INC_OC_STDTYPES_H_

#include <stdint.h>
#include "stdbool.h"
#include <oc_null.h>

//==========================================================================================================================================
/**
 * @brief type for 8 bit unsigned integer
 *
 * The type stores unsigned integer that stores 8 bits values.
 */
//==========================================================================================================================================
typedef uint8_t oC_uint8_t;
//==========================================================================================================================================
/**
 *  @brief type for 16 bit unsigned integer
 *
 * The type stores unsigned integer that stores 16 bits values.
 */
//==========================================================================================================================================
typedef uint16_t    oC_uint16_t;
//==========================================================================================================================================
/**
 *  @brief type for 32 bit unsigned integer
 *
 * The type stores unsigned integer that stores 32 bits values.
 */
//==========================================================================================================================================
typedef uint32_t    oC_uint32_t;
//==========================================================================================================================================
/**
 *  @brief type for 64 bit unsigned integer
 *
 * The type stores unsigned integer that stores 64 bits values.
 */
//==========================================================================================================================================
typedef uint64_t    oC_uint64_t;
//==========================================================================================================================================
/**
 * @brief type for 8 bit signed integer
 *
 * The type stores signed integer that stores 8 bits values.
 */
//==========================================================================================================================================
typedef int8_t      oC_int8_t;
//==========================================================================================================================================
/**
 * @brief type for 16 bit signed integer
 *
 * The type stores signed integer that stores 16 bits values.
 */
//==========================================================================================================================================
typedef int16_t     oC_int16_t;
//==========================================================================================================================================
/**
 * @brief type for 32 bit signed integer
 *
 * The type stores signed integer that stores 32 bits values.
 */
//==========================================================================================================================================
typedef int32_t     oC_int32_t;
//==========================================================================================================================================
/**
 * @brief type for 64 bit signed integer
 *
 * The type stores signed integer that stores 64 bits values.
 */
//==========================================================================================================================================
typedef int64_t     oC_int64_t;
//==========================================================================================================================================
/**
 * @brief machine memory size unsigned integer
 *
 * Unsigned integer that is in size of the memory architecture (in 32bits machine it will be 32bits length).
 */
//==========================================================================================================================================
#if MACHINE_WORD_SIZE==4
typedef oC_uint32_t oC_UInt_t;
#elif MACHINE_WORD_SIZE==2
typedef oC_uint16_t oC_UInt_t;
#elif MACHINE_WORD_SIZE==1
typedef oC_uint8_t oC_UInt_t;
#else
#   error Unsupported machine word size architecture
#endif
//==========================================================================================================================================
/**
 * @brief machine memory size signed integer
 *
 * Signed integer that is in size of the memory architecture (in 32bits machine it will be 32bits length).
 */
//==========================================================================================================================================
#if MACHINE_WORD_SIZE==4
typedef oC_int32_t  oC_Int_t;
#elif MACHINE_WORD_SIZE==2
typedef oC_int16_t oC_Int_t;
#elif MACHINE_WORD_SIZE==1
typedef oC_int8_t oC_Int_t;
#else
#   error Unsupported machine word size architecture
#endif
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for uint8_t type
 */
//==========================================================================================================================================
#define oC_uint8_MAX        (0xFFU)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for uint8_t type
 */
//==========================================================================================================================================
#define oC_uint8_MIN        (0)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for uint16_t type
 */
//==========================================================================================================================================
#define oC_uint16_MAX       (0xFFFFU)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for uint16_t type
 */
//==========================================================================================================================================
#define oC_uint16_MIN       (0)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for uint32_t type
 */
//==========================================================================================================================================
#define oC_uint32_MAX       (0xFFFFFFFFUL)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for uint32_t type
 */
//==========================================================================================================================================
#define oC_uint32_MIN       (0)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for uint64_t type
 */
//==========================================================================================================================================
#define oC_uint64_MAX       (0xFFFFFFFFFFFFFFFFULL)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for uint64_t type
 */
//==========================================================================================================================================
#define oC_uint64_MIN       (0)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for int8_t type
 */
//==========================================================================================================================================
#define oC_int8_MAX         (127)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for int8_t type
 */
//==========================================================================================================================================
#define oC_int8_MIN         (-128)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for int16_t type
 */
//==========================================================================================================================================
#define oC_int16_MAX        (31767)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for int16_t type
 */
//==========================================================================================================================================
#define oC_int16_MIN        (-32768)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for int32_t type
 */
//==========================================================================================================================================
#define oC_int32_MAX        (2147483647L)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for int32_t type
 */
//==========================================================================================================================================
#define oC_int32_MIN        (-2147483648L)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum value for int64_t type
 */
//==========================================================================================================================================
#define oC_int64_MAX        (9223372036854775807LL)
//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief minimum value for int64_t type
 */
//==========================================================================================================================================
#define oC_int64_MIN        (-9223372036854775808LL)

//==========================================================================================================================================
/**
 * @brief stores registers power state
 *
 * The type stores state of the power. This type is used by the Power Manager to set or return current state of the power.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Power_Off ,          //!< Something is powered off
    oC_Power_On  ,          //!< Something is powered on
    oC_Power_NotHandled ,   //!< Something power is not handled
    oC_Power_Invalid        //!< Something index is not correct
} oC_Power_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef enum
{
    oC_IoFlags_WaitForAllElements = (1<<0) ,
    oC_IoFlags_ReadOneLine        = (1<<1) ,
    oC_IoFlags_EchoWhenRead       = (1<<2) ,
    oC_IoFlags_EchoAsPassword     = (1<<3) ,
    oC_IoFlags_SleepWhileWaiting  = (1<<4) ,
    oC_IoFlags_60sTimeout         = (1<<5) ,
    oC_IoFlags_10sTimeout         = (1<<6) ,
    oC_IoFlags_1sTimeout          = (1<<7) ,
    oC_IoFlags_500msTimeout       = (1<<8) ,
    oC_IoFlags_0sTimeout          = (1<<9) ,
    oC_IoFlags_NoTimeout          = (1<<10) ,
    oC_IoFlags_WaitForSomeElements= (1<<11) ,
    oC_IoFlags_ClearRxBeforeRead  = (1<<12) ,
    oC_IoFlags_WriteToStdError    = (1<<13) ,
    oC_IoFlags_Default            = oC_IoFlags_SleepWhileWaiting | oC_IoFlags_WaitForAllElements | oC_IoFlags_ReadOneLine | oC_IoFlags_EchoWhenRead | oC_IoFlags_NoTimeout | oC_IoFlags_ClearRxBeforeRead,
} oC_IoFlags_t;

#define oC_IoFlags_GetTimeout(IoFlags)      ( ((IoFlags) & oC_IoFlags_60sTimeout)   ? oC_s(60) : \
                                              ((IoFlags) & oC_IoFlags_10sTimeout)   ? oC_s(10) : \
                                              ((IoFlags) & oC_IoFlags_1sTimeout)    ? oC_s(1)  : \
                                              ((IoFlags) & oC_IoFlags_NoTimeout)    ? oC_hour(1) : \
                                              ((IoFlags) & oC_IoFlags_500msTimeout) ? oC_ms(500) : 0 \
                                            )

#define oC_CastType(Type,Value)             ( (Type) (Value) )

#define oC_InitByte(B7,B6,B5,B4,B3,B2,B1,B0)                ( B0 << 0 ) | \
                                                            ( B1 << 1 ) | \
                                                            ( B2 << 2 ) | \
                                                            ( B3 << 3 ) | \
                                                            ( B4 << 4 ) | \
                                                            ( B5 << 5 ) | \
                                                            ( B6 << 6 ) | \
                                                            ( B7 << 7 )

#endif /* SYSTEM_LIBRARIES_INC_OC_STDTYPES_H_ */
