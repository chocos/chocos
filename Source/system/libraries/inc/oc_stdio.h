/** ****************************************************************************************************************************************
 *
 * @file       oc_stdio.h
 *
 * @brief      The file with standard input/output operations
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 20:31:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup stdio Stdio
 * @ingroup LibrariesSpace
 * @brief The module for handling standard input/output operations
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_POSIX_OC_STDIO_H_
#define SYSTEM_CORE_INC_POSIX_OC_STDIO_H_

#include <oc_errors.h>
#include <oc_vt100.h>
#include <oc_stdtypes.h>
#include <oc_array.h>
#include <stdbool.h>
#include <stdarg.h>
#include <stddef.h>
#include <oc_compiler.h>
#include <string.h>
#include <oc_posix_ptrs.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#undef feof
#undef putc
#undef putchar
#undef getchar
#undef ferror

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_PrintErrorMessage( Message , ErrorCode )             printf(oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_WHITE Message oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_FG_RED "%R\n\r" oC_VT100_RESET_ALL_ATTRIBUTES , ErrorCode)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_PrintWarningMessage( Message )                       printf(oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_YELLOW Message oC_VT100_RESET_ALL_ATTRIBUTES "\n\r")

//==========================================================================================================================================
/**
 * @brief prints a formatted string to the standard output
 * 
 * @param outString            The destination string where the formatted string will be stored
 * @param Size                 The size of the string
 * @param Format               The format string
 * 
 * @return The function returns the number of characters printed or negative value if error occured
 * 
 * @note
 * The function can be used only for static buffers. If you want to use it for dynamic buffers, please use #sprintf_s() function.
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters printed,
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
#define sprintf(outString,...)                                  sprintf_s(outString , oC_ARRAY_SIZE(outString) , __VA_ARGS__) oC_FUNCTION_ONLY_FOR_STATIC_BUFFER(outString,sprintf,sprintf_s)

//==========================================================================================================================================
/**
 * @brief reads a string from the standard input
 * 
 * @param outString            The destination string where the read string will be stored
 * 
 * @return The function returns the number of characters read or negative value if error occured
 * 
 * @note
 * The function can be used only for static buffers. If you want to use it for dynamic buffers, please use #gets_s() function.
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters read,
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
#define gets(outString)                                         fgets(outString , oC_ARRAY_SIZE(outString) , stdin) oC_FUNCTION_ONLY_FOR_STATIC_BUFFER(outString,gets,fgets)

#define stdin   ((void*)0)
#define stdout  ((void*)1)
#define stderr  ((void*)2)

#define EOF     (-1)

#define SEEK_SET	0	/* Seek from beginning of file.  */
#define SEEK_CUR	1	/* Seek from current position.  */
#define SEEK_END	2	/* Seek from end of file.  */

#define PrintIfErrorOccur( Message, ErrorCode )                 { oC_ErrorCode_t __errorCode =  ErrorCode; \
                                                                  if(oC_ErrorOccur(__errorCode)) { printf(Message ": " oC_VT100_FG_RED "%R\n" oC_VT100_FG_WHITE, __errorCode); } }

#define PrintAssert( ErrorCode )                PrintIfErrorOccur( #ErrorCode, ErrorCode )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup stdio
//! @{

typedef void FILE;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup stdio
//! @{

// Definition of functions from stdio library
EXTERNAL_PREFIX int      EXTERNAL_FUNC( vprintf              ,   ( const char * Format , va_list ArgumentList )                          );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( printf               ,   ( const char * Format , ... )                                           );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( puts                 ,   ( const char * String )                                                 );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( putc                 ,   ( int C, FILE *stream)                                                  );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( putchar              ,   ( int C )                                                               );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( getchar              ,   ( void )                                                                );

EXTERNAL_PREFIX int      EXTERNAL_FUNC( sprintf_s            ,   ( char * outString , oC_UInt_t Size , const char * Format, ... )        );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( snprintf             ,   ( char * outString , oC_UInt_t Size , const char * Format, ... )        );

EXTERNAL_PREFIX int      EXTERNAL_FUNC( scanf                ,   ( const char * Format , ... )                                           );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( sscanf               ,   ( const char * String , const char * Format , ...)                      );

EXTERNAL_PREFIX FILE *   EXTERNAL_FUNC( fopen                ,   ( const char * FileName, const char * Mode )                            );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( fclose               ,   ( FILE * stream )                                                       );
EXTERNAL_PREFIX size_t   EXTERNAL_FUNC( fread                ,   ( void * Buffer, size_t ElementSize, size_t Count, FILE * File )        );
EXTERNAL_PREFIX size_t   EXTERNAL_FUNC( fwrite               ,   ( const void * Buffer, size_t ElementSize, size_t Count, FILE * File )  );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( vfprintf             ,   ( FILE * File, const char * Format , va_list ArgumentList )             );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( fprintf              ,   ( FILE * File, const char * Format, ... )                               );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( feof                 ,   ( FILE * File )                                                         );
EXTERNAL_PREFIX uint32_t EXTERNAL_FUNC( fsize                ,   ( FILE * File )                                                         );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( fgetc                ,   ( FILE * File )                                                         );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( fputc                ,   ( int C, FILE * File )                                                  );
EXTERNAL_PREFIX char *   EXTERNAL_FUNC( fgets                ,   ( char * Buffer, int Size , FILE * File )                               );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( fflush               ,   ( FILE * File )                                                         );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( fseek                ,   ( FILE * File, long Offset, int Origin )                                );
EXTERNAL_PREFIX long     EXTERNAL_FUNC( ftell                ,   ( FILE * File )                                                         );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( remove               ,   ( const char * FileName )                                               );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( rename               ,   ( const char * OldFileName, const char * NewFileName )                  );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( ferror               ,   ( FILE * File )                                                         );

// ChocoOS specific functions with stdio naming convention
EXTERNAL_PREFIX bool     EXTERNAL_FUNC( file_exist           ,   ( const char * File )                                                   );

// ChocoOS specific functions 
EXTERNAL_PREFIX bool     EXTERNAL_FUNC( oC_VerifyStdioResult ,   ( int Result )                                                          );
EXTERNAL_PREFIX char *   EXTERNAL_FUNC( oC_GetArgumentAfter  ,   ( int Argc, char ** Argv , const char * Argument )                      );
EXTERNAL_PREFIX bool     EXTERNAL_FUNC( oC_ArgumentOccur     ,   ( int Argc , char ** Argv , const char * Argument )                     );
EXTERNAL_PREFIX int      EXTERNAL_FUNC( oC_GetArgumentNumber ,   ( int Argc , char ** Argv , const char * Argument )                     );
EXTERNAL_PREFIX bool     EXTERNAL_FUNC( oC_IsMemSetTo        ,   ( void * Buffer , oC_UInt_t Size , uint8_t Value )                      );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* SYSTEM_CORE_INC_POSIX_OC_STDIO_H_ */
