/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the module library
 *
 * @file       oc_module.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Module Module - The module library
 * @ingroup LibrariesSpace
 * @brief The library for managing modules
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_MODULE_H_
#define SYSTEM_LIBRARIES_INC_OC_MODULE_H_

#include <stdbool.h>
#include <oc_stdtypes.h>
#include <oc_bits.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

#define MAX_REQUIRED_MODULES        5

typedef enum
{
    oC_Module_None = 0,
    oC_Module_GPIO ,
    oC_Module_CLOCK_LLD ,
    oC_Module_MEM_LLD ,
    oC_Module_SYS_LLD ,
    oC_Module_GPIO_LLD ,
    oC_Module_UART_LLD ,
    oC_Module_LCDTFT_LLD ,
    oC_Module_LCDTFT ,
    oC_Module_FMC ,
    oC_Module_FMC_LLD ,
    oC_Module_ScreenMan ,
    oC_Module_ETH ,
    oC_Module_ETH_LLD ,
    oC_Module_NetifMan ,
    oC_Module_Udp ,
    oC_Module_ExcHan ,
    oC_Module_Icmp ,
    oC_Module_Tcp ,
    oC_Module_PortMan ,
    oC_Module_ServiceMan ,
    oC_Module_ProcessMan ,
    oC_Module_ThreadMan ,
    oC_Module_GTD ,
    oC_Module_I2C_LLD ,
    oC_Module_I2C ,
    oC_Module_FT5336 ,
    oC_Module_ICtrlMan ,
    oC_Module_SPI ,
    oC_Module_SPI_LLD ,
    oC_Module_SDMMC ,
    oC_Module_SDMMC_LLD ,
    oC_Module_SDMMC_LLD_Mode,
    oC_Module_SDMMC_SPI ,
    oC_Module_SDMMC_1BIT ,
    oC_Module_SDMMC_4BIT ,
    oC_Module_DriverMan ,
    oC_Module_DevFs ,
    oC_Module_StorageMan ,
    oC_Module_DiskMan ,
    oC_Module_NEUNET ,

    /* This should be always at the end of the list */
    oC_Module_NumberOfModules ,
    oC_Module_MaxRequiredModules  = MAX_REQUIRED_MODULES,
} oC_Module_t;

typedef oC_ErrorCode_t (*oC_Module_TurnFunction_t)( void );

typedef oC_Module_t oC_Module_RequiredArray_t[MAX_REQUIRED_MODULES];

typedef struct
{
    const char *                    Name;
    const char *                    LongName;
    oC_Module_t                     Module;
    oC_Module_TurnFunction_t        TurnOnFunction;
    oC_Module_TurnFunction_t        TurnOffFunction;
    oC_Module_RequiredArray_t       RequiredModules;
} oC_Module_Registration_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

extern uint32_t oC_Module_EnabledFlags[ oC_Module_NumberOfModules/32 + ((oC_Module_NumberOfModules%32) ? 1 : 0)];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the module is turned on
 *
 * The function checks if the module is turned on
 *
 * @param Module    Module to check
 *
 * @return true if module is turned on
 */
//==========================================================================================================================================
static inline bool oC_Module_IsTurnedOn( oC_Module_t Module )
{
    return (Module < oC_Module_NumberOfModules) && (oC_Bits_IsBitSetU32(oC_Module_EnabledFlags[Module/32],Module % 32));
}

//==========================================================================================================================================
/**
 * @brief verify if module is turned on
 *
 * The function for turn-off function for verification if module is not turned off already
 *
 * @param outErrorCode      Reference to the error code variable
 * @param Module            Index of the module
 *
 * @return true if module is turned on already
 */
//==========================================================================================================================================
static inline bool oC_Module_TurnOnVerification( oC_ErrorCode_t * outErrorCode , oC_Module_t Module )
{
    return oC_AssignErrorCodeIfFalse( outErrorCode , oC_Module_IsTurnedOn(Module) , oC_ErrorCode_ModuleNotStartedYet );
}

//==========================================================================================================================================
/**
 * @brief verify if module is turned off
 *
 * The function for verification if module is not turned on
 *
 * @param outErrorCode      Reference to the error code variable
 * @param Module            Index of the module
 *
 * @return true if module is turned off already
 */
//==========================================================================================================================================
static inline bool oC_Module_TurnOffVerification( oC_ErrorCode_t * outErrorCode , oC_Module_t Module )
{
    return oC_AssignErrorCodeIfFalse( outErrorCode , !oC_Module_IsTurnedOn(Module) , oC_ErrorCode_ModuleIsTurnedOn );
}

//==========================================================================================================================================
/**
 * @brief sets module as turned on
 *
 * The function turns on the module without checking if it is already enabled
 *
 * @param Module        index of the module to turn on
 *
 */
//==========================================================================================================================================
static inline void oC_Module_TurnOn( oC_Module_t Module )
{
    oC_Bits_SetBitU32(&oC_Module_EnabledFlags[Module/32],Module % 32);
}

//==========================================================================================================================================
/**
 * @brief sets module as turned off
 *
 * The function turns off the module without checking if it is already enabled
 *
 * @param Module        index of the module to turn off
 *
 */
//==========================================================================================================================================
static inline void oC_Module_TurnOff( oC_Module_t Module )
{
    oC_Bits_ClearBitU32(&oC_Module_EnabledFlags[Module/32],Module % 32);
}
#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#endif /* SYSTEM_LIBRARIES_INC_OC_MODULE_H_ */
