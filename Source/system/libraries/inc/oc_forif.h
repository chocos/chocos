/** ****************************************************************************************************************************************
 *
 * @brief      Definition of forif macro loop
 *
 * @file       oc_forif.h
 *
 * @author     Patryk Kubiak - (Created on: 11 kwi 2015 12:45:47) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ForIf ForIf - For loop with else condition
 * @ingroup LibrariesSpace
 * @brief The macro for loop with else condition
 * 
 ******************************************************************************************************************************************/

#ifndef SYSTEM_LIBRARIES_FORIF_OC_FORIF_H_
#define SYSTEM_LIBRARIES_FORIF_OC_FORIF_H_

/**
 * @brief for-loop with else condition
 *
 * @hideinitializer
 *
 * The macro is the loop with else condition. You can use it as standard 'for'.
 *
 * @param INITIALS      Operation, that should be performed before the loop. There is one
 *                      restriction, that in initials you cannot declare a variable.
 * @param CONDITION     The condition to check before each iteration
 * @param INCREMENTS    Increment variable operation (only for example)
 *
 * ~~~~~~~~~~~~{.c}
 * int i = 0;
 * int count = 10;
 *
 * scanf("%d" , &count);
 *
 * forif(i=0,i<count,i++)
 *     printf("i=%d\n", i);
 * else
 *     printf("count is 0\n");
 * ~~~~~~~~~~~~
 */
#define forif( INITIALS , CONDITION , INCREMENTS )   \
    if(CONDITION) for(INITIALS ; CONDITION ; INCREMENTS)

#endif /* SYSTEM_LIBRARIES_FORIF_OC_FORIF_H_ */
