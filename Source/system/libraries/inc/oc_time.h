/** ****************************************************************************************************************************************
 *
 * @brief      The library with time definitions
 *
 * @file       oc_time.h
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 17:56:56)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Time Time - The time library
 * @ingroup LibrariesSpace
 * @brief The library for managing time
 * 
 ******************************************************************************************************************************************/


#ifndef INC_OC_TIME_H_
#define INC_OC_TIME_H_

#include <stdint.h>
#include <oc_null.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define oC_ns(time)         (oC_us(time) / 1000 )
#define oC_us(time)         (oC_ms(time) / 1000 )
#define oC_ms(time)         (oC_s(time)  / 1000 )
#define oC_s(time)          ((oC_Time_t)   (time))
#define oC_min(time)        (oC_s(60)    * (time))
#define oC_hour(time)       (oC_min(60)  * (time))
#define oC_day(time)        (oC_hour(24) * (time))

#define oC_Time_ToNanoseconds(time)     (oC_Time_ToMicroseconds(time) * 1000)
#define oC_Time_ToMicroseconds(time)    (oC_Time_ToMiliseconds(time) * 1000)
#define oC_Time_ToMiliseconds(time)     (oC_Time_ToSeconds(time) * 1000)
#define oC_Time_ToSeconds(time)         (time)
#define oC_Time_ToMinutes(time)         (oC_Time_ToSeconds(time) / 60 )
#define oC_Time_ToHours(time)           (oC_Time_ToMinutes(time) / 60 )
#define oC_Time_ToDays(time)            (oC_Time_ToHours(time) / 24 )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Time
//! @{

typedef double oC_Time_t;
typedef double oC_Timestamp_t;
typedef struct
{
    uint16_t             Nanoseconds;
    uint16_t             Microseconds;
    uint16_t             Miliseconds;
    uint8_t              Seconds;
    uint8_t              Minutes;
    uint8_t              Hours;
    uint32_t             Days;
} oC_Time_Divided_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_SECTION_____________________________________________________________________________
//! @addtogroup Time
//! @{

//==========================================================================================================================================
/**
 * @brief divided time
 */
//==========================================================================================================================================
static inline void oC_Time_Divide( oC_Timestamp_t Timestamp , oC_Time_Divided_t * outTime )
{
    if(Timestamp >= 0 && outTime != NULL)
    {
        uint32_t ns   = ((uint32_t)oC_Time_ToNanoseconds(Timestamp)  ) % 1000;
        uint32_t us   = ((uint32_t)oC_Time_ToMicroseconds(Timestamp) ) % 1000;
        uint32_t ms   = ((uint32_t)oC_Time_ToMiliseconds(Timestamp)  ) % 1000;
        uint32_t s    = ((uint32_t)oC_Time_ToSeconds(Timestamp)      ) % 60;
        uint32_t min  = ((uint32_t)oC_Time_ToMinutes(Timestamp)      ) % 60;
        uint32_t h    = ((uint32_t)oC_Time_ToHours(Timestamp)        ) % 60;
        uint32_t days = (uint32_t)oC_Time_ToDays(Timestamp);

        outTime->Nanoseconds    = (uint16_t)(ns);
        outTime->Microseconds   = (uint16_t)(us);
        outTime->Miliseconds    = (uint16_t)(ms);
        outTime->Seconds        = (uint8_t)(s);
        outTime->Minutes        = (uint8_t)(min);
        outTime->Hours          = (uint8_t)(h);
        outTime->Days           = (uint32_t)(days);
    }
}

#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________
//! @}


#endif /* INC_OC_TIME_H_ */
