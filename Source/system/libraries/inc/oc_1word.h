/** ****************************************************************************************************************************************
 *
 * @file       oc_1word.h
 *
 * @brief      Contains macros for creating one word from more words in macros
 *
 * @author     Patryk Kubiak - (Created on: 12 04 2015 12:56:48)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup 1Word 1Word
 * @ingroup LibrariesSpace
 * @brief The module for creating one word from more words
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_1WORD_OC_1WORD_H_
#define SYSTEM_LIBRARIES_1WORD_OC_1WORD_H_

//==========================================================================================================================================
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
//==========================================================================================================================================
#define _oC_1WORD_FROM_2( A , B )                          A##B
//==========================================================================================================================================
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
//==========================================================================================================================================
#define _oC_1WORD_FROM_3( A , B , C)                       A##B##C
//==========================================================================================================================================
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
//==========================================================================================================================================
#define _oC_1WORD_FROM_4( A , B , C , D)                   A##B##C##D
//==========================================================================================================================================
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
//==========================================================================================================================================
#define _oC_1WORD_FROM_5( A , B , C , D , E)               A##B##C##D##E
//==========================================================================================================================================
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
//==========================================================================================================================================
#define _oC_1WORD_FROM_6( A , B , C , D , E , F)           A##B##C##D##E##F
//==========================================================================================================================================
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
//==========================================================================================================================================
#define _oC_1WORD_FROM_7( A , B , C , D , E , F , G)       A##B##C##D##E##F##G
//==========================================================================================================================================
/**
 * The macro is for some kind of compilers. It concatenates 2 or more parameters to receive one word. It is only for
 * internal usage. Do not use it out of this file!!
 */
//==========================================================================================================================================
#define _oC_1WORD_FROM_8( A , B , C , D , E , F , G , H)   A##B##C##D##E##F##G##H

//==========================================================================================================================================
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_1WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
//==========================================================================================================================================
#define oC_1WORD_FROM_2( A , B )                           _oC_1WORD_FROM_2( A , B )
//==========================================================================================================================================
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_1WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
//==========================================================================================================================================
#define oC_1WORD_FROM_3( A , B , C)                        _oC_1WORD_FROM_3( A , B , C)
//==========================================================================================================================================
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_1WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
//==========================================================================================================================================
#define oC_1WORD_FROM_4( A , B , C , D)                    _oC_1WORD_FROM_4( A , B , C , D)
//==========================================================================================================================================
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_1WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
//==========================================================================================================================================
#define oC_1WORD_FROM_5( A , B , C , D , E)                _oC_1WORD_FROM_5( A , B , C , D , E)
//==========================================================================================================================================
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_1WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
//==========================================================================================================================================
#define oC_1WORD_FROM_6( A , B , C , D , E, F)             _oC_1WORD_FROM_6( A , B , C , D , E , F)
//==========================================================================================================================================
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_1WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
//==========================================================================================================================================
#define oC_1WORD_FROM_7( A , B , C , D , E, F , G)         _oC_1WORD_FROM_7( A , B , C , D , E , F , G)
//==========================================================================================================================================
/**
 * Concatenates words to becomes one word. For example, if you have a channel type, which always contains a xX prefix,
 * you can use it to receive channel name:
 *     oC_1WORD_FROM_2( xX , ChannelName ) - it will return xXChannelName
 *
 * @param           All parameters will be concatenates to receive one word
 */
//==========================================================================================================================================
#define oC_1WORD_FROM_8( A , B , C , D , E, F , G , H)     _oC_1WORD_FROM_8( A , B , C , D , E , F , G , H)

#endif /* SYSTEM_LIBRARIES_1WORD_OC_1WORD_H_ */
