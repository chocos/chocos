/** ****************************************************************************************************************************************
 *
 * @file       oc_object.h
 *
 * @brief      The file with helper macros for managing objects
 *
 * @author     Patryk Kubiak - (Created on: 30 08 2015 08:31:16)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Object Object - The object library
 * @ingroup LibrariesSpace
 * @brief The library for managing objects
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_OBJECT_H_
#define SYSTEM_LIBRARIES_INC_OC_OBJECT_H_

#include <oc_1word.h>
#include <stdbool.h>
#include <stdint.h>
#include <oc_assert.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Object
//! @{

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of object registrations
 *
 * The macro is for storing list of objects. To add object to the list just use macro `ADD_OBJECT` in format:
 *
 *      ADD_OBJECT(ObjectName) \
 *
 * Example:
 * @code{.c}
     #define oC_OBJECTS_REGISTRATIONS_LIST(ADD_OBJECT) \
            ADD_OBJECT(Thread) \
            ADD_OBJECT(OtherObject) \
            ADD_OBJECT(MyObject) \
 * @endcode
 */
//==========================================================================================================================================
#define oC_OBJECTS_REGISTRATIONS_LIST(ADD_OBJECT) \
    ADD_OBJECT(Thread) \
    ADD_OBJECT(Mutex) \
    ADD_OBJECT(Semaphore) \
    ADD_OBJECT(Event) \
    ADD_OBJECT(Signal) \
    ADD_OBJECT(List) \
    ADD_OBJECT(TimerContext) \
    ADD_OBJECT(Process) \
    ADD_OBJECT(User) \
    ADD_OBJECT(Stream) \
    ADD_OBJECT(UartContext) \
    ADD_OBJECT(DevFsFile) \
    ADD_OBJECT(DevFsDir) \
    ADD_OBJECT(RamFsContext) \
    ADD_OBJECT(RamFsFile) \
    ADD_OBJECT(RamFsDir) \
    ADD_OBJECT(LedContext) \
    ADD_OBJECT(PwmContext) \
    ADD_OBJECT(LcdTftContext) \
    ADD_OBJECT(FmcContext) \
    ADD_OBJECT(FlashFsFile) \
    ADD_OBJECT(FlashFsDir) \
    ADD_OBJECT(Screen) \
    ADD_OBJECT(ETHContext) \
    ADD_OBJECT(Netif) \
    ADD_OBJECT(Queue) \
    ADD_OBJECT(TcpConnection) \
    ADD_OBJECT(TcpServer) \
    ADD_OBJECT(TelnetConnectionContext) \
    ADD_OBJECT(TelnetServiceContext) \
    ADD_OBJECT(Service) \
    ADD_OBJECT(GTDContext) \
    ADD_OBJECT(I2CContext) \
    ADD_OBJECT(FT5336Context) \
    ADD_OBJECT(ICtrl) \
    ADD_OBJECT(ICtrlManService) \
    ADD_OBJECT(Widget) \
    ADD_OBJECT(WidgetScreen) \
    ADD_OBJECT(Widgets) \
    ADD_OBJECT(SPIContext) \
    ADD_OBJECT(SDMMCContext) \
    ADD_OBJECT(SDMMC1ByteContext) \
    ADD_OBJECT(SDMMC4ByteContext) \
    ADD_OBJECT(SDMMCSpiContext) \
    ADD_OBJECT(SDMMCModeLLD) \
    ADD_OBJECT(DevFs) \
    ADD_OBJECT(Storage) \
    ADD_OBJECT(Disk) \
    ADD_OBJECT(Partition) \
    ADD_OBJECT(DiskContext) \
    ADD_OBJECT(DiskManServiceContext) \
    ADD_OBJECT(DriverInstanceContext) \
    ADD_OBJECT(FatFs) \
    ADD_OBJECT(FatFsFile) \
    ADD_OBJECT(FatFsDir) \
    ADD_OBJECT(NEUNETContext) \
    ADD_OBJECT(ElfFile) \

//==========================================================================================================================================
/**
 * @brief returns name of the object id created for the object
 */
//==========================================================================================================================================
#define oC_ObjectId_(Name)      oC_1WORD_FROM_2(oC_ObjectId_ , Name)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Object
//! @{

//==========================================================================================================================================
/**
 * @brief stores id of object
 *
 * Each object in the #oC_OBJECTS_REGISTRATIONS_LIST list has assigned own ID number, that can be stored in this type.
 */
//==========================================================================================================================================
typedef enum
{
#define DEFINE_OBJECT_ID(Name)  oC_ObjectId_(Name) ,
    oC_OBJECTS_REGISTRATIONS_LIST(DEFINE_OBJECT_ID)//!< oC_ObjectId_Thread
#undef DEFINE_OBJECT_ID
    oC_ObjectId_NumberOfElements ,            //!< oC_ObjectId_NumberOfElements
    oC_ObjectId_Mask               = 0xFFF ,  //!< oC_ObjectId_Mask
    oC_ObjectId_MagicNumberMask    = 0xFFFFF000 ,
} oC_ObjectId_t;



//==========================================================================================================================================
/**
 * @brief stores object control value
 *
 * The type is for storing object control value. It should be included to the object structure.
 */
//==========================================================================================================================================
typedef uint32_t oC_ObjectControl_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with static inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @addtogroup Object
//! @{

//==========================================================================================================================================
/**
 * @brief counts object control for object
 *
 * The function is for counting special key, that is assigned for each object for protection again wrong pointers given to object functions.
 * Objects cannot be copied without recounting object control value. The function is not checking if the pointer is correct.
 *
 * @param ObjectPointer     Pointer to the object
 * @param ObjectId          Id of the object
 *
 * @return object control value
 */
//==========================================================================================================================================
static inline oC_ObjectControl_t oC_CountObjectControl( void * ObjectPointer , oC_ObjectId_t ObjectId )
{
    oC_STATIC_ASSERT( oC_ObjectId_MagicNumberMask > oC_ObjectId_NumberOfElements, "Too many object definitions!" );

    return (((uint32_t)ObjectPointer) & oC_ObjectId_MagicNumberMask ) | ObjectId;
}

//==========================================================================================================================================
/**
 * @brief returns id of object
 *
 * The function returns id of object stored in the object control value.
 *
 * @param ObjectControl     Object control value
 *
 * @return id of object
 */
//==========================================================================================================================================
static inline oC_ObjectId_t oC_GetObjectId(oC_ObjectControl_t ObjectControl)
{
    return ObjectControl & oC_ObjectId_Mask;
}

//==========================================================================================================================================
/**
 * @brief checks if object control is correct
 *
 * The function is for checking if the object control value is correct for this object. It can be used for protection again usage of wrong
 * pointers in the object functions.
 *
 * @param ObjectPointer     Pointer to the object
 * @param ObjectId          Id of the object
 * @param ObjectControl     Object control value
 *
 * @return true if object control is correct
 */
//==========================================================================================================================================
static inline bool oC_CheckObjectControl( void * ObjectPointer , oC_ObjectId_t ObjectId , oC_ObjectControl_t ObjectControl )
{
    return oC_CountObjectControl(ObjectPointer,ObjectId) == ObjectControl;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @}

#endif /* SYSTEM_LIBRARIES_INC_OC_OBJECT_H_ */
