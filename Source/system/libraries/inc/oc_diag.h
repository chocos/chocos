/*
 * oc_diag.h
 *
 *  Created on: 13.08.2016
 *      Author: pkubiak
 */

#ifndef SYSTEM_LIBRARIES_INC_OC_DIAG_H_
#define SYSTEM_LIBRARIES_INC_OC_DIAG_H_

#include <oc_errors.h>
#include <oc_memory.h>
#include <string.h>
#include <oc_array.h>

typedef enum
{
    oC_Diag_State_NotStarted ,
    oC_Diag_State_Prepared   ,
    oC_Diag_State_InProgress ,
    oC_Diag_State_Finished   ,
    oC_Diag_State_NumberOfStates
} oC_Diag_State_t;

typedef struct oC_Diag_t
{
    const char *       Name;
    const char *       SubName;
    const char *       ResultDescription;
    oC_ErrorCode_t     Result;
    oC_Diag_State_t    State;

    //==============================================================
    /**
     * @brief prints state of the diagnostic
     */
    //==============================================================
    void (*PrintFunction)( struct oC_Diag_t * Diag );
} oC_Diag_t;

typedef oC_ErrorCode_t (*oC_Diag_PerformFunction_t)(oC_Diag_t * Diag , void * Context );

typedef struct
{
    const char *                    Name;
    oC_Diag_PerformFunction_t       PerformFunction;
} oC_Diag_SupportedDiagData_t;

#define oC_Diag_GetNumberOfSupportedDiagnostics( SupportedDiagArray )                oC_ARRAY_SIZE(SupportedDiagArray)

//==========================================================================================================================================
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Diag_PerformDiagnostic( oC_Diag_t * Diag, const char * Name , const char * SubName , oC_Diag_PerformFunction_t PerformFunction , void * Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( Diag            != NULL, oC_ErrorCode_WrongAddress )
     && ErrorCondition( Name            != NULL, oC_ErrorCode_WrongAddress )
     && ErrorCondition( PerformFunction != NULL, oC_ErrorCode_WrongAddress )
        )
    {
        Diag->Name              = Name;
        Diag->SubName           = SubName;
        Diag->ResultDescription = NULL;
        Diag->State             = oC_Diag_State_InProgress;

        if(Diag->PrintFunction)
        {
            Diag->PrintFunction(Diag);
        }

        Diag->Result = PerformFunction(Diag,Context);
        Diag->State  = oC_Diag_State_Finished;

        if(Diag->PrintFunction)
        {
            Diag->PrintFunction(Diag);
        }

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

#define oC_Diag_PerformDiagnostics(SupportedArray,DiagsArray,DiagsSize,SubName,Context)       oC_Diag_PerformDiagnosticsFunction((SupportedArray),oC_ARRAY_SIZE(SupportedArray),(DiagsArray),(DiagsSize),(SubName),(Context))

//==========================================================================================================================================
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Diag_PerformDiagnosticsFunction( const oC_Diag_SupportedDiagData_t * SupportedArray , uint32_t NumberOfSupportedDiags , oC_Diag_t * DiagsArray, uint32_t DiagsSize , const char * SubName , void * Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( SupportedArray != NULL , oC_ErrorCode_WrongAddress )
        )
    {
        errorCode = oC_ErrorCode_None;

        for(uint32_t i = 0; i < NumberOfSupportedDiags && i < DiagsSize && !oC_ErrorOccur(errorCode) ; i++)
        {
            errorCode = oC_Diag_PerformDiagnostic(&DiagsArray[i],SupportedArray->Name,SubName,SupportedArray->PerformFunction,Context);
        }
    }

    return errorCode;
}

#endif /* SYSTEM_LIBRARIES_INC_OC_DIAG_H_ */
