/** ****************************************************************************************************************************************
 *
 * @file       oc_libraries.h
 *
 * @brief      The main file of libraries. It includes all libraries
 *
 * @author     Patryk Kubiak - (Created on: 10 06 2015 18:36:25)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup LibrariesSpace Libraries Space
 * @brief The space for libraries
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_LIBRARIES_H_
#define SYSTEM_LIBRARIES_INC_OC_LIBRARIES_H_

/** ========================================================================================================================================
 *
 *              The section with libraires space description
 *
 *  ======================================================================================================================================*/
#define _________________________________________LIBRARIES_DESCRIPTION_SECTION______________________________________________________________

/**
 * @page LibrariesSpace Libraries Space
 *
 * ![Libraries Architecture](libraries_architecture.jpg)
 *
 */

#undef  _________________________________________LIBRARIES_DESCRIPTION_SECTION______________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with error codes
 *
 *  ======================================================================================================================================*/
#define _________________________________________ERROR_CODES________________________________________________________________________________

#ifdef DOXYGEN
//==========================================================================================================================================
/**
 * @brief stores code of error
 *
 * The type is for storing code of error. The full list of error codes you can find in `oc_errors.h` file
 */
//==========================================================================================================================================
typedef enum
{
oC_ErrorCode_None, //!< No error occurred
oC_ErrorCode_AllocationError , //!< Cannot allocate memory
oC_ErrorCode_ReleaseError , //!< Release memory error
oC_ErrorCode_TimeoutError , //!< Timeout error
oC_ErrorCode_VoltageNotSupported, //!< Voltage is not supported
oC_ErrorCode_ReleasingMutexError , //!< Releasing mutex error
oC_ErrorCode_ChannelNotConfigured , //!< Channel not configured
oC_ErrorCode_ExtiLineIsUsed , //!< Exti line is used
oC_ErrorCode_ParentProcessNotSet , //!< None of processes contains this thread
oC_ErrorCode_ThreadNotSet , //!< Thread not set
oC_ErrorCode_ProhibitedByArchitecture , //!< Prohibited by architecture
oC_ErrorCode_PixelFormatNotCorrect , //!< Pixel format not correct
oC_ErrorCode_PixelFormatNotConfigured , //!< Pixel format not configured
oC_ErrorCode_WrongLayer , //!< Wrong layer
oC_ErrorCode_AddressNotAligned , //!< Address is not aligned
oC_ErrorCode_TestFailed , //!< Test failed
oC_ErrorCode_SyntaxNotCorrect , //!< Syntax not correct
oC_ErrorCode_NoCommandSpecified , //!< No command specified
oC_ErrorCode_OutputBufferTooSmall , //!< Output buffer is too small
oC_ErrorCode_WrongModuleIndex , //!< Wrong module index
oC_ErrorCode_DaemonThreadAlreadyExists , //!< Daemon thread already exists
oC_ErrorCode_LoopbackNotSupported , //!< Loopback not supported
oC_ErrorCode_NotAllSent , //!< Not all selements sent
oC_ErrorCode_CannotFindModulePin , //!< Cannot find module pin
oC_ErrorCode_WrongModulePinChannel , //!< Wrong module pin channel
oC_ErrorCode_ModulePinIsNotGiven , //!< Module pin is not given
oC_ErrorCode_NoneOfModulePinAssigned , //!< None of module pin assigned to the given pin
oC_ErrorCode_DaemonProcessAlreadyExists , //!< Daemon process already exists
oC_ErrorCode_ChipNotDefined , //!< Chip is not defined
oC_ErrorCode_CannotCreateProcess , //!< Cannot create process
oC_ErrorCode_ArrayNotCorrect , //!< Array not correct
oC_ErrorCode_ArraySizeTooBig , //!< Array size too big
oC_ErrorCode_CannotCreateThread , //!< Cannot create thread
oC_ErrorCode_CannotInitializeModule , //!< Cannot initialize module
oC_ErrorCode_NotHandledByFileSystem , //!< Not handled by file system
oC_ErrorCode_CannotRunThread , //!< Cannot run thread
oC_ErrorCode_NotAllPinsInTheSameState , //!< Not all pins in the same state
oC_ErrorCode_FileSystemNotCorrect , //!< File System not correct
oC_ErrorCode_PathAlreadyUsed , //!< Path already used
oC_ErrorCode_DriverContextNotSet , //!< Driver context not set
oC_ErrorCode_StringNotAsExpected , //!< String is not as expected
oC_ErrorCode_SpecifierNotHandled , //!< Specifier not handled
oC_ErrorCode_WrongNumberFormat , //!< Wrong number format
oC_ErrorCode_NoSuchFile , //!< No such file
oC_ErrorCode_FileAlreadyExists , //!< File already exists
oC_ErrorCode_FileIsReadOnly , //!< File is read only
oC_ErrorCode_CannotCreateFile , //!< Cannot create file
oC_ErrorCode_CannotDeleteFile , //!< Cannot delete file
oC_ErrorCode_DirectoryNotExists , //!< Directory not exists
oC_ErrorCode_VoltageNotCorrect , //!< Voltage is not correct
oC_ErrorCode_ExistingFileNotCorrect , //!< Existing file is not correct
oC_ErrorCode_FileIsBusy , //!< File is busy
oC_ErrorCode_FileIsEmpty , //!< File is empty
oC_ErrorCode_EndOfFile , //!< End of file
oC_ErrorCode_FileNotCorrect , //!< File not correct
oC_ErrorCode_NoAllBytesRead , //!< No all bytes read
oC_ErrorCode_OffsetTooBig , //!< Offset too big
oC_ErrorCode_ModeNotCorrect , //!< Mode not correct
oC_ErrorCode_DigitBaseNotCorrect , //!< Digit base is not correct
oC_ErrorCode_StreamTypeNotCorrect , //!< Stream type is not correct
oC_ErrorCode_ProcessNotCorrect , //!< Process is not correct
oC_ErrorCode_RecursiveDriverUsage , //!< Driver cannot be used in recursive mode
oC_ErrorCode_ThreadNotCorrect , //!< Thread is not correct
oC_ErrorCode_WidthTooBig , //!< Width too big
oC_ErrorCode_HeightTooBig , //!< Height too big
oC_ErrorCode_InvertNotSupported , //!< Invert not supported
oC_ErrorCode_DefaultProgramNotSet , //!< Default program is not set
oC_ErrorCode_StreamNotCorrect , //!< Stream is not correct
oC_ErrorCode_ContextNotCorrect , //!< Context is not correct
oC_ErrorCode_CannotDeleteCurrentThread , //!< Cannot delete current thread
oC_ErrorCode_CannotDeleteProcess , //!< Cannot delete process
oC_ErrorCode_ListNotCorrect , //!< List is not correct
oC_ErrorCode_BitOrderNotSupported , //!< Bit order is not supported
oC_ErrorCode_PathNotCorrect , //!< Path not correct
oC_ErrorCode_ParityNotSupported , //!< Parity is not supported
oC_ErrorCode_ParityNotCorrect , //!< Parity is not correct
oC_ErrorCode_StopBitNotSupported , //!< Stop bit is not supported
oC_ErrorCode_Timeout , //!< Timeout
oC_ErrorCode_ModuleIsBusy , //!< Module is busy
oC_ErrorCode_NoFileSystemMounted , //!< No file system mounted
oC_ErrorCode_NoSupportedByFileSystem , //!< No supported by file system
oC_ErrorCode_ObjectNotCorrect , //!< Object is not correct
oC_ErrorCode_ObjectNotFoundOnList , //!< Object not found on list
oC_ErrorCode_SomeDataLost , //!< Some data was lost
oC_ErrorCode_CannotUnblockThread , //!< Cannot unblock thread
oC_ErrorCode_RootUserNotExist , //!< Root user not exists
oC_ErrorCode_CannotRemoveRoot , //!< Cannot remove root
oC_ErrorCode_UserInUse , //!< User already in use
oC_ErrorCode_UserExists , //!< User already exists
oC_ErrorCode_UserNotExists , //!< User doesn't exist
oC_ErrorCode_CannotRenameUser , //!< Cannot rename user
oC_ErrorCode_InvalidUserName , //!< Invalid user name
oC_ErrorCode_WrongThread , //!< Wrong thread
oC_ErrorCode_NotImplemented , //!< Functionality not implemented
oC_ErrorCode_ImplementError , //!< Developer has forgot about this path!
oC_ErrorCode_WrongParameters , //!< Given parameters are not correct
oC_ErrorCode_WrongFrequency , //!< Given frequency is not correct
oC_ErrorCode_WrongUser , //!< User is not correct
oC_ErrorCode_WrongAddress , //!< Given address is not correct
oC_ErrorCode_WrongConfigAddress , //!< Given configuration address is not correct
oC_ErrorCode_WrongEventHandlerAddress , //!< The event handler address is not correct
oC_ErrorCode_FrequencyNotPossible , //!< Configuration of this frequency is not possible
oC_ErrorCode_OutputAddressNotInRAM , //!< The address for output function variable is not pointing to RAM section.
oC_ErrorCode_WrongChannel , //!< Given channel is not correct
oC_ErrorCode_BitRateNotCorrect , //!< Bit rate is not correct
oC_ErrorCode_SpeedNotCorrect , //!< Speed is not correct
oC_ErrorCode_BitRateNotSupported , //!< Bit rate is not supported
oC_ErrorCode_BitRateNotSet , //!< Bit rate is not set
oC_ErrorCode_ChannelIsUsed , //!< Given channel is used
oC_ErrorCode_ValueTooBig , //!< Given value is too big
oC_ErrorCode_MaximumValueNotCorrect , //!< Maximum value is not correct
oC_ErrorCode_CannotRestoreDefaultState , //!< Cannot restore default module state
oC_ErrorCode_DriverNotTurnedOn , //!< The driver is not turned on!
oC_ErrorCode_DriverNotTurnedOff , //!< The driver is not turned off!
oC_ErrorCode_DriverNotRegistered , //!< The given driver is not registered!
oC_ErrorCode_NotHandledByDriver , //!< The given driver not handle this functionality!
oC_ErrorCode_FunctionNotExists , //!< Function not exists!
oC_ErrorCode_ModuleNotStartedYet , //!< Module has not started yet!
oC_ErrorCode_NoneElementReceived , //!< None element received!
oC_ErrorCode_ModuleIsTurnedOn , //!< Module is already turned on!
oC_ErrorCode_ModuleIsTestedAlready , //!< Module is already tested!
oC_ErrorCode_InterruptHandlerNotSet , //!< Handler function for interrupt is not set!
oC_ErrorCode_InterruptHandlerAlreadySet , //!< Handler function for interrupt is already set!
oC_ErrorCode_CannotTurnOnInterruptInMachineModule , //!< Cannot turn on interrupt in the machine!
oC_ErrorCode_CannotTurnOffInterruptInMachineModule , //!< Cannot turn off interrupt in the machine!
oC_ErrorCode_MachineHeapError , //!< Machine heap is not correct!
oC_ErrorCode_PositionNotCorrect , //!< Position is not correct!
oC_ErrorCode_MachineCanBeDamaged , //!< Unexpected behavior! Machine can be damaged!
oC_ErrorCode_WrongStackSize , //!< The given stack size is not correct!
oC_ErrorCode_PolarityNotCorrect , //!< Polarity not correct!
oC_ErrorCode_ChannelOperationsNotDisabled , //!< Channel operations not disabled!
oC_ErrorCode_ChannelOperationsNotEnabled , //!< Channel operations not enabled!
oC_ErrorCode_CannotEnableInterrupt , //!< Cannot enable interrupt!
oC_ErrorCode_CannotDisableInterrupt , //!< Cannot disable interrupt!
oC_ErrorCode_HeapPointersNotInitialized , //!< Heap pointers are not initialized!
oC_ErrorCode_ModuleNeedRestart , //!< Restart of the module is required!
oC_ErrorCode_ModuleUsedByDifferentModule , //!< Module used by different module!
oC_ErrorCode_CannotInitializeStack , //!< Cannot initialize stack!
oC_ErrorCode_ClockConfigurationError , //!< Error while configuration of clock
oC_ErrorCode_UnsupportedOscillator , //!< The given oscillator is not supported
oC_ErrorCode_ResolutionNotSet , //!< Resolution not set
oC_ErrorCode_SystemClockNotConfigured , //!< The system clock is not configured
oC_ErrorCode_NotSupportedOnTargetMachine , //!< The feature is not supported on the target machine
oC_ErrorCode_ChannelNotPoweredOn , //!< The given channel is not turned on!
oC_ErrorCode_CannotEnableChannel , //!< The given channel cannot be turned on!
oC_ErrorCode_NotSinglePin , //!< The given pin is not single pin!
oC_ErrorCode_PowerStateNotCorrect , //!< The given power state is not correct!
oC_ErrorCode_PeripheralPinNotDefined , //!< Peripheral pin is not defined
oC_ErrorCode_PinNotDefined , //!< Pin is not defined
oC_ErrorCode_WordLengthNotCorrect , //!< Word length is not correct
oC_ErrorCode_ColorNotCorrect , //!< Color is not correct
oC_ErrorCode_PinIsUsed , //!< Pin is used
oC_ErrorCode_PinIsNotUsed , //!< Pin is not used
oC_ErrorCode_OutputArrayToSmall , //!< Output array is too small
oC_ErrorCode_MachineSpecificValueNotCorrect , //!< The machine specific value is not correct!
oC_ErrorCode_SizeNotCorrect , //!< The size is not correct!
oC_ErrorCode_NoChannelAvailable , //!< None of channel is available!
oC_ErrorCode_CannotAddObjectToList , //!< Cannot add object to list!
oC_ErrorCode_CannotRemoveObjectFromList , //!< Cannot remove object from list!
oC_ErrorCode_CannotDeleteObject , //!< Cannot delete object!
oC_ErrorCode_IndexNotCorrect , //!< Index is not correct
oC_ErrorCode_CommandNotCorrect , //!< Command is not correct!
oC_ErrorCode_CommandNotHandled , //!< Command is not handled by the module!
oC_ErrorCode_DmaModeNotCorrect , //!< DMA mode is not correct
oC_ErrorCode_DMAAddressNotHandledByDma , //!< The address is not handled by the DMA!
oC_ErrorCode_DMAElementSizeNotCorrect , //!< Element size is not correct!
oC_ErrorCode_DMANumberOfTransfersNotCorrect , //!< Number of transfers is not correct!
oC_ErrorCode_DMABufferSizeMustBePowerOf2 , //!< Buffer size must be power of 2!
oC_ErrorCode_DMASoftwareTransferNotPossibleOnThisChannel , //!< Software transfer is not possible on this channel!
oC_ErrorCode_DMAPeripheralTransferNotPossibleOnThisChannel , //!< This peripheral transfer is not possible on this channel!
oC_ErrorCode_DMATransmitDirectionNotCorrect , //!< Transmit direction is not correct!
oC_ErrorCode_PinNeedUnlock , //!< This pin need to be unlocked before usage
oC_ErrorCode_GPIOPinNeedUnlock , //!< This pin need to be unlocked before usage
oC_ErrorCode_GPIOPinIncorrect , //!< Pin is incorrect
oC_ErrorCode_GPIOPinIsUsed , //!< Pin is used
oC_ErrorCode_GPIOCurrentNotCorrect , //!< The given GPIO current is not correct
oC_ErrorCode_GPIOModeNotCorrect , //!< The given GPIO mode is not correct
oC_ErrorCode_PullNotCorrect , //!< The given pull is not correct
oC_ErrorCode_OutputCircuitNotCorrect , //!< The given output circuit mode is not correct
oC_ErrorCode_GPIOIntTriggerNotCorrect , //!< The given GPIO interrupt trigger is not correct
oC_ErrorCode_GPIOModeConfigureError , //!< Error while setting mode of GPIO pin
oC_ErrorCode_GPIOOutputCircuitConfigureError , //!< Error while setting output circuit of GPIO pin
oC_ErrorCode_GPIOPullConfigureError , //!< Error while setting pull of GPIO pin
oC_ErrorCode_GPIOCurrentConfigureError , //!< Error while setting current of GPIO pin
oC_ErrorCode_GPIOSpeedConfigureError , //!< Error while setting speed of GPIO pin
oC_ErrorCode_GPIOInterruptConfigureError , //!< Error while setting interrupt of GPIO pin
oC_ErrorCode_GPIOADDInterruptToListError , //!< Error while adding interrupt of GPIO pin to list
oC_ErrorCode_SYSUnknownOscillatorSource , //!< Unknown oscillator source
oC_ErrorCode_SYSConfigurationError , //!< Error while configuration of system clock
oC_ErrorCode_OSPIDNotExists , //!< Process with given ID not exists
oC_ErrorCode_OSNotAllThreadsCancelled , //!< One or more threads cannot be cancelled
oC_ErrorCode_OSDuplicatePID , //!< Process ID is duplicated
oC_ErrorCode_OSProcessNotFinished , //!< Process is not finished
oC_ErrorCode_OSGlobalVariableRegistrationError , //!< Error while registration of the global variable
oC_ErrorCode_OSCannotKillChildProcess , //!< At least one of children processes cannot be killed
oC_ErrorCode_OSWrongThread , //!< Not correct thread reference
oC_ErrorCode_OSWrongTTY , //!< Not correct TTY reference
oC_ErrorCode_TTYConfigurationIncorrect , //!< Configuration is not correct
oC_ErrorCode_TTYCannotCreateDLIST , //!< Cannot create dynamic list
oC_ErrorCode_TTYCannotCreateMutex , //!< Cant create mutex object
oC_ErrorCode_UARTNotCorrectChannel , //!< The given channel number is not correct
oC_ErrorCode_UARTCannotPrepareTRAHN , //!< Error while creating TRANH object
oC_ErrorCode_UARTPinNotDefined , //!< Given pin is not defined
oC_ErrorCode_UARTCantConfigurePin , //!< Cannot configure GPIO pin
oC_ErrorCode_UARTNotCorrectWordLength , //!< Given word length is not correct
oC_ErrorCode_UARTWordLengthNotSupported , //!< Given word length is not supported on selected architecture
oC_ErrorCode_UARTParityNotSupported , //!< Given parity mode is not supported on selected architecture
oC_ErrorCode_UARTStopBitNotSupported , //!< Given stop bit mode is not supported on selected architecture
oC_ErrorCode_UARTBitOrderNotSupported , //!< Given bit order mode is not supported on selected architecture
oC_ErrorCode_UARTInvertNotSupported , //!< Given invert mode is not supported on selected architecture
oC_ErrorCode_UARTBitRateNotSupported , //!< Given bit-rate is not supported on this architecture
oC_ErrorCode_UARTInterruptConfigureError , //!< Error while setting interrupt priority for UART channel
oC_ErrorCode_TIMLockingLockedChannel , //!< Trying to lock locked channel
oC_ErrorCode_TIMUnlockingUnlockedChannel , //!< Trying to unlock channel which is not locked
oC_ErrorCode_TIMChannelNotConfigured , //!< The selected channel is not configured
oC_ErrorCode_TIMMatchCallbackNotCorrect , //!< The match callback function pointer is not correct
oC_ErrorCode_TIMMatchContextNotCorrect , //!< The match callback context pointer is not correct
oC_ErrorCode_TIMTimeoutCallbackNotCorrect , //!< The timeout callback function pointer is not correct
oC_ErrorCode_TIMTimeoutContextNotCorrect , //!< The timeout callback context pointer is not correct
oC_ErrorCode_TIMNoneOfChannelsAvailable , //!< None of channel available
oC_ErrorCode_TIMModeNotCorrect , //!< Timer mode is not correct
oC_ErrorCode_TIMERCountsDirNotCorrect , //!< Count direction is not correct
oC_ErrorCode_TIMERSubTimerNotCorrect , //!< SubTimer is not correct
oC_ErrorCode_TIMERBothSubTimersNeeeded , //!< Both SubTimers are needed
oC_ErrorCode_TIMERCountsDirNotPossible , //!< Count direction is not possible with this configuration
oC_ErrorCode_TIMERModeNotCorrect , //!< Mode is not correct
oC_ErrorCode_TIMERModeNotSelected , //!< Timer mode not selected
oC_ErrorCode_TIMERTriggerNotCorrect , //!< Input trigger is not correct
oC_ErrorCode_TIMERChannelNotStopped , //!< Channel is not stopped
oC_ErrorCode_TIMERFrequencyNotPossibleInFullMode , //!< Frequency not possible in full timer mode
oC_ErrorCode_TIMERMaximumValueTooBig , //!< Maximum value is too big
oC_ErrorCode_TIMTickSourceNotSupported , //!< The selected TickSource is not supported
oC_ErrorCode_TIMSizeNotSupported , //!< The given size is not supported
oC_ErrorCode_TIMTickPeriodNotSupported , //!< The given tick period is not supported
oC_ErrorCode_TIMSizeNotSupportedOnSelectedChannel , //!< The given size is not supported on selected channel
oC_ErrorCode_TIMMatchValueTooBig , //!< The given match value is too big
oC_ErrorCode_TIMMaxValueTooBig , //!< The given max value is too big
oC_ErrorCode_TIMCantCalculatePrescaler , //!< Error while calculating prescaler
oC_ErrorCode_TIMTickPeriodTooSmall , //!< The given tick period is too small for this channel
oC_ErrorCode_TIMTickPeriodTooBig , //!< The given tick period is too big for this channel
oC_ErrorCode_TIMCountsDirNotSupported , //!< The given counts direction is not supported
oC_ErrorCode_TIMValueTooBig , //!< The given timer value is too big for this channel
oC_ErrorCode_TimeAlreadyTurnedOn , //!< Time module is already turned on
oC_ErrorCode_TimeAlreadyTurnedOff , //!< Time module is already turned off
oC_ErrorCode_TimeCannotCreateWakeUpList , //!< Error while creating wake up list
oC_ErrorCode_TimeCannotCreateConfigEvent , //!< Error while creating configuration event
oC_ErrorCode_ClockPhaseNotSupported , //!< Given clock phase is not supported on selected architecture
oC_ErrorCode_ClockPolarityNotSupported , //!< Given clock polarity is not supported on selected architecture
oC_ErrorCode_ChipSelectPolarityNotSupported , //!< Given chip select polarity is not supported on selected architecture
oC_ErrorCode_FunctionNotSupportedInCurrentMode , //!< The function is not supported in current mode
oC_ErrorCode_ModeNotSupported , //!< Given mode is not supported on selected architecture
oC_ErrorCode_FrameWidthNotSupported , //!< Given frame width is not supported on selected architecture
oC_ErrorCode_FrameTypeNotSupported , //!< Given frame type is not supported on selected architecture
oC_ErrorCode_StateNotSupported , //!< Given state of Chip Select is not supported
oC_ErrorCode_PulseWidthTooBig , //!< Pulse width too big
oC_ErrorCode_AddressNotInRam , //!< Address is not in ram
oC_ErrorCode_WrongBufferEndAddress , //!< Wrong buffer end address
oC_ErrorCode_ColorFormatNotSupported , //!< Color format not supported
oC_ErrorCode_ColorFormatNotCorrect , //!< Color format not correct
oC_ErrorCode_ColorMapNotCorrect , //!< Color map not correct
oC_ErrorCode_TimingParametersNotSupported , //!< Timing parameters not supported
oC_ErrorCode_TimingParametersNotFilled , //!< Timing parameters not filled
oC_ErrorCode_AddressNotInExternalMemory , //!< Address is not stored in external memory
oC_ErrorCode_HeapMapAlreadyConfigured , //!< Heap map already configured
oC_ErrorCode_HeapMapNotConfigured , //!< Heap map not configured
oC_ErrorCode_WritingNotPermitted , //!< Writing not permitted
oC_ErrorCode_ReadingNotPermitted , //!< Reading not permitted
oC_ErrorCode_ProtectionNotCorrect , //!< Protection not correct
oC_ErrorCode_DataBusWidthNotCorrect , //!< Data bus width not correct
oC_ErrorCode_DataBusWidthNotSupported , //!< Data bus width not supported
oC_ErrorCode_AccessWidthNotCorrect , //!< Access width not correct
oC_ErrorCode_SizeTooBig , //!< Size too big
oC_ErrorCode_NoFreeBankAvailable , //!< No free bank available
oC_ErrorCode_MemoryTypeNotCorrect , //!< Memory type is not correct
oC_ErrorCode_DelayNotPossible , //!< Delay is not possible
oC_ErrorCode_RowBitsNumberNotSupported , //!< Row bits number not supported
oC_ErrorCode_ColumnBitsNumberNotSupported , //!< Column bits number not supported
oC_ErrorCode_AutoStackMethodNotCorrect , //!< Auto stack method is not correct
oC_ErrorCode_NumberOfBanksNotCorrect , //!< Number of banks not correct
oC_ErrorCode_NumberOfBanksNotSupported , //!< Number of banks not supported
oC_ErrorCode_CasLatencyNotCorrect , //!< CAS Latency not correct
oC_ErrorCode_CasLatencyNotSupported , //!< CAS Latency not supported
oC_ErrorCode_DirectAccessNotPossible , //!< Direct access not possible
oC_ErrorCode_CannotAccessMemory , //!< Cannot access memory
oC_ErrorCode_AccessPermissionsNotPossible , //!< Access permissions are not possible
oC_ErrorCode_AccessPermissionsNotCorrect , //!< Access permissions are not correct
oC_ErrorCode_RegionNumberNotCorrect , //!< Region number is not correct
oC_ErrorCode_AutoConfigurationsNotAvailable , //!< Auto configurations not available
oC_ErrorCode_NumberOfRowsNotCorrect , //!< Number of rows is not correct
oC_ErrorCode_MemoryNotConfigured , //!< Memory not configured
oC_ErrorCode_AutoRefreshPeriodNotCorrect , //!< Auto refresh period not correct
oC_ErrorCode_ReadPipeDelayNotPossible , //!< Read pipe delay not possible
oC_ErrorCode_NotGraphicDriver , //!< Driver is not graphic driver!
oC_ErrorCode_AlreadyConfigured , //!< Already configured
oC_ErrorCode_NotConfiguredYet , //!< Not configured yet
oC_ErrorCode_ObjectProtected , //!< Object is protected
oC_ErrorCode_CommunicationInterfaceNotCorrect , //!< Communication interface not correct
oC_ErrorCode_PinNotSet , //!< Required pin is not set
oC_ErrorCode_CannotDisableChannel , //!< Cannot disable channel
oC_ErrorCode_MacNotInitialized , //!< MAC not initialized
oC_ErrorCode_PhyAddressNotCorrect , //!< PHY address not correct
oC_ErrorCode_RegisterAddressNotCorrect , //!< Register address not correct
oC_ErrorCode_CannotAccessRegister , //!< Cannot access register
oC_ErrorCode_OtherCommunicationInterfaceInUse , //!< Other communication interface in use
oC_ErrorCode_NoMoreConfigurationsPossible , //!< No more configurations possible
oC_ErrorCode_ChipNameNotDefined , //!< Chip name is not defined
oC_ErrorCode_DriverTypeNotCorrect , //!< Driver type not correct
oC_ErrorCode_StringIsEmpty , //!< String is empty
oC_ErrorCode_StringIsTooLong , //!< String is too long
oC_ErrorCode_CannotGiveSemaphore , //!< Cannot give semaphore
oC_ErrorCode_FoundOnList , //!< Already found on the list
oC_ErrorCode_NoneActiveNetif , //!< None of network interfaces isn't active
oC_ErrorCode_IpAddressNotCorrect , //!< IP address is not correct
oC_ErrorCode_IpAddressEmpty , //!< IP address is empty
oC_ErrorCode_AddressTypeNotCorrect , //!< Address type is not correct
oC_ErrorCode_RoutingTableEntryNotFound , //!< Routing table entry not found
oC_ErrorCode_NetifNotCorrect , //!< Netif not correct
oC_ErrorCode_ProcessAlreadyStarted , //!< Process has started already
oC_ErrorCode_CannotDeleteThread , //!< Cannot delete thread
oC_ErrorCode_LinkNotDetected , //!< Link not detected
oC_ErrorCode_UnknownHardwareType , //!< Unknown hardware type
oC_ErrorCode_UnknownAddressType , //!< Unknown address type
oC_ErrorCode_UnknownHardwareAddress , //!< Unknown hardware address
oC_ErrorCode_HardwareAddressIsEmpty , //!< Hardware address is empty
oC_ErrorCode_AlignmentNotCorrect , //!< Alignment not correct
oC_ErrorCode_DescriptorListNotInitialized , //!< Descriptor list not initialized
oC_ErrorCode_NoFreeSlots , //!< No free slots
oC_ErrorCode_InternalDataAreDamaged , //!< Internal data are damaged (stack has been overwritten?)
oC_ErrorCode_DataNotAvailable , //!< Data not available
oC_ErrorCode_BaudRateNotSupported , //!< Baud rate not supported
oC_ErrorCode_OperationModeNotCorrect , //!< Operation mode not correct
oC_ErrorCode_WrongMacAddressIndex , //!< Wrong MAC address index
oC_ErrorCode_TimeNotCorrect , //!< Time not correct
oC_ErrorCode_AllocatorNotCorrect , //!< Allocator not correct
oC_ErrorCode_ReceiveError , //!< Receive error
oC_ErrorCode_UnfinishedPacket , //!< Unfinished packet
oC_ErrorCode_MemoryAlreadyAllocated , //!< Memory already allocated
oC_ErrorCode_MemoryNotAllocated , //!< Memory not allocated
oC_ErrorCode_UnknownLayer , //!< Unknown layer
oC_ErrorCode_NotDmaAddress , //!< The given address is not correct DMA address
oC_ErrorCode_CannotConvertHeaderEndianess , //!< Cannot convert header endianess
oC_ErrorCode_PortNotCorrect , //!< Port not correct
oC_ErrorCode_PortNotReserved , //!< Port not reserved
oC_ErrorCode_PortNotAvailable , //!< Port not available
oC_ErrorCode_PortReservedByDifferentProcess , //!< Port reserved by different process
oC_ErrorCode_PermissionDenied , //!< Permission denied
oC_ErrorCode_PossibleOnlyForRoot , //!< Possible only for root
oC_ErrorCode_TypeNotCorrect , //!< Type is not correct
oC_ErrorCode_PacketNotCorrect , //!< Packet is not correct
oC_ErrorCode_CannotFindNetifForTheAddress , //!< Cannot find Netif for the given address
oC_ErrorCode_ModuleBusy , //!< Module is busy
oC_ErrorCode_PortBusy , //!< Port is busy
oC_ErrorCode_AddressNotInInternalRam , //!< Address is not in internal RAM
oC_ErrorCode_SegmentTypeNotSupported , //!< Segment type is not supported
oC_ErrorCode_HardwareAddressNotFilled , //!< Hardware address not filled
oC_ErrorCode_SizeNotAligned , //!< Size is not aligned
oC_ErrorCode_TypeReservedByDifferentProcess , //!< Type reserved by different process
oC_ErrorCode_TypeNotReserved , //!< Type is not reserved
oC_ErrorCode_TypeBusy , //!< Type is busy
oC_ErrorCode_HostNotAvailable , //!< Host not available
oC_ErrorCode_NotConnected , //!< Not connected
oC_ErrorCode_AlreadyConnected , //!< Already connected
oC_ErrorCode_ServerNotStarted , //!< Server not started
oC_ErrorCode_DriverImplementedIncorrectly , //!< Driver implemented incorrectly
oC_ErrorCode_NoSpaceAvailable , //!< No space available
oC_ErrorCode_OptionNotFound , //!< Option not found
oC_ErrorCode_OptionNotCorrect , //!< Option not correct
oC_ErrorCode_OptionNotAdded , //!< Option not added
oC_ErrorCode_WindowSizeNotCorrect , //!< Window size not correct
oC_ErrorCode_ObjectInUse , //!< Object in use
oC_ErrorCode_CannotClearOptions , //!< Cannot clear options
oC_ErrorCode_ConnectionNotRequested , //!< Connection not requested
oC_ErrorCode_AlreadyRunning , //!< Already running
oC_ErrorCode_NotRunning , //!< Not running
oC_ErrorCode_ConnectionExpired , //!< Connection has expired
oC_ErrorCode_ConnectionFromDifferentServer , //!< Connection from different server
oC_ErrorCode_ModuleNotCorrect , //!< Module not correct
oC_ErrorCode_ModuleAlreadyRegistered , //!< Module already registered
oC_ErrorCode_ModuleNotRegistered , //!< Module not registered
oC_ErrorCode_PortAlreadyReserved , //!< Port already reserved
oC_ErrorCode_RequiredModuleNotEnabled , //!< Required module not enabled
oC_ErrorCode_WrongCommandArgument , //!< Wrong command argument
oC_ErrorCode_CannotKillProcess , //!< Cannot kill process
oC_ErrorCode_ConnectionInProgress , //!< Connection in progress
oC_ErrorCode_ConnectionHasBeenFinishedTooEarly , //!< Connection has been finished too early
oC_ErrorCode_ModuleHasNotStartedCorrectly , //!< Module has not started correctly
oC_ErrorCode_ModuleHasNotBeenStoppedCorrectly , //!< Module has not been stopped correctly
oC_ErrorCode_ServiceNotStarted , //!< Service has not started yet
oC_ErrorCode_ServiceAlreadyStarted , //!< Service already started
oC_ErrorCode_ProgramNotCorrect , //!< Program not correct
oC_ErrorCode_NoDataToSend , //!< No data to send
oC_ErrorCode_NoSpaceOnRemoteServer , //!< No space on remote server
oC_ErrorCode_InterruptsNotEnabled , //!< Interrupts not enabled
oC_ErrorCode_NoSuchScreen , //!< No such screen
oC_ErrorCode_WidthNotCorrect , //!< Width not correct
oC_ErrorCode_HeightNotCorrect , //!< Height not correct
oC_ErrorCode_FontNotCorrect , //!< Font not correct
oC_ErrorCode_DeviceAddressNotCorrect , //!< Device address not correct
oC_ErrorCode_SpeedModeNotCorrect , //!< Speed mode not correct
oC_ErrorCode_SpeedModeNotSupported , //!< Speed mode not supported
oC_ErrorCode_PeripheralNotEnabled , //!< Peripheral not enabled
oC_ErrorCode_NoDataToReceive , //!< No data to receive
oC_ErrorCode_NotMasterMode , //!< Not master mode
oC_ErrorCode_MasterMode , //!< It is master mode
oC_ErrorCode_AddressModeNotCorrect , //!< Address mode is not correct
oC_ErrorCode_TransmitFifoIsFull , //!< Transmit fifo is full
oC_ErrorCode_AddressNotCorrect , //!< Address not correct
oC_ErrorCode_InterruptNotSupported , //!< Interrupt not supported
oC_ErrorCode_AcknowledgeNotReceived , //!< Acknowledge not received
oC_ErrorCode_ModuleHasHanged , //!< Module has hanged
oC_ErrorCode_NotAllDataSent , //!< Not all data has been sent
oC_ErrorCode_TransmissionNotStarted , //!< Transmission not started
oC_ErrorCode_TransmissionStartedForRead , //!< Transmission started for read
oC_ErrorCode_TransmissionStartedForWrite , //!< Transmission started for write
oC_ErrorCode_NoRequestFromMaster , //!< No request from master
oC_ErrorCode_MasterAskedAboutDataWrite , //!< Master asked about data write
oC_ErrorCode_MasterAskedAboutDataRead , //!< Master asked about data read
oC_ErrorCode_MasterChangedHisMind , //!< Master has changed his mind
oC_ErrorCode_RawTransmissionStarted , //!< Raw transmission started
oC_ErrorCode_RawTransmissionNotStarted , //!< Raw transmission not started
oC_ErrorCode_BusOwnershipNotTaken , //!< Bus ownership not taken
oC_ErrorCode_TransmissionNotComplete , //!< Transmission not completed
oC_ErrorCode_EarlyStopDetected , //!< Early stop detected
oC_ErrorCode_ArbitrationLost , //!< Arbitration lost
oC_ErrorCode_BusError , //!< Bus error
oC_ErrorCode_PecError , //!< Pec error
oC_ErrorCode_OverrunError , //!< Overrun error
oC_ErrorCode_UnderrunError , //!< Underrun error
oC_ErrorCode_IncorrectMask , //!< Incorrect mask
oC_ErrorCode_EventNotSupported , //!< Event not supported
oC_ErrorCode_UnexpectedRegisterValue , //!< Unexpected register value
oC_ErrorCode_DriverNotConfiguredYet , //!< Driver not configured yet
oC_ErrorCode_ChipNotDetected , //!< Chip not detected
oC_ErrorCode_UnknownGesture , //!< Unknown gesture
oC_ErrorCode_DirectionNotCorrect , //!< Direction is not correct
oC_ErrorCode_StringLengthNotCorrect , //!< String length is not correct
oC_ErrorCode_NumberOfElementsNotCorrect , //!< Number of elements is not correct
oC_ErrorCode_StyleNotCorrect , //!< Style is not correct
oC_ErrorCode_NoSuchDefinition , //!< No such definition
oC_ErrorCode_IncreaseValueNotCorrect , //!< Increase value not correct
oC_ErrorCode_CannotRegisterActivity , //!< Cannot register activity
oC_ErrorCode_WrongWidgetDefinition , //!< Wrong widget definition
oC_ErrorCode_LayerIndexNotCorrect , //!< Layer index is not correct
oC_ErrorCode_CannotSwitchColorMapLayer , //!< Cannot switch color map layer
oC_ErrorCode_NotElfFile , //!< It is not an ELF file
oC_ErrorCode_FileNotLoaded , //!< File not loaded
oC_ErrorCode_NotExecFile , //!< It is not an executable file
oC_ErrorCode_RequiredSectionNotFound , //!< Required section has not been found
oC_ErrorCode_WrongOffset , //!< Wrong offset
oC_ErrorCode_SectionSizeNotCorrect , //!< Section size is not correct
oC_ErrorCode_NotCBinFile , //!< It is not an CBin file
oC_ErrorCode_ProgramHeaderTooSmall , //!< Program header is too small!
oC_ErrorCode_SectionsDefinitionTooSmall , //!< Sections definition is too small!
oC_ErrorCode_UnknownSystemCall , //!< Unknown system call!
oC_ErrorCode_ProgramNotPrepared , //!< Program not prepared!
oC_ErrorCode_ArgumentCountNotCorrect , //!< Argument count is not correct
oC_ErrorCode_FrameFormatNotCorrect , //!< Frame format is not correct
oC_ErrorCode_ChipIdNotCorrect , //!< Chip ID is not correct
oC_ErrorCode_BaudRateNotCorrect , //!< Baud rate is not correct
oC_ErrorCode_DataSizeNotSupported , //!< Data size is not supported
oC_ErrorCode_ChannelNotLocked , //!< Channel is not locked
oC_ErrorCode_ToleranceNotCorrect , //!< Tolerance is not correct
oC_ErrorCode_CardNotDetected , //!< Card has not been detected
oC_ErrorCode_NotHandledByInterfaceMode , //!< It is not handled in the given interface mode
oC_ErrorCode_CallbackAlreadySet , //!< Callback already set
oC_ErrorCode_WrongSectorNumber , //!< Wrong sector number
oC_ErrorCode_ObjectAlreadyExist , //!< Object already exist
oC_ErrorCode_NameNotAvailable , //!< Name is not available
oC_ErrorCode_NoSuchPartition , //!< So such partition
oC_ErrorCode_PartitionAlreadyExist , //!< Partition already exist
oC_ErrorCode_WrongPartitionType , //!< Wrong partition type
oC_ErrorCode_WrongSectorCount , //!< Wrong sector count
oC_ErrorCode_StorageNotFound , //!< Storage has not been found
oC_ErrorCode_FreeSliceNotFound , //!< Free slice has not been found
oC_ErrorCode_LevelNotCorrect , //!< Level not correct
oC_ErrorCode_LocationNotFilled , //!< Location not filled
oC_ErrorCode_UnknownInterruptSource , //!< Unknown interrupt source
oC_ErrorCode_PinFunctionNotCorrect, //!< Pin Function is not correct
oC_ErrorCode_ResponseTypeNotCorrect, //!< Response type is not correct
oC_ErrorCode_ResponseTypeNotSupported, //!< Response type is not supported
oC_ErrorCode_CommandNotSent, //!< Command has not been sent
oC_ErrorCode_ResponseNotReady, //!< Response is not ready yet
oC_ErrorCode_ResponseNotReceived, //!< Response has been not received yet
oC_ErrorCode_DataPackageNotInitialized, //!< Data package is not initialized
oC_ErrorCode_DataPackageIsEmpty, //!< Data package is empty
oC_ErrorCode_DataPackageIsFull, //!< Data package is full
oC_ErrorCode_DataPackageIsReadOnly, //!< Data package is read only
oC_ErrorCode_CommandTransmissionInProgress, //!< Previous command transmission has not finished yet
oC_ErrorCode_SizeNotSupported, //!< Size not supported
oC_ErrorCode_WideBusNotCorrect, //!< Wide bus is not correct
oC_ErrorCode_WideBusNotSupported, //!< Wide bus is not supported
oC_ErrorCode_TooManyOpenFiles, //!< Too many open files
oC_ErrorCode_CannotAllocateLfnBuffer, //!< LFN working buffer could not be allocated
oC_ErrorCode_FileIsLocked, //!< The operation is rejected according to the file sharing policy
oC_ErrorCode_NotSupportedByFileSystem, //!< Not supported by the file system
oC_ErrorCode_SdmmcModeInterfaceNotFound,//!< SDMMC mode interface not found
oC_ErrorCode_SdmmcConfigNotSupported,//!< Given SDMMC configuration is not supported
oC_ErrorCode_CannotSetAddress,//!< The given address cannot be set
oC_ErrorCode_InvalidSourceAddress,  //!< The given source address is not valid
oC_ErrorCode_InvalidInitialization  //!< The given initialization is not correct
oC_ErrorCode_ResponseNotCorrect,    //!< The given response is not correct
oC_ErrorCode_ResponseNotSupported,  //!< The given response is not supported
oC_ErrorCode_ClassNotCorrect,       //!< The given class is not correct
oC_ErrorCode_ClassNotSupported,       //!< The given class is not supported
oC_ErrorCode_InterfaceNotCorrect,       //!< Interface is not correct
oC_ErrorCode_OutOfRange,       //!< Value out of range
oC_ErrorCode_InvalidBlockLength,       //!< Invalid block length
oC_ErrorCode_EraseParameterNotCorrect,       //!< Erase parameter not correct
oC_ErrorCode_CardIsLocked,       //!< Card is locked
oC_ErrorCode_LockUnlockFailed,       //!< Lock/Unlock operation failed
oC_ErrorCode_CommunicationCrcError,       //!< Communication CRC error
oC_ErrorCode_IllegalCommand,       //!< Illegal Command
oC_ErrorCode_CardEccFailed,       //!< Card ECC Failed
oC_ErrorCode_UnknownCardError,       //!< Unknown Card Error
oC_ErrorCode_NotEfficientEnough,       //!< The object is not efficient enough
oC_ErrorCode_CIDCSDOverwrite,       //!< CID/CSD Overwrite
oC_ErrorCode_ApplicationCommandNotConfirmed,       //!< Application Command not applied
oC_ErrorCode_CommandArgumentCannotBeRead,           //!< Command argument cannot be read
oC_ErrorCode_TransferModeNotCorrect,           //!< Transfer mode is not correct
oC_ErrorCode_TransferModeNotSupported,           //!< Transfer mode is not supported
oC_ErrorCode_ResponseIndexNotCorrect,           //!< Response index is not correct
oC_ErrorCode_ResponseNotCorrect,                //!< Response is not correct
oC_ErrorCode_InterfaceNotSupported,             //!< Interface is not supported
oC_ErrorCode_InterfaceNotDetected,             //!< Interface is not detected
oC_ErrorCode_DetectionModeNotCorrect,          //!< Detection mode is not correct
oC_ErrorCode_VersionNotCorrect,                 //!< The given version is not correct
oC_ErrorCode_VersionNotSupported,              //!< The given version is not supported
oC_ErrorCode_EntryPointNotValid,                //!< The given entry point is not valid
oC_ErrorCode_GotSectionIsMissing,               //!< .got section is missing
oC_ErrorCode_NoProgramHeaderFound,              //!< Program header has not been found
} oC_ErrorCode_t;

#endif

#undef  _________________________________________ERROR_CODES________________________________________________________________________________



#endif /* SYSTEM_LIBRARIES_INC_OC_LIBRARIES_H_ */
