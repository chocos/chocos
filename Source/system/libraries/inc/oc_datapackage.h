/** ****************************************************************************************************************************************
 *
 * @file       oc_datapackage.h
 *
 * @brief      interface for DataPackage library
 *
 * @author     Patryk Kubiak - (Created on: 11 09 2017 18:47:09)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup DataPackage DataPackage - Data Package Library
 * @ingroup LibrariesSpace
 * @brief helper library for handling package with data
 *
 * There is a lot of functions in the system that handles read/write operations on buffers. Very often it is required to pass the
 * buffer size and address to the function and also request about number of bytes that has been read/written. The library #DataPackege is
 * a simple set of functions that makes accessing these data easier.
 *
 * To use the library, please create instance of the #oC_DataPackage_t structure and initialize it by using function #oC_DataPackage_Initialize.
 * When the structure is initialized, it can be accessed by the other functions such as #oC_DataPackage_GetDataU8 or #oC_DataPackage_IsEmpty.
 * Here you have some example:
 * @code{.c}

void WriteData( const void * Buffer, oC_MemorySize_t * Size )
{
   oC_DataPackage_t dataPackage;

   // We give here pointer to the dataPackage,
   // pointer to the buffer with data,
   // size of the buffer,
   // number of bytes of data in the buffer
   // And flag set to true if the buffer is for reading only
   oC_DataPackage_Initialize( &dataPackage, Buffer, *Size, *Size, true );

   // We read data from the buffer as long as it is no empty and we can send the byte
   while(  !oC_DataPackage_IsEmpty( &dataPackage )
        && SendByte( oC_DataPackage_GetDataU8( &dataPackage ) )
           );

   // At the end we can set the number of sent data in the `Size` function parameter
   *Size -= oC_DataPackage_GetLeftSize( &dataPackage );
}
   @endcode
 *
 ******************************************************************************************************************************************/


#ifndef OC_DATAPACKAGE_H_
#define OC_DATAPACKAGE_H_

#include <stdint.h>

#include "oc_memory.h"
#include "stdbool.h"

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_____________________________________________________________________________________
//! @addtogroup DataPackage
//! @{

//==========================================================================================================================================
/**
 * @brief Magic number for the structure validation
 */
//==========================================================================================================================================
#define oC_DATA_PACKAGE_MAGIC_NUMBER        0xBEEF0123

#undef  _________________________________________MACROS_____________________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________
//! @addtogroup DataPackage
//! @{

//==========================================================================================================================================
/**
 * @brief stores variables helpful in data managing
 *
 * The type is for storing variables required for data package managing. It should be initialized at the start by the function #oC_DataPackage_Initialize
 */
//==========================================================================================================================================
typedef struct
{
    uint32_t        MagicNumber;            //!< Magic number for the structure validation. It should be set to #oC_DATA_PACKAGE_MAGIC_NUMBER
    union
    {
        void *             WriteBuffer;     //!< Destination pointer to a buffer for data
        uint8_t *          WriteArrayU8;    //!< Destination pointer to an array of #uint8_t data
        uint16_t *         WriteArrayU16;   //!< Destination pointer to an array of #uint16_t data
        uint32_t *         WriteArrayU32;   //!< Destination pointer to an array of #uint32_t data
        uint64_t *         WriteArrayU64;   //!< Destination pointer to an array of #uint64_t data
        const void *       ReadBuffer;      //!< Pointer to the buffer, that can be used for reading data
        const uint8_t *    ReadArrayU8;     //!< Pointer to an array of #uint8_t  data for reading
        const uint16_t *   ReadArrayU16;    //!< Pointer to an array of #uint16_t data for reading
        const uint32_t *   ReadArrayU32;    //!< Pointer to an array of #uint32_t data for reading
        const uint64_t *   ReadArrayU64;    //!< Pointer to an array of #uint64_t data for reading
    };
    bool            ReadOnly;              //!< Flag set if the buffer is read only
    oC_MemorySize_t BufferSize;            //!< Size of the buffer in bytes
    oC_MemorySize_t DataLength;            //!< Number of data bytes inside the buffer
    oC_MemorySize_t PutOffset;             //!< Offset in the buffer where data has started
    oC_MemorySize_t GetOffset;             //!< Offset in the buffer where data should be get from
} oC_DataPackage_t;

#undef  _________________________________________TYPES______________________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_________________________________________________________________________________
//! @addtogroup DataPackage
//! @{

extern void             oC_DataPackage_Initialize       ( oC_DataPackage_t * DataPackage, const void * Buffer, oC_MemorySize_t Size, oC_MemorySize_t DataLength , bool ReadOnly );
extern bool             oC_DataPackage_IsFull           ( const oC_DataPackage_t * DataPackage );
extern bool             oC_DataPackage_IsEmpty          ( const oC_DataPackage_t * DataPackage );
extern bool             oC_DataPackage_PutDataU8        ( oC_DataPackage_t * DataPackage , uint8_t Data );
extern bool             oC_DataPackage_PutDataU16       ( oC_DataPackage_t * DataPackage , uint16_t Data );
extern bool             oC_DataPackage_PutDataU32       ( oC_DataPackage_t * DataPackage , uint32_t Data );
extern bool             oC_DataPackage_PutDataU64       ( oC_DataPackage_t * DataPackage , uint64_t Data );
extern uint8_t          oC_DataPackage_GetDataU8        ( oC_DataPackage_t * DataPackage );
extern uint16_t         oC_DataPackage_GetDataU16       ( oC_DataPackage_t * DataPackage );
extern uint32_t         oC_DataPackage_GetDataU32       ( oC_DataPackage_t * DataPackage );
extern uint64_t         oC_DataPackage_GetDataU64       ( oC_DataPackage_t * DataPackage );
extern oC_MemorySize_t  oC_DataPackage_GetLeftSize      ( const oC_DataPackage_t * DataPackage );
extern void             oC_DataPackage_Clear            ( oC_DataPackage_t * DataPackage );

#undef  _________________________________________PROTOTYPES_________________________________________________________________________________
//! @}

#endif /* OC_DATAPACKAGE_H_ */
