/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the color library
 *
 * @file       oc_color.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Color Color
 * @ingroup LibrariesSpace
 * @brief The module for handling colors
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_COLOR_H_
#define SYSTEM_LIBRARIES_INC_OC_COLOR_H_

#include <stdint.h>
#include <oc_bits.h>
#include <oc_stdtypes.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores colors in any format
 *
 * The type is for storing colors in any #oC_ColorFormat_t
 */
//==========================================================================================================================================
typedef enum
{
    oC_Color_White      = 0xffffff ,   //!< Definition of the color White
    oC_Color_Black      = 0x000000 ,   //!< Definition of the color Black
    oC_Color_Red        = 0xff0000 ,   //!< Definition of the color Red
    oC_Color_Green      = 0x00ff00 ,   //!< Definition of the color Green
    oC_Color_Blue       = 0x0000ff ,   //!< Definition of the color Blue
    oC_Color_Yellow     = 0xffff00 ,   //!< Definition of the color Yellow
    oC_Color_Purple     = 0xff00ff ,   //!< Definition of the color Purple
} oC_Color_t;

//==========================================================================================================================================
/**
 * @brief stores color format (RGB888/RGB565,etc)
 *
 * The type is for storing color format. It is information about number of bits used for color components
 */
//==========================================================================================================================================
typedef enum
{
    oC_ColorFormat_Unknown  = 0 ,                                                //!< The format is unknown
    oC_ColorFormat_ARGB8888 = ( 8 << 12 ) | ( 8 << 8 ) | ( 8 << 4 ) | ( 8 << 0 ),//!< ARGB8888 - 4 bytes color format, with 3 bytes for each color component and 1 byte for opacity
    oC_ColorFormat_RGB888   = ( 0 << 12 ) | ( 8 << 8 ) | ( 8 << 4 ) | ( 8 << 0 ),//!< RGB888   - 3 bytes color format, with 3 bytes for each color component
    oC_ColorFormat_RGB565   = ( 0 << 12 ) | ( 5 << 8 ) | ( 6 << 4 ) | ( 5 << 0 ),//!< RGB565   - 2 bytes color format, with 5 bits for red, 6 bits for green and 5 bits for blue
} oC_ColorFormat_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define oC_ColorFormat_IsCorrect(ColorFormat)           ( (ColorFormat) != 0 )
#define oC_ColorFormat_GetNoAlphaBits(ColorFormat)      ( ((ColorFormat) & 0xF000) >> 12 )
#define oC_ColorFormat_GetNoRedBits(ColorFormat)        ( ((ColorFormat) & 0x0F00) >>  8 )
#define oC_ColorFormat_GetNoGreenBits(ColorFormat)      ( ((ColorFormat) & 0x00F0) >>  4 )
#define oC_ColorFormat_GetNoBlueBits(ColorFormat)       ( ((ColorFormat) & 0x000F) >>  0 )
#define oC_Color_IsCorrect(Color)               ( (Color) <= 0xFFFFFFFF && (Color) >= 0 )
#define oC_Color_FormatSize(ColorFormat)       ((oC_ColorFormat_GetNoAlphaBits(ColorFormat) + \
                                                 oC_ColorFormat_GetNoRedBits(ColorFormat)   + \
                                                 oC_ColorFormat_GetNoGreenBits(ColorFormat) + \
                                                 oC_ColorFormat_GetNoBlueBits(ColorFormat)) / 8 )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with static inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________

static inline oC_UInt_t oC_Color_GetFormatSize( oC_ColorFormat_t ColorFormat )
{
    return (oC_Color_FormatSize(ColorFormat));
}

static inline oC_Color_t oC_Color_ConvertToFormat( oC_Color_t Color , oC_ColorFormat_t FormatIn , oC_ColorFormat_t FormatOut )
{
    oC_Color_t  convertedColor             = 0;

    if(FormatIn != FormatOut)
    {
        uint32_t    formatMask                 = 0xF;
        uint8_t     componentStartBitIndexIn   = 0;
        uint8_t     componentStartBitIndexOut  = 0;

        for(uint8_t colorComponentIndex = 0;colorComponentIndex<4;colorComponentIndex++)
        {
            uint8_t  formatStartBitIndex    = colorComponentIndex * 4;
            uint8_t  bitsIn                 = (uint8_t)((FormatIn  & formatMask) >> formatStartBitIndex);
            uint8_t  bitsOut                = (uint8_t)((FormatOut & formatMask) >> formatStartBitIndex);
            uint8_t  componentEndBitIndexIn = componentStartBitIndexIn  + bitsIn  - 1;
            uint8_t  componentEndBitIndexOut= componentStartBitIndexOut + bitsOut - 1;
            uint32_t componentIn            = (bitsIn  > 0) ? oC_Bits_GetValueU32(Color, componentStartBitIndexIn , componentEndBitIndexIn) : 0;
            uint32_t componentOut           = (bitsOut > 0) ? oC_Bits_GetValueU32(componentIn , 0 , bitsOut - 1) : 0;

            oC_Bits_SetValueU32((uint32_t*)(&convertedColor),componentOut,componentStartBitIndexOut,componentEndBitIndexOut);

            componentStartBitIndexIn  = (bitsIn  > 0) ? componentEndBitIndexIn  + 1 : componentStartBitIndexIn;
            componentStartBitIndexOut = (bitsOut > 0) ? componentEndBitIndexOut + 1 : componentStartBitIndexOut;

            formatMask = formatMask << 4;
        }
    }
    else
    {
        convertedColor = Color;
    }

    return convertedColor;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#endif /* SYSTEM_LIBRARIES_INC_OC_COLOR_H_ */
