/** ****************************************************************************************************************************************
 *
 * @file          oc_endianess.h
 *
 * @brief       The file with functions for the endianess operation
 *
 * @author     Patryk Kubiak - (Created on: Sep 26, 2022 2:33:32 PM) 
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Endianess Endianess - Endianess Library
 * @ingroup LibrariesSpace
 * @brief The library for handling endianess
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_ENDIANESS_H_
#define SYSTEM_LIBRARIES_INC_OC_ENDIANESS_H_

//==========================================================================================================================================
/**
 * @brief swap bytes in word for endianess
 * @param v         value to swap
 * @return bytes with swapped endianess
 */
//==========================================================================================================================================
static inline uint16_t oC_Endianess_SwapU16( uint16_t v )
{
    return ((v & 0xff) << 8) | ((v & 0xff00) >> 8);
}

//==========================================================================================================================================
/**
 * @brief swap bytes in u32 for endianess
 * @param v         value to swap
 * @return bytes with swapped endianess
 */
//==========================================================================================================================================
static inline uint32_t oC_Endianess_SwapU32( uint32_t v )
{
    return ((((uint32_t)oC_Endianess_SwapU16((uint16_t)(v & 0xffff))) ) << 16)| ((((uint32_t)oC_Endianess_SwapU16((uint16_t)((v & 0xffff0000) >> 16)) )) );
}

//==========================================================================================================================================
/**
 * @brief swap bytes in u64 for endianess
 * @param v         value to swap
 * @return bytes with swapped endianess
 */
//==========================================================================================================================================
static inline uint64_t oC_Endianess_SwapU64( uint64_t v )
{
    return (((uint64_t)oC_Endianess_SwapU32((uint32_t)(v & 0xffffffff)) ) << 32) | (((uint64_t)oC_Endianess_SwapU32((uint32_t)(v & 0xffffffff00000000 >> 32)) ) );
}


#endif /* SYSTEM_LIBRARIES_INC_OC_ENDIANESS_H_ */
