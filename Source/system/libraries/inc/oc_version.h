/** ****************************************************************************************************************************************
 *
 * @file       oc_version.h
 *
 * @brief      The file with version handling
 *
 * @author     Patryk Kubiak - (Created on: 27 05 2015 20:04:28)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Version Version - The version library
 * @ingroup LibrariesSpace
 * @brief The library for managing system version
 * 
 ******************************************************************************************************************************************/


#ifndef INC_OC_VERSION_H_
#define INC_OC_VERSION_H_

#include <oc_compiler.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Version
//! @{

#define oC_Version_Make( MAJOR , YEAR , MONTH , PATCH )         ( (PATCH<<0) | (MONTH<<8) | (YEAR<<16) | (MAJOR<<24) )

#define oC_VERSION                                              oC_Version_Make(oC_BUILD_MAJOR,oC_BUILD_YEAR,oC_BUILD_MONTH,oC_BUILD_PATCH)

#define oC_VERSION_NAME                                         oC_BUILD_NAME

#define oC_Version_IsNewerThanOther( OtherVersion )             (OtherVersion) < oC_VERSION
#define oC_Version_IsNewerThan( MAJOR , YEAR , MONTH , PATCH )  oC_Version_IsNewerThanOther( oC_Version_Make(MAJOR,YEAR,MONTH,PATCH) )
#define oC_Version_IsEqualToOther( OtherVersion )               (OtherVersion) == oC_VERSION
#define oC_Version_Is( MAJOR , YEAR , MONTH , PATCH )           oC_Version_IsEqualToOther( oC_Version_Make(MAJOR,YEAR,MONTH,PATCH) )    `

#define oC_FULL_VERSION_STRING                                  oC_TO_STRING(oC_BUILD_MAJOR) "."\
                                                                oC_TO_STRING(oC_BUILD_YEAR)  "."\
                                                                oC_TO_STRING(oC_BUILD_MONTH) "."\
                                                                oC_TO_STRING(oC_BUILD_PATCH) "-"\
                                                                oC_TO_STRING(oC_VERSION_NAME)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

#endif /* INC_OC_VERSION_H_ */
