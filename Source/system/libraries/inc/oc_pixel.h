/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_pixel.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Pixel Pixel - The pixel object
 * @ingroup GUI
 * @brief The pixel object
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_PIXEL_H_
#define SYSTEM_LIBRARIES_INC_OC_PIXEL_H_

#include <oc_stdtypes.h>
#include <oc_color.h>
#include <oc_assert.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Pixel
//! @{

//==========================================================================================================================================
/**
 * @brief unsigned type for storing pixel position
 *
 * The type is designed for storing pixel position in unsigned format.
 */
//==========================================================================================================================================
typedef uint16_t oC_Pixel_ResolutionUInt_t;

//==========================================================================================================================================
/**
 * @brief signed type for storing pixel position
 *
 * The type is designed for storing pixel position in signed format.
 */
//==========================================================================================================================================
typedef int32_t  oC_Pixel_ResolutionInt_t;

//==========================================================================================================================================
/**
 * @brief stores alpha of the pixel
 */
//==========================================================================================================================================
typedef uint8_t oC_Pixel_Alpha_t;

//==========================================================================================================================================
/**
 * @brief stores pixel format
 */
//==========================================================================================================================================
typedef enum
{
    oC_PixelFormat_ARGB8888   ,     //!< ARGB8888 pixel format
    oC_PixelFormat_RGB888     ,     //!< RGB888 pixel format
    oC_PixelFormat_RGB565     ,     //!< RGB565 pixel format
    oC_PixelFormat_ARGB1555   ,     //!< ARGB1555 pixel format
    oC_PixelFormat_ARGB4444   ,     //!< ARGB4444 pixel format
    oC_PixelFormat_L8         ,     //!< L8 pixel format
    oC_PixelFormat_AL44       ,     //!< AL44 pixel format
    oC_PixelFormat_AL88       ,     //!< AL88 pixel format
} oC_PixelFormat_t;

//==========================================================================================================================================
/**
 * @brief stores usage of the pixel
 */
//==========================================================================================================================================
typedef enum
{
    oC_Pixel_Usage_NotUsed ,        //!< pixel is not used
    oC_Pixel_Usage_Used             //!< pixel is used
} oC_Pixel_Usage_t;

//==========================================================================================================================================
/**
 * @brief stores information about the pixel
 */
//==========================================================================================================================================
typedef struct
{
    oC_Color_t          Color;          //!< color of the pixel
    oC_Pixel_Alpha_t    Alpha;          //!< alpha of the pixel
    oC_Pixel_Usage_t    Usage;          //!< usage of the pixel
} oC_Pixel_t;

//==========================================================================================================================================
/**
 * @brief stores information about the pixel
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pixel_ResolutionUInt_t   X;      //!< X position
    oC_Pixel_ResolutionUInt_t   Y;      //!< Y position
} oC_Pixel_Position_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}


#endif /* SYSTEM_LIBRARIES_INC_OC_PIXEL_H_ */
