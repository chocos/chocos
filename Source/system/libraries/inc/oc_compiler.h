/** ****************************************************************************************************************************************
 *
 * @file       oc_compiler.h
 *
 * @brief      The file contains definitions for the compiler, that helps to manage errors, etc.
 *
 * @author     Patryk Kubiak - (Created on: 31 mar 2015 19:30:07) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_OC_COMPILER_H_
#define SYSTEM_CORE_OC_COMPILER_H_

#include <oc_assert.h>


#ifdef oC_USER_SPACE
#   define POINTER          __attribute__((__section__(".got")))
#else
#   define POINTER
#endif
#define oC_DO_PRAGMA( x )                   _Pragma(#x)
#define oC_COMPILER_ERROR( MESSAGE )        oC_DO_PRAGMA( GCC error #MESSAGE )
#define oC_COMPILER_WARNING( MESSAGE )      oC_DO_PRAGMA( GCC warning #MESSAGE )
#define oC_COMPILER_NOTE( MESSAGE )         oC_DO_PRAGMA( GCC message #MESSAGE )
#define oC_WEAK_DEFAULT( ALIAS , SYMBOL )   oC_DO_PRAGMA( weak ALIAS = SYMBOL )
#define oC_WEAK( SYMBOL )                   oC_DO_PRAGMA( weak SYMBOL )
#define _oC_TO_STRING(DEF)                  #DEF
#define oC_TO_STRING( DEFINITION )          _oC_TO_STRING(DEFINITION)
#define oC_IGNORE_MISSED_BREAK              oC_DO_PRAGMA( GCC diagnostic ignored "-Wunused-variable")

#ifdef __GNUC__
#   define oC_FILE          __FILE__
#   define oC_FUNCTION      __FUNCTION__
#   define oC_LINE          __LINE__
#else
#   define oC_FILE          "unknown-compiler"
#   define oC_FUNCTION      "unknown-compiler"
#   define oC_LINE          0
#endif

#define PACKED                              __attribute__((packed))
#define ALIGNED(x)                          __attribute__((aligned(x)))

# define Unused(x)                          UNUSED_ ## x __attribute__((unused))
#define MASK_AS_USED( X )                   (void)X

#define oC_FUNCTION_REDEFINITION( NEW_FUNCTION_NAME , OLD_FUNCTION_NAME )   oC_WEAK_DEFAULT( NEW_FUNCTION_NAME , OLD_FUNCTION_NAME )

#define oC_FUNCTION_ONLY_FOR_STATIC_BUFFER( Buffer , FunctionName , Replacement )              \
           ;oC_STATIC_ASSERT( sizeof(Buffer) != sizeof(void*) , "The function " #FunctionName " is designed only for static buffers! You cannot use it with pointers. Please use " #Replacement " instead!" )

#define oC_AUTO_TYPE(Source)                __typeof__(Source)

#endif /* SYSTEM_CORE_OC_COMPILER_H_ */
