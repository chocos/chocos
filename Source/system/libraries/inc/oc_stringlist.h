/** ****************************************************************************************************************************************
 *
 * @file       oc_stringlist.h
 *
 * @brief      With interface for string list library
 *
 * @author     Patryk Kubiak - (Created on: 31 05 2015 13:30:22)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup StringList StringList
 * @ingroup LibrariesSpace
 * @brief The module for handling string lists
 * 
 ******************************************************************************************************************************************/


#ifndef INC_OC_STRINGLIST_H_
#define INC_OC_STRINGLIST_H_

#include <oc_errors.h>
#include <oc_list.h>
#include <oc_string.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup StringList
//! @{

typedef oC_List(oC_String_t) oC_StringList_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup StringList
//! @{

extern oC_StringList_t          oC_StringList_New       ( void );
extern oC_ErrorCode_t           oC_StringList_Delete    ( oC_StringList_t * StringList );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_OC_STRINGLIST_H_ */
