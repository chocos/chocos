/** ****************************************************************************************************************************************
 *
 * @brief      Program types
 * 
 * @file          oc_program_types.h
 *
 * @author     Patryk Kubiak - (Created on: 08.07.2017 16:38:07) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_PROGRAM_TYPES_H_
#define SYSTEM_LIBRARIES_INC_OC_PROGRAM_TYPES_H_

#include <oc_stdtypes.h>
#include <oc_version.h>

#define PROGRAM_HEADER_MAGIC_STRING_SIZE      8
#define PROGRAM_HEADER_MAGIC_STRING           "ChocoOS"
#define PROGRAM_NAME_STRING_SIZE              30
#define SYSTEM_CALL_NAME_STRING_SIZE          50
#define MACHINE_NAME_STRING_SIZE              20
#define FILE_TYPE_STRING_SIZE                 4
#define FILE_TYPE_CBIN                        "cbin"
#define FILE_TYPE_ELF                         "elf"

typedef int (*oC_Program_MainFunction_t)( int Argc , const char ** Argv );

typedef char oC_Program_Name_t[PROGRAM_NAME_STRING_SIZE];
typedef char oC_SystemCall_Name_t[SYSTEM_CALL_NAME_STRING_SIZE];
typedef char oC_Machine_Name_t[MACHINE_NAME_STRING_SIZE];
typedef char oC_Program_FileType_t[FILE_TYPE_STRING_SIZE];

typedef struct
{
    void *                      CallAddress;
    oC_SystemCall_Name_t        Name;
    char                        NullTerminator;
} oC_SystemCall_t;

typedef struct
{
    uint32_t                    Priority;
    oC_Program_Name_t           Name;
    oC_Program_MainFunction_t   MainFunction;
    char *                      StdInName;
    char *                      StdOutName;
    char *                      StdErrName;
    uint32_t                    HeapMapSize;
    int32_t                     ThreadStackSize;
    uint32_t                    AllocationLimit;
    bool                        TrackAllocations;
    char *                      ArgumentsFormatFilePath;
} oC_Program_Registration_t;

typedef struct
{
    struct
    {
        uint32_t            Offset;
        uint32_t            Size;
    } text , data , bss , syscalls, got;
    uint32_t        BinSize;
} oC_Program_Sections_t;

typedef struct
{
    uint32_t                        HeaderSize;
    uint32_t                        SectionsDefinitionSize;
    char                            MagicString[PROGRAM_HEADER_MAGIC_STRING_SIZE];
    uint32_t                        Version;
    oC_Machine_Name_t               MachineName;
    const oC_SystemCall_t *         SystemCalls;
    oC_Program_FileType_t           FileType;
    uint32_t                        NumberOfSystemCalls;
    uint32_t                        SystemCallDefinitionSize;
    oC_Program_Registration_t       ProgramRegistration;
} oC_Program_Header_t;

#endif /* SYSTEM_LIBRARIES_INC_OC_PROGRAM_TYPES_H_ */
