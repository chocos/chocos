/** ****************************************************************************************************************************************
 *
 * @file       oc_bits.h
 *
 * @brief      The file with functions for the bits operation
 *
 * @author     Krzysztof Dworzynski - (Created on: 22 03 2015 21:34:57)
 *
 * @note       Copyright (C) 2015 Krzysztof Dworzynski <dworniok@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Bits (Bits) The library for the bits operation
 * @ingroup LibrariesSpace
 * @{
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_BITS_OC_BITS_H_
#define SYSTEM_LIBRARIES_BITS_OC_BITS_H_

#include <stdint.h>
#include <stdbool.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * This definition create uint8_t type mask.
 *
 * @param FROM			Number of bit when the mask will be started.
 * @param TO			Number of bit when the mask will be finished.
 *
 */
//==========================================================================================================================================
#define oC_Bits_Mask_U8( FROM , TO )   ((((((uint8_t)1)<<(TO)) - 1 ) | (((uint8_t)1)<<(TO))) ^ ((((uint8_t)1)<<(FROM)) - 1))

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * This definition create uint16_t type mask.
 *
 * @param FROM			Number of bit when the mask will be started.
 * @param TO			Number of bit when the mask will be finished.
 *
 */
//==========================================================================================================================================
#define oC_Bits_Mask_U16( FROM , TO )   ((((((uint16_t)1)<<(TO)) - 1 ) | (((uint16_t)1)<<(TO))) ^ ((((uint16_t)1)<<(FROM)) - 1))

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * This definition create uint32_t type mask.
 *
 * @param FROM			Number of bit when the mask will be started.
 * @param TO			Number of bit when the mask will be finished.
 *
 */
//==========================================================================================================================================
#define oC_Bits_Mask_U32( FROM , TO )   ((((((uint32_t)1)<<(TO)) - 1 ) | (((uint32_t)1)<<(TO))) ^ ((((uint32_t)1)<<(FROM)) - 1))

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * This definition create uint64_t type mask.
 *
 * @param FROM			Number of bit when the mask will be started.
 * @param TO			Number of bit when the mask will be finished.
 *
 */
//==========================================================================================================================================
#define oC_Bits_Mask_U64( FROM , TO )   ((((((uint64_t)1)<<(TO)) - 1 ) | (((uint64_t)1)<<(TO))) ^ ((((uint64_t)1)<<(FROM)) - 1))

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * The definition creates value at the given index with mask.
 *
 * For example:
 * @code
   uint16_t value = oC_Bits_ValueWithMask( 0xAA , 8 , 0xFF ) | oC_Bits_ValueWithMask( 0xBB , 0 , 0xFF00 ) ;

   // now value = 0xAABB
   @endcode
 *
 * @param VALUE         Value to set
 * @param FROM          Start bit for value
 * @param MASK          Mask of value
 */
//==========================================================================================================================================
#define oC_Bits_ValueWithMask(VALUE,FROM,MASK)      ( ((VALUE) << (FROM)) & (MASK) )

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * The function to set uint8_t type mask on uint8_t type variable.
 *
 * @param *outVariable  Variable in which bits will be set.
 * @param Mask			Mask to be used.
 *
 * @return Variable with sets bits.
 */
//==========================================================================================================================================
static inline uint8_t  oC_Bits_SetBitsU8(  uint8_t  * outVariable , uint8_t  Mask )
{
    *outVariable |= Mask;
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to set uint16_t type mask on uint16_t type variable.
 *
 * @param *outVariable  Variable in which bits will be set.
 * @param Mask			Mask to be used.
 *
 * @return Variable with sets bits.
 *
 */
//==========================================================================================================================================
static inline uint16_t oC_Bits_SetBitsU16( uint16_t * outVariable , uint16_t Mask )
{
    *outVariable |= Mask;
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to set uint32_t type mask on uint32_t type variable.
 *
 * @param *outVariable  Variable in which bits will be set.
 * @param Mask			Mask to be used.
 *
 * @return Variable with sets bits.
 *
 */
//==========================================================================================================================================
static inline uint32_t oC_Bits_SetBitsU32( uint32_t * outVariable , uint32_t Mask )
{
    *outVariable |= Mask;
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to set uint64_t type mask on uint64_t type variable.
 *
 * @param *outVariable  Variable in which bits will be set.
 * @param Mask			Mask to be used.
 *
 * @return Variable with sets bits.
 *
 */
//==========================================================================================================================================
static inline uint64_t oC_Bits_SetBitsU64( uint64_t * outVariable , uint64_t Mask )
{
    *outVariable |= Mask;
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to clear uint8_t type mask on uint8_t type variable.
 *
 * @param *outVariable  Variable in which bits will be clear.
 * @param Mask			Mask to be used.
 *
 * @return Variable with clears bits.
 *
 */
//==========================================================================================================================================
static inline uint8_t  oC_Bits_ClearBitsU8(  uint8_t  * outVariable , uint8_t  Mask )
{
    *outVariable &= ( ~Mask );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to clear uint16_t type mask on uint16_t type variable.
 *
 * @param *outVariable  Variable in which bits will be clear.
 * @param Mask			Mask to be used.
 *
 * @return Variable with clears bits.
 *
 */
//==========================================================================================================================================
static inline uint16_t oC_Bits_ClearBitsU16( uint16_t * outVariable , uint16_t Mask )
{
    *outVariable &= ( ~Mask );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to clear uint32_t type mask on uint32_t type variable.
 *
 * @param *outVariable  Variable in which bits will be clear.
 * @param Mask			Mask to be used.
 *
 * @return Variable with clears bits.
 *
 */
//==========================================================================================================================================
static inline uint32_t oC_Bits_ClearBitsU32( uint32_t * outVariable , uint32_t Mask )
{
    *outVariable &= ( ~Mask );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to clear uint64_t type mask on uint64_t type variable.
 *
 * @param *outVariable  Variable in which bits will be clear.
 * @param Mask			Mask to be used.
 *
 * @return Variable with clears bits.
 *
 */
//==========================================================================================================================================
static inline uint64_t oC_Bits_ClearBitsU64( uint64_t * outVariable , uint64_t Mask )
{
    *outVariable &= ( ~Mask );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to set  with shift uint8_t type value into uint8_t type variable.
 *
 * @param *outVariable  Variable in which value will be set.
 * @param Value			Value to set.
 * @param StartBitNr	Number of bit when the value will be started.
 * @param EndBitNr		Number of bit when the value will be finished.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 7!
 * @warning EndBitNr can not be greater than 7!
 *
 * @return Variable in which value is set.
 *
 */
//==========================================================================================================================================
static inline uint8_t  oC_Bits_SetValueU8(  uint8_t  * outVariable , uint8_t  Value , uint8_t StartBitNr , uint8_t EndBitNr)
{
    *outVariable = ( *outVariable & ( ~oC_Bits_Mask_U8 ( StartBitNr , EndBitNr ))) | ((Value<<StartBitNr) & oC_Bits_Mask_U8 ( StartBitNr , EndBitNr ));
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to set  with shift uint16_t type value into uint16_t type variable.
 *
 * @param *outVariable  Variable in which value will be set.
 * @param Value			Value to set.
 * @param StartBitNr	Number of bit when the value will be started.
 * @param EndBitNr		Number of bit when the value will be finished.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 15!
 * @warning EndBitNr can not be greater than 15!
 *
 * @return Variable in which value is set.
 *
 */
//==========================================================================================================================================
static inline uint16_t oC_Bits_SetValueU16( uint16_t * outVariable , uint16_t Value , uint8_t StartBitNr , uint8_t EndBitNr)
{
    *outVariable = ( *outVariable & ( ~oC_Bits_Mask_U16 ( StartBitNr , EndBitNr ))) | ((Value<<StartBitNr) & oC_Bits_Mask_U16 ( StartBitNr , EndBitNr ));
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to set  with shift uint32_t type value into uint32_t type variable.
 *
 * @param *outVariable  Variable in which value will be set.
 * @param Value			Value to set.
 * @param StartBitNr	Number of bit when the value will be started.
 * @param EndBitNr		Number of bit when the value will be finished.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 31!
 * @warning EndBitNr can not be greater than 31!
 *
 * @return Variable in which value is set.
 *
 */
//==========================================================================================================================================
static inline uint32_t oC_Bits_SetValueU32( uint32_t * outVariable , uint32_t Value , uint8_t StartBitNr , uint8_t EndBitNr)
{
    *outVariable = ( *outVariable & ( ~oC_Bits_Mask_U32 ( StartBitNr , EndBitNr ))) | ((Value<<StartBitNr) & oC_Bits_Mask_U32 ( StartBitNr , EndBitNr ));
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to set  with shift uint64_t type value into uint64_t type variable.
 *
 * @param *outVariable  Variable in which value will be set.
 * @param Value			Value to set.
 * @param StartBitNr	Number of bit when the value will be started.
 * @param EndBitNr		Number of bit when the value will be finished.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 63!
 * @warning EndBitNr can not be greater than 63!
 *
 * @return Variable in which value is set.
 *
 */
//==========================================================================================================================================
static inline uint64_t oC_Bits_SetValueU64( uint64_t * outVariable , uint64_t Value , uint8_t StartBitNr , uint8_t EndBitNr)
{
    *outVariable = ( *outVariable & ( ~oC_Bits_Mask_U64 ( StartBitNr , EndBitNr ))) | ((Value<<StartBitNr) & oC_Bits_Mask_U64 ( StartBitNr , EndBitNr ));
    return *outVariable;
}

//==========================================================================================================================================
/**
 * The function to get value from uint8_t type variable.
 *
 * @param Variable      Variable which stores some value.
 * @param StartBitNr	Number of first bit of stored value.
 * @param EndBitNr		Number of last bit of stored value.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 7!
 * @warning EndBitNr can not be greater than 7!
 *
 * @return Resulting value.
 *
 */
//==========================================================================================================================================
static inline uint8_t oC_Bits_GetValueU8( uint8_t Variable , uint8_t StartBitNr , uint8_t EndBitNr )
{
    return (oC_Bits_Mask_U8 ( StartBitNr , EndBitNr ) & Variable)>>StartBitNr;
}

//==========================================================================================================================================
/**
 * The function to get value from uint16_t type variable.
 *
 * @param Variable      Variable which stores some value.
 * @param StartBitNr	Number of first bit of stored value.
 * @param EndBitNr		Number of last bit of stored value.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 15!
 * @warning EndBitNr can not be greater than 15!
 *
 * @return Resulting value.
 *
 */
//==========================================================================================================================================
static inline uint16_t oC_Bits_GetValueU16( uint16_t Variable , uint8_t StartBitNr , uint8_t EndBitNr )
{
    return (oC_Bits_Mask_U16 ( StartBitNr , EndBitNr ) & Variable)>>StartBitNr;
}

//==========================================================================================================================================
/**
 * The function to get value from uint32_t type variable.
 *
 * @param Variable      Variable which stores some value.
 * @param StartBitNr	Number of first bit of stored value.
 * @param EndBitNr		Number of last bit of stored value.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 31!
 * @warning EndBitNr can not be greater than 31!
 *
 * @return Resulting value.
 *
 */
//==========================================================================================================================================
static inline uint32_t oC_Bits_GetValueU32( uint32_t Variable , uint8_t StartBitNr , uint8_t EndBitNr )
{
    return (oC_Bits_Mask_U32 ( StartBitNr , EndBitNr ) & Variable)>>StartBitNr;
}

//==========================================================================================================================================
/**
 * The function to get value from uint64_t type variable.
 *
 * @param Variable      Variable which stores some value.
 * @param StartBitNr	Number of first bit of stored value.
 * @param EndBitNr		Number of last bit of stored value.
 *
 * @warning StartBitNr can not be greater than EndBit!
 * @warning StartBitNr can not be greater than 63!
 * @warning EndBitNr can not be greater than 63!
 *
 * @return Resulting value.
 *
 */
//==========================================================================================================================================
static inline uint64_t oC_Bits_GetValueU64( uint64_t Variable , uint8_t StartBitNr , uint8_t EndBitNr )
{
    return (oC_Bits_Mask_U64 ( StartBitNr , EndBitNr ) & Variable)>>StartBitNr;
}

//==========================================================================================================================================
/**
 * The function to split uint64_t type variable.
 *
 * @param Variable      Variable which stores some value.
 * @param *outLow		Lower part.
 * @param *outHigh		Higher part.
 *
 */
//==========================================================================================================================================
static inline void oC_Bits_SplitU64ToU32( uint64_t Variable , uint32_t * outLow , uint32_t * outHigh )
{
    *outLow = (uint32_t)Variable;
    *outHigh = (uint32_t)(Variable>>32);
}

//==========================================================================================================================================
/**
 * The function to split uint32_t type variable.
 *
 * @param Variable      Variable which stores some value.
 * @param *outLow		Lower part.
 * @param *outHigh		Higher part.
 *
 */
//==========================================================================================================================================
static inline void oC_Bits_SplitU32ToU16( uint32_t Variable , uint16_t * outLow , uint16_t * outHigh )
{
    *outLow = (uint16_t)Variable;
    *outHigh = (uint16_t)(Variable>>16);
}

//==========================================================================================================================================
/**
 * The function to split uint16_t type variable.
 *
 * @param Variable      Variable which stores some value.
 * @param *outLow		Lower part.
 * @param *outHigh		Higher part.
 *
 */
//==========================================================================================================================================
static inline void oC_Bits_SplitU16ToU8( uint16_t Variable , uint8_t * outLow , uint8_t * outHigh )
{
    *outLow = (uint8_t)Variable;
    *outHigh = (uint8_t)(Variable>>8);
}

//==========================================================================================================================================
/**
 * The function to join two uint8_t type variables.
 *
 * @param LowPart		Lower part.
 * @param HighPart		Higher part.
 *
 * @return				Jointed variable.
 *
 */
//==========================================================================================================================================
static inline uint16_t oC_Bits_JoinU8ToU16( uint8_t LowPart , uint8_t HighPart )
{
    return (((uint16_t)HighPart)<<8)|((uint16_t)LowPart);
}

//==========================================================================================================================================
/**
 * The function to join two uint16_t type variables.
 *
 * @param LowPart		Lower part.
 * @param HighPart		Higher part.
 *
 * @return				Jointed variable.
 *
 */
//==========================================================================================================================================
static inline uint32_t oC_Bits_JoinU16ToU32( uint16_t LowPart , uint16_t HighPart )
{
    return (((uint32_t)HighPart)<<16)|((uint32_t)LowPart);
}

//==========================================================================================================================================
/**
 * The function to join two uint32_t type variables.
 *
 * @param LowPart		Lower part.
 * @param HighPart		Higher part.
 *
 * @return				Jointed variable.
 *
 */
//==========================================================================================================================================
static inline uint64_t oC_Bits_JoinU32ToU64( uint32_t LowPart , uint32_t HighPart )
{
    return (((uint64_t)HighPart)<<32)|((uint64_t)LowPart);
}

//==========================================================================================================================================
/**
 * The function return number of first bit which is set in uint8_t type Mask.
 *
 * @param BitMask		Mask to be check.
 *
 * @return				Number of first sets bit (-1 when there is no set bits).
 *
 */
//==========================================================================================================================================
static inline int8_t oC_Bits_GetBitNumberU8( uint8_t BitMask )
{
    int8_t i;
    int8_t ret = -1;
    for (i = 0; i < 8; i++)
    {
        if ((BitMask & 0x01) == 0x01)
        {
            ret = i;
            break;
        }
        else
        {
            BitMask = (BitMask >> 1);
        }
    }
    return ret;
}

//==========================================================================================================================================
/**
 * The function return number of first bit which is set in uint16_t type Mask.
 *
 * @param BitMask		Mask to be check.
 *
 * @return				Number of first sets bit (-1 when there is no set bits).
 *
 */
//==========================================================================================================================================
static inline int8_t oC_Bits_GetBitNumberU16( uint16_t BitMask )
{
    int8_t i;
    int8_t ret = -1;
    for (i = 0; i < 16; i++)
    {
        if ((BitMask & 0x0001) == 0x0001)
        {
            ret = i;
            break;
        }
        else
        {
            BitMask = (BitMask >> 1);
        }
    }
    return ret;
}

//==========================================================================================================================================
/**
 * The function return number of first bit which is set in uint32_t type Mask.
 *
 * @param BitMask		Mask to be check.
 *
 * @return				Number of first sets bit (-1 when there is no set bits).
 *
 */
//==========================================================================================================================================
static inline int8_t oC_Bits_GetBitNumberU32( uint32_t BitMask )
{
    int8_t i;
    int8_t ret = -1;
    for (i = 0; i < 32; i++)
    {
        if ((BitMask & 0x00000001) == 0x00000001)
        {
            ret = i;
            break;
        }
        else
        {
            BitMask = (BitMask >> 1);
        }
    }
    return ret;
}

//==========================================================================================================================================
/**
 * The function return number of first bit which is set in uint64_t type Mask.
 *
 * @param BitMask		Mask to be check.
 *
 * @return				Number of first sets bit (-1 when there is no set bits).
 *
 */
//==========================================================================================================================================
static inline int8_t oC_Bits_GetBitNumberU64( uint64_t BitMask )
{
    int8_t i;
    int8_t ret = -1;
    for (i = 0; i < 64; i++)
    {
        if ((BitMask & 0x0000000000000001) == 0x0000000000000001)
        {
            ret = i;
            break;
        }
        else
        {
            BitMask = (BitMask >> 1);
        }
    }
    return ret;
}

//==========================================================================================================================================
/**
 * @brief checks if bit is set
 *
 * The function is checking if the bit is set
 *
 * @param BitMask       Variable with mask of bits
 * @param BitIndex      Index of bit to check
 *
 * @return true if bit is set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsBitSetU8( uint8_t BitMask , uint8_t BitIndex )
{
    return (BitMask & (1<<BitIndex)) == (1<<BitIndex);
}

//==========================================================================================================================================
/**
 * @brief checks if bit is set
 *
 * The function is checking if the bit is set
 *
 * @param BitMask       Variable with mask of bits
 * @param BitIndex      Index of bit to check
 *
 * @return true if bit is set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsBitSetU16( uint16_t BitMask , uint8_t BitIndex )
{
    return (BitMask & ((uint16_t)1<<BitIndex)) == ((uint16_t)1<<BitIndex);
}

//==========================================================================================================================================
/**
 * @brief checks if bit is set
 *
 * The function is checking if the bit is set
 *
 * @param BitMask       Variable with mask of bits
 * @param BitIndex      Index of bit to check
 *
 * @return true if bit is set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsBitSetU32( uint32_t BitMask , uint8_t BitIndex )
{
    return (BitMask & ((uint32_t)1<<BitIndex)) == ((uint32_t)1<<BitIndex);
}

//==========================================================================================================================================
/**
 * @brief checks if bit is set
 *
 * The function is checking if the bit is set
 *
 * @param BitMask       Variable with mask of bits
 * @param BitIndex      Index of bit to check
 *
 * @return true if bit is set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsBitSetU64( uint64_t BitMask , uint8_t BitIndex )
{
    return (BitMask & ((uint64_t)1<<BitIndex)) == ((uint64_t)1<<BitIndex);
}

//==========================================================================================================================================
/**
 * @brief sets bit in the variable
 *
 * @param outVariable   variable to set
 * @param BitIndex      index of bit to set in the variable
 *
 * @return value after changes
 */
//==========================================================================================================================================
static inline uint8_t  oC_Bits_SetBitU8(  uint8_t  * outVariable , uint8_t  BitIndex )
{
    *outVariable |= (uint8_t)1<<BitIndex;
    return *outVariable;
}

//==========================================================================================================================================
/**
 * @brief sets bit in the variable
 *
 * @param outVariable   variable to set
 * @param BitIndex      index of bit to set in the variable
 *
 * @return value after changes
 */
//==========================================================================================================================================
static inline uint16_t oC_Bits_SetBitU16( uint16_t * outVariable , uint8_t BitIndex )
{
    *outVariable |= (uint16_t)1<<BitIndex;
    return *outVariable;
}

//==========================================================================================================================================
/**
 * @brief sets bit in the variable
 *
 * @param outVariable   variable to set
 * @param BitIndex      index of bit to set in the variable
 *
 * @return value after changes
 */
//==========================================================================================================================================
static inline uint32_t oC_Bits_SetBitU32( uint32_t * outVariable , uint8_t BitIndex )
{
    *outVariable |= (uint32_t)1<<BitIndex;
    return *outVariable;
}

//==========================================================================================================================================
/**
 * @brief sets bit in the variable
 *
 * @param outVariable   variable to set
 * @param BitIndex      index of bit to set in the variable
 *
 * @return value after changes
 */
//==========================================================================================================================================
static inline uint64_t oC_Bits_SetBitU64( uint64_t * outVariable , uint8_t BitIndex )
{
    *outVariable |= (uint64_t)1<<BitIndex;
    return *outVariable;
}


//==========================================================================================================================================
/**
 * @brief clear selected bit
 *
 * The function is for clearing bit at BitIndex position
 *
 * @param outVariable       Variable to clear bit
 * @param BitIndex          Index of the bit to clear
 *
 * @return variable with bit cleared
 */
//==========================================================================================================================================
static inline uint8_t  oC_Bits_ClearBitU8(  uint8_t  * outVariable , uint8_t  BitIndex )
{
    *outVariable &= ~((uint8_t)1<<BitIndex );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * @brief clear selected bit
 *
 * The function is for clearing bit at BitIndex position
 *
 * @param outVariable       Variable to clear bit
 * @param BitIndex          Index of the bit to clear
 *
 * @return variable with bit cleared
 */
//==========================================================================================================================================
static inline uint16_t oC_Bits_ClearBitU16( uint16_t * outVariable , uint8_t  BitIndex )
{
    *outVariable &= ~((uint16_t)1<<BitIndex );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * @brief clear selected bit
 *
 * The function is for clearing bit at BitIndex position
 *
 * @param outVariable       Variable to clear bit
 * @param BitIndex          Index of the bit to clear
 *
 * @return variable with bit cleared
 */
//==========================================================================================================================================
static inline uint32_t oC_Bits_ClearBitU32( uint32_t * outVariable , uint8_t  BitIndex )
{
    *outVariable &= ~((uint32_t)1<<BitIndex );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * @brief clear selected bit
 *
 * The function is for clearing bit at BitIndex position
 *
 * @param outVariable       Variable to clear bit
 * @param BitIndex          Index of the bit to clear
 *
 * @return variable with bit cleared
 */
//==========================================================================================================================================
static inline uint64_t oC_Bits_ClearBitU64( uint64_t * outVariable , uint8_t  BitIndex )
{
    *outVariable &= ~((uint64_t)1<<BitIndex );
    return *outVariable;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are set
 *
 * The function is for checking if each bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsSetU8( uint8_t BitMask , uint8_t  BitsToCheck )
{
    return (BitMask & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are set
 *
 * The function is for checking if each bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsSetU16( uint16_t BitMask , uint16_t  BitsToCheck )
{
    return (BitMask & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are set
 *
 * The function is for checking if each bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsSetU32( uint32_t BitMask , uint32_t  BitsToCheck )
{
    return (BitMask & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are set
 *
 * The function is for checking if each bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsSetU64( uint64_t BitMask , uint64_t  BitsToCheck )
{
    return (BitMask & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are clear
 *
 * The function is for checking if each bit in the mask is clear
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be clear
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsClearU8( uint8_t BitMask , uint8_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are clear
 *
 * The function is for checking if each bit in the mask is clear
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be clear
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsClearU16( uint16_t BitMask , uint16_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are clear
 *
 * The function is for checking if each bit in the mask is clear
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be clear
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsClearU32( uint32_t BitMask , uint32_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if all bits in field are clear
 *
 * The function is for checking if each bit in the mask is clear
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be clear
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_AreBitsClearU64( uint64_t BitMask , uint64_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck) == BitsToCheck;
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitSetU8( uint8_t BitMask , uint8_t  BitsToCheck )
{
    return (BitMask & BitsToCheck);
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitSetU16( uint16_t BitMask , uint16_t  BitsToCheck )
{
    return (BitMask & BitsToCheck);
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitSetU32( uint32_t BitMask , uint32_t  BitsToCheck )
{
    return (BitMask & BitsToCheck);
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitSetU64( uint64_t BitMask , uint64_t  BitsToCheck )
{
    return (BitMask & BitsToCheck);
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitClearU8( uint8_t BitMask , uint8_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck);
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitClearU16( uint16_t BitMask , uint16_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck);
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitClearU32( uint32_t BitMask , uint32_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck);
}

//==========================================================================================================================================
/**
 * @brief checks if at least one of bits in field is set
 *
 * The function is for checking if at least one bit in the mask is set
 *
 * @param BitMask           Bits mask, that should contain all bits from the BitsToCheck variable
 * @param BitsToCheck       Bits that should be set
 *
 * @return true if all bits are set
 */
//==========================================================================================================================================
static inline bool oC_Bits_IsAtLeastOneBitClearU64( uint64_t BitMask , uint64_t  BitsToCheck )
{
    return ((~BitMask) & BitsToCheck);
}

/** @} */
#endif /* SYSTEM_LIBRARIES_BITS_OC_BITS_H_ */
