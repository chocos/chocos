/*
 * oc_baudrate.h
 *
 *  Created on: 10.08.2016
 *      Author: pkubiak
 */

#ifndef SYSTEM_LIBRARIES_INC_OC_BAUDRATE_H_
#define SYSTEM_LIBRARIES_INC_OC_BAUDRATE_H_

#include <stdint.h>

#define Bd(V)        oC_BaudRate_Bd(V)
#define kBd(V)       oC_BaudRate_kBd(V)
#define MBd(V)       oC_BaudRate_MBd(V)
#define GBd(V)       oC_BaudRate_GBd(V)
#define TBd(V)       oC_BaudRate_TBd(V)

#define oC_BaudRate_Bd( B )         (B)
#define oC_BaudRate_kBd( kBd )      ( (kBd) * oC_BaudRate_Bd(1000) )
#define oC_BaudRate_MBd( MBd )      ( (MBd) * oC_BaudRate_kBd(1000) )
#define oC_BaudRate_GBd( GBd )      ( (GBd) * oC_BaudRate_MBd(1000) )
#define oC_BaudRate_TBd( TBd )      ( (TBd) * oC_BaudRate_GBd(1000) )

typedef uint64_t oC_BaudRate_t;

#endif /* SYSTEM_LIBRARIES_INC_OC_BAUDRATE_H_ */
