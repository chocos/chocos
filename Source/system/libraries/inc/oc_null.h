/** ****************************************************************************************************************************************
 *
 * @file       oc_null.h
 *
 * @brief      Definition of the null pointer
 *
 * @author     Patryk Kubiak - (Created on: 12 kwi 2015 18:59:45) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Null Null - The null pointer
 * @ingroup LibrariesSpace
 * @brief The null pointer
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_NULL_OC_NULL_H_
#define SYSTEM_LIBRARIES_NULL_OC_NULL_H_

#include <stddef.h>

#ifndef NULL
/**
 * @brief pointer to a zero
 */
#define NULL        ((void*)0)
#endif

#ifndef oC_NULL
/**
 * @brief pointer to a zero (NULL redefinition)
 */
#define oC_NULL     NULL
#endif

#define gen_point_value(TYPE,POINTER)      ( *((TYPE*)POINTER) )

#define oC_FROM_POINTER( TYPE , POINTER )     ( *((TYPE*)POINTER) )

#endif /* SYSTEM_LIBRARIES_NULL_OC_NULL_H_ */
