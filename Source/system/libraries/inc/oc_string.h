/** ****************************************************************************************************************************************
 *
 * @file       oc_string.h
 *
 * @brief      The file with interface for string library
 *
 * @author     Patryk Kubiak - (Created on: 31 05 2015 13:26:54)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup String String
 * @ingroup LibrariesSpace
 * @brief The module for handling strings
 * 
 ******************************************************************************************************************************************/


#ifndef INC_OC_STRING_H_
#define INC_OC_STRING_H_

#include <oc_errors.h>
#include <string.h>
#include <ctype.h>

#define oC_DEFAULT_STRING_SIZE      50
#define oC_DEFAULT_PATH_LENGTH      200

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup String
//! @{

typedef char * oC_String_t;

typedef const char * oC_ConstString_t;

typedef char oC_DefaultString_t[oC_DEFAULT_STRING_SIZE];
typedef char oC_DefaultPathString_t[oC_DEFAULT_PATH_LENGTH];

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with static inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @addtogroup String
//! @{

//==========================================================================================================================================
/**
 * @brief works like tolower but casts arguments
 *
 * @param character     Pointer to the object
 *
 * @return lower character
 */
//==========================================================================================================================================
static inline char oC_tolower( int character )
{
    return (char)tolower((unsigned char)character);
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup String
//! @{

extern oC_String_t      oC_String_New       ( uint32_t Length );
extern oC_ErrorCode_t   oC_String_Delete    ( oC_String_t * String );

extern void     memset_u16      ( void * Buffer , uint16_t Value , uint32_t NumberOfElements );
extern void     memset_u32      ( void * Buffer , uint32_t Value , uint32_t NumberOfElements );
extern void     memset_u64      ( void * Buffer , uint64_t Value , uint32_t NumberOfElements );
extern void     string_reverse  ( char * String );
extern void     remove_in_string( char * String , uint32_t Index , uint32_t NumberOfCharacters );
extern uint32_t put_to_string   ( char * String , uint32_t Size , uint32_t Index , char C );
extern uint32_t string_backspace( char * String , uint32_t Index );
extern bool     string_contains ( const char * String , const char * SubString , bool CaseSensitive );
extern bool     string_contains_special_characters( const char * String);

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_OC_STRING_H_ */
