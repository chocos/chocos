/*******************************************************************************************************************************************
 *
 *    @file       stdbool.h
 *
 *    @brief      The file with standard bool definitions
 *
 *    @author     Patryk Kubiak - (Created on: 6 pa� 2014 17:33:15) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef STDBOOL_H_
#define STDBOOL_H_

#define __bool_true_false_are_defined   1

#ifndef __cplusplus

#define FALSE        0
#define TRUE         (!FALSE)
#define true         TRUE
#define false        FALSE

#ifdef bool
#   undef bool
#endif

typedef _Bool bool;

#if __STDC_VERSION__ < 199901L && __GNUC__ < 3
typedef unsigned char _Bool;
#endif

#endif /* !__cplusplus */
#endif /* STDBOOL_H_ */
