/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_memory.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Memory Memory - Memory management
 * @ingroup Drivers
 * @brief Memory - Memory management
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_MEMORY_H_
#define SYSTEM_LIBRARIES_INC_OC_MEMORY_H_

#include <oc_stdtypes.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define oC_Bits(b)               (b / 8 + (((b % 8) != 0) ? 1 : 0) )
#define oC_kiBits(kb)            (oC_Bits(1024ULL) * (kb))
#define oC_MiBits(Mb)            (oC_kiBits(1024ULL)* (Mb))
#define oC_GiBits(Gb)            (oC_MiBits(1024ULL)* (Gb))
#define oC_TiBits(Tb)            (oC_GiBits(1024ULL)* (Tb))

#define oC_Bytes(B)              (B)
#define oC_kiBytes(kB)           (oC_Bytes(1024ULL) * (kB))
#define oC_MiBytes(MB)           (oC_kiBytes(1024ULL)* (MB))
#define oC_GiBytes(GB)           (oC_MiBytes(1024ULL)* (GB))
#define oC_TiBytes(TB)           (oC_GiBytes(1024ULL)* (TB))

#define oC_B(B)                  (B)
#define oC_KB(kB)                (oC_B(1000ULL) * (kB))
#define oC_MB(MB)                (oC_KB(1000ULL)* (MB))
#define oC_GB(GB)                (oC_MB(1000ULL)* (GB))
#define oC_TB(TB)                (oC_GB(1000ULL)* (TB))

#define oC_MemorySize_b(V)       oC_Bits(V)
#define oC_MemorySize_kib(V)     oC_kiBits(V)
#define oC_MemorySize_Mib(V)     oC_MiBits(V)
#define oC_MemorySize_Gib(V)     oC_GiBits(V)
#define oC_MemorySize_Tib(V)     oC_TiBits(V)

#define oC_MemorySize_B(V)       oC_Bytes(V)
#define oC_MemorySize_kiB(V)     oC_kiBytes(V)
#define oC_MemorySize_MiB(V)     oC_MiBytes(V)
#define oC_MemorySize_GiB(V)     oC_GiBytes(V)
#define oC_MemorySize_TiB(V)     oC_TiBytes(V)

#define oC_BitsPerSecond(V)     oC_Bits(V)
#define oC_kBitsPerSecond(V)    oC_kiBits(V)
#define oC_MBitsPerSecond(V)    oC_MiBits(V)
#define oC_GBitsPerSecond(V)    oC_GiBits(V)
#define oC_TBitsPerSecond(V)    oC_TiBits(V)

#define oC_TransferSpeed_bps(V)     oC_BitsPerSecond(V)
#define oC_TransferSpeed_kbps(V)    oC_kBitsPerSecond(V)
#define oC_TransferSpeed_Mbps(V)    oC_MBitsPerSecond(V)
#define oC_TransferSpeed_Gbps(V)    oC_GBitsPerSecond(V)
#define oC_TransferSpeed_Tbps(V)    oC_TBitsPerSecond(V)

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief Macro for receiving name of Access type
 *
 * The macro helps to receive name of an enumerator in oC_Access_t type.
 *
 * @param ACCESS_SHORTCUT       Shortcut of access: R/W/RW
 */
//==========================================================================================================================================
#define oC_Access_(ACCESS_SHORTCUT)                             oC_1WORD_FROM_2(oC_Access_ , ACCESS_SHORTCUT)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

typedef uint32_t oC_SectorNumber_t;
typedef uint32_t oC_DiskId_t;
typedef uint64_t oC_MemorySize_t;
typedef uint64_t oC_MemoryOffset_t;
typedef uint64_t oC_TransferSpeed_t;

//==========================================================================================================================================
/**
 * @brief Type for storing access (R/W/RW)
 *
 * The type for storing access. It is an information if a value is writable or readable. Enumerations in this types are defined as flags,
 * so they can be joined.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Access_None      = 0,                        //!< None access is allowed
    oC_Access_Read      = (1<<0),                   //!< If this flag is set it does mean, that this resource is readable
    oC_Access_Write     = (1<<1),                   //!< If this flag is set it does mean, that this resource is writable
    oC_Access_Execute   = (1<<2),                   //!< If this flag is set it does mean, that this resource is executable
    oC_Access_R     = oC_Access_Read ,              //!< @see oC_Access_Read
    oC_Access_W     = oC_Access_Write ,             //!< @see oC_Access_Write
    oC_Access_X     = oC_Access_Execute,            //!< @see oC_Access_Execute
    oC_Access_RW    = oC_Access_R  | oC_Access_W,   //!< @see oC_Access_Read , oC_Access_Write
    oC_Access_RWX   = oC_Access_RW | oC_Access_X,   //!< @see oC_Access_Read , oC_Access_Write, oC_Access_Execute
    oC_Access_Full  = oC_Access_RW | oC_Access_Execute, //!< All access allowed
} oC_Access_t;

#endif /* SYSTEM_LIBRARIES_INC_OC_MEMORY_H_ */
