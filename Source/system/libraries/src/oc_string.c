/** ****************************************************************************************************************************************
 *
 * @file       oc_string.c
 *
 * @brief      The file with source for the string library
 *
 * @author     Patryk Kubiak - (Created on: 9 10 2015 00:27:38)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define _GNU_SOURCE
#include <oc_string.h>
#include <stdint.h>

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________


void memset_u16( void * Buffer , uint16_t Value , uint32_t NumberOfElements )
{
    uint16_t * buffer = Buffer;

    for(uint32_t offset = 0 ; offset < NumberOfElements; offset++)
    {
        buffer[offset] = Value;
    }
}

void memset_u32( void * Buffer , uint32_t Value , uint32_t NumberOfElements )
{
    uint32_t * buffer = Buffer;

    for(uint32_t offset = 0 ; offset < NumberOfElements; offset++)
    {
        buffer[offset] = Value;
    }
}

void memset_u64( void * Buffer , uint64_t Value , uint32_t NumberOfElements )
{
    uint64_t * buffer = Buffer;

    for(uint32_t offset = 0 ; offset < NumberOfElements; offset++)
    {
        buffer[offset] = Value;
    }
}

void string_reverse( char * String )
{
    uint32_t stringLength = strlen(String);
    uint32_t leftIndex    = 0;
    uint32_t rightIndex   = stringLength - 1;
    uint32_t leftEndIndex = stringLength / 2;
    char     savedSign    = 0;

    while(leftIndex < leftEndIndex)
    {
        savedSign = String[leftIndex];

        String[leftIndex] = String[rightIndex];
        String[rightIndex]   = savedSign;

        rightIndex--;
        leftIndex++;
    }
}

void remove_in_string( char * String , uint32_t Index , uint32_t NumberOfCharacters )
{
    uint32_t length             = strlen(String);
    uint32_t lengthAfterRemove  = length - NumberOfCharacters;

    for(uint32_t i = Index ; i < length ; i++)
    {
        if(i < lengthAfterRemove)
        {
            String[i] = String[i+NumberOfCharacters];
        }
        else
        {
            String[i] = 0;
        }
    }
}

uint32_t put_to_string( char * String , uint32_t Size , uint32_t Index , char C )
{
    uint32_t length         = strlen(String);
    uint32_t lengthAfterPut = length + 1;
    uint32_t maxStringLength= Size - 1;
    if(Size > 1 && Index < maxStringLength)
    {
        if(lengthAfterPut > maxStringLength)
        {
            lengthAfterPut = maxStringLength;
        }
        for(uint32_t i = (lengthAfterPut - 1); i > Index ; i--)
        {
            String[i] = String[i-1];
        }

        String[Index++]        = C;
        String[lengthAfterPut] = 0;
    }
    return Index;
}

uint32_t string_backspace( char * String , uint32_t Index )
{
    uint32_t length = strlen(String);
    if(Index <= length && Index > 0)
    {
        Index--;
        if(Index > 0 && String[Index] == '\r' && String[Index-1] == '\n')
        {
            Index--;
            remove_in_string(String,Index,2);
        }
        else
        {
            remove_in_string(String,Index,1);
        }
    }

    return Index;
}

bool string_contains( const char * String , const char * SubString , bool CaseSensitive )
{
    if(CaseSensitive)
    {
        return strstr(String,SubString) != NULL;
    }
    else
    {
        return strcasestr(String,SubString) != NULL;
    }
}

bool string_contains_special_characters( const char * String)
{
    bool contains = false;

    if(String != NULL)
    {
        uint32_t length = strlen(String);

        for(uint32_t i = 0 ; i < length ; i++)
        {
            if(   (uint8_t)String[i] < (uint8_t)'0' ||
                ( (uint8_t)String[i] > (uint8_t)'9' && (uint8_t)String[i] < (uint8_t)'A' )  ||
                ( (uint8_t)String[i] > (uint8_t)'Z' && (uint8_t)String[i] < (uint8_t)'a' )  ||
                ( (uint8_t)String[i] > (uint8_t)'z')
               )
            {
                contains = true;
                break;
            }
        }
    }

    return contains;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________
