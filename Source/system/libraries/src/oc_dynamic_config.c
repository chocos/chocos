/** ****************************************************************************************************************************************
 *
 * @brief      Functions of dynamic configuration
 *
 * @file       oc_dynamic_config.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_dynamic_config.h>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/*                                  Creating variables to store values of configurations                                                  */
//==========================================================================================================================================
#define ADD( MODULE_NAME, TYPE , VARIABLE_NAME, DEFAULT_VALUE, FRIENDLY_NAME, BRIEF  )              \
                    TYPE DYNAMIC_CONFIG_MAKE_VARIABLE_NAME(MODULE_NAME, VARIABLE_NAME) = DEFAULT_VALUE;
#define DONT_ADD(...)

oC_CONFIGURATIONS_LIST(ADD,DONT_ADD);

#undef ADD
#undef DONT_ADD

//==========================================================================================================================================
/*                                  Creating array with details of configuration variables                                                */
//==========================================================================================================================================
#define ADD( MODULE_NAME, TYPE , VARIABLE_NAME, DEFAULT_VALUE, FRIENDLY_NAME, BRIEF  )              \
                {\
                    .Module         = oC_DynamicConfig_Module_(MODULE_NAME) , \
                    .TypeName       = #TYPE , \
                    .TypeSize       = sizeof(TYPE), \
                    .ValueReference = &DYNAMIC_CONFIG_MAKE_VARIABLE_NAME(MODULE_NAME, VARIABLE_NAME), \
                    .VariableName   = #VARIABLE_NAME, \
                    .FriendlyName   = FRIENDLY_NAME, \
                    .Brief          = BRIEF, \
                } ,

#define DONT_ADD(...)

//==========================================================================================================================================
/**
 * @brief array with definitions of variables details
 */
//==========================================================================================================================================
oC_DynamicConfig_VariableDetails_t oC_DynamicConfig_VariablesDetails[oC_DynamicConfig_VariableId_NumberOfElements] = {
                oC_CONFIGURATIONS_LIST(ADD,DONT_ADD)
};

#undef ADD
#undef DONT_ADD

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

