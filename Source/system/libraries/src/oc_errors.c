/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          oc_errors.c
 *
 *    @brief        __FILE__DESCRIPTION__
 *
 *    @author     Patryk Kubiak - (Created on: 9 gru 2014 12:21:22) 
 *
 *    @note       Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


/*==========================================================================================================================================
//
//     INCLUDES
//
//========================================================================================================================================*/

#include <oc_errors.h>
#include <oc_array.h>
#include <oc_null.h>
#include <stdio.h>
#include <stdint.h>
#include <oc_system_cfg.h>
#include <oc_pixel.h>

/*==========================================================================================================================================
//
//     LOCAL CONSTS
//
//========================================================================================================================================*/


/*==========================================================================================================================================
//
//     LOCAL TYPES
//
//========================================================================================================================================*/

typedef struct
{
    const char *    Function;
    uint32_t        LineNumber;
    const char *    Description;
    oC_ErrorCode_t  ErrorCode;
} SavedError_t;

/*==========================================================================================================================================
//
//     MACROS
//
//========================================================================================================================================*/

#define _ADD_ERROR_TO_STRING_LIST( CODE_NAME , CODE_STR )           CODE_STR ,

/*==========================================================================================================================================
//
//     LOCAL VARIABLES
//
//========================================================================================================================================*/

/**
 * List of error strings
 */
static const char* oC_ErrorsStrings[] = {
                                                                        "Success" ,
                                                                        oC_ERRORS_LIST(_ADD_ERROR_TO_STRING_LIST)
                                                                        "Unknown error code"
};

static SavedError_t SavedErrors[CFG_UINT8_MAXIMUM_NUMBER_OF_SAVED_ERRORS];
static uint8_t      FreeIndex = 0;
static bool         Locked    = false;

/*==========================================================================================================================================
//
//     LOCAL FUNCTION PROTOTYPES
//
//========================================================================================================================================*/



/*==========================================================================================================================================
//
//     INTERFACE FUNCTIONS
//
//========================================================================================================================================*/

//==========================================================================================================================================
/**
 * The function returns string of the error code, if it exists.
 *
 * @param ErrorCode
 *
 * @return reference to the error string
 */
//==========================================================================================================================================
const char * oC_GetErrorString( oC_ErrorCode_t ErrorCode )
{
    const char * string = NULL;
    if ( ErrorCode < oC_ErrorCode_NumberOfErrorsDefinitions )
    {
        string = oC_ErrorsStrings[ErrorCode];
    }
    else
    {
        string = oC_ARRAY_LAST_ELEMENT(oC_ErrorsStrings);
    }
    return string;
}

//==========================================================================================================================================
/**
 * @brief save error code with description for later
 *
 * @param Description   any description of the error (it must be stored in flash, because the string is not copied)
 * @param ErrorCode     code of error to save
 *
 * @return true if success, false if there is no more place
 */
//==========================================================================================================================================
bool oC_SaveErrorFunction( const char * Description , oC_ErrorCode_t ErrorCode , const char * Function, uint32_t LineNumber )
{
    bool success = false;

    if(Locked == false && FreeIndex < CFG_UINT8_MAXIMUM_NUMBER_OF_SAVED_ERRORS)
    {
        SavedErrors[FreeIndex].Description = Description;
        SavedErrors[FreeIndex].ErrorCode   = ErrorCode;
        SavedErrors[FreeIndex].Function    = Function;
        SavedErrors[FreeIndex].LineNumber  = LineNumber;
        FreeIndex++;

        success = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads error code with description from the errors stack
 */
//==========================================================================================================================================
bool oC_ReadLastError( const char ** outDescription , oC_ErrorCode_t * outErrorCode , const char ** outFunction , uint32_t * outLineNumber )
{
    bool read = false;

    if(FreeIndex > 0)
    {
        FreeIndex--;
        *outDescription = SavedErrors[FreeIndex].Description;
        *outErrorCode   = SavedErrors[FreeIndex].ErrorCode;
        *outFunction    = SavedErrors[FreeIndex].Function;
        *outLineNumber  = SavedErrors[FreeIndex].LineNumber;

        read = true;
    }

    return read;
}

//==========================================================================================================================================
/**
 * @brief locks or unlocks saving errors
 */
//==========================================================================================================================================
void oC_SetLockSavingErrors( bool Lock )
{
    Locked = Lock;
}

/*==========================================================================================================================================
//
//     LOCAL FUNCTION 
//
//========================================================================================================================================*/
