/** ****************************************************************************************************************************************
 *
 * @file       oc_datapackage.c
 *
 * @brief      stores implementation of DataPackage functions
 *
 * @author     Patryk Kubiak - (Created on: 11 09 2017 19:59:57)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_datapackage.h>
#include <string.h>

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS__________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief initializes data package
 *
 * The function is for initialization of the #oC_DataPackage_t structure. It should be used always after definition of the structure to fill
 * all data package fields.
 *
 * @param DataPackage       Pointer to the data package structure
 * @param Buffer            Pointer to the buffer that should be used for storing data
 * @param Size              Size of the buffer with/for data
 * @param DataLength        Number of bytes with data in the `Buffer` - it is for reading bytes from the buffer
 * @param ReadOnly          Set it to `true` if the data package should be used only for reading data
 */
//==========================================================================================================================================
void oC_DataPackage_Initialize( oC_DataPackage_t * DataPackage, const void * Buffer, oC_MemorySize_t Size, oC_MemorySize_t DataLength , bool ReadOnly )
{
    if( DataPackage != NULL && Buffer != NULL && Size > 0 && DataLength <= Size )
    {
        DataPackage->MagicNumber    = oC_DATA_PACKAGE_MAGIC_NUMBER;
        DataPackage->BufferSize     = Size;
        DataPackage->ReadBuffer     = Buffer;
        DataPackage->ReadOnly       = ReadOnly;
        DataPackage->PutOffset      = 0;
        DataPackage->GetOffset      = 0;
        DataPackage->DataLength     = DataLength;
    }
}

//==========================================================================================================================================
/**
 * @brief checks if the data package is full
 *
 * The function is for checking if the given data package is full and if it cannot store more data in the buffer.
 *
 * @note
 * To put data into the `Buffer`, the structure has to be initialized as NOT `ReadOnly` in #oC_DataPackage_Initialize
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 *
 * @return `true` if data package is not correct or if it is full and no more data can be put by using #oC_DataPackage_PutDataU8/`U16`/`U32`/`U64`,
 * `false` if the data package is correct and can be used to put more data inside
 */
//==========================================================================================================================================
bool oC_DataPackage_IsFull( const oC_DataPackage_t * DataPackage )
{
    bool isFull = true;

    if( DataPackage != NULL && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER && DataPackage->ReadOnly == false )
    {
        isFull = DataPackage->DataLength >= DataPackage->BufferSize;
    }

    return isFull;
}

//==========================================================================================================================================
/**
 * @brief checks if the data package is empty
 *
 * The function is for checking if the given data package is empty and if it cannot return more data from the buffer.
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 *
 * @return `true` if data package is not correct or if it is full and no more data can be read by using #oC_DataPackage_GetDataU8/`U16`/`U32`/`U64`,
 * `false` if the data package is correct and can be used to read more data
 */
//==========================================================================================================================================
bool oC_DataPackage_IsEmpty( const oC_DataPackage_t * DataPackage )
{
    bool isEmpty = true;

    if( DataPackage != NULL && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER )
    {
        isEmpty = DataPackage->DataLength == 0;
    }

    return isEmpty;
}

//==========================================================================================================================================
/**
 * @brief puts data into the buffer
 *
 * The function is for putting data of type `uint8_t` to the `DataPackage` buffer.
 *
 * @note
 * To put data into the buffer, `DataPackage` has to be initialized as NOT `ReadOnly`
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 * @param Data              Data to put into the buffer
 *
 * @return `true` if the `DataPackage` buffer is full, and no more data of the type `uint8_t` can be putted
 */
//==========================================================================================================================================
bool oC_DataPackage_PutDataU8( oC_DataPackage_t * DataPackage , uint8_t Data )
{
    bool isFull = true;
    if( DataPackage != NULL
     && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
     && DataPackage->DataLength  <  DataPackage->BufferSize
     && DataPackage->ReadOnly    == false
     && DataPackage->PutOffset  <  DataPackage->BufferSize
     && oC_DataPackage_GetLeftSize(DataPackage) >= sizeof(Data)
        )
    {
        typeof(Data) * out = &DataPackage->WriteArrayU8[ DataPackage->PutOffset ];

        *out = Data;

        DataPackage->DataLength += sizeof(Data);
        DataPackage->PutOffset += sizeof(Data);
        if ( DataPackage->PutOffset >= DataPackage->BufferSize )
        {
            DataPackage->PutOffset = 0;
        }

        isFull = DataPackage->DataLength < DataPackage->BufferSize;
    }

    return isFull == false;
}

//==========================================================================================================================================
/**
 * @brief puts data into the buffer
 *
 * The function is for putting data of type `uint16_t` to the `DataPackage` buffer.
 *
 * @note
 * To put data into the buffer, `DataPackage` has to be initialized as NOT `ReadOnly`
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 * @param Data              Data to put into the buffer
 *
 * @return `true` if the `DataPackage` buffer is full, and no more data of the type `uint16_t` can be putted
 */
//==========================================================================================================================================
bool oC_DataPackage_PutDataU16( oC_DataPackage_t * DataPackage , uint16_t Data )
{
    bool isFull = true;

    if( DataPackage != NULL
     && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
     && DataPackage->DataLength  <  DataPackage->BufferSize
     && DataPackage->ReadOnly    == false
     && DataPackage->PutOffset  <  DataPackage->BufferSize
     && oC_DataPackage_GetLeftSize(DataPackage) >= sizeof(Data)
        )
    {
        typeof(Data) * out = (void*)&DataPackage->WriteArrayU8[ DataPackage->PutOffset ];

        *out = Data;

        DataPackage->DataLength += sizeof(Data);
        DataPackage->PutOffset += sizeof(Data);
        if ( DataPackage->PutOffset >= DataPackage->BufferSize )
        {
            DataPackage->PutOffset = 0;
        }

        isFull = DataPackage->DataLength < DataPackage->BufferSize;
    }

    return isFull == false;
}

//==========================================================================================================================================
/**
 * @brief puts data into the buffer
 *
 * The function is for putting data of type `uint32_t` to the `DataPackage` buffer.
 *
 * @note
 * To put data into the buffer, `DataPackage` has to be initialized as NOT `ReadOnly`
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 * @param Data              Data to put into the buffer
 *
 * @return `true` if the `DataPackage` buffer is full, and no more data of the type `uint32_t` can be putted
 */
//==========================================================================================================================================
bool oC_DataPackage_PutDataU32( oC_DataPackage_t * DataPackage , uint32_t Data )
{
    bool isFull = true;

    if( DataPackage != NULL
     && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
     && DataPackage->DataLength  <  DataPackage->BufferSize
     && DataPackage->ReadOnly    == false
     && DataPackage->PutOffset  <  DataPackage->BufferSize
     && oC_DataPackage_GetLeftSize(DataPackage) >= sizeof(Data)
        )
    {
        typeof(Data) * out = (void*)&DataPackage->WriteArrayU8[ DataPackage->PutOffset ];

        *out = Data;

        DataPackage->DataLength += sizeof(Data);
        DataPackage->PutOffset += sizeof(Data);
        if ( DataPackage->PutOffset >= DataPackage->BufferSize )
        {
            DataPackage->PutOffset = 0;
        }

        isFull = DataPackage->DataLength >= DataPackage->BufferSize;
    }

    return isFull;
}

//==========================================================================================================================================
/**
 * @brief puts data into the buffer
 *
 * The function is for putting data of type `uint64_t` to the `DataPackage` buffer.
 *
 * @note
 * To put data into the buffer, `DataPackage` has to be initialized as NOT `ReadOnly`
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 * @param Data              Data to put into the buffer
 *
 * @return `true` if the `DataPackage` buffer is full, and no more data of the type `uint64_t` can be putted
 */
//==========================================================================================================================================
bool oC_DataPackage_PutDataU64( oC_DataPackage_t * DataPackage , uint64_t Data )
{
    bool isFull = true;

    if( DataPackage != NULL
     && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
     && DataPackage->DataLength  <  DataPackage->BufferSize
     && DataPackage->ReadOnly    == false
     && DataPackage->PutOffset  <  DataPackage->BufferSize
     && oC_DataPackage_GetLeftSize(DataPackage) >= sizeof(Data)
        )
    {
        typeof(Data) * out = (void*)&DataPackage->WriteArrayU8[ DataPackage->PutOffset ];

        *out = Data;

        DataPackage->DataLength += sizeof(Data);
        DataPackage->PutOffset += sizeof(Data);
        if ( DataPackage->PutOffset >= DataPackage->BufferSize )
        {
            DataPackage->PutOffset = 0;
        }

        isFull = DataPackage->DataLength < DataPackage->BufferSize;
    }

    return isFull == false;
}

//==========================================================================================================================================
/**
 * @brief returns data from the data package buffer
 *
 * The function is for reading data of type `uint8_t` from the `DataPackage` buffer.
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 *
 * @return data read from the `DataPackage` buffer
 *
 * @see oC_DataPackage_IsEmpty
 */
//==========================================================================================================================================
uint8_t oC_DataPackage_GetDataU8( oC_DataPackage_t * DataPackage )
{
    uint8_t data = 0;

    if(    DataPackage != NULL
        && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
        && DataPackage->GetOffset   <  DataPackage->BufferSize
        && DataPackage->DataLength  >= sizeof(data)
        )
    {
        const typeof(data) * in = (void*)&DataPackage->ReadArrayU8[ DataPackage->GetOffset ];

        data = *in;

        DataPackage->DataLength -= sizeof(data);
        DataPackage->GetOffset  += sizeof(data);
        if ( DataPackage->GetOffset >= DataPackage->BufferSize )
        {
            DataPackage->GetOffset = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief returns data from the data package buffer
 *
 * The function is for reading data of type `uint16_t` from the `DataPackage` buffer.
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 *
 * @return data read from the `DataPackage` buffer
 *
 * @see oC_DataPackage_IsEmpty
 */
//==========================================================================================================================================
uint16_t oC_DataPackage_GetDataU16( oC_DataPackage_t * DataPackage )
{
    uint16_t data = 0;

    if(    DataPackage != NULL
        && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
        && DataPackage->GetOffset   <  DataPackage->BufferSize
        && DataPackage->DataLength  >= sizeof(data)
        )
    {
        const typeof(data) * in = (void*)&DataPackage->ReadArrayU8[ DataPackage->GetOffset ];

        data = *in;

        DataPackage->DataLength -= sizeof(data);
        DataPackage->GetOffset  += sizeof(data);
        if ( DataPackage->GetOffset >= DataPackage->BufferSize )
        {
            DataPackage->GetOffset = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief returns data from the data package buffer
 *
 * The function is for reading data of type `uint32_t` from the `DataPackage` buffer.
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 *
 * @return data read from the `DataPackage` buffer
 *
 * @see oC_DataPackage_IsEmpty
 */
//==========================================================================================================================================
uint32_t oC_DataPackage_GetDataU32( oC_DataPackage_t * DataPackage )
{
    uint32_t data = 0;

    if(    DataPackage != NULL
        && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
        && DataPackage->GetOffset   <  DataPackage->BufferSize
        && DataPackage->DataLength  >= sizeof(data)
        )
    {
        const typeof(data) * in = (void*)&DataPackage->ReadArrayU8[ DataPackage->GetOffset ];

        data = *in;

        DataPackage->DataLength -= sizeof(data);
        DataPackage->GetOffset  += sizeof(data);
        if ( DataPackage->GetOffset >= DataPackage->BufferSize )
        {
            DataPackage->GetOffset = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief returns data from the data package buffer
 *
 * The function is for reading data of type `uint64_t` from the `DataPackage` buffer.
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 *
 * @return data read from the `DataPackage` buffer
 *
 * @see oC_DataPackage_IsEmpty
 */
//==========================================================================================================================================
uint64_t oC_DataPackage_GetDataU64( oC_DataPackage_t * DataPackage )
{
    uint64_t data = 0;

    if(    DataPackage != NULL
        && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
        && DataPackage->GetOffset   <  DataPackage->BufferSize
        && DataPackage->DataLength  >= sizeof(data)
        )
    {
        const typeof(data) * in = (void*)&DataPackage->ReadArrayU8[ DataPackage->GetOffset ];

        data = *in;

        DataPackage->DataLength -= sizeof(data);
        DataPackage->GetOffset  += sizeof(data);
        if ( DataPackage->GetOffset >= DataPackage->BufferSize )
        {
            DataPackage->GetOffset = 0;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief returns left size of the buffer
 *
 * The function returns number of free bytes left in the buffer
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 *
 * @return left free size of buffer
 */
//==========================================================================================================================================
oC_MemorySize_t oC_DataPackage_GetLeftSize( const oC_DataPackage_t * DataPackage )
{
    oC_MemorySize_t size = 0;

    if( DataPackage != NULL && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER && DataPackage->BufferSize >= DataPackage->DataLength )
    {
        size = DataPackage->BufferSize - DataPackage->DataLength;
    }

    return size;
}

//==========================================================================================================================================
/**
 * Clears all the data inside the data package
 *
 * @param DataPackage       Pointer to the data package structure initialized with #oC_DataPackage_Initialize
 */
//==========================================================================================================================================
void oC_DataPackage_Clear( oC_DataPackage_t * DataPackage )
{
    if( DataPackage != NULL
     && DataPackage->MagicNumber == oC_DATA_PACKAGE_MAGIC_NUMBER
     && DataPackage->ReadOnly    == false
        )
    {
        DataPackage->DataLength = 0;
        DataPackage->PutOffset = 0;
        DataPackage->GetOffset = 0;
        memset(DataPackage->WriteBuffer,0,DataPackage->BufferSize);
    }
}

#undef  _________________________________________FUNCTIONS__________________________________________________________________________________

