############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/04/27
##  Description:    makefile for the libraries space
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              PREPARATION VARIABLES               
##                                          
##============================================================================================================================
ifeq ($(PROJECT_DIR),)
    PROJECT_DIR = ../../..
endif

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
SYSTEM_LIBRARIES_OPTIMALIZE     = Os
SYSTEM_LIBRARIES_WARNING_FLAGS  = -Wall -Werror
SYSTEM_LIBRARIES_CSTD           = gnu11
SYSTEM_LIBRARIES_DEFINITIONS    = oC_LIBRARIES_SPACE 

##============================================================================================================================
##                                          
##          INCLUDE MAKEFILES       
##                                          
##============================================================================================================================
include $(GLOBAL_DEFS_MK_FILE_PATH)

##============================================================================================================================
##                                          
##              EXTENSIONS CONFIGURATION
##                                          
##============================================================================================================================
C_EXT                       = c
OBJ_EXT                     = o

##============================================================================================================================
##                                          
##              PREPARATION OF DIRECTORIES LIST             
##                                          
##============================================================================================================================
SYSTEM_LIBRARIES_DIRS            = $(filter %/, $(wildcard $(SYSTEM_LIBRARIES_SOURCES_DIR)/*/))  $(SYSTEM_LIBRARIES_SOURCES_DIR)
SYSTEM_LIBRARIES_INCLUDES_DIRS   = $(filter %/, $(wildcard $(SYSTEM_LIBRARIES_INCLUDES_DIR)/*/)) $(SYSTEM_LIBRARIES_INCLUDES_DIR) $(SYSTEM_CONFIG_DIR) $(SYSTEM_CORE_INCLUDES_DIR)

##============================================================================================================================
##                                          
##              PREPARATION OF SOURCE FILES         
##                                          
##============================================================================================================================
SYSTEM_LIBRARIES_SOURCES           = $(foreach src,$(foreach subdir, $(SYSTEM_LIBRARIES_DIRS), $(wildcard $(subdir)/*.c)),$(subst //,/,$(src)))

##============================================================================================================================
##                                          
##              ADD EXTRA DEFINITIONS        
##                                          
##============================================================================================================================
SYSTEM_LIBRARIES_DEFINITIONS       += $(BUILD_VERSION_DEFINITIONS) $(ARCHITECTURE_DEFINITIONS) __POSIX_VISIBLE=200809
                                  
##============================================================================================================================
##                                          
##              PREPARATION OF COMPILER ARGUMENTS   
##                                          
##============================================================================================================================
SYSTEM_LIBRARIES_INCLUDES          = $(foreach dir,$(SYSTEM_LIBRARIES_INCLUDES_DIRS),-I$(dir))
SYSTEM_LIBRARIES_DEFINITIONS_FLAGS = $(foreach def,$(SYSTEM_LIBRARIES_DEFINITIONS),-D$(def))
SYSTEM_LIBRARIES_CFLAGS            = -c -g -std=$(SYSTEM_LIBRARIES_CSTD) $(SYSTEM_LIBRARIES_WARNING_FLAGS) -$(SYSTEM_LIBRARIES_OPTIMALIZE) $(CPUCONFIG_CFLAGS) \
                                     -fdata-sections -ffunction-sections $(ADDITIONAL_CFLAGS)

##============================================================================================================================
##                                          
##              TARGETS 
##                                          
##============================================================================================================================
print_libraries_info:
	@echo "=================================================================================================================="
	@echo "                                           Building Libraries Space"
	@echo "=================================================================================================================="
	@echo " $(SYSTEM_LIBRARIES_SOURCES_DIR)"
	@echo "Libraries includes list:"
	@echo -e " $(foreach inc,$(SYSTEM_LIBRARIES_INCLUDES_DIRS), include directory: $(inc)\n)"
	@echo "Libraries sources list:"
	@echo -e " $(foreach src,$(SYSTEM_LIBRARIES_SOURCES), source file: $(src)\n)"
	@echo "Creating directories..."
	@$(MKDIR) $(MACHINE_OUTPUT_DIR)
build_libraries_dependences:
	@echo "Creating dependeces..."
	@$(MKDEP) -f libraries.mak $(SYSTEM_LIBRARIES_INCLUDES) -- $(SYSTEM_LIBRARIES_CFLAGS) -- $(SYSTEM_LIBRARIES_SOURCES) -o .$(OBJ_EXT)
	
build_libraries_objects:
	@echo "Building libraries..."
	@$(CC) $(SYSTEM_LIBRARIES_SOURCES) $(SYSTEM_LIBRARIES_INCLUDES) $(SYSTEM_LIBRARIES_CFLAGS) $(SYSTEM_LIBRARIES_DEFINITIONS_FLAGS)
    
archieve_libraries:
	@echo "Archieve library space..."
	@$(AR) rcs $(LIBRARIES_SPACE_LIB_FILE_PATH) *.o
	@$(RM) *.o
    
libraries_status:
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Libraries Space building success"
	@echo ""
    
build_libraries: print_libraries_info clean_libraries_output build_libraries_objects archieve_libraries libraries_status
    
clean_libraries_output:
	@echo "Removing old libraries space library file..."
	$(RM) -f $(LIBRARIES_SPACE_LIB_FILE_PATH)
	
clean_libraries:
	@echo "=================================================================================================================="
	@echo "                                           Cleaning Libraries Space"
	@echo "=================================================================================================================="
	@echo " "
	$(RM) -f *.o
	$(RM) -f *.a
	$(RM) -f $(LIBRARIES_SPACE_LIB_FILE_PATH)
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Libraries Space cleaning success"
	@echo ""
    
