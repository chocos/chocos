/** ****************************************************************************************************************************************
 *
 *    Changing the Hardware On Click Operating System       (ChOC)
 *
 *    @file          choc.h
 *
 *    @brief         Main file of the ChOC system. It provides interface to all modules in the system.
 *
 *    @author        Patryk Kubiak
 *
 *    @note          Copyright (C) 2014 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                   This program is free software; you can redistribute it and/or modify
 *                   it under the terms of the GNU General Public License as published by
 *                   the Free Software Foundation; either version 2 of the License, or
 *                   (at your option) any later version.
 *
 *                   This program is distributed in the hope that it will be useful,
 *                   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                   GNU General Public License for more details.
 *
 *                   You should have received a copy of the GNU General Public License
 *                   along with this program; if not, write to the Free Software
 *                   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @mainpage Choco OS - Chocolate Operating System
 *
 * @par
 * The **Choco-OS** is a real time operating system designed for microcontrollers. It is not only RTOS kernel - the system includes drivers
 * for microcontroller peripherals, such as UART, SPI or TIMER, drivers for chips (HD44780,nRF24L01,LCD-TFT, etc..), file system,
 * most of POSIX libraries and much more.
 *
 * ![Product Flow](idea_product_flow.png)
 *
 * @par
 * The main assumption of the system is to speed up product development time. It provides simple interface for all supported devices and peripherals.
 * Thanks to that developer must not learn all unnecessary details before he starts implementation of the project main point. It's simple -
 * you don't have to know how exactly SD-Card is handled. It is enough, that you know, that it exists, and where it is connected.
 *
 * ![Main Architecture](main_architecture.png)
 *
 * @par
 * Architecture of the system is based on the main division - kernel space and user space. The Kernel part is an lower layer responsible for
 * managing system resources, drivers, programs, etc. Modules, that belongs to the space has access for all resources. It is important, that
 * each kernel module must be compiled and programmed together. The User Space is a layer with limited access. It allows to prevent against
 * unauthorized operations what makes the system more safe. The other feature specific for the space is possibility of programs execution
 * that are compiled separately and can be added during work of the system. An engineer during development of a program has to choose one
 * of these layers. But what actually program is?
 *
 * ![Programs Types](programs_user_kernel.png)
 *
 * @par
 * The program is part of the software which realizes some task by using system resources and libraries. The system can executes many
 * independent programs in the same time by sharing resources between them. Any program can be compiled with the system and be included to
 * the common binary file. In such case we are talking about the **Embedded Programs**. These programs cannot be removed or replaced without
 * recompilation of the system. The other solution, named **Floating Programs**, allows for managing and changing the software during run
 * of the system (for example by using SD Card). How programs can be executed?
 *
 * ![Connected everywhere](connections_to_chocoos.png)
 * @par
 * If you want to release a product, that will not be changed in the future you can set your program as main entry point. It will be
 * automatically executed in the loop after start of the system. This allows you to remove all not needed modules and save some memory.
 * In the other case you can connect to the system by using one of supported ways, and run program by using GUI or terminal. The list is
 * still developed and depends of the system version, but the basic type of connection is an access via UART interface (by usage of any
 * computer program that handles VT100 terminals, e.g. Putty).
 *
 * @par
 * Now you know basics of the system and you are ready to write your first application. Let's to this.
 * **JOIN TO THE CHOCOLATE WORLD!!**
 *
 * @page Trainings Trainings
 *
 * @section Step-1 Step 1. Let's run the system!
 * _________________________________________________________________________________________________________________________________________
 *
 * First of all you need to choose target microcontroller, that you want to flash. The list of supported targets you can find in the system release notes.
 * In the example we will use **STM32F7 Discovery Board** with embedded **stm32f746ngh6** microcontroller featuring 1 MB of Flash and 340 KB RAM.
 * More informations about the board you can find at <b><a href="http://www2.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-eval-tools/stm32-mcu-eval-tools/stm32-mcu-discovery-kits/32f746gdiscovery.html">this</a></b> link.
 * ![STM32F7 Discovery Board](en.stm32f746g-disco.jpg)
 *
 * In this example we will use UART to connect the system with PC. This is the most basic way supported from version V.0.4.0. For this reason
 * you will need an UART converter such as (you can buy it e.x. at <a href="http://sklep.msx-elektronika.pl/strona-glowna/180-konwerter-usb-uart-rs232-ttl-na-ft232-33-5v.html">THIS LINK</a>):
 * ![UART Converter](konwerter-uart.jpg)
 *
 * Moreover you have to get the system source code. You can do it by download the latest release version from the <b><a href="http://chocoos.pl">ChocoOS.pl</a></b> site or by
 * cloning the **ChocoOS** repository:
 * @code
   git clone git@bitbucket.org:chocos/chocos.git
   @endcode
 *
 * @subsection Windows
 * When you have the sources, you should open command prompt and open a *Source* directory in it. Now use **select_arch.bat** script to select
 * target architecture, that you want to use. In our example we have to select **stm32f746ngh6** architecture:
 * ![Architecture Selection](select_arch.png)
 *
 * Now you are able to compile the project. To do it, just run **build.bat** script:
 * ![Project building](build.png)
 *
 * When the build will finish with success you should get the following message:
 * ![Build Success](build_success.png)
 *
 * All resultant files are stored in the **Output** directory created in the main system dir (**ChocoOS/Output**). It can be useful for you
 * if you want to manually flash the microcontroller - you can find there compiled project in formats: HEX, ELF and BIN.
 *
 * The system by default is prepared for handling most popular boards - thanks to that in our case we can just use script **connect.bat**
 * for connection with the debugger embedded in the board. The script will open command prompt in new window, and if everything will be success,
 * we will see following screen:
 * ![Connection with debugger](connect.png)
 *
 * It is very likely, that when you will connect the **Discovery Board** for the first time, you will need to install USB drivers first. You can
 * find signed drivers in @code ChocoOS\Tools\drivers\en.stsw-link009@endcode or download
 * from the <a href="http://www.st.com/en/development-tools/st-link-v2.html">ST website</a>.
 *
 * @note
 * The **ChocoOS** uses **OpenOCD** to connect and flash a microcontroller. If it does not work correctly, you can try to find an answer in the official
 * **OpenOCD** page: <a href="http://openocd.org">openocd.org</a>
 *
 * To write system image into the chip flash memory you use script **flash.bat**. Remember, that the connection window should be still open during
 * this operation, just like in the example below:
 * ![Flashing windows](flash.png)
 *
 * If this operation will be success you should get message in **flash.bat** window with informations about **Transfer rate** and **Start address**.
 *
 * @subsection Linux
 *
 * Linux support is not ready yet, sorry :(
 *
 * @subsection ConverterPinsConecting UART converter pins connection
 *
 * When you are able to flash the chip, we can connect to the system. As I mentioned earlier we will use for this a UART converter. Before it,
 * we have to connect the board with converter. The list of pins used by the system you can find in the following directory:
 * @code ChocoOS\Source\system\config\stm32f746ngh6 @endcode
 * As you probably noticed, there is the target name in the path (**stm32f746ngh6**). There is separate directory for each supported target.
 * It does not contain all configurations files, but only these, that should be specific for the selected target. More informations about
 * configuration you will find in the configuration training. Anyway, in this case we will need to look at the file named **oc_pinsmap.h**.
 * Inside, you will find these lines:
 * @code
    ADD_PIN( UART_RX            , PC7 ) \
    ADD_PIN( UART_TX            , PC6 ) \
   @endcode
 * This is what we are looking for - names of pins, where the UART is connected. Remember, that you should connect it crosswise to the converter:
 * Rx to Tx and Tx to Rx. In the documentation of **STM32F7 Discovery Board** you can find pins output. In this case it is D0 and D1:
 * ![UART Pins Connections](uart-pins.jpg)
 *
 * Remember, that you also have to connect GND signal and make sure that all connections are correct.
 *
 * @subsection PuttyConfiguration PuTTY configuration
 *
 * In this example we will use the PuTTY as a terminal for the system. You should connect the converter to the PC and run the PuTTY app. You
 * must configure only 3 fields here:
 *
 * 1. Connection Type as **Serial**
 * 2. Serial Line to **COM[x]**, where the **X** is line of your UART converter (you can check it in the control panel)
 * 3. Speed to **921600** - this is a bit rate of the transmission. It can be always changed and checked in the configuration file (The **BitRate** field):
 * @code ChocoOS\Source\system\config\oc_streams_cfg.c @endcode
 *
 * ![PuTTY Configuration](PuTTY.png)
 *
 * @note
 * It is recommended to also configure PuTTY character set translation to "Use font encoding" option. Thanks to that you will see correct
 * borders in the terminal GUI.
 *
 * ![PuTTY Font Encoding](putty-font-encoding.png)
 *
 * Now just click **Open** and wait a while (max 10 sec). You should see following screen:
 * ![Login Form](login-screen.png)
 *
 * If you didn't see it, firstly try to reset the board, and again - wait a while. If it does not help, here is list of possible problems:
 * 1. Some converter-board connection is not correct
 * 2. You typed wrong Serial Line number in PuTTY configuration
 * 3. The microcontroller was not flashed correctly
 *
 * @note
 * If the system prints something, but printed text is not correct, the transmission speed is set to wrong value in the PuTTY configuration. Check
 * it again and restart the PuTTY.
 *
 * Remember - if it does not work, it is more than sure, that you have one of the problems above.
 *
 * @section Step-2 Step 2. First glance at terminal
 *
 * Login and password by default is "root". Please type it, you will see terminal, and welcome message:
 * ![Welcome Message](welcome-message.png)
 *
 * @subsection MemoryProgram Program Memory
 *
 * If you type the "memory" command in the chell, you will see the program for showing memory informations, such as:
 * - RAM or ROM size,
 * - Usage of memory
 * - Memory alignment
 * - And sizes of few sections
 *
 * ![Memory Program](memory_program.png)
 *
 * But more important thing hides under the "Show Allocations" menu.
 * It's here, where you can see list of allocated memory.
 * There is not a way to allocate memory in the ChocoOS without specify "Allocator"
 * that describes module requesting for. It is very useful to find a memory leakage.
 *
 * ![Memory Allocations](memory_allocations.png)
 *
 * @subsection SystemProgram Program System
 *
 * To get informations about the system, you type "system" command in the chell. You can see here:
 *
 * - version of the system
 * - build date and time - you should note, that it is not a release date, but only compilation date
 * - also here you can find CPU Architecture
 * - and time of system startup
 *
 * Moreover the program allows for showing system logs and errors. These can be cleared by choosing a "clear" options.
 *
 * ![System Program](system_program.png)
 *
 * In the "Config" menu you can change some system configuration such as:
 *
 * - size of a default stack - it's just size of stack for new threads, when it is not given
 * - enable automatic stack increase option. It is a mechanism for automatically detecting size of the stack,
 * but note that this is not fully reliable way and you should avoid usage of it. It should be treated as additional protection against overflow of the stack.
 * - The red zone size determines size of the memory, that is reserved for each thread as additional emergency stack.
 * When the basic stack is exhausted, the system uses a memory reserved as the redzone.
 *
 * @section Step-3 Step 3. Write your first "Hello World!"
 *
 * Let's write our first program - "Hello World". It will use the STDOUT for print "Hello World!". The program will be included to the CORE
 * space (for more info please look at the system architecture - for now it is not important) and will be stored in the "user" directory.
 *
 * @subsection GenerateTemplate Generate a template
 *
 * First of all we have to generate empty program files. We will use for that the script "newprogram". Please type command in the
 * **ChocoOS** source directory (Windows Command Prompt):
 *
 * @code
   newprogram HelloWorld 1024 CORE USER
   @endcode
 *
 * The **HelloWorld** will be name of the program and **1024** is a size for main thread stack. The argument **CORE** determines layer
 * of architecture. It can be set to values **CORE** or **USER**. The last argument allows to choose if the program should be stored in
 * the system or user directory.
 *
 * ![Hello World Generation](newprogram_helloworld.png)
 *
 *  Now you can open the **Source/user/programs** path and look at the generated files stored in the **Hello World** directory.
 *  Please, open it - you should see 2 files: makefile and a source file called main.c.
 *
 * ![Hello World Files](helloworld_files.png)
 *
 * @subsection EditSource Edit source file
 *
 * The file called **main.c** is the program main file. It is included by default. To add more program files, please edit the file **Makefile**.
 * Now open the **main.c** file and add message as in the example below.
 *
 * ![Hello World Source File](helloworld.png)
 *
 * @subsection AddingToList Adding Hello World to programs list
 *
 * Ok, now the program is almost ready. You just have to add it to programs list. To do it open the **oc_programs_list.h** file
 * (it is stored in the **config** directory), and add your program to the list:
 *
 * ![Adding program HelloWorld to programs list](programs_list.png)
 *
 * Your hello world program is ready to run! You have to compile the system and write the image to the microcontroller like in the previous step:
 * @ref Step-1
 * Now type **HelloWorld** command in chell, and enjoy!
 *
 * ![Hello World Run](helloworld_runned.png)
 *
 * @section Step-4 Step 4. Working with Eclipse
 *
 * Developing, building and debugging ChocoOS in Eclipse is pleasant and easy.
 *
 * @subsection ProjectImport Importing project
 *
 * Import project from the @code ChocoOS\Source@endcode directory.
 *
 * @note
 * In older revisions there is another project with the same name in directory above, so import project exactly from this path.
 *
 * OS supports more than one architecture so there are objects redefinitions. To hide files used for other architecture just run **select_arch.bat**
 * script from command line (like described in Step 1).
 *
 * @subsection Building
 *
 * Click a build button for architecture you want:
 * ![Building script selection](eclipse_build.png)
 *
 * @subsection Debugging
 *
 * First install ARM GDB plug-in to Eclipse (**only** CDT Optional Features -> C/C++ GDB Hardware Debugging). More details and
 * installation description can be found here: <a href="http://gnuarmeclipse.github.io/plugins/install">GNU ARM Eclipse</a>
 * @note
 * If you have problems during installation use <i>off-line install</i> instruction from link above.
 *
 * Create or modify existing **debug configuration** like on the picture below:
 * ![Debug configuration settings](eclipse_debug_config.png)
 *
 * Run **connect.bat** script in the background and now you can run the configuration. MCU is reflashed automatically.
 *
 * @section Step-5 Step 5. Placing any file inside the target flash memory
 *
 * TBD
 *
 * @section Step-6 Step 6. Usage of stdio and TGUI for generating terminal GUI
 *
 * TBD
 *
 * @section Step-7 Step 7. Making GUI application with real widgets
 *
 * TBD
 *
 * @section Step-8 Step.8 Changing configuration of the project
 *
 * TBD
 *
 * @subsection ListOfDrivers List of drivers
 *
 * TBD
 *
 *
 * @subsection ListOfStreams List of streams
 *
 * TBD
 *
 * @subsection ListOfServices List of services
 *
 * TBD
 *
 * @subsection AutoConfigurations Auto-Configurations
 *
 * TBD
 *
 * @subsection ListOfFileSystems List of file systems
 *
 * TBD
 *
 * @subsection ListOfScreens List of screens
 *
 * TBD
 *
 * @subsection NetworkConfiguration Network configuration
 *
 * TBD
 *
 * @subsection SystemConfiguration System configuration
 *
 * TBD
 *
 * @subsection PinsDefinition Pins definition
 *
 * TBD
 *
 * @section Step-9 Step.9 Write your first driver
 *
 * TBD
 *
 * @section Step-10 Step.10 Write your first file system
 *
 * TBD
 *
 * @section Step-11 Step.11 Write your service
 *
 * TBD
 *
 * _________________________________________________________________________________________________________________________________________
 *
 *
 ******************************************************************************************************************************************/
#ifndef CHOC_H_
#define CHOC_H_

/** ========================================================================================================================================
 *
 *              The section with coding standard
 *
 *  ======================================================================================================================================*/
#define _________________________________________CODING_STANDARD_SECTION____________________________________________________________________

/**
 * @page CodingStandard Coding Standard
 *
 * @section Introduction
 *
 * This is a document that describes coding style for the ChocoOS system sources. It is required to follow this rules in case of coding
 * part of the system. This provides, that it will be easy to understand independently from author of the module. Moreover this rules helps
 * to quickly find bugs and add new features to the existing code. Note, that each rule has assigned it's unique number with `&` sign. Thanks to that
 * it is possible to use rule number during code review.
 *
 * @section Rules
 *
 * The first of all, the project is adapted to work with Eclipse IDE. There is special file, in the resources directory, named **code_style.xml**
 * that can be imported in the eclipse environment. It contains general style for the system.
 *
 * <b>&1.</b> **Margin** for the project is set to **140**. This should be readable for most of todays screens.<br/>
 * <b>&2.</b> **Do not use tabs** - most of editors supports system, that replace tabs to spaces. The width of the tab should be set to 4 spaces.
 * This is because tabs are treated by editors differently, but the code must look the same in each one.<br/>
 * <b>&3.</b> In each case, **braces** should be inserted in **separated line**. It is easier to find blocks using this rule.
 *
 * For example this is **bad**:
 * @code{.c}
 * if(){
 *      // code
 * }
 * @endcode
 * And this is **good**:
 * @code{.c}
 * if()
 * {
 *      // code
 * }
 * @endcode
 *
 * <b>&4.</b> **Spaces** should be inserted after opening parenthesis and before closing parenthesis like in the following example:
 * @code{.c}
 * if( true == false )
 * {
 *      // code
 * }
 * @endcode
 *
 * <b>&5.</b> Each block keyword (if,else,else if,do,while,for,switch) should be **placed in new line** (the 'do..while' is an exception from this rule)
 *
 * Examples that shows this rule:
 * @code{.c}
 * // This is correct
 * if( true )
 * {
 *     foo();
 * }
 * else if( false )
 * {
 *     do_sth();
 * }
 * else
 * {
 *     bar();
 * }
 *
 * // This is BAD!!
 * if( true )
 * {
 *     foo();
 * } else
 * {
 *     bar();
 * }
 *
 * // 'do..while' exception:
 * do
 * {
 *     foo();
 * } while( 0 );
 * @endcode
 *
 * <b>&6.</b> **Single statement blocks** are prohibited. The reason of it is that there is a risk, that someone will add new line to the block and will forgot
 * that only the first line will executed. This is presented in the example below:
 * @code{.c}
 * // This is BAD
 * if( condition ) foo();
 * @endcode
 * Suppose, that someone want to add bar() function to this condition:
 * @code{.c}
 * // This is still BAD
 * if( condition ) foo(); bar();
 * @endcode
 * Note, that only `foo()` will be executed in the condition statement. The `bar()` will be executed in any time. The correct way to write
 * this `if` is as follows:
 * @code{.c}
 * // This is GOOD
 * if( condition )
 * {
 *     foo();
 *     bar();
 * }
 * @endcode
 * <b>&7.</b> **Avoid magic numbers** - each number in the code should be defined as definition, value of enumerator, or constant variable in the place
 * that provide usage of it everywhere when it can be useful. Every time, when you are need to place some number in the code, you should try
 * to name it and create some definition for it. Only the **0** value does not need to be defined. The code below represents some examples
 * for this rule:
 * @code{.c}
 * // This is BAD
 * int findString( char ** Array )
 * {
 *     int Index;
 *
 *     for(Index=9;Index<33;Index++)
 *     {
 *        if(strcmp(Array[Index],"hello")==0)
 *        {
 *            break;
 *        }
 *     }
 *     return Index;
 * }
 * @endcode
 * The example above shows magic numbers that are inserted into the function. It is not clear why these numbers were used - why the Index is
 * initialized with 9, and not 0, why the Index is incremented to 33, and why it is compared with the "hello" string. Each one of these values
 * should be defined. Here is correct version of this function:
 * @code{.c}
 * // This is GOOD
 * #define ARRAY_STRING_START_INDEX     9
 * #define ARRAY_STRING_END_INDEX       33
 * #define SPECIAL_STRING               "hello"
 * int findSpecialString( char ** Array )
 * {
 *     int Index;
 *
 *     for(Index=ARRAY_STRING_START_INDEX;Index<ARRAY_STRING_END_INDEX;Index++)
 *     {
 *         if(strcmp(Array[Index],SPECIAL_STRING)==0)
 *         {
 *             break;
 *         }
 *     }
 *
 *     return Index;
 * }
 * @endcode
 *
 * <b>&8.</b> There can be **only one return** in function and **only one break** in block. When a function need to return some value, the variable
 * should be created, and set to this value to return at the end of the function. The same is in case of 'break' keyword. Here some example:
 * @code{.c}
 * bool Configure( void * Config )
 * {
 *     bool result = false;
 *
 *     if( Config != NULL )
 *     {
 *         ReadConfig(Config);
 *         result = true;
 *     }
 *
 *     return result;
 * @endcode
 *
 * <b>&9.</b> **Use enumerators instead of integers** when it is possible. When you need to create some status type, commands list, etc, you should
 * define a special enum type for it, instead of use universal integer. It is easier to find out what values can be stored in the type. For example:
 *
 * @code{.c}
 * // This is BAD
 * int DoSth( void * Config )
 * {
 *     int Result = 0;
 *     if( Config == NULL )
 *     {
 *         Result = EINVAL;
 *     }
 *     else if ( Config == Temp )
 *     {
 *         Result = NCORR;
 *     }
 *
 *     return Result;
 * }
 * @endcode
 *
 * @code{.c}
 * // This is GOOD
 * typedef enum
 * {
 *    Result_InvalidArgument,
 *    Result_CorrectValue ,
 *    Result_Success
 * } Result_t;
 *
 * Result_t DoSth( void * Config )
 * {
 *     Result_t Result = Result_Success;
 *
 *     if( Config == NULL )
 *     {
 *         Result = Result_InvalidArgument;
 *     }
 *     else if ( Config == Temp )
 *     {
 *         Result = Result_CorrectValue;
 *     }
 *
 *     return Result;
 * }
 * @endcode
 *
 * <b>&10.</b> **Use the error code type** (`oC_ErrorCode_t` from the oc_errors.h header) to return result of an operation everywhere where it is
 * possible. You can add your own codes to the list, but you should firstly ensure, that this code not exist already.
 *
 * <b>&11.</b> Each function, type, global variable, definition, macro and value, that is part of the system and is not local, **must starts with prefix**
 * `oC_`. Note that when it is only **local** thing, it **cannot contain it**. This rule ensure, that names will not be duplicated, and helps
 * to identify local names.
 *
 * @code{.c}
 * extern oC_ErrorCode_t oC_GPIO_Configure( oC_GPIO_Config_t * Config );
 *
 * static bool SetMode( oC_GPIO_Mode_t Mode );
 * @endcode
 *
 * <b>&12.</b> Each function, type, global variable, definition, macro and value name, that is not local, must contain **name of the module**, that
 * is a part of. Name of the module is given after `oC_` prefix, as in example below:
 * @code{.c}
 * // This is function from the module GPIO
 * oC_GPIO_DoSth( void );
 * @endcode
 * There are some exceptions from this rule, but in each case it should be discussed with the ChocoOS team owner. Moreover it is possible to
 * define a type, or function that is specific and unique for the module using only module name without type/function name. For example:
 * @code{.c}
 * typedef void * oC_Mutex_t; // this is for Mutex module in oc_mutex.h
 * @endcode
 * Moreover name of the module should be **written with uppercase only when it is driver name**, otherwise **only the first letter of the word**
 * should be uppercase (also if a word is shortcut).
 *
 * @code{.c}
 * extern void       oC_TIMER_TurnOn    ( void ); // function of the driver named TIMER
 * extern oC_Mutex_t oC_Mutex_New       ( void ); // function of the module Mutex
 * extern void       oC_DevFs_FileOpen  ( void ); // function of the DevFs module - Device File System
 * @endcode
 *
 * <b>&13.</b> Each type should be defined used `typedef` keyword, and the name must **contain `_t` suffix**. It is for comfortable usage of types,
 * and for recognition of its.
 * Here some example:
 * @code{.c}
 * typedef uint8_t oC_Index_t;
 *
 * int main( void )
 * {
 *     oC_Index_t Index = 0;
 *
 *     return Index;
 * }
 * @endcode
 *
 * <b>&14.</b> **Avoid using shortcuts**. It is allowed only in special situations (in case of names longer than 60 characters or when the shortcut exist
 * in standard C libraries), and in modules names.
 *
 * <b>&15.</b> **Do not use `i`,`j`,`k` for index variables**. It is easier to find a bug and understand how it works, when the index is named as its
 * destination. For example:
 * @code{.c}
 * char usersNames[MAX_USERS][MAX_USER_NAME_LENGTH];
 *
 * for(int userIndex;userIndex<MAX_USERS;userIndex++)
 * {
 *     for(int characterIndex=0;characterIndex<MAX_USER_NAME_LENGTH;characterIndex++)
 *     {
 *         usersNames[userIndex][characterIndex] = '\0';
 *     }
 * }
 * @endcode
 *
 * <b>&16.</b> **Global variables** must starts with **uppercase for each word**. Moreover, when the variable is not static, it must contain prefix with
 * `oC_` and name of a module. For example:
 *
 * @code{.c}
 * extern void * oC_GPIO_DefaultConfigurations[];
 * static void * CurrentConfigurations[MAX_CONFIGURATIONS];
 * @endcode
 *
 * <b>&17.</b> **Local variables** must starts with **lowercase** and then must contain **uppercase for each word**. Some examples below:
 * @code{.c}
 * int main(void)
 * {
 *     int firstVariable  = 0;
 *     int secondVariable = 0;
 *     int result         = 0;
 *
 *     return result;
 * }
 * @endcode
 *
 * <b>&18.</b> **Name of variable** must describe, what it stores. It cannot contain predicates, and there should be noun at the end of the name.
 * Here examples:
 * @code{.c}
 * // this is GOOD
 * int     myVariable;
 * char    personName[];
 * bool    busyFlag;
 *
 * // this is BAD
 * uint8_t someWeird; //there is no noun
 * int     drink;
 * @endcode
 *
 * <b>&19.</b> **Output parameters** in functions must starts with `out` prefix as in example below:
 *
 * @code{.c}
 * static int ReadState( State_t * outState );
 * @endcode
 *
 * <b>&20.</b> **Name of function** must describe, what it do. It must be in order form, and it must contain predicate. There are only few exception
 * from this rule - `is` and `new` functions types. Function names should be written without words: 'a', 'the' and 'an'. Here some examples:
 * @code{.c}
 * // Some normal examples
 * static int AddUser( oC_User_t User );
 * static int RemoveUser( oC_User_t User );
 * static int RestartSystemAndWakeUpUser( oC_User_t User );
 *
 * // 'is' function type exception:
 * static bool IsCorrect( oC_User_t User );
 *
 * // 'new' function type exception:
 * static void * oC_Mutex_New( void );
 * @endcode
 *
 * <b>&21.</b> There is few special types of function, that should be defined always with following rules:
 *
 *  - `Set` - Sets parameter to the value. It must return oC_ErrorCode_t
 *  Example:
 *  @code{.c}
 *  static oC_ErrorCode_t SetMode( Mode_t Mode );
 *  @endcode
 *
 *  - `Read` - Reads parameter state and return it via function argument or reads data from a stream. It must get `outData` or `outBuffer`
 *  argument and must return oC_ErrorCode_t.
 *  Example:
 *  @code{.c}
 *  static oC_ErrorCode_t ReadMode( Mode_t* outMode);
 *  @endcode
 *
 *  - `Write` - Writes data to a stream, for example to the SPI channel queue. It must return oC_ErrorCode_t, and get `Data` or `Buffer` argument.
 *  Example:
 *  @code{.c}
 *  static oC_ErrorCode_t WriteData( uint8_t Data );
 *  @endcode
 *
 *  - `Get` - Returns parameter state via return value. It is important, that this function type is for reasons of speed operations, so
 *  it must not checks arguments.
 *  Example:
 *  @code{.c}
 *  static Mode_t GetMode( void );
 *  @endcode
 *
 *  - `Is` - Checks if the argument is correct. It must return `bool` type.
 *  Example:
 *  @code{.c}
 *  static bool IsCorrect( Mode_t Mode );
 *  @endcode
 *
 *  - `TurnOn` - Prepares a module to work. It must return oC_ErrorCode_t type.
 *  Example:
 *  @code{.c}
 *  oC_ErrorCode_t oC_GPIO_TurnOn( void );
 *  @endcode
 *
 *  - `TurnOff` - Release module. It must work after call of this function. Function must return oC_ErrorCode_t type.
 *  Example:
 *  @code{.c}
 *  oC_ErrorCode_t oC_GPIO_TurnOff( void );
 *  @endcode
 *
 *  - `New` - Allocates and initialize memory for an object. It must return a pointer to the allocated memory.
 *  Example:
 *  @code{.c}
 *  oC_User_t* oC_User_New( void );
 *  @endcode
 *
 *  - `Delete` - Deletes and free memory allocated for an object. It must take a double pointer (pointer to pointer) to the object,
 *  set the object pointer to the NULL, and return oC_ErrorCode_t type.
 *  Example:
 *  @code{.c}
 *  oC_ErrorCode_t oC_User_Delete( oC_User_t** User);
 *  @endcode
 *
 * <b>&22.</b> **Enumerators values** must starts with the name of the type (of course without `_t` suffix). Most of todays editors supports hints
 * system. Thanks to that it will be easier to find values that are assigned for the enum type (for example in eclipse press Ctrl+Space). There
 * is some example below, that illustrate this rule:
 *
 * @code{.c}
 * typedef enum
 * {
 *     ExampleType_FirstValue ,
 *     ExampleType_OtherValue ,
 *     ExampleType_LastValue
 * } ExampleType_t;
 *
 * // Now, we want to use this type:
 * ExampleType_t Value;
 *
 * Value = ExampleType_ // when you write this string, and press Ctrl+Space, you will get hints from the eclipse with values, that you can
 * // assign to this type.
 * @endcode
 *
 * <b>&23.</b> **Constant macros definitions** must be named uppercase, with `_` words seperated by `_` sign. Modules names in it should also be
 * written uppercase, but `oC_` stay without changes. Example:
 * @code{.c}
 * #define oC_GPIO_NUMBER_OF_PORTS    10
 * @endcode
 *
 * <b>&24.</b> **Function-like macros** should be named like functions
 * @code{.c}
 * #define oC_Machine_IsChannelCorrect(MODULE_NAME,Channel)
 * @endcode
 *
 * <b>&25.</b> **Hungarian notation is prohibited**! Do not use any suffixes or prefixes that means what type is stored in the variable.
 *
 * <b>&26.</b> **The sign** `=` in adjacent lines must always be aligned. The example below represent this rule:
 *
 * @code{.c}
 * int someVariable      = 0;
 * int someOtherVariable = 0;
 * @endcode
 *
 * <b>&27.</b> **Names of types** and **names of variables** in variables definitions should also be aligned like in example:
 * @code{.c}
 * SomeLongFooBarType_t fooBarVariable = 0;
 * int                  stringIndex    = 0;
 * char *               stringToReturn = NULL;
 * @endcode
 *
 * <b>&28.</b> Everytime when operation is **repeated in adjacent lines** without or with small changes, it should be aligned to emphasize these
 * differences. Example:
 * @code{.c}
 * CallFunction( Config, SomeVariable, Mode_Valid   );
 * CallFunction( Config, SomeVariable, Mode_Invalid );
 *
 * CallOtherFunction( Config, SomeOtherVariable );
 * CallOtherFunction( Config, SomeVariable      );
 * @endcode
 *
 * <b>&29.</b> Each type in the system must be defined using `typedef` keyword. It is easier to use.
 *
 * <b>&30.</b> Only name of the type is required. There is no reason to set also enum, or struct name. The example below shows this rule:
 *
 * @code{.c}
 * typedef enum _SomeEnumType_t
 * {
 *     SomeEnumType_ValueZero ,
 *     SomeEnumType_ValueOne
 * } SomeEnumType_t;
 * @endcode
 * The _SomeEnumType_t word is not necessary.
 *
 * <b>&31.</b> **Fields in structures** should be **ordered via size** from the biggest to the smallest one. This helps to save memory (alignment).
 * Example:
 * @code{.c}
 * typedef struct
 * {
 *      uint32_t        Buffer[100];
 *      uint32_t        BufferSize;
 *      uint16_t        Foo;
 *      uint8_t         Bar;
 * } ExampleStruct_t;
 * @endcode
 *
 * <b>&32.</b> **Interface functions must check all arguments**. It should be checked as much as possible. Note, that there are functions
 * in the memory manager (in kernel for core layer) and in the standard memory libraries, that helps to verify if the pointer is correct.
 * You should use them everywhere where it is a possible to give some pointer. Moreover it is possible to check the size of dynamic allocated
 * address (from `malloc` type functions).
 *
 * @code{.c}
 * oC_ErrorCode_t oC_GPIO_Configure( const oC_GPIO_Config_t * Config )
 * {
 *    oC_ErrorCode_t errorCode = oC_ErrorCode_ImlementError;
 *
 *    if( oC_MemMan_IsAddressCorrect( Config ) )
 *    {
 *        errorCode = oC_ErrorCode_None;
 *    }
 *    else
 *    {
 *        errorCode = oC_ErrorCode_WrongAddress;
 *    }
 *
 *    return errorCode;
 * }
 * @endcode
 *
 * <b>&33.</b> **Static and internal module functions must not check arguments**. It should be done in interface functions.
 *
 * <b>&34.</b> **Use `oC_ErrorCode_t` type for as status**. Dont be afraid to add new error codes, but always you could try to use existing.
 *
 * <b>&35.</b> **Error codes variables should in functions should be initialized as `oC_ErrorCode_ImplementError`**. Thanks to that, when
 * you will not set correct state for each condition in a function, you will receive an error, that you will find and track shortly. Remember,
 * that always it is better to return failure, when everything is OK, than return success, when something is wrong.
 * Example:
 * @code{.c}
 * oC_ErrorCode_t oC_GPIO_Configure( const oC_GPIO_Config_t * Config )
 * {
 *    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
 *
 *    if( oC_MemMan_IsAddressCorrect( Config ) )
 *    {
 *        if( oC_GPIO_LLD_Configure( Config ) )
 *        {
 *            if(oC_GPIO_LLD_SetState(Config , 0x00) == false)
 *            {
 *                errorCode = oC_ErrorCode_GPIOLLDCantSetState;
 *            }

 *        }
 *        else
 *        {
 *           errorCode = oC_ErrorCode_GPIOLLDConfigurationFailure;
 *        }
 *    }
 *    else
 *    {
 *        errorCode = oC_ErrorCode_WrongAddress;
 *    }
 *
 *    return errorCode;
 * }
 * @endcode
 *
 * <b>&36.</b> **Definitions of variables should be ordered via size** from the biggest to the smallest one. This helps to save memory (alignment).
 * Example:
 * @code{.c}
 * int main()
 * {
 *  uint32_t        Buffer[100];
 *  uint32_t        BufferSize;
 *  uint16_t        Foo;
 *  uint8_t         Bar;
 *
 *  // some code here
 * }
 * @endcode
 *
 * <b>&37.</b> **Each file must contain `oc_` prefix in the name**. (standard libraries are exceptions from this rule)
 *
 * <b>&38.</b> **File name** must be **lowercase** with `_` instead of spaces.
 * Example: `oc_gpio.h`
 *
 * <b>&39.</b> **File name** must contain also **module name**, example: `oc_gpio_defs.h`
 *
 * <b>&40.</b> **Functions can take maximum 5 arguments**. If some function need to more, it should be given using structures.
 *
 * TODO:
 * - file template description
 * - sections definitions
 * - protection again enabling enabled, and disable disabled modules
 * - definition of variable at the start of the block.
 * - functions cannot be longer than 60 lines
 * - oC_AssignErrorCodes in if ()
 * - one lines switches can be in one line with case and break
 * - error code return variable should be named errorCode
 * - &&, || operations at the start of the lines:
 * @code{.c}
   if( Some      == 1
    || SomeOther == 2
    )
   @endcode
 * - function comments layout
 * - files layout
 *
 *
 * Features:
 * - dynamic memory overflow detector
 * - allocators
 * - memory events
 * - heap maps
 * - redefinition for unsigned integer types everywhere, where it can be changed in the future
 */

#undef  _________________________________________CODING_STANDARD_SECTION____________________________________________________________________

/** ****************************************************************************************************************************************
 * The section with architecture
 */
#define _________________________________________ARCHITECTURE_SECTION_______________________________________________________________________

/**
 * @page Architecture Architecture
 *
 * @section Introduction Introduction
 *  ________________________________________________________________________________________________________________________________________
 *
 * The **Choco OS** system is divided on spaces that defines access to the system resources. Modules in the same space can communicate each
 * other, but it cannot use resources from higher layers. There is one exception of this rule: libraries space, that can be
 * accessed from each layer. Detailed description of each space is placed in a later section of this page. The picture below shows arrangement
 * of basic system spaces.
 *
 * ![Main Architecture](main_architecture.jpg)
 *
 * - @subpage UserSpace  - <i>Highest space of the system. It is more safe layer, but can be little slower than other layers.</i>
 * - @subpage CoreSpace  - <i>The space for system based and hardware independent operations.</i>
 * - @subpage PortableSpace - <i>Space for hardware definitions and operations on computer registers. It is depended of the target hardware.</i>
 * - @subpage LibrariesSpace - <i>Special space, that can be accessed from each layer, but it cannot use any of other layers.</i>
 *
 */

/* END OF SECTION */
#undef  _________________________________________ARCHITECTURE_SECTION_______________________________________________________________________

#endif /* CHOC_H_ */
