############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2024-11-26 - 18:07:40
##  Description:    makefile for tictactoe program
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
PROGRAM_NAME            = tictactoe
SPACE                   = CORE_SPACE
OPTIMALIZE              = O0
WARNING_FLAGS           = -Wall
CSTD                    = c99
DEFINITIONS             =
SOURCE_FILES            = main.c
INCLUDES_DIRS           = 
STANDARD_INPUT          = uart_stdio
STANDARD_OUTPUT         = uart_stdio
STANDARD_ERROR          = uart_stdio
HEAP_MAP_SIZE			= 0
PROCESS_STACK_SIZE      = 2048
ALLOCATION_LIMIT        = 0
TRACK_ALLOCATION        = FALSE
FILE_TYPE               = CBIN

##============================================================================================================================
##                                          
##              INCLUDE MAIN MAKEFILE               
##                                          
##============================================================================================================================
include $(PROGRAM_MK_FILE_PATH)
