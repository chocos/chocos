/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the kill program
 *
 * @author     Patryk Kubiak - (Created on: 2016-12-19 - 09:43:49) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_processman.h>

//==========================================================================================================================================
/**
 * @brief prints usage
 */
//==========================================================================================================================================
static void PrintUsage( int Argc , char ** Argv )
{
    printf("Usage: \n");
    printf("    %s [PID/PName]\n", Argv[0]);
    printf("        where:    \n");
    printf("            PID   - Process ID    (you can use PID or PName)\n");
    printf("            PName - Process Name  \n");
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
	oC_Process_t   process   = NULL;
	oC_UInt_t      pid       = 0;

	if(ErrorCondition( Argc >= 2, oC_ErrorCode_WrongCommandArgument ))
	{
	    if( ErrorCode( sscanf(Argv[1], "%d", &pid) ) )
	    {
	        process = oC_ProcessMan_GetProcessById(pid);
	    }
	    else
	    {
	        process = oC_ProcessMan_GetProcess(Argv[1]);
	    }

	    if(
            ErrorCondition( process != NULL          , oC_ErrorCode_WrongCommandArgument )
         && ErrorCondition( oC_Process_Kill(process) , oC_ErrorCode_CannotKillProcess    )
            )
	    {
	        printf(oC_VT100_FG_GREEN "Success\n");
	        errorCode = oC_ErrorCode_None;
	    }
	    else
	    {
	        oC_PrintErrorMessage("Cannot kill process: ", errorCode);
	    }
	}
	else
	{
	    PrintUsage(Argc,Argv);
	}

    return errorCode;
}
