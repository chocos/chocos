/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          show_upng.c
 *
 * @author     Patryk Kubiak - (Created on: 16.05.2017 00:23:06) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#if 0
//==========================================================================================================================================
/**
 * @brief shows PNG file
 */
//==========================================================================================================================================
static void ShowImage( upng_t * upng )
{
    oC_Screen_t     screen          = oC_ScreenMan_GetDefaultScreen();
    oC_ColorMap_t * colorMap        = NULL;

    oC_Screen_ReadColorMap(screen,&colorMap);

    if(colorMap != NULL)
    {
        const uint8_t * buffer = upng_get_buffer(upng);

        if(buffer != NULL)
        {
            unsigned width, height, depth;
            unsigned x, y, d;
            oC_Pixel_Position_t pos;

            width = upng_get_width(upng);
            height = upng_get_height(upng);
            depth = upng_get_bpp(upng) / 8;

            printf("");

            for( y = 0; y < height ; y++)
            {
                for( x = 0; x < width ; x++)
                {
                    oC_Color_t color = 0;

                    for (d = 0; d != depth; ++d) {
                        color |= buffer[(height - y - 1) * width * depth + x * depth + (depth - d - 1)] << (d*8);
                    }

                    pos.X = x;
                    pos.Y = height - y;

                    oC_ColorMap_SetColor(colorMap,pos,color,colorMap->ColorFormat);
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief shows the image from the path
 */
//==========================================================================================================================================
static void ShowImageFromPath( const char * Path )
{
    upng_t * upng = NULL;

    upng = upng_new_from_file(Path);
    if(upng_get_error(upng) == UPNG_EOK)
    {
        upng_decode(upng);

        if (upng_get_error(upng) == UPNG_EOK)
        {
            ShowImage(upng);
        }
        else
        {
            printf("Cannot decode upng!\n");
        }
        upng_free(upng);
    }
}

#endif
