/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          main2.c
 *
 * @author     Patryk Kubiak - (Created on: 01.06.2017 18:59:52) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define WIDGET_SCREEN_DEFINE_SHORT_TYPES
#include <oc_stdio.h>
#include <oc_color.h>
#include <oc_screenman.h>
#include <oc_array.h>
#include <oc_font.h>
#include <oc_list.h>

#include <oc_widgetscreen.h>

#define MAX_PLAYERS                 5
#define MAX_TRAINSTATIONS           3
#define MAX_DRAWN_TRACKS            3
#define NUMBER_OF_RAILCARS          45
#define POINTS_FOR_TRAINSTATION     4
#define POINTS_FOR_LONGEST_ROUTE    10
#define RAILCARS_TO_FINISH          2

typedef struct
{
    char *      ColorName;
    oC_Color_t  Color;
} PlayerColor_t;

typedef struct
{
    char *      CityA;
    char *      CityB;
    uint32_t    Score;
} Track_t;

typedef struct
{
    const char * CityA;
    const char * CityB;
    uint32_t     NumberOfRailcars;
} Route_t;

typedef struct
{
    uint32_t    NumberOfRailcars;
    uint32_t    Score;
} RoutePoints_t;

typedef struct
{
    bool            Used;
    const Route_t * Route;
} TrainStation_t;

typedef int16_t Score_t;

typedef uint8_t PlayerIndex_t;

typedef struct
{
    const PlayerColor_t *       Color;
    oC_List(const Track_t *)    Tracks;
    const Track_t *             DrawnTracks[MAX_DRAWN_TRACKS];
    oC_List(const Track_t *)    BuiltTracks;
    oC_List(const Route_t *)    BuiltRoutes;
    Score_t                     Score;
    int16_t                     RailcarsCount;
    TrainStation_t              TrainStations[MAX_TRAINSTATIONS];
    Route_t                     LongestRoute;
} PlayerState_t;

typedef enum
{
    ScreenId_Intro ,
    ScreenId_SelectNumberOfPlayers ,
    ScreenId_SelectPlayerColor ,
    ScreenId_SelectStartTracks ,
    ScreenId_SelectLongTrack ,
    ScreenId_SelectShortTrack ,
    ScreenId_PlayerStatus ,
    ScreenId_BuildTrainStation ,
    ScreenId_BuildRoute ,
    ScreenId_PrivatePlayerState ,
    ScreenId_DrawShortTracks ,
    ScreenId_FinishScreen ,

    ScreenId_NumberOfScreens ,
} ScreenId_t;

typedef enum
{
    WidgetIndex_SelectPlayerColor_Title ,
    WidgetIndex_SelectPlayerColor_NextPlayer ,
    WidgetIndex_SelectPlayerColor_PreviousPlayer ,
    WidgetIndex_SelectPlayerColor_PlayerColors
} WidgetIndex_SelectPlayerColor_t;

typedef enum
{
    WidgetIndex_SelectLongTrack_Title ,
    WidgetIndex_SelectLongTrack_AddTrack ,
    WidgetIndex_SelectLongTrack_Cancel ,
    WidgetIndex_SelectLongTrack_SetLongTrack ,
} WidgetIndex_SelectLongTrack_t;

typedef enum
{
    WidgetIndex_StartTracks_Title ,
    WidgetIndex_StartTracks_AddLong ,
    WidgetIndex_StartTracks_AddShort ,
    WidgetIndex_StartTracks_Previous ,
    WidgetIndex_StartTracks_Next ,
    WidgetIndex_StartTracks_RemoveTrack
} WidgetIndex_StartTracks_t;

typedef enum
{
    WidgetIndex_DrawTracks_Title ,
    WidgetIndex_DrawTracks_AddShort ,
    WidgetIndex_DrawTracks_Cancel ,
    WidgetIndex_DrawTracks_OK ,
    WidgetIndex_DrawTracks_RemoveTrack
} WidgetIndex_DrawTracks_t;

typedef enum
{
    WidgetIndex_SelectShortTrack_CityA ,
    WidgetIndex_SelectShortTrack_Routes ,
    WidgetIndex_SelectShortTrack_AddTrack ,
    WidgetIndex_SelectShortTrack_Cancel ,
    WidgetIndex_SelectShortTrack_ScrollUp ,
    WidgetIndex_SelectShortTrack_ScrollDown ,
    WidgetIndex_SelectShortTrack_SetShortTrack ,
    WidgetIndex_SelectShortTrack_SetCityA ,
    WidgetIndex_SelectShortTrack_FilteredRoutes ,
} WidgetIndex_SelectShortTrack_t;

typedef enum
{
    WidgetIndex_PrivateState_Tracks ,
    WidgetIndex_PrivateState_ScrollUp ,
    WidgetIndex_PrivateState_ScrollDown ,
    WidgetIndex_PrivateState_TracksToBuildButton ,
    WidgetIndex_PrivateState_BuiltTracksButton ,
    WidgetIndex_PrivateState_BuiltTrainStations ,
    WidgetIndex_PrivateState_BuiltRoutes ,
    WidgetIndex_PrivateState_PreviousScreen ,
} WidgetIndex_PrivateState_t;

typedef enum
{
    WidgetIndex_BuildTrainStation_CityA ,
    WidgetIndex_BuildTrainStation_ShowFilteredRoutes ,
    WidgetIndex_BuildTrainStation_Routes ,
    WidgetIndex_BuildTrainStation_SetCityA ,
    WidgetIndex_BuildTrainStation_FilteredRoutes ,
    WidgetIndex_BuildTrainStation_ScrollUp ,
    WidgetIndex_BuildTrainStation_ScrollDown ,
    WidgetIndex_BuildTrainStation_PreviousScreen ,
    WidgetIndex_BuildTrainStation_BuildTrainStation ,
} WidgetIndex_BuildTrainStation_t;

typedef enum
{
    WidgetIndex_BuildRoute_CityA ,
    WidgetIndex_BuildRoute_ShowFilteredRoutes ,
    WidgetIndex_BuildRoute_Routes ,
    WidgetIndex_BuildRoute_SetCityA ,
    WidgetIndex_BuildRoute_FilteredRoutes ,
    WidgetIndex_BuildRoute_ScrollUp ,
    WidgetIndex_BuildRoute_ScrollDown ,
    WidgetIndex_BuildRoute_Cancel ,
    WidgetIndex_BuildRoute_OK ,
} WidgetIndex_BuildRoute_t;

typedef struct Context_t
{
    PlayerState_t       PlayersStates[MAX_PLAYERS+1];
    PlayerIndex_t       PlayerIndex;
    PlayerIndex_t       PlayerCount;
    PlayerIndex_t       LongestRoutePlayerIndex;
    const Track_t *     Track;
    const Route_t *     Route;
    ScreenId_t          NextScreen;
    ScreenId_t          PreviousScreen;
    uint32_t            NumberOfCities;
    const char **       Cities;
    const char *        City;
    PlayerIndex_t       Ranking[MAX_PLAYERS];
} GameState_t;

typedef struct
{
    const Route_t **    Routes;
    uint32_t            ArraySize;
    uint32_t            NumberOfRoutes;
    int16_t             RouteLength;
    int16_t             MaxRouteLength;
} TrackPath_t;

//==========================================================================================================================================
//
//                                                  LOCAL PROTOTYPES
//
//==========================================================================================================================================
static ScreenId_t PrepareScreen_SelectNumberOfPlayers       ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_SelectPlayerColor           ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_SelectStartTracks           ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_SelectLongTracks            ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_SelectShortTracks           ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_PlayerStatus                ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_PrivatePlayerState          ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_BuildTrainStation           ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_BuildRoute                  ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_DrawShortTracks             ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t PrepareScreen_FinishScreen                ( Screen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );

static ScreenId_t HandleScreen_Intro                        ( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );
static ScreenId_t HandleScreen_PlayerStatus                 ( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID );

static void       UpdateWidgetString_NumberOfPlayers        ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_PlayerName             ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_WonPlayerName          ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_LongestRoute           ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_LongestRoute           ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_WonPoints              ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_2ndPlacePoints         ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_3rdPlacePoints         ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_4thPlacePoints         ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_5thPlacePoints         ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_PlayerPoints           ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );
static void       UpdateWidgetString_PlayerNumberOfRailcars ( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );


static ScreenId_t HandleWidget_IncrementNumberOfPlayers     ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_DecrementNumberOfPlayers     ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_NextScreen                   ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_PreviousScreen               ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_AddDrawnTracks               ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_NextPlayer                   ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_PreviousPlayer               ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_SetPlayerColor               ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowSelectLongTrackScreen    ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowSelectShortTrackScreen   ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowDrawShortTracksScreen    ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_SelectCityA                  ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_SelectFilteredRoutes         ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_AddTrack                     ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_BuildRoute                   ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_RemoveUserTrack              ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_RemoveDrawTrack              ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_SetLongTrack                 ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_SetShortTrack                ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_SetCityA                     ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_SetRoute                     ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ScrollShortTracksUp          ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ScrollShortTracksDown        ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ScrollRoutesUp               ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ScrollRoutesDown             ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowBuildTrainStationScreen  ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowBuildRouteScreen         ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowPrivatePlayerStateScreen ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowBuiltTracks              ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowTracksToBuild            ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowBuiltTrainStations       ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ShowBuiltRoutes              ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ScrollPlayerTracksUp         ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ScrollPlayerTracksDown       ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_BuildTrainStation            ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );
static ScreenId_t HandleWidget_ExitPressed                  ( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen );

static bool       IsTrackBuilt                              ( GameState_t * GameState, PlayerState_t * PlayerState , const Track_t * Track );
static void       CountLongestRoute                         ( PlayerState_t * PlayerState , Route_t * outRoute );
static bool       PreparePlayerState                        ( PlayerState_t * PlayerState );
static bool       PrepareGameState                          ( GameState_t * GameState );

//==========================================================================================================================================
/**
 * List of possible player colors
 */
//==========================================================================================================================================
static const PlayerColor_t PlayerColors[] = {
    { .ColorName = "Black"      ,  .Color = 0x000000  } ,
    { .ColorName = "Yellow"     ,  .Color = 0xfee91b  } ,
    { .ColorName = "Red"        ,  .Color = 0xe41909  } ,
    { .ColorName = "Green"      ,  .Color = 0x93b905  } ,
    { .ColorName = "Blue"       ,  .Color = 0x019fdd  } ,
};

//==========================================================================================================================================
/**
 * List of available of long tracks
 */
//==========================================================================================================================================
static const Track_t LongTracks[] = {
    { .CityA = "Brest"      ,  .CityB = "Petrogard"   ,  .Score = 20 } ,
    { .CityA = "Edinburgh"  ,  .CityB = "Athina"      ,  .Score = 21 } ,
    { .CityA = "Cadiz"      ,  .CityB = "Stockholm"   ,  .Score = 21 } ,
    { .CityA = "Palermo"    ,  .CityB = "Moskva"      ,  .Score = 20 } ,
    { .CityA = "Lisboa"     ,  .CityB = "Danzig"      ,  .Score = 20 } ,
    { .CityA = "Kobenhavn"  ,  .CityB = "Erzurum"     ,  .Score = 21 } ,
};

//==========================================================================================================================================
/**
 * List of available of short tracks
 */
//==========================================================================================================================================
static const Track_t ShortTracks[] = {
    { .CityA = "Riga"           ,  .CityB = "Bucuresti"     ,  .Score = 10 } ,
    { .CityA = "London"         ,  .CityB = "Wien"          ,  .Score = 10 } ,
    { .CityA = "Barcelona"      ,  .CityB = "Bruxelles"     ,  .Score = 8  } ,
    { .CityA = "Frankfurt"      ,  .CityB = "Kobenhavn"     ,  .Score = 5  } ,
    { .CityA = "Zurich"         ,  .CityB = "Budapest"      ,  .Score = 6  } ,
    { .CityA = "Smolensk"       ,  .CityB = "Rostov"        ,  .Score = 8  } ,
    { .CityA = "Edinburgh"      ,  .CityB = "Paris"         ,  .Score = 7  } ,
    { .CityA = "Paris"          ,  .CityB = "Zagrab"        ,  .Score = 7  } ,
    { .CityA = "London"         ,  .CityB = "Berlin"        ,  .Score = 7  } ,
    { .CityA = "Berlin"         ,  .CityB = "Moskva"        ,  .Score = 12 } ,
    { .CityA = "Amsterdam"      ,  .CityB = "Wilno"         ,  .Score = 12 } ,
    { .CityA = "Barcelona"      ,  .CityB = "Munchen"       ,  .Score = 8  } ,
    { .CityA = "Madrid"         ,  .CityB = "Dieppe"        ,  .Score = 8  } ,
    { .CityA = "Bruxelles"      ,  .CityB = "Danzig"        ,  .Score = 9  } ,
    { .CityA = "Kyiv"           ,  .CityB = "Sochi"         ,  .Score = 8  } ,
    { .CityA = "Zurich"         ,  .CityB = "Brindisi"      ,  .Score = 6  } ,
    { .CityA = "Berlin"         ,  .CityB = "Roma"          ,  .Score = 9  } ,
    { .CityA = "Essen"          ,  .CityB = "Kyiv"          ,  .Score = 10 } ,
    { .CityA = "Marseille"      ,  .CityB = "Essen"         ,  .Score = 8  } ,
    { .CityA = "Paris"          ,  .CityB = "Wien"          ,  .Score = 8  } ,
    { .CityA = "Venezia"        ,  .CityB = "Constantinople",  .Score = 10 } ,
    { .CityA = "Athina"         ,  .CityB = "Angora"        ,  .Score = 5  } ,
    { .CityA = "Kyiv"           ,  .CityB = "Petrogard"     ,  .Score = 6  } ,
    { .CityA = "Stockholm"      ,  .CityB = "Wien"          ,  .Score = 11 } ,
    { .CityA = "Amsterdam"      ,  .CityB = "Pamplona"      ,  .Score = 7  } ,
    { .CityA = "Berlin"         ,  .CityB = "Bucuresti"     ,  .Score = 8  } ,
    { .CityA = "Frankfurt"      ,  .CityB = "Smolensk"      ,  .Score = 13 } ,
    { .CityA = "Palermo"        ,  .CityB = "Constantinople",  .Score = 8  } ,
    { .CityA = "Sarajevo"       ,  .CityB = "Sevastopol"    ,  .Score = 8  } ,
    { .CityA = "Angora"         ,  .CityB = "Kharkov"       ,  .Score = 10 } ,
    { .CityA = "Warszawa"       ,  .CityB = "Smolensk"      ,  .Score = 6  } ,
    { .CityA = "Madrid"         ,  .CityB = "Zurich"        ,  .Score = 8  } ,
    { .CityA = "Budapest"       ,  .CityB = "Sofia"         ,  .Score = 5  } ,
    { .CityA = "Sofia"          ,  .CityB = "Smyrna"        ,  .Score = 5  } ,
    { .CityA = "Zagrab"         ,  .CityB = "Brindisi"      ,  .Score = 6  } ,
    { .CityA = "Athina"         ,  .CityB = "Wilno"         ,  .Score = 11 } ,
    { .CityA = "Rostov"         ,  .CityB = "Erzurum"       ,  .Score = 5  } ,
    { .CityA = "Roma"           ,  .CityB = "Smyrna"        ,  .Score = 8  } ,
    { .CityA = "Brest"          ,  .CityB = "Venezia"       ,  .Score = 8  } ,
    { .CityA = "Brest"          ,  .CityB = "Marseille"     ,  .Score = 7  } ,
};
//==========================================================================================================================================
/**
 * @brief list of available routes
 */
//==========================================================================================================================================
static const Route_t Routes[] = {
    { .CityA = "London"         , .CityB = "Edinburgh"      , .NumberOfRailcars = 4 } ,
    { .CityA = "London"         , .CityB = "Dieppe"         , .NumberOfRailcars = 2 } ,
    { .CityA = "London"         , .CityB = "Amsterdam"      , .NumberOfRailcars = 2 } ,
    { .CityA = "Dieppe"         , .CityB = "Brest"          , .NumberOfRailcars = 2 } ,
    { .CityA = "Dieppe"         , .CityB = "Paris"          , .NumberOfRailcars = 1 } ,
    { .CityA = "Brest"          , .CityB = "Paris"          , .NumberOfRailcars = 3 } ,
    { .CityA = "Bruxelles"      , .CityB = "Dieppe"         , .NumberOfRailcars = 2 } ,
    { .CityA = "Bruxelles"      , .CityB = "Paris"          , .NumberOfRailcars = 2 } ,
    { .CityA = "Bruxelles"      , .CityB = "Amsterdam"      , .NumberOfRailcars = 1 } ,
    { .CityA = "Bruxelles"      , .CityB = "Frankfurt"      , .NumberOfRailcars = 2 } ,
    { .CityA = "Paris" , .CityB = "Frankfurt", .NumberOfRailcars = 3 } ,
    { .CityA = "Pamplona" , .CityB = "Brest", .NumberOfRailcars = 4 } ,
    { .CityA = "Pamplona" , .CityB = "Paris", .NumberOfRailcars = 4 } ,
    { .CityA = "Zurich" , .CityB = "Paris", .NumberOfRailcars = 3 } ,
    { .CityA = "Marseille" , .CityB = "Paris", .NumberOfRailcars = 4 } ,
    { .CityA = "Pamplona" , .CityB = "Madrid", .NumberOfRailcars = 3 } ,
    { .CityA = "Madrid" , .CityB = "Lisboa", .NumberOfRailcars = 3 } ,
    { .CityA = "Lisboa" , .CityB = "Cadiz", .NumberOfRailcars = 2 } ,
    { .CityA = "Cadiz" , .CityB = "Madrid", .NumberOfRailcars = 3 } ,
    { .CityA = "Madrid" , .CityB = "Barcelona", .NumberOfRailcars = 2 } ,
    { .CityA = "Barcelona" , .CityB = "Pamplona", .NumberOfRailcars = 2 } ,
    { .CityA = "Barcelona" , .CityB = "Marseille", .NumberOfRailcars = 4 } ,
    { .CityA = "Pamplona" , .CityB = "Marseille", .NumberOfRailcars = 4 } ,
    { .CityA = "Marseille" , .CityB = "Zurich", .NumberOfRailcars = 2 } ,
    { .CityA = "Marseille" , .CityB = "Roma", .NumberOfRailcars = 4 } ,
    { .CityA = "Zurich" , .CityB = "Venezia", .NumberOfRailcars = 2 } ,
    { .CityA = "Zurich" , .CityB = "Munchen", .NumberOfRailcars = 2 } ,
    { .CityA = "Munchen" , .CityB = "Venezia", .NumberOfRailcars = 2 } ,
    { .CityA = "Venezia" , .CityB = "Roma", .NumberOfRailcars = 2 } ,
    { .CityA = "Frankfurt" , .CityB = "Munchen", .NumberOfRailcars = 2 } ,
    { .CityA = "Roma" , .CityB = "Palermo", .NumberOfRailcars = 4 } ,
    { .CityA = "Roma" , .CityB = "Brindisi", .NumberOfRailcars = 2 } ,
    { .CityA = "Brindisi" , .CityB = "Palermo", .NumberOfRailcars = 3 } ,
    { .CityA = "Munchen" , .CityB = "Wien", .NumberOfRailcars = 3 } ,
    { .CityA = "Palermo" , .CityB = "Smyrna", .NumberOfRailcars = 6 } ,
    { .CityA = "Brindisi" , .CityB = "Athina", .NumberOfRailcars = 4 } ,
    { .CityA = "Smyrna" , .CityB = "Angora", .NumberOfRailcars = 3 } ,
    { .CityA = "Athina" , .CityB = "Smyrna", .NumberOfRailcars = 2 } ,
    { .CityA = "Angora" , .CityB = "Constantinople", .NumberOfRailcars = 2 } ,
    { .CityA = "Constantinople" , .CityB = "Smyrna", .NumberOfRailcars = 2 } ,
    { .CityA = "Sofia" , .CityB = "Athina", .NumberOfRailcars = 3 } ,
    { .CityA = "Sofia" , .CityB = "Constantinople", .NumberOfRailcars = 3 } ,
    { .CityA = "Athina" , .CityB = "Sarajevo", .NumberOfRailcars = 4 } ,
    { .CityA = "Zagrab" , .CityB = "Sarajevo", .NumberOfRailcars = 3 } ,
    { .CityA = "Venezia" , .CityB = "Zagrab", .NumberOfRailcars = 2 } ,
    { .CityA = "Sarajevo" , .CityB = "Sofia", .NumberOfRailcars = 2 } ,
    { .CityA = "Sofia" , .CityB = "Bucuresti", .NumberOfRailcars = 2 } ,
    { .CityA = "Bucuresti" , .CityB = "Constantinople", .NumberOfRailcars = 3 } ,
    { .CityA = "Constantinople" , .CityB = "Sevastopol", .NumberOfRailcars = 4 } ,
    { .CityA = "Angora" , .CityB = "Erzurum", .NumberOfRailcars = 3 } ,
    { .CityA = "Erzurum" , .CityB = "Sochi", .NumberOfRailcars = 3 } ,
    { .CityA = "Sevastopol" , .CityB = "Erzurum", .NumberOfRailcars = 4 } ,
    { .CityA = "Sevastopol" , .CityB = "Sochi", .NumberOfRailcars = 2 } ,
    { .CityA = "Rostov" , .CityB = "Sochi", .NumberOfRailcars = 2 } ,
    { .CityA = "Rostov" , .CityB = "Sevastopol", .NumberOfRailcars = 4 } ,
    { .CityA = "Sevastopol" , .CityB = "Bucuresti", .NumberOfRailcars = 4 } ,
    { .CityA = "Bucuresti" , .CityB = "Budapest", .NumberOfRailcars = 4 } ,
    { .CityA = "Budapest" , .CityB = "Sarajevo", .NumberOfRailcars = 3 } ,
    { .CityA = "Zagrab" , .CityB = "Budapest", .NumberOfRailcars = 2 } ,
    { .CityA = "Wien" , .CityB = "Zagrab", .NumberOfRailcars = 2 } ,
    { .CityA = "Wien" , .CityB = "Budapest", .NumberOfRailcars = 1 } ,
    { .CityA = "Budapest" , .CityB = "Kyiv", .NumberOfRailcars = 6 } ,
    { .CityA = "Kyiv" , .CityB = "Bucuresti", .NumberOfRailcars = 4 } ,
    { .CityA = "Amsterdam" , .CityB = "Essen", .NumberOfRailcars = 3 } ,
    { .CityA = "Amsterdam" , .CityB = "Frankfurt", .NumberOfRailcars = 2 } ,
    { .CityA = "Essen" , .CityB = "Frankfurt", .NumberOfRailcars = 2 } ,
    { .CityA = "Frankfurt" , .CityB = "Berlin", .NumberOfRailcars = 3 } ,
    { .CityA = "Essen" , .CityB = "Berlin", .NumberOfRailcars = 2 } ,
    { .CityA = "Berlin" , .CityB = "Wien", .NumberOfRailcars = 3 } ,
    { .CityA = "Essen" , .CityB = "Kobenhavn", .NumberOfRailcars = 3 } ,
    { .CityA = "Kobenhavn" , .CityB = "Stockholm", .NumberOfRailcars = 3 } ,
    { .CityA = "Berlin" , .CityB = "Danzig", .NumberOfRailcars = 4 } ,
    { .CityA = "Berlin" , .CityB = "Warszawa"   , .NumberOfRailcars = 4 } ,
    { .CityA = "Wien" , .CityB = "Warszawa"     , .NumberOfRailcars = 4 } ,
    { .CityA = "Danzig" , .CityB = "Warszawa"   , .NumberOfRailcars = 2 } ,
    { .CityA = "Warszawa" , .CityB = "Kyiv"     , .NumberOfRailcars = 4 } ,
    { .CityA = "Warszawa" , .CityB = "Wilno"    , .NumberOfRailcars = 3 } ,
    { .CityA = "Riga" , .CityB = "Wilno"        , .NumberOfRailcars = 4 } ,
    { .CityA = "Wilno" , .CityB = "Petrogard"   , .NumberOfRailcars = 4 } ,
    { .CityA = "Danzig" , .CityB = "Riga"       , .NumberOfRailcars = 3 } ,
    { .CityA = "Riga" , .CityB = "Petrogard"    , .NumberOfRailcars = 4 } ,
    { .CityA = "Wilno" , .CityB = "Kyiv"        , .NumberOfRailcars = 2 } ,
    { .CityA = "Kyiv" , .CityB = "Kharkov"      , .NumberOfRailcars = 4 } ,
    { .CityA = "Wilno" , .CityB = "Smolensk"    , .NumberOfRailcars = 3 } ,
    { .CityA = "Kyiv" , .CityB = "Smolensk"     , .NumberOfRailcars = 3 } ,
    { .CityA = "Smolensk" , .CityB = "Moskva"   , .NumberOfRailcars = 2 } ,
    { .CityA = "Moskva" , .CityB = "Kharkov"    , .NumberOfRailcars = 4 } ,
    { .CityA = "Kharkov" , .CityB = "Rostov"    , .NumberOfRailcars = 2 } ,
    { .CityA = "Petrogard" , .CityB = "Moskva"  , .NumberOfRailcars = 4 } ,
    { .CityA = "Stockholm" , .CityB = "Petrogard", .NumberOfRailcars= 8 } ,
};
//==========================================================================================================================================
/**
 * Number of points for building a route
 */
//==========================================================================================================================================
static const RoutePoints_t RoutePoints[] = {
    { .NumberOfRailcars = 1     , .Score = 1 } ,
    { .NumberOfRailcars = 2     , .Score = 2 } ,
    { .NumberOfRailcars = 3     , .Score = 4 } ,
    { .NumberOfRailcars = 4     , .Score = 7 } ,
    { .NumberOfRailcars = 6     , .Score = 15 } ,
    { .NumberOfRailcars = 8     , .Score = 21 } ,
};
//==========================================================================================================================================
/**
 * List of colors for different widget states
 */
//==========================================================================================================================================
static const Palette_t  Palette = {
    [WidgetState_Ready] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0xC9D0A6 ,
           .FillColor   = 0x2C4051 ,
           .BorderColor = 0x3F7E98 ,
    } ,
    [WidgetState_Activated] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0x3F7E98 ,
           .FillColor   = 0xB5BE77 ,
           .BorderColor = 0xC9D0A6 ,
    } ,
    [WidgetState_Pressed] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0x3F7E98 ,
           .FillColor   = 0xB5BE77 ,
           .BorderColor = 0xC9D0A6 ,
    } ,
    [WidgetState_Hovered] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0x3F7E98 ,
           .FillColor   = 0xB5BE77 ,
           .BorderColor = 0x1E2124 ,
    } ,
    [WidgetState_Disabled] = {
           .ColorFormat = oC_ColorFormat_RGB888 ,
           .TextColor   = 0x1E2124 ,
           .FillColor   = 0x1E2124 ,
           .BorderColor = 0x2C4051 ,
    } ,
};
//==========================================================================================================================================
/**
 * Stores style of drawing widgets
 */
//==========================================================================================================================================
static const DrawStyle_t DrawStyle = {
    [WidgetState_Ready] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Dashed ,
           .BorderWidth = 1 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Pressed] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Activated] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Hovered] = {
           .BorderStyle = BorderStyle_Rounded | BorderStyle_Solid ,
           .BorderWidth = 3 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
    [WidgetState_Disabled] = {
           .BorderStyle = BorderStyle_None ,
           .BorderWidth = 0 ,
           .DrawFill    = true ,
           .DrawText    = true ,
           .Opacity     = 0xFF ,
    } ,
};

//==========================================================================================================================================
/**
 * List of widgets used for introduction screen
 */
//==========================================================================================================================================
static const WidgetDefinition_t IntroWidgets[] = {
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString      = "Ticket2Ride" ,
            .Font               = oC_Font_(Castellar_28pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 100 } ,
        .Height     = 100 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString  = "Please wait" ,
            .Font           = oC_Font_(ShowcardGothic_36pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
    } ,
};
//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t SelectNumberOfPlayersWidgets[] = {
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString  = "Number of Players" ,
            .Font           = oC_Font_(Castellar_28pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 190 , .Y = 50 } ,
        .Height     = 40 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_IncrementNumberOfPlayers ,
        } ,
        .String     = {
            .DefaultString  = { 0 } ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowUp
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 190 , .Y = 200 } ,
        .Height     = 40 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_DecrementNumberOfPlayers ,
        } ,
        .String     = {
            .DefaultString  = { 0 } ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowDown
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 190 , .Y = 100 } ,
        .Height     =  90 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "1" ,
            .Font                   = oC_Font_(ShowcardGothic_36pt) ,
            .TextAlign                  = TextAlign_Center ,
            .VerticalTextAlign          = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_NumberOfPlayers ,
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_NextScreen ,
        } ,
        .String     = {
            .DefaultString  = "Next" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen ,
        } ,
        .String     = {
            .DefaultString  = "Exit" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
};
//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t SelectPlayerColorWidgets[] = {
    [WidgetIndex_SelectPlayerColor_Title] = {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "Player Color" ,
            .Font                   = oC_Font_(Castellar_28pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_PlayerName ,
        } ,
    } ,
    [WidgetIndex_SelectPlayerColor_NextPlayer] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_NextPlayer ,
        } ,
        .String     = {
            .DefaultString  = "Next" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectPlayerColor_PreviousPlayer] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousPlayer ,
        } ,
        .String     = {
            .DefaultString  = "Back" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectPlayerColor_PlayerColors] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  70 } ,
        .Height     = 120 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetPlayerColor ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 60 ,
            .OptionHeight           = 60 ,
            .HorizontalCellSpacing  = 10 ,
            .VerticalCellSpacing    = 10 ,
        } ,
    } ,
};
//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t SelectStartTracksWidgets[] = {
    [WidgetIndex_StartTracks_Title] = {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "Start tracks" ,
            .Font                   = oC_Font_(Castellar_28pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_PlayerName ,
        } ,
    } ,
    [WidgetIndex_StartTracks_Next] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_NextPlayer ,
        } ,
        .String     = {
            .DefaultString      = "Next" ,
            .Font               = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_StartTracks_Previous] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen ,
        } ,
        .String     = {
            .DefaultString  = "Back" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_StartTracks_AddLong] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  10 , .Y = 60 } ,
        .Height     =  30 ,
        .Width      = 220 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowSelectLongTrackScreen ,
        } ,
        .String     = {
            .DefaultString  = "Add Long" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_StartTracks_AddShort] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 240 , .Y = 60 } ,
        .Height     =  30 ,
        .Width      = 220 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowSelectShortTrackScreen ,
        } ,
        .String     = {
            .DefaultString  = "Add Short" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_StartTracks_RemoveTrack] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  100 } ,
        .Height     = 120 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_RemoveUserTrack ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  35 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
};

//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t SelectLongTrackWidgets[] = {
    [WidgetIndex_SelectLongTrack_Title] = {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString  = "Select track" ,
            .Font           = oC_Font_(Castellar_28pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
    } ,
    [WidgetIndex_SelectLongTrack_AddTrack] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_AddTrack ,
        } ,
        .String     = {
            .DefaultString  = "Add" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectLongTrack_Cancel] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen ,
        } ,
        .String     = {
            .DefaultString  = "Cancel" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectLongTrack_SetLongTrack] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  70 } ,
        .Height     = 172 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetLongTrack ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  35 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
};

//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t SelectShortTrackWidgets[] = {
    [WidgetIndex_SelectShortTrack_CityA] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 175 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SelectCityA ,
        } ,
        .String     = {
            .DefaultString      = "City A" ,
            .Font               = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_Routes] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 290 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 175 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SelectFilteredRoutes ,
        } ,
        .String     = {
            .DefaultString      = "Filtered" ,
            .Font               = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_AddTrack] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_AddTrack ,
        } ,
        .String     = {
            .DefaultString  = "Add" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_Cancel] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen ,
        } ,
        .String     = {
            .DefaultString  = "Cancel" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_ScrollUp] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  200 , .Y = 10 } ,
        .Height     =  60 ,
        .Width      =  80 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollShortTracksUp ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowUp
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_ScrollDown] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  200 , .Y = 210 } ,
        .Height     =  60 ,
        .Width      =  80 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollShortTracksDown ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowDown
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_SetShortTrack] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 130 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetShortTrack ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_FilteredRoutes] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 130 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetShortTrack ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
    [WidgetIndex_SelectShortTrack_SetCityA] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 130 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetCityA ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
};
//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t PlayerStatusWidgets[] = {
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "Player Color" ,
            .Font                   = oC_Font_(Castellar_28pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_PlayerName ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 50 } ,
        .Height     = 40 ,
        .Width      = 112 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "0 pts" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_PlayerPoints ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 122 , .Y = 50 } ,
        .Height     = 40 ,
        .Width      = 113 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "3 RC" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_PlayerNumberOfRailcars ,
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  10 , .Y = 100 } ,
        .Height     =   70 ,
        .Width      =  225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowDrawShortTracksScreen,
        } ,
        .String     = {
            .DefaultString      = "Draw Track" ,
            .Font               = oC_Font_(BlackadderITC_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 10 , .Y = 170 } ,
        .Height     =  70 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_NextPlayer,
        } ,
        .String     = {
            .DefaultString      = "Draw Railcars" ,
            .Font               = oC_Font_(BlackadderITC_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 235 , .Y = 100 } ,
        .Height     =  70 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowBuildTrainStationScreen,
        } ,
        .String     = {
            .DefaultString  = "Build Trainstation" ,
            .Font           = oC_Font_(BlackadderITC_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 235 , .Y = 170 } ,
        .Height     =  70 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowBuildRouteScreen,
        } ,
        .String     = {
            .DefaultString      = "Build Route" ,
            .Font               = oC_Font_(BlackadderITC_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 235 , .Y = 50 } ,
        .Height     =  40 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowPrivatePlayerStateScreen,
        } ,
        .String     = {
            .DefaultString  = "Private" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
};

//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t PrivatePlayerStateWidgets[] = {
    [WidgetIndex_PrivateState_PreviousScreen] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 370 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      =  80 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen,
        } ,
        .String     = {
            .DefaultString  = "OK" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_PrivateState_BuiltTracksButton] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 200 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowBuiltTracks,
        } ,
        .String     = {
            .DefaultString  = "BUILT TRACKS" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_PrivateState_TracksToBuildButton] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 250 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 200 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowTracksToBuild,
        } ,
        .String     = {
            .DefaultString  = "TRACKS TO BUILD" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_PrivateState_BuiltTrainStations] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 10 , .Y = 40 } ,
        .Height     =  30 ,
        .Width      = 200 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowBuiltTrainStations,
        } ,
        .String     = {
            .DefaultString  = "TRAIN STATIONS" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_PrivateState_BuiltRoutes] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 250 , .Y = 40 } ,
        .Height     =  30 ,
        .Width      = 200 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowBuiltRoutes,
        } ,
        .String     = {
            .DefaultString  = "ROUTES" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_PrivateState_ScrollUp] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  215 , .Y = 40 } ,
        .Height     =  30 ,
        .Width      =  30 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollPlayerTracksUp ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowUp
        } ,
    } ,
    [WidgetIndex_PrivateState_ScrollDown] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  215 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      =  30 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollPlayerTracksDown ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowDown
        } ,
    } ,
    [WidgetIndex_PrivateState_Tracks] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 180 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = NULL ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
};

//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t BuildTrainStationWidgets[] = {
    [WidgetIndex_BuildTrainStation_CityA] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 175 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SelectCityA ,
        } ,
        .String     = {
            .DefaultString      = "City A" ,
            .Font               = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_BuildTrainStation_ShowFilteredRoutes] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 290 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 175 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SelectFilteredRoutes ,
        } ,
        .String     = {
            .DefaultString      = "Filtered" ,
            .Font               = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_BuildTrainStation_BuildTrainStation] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_BuildTrainStation ,
        } ,
        .String     = {
            .DefaultString  = "Build" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_BuildTrainStation_PreviousScreen] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen ,
        } ,
        .String     = {
            .DefaultString  = "Cancel" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_BuildTrainStation_ScrollUp] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  200 , .Y = 10 } ,
        .Height     =  60 ,
        .Width      =  80 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollRoutesUp ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowUp
        } ,
    } ,
    [WidgetIndex_BuildTrainStation_ScrollDown] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  200 , .Y = 210 } ,
        .Height     =  60 ,
        .Width      =  80 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollRoutesDown,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowDown
        } ,
    } ,
    [WidgetIndex_BuildTrainStation_Routes] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 140 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetRoute ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
    [WidgetIndex_BuildRoute_FilteredRoutes    ] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 140 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetRoute ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
    [WidgetIndex_BuildTrainStation_SetCityA    ] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 140 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetCityA ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
};
//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t BuildRouteWidgets[] = {
    [WidgetIndex_BuildRoute_CityA] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 175 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SelectCityA ,
        } ,
        .String     = {
            .DefaultString      = "City A" ,
            .Font               = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_BuildRoute_ShowFilteredRoutes] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 290 , .Y = 10 } ,
        .Height     =  30 ,
        .Width      = 175 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SelectFilteredRoutes ,
        } ,
        .String     = {
            .DefaultString      = "Filtered" ,
            .Font               = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_BuildRoute_Routes     ] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 140 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetRoute ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
    [WidgetIndex_BuildRoute_SetCityA    ] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 140 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetCityA ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
    [WidgetIndex_BuildRoute_FilteredRoutes ] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  80 } ,
        .Height     = 140 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_SetRoute ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = oC_Font_(Arial_10pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 220 ,
            .OptionHeight           =  30 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
    [WidgetIndex_BuildRoute_ScrollUp   ] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  200 , .Y = 10 } ,
        .Height     =  60 ,
        .Width      =  80 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollRoutesUp ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowUp
        } ,
    } ,
    [WidgetIndex_BuildRoute_ScrollDown ] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  200 , .Y = 210 } ,
        .Height     =  60 ,
        .Width      =  80 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ScrollRoutesDown ,
        } ,
        .String     = {
            .DefaultString  = "" ,
            .Font           = NULL ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_ArrowDown
        } ,
    } ,
    [WidgetIndex_BuildRoute_Cancel     ] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen ,
        } ,
        .String     = {
            .DefaultString  = "Cancel" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_BuildRoute_OK         ] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_BuildRoute ,
        } ,
        .String     = {
            .DefaultString      = "OK" ,
            .Font               = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
};

//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t DrawTracksWidgets[] = {
    [WidgetIndex_DrawTracks_Title] = {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "Select Tracks" ,
            .Font                   = oC_Font_(Castellar_28pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_PlayerName ,
        } ,
    } ,
    [WidgetIndex_DrawTracks_OK] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 360 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_AddDrawnTracks ,
        } ,
        .String     = {
            .DefaultString  = "OK" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_DrawTracks_Cancel] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X =  20 , .Y = 230 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_PreviousScreen ,
        } ,
        .String     = {
            .DefaultString  = "Cancel" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_DrawTracks_AddShort] = {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 220 , .Y = 60 } ,
        .Height     =  30 ,
        .Width      = 200 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ShowSelectShortTrackScreen ,
        } ,
        .String     = {
            .DefaultString  = "Add Track" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
    [WidgetIndex_DrawTracks_RemoveTrack] = {
        .Type       = WidgetType_RadioButton ,
        .Position   = { .X =  10 , .Y =  100 } ,
        .Height     = 120 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_RemoveDrawTrack ,
        } ,
        .TypeSpecific.RadioButton = {
            .OptionWidth            = 200 ,
            .OptionHeight           =  35 ,
            .HorizontalCellSpacing  =  10 ,
            .VerticalCellSpacing    =  10 ,
        } ,
    } ,
};

//==========================================================================================================================================
/**
 * List of widgets used for screen for choosing number of players
 */
//==========================================================================================================================================
static const WidgetDefinition_t FinishWidgets[] = {
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 10 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "WINNER" ,
            .Font                   = oC_Font_(Castellar_28pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_WonPlayerName ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 50 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "LONGEST TRACK" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_LongestRoute ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 90 } ,
        .Height     = 40 ,
        .Width      = 450 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "won score" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_WonPoints ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 130 } ,
        .Height     = 40 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "2nd place" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_2ndPlacePoints ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 235 , .Y = 130 } ,
        .Height     = 40 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "3rd place" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_3rdPlacePoints ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 10 , .Y = 170 } ,
        .Height     = 40 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "4th place" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_4thPlacePoints ,
        } ,
    } ,
    {
        .Type       = WidgetType_TextBox ,
        .Position   = { .X = 235 , .Y = 170 } ,
        .Height     = 40 ,
        .Width      = 225 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {

        } ,
        .String     = {
            .DefaultString          = "5th place" ,
            .Font                   = oC_Font_(Arial_10pt) ,
            .TextAlign              = TextAlign_Center ,
            .VerticalTextAlign      = VerticalTextAlign_Center ,
            .UpdateStringHandler    = (oC_WidgetScreen_UpdateWidgetStringHandler_t)UpdateWidgetString_5thPlacePoints ,
        } ,
    } ,
    {
        .Type       = WidgetType_PushButton ,
        .Position   = { .X = 350 , .Y = 220 } ,
        .Height     =  30 ,
        .Width      = 100 ,
        .ZPosition  = 1 ,
        .Palette    = &Palette ,
        .DrawStyle  = &DrawStyle ,
        .Handlers   = {
             [WidgetState_Activated] = (oC_WidgetScreen_WidgetHandler_t)HandleWidget_ExitPressed ,
        } ,
        .String     = {
            .DefaultString  = "Exit" ,
            .Font           = oC_Font_(CooperBlack_20pt) ,
            .TextAlign          = TextAlign_Center ,
            .VerticalTextAlign  = VerticalTextAlign_Center ,
        } ,
        .TypeSpecific.PushButton = {
            .Type           = PushButtonType_Standard
        } ,
    } ,
};

//==========================================================================================================================================
/**
 * List of defined screens
 */
//==========================================================================================================================================
static const ScreenDefinition_t ScreensDefinitions[ScreenId_NumberOfScreens] = {
    [ScreenId_Intro] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = NULL ,
         .Handler                       = (oC_WidgetScreen_ScreenHandler_t)HandleScreen_Intro ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = IntroWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(IntroWidgets) ,
    } ,
    [ScreenId_SelectNumberOfPlayers] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_SelectNumberOfPlayers ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = SelectNumberOfPlayersWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(SelectNumberOfPlayersWidgets) ,
    } ,
    [ScreenId_SelectPlayerColor] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_SelectPlayerColor ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = SelectPlayerColorWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(SelectPlayerColorWidgets) ,
    } ,
    [ScreenId_SelectStartTracks] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_SelectStartTracks ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = SelectStartTracksWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(SelectStartTracksWidgets) ,
    } ,
    [ScreenId_SelectLongTrack] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_SelectLongTracks ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = SelectLongTrackWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(SelectLongTrackWidgets) ,
    } ,
    [ScreenId_SelectShortTrack] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_SelectShortTracks ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = SelectShortTrackWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(SelectShortTrackWidgets) ,
    } ,
    [ScreenId_PlayerStatus] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_PlayerStatus ,
         .Handler                       = (oC_WidgetScreen_ScreenHandler_t)HandleScreen_PlayerStatus ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = PlayerStatusWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(PlayerStatusWidgets) ,
    } ,
    [ScreenId_PrivatePlayerState] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_PrivatePlayerState ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = PrivatePlayerStateWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(PrivatePlayerStateWidgets) ,
    } ,
    [ScreenId_BuildTrainStation] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_BuildTrainStation ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = BuildTrainStationWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(BuildTrainStationWidgets) ,
    } ,
    [ScreenId_BuildRoute] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_BuildRoute ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = BuildRouteWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(BuildRouteWidgets) ,
    } ,
    [ScreenId_DrawShortTracks] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_DrawShortTracks ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = DrawTracksWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(DrawTracksWidgets) ,
    } ,
    [ScreenId_FinishScreen] = {
         .Position                      = { 0 , 0 } ,
         .Width                         = 480 ,
         .Height                        = 272 ,
         .PrepareHandler                = (oC_WidgetScreen_ScreenHandler_t)PrepareScreen_FinishScreen ,
         .Handler                       = NULL ,
         .BackgroundColor               = 0x1E2124 ,
         .ColorFormat                   = oC_ColorFormat_RGB888 ,
         .WidgetsDefinitions            = FinishWidgets ,
         .NumberOfWidgetsDefinitions    = oC_ARRAY_SIZE(FinishWidgets) ,
    } ,
};

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc ,  char ** Argv )
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;
    GameState_t     gameState;

    if(PrepareGameState(&gameState))
    {
        errorCode = oC_WidgetScreen_HandleScreens( NULL, ScreensDefinitions, oC_ARRAY_SIZE(ScreensDefinitions), &gameState );
        if(oC_ErrorOccur(errorCode))
        {
            printf("Error during handle screens: %R\n", errorCode);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_SelectNumberOfPlayers( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    GameState->NextScreen       = ScreenId_SelectPlayerColor;
    GameState->PreviousScreen   = ScreenId_NumberOfScreens;
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_SelectPlayerColor( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    oC_WidgetScreen_Option_t option;

    GameState->NextScreen       = ScreenId_SelectStartTracks;
    GameState->PreviousScreen   = ScreenId_SelectNumberOfPlayers;

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_SelectPlayerColor_PlayerColors);

    memset(&option,0,sizeof(option));

    for(uint32_t colorIndex = 0; colorIndex < oC_ARRAY_SIZE(PlayerColors); colorIndex++)
    {
        bool found = false;

        for(PlayerIndex_t playerIndex = 0; playerIndex < GameState->PlayerCount; playerIndex++)
        {
            if(GameState->PlayerIndex != playerIndex && GameState->PlayersStates[playerIndex].Color == &PlayerColors[colorIndex])
            {
                found = true;
                break;
            }
        }
        if(found == false)
        {
            option.ChangeFillColor  = true;
            option.FillColor        = PlayerColors[colorIndex].Color;
            option.ConstUserPointer = &PlayerColors[colorIndex];
            oC_WidgetScreen_AddOption(Screen,WidgetIndex_SelectPlayerColor_PlayerColors,&option);
        }
    }

    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectPlayerColor_NextPlayer,GameState->PlayersStates[GameState->PlayerIndex].Color != NULL);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_SelectStartTracks( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    GameState->NextScreen       = ScreenId_PlayerStatus;
    GameState->PreviousScreen   = ScreenId_SelectPlayerColor;

    memset(&option,0,sizeof(option));

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_StartTracks_RemoveTrack);

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    foreach(GameState->PlayersStates[GameState->PlayerIndex].Tracks,track)
    {
        option.ConstUserPointer = track;
        memset(pathString,0,sizeof(pathString));
        sprintf(pathString,"%s-%s (%d)", track->CityA, track->CityB, track->Score);

        oC_WidgetScreen_AddOption(Screen,WidgetIndex_StartTracks_RemoveTrack,&option);
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_SelectLongTracks( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    static bool              firstCall = true;
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    if(firstCall)
    {
        oC_ARRAY_FOREACH_IN_ARRAY(LongTracks,track)
        {
            option.ConstUserPointer = track;
            memset(pathString,0,sizeof(pathString));
            sprintf(pathString,"%s-%s (%d)", track->CityA, track->CityB, track->Score);

            oC_WidgetScreen_AddOption(Screen,WidgetIndex_SelectLongTrack_SetLongTrack,&option);
        }
    }

    firstCall = false;

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_SelectShortTracks( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    static bool              firstCall = true;
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    if(firstCall)
    {
        oC_ARRAY_FOREACH_IN_ARRAY(ShortTracks,track)
        {
            option.ConstUserPointer = track;
            memset(pathString,0,sizeof(pathString));
            sprintf(pathString,"%s-%s (%d)", track->CityA, track->CityB, track->Score);

            oC_WidgetScreen_AddOption(Screen,WidgetIndex_SelectShortTrack_SetShortTrack,&option);
        }

        memset(&option, 0 , sizeof(option));

        option.ChangeFillColor      = false;
        option.Font                 = oC_Font_(Consolas);
        option.Margin               = 2;
        option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
        option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

        for(int i = 0; i < GameState->NumberOfCities; i++)
        {
            option.Label                = GameState->Cities[i];
            option.ConstUserPointer     = GameState->Cities[i];

            oC_WidgetScreen_AddOption(Screen, WidgetIndex_SelectShortTrack_SetCityA, &option);
        }
    }

    firstCall = false;

    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_SetCityA      ,   false);
    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_SetShortTrack ,   true );
    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_FilteredRoutes,   false);
    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_AddTrack      ,   false);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_PlayerStatus( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_PrivatePlayerState( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    GameState->NextScreen       = ScreenId_PlayerStatus;
    GameState->PreviousScreen   = ScreenId_PlayerStatus;

    foreach(GameState->PlayersStates[GameState->PlayerIndex].Tracks,track)
    {
        bool found = oC_List_Contains(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks,track);

        if(found == false && IsTrackBuilt(GameState, &GameState->PlayersStates[GameState->PlayerIndex], track))
        {
            oC_List_PushBack(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks,track,getcurallocator());
        }
    }

    foreach(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks,track)
    {
        oC_List_RemoveAll(GameState->PlayersStates[GameState->PlayerIndex].Tracks,track);
    }

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_PrivateState_Tracks);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_BuildTrainStation( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    static bool firstCall = true;

    GameState->NextScreen       = ScreenId_PlayerStatus;
    GameState->PreviousScreen   = ScreenId_PlayerStatus;

    if(firstCall)
    {
        oC_WidgetScreen_Option_t option;
        oC_DefaultString_t       pathString;

        memset(&option,0,sizeof(option));

        option.ChangeFillColor      = false;
        option.Label                = pathString;
        option.Font                 = oC_Font_(Consolas);
        option.Margin               = 2;
        option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
        option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

        oC_ARRAY_FOREACH_IN_ARRAY(Routes,route)
        {
            option.ConstUserPointer = route;
            memset(pathString,0,sizeof(pathString));
            sprintf(pathString,"%s-%s (%d)", route->CityA, route->CityB, route->NumberOfRailcars);

            oC_WidgetScreen_AddOption(Screen, WidgetIndex_BuildTrainStation_Routes, &option);
        }

        memset(&option, 0 , sizeof(option));

        option.ChangeFillColor      = false;
        option.Font                 = oC_Font_(Consolas);
        option.Margin               = 2;
        option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
        option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

        for(int i = 0; i < GameState->NumberOfCities; i++)
        {
            option.Label                = GameState->Cities[i];
            option.ConstUserPointer     = GameState->Cities[i];

            oC_WidgetScreen_AddOption(Screen, WidgetIndex_BuildTrainStation_SetCityA, &option);
        }
    }

    firstCall = false;

    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_SetCityA         ,   false);
    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_Routes           ,   true );
    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_FilteredRoutes   ,   false);
    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_BuildTrainStation,   false);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_BuildRoute( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    static bool              firstCall = true;
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;


    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    GameState->NextScreen       = ScreenId_PlayerStatus;
    GameState->PreviousScreen   = ScreenId_PlayerStatus;

    if(firstCall)
    {
        oC_ARRAY_FOREACH_IN_ARRAY(Routes,route)
        {
            option.ConstUserPointer = route;
            memset(pathString,0,sizeof(pathString));
            sprintf(pathString,"%s-%s (%d)", route->CityA, route->CityB, route->NumberOfRailcars);

            oC_WidgetScreen_AddOption(Screen, WidgetIndex_BuildRoute_Routes, &option);
        }


        memset(&option, 0 , sizeof(option));

        option.ChangeFillColor      = false;
        option.Font                 = oC_Font_(Consolas);
        option.Margin               = 2;
        option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
        option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

        for(int i = 0; i < GameState->NumberOfCities; i++)
        {
            option.Label                = GameState->Cities[i];
            option.ConstUserPointer     = GameState->Cities[i];

            oC_WidgetScreen_AddOption(Screen, WidgetIndex_BuildRoute_SetCityA, &option);
        }
    }

    firstCall = false;

    oC_WidgetScreen_SetVisible( Screen, WidgetIndex_BuildRoute_SetCityA       ,   false);
    oC_WidgetScreen_SetVisible( Screen, WidgetIndex_BuildRoute_Routes         ,   true );
    oC_WidgetScreen_SetVisible( Screen, WidgetIndex_BuildRoute_FilteredRoutes ,   false);
    oC_WidgetScreen_SetVisible( Screen, WidgetIndex_BuildRoute_OK             ,   false);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_DrawShortTracks( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    GameState->NextScreen       = ScreenId_PlayerStatus;
    GameState->PreviousScreen   = ScreenId_PlayerStatus;

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_DrawTracks_RemoveTrack);

    memset(&option,0,sizeof(option));

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    for(int i = 0; i < MAX_DRAWN_TRACKS; i++)
    {
        const Track_t * track = GameState->PlayersStates[GameState->PlayerIndex].DrawnTracks[i];

        if(track != NULL)
        {

            option.ConstUserPointer = track;
            memset(pathString,0,sizeof(pathString));
            sprintf(pathString,"%s-%s (%d)", track->CityA, track->CityB, track->Score);

            oC_WidgetScreen_AddOption(Screen,WidgetIndex_DrawTracks_RemoveTrack,&option);
        }
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t PrepareScreen_FinishScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    int16_t         longestRouteLength      = 0;
    PlayerIndex_t   longestRoutePlayerIndex = 0;

    for( PlayerIndex_t playerIndex = 0; playerIndex < GameState->PlayerCount; playerIndex++ )
    {
        PlayerState_t * playerState = &GameState->PlayersStates[playerIndex];

        playerState->Score = 0;

        foreach( playerState->BuiltRoutes, route )
        {
            oC_ARRAY_FOREACH_IN_ARRAY(RoutePoints,points)
            {
                if(points->NumberOfRailcars == route->NumberOfRailcars)
                {
                    playerState->Score += points->Score;
                    break;
                }
            }
        }

        foreach(playerState->Tracks,track)
        {
            bool found = oC_List_Contains(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks,track);

            if(found == false && IsTrackBuilt(GameState, &GameState->PlayersStates[GameState->PlayerIndex], track))
            {
                oC_List_PushBack(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks,track,getcurallocator());
            }
        }

        foreach(playerState->BuiltTracks,track)
        {
            oC_List_RemoveAll(GameState->PlayersStates[GameState->PlayerIndex].Tracks,track);
            playerState->Score += track->Score;
        }

        oC_ARRAY_FOREACH_IN_ARRAY(playerState->TrainStations,trainStation)
        {
            if(trainStation->Used == false)
            {
                playerState->Score += POINTS_FOR_TRAINSTATION;
            }
        }

        foreach(playerState->Tracks,track)
        {
            playerState->Score -= track->Score;
        }

        CountLongestRoute(playerState, &playerState->LongestRoute);

        if( playerState->LongestRoute.NumberOfRailcars > longestRouteLength )
        {
            longestRouteLength      = playerState->LongestRoute.NumberOfRailcars;
            longestRoutePlayerIndex = playerIndex;
        }
    }

    GameState->LongestRoutePlayerIndex = longestRoutePlayerIndex;
    GameState->PlayersStates[longestRoutePlayerIndex].Score += POINTS_FOR_LONGEST_ROUTE;

    for(PlayerIndex_t playerIndex = 0; playerIndex < MAX_PLAYERS; playerIndex++)
    {
        GameState->Ranking[playerIndex] = playerIndex;
    }

    int n = GameState->PlayerCount;

    do
    {
        for(int i = 0 ; i < (n-1); i++)
        {
            if(GameState->PlayersStates[GameState->Ranking[i]].Score < GameState->PlayersStates[GameState->Ranking[i+1]].Score)
            {
                PlayerIndex_t temp = GameState->Ranking[i];

                GameState->Ranking[i]   = GameState->Ranking[i+1];
                GameState->Ranking[i+1] = temp;
            }
        }
        n = n - 1;
    } while( n > 1 );

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleScreen_Intro( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    sleep(s(1));
    return ScreenId_SelectNumberOfPlayers;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleScreen_PlayerStatus( oC_WidgetScreen_t Screen , GameState_t * GameState , ScreenId_t ScreenID )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->PlayerIndex];

    if(playerState->RailcarsCount <= RAILCARS_TO_FINISH)
    {
        ScreenID = ScreenId_FinishScreen;
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_NumberOfPlayers( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    sprintf_s(outString,BufferLength,"%d", GameState->PlayerCount);
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_PlayerName( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    memset(outString,0,BufferLength);

    if(GameState->PlayersStates[GameState->PlayerIndex].Color == NULL)
    {
        sprintf_s(outString,BufferLength,"Player %d", GameState->PlayerIndex + 1);
    }
    else
    {
        oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,WidgetIndex);

        if(palette != NULL && (*palette) != NULL)
        {
            (*palette)[WidgetState_Ready].TextColor = GameState->PlayersStates[GameState->PlayerIndex].Color->Color;
        }

        sprintf_s(outString,BufferLength,"Player %s", GameState->PlayersStates[GameState->PlayerIndex].Color->ColorName);
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_WonPlayerName( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    memset(outString,0,BufferLength);

    if(GameState->PlayersStates[GameState->Ranking[0]].Color == NULL)
    {
        sprintf_s(outString,BufferLength,"WINNER: Player %d", GameState->Ranking[0] + 1);
    }
    else
    {
        oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,WidgetIndex);

        if(palette != NULL && (*palette) != NULL)
        {
            (*palette)[WidgetState_Ready].TextColor = GameState->PlayersStates[GameState->Ranking[0]].Color->Color;
        }

        sprintf_s(outString,BufferLength,"WINNER: %s", GameState->PlayersStates[GameState->Ranking[0]].Color->ColorName);
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_LongestRoute( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->LongestRoutePlayerIndex];
    memset(outString,0,BufferLength);

    sprintf_s(outString,BufferLength,"Longest track: %s - %s (%d) - %s",
              playerState->LongestRoute.CityA,
              playerState->LongestRoute.CityB,
              playerState->LongestRoute.NumberOfRailcars,
              playerState->Color->ColorName);
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_WonPoints( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->Ranking[0]];
    memset(outString,0,BufferLength);

    oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,WidgetIndex);

    if(palette != NULL && (*palette) != NULL)
    {
        (*palette)[WidgetState_Ready].TextColor = playerState->Color->Color;
    }

    sprintf_s(outString,BufferLength,"Winner score: %d", playerState->Score);
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_2ndPlacePoints( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->Ranking[1]];
    memset(outString,0,BufferLength);

    if(playerState->Color != NULL)
    {
        oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,WidgetIndex);

        if(palette != NULL && (*palette) != NULL)
        {
            (*palette)[WidgetState_Ready].TextColor = playerState->Color->Color;
        }

        sprintf_s(outString,BufferLength,"2nd score: %d (%s)", playerState->Score, playerState->Color->ColorName);
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_3rdPlacePoints( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->Ranking[2]];
    memset(outString,0,BufferLength);

    if(playerState->Color != NULL)
    {
        oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,WidgetIndex);

        if(palette != NULL && (*palette) != NULL)
        {
            (*palette)[WidgetState_Ready].TextColor = playerState->Color->Color;
        }

        sprintf_s(outString,BufferLength,"3rd score: %d", playerState->Score);
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_4thPlacePoints( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->Ranking[3]];
    memset(outString,0,BufferLength);

    if(playerState->Color != NULL)
    {
        oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,WidgetIndex);

        if(palette != NULL && (*palette) != NULL)
        {
            (*palette)[WidgetState_Ready].TextColor = playerState->Color->Color;
        }

        sprintf_s(outString,BufferLength,"4th score: %d", playerState->Score);
    }

}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_5thPlacePoints( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->Ranking[4]];
    memset(outString,0,BufferLength);

    if(playerState->Color != NULL)
    {
        oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,WidgetIndex);

        if(palette != NULL && (*palette) != NULL)
        {
            (*palette)[WidgetState_Ready].TextColor = playerState->Color->Color;
        }

        sprintf_s(outString,BufferLength,"5th score: %d", playerState->Score);
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_PlayerPoints( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->PlayerIndex];

    playerState->Score = 0;

    foreach( playerState->BuiltRoutes, route )
    {
        oC_ARRAY_FOREACH_IN_ARRAY(RoutePoints,points)
        {
            if(points->NumberOfRailcars == route->NumberOfRailcars)
            {
                playerState->Score += points->Score;
                break;
            }
        }
    }

    foreach(playerState->Tracks,track)
    {
        bool found = oC_List_Contains(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks,track);

        if(found == false && IsTrackBuilt(GameState, &GameState->PlayersStates[GameState->PlayerIndex], track))
        {
            oC_List_PushBack(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks,track,getcurallocator());
        }
    }

    foreach(playerState->BuiltTracks,track)
    {
        oC_List_RemoveAll(GameState->PlayersStates[GameState->PlayerIndex].Tracks,track);
        playerState->Score += track->Score;
    }

    oC_ARRAY_FOREACH_IN_ARRAY(playerState->TrainStations,trainStation)
    {
        if(trainStation->Used == false)
        {
            playerState->Score += POINTS_FOR_TRAINSTATION;
        }
    }

    memset(outString,0,BufferLength);

    sprintf_s(outString,BufferLength,"%d pts", GameState->PlayersStates[GameState->PlayerIndex].Score);
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UpdateWidgetString_PlayerNumberOfRailcars( oC_WidgetScreen_t Screen , GameState_t * GameState , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->PlayerIndex];

    playerState->RailcarsCount = NUMBER_OF_RAILCARS;

    foreach(playerState->BuiltRoutes,route)
    {
        playerState->RailcarsCount -= route->NumberOfRailcars;
    }

    memset(outString,0,BufferLength);

    sprintf_s(outString,BufferLength,"%d RC", GameState->PlayersStates[GameState->PlayerIndex].RailcarsCount);
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_IncrementNumberOfPlayers( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PlayerCount++;
    if(GameState->PlayerCount > MAX_PLAYERS)
    {
        GameState->PlayerCount = MAX_PLAYERS;
    }
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_DecrementNumberOfPlayers( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    if(GameState->PlayerCount > 1)
    {
        GameState->PlayerCount--;
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_NextScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PlayerIndex = 0;
    return GameState->NextScreen;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_PreviousScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    return GameState->PreviousScreen;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_AddDrawnTracks( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    for(int i = 0; i < MAX_DRAWN_TRACKS; i++)
    {
        const Track_t * track = GameState->PlayersStates[GameState->PlayerIndex].DrawnTracks[i];

        GameState->PlayersStates[GameState->PlayerIndex].DrawnTracks[i] = NULL;

        if(track != NULL)
        {
            oC_List_PushBack(GameState->PlayersStates[GameState->PlayerIndex].Tracks,track,getcurallocator());
        }
    }

    GameState->PlayerIndex++;

    if(GameState->PlayerIndex >= GameState->PlayerCount)
    {
        GameState->PlayerIndex = 0;
    }

    return ScreenId_PlayerStatus;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_NextPlayer( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    ScreenId_t nextScreen = ScreenID;

    GameState->PlayerIndex++;

    if(GameState->PlayerIndex >= GameState->PlayerCount)
    {
        GameState->PlayerIndex = 0;
        nextScreen             = GameState->NextScreen;
    }

    *outReprepareScreen = true;

    return nextScreen;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_PreviousPlayer( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    ScreenId_t nextScreen = ScreenID;

    if(GameState->PlayerIndex > 0)
    {
        GameState->PlayerIndex--;
    }
    else
    {
        GameState->PlayerIndex = 0;
        nextScreen             = GameState->PreviousScreen;
    }
    if(outReprepareScreen)
    {
        *outReprepareScreen = true;
    }

    return nextScreen;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_SetPlayerColor( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption( Screen, WidgetIndex );

    oC_ASSERT( option != NULL );

    GameState->PlayersStates[GameState->PlayerIndex].Color = option->ConstUserPointer;

    if(option != NULL)
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectPlayerColor_NextPlayer,true);
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowSelectLongTrackScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PreviousScreen = ScreenID;
    return ScreenId_SelectLongTrack;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowSelectShortTrackScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PreviousScreen = ScreenID;
    return ScreenId_SelectShortTrack;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowDrawShortTracksScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PreviousScreen = ScreenID;
    return ScreenId_DrawShortTracks;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_SelectCityA( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->City     = NULL;
    GameState->Route    = NULL;

    if(ScreenID == ScreenId_BuildRoute)
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildRoute_SetCityA       ,   true);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildRoute_Routes         ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildRoute_FilteredRoutes ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildRoute_OK             ,   false);
    }
    else if(ScreenID == ScreenId_BuildTrainStation)
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_SetCityA            ,   true);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_Routes              ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_FilteredRoutes      ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_BuildTrainStation   ,   false);
    }
    else
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_SetCityA         ,   true);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_SetShortTrack    ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_FilteredRoutes   ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_AddTrack         ,   false);
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_SelectFilteredRoutes( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    WidgetIndex_t filteredRoutesIndex = ScreenID == ScreenId_BuildRoute         ? WidgetIndex_BuildRoute_FilteredRoutes :
                                        ScreenID == ScreenId_BuildTrainStation  ? WidgetIndex_BuildTrainStation_FilteredRoutes : WidgetIndex_SelectShortTrack_FilteredRoutes;
    oC_WidgetScreen_ClearOptions(Screen,filteredRoutesIndex);

    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    if(ScreenID == ScreenId_BuildRoute || ScreenID == ScreenId_BuildTrainStation)
    {
        oC_ARRAY_FOREACH_IN_ARRAY(Routes,route)
        {
            if(
                strcmp(route->CityA,GameState->City) == 0
             || strcmp(route->CityB,GameState->City) == 0
                )
            {
                option.ConstUserPointer = route;
                memset(pathString,0,sizeof(pathString));
                sprintf(pathString,"%s-%s (%d)", route->CityA, route->CityB, route->NumberOfRailcars);

                oC_WidgetScreen_AddOption(Screen,filteredRoutesIndex,&option);
            }
        }
    }
    else
    {
        oC_ARRAY_FOREACH_IN_ARRAY(ShortTracks,track)
        {
            if(
                strcmp(track->CityA,GameState->City) == 0
             || strcmp(track->CityB,GameState->City) == 0
                )
            {
                option.ConstUserPointer = track;
                memset(pathString,0,sizeof(pathString));
                sprintf(pathString,"%s-%s (%d)", track->CityA, track->CityB, track->Score);

                oC_WidgetScreen_AddOption(Screen,filteredRoutesIndex,&option);
            }
        }
    }

    if(ScreenID == ScreenId_BuildRoute)
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildRoute_SetCityA           ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildRoute_Routes             ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildRoute_FilteredRoutes     ,   true );
    }
    else if(ScreenID == ScreenId_BuildTrainStation)
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_SetCityA           ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_Routes             ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_FilteredRoutes     ,   true );
    }
    else
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_SetCityA         ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_SetShortTrack    ,   false);
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_FilteredRoutes   ,   true );
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_AddTrack( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    if(GameState->PreviousScreen == ScreenId_DrawShortTracks)
    {
        for(int i = 0; i < MAX_DRAWN_TRACKS; i++)
        {
            if(GameState->PlayersStates[GameState->PlayerIndex].DrawnTracks[i] == NULL)
            {
                GameState->PlayersStates[GameState->PlayerIndex].DrawnTracks[i] = GameState->Track;
                break;
            }
        }
    }
    else
    {
        oC_List_PushBack(GameState->PlayersStates[GameState->PlayerIndex].Tracks, GameState->Track, getcurallocator());
    }
    return GameState->PreviousScreen;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_BuildRoute( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    ScreenId_t nextScreenId = ScreenID;

    if(GameState->Route != NULL)
    {
        nextScreenId = ScreenId_PlayerStatus;

        bool contains = oC_List_Contains(GameState->PlayersStates[GameState->PlayerIndex].BuiltRoutes, GameState->Route);

        if(contains == false)
        {
            oC_List_PushBack( GameState->PlayersStates[GameState->PlayerIndex].BuiltRoutes, GameState->Route, getcurallocator() );

            GameState->PlayerIndex++;
            if(GameState->PlayerIndex >= GameState->PlayerCount)
            {
                GameState->PlayerIndex = 0;
            }
        }
    }

    return nextScreenId;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_RemoveUserTrack( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const Track_t * track = NULL;

    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption(Screen,WidgetIndex);

    track = option->ConstUserPointer;

    oC_List_RemoveAll(GameState->PlayersStates[GameState->PlayerIndex].Tracks,track);

    oC_WidgetScreen_RemoveCurrentOption(Screen,WidgetIndex);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_RemoveDrawTrack( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const Track_t * track = NULL;

    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption(Screen,WidgetIndex);

    track = option->ConstUserPointer;

    for(int i = 0; i < MAX_DRAWN_TRACKS; i++)
    {
        if(GameState->PlayersStates[GameState->PlayerIndex].DrawnTracks[i] == track)
        {
            GameState->PlayersStates[GameState->PlayerIndex].DrawnTracks[i] = NULL;
        }
    }

    oC_WidgetScreen_RemoveCurrentOption(Screen,WidgetIndex);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_SetLongTrack( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption( Screen, WidgetIndex );

    GameState->Track = option->ConstUserPointer;
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_SetShortTrack( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption( Screen, WidgetIndex );

    GameState->Track = option->ConstUserPointer;

    oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_AddTrack,   true);

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_SetCityA( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption( Screen, WidgetIndex );

    GameState->City = option->ConstUserPointer;

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_SetRoute( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption( Screen, WidgetIndex );

    GameState->Route = option->ConstUserPointer;

    if(ScreenID == ScreenId_BuildRoute)
    {
        oC_WidgetScreen_SetVisible( Screen, WidgetIndex_BuildRoute_OK,   true );
    }
    else if(ScreenID == ScreenId_BuildTrainStation)
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_BuildTrainStation_BuildTrainStation,   true);
    }
    else
    {
        oC_WidgetScreen_SetVisible(Screen,WidgetIndex_SelectShortTrack_AddTrack,   true);
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ScrollShortTracksUp( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_SelectShortTrack_SetShortTrack  , 0);
    oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_SelectShortTrack_SetCityA       , 0);
    oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_SelectShortTrack_FilteredRoutes , 0);
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ScrollShortTracksDown( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_SelectShortTrack_SetShortTrack  , 0);
    oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_SelectShortTrack_SetCityA       , 0);
    oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_SelectShortTrack_FilteredRoutes , 0);
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ScrollRoutesUp( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    if(ScreenID == ScreenId_BuildRoute)
    {
        oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_BuildRoute_Routes         , 0);
        oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_BuildRoute_SetCityA       , 0);
        oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_BuildRoute_FilteredRoutes , 0);
    }
    else
    {
        oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_BuildTrainStation_Routes         , 0);
        oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_BuildTrainStation_SetCityA       , 0);
        oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_BuildTrainStation_FilteredRoutes , 0);
    }
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ScrollRoutesDown( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    if(ScreenID == ScreenId_BuildRoute)
    {
        oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_BuildRoute_Routes         , 0);
        oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_BuildRoute_SetCityA       , 0);
        oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_BuildRoute_FilteredRoutes , 0);
    }
    else
    {
        oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_BuildTrainStation_Routes         , 0);
        oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_BuildTrainStation_SetCityA       , 0);
        oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_BuildTrainStation_FilteredRoutes , 0);
    }
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowBuildTrainStationScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PreviousScreen = ScreenID;
    return ScreenId_BuildTrainStation;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowBuildRouteScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PreviousScreen = ScreenID;
    return ScreenId_BuildRoute;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowPrivatePlayerStateScreen( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    GameState->PreviousScreen = ScreenID;
    return ScreenId_PrivatePlayerState;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowBuiltTracks( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_PrivateState_Tracks);

    memset(&option,0,sizeof(option));

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    foreach(GameState->PlayersStates[GameState->PlayerIndex].BuiltTracks, track)
    {
        memset(pathString,0,sizeof(pathString));
        sprintf(pathString,"%s - %s (%d)", track->CityA, track->CityB, track->Score);
        oC_WidgetScreen_AddOption(Screen,WidgetIndex_PrivateState_Tracks,&option);
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowTracksToBuild( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_PrivateState_Tracks);

    memset(&option,0,sizeof(option));

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    foreach(GameState->PlayersStates[GameState->PlayerIndex].Tracks, track)
    {
        memset(pathString,0,sizeof(pathString));
        sprintf(pathString,"%s - %s (%d)", track->CityA, track->CityB, track->Score);
        oC_WidgetScreen_AddOption(Screen,WidgetIndex_PrivateState_Tracks,&option);
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowBuiltTrainStations( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_PrivateState_Tracks);

    memset(&option,0,sizeof(option));

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    oC_ARRAY_FOREACH_IN_ARRAY(GameState->PlayersStates[GameState->PlayerIndex].TrainStations, trainStation)
    {
        if(trainStation->Route != NULL && trainStation->Used == true)
        {
            memset(pathString,0,sizeof(pathString));
            sprintf(pathString,"%s - %s (%d)", trainStation->Route->CityA, trainStation->Route->CityB, trainStation->Route->NumberOfRailcars);
            oC_WidgetScreen_AddOption(Screen,WidgetIndex_PrivateState_Tracks,&option);
        }
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ShowBuiltRoutes( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_Option_t option;
    oC_DefaultString_t       pathString;

    oC_WidgetScreen_ClearOptions(Screen,WidgetIndex_PrivateState_Tracks);

    memset(&option,0,sizeof(option));

    option.ChangeFillColor      = false;
    option.Label                = pathString;
    option.Font                 = oC_Font_(Consolas);
    option.Margin               = 2;
    option.TextAlign            = oC_WidgetScreen_TextAlign_Center;
    option.VerticalTextAlign    = oC_WidgetScreen_VerticalTextAlign_Center;

    foreach(GameState->PlayersStates[GameState->PlayerIndex].BuiltRoutes, route)
    {
        memset(pathString,0,sizeof(pathString));
        sprintf(pathString,"%s - %s (%d)", route->CityA, route->CityB, route->NumberOfRailcars);
        oC_WidgetScreen_AddOption(Screen,WidgetIndex_PrivateState_Tracks,&option);
    }

    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ScrollPlayerTracksUp( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_ScrollUp(Screen,WidgetIndex_PrivateState_Tracks,0);
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ScrollPlayerTracksDown( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_ScrollDown(Screen,WidgetIndex_PrivateState_Tracks,0);
    return ScreenID;
}

//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_BuildTrainStation( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    PlayerState_t * playerState = &GameState->PlayersStates[GameState->PlayerIndex];

    oC_ARRAY_FOREACH_IN_ARRAY(playerState->TrainStations,station)
    {
        if(station->Used == false)
        {
            station->Route      = GameState->Route;
            station->Used       = true;
            ScreenID            = GameState->PreviousScreen;
            break;
        }
    }

    GameState->PlayerIndex++;

    if(GameState->PlayerIndex >= GameState->PlayerCount)
    {
        GameState->PlayerIndex = 0;
    }

    return GameState->NextScreen;
}
//==========================================================================================================================================
//==========================================================================================================================================
static ScreenId_t HandleWidget_ExitPressed( oC_WidgetScreen_t Screen , GameState_t * GameState , WidgetIndex_t WidgetIndex , ScreenId_t ScreenID , bool * outReprepareScreen )
{
    return ScreenId_NumberOfScreens;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool DoesRouteToCityExist( GameState_t * GameState, PlayerState_t * PlayerState, TrackPath_t * TrackPath, const char * CityA , const char * CityB )
{
    bool        exist       = false;
    uint32_t    routeIndex  = TrackPath->NumberOfRoutes;

    if( routeIndex < TrackPath->ArraySize )
    {
        foreach(PlayerState->BuiltRoutes,route)
        {
            const char * cityA = strcmp(route->CityA,CityA) == 0 ? route->CityA :
                                 strcmp(route->CityB,CityA) == 0 ? route->CityB : NULL;

            if( cityA != NULL )
            {
                bool routeUsed = false;

                for(uint32_t i = 0; i < routeIndex; i++)
                {
                    if(TrackPath->Routes[i] == route)
                    {
                        routeUsed = true;
                        break;
                    }
                }

                if(routeUsed == false)
                {
                    const char * cityB = cityA == route->CityA ? route->CityB : route->CityA;

                    if(strcmp(cityB,CityB) == 0)
                    {
                        exist = true;
                        break;
                    }
                    else
                    {
                        TrackPath->NumberOfRoutes       = routeIndex + 1;
                        TrackPath->Routes[routeIndex]   = route;

                        exist = DoesRouteToCityExist(GameState,PlayerState,TrackPath,cityB,CityB);

                        if(exist)
                        {
                            break;
                        }
                    }
                }
            }
        }
    }

    if(exist == false)
    {
        oC_ARRAY_FOREACH_IN_ARRAY(PlayerState->TrainStations,trainStation)
        {
            if(trainStation->Used == true)
            {
                const Route_t * route = trainStation->Route;
                const char * cityA = strcmp(route->CityA,CityA) == 0 ? route->CityA :
                                     strcmp(route->CityB,CityA) == 0 ? route->CityB : NULL;

                if(cityA != NULL)
                {
                    for(PlayerIndex_t playerIndex = 0; playerIndex < MAX_PLAYERS; playerIndex++)
                    {
                        foreach(GameState->PlayersStates[playerIndex].BuiltRoutes, realRoute)
                        {
                            if(route == realRoute)
                            {
                                exist = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    return exist;
}


//==========================================================================================================================================
//==========================================================================================================================================
static bool IsTrackBuilt( GameState_t * GameState, PlayerState_t * PlayerState , const Track_t * Track )
{
    bool        built     = false;
    TrackPath_t trackPath;

    trackPath.Routes = malloc(sizeof(Route_t *) * oC_List_Count(PlayerState->BuiltRoutes), AllocationFlags_ZeroFill);

    if(trackPath.Routes != NULL)
    {
        trackPath.ArraySize         = oC_List_Count(PlayerState->BuiltRoutes);
        trackPath.NumberOfRoutes    = 0;

        if(DoesRouteToCityExist(GameState, PlayerState,&trackPath,Track->CityA,Track->CityB))
        {
            built = true;
        }

        free(trackPath.Routes,0);
    }

    return built;
}

//==========================================================================================================================================
//==========================================================================================================================================
static const char * FindLongestRouteFromCity( PlayerState_t * PlayerState, TrackPath_t * TrackPath, const char * CityA )
{
    const char * destinationCity = NULL;
    uint32_t     routeIndex      = TrackPath->NumberOfRoutes;
    int16_t      routeLength     = TrackPath->RouteLength;

    if( routeIndex < TrackPath->ArraySize )
    {
        foreach(PlayerState->BuiltRoutes,route)
        {
            const char * cityA = strcmp(route->CityA,CityA) == 0 ? route->CityA :
                                 strcmp(route->CityB,CityA) == 0 ? route->CityB : NULL;

            if( cityA != NULL )
            {
                bool routeUsed = false;

                for(uint32_t i = 0; i < routeIndex; i++)
                {
                    if(TrackPath->Routes[i] == route)
                    {
                        routeUsed = true;
                        break;
                    }
                }

                if(routeUsed == false)
                {
                    const char * cityB = cityA == route->CityA ? route->CityB : route->CityA;

                    TrackPath->RouteLength          = routeLength + route->NumberOfRailcars;
                    TrackPath->NumberOfRoutes       = routeIndex + 1;
                    TrackPath->Routes[routeIndex]   = route;

                    if(TrackPath->RouteLength > TrackPath->MaxRouteLength)
                    {
                        TrackPath->MaxRouteLength = TrackPath->RouteLength;
                        destinationCity           = cityB;
                    }

                    const char * city = FindLongestRouteFromCity(PlayerState,TrackPath,cityB);

                    if(city != NULL)
                    {
                        destinationCity = city;
                    }
                }
            }
        }
    }

    return destinationCity;
}

//==========================================================================================================================================
//==========================================================================================================================================
static void CountLongestRoute( PlayerState_t * PlayerState , Route_t * outRoute )
{
    uint32_t    arraySize = oC_List_Count(PlayerState->BuiltRoutes);
    TrackPath_t trackPath;

    memset(&trackPath, 0, sizeof(trackPath));

    outRoute->CityA             = NULL;
    outRoute->CityB             = NULL;
    outRoute->NumberOfRailcars  = 0;

    trackPath.Routes = malloc(sizeof(Route_t *) * arraySize, AllocationFlags_ZeroFill);

    if(trackPath.Routes != NULL)
    {
        foreach(PlayerState->BuiltRoutes, route)
        {
            const char * city;

            trackPath.ArraySize         = arraySize;
            trackPath.MaxRouteLength    = 0;
            trackPath.RouteLength       = 0;
            trackPath.NumberOfRoutes    = 0;

            city = FindLongestRouteFromCity(PlayerState, &trackPath, route->CityA);

            if(city != NULL && trackPath.MaxRouteLength > outRoute->NumberOfRailcars)
            {
                outRoute->CityA             = route->CityA;
                outRoute->CityB             = city;
                outRoute->NumberOfRailcars  = trackPath.MaxRouteLength;
            }

            trackPath.ArraySize         = arraySize;
            trackPath.MaxRouteLength    = 0;
            trackPath.RouteLength       = 0;
            trackPath.NumberOfRoutes    = 0;

            city = FindLongestRouteFromCity(PlayerState, &trackPath, route->CityB);

            if(city != NULL && trackPath.MaxRouteLength > outRoute->NumberOfRailcars)
            {
                outRoute->CityA             = route->CityB;
                outRoute->CityB             = city;
                outRoute->NumberOfRailcars  = trackPath.MaxRouteLength;
            }
        }

        free(trackPath.Routes,0);
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool PreparePlayerState( PlayerState_t * PlayerState )
{
    bool result = false;

    PlayerState->BuiltRoutes    = oC_List_New(getcurallocator(), 0);
    PlayerState->BuiltTracks    = oC_List_New(getcurallocator(), 0);
    PlayerState->Color          = NULL;
    PlayerState->RailcarsCount  = NUMBER_OF_RAILCARS;
    PlayerState->Score          = 0;
    PlayerState->Tracks         = oC_List_New(getcurallocator(), 0);
    PlayerState->TrainStations[0].Route     = NULL;
    PlayerState->TrainStations[0].Used      = false;
    PlayerState->TrainStations[1].Route     = NULL;
    PlayerState->TrainStations[1].Used      = false;
    PlayerState->TrainStations[2].Route     = NULL;
    PlayerState->TrainStations[2].Used      = false;

    if(
        PlayerState->BuiltRoutes != NULL
     && PlayerState->BuiltTracks != NULL
     && PlayerState->Tracks      != NULL
        )
    {
        result = true;
    }

    return result;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool PrepareGameState( GameState_t * GameState )
{
    bool success = true;

    memset(GameState,0,sizeof(GameState_t));

    GameState->PlayerCount = 1;

    for(PlayerIndex_t playerIndex = 0; playerIndex < MAX_PLAYERS && success; playerIndex++)
    {
        success = PreparePlayerState(&GameState->PlayersStates[playerIndex]);
    }

    if(success)
    {
        GameState->Cities = malloc( oC_ARRAY_SIZE(Routes) * 2 * sizeof(Route_t) , AllocationFlags_ZeroFill );

        if(GameState->Cities == NULL)
        {
            success = false;
        }
        else
        {
            GameState->NumberOfCities = 0;

            oC_ARRAY_FOREACH_IN_ARRAY(Routes,route)
            {
                bool cityAFound = false;
                bool cityBFound = false;

                for(uint32_t i = 0; i < GameState->NumberOfCities; i++)
                {
                    if(strcmp(route->CityA,GameState->Cities[i]) == 0)
                    {
                        cityAFound = true;
                    }
                    if(strcmp(route->CityB,GameState->Cities[i]) == 0)
                    {
                        cityBFound = true;
                    }
                }

                if(!cityAFound)
                {
                    GameState->Cities[GameState->NumberOfCities++] = route->CityA;
                }
                if(!cityBFound)
                {
                    GameState->Cities[GameState->NumberOfCities++] = route->CityB;
                }
            }
        }
    }

    return success;
}
