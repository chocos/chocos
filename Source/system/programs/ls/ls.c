/** ****************************************************************************************************************************************
 *
 * @file       ls.c
 *
 * @brief      The file contains ls program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vfs.h>
#include <oc_vt100.h>
#include <oc_stdlib.h>
#include <oc_processman.h>

//==========================================================================================================================================
/**
 * @brief prints usage syntax of this command
 */
//==========================================================================================================================================
static void PrintUsage( void )
{
    const char * programName = oC_Process_GetName(oC_ProcessMan_GetCurrentProcess());
    printf("Usage: %s path\n\r" , programName );
}

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    if(Argc == 1 || Argc == 2)
    {
        oC_Dir_t       dir       = NULL;
        char *         path      = (Argc == 2) ? Argv[1] : oC_Process_GetPwd(oC_ProcessMan_GetCurrentProcess());

        oC_ErrorCode_t errorCode = oC_VirtualFileSystem_opendir(&dir,path);

        if(!oC_ErrorOccur(errorCode))
        {
            oC_FileInfo_t fileInfo;

            printf(
                   oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_WHITE
                   " %-30s | Size        | Touched \n\r"
                   "-------------------------------|-------------|---------------\n\r" , "File name" );

            errorCode = oC_VirtualFileSystem_readdir(dir,&fileInfo);

            while(!oC_ErrorOccur(errorCode))
            {
                printf(" %-30s | %10luB | %10.3f s\n\r" , fileInfo.Name , fileInfo.Size , fileInfo.Timestamp);
                errorCode = oC_VirtualFileSystem_readdir(dir,&fileInfo);
            }

            if(oC_ErrorCode_NoSuchFile == errorCode)
            {
                errorCode = oC_ErrorCode_None;
            }

            oC_AssignErrorCode(&errorCode , oC_VirtualFileSystem_closedir(dir));
        }

        if(oC_ErrorOccur(errorCode))
        {
            oC_PrintErrorMessage("Error: " ,errorCode);
        }
    }
    else
    {
        PrintUsage();
    }

    return 0;
}

