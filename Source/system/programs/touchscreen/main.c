/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the touchscreen program
 *
 * @author     Patryk Kubiak - (Created on: 2017-05-05 - 08:58:27) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_ictrlman.h>
#include <oc_screenman.h>
#include <oc_struct.h>
#include <oc_tgui.h>
#include <oc_array.h>
#include <oc_gpio.h>

static const char * GestNames[] = {
                    "None                      " ,
                    "ClickWithoutRelease       " ,
                    "Click                     " ,
                    "DoubleClick               " ,
                    "TripleClick               " ,
                    "MoveUp                    " ,
                    "MoveDown                  " ,
                    "MoveLeft                  " ,
                    "MoveRight                 " ,
                    "MoveUpLeft                " ,
                    "MoveUpRight               " ,
                    "MoveDownLeft              " ,
                    "MoveDownRight             " ,
                    "RotateClockwise           " ,
                    "RotateCounterClockwise    " ,
                    "ZoomIn                    " ,
                    "ZoomOut                   " ,
                    "RightClick                " ,
                    "MiddleClick               " ,
                    "MoveCursor                " ,
};

//==========================================================================================================================================
/**
 * @brief returns name of the gesture
 */
//==========================================================================================================================================
static const char * GetGestureName( oC_IDI_EventId_t EventId )
{
    const char *                name    = "unknown gest";
    oC_IDI_EventId_t            moveId  = EventId & oC_IDI_EventId_IndexMask;
    static oC_DefaultString_t   string  = {0};

    if(moveId < oC_ARRAY_SIZE(GestNames))
    {
        name = GestNames[moveId];
    }
    else
    {
        sprintf(string,"unknown gest %d\n", moveId);
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief prints program usage
 */
//==========================================================================================================================================
static void PrintUsage( int Argc, char ** Argv )
{
    printf("Usage: \n   %s [--screen name] [--help]\n", Argv[0]);
    printf("   where: \n");
    printf("           --screen name         - Name of the screen to use for tests (optional)\n");
    printf("           --help                - Prints this help (optional)\n");
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;
    const char *    screenName      = oC_GetArgumentAfter(Argc,Argv,"--screen");
    oC_Screen_t     screen          = oC_ScreenMan_GetDefaultScreen();
    bool            commandCorrect  = true;

    if(oC_ArgumentOccur(Argc,Argv,"--screen"))
    {
        screen = oC_ScreenMan_GetScreen(screenName);

        if(screenName == NULL || screen == NULL)
        {
            commandCorrect = false;
        }
    }

    if(commandCorrect == false || oC_ArgumentOccur(Argc,Argv,"--help"))
    {
        PrintUsage(Argc,Argv);
    }
    else
    {
        oC_TGUI_Key_t key = 0;
        oC_Struct_Define(oC_ICtrlMan_Activity_t,activity);

        printf("Press Ctrl+C to exit...\n");

        while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC)
        {
            oC_Struct_Define(oC_IDI_Event_t,event);

            activity.EventsMask = oC_IDI_EventId_AnyController | oC_IDI_EventId_AnyMove;
            activity.Position.X = 0;
            activity.Position.Y = 0;
            activity.Screen     = screen;

            oC_Screen_ReadResolution(screen, &activity.Width, &activity.Height );

            if(ErrorCode(oC_ICtrlMan_WaitForActivity(&activity,&event,s(4))))
            {
                printf("Detected gest: %s\n", GetGestureName(event.EventId));
                printf("    Number of fingers: %d\n", event.NumberOfPoints);

                for(uint16_t i = 0; i < event.NumberOfPoints; i++)
                {
                    printf("        Position of finger %d: %d.%d, weight: %d, delta: (%d,%d)\n", i, event.Position[i].X, event.Position[i].Y, event.Weight[i], event.DeltaX[i],event.DeltaY[i]);
                    printf("                               Rotate angle: %d\n", event.RotateAngle);
                }
            }
            else if(errorCode == oC_ErrorCode_UnknownGesture)
            {
                printf("Unknown gesture\n");
            }
            else if(errorCode != oC_ErrorCode_Timeout)
            {
                printf("Error: %R\n", errorCode);
                break;
            }
        }
    }

    return errorCode;
}
