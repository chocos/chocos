/** ****************************************************************************************************************************************
 *
 * @file       ps.c
 *
 * @brief      The file contains ps program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vt100.h>
#include <oc_stdlib.h>
#include <oc_processman.h>
#include <oc_ktime.h>
#include <oc_intman.h>
#include <oc_threadman.h>

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_List(oC_Process_t) processes = oC_List_New(oC_ProcessMan_GetCurrentAllocator(),AllocationFlags_CanWait1Second);
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , oC_List_IsCorrect(processes) , oC_ErrorCode_AllocationError) &&
        oC_AssignErrorCode(       &errorCode , oC_ProcessMan_GetList(processes))
        )
    {
        errorCode = oC_ErrorCode_None;


        printf( oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_WHITE oC_VT100_DISABLE_LINE_WRAP );
        printf( " PID        | Process name         | User Name  | Heap Map    | Heap Map    | Threads     | Threads     | Threads S.  | Execution    |    %%    |\n\r");
        printf( "            |                      |            |   Size      |   Free      | Stack Size  | Stack Free  | Free (now)  | Time         |         |\n\r");
        printf( "------------|----------------------|------------|-------------|-------------|-------------|-------------|-------------|--------------|---------|\n\r");

        oC_List_Foreach(processes,process)
        {
            oC_Timestamp_t timestamp     = oC_KTime_GetTimestamp();
            oC_Time_t      exucutionTime = oC_Process_GetExecutionTime(process);
            oC_User_t      user          = oC_Process_GetUser(process);
            double         cpuLoad       = (exucutionTime / timestamp) * 100;
            oC_UInt_t      pid           = oC_Process_GetPid(process);
            const char *   pname         = oC_Process_GetName(process);
            const char *   uname         = oC_User_GetName(user);
            oC_UInt_t      heapMapSize   = oC_Process_GetHeapMapSize(process);
            oC_UInt_t      freeHeapMap   = oC_Process_GetFreeHeapMapSize(process);
            oC_UInt_t      threadsSize   = oC_Process_GetThreadsStackSize(process);
            oC_Int_t       threadsFree   = oC_Process_GetFreeThreadsStackSize(process,false);
            oC_Int_t       threadsFreeNow= oC_Process_GetFreeThreadsStackSize(process,true);
            printf(
                    /* pid  | pname| uname| hmsize | frhmsiz| tssize | tfree  | tfreen | exectime| cpuLoad | */
                    " %10lu | %20s | %10s | %10luB | %10luB | %10luB | %10ldB | %10ldB | %11.2fs | %6.2f%% |\n\r" ,
                       pid ,
                       pname ,
                       uname ,
                       heapMapSize ,
                       freeHeapMap ,
                       threadsSize ,
                       threadsFree ,
                       threadsFreeNow,
                       exucutionTime ,
                       cpuLoad
                );

            if(oC_ArgumentOccur(Argc,Argv,"-v"))
            {
                oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

                foreach(threads,thread)
                {
                    if(oC_Process_ContainsThread(process,thread))
                    {
                        oC_Time_t texecutionTime = oC_Thread_GetExecutionTime(thread);
                        double    tcpuLoad       = (texecutionTime / timestamp) * 100;

                        printf(" %10s | %20s | %10s | %10luB | %10luB | %10luB | %10ldB | %10ldB | %11.2fs | %6.2f%% |\n\r" ,
                               "thread:" ,
                               oC_Thread_GetName(thread) ,
                               uname,
                               0,
                               0,
                               oC_Thread_GetStackSize(thread) ,
                               oC_Thread_GetFreeStackSize(thread,false) ,
                               oC_Thread_GetFreeStackSize(thread,true),
                               exucutionTime ,
                               tcpuLoad
                               );
                    }
                }
            }
        }

        oC_List_Clear(processes);

        if(oC_List_Delete(processes,AllocationFlags_CanWaitForever)==false)
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    if(oC_ErrorOccur(errorCode))
    {
        oC_PrintErrorMessage("Error: " ,errorCode);
    }
    else
    {
        printf("Current CPU Load: %6.3f%% (General CPU Load: %6.3f%%) \n\r" , oC_ThreadMan_GetCurrentCpuLoad() , oC_ThreadMan_GetCpuLoad());
    }
    return 0;
}

