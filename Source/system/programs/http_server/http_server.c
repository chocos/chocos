/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the http_server program
 *
 * @author     Patryk Kubiak - (Created on: 2016-12-13 - 18:35:52) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_tcp.h>
#include <oc_tgui.h>
#include <oc_http.h>

//==========================================================================================================================================
/**
 * Handles HTTP request
 */
//==========================================================================================================================================
static void HandleRequest( oC_Tcp_Connection_t Connection, const oC_Http_Request_t* Request, char* ResponseBuffer, oC_MemorySize_t BufferSize, const char* RootPath, oC_Time_t Timeout )
{
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
    char*           buffer      = ResponseBuffer;
    oC_MemorySize_t size        = BufferSize;
    char responseBody[100] = {0};
    oC_Http_Response_t response;
    memset(&response, 0, sizeof(response));

    response.Version = oC_Http_Version_1_1;

    switch(Request->Method)
    {
        case oC_Http_Method_Get:
            response.StatusCode = oC_Http_StatusCode_OK;
            response.Packet.Body = responseBody;
            sprintf(responseBody, "<html><head><title>Hello</title></head><body>Time: %0.2g s</body></html>", gettimestamp());
            response.ContentLength = strlen(response.Packet.Body);
            response.ContentType   = oC_Http_ContentType_TextHtml;
            break;
        default:
            response.StatusCode = oC_Http_StatusCode_MethodNotAllowed;
            break;
    }
    if(
        ErrorCode( oC_Http_PrepareResponse(&response, &buffer, &size) )
     && ErrorCode( oC_Tcp_Connection_Send(Connection, ResponseBuffer, BufferSize - size, Timeout) )
        )
    {
        errorCode = oC_ErrorCode_None;
    }
    else
    {
        oC_PrintErrorMessage("Cannot handle request: ", errorCode);
    }
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
	oC_Tcp_Server_t     server          = NULL;
	oC_Tcp_Connection_t connection      = NULL;
	oC_ErrorCode_t      errorCode       = oC_ErrorCode_ImplementError;
	char*               tempString      = NULL;
	oC_MemorySize_t     maxSize         = 2000;
	oC_DefaultString_t  ipString        = {0};
	const char*         rootPath        = "/";
	oC_Net_Address_t    remoteAddress;
	oC_Net_Address_t    address         = {
	                .Type           = oC_Net_AddressType_IPv4 ,
	                .IPv4           = 0 ,
	                .Port           = oC_Tcp_Port_HTTP ,
	                .Protocol       = oC_Net_Protocol_TCP
	};

	printf("Preparing server ...\n");
	tempString = malloc( maxSize );

	if(ErrorCode(oC_Tcp_Listen( &address, &server, 6, hour(60) )))
	{
	    oC_TGUI_Key_t key = 0;

	    while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC )
	    {
            printf("Waiting for connections...");

	        if(ErrorCode( oC_Tcp_Accept(server, &connection, hour(24)) ))
	        {
                oC_Tcp_Connection_ReadRemote(connection, &remoteAddress);
                oC_Net_AddressToString(&remoteAddress, ipString, sizeof(ipString));
                printf("Received connection from %s\nWaiting for packet...", ipString);

                oC_MemorySize_t size = maxSize;
                oC_Http_Request_t request = {0};
                memset(tempString, 0, maxSize);
                if(
                    ErrorCode(oC_Tcp_Receive(connection,tempString,&size, s(10)))
                 && ErrorCode(oC_Http_ParseRequest(&request, tempString, size))
                    )
                {
                    HandleRequest(connection, &request, tempString, maxSize, rootPath, s(10));
                    oC_Tcp_Disconnect(&connection, s(3));
                }
                else
                {
                    printf(oC_VT100_FG_RED"%R\n"oC_VT100_FG_WHITE, errorCode);
                }
	        }
	        else
	        {
	            printf(oC_VT100_FG_RED"%R\n"oC_VT100_FG_WHITE, errorCode);
	        }
	    }
	}
	else
	{
	    oC_PrintErrorMessage("Cannot start server - ", errorCode);
	}
	
    return errorCode;
}
