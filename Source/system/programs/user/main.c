/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the user program
 *
 * @author     Mikolaj Filar - (Created on: 2017-01-26 - 09:21:49) 
 *
 * @note       Copyright (C) 2017 Mikolaj Filar <fimikolaj@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_userman.h>
#include <oc_system_cfg.h>
#include <oc_string.h>
#include <oc_process.h>
#include <oc_processman.h>
#include <oc_stdtypes.h>

#define _TO_STRING(V)   #V
#define TO_STRING(V)    _TO_STRING(V)

#define AppCondition(Condition, AppStatus)      ErrorCondition((Condition) , (oC_ErrorCode_t)(AppStatus))
#define AppCode( ErrorCode )                    ErrorCode((oC_ErrorCode_t)(AppStatus))

typedef enum
{
    AppStatus_LastStandardError             = oC_ErrorCode_NumberOfErrorsDefinitions,
    AppStatus_ParameterNotPassed,
    AppStatus_UserNameNotPassed,
    AppStatus_InvalidPassword,
    AppStatus_CannotRenameUser,
    AppStatus_CannotChangeUserPermisions
} AppStatus_t;


static void printHelpMessage( )
{
    printf("=== Program allows to manage users in system. ===\n\n");
    printf("Options list:\n");
    printf(" -l, --list       - print list of all users\n");
    printf(" -h, --help       - this help message\n\n");
    printf(" Pass user name/names after these parameters:\n");
    printf(" -c, --create     - create new user, add +r option after this parameter for root\n"
           "                    privileges for new user \n");
    printf(" -d, --delete     - delete user\n");
    printf(" -n, --name       - pass old user name and then new user name for renaming \n");
    printf(" +r, ++root       - add root privileges\n");
    printf(" -r, --root       - remove root privileges\n\n");
    printf("====== Examples ======\n\n");
    printf("Create \"myNewUser\" with root privileges:\n");
    printf("user -c +r myNewUser\n\n");
    printf("Change name of user \"oldNaffUser\" to \"newAceUser\":\n");
    printf("user --name oldNaffUser newAceUser\n");

}

static void listAllUsers( )
{
    const char * permissions = "";
    oC_List(oC_User_t) users = NULL;

    users = oC_UserMan_GetList();

    if(users != NULL)
    {
        oC_List_Foreach(users , user)
        {
            const char * name = oC_User_GetName(user);

            if( oC_User_IsRoot(user) )
            {
                permissions = "root";
            }
            else if(oC_User_CheckPermissions(user, oC_User_GetPermissions(oC_UserMan_GetRootUser())))
            {
                permissions = "root permissions";
            }
            else
            {
                permissions = "user";
            }

            printf(   /* name  | permissions |*/
                      " %20s | %20s |\n" ,
                      name,
                      permissions
                  );
        }
    }
}

static bool getPassword( char Password[CFG_UINT8_MAX_PASSWORD_LENGTH] )
{
    oC_Process_t        currentProcess  = oC_ProcessMan_GetCurrentProcess();
    oC_IoFlags_t        ioFlags         = oC_Process_GetIoFlags(currentProcess);

    ioFlags |= oC_IoFlags_EchoAsPassword;
    oC_Process_SetIoFlags(currentProcess,ioFlags);
    scanf("%" TO_STRING(CFG_UINT8_MAX_PASSWORD_LENGTH) "s" , Password );
    printf("password: %s\n", Password);
    ioFlags &= ~oC_IoFlags_EchoAsPassword;
    oC_Process_SetIoFlags(currentProcess,ioFlags);

    return strlen(Password) > 0;
}

// Get pointer to argument, skip strings containing special characters
static char* getArgumentPointer( int Start, int Argc , char ** Argv )
{
    char* argument = NULL;

    if(Start >= 0)
    {
        /* Get the name and skip parameters */
        for(int index = Start; index < Argc; index++)
        {
            if( !string_contains_special_characters(Argv[index]) )
            {
                argument = Argv[index];
                break;
            }
        }
    }

    return argument;
}

static int GetOptionParameterPosition(
                int Argc ,
                char ** Argv ,
                const char * Option ,
                const char * ExtendedOption
                )
{
    int position = oC_GetArgumentNumber(Argc,Argv,Option);

    if(position == -1)
    {
        position = oC_GetArgumentNumber(Argc,Argv,ExtendedOption);
    }

    return position;
}

static void printAppStatusMessage(AppStatus_t Status)
{
    const char * string = "Undefined user application error";

    if( Status < AppStatus_LastStandardError )
    {
        string = oC_GetErrorString(Status);
    }
    else
    {
        switch(Status)
        {
            case AppStatus_ParameterNotPassed:
                string = "Invalid arguments number.";
                break;
            case AppStatus_UserNameNotPassed:
                string = "User name not passed.";
                break;
            case AppStatus_InvalidPassword:
                string = "Invalid password";
                break;
            case AppStatus_CannotRenameUser:
                string = "Cannot rename user.";
                break;
            case AppStatus_CannotChangeUserPermisions:
                string = "Cannot change user permissions.";
                break;
            default:
                break;
        }
    }

    if(oC_ErrorOccur((oC_ErrorCode_t)Status))
    {
        printf("%s Add -h parameter for command details.\n", string);
    }
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    bool                    commandSelected                             = false;
    int                     argumentPos, argument2Pos, rootParameterPos = 0;
    oC_ErrorCode_t          errorCode                                   = oC_ErrorCode_ImplementError;
    char                    password[CFG_UINT8_MAX_PASSWORD_LENGTH]     = {0};
    char*                   login                                       = NULL;
    char*                   login2                                      = NULL;
    oC_User_Permissions_t   permissions                                 = oC_User_Permissions_Root;
    oC_User_t               user                                        = NULL;

    if( Argc <= 1 )
    {
        commandSelected = true;
        errorCode = (oC_ErrorCode_t)AppStatus_ParameterNotPassed;
    }
    else if(oC_ArgumentOccur(Argc,Argv,"-h") || oC_ArgumentOccur(Argc,Argv,"--help"))
    {
        printHelpMessage();
        commandSelected = true;
        errorCode       = oC_ErrorCode_None;
    }
    else if(oC_ArgumentOccur(Argc,Argv,"-l") || oC_ArgumentOccur(Argc,Argv,"--list"))
    {
        listAllUsers();
        commandSelected = true;
        errorCode       = oC_ErrorCode_None;
    }
    else if(Argc > 2)
    {
        /* Create new user */
        argumentPos = GetOptionParameterPosition(Argc, Argv, "-c", "--create");

        rootParameterPos = GetOptionParameterPosition(Argc,Argv,"+r", "++root");

        /* Create new user command passed */
        if(commandSelected == false &&
           AppCondition( argumentPos != -1 , AppStatus_ParameterNotPassed))
        {
            commandSelected = true;

            login = getArgumentPointer(argumentPos + 1 , Argc, Argv);

            if(AppCondition( login != NULL                              , AppStatus_UserNameNotPassed ) &&
               AppCondition( NULL == oC_UserMan_GetUser(login)          , oC_ErrorCode_UserExists     ) &&
               0 == printf("Pass new user password: ")                                                  &&
               AppCondition( getPassword(password)                      , AppStatus_InvalidPassword   )
               )
            {
                /* Get current user permissions if root not requested */
                if(rootParameterPos == -1)
                {
                    permissions = oC_User_GetPermissions(getcuruser());
                    errorCode   = oC_ErrorCode_None;
                }
                else if(iscurroot())
                {
                    permissions = oC_User_Permissions_Root;
                }
                else
                {
                    errorCode = oC_ErrorCode_PermissionDenied;
                }

                if(false == oC_ErrorOccur(errorCode))
                {
                    errorCode = oC_UserMan_CreateAndAddUser(login, password, permissions);
                }
            }
        }

        argumentPos = GetOptionParameterPosition(Argc,Argv,"-d", "--delete");

        /* Delete user command passed */
        if(commandSelected == false &&
           AppCondition( argumentPos != -1 , AppStatus_ParameterNotPassed))
        {
            commandSelected = true;

            login = getArgumentPointer(argumentPos + 1 , Argc, Argv);
            user  = oC_UserMan_GetUser(login);

            if(AppCondition( NULL  != login , AppStatus_UserNameNotPassed )  &&
               AppCondition( NULL  != user  , oC_ErrorCode_UserNotExists  )
               )
            {
                errorCode = oC_UserMan_RemoveUser(user);
            }
        }

        argumentPos = GetOptionParameterPosition(Argc,Argv,"-n", "--name");

        login = getArgumentPointer(argumentPos + 1 , Argc, Argv);

        argument2Pos = oC_GetArgumentNumber(Argc,Argv,login);

        login2 = getArgumentPointer(argument2Pos + 1 , Argc, Argv);

        user = oC_UserMan_GetUser(login);

        /* Rename user command passed */
        if(commandSelected == false &&
           AppCondition( argumentPos                != -1   , AppStatus_ParameterNotPassed ) &&
           AppCondition( argument2Pos               != -1   , AppStatus_UserNameNotPassed  ) &&
           AppCondition( login2                     != NULL , AppStatus_UserNameNotPassed  ) &&
           AppCondition( user                       != NULL , oC_ErrorCode_InvalidUserName ) &&
           AppCondition( oC_UserMan_GetUser(login)  != NULL , oC_ErrorCode_UserNotExists   ) &&
           AppCondition( oC_UserMan_GetUser(login2) == NULL , oC_ErrorCode_UserExists      ) )
        {
            commandSelected = true;

            errorCode = oC_UserMan_Rename(user, login2);
        }

        login = getArgumentPointer(rootParameterPos + 1 , Argc, Argv);
        user = oC_UserMan_GetUser(login);

        /* ++root command selected */
        if( commandSelected  == false                                     &&
            rootParameterPos != -1                                        &&
            AppCondition( NULL  != login , AppStatus_UserNameNotPassed )  &&
            AppCondition( NULL  != user  , oC_ErrorCode_UserNotExists  )
            )
        {
            commandSelected = true;
            if( oC_User_ModifyPermissions(user, oC_User_Permissions_Root))
            {
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                errorCode = AppStatus_CannotChangeUserPermisions;
            }
        }

        argumentPos = GetOptionParameterPosition(Argc,Argv,"-r", "--root");

        login = getArgumentPointer(argumentPos + 1 , Argc, Argv);
        user = oC_UserMan_GetUser(login);

        /* --root command selected */
        if( commandSelected  == false                                     &&
            argumentPos != -1                                             &&
            AppCondition( NULL  != login , AppStatus_UserNameNotPassed )  &&
            AppCondition( NULL  != user  , oC_ErrorCode_UserNotExists  )
            )
        {
            commandSelected = true;
            if( oC_User_ModifyPermissions(user, 0))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = AppStatus_CannotChangeUserPermisions;
            }
        }

    }

    // Invalid parameter passed
    if(errorCode == oC_ErrorCode_ImplementError && commandSelected == false)
    {
        errorCode = oC_ErrorCode_WrongParameters;
    }
	
    printAppStatusMessage(errorCode);

    return 0;
}
