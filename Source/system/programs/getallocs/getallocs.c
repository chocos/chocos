/** ****************************************************************************************************************************************
 *
 * @file       getallocs.c
 *
 * @brief      The program for getting memory allocations
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_memman.h>
#include <oc_array.h>
#include <string.h>

#define MAX_STATS               50

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_MemMan_AllocatorsStats_t *   stats        = NULL;
    oC_MemMan_AllocationStats_t *   allStats     = NULL;
    oC_UInt_t                       statsSize    = MAX_STATS;
    oC_UInt_t                       allStatsSize = MAX_STATS;
    oC_ErrorCode_t                  errorCode    = oC_ErrorCode_ImplementError;
    bool                            joinSimilar  = oC_ArgumentOccur(Argc,Argv,"--join") || oC_ArgumentOccur(Argc,Argv,"-j");

    stats    = malloc(MAX_STATS * sizeof(oC_MemMan_AllocatorsStats_t) , AllocationFlags_ZeroFill);
    allStats = malloc(MAX_STATS * sizeof(oC_MemMan_AllocationStats_t) , AllocationFlags_ZeroFill);

    if(stats != NULL && allStats != NULL)
    {
        errorCode = oC_MemMan_ReadAllocatorsStats(stats,&statsSize);

        if(!oC_ErrorOccur(errorCode) || errorCode == oC_ErrorCode_OutputArrayToSmall)
        {
            printf(
                    oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_YELLOW
                    "%-70s | Size \n\r"
                    "----------------------------------------------------------------------|-------------\n\r"
                    , "Allocator");

            oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(stats,statsSize,stat)
            {
                printf(oC_VT100_FG_YELLOW "%-70s | %10lu B\n\r" , stat->Allocator->Name , stat->Size);

                if(oC_ArgumentOccur(Argc,Argv,"-v"))
                {
                    allStatsSize = MAX_STATS;

                    if(ErrorCode( oC_MemMan_ReadAllocationsStats(stat->Allocator,allStats,&allStatsSize, joinSimilar) ))
                    {
                        oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(allStats,allStatsSize,allStat)
                        {
                            printf(oC_VT100_FG_WHITE "  [%p] %-50s:%-4d | %10lu B (%s)\n\r", allStat->Address , allStat->Function, allStat->LineNumber, allStat->Size, allStat->Overflowed ? oC_VT100_FG_RED "OVERFLOWED"oC_VT100_FG_WHITE : oC_VT100_FG_GREEN "OK"oC_VT100_FG_WHITE);
                        }
                    }
                    else
                    {
                        oC_PrintErrorMessage("Error occurs: ",errorCode);
                    }
                }
            }
        }
        else
        {
            oC_PrintErrorMessage("Error occurs: ",errorCode);
        }
    }
    else
    {
        printf("Cannot allocate memory!\n");
    }

    return 0;
}

