/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the example_udp program
 *
 * @author     Patryk Kubiak - (Created on: 2024-11-29 - 22:12:06) 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_udp.h>
#include <oc_netifman.h>
#include <string.h>

//=============================================================================
//                   == UDP SERVER ==
//
//          This function shows how to create simple UDP server
//
//=============================================================================
static int ExampleUdpServer(int Argc, char ** Argv)
{
    //
    // Prepare source address
    //
    oC_Net_Address_t sourceAddress = {
                     .Type = oC_Net_AddressType_IPv4,
                     .IPv4 = IP(255,255,255,255),
                     .Protocol = oC_Net_Protocol_UDP
    };
    oC_Udp_Port_t udpPort = 3000;

    //
    //  Get network interface for this address
    //
    oC_Netif_t netif = oC_NetifMan_GetNetifForAddress(&sourceAddress);
    if(netif == NULL)
    {
        oC_PrintErrorMessage("Network interface not found: ", oC_ErrorCode_NoSuchFile);
        return -1;
    }

    //
    //  Reserve UDP port
    //
    oC_ErrorCode_t errorCode = oC_Udp_ReservePort(&udpPort, s(10));
    if(errorCode != oC_ErrorCode_None)
    {
        oC_PrintErrorMessage("Cannot reserve UDP port: ", errorCode);
        return -1;
    }

    //
    //  Allocates memory for UDP packet
    //
    oC_MemorySize_t bufferSize = 100;
    oC_Udp_Packet_t* udpPacket = oC_Udp_Packet_New(getcurallocator(), oC_Net_PacketType_IPv4, NULL, bufferSize);
    if(udpPacket == NULL)
    {
        oC_PrintErrorMessage("Cannot allocate memory for a packet: ", oC_ErrorCode_AllocationError);
        return -1;
    }

    //
    //  Waits for client data
    //
    printf("Listening on port %d\n", udpPort);
    while(ErrorCode( oC_Udp_Receive(netif, udpPort, udpPacket, day(365)) ))
    {
        //
        //  Read source IP
        //
        oC_Net_Address_t sourceAddress;
        memset(&sourceAddress, 0, sizeof(sourceAddress));
        errorCode = oC_Net_Packet_ReadSourceAddress(&udpPacket->Packet, &sourceAddress);

        //
        //  Convert source IP to string
        //
        char ipString[sizeof("255.255.255.255")];
        memset(ipString, 0, sizeof(ipString));
        oC_Net_Ipv4AddressToString(sourceAddress.IPv4, ipString, sizeof(ipString));

        //
        // Read source port
        //
        oC_Udp_Port_t sourcePort = udpPacket->IPv4.UdpDatagram.Header.SourcePort;
        sourceAddress.Port = (oC_Net_Port_t)sourcePort;

        //
        //  Read received data
        //
        char* dataRef = oC_Udp_Packet_GetDataReference(udpPacket);
        printf("Received data: %s from %s:%d\n", dataRef, ipString, sourcePort);

        //
        //  Prepare response
        //
        char buffer[100];
        memset(buffer, 0, sizeof(buffer));
        sprintf_s(buffer, sizeof(buffer), "Hello from ChocoOS. Current time: %.03g s. Your data: %s\n", gettimestamp(), dataRef);
        printf("Sending %s\n", buffer);

        //
        //  Restore original packet's size (the one from oC_Udp_Packet_New)
        //
        if(!oC_Udp_Packet_SetDataSize(udpPacket, bufferSize))
        {
            oC_PrintErrorMessage("Cannot change packet's size: ", oC_ErrorCode_SizeNotCorrect);
        }

        //
        //  Set our data to the packet
        //
        if(!oC_Udp_Packet_SetData(udpPacket, buffer, sizeof(buffer)))
        {
            oC_PrintErrorMessage("Cannot set packet's data: ", oC_ErrorCode_EndOfFile);
        }

        //
        // Prepare data to respond
        //
        errorCode = oC_Udp_Send(netif, udpPort, &sourceAddress, udpPacket, s(10));
        if(errorCode != oC_ErrorCode_None)
        {
            oC_PrintErrorMessage("Cannot send UDP packet: ", errorCode);
        }
    }

    return 0;
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    return ExampleUdpServer(Argc, Argv);
}
