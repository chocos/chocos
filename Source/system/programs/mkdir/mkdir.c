/** ****************************************************************************************************************************************
 *
 * @file       mkdir.c
 *
 * @brief      The file contains mkdir program source
 *
 * @author     Patryk Kubiak - (Created on: 21 10 2015 19:12)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vt100.h>
#include <oc_vfs.h>
#include <oc_processman.h>

//==========================================================================================================================================
/**
 * @brief prints usage syntax of this command
 */
//==========================================================================================================================================
static void PrintUsage( void )
{
    const char * programName = oC_Process_GetName(oC_ProcessMan_GetCurrentProcess());
    printf("Usage: %s path\n\r" , programName );

}

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Argc != 2)
    {
        errorCode = oC_ErrorCode_SyntaxNotCorrect;
        PrintUsage();
    }
    else
    {
        errorCode = oC_VirtualFileSystem_mkdir(Argv[1]);

        if(oC_ErrorOccur(errorCode))
        {
            oC_PrintErrorMessage("Cannot create directory: ",errorCode);
        }
    }

    return (int)errorCode;
}

