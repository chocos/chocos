/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the getlogs program
 *
 * @author     Patryk Kubiak - (Created on: 2016-12-27 - 16:30:45) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_debug.h>
#include <string.h>
#include <oc_tgui.h>

//==========================================================================================================================================
/**
 * @brief prints usage message
 */
//==========================================================================================================================================
static void PrintUsage( const char * ProgramName )
{
    printf("Usage: %s [--real]\n" , ProgramName);
    printf("    where\n"
           "           --real, -r   - Waits for new logs and prints it in real time\n");
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    char log[400] = {0};

    if(oC_ArgumentOccur(Argc,Argv,"--real") || oC_ArgumentOccur(Argc,Argv,"-r"))
    {
        oC_TGUI_Key_t key = oC_TGUI_Key_Enter;

        oC_TGUI_SetForegroundColor(oC_TGUI_Color_Yellow);
        printf("Reading debug logs... Press ESC key to exit\n");
        oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);

        while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ESC)
        {
            if(readoldestkdebuglog(log,sizeof(log)))
            {
                uint32_t len = strlen(log);
                if(len > 0 && (log[len-1] == '\n' || log[len-1] == '\r'))
                {
                    printf(log);
                }
                else
                {
                    puts(log);
                }
                memset(log,0,sizeof(log));
            }
            else
            {
                sleep( ms(100) );
            }
        }
    }
    else if(Argc == 1)
    {
        lockkdebuglog();

        while(readoldestkdebuglog(log,sizeof(log)))
        {
            uint32_t len = strlen(log);
            if(len > 0 && (log[len-1] == '\n' || log[len-1] == '\r'))
            {
                printf(log);
            }
            else
            {
                puts(log);
            }
            memset(log,0,sizeof(log));
        }

        unlockkdebuglog();
    }
    else
    {
        PrintUsage(Argv[0]);
    }
    return 0;
}
