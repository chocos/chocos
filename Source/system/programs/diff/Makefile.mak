############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2017-07-31 - 22:37:57
##  Description:    makefile for diff program
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
PROGRAM_NAME            = diff
SPACE                   = USER_SPACE
OPTIMALIZE              = O0
WARNING_FLAGS           = -Wall
CSTD                    = c99
DEFINITIONS             =
SOURCE_FILES            = main.c
INCLUDES_DIRS           = 
STANDARD_INPUT          = uart_stdio
STANDARD_OUTPUT         = uart_stdio
STANDARD_ERROR          = uart_stdio
HEAP_MAP_SIZE           = 0
PROCESS_STACK_SIZE      = 1024
ALLOCATION_LIMIT        = 0
TRACK_ALLOCATION        = FALSE
FILE_TYPE			    = CBIN

##============================================================================================================================
##                                          
##              INCLUDE MAIN MAKEFILE               
##                                          
##============================================================================================================================
include $(PROGRAM_MK_FILE_PATH)
