/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the diff program
 *
 * @author     Patryk Kubiak - (Created on: 2017-07-31 - 22:37:57) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>

//==========================================================================================================================================
/**
 * Prints help message
 */
//==========================================================================================================================================
void PrintHelp()
{
	// Print description of the program
	printf("The program compares two files and shows differences\n");
	printf("Usage: diff f1 f2\n");
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
	if(Argc != 3)
	{
	    printf("Usage: %s f1 f2\n", Argv[0]);
	}
	else if(strcmp(Argv[1],"--help") == 0)
	{
	    PrintHelp();
	}
	else
	{
	    char * f1name = Argv[1];
	    char * f2name = Argv[2];
	    FILE * f1     = fopen(f1name,"r");
	    FILE * f2     = fopen(f2name,"r");

	    if(f1 == NULL)
	    {
	        printf("Cannot open file %s\n", f1name);
	    }
	    else if(f2 == NULL)
	    {
	        printf("Cannot open file %s\n", f2name);
	    }
	    else
	    {
	        char c1, c2;

	        while( !feof(f1)  && !feof(f2) )
	        {
	            c1 = fgetc(f1);
	            c2 = fgetc(f2);

	            if(c1 == c2)
	            {
	                printf(oC_VT100_FG_WHITE "%c", c1);
	            }
	            else
	            {
	                do
	                {
                        printf(oC_VT100_FG_RED "%c", c1);
	                } while( (c1 = fgetc(f1)) != '\n' && !feof(f1) );

	                printf(" \t\t => ");

	                do
	                {
                        printf(oC_VT100_FG_RED "%c", c2);
	                } while( (c2 = fgetc(f2)) != '\n' && !feof(f2) );
	                printf("\n");
	            }
	        }
	    }
	    fclose(f1);
	    fclose(f2);
	    printf("\n");
	}
    return 0;
}
