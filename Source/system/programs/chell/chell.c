/** ****************************************************************************************************************************************
 *
 * @file       chell.c
 *
 * @brief      The file contains chell program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vt100.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <oc_system_cfg.h>
#include <oc_userman.h>
#include <oc_processman.h>
#include <oc_threadman.h>
#include <oc_sys_lld.h>
#include <oc_kprint.h>
#include <ctype.h>
#include <oc_stdlib.h>
#include <oc_programman.h>
#include <oc_tgui.h>
#include <oc_vfs.h>
#include <oc_fs.h>
#include <oc_streamman.h>
#include <oc_elf.h>
#include <oc_cbin.h>

#define _TO_STRING(V)   #V
#define TO_STRING(V)    _TO_STRING(V)

typedef struct
{
    char        Login[CFG_UINT8_MAX_LOGIN_LENGTH];
    char        Password[CFG_UINT8_MAX_PASSWORD_LENGTH];
    oC_User_t   User;
    int32_t     NumberOfAttemps;
} LoginStructure_t;

typedef struct
{
    int      Argc;
    char **  Argv;
    char *   SubCommand;
    char *   StdInPath;
    char *   StdOutPath;
    char *   StdErrPath;
    char *   ArgumentsFilePath;
    char *   ArgumentsFileContent;
    int      ArgcFromFile;
    char **  ArgvFromFile;
    void *   Context;
} Order_t;

typedef enum
{
    JoinType_None ,
    JoinType_And ,
    JoinType_Or
} JoinType_t;

const oC_TGUI_Column_t             ErrorMessageBoxWidth    = 50;
const oC_TGUI_Position_t           ErrorMessageBoxPosition = { .Column = 20 , .Line = 5 };
const oC_TGUI_Style_t              ErrorMessageBoxStyle    = {
                .Foreground       = oC_TGUI_Color_Red,
                .Background       = oC_TGUI_Color_Black,
};
const oC_TGUI_Column_t             LoginFailureProgressBarWidth    = 50;
const oC_TGUI_Position_t           LoginFailureProgressBarPosition = { .Column = 20 , .Line = 5 };
const oC_TGUI_ProgressBarStyle_t   LoginFailureProgressBarStyle = {
                .BorderStyle.DontDraw       = true ,
                .NonActiveColor             = oC_TGUI_Color_LightGray ,
                .ActiveColor                = oC_TGUI_Color_DarkGray ,
};
const oC_TGUI_QuickEditBoxStyle_t  QuickEditBoxStyle = {
                .ActiveBorder.Foreground    = oC_TGUI_Color_Yellow ,
                .ActiveBorder.Background    = oC_TGUI_Color_LightGray ,
                .ActiveBorder.DontDraw      = true ,
                .NotActiveBorder.Foreground = oC_TGUI_Color_DarkGray ,
                .NotActiveBorder.Background = oC_TGUI_Color_LightGray ,
                .NotActiveBorder.DontDraw   = true ,
                .ActiveText.Foreground      = oC_TGUI_Color_Yellow ,
                .ActiveText.Background      = oC_TGUI_Color_DarkGray ,
                .NotActiveText.Foreground   = oC_TGUI_Color_DarkGray ,
                .NotActiveText.Background   = oC_TGUI_Color_LightGray,
};

const oC_TGUI_PushButtonStyle_t PushButtonStyle = {
                .ActiveBorder.DontDraw      = true ,
                .Active.Foreground          = oC_TGUI_Color_Yellow ,
                .Active.Background          = oC_TGUI_Color_DarkGray ,
                .NotActive.Foreground       = oC_TGUI_Color_DarkGray ,
                .NotActive.Background       = oC_TGUI_Color_LightGray ,
};
const oC_TGUI_TextBoxStyle_t    TextBoxStyle = {
                .BorderStyle.DontDraw       = true ,
                .TextStyle.Foreground       = oC_TGUI_Color_LightGray,
                .TextStyle.Background       = oC_TGUI_Color_Black,
};

//==========================================================================================================================================
//==========================================================================================================================================
static void PrintError( const char * String )
{
    oC_TGUI_SetStyle(&ErrorMessageBoxStyle);
    oC_TGUI_DrawAtPositionWithSize(ErrorMessageBoxPosition,String,ErrorMessageBoxWidth);
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool VerifyLogin( void * LoginStructure )
{
    LoginStructure_t * loginStructure = LoginStructure;
    bool stop = false;

    if(isaddresscorrect(loginStructure))
    {
        loginStructure->User = oC_UserMan_GetUser(loginStructure->Login);

        if(
            oC_User_IsCorrect(loginStructure->User) &&
            oC_User_CheckPassword(loginStructure->User,loginStructure->Password)
            )
        {
            stop = true;
        }
        else
        {
            PrintError("Login or password not correct!");
            memset(loginStructure->Login   ,0,sizeof(loginStructure->Login));
            memset(loginStructure->Password,0,sizeof(loginStructure->Password));
            loginStructure->User = NULL;
            loginStructure->NumberOfAttemps--;
            stop = loginStructure->NumberOfAttemps == 0;
        }
    }
    else
    {
        PrintError("Fatal error - login structure address is not correct!");
    }

    return stop;
}

//==========================================================================================================================================
/**
 * The function shows Terminal GUI login form
 *
 * @return user if logged
 */
//==========================================================================================================================================
static oC_User_t ShowTGuiLoginForm( void )
{
    LoginStructure_t        loginStructure;
    oC_TGUI_Position_t      position;
    oC_TGUI_Spiner_t        spanner;
    oC_ErrorCode_t          errorCode       = oC_ErrorCode_ImplementError;
    bool                    anyKeyPressed   = false;
    oC_TGUI_ActiveObject_t  activeObjects[] = {
          {
            .Type                       = oC_TGUI_ActiveObjecType_QuickEdit ,
            .Position                   = { .Column = 27 , .Line = 10 } ,
            .Width                      = 30 ,
            .QuickEdit.Buffer           = loginStructure.Login ,
            .QuickEdit.BufferSize       = sizeof(loginStructure.Login) ,
            .QuickEdit.InputType        = oC_TGUI_InputType_Default ,
            .QuickEdit.SaveHandler      = NULL ,
            .QuickEdit.SaveParameter    = NULL ,
            .QuickEdit.Style            = &QuickEditBoxStyle
          } ,
          {
            .Type                       = oC_TGUI_ActiveObjecType_QuickEdit ,
            .Position                   = { .Column = 27 , .Line = 12 } ,
            .Width                      = 30 ,
            .QuickEdit.Buffer           = loginStructure.Password ,
            .QuickEdit.BufferSize       = sizeof(loginStructure.Password) ,
            .QuickEdit.InputType        = oC_TGUI_InputType_Default | oC_TGUI_InputType_Password ,
            .QuickEdit.SaveHandler      = NULL ,
            .QuickEdit.SaveParameter    = NULL ,
            .QuickEdit.Style            = &QuickEditBoxStyle
          } ,
          {
            .Type                       = oC_TGUI_ActiveObjecType_PushButton ,
            .Position                   = { .Column = 48 , .Line = 14 } ,
            .Width                      = 10 ,
            .Height                     = 3 ,
            .PushButton.Text            = "OK" ,
            .PushButton.PressHandler    = VerifyLogin ,
            .PushButton.PressParameter  = &loginStructure,
            .PushButton.Style           = &PushButtonStyle,
          } ,
    };


    while(anyKeyPressed == false)
    {
        oC_TGUI_ResetDevice();
        printf("Press any key to login...");
        oC_TGUI_DrawSpiner(&spanner);

        errorCode = oC_KPrint_ReadFromStdIn(oC_IoFlags_500msTimeout,loginStructure.Login,1);

        if(errorCode == oC_ErrorCode_None || errorCode == oC_ErrorCode_OutputBufferTooSmall)
        {
            anyKeyPressed = true;
        }
        else if(errorCode != oC_ErrorCode_Timeout)
        {
            printf("Cannot read from stdin = %R\n", errorCode);
            oC_Process_Kill(getcurprocess());
            sleep(s(3));
        }
    }

    memset(loginStructure.Login     , 0 , sizeof(loginStructure.Login));
    memset(loginStructure.Password  , 0 , sizeof(loginStructure.Password));

    loginStructure.NumberOfAttemps  = 3;
    loginStructure.User             = NULL;


    oC_TGUI_ResetDevice();

    position.Column = 26;
    position.Line   = 8;

    oC_TGUI_DrawTextBox("Login" , position,30,&TextBoxStyle);

    position.Column = 26;
    position.Line   = 10;

    oC_TGUI_DrawTextBox("Password" , position,30,&TextBoxStyle);

    oC_TGUI_DrawActiveObjects(activeObjects,oC_ARRAY_SIZE(activeObjects));

    return loginStructure.User;
}

//==========================================================================================================================================
/**
 * The function shows login form
 *
 * @return user if logged
 */
//==========================================================================================================================================
static oC_User_t ShowLoginForm( void )
{
    char                login[CFG_UINT8_MAX_LOGIN_LENGTH];
    char                password[CFG_UINT8_MAX_PASSWORD_LENGTH];
    uint8_t             numberOfAttemps = 3;
    oC_User_t           user            = NULL;
    oC_Process_t        currentProcess  = oC_ProcessMan_GetCurrentProcess();
    oC_IoFlags_t        ioFlags         = oC_Process_GetIoFlags(currentProcess);
    oC_ErrorCode_t      errorCode       = oC_ErrorCode_ImplementError;

    printf(oC_VT100_RESET_DEVICE);

    for(;numberOfAttemps > 0;numberOfAttemps--)
    {
        memset(login,0,sizeof(login));

        do
        {
            printf("login: ");

            errorCode = scanf( "%" TO_STRING(CFG_UINT8_MAX_LOGIN_LENGTH) "s" ,login);

            if(oC_ErrorOccur(errorCode))
            {
                printf(oC_VT100_RESET_DEVICE);
            }
        }while( oC_ErrorOccur(errorCode) );


        user = oC_UserMan_GetUser(login);

        if(oC_User_IsCorrect(user))
        {
            memset(password,0,sizeof(password));

            printf("password: ");
            ioFlags |= oC_IoFlags_EchoAsPassword;
            oC_Process_SetIoFlags(currentProcess,ioFlags);
            scanf("%" TO_STRING(CFG_UINT8_MAX_PASSWORD_LENGTH) "s" , password );
            ioFlags &= ~oC_IoFlags_EchoAsPassword;
            oC_Process_SetIoFlags(currentProcess,ioFlags);

            if(oC_User_CheckPassword(user,password))
            {
                break;
            }
            else
            {
                printf("Password is not correct!\n\r");
                user = NULL;
            }
        }
        else
        {
            printf("User %s does not exist!\n\r" , login);
            user = NULL;
        }
    }

    return user;
}

//==========================================================================================================================================
/**
 * @brief shows welcome message
 */
//==========================================================================================================================================
static void ShowWelcomeMessage( oC_User_t user )
{
    printf( oC_VT100_RESET_DEVICE oC_VT100_FG_RED oC_VT100_BRIGHT                                            );
    printf( oC_VT100_FG_BLUE                                                                                 );
    printf( "============================\n\r"                                                               );
    printf( "\n\r"                                                                                           );
    printf( oC_VT100_FG_WHITE                                                                                );
    printf( "    Welcome, " oC_VT100_FG_YELLOW "%s\n\r\n\r" oC_VT100_RESET_ALL_ATTRIBUTES , oC_User_GetName(user));

}

//==========================================================================================================================================
/**
 * @brief shows command incentive (user name + path)
 */
//==========================================================================================================================================
static void ShowCommandIncentive( oC_User_t user , const char * path )
{
    printf(oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_FG_GREEN "%s" oC_VT100_FG_YELLOW "@" oC_VT100_FG_CYAN "%s:" oC_VT100_FG_WHITE "%s" oC_VT100_FG_GREEN oC_VT100_BRIGHT "$ " oC_VT100_FG_WHITE , oC_User_GetName(user) , oC_SYS_LLD_GetMachineName() , path );
}

//==========================================================================================================================================
/**
 * @brief prints string with hint
 */
//==========================================================================================================================================
static void ShowHint( const char * Hint , oC_UInt_t MatchedLength )
{
    oC_UInt_t hintLength = strlen(Hint);

    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);

    for(oC_UInt_t i = 0; i < MatchedLength && i < hintLength ; i++)
    {
        putc(Hint[i],stdout);
    }
    oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);
    printf("%s " , &Hint[MatchedLength]);
}

//==========================================================================================================================================
/**
 * @brief searches for command matching the begin of the string
 */
//==========================================================================================================================================
static bool SearchCommand( char * CommandBuffer , oC_UInt_t * SignIndex , oC_UInt_t Size , oC_User_t User )
{
    bool      found       = false;
    char *    pathString  = CommandBuffer;
    oC_UInt_t startIndex  = *SignIndex;
    oC_List(oC_Program_t) programs = oC_ProgramMan_GetList();
    oC_Program_t matchProgram = NULL;
    oC_UInt_t numberOfMatches = 0;
    oC_UInt_t matchStringLength = 0;
    oC_UInt_t fullMatchStringLength = 0;

    if((*SignIndex) < Size)
    {
        for(;startIndex > 0; startIndex--)
        {
            if(isspace((int)CommandBuffer[startIndex]))
            {
                pathString = &CommandBuffer[++startIndex];
                break;
            }
        }

        matchStringLength       = (*SignIndex) - startIndex;
        fullMatchStringLength   = matchStringLength;

        oC_List_Foreach(programs,program)
        {
            if(strncmp(program->Name,pathString,matchStringLength) == 0)
            {
                numberOfMatches++;
                if(numberOfMatches == 1)
                {
                    fullMatchStringLength = strlen(program->Name);
                    matchProgram          = program;
                }
                else
                {
                    for(oC_UInt_t i = 0; i < fullMatchStringLength; i++)
                    {
                        if(
                            program->Name[i] == 0
                         || matchProgram->Name[i] == 0
                         || program->Name[i] != matchProgram->Name[i]
                           )
                        {
                            fullMatchStringLength = i;
                            break;
                        }
                    }

                    if(numberOfMatches == 2)
                    {
                        putc('\n' , stdout);

                        ShowHint(matchProgram->Name,matchStringLength);
                    }
                    ShowHint(program->Name,matchStringLength);
                }
            }
        }

        if(numberOfMatches == 1)
        {
            oC_UInt_t programNameLength = strlen(matchProgram->Name);
            for(oC_UInt_t commandBufferIndex = startIndex + matchStringLength ; commandBufferIndex < (programNameLength + startIndex) ; commandBufferIndex++)
            {
                put_to_string(CommandBuffer,Size,(*SignIndex)++,matchProgram->Name[commandBufferIndex - startIndex]);
            }
            found = true;
        }
        else if(numberOfMatches > 1)
        {
            for(oC_UInt_t commandBufferIndex = startIndex + matchStringLength ; commandBufferIndex < (fullMatchStringLength + startIndex) ; commandBufferIndex++)
            {
                put_to_string(CommandBuffer,Size,(*SignIndex)++,matchProgram->Name[commandBufferIndex - startIndex]);
            }
            putc('\n',stdout);
            ShowCommandIncentive(User,oC_Process_GetPwd(oC_ProcessMan_GetCurrentProcess()));
            printf(oC_VT100_SAVE_CURSOR_AND_ATTRS);
            found = true;
        }

    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief searches for path matching the begin of the string
 */
//==========================================================================================================================================
static bool SearchPath( char * CommandBuffer , oC_UInt_t * SignIndex , oC_UInt_t Size , oC_User_t User )
{
    bool          found                 = false;
    char *        pathString            = CommandBuffer;
    oC_UInt_t     startIndex            = *SignIndex;
    oC_UInt_t     numberOfMatches       = 0;
    oC_UInt_t     matchStringLength     = 0;
    oC_UInt_t     fullMatchStringLength = 0;
    const char *  matchedFile           = NULL;
    oC_Dir_t      dir                   = NULL;
    char *        parentDirPath         = NULL;
    oC_FileInfo_t fileInfo              = {0};
    oC_UInt_t     parentDirLen          = 0;

    if((*SignIndex) < Size)
    {
        for(;startIndex > 0; startIndex--)
        {
            if(isspace((int)CommandBuffer[startIndex]))
            {
                pathString = &CommandBuffer[++startIndex];
                break;
            }
        }

        matchStringLength       = (*SignIndex) - startIndex;
        fullMatchStringLength   = matchStringLength;

        parentDirPath = malloc(matchStringLength + 1,AllocationFlags_ZeroFill);
        if(parentDirPath != NULL)
        {
            strncpy(parentDirPath,pathString,matchStringLength);

            for(oC_Int_t i = matchStringLength - 1; i >= 0 ; i--)
            {
                if(parentDirPath[i] == '/' || parentDirPath[i] == '\\')
                {
                    break;
                }
                else
                {
                    parentDirPath[i] = 0;
                }
            }

            parentDirLen        = strlen(parentDirPath);
            matchStringLength  -= parentDirLen;

            if(parentDirPath[0] == 0)
            {
                parentDirPath[0] = '/';
            }

            putc('\n' , stdout);

            oC_VirtualFileSystem_opendir(&dir,parentDirPath);

            while(!oC_ErrorOccur(oC_VirtualFileSystem_readdir(dir,&fileInfo)))
            {
                if(strncmp(fileInfo.Name,&pathString[parentDirLen],matchStringLength) == 0)
                {
                    numberOfMatches++;
                    if(numberOfMatches == 1)
                    {
                        matchedFile           = fileInfo.Name;
                        fullMatchStringLength = strlen(fileInfo.Name);
                    }
                    else
                    {
                        for(oC_UInt_t i = 0; i < fullMatchStringLength; i++)
                        {
                            if(
                                fileInfo.Name[i] == 0
                             || matchedFile[i] == 0
                             || fileInfo.Name[i] != matchedFile[i]
                               )
                            {
                                fullMatchStringLength = i;
                                break;
                            }
                        }

                        if(numberOfMatches == 2)
                        {
                            putc('\n' , stdout);

                            ShowHint(matchedFile,matchStringLength);
                        }
                        ShowHint(fileInfo.Name,matchStringLength);
                    }
                }
            }

            oC_VirtualFileSystem_closedir(dir);

            if(numberOfMatches == 1)
            {
                oC_UInt_t fileNameLength = strlen(matchedFile);
                for(oC_UInt_t commandBufferIndex = startIndex + matchStringLength ; commandBufferIndex < (fileNameLength + startIndex) ; commandBufferIndex++)
                {
                    put_to_string(CommandBuffer,Size,(*SignIndex)++,matchedFile[commandBufferIndex - startIndex]);
                }
                found = true;
            }
            else if(numberOfMatches > 1)
            {
                for(oC_UInt_t commandBufferIndex = startIndex + matchStringLength ; commandBufferIndex < (fullMatchStringLength + startIndex) ; commandBufferIndex++)
                {
                    put_to_string(CommandBuffer,Size,(*SignIndex)++,matchedFile[commandBufferIndex - startIndex]);
                }

                putc('\n',stdout);
                ShowCommandIncentive(User,oC_Process_GetPwd(oC_ProcessMan_GetCurrentProcess()));
                printf(oC_VT100_SAVE_CURSOR_AND_ATTRS);
                found = true;
            }

            free(parentDirPath,AllocationFlags_CanWait1Second);
        }
        else
        {
            oC_PrintErrorMessage("chell: cannot search path - " , oC_ErrorCode_AllocationError);
        }
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief returns last typed command
 */
//==========================================================================================================================================
static bool GetLastCommand( oC_UInt_t Size , oC_UInt_t MaxCommands , char LastCommands[MaxCommands][Size] , char outCommand[Size] , oC_UInt_t Index )
{
    bool found = false;

    if(MaxCommands > 0 && Index < MaxCommands && strlen(LastCommands[Index]) > 0)
    {
        memset(outCommand,0,Size);
        strcpy(outCommand,LastCommands[Index]);
        found = true;
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief save command to last typed commands list
 */
//==========================================================================================================================================
static void SaveCommand( oC_UInt_t Size , oC_UInt_t MaxCommands , char LastCommands[MaxCommands][Size] , char Command[Size] )
{
    oC_UInt_t commandIndex = 0;

    if(strlen(Command) > 0)
    {
        for(commandIndex = 0 ; MaxCommands > 0 && commandIndex < (MaxCommands - 1); commandIndex++)
        {
            strcpy(LastCommands[commandIndex],LastCommands[commandIndex+1]);
        }

        if(MaxCommands > 0)
        {
            strcpy(LastCommands[MaxCommands - 1],Command);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief reads command from the STDIN
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadCommand( char * outCommandBuffer , oC_UInt_t Size , oC_UInt_t MaxCommands , char LastCommands[MaxCommands][Size] , oC_User_t User )
{
    oC_TGUI_Key_t       key                 = 0;
    bool                commandTyped        = false;
    oC_UInt_t           signIndex           = 0;
    oC_ErrorCode_t      errorCode           = oC_ErrorCode_ImplementError;
    oC_UInt_t           lastCommandIndex    = MaxCommands;

    memset(outCommandBuffer,0,Size);
    printf(oC_VT100_SAVE_CURSOR_AND_ATTRS);

    while(commandTyped == false)
    {
        if(oC_TGUI_WaitForKeyPress(&key))
        {
            if(key == oC_TGUI_Key_Backspace)
            {
                if(signIndex > 0)
                {
                    signIndex = string_backspace(outCommandBuffer,signIndex);
                    printf("%c" , key);
                }
            }
            else if(key == oC_TGUI_Key_Enter)
            {
                printf("\n\r");
                errorCode    = oC_ErrorCode_None;
                commandTyped = true;
                SaveCommand(Size,MaxCommands,LastCommands,outCommandBuffer);
                break;
            }
            else if(key == oC_TGUI_Key_ArrowLeft)
            {
                if(signIndex > 0)
                {
                    signIndex--;
                    printf("\033[D");
                }
            }
            else if(key == oC_TGUI_Key_ArrowRight)
            {
                if(signIndex < strlen(outCommandBuffer))
                {
                    signIndex++;
                    printf("\033[C");
                }
            }
            else if(key == oC_TGUI_Key_ArrowUp)
            {
                lastCommandIndex--;
                if(GetLastCommand(Size,MaxCommands,LastCommands,outCommandBuffer,lastCommandIndex))
                {
                    signIndex = strlen(outCommandBuffer);
                }
                else
                {
                    lastCommandIndex++;
                }
            }
            else if(key == oC_TGUI_Key_ArrowDown)
            {
                lastCommandIndex++;
                if(GetLastCommand(Size,MaxCommands,LastCommands,outCommandBuffer,lastCommandIndex))
                {
                    signIndex = strlen(outCommandBuffer);
                }
                else
                {
                    lastCommandIndex--;
                }
            }
            else if(key == oC_TGUI_Key_Tab)
            {
                if(SearchCommand(outCommandBuffer,&signIndex,Size,User) == false)
                {
                    SearchPath(outCommandBuffer,&signIndex,Size,User);
                }
            }
            else if(signIndex < Size)
            {
                signIndex = put_to_string(outCommandBuffer,Size,signIndex,(char)key);
            }
            printf(oC_VT100_RESTORE_CURSOR_AND_ATTRS oC_VT100_ERASE_END_OF_LINE "%s"oC_VT100_RESTORE_CURSOR_AND_ATTRS, outCommandBuffer);

            for(oC_UInt_t i = 0 ; i < signIndex ; i++)
            {
                putc(outCommandBuffer[i],stdout);
            }
        }
        else
        {
            errorCode = oC_ErrorCode_ReadingNotPermitted;
            break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief split command to list of arguments
 */
//==========================================================================================================================================
static oC_ErrorCode_t SplitCommandToArguments( char * Command , int * outArgc , char *** outArgv )
{
    int argumentIndex       = 0;
    int argumentCount       = 0;
    oC_UInt_t cmdSize       = strlen(Command)+1;
    oC_ErrorCode_t errorCode= oC_ErrorCode_ImplementError;
    bool newArgumentStarted = false;
    bool quoteStarted       = false;

    //========================================================
    /* Counting number of arguments                         */
    //========================================================
    for(int signIndex = 0; signIndex < cmdSize ; signIndex++ )
    {
        /* If it is space, the new argument has started */
        if(isspace((int)Command[signIndex]) && !quoteStarted)
        {
            /* if some argument occurs before */
            if(newArgumentStarted)
            {
                argumentCount++;
            }
            newArgumentStarted = false;
        }
        else if(Command[signIndex] == '\"')
        {
            if(quoteStarted == false)
            {
                quoteStarted = true;
            }
            else
            {
                quoteStarted = false;
            }
        }
        else if(Command[signIndex] != 0)
        {
            /* found some argument */
            newArgumentStarted = true;
        }
    }

    if(newArgumentStarted == true)
    {
        argumentCount++;
    }

    //========================================================
    /* Allocation memory for argument array                 */
    //========================================================
    if(argumentCount > 0)
    {
        char ** argv = malloc(argumentCount * sizeof(char*),AllocationFlags_ZeroFill | AllocationFlags_CanWait1Second);

        newArgumentStarted = false;
        quoteStarted       = false;
        argumentIndex      = 0;

        for(int signIndex = 0; signIndex < cmdSize ; signIndex++ )
        {
            /* If it is space, the new argument has started */
            if(isspace((int)Command[signIndex]) && !quoteStarted)
            {
                /* if some argument occurs before */
                if(newArgumentStarted)
                {
                    argumentIndex++;
                }
                newArgumentStarted = false;

                /* Set this sign to 0 to mark an end of the string */
                Command[signIndex] = 0;
            }
            else if(Command[signIndex] == '\"')
            {
                if(quoteStarted == false)
                {
                    quoteStarted = true;
                }
                else
                {
                    quoteStarted = false;
                }
                Command[signIndex] = 0;
            }
            else
            {
                if(newArgumentStarted == false)
                {
                    /* It this is new argument, then save pointer to it */
                    argv[argumentIndex] = &Command[signIndex];
                }
                /* found some argument */
                newArgumentStarted = true;
            }
        }

        if(quoteStarted)
        {
            oC_PrintWarningMessage("Quote started but not finished!");
        }

        *outArgv = argv;
        *outArgc = argumentCount;
        errorCode= oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_NoCommandSpecified;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns pointer to the next SubCommand
 */
//==========================================================================================================================================
static char * GetNextSubCommand( char * Command , char ** outNextCommandRef , int * outLength , JoinType_t * outJoinType )
{
    char * subCommand       = NULL;
    char * endOfSubCommand  = Command;
    int    length           = 0;

    while(
            (*endOfSubCommand) != 0
         && strncmp(endOfSubCommand,"||", 2) != 0
         && strncmp(endOfSubCommand,"&&", 2) != 0
           )
    {
        length++;
        endOfSubCommand++;
    }

    if(length > 0)
    {
        if(strncmp(endOfSubCommand,"||", 2) == 0)
        {
            *outNextCommandRef  = endOfSubCommand + 2;
            *outJoinType        = JoinType_Or;
        }
        else if(strncmp(endOfSubCommand,"&&", 2) == 0)
        {
            *outNextCommandRef  = endOfSubCommand + 2;
            *outJoinType        = JoinType_And;
        }
        else
        {
            *outNextCommandRef  = endOfSubCommand;
            *outJoinType        = JoinType_None;
        }
        *outLength   = length;
        subCommand   = Command;
    }

    return subCommand;
}

//==========================================================================================================================================
/**
 * @brief removes arguments from order
 */
//==========================================================================================================================================
static void RemoveArgumentsInOrder( Order_t * Order , int ArgumentStart, int NumberOfArgumentsToRemove )
{
    for(int i = ArgumentStart; i < Order->Argc; i++)
    {
        for(int j = ArgumentStart + NumberOfArgumentsToRemove; j < Order->Argc; j++)
        {
            Order->Argv[i] = Order->Argv[j];
        }
    }
    while(NumberOfArgumentsToRemove > 0)
    {
        if(Order->Argc > 0)
        {
            Order->Argc -= 1;
        }
        NumberOfArgumentsToRemove--;
    }
}

//==========================================================================================================================================
/**
 * @brief reads streams names from SubCommand
 */
//==========================================================================================================================================
static bool UpdateStreamsNamesInOrder( Order_t * Order )
{
    bool success = true;

    for(int i = 0; i < Order->Argc; i++)
    {
        if(
            strcmp(Order->Argv[i], ">" ) == 0
         || strcmp(Order->Argv[i], "1>") == 0
            )
        {
            if((i+1) < Order->Argc && i > 0)
            {
                Order->StdOutPath = Order->Argv[i+1];
                RemoveArgumentsInOrder(Order,i,2);
            }
            else
            {
                printf("incorrect stdout redirection!\n");
                success = false;
                break;
            }
        }
        else if(
                strcmp(Order->Argv[i], "<" ) == 0
             || strcmp(Order->Argv[i], "0<") == 0
                )
        {
            if((i+1) < Order->Argc && i > 0)
            {
                Order->StdInPath = Order->Argv[i+1];
                RemoveArgumentsInOrder(Order,i,2);
            }
            else
            {
                printf("incorrect stdin redirection!\n");
                success = false;
                break;
            }
        }
        else if(
                strcmp(Order->Argv[i], "2>" ) == 0
                )
        {
            if((i+1) < Order->Argc && i > 0)
            {
                Order->StdErrPath = Order->Argv[i+1];
                RemoveArgumentsInOrder(Order,i,2);
            }
            else
            {
                printf("incorrect stderr redirection!\n");
                success = false;
                break;
            }
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads file and adds arguments from it to the order
 */
//==========================================================================================================================================
static bool AddArgumentsFromFileToOrder( Order_t * Order , int ArgumentStart)
{
    bool    success = false;
    FILE *  file    = fopen(Order->ArgumentsFilePath,"r");

    if(file != NULL)
    {
        size_t size = fsize(file) + 1;

        if(size > 0)
        {
            Order->ArgumentsFileContent = malloc(size,AllocationFlags_ZeroFill);

            if(Order->ArgumentsFileContent != NULL)
            {
                oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

                fread(Order->ArgumentsFileContent,1,size,file);

                if(ErrorCode(SplitCommandToArguments(Order->ArgumentsFileContent,&Order->ArgcFromFile,&Order->ArgvFromFile)))
                {
                    int     argc = Order->Argc + Order->ArgcFromFile;
                    char ** argv = malloc( argc * sizeof(char*), AllocationFlags_ZeroFill );

                    if(argv != NULL)
                    {
                        success = true;

                        for(int i = 0; i < ArgumentStart; i++)
                        {
                            argv[i] = Order->Argv[i];
                        }
                        for(int i = 0; i < Order->ArgcFromFile; i++)
                        {
                            argv[i + ArgumentStart] = Order->ArgvFromFile[i];
                        }
                        for(int i = ArgumentStart; i < Order->Argc; i++)
                        {
                            argv[i+Order->ArgcFromFile] = Order->Argv[i];
                        }
                        oC_SaveIfFalse("Release Argv error ", free(Order->Argv,0), oC_ErrorCode_ReleaseError);

                        Order->Argc = argc;
                        Order->Argv = argv;
                    }
                    else
                    {
                        printf("Cannot allocate memory for a arguments array!\n");
                    }
                }
                else
                {
                    printf("Cannot split arguments from file '%s': %R\n", Order->ArgumentsFilePath, errorCode);
                }
            }
            else
            {
                printf("Cannot allocate memory for a file '%s' content\n", Order->ArgumentsFilePath);
            }
        }
        else
        {
            success = true;
        }

        fclose(file);
    }
    else
    {
        printf("The file '%s' does not exist!\n", Order->ArgumentsFilePath);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief parses and updates arguments list
 */
//==========================================================================================================================================
static bool UpdateArgumentsListInOrder( Order_t * Order )
{
    bool success = true;

    for(int i = 0; i < Order->Argc && success; i++)
    {
        if(
            strcmp(Order->Argv[i], "<=" ) == 0
            )
        {
            Order->ArgumentsFilePath = Order->Argv[i+1];
            RemoveArgumentsInOrder(Order,i,2);

            success = AddArgumentsFromFileToOrder(Order,i);
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads Order according to the SubCommand
 */
//==========================================================================================================================================
static Order_t * NewOrderFromSubCommand( char * SubCommand , int Length )
{
    Order_t * order = malloc( sizeof(Order_t), AllocationFlags_ZeroFill );

    if(order != NULL )
    {
        order->SubCommand = malloc( Length + 1, AllocationFlags_ZeroFill );

        if(order->SubCommand != NULL)
        {
            oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

            strncpy(order->SubCommand, SubCommand,Length);

            if(ErrorCode(SplitCommandToArguments(order->SubCommand,&order->Argc,&order->Argv)))
            {
                if( !UpdateArgumentsListInOrder(order) || !UpdateStreamsNamesInOrder(order) )
                {
                    oC_SaveIfFalse( "subcommand" , free(order->SubCommand,0) , oC_ErrorCode_ReleaseError );
                    oC_SaveIfFalse( "Argv"       , free(order->Argv,0)       , oC_ErrorCode_ReleaseError );
                    oC_SaveIfFalse( "order"      , free(order,0)             , oC_ErrorCode_ReleaseError );
                    order = NULL;
                }
            }
            else
            {
                oC_PrintErrorMessage("Error: ", errorCode);
                oC_SaveIfFalse("subcommand", free(order->SubCommand,0), oC_ErrorCode_ReleaseError);
                oC_SaveIfFalse("order", free(order,0), oC_ErrorCode_ReleaseError);
                order = NULL;
            }
        }
        else
        {
            oC_PrintErrorMessage("Cannot allocate memory for sub-command: ", oC_ErrorCode_AllocationError);
            oC_SaveIfFalse("order", free(order,0), oC_ErrorCode_ReleaseError);
            order = NULL;
        }
    }
    else
    {
        oC_PrintErrorMessage("Cannot allocate memory for order: ", oC_ErrorCode_AllocationError);
    }

    return order;
}

//==========================================================================================================================================
/**
 * @brief releases memory allocated for the order
 */
//==========================================================================================================================================
static void DeleteOrder( Order_t * Order )
{
    oC_SaveIfFalse( "subcommand" , free(Order->SubCommand,0)            , oC_ErrorCode_ReleaseError );
    oC_SaveIfFalse( "Argv"       , free(Order->Argv,0)                  , oC_ErrorCode_ReleaseError );
    oC_SaveIfFalse( "AF Content" , Order->ArgumentsFileContent == NULL
                                || free(Order->ArgumentsFileContent,0)  , oC_ErrorCode_ReleaseError );
    oC_SaveIfFalse( "Argv FF"    , Order->ArgvFromFile == NULL
                                || free(Order->ArgvFromFile,0)          , oC_ErrorCode_ReleaseError );
    oC_SaveIfFalse( "order"      , free(Order,0)                        , oC_ErrorCode_ReleaseError );
}

//==========================================================================================================================================
/**
 * @brief changes allocator for the order object
 */
//==========================================================================================================================================
static void ChangeAllocatorForOrder( Order_t * Order , Allocator_t Allocator )
{
    if(Order->ArgumentsFileContent != NULL)
    {
        oC_SaveIfFalse( "AF Content" , oC_MemMan_ChangeAllocatorOfAddress(Order->ArgumentsFileContent , Allocator ) , oC_ErrorCode_WrongAddress );
    }
    if(Order->ArgvFromFile != NULL)
    {
        oC_SaveIfFalse( "ArgvFromFile" , oC_MemMan_ChangeAllocatorOfAddress(Order->ArgvFromFile , Allocator ) , oC_ErrorCode_WrongAddress );
    }

    oC_SaveIfFalse( "subcommand" , oC_MemMan_ChangeAllocatorOfAddress(Order->SubCommand , Allocator ) , oC_ErrorCode_WrongAddress );
    oC_SaveIfFalse( "argv"       , oC_MemMan_ChangeAllocatorOfAddress(Order->Argv       , Allocator ) , oC_ErrorCode_WrongAddress );
    oC_SaveIfFalse( "order"      , oC_MemMan_ChangeAllocatorOfAddress(Order             , Allocator ) , oC_ErrorCode_WrongAddress );
}

//==========================================================================================================================================
/**
 * @brief executes an ELF file
 */
//==========================================================================================================================================
static bool ExecuteElfFile( Order_t * Order , oC_User_t User )
{
    bool            success         = false;
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;
    bool            waitForFinish   = !(Order->Argc > 0 && strcmp(Order->Argv[Order->Argc - 1], "&") == 0);

    oC_Elf_File_t elfFile = NULL;

    if(
        ErrorCode( oC_Elf_LoadFile(Order->Argv[0], &elfFile) )
     && ErrorCode( oC_Elf_LoadProgram(elfFile) )
        )
    {
        kdebuglog( oC_LogType_Track, "loading program has been finished\n" );
        ChangeAllocatorForOrder(Order,oC_Process_GetAllocator( oC_Elf_GetProcess(elfFile) ));
        if( ErrorCode( oC_Elf_Execute(elfFile, Order->Argc, (const char**)Order->Argv) ) )
        {
            kdebuglog( oC_LogType_Track, "Execution has been finished" );
            oC_Process_t process = oC_Elf_GetProcess(elfFile);
            kdebuglog( oC_LogType_Track, "process name: %s\n", oC_Process_GetName(process) );
            if(waitForFinish)
            {
                success = true;
                if ( ErrorCode( oC_Elf_WaitForFinish(elfFile, ms(100), day(365)) ) )
                {
                    kdebuglog( oC_LogType_Track, "The program has been finished\n" );
                }
                else
                {
                    printf("Program has returned %d (%R)\n", errorCode, errorCode);
                    kdebuglog( oC_LogType_Error, "Waiting for finish of the elf program has failed: %R\n", errorCode );
                    kdebuglog( oC_LogType_Track, "process name: %s\n", oC_Process_GetName(process) );
                    oC_Program_t program = oC_Elf_GetProgram(elfFile);
                    kdebuglog( oC_LogType_Track, "program name: %s\n", oC_Program_GetName(program) );
                }
            }
            else
            {
                sleep(ms(500));
                success = true;
            }
        }
        else
        {
            kdebuglog( oC_LogType_Error, "Cannot execute ELF file: '%s': %R", Order->Argv[0], errorCode );
        }
    }
    else if(errorCode != oC_ErrorCode_NoSuchFile && errorCode != oC_ErrorCode_NotElfFile)
    {
        kdebuglog( oC_LogType_Warning, "Cannot prepare file '%s' for execution: %R\n", Order->Argv[0], errorCode );
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief executes an ELF file
 */
//==========================================================================================================================================
static bool ExecuteCBinFile( Order_t * Order , oC_User_t User )
{
    bool                        success         = false;
    oC_CBin_ProgramContext_t    programContext;
    oC_ErrorCode_t              errorCode       = oC_ErrorCode_ImplementError;
    oC_Process_t                currentProcess  = oC_ProcessMan_GetCurrentProcess();
    bool                        waitForFinish   = !(Order->Argc > 0 && strcmp(Order->Argv[Order->Argc - 1], "&") == 0);
    oC_CBin_File_t              cbinFile;

    memset(&programContext, 0, sizeof(programContext));
    memset(&cbinFile      , 0, sizeof(cbinFile));

    programContext.Argv         = Order->Argv;
    programContext.Argc         = Order->Argc;
    programContext.Priority     = waitForFinish ?
                                                      oC_Process_GetPriority(currentProcess) :
                                                      oC_Process_Priority_DefaultProcess ;
    programContext.Process      = NULL;
    programContext.StreamIn     = oC_Process_GetInputStream(currentProcess);
    programContext.StreamOut    = oC_Process_GetOutputStream(currentProcess);
    programContext.StreamErr    = oC_Process_GetErrorStream(currentProcess);

    if( ErrorCode( oC_CBin_LoadFile(Order->Argv[0], &cbinFile) ) )
    {
        if( ErrorCode( oC_CBin_PrepareForExecution(&cbinFile,&programContext) ) )
        {
            ChangeAllocatorForOrder(Order,oC_Process_GetAllocator(programContext.Process));

            if( ErrorCode( oC_CBin_Execute(&programContext) ) )
            {
                if(waitForFinish)
                {
                    if(!oC_ErrorOccur(oC_Program_WaitForFinish(programContext.ProgramContext,oC_ms(100),oC_day(10))))
                    {
                        success = true;
                    }
                }
                else
                {
                    sleep(ms(500));
                    success = true;
                }
            }
            else
            {
                kdebuglog( oC_LogType_Error, "Cannot execute CBIN file: '%s': %R", Order->Argv[0], errorCode );
            }
        }
        else
        {
            kdebuglog( oC_LogType_Warning, "Cannot prepare file '%s' for execution: %R\n", Order->Argv[0], errorCode );
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief runs a program
 */
//==========================================================================================================================================
static bool RunProgram( Order_t * Order , oC_User_t User )
{
    bool            success = false;
    oC_Program_t    program = oC_ProgramMan_GetProgram(Order->Argv[0]);

    if(program != NULL)
    {
        oC_Process_t process        = oC_Program_NewProcess(program,User);
        oC_Process_t currentProcess = oC_ProcessMan_GetCurrentProcess();

        if(process != NULL)
        {
            oC_Stream_t streamIn    = oC_StreamMan_GetStream(Order->StdInPath);
            oC_Stream_t streamOut   = oC_StreamMan_GetStream(Order->StdOutPath);
            oC_Stream_t streamErr   = oC_StreamMan_GetStream(Order->StdErrPath);

            if(streamIn  == NULL) streamIn  = oC_Process_GetInputStream(currentProcess);
            if(streamOut == NULL) streamOut = oC_Process_GetOutputStream(currentProcess);
            if(streamErr == NULL) streamErr = oC_Process_GetErrorStream(currentProcess);

            if(streamIn  != NULL) oC_Process_SetInputStream ( process , streamIn  );
            if(streamOut != NULL) oC_Process_SetOutputStream( process , streamOut );
            if(streamErr != NULL) oC_Process_SetErrorStream ( process , streamErr );

            ChangeAllocatorForOrder(Order,oC_Process_GetAllocator(process));

            oC_ErrorCode_t errorCode = oC_ProcessMan_AddProcess(process);

            if(!oC_ErrorOccur(errorCode))
            {
                oC_Process_Priority_t   priority = oC_Process_GetPriority(currentProcess);
                bool                    dontWait = false;

                if(Order->Argc > 0 && strcmp(Order->Argv[Order->Argc - 1], "&") == 0)
                {
                    priority = oC_Process_Priority_DefaultProcess;
                    dontWait = true;
                    Order->Argc--;
                }

                if(ErrorCode( oC_Process_SetPriority(process,priority)))
                {
                    errorCode = oC_Program_Exec(program,process,Order->Argc,(const char **)Order->Argv,NULL,&Order->Context);

                    if(oC_ErrorOccur(errorCode))
                    {
                        if(oC_Process_Delete(&process) == false)
                        {
                            printf("Error: cannot delete process\n\r");
                        }

                        oC_PrintErrorMessage("Cannot execute program: " , errorCode);

                    }
                    else
                    {
                        if(dontWait)
                        {
                            sleep(ms(500));
                            success = true;
                        }
                        else
                        {
                            if(!oC_ErrorOccur(oC_Program_WaitForFinish(Order->Context,oC_ms(100),oC_day(10))))
                            {
                                success = true;
                            }

                        }
                    }
                }
                else
                {
                    oC_PrintErrorMessage("Cannot set process priority: " , errorCode);
                }
            }
            else
            {
                oC_PrintErrorMessage("Cannot add process to list: " , errorCode );
            }

        }
        else
        {
            printf("Cannot create process\n");
            DeleteOrder(Order);
        }
    }
    else if(ExecuteElfFile(Order,User))
    {
        success = true;
    }
    else if(ExecuteCBinFile(Order,User))
    {
        success = true;
    }
    else
    {
        printf("command %s is not recognized!\n\r" , Order->Argv[0]);
        DeleteOrder(Order);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief executes order
 */
//==========================================================================================================================================
static bool ExecuteOrder( Order_t * Order , bool * outLogout , oC_User_t User )
{
    bool success = false;

    if(strcmp(Order->Argv[0],"logout") == 0)
    {
        *outLogout  = true;
        success     = true;
        DeleteOrder(Order);
    }
    else if(strcmp(Order->Argv[0],"exit") == 0)
    {
        oC_ErrorCode_t errorCode = oC_Process_Kill(oC_ProcessMan_GetCurrentProcess());

        if(oC_ErrorOccur(errorCode))
        {
            oC_PrintErrorMessage("Cannot kill process: " , errorCode);
        }
        else
        {
            success = true;
        }
        DeleteOrder(Order);
    }
    else if(strcmp(Order->Argv[0],"clear") == 0)
    {
        success = true;
        printf(oC_VT100_RESET_DEVICE);
        DeleteOrder(Order);
    }
    else if(strcmp(Order->Argv[0],"cd") == 0)
    {
        oC_Process_t    currentProcess = oC_ProcessMan_GetCurrentProcess();
        oC_ErrorCode_t  errorCode      = oC_ErrorCode_ImplementError;

        if(oC_Process_IsCorrect(currentProcess))
        {
            if(Order->Argc >= 2)
            {
                errorCode = oC_Process_SetPwd(currentProcess,Order->Argv[1]);
            }
            else
            {
                errorCode = oC_ErrorCode_NoSuchFile;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_ProcessNotCorrect;
        }

        if(oC_ErrorOccur(errorCode))
        {
            oC_PrintErrorMessage( "Cannot change directory: " , errorCode);
        }
        else
        {
            success = true;
        }
        DeleteOrder(Order);
    }
    else
    {
        success = RunProgram(Order,User);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief parses and executes command
 */
//==========================================================================================================================================
static bool ParseAndExecuteCommand( char * Command , oC_User_t User )
{
    bool        success          = false;
    char *      subCommand       = NULL;
    int         subCommandLength = 0;
    JoinType_t  joinType         = JoinType_None;
    bool        logout           = false;

    while((subCommand = GetNextSubCommand(Command,&Command,&subCommandLength,&joinType)) != NULL)
    {
        Order_t * order = NewOrderFromSubCommand(subCommand,subCommandLength);

        if(order != NULL)
        {
            success = ExecuteOrder(order,&logout,User);

            if(
                (joinType == JoinType_Or  && success == true  )
             || (joinType == JoinType_And && success == false )
                )
            {
                break;
            }
        }
        subCommandLength = 0;
    }

    return logout;
}

//==========================================================================================================================================
/**
 * @brief main chell function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    int             result          = ENOTSUP; /* Operation not supported as default for unknown error */
    oC_Thread_t     thread          = oC_ThreadMan_GetCurrentThread();
    oC_User_t       user            = NULL;
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;
    oC_Process_t    currentProcess  = oC_ProcessMan_GetCurrentProcess();
    char commandBuffer[200]                                                       = {0};
    char lastCommandsBuffer[CFG_UINT16_MAX_SAVED_COMMANDS][sizeof(commandBuffer)];

    memset(lastCommandsBuffer,0,sizeof(lastCommandsBuffer));

    while(1)
    {
#if CFG_BOOL_USE_TGUI_LOGIN
        if(oC_ArgumentOccur(Argc,Argv,"--textlogin"))
        {
            user = ShowLoginForm();
        }
        else
        {
            user = ShowTGuiLoginForm();
        }
#else
        user = ShowLoginForm();
#endif
        if(user)
        {
            result = 0;

            ShowWelcomeMessage(user);

            while(1)
            {
                memset(commandBuffer,0,sizeof(commandBuffer));

                ShowCommandIncentive(user,oC_Process_GetPwd(currentProcess));

                errorCode = ReadCommand(commandBuffer,sizeof(commandBuffer),CFG_UINT16_MAX_SAVED_COMMANDS,lastCommandsBuffer,user);

                if(oC_ErrorOccur(errorCode))
                {
                    if(errorCode == oC_ErrorCode_Timeout)
                    {
                        break;
                    }
                    else
                    {
                        printf("Error while read: %R\n\r" , errorCode);
                    }
                }

                if(ParseAndExecuteCommand(commandBuffer,user))
                {
                    break;
                }
            }
        }
        else
        {
            int secondsToSleep = 60;

            printf("Login failed!\n\r");

            while(secondsToSleep > 0)
            {
                oC_TGUI_DrawProgressBar(LoginFailureProgressBarPosition,LoginFailureProgressBarWidth,secondsToSleep,60,&LoginFailureProgressBarStyle);
                oC_TGUI_SetStyle(&ErrorMessageBoxStyle);
                oC_TGUI_DrawNCharAtPosition(ErrorMessageBoxPosition,' ',ErrorMessageBoxWidth);
                oC_TGUI_DrawFormatAtPosition(ErrorMessageBoxPosition,"Login failed, next attempt possible at %2d seconds..",secondsToSleep);
                oC_Thread_Sleep(thread,s(1));
                secondsToSleep--;
            }
        }
    }

    return result;
}

