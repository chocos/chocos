/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      The program for testing the FT5336 touch driver
 *
 * @author     Patryk Kubiak - (Created on: 2017-05-03 - 00:17:17) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_i2c.h>
#include <oc_tgui.h>
#include <oc_ft5336.h>
#include <oc_struct.h>

typedef struct
{
    uint8_t         Value;
    const char *    Name;
} RegisterValue_t;

int CurrentColumn = 0;

//==========================================================================================================================================
/**
 * @brief prints register value
 */
//==========================================================================================================================================
static void PrintRegister( oC_I2C_Context_t Context, const char * Name , oC_I2C_Register_t Offset , const RegisterValue_t * ExpectedValues, uint8_t Count )
{
    uint8_t         value     = 0;
    oC_MemorySize_t size      = sizeof(uint8_t);
    oC_ErrorCode_t  errorCode = oC_I2C_ReadRegister(Context, 0, Offset, &value, &size, ms(100));

    CurrentColumn++;

    if(errorCode == oC_ErrorCode_None)
    {

        const RegisterValue_t * definition = NULL;

        oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(ExpectedValues,Count,element)
        {
            if(element->Value == value)
            {
                definition = element;
                break;
            }
        }

        printf(oC_VT100_FG_GREEN "%25s" oC_VT100_FG_WHITE ": 0x%02X (%10s) ", Name, value, definition != NULL ? definition->Name : "HEX");

        if(CurrentColumn == 5)
        {
            printf("\n");
            CurrentColumn = 0;
        }
    }
    else
    {
        printf("Cannot read register %s: %R ", Name, errorCode);
    }
}

static const RegisterValue_t DEV_MODE_REG[] = {
                { .Value = 0x00 , .Name = "WORKING" } ,
                { .Value = 0x04 , .Name = "FACTORY" } ,
};
static const RegisterValue_t GEST_ID_REG[] = {
                { .Value = 0x00 , .Name = "NO GESTURE" } ,
                { .Value = 0x10 , .Name = "MOVE UP" } ,
                { .Value = 0x14 , .Name = "MOVE RIGHT" } ,
                { .Value = 0x18 , .Name = "MOVE DOWN" } ,
                { .Value = 0x1C , .Name = "MOVE LEFT" } ,
                { .Value = 0x20 , .Name = "SINGLE CLICK" } ,
                { .Value = 0x22 , .Name = "DOUBLE CLICK" } ,
                { .Value = 0x28 , .Name = "ROTATE CLOCKWISE" } ,
                { .Value = 0x29 , .Name = "ROTATE C CLOCKWISE" } ,
                { .Value = 0x40 , .Name = "ZOOM IN" } ,
                { .Value = 0x49 , .Name = "ZOOM OUT" } ,
};
static const RegisterValue_t STAT_REG[] = {
                { .Value = 0x00 , .Name = "NO TOUCH" } ,
                { .Value = 0x01 , .Name = "1 FINGER" } ,
                { .Value = 0x02 , .Name = "2 FINGERS" } ,
                { .Value = 0x03 , .Name = "3 FINGERS" } ,
                { .Value = 0x04 , .Name = "4 FINGERS" } ,
                { .Value = 0x05 , .Name = "5 FINGERS" } ,
};

//==========================================================================================================================================
/**
 * @brief prints program usage
 */
//==========================================================================================================================================
static void PrintUsage( int Argc , char ** Argv )
{
    printf("%s [--registers] [--beauty]\n", Argc >= 1 ? Argv[0] : "ft5336");
    puts("   where: ");
    puts("       --registers              - prints ft5336 registers state by using I2C driver directly");
    puts("       --beauty                 - prints touch data by using ft5336 driver");
    puts("       --polling-mode           - uses polling mode instead of GPIO interrupt (optional for beuaty mode)");
    puts("       --period time(s)         - in polling mode it is period of requesting of the TC state");
    puts("       --auto-config name       - use auto-configuration of the i2c");
}

//==========================================================================================================================================
/**
 * @brief prints registers by using simple I2C interface
 */
//==========================================================================================================================================
void PrintRegisters( void )
{
#ifdef oC_I2C_LLD_AVAILABLE
    oC_I2C_Config_t config = {
                 .Mode                          = oC_I2C_Mode_Master ,
                 .SpeedMode                     = oC_I2C_SpeedMode_Normal ,
                 .Master.DefaultSlaveAddress    = 0x70 ,
                 .Pins.SDA                      = oC_Pin_FT5336_SDA ,
                 .Pins.SCL                      = oC_Pin_FT5336_SCL ,
                 .AddressMode                   = oC_I2C_AddressMode_7Bits ,
    };
    oC_I2C_Context_t    context   = NULL;
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_TGUI_Key_t       key       = 0;
    oC_TGUI_Position_t  startPosition;

    oC_TGUI_ClearScreen();

    startPosition.Column = 1;
    startPosition.Line   = 1;

    oC_TGUI_SetCursorPosition(startPosition);

    startPosition.Line   = 4;

    printf("Configuration of the I2C ... ");
    printf("%R\n", errorCode = oC_I2C_Configure(&config,&context));

    if(errorCode == oC_ErrorCode_None)
    {
        printf("Please press Ctrl+C to exit ...\n" );

        while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC)
        {
            oC_TGUI_SetCursorPosition(startPosition);
            CurrentColumn = 0;
            PrintRegister(context, "DEV MODE REG   ", 0x00, DEV_MODE_REG, oC_ARRAY_SIZE(DEV_MODE_REG ) );
            PrintRegister(context, "GEST_ID_REG    ", 0x01, GEST_ID_REG , oC_ARRAY_SIZE(GEST_ID_REG  ) );
            PrintRegister(context, "STAT_REG       ", 0x02, STAT_REG    , oC_ARRAY_SIZE(STAT_REG     ) );
            PrintRegister(context, "P1_XH_REG      ", 0x03, NULL        , 0                            );
            PrintRegister(context, "P1_XL_REG      ", 0x04, NULL        , 0                            );
            PrintRegister(context, "P1_YH_REG      ", 0x05, NULL        , 0                            );
            PrintRegister(context, "P1_YL_REG      ", 0x06, NULL        , 0                            );
            PrintRegister(context, "P1_WEIGHT_REG  ", 0x07, NULL        , 0                            );
            PrintRegister(context, "P1_MISC_REG    ", 0x08, NULL        , 0                            );
            PrintRegister(context, "P2_XH_REG      ", 0x09, NULL        , 0                            );
            PrintRegister(context, "P2_XL_REG      ", 0x0A, NULL        , 0                            );
            PrintRegister(context, "P2_YH_REG      ", 0x0B, NULL        , 0                            );
            PrintRegister(context, "P2_YL_REG      ", 0x0C, NULL        , 0                            );
            PrintRegister(context, "P2_WEIGHT_REG  ", 0x0D, NULL        , 0                            );
            PrintRegister(context, "P2_MISC_REG    ", 0x0E, NULL        , 0                            );
            PrintRegister(context, "P3_XH_REG      ", 0x0F, NULL        , 0                            );
            PrintRegister(context, "P3_XL_REG      ", 0x10, NULL        , 0                            );
            PrintRegister(context, "P3_YH_REG      ", 0x11, NULL        , 0                            );
            PrintRegister(context, "P3_YL_REG      ", 0x12, NULL        , 0                            );
            PrintRegister(context, "P3_WEIGHT_REG  ", 0x13, NULL        , 0                            );
            PrintRegister(context, "P3_MISC_REG    ", 0x14, NULL        , 0                            );
            PrintRegister(context, "P4_XH_REG      ", 0x15, NULL        , 0                            );
            PrintRegister(context, "P4_XL_REG      ", 0x16, NULL        , 0                            );
            PrintRegister(context, "P4_YH_REG      ", 0x17, NULL        , 0                            );
            PrintRegister(context, "P4_YL_REG      ", 0x18, NULL        , 0                            );
            PrintRegister(context, "P4_WEIGHT_REG  ", 0x19, NULL        , 0                            );
            PrintRegister(context, "P4_MISC_REG    ", 0x1A, NULL        , 0                            );
            PrintRegister(context, "P5_XH_REG      ", 0x1B, NULL        , 0                            );
            PrintRegister(context, "P5_XL_REG      ", 0x1C, NULL        , 0                            );
            PrintRegister(context, "P5_YH_REG      ", 0x1D, NULL        , 0                            );
            PrintRegister(context, "P5_YL_REG      ", 0x1E, NULL        , 0                            );
            PrintRegister(context, "P5_WEIGHT_REG  ", 0x1F, NULL        , 0                            );
            PrintRegister(context, "P5_MISC_REG    ", 0x20, NULL        , 0                            );
            PrintRegister(context, "P6_XH_REG      ", 0x21, NULL        , 0                            );
            PrintRegister(context, "P6_XL_REG      ", 0x22, NULL        , 0                            );
            PrintRegister(context, "P6_YH_REG      ", 0x23, NULL        , 0                            );
            PrintRegister(context, "P6_YL_REG      ", 0x24, NULL        , 0                            );
            PrintRegister(context, "P6_WEIGHT_REG  ", 0x25, NULL        , 0                            );
            PrintRegister(context, "P6_MISC_REG    ", 0x26, NULL        , 0                            );
            PrintRegister(context, "P7_XH_REG      ", 0x27, NULL        , 0                            );
            PrintRegister(context, "P7_XL_REG      ", 0x28, NULL        , 0                            );
            PrintRegister(context, "P7_YH_REG      ", 0x29, NULL        , 0                            );
            PrintRegister(context, "P7_YL_REG      ", 0x2A, NULL        , 0                            );
            PrintRegister(context, "P7_WEIGHT_REG  ", 0x2B, NULL        , 0                            );
            PrintRegister(context, "P7_MISC_REG    ", 0x2C, NULL        , 0                            );
            PrintRegister(context, "P8_XH_REG      ", 0x2D, NULL        , 0                            );
            PrintRegister(context, "P8_XL_REG      ", 0x2E, NULL        , 0                            );
            PrintRegister(context, "P8_YH_REG      ", 0x2F, NULL        , 0                            );
            PrintRegister(context, "P8_YL_REG      ", 0x30, NULL        , 0                            );
            PrintRegister(context, "P8_WEIGHT_REG  ", 0x31, NULL        , 0                            );
            PrintRegister(context, "P8_MISC_REG    ", 0x32, NULL        , 0                            );
            PrintRegister(context, "P9_XH_REG      ", 0x33, NULL        , 0                            );
            PrintRegister(context, "P9_XL_REG      ", 0x34, NULL        , 0                            );
            PrintRegister(context, "P9_YH_REG      ", 0x35, NULL        , 0                            );
            PrintRegister(context, "P9_YL_REG      ", 0x36, NULL        , 0                            );
            PrintRegister(context, "P9_WEIGHT_REG  ", 0x37, NULL        , 0                            );
            PrintRegister(context, "P9_MISC_REG    ", 0x38, NULL        , 0                            );
            PrintRegister(context, "P10_XH_REG     ", 0x39, NULL        , 0                            );
            PrintRegister(context, "P10_XL_REG     ", 0x3A, NULL        , 0                            );
            PrintRegister(context, "P10_YH_REG     ", 0x3B, NULL        , 0                            );
            PrintRegister(context, "P10_YL_REG     ", 0x3C, NULL        , 0                            );
            PrintRegister(context, "P10_WEIGHT_REG ", 0x3D, NULL        , 0                            );
            PrintRegister(context, "P10_MISC_REG   ", 0x3E, NULL        , 0                            );
            PrintRegister(context, "TH_GROUP_REG   ", 0x80, NULL        , 0                            );
            PrintRegister(context, "TH_DIFF_REG    ", 0x85, NULL        , 0                            );

            sleep(ms(200));
        }
        printf("Unconfiguration of the I2C ... ");
        printf("%R\n", errorCode = oC_I2C_Unconfigure(&config,&context));
    }
#else
    printf("I2C driver not available\n");
#endif
}

//==========================================================================================================================================
/**
 * @brief prints touch data by using ft5336 driver
 */
//==========================================================================================================================================
void PrintBeauty( int Argc , char ** Argv )
{
    bool                commandCorrect = true;
    oC_FT5336_Context_t context        = NULL;
    oC_ErrorCode_t      errorCode      = oC_ErrorCode_ImplementError;
    oC_TGUI_Key_t       key            = 0;
    oC_Struct_Define( oC_FT5336_Config_t, config);
    oC_Struct_Define( oC_IDI_Event_t, event);

    config.Mode                         = oC_ArgumentOccur(Argc,Argv,"--polling-mode") ? oC_FT5336_Mode_PollingMode : oC_FT5336_Mode_InterruptMode;
    config.InterruptPin                 = oC_Pin_FT5336_IT;
    config.PollingTime                  = ms(500);
    config.I2C.AutoConfigurationName    = oC_GetArgumentAfter(Argc,Argv,"--auto-config");
    config.I2C.Context                  = NULL;
    config.I2C.SCL                      = oC_Pin_FT5336_SCL;
    config.I2C.SDA                      = oC_Pin_FT5336_SDA;

    if(config.Mode == oC_FT5336_Mode_PollingMode && oC_ArgumentOccur(Argc,Argv,"--period"))
    {
        const char * periodString = oC_GetArgumentAfter(Argc,Argv,"--period");

        if(periodString != NULL)
        {
            if(sscanf(periodString, "%f", &config.PollingTime) == oC_ErrorCode_None)
            {
                printf("The given time is not correct\n");
                commandCorrect = false;
            }
            else
            {
                PrintUsage(Argc,Argv);
            }
        }
        else
        {
            printf("Lack of time argument!\n");
            commandCorrect = false;
            PrintUsage(Argc,Argv);
        }
    }

    if(commandCorrect)
    {
        oC_TGUI_Position_t startPosition;

        oC_TGUI_ClearScreen();

        startPosition.Line      = 1;
        startPosition.Column    = 1;

        oC_TGUI_SetCursorPosition(startPosition);

        printf("Configuration of the FT5336 driver ... ");
        printf("%R\n", errorCode = oC_FT5336_Configure(&config,&context));

        startPosition.Line      = 4;

        if(errorCode == oC_ErrorCode_None)
        {
            printf("Please press Ctrl+C to exit ...\n");

            while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC)
            {
                oC_TGUI_SetCursorPosition(startPosition);

                memset(&event,0,sizeof(event));

                if( ! ErrorCode( oC_FT5336_WaitForInput(context, oC_IDI_EventId_TouchScreenEvent, &event, ms(200)) ) )
                {
                    if(errorCode != oC_ErrorCode_Timeout)
                    {
                        printf("Error: %R" oC_VT100_ERASE_END_OF_LINE "\n", errorCode);
                    }
                }

                for(uint16_t i = 0 ; i < 10; i++)
                {
                    if(i < event.NumberOfPoints)
                    {
                        printf("Detect touch at %d:%d (width: %d)" oC_VT100_ERASE_END_OF_LINE "\n", event.Position[i].X, event.Position[i].Y, event.Weight[i]);
                    }
                    else
                    {
                        printf("Touch %d not detected" oC_VT100_ERASE_END_OF_LINE "\n", i );
                    }
                }
                if(event.NumberOfPoints > 0)
                {
                    sleep(ms(200));
                }
            }

            printf("Unconfiguration of the FT5336 driver ... ");
            printf("%R\n", errorCode = oC_FT5336_Unconfigure(&config,&context));
        }
    }
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_ArgumentOccur(Argc,Argv,"--registers"))
    {
        PrintRegisters();
        errorCode = oC_ErrorCode_None;
    }
    else if(oC_ArgumentOccur(Argc,Argv,"--beauty"))
    {
        PrintBeauty(Argc,Argv);
        errorCode = oC_ErrorCode_None;
    }
    else
    {
        PrintUsage(Argc,Argv);
        errorCode = oC_ErrorCode_CommandNotCorrect;
    }

    return errorCode;
}

