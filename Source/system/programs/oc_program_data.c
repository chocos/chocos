/** ****************************************************************************************************************************************
 *
 * @file       oc_program_data.c
 *
 * @brief      The file contains data for the program
 *
 * @author     Patryk Kubiak - (Created on: 28 05 2015 20:51:20)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifdef oC_CORE_SPACE
#include <oc_program.h>

//==========================================================================================================================================
/**
 * This is the prototype for the main function of the program
 *
 * @param argc      Argument  counter
 * @param argv      Arguments array
 *
 * @return 0 if success
 */
//==========================================================================================================================================
extern int main( int Argc , const char ** Argv );

//==========================================================================================================================================
/**
 * Macro converts definition to string
 */
//==========================================================================================================================================
#define _TO_STRING(T)     #T
#define TO_STRING(T)      _TO_STRING(T)

//==========================================================================================================================================
/**
 * The structure with registration of the program
 */
//==========================================================================================================================================
const oC_Program_Registration_t ProgramRegistration = {
                                                         .Name              = TO_STRING(oC_PROGRAM_NAME) ,
                                                         .Priority          = oC_Program_Priority_CoreSpaceProgram ,
                                                         .MainFunction      = main ,
                                                         .StdInName         = TO_STRING(oC_STDIN),
                                                         .StdOutName        = TO_STRING(oC_STDOUT),
                                                         .StdErrName        = TO_STRING(oC_STDERR),
                                                         .HeapMapSize       = HEAP_MAP_SIZE,
                                                         .ThreadStackSize   = PROCESS_STACK_SIZE,
#ifdef ALLOCATION_LIMIT
                                                         .AllocationLimit   = ALLOCATION_LIMIT,
#else
                                                         .AllocationLimit   = 0 ,
#endif
#ifdef TRACK_ALLOCATION
                                                         .TrackAllocations  = true,
#else
                                                         .TrackAllocations  = false,
#endif
#ifdef ARGUMENTS_FORMAT_FILE
                                                         .ArgumentsFormatFilePath = TO_STRING(ARGUMENTS_FORMAT_FILE),
#else
                                                         .ArgumentsFormatFilePath = NULL,
#endif
};
#elif defined(oC_USER_SPACE)
#define oC_PROGRAM_DATA
#include <oc_program_types.h>
#include <oc_posix_ptrs.h>
#include <oc_array.h>
#include <oc_posix.h>

oC_SystemCall_t SystemCalls[] __attribute__ ((section (".syscalls"))) = {
#define ADD_FUNCTION(NAME)      { .Name = #NAME , .CallAddress = (void*)0xDEADBABA } ,
POSIX_FUNCTIONS_LIST(ADD_FUNCTION)
#undef ADD_FUNCTION
};

//==========================================================================================================================================
/**
 * This is the prototype for the main function of the program
 *
 * @param argc      Argument  counter
 * @param argv      Arguments array
 *
 * @return 0 if success
 */
//==========================================================================================================================================
extern int main( int Argc , const char ** Argv );


void __attribute__((optimize("O0"))) InitializeProgram( void )
{
#define ADD_FUNCTION(NAME)      NAME = SystemCalls[oC_Posix_CallId_##NAME].CallAddress;
    POSIX_FUNCTIONS_LIST(ADD_FUNCTION);
#undef ADD_FUNCTION
}

int MainFunction( int Argc , const char ** Argv )
{
    InitializeProgram();
    return main(Argc,Argv);
}

//==========================================================================================================================================
/**
 * Macro converts definition to string
 */
//==========================================================================================================================================
#define _TO_STRING(T)     #T
#define TO_STRING(T)      _TO_STRING(T)

static volatile const oC_Program_Header_t ProgramHeader __attribute__ ((section (".header"))) = {
                .HeaderSize                  = sizeof(oC_Program_Header_t) ,
                .SectionsDefinitionSize      = sizeof(oC_Program_Sections_t) ,
                .SystemCallDefinitionSize    = sizeof(oC_SystemCall_t) ,
                .MagicString                 = PROGRAM_HEADER_MAGIC_STRING ,
                .Version                     = oC_VERSION ,
                .MachineName                 = TO_STRING(MACHINE_NAME) ,

                .SystemCalls                 = SystemCalls ,
                .NumberOfSystemCalls         = oC_ARRAY_SIZE(SystemCalls),
#if defined(oC_FILE_TYPE_CBIN)
                .FileType                    = FILE_TYPE_CBIN,
#else
                .FileType                    = FILE_TYPE_ELF,
#endif
                .ProgramRegistration = {
                    .Name              = TO_STRING(oC_PROGRAM_NAME) ,
                    .Priority          = 0 ,
                    .MainFunction      = MainFunction ,
                    .StdInName         = TO_STRING(oC_STDIN),
                    .StdOutName        = TO_STRING(oC_STDOUT),
                    .StdErrName        = TO_STRING(oC_STDERR),
                    .HeapMapSize       = HEAP_MAP_SIZE,
                    .ThreadStackSize   = PROCESS_STACK_SIZE,
                    #ifdef ALLOCATION_LIMIT
                                                                             .AllocationLimit   = ALLOCATION_LIMIT,
                    #else
                                                                             .AllocationLimit   = 0 ,
                    #endif
                    #ifdef TRACK_ALLOCATION
                                                                             .TrackAllocations  = true,
                    #else
                                                                             .TrackAllocations  = false,
                    #endif
                    #ifdef ARGUMENTS_FORMAT_FILE
                                                                             .ArgumentsFormatFilePath = TO_STRING(ARGUMENTS_FORMAT_FILE),
                    #else
                                                                             .ArgumentsFormatFilePath = NULL,
                    #endif
                } ,

};

#endif
