/** ****************************************************************************************************************************************
 *
 * @file       geterrors.c
 *
 * @brief      The file contains geterrors program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_errors.h>
#include <oc_vt100.h>
#include <oc_clock_lld.h>
#include <oc_tgui.h>

//==========================================================================================================================================
/**
 * @brief prints usage message
 */
//==========================================================================================================================================
static void PrintUsage( const char * ProgramName )
{
    printf("Usage: %s [--real]\n" , ProgramName);
    printf("    where\n"
           "           --real, -r   - Waits for new logs and prints it in real time\n");
}

//==========================================================================================================================================
/**
 * @brief prints formatted error
 */
//==========================================================================================================================================
static oC_ErrorCode_t PrintError( const char * Function , uint32_t LineNumber, const char * Description, oC_ErrorCode_t ErrorCode )
{
    return printf(
                   oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_FG_WHITE  "%-40s:"
                   oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_FG_YELLOW "%-6d"
                   oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_FG_WHITE  " | %s "
                   oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_FG_RED    "%R\n"
                   ,Function, LineNumber, Description , ErrorCode);
}

//==========================================================================================================================================
/**
 * @brief main rerrors function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    const char *    description = NULL;
    const char *    function    = NULL;
    uint32_t        lineNumber  = 0;
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
    oC_TGUI_Key_t   key         = 0;

    if(oC_ArgumentOccur(Argc,Argv,"--real") || oC_ArgumentOccur(Argc,Argv,"-r"))
    {
        printf(oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_WHITE "Reading errors from the errors stack (Press ESC to exit) ...\n");

        while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ESC)
        {
            if(oC_ReadLastError(&description,&errorCode,&function,&lineNumber))
            {
                if(PrintError(function,lineNumber,description,errorCode) != oC_ErrorCode_None)
                {
                    break;
                }
            }
            else
            {
                sleep(ms(500));
            }
        }
    }
    else if(Argc == 1)
    {
        oC_SetLockSavingErrors(true);

        printf(oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_WHITE "Reading errors from the errors stack...\n\r");

        while(oC_ReadLastError(&description,&errorCode,&function,&lineNumber))
        {
            if(PrintError(function,lineNumber,description,errorCode) != oC_ErrorCode_None)
            {
                break;
            }
        }

        oC_SetLockSavingErrors(false);
    }
    else
    {
        PrintUsage(Argv[0]);
    }

    return 0;
}

