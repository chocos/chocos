/** ****************************************************************************************************************************************
 *
 * @file       system.c
 *
 * @brief      The file contains system program source
 *
 * @author     Patryk Kubiak
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_tgui.h>
#include <oc_version.h>
#include <oc_list.h>
#include <oc_debug.h>
#include <oc_boot.h>
#include <string.h>
#include <oc_threadman.h>
#include <oc_memman.h>

typedef struct
{
    char                    DefaultStackSize[20];
    char                    RedZoneSize[20];
    char                    IncreaseStackStep[20];
    char                    PanicMemoryLimit[20];
    char                    DefaultMemoryLimit[20];
    uint32_t                StackMethod;
} SystemConfig_t;

typedef struct
{
    const char *    Description;
    oC_ErrorCode_t  ErrorCode;
    const char *    Function;
    uint32_t        LineNumber;
} ErrorCodeData_t;

typedef struct
{
    char    Log[CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH];
} LogData_t;

typedef struct
{
    oC_TGUI_Position_t  Position;
    oC_TGUI_Position_t  BottomRight;
    oC_TGUI_Column_t    Width;
    oC_TGUI_Line_t      Height;
} BoxData_t;

static const BoxData_t          DetailsBoxData      = {
                .Position       = { .Column = 3 , .Line = 12 } ,
                .Width          = 75 ,
                .Height         = 10 ,
                .BottomRight    = { .Column = 3 + 75 , .Line = 12 + 10 },
};
static const oC_TGUI_BoxStyle_t MainBoxStyle = {
                .BorderStyle.Foreground     = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background     = oC_TGUI_Color_LightGray ,
                .TitleStyle.Foreground      = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background      = oC_TGUI_Color_LightGray ,
                .ShadowStyle.Background     = oC_TGUI_Color_DarkGray ,
                .ShadowStyle.Foreground     = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background     = oC_TGUI_Color_LightGray ,
                .InsideStyle.Foreground     = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_BoxStyle_t GeneralInfoBoxStyle = {
                .BorderStyle.Foreground     = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background     = oC_TGUI_Color_LightGray ,
                .TitleStyle.Foreground      = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background      = oC_TGUI_Color_LightGray ,
                .ShadowStyle.Background     = oC_TGUI_Color_DarkGray ,
                .ShadowStyle.Foreground     = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background     = oC_TGUI_Color_LightGray ,
                .InsideStyle.Foreground     = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_PropertyStyle_t PropertiesStyle = {
                .DescriptionStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .DescriptionStyle.Background = oC_TGUI_Color_LightGray ,
                .ValueStyle.Foreground       = oC_TGUI_Color_Blue ,
                .ValueStyle.Background       = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_MenuStyle_t     MenuStyle  = {
                .Border.DontDraw             = true ,
                .ActiveEntry.Foreground      = oC_TGUI_Color_LightGray ,
                .ActiveEntry.Background      = oC_TGUI_Color_DarkGray ,
                .NotActiveEntry.Foreground   = oC_TGUI_Color_DarkGray ,
                .NotActiveEntry.Background   = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_QuickEditBoxStyle_t QuickEditBoxStyle = {
                .ActiveBorder.DontDraw       = true ,
                .NotActiveBorder.DontDraw    = true ,
                .ActiveText.Foreground       = oC_TGUI_Color_Yellow ,
                .ActiveText.Background       = oC_TGUI_Color_DarkGray ,
                .NotActiveText.Foreground    = oC_TGUI_Color_DarkGray ,
                .NotActiveText.Background    = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_Style_t             LabelStyle        = {
                .Foreground                  = oC_TGUI_Color_DarkGray ,
                .Background                  = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_SelectionBoxStyle_t SelectionBoxStyle = {
                .Arrow.Foreground            = oC_TGUI_Color_DarkGray ,
                .Arrow.Background            = oC_TGUI_Color_LightGray ,
                .Active.Foreground           = oC_TGUI_Color_Yellow ,
                .Active.Background           = oC_TGUI_Color_DarkGray ,
                .NotActive.Foreground        = oC_TGUI_Color_DarkGray ,
                .NotActive.Background        = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_PushButtonStyle_t PushButtonStyle = {
                .ActiveBorder.DontDraw      = true ,
                .Active.Foreground          = oC_TGUI_Color_Yellow ,
                .Active.Background          = oC_TGUI_Color_DarkGray ,
                .NotActive.Foreground       = oC_TGUI_Color_White ,
                .NotActive.Background       = oC_TGUI_Color_DarkGray ,
};

static const oC_String_t AutoStackMethodsStrings[] = {
                "disabled" ,
                "rerun" ,
};

//==========================================================================================================================================
/**
 * Shows general informations about the system
 *
 * @param TopLeft       Position of the top left corner for the general info box
 * @param Width         Width of the box
 * @param Height        Height of the box
 */
//==========================================================================================================================================
static void ShowGeneralInformations( oC_TGUI_Position_t TopLeft , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height )
{
    oC_TGUI_Position_t              generalInfoBoxPosition  = TopLeft;
    oC_TGUI_Column_t                generalInfoBoxWidth     = Width;
    oC_TGUI_Line_t                  generalInfoBoxHeight    = Height;
    oC_TGUI_Property_t              properties[]            = {
        { .Description = "Version:     " , .Format = "%s" , .Type = oC_TGUI_ValueType_ValueString , .ValueString = oC_FULL_VERSION_STRING } ,
        { .Description = "Build date:  " , .Format = "%s" , .Type = oC_TGUI_ValueType_ValueString , .ValueString = __DATE__ } ,
        { .Description = "Build time:  " , .Format = "%s" , .Type = oC_TGUI_ValueType_ValueString , .ValueString = __TIME__ } ,
        { .Description = "Word size:   " , .Format = "%d" , .Type = oC_TGUI_ValueType_UINT        , .ValueUINT   = MACHINE_WORD_SIZE } ,
        { .Description = "CPU arch:    " , .Format = "%s" , .Type = oC_TGUI_ValueType_ValueString , .ValueString = oC_TO_STRING(MACHINE_CORE_ARCHITECTURE_FAMILY) } ,
        { .Description = "Startup time:" , .Format = "%.3fs" , .Type = oC_TGUI_ValueType_ValueDouble , .ValueDouble = oC_Boot_GetStartupTimestamp() } ,
    };
    oC_TGUI_DrawPropertyConfig_t    propertyConfig          = {
                    .StartPosition      = { .Column = TopLeft.Column + 1 , .Line = TopLeft.Line + 1 } ,
                    .Width              = Width - 3 ,
                    .Height             = Height - 3 ,
                    .DescriptionWidth   = 13 ,
                    .Properties         = properties ,
                    .NumberOfProperties = oC_ARRAY_SIZE(properties),
                    .Style              = &PropertiesStyle
    };
    oC_TGUI_DrawBox(" GENERAL " , NULL , generalInfoBoxPosition , generalInfoBoxWidth , generalInfoBoxHeight , &GeneralInfoBoxStyle);

    oC_TGUI_DrawProperties(&propertyConfig);
}

//==========================================================================================================================================
/**
 * Draws system error (function required by the TGUI)
 */
//==========================================================================================================================================
static void DrawSystemError( oC_TGUI_Position_t Position , oC_List_ElementHandle_t ListElement , oC_TGUI_Column_t Width )
{
    if(isaddresscorrect(ListElement))
    {
        ErrorCodeData_t data = oC_List_ElementHandle_Value(ListElement,ErrorCodeData_t);

        oC_TGUI_DrawAtPositionWithSize(Position,data.Description,Width / 2 - 1);
        Position.Column += Width / 2;
        oC_TGUI_DrawAtPositionWithSize(Position,oC_GetErrorString(data.ErrorCode),Width / 2);
    }
    else
    {
        oC_TGUI_DrawAtPosition(Position,"Invalid list handle");
    }
}

//==========================================================================================================================================
/**
 * Draws system error (function required by the TGUI)
 */
//==========================================================================================================================================
static void DrawSystemLog( oC_TGUI_Position_t Position , oC_List_ElementHandle_t ListElement , oC_TGUI_Column_t Width )
{
    if(isaddresscorrect(ListElement))
    {
        LogData_t data = oC_List_ElementHandle_Value(ListElement,LogData_t);

        oC_TGUI_DrawAtPositionWithSize(Position,data.Log,Width);
    }
    else
    {
        oC_TGUI_DrawAtPosition(Position,"Invalid list handle");
    }
}

//==========================================================================================================================================
/**
 * Clears errors logged in the system
 */
//==========================================================================================================================================
static void ClearLoggedErrors( void * Parameter )
{
    const char *    description = NULL;
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
    const char *    function    = NULL;
    uint32_t        lineNumber  = 0;

    while(oC_ReadLastError(&description,&errorCode,&function,&lineNumber));
}

//==========================================================================================================================================
/**
 * Clears system logs
 */
//==========================================================================================================================================
static void ClearSystemLogs( void * Parameter )
{
    char log[CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH];

    while(readlastkdebuglog(log,CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH));
}

//==========================================================================================================================================
/**
 * Shows system errors
 * @param Parameter
 */
//==========================================================================================================================================
static void ShowSystemErrors( void * Parameter )
{
    oC_List(ErrorCodeData_t) errorsList = oC_List_New(getcurallocator(),0);

    if(oC_List_IsCorrect(errorsList))
    {
        bool errorStackNotEmpty = true;

        while(errorStackNotEmpty)
        {
            ErrorCodeData_t data = {0};

            errorStackNotEmpty = oC_ReadLastError(&data.Description, &data.ErrorCode, &data.Function, &data.LineNumber);

            if(errorStackNotEmpty)
            {
                oC_List_PushBack(errorsList,data,getcurallocator());
            }
        }

        oC_TGUI_DrawListMenuConfig_t  config = {
                        .TopLeft                = { .Column = DetailsBoxData.Position.Column + 1 , .Line = DetailsBoxData.Position.Line + 1} ,
                        .Width                  = DetailsBoxData.Width  - 3,
                        .Height                 = DetailsBoxData.Height - 4,
                        .List                   = (oC_List_t)errorsList ,
                        .DrawHandler            = DrawSystemError,
                        .SelectHandler          = NULL ,
                        .SelectHandlerParameter = NULL ,
                        .Style                  = &MenuStyle ,
        };

        oC_TGUI_DrawBox(" LOGGED ERRORS ", NULL , DetailsBoxData.Position , DetailsBoxData.Width,DetailsBoxData.Height,&GeneralInfoBoxStyle);

        oC_TGUI_DrawListMenu(&config);

        for(oC_List_ElementHandle_t handle = oC_List_GetFirstElementHandle(errorsList); handle != NULL ; handle = oC_List_ElementHandle_Next(handle))
        {
            ErrorCodeData_t data = oC_List_ElementHandle_Value(handle,ErrorCodeData_t);
            oC_SaveError(data.Description,data.ErrorCode);
        }

        oC_List_Delete(errorsList,0);

        oC_TGUI_DrawFillBetween(DetailsBoxData.Position,DetailsBoxData.BottomRight,' ');
    }
    else
    {
        oC_SaveError("system: Cannot create error code list" , oC_ErrorCode_AllocationError);
    }
}

//==========================================================================================================================================
/**
 * Shows system logs
 */
//==========================================================================================================================================
static void ShowSystemLogs( void * Parameter )
{
    oC_List(LogData_t) errorsList = oC_List_New(getcurallocator(),0);

    if(oC_List_IsCorrect(errorsList))
    {
        bool logsStackNotEmpty = true;

        lockkdebuglog();
        while(logsStackNotEmpty)
        {
            LogData_t data;

            logsStackNotEmpty = readlastkdebuglog(data.Log,CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH);

            if(logsStackNotEmpty)
            {
                oC_List_PushBack(errorsList,data,getcurallocator());
            }
        }
        unlockkdebuglog();

        oC_TGUI_DrawListMenuConfig_t  config = {
                        .TopLeft                = { .Column = DetailsBoxData.Position.Column + 1 , .Line = DetailsBoxData.Position.Line + 1} ,
                        .Width                  = DetailsBoxData.Width  - 3,
                        .Height                 = DetailsBoxData.Height - 4,
                        .List                   = (oC_List_t)errorsList ,
                        .DrawHandler            = DrawSystemLog,
                        .SelectHandler          = NULL ,
                        .SelectHandlerParameter = NULL ,
                        .Style                  = &MenuStyle ,
        };

        oC_TGUI_DrawBox(" SYSTEM LOGS ", NULL , DetailsBoxData.Position , DetailsBoxData.Width,DetailsBoxData.Height,&GeneralInfoBoxStyle);

        oC_TGUI_DrawListMenu(&config);

        for(oC_List_ElementHandle_t handle = oC_List_GetFirstElementHandle(errorsList); handle != NULL ; handle = oC_List_ElementHandle_Next(handle))
        {
            LogData_t data = oC_List_ElementHandle_Value(handle,LogData_t);
            savekdebuglog(data.Log);
        }

        oC_List_Delete(errorsList,0);

        oC_TGUI_DrawFillBetween(DetailsBoxData.Position,DetailsBoxData.BottomRight,' ');
    }
    else
    {
        oC_SaveError("system: Cannot create error code list" , oC_ErrorCode_AllocationError);
    }
}

//==========================================================================================================================================
/**
 * Saves configuration (called by TGUI)
 */
//==========================================================================================================================================
static bool SaveConfig( void * Parameter )
{
    SystemConfig_t * config = Parameter;

    if(isaddresscorrect(config))
    {
        unsigned int    size        = 0;
        float           limit       = 0;
        oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;

        errorCode = sscanf(config->DefaultStackSize,"%u",&size);

        if(oC_ErrorOccur(errorCode) == false)
        {
            oC_ThreadMan_SetDefaultStackSize(size);
        }

        errorCode = sscanf(config->IncreaseStackStep,"%u",&size);

        if(oC_ErrorOccur(errorCode) == false)
        {
            oC_ThreadMan_SetStackIncreaseStep(size);
        }

        errorCode = sscanf(config->RedZoneSize,"%u",&size);

        if(oC_ErrorOccur(errorCode) == false)
        {
            oC_ThreadMan_SetRedZoneSize(size);
        }

        oC_ThreadMan_SetAutoStackMethod(config->StackMethod);

        errorCode = sscanf(config->PanicMemoryLimit,"%f",&limit);

        if(oC_ErrorOccur(errorCode) == false)
        {
            oC_MemMan_SetPanicMemoryExhaustedLimit(limit);
        }

        errorCode = sscanf(config->DefaultMemoryLimit,"%f",&limit);

        if(oC_ErrorOccur(errorCode) == false)
        {
            oC_MemMan_SetMemoryExhaustedLimit(limit);
        }
    }

    return true;
}

//==========================================================================================================================================
/**
 * Shows system configuration screen
 */
//==========================================================================================================================================
static void ShowSystemConfig( void * Parameter )
{
    oC_TGUI_Position_t          insideTopLeft   = { .Column = DetailsBoxData.Position.Column + 1 , .Line = DetailsBoxData.Position.Line + 1};
    oC_TGUI_Column_t            columnWidth     = DetailsBoxData.Width / 2 - 2;
    oC_TGUI_Column_t            secondColumn    = insideTopLeft.Column + columnWidth + 1;
    oC_TGUI_Column_t            itemValueWidth  = 10;
    oC_TGUI_Column_t            itemTitleWidth  = columnWidth - itemValueWidth;
    SystemConfig_t              systemConfig;
    memset(&systemConfig, 0, sizeof(systemConfig));
    oC_TGUI_ActiveObject_t      activeObjects[] = {
                    /* Default stack size */
                    {
                        .Type                        = oC_TGUI_ActiveObjecType_QuickEdit ,
                        .Position                    = { .Column = insideTopLeft.Column + itemTitleWidth , .Line = insideTopLeft.Line } ,
                        .LabelPosition               = { .Column = insideTopLeft.Column                  , .Line = insideTopLeft.Line } ,
                        .Width                       = itemValueWidth ,
                        .LabelText                   = "Default stack size:" ,
                        .LabelWidth                  = itemTitleWidth ,
                        .LabelStyle                  = &LabelStyle ,
                        .QuickEdit.Buffer            = systemConfig.DefaultStackSize,
                        .QuickEdit.BufferSize        = sizeof(systemConfig.DefaultStackSize),
                        .QuickEdit.InputType         = oC_TGUI_InputType_Digits ,
                        .QuickEdit.SaveHandler       = NULL ,
                        .QuickEdit.SaveParameter     = NULL ,
                        .QuickEdit.Style             = &QuickEditBoxStyle ,
                    } ,
                    /* Automatic increasing stack size */
                    {
                        .Type                           = oC_TGUI_ActiveObjecType_SelectionBox ,
                        .Position                       = { .Column = insideTopLeft.Column + itemTitleWidth , .Line = insideTopLeft.Line + 1} ,
                        .LabelPosition                  = { .Column = insideTopLeft.Column                  , .Line = insideTopLeft.Line + 1} ,
                        .Width                          = itemValueWidth ,
                        .LabelText                      = "Auto-increase stack:" ,
                        .LabelWidth                     = itemTitleWidth ,
                        .LabelStyle                     = &LabelStyle ,
                        .SelectionBox.SelectedIndex     = &systemConfig.StackMethod ,
                        .SelectionBox.Options           = AutoStackMethodsStrings ,
                        .SelectionBox.NumberOfOptions   = 2 ,
                        .SelectionBox.Style             = &SelectionBoxStyle ,
                        .SelectionBox.OnChangeHandler   = NULL ,
                        .SelectionBox.OnChangeParameter = NULL ,
                    } ,
                    /* Red zone size */
                    {
                        .Type                        = oC_TGUI_ActiveObjecType_QuickEdit ,
                        .Position                    = { .Column = insideTopLeft.Column + itemTitleWidth , .Line = insideTopLeft.Line + 2} ,
                        .LabelPosition               = { .Column = insideTopLeft.Column                  , .Line = insideTopLeft.Line + 2} ,
                        .Width                       = itemValueWidth ,
                        .LabelText                   = "Red-zone size:" ,
                        .LabelWidth                  = itemTitleWidth ,
                        .LabelStyle                  = &LabelStyle ,
                        .QuickEdit.Buffer            = systemConfig.RedZoneSize,
                        .QuickEdit.BufferSize        = sizeof(systemConfig.RedZoneSize),
                        .QuickEdit.InputType         = oC_TGUI_InputType_Digits ,
                        .QuickEdit.SaveHandler       = NULL ,
                        .QuickEdit.SaveParameter     = NULL ,
                        .QuickEdit.Style             = &QuickEditBoxStyle ,
                    } ,
                    /* Increase stack step size */
                    {
                        .Type                        = oC_TGUI_ActiveObjecType_QuickEdit ,
                        .Position                    = { .Column = insideTopLeft.Column + itemTitleWidth , .Line = insideTopLeft.Line + 3} ,
                        .LabelPosition               = { .Column = insideTopLeft.Column                  , .Line = insideTopLeft.Line + 3} ,
                        .Width                       = itemValueWidth ,
                        .LabelText                   = "Auto-Increase step:" ,
                        .LabelWidth                  = itemTitleWidth ,
                        .LabelStyle                  = &LabelStyle ,
                        .QuickEdit.Buffer            = systemConfig.IncreaseStackStep,
                        .QuickEdit.BufferSize        = sizeof(systemConfig.IncreaseStackStep),
                        .QuickEdit.InputType         = oC_TGUI_InputType_Digits ,
                        .QuickEdit.SaveHandler       = NULL ,
                        .QuickEdit.SaveParameter     = NULL ,
                        .QuickEdit.Style             = &QuickEditBoxStyle ,
                    } ,
                    /* Panic memory exhausted limit */
                    {
                        .Type                        = oC_TGUI_ActiveObjecType_QuickEdit ,
                        .Position                    = { .Column = secondColumn + itemTitleWidth , .Line = insideTopLeft.Line } ,
                        .LabelPosition               = { .Column = secondColumn                  , .Line = insideTopLeft.Line } ,
                        .Width                       = itemValueWidth ,
                        .LabelText                   = "Panic-memory-margin [%]:" ,
                        .LabelWidth                  = itemTitleWidth ,
                        .LabelStyle                  = &LabelStyle ,
                        .QuickEdit.Buffer            = systemConfig.PanicMemoryLimit,
                        .QuickEdit.BufferSize        = sizeof(systemConfig.PanicMemoryLimit),
                        .QuickEdit.InputType         = oC_TGUI_InputType_Digits ,
                        .QuickEdit.SaveHandler       = NULL ,
                        .QuickEdit.SaveParameter     = NULL ,
                        .QuickEdit.Style             = &QuickEditBoxStyle ,
                    } ,
                    /* Increase stack step size */
                    {
                        .Type                        = oC_TGUI_ActiveObjecType_QuickEdit ,
                        .Position                    = { .Column = secondColumn + itemTitleWidth , .Line = insideTopLeft.Line + 1} ,
                        .LabelPosition               = { .Column = secondColumn                  , .Line = insideTopLeft.Line + 1} ,
                        .Width                       = itemValueWidth ,
                        .LabelText                   = "Memory-margin [%]:" ,
                        .LabelWidth                  = itemTitleWidth ,
                        .LabelStyle                  = &LabelStyle ,
                        .QuickEdit.Buffer            = systemConfig.DefaultMemoryLimit,
                        .QuickEdit.BufferSize        = sizeof(systemConfig.DefaultMemoryLimit),
                        .QuickEdit.InputType         = oC_TGUI_InputType_Digits ,
                        .QuickEdit.SaveHandler       = NULL ,
                        .QuickEdit.SaveParameter     = NULL ,
                        .QuickEdit.Style             = &QuickEditBoxStyle ,
                    } ,
                    /* Save button */
                    {
                        .Type                        = oC_TGUI_ActiveObjecType_PushButton ,
                        .Position                    = { .Column = secondColumn + 5  , .Line = insideTopLeft.Line + 6} ,
                        .Width                       = 10 ,
                        .Height                      = 3 ,
                        .PushButton.Text             = "Save" ,
                        .PushButton.PressHandler     = SaveConfig ,
                        .PushButton.PressParameter   = &systemConfig,
                        .PushButton.Style            = &PushButtonStyle,
                    } ,
                    /* Discard button */
                    {
                        .Type                        = oC_TGUI_ActiveObjecType_PushButton ,
                        .Position                    = { .Column = secondColumn + 17 , .Line = insideTopLeft.Line + 6} ,
                        .Width                       = 11 ,
                        .Height                      = 3 ,
                        .PushButton.Text             = "Discard" ,
                        .PushButton.PressHandler     = NULL ,
                        .PushButton.PressParameter   = NULL,
                        .PushButton.Style            = &PushButtonStyle,
                    } ,
    };
    oC_TGUI_DrawBox(" CONFIG " , NULL , DetailsBoxData.Position,DetailsBoxData.Width,DetailsBoxData.Height,&GeneralInfoBoxStyle);

    sprintf(systemConfig.DefaultStackSize  , "%llu"    , oC_ThreadMan_GetDefaultStackSize());
    sprintf(systemConfig.RedZoneSize       , "%llu"    , oC_ThreadMan_GetRedZoneSize());
    sprintf(systemConfig.IncreaseStackStep , "%llu"    , oC_ThreadMan_GetStackIncreaseStep());
    sprintf(systemConfig.PanicMemoryLimit  , "%.0f" , oC_MemMan_GetPanicMemoryExhaustedLimit());
    sprintf(systemConfig.DefaultMemoryLimit, "%.0f" , oC_MemMan_GetMemoryExhaustedLimit());

    systemConfig.StackMethod = oC_ThreadMan_GetAutoStackMethod();

    oC_TGUI_DrawActiveObjects(activeObjects,oC_ARRAY_SIZE(activeObjects));

    oC_TGUI_SetStyle(&LabelStyle);
    oC_TGUI_DrawFillBetween(DetailsBoxData.Position,DetailsBoxData.BottomRight,' ');
}

//==========================================================================================================================================
/**
 * Show the main menu
 *
 * @param TopLeft   top-left menu corner position
 * @param Width     width of the box menu
 * @param Height    height of the box menu
 */
//==========================================================================================================================================
static void ShowMainMenu( oC_TGUI_Position_t TopLeft , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height )
{
    oC_TGUI_MenuEntry_t menuEntries[]   = {
         { .Title = "Config"              , .Help = "Shows system configuration"    , .Handler = ShowSystemConfig  , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
         { .Title = "Show Logs"           , .Help = "Shows system logs"             , .Handler = ShowSystemLogs    , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
         { .Title = "Clear Logs"          , .Help = "Clears logs"                   , .Handler = ClearSystemLogs   , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
         { .Title = "Show Errors"         , .Help = "Shows logged errors"           , .Handler = ShowSystemErrors  , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
         { .Title = "Clear Errors"        , .Help = "Clears logged errors"          , .Handler = ClearLoggedErrors , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
         { .Title = "Exit"                , .Help = "Close the program"             , .Handler = NULL              , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
    };

    oC_TGUI_DrawMenu(TopLeft,Width,Height,menuEntries,oC_ARRAY_SIZE(menuEntries),&MenuStyle);
}

//==========================================================================================================================================
/**
 * @brief main system function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_TGUI_Position_t      mainBoxPosition     = { .Column = 1 , .Line = 1 };
    oC_TGUI_Column_t        mainBoxWidth        = oC_TGUI_DefaultScreenWidth;
    oC_TGUI_Line_t          mainBoxHeight       = oC_TGUI_DefaultScreenHeight;
    oC_TGUI_Position_t      generalInfoPosition = { .Column = 3 , .Line = 2 };
    oC_TGUI_Column_t        generalInfoWidth    = 35;
    oC_TGUI_Column_t        generalInfoHeight   = 9;
    oC_TGUI_Position_t      mainMenuPosition    = { .Column = 40 , .Line = 3 };
    oC_TGUI_Column_t        mainMenuWidth       = 35;
    oC_TGUI_Column_t        mainMenuHeight      = 7;

    oC_TGUI_ResetDevice();

    oC_TGUI_DrawBox("[ SYSTEM INFO ]" , NULL , mainBoxPosition , mainBoxWidth , mainBoxHeight , &MainBoxStyle);

    ShowGeneralInformations(generalInfoPosition,generalInfoWidth,generalInfoHeight);

    ShowMainMenu(mainMenuPosition,mainMenuWidth,mainMenuHeight);

    oC_TGUI_ResetDevice();

    return 0;
}

