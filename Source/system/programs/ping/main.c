/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the ping program
 *
 * @author     Patryk Kubiak - (Created on: 2016-11-11 - 21:04:00) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_tgui.h>
#include <oc_ktime.h>
#include <oc_icmp.h>

#define STRING_TO_SEND              "ChocoOS is the best OS in the world"

void Usage( const char * ProgramName )
{
    printf("Usage:     \n");
    printf("     %s IP \n", ProgramName);
}

//==========================================================================================================================================
/**
 * Prints buffer in the user friendly format
 */
//==========================================================================================================================================
void PrintBuffer( void * Buffer, uint16_t Size )
{
    uint8_t *                    array                   = Buffer;
    static const oC_TGUI_Color_t dataForegroundColors[4] = {
                    oC_TGUI_Color_Blue ,
                    oC_TGUI_Color_LightGreen ,
                    oC_TGUI_Color_Cyan ,
                    oC_TGUI_Color_Magenda
    };

    for(uint16_t offset = 0, row = 0, column = 0 ; offset < Size ; offset++)
    {
        oC_TGUI_SetForegroundColor(dataForegroundColors[column % 4]);
        printf("%02X ", (char)array[offset]);
        oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);

        column++;

        if(column >= 32)
        {
            printf(" [ row: %d ]\n        ", row);
            row++;
            column = 0;
        }
    }

    printf("\n");
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_Net_Address_t        address;
    oC_ErrorCode_t          errorCode           = oC_ErrorCode_ImplementError;
    oC_TGUI_Key_t           key                 = 0;
    oC_Icmp_Message_Echo_t* echoMessage         = NULL;
    oC_Icmp_Message_Echo_t* receivedEchoMessage = NULL;
    uint16_t                sequenceId          = 1;

    struct
    {
        oC_Timestamp_t      Timestamp;
        char                String[sizeof(STRING_TO_SEND)];
    } * payload , * receivedPayload;

	if(Argc != 2 || oC_ErrorOccur(oC_Net_AddressFromString(Argv[1],&address)))
	{
	    Usage(Argv[0]);
	}
	else
	{
	    printf("Reserving ICMP echo reply type...\n");
	    if(
            ErrorCode(oC_Icmp_ReserveType(oC_Icmp_Type_EchoReply,s(3)))
            )
	    {
	        oC_Icmp_Packet_t * packet           = oC_Icmp_Packet_New(getcurallocator(), (oC_Net_PacketType_t)address.Type, oC_Icmp_Type_EchoRequest, 0, NULL, sizeof(oC_Icmp_Message_Echo_t));
	        oC_Icmp_Packet_t * receivedPacket   = malloc( sizeof(oC_Icmp_Packet_t), AllocationFlags_ZeroFill );

	        if( packet != NULL && receivedPacket != NULL)
	        {
	            echoMessage = oC_Icmp_Packet_GetMessageReference(packet);

	            if(echoMessage != NULL)
	            {
	                payload = (void*)echoMessage->Payload;

	                memcpy(payload->String,STRING_TO_SEND,strlen(STRING_TO_SEND) + 1);

                    printf("Ping %s (0x%08X)...(Press Ctrl+C to finish)\n", Argv[1], address.IPv4);

                    while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC )
                    {
                        echoMessage->ID                 = 0xBABA;
                        echoMessage->SequenceNumber     = sequenceId;
                        payload->Timestamp              = oC_KTime_GetTimestamp();

                        if( ErrorCode( oC_Icmp_Send( NULL, &address, packet, s(3) ) ) )
                        {
                            if( ErrorCode( oC_Icmp_Receive(NULL, receivedPacket , oC_Icmp_Type_EchoReply, s(1))  ))
                            {
                                receivedEchoMessage = oC_Icmp_Packet_GetMessageReference(receivedPacket);

                                if(receivedEchoMessage != NULL)
                                {
                                    receivedPayload = (void*)receivedEchoMessage->Payload;

                                    if(receivedEchoMessage->ID == 0xBABA && receivedEchoMessage->SequenceNumber == sequenceId)
                                    {
                                        oC_Time_t receptionTime = oC_KTime_GetTimestamp() - receivedPayload->Timestamp;

                                        printf("Received reply with %d bytes after %7.3f ms (ICMP Sequence = %d, TTL = %d (received string = %s)\n",
                                               receivedPacket->IPv4.Header.Length,
                                               receptionTime * 1000, /* we want time in [ms] */
                                               receivedEchoMessage->SequenceNumber,
                                               receivedPacket->IPv4.Header.TTL,
                                               receivedPayload->String);

                                        sequenceId++;
                                    }
                                    else
                                    {
                                        printf("Incorrect packet received!\n");

                                        PrintBuffer(receivedPacket, oC_Net_GetPacketSize(&receivedPacket->Packet, true));

                                    }
                                }
                                else
                                {
                                    printf("Incorrect packet received!\n");

                                    PrintBuffer(receivedPacket, oC_Net_GetPacketSize(&receivedPacket->Packet, true));
                                }
                                memset(receivedPacket,0,sizeof(oC_Icmp_Packet_t));
                            }
                            else
                            {
                                oC_PrintErrorMessage("Packet not received - ", errorCode);
                            }
                        }
                        else
                        {
                            oC_PrintErrorMessage("Cannot send packet - ", errorCode);
                        }
                        sleep(s(1));
                    }
	            }
	            else
	            {
	                printf("Cannot get packet message reference!\n");
	            }

                oC_Icmp_Packet_Delete(&packet);
                kfree(receivedPacket,0);
	        }
	        else
	        {
	            printf("Cannot allocate memory for a packet!\n");
	        }

            printf("Releasing ICMP echo reply type...\n");

            if( ErrorCode(oC_Icmp_ReleaseType(oC_Icmp_Type_EchoReply,s(3))) == false )
            {
                oC_PrintErrorMessage("Cannot release ICMP echo reply type - ", errorCode);
            }
	    }
	    else
	    {
	        oC_PrintErrorMessage("Cannot reserve ICMP echo reply type - ", errorCode);
	    }
	}
    return 0;
}
