/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the ifconfig program
 *
 * @author     Patryk Kubiak - (Created on: 2016-09-22 - 13:27:38) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_netifman.h>
#include <oc_stdlib.h>
#include <string.h>
#include <oc_tgui.h>
#include <oc_userman.h>
#include <oc_process.h>
#include <oc_udp.h>
#include <oc_icmp.h>
#include <oc_tcp.h>

typedef enum
{
    Action_PrintUsage          ,
    Action_ListDevices         ,
    Action_PrintOneNetif       ,
    Action_PerformLoopbackTest ,
    Action_PerformDiagnostics  ,
    Action_Listen              ,
} Action_t;

typedef struct
{
    const char *    ProgramName;
    bool            Verbose;
    Action_t        Action;
    oC_Netif_t      Netif;
    oC_Net_Layer_t  Layer;
    bool            LayerSet;
} Context_t;

//==========================================================================================================================================
/**
 * @brief prints 'usage' message
 */
//==========================================================================================================================================
static void PrintUsage( Context_t * Context )
{
    printf("Usage: %s [friendly_name] [layer] [-l] [-v] [-h] [-d]                    \n" , Context->ProgramName );
    printf("   where:                                                                \n" );
    printf("         -v, --verbose        - be verbose                               \n" );
    printf("         -l, --loopback-test  - perform loopback test                    \n" );
    printf("         -h, --help           - print help                               \n" );
    printf("         -d, --diagnostics    - perform diagnostic                       \n" );
    printf("         -n, --listen         - prints all received packets              \n" );
    printf("         friendly_name        - friendly name of an network interface    \n" );
    printf("                                to select and work with                  \n" );
    printf("         layer                - layer of the network OSI to perform test \n" );
}

//==========================================================================================================================================
/**
 * @brief performs loopback test at the given OSI model layer
 */
//==========================================================================================================================================
bool PerformLoopbackTest( oC_Netif_t Netif , oC_Net_Layer_t Layer )
{
    bool           passed           = false;
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    char           layerName[20]    = {0};

    if(ErrorCode(oC_Net_ReadLayerName(Layer,layerName,sizeof(layerName))))
    {
        oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);
        printf("Performing loopback test for layer `%s`\n", layerName);

        if(ErrorCode(oC_Netif_SetLoopback(Netif,Layer,true)))
        {
            oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
            printf("Loopback enabled, preparing packet...\n");

            oC_Net_Packet_t * packet = malloc(sizeof(oC_Net_Packet_t),AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill);

            if(ErrorCondition(packet != NULL, oC_ErrorCode_AllocationError))
            {
                for(int i = 0; i < sizeof(packet->IPv4.Data); i++)
                {
                    packet->IPv4.Data[i] = 0xFF;
                }

                packet->IPv4.Header.Version         = oC_Net_PacketType_IPv4;
                packet->IPv4.Header.TTL             = 255;
                packet->IPv4.Header.DestinationIp   = IP(0xFF,0xFF,0xFF,0xFF);

                oC_Net_Packet_SetSize(packet,sizeof(packet->IPv4.Data));

                oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                printf("Packet prepared, sending...\n");

                if(ErrorCode(oC_Netif_SendPacket(Netif,packet,s(3))))
                {
                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                    printf("Packet send, receiving response...\n");

                    oC_Net_Packet_t * receivedPacket = packet;

                    if(ErrorCode(oC_Netif_ReceivePacket(Netif, receivedPacket, sizeof(oC_Net_Packet_t) ,s(3))))
                    {
                        oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                        printf("Packet received, comparing...\n");

                        if(
                            ErrorCondition( isram(receivedPacket)                                                           ,  oC_ErrorCode_WrongAddress   )
                         && ErrorCondition( oC_Net_GetPacketSize(receivedPacket,true) == oC_Net_GetPacketSize(packet,true)  ,  oC_ErrorCode_SizeNotCorrect )
                            )
                        {
                            bool dataCorrect = oC_Net_GetPacketSize(receivedPacket,false) > 0;

                            for(int i = 0 ; i < oC_Net_GetPacketSize(receivedPacket,false); i++)
                            {
                                if(receivedPacket->IPv4.Data[i] != 0xFF)
                                {
                                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
                                    printf("Wrong value at offset 0x%X - 0x%X != 0x%X\n", receivedPacket->IPv4.Data[i], 0xFF);
                                    dataCorrect = false;
                                    break;
                                }
                            }

                            if(dataCorrect)
                            {
                                oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                                printf("Data correct!\n");
                            }
                            else
                            {
                                oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
                                printf("Data incorrect! Size = %d\n", oC_Net_GetPacketSize(receivedPacket,false));
                            }
                        }
                        else
                        {
                            oC_PrintErrorMessage("Incorrect packet received - ", errorCode);
                        }

                        oC_SaveIfFalse("cannot release packet - " , kfree(receivedPacket,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError );
                    }
                    else
                    {
                        oC_PrintErrorMessage("Cannot receive packet - ", errorCode);
                    }
                }
                else
                {
                    oC_PrintErrorMessage("Cannot send packet - ", errorCode);
                }
            }
            else
            {
                oC_PrintErrorMessage("Cannot prepare packet - ", errorCode);
            }

            if(ErrorCode(oC_Netif_SetLoopback(Netif,Layer,false)))
            {
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);
                printf("Loopback mode correctly disabled\n");
            }
            else
            {
                oC_PrintErrorMessage("Cannot disable loopback - ", errorCode);
            }
        }
        else
        {
            oC_PrintErrorMessage("Cannot set loopback - ", errorCode);
        }

    }
    else
    {
        oC_PrintErrorMessage("Layer is not correct - ", errorCode);
    }

    return passed;
}

//==========================================================================================================================================
/**
 * @brief prints diagnostic state
 */
//==========================================================================================================================================
void PrintDiagnosticState( oC_Diag_t * Diag )
{
    static const char * States[] = {
           [oC_Diag_State_NotStarted ] = "Not started" ,
           [oC_Diag_State_Prepared   ] = "Prepared" ,
           [oC_Diag_State_InProgress ] = "In Progress" ,
           [oC_Diag_State_Finished   ] = "Finished"
    };

    if(isram(Diag))
    {
        oC_STATIC_ASSERT( oC_Diag_State_NumberOfStates == oC_ARRAY_SIZE(States) , "At least one of available states is not supported");
        oC_STATIC_ASSERT( oC_Diag_State_NumberOfStates <  20                    , "Number of states exceeds the limit!");

        if(Diag->State < oC_Diag_State_NumberOfStates)
        {
            oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);
            printf(oC_VT100_ERASE_LINE "\rPerforming diagnostic %s (%s) ... %s", Diag->Name, isaddresscorrect(Diag->SubName) ? Diag->SubName : "-" , States[Diag->State]);

            if(Diag->State == oC_Diag_State_Finished)
            {
                printf(" - ");

                if(oC_ErrorOccur(Diag->Result))
                {
                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
                    printf("FAILED - %R - %s\n", Diag->Result, isaddresscorrect(Diag->ResultDescription) ? Diag->ResultDescription : "description not available" );
                }
                else
                {
                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                    printf("OK\n");
                }
            }
        }
        else
        {
            printf("Unexpected behavior - cannot print diagnostic - state %d is not supported!\n", Diag->State);
        }
    }
    else
    {
        printf("Unexpected behavior - cannot print diagnostic - Diag address %p is not in RAM section!\n", Diag);
    }
}

//==========================================================================================================================================
/**
 * @brief performs diagnostics for the Netif
 */
//==========================================================================================================================================
void PerformDiagnostics( Context_t * Context , oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode     = oC_ErrorCode_ImplementError;
    uint32_t       numberOfDiags = 0;

    oC_TGUI_SetForegroundColor( oC_TGUI_Color_White );
    printf("Reading number of supported diagnostics of `%s` interface...", oC_Netif_GetFriendlyName(Netif));

    if(ErrorCode( oC_Netif_PerformDiagnostics(Netif, NULL, &numberOfDiags) ))
    {
        oC_TGUI_SetForegroundColor( oC_TGUI_Color_Green );
        printf("OK\n");

        if(numberOfDiags > 0)
        {
            oC_TGUI_SetForegroundColor( oC_TGUI_Color_White );
            printf("Allocating memory for %d diagnostics...", numberOfDiags);

            oC_Diag_t * diags = malloc( numberOfDiags * sizeof(oC_Diag_t) , AllocationFlags_ZeroFill );

            if(diags != NULL)
            {
                oC_TGUI_SetForegroundColor( oC_TGUI_Color_Green );
                printf("OK\n");

                oC_TGUI_SetForegroundColor( oC_TGUI_Color_White );
                printf("Preparing diagnostics data...");

                oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(diags,numberOfDiags,diag)
                {
                    diag->PrintFunction = PrintDiagnosticState;
                    diag->State         = oC_Diag_State_NotStarted;
                }

                oC_TGUI_SetForegroundColor( oC_TGUI_Color_Green );
                printf("OK\n");

                oC_TGUI_SetForegroundColor( oC_TGUI_Color_White );
                printf("Performing diagnostics...\n");

                if( ErrorCode( oC_Netif_PerformDiagnostics(Netif,diags,&numberOfDiags) ) )
                {
                    printf("FINISHED!\n");
                }
                else
                {
                    oC_TGUI_SetForegroundColor( oC_TGUI_Color_Red );
                    printf("FAILED - %R\n", errorCode);
                }

                free(diags,AllocationFlags_CanWait1Second);
            }
            else
            {
                oC_TGUI_SetForegroundColor( oC_TGUI_Color_Red );
                printf("FAILED\n");
            }
        }
        else
        {
            oC_TGUI_SetForegroundColor(oC_TGUI_Color_Yellow);
            printf("Number of supported diagnostics for %s is 0 - no diagnostic to perform!\n", oC_Netif_GetFriendlyName(Netif));
        }
    }
    else
    {
        oC_TGUI_SetForegroundColor( oC_TGUI_Color_Red );
        printf("FAILED - %R\n", errorCode);
    }
}

//==========================================================================================================================================
/**
 * @brief prints all received frames
 */
//==========================================================================================================================================
void Listen( Context_t * Context )
{
    oC_TGUI_Key_t key = 0;

    if(oC_UserMan_GetRootUser() == oC_Process_GetUser(getcurprocess()))
    {
        oC_ErrorCode_t   errorCode   = oC_ErrorCode_ImplementError;

        oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);
        printf("Enabling listen mode on interface `%s`...", oC_Netif_GetFriendlyName(Context->Netif));

        if( ErrorCode( oC_Netif_SetListenMode(Context->Netif,true)) )
        {
            oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
            printf("OK\n");

            oC_Net_Packet_t* packet   = malloc( sizeof(oC_Net_Packet_t) , AllocationFlags_ZeroFill );

            if(packet != NULL)
            {
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);
                printf("Listening on interface `%s`...\n", oC_Netif_GetFriendlyName(Context->Netif));

                while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC)
                {
                    char             ip[30]      = {0};

                    memset(packet, 0, sizeof(oC_Net_Packet_t));

                    if(
                        ErrorCode( oC_Netif_ReceivePacket( Context->Netif, packet, sizeof(oC_Net_Packet_t), s(1) ) )
                     || errorCode == oC_ErrorCode_OutputBufferTooSmall
                        )
                    {
                        printf("\n\n------------------ PACKET RECEIVED ------------------\n");
                        printf(" Version: %d \n", packet->IPv4.Header.Version);
                        if(packet->IPv4.Header.Version == oC_Net_PacketType_IPv4 || packet->IPv4.Header.Version == 5)
                        {
                            uint16_t ipExpectedChecksum = oC_Net_Packet_CalculateChecksum(packet);

                            printf(" IHL: 0x%x \n", packet->IPv4.Header.IHL);
                            printf(" QoS: 0x%x \n", packet->IPv4.Header.QoS);
                            printf(" Length: 0x%x \n", packet->IPv4.Header.Length);
                            printf(" ID: 0x%x \n", packet->IPv4.Header.ID);
                            printf(" Don't fragment: %s \n", packet->IPv4.Header.DF ? "true" : "false");
                            printf(" More fragments: %s \n", packet->IPv4.Header.MF ? "true" : "false");
                            printf(" Fragment offset: 0x%X \n", packet->IPv4.Header.FragmentOffset);
                            printf(" TTL: 0x%X \n", packet->IPv4.Header.TTL);
                            printf(" Protocol: 0x%X - '%s'\n", packet->IPv4.Header.Protocol, oC_Net_GetProtocolName((oC_Net_Protocol_t)packet->IPv4.Header.Protocol));
                            printf(" Checksum: %s 0x%X (expected: 0x%X) \n"oC_VT100_FG_WHITE,
                                   ipExpectedChecksum != packet->IPv4.Header.Checksum ? oC_VT100_FG_RED : oC_VT100_FG_GREEN,
                                                   packet->IPv4.Header.Checksum, ipExpectedChecksum);

                            oC_Net_Ipv4AddressToString(packet->IPv4.Header.SourceIp,ip,sizeof(ip));
                            printf(" Source IP: %s \n", ip);
                            oC_Net_Ipv4AddressToString(packet->IPv4.Header.DestinationIp,ip,sizeof(ip));
                            printf(" Destination IP: %s \n", ip);

                            if(packet->IPv4.Header.Protocol == oC_Net_Protocol_UDP)
                            {
                                printf("    =========== UDP HEADER ===========\n");
                                oC_Udp_Header_t * header = (void*)packet->IPv4.Data;

                                uint16_t calculatedUdpChecksum = 0;

                                header->Checksum        = oC_Net_ConvertUint16FromNetworkEndianess(header->Checksum       );
                                header->DestinationPort = oC_Net_ConvertUint16FromNetworkEndianess(header->DestinationPort);
                                header->Length          = oC_Net_ConvertUint16FromNetworkEndianess(header->Length         );
                                header->SourcePort      = oC_Net_ConvertUint16FromNetworkEndianess(header->SourcePort     );

                                oC_Udp_CalculateChecksum((void*)packet,NULL,&calculatedUdpChecksum);

                                printf("        Source port: %d (0x%X)\n"                                   , header->SourcePort      , header->SourcePort      );
                                printf("        Destination port: %d (0x%X)\n"                              , header->DestinationPort , header->DestinationPort );
                                printf("        Length: %d (0x%X)\n"                                        , header->Length          , header->Length          );
                                printf("        Checksum: %s 0x%X (calculated: 0x%X)\n" oC_VT100_FG_WHITE   , calculatedUdpChecksum != header->Checksum ? oC_VT100_FG_RED : oC_VT100_FG_GREEN, header->Checksum ,  calculatedUdpChecksum);
                            }
                            else if(packet->IPv4.Header.Protocol == oC_Net_Protocol_ICMP)
                            {
                                printf("    =========== ICMP HEADER ===========\n");
                                oC_Icmp_Header_t * header = (void*)packet->IPv4.Data;

                                uint16_t calculatedIcmpChecksum = 0;

                                oC_Icmp_CalculateChecksum((void*)packet,&calculatedIcmpChecksum);

                                printf("        Type: %d (0x%X)\n", header->Type        , header->Type);
                                printf("        Code: %d (0x%X)\n", header->Code        , header->Code);
                                printf("        Checksum: %s 0x%X (calculated: 0x%X)\n" oC_VT100_FG_WHITE   , calculatedIcmpChecksum != header->Checksum ? oC_VT100_FG_RED : oC_VT100_FG_GREEN, header->Checksum ,  calculatedIcmpChecksum);
                            }
                            else if(packet->IPv4.Header.Protocol == oC_Net_Protocol_TCP)
                            {
                                printf("    =========== TCP HEADER ===========\n");
                                oC_Tcp_Header_t * header = (void*)packet->IPv4.Data;

                                uint16_t calculatedIcmpChecksum = 0;


                                if(oC_Tcp_Packet_ConvertFromNetworkEndianess((void*)packet) == false)
                                {
                                    printf(oC_VT100_FG_RED "ERROR - Cannot convert TCP header from network endianess!\n");
                                }

                                calculatedIcmpChecksum = oC_Tcp_Packet_CalculateChecksum((void*)packet);

                                printf("        SourcePort          : %d (0x%X) \n" , header->SourcePort           , header->SourcePort           );
                                printf("        DestinationPort     : %d (0x%X) \n" , header->DestinationPort      , header->DestinationPort      );
                                printf("        SequenceNumber      : %d (0x%X) \n" , header->SequenceNumber       , header->SequenceNumber       );
                                printf("        AcknowledgmentNumber: %d (0x%X) \n" , header->AcknowledgmentNumber , header->AcknowledgmentNumber );
                                printf("        DataOffset          : %d (0x%X) \n" , header->DataOffset           , header->DataOffset           );
                                printf("        Reserved            : %d (0x%X) \n" , header->Reserved             , header->Reserved             );
                                printf("        NS                  : %d (0x%X) \n" , header->NS                   , header->NS                   );
                                printf("        CWR                 : %d (0x%X) \n" , header->CWR                  , header->CWR                  );
                                printf("        ECE                 : %d (0x%X) \n" , header->ECE                  , header->ECE                  );
                                printf("        URG                 : %d (0x%X) \n" , header->URG                  , header->URG                  );
                                printf("        ACK                 : %d (0x%X) \n" , header->ACK                  , header->ACK                  );
                                printf("        PSH                 : %d (0x%X) \n" , header->PSH                  , header->PSH                  );
                                printf("        RST                 : %d (0x%X) \n" , header->RST                  , header->RST                  );
                                printf("        SYN                 : %d (0x%X) \n" , header->SYN                  , header->SYN                  );
                                printf("        FIN                 : %d (0x%X) \n" , header->FIN                  , header->FIN                  );
                                printf("        WindowSize          : %d (0x%X) \n" , header->WindowSize           , header->WindowSize           );
                                printf("        Checksum            : %d (0x%X) \n" , header->Checksum             , header->Checksum             );
                                printf("        UrgentPointer       : %d (0x%X) \n" , header->UrgentPointer        , header->UrgentPointer        );

                                printf("        Checksum: %s 0x%X (calculated: 0x%X)\n" oC_VT100_FG_WHITE   , calculatedIcmpChecksum != header->Checksum ? oC_VT100_FG_RED : oC_VT100_FG_GREEN, header->Checksum ,  calculatedIcmpChecksum);
                            }

                            printf(" Data (Hex format:)\n        ");

                            static const oC_TGUI_Color_t dataForegroundColors[4] = {
                                            oC_TGUI_Color_Blue ,
                                            oC_TGUI_Color_LightGreen ,
                                            oC_TGUI_Color_Cyan ,
                                            oC_TGUI_Color_Magenda
                            };

                            for(uint16_t offset = 0, row = 0, column = 0 ; offset < oC_Net_GetPacketSize(packet,false) && offset < sizeof(oC_Net_Packet_t); offset++)
                            {
                                oC_TGUI_SetForegroundColor(dataForegroundColors[column % 4]);
                                printf("%02X ", (char)packet->IPv4.Data[offset]);
                                oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);

                                column++;

                                if(column >= 32)
                                {
                                    printf(" [ row: %d ]\n        ", row);
                                    row++;
                                    column = 0;
                                }
                            }

                            printf("\n Data (String format:)\n        ");
                            for(uint16_t offset = 0, column = 0, row = 0 ; offset < oC_Net_GetPacketSize(packet,false) && offset < sizeof(oC_Net_Packet_t); offset++)
                            {
                                oC_TGUI_SetForegroundColor(dataForegroundColors[column % 4]);
                                char sign = (char)packet->IPv4.Data[offset];
                                if(sign >= 32 && sign <= 126)
                                {
                                    if(sign == '\n')
                                    {
                                        printf("\\n");
                                    }
                                    else if(sign == '\r')
                                    {
                                        printf("\\r");
                                    }
                                    else
                                    {
                                        printf("%c", (char)sign);
                                    }
                                }
                                else
                                {
                                    printf("?", (char)sign);
                                }

                                oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);

                                column++;

                                if(column >= 32)
                                {
                                    printf(" [ row: %d ]\n        ", row);
                                    row++;
                                    column = 0;
                                }
                            }
                        }
                        else if(packet->IPv6.Header.Version == oC_Net_PacketType_IPv6)
                        {
                            printf(" Traffic Class: 0x%x \n", packet->IPv6.Header.TrafficClass);
                            printf(" Flow label: 0x%x \n", packet->IPv6.Header.FlowLabel);
                            printf(" Payload Length: 0x%x \n", packet->IPv6.Header.PayloadLength);
                            printf(" Next Header: 0x%x \n", packet->IPv6.Header.NextHeader);
                            printf(" Hop limit (TTL): 0x%X \n", packet->IPv6.Header.HopLimit);


                            for(uint16_t offset = 0; offset < packet->IPv6.Header.PayloadLength && offset < sizeof(oC_Net_Packet_t); offset++)
                            {
                                printf("%c", (char)packet->IPv4.Data[offset]);
                            }
                        }
                        else
                        {
                            printf("packet type (%d) is currently not supported\n", packet->IPv4.Header.Version);
                        }
                    }
                }
                oC_SaveIfFalse("ifconfig::Listen - cannot release packet memory - ", kfree(packet,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
            }
            else
            {
                printf("Cannot allocate memory for packet\n");
            }

            if(ErrorCode(oC_Netif_SetListenMode(Context->Netif,false)))
            {
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                printf("\nListen mode disabled\n");
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_Default);
            }
            else
            {
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
                printf("Error - cannot disable listen mode! (%s)\n", oC_GetErrorString(errorCode));
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_Default);
            }
        }
        else
        {
            oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
            printf("Error!... %R\n", errorCode);
            oC_TGUI_SetForegroundColor(oC_TGUI_Color_Default);
        }
    }
    else
    {
        oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
        printf("You are not allowed to perform this operation!\n");
        oC_TGUI_SetForegroundColor(oC_TGUI_Color_Default);
    }
}

//==========================================================================================================================================
/**
 * @brief prints info about the netif
 */
//==========================================================================================================================================
void PrintNetifInfo( Context_t * Context , oC_Netif_t Netif )
{
    oC_Net_Info_t info;
    char          hwAddressString[100];
    char          hwBroadcastAddressString[100];
    char          hwRouterAddressString[100];
    char          ipAddress[30];
    char          netIpAddress[30];
    char          netMaskAddress[30];
    char          dhcpAddress[30];
    char          dnsAddress[30];
    char          broadcastAddress[30];
    char          ntpAddress[30];

    memset(&info                     , 0 , sizeof( info                      ));
    memset(&hwAddressString          , 0 , sizeof( hwAddressString           ));
    memset(&hwBroadcastAddressString , 0 , sizeof( hwBroadcastAddressString  ));
    memset(&hwRouterAddressString    , 0 , sizeof( hwRouterAddressString     ));
    memset(&ipAddress                , 0 , sizeof( ipAddress                 ));
    memset(&netIpAddress             , 0 , sizeof( netIpAddress              ));
    memset(&netMaskAddress           , 0 , sizeof( netMaskAddress            ));
    memset(&dhcpAddress              , 0 , sizeof( dhcpAddress               ));
    memset(&dnsAddress               , 0 , sizeof( dnsAddress                ));
    memset(&broadcastAddress         , 0 , sizeof( broadcastAddress          ));
    memset(&ntpAddress               , 0 , sizeof( ntpAddress                ));

    if(oC_SaveIfErrorOccur("ifconfig: cannot read network info - ", oC_Netif_ReadNetInfo(Netif,&info)) == false)
    {
        printf("Cannot read network info for %s\n", oC_Netif_GetFriendlyName(Netif));
    }

    printf("%s (%s) - %s - %s\n",
           oC_Netif_GetFriendlyName(Netif),
           info.InterfaceName,
           info.LinkStatus == oC_Net_LinkStatus_Up ? "UP"         : "DOWN"          ,
           oC_Netif_IsConfigured(Netif)            ? "CONFIGURED" : "NOT CONFIGURED"
                           );

    if(Context->Verbose)
    {
        oC_SaveIfErrorOccur("ifconfig: cannot read HW address - "           , oC_Net_HardwareAddressToString(&info.HardwareAddress                          , info.HardwareType, hwAddressString            , sizeof(hwAddressString          )));
        oC_SaveIfErrorOccur("ifconfig: cannot read HW broadcast address - " , oC_Net_HardwareAddressToString(&info.HardwareBroadcastAddress                 , info.HardwareType, hwBroadcastAddressString   , sizeof(hwBroadcastAddressString )));
        oC_SaveIfErrorOccur("ifconfig: cannot read HW router address - "    , oC_Net_HardwareAddressToString(&info.NetworkLayer.IPv4.HardwareRouterAddress  , info.HardwareType, hwRouterAddressString      , sizeof(hwRouterAddressString    )));
        oC_SaveIfErrorOccur("ifconfig: cannot read IP address - "           , oC_Net_Ipv4AddressToString(info.NetworkLayer.IPv4.IP            , ipAddress          , sizeof( ipAddress             )));
        oC_SaveIfErrorOccur("ifconfig: cannot read NetIP address - "        , oC_Net_Ipv4AddressToString(info.NetworkLayer.IPv4.NetIP         , netIpAddress       , sizeof( netIpAddress          )));
        oC_SaveIfErrorOccur("ifconfig: cannot read Netmask address - "      , oC_Net_Ipv4AddressToString(info.NetworkLayer.IPv4.Netmask       , netMaskAddress     , sizeof( netMaskAddress        )));
        oC_SaveIfErrorOccur("ifconfig: cannot read DHCP address - "         , oC_Net_Ipv4AddressToString(info.NetworkLayer.IPv4.DhcpIP        , dhcpAddress        , sizeof( dhcpAddress           )));
        oC_SaveIfErrorOccur("ifconfig: cannot read DNS address - "          , oC_Net_Ipv4AddressToString(info.NetworkLayer.IPv4.DnsIP         , dnsAddress         , sizeof( dnsAddress            )));
        oC_SaveIfErrorOccur("ifconfig: cannot read broadcast address - "    , oC_Net_Ipv4AddressToString(info.NetworkLayer.IPv4.BroadcastIP   , broadcastAddress   , sizeof( broadcastAddress      )));
        oC_SaveIfErrorOccur("ifconfig: cannot read NTP IP address - "       , oC_Net_Ipv4AddressToString(info.NetworkLayer.IPv4.NtpIP         , ntpAddress         , sizeof( ntpAddress            )));

        printf(  "    Driver              : %s   \n", info.DriverName                               );
        printf(  "    IF-IDX              : 0x%X \n", info.InterfaceIndex                           );
        printf(  "    HW-TYPE             : %s   \n", oC_Net_GetHardwareTypeName(info.HardwareType) );
        printf(  "    HW-ADDR             : %s   \n", hwAddressString                               );
        printf(  "    HW-BROADCAST-ADDR   : %s   \n", hwBroadcastAddressString                      );
        printf(  "    HW-ADDR-LEN         : %d   \n", info.HardwareAddressLength                    );
        printf(  "    Sent-Bytes          : %d   \n", info.TransmittedBytes                         );
        printf(  "    Recv-Bytes          : %d   \n", info.ReceivedBytes                            );
        printf(  "                               \n"                                                );
        printf(  "        ---- IPv4 ----         \n"                                                );
        printf(  "    IP          : %s           \n", ipAddress                                     );
        printf(  "    NET-IP      : %s           \n", netIpAddress                                  );
        printf(  "    NET-MASK    : %s           \n", netMaskAddress                                );
        printf(  "    DHCP-IP     : %s           \n", dhcpAddress                                   );
        printf(  "    DHCP-NAME   : %s           \n", info.NetworkLayer.IPv4.DhcpServerName         );
        printf(  "    DNS-IP      : %s           \n", dnsAddress                                    );
        printf(  "    MTU         : %d           \n", info.NetworkLayer.IPv4.MTU                    );
        printf(  "    BROADCAST-IP: %s           \n", broadcastAddress                              );
        printf(  "    Default-TTL : 0x%X         \n", info.NetworkLayer.IPv4.DefaultTTL             );
        printf(  "    NTP-IP      : %s           \n", ntpAddress                                    );
        printf(  "    Lease-Time  : %d s         \n", (uint32_t)info.NetworkLayer.IPv4.LeaseTime    );
        printf(  "    HW-ROUT-ADDR: %s           \n", hwRouterAddressString                         );
        printf(  "                               \n"                                                );

    }
}

//==========================================================================================================================================
/**
 * @brief prints list of network interfaces
 */
//==========================================================================================================================================
void ListNetworkInterfaces( Context_t * Context )
{
    oC_List(oC_Netif_t) netifs = oC_NetifMan_GetList();

    foreach(netifs,netif)
    {
        PrintNetifInfo(Context,netif);
    }
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    Context_t context;

    context.ProgramName = Argv[0];
    context.Verbose     = oC_ArgumentOccur(Argc,Argv,"-v") || oC_ArgumentOccur(Argc,Argv,"--verbose");
    context.Action      = Action_ListDevices;

    if(Argc > 1)
    {
        context.Netif  = oC_NetifMan_GetNetif(Argv[1]);
        context.Action = context.Netif != NULL ? Action_PrintOneNetif : context.Action;
    }
    if(Argc > 2)
    {
        context.LayerSet = oC_Net_ReadLayerByName(&context.Layer,Argv[2]) == oC_ErrorCode_None;
    }

    if(oC_ArgumentOccur(Argc,Argv,"-h") || oC_ArgumentOccur(Argc,Argv,"--help"))
    {
        context.Action = Action_PrintUsage;
    }

    if(oC_ArgumentOccur(Argc,Argv,"-n") || oC_ArgumentOccur(Argc,Argv,"--listen"))
    {
        if(context.Netif == NULL)
        {
            context.Action = Action_PrintUsage;
        }
        else
        {
            context.Action = Action_Listen;
        }
    }

    if(oC_ArgumentOccur(Argc,Argv,"-l") || oC_ArgumentOccur(Argc,Argv,"--loopback-test"))
    {
        if(context.Netif == NULL)
        {
            context.Action = Action_PrintUsage;
        }
        else
        {
            context.Action = Action_PerformLoopbackTest;
        }
    }

    if(oC_ArgumentOccur(Argc,Argv,"-d") || oC_ArgumentOccur(Argc,Argv,"--diagnostics"))
    {
        if(context.Netif == NULL)
        {
            context.Action = Action_PrintUsage;
        }
        else
        {
            context.Action = Action_PerformDiagnostics;
        }
    }

    if(context.Action == Action_PerformLoopbackTest)
    {
        if(context.LayerSet)
        {
            PerformLoopbackTest(context.Netif, context.Layer);
        }
        else
        {
            PerformLoopbackTest(context.Netif, oC_Net_Layer_Netif   );
            PerformLoopbackTest(context.Netif, oC_Net_Layer_MAC     );
            PerformLoopbackTest(context.Netif, oC_Net_Layer_PHY     );
        }
    }
    if(context.Action == Action_PerformDiagnostics)
    {
        PerformDiagnostics(&context,context.Netif);
    }
    if(context.Action == Action_PrintUsage)
    {
        PrintUsage(&context);
    }
    if(context.Action == Action_PrintOneNetif)
    {
        PrintNetifInfo(&context,context.Netif);
    }
    if(context.Action == Action_ListDevices)
    {
        ListNetworkInterfaces(&context);
    }
    if(context.Action == Action_Listen)
    {
        Listen(&context);
    }

    return 0;
}
