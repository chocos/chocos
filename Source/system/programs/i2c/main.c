/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the i2c program
 *
 * @author     Patryk Kubiak - (Created on: 2017-02-17 - 19:41:20) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_i2c.h>
#include <oc_gpio.h>
#include <oc_tgui.h>
#include <string.h>
#include <oc_kprint.h>
#include <ctype.h>

#ifndef oC_I2C_LLD_AVAILABLE
//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    printf("I2C is not available or not implemented for your target!\n");
    return 0;
}

#else

static bool PrintCommandHelp( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv );
static bool Write           ( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv );
static bool Read            ( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv );
static bool ScanBus         ( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv );

//==========================================================================================================================================
/**
 * @brief list of commands available in the command-mode interface
 */
//==========================================================================================================================================
static const struct
{
    char *      Command;
    char *      Usage;
    char *      Brief;
    char *      Description;
    bool (*Handler)( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv );
} CommandsList[] = {
                {
                   .Command             = "exit" ,
                   .Usage               = "exit" ,
                   .Brief               = "closes the terminal",
                   .Description         = "The command designed for closing the command-mode interface",
                   .Handler             = NULL ,
                } ,
                {
                   .Command             = "help" ,
                   .Usage               = "help [command]" ,
                   .Brief               = "prints this help or detailed informations about the command",
                   .Description         = "Prints list of available commands or help in details about the given command" ,
                   .Handler             = PrintCommandHelp ,
                } ,
                {
                   .Command             = "write" ,
                   .Usage               = "write -a [address] -r [offset] -f [file_name] -n [number] -s [string] -t [timeout(s)]" ,
                   .Brief               = "Sends a message to the given address",
                   .Description         = "The command is for sending a message by using configured I2C channel. It is possible to send a message from a file, string or just a number.\n"
                                          "    -a [address]         - Destination address for the command <optional>\n"
                                          "    -f [file_name]       - Source file to send by using the I2C interface\n"
                                          "    -n [number]          - Number to send in any of supported formats (decimal/hex/octal/binary)\n"
                                          "    -s [string]          - String to send\n"
                                          "    -t [timeout(s)]      - Maximum time to wait for a data in seconds<optional>\n"
                                          "    -r [offset]          - Writes register in a remote device<optional>\n"
                                          "Note, that only one message can be send at once - so you have to choose which format of data you want to send (you cannot try to send file and string at once for example)\n"
                                           ,
                   .Handler             = Write ,
                } ,
                {
                   .Command             = "read" ,
                   .Usage               = "read no_bytes -r [offset] -a [address] -f [file_name] -t [timeout(s)]" ,
                   .Brief               = "Receive a message from the given address",
                   .Description         = "The command is for receiving a message by using configured I2C channel.\n"
                                          "    -a [address]         - Remote address to read data from <optional>\n"
                                          "    no_bytes             - Number of bytes to request/read\n"
                                          "    -f [file_name]       - Destination file name to write received data <optional>\n"
                                          "    -t [timeout(s)]      - Maximum time to wait for a data in seconds<optional>\n"
                                          "    -r [offset]          - Reads register from a remote device<optional>\n"
                                          ,
                   .Handler             = Read ,
                } ,
                {
                   .Command             = "scan" ,
                   .Usage               = "scan -c [command] -v" ,
                   .Brief               = "Scans a bus to find active devices",
                   .Description         = "The command is for scanning a I2C bus to find active devices\n"
                                          "    -c [command]         - Dummy command to use for scanning (1 byte only!)<optional>\n"
                                          "    -v                   - Be verbose (print additional messages)<optional>\n"
                                          ,
                   .Handler             = ScanBus ,
                } ,
};

//==========================================================================================================================================
/**
 * @brief prints user message
 */
//==========================================================================================================================================
static void PrintUsage( int Argc , char ** Argv )
{
    char * name = Argc > 0 ? Argv[0] : "i2c";

    printf("%s --channels-list --SCL SCL_PIN --SDA SDA_PIN --channel I2C_CHANNEL\n", name);
    printf("   where:\n");
    printf("          --channels-list           - will print list of channels\n");
    printf("          --mode MODE               - I2C mode of the device (master/slave)\n");
    printf("          --local-address ADDRESS   - I2C address of the local device\n");
    printf("          --remote-address ADDRESS  - default I2C address of the remote device\n");
    printf("          --clock-frequency FREQ    - I2C clock frequency (required)\n");
    printf("          --SCL SCL_PIN             - program will use the given SCL_PIN\n");
    printf("          --SDA SDA_PIN             - program will use the given SDA_PIN\n");
    printf("          --SMBA SMBA_PIN           - program will use the given SMBA_PIN (optional)\n");
    printf("          --channel I2C_CHANNEL     - program will use the given I2C channel (optional)\n");
    printf("          --10bits                  - Using 10 bits address mode length (optional)\n");
    printf("          --general-calls           - If set, general calls will be enabled (optional)\n");
    printf("          --speed-mode MODE         - Speed mode to use (normal/fast/fast-plus/high/ultra) (optional)\n");
    printf("          --dummy-message MSG       - Dummy message to send for tests (optional)\n");
    printf("          --command-mode            - Allows for commands execution before unconfiguration (optional)\n");
}

//==========================================================================================================================================
/**
 * @brief prints list of I2C channels
 */
//==========================================================================================================================================
static void PrintI2cChannels(void)
{
    oC_TGUI_SetForegroundColor(oC_TGUI_Color_DarkGray);

    printf("List of I2C channels available in your target: \n");

    oC_I2C_LLD_ForEachChannel(channel,ChannelIndex)
    {
        oC_TGUI_SetForegroundColor(oC_TGUI_Color_LightBlue);
        printf(" %s\n", oC_Channel_GetName(channel));

        oC_ModulePin_ForeachDefined(pin)
        {
            if(pin->Channel == channel)
            {
                bool used = false;
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);

                printf("    %5s | %10s | ", oC_GPIO_GetPinName(pin->Pin), oC_PinFunction_GetName(pin->PinFunction));

                oC_GPIO_CheckIsPinUsed(pin->Pin,&used);

                if(used)
                {
                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
                    printf("USED\n");
                }
                else
                {
                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                    printf("Free\n");
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief reads pin by name
 */
//==========================================================================================================================================
static bool ReadPinFromName( char * PinName , char * FunctionName , oC_Pin_t * outPin , oC_I2C_Channel_t Channel )
{
    bool found = false;

    if(PinName != NULL)
    {
        if(oC_GPIO_FindPinByName(PinName,outPin) == oC_ErrorCode_None)
        {
            bool used = false;

            oC_GPIO_CheckIsPinUsed(*outPin,&used);

            if(used == false)
            {
                oC_ModulePin_ForeachDefined(pin)
                {
                    if(oC_I2C_LLD_IsChannelCorrect(pin->Channel) && pin->Pin == *outPin && string_contains(oC_PinFunction_GetName(pin->PinFunction),FunctionName,false))
                    {
                        if(Channel == 0 || (oC_Channel_t)Channel == pin->Channel)
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if(!found)
                {
                    if(Channel != 0)
                    {
                        printf("Pin '%s' is not '%s' pin of channel '%s'!\n", PinName, FunctionName, oC_Channel_GetName(Channel));
                    }
                    else
                    {
                        printf("Pin '%s' is not '%s' pin!\n", PinName, FunctionName);
                    }
                }
            }
            else
            {
                printf("Pin '%s' is already in use\n", PinName);
            }
        }
        else
        {
            printf("Pin '%s' is not correct!\n", PinName);
        }
    }
    else
    {
        found = true;
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief reads channel according to its name
 */
//==========================================================================================================================================
static bool ReadChannelByName( char * ChannelName , oC_I2C_Channel_t * outChannel )
{
    bool found = false;

    if(ChannelName != NULL)
    {
        oC_I2C_LLD_ForEachChannel(channel,channelIndex)
        {
            if(strcmp(ChannelName,oC_Channel_GetName(channel)) == 0)
            {
                *outChannel = channel;
                found       = true;
                break;
            }
        }

        if(!found)
        {
            printf("Channel %s is not correct\n", ChannelName);
        }
    }
    else
    {
        found = true;
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief reads speed mode according to its name
 */
//==========================================================================================================================================
static bool ReadSpeedModeByName( char * SpeedModeName , oC_I2C_SpeedMode_t * outSpeedMode )
{
    bool found = false;

    if(SpeedModeName != NULL)
    {
        if(strcmp(SpeedModeName,"normal") == 0)
        {
            *outSpeedMode = oC_I2C_SpeedMode_Normal;
            found         = true;
        }
        else if(strcmp(SpeedModeName,"fast") == 0)
        {
            *outSpeedMode = oC_I2C_SpeedMode_Fast;
            found         = true;
        }
        else if(strcmp(SpeedModeName,"fast-plus") == 0)
        {
            *outSpeedMode = oC_I2C_SpeedMode_FastPlus;
            found         = true;
        }
        else if(strcmp(SpeedModeName,"high") == 0)
        {
            *outSpeedMode = oC_I2C_SpeedMode_HighSpeed;
            found         = true;
        }
        else if(strcmp(SpeedModeName,"ultra") == 0)
        {
            *outSpeedMode = oC_I2C_SpeedMode_UltraFast;
            found         = true;
        }
        else
        {
            printf("Speed mode %s is not supported\n", SpeedModeName);
        }
    }
    else
    {
        found = true;
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief reads I2C address by using its name
 */
//==========================================================================================================================================
static bool ReadAddressByName( char * AddressName , oC_I2C_Address_t * outAddress , oC_I2C_AddressMode_t AddressMode , bool Obligatory )
{
    bool correct = false;

    if(AddressName != NULL)
    {
        uint32_t address = 0;

        if(sscanf(AddressName,"%i",&address) == oC_ErrorCode_None)
        {
            if(
                (AddressMode == oC_I2C_AddressMode_10Bits && address <= 0x3FF)
             || (AddressMode == oC_I2C_AddressMode_7Bits  && address <= 0x7F )
                )
            {
                *outAddress = (oC_I2C_Address_t)address;
                correct     = true;
            }
            else
            {
                printf("The given address '%s' is too big!\n", AddressName);
            }
        }
        else
        {
            printf("The given address '%s' is not correct!\n", AddressName);
        }
    }
    else
    {
        if(Obligatory)
        {
            printf("Address of the slave device is not given!\n");
        }
        correct = Obligatory == false;
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief reads mode from the string
 */
//==========================================================================================================================================
static bool ReadModeByName( char * ModeName , oC_I2C_Mode_t * outMode )
{
    bool correct = false;

    if(ModeName != NULL)
    {
        if(strcmp(ModeName,"master") == 0)
        {
            *outMode = oC_I2C_Mode_Master;
            correct  = true;
        }
        else if(strcmp(ModeName,"slave") == 0)
        {
            *outMode = oC_I2C_Mode_Slave;
            correct  = true;
        }
        else
        {
            printf("The given mode '%s' is not correct\n", ModeName);
        }
    }
    else
    {
        correct = true;
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief reads clock frequency
 */
//==========================================================================================================================================
static bool ReadClockFrequency( char * FrequencyName , oC_Frequency_t * outFrequency )
{
    bool correct = false;

    if(FrequencyName != NULL)
    {
        if(sscanf(FrequencyName, "%d", outFrequency) == oC_ErrorCode_None)
        {
            correct = true;
        }
        else
        {
            printf("The given frequency '%s' is not correct\n", FrequencyName);
        }
    }
    else
    {
        correct = true;
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief searches for a common I2C channel
 */
//==========================================================================================================================================
static bool FindCommonChannel( oC_Pin_t SdaPin , oC_Pin_t SclPin, oC_I2C_Channel_t * outChannel )
{
    bool found = false;

    oC_ModulePin_ForeachDefined(pinSda)
    {
        if(oC_I2C_LLD_IsChannelCorrect(pinSda->Channel) && pinSda->Pin == SdaPin && string_contains(oC_PinFunction_GetName(pinSda->PinFunction), "SDA", false))
        {
            oC_ModulePin_ForeachDefined(pinScl)
            {
                if(pinScl->Pin == SclPin && string_contains(oC_PinFunction_GetName(pinScl->PinFunction), "SCL", false))
                {
                    if(pinScl->Channel == pinSda->Channel)
                    {
                        found       = true;
                        *outChannel = pinSda->Channel;
                        break;
                    }
                }
            }
        }
    }

    if(!found)
    {
        printf("Cannot find common channel for the given I2C pins\n");
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief prints command incentive
 */
//==========================================================================================================================================
static void PrintCommandIncentive( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context )
{
    const char *     modeString = "slave,la:";
    oC_I2C_Address_t address    = Config->Mode == oC_I2C_Mode_Master ? Config->Master.DefaultSlaveAddress : Config->Slave.LocalAddress;
    oC_I2C_Channel_t channel    = Config->Advanced.Channel;
    const char *     channelName= "unknown";

    if(Config->Mode == oC_I2C_Mode_Master)
    {
        oC_I2C_ReadSlaveAddress(Context,&address);
        modeString = "master,ra:";
    }

    oC_I2C_ReadChannel(Context,&channel);

    if(oC_Channel_IsCorrect(I2C,channel))
    {
        channelName = oC_Channel_GetName(channel);
    }

    printf("\r" oC_VT100_FG_GREEN "%s:" oC_VT100_FG_YELLOW "%s" oC_VT100_FG_CYAN "0x%03x" oC_VT100_FG_WHITE "@" oC_VT100_FG_GREEN "%s" oC_VT100_FG_WHITE ":" oC_VT100_FG_GREEN "%s" oC_VT100_FG_YELLOW "$ " oC_VT100_FG_WHITE, channelName, modeString, address, oC_GPIO_GetPinName(Config->Pins.SCL), oC_GPIO_GetPinName(Config->Pins.SDA));
}

//==========================================================================================================================================
/**
 * @brief reads command into the buffer and returns true if the command is given
 */
//==========================================================================================================================================
static bool ReadCommand( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , char * outBuffer, oC_MemorySize_t Size )
{
    memset(outBuffer,0,Size);
    printf(oC_VT100_SAVE_CURSOR_AND_ATTRS);

    return oC_KPrint_ReadFromStdIn(oC_IoFlags_EchoWhenRead | oC_IoFlags_ReadOneLine | oC_IoFlags_WaitForAllElements | oC_IoFlags_SleepWhileWaiting | oC_IoFlags_NoTimeout , outBuffer , Size ) == oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief split command to list of arguments
 */
//==========================================================================================================================================
static bool SplitCommandToArguments( char * Command , int * outArgc , char *** outArgv )
{
    int argumentIndex       = 0;
    int argumentCount       = 0;
    oC_UInt_t cmdSize       = strlen(Command)+1;
    bool commandSpecified   = false;
    bool newArgumentStarted = false;
    bool quoteStarted       = false;

    //========================================================
    /* Counting number of arguments                         */
    //========================================================
    for(int signIndex = 0; signIndex < cmdSize ; signIndex++ )
    {
        /* If it is space, the new argument has started */
        if(isspace((int)Command[signIndex]) && !quoteStarted)
        {
            /* if some argument occurs before */
            if(newArgumentStarted)
            {
                argumentCount++;
            }
            newArgumentStarted = false;
        }
        else if(Command[signIndex] == '\"')
        {
            if(quoteStarted == false)
            {
                quoteStarted = true;
            }
            else
            {
                quoteStarted = false;
            }
        }
        else if(Command[signIndex] != 0)
        {
            /* found some argument */
            newArgumentStarted = true;
        }
    }

    if(newArgumentStarted == true)
    {
        argumentCount++;
    }

    //========================================================
    /* Allocation memory for argument array                 */
    //========================================================
    if(argumentCount > 0)
    {
        char ** argv = malloc(argumentCount * sizeof(char*),AllocationFlags_ZeroFill | AllocationFlags_CanWait1Second);

        newArgumentStarted = false;
        quoteStarted       = false;
        argumentIndex      = 0;

        for(int signIndex = 0; signIndex < cmdSize ; signIndex++ )
        {
            /* If it is space, the new argument has started */
            if(isspace((int)Command[signIndex]) && !quoteStarted)
            {
                /* if some argument occurs before */
                if(newArgumentStarted)
                {
                    argumentIndex++;
                }
                newArgumentStarted = false;

                /* Set this sign to 0 to mark an end of the string */
                Command[signIndex] = 0;
            }
            else if(Command[signIndex] == '\"')
            {
                if(quoteStarted == false)
                {
                    quoteStarted = true;
                }
                else
                {
                    quoteStarted = false;
                }
                Command[signIndex] = 0;
            }
            else
            {
                if(newArgumentStarted == false)
                {
                    /* It this is new argument, then save pointer to it */
                    argv[argumentIndex] = &Command[signIndex];
                }
                /* found some argument */
                newArgumentStarted = true;
            }
        }

        if(quoteStarted)
        {
            oC_PrintWarningMessage("Quote started but not finished!");
        }

        *outArgv         = argv;
        *outArgc         = argumentCount;
        commandSpecified = true;
    }

    return commandSpecified;
}

//==========================================================================================================================================
/**
 * @brief prints help for command interface
 */
//==========================================================================================================================================
static bool PrintCommandHelp( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv )
{
    bool commandCorrect = false;

    if(Argc > 1)
    {
        oC_ARRAY_FOREACH_IN_ARRAY(CommandsList,command)
        {
            if(strcmp(Argv[1],command->Command) == 0)
            {
                commandCorrect = true;

                printf("\nHelp for command '%s' (%s): \n\n", command->Command, command->Brief);
                printf("    %s\n\n", command->Usage);
                oC_KPrint_WriteToStdOut(oC_IoFlags_Default,command->Description,strlen(command->Description));
                printf("\n");
            }
        }
        if(commandCorrect == false)
        {
            printf("No help for command '%s'\n", Argv[1]);
        }
    }
    else
    {
        commandCorrect = true;

        printf("List of available commands: (please type 'help <command>' for details)\n");
        oC_ARRAY_FOREACH_IN_ARRAY(CommandsList,command)
        {
            printf("    %-15s - %s\n", command->Command, command->Brief);
        }
    }

    return commandCorrect;
}

//==========================================================================================================================================
/**
 * @brief sends a message via I2C
 */
//==========================================================================================================================================
static bool Write( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv )
{
    bool                commandCorrect   = true;
    char *              remoteAddress    = oC_GetArgumentAfter(Argc,Argv,"-a");
    char *              fileToSend       = oC_GetArgumentAfter(Argc,Argv,"-f");
    char *              numberToSend     = oC_GetArgumentAfter(Argc,Argv,"-n");
    char *              stringToSend     = oC_GetArgumentAfter(Argc,Argv,"-s");
    char *              timeoutString    = oC_GetArgumentAfter(Argc,Argv,"-t");
    char *              registerString   = oC_GetArgumentAfter(Argc,Argv,"-r");
    void *              bufferToSend     = NULL;
    oC_MemorySize_t     bufferToSendSize = 0;
    char *              fileBuffer       = NULL;
    uint32_t            number           = 0;
    uint8_t             u8number         = 0;
    uint32_t            address          = 0;
    oC_Time_t           timeout          = s(3);
    uint32_t            registerOffset   = 0;
    bool                writeRegister    = false;

    if(fileToSend != NULL)
    {
        FILE * fp = fopen(fileToSend,"r");

        if(fp != NULL)
        {
            uint32_t size = fsize(fp);
            if(size > 0)
            {
                fileBuffer = malloc(size,AllocationFlags_ZeroFill);

                if(fileBuffer != NULL)
                {
                    fread(fileBuffer,1,size,fp);
                    bufferToSend        = fileBuffer;
                    bufferToSendSize    = size;
                }
                else
                {
                    commandCorrect = false;
                    printf("Cannot allocate memory for a file '%s'\n", fileToSend);
                }
            }
            else
            {
                commandCorrect = false;
                printf("File '%s' is empty!\n", fileToSend);
            }
            fclose(fp);
        }
        else
        {
            commandCorrect = false;
            printf("File '%s' does not exist!\n", fileToSend);
        }
    }
    else if(numberToSend != NULL)
    {
        if(sscanf(numberToSend,"%lli",&number))
        {
            commandCorrect = false;
            printf("Cannot parse number '%s'\n", numberToSend);
        }
        else if(number > 0xFF)
        {
            commandCorrect = false;
            printf("Number cannot be bigger than 0xFF!\n");
        }
        else
        {
            u8number            = (uint8_t)number;
            bufferToSend        = &u8number;
            bufferToSendSize    = 1;
        }
    }
    else if(stringToSend != NULL)
    {
        bufferToSend        = stringToSend;
        bufferToSendSize    = strlen(bufferToSend) + 1;
    }
    else
    {
        commandCorrect = false;
    }

    if(registerString != NULL)
    {
        if(Config->Mode != oC_I2C_Mode_Master)
        {
            printf("Registers can be write only in master mode\n");
            commandCorrect = false;
        }
        else if(sscanf(registerString,"%i",&registerOffset))
        {
            commandCorrect = false;
        }
        else if(registerOffset > 0xFF)
        {
            printf("Sorry, we do not support registers with offset bigger than 0xFF yet\n");
            commandCorrect = false;
        }
        else
        {
            writeRegister = true;
        }
    }

    if(remoteAddress != NULL)
    {
        if(sscanf(remoteAddress,"%i",&address) != 0)
        {
            commandCorrect = false;
            printf("Cannot parse number '%s'\n", remoteAddress);
        }
    }

    if(timeoutString != NULL)
    {
        if(sscanf(timeoutString,"%f",&timeout) != 0)
        {
            commandCorrect = false;
            printf("Cannot parse timeout '%s'\n", timeoutString);
        }
    }

    if(commandCorrect)
    {
        if(writeRegister)
        {
            printf("Writing register 0x%02X to device 0x%03X...", registerOffset, address);

            printf("%R\n", oC_I2C_WriteRegister(Context,(oC_I2C_Address_t)address,(oC_I2C_Register_t)registerOffset,bufferToSend,bufferToSendSize,timeout));
        }
        else
        {
            printf("Sending a message...");

            printf("%R\n", oC_I2C_Transmit(Context,(oC_I2C_Address_t)address,bufferToSend,bufferToSendSize,timeout));
        }
    }

    if(fileBuffer != NULL)
    {
        free(fileBuffer,0);
    }

    return commandCorrect;
}

//==========================================================================================================================================
/**
 * @brief receives a message via I2C
 */
//==========================================================================================================================================
static bool Read( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv )
{
    bool                commandCorrect  = true;
    char *              noBytesString   = Argc >= 2 ? Argv[1] : NULL;
    char *              addressString   = oC_GetArgumentAfter(Argc,Argv,"-a");
    char *              fileNameString  = oC_GetArgumentAfter(Argc,Argv,"-f");
    char *              timeoutString   = oC_GetArgumentAfter(Argc,Argv,"-t");
    char *              registerString  = oC_GetArgumentAfter(Argc,Argv,"-r");
    oC_MemorySize_t     noBytes         = 0;
    uint32_t            address         = 0;
    FILE *              filePointer     = NULL;
    oC_Time_t           timeout         = s(3);
    char *              bufferForData   = NULL;
    uint32_t            registerOffset  = 0;
    bool                readRegister    = false;

    if(noBytesString == NULL)
    {
        commandCorrect = false;
    }
    else
    {
        if(sscanf(noBytesString,"%u",&noBytes))
        {
            commandCorrect = false;
            printf("Cannot parse number '%s'\n", noBytesString);
        }
        else if(noBytes == 0)
        {
            printf("noBytes cannot be 0!\n", noBytes);
            commandCorrect = false;
        }
    }

    if(addressString != NULL)
    {
        if(sscanf(addressString,"%i",&address))
        {
            commandCorrect = false;
        }
    }

    if(registerString != NULL)
    {
        if(Config->Mode != oC_I2C_Mode_Master)
        {
            printf("Registers can be read only in master mode\n");
            commandCorrect = false;
        }
        else if(sscanf(registerString,"%i",&registerOffset))
        {
            commandCorrect = false;
        }
        else if(registerOffset > 0xFF)
        {
            printf("Sorry, we do not support registers with offset bigger than 0xFF yet\n");
            commandCorrect = false;
        }
        else
        {
            readRegister = true;
        }
    }

    if(timeoutString != NULL)
    {
        if(sscanf(timeoutString,"%f",&timeout))
        {
            commandCorrect = false;
        }
    }

    if(commandCorrect)
    {
        bufferForData = malloc(noBytes,AllocationFlags_ZeroFill);

        if(bufferForData)
        {

            if(readRegister)
            {
                printf("Reading register 0x%02X...", registerOffset);

                printf("%R\n", oC_I2C_ReadRegister(Context,(oC_I2C_Address_t)address,(oC_I2C_Register_t)registerOffset,bufferForData,&noBytes,timeout));
            }
            else
            {
                printf("Waiting for a message...");

                printf("%R\n", oC_I2C_Receive(Context,(oC_I2C_Address_t)address,bufferForData,&noBytes,timeout));
            }

            if(fileNameString != NULL)
            {
                printf("Saving received data into the file '%s'...", fileNameString);

                filePointer = fopen(fileNameString,"w");
                if(filePointer)
                {
                    if(fwrite(bufferForData,1,noBytes,filePointer) == noBytes)
                    {
                        printf("Success\n");
                    }
                    else
                    {
                        printf("ERROR - not all bytes written\n");
                    }
                    fclose(filePointer);
                }
                else
                {
                    printf("ERROR - cannot create file\n");
                }
            }
            else
            {
                printf("Received data: \n");
                for(oC_MemorySize_t offset = 0; offset < noBytes; offset++)
                {
                    printf("0x%02X ", bufferForData[offset]);
                }
                printf("\n");
            }

            free(bufferForData,0);
        }
        else
        {
            printf("Cannot allocate memory for a data\n");
        }
    }

    return commandCorrect;
}

//==========================================================================================================================================
/**
 * @brief scans a bus to find devices
 */
//==========================================================================================================================================
static bool ScanBus( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , int Argc , char ** Argv )
{
    bool            commandCorrect = true;
    oC_ErrorCode_t  errorCode      = oC_ErrorCode_ImplementError;

    if(Config->Mode == oC_I2C_Mode_Master)
    {
        const char * commandToSendString = oC_GetArgumentAfter(Argc,Argv,"-c");
        bool         verbose             = oC_ArgumentOccur(Argc,Argv,"-v");

        uint32_t commandToSend = 0;

        if(commandToSendString != NULL)
        {
            if(sscanf(commandToSendString,"%i",&commandToSend))
            {
                printf("Incorrect command to send number (expected integer): %s\n", commandToSendString);
                commandCorrect = false;
            }
            else if(commandToSend > 0xFF)
            {
                printf("Value '%s' (0x%X) is too big! (max 0xFF)\n", commandToSendString, commandToSend);
                commandCorrect = false;
            }
        }

        if(commandCorrect)
        {
            printf("Scanning a bus with command 0x%03X...\n", commandToSend);

            for(oC_I2C_Address_t slaveAddress = 0; slaveAddress <= Config->AddressMode; slaveAddress+= 2)
            {
                uint8_t command  = (uint8_t)commandToSend;

                if(verbose)
                {
                    printf(oC_VT100_FG_WHITE "Checking address 0x%03X...", slaveAddress);
                }

                if(
                    ErrorCode( oC_I2C_SetSlaveAddress(Context,slaveAddress)             )
                 && ErrorCode( oC_I2C_Transmit(Context,0x0,&command,1,ms(100))          )
                    )
                {
                    errorCode = oC_ErrorCode_None;

                    if(verbose)
                    {
                        printf(oC_VT100_FG_GREEN "%R\n", errorCode);
                    }
                    else
                    {
                        printf(oC_VT100_FG_WHITE "Detect device 0x%03X\n", slaveAddress);
                    }
                }
                else if(verbose)
                {
                    printf(oC_VT100_FG_RED "%R\n", errorCode);
                }
                else if(errorCode != oC_ErrorCode_AcknowledgeNotReceived)
                {
                    printf(oC_VT100_FG_WHITE "There is some problem with device 0x%03X - unexpected type of error: " oC_VT100_FG_RED "%R\n", slaveAddress, errorCode);
                }
            }
            printf(oC_VT100_FG_WHITE);
            oC_I2C_SetSlaveAddress(Context,Config->Master.DefaultSlaveAddress);
        }
    }
    else
    {
        printf("Cannot scan bus as a slave!\n");
        commandCorrect = false;
    }

    return commandCorrect;
}

//==========================================================================================================================================
/**
 * @brief parses and executes command (returns true if exit has been typed)
 */
//==========================================================================================================================================
static bool ParseAndExecuteCommand( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context , char * Buffer, oC_MemorySize_t Size )
{
    bool    exitTyped   = false;
    int     argc        = 0;
    char ** argv        = NULL;
    bool    recognized  = false;

    if(SplitCommandToArguments(Buffer,&argc,&argv))
    {
        oC_ARRAY_FOREACH_IN_ARRAY(CommandsList,command)
        {
            if(strcmp(argv[0],command->Command) == 0)
            {
                recognized = true;

                if(command->Handler == NULL)
                {
                    exitTyped = true;
                }
                else if(command->Handler(Config,Context,argc,argv) == false)
                {
                    printf("Usage: %s\n", command->Usage);
                }
                break;
            }
        }
        if(recognized == false)
        {
            printf("Command '%s' is not recognized!\n", argv[0]);
        }

        free(argv,0);
    }

    return exitTyped;
}

//==========================================================================================================================================
/**
 * @brief runs command mode
 */
//==========================================================================================================================================
static void RunCommandMode( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context )
{
    char commandBuffer[100];

    while(1)
    {
        PrintCommandIncentive(Config,Context);
        if(
            ReadCommand           ( Config, Context, commandBuffer, sizeof(commandBuffer) )
         && ParseAndExecuteCommand( Config, Context, commandBuffer, sizeof(commandBuffer) )
            )
        {
            break;
        }
    }
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    char * sclPinName        = oC_GetArgumentAfter(Argc,Argv,"--SCL");
    char * sdaPinName        = oC_GetArgumentAfter(Argc,Argv,"--SDA");
    char * channelName       = oC_GetArgumentAfter(Argc,Argv,"--channel");
    char * speedModeName     = oC_GetArgumentAfter(Argc,Argv,"--speed-mode");
    char * localAddressName  = oC_GetArgumentAfter(Argc,Argv,"--local-address");
    char * remoteAddressName = oC_GetArgumentAfter(Argc,Argv,"--remote-address");
    char * modeName          = oC_GetArgumentAfter(Argc,Argv,"--mode");
    char * clockFrequencyName= oC_GetArgumentAfter(Argc,Argv,"--clock-frequency");
    char * dummyMessage      = oC_GetArgumentAfter(Argc,Argv,"--dummy-message");

    if(oC_ArgumentOccur(Argc,Argv,"--channels-list"))
    {
        PrintI2cChannels();
    }
    else if(modeName != NULL && sclPinName != NULL && sdaPinName != NULL)
    {
        oC_I2C_Channel_t channel        = 0;
        oC_I2C_Config_t config = {
                        .SpeedMode                              = oC_I2C_SpeedMode_Normal ,
                        .AddressMode                            = oC_ArgumentOccur(Argc,Argv,"--10bits") ? oC_I2C_AddressMode_10Bits : oC_I2C_AddressMode_7Bits ,
                        .Slave.LocalAddress                     = 0 ,
                        .Master.DefaultSlaveAddress             = 0 ,
                        .Slave.GeneralCallDetectionEnabled      = oC_ArgumentOccur(Argc,Argv,"--general-calls") ? true : false,
                        .Mode                                   = oC_I2C_Mode_Master ,
                        .Advanced.ClockFrequency                = 0 ,
                        .Advanced.Channel                       = 0 ,
        };

        if(
            ReadChannelByName   ( channelName,&channel )
         && ReadModeByName      ( modeName   , &config.Mode )
         && ReadPinFromName     ( sclPinName , "SCL" , &config.Pins.SCL , channel )
         && ReadPinFromName     ( sdaPinName , "SDA" , &config.Pins.SDA , channel )
         && FindCommonChannel   ( config.Pins.SDA, config.Pins.SCL, &config.Advanced.Channel )
         && ReadSpeedModeByName ( speedModeName, &config.SpeedMode )
         && ReadAddressByName   ( localAddressName  , &config.Slave.LocalAddress        , config.AddressMode , config.Mode == oC_I2C_Mode_Slave  )
         && ReadAddressByName   ( remoteAddressName , &config.Master.DefaultSlaveAddress, config.AddressMode , config.Mode == oC_I2C_Mode_Master )
         && ReadClockFrequency  ( clockFrequencyName , &config.Advanced.ClockFrequency                  )
            )
        {
            oC_ErrorCode_t   errorCode = oC_ErrorCode_ImplementError;
            oC_I2C_Context_t context   = NULL;

            /* Set channel to 0 or the given channel, because we cannot check here if the given channel is used */
            config.Advanced.Channel = channel;

            //================================================
            //          CONFIGURATION OF THE I2C
            //================================================
            printf("Configuration of the I2C...");

            if( ErrorCode( oC_I2C_Configure(&config, &context) ) )
            {
                oC_MemorySize_t size = 0;

                printf("Success\n");

                if(dummyMessage == NULL)
                {
                    dummyMessage = "Dummy message in ChocoOS";
                }
                size = strlen(dummyMessage) + 1;

                if(oC_ArgumentOccur(Argc,Argv,"--dont-send-dummy") == false)
                {
                    printf("Sending dummy message...");

                    if(ErrorCode(oC_I2C_Write(context,dummyMessage,&size,s(1))))
                    {
                        printf("Success\n");
                    }
                    else
                    {
                        printf("%R\n", errorCode);
                    }
                }

                if(oC_ArgumentOccur(Argc,Argv,"--command-mode"))
                {
                    RunCommandMode(&config,context);
                }

                if(oC_ArgumentOccur(Argc,Argv,"--dont-unconfigure") == false)
                {
                    //================================================
                    //          UNCONFIGURATION OF THE I2C
                    //================================================
                    printf("Unconfiguration...");

                    if( ErrorCode(oC_I2C_Unconfigure(&config,&context)) )
                    {
                        printf("Success\n");
                    }
                    else
                    {
                        printf("%R\n", errorCode);
                    }
                }
                else
                {
                    printf("Skipping unconfiguration (--dont-unconfigure is set)\n");
                }
            }
            else
            {
                printf("%R\n", errorCode);
            }
        }

    }
    else
    {
        PrintUsage(Argc,Argv);
    }

    return 0;
}

#endif
