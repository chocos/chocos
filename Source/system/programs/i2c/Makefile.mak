############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2017-02-17 - 19:41:20
##  Description:    makefile for i2c program
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
PROGRAM_NAME            = i2c
SPACE                   = CORE_SPACE
OPTIMALIZE              = O0
WARNING_FLAGS           = -Wall
CSTD                    = c99
DEFINITIONS             =
SOURCE_FILES            = main.c
INCLUDES_DIRS           = 
STANDARD_INPUT          = uart_stdio
STANDARD_OUTPUT         = uart_stdio
STANDARD_ERROR          = uart_stdio
HEAP_MAP_SIZE			= 0
PROCESS_STACK_SIZE      = 2048
ALLOCATION_LIMIT        = 0
TRACK_ALLOCATION        = FALSE

##============================================================================================================================
##                                          
##              INCLUDE MAIN MAKEFILE               
##                                          
##============================================================================================================================
include $(PROGRAM_MK_FILE_PATH)
