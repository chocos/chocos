/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the paint program
 *
 * @author     Patryk Kubiak - (Created on: 2017-05-06 - 22:38:43) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_ictrlman.h>
#include <oc_screenman.h>
#include <oc_struct.h>
#include <oc_tgui.h>

typedef enum
{
    Tool_Pen ,
    Tool_Brush ,
    Tool_Eraser ,
} Tool_t;

//==========================================================================================================================================
/**
 * @brief draws color picker
 */
//==========================================================================================================================================
void DrawColorPicker( oC_Screen_t Screen, oC_ColorMap_t * ColorMap, oC_Color_t Color, oC_ICtrlMan_Activity_t * Activity , oC_Pixel_ResolutionUInt_t X , oC_Pixel_ResolutionUInt_t Y , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_IDI_Event_t * outEvent )
{
    oC_Pixel_Position_t     topLeftPosition, bottomRightPosition;

    topLeftPosition.X       = X;
    topLeftPosition.Y       = Y;
    bottomRightPosition.X   = X + Width;
    bottomRightPosition.Y   = Y + Height;

    oC_ColorMap_DrawRect(ColorMap,topLeftPosition,bottomRightPosition,2,oC_ColorMap_BorderStyle_Rounded,oC_Color_White,Color,oC_ColorFormat_RGB888,true);

    Activity->EventsMask    = oC_IDI_EventId_AnyController | oC_IDI_EventId_Click;
    Activity->Height        = Height;
    Activity->Width         = Width;
    Activity->Position.X    = X;
    Activity->Position.Y    = Y;
    Activity->Screen        = Screen;

    printf("Event registration: %R\n", oC_ICtrlMan_RegisterActivity(Activity,outEvent));
}

//==========================================================================================================================================
/**
 * @brief draws tool
 */
//==========================================================================================================================================
void DrawTool( oC_Screen_t Screen, oC_ColorMap_t * ColorMap, char C , oC_ICtrlMan_Activity_t * Activity , oC_Pixel_ResolutionUInt_t X , oC_Pixel_ResolutionUInt_t Y , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_IDI_Event_t * outEvent )
{
    oC_Pixel_Position_t     topLeftPosition, bottomRightPosition;

    topLeftPosition.X       = X;
    topLeftPosition.Y       = Y;
    bottomRightPosition.X   = X + Width;
    bottomRightPosition.Y   = Y + Height;

    oC_ColorMap_DrawRect(ColorMap,topLeftPosition,bottomRightPosition,2,oC_ColorMap_BorderStyle_Rounded,oC_Color_Blue,oC_Color_White,oC_ColorFormat_RGB888,true);


    Activity->EventsMask    = oC_IDI_EventId_AnyController | oC_IDI_EventId_Click;
    Activity->Height        = Height;
    Activity->Width         = Width;
    Activity->Position.X    = X;
    Activity->Position.Y    = Y;
    Activity->Screen        = Screen;

    printf("Event registration: %R\n", oC_ICtrlMan_RegisterActivity(Activity,outEvent));

    topLeftPosition.X       = X + (Width  / 2);
    topLeftPosition.Y       = Y + (Height / 2);

    oC_ColorMap_DrawChar(ColorMap,topLeftPosition,oC_Color_Black,oC_ColorFormat_RGB888,C, oC_Font_(Consolas8pt));
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_Screen_t screen = oC_ScreenMan_GetDefaultScreen();

    if(screen == NULL)
    {
        printf("No screen detected!\n");
    }
    else
    {
        oC_ErrorCode_t          errorCode           = oC_ErrorCode_ImplementError;
        oC_ColorMap_t *         colorMap            = NULL;
        oC_TGUI_Key_t           key                 = 0;
        oC_Color_t              color               = oC_Color_Green;
        Tool_t                  tool                = Tool_Pen;

        oC_Struct_Define( oC_IDI_Event_t         , event);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , yellowActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , greenActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , redActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , blueActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , purpleActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , whiteActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , eraserActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , penActivity);
        oC_Struct_Define( oC_ICtrlMan_Activity_t , brushActivity);

        printf("Reading color map: %R\n", errorCode = oC_Screen_ReadColorMap(screen,&colorMap));

        DrawColorPicker(screen, colorMap, oC_Color_Yellow, &yellowActivity   , 5, 30,50,50, &event);
        DrawColorPicker(screen, colorMap, oC_Color_Green , &greenActivity    , 5, 90,50,50, &event);
        DrawColorPicker(screen, colorMap, oC_Color_Red   , &redActivity      , 5,150,50,50, &event);
        DrawColorPicker(screen, colorMap, oC_Color_Blue  , &blueActivity     ,60, 30,50,50, &event);
        DrawColorPicker(screen, colorMap, oC_Color_Purple, &purpleActivity   ,60, 90,50,50, &event);
        DrawColorPicker(screen, colorMap, oC_Color_White , &whiteActivity    ,60,150,50,50, &event);
        DrawTool       (screen, colorMap, 'E', &eraserActivity   , 5,210,30,40, &event);
        DrawTool       (screen, colorMap, 'P', &penActivity      ,40,210,30,40, &event);
        DrawTool       (screen, colorMap, 'B', &brushActivity    ,75,210,30,40, &event);

        printf("Press Ctrl+C to exit...\n");

        while(
                oC_ErrorOccur(errorCode) == false &&
                (oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC )
             )
        {
            if( ErrorCode( oC_ICtrlMan_WaitForRawEvent(screen,oC_IDI_EventId_AnyController | oC_IDI_EventId_ClickWithoutRelease,&event,hour(10),ms(10)) ) )
            {
                if( oC_ICtrlMan_HasActivityOccurred(&yellowActivity) )
                {
                    color = oC_Color_Yellow;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&greenActivity) )
                {
                    color = oC_Color_Green;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&redActivity) )
                {
                    color = oC_Color_Red;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&blueActivity) )
                {
                    color = oC_Color_Blue;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&purpleActivity) )
                {
                    color = oC_Color_Purple;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&whiteActivity) )
                {
                    color = oC_Color_White;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&eraserActivity) )
                {
                    tool = Tool_Eraser;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&penActivity) )
                {
                    tool = Tool_Pen;
                }
                else if( oC_ICtrlMan_HasActivityOccurred(&brushActivity) )
                {
                    tool = Tool_Brush;
                }
                else if(event.Position[0].X > 115 && event.Position[0].Y > 30)
                {
                    if(tool == Tool_Pen)
                    {
                        oC_ColorMap_SetColor(colorMap,event.Position[0],color,oC_ColorFormat_RGB888);
                    }
                    else if(tool == Tool_Brush)
                    {
                        oC_Pixel_Position_t endPosition;

                        endPosition.X = event.Position[0].X + event.Weight[0] / 2;
                        endPosition.Y = event.Position[0].Y + event.Weight[0] / 2;

                        oC_ColorMap_DrawRect(colorMap,event.Position[0],endPosition,0,0,oC_ColorMap_BorderStyle_Rounded,color,oC_ColorFormat_RGB888,true);
                    }
                    else if(tool == Tool_Eraser)
                    {
                        oC_Pixel_Position_t endPosition;

                        endPosition.X = event.Position[0].X + event.Weight[0];
                        endPosition.Y = event.Position[0].Y + event.Weight[0];

                        oC_ColorMap_DrawRect(colorMap,event.Position[0],endPosition,0,0,oC_ColorMap_BorderStyle_Rounded,oC_Color_Black,oC_ColorFormat_RGB888,true);
                    }
                }
            }
            else
            {
                printf("Error: %R\n", errorCode);
            }
        }
        oC_ICtrlMan_UnregisterActivity(&yellowActivity);
        oC_ICtrlMan_UnregisterActivity(&greenActivity);
        oC_ICtrlMan_UnregisterActivity(&redActivity);
        oC_ICtrlMan_UnregisterActivity(&blueActivity);
        oC_ICtrlMan_UnregisterActivity(&purpleActivity);
        oC_ICtrlMan_UnregisterActivity(&whiteActivity);
        oC_ICtrlMan_UnregisterActivity(&eraserActivity);
        oC_ICtrlMan_UnregisterActivity(&penActivity);
        oC_ICtrlMan_UnregisterActivity(&brushActivity);
    }

    return 0;
}
