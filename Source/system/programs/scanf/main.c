/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the scanf program
 *
 * @author     Patryk Kubiak - (Created on: 2023-04-06 - 12:34:16) 
 *
 * @note       Copyright (C) 2023 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vt100.h>
#include <oc_compiler.h>
#include <oc_errors.h>

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * @param FileName      Name of the file where the function was called
 * @param FunctionName  Name of the function where the function was called
 * @param ProcedureName Name of the procedure where the function was called
 * @param LineNumber    Number of the line where the function was called
 *
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
static bool CheckEqualInt( int Value, int Expected, const char* FileName, const char* FunctionName, const char* ProcedureName, int LineNumber )
{
    if( Value != Expected )
    {
        printf( oC_VT100_FG_RED "%s:%s:%d: Procedure '%s' failed! Value: %d Expected: %d\n" oC_VT100_FG_WHITE, FileName, FunctionName, LineNumber, ProcedureName, Value, Expected );
        return false;
    }
    return true;
}

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * @param FileName      Name of the file where the function was called
 * @param FunctionName  Name of the function where the function was called
 * @param ProcedureName Name of the procedure where the function was called
 * @param LineNumber    Number of the line where the function was called
 *
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
static bool CheckEqualChar( char Value, char Expected, const char* FileName, const char* FunctionName, const char* ProcedureName, int LineNumber )
{
    if( Value != Expected )
    {
        printf( oC_VT100_FG_RED "%s:%s:%d: Procedure '%s' failed! Value: '%c' Expected: '%c'\n" oC_VT100_FG_WHITE, FileName, FunctionName, LineNumber, ProcedureName, Value, Expected );
        return false;
    }
    return true;
}

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * @param FileName      Name of the file where the function was called
 * @param FunctionName  Name of the function where the function was called
 * @param ProcedureName Name of the procedure where the function was called
 * @param LineNumber    Number of the line where the function was called
 *
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
static bool CheckEqualString( const char * Value, const char * Expected, const char* FileName, const char* FunctionName, const char* ProcedureName, int LineNumber )
{
    if( strcmp( Value, Expected ) != 0 )
    {
        printf( oC_VT100_FG_RED "%s:%s:%d: Procedure '%s' failed! Value: %s Expected: %s\n" oC_VT100_FG_WHITE, FileName, FunctionName, LineNumber, ProcedureName, Value, Expected );
        return false;
    }
    return true;
}

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * @param FileName      Name of the file where the function was called
 * @param FunctionName  Name of the function where the function was called
 * @param ProcedureName Name of the procedure where the function was called
 * @param LineNumber    Number of the line where the function was called
 *
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
static bool CheckEqualFloat( float Value, float Expected, const char* FileName, const char* FunctionName, const char* ProcedureName, int LineNumber )
{
    if( Value != Expected )
    {
        printf( oC_VT100_FG_RED "%s:%s:%d: Procedure '%s' failed! Value: %f Expected: %f\n" oC_VT100_FG_WHITE, FileName, FunctionName, LineNumber, ProcedureName, Value, Expected );
        return false;
    }
    return true;
}

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * 
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
#define CHECK_EQUAL_INT( Value, Expected )      if (!CheckEqualInt( Value, Expected, oC_FILE, oC_FUNCTION,__test_procedure_name, oC_LINE )) return false

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * 
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
#define CHECK_EQUAL_CHAR( Value, Expected )     if (!CheckEqualChar( Value, Expected, oC_FILE, oC_FUNCTION,__test_procedure_name, oC_LINE )) return false

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * 
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
#define CHECK_EQUAL_STRING( Value, Expected )   if (!CheckEqualString( Value, Expected, oC_FILE, oC_FUNCTION,__test_procedure_name, oC_LINE )) return false

//==========================================================================================================================================
/**
 * The function that checks if the value is equal to the expected value
 *
 * @param Value         Value to check
 * @param Expected      Expected value
 * 
 * @return true if the value is equal to the expected value, false otherwise
 */
//==========================================================================================================================================
#define CHECK_EQUAL_FLOAT( Value, Expected )    if (!CheckEqualFloat( Value, Expected, oC_FILE, oC_FUNCTION,__test_procedure_name, oC_LINE )) return false

//==========================================================================================================================================
/**
 * @brief Definition of the test procedure
 * 
 * @param Name  Name of the test procedure
 */
//==========================================================================================================================================
#define oC_TestProcedure_Begin( Name ) do{\
    testResult = false;\
    const char* __test_procedure_name = Name;\

//==========================================================================================================================================
/**
 * @brief Definition of the test procedure
 * 
 * @param Name  Name of the test procedure
 */
//==========================================================================================================================================
#define oC_TestProcedure_End \
        testResult = true;\
        printf(oC_VT100_FG_GREEN "%s:%s:%d: Procedure '%s' passed!\n" oC_VT100_FG_WHITE, oC_FILE, oC_FUNCTION, oC_LINE, __test_procedure_name );\
        }while(0);

//==========================================================================================================================================
/**
 * @brief  The function that tests the sscanf function with the integer format is handled correctly
 * 
 * @return true if the test passed, false otherwise 
 */
//==========================================================================================================================================
static bool TestCase_sscanf_Integer()
{
    bool testResult = false;
    int i;

    oC_TestProcedure_Begin( "valid integer" )
    {
        CHECK_EQUAL_INT( sscanf( "123", "%d", &i ), 0 );
        CHECK_EQUAL_INT( i, 123 );

        CHECK_EQUAL_INT( sscanf( "123 ", "%d ", &i ), 0 );
        CHECK_EQUAL_INT( i, 123 );

        CHECK_EQUAL_INT( sscanf( " 123", " %d", &i ), 0 );
        CHECK_EQUAL_INT( i, 123 );

        CHECK_EQUAL_INT( sscanf( " 123 ", " %d ", &i ), 0 );
        CHECK_EQUAL_INT( i, 123 );

        CHECK_EQUAL_INT( sscanf( "123abc", "%d", &i ), 0 );
        CHECK_EQUAL_INT( i, 123 );

        CHECK_EQUAL_INT( sscanf( "123abc ", "%d ", &i ), 0 );
        CHECK_EQUAL_INT( i, 123 );

        CHECK_EQUAL_INT( sscanf( " 123abc", " %d", &i ), 0 );
        CHECK_EQUAL_INT( i, 123 );

        CHECK_EQUAL_INT( sscanf( "0", "%d", &i ), 0 );
        CHECK_EQUAL_INT( i, 0 );
    }
    oC_TestProcedure_End

    return testResult;
}

//==========================================================================================================================================
/**
 * @brief  The function that tests the sscanf function with the float format is handled correctly
 * 
 * @return true if the test passed, false otherwise 
 */
//==========================================================================================================================================
static bool TestCase_sscanf_Float()
{
    bool testResult = false;
    float f;

    oC_TestProcedure_Begin( "valid float" )
    {
        CHECK_EQUAL_INT( sscanf( "123.456", "%f", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 123.456 );

        CHECK_EQUAL_INT( sscanf( "123.456 ", "%f ", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 123.456 );

        CHECK_EQUAL_INT( sscanf( " 123.456", " %f", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 123.456 );

        CHECK_EQUAL_INT( sscanf( " 123.456 ", " %f ", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 123.456 );

        CHECK_EQUAL_INT( sscanf( "123.456abc", "%f", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 123.456 );

        CHECK_EQUAL_INT( sscanf( "123.456abc ", "%f ", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 123.456 );

        CHECK_EQUAL_INT( sscanf( " 123.456abc", " %f", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 123.456 );

        CHECK_EQUAL_INT( sscanf( "0.0", "%f", &f ), 0 );
        CHECK_EQUAL_FLOAT( f, 0.0 );
    }
    oC_TestProcedure_End

    return testResult;
}

//==========================================================================================================================================
/**
 * @brief  The function that tests the sscanf function with the string format is handled correctly
 * 
 * @return true if the test passed, false otherwise 
 */
//==========================================================================================================================================
static bool TestCase_sscanf_String()
{
    bool testResult = false;
    char s[100];

    oC_TestProcedure_Begin( "valid string" )
    {
        CHECK_EQUAL_INT( sscanf( "123.456", "%s", s ), 0 );
        CHECK_EQUAL_STRING( s, "123.456" );

        CHECK_EQUAL_INT( sscanf( "123.456 ", "%s", s ), 0 );
        CHECK_EQUAL_STRING( s, "123.456" );

        CHECK_EQUAL_INT( sscanf( " 123.456", " %s", s ), 0 );
        CHECK_EQUAL_STRING( s, "123.456" );

        CHECK_EQUAL_INT( sscanf( " 123.456 ", " %s ", s ), 0 );
        CHECK_EQUAL_STRING( s, "123.456" );

        CHECK_EQUAL_INT( sscanf( "123.456abc", "%s", s ), 0 );
        CHECK_EQUAL_STRING( s, "123.456abc" );

        CHECK_EQUAL_INT( sscanf( "123.456abc ", "%s ", s ), 0 );
        CHECK_EQUAL_STRING( s, "123.456abc" );

        CHECK_EQUAL_INT( sscanf( " 123.456abc", " %s", s ), 0 );
        CHECK_EQUAL_STRING( s, "123.456abc" );

        CHECK_EQUAL_INT( sscanf( "0.0", "%s", s ), 0 );
        CHECK_EQUAL_STRING( s, "0.0" );
    }
    oC_TestProcedure_End

    return testResult;
}

//==========================================================================================================================================
/**
 * @brief  The function that tests the sscanf function with the char format is handled correctly
 * 
 * @return true if the test passed, false otherwise 
 */
//==========================================================================================================================================
static bool TestCase_sscanf_Char()
{
    bool testResult = false;
    char c;

    oC_TestProcedure_Begin( "valid char" )
    {
        CHECK_EQUAL_INT( sscanf( "1", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '1' );

        CHECK_EQUAL_INT( sscanf( "1 ", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '1' );

        CHECK_EQUAL_INT( sscanf( " 1", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '1' );

        CHECK_EQUAL_INT( sscanf( " @ ", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '@' );

        CHECK_EQUAL_INT( sscanf( "!", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '!' );

        CHECK_EQUAL_INT( sscanf( "123.456abc ", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '1' );

        CHECK_EQUAL_INT( sscanf( " 123.456abc", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '1' );

        CHECK_EQUAL_INT( sscanf( "0.0", "%c", &c ), 0 );
        CHECK_EQUAL_CHAR( c, '0' );
    }
    oC_TestProcedure_End

    return testResult;
}

//==========================================================================================================================================
/**
 * The application that allows for reading data from the standard input according to the format string
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    TestCase_sscanf_Integer();
    TestCase_sscanf_Float();
    TestCase_sscanf_String();
    TestCase_sscanf_Char();

    return 0;
}
