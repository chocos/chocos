/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the spi program
 *
 * @author     Patryk Kubiak - (Created on: 2017-07-22 - 17:40:59) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_spi.h>
#include <oc_tgui.h>
#include <oc_kprint.h>
#include <ctype.h>

#ifndef oC_SPI_LLD_AVAILABLE
int main( int Argc , char ** Argv )
{
    printf("SPI is not available (or not implemented) on this machine\n");
    return oC_ErrorCode_NotSupportedOnTargetMachine;
}
#else

typedef enum
{
    OptionType_String ,
    OptionType_List ,
    OptionType_Pin ,
    OptionType_Numeric ,
    OptionType_Channel ,
} OptionType_t;

typedef struct
{
    const char *    String;
    OptionType_t    Type;
    bool            Required;
    struct
    {
        const char *    Name;
        uint32_t        Value;
    } Options[5];
} CommandArgument_t;

static const CommandArgument_t PossibleArguments[] = {
    {
        .Required    = true ,
        .String      = "--mode" ,
        .Type        = OptionType_List ,
        .Options     = {
                        { .Name = "master" , .Value = oC_SPI_Mode_Master    } ,
                        { .Name = "slave"  , .Value = oC_SPI_Mode_Slave     } ,
        } ,
    } ,
    {
        .Required    = true ,
        .String      = "--baud-rate" ,
        .Type        = OptionType_Numeric ,
    } ,
    {
        .Required    = true ,
        .String      = "--tolerance" ,
        .Type        = OptionType_Numeric ,
    } ,
    {
        .Required    = true ,
        .String      = "--CLK" ,
        .Type        = OptionType_Pin ,
    } ,
    {
        .Required    = true ,
        .String      = "--MOSI" ,
        .Type        = OptionType_Pin ,
    } ,
    {
        .Required    = true ,
        .String      = "--MISO" ,
        .Type        = OptionType_Pin ,
    } ,
    {
        .Required    = false ,
        .String      = "--CS" ,
        .Type        = OptionType_Pin ,
    } ,
    {
        .Required    = true ,
        .String      = "--frame-format" ,
        .Type        = OptionType_List ,
        .Options     = {
                        { .Name = "LSB" , .Value = oC_SPI_FrameFormat_LSB } ,
                        { .Name = "MSB" , .Value = oC_SPI_FrameFormat_MSB } ,
        } ,
    } ,
    {
        .Required    = true ,
        .String      = "--clock-polarity" ,
        .Type        = OptionType_List ,
        .Options     = {
                        { .Name = "LowActive"  , .Value = oC_SPI_ClockPolarity_LowActive  } ,
                        { .Name = "HighActive" , .Value = oC_SPI_ClockPolarity_HighActive } ,
        } ,
    } ,
    {
        .Required    = true ,
        .String      = "--clock-phase" ,
        .Type        = OptionType_List ,
        .Options     = {
                        { .Name = "FirstEdge"  , .Value = oC_SPI_ClockPhase_FirstTransition     } ,
                        { .Name = "SecondEdge" , .Value = oC_SPI_ClockPhase_SecondTransition    } ,
        } ,
    } ,
    {
        .Required    = true ,
        .String      = "--data-size" ,
        .Type        = OptionType_Numeric,
    } ,
    {
        .Required    = true ,
        .String      = "--chip-activity" ,
        .Type        = OptionType_List ,
        .Options     = {
                        { .Name = "Low"         , .Value = false    } ,
                        { .Name = "High"        , .Value = true     } ,
        } ,
    } ,
    {
        .Required    = false ,
        .String      = "--channel" ,
        .Type        = OptionType_Channel ,
    } ,
    {
        .Required    = false ,
        .String      = "--message" ,
        .Type        = OptionType_String ,
    } ,
};

static bool PrintCommandHelp( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv );
static bool Write           ( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv );
static bool Read            ( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv );
static bool Tranceive       ( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv );

//==========================================================================================================================================
/**
 * @brief list of commands available in the command-mode interface
 */
//==========================================================================================================================================
static const struct
{
    char *      Command;
    char *      Usage;
    char *      Brief;
    char *      Description;
    bool (*Handler)( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv );
} CommandsList[] = {
                {
                   .Command             = "exit" ,
                   .Usage               = "exit" ,
                   .Brief               = "closes the terminal",
                   .Description         = "The command designed for closing the command-mode interface",
                   .Handler             = NULL ,
                } ,
                {
                   .Command             = "help" ,
                   .Usage               = "help [command]" ,
                   .Brief               = "prints this help or detailed informations about the command",
                   .Description         = "Prints list of available commands or help in details about the given command" ,
                   .Handler             = PrintCommandHelp ,
                } ,
                {
                   .Command             = "write" ,
                   .Usage               = "write -f [file_name] -n [number] -s [string] -t [timeout(s)]" ,
                   .Brief               = "Sends a message to the slave",
                   .Description         = "The command is for sending a message by using configured SPI channel. It is possible to send a message from a file, string or just a number.\n"
                                          "    -f [file_name]       - Source file to send by using the SPI interface\n"
                                          "    -n [number]          - Number to send in any of supported formats (decimal/hex/octal/binary)\n"
                                          "    -s [string]          - String to send\n"
                                          "    -t [timeout(s)]      - Maximum time to wait for a data in seconds<optional>\n"
                                          "Note, that only one message can be send at once - so you have to choose which format of data you want to send (you cannot try to send file and string at once for example)\n"
                                           ,
                   .Handler             = Write ,
                } ,
                {
                   .Command             = "read" ,
                   .Usage               = "read no_bytes -f [file_name] -t [timeout(s)]" ,
                   .Brief               = "Receive a message from the slave",
                   .Description         = "The command is for receiving a message by using configured SPI channel.\n"
                                          "    no_bytes             - Number of bytes to request/read\n"
                                          "    -f [file_name]       - Destination file name to write received data <optional>\n"
                                          "    -t [timeout(s)]      - Maximum time to wait for a data in seconds<optional>\n"
                                          ,
                   .Handler             = Read ,
                } ,
                {
                   .Command             = "tranceive" ,
                   .Usage               = "tranceive -fi [input_file_name] -fo [output_file_name] -s [string] -n [number] -t [timeout(s)]" ,
                   .Brief               = "Sends and receives a message to the slave",
                   .Description         = "The command is for receiving a message by using configured SPI channel.\n"
                                          "    no_bytes             - Number of bytes to request/read\n"
                                          "    -fi [file_name]      - Source file name to send<optional>\n"
                                          "    -fo [file_name]      - Destination file name for received data<optional>\n"
                                          "    -s [string]          - String to send\n"
                                          "    -n [number]          - Number to send in any of supported formats (decimal/hex/octal/binary)\n"
                                          "    -t [timeout(s)]      - Maximum time to wait for a data in seconds<optional>\n"
                                          "Note, that only one message can be send at once - so you have to choose which format of data you want to send (you cannot try to send file and string at once for example)\n"
                                          ,
                   .Handler             = Tranceive ,
                } ,
};

//==========================================================================================================================================
/**
 * @brief searches for and reads command argument
 */
//==========================================================================================================================================
static bool ReadCommandArgument( int Argc , char ** Argv , const char * Name , void * outValue )
{
    bool correct = false;

    oC_ARRAY_FOREACH_IN_ARRAY(PossibleArguments,argument)
    {
        if( strcmp(Name,argument->String) == 0 )
        {
            char * valueString = oC_GetArgumentAfter(Argc,Argv,Name);

            if(valueString == NULL)
            {
                correct = argument->Required == false;

                if(argument->Required)
                {
                    printf("Required command argument '%s' has not been found!\n", Name);
                }
            }
            else
            {
                if(argument->Type == OptionType_Channel)
                {
                    oC_SPI_LLD_ForEachChannel(channel,ChannelIndex)
                    {
                        if(strcmp(valueString,oC_Channel_GetName(channel)) == 0)
                        {
                            oC_SPI_Channel_t * outChannel = outValue;
                            *outChannel = channel;
                            correct = true;
                            break;
                        }
                    }
                    if(correct == false)
                    {
                        printf("Channel '%s' is not recognized\n", valueString);
                    }
                }
                else if(argument->Type == OptionType_List)
                {
                    oC_ARRAY_FOREACH_IN_ARRAY(argument->Options,option)
                    {
                        if( strcmp(valueString,option->Name) == 0 )
                        {
                            uint32_t * outInt = outValue;
                            *outInt = option->Value;
                            correct = true;
                            break;
                        }
                    }
                    if(correct == false)
                    {
                        printf("Unknown option '%s' as argument for '%s'\n", valueString, Name);
                    }
                }
                else if(argument->Type == OptionType_Numeric)
                {
                    if( sscanf( valueString, "%i", outValue ) == 0)
                    {
                        correct = true;
                    }
                }
                else if(argument->Type == OptionType_Pin)
                {
                    oC_Pin_t *      outPin      = outValue;
                    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;

                    errorCode = oC_GPIO_FindPinByName(valueString,outPin);

                    if(oC_ErrorOccur(errorCode))
                    {
                        printf("Pin of argument '%s' (%s) has not been found: %R\n", Name, valueString, errorCode);
                    }
                    else
                    {
                        correct = true;
                    }
                }
                else if(argument->Type == OptionType_String)
                {
                    char ** outString = outValue;

                    *outString = valueString;

                    correct = true;
                }
            }

            break;
        }
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief prints usage message
 */
//==========================================================================================================================================
static void PrintUsage( char * ProgramName )
{
    printf("%s --channels-list / --CLK CLK_PIN (...) --message MESSAGE / --command-mode \n", ProgramName );
    printf("   where:\n");
    printf("          --channels-list           - Will print list of channels\n");
    printf("          --mode MODE               - SPI mode of the device (master/slave) (required)\n");
    printf("          --baud-rate BAUD_RATE     - Baud rate of the transmission (required)\n");
    printf("          --CLK CLK_PIN             - Program will use the given CLK_PIN (required)\n");
    printf("          --MOSI MOSI_PIN           - Program will use the given MOSI_PIN (required)\n");
    printf("          --MISO MISO_PIN           - Program will use the given MISO_PIN (required)\n");
    printf("          --CS CS_PIN               - Program will use the given (chip select) CS_PIN (optional)\n");
    printf("          --frame-format FORMAT     - Format of the frame ('LSB' or 'MSB') (required)\n");
    printf("          --clock-polarity POLARITY - Polarity of the clock ('LowActive' or 'HighActive') (required)\n");
    printf("          --clock-phase PHASE       - Phase of the clock ('FirstEdge' or 'SecondEdge') (required)\n");
    printf("          --data-size SIZE          - size of data in bits (required)\n");
    printf("          --chip-activity ACTIVITY  - Activity of the CS - ('Low' or 'High') (required)\n");
    printf("          --channel SPI_CHANNEL     - Program will use the given SPI channel (optional)\n");
    printf("          --message MESSAGE         - Message to send after configuration (optional)\n");
    printf("          --command-mode            - Allows for commands execution before unconfiguration (optional)\n");
}

//==========================================================================================================================================
/**
 * @brief configures SPI
 */
//==========================================================================================================================================
static bool ConfigureSPI( int Argc , char ** Argv , oC_SPI_Config_t * Config, oC_SPI_Context_t * Context )
{
    bool            success         = false;
    uint32_t        mode            = 0;
    uint32_t        baudRate        = 0;
    uint32_t        tolerance       = 0;
    uint32_t        frameFormat     = 0;
    uint32_t        clockPolarity   = 0;
    uint32_t        clockPhase      = 0;
    uint32_t        dataSize        = 0;
    uint32_t        chipActivity    = 0;
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;

    if(
        ReadCommandArgument( Argc, Argv, "--mode"           , &mode                         )
     && ReadCommandArgument( Argc, Argv, "--baud-rate"      , &baudRate                     )
     && ReadCommandArgument( Argc, Argv, "--tolerance"      , &tolerance                    )
     && ReadCommandArgument( Argc, Argv, "--CLK"            , &Config->Pins.CLK             )
     && ReadCommandArgument( Argc, Argv, "--MOSI"           , &Config->Pins.MOSI            )
     && ReadCommandArgument( Argc, Argv, "--MISO"           , &Config->Pins.MISO            )
     && ReadCommandArgument( Argc, Argv, "--CS"             , &Config->Pins.ChipSelect[0]   )
     && ReadCommandArgument( Argc, Argv, "--frame-format"   , &frameFormat                  )
     && ReadCommandArgument( Argc, Argv, "--clock-polarity" , &clockPolarity                )
     && ReadCommandArgument( Argc, Argv, "--clock-phase"    , &clockPhase                   )
     && ReadCommandArgument( Argc, Argv, "--data-size"      , &dataSize                     )
     && ReadCommandArgument( Argc, Argv, "--chip-activity"  , &chipActivity                 )
     && ReadCommandArgument( Argc, Argv, "--channel"        , &Config->Advanced.Channel     )
        )
    {
        Config->Mode                 = mode;
        Config->BaudRate             = baudRate;
        Config->Tolerance            = tolerance;
        Config->FrameFormat          = frameFormat;
        Config->ClockPolarity        = clockPolarity;
        Config->ClockPhase           = clockPhase;
        Config->DataSize             = dataSize;
        Config->ChipSelectActiveHigh = (bool)chipActivity;

        printf("Configuration of the SPI ... ");

        if( ErrorCode(oC_SPI_Configure(Config,Context)) )
        {
            printf(oC_VT100_FG_GREEN "OK\n" oC_VT100_FG_WHITE);
            success = true;

            printf("\t Baud rate: %llu\n", oC_SPI_GetConfiguredBaudRate(*Context));
        }
        else
        {
            printf(oC_VT100_FG_RED "ERROR: %R\n" oC_VT100_FG_WHITE, errorCode);
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief configures SPI
 */
//==========================================================================================================================================
static bool UnconfigureSPI( oC_SPI_Context_t * Context )
{
    printf("Unconfiguration of the SPI ... ");

    oC_ErrorCode_t errorCode = oC_SPI_Unconfigure(NULL,Context);

    printf("%R\n", errorCode);

    return true;
}

//==========================================================================================================================================
/**
 * @brief configures SPI
 */
//==========================================================================================================================================
static bool SendMessage( int Argc , char ** Argv , oC_SPI_Context_t Context )
{
    char *              message     = oC_GetArgumentAfter(Argc,Argv,"--message");
    oC_MemorySize_t     size        = strlen(message) + 1;
    oC_ErrorCode_t      errorCode   = oC_ErrorCode_ImplementError;
    oC_DefaultString_t  rxBuffer    = {0};

    printf("Sending message '%s' ... ", message);

    errorCode = oC_SPI_Write(Context,message,&size,s(3));

    if(oC_ErrorOccur(errorCode))
    {
        printf(oC_VT100_FG_RED "%R\n" oC_VT100_FG_WHITE, errorCode);
    }
    else
    {
        printf(oC_VT100_FG_GREEN "OK\n" oC_VT100_FG_WHITE);

        printf("Receiving message ... ");

        size = sizeof(rxBuffer);

        errorCode = oC_SPI_Read(Context,rxBuffer,&size,s(3));

        if(oC_ErrorOccur(errorCode))
        {
            printf(oC_VT100_FG_RED "%R\n" oC_VT100_FG_WHITE, errorCode);
        }
        else
        {
            printf(oC_VT100_FG_GREEN "OK\n" oC_VT100_FG_WHITE);
            printf("Received message: %s\n", rxBuffer);
        }
    }

    return true;
}


//==========================================================================================================================================
/**
 * @brief prints command incentive
 */
//==========================================================================================================================================
static void PrintCommandIncentive( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context )
{
    const char *     modeString = "master:";
    oC_SPI_Channel_t channel    = Config->Advanced.Channel;
    const char *     channelName= "unknown";

    oC_SPI_ReadChannel(Context,&channel);

    if(oC_Channel_IsCorrect(SPI,channel))
    {
        channelName = oC_Channel_GetName(channel);
    }

    printf("\r" oC_VT100_FG_GREEN "%s:" oC_VT100_FG_YELLOW "%s" oC_VT100_FG_WHITE "@" oC_VT100_FG_GREEN "%s" oC_VT100_FG_WHITE ":" oC_VT100_FG_GREEN "%s" oC_VT100_FG_YELLOW "$ " oC_VT100_FG_WHITE, channelName, modeString, oC_GPIO_GetPinName(Config->Pins.MOSI), oC_GPIO_GetPinName(Config->Pins.MISO));
}


//==========================================================================================================================================
/**
 * @brief reads command into the buffer and returns true if the command is given
 */
//==========================================================================================================================================
static bool ReadCommand( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , char * outBuffer, oC_MemorySize_t Size )
{
    memset(outBuffer,0,Size);
    printf(oC_VT100_SAVE_CURSOR_AND_ATTRS);

    return oC_KPrint_ReadFromStdIn(oC_IoFlags_EchoWhenRead | oC_IoFlags_ReadOneLine | oC_IoFlags_WaitForAllElements | oC_IoFlags_SleepWhileWaiting | oC_IoFlags_NoTimeout , outBuffer , Size ) == oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief split command to list of arguments
 */
//==========================================================================================================================================
static bool SplitCommandToArguments( char * Command , int * outArgc , char *** outArgv )
{
    int argumentIndex       = 0;
    int argumentCount       = 0;
    oC_UInt_t cmdSize       = strlen(Command)+1;
    bool commandSpecified   = false;
    bool newArgumentStarted = false;
    bool quoteStarted       = false;

    //========================================================
    /* Counting number of arguments                         */
    //========================================================
    for(int signIndex = 0; signIndex < cmdSize ; signIndex++ )
    {
        /* If it is space, the new argument has started */
        if(isspace((int)Command[signIndex]) && !quoteStarted)
        {
            /* if some argument occurs before */
            if(newArgumentStarted)
            {
                argumentCount++;
            }
            newArgumentStarted = false;
        }
        else if(Command[signIndex] == '\"')
        {
            if(quoteStarted == false)
            {
                quoteStarted = true;
            }
            else
            {
                quoteStarted = false;
            }
        }
        else if(Command[signIndex] != 0)
        {
            /* found some argument */
            newArgumentStarted = true;
        }
    }

    if(newArgumentStarted == true)
    {
        argumentCount++;
    }

    //========================================================
    /* Allocation memory for argument array                 */
    //========================================================
    if(argumentCount > 0)
    {
        char ** argv = malloc(argumentCount * sizeof(char*),AllocationFlags_ZeroFill | AllocationFlags_CanWait1Second);

        newArgumentStarted = false;
        quoteStarted       = false;
        argumentIndex      = 0;

        for(int signIndex = 0; signIndex < cmdSize ; signIndex++ )
        {
            /* If it is space, the new argument has started */
            if(isspace((int)Command[signIndex]) && !quoteStarted)
            {
                /* if some argument occurs before */
                if(newArgumentStarted)
                {
                    argumentIndex++;
                }
                newArgumentStarted = false;

                /* Set this sign to 0 to mark an end of the string */
                Command[signIndex] = 0;
            }
            else if(Command[signIndex] == '\"')
            {
                if(quoteStarted == false)
                {
                    quoteStarted = true;
                }
                else
                {
                    quoteStarted = false;
                }
                Command[signIndex] = 0;
            }
            else
            {
                if(newArgumentStarted == false)
                {
                    /* It this is new argument, then save pointer to it */
                    argv[argumentIndex] = &Command[signIndex];
                }
                /* found some argument */
                newArgumentStarted = true;
            }
        }

        if(quoteStarted)
        {
            oC_PrintWarningMessage("Quote started but not finished!");
        }

        *outArgv         = argv;
        *outArgc         = argumentCount;
        commandSpecified = true;
    }

    return commandSpecified;
}


//==========================================================================================================================================
/**
 * @brief parses and executes command (returns true if exit has been typed)
 */
//==========================================================================================================================================
static bool ParseAndExecuteCommand( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , char * Buffer, oC_MemorySize_t Size )
{
    bool    exitTyped   = false;
    int     argc        = 0;
    char ** argv        = NULL;
    bool    recognized  = false;

    if(SplitCommandToArguments(Buffer,&argc,&argv))
    {
        oC_ARRAY_FOREACH_IN_ARRAY(CommandsList,command)
        {
            if(strcmp(argv[0],command->Command) == 0)
            {
                recognized = true;

                if(command->Handler == NULL)
                {
                    exitTyped = true;
                }
                else if(command->Handler(Config,Context,argc,argv) == false)
                {
                    printf("Usage: %s\n", command->Usage);
                }
                break;
            }
        }
        if(recognized == false)
        {
            printf("Command '%s' is not recognized!\n", argv[0]);
        }

        free(argv,0);
    }

    return exitTyped;
}

//==========================================================================================================================================
/**
 * @brief configures SPI
 */
//==========================================================================================================================================
static void RunCommandMode( oC_SPI_Config_t * Config , oC_SPI_Context_t Context )
{
    char commandBuffer[100];

    while(1)
    {
        PrintCommandIncentive(Config,Context);
        if(
            ReadCommand           ( Config, Context, commandBuffer, sizeof(commandBuffer) )
         && ParseAndExecuteCommand( Config, Context, commandBuffer, sizeof(commandBuffer) )
            )
        {
            break;
        }
    }
}

//==========================================================================================================================================
/**
 * @brief prints list of SPI channels
 */
//==========================================================================================================================================
static void PrintSPIChannels(void)
{
    oC_TGUI_SetForegroundColor(oC_TGUI_Color_DarkGray);

    printf("List of SPI channels available in your target: \n");

    oC_SPI_LLD_ForEachChannel(channel,ChannelIndex)
    {
        oC_TGUI_SetForegroundColor(oC_TGUI_Color_LightBlue);
        printf(" %s\n", oC_Channel_GetName(channel));

        oC_ModulePin_ForeachDefined(pin)
        {
            if(pin->Channel == channel)
            {
                bool used = false;
                oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);

                printf("    %5s | %10s | ", oC_GPIO_GetPinName(pin->Pin), oC_PinFunction_GetName(pin->PinFunction));

                oC_GPIO_CheckIsPinUsed(pin->Pin,&used);

                if(used)
                {
                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Red);
                    printf("USED\n");
                }
                else
                {
                    oC_TGUI_SetForegroundColor(oC_TGUI_Color_Green);
                    printf("Free\n");
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
	bool                argumentsCorrect = false;
	oC_SPI_Context_t    context          = NULL;
    oC_SPI_Config_t     config;

    memset(&config, 0, sizeof(config));

	if( oC_ArgumentOccur(Argc,Argv,"--channels-list") )
	{
	    PrintSPIChannels();
	    argumentsCorrect = true;
	}
	else if( oC_ArgumentOccur(Argc,Argv,"--mode") )
	{
	    if(ConfigureSPI(Argc,Argv,&config,&context))
	    {
	        argumentsCorrect = true;

            if(oC_ArgumentOccur(Argc,Argv,"--message") )
            {
                SendMessage(Argc,Argv,context);
            }
            if(oC_ArgumentOccur(Argc,Argv,"--command-mode") )
            {
                RunCommandMode(&config,context);
            }
            UnconfigureSPI( &context );
	    }
	    else
	    {
	        argumentsCorrect = false;
	    }
	}

	if(argumentsCorrect == false)
	{
	    PrintUsage( Argv[0] );
	}
    return 0;
}


//==========================================================================================================================================
/**
 * @brief prints help for command interface
 */
//==========================================================================================================================================
static bool PrintCommandHelp( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv )
{
    bool commandCorrect = false;

    if(Argc > 1)
    {
        oC_ARRAY_FOREACH_IN_ARRAY(CommandsList,command)
        {
            if(strcmp(Argv[1],command->Command) == 0)
            {
                commandCorrect = true;

                printf("\nHelp for command '%s' (%s): \n\n", command->Command, command->Brief);
                printf("    %s\n\n", command->Usage);
                oC_KPrint_WriteToStdOut(oC_IoFlags_Default,command->Description,strlen(command->Description));
                printf("\n");
            }
        }
        if(commandCorrect == false)
        {
            printf("No help for command '%s'\n", Argv[1]);
        }
    }
    else
    {
        commandCorrect = true;

        printf("List of available commands: (please type 'help <command>' for details)\n");
        oC_ARRAY_FOREACH_IN_ARRAY(CommandsList,command)
        {
            printf("    %-15s - %s\n", command->Command, command->Brief);
        }
    }

    return commandCorrect;
}

//==========================================================================================================================================
/**
 * @brief sends a message via SPI
 */
//==========================================================================================================================================
static bool Write( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv )
{
    bool                commandCorrect   = true;
    char *              fileToSend       = oC_GetArgumentAfter(Argc,Argv,"-f");
    char *              numberToSend     = oC_GetArgumentAfter(Argc,Argv,"-n");
    char *              stringToSend     = oC_GetArgumentAfter(Argc,Argv,"-s");
    char *              timeoutString    = oC_GetArgumentAfter(Argc,Argv,"-t");
    void *              bufferToSend     = NULL;
    oC_MemorySize_t     bufferToSendSize = 0;
    char *              fileBuffer       = NULL;
    uint32_t            number           = 0;
    uint8_t             u8number         = 0;
    oC_Time_t           timeout          = s(3);

    if(fileToSend != NULL)
    {
        FILE * fp = fopen(fileToSend,"r");

        if(fp != NULL)
        {
            uint32_t size = fsize(fp);
            if(size > 0)
            {
                fileBuffer = malloc(size,AllocationFlags_ZeroFill);

                if(fileBuffer != NULL)
                {
                    fread(fileBuffer,1,size,fp);
                    bufferToSend        = fileBuffer;
                    bufferToSendSize    = size;
                }
                else
                {
                    commandCorrect = false;
                    printf("Cannot allocate memory for a file '%s'\n", fileToSend);
                }
            }
            else
            {
                commandCorrect = false;
                printf("File '%s' is empty!\n", fileToSend);
            }
            fclose(fp);
        }
        else
        {
            commandCorrect = false;
            printf("File '%s' does not exist!\n", fileToSend);
        }
    }
    else if(numberToSend != NULL)
    {
        if(sscanf(numberToSend,"%lli",&number))
        {
            commandCorrect = false;
            printf("Cannot parse number '%s'\n", numberToSend);
        }
        else if(number > 0xFF)
        {
            commandCorrect = false;
            printf("Number cannot be bigger than 0xFF!\n");
        }
        else
        {
            u8number            = (uint8_t)number;
            bufferToSend        = &u8number;
            bufferToSendSize    = 1;
        }
    }
    else if(stringToSend != NULL)
    {
        bufferToSend        = stringToSend;
        bufferToSendSize    = strlen(bufferToSend) + 1;
    }
    else
    {
        commandCorrect = false;
    }

    if(timeoutString != NULL)
    {
        if(sscanf(timeoutString,"%f",&timeout) != 0)
        {
            commandCorrect = false;
            printf("Cannot parse timeout '%s'\n", timeoutString);
        }
    }

    if(commandCorrect)
    {
        printf("Sending a message...");

        printf("%R\n", oC_SPI_Write(Context,bufferToSend,&bufferToSendSize,timeout));

    }

    if(fileBuffer != NULL)
    {
        free(fileBuffer,0);
    }

    return commandCorrect;
}

//==========================================================================================================================================
/**
 * @brief receives a message via SPI
 */
//==========================================================================================================================================
static bool Read( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv )
{
    bool                commandCorrect  = true;
    char *              noBytesString   = Argc >= 2 ? Argv[1] : NULL;
    char *              fileNameString  = oC_GetArgumentAfter(Argc,Argv,"-f");
    char *              timeoutString   = oC_GetArgumentAfter(Argc,Argv,"-t");
    oC_MemorySize_t     noBytes         = 0;
    FILE *              filePointer     = NULL;
    oC_Time_t           timeout         = s(3);
    char *              bufferForData   = NULL;

    if(noBytesString == NULL)
    {
        commandCorrect = false;
    }
    else
    {
        if(sscanf(noBytesString,"%u",&noBytes))
        {
            commandCorrect = false;
            printf("Cannot parse number '%s'\n", noBytesString);
        }
        else if(noBytes == 0)
        {
            printf("noBytes cannot be 0!\n", noBytes);
            commandCorrect = false;
        }
    }

    if(timeoutString != NULL)
    {
        if(sscanf(timeoutString,"%f",&timeout))
        {
            commandCorrect = false;
        }
    }

    if(commandCorrect)
    {
        bufferForData = malloc(noBytes + 1,AllocationFlags_ZeroFill);

        if(bufferForData)
        {
            printf("Waiting for a message...");

            printf("%R\n", oC_SPI_Read(Context,bufferForData,&noBytes,timeout));

            if(fileNameString != NULL)
            {
                printf("Saving received data into the file '%s'...", fileNameString);

                filePointer = fopen(fileNameString,"w");
                if(filePointer)
                {
                    if(fwrite(bufferForData,1,noBytes,filePointer) == noBytes)
                    {
                        printf("Success\n");
                    }
                    else
                    {
                        printf("ERROR - not all bytes written\n");
                    }
                    fclose(filePointer);
                }
                else
                {
                    printf("ERROR - cannot create file\n");
                }
            }
            else
            {
                printf("Received data: (%s)\n", bufferForData);
                for(oC_MemorySize_t offset = 0; offset < noBytes; offset++)
                {
                    printf("0x%02X ", bufferForData[offset]);
                }
                printf("\n");
            }

            free(bufferForData,0);
        }
        else
        {
            printf("Cannot allocate memory for a data\n");
        }
    }

    return commandCorrect;
}

//==========================================================================================================================================
/**
 * @brief sends and receives a message via SPI
 */
//==========================================================================================================================================
static bool Tranceive( const oC_SPI_Config_t * Config , oC_SPI_Context_t Context , int Argc , char ** Argv )
{
    bool                commandCorrect   = true;
    char *              fileToSend       = oC_GetArgumentAfter(Argc,Argv,"-fi");
    char *              fileToReceive    = oC_GetArgumentAfter(Argc,Argv,"-fo");
    char *              numberToSend     = oC_GetArgumentAfter(Argc,Argv,"-n");
    char *              stringToSend     = oC_GetArgumentAfter(Argc,Argv,"-s");
    char *              timeoutString    = oC_GetArgumentAfter(Argc,Argv,"-t");
    void *              bufferToSend     = NULL;
    oC_MemorySize_t     bufferToSendSize = 0;
    char *              fileBuffer       = NULL;
    uint32_t            number           = 0;
    uint8_t             u8number         = 0;
    oC_Time_t           timeout          = s(5);
    char *              receiveBuffer    = NULL;
    FILE *              filePointer      = NULL;

    if(fileToSend != NULL)
    {
        FILE * fp = fopen(fileToSend,"r");

        if(fp != NULL)
        {
            uint32_t size = fsize(fp);
            if(size > 0)
            {
                fileBuffer = malloc(size,AllocationFlags_ZeroFill);

                if(fileBuffer != NULL)
                {
                    fread(fileBuffer,1,size,fp);
                    bufferToSend        = fileBuffer;
                    bufferToSendSize    = size;
                }
                else
                {
                    commandCorrect = false;
                    printf("Cannot allocate memory for a file '%s'\n", fileToSend);
                }
            }
            else
            {
                commandCorrect = false;
                printf("File '%s' is empty!\n", fileToSend);
            }
            fclose(fp);
        }
        else
        {
            commandCorrect = false;
            printf("File '%s' does not exist!\n", fileToSend);
        }
    }
    else if(numberToSend != NULL)
    {
        if(sscanf(numberToSend,"%lli",&number))
        {
            commandCorrect = false;
            printf("Cannot parse number '%s'\n", numberToSend);
        }
        else if(number > 0xFF)
        {
            commandCorrect = false;
            printf("Number cannot be bigger than 0xFF!\n");
        }
        else
        {
            u8number            = (uint8_t)number;
            bufferToSend        = &u8number;
            bufferToSendSize    = 1;
        }
    }
    else if(stringToSend != NULL)
    {
        bufferToSend        = stringToSend;
        bufferToSendSize    = strlen(bufferToSend) + 1;
    }
    else
    {
        commandCorrect = false;
    }

    if(timeoutString != NULL)
    {
        if(sscanf(timeoutString,"%f",&timeout) != 0)
        {
            commandCorrect = false;
            printf("Cannot parse timeout '%s'\n", timeoutString);
        }
    }

    if(commandCorrect)
    {
        receiveBuffer = malloc( bufferToSendSize + 1, AllocationFlags_ZeroFill );

        if(receiveBuffer != NULL)
        {
            printf("Tranceive message ... ");
            printf("%R\n", oC_SPI_Tranceive(Context,bufferToSend,receiveBuffer,bufferToSendSize,timeout));

            if(fileToReceive != NULL)
            {
                printf("Saving received data into the file '%s'...", fileToReceive);

                filePointer = fopen(fileToReceive,"w");
                if(filePointer)
                {
                    if(fwrite(receiveBuffer,1,bufferToSendSize,filePointer) == bufferToSendSize)
                    {
                        printf("Success\n");
                    }
                    else
                    {
                        printf("ERROR - not all bytes written\n");
                    }
                    fclose(filePointer);
                }
                else
                {
                    printf("ERROR - cannot create file\n");
                }
            }
            else
            {
                printf("Received data: (%s)\n", receiveBuffer);
                for(oC_MemorySize_t offset = 0; offset < bufferToSendSize; offset++)
                {
                    printf("0x%02X ", receiveBuffer[offset]);
                }
                printf("\n");
            }
        }
        else
        {
            printf("Cannot allocate memory for a data\n");
        }
    }

    if(fileBuffer != NULL)
    {
        free(fileBuffer,0);
    }

    if(receiveBuffer != NULL)
    {
        free(receiveBuffer,0);
    }

    return commandCorrect;
}


#endif /* oC_SPI_LLD_AVAILABLE */
