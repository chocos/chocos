/** ****************************************************************************************************************************************
 *
 * @file       cpu.c
 *
 * @brief      The file contains memory program source
 *
 * @author     Kamil Drobienko - (Created on: 2- 04 2016 21:18:12)
 *
 * @note       Copyright (C) 2016 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <stdbool.h>
#include <stddef.h>
#include <oc_gpio.h>
#include <string.h>
#include <oc_string.h>
#include <oc_stdio.h>
#include <oc_memman.h>
#include <oc_mem_lld.h>
#include <oc_sys_lld.h>
#include <oc_vt100.h>
#include <oc_tgui.h>
#include <oc_system.h>
#include <oc_list.h>
#include <oc_clock_lld.h>
#include <oc_thread.h>
#include <oc_ktime.h>
#include <oc_threadman.h>
#include <oc_process.h>

#define MAXIMUM_ALLOCATORS                  512

static const oC_TGUI_Column_t               HorizontalMargin    = 5;
static const oC_TGUI_Column_t               HorizontalGPIOMargin= 5;
static const oC_TGUI_Column_t               VerticalMargin      = 1;
static const oC_TGUI_Column_t               MainScreenWidth     = 80;
static const oC_TGUI_Line_t                 MainScreenHeight    = 24;
static const oC_TGUI_Column_t               CpuBoxWidth         = 35;
static const oC_TGUI_Line_t                 CpuBoxHeight        = 10;
static const oC_TGUI_Column_t               CpuTextWidth        = 17;
static const oC_TGUI_Column_t               CpuPropertyWidth    = 28;
static const oC_TGUI_Column_t               CpuPropertyHeight   = 5;
static const oC_TGUI_Column_t               GpioBoxWidth        = 35;
static const oC_TGUI_Line_t                 GpioBoxHeight       = 18;
static const oC_TGUI_Column_t               GpioTextWidth       = 22;
static const oC_TGUI_Column_t               GpioPropertyWidth   = 23;
static const oC_TGUI_Column_t               GpioPropertyHeight  = 8;
static const oC_TGUI_Column_t               MenuWidth           = 33;
static const oC_TGUI_Line_t                 MenuHeight          = 8;
static const oC_TGUI_Color_t                BackgroundColor     = oC_TGUI_Color_Black;

static       oC_Thread_t                    CpuUsageThread      = NULL;

static const oC_TGUI_BoxStyle_t             MainBoxStyle        = {
                .InsideStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background = oC_TGUI_Color_LightGray ,
                .InsideStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .TitleStyle.Foreground  = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background  = oC_TGUI_Color_LightGray ,
                .TitleStyle.TextStyle   = oC_TGUI_TextStyle_Bold ,
                .BorderStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background = oC_TGUI_Color_LightGray ,
                .BorderStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ShadowStyle.Background = oC_TGUI_Color_DarkGray ,
};
static const oC_TGUI_BoxStyle_t             CpuBoxStyle         = {
                .InsideStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background = oC_TGUI_Color_LightGray ,
                .InsideStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .TitleStyle.Foreground  = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background  = oC_TGUI_Color_LightGray ,
                .TitleStyle.TextStyle   = oC_TGUI_TextStyle_Bold ,
                .BorderStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background = oC_TGUI_Color_LightGray ,
                .BorderStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ShadowStyle.Background = oC_TGUI_Color_DarkGray ,
};
static const oC_TGUI_MenuStyle_t            MenuStyle           = {
                .ActiveEntry.Foreground     = oC_TGUI_Color_LightGray ,
                .ActiveEntry.Background     = oC_TGUI_Color_DarkGray ,
                .ActiveEntry.TextStyle      = oC_TGUI_TextStyle_Default ,
                .NotActiveEntry.Foreground  = oC_TGUI_Color_DarkGray ,
                .NotActiveEntry.Background  = oC_TGUI_Color_LightGray ,
                .NotActiveEntry.TextStyle   = oC_TGUI_TextStyle_Default ,
                .Border.Foreground          = oC_TGUI_Color_DarkGray ,
                .Border.Background          = oC_TGUI_Color_LightGray ,
                .Border.TextStyle           = oC_TGUI_TextStyle_Default ,
};
static const oC_TGUI_BoxStyle_t             GpioBoxStyle         = {
                .InsideStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background = oC_TGUI_Color_LightGray ,
                .InsideStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .TitleStyle.Foreground  = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background  = oC_TGUI_Color_LightGray ,
                .TitleStyle.TextStyle   = oC_TGUI_TextStyle_Bold ,
                .BorderStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background = oC_TGUI_Color_LightGray ,
                .BorderStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ShadowStyle.Background = oC_TGUI_Color_DarkGray ,
};
static const oC_TGUI_PropertyStyle_t        CpuPropertyStyle    = {
                .DescriptionStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .DescriptionStyle.Background = oC_TGUI_Color_LightGray ,
                .DescriptionStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ValueStyle.Foreground       = oC_TGUI_Color_Blue ,
                .ValueStyle.Background       = oC_TGUI_Color_LightGray ,
                .ValueStyle.TextStyle        = oC_TGUI_TextStyle_Bold
};
static const oC_TGUI_PropertyStyle_t        GpioPropertyStyle    = {
                .DescriptionStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .DescriptionStyle.Background = oC_TGUI_Color_LightGray ,
                .DescriptionStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ValueStyle.Foreground       = oC_TGUI_Color_Blue ,
                .ValueStyle.Background       = oC_TGUI_Color_LightGray ,
                .ValueStyle.TextStyle        = oC_TGUI_TextStyle_Bold
};
static const oC_TGUI_Style_t             LabelStyle        = {
                .Foreground                  = oC_TGUI_Color_DarkGray ,
                .Background                  = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_SelectionBoxStyle_t SelectionBoxStyle = {
                .Arrow.Foreground            = oC_TGUI_Color_DarkGray ,
                .Arrow.Background            = oC_TGUI_Color_LightGray ,
                .Active.Foreground           = oC_TGUI_Color_Yellow ,
                .Active.Background           = oC_TGUI_Color_DarkGray ,
                .NotActive.Foreground        = oC_TGUI_Color_DarkGray ,
                .NotActive.Background        = oC_TGUI_Color_LightGray ,
};
static const oC_TGUI_PushButtonStyle_t PushButtonStyle = {
                .ActiveBorder.DontDraw      = true ,
                .Active.Foreground          = oC_TGUI_Color_Yellow ,
                .Active.Background          = oC_TGUI_Color_DarkGray ,
                .NotActive.Foreground       = oC_TGUI_Color_White ,
                .NotActive.Background       = oC_TGUI_Color_DarkGray ,
};
static const oC_TGUI_ProgressBarStyle_t     ProgressBarStyle    = {
                .ActiveColor            = oC_TGUI_Color_Blue ,
                .NonActiveColor         = oC_TGUI_Color_LightBlue ,
                .BorderStyle.DontDraw   = true ,
};
static void ReadPinInformation( oC_Pin_t Pin);
static void ShowCPUMenu( oC_TGUI_Position_t TopLeft );

//==========================================================================================================================================
/**
 * Shows CPU Usage
 */
//==========================================================================================================================================
static void ShowCpuUsage( void * Parameter )
{
    while(1)
    {
        uint32_t           cpuLoadUint  = (uint32_t)(oC_ThreadMan_GetCurrentCpuLoad() * 100.0);
        oC_TGUI_Position_t position     = { .Line = 7 , .Column = 6 };
        oC_TGUI_DrawProgressBar(position,CpuBoxWidth - 2,cpuLoadUint,10000,&ProgressBarStyle);
        sleep(s(3));
    }
}

//==========================================================================================================================================
/**
 * @brief prints informations about CPU
 */
//==========================================================================================================================================
static void ShowCpuInformations( oC_TGUI_Position_t TopLeft )
{
    const char * machineProducentName   = oC_SYS_LLD_GetMachineProducentName();
    const char * machineFamilyName      = oC_SYS_LLD_GetMachineFamilyName();
    const char * machineName            = oC_SYS_LLD_GetMachineName();
    oC_Frequency_t clockFrequency       = oC_CLOCK_LLD_GetClockFrequency();
    oC_Frequency_t maxClockFrequency    = oC_CLOCK_LLD_GetMaximumClockFrequency();
    double         cpuLoad              = oC_ThreadMan_GetCpuLoad();

    oC_TGUI_Position_t           position       = TopLeft;
    oC_TGUI_Column_t             textColumn     = TopLeft.Column + 1;
    oC_TGUI_DrawPropertyConfig_t propertyConfig;
    oC_TGUI_Property_t           properties[]   = {
                    { .Description = "Producer name: "      , .Type = oC_TGUI_ValueType_ValueString , .Format = "%14s"     , .ValueString  = machineProducentName  } ,
                    { .Description = "Family name: "        , .Type = oC_TGUI_ValueType_ValueString , .Format = "%14s"     , .ValueString  = machineFamilyName     } ,
                    { .Description = "Machine name: "       , .Type = oC_TGUI_ValueType_ValueString , .Format = "%14s"     , .ValueString  = machineName           } ,
                    { .Description = "CPU usage: "          , .Type = oC_TGUI_ValueType_ValueDouble , .Format = "%13.1f%%" , .ValueDouble  = cpuLoad             }
    };

    oC_TGUI_Property_t           propertiesFreq[]   = {
                    { .Description = "Target Frequency: "   , .Type = oC_TGUI_ValueType_UINT        , .Format = "%12uHz", .ValueUINT    = clockFrequency        } ,
                    { .Description = "Max Target Freq.: "   , .Type = oC_TGUI_ValueType_UINT        , .Format = "%12uHz", .ValueUINT    = maxClockFrequency     },
    };

    oC_TGUI_DrawBox(" CPU " , NULL , position,CpuBoxWidth,CpuBoxHeight,&CpuBoxStyle);

    position.Column  = textColumn ;
    position.Line   += 1;

    propertyConfig.StartPosition        = position;
    propertyConfig.DescriptionWidth     = CpuTextWidth;
    propertyConfig.Height               = CpuPropertyHeight;
    propertyConfig.Width                = CpuPropertyWidth;
    propertyConfig.NumberOfProperties   = oC_ARRAY_SIZE(properties);
    propertyConfig.Properties           = properties;
    propertyConfig.Style                = &CpuPropertyStyle;

    oC_TGUI_DrawProperties(&propertyConfig);

    position.Line   += 5 ;
    position.Column  = textColumn;
    propertyConfig.StartPosition        = position;
    propertyConfig.NumberOfProperties   = oC_ARRAY_SIZE(propertiesFreq);
    propertyConfig.Properties           = propertiesFreq;
    propertyConfig.Height               = CpuPropertyHeight-3;

    oC_TGUI_DrawProperties(&propertyConfig);

    CpuUsageThread = oC_Thread_New(oC_Process_Priority_CoreSpaceProcess,0,NULL,"cpu-usage-print",ShowCpuUsage,NULL);

    oC_Thread_Run(CpuUsageThread);
}

//==========================================================================================================================================
/**
 * @brief prints informations about ROM
 */
//==========================================================================================================================================
static void ShowGPIOInformations( void * Parameter )
{
    oC_TGUI_Position_t  gpioPosition    = {0};
    oC_uint32_t         pinIndex        = 0;
    oC_String_t *       pinNameList     = NULL;
    oC_Pin_t          * pinNumberList   = NULL;

    pinNameList   = malloc_array(oC_String_t,oC_PinIndex_NumberOfElements,AllocationFlags_ZeroFill);
    pinNumberList = malloc_array(oC_Pin_t   ,oC_PinIndex_NumberOfElements,AllocationFlags_ZeroFill);

    if(pinNameList == NULL || pinNumberList == NULL)
    {
        oC_SaveError("cpuinfo: cannot show GPIO informations - " , oC_ErrorCode_AllocationError);
    }
    else
    {
        gpioPosition.Column                 = CpuBoxWidth + HorizontalGPIOMargin + 3;
        gpioPosition.Line                   = VerticalMargin + 2;

        oC_TGUI_DrawBox( " GPIO " , NULL , gpioPosition , GpioBoxWidth , GpioBoxHeight , &GpioBoxStyle );

        oC_Pin_ForeachDefined(PinName)
        {
            pinNameList[pinIndex]   = (oC_String_t)PinName->Name;
            pinNumberList[pinIndex] = PinName->Pin;
            pinIndex++;
        }
        pinIndex=0;
        oC_TGUI_ActiveObject_t      activeObjects[] = {
                        {
                            .Type                           = oC_TGUI_ActiveObjecType_SelectionBox ,
                            .Position                       = { .Column = gpioPosition.Column + 18 , .Line = gpioPosition.Line + 1 } ,
                            .LabelPosition                  = { .Column = gpioPosition.Column + 7  , .Line = gpioPosition.Line + 1 } ,
                            .Width                          = 8 ,
                            .LabelText                      = "Pin number:" ,
                            .LabelWidth                     = 10 ,
                            .LabelStyle                     = &LabelStyle ,
                            .SelectionBox.SelectedIndex     = &pinIndex ,
                            .SelectionBox.Options           = pinNameList ,
                            .SelectionBox.NumberOfOptions   = oC_PinIndex_NumberOfElements ,
                            .SelectionBox.Style             = &SelectionBoxStyle ,
                            .SelectionBox.OnChangeHandler   = NULL ,
                            .SelectionBox.OnChangeParameter = NULL ,
                        } ,
                        {
                            .Type                        = oC_TGUI_ActiveObjecType_PushButton ,
                            .Position                    = { .Column = gpioPosition.Column + 7 , .Line = gpioPosition.Line + 3} ,
                            .Width                       = 20 ,
                            .Height                      = 3 ,
                            .PushButton.Text             = "Show configuration" ,
                            .PushButton.PressHandler     = NULL,
                            .PushButton.PressParameter   = NULL,
                            .PushButton.Style            = &PushButtonStyle,
                        } ,
        };

        oC_TGUI_DrawActiveObjects(activeObjects,oC_ARRAY_SIZE(activeObjects));

        ReadPinInformation( pinNumberList[pinIndex] );

        free(pinNameList,AllocationFlags_CanWaitForever);
        free(pinNumberList,AllocationFlags_CanWaitForever);
    }
}

void ReadPinInformation( oC_Pin_t Pin )
{
    oC_TGUI_Position_t              position;
    oC_TGUI_DrawPropertyConfig_t    propertyConfig;

    position.Column  = CpuBoxWidth + HorizontalGPIOMargin + 4;
    position.Line    = VerticalMargin + 7;

    oC_GPIO_Current_t       current;
    oC_GPIO_Mode_t          mode;
    oC_GPIO_OutputCircuit_t outputCircuit;
    oC_GPIO_Pull_t          pull;
    oC_GPIO_Speed_t         speed;
    oC_GPIO_IntTrigger_t    intTrigger;
    bool                    used = false;

    if( oC_GPIO_IsPinDefined( Pin ) )
    {
        oC_GPIO_ReadCurrent(Pin,&current);
        oC_GPIO_ReadMode(Pin,&mode);
        oC_GPIO_ReadOutputCircuit(Pin,&outputCircuit);
        oC_GPIO_ReadPull(Pin, &pull);
        oC_GPIO_ReadSpeed(Pin,&speed);
        oC_GPIO_ReadInterruptTrigger(Pin,&intTrigger);
        oC_GPIO_CheckIsPinUsed(Pin,&used);

        const char * pinName;
        const char * currentName;
        const char * modeName;
        const char * outputCircuitName;
        const char * pullName;
        const char * speedName;
        const char * interruptTriggerName;

        pinName             = malloc(sizeof(pinName)            , 10);
        currentName         = malloc(sizeof(currentName)        , 10);
        modeName            = malloc(sizeof(modeName)           , 10);
        outputCircuitName   = malloc(sizeof(outputCircuitName)  , 10);
        pullName            = malloc(sizeof(pullName)           , 10);
        speedName           = malloc(sizeof(speedName)          , 10);
        interruptTriggerName= malloc(sizeof(interruptTriggerName) , 10);

        if(used == false)
        {
            pinName             = oC_GPIO_GetPinName( Pin );
            modeName            = "- n/a -";
            currentName         = "- n/a -";
            outputCircuitName   = "- n/a -";
            pullName            = "- n/a -";
            speedName           = "- n/a -";
            interruptTriggerName= "- n/a -";
        }
        else if( mode == oC_GPIO_Mode_Alternate )
        {
            pinName             = oC_GPIO_GetPinName( Pin );
            modeName            = oC_GPIO_GetModeNameFromEnum(mode);
            currentName         = "- alt -";
            outputCircuitName   = "- alt -";
            pullName            = "- alt -";
            speedName           = "- alt -";
            interruptTriggerName= "- alt -";
        }
        else
        {
            pinName             = oC_GPIO_GetPinName( Pin );
            modeName            = oC_GPIO_GetModeNameFromEnum(mode);
            currentName         = oC_GPIO_GetCurrentNameFromEnum(current);
            outputCircuitName   = oC_GPIO_GetOutputCircuitNameFromEnum(outputCircuit);
            pullName            = oC_GPIO_GetPullNameFromEnum(pull);
            speedName           = oC_GPIO_GetSpeedNameFromEnum(speed);
            interruptTriggerName= oC_GPIO_GetIntTriggerNameFromEnum(intTrigger);
        }

        oC_TGUI_Property_t           properties[]   = {
            { .Description = "GPIO pin number: "       , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = pinName                  },
            { .Description = "Usage: "                 , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = (used) ? "used" : "not used"},
            { .Description = "Mode type: "             , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = modeName                 },
            { .Description = "Current type: "          , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = currentName              },
            { .Description = "OutputCircuit type:"     , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = outputCircuitName        },
            { .Description = "Pull type: "             , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = pullName                 },
            { .Description = "Speed type: "            , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = speedName                },
            { .Description = "InterruptTrigger type: " , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = interruptTriggerName     },
        };

        propertyConfig.StartPosition        = position;
        propertyConfig.DescriptionWidth     = GpioTextWidth;
        propertyConfig.Height               = GpioPropertyHeight;
        propertyConfig.Width                = GpioPropertyWidth;
        propertyConfig.NumberOfProperties   = oC_ARRAY_SIZE(properties);
        propertyConfig.Properties           = properties;
        propertyConfig.Style                = &GpioPropertyStyle;

        oC_TGUI_DrawProperties( &propertyConfig );

        free( &currentName            , AllocationFlags_Default );
        free( &modeName               , AllocationFlags_Default );
        free( &outputCircuitName      , AllocationFlags_Default );
        free( &pullName               , AllocationFlags_Default );
        free( &speedName              , AllocationFlags_Default );
        free( &interruptTriggerName   , AllocationFlags_Default );
    }
    else
    {
        const char * pinName;
        const char * pinAvaibility = "Pin is not defined";
        pinName             = malloc(sizeof(pinName) , 5);
        pinName             = oC_GPIO_GetPinName( Pin );


        oC_TGUI_Property_t           properties2[]   = {
            { .Description = "GPIO pin : "    , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = pinName           },
            { .Description = " "              , .Type = oC_TGUI_ValueType_ValueString , .Format = "%10s" , .ValueString = pinAvaibility     },
        };

        propertyConfig.StartPosition        = position;
        propertyConfig.DescriptionWidth     = GpioTextWidth;
        propertyConfig.Height               = GpioPropertyHeight;
        propertyConfig.Width                = GpioPropertyWidth;
        propertyConfig.NumberOfProperties   = oC_ARRAY_SIZE(properties2);
        propertyConfig.Properties           = properties2;
        propertyConfig.Style                = &GpioPropertyStyle;

        oC_TGUI_DrawProperties(&propertyConfig);

        free(&pinName            , AllocationFlags_Default);
    }
}

//==========================================================================================================================================
/**
 * Prints the menu for the program
 *
 * @param TopLeft       Left top corner of the menu
 */
//==========================================================================================================================================
static void ShowCPUMenu( oC_TGUI_Position_t TopLeft )
{
    oC_TGUI_MenuEntry_t MenuEntries[] = {
        { .Title = "Show GPIO details", .Help = "Shows GPIO pin with configuration" , .Handler = ShowGPIOInformations , .Parameter = NULL } ,
        { .Title = "Exit"             , .Help = "Close the program"                 , .Handler = NULL  }
    };
    oC_TGUI_DrawMenu(TopLeft,MenuWidth,MenuHeight,MenuEntries,oC_ARRAY_SIZE(MenuEntries),&MenuStyle);
}

//==========================================================================================================================================
/**
 * @brief main memory function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_TGUI_Position_t position     = {0};
    oC_TGUI_Position_t cpuPosition  = {0};
    oC_TGUI_Position_t menuPosition = {0};

    // /////////////////////////////////////////////////////////////
    /*  Initialization of new screen */
    oC_TGUI_SaveAttributes();
    oC_TGUI_SetBackgroundColor(BackgroundColor);
    oC_TGUI_ClearScreen();

    position.Column = 1;
    position.Line   = 1;

    oC_TGUI_DrawBox("[CPU INFO]" , NULL , position,MainScreenWidth,MainScreenHeight,&MainBoxStyle);

    cpuPosition.Column = position.Column + HorizontalMargin;
    cpuPosition.Line   = position.Line   + VerticalMargin   + 1;

    ShowCpuInformations( cpuPosition );

    menuPosition.Column = cpuPosition.Column;
    menuPosition.Line   = cpuPosition.Line  + CpuBoxHeight + VerticalMargin;

    ShowCPUMenu( menuPosition );

    // /////////////////////////////////////////////////////////////
    /* Prepare old screen */
    oC_TGUI_RestoreAttributes();
    oC_TGUI_ClearScreen();
    position.Line   = 1;
    position.Column = 1;
    oC_TGUI_SetCursorPosition( position );

    oC_TGUI_ResetDevice();

    oC_ThreadMan_RemoveThread(CpuUsageThread);

    return 0;
}

