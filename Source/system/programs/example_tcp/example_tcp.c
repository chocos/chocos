/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the example_tcp program
 *
 * @author     Patryk Kubiak - (Created on: 2024-11-28 - 17:40:09) 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_tcp.h>

//! [read_port]
//=============================================================================
//                    == READ PORT ==
//
//          This function reads the port from the command arguments.
//
//=============================================================================
static bool ReadPort( int Argc , char** Argv , oC_Tcp_Port_t* Port )
{
    const char* portString = oC_GetArgumentAfter(Argc, Argv, "--port");
    if( portString == NULL )
    {
        printf("The port is not specified\n");
        return false;
    }

    // Read the port
    uint32_t port = 0;
    if( sscanf(portString, "%u", &port) != oC_ErrorCode_None )
    {
        printf("Cannot read the port: %s\n", portString);
        return false;
    }
    *Port = port;

    return true;
}

//=============================================================================
//                   == MAIN FUNCTION ==
//
//          This is the main function of the example_tcp program. 
//          The program shows how to use the TCP module.
//
//=============================================================================
int main( int Argc , char ** Argv )
{
	oC_Tcp_Port_t port = 0;
    if( !ReadPort(Argc, Argv, &port) )
    {
        return -1;
    }
    oC_Net_Address_t    source           = {
                    .Port       = port,
                    .Type       = oC_Net_AddressType_IPv4 ,
                    .Protocol   = oC_Net_Protocol_TCP ,
                    .IPv4       = 0 ,
    };
    oC_Tcp_Server_t server = NULL;
    uint32_t maxConnections = 5;
    oC_Time_t timeout = 1000;
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    static const oC_MemorySize_t bufferSize = 2000;
    char* buffer = malloc(bufferSize);

    if( !ErrorCode(oC_Tcp_Listen( &source , &server , maxConnections , timeout )) )
    {
        printf("Cannot start the server: %R\n", errorCode);
        return -1;
    }

    printf("The server is started at the port: %u\n", port);

    oC_Tcp_Connection_t connection = NULL;

    while( ErrorCode(oC_Tcp_Accept(server, &connection, day(365))) )
    {
        printf("New connection received: %s\n", oC_Tcp_Connection_GetName(connection));

        oC_MemorySize_t size = bufferSize;
        if( ErrorCode(oC_Tcp_Connection_Receive(connection, buffer, &size, s(3))) )
        {
            printf("Received data: %s\n", buffer);

            // Echo the data
            if( !ErrorCode(oC_Tcp_Connection_Send(connection, buffer, size, s(1))) )
            {
                printf("Cannot send the data: %R\n", errorCode);
            }
        }

        printf("Timeout - closing connection: ");
        errorCode = oC_Tcp_Connection_Disconnect(connection, s(3));
        printf("%R\n", errorCode);
    }

    printf("TCP Accept error: %R\n", errorCode);

    return 0;
}
