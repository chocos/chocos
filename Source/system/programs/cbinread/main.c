/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the cbinread program
 *
 * @author     Patryk Kubiak - (Created on: 2017-07-08 - 18:48:54) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_cbin.h>

//==========================================================================================================================================
/**
 * @brief prints usage message
 */
//==========================================================================================================================================
static void PrintUsage( char * ProgramName )
{
    printf("Usage: %s <file>\n", ProgramName);
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    void *                  fileBuffer    = NULL;
    oC_Program_Header_t *   programHeader = NULL;
    FILE *                  filePointer   = NULL;
    oC_MemorySize_t         fileSize      = 0;
    oC_Program_Sections_t * sections      = NULL;

    memset(&programHeader,0,sizeof(programHeader));

    if(Argc < 2)
    {
        PrintUsage(Argv[0]);
    }
    else
    {
        filePointer = fopen(Argv[1],"r");

        if(filePointer != NULL)
        {
            fileSize = fsize(filePointer);
            if(fileSize == 0)
            {
                printf("Error - file is empty!\n");
                return oC_ErrorCode_FileIsEmpty;
            }

            fileBuffer = malloc( fileSize , AllocationFlags_ZeroFill );
            if(fileBuffer == NULL)
            {
                printf("Cannot allocate %d bytes for the file content!\n", fileSize);
                return oC_ErrorCode_AllocationError;
            }

            programHeader = fileBuffer;

            fread(fileBuffer,1,fileSize, filePointer);

            sections = fileBuffer + (fileSize - programHeader->SectionsDefinitionSize);

            printf("Header size:            %d      \n"     , programHeader->HeaderSize);
            printf("Magic string:           %s      \n"     , programHeader->MagicString);
            printf("Version:                0x%08X (%s)\n"  , programHeader->Version   , programHeader->Version == oC_VERSION ? "current" : programHeader->Version > oC_VERSION ? "newer" : "depracated");
            printf("Machine Name:           %s      \n"     , programHeader->MachineName         );
            printf("System Calls:           0x%08X  \n"     , programHeader->SystemCalls         );
            printf("Number of system calls: %d      \n"     , programHeader->NumberOfSystemCalls );

            printf("\nSections (%d):\n", programHeader->SectionsDefinitionSize);

            printf(".text offset:           0x%08X  \n"     , sections->text.Offset );
            printf(".text size:             0x%08X  \n"     , sections->text.Size   );
            printf(".data offset:           0x%08X  \n"     , sections->data.Offset );
            printf(".data size:             0x%08X  \n"     , sections->data.Size   );
            printf(".bss offset:            0x%08X  \n"     , sections->bss.Offset );
            printf(".bss size:              0x%08X  \n"     , sections->bss.Size   );
            printf(".syscalls offset:       0x%08X  \n"     , sections->syscalls.Offset );
            printf(".syscalls size:         0x%08X  \n"     , sections->syscalls.Size   );

            printf(" Priority                  0x%08X \n" , programHeader->ProgramRegistration.Priority);
            printf(" Name                      %s     \n" , programHeader->ProgramRegistration.Name);
            printf(" MainFunction              0x%08X \n" , programHeader->ProgramRegistration.MainFunction);
            printf(" StdInName                 0x%08X \n" , programHeader->ProgramRegistration.StdInName);
            printf(" StdOutName                0x%08X \n" , programHeader->ProgramRegistration.StdOutName);
            printf(" StdErrName                0x%08X \n" , programHeader->ProgramRegistration.StdErrName);
            printf(" HeapMapSize               0x%08X \n" , programHeader->ProgramRegistration.HeapMapSize);
            printf(" ThreadStackSize           0x%08X \n" , programHeader->ProgramRegistration.ThreadStackSize);
            printf(" AllocationLimit           0x%08X \n" , programHeader->ProgramRegistration.AllocationLimit);
            printf(" TrackAllocations          0x%08X \n" , programHeader->ProgramRegistration.TrackAllocations);
            printf(" ArgumentsFormatFilePath   0x%08X \n" , programHeader->ProgramRegistration.ArgumentsFormatFilePath);

            fclose(filePointer);
        }
        else
        {
            printf("Cannot open file '%s'\n", Argv[1]);
        }
    }

    return 0;
}
