/** ****************************************************************************************************************************************
 *
 * @file       memory.c
 *
 * @brief      The file contains memory program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_memman.h>
#include <oc_mem_lld.h>
#include <oc_sys_lld.h>
#include <oc_vt100.h>
#include <oc_tgui.h>
#include <oc_system.h>
#include <oc_list.h>

#define MAXIMUM_ALLOCATORS                  512

static const oC_TGUI_Column_t               HorizontalMargin    = 5;
static const oC_TGUI_Column_t               VerticalMargin      = 1;
static const oC_TGUI_Column_t               MainScreenWidth     = 80;
static const oC_TGUI_Line_t                 MainScreenHeight    = 24;
static const oC_TGUI_Column_t               RomBoxWidth         = 33;
static const oC_TGUI_Line_t                 RomBoxHeight        = 10;
static const oC_TGUI_Column_t               RomTextWidth        = 17;
static const oC_TGUI_Column_t               RomPropertyWidth    = 29;
static const oC_TGUI_Column_t               RomPropertyHeight   = 5;
static const oC_TGUI_Column_t               RomProgressBarWidth = 28;
static const oC_TGUI_Column_t               RamBoxWidth         = 33;
static const oC_TGUI_Line_t                 RamBoxHeight        = 18;
static const oC_TGUI_Column_t               RamTextWidth        = 17;
static const oC_TGUI_Column_t               RamPropertyWidth    = 29;
static const oC_TGUI_Column_t               RamPropertyHeight   = 13;
static const oC_TGUI_Column_t               RamProgressBarWidth = 28;
static const oC_TGUI_Column_t               MenuWidth           = 33;
static const oC_TGUI_Line_t                 MenuHeight          = 8;
static const oC_TGUI_Color_t                BackgroundColor     = oC_TGUI_Color_Black;
static const oC_TGUI_ProgressBarStyle_t     ProgressBarStyle    = {
                .ActiveColor            = oC_TGUI_Color_Blue ,
                .NonActiveColor         = oC_TGUI_Color_LightBlue ,
                .BorderStyle.DontDraw   = true ,
};
static const oC_TGUI_BoxStyle_t             MainBoxStyle        = {
                .InsideStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background = oC_TGUI_Color_LightGray ,
                .InsideStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .TitleStyle.Foreground  = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background  = oC_TGUI_Color_LightGray ,
                .TitleStyle.TextStyle   = oC_TGUI_TextStyle_Bold ,
                .BorderStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background = oC_TGUI_Color_LightGray ,
                .BorderStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ShadowStyle.Background = oC_TGUI_Color_DarkGray ,
};
static const oC_TGUI_BoxStyle_t             RomBoxStyle         = {
                .InsideStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background = oC_TGUI_Color_LightGray ,
                .InsideStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .TitleStyle.Foreground  = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background  = oC_TGUI_Color_LightGray ,
                .TitleStyle.TextStyle   = oC_TGUI_TextStyle_Bold ,
                .BorderStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background = oC_TGUI_Color_LightGray ,
                .BorderStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ShadowStyle.Background = oC_TGUI_Color_DarkGray ,
};
static const oC_TGUI_BoxStyle_t             RamBoxStyle         = {
                .InsideStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .InsideStyle.Background = oC_TGUI_Color_LightGray ,
                .InsideStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .TitleStyle.Foreground  = oC_TGUI_Color_DarkGray ,
                .TitleStyle.Background  = oC_TGUI_Color_LightGray ,
                .TitleStyle.TextStyle   = oC_TGUI_TextStyle_Bold ,
                .BorderStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .BorderStyle.Background = oC_TGUI_Color_LightGray ,
                .BorderStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ShadowStyle.Background = oC_TGUI_Color_DarkGray ,
};
static const oC_TGUI_MenuStyle_t            MenuStyle           = {
                .ActiveEntry.Foreground     = oC_TGUI_Color_LightGray ,
                .ActiveEntry.Background     = oC_TGUI_Color_DarkGray ,
                .ActiveEntry.TextStyle      = oC_TGUI_TextStyle_Default ,
                .NotActiveEntry.Foreground  = oC_TGUI_Color_DarkGray ,
                .NotActiveEntry.Background  = oC_TGUI_Color_LightGray ,
                .NotActiveEntry.TextStyle   = oC_TGUI_TextStyle_Default ,
                .Border.Foreground          = oC_TGUI_Color_DarkGray ,
                .Border.Background          = oC_TGUI_Color_LightGray ,
                .Border.TextStyle           = oC_TGUI_TextStyle_Default ,
};
static const oC_TGUI_PropertyStyle_t        RomPropertyStyle    = {
                .DescriptionStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .DescriptionStyle.Background = oC_TGUI_Color_LightGray ,
                .DescriptionStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ValueStyle.Foreground       = oC_TGUI_Color_Blue ,
                .ValueStyle.Background       = oC_TGUI_Color_LightGray ,
                .ValueStyle.TextStyle        = oC_TGUI_TextStyle_Bold
};
static const oC_TGUI_PropertyStyle_t        RamPropertyStyle    = {
                .DescriptionStyle.Foreground = oC_TGUI_Color_DarkGray ,
                .DescriptionStyle.Background = oC_TGUI_Color_LightGray ,
                .DescriptionStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .ValueStyle.Foreground       = oC_TGUI_Color_Blue ,
                .ValueStyle.Background       = oC_TGUI_Color_LightGray ,
                .ValueStyle.TextStyle        = oC_TGUI_TextStyle_Bold
};
static const oC_TGUI_MenuStyle_t            AllocationsMenuStyle = {
                .ActiveEntry.Foreground     = oC_TGUI_Color_LightGray ,
                .ActiveEntry.Background     = oC_TGUI_Color_DarkGray ,
                .ActiveEntry.TextStyle      = oC_TGUI_TextStyle_Default ,
                .NotActiveEntry.Foreground  = oC_TGUI_Color_DarkGray ,
                .NotActiveEntry.Background  = oC_TGUI_Color_LightGray ,
                .NotActiveEntry.TextStyle   = oC_TGUI_TextStyle_Default ,
                .Border.Foreground          = oC_TGUI_Color_DarkGray ,
                .Border.Background          = oC_TGUI_Color_LightGray ,
                .Border.TextStyle           = oC_TGUI_TextStyle_Default ,
};
//==========================================================================================================================================
/**
 * @brief prints informations about ROM
 */
//==========================================================================================================================================
static void ShowRomInformations( oC_TGUI_Position_t TopLeft )
{
    oC_UInt_t                    freeRomSize    = oC_MemMan_GetFreeFlashSize();
    oC_UInt_t                    romSize        = oC_MemMan_GetFlashSize();
    oC_TGUI_Position_t     position       = TopLeft;
    oC_TGUI_Column_t             textColumn     = TopLeft.Column + 1;
    oC_TGUI_DrawPropertyConfig_t propertyConfig;
    oC_TGUI_Property_t           properties[]   = {
                    { .Description = "ROM Size: "      , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = romSize } ,
                    { .Description = "Used ROM Size: " , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = romSize - freeRomSize } ,
                    { .Description = "Free ROM Size: " , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = freeRomSize } ,
    };

    oC_TGUI_DrawBox(" ROM " , NULL , position,RomBoxWidth,RomBoxHeight,&RomBoxStyle);

    position.Column  = textColumn ;
    position.Line   += 1;

    oC_TGUI_DrawProgressBar(position,RomProgressBarWidth,romSize - freeRomSize,romSize,&ProgressBarStyle);

    position.Column  = textColumn ;
    position.Line   += 2 ;

    propertyConfig.StartPosition        = position;
    propertyConfig.DescriptionWidth     = RomTextWidth;
    propertyConfig.Height               = RomPropertyHeight;
    propertyConfig.Width                = RomPropertyWidth;
    propertyConfig.NumberOfProperties   = oC_ARRAY_SIZE(properties);
    propertyConfig.Properties           = properties;
    propertyConfig.Style                = &RomPropertyStyle;

    oC_TGUI_DrawProperties(&propertyConfig);
}

//==========================================================================================================================================
/**
 * @brief prints informations about ROM
 */
//==========================================================================================================================================
static void ShowRamInformations( oC_TGUI_Position_t TopLeft )
{
    oC_UInt_t ramSize                 = oC_MemMan_GetRamSize();
    oC_UInt_t freeRamSize             = oC_MemMan_GetFreeRamSize();
    oC_UInt_t dataSize                = oC_MEM_LLD_GetDataSize() + oC_MEM_LLD_GetBssSize();
    oC_UInt_t mainHeapSize            = oC_MEM_LLD_GetHeapSize();
    oC_UInt_t mainStackSize           = oC_MEM_LLD_GetStackSize();
    oC_UInt_t alignmentSize           = oC_MEM_LLD_GetAlignmentSize();
    oC_UInt_t sysStackSize            = oC_SYS_LLD_GetContextStackSize(oC_SYS_LLD_GetSystemContext());
    oC_UInt_t externalHeapSize        = oC_MemMan_GetExternalHeapSize();
    oC_UInt_t dmaRamSize              = oC_MemMan_GetDmaRamHeapSize();
    oC_TGUI_Position_t position = TopLeft;
    oC_TGUI_Column_t        textColumn= TopLeft.Column + 1;
    oC_TGUI_DrawPropertyConfig_t propertyConfig;
    oC_TGUI_Property_t           properties[]   = {
        { .Description = "RAM Size: "      , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = ramSize                  } ,
        { .Description = "Used RAM Size: " , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = ramSize - freeRamSize    } ,
        { .Description = "Free RAM Size: " , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = freeRamSize              } ,
        { .Description = "Data section: "  , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = dataSize                 } ,
        { .Description = "Main heap: "     , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = mainHeapSize             } ,
        { .Description = "Main stack: "    , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = mainStackSize            } ,
        { .Description = "System stack: "  , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = sysStackSize             } ,
        { .Description = "External heap: " , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = externalHeapSize         } ,
        { .Description = "DMA RAM Size: "  , .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = dmaRamSize               } ,
        { .Description = "Heap alignment: ", .Type = oC_TGUI_ValueType_UINT , .Format = "%10uB" , .ValueUINT = alignmentSize            } ,
    };

    oC_TGUI_DrawBox(" RAM " , NULL , position,RamBoxWidth,RamBoxHeight,&RamBoxStyle);

    position.Column  = textColumn ;
    position.Line   += 1;

    oC_TGUI_DrawProgressBar(position,RamProgressBarWidth,ramSize - freeRamSize,ramSize,&ProgressBarStyle);

    position.Column  = textColumn ;
    position.Line   += 2 ;

    propertyConfig.StartPosition        = position;
    propertyConfig.DescriptionWidth     = RamTextWidth;
    propertyConfig.Height               = RamPropertyHeight;
    propertyConfig.Width                = RamPropertyWidth;
    propertyConfig.NumberOfProperties   = oC_ARRAY_SIZE(properties);
    propertyConfig.Properties           = properties;
    propertyConfig.Style                = &RamPropertyStyle;

    oC_TGUI_DrawProperties(&propertyConfig);
}

//==========================================================================================================================================
/**
 * Function called by the TGUI for drawing list element
 */
//==========================================================================================================================================
static void DrawAllocation( oC_TGUI_Position_t Position , oC_List_ElementHandle_t ListElement , oC_TGUI_Column_t Width )
{
    if(isaddresscorrect(ListElement))
    {
        oC_MemMan_AllocatorsStats_t * stats = oC_List_ElementHandle_Value(ListElement,oC_MemMan_AllocatorsStats_t*);
        if(isaddresscorrect(stats))
        {
            oC_TGUI_DrawAtPositionWithSize(Position,stats->Allocator->Name,RamTextWidth);
            Position.Column += RamTextWidth;
            oC_TGUI_DrawFormatAtPosition(Position,"%10uB" , stats->Size);
        }
        else
        {
            oC_TGUI_DrawAtPosition(Position,"Invalid pointer");
        }
    }
    else
    {
        oC_TGUI_DrawAtPosition(Position,"Invalid list handle");
    }
}

//==========================================================================================================================================
/**
 * Function called by the TGUI - shows memory allocations screen
 */
//==========================================================================================================================================
static void ShowAllocations( void * Parameter )
{
    oC_List(oC_MemMan_AllocatorsStats_t*) allocations = oC_List_New(getcurallocator(),0);

    if(allocations)
    {
        oC_MemMan_AllocatorsStats_t* stats           = NULL;
        oC_UInt_t                    size            = MAXIMUM_ALLOCATORS;
        oC_UInt_t                    numberOfElements= size;
        oC_ErrorCode_t               errorCode       = oC_ErrorCode_OutputArrayToSmall;
        oC_TGUI_Position_t *   topLeft        = Parameter;
        oC_TGUI_Position_t     position       = *topLeft;
        oC_TGUI_DrawListMenuConfig_t drawListConfig = {
                        .TopLeft                = position ,
                        .Width                  = MenuWidth ,
                        .Height                 = MenuHeight ,
                        .List                   = (oC_List_t)allocations ,
                        .DrawHandler            = DrawAllocation ,
                        .SelectHandler          = NULL ,
                        .SelectHandlerParameter = NULL ,
                        .Style                  = &AllocationsMenuStyle ,
        };

        while(errorCode == oC_ErrorCode_OutputArrayToSmall)
        {
            numberOfElements = size;
            stats = smartalloc(sizeof(oC_MemMan_AllocatorsStats_t) * size , AllocationFlags_ZeroFill);

            if(stats != NULL)
            {
                errorCode = oC_MemMan_ReadAllocatorsStats(stats,&numberOfElements);

                if(errorCode == oC_ErrorCode_OutputArrayToSmall)
                {
                    smartfree(stats,sizeof(oC_MemMan_AllocatorsStats_t) * size ,AllocationFlags_Default);
                    size *= 2;
                }
            }
            else
            {
                errorCode = oC_ErrorCode_AllocationError;
            }
        }

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError("Memory:ShowAllocations - error while read allocators stats: " , errorCode);
        }
        else
        {
            oC_MemMan_AllocatorsStats_t *stat = stats;

            for(oC_UInt_t index = 0 ; index < numberOfElements ; index++)
            {
                stat = &stats[index];
                oC_List_PushBack(allocations,stat,getcurallocator());
            }

            oC_TGUI_DrawListMenu(&drawListConfig);
        }

        smartfree(stats,sizeof(oC_MemMan_AllocatorsStats_t) * size ,AllocationFlags_Default);

        oC_SaveIfFalse("Memory: Cannot delete allocation list" , oC_List_Delete(allocations,0),oC_ErrorCode_ReleaseError);
    }
}

//==========================================================================================================================================
/**
 * Prints the menu for the program
 *
 * @param TopLeft       Left top corner of the menu
 */
//==========================================================================================================================================
static void ShowMenu( oC_TGUI_Position_t TopLeft )
{
    oC_TGUI_MenuEntry_t MenuEntries[] = {
            { .Title = "Show allocations" , .Help = "Shows list of memory allocations with size and name" , .Handler = ShowAllocations , .Parameter = &TopLeft } ,
            { .Title = "Exit"             , .Help = "Close the program"                                   , .Handler = NULL  }
    };
    oC_TGUI_DrawMenu(TopLeft,MenuWidth,MenuHeight,MenuEntries,oC_ARRAY_SIZE(MenuEntries),&MenuStyle);
}

//==========================================================================================================================================
/**
 * @brief main memory function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_TGUI_Position_t position    = {0};
    oC_TGUI_Position_t romPosition = {0};
    oC_TGUI_Position_t ramPosition = {0};
    oC_TGUI_Position_t menuPosition= {0};

    // /////////////////////////////////////////////////////////////
    /*  Initialization of new screen */
    oC_TGUI_ResetDevice();
    oC_TGUI_SetBackgroundColor(BackgroundColor);
    oC_TGUI_ClearScreen();

    position.Column = 1;
    position.Line   = 1;

    oC_TGUI_DrawBox("[MEMORY INFO]" , NULL , position,MainScreenWidth,MainScreenHeight,&MainBoxStyle);

    romPosition.Column = position.Column + HorizontalMargin;
    romPosition.Line   = position.Line   + VerticalMargin   + 1;

    ShowRomInformations( romPosition );

    ramPosition.Column  = romPosition.Column + RomBoxWidth + HorizontalMargin;
    ramPosition.Line    = romPosition.Line;

    ShowRamInformations( ramPosition );

    menuPosition.Column = romPosition.Column;
    menuPosition.Line   = romPosition.Line  + RomBoxHeight + VerticalMargin;

    ShowMenu(menuPosition);

    oC_TGUI_ResetDevice();
    return 0;
}

