/** ****************************************************************************************************************************************
 *
 * @file       printf.c
 *
 * @brief      The file contains printf program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vt100.h>
#include <oc_kprint.h>
#include <oc_ktime.h>
#include <oc_tgui.h>

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_TGUI_Key_t key = 0;

    printf( oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_FG_RED );

    while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC )
    {
        oC_Time_t           time    = oC_KTime_GetTimestamp();
        oC_Time_Divided_t   divided;

        oC_Time_Divide(time,&divided);

        printf(oC_VT100_ERASE_LINE "Current time: %03d d %02d:%02d:%02d.%03d\r" , divided.Days, divided.Hours, divided.Minutes, divided.Seconds, divided.Miliseconds);
    }

    printf("\n");

    return 0;
}

