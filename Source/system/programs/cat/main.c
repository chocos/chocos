/** ****************************************************************************************************************************************
 *
 * @file       echo.c
 *
 * @brief      The file contains echo program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_string.h>

static void PrintUsage(char * ProgramName)
{
    printf("Usage: %s [-x] file_path\n" , ProgramName);
}

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    if(Argc >= 2)
    {
        FILE * file = fopen(Argv[Argc - 1],"r");

        if(file == NULL)
        {
            printf("Cannot open file %s!\n\r" , Argv[Argc - 1]);
        }
        else
        {
            char line[101];

            while(feof(file) == 0)
            {
                memset(line,0,sizeof(line));

                size_t readBytes = fread(line,1,100,file);

                if(readBytes == 0)
                {
                    break;
                }
                else
                {
                    if(oC_ArgumentOccur(Argc,Argv,"-x"))
                    {
                        for(size_t i = 0; i < readBytes; i++)
                        {
                            printf("0x%02X ", line[i]);
                        }
                    }
                    else
                    {
                        printf("%s" , line);
                    }
                }
            }

            fclose(file);
        }
    }
    else
    {
        PrintUsage(Argv[0]);
    }
    printf("\n\r");
    return 0;
}

