#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/07
#   Description:    Build script for building programs
#================================================================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR! ($0)                                                 "
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

ArchitectureNotSelected()
{
    SetForegroundColor "red"
    echo "=========================================================================="
    echo "=	                                                                    "
    echo "= 	ERROR! ($0)                                                         "
    echo "= 			Target architecture is not selected                 "
    echo "= 			run select_arch.bat first                           "
    echo "=                                                                         "
    echo "=========================================================================="
    ResetAllAttributes
    exit 1
}

ProgramNotFound()
{
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=     ERROR!                                                              "
    echo "=     Program not found at $PROGRAM_PATH                                  "
    echo "=                                                                         "
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=	List of available program dirs:                                     "
    echo "=                                                                         "
    for PROGRAM_PATH in `ls -d $SYSTEM_PROGRAMS_DIR/*/`;
    do 
        PROGRAM_DIR_NAME=`basename $PROGRAM_PATH`
        echo "=         $PROGRAM_DIR_NAME"
    done
    exit 1
}

Usage()
{
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=      Usage:                                                             "
    echo "=         $0 [PROGRAM_DIR_NAME] [TARGET]                                  "
    echo "=				                                            "
    echo "=			where TARGET is:                                    "
    echo "= 					all                                 "
    echo "= 					clean                               "
    echo "=========================================================================="
    exit 1
}

CompileProgram()
{
    . $BUILD_PROGRAM_FILE_PATH $1 $2 $3
}

#=====================================================================================================
#	HELPER FUNCTIONS
#=====================================================================================================
IsMachineSelected()
{
    if [ "$MACHINE_SELECTED" = "true" ]; then
        return 0;
    else
        return 1;
    fi
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR"         = "" ]; then PROJECT_DIR=$PWD/../../..;                    fi
if [ "$SET_PATHS_FILE_PATH" = "" ]; then SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh;fi
if [ "$CONSOLE_FILE_PATH"   = "" ]; then CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh;    fi

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#    ARCHITECTURE SELECTED VERIFICATION
#=====================================================================================================
if ! IsMachineSelected ; then ArchitectureNotSelected; fi

#=====================================================================================================
#   ARGUMENTS PREPARATION
#=====================================================================================================
TARGET="all"
PROGRAM_TYPE="system"
PROGRAM_DIR_NAME=$1
PROGRAM_PATH=$SOURCE_DIR/$PROGRAM_TYPE/programs/$PROGRAM_DIR_NAME

#=====================================================================================================
#   ARGUMENTS VERIFICATION
#=====================================================================================================
if [ ! -e $PROGRAM_PATH ]; then ProgramNotFound; fi
if [ "$1" = "" ]; then Usage; fi
if [ ! "$2" = "" ]; then
    if   [ "$2" = "all"   ]; then TARGET="all"; 
    elif [ "$2" = "clean" ]; then TARGET="clean"; 
    else 
        Usage
    fi
fi

#=====================================================================================================
#   ADDITIONAL PATHS VERIFICATION
#=====================================================================================================
if [ ! -e $BUILD_PROGRAM_FILE_PATH ]; then PathDoesNotExist $BUILD_PROGRAM_FILE_PATH; fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $BUILD_PROGRAM_FILE_PATH

#=====================================================================================================
#   COMPILE A PROGRAM
#=====================================================================================================
CompileProgram $PROGRAM_TYPE $PROGRAM_DIR_NAME $TARGET
