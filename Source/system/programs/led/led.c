/** ****************************************************************************************************************************************
 *
 * @file       led.c
 *
 * @brief      The file contains led program source
 *
 * @author     Patryk Kubiak
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_led.h>
#include <oc_clock_lld.h>
#include <oc_threadman.h>
#include <oc_processman.h>
#include <string.h>

//==========================================================================================================================================
/**
 * @brief prints usage syntax of this command
 */
//==========================================================================================================================================
static void PrintUsage( void )
{
    const char * programName = oC_Process_GetName(oC_ProcessMan_GetCurrentProcess());
    printf(
            "Usage: %s color period units repeats\n\r"
            "Example: %s 0xff0000 500 ms 10\n\r"
                    , programName , programName );
}

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_CommandNotCorrect;

    if(Argc == 5)
    {
        oC_Thread_t     currentThread = oC_ThreadMan_GetCurrentThread();
        oC_LED_Config_t Config;
        oC_LED_Context_t Context;

        Config.Pins[oC_LED_Index_Red]   = oC_Pin_RED_LED;
        Config.Pins[oC_LED_Index_Blue]  = oC_Pin_BLUE_LED;
        Config.Pins[oC_LED_Index_Green] = oC_Pin_GREEN_LED;

        Config.Mode = oC_LED_Mode_RGB;

        if(oC_AssignErrorCode(&errorCode , oC_LED_Configure(&Config,&Context)))
        {
            unsigned int repeats   = 0;
            unsigned int timeInt   = 0;
            oC_Time_t    time      = 0;
            unsigned int color     = 0;

            sscanf(Argv[2],"%u" , &timeInt );
            sscanf(Argv[4],"%u" , &repeats);
            sscanf(Argv[1],"%x" , &color);

            if(strcmp(Argv[3],"s")==0)
            {
                time = s(timeInt);
            }
            else if(strcmp(Argv[3],"ms")==0)
            {
                time = ms(timeInt);
            }
            else if(strcmp(Argv[3],"min")==0)
            {
                time = min(timeInt);
            }
            else
            {
                printf("units = %s\n\r" , Argv[3]);
                errorCode = oC_ErrorCode_WrongNumberFormat;
            }

            if(time > 0)
            {
                errorCode = oC_ErrorCode_None;

                oC_Color_t currentColor = (oC_Color_t)color;

                while(repeats > 0 && !oC_ErrorOccur(errorCode))
                {
                    oC_AssignErrorCode(&errorCode , oC_LED_SetColor(Context,currentColor));
                    oC_Thread_Sleep(currentThread,time);
                    currentColor ^= (oC_Color_t)color;
                    repeats--;
                }
            }

            oC_AssignErrorCode(&errorCode , oC_LED_Unconfigure(&Config,&Context));

        }
        if(oC_ErrorOccur(errorCode))
        {
            oC_PrintErrorMessage("Error: " , errorCode);
        }
    }
    else
    {
        PrintUsage();

        errorCode = oC_ErrorCode_CommandNotCorrect;
    }

    return errorCode;
}

