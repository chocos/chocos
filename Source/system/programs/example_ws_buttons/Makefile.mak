############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2024-11-27 - 20:31:24
##  Description:    makefile for ws_buttons.c program
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
PROGRAM_NAME            = example_ws_buttons
SPACE                   = CORE_SPACE
OPTIMALIZE              = O0
WARNING_FLAGS           = -Wall
CSTD                    = c99
DEFINITIONS             =
SOURCE_FILES            = example_ws_buttons.c
INCLUDES_DIRS           = 
STANDARD_INPUT          = uart_stdio
STANDARD_OUTPUT         = uart_stdio
STANDARD_ERROR          = uart_stdio
HEAP_MAP_SIZE			= 0
PROCESS_STACK_SIZE      = 1024
ALLOCATION_LIMIT        = 0
TRACK_ALLOCATION        = FALSE
FILE_TYPE               = CBIN

##============================================================================================================================
##                                          
##              INCLUDE MAIN MAKEFILE               
##                                          
##============================================================================================================================
include $(PROGRAM_MK_FILE_PATH)
