/** ****************************************************************************************************************************************
 *
 * @file       reboot.c
 *
 * @brief      The file contains reboot program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vt100.h>
#include <oc_boot.h>
#include <oc_processman.h>
#include <oc_userman.h>

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_User_t user = oC_Process_GetUser(oC_ProcessMan_GetCurrentProcess());
    printf(
            oC_VT100_RESET_ALL_ATTRIBUTES oC_VT100_BRIGHT oC_VT100_BLINK oC_VT100_FG_RED
            "Restarting of the system by user...%s\n\r" , oC_User_GetName(user));
    oC_Boot_Restart( oC_Boot_Reason_UserRequest , user );
    return 0;
}

