/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the example_allocator program
 *
 * @author     Patryk Kubiak - (Created on: 2023-11-20 - 19:56:34)
 *
 * @note       Copyright (C) 2023 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <stdint.h>
#include <oc_stdio.h>
#include <oc_vt100.h>
#include <oc_memman.h>
#include <oc_math.h>
#include <oc_debug.h>

//==========================================================================================================================================
/**
 * @brief Callback called whenever the memory-event has occurred
 *
 * The function that handles memory events. It is for allocator events and module events. Note, that
 * this function is like interrupt, and there cannot be any 'thread wait' operations, because none of threads is active while execution of it.
 * Moreover, the system is stopped for handling this operations, so this should be as short as possible.
 *
 * @param Address           Address of event. It can be NULL, when the address is not used in this event.
 * @param Event             Flags of the event, that already occurs. It can be set to more than one flag.
 * @param LineNumber        Number of line where the allocation was called. It can be set to 0, if it is not used.
 */
//==========================================================================================================================================
static void MemoryEventHandler( void * Address , MemoryEventFlags_t Event, const char * Function, uint32_t LineNumber )
{
    if (Event & MemoryEventFlags_BufferOverflow)
    {
        kdebuglog(oC_LogType_Error, "BufferOverflow for address %p\n", Address);
    }
    if (Event & MemoryEventFlags_MemoryExhausted)
    {
        kdebuglog(oC_LogType_Error, "Memory exhausted\n");
    }
}

//==========================================================================================================================================
/**
 * @brief Example allocator
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
              .Name         = "example_allocator_app",              /**< Printable name of the allocator (for printing to user) */
              .EventHandler = MemoryEventHandler,                   /**< Function, that should be called, when some event occurs. It can be set to NULL if not used */
              .EventFlags   = MemoryEventFlags_BufferOverflow
                            | MemoryEventFlags_MemoryExhausted,     /**< List of events, that should be turned on for this allocator. Flags are joined with 'OR' operation. */
              .Limit        = kB(1)                                 /**< Limit of size of memory allocations - set it to 0 if not used */
};

//==========================================================================================================================================
/**
 * @brief prints help of this program
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 */
//==========================================================================================================================================
static void PrintHelp()
{
    printf("Example program showing usage of the Allocators\n\n");
    printf("Syntax example_allocator <command>\n\n");
    printf("          allocate <size>   - allocates a block of memory with the given size, and prints an address.\n"
           "                              The <size> should be unsigned integer smaller than %u\n\n", Allocator.Limit);
    printf("          release <address> - releases memory allocated at the given address (or all if the <address> is not given)\n");

}

//==========================================================================================================================================
/**
 * Reads address from the argument
 */
//==========================================================================================================================================
static void* GetAddress( const char* Arg )
{
    void* address = NULL;
    if ( !oC_VerifyStdioResult( sscanf(Arg, "%p", &address) ) || address == NULL )
    {
        PrintHelp();
        return NULL;
    }
    if (oC_MemMan_GetAllocatorOfAddress(address) != &Allocator)
    {
        printf("Address %p was not allocated by this program\n", address);
        return NULL;
    }
    return address;
}

//==========================================================================================================================================
/**
 * Reads size from the argument
 */
//==========================================================================================================================================
static oC_MemorySize_t GetSize( const char* Arg )
{
    oC_MemorySize_t size = 0;
    if ( !oC_VerifyStdioResult( sscanf(Arg, "%llu", &size) ) || size == 0)
    {
        PrintHelp();
        return 0;
    }
    return size;
}

//==========================================================================================================================================
/**
 * Handles 'allocate' menu
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 * @return program exit code
 */
//==========================================================================================================================================
static int Allocate( int Argc , char ** Argv )
{
    if ( Argc < 3 )
    {
        PrintHelp();
        return -1;
    }

    oC_MemorySize_t size = GetSize(Argv[2]);
    if ( size == 0 )
    {
        return -1;
    }

    uint8_t* buffer = kmalloc(size, &Allocator, AllocationFlags_DontRepair);
    if (buffer == NULL)
    {
        printf("Cannot allocate memory!\n");
        return -1;
    }
    printf("Allocated buffer: %p\n", buffer);

    for(oC_MemorySize_t offset = 0; offset < size + sizeof(uint32_t); offset++)
    {
        if (offset % 16 == 0)
        {
            printf("\n");
        }
        printf("%02x ", buffer[offset]);
    }
    printf("\n");

    size = oC_MemMan_GetMemoryOfAllocatorSize(&Allocator);
    printf("Total allocation size: %.0M\n", size);

    return 0;
}

//==========================================================================================================================================
/**
 * Handles 'release' menu
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 * @return program exit code
 */
//==========================================================================================================================================
static int Release( int Argc , char ** Argv )
{
    if ( Argc < 3 )
    {
        oC_MemMan_FreeAllMemoryOfAllocator(&Allocator);
        printf("All memory of allocator %s has been released\n", Allocator.Name);
        return 0;
    }

    void* address = GetAddress(Argv[2]);
    if (address == NULL)
    {
        return -1;
    }

    kfree(address, AllocationFlags_Default);
    printf("%p memory has been released\n", address);

    oC_MemorySize_t size = oC_MemMan_GetMemoryOfAllocatorSize(&Allocator);
    printf("Total allocation size: %llu\n", size);

    return 0;
}

//==========================================================================================================================================
/**
 * Handles 'write' menu
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 * @return program exit code
 */
//==========================================================================================================================================
static int Write( int Argc , char ** Argv )
{
    if ( Argc < 4 )
    {
        PrintHelp();
        return -1;
    }
    uint8_t* buffer = GetAddress(Argv[2]);;
    oC_MemorySize_t size = GetSize(Argv[3]);
    if (buffer == NULL || size == 0)
    {
        return -1;
    }

    oC_MemorySize_t allocationSize = oC_MemMan_GetSizeOfAllocation(buffer) + sizeof(uint32_t);
    printf("Filling buffer %p of size %.0M\n", buffer, size);
    for(oC_MemorySize_t offset = 0; offset < oC_MIN(size, allocationSize); offset++)
    {
        buffer[offset] = offset;
    }

    for(oC_MemorySize_t offset = 0; offset < allocationSize; offset++)
    {
        if (offset % 16 == 0)
        {
            printf("\n");
        }
        printf("%02x ", buffer[offset]);
    }
    printf("\n");
    return 0;
}

//==========================================================================================================================================
/**
 * Handles 'read' menu
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 * @return program exit code
 */
//==========================================================================================================================================
static int Read( int Argc , char ** Argv )
{
    if ( Argc < 3 )
    {
        PrintHelp(Argc, Argv);
        return -1;
    }
    uint8_t* address = GetAddress(Argv[2]);

    oC_MemorySize_t size = oC_MemMan_GetSizeOfAllocation(address);
    for(oC_MemorySize_t offset = 0; offset < size + sizeof(uint32_t); offset++)
    {
        if (offset % 16 == 0)
        {
            printf("\n");
        }
        printf("%02x ", address[offset]);
    }
    printf("\n");
    return 0;
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    if ( Argc < 2 )
    {
        PrintHelp();
        return -1;
    }

    if ( strcmp(Argv[1],"allocate") == 0 )
    {
        return Allocate(Argc, Argv);
    }
    else if ( strcmp(Argv[1],"release") == 0 )
    {
        return Release(Argc, Argv);
    }
    else if ( strcmp(Argv[1],"write") == 0 )
    {
        return Write(Argc, Argv);
    }
    else if ( strcmp(Argv[1],"read") == 0 )
    {
        return Read(Argc, Argv);
    }
    else
    {
        PrintHelp();
    }
    return 0;
}
