/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the example_cm_draw program
 *
 * @author     Patryk Kubiak - (Created on: 2024-11-28 - 10:11:12) 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_screenman.h>
#include <oc_colormap.h>
#include <oc_tgui.h>

//=============================================================================
//                    == BOARD CONFIGURATION ==
//
//          This structure contains the configuration of the checkers board.
//
//=============================================================================
typedef struct
{
    oC_Pixel_Position_t         TopLeft;                // The top left corner of the board
    oC_Pixel_ResolutionUInt_t   BoardSize;              // The size of the board (in pixels)
    oC_Pixel_ResolutionUInt_t   BorderWidth;            // The width of the border
    oC_ColorMap_BorderStyle_t   BorderStyle;            // The style of the border
    oC_Color_t                  BorderColor;            // The color of the border
    uint8_t                     NumberOfFieldsInRow;    // The number of fields in a row
    oC_Pixel_ResolutionUInt_t   FieldSize;              // The size of the field
    oC_Color_t                  BlackFieldColor;        // The color of the black field
    oC_Color_t                  WhiteFieldColor;        // The color of the white field
    oC_Color_t                  WhitePawnColor;         // The color of the white pawn
    oC_Color_t                  BlackPawnColor;         // The color of the black pawn
} BoardConfig_t;

//=============================================================================
//                    == BOARD CONFIGURATION ==
//
//          This structure contains the configuration of the checkers board.
//
//=============================================================================
static BoardConfig_t BoardConfig = {
      .TopLeft     = {
          .X = 0,
          .Y = 0,
      },
      .BoardSize            = 0,
      .NumberOfFieldsInRow  = 8,
      .BorderWidth          = 5,
      .BorderColor          = 0x8e4e36,
      .BorderStyle          = oC_ColorMap_BorderStyle_Rounded | oC_ColorMap_BorderStyle_Solid,
      .WhiteFieldColor      = 0xeae0a4,
      .BlackFieldColor      = 0x553122,
      .WhitePawnColor       = oC_Color_White,
      .BlackPawnColor       = oC_Color_Black
};

//=============================================================================
//                   == ADAPTS CONFIG TO SCREEN SIZE ==
//
//          This function adapts the board configuration to the screen size.
//          It centers the board on the screen and adjusts the size of the
//          board to the screen size.
//
//=============================================================================
static bool AdaptConfigToScreen( oC_Screen_t Screen, BoardConfig_t* Config )
{
    oC_Pixel_ResolutionUInt_t screenWidth = 0, screenHeight = 0;

    if(oC_ErrorOccur(oC_Screen_ReadResolution(Screen, &screenWidth, &screenHeight)))
    {
        return false;
    }
    oC_Pixel_ResolutionUInt_t maxBoardSize = oC_MIN(screenWidth, screenHeight);
    if( maxBoardSize == 0 )
    {
        return false;
    }

    Config->BoardSize = Config->BoardSize > 0 ? oC_MIN(Config->BoardSize, maxBoardSize) : maxBoardSize;
    Config->TopLeft.X = (screenWidth  - Config->BoardSize) / 2;
    Config->TopLeft.Y = (screenHeight - Config->BoardSize) / 2;
    Config->FieldSize = (Config->BoardSize - Config->BorderWidth) / Config->NumberOfFieldsInRow;

    return true;
}

//=============================================================================
//                    == DRAWS CHECKERS BOARD ==
//
//          This function draws the checkers board on the screen.
//
//=============================================================================
static void DrawCheckersBoard( oC_Screen_t Screen, oC_ColorMap_t* ColorMap, const BoardConfig_t* Config )
{
    oC_ColorMap_LayerIndex_t drawLayer = oC_ColorMap_GetInactiveLayer(ColorMap);

    oC_ColorMap_SwitchLayer(ColorMap, drawLayer);

    oC_Pixel_Position_t bottomRight = {
           .X = Config->TopLeft.X + Config->BoardSize,
           .Y = Config->TopLeft.Y + Config->BoardSize
    };
    oC_ColorMap_DrawRect(
                    ColorMap,
                    Config->TopLeft,
                    bottomRight,
                    Config->BorderWidth,
                    Config->BorderStyle,
                    Config->BorderColor,
                    Config->WhiteFieldColor,
                    oC_ColorFormat_RGB888,
                    true
                    );

    oC_Pixel_ResolutionUInt_t horizontalMargin = Config->BorderWidth;
    oC_Pixel_ResolutionUInt_t verticalMargin   = Config->BorderWidth;
    oC_Pixel_ResolutionUInt_t fieldWidth  = Config->FieldSize;
    oC_Pixel_ResolutionUInt_t fieldHeight = Config->FieldSize;

    for(uint8_t columnIndex = 0; columnIndex < Config->NumberOfFieldsInRow; columnIndex++)
    {
        for(uint8_t rowIndex = 0; rowIndex < Config->NumberOfFieldsInRow; rowIndex++)
        {
            if( (columnIndex + rowIndex) % 2 == 0 )
            {
                continue;
            }
            oC_Pixel_Position_t fieldTopLeft = {
                .X = Config->TopLeft.X + columnIndex * fieldWidth + horizontalMargin,
                .Y = Config->TopLeft.Y + rowIndex * fieldHeight + verticalMargin
            };
            oC_ColorMap_FillRectWithColor(ColorMap, fieldTopLeft, fieldWidth, fieldHeight, Config->BlackFieldColor, oC_ColorFormat_RGB888);

            if( rowIndex > 2 && rowIndex < 5 )
            {
                continue;
            }

            oC_Pixel_Position_t fieldCenter = {
                .X = fieldTopLeft.X + fieldWidth / 2,
                .Y = fieldTopLeft.Y + fieldHeight / 2
            };
            oC_Pixel_ResolutionUInt_t radius = fieldWidth / 2;
            oC_Color_t pawnColor = rowIndex < 3 ? Config->BlackPawnColor : Config->WhitePawnColor;
            oC_ColorMap_FillCircleWithColor(ColorMap, fieldCenter, radius, pawnColor, oC_ColorFormat_RGB888);
        }
    }

    oC_Screen_SwitchLayer(Screen, drawLayer);
}

//! [init]
//=============================================================================
//                    == MAIN FUNCTION ==
//
//          This is the main function of the example_cm_draw program.
//          It shows an example of how to draw a checkers board on the screen
//          using the color map.
//
//=============================================================================
int main()
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;
    oC_ColorMap_t*  colorMap  = NULL;

    // get the default screen from the screen manager
    oC_Screen_t screen = oC_ScreenMan_GetDefaultScreen();

    // read the color map from the screen
    if(ErrorCode( oC_Screen_ReadColorMap(screen, &colorMap) ))
    {
        // adapt the board configuration to the screen size
        if( AdaptConfigToScreen(screen, &BoardConfig) )
        {
            // draw the checkers board
            DrawCheckersBoard(screen,colorMap,&BoardConfig);

            printf("Press any key to continue...");
            oC_TGUI_WaitForKeyPress(NULL);
        }
    }

    return errorCode;
}
//! [init]
