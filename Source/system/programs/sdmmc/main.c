/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the sdmmc program
 *
 * @author     Patryk Kubiak - (Created on: 2022-07-03 - 11:08:27) 
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define oC_SDMMC_PRIVATE
#include <oc_stdio.h>
#include <oc_sdmmc.h>
#include <oc_sdmmc_cmd.h>

static const oC_SDMMC_Config_t STM32F7Discovery_SDMMC = {
                .Mode           = oC_SDMMC_Mode_LLD,
                .TransferMode   = oC_SDMMC_TransferMode_4Bit,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT0] = oC_Pin_uSD_D0,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT1] = oC_Pin_uSD_D1,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT2] = oC_Pin_uSD_D2,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT3] = oC_Pin_uSD_D3,
                .Pins[oC_SDMMC_PinIndex_4BitMode_CMD]  = oC_Pin_uSD_CMD,
                .Pins[oC_SDMMC_PinIndex_4BitMode_CLK]  = oC_Pin_uSD_CLK,
                .Pins[oC_SDMMC_PinIndex_4BitMode_nIRQ] = oC_Pin_uSD_Detect,
                .SectorSize     = B(512),
                .DetectionMode  = oC_SDMMC_DetectionMode_Polling
};

//==========================================================================================================================================
/**
 * @brief sends a command given in the argument
 *
 * The function sends a command from the argument
 *
 * @param Context                   context of the driver
 * @param CommandName               name of a command to send
 * @param CommandArgument           argument
 * @param Timeout                   maximum time for the operation
 * @param RelativeCardAddress       RCA - relative card address
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendCommand( oC_SDMMC_Context_t Context, const char* CommandName, const char* CommandArgument, const char* TimeoutString, const char* ResponseType, const char* RelativeCardAddressString )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_SDMMC_Cmd_CommandData_t commandData = {0};
    oC_Time_t timeout = oC_s(10);
    oC_SDMMC_RelativeCardAddress_t relativeCardAddress = 0;
    if (
         ErrorCode( oC_SDMMC_Cmd_ReadCommandIndex(CommandName, &commandData.Command) )
      && ErrorCondition( CommandArgument == NULL || oC_VerifyStdioResult( sscanf( CommandArgument, "%i", &commandData.Argument.Generic ) )
                         , oC_ErrorCode_CommandArgumentCannotBeRead )
      && ErrorCondition( TimeoutString == NULL || oC_VerifyStdioResult( sscanf(TimeoutString ,"%f", &timeout) ), oC_ErrorCode_TimeNotCorrect )
      && ErrorCondition( RelativeCardAddressString == NULL || oC_VerifyStdioResult( sscanf(RelativeCardAddressString,"%i", &relativeCardAddress) ), oC_ErrorCode_WrongAddress )
         )
    {
        char commandName[100] = {0};
        const char* cmd = commandName;
        if ( !ErrorCode( oC_SDMMC_Cmd_ReadCommandName(commandData.Command, 100, commandName) ) )
        {
            cmd = CommandName;
        }
        if ( ResponseType == NULL || strcmp(ResponseType,"Short") == 0 )
        {
            commandData.Response.Type = oC_SDMMC_Responses_ResponseType_Short;
            printf("Expecting short response\n");
        }
        else if ( strcmp(ResponseType,"Long") == 0 )
        {
            commandData.Response.Type = oC_SDMMC_Responses_ResponseType_Long;
            printf("Expecting long response\n");
        }
        else if ( strcmp(ResponseType,"None") == 0 )
        {
            commandData.Response.Type = oC_SDMMC_Responses_ResponseType_None;
            printf("Expecting no response\n");
        }
        else
        {
            printf("Invalid response type\n");
        }
        printf( "Sending command %s with argument 0x%x: ", cmd, commandData.Argument );
        errorCode = oC_SDMMC_Cmd_SendCommand(Context, relativeCardAddress, &commandData, timeout);
        printf("%R\n", errorCode);
        printf("Result: 0x%08X\n", commandData.Response.ShortResponse);
    }
    else
    {
        printf("Cannot send command: %R\n", errorCode);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets transfer mode according to the wide-bus string
 *
 * @param Context           Context of the driver
 * @param TransferMode      Transfer mode to set
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t SetTransferMode( oC_SDMMC_Context_t Context, const char* TransferMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_SDMMC_TransferMode_t transferMode = oC_SDMMC_TransferMode_Auto;
    if (
        ErrorCondition( isaddresscorrect(TransferMode), oC_ErrorCode_MissingArgument )
     && ErrorCode( oC_SDMMC_ReadTransferModeByName(TransferMode, &transferMode) )
        )
    {
        const char* transferModeName = NULL;
        oC_SDMMC_ReadTransferModeName(transferMode, &transferModeName);
        printf("Setting transfer mode to %s ... ", transferModeName);
        errorCode = oC_SDMMC_SetTransferMode( Context, transferMode );
        printf("%R\n", errorCode);
    }
    else
    {
        printf("Cannot set transfer mode: %R\n", errorCode);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prints buffer on stdout
 *
 * @param Buffer                Buffer to print
 * @param Size                  Size of the buffer
 * @param ExpectedValues        Buffer with expected values or NULL if not used
 *
 * @return true if the buffer is similar to expected
 */
//==========================================================================================================================================
static bool PrintBuffer( const uint8_t* Buffer, oC_MemorySize_t Size, const uint8_t* ExpectedValues )
{
    bool similar = true;
    const oC_MemoryOffset_t bytesPerLine = 16;
    for ( oC_MemoryOffset_t offset = 0; offset < Size; offset++)
    {
        if ( ExpectedValues != NULL )
        {
            if ( Buffer[offset] != ExpectedValues[offset] )
            {
                printf(oC_VT100_FG_RED);
                similar = false;
            }
            else
            {
                printf(oC_VT100_FG_GREEN);
            }
        }
        printf("%02X "oC_VT100_FG_WHITE, Buffer[offset]);
        if ( (offset % bytesPerLine) == (bytesPerLine - 1) )
        {
            printf("\n");
        }
    }
    return similar;
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
	oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
	oC_SDMMC_Context_t context = NULL;
	oC_DiskId_t diskId = 0;
	oC_Struct_Define(oC_SDMMC_CardInfo_t, cardInfo);
	uint8_t* sectorData = NULL;
	uint8_t* backupSectorData = NULL;
	uint8_t* randomSectorData = NULL;
	oC_SectorNumber_t sectorNumber = 0;

	oC_Procedure_Begin
	{
        printf("Configuration of the sdmmc: ");
        errorCode = oC_SDMMC_Configure(&STM32F7Discovery_SDMMC, &context);
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);
        sleep(s(1));
        if ( oC_ArgumentOccur(Argc, Argv, "-w") )
        {
            const char* widthString = oC_GetArgumentAfter(Argc, Argv, "-w");
            printf("Setting transfer mode to %s ...");
            errorCode = SetTransferMode(context, widthString);
            printf("%R\n", errorCode);
            oC_Procedure_ExitIfError(errorCode);
        }

        if ( oC_ArgumentOccur(Argc, Argv, "-c") )
        {
            const char* cmd = oC_GetArgumentAfter(Argc, Argv, "-c");
            printf("Sending command %s ... ");
            errorCode = SendCommand(
                            context,
                            cmd,
                            oC_GetArgumentAfter(Argc, Argv, "-a"),
                            oC_GetArgumentAfter(Argc, Argv, "-t"),
                            oC_GetArgumentAfter(Argc, Argv, "-r"),
                            oC_GetArgumentAfter(Argc, Argv, "-d"));
            printf("%R\n", errorCode);
            oC_Procedure_ExitIfError(errorCode);
            break;
        }

        printf("Waiting for a new disk (max 5s): ");
        errorCode = oC_SDMMC_WaitForNewDisk(context, &diskId, s(10));
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);
        printf("Disk ID: %d\n", diskId);
        printf("Reading of Card Information ... ");
        errorCode = oC_SDMMC_ReadCardInfo(context, diskId, &cardInfo);
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);

        printf("Size: %.2M\n", cardInfo.CardSize);
        printf("Sector Size: %.0M\n", cardInfo.SectorSize);
        printf("Number of sectors: %lu\n", cardInfo.NumberOfSectors);
        printf("Product name: %s\n", cardInfo.CardId.ProductName);
        printf("Manufacturer ID: %d\n", cardInfo.CardId.ManufacturerId);
        printf("OEM App ID: %s\n", cardInfo.CardId.OemApplicationId);
        printf("Manufacturing Date: %04d-%02d\n", cardInfo.CardId.ManufacturingDate.Year + 2000, cardInfo.CardId.ManufacturingDate.Month);
        printf("Product Revision: %d.%d\n", cardInfo.CardId.ProductRevision.Major, cardInfo.CardId.ProductRevision.Minor);
        printf("Serial Number: 0x%08X\n", cardInfo.CardId.SerialNumber);

        sectorData = malloc( cardInfo.SectorSize, AllocationFlags_ZeroFill );
        oC_Procedure_ExitIfFalse(sectorData != NULL, oC_ErrorCode_AllocationError);

        printf("Reading of sector %d ... ", sectorNumber);
        errorCode = oC_SDMMC_ReadSectors(context, diskId, sectorNumber, sectorData, cardInfo.SectorSize, s(10));
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);
        PrintBuffer(sectorData, cardInfo.SectorSize, NULL);

        if (!oC_ArgumentOccur(Argc, Argv, "--write"))
        {
            break;
        }

        backupSectorData = malloc( cardInfo.SectorSize, AllocationFlags_ZeroFill );
        oC_Procedure_ExitIfFalse(backupSectorData != NULL, oC_ErrorCode_AllocationError);
        randomSectorData = malloc( cardInfo.SectorSize, AllocationFlags_ZeroFill );
        oC_Procedure_ExitIfFalse(randomSectorData != NULL, oC_ErrorCode_AllocationError);

        memcpy(backupSectorData,sectorData,cardInfo.SectorSize);

        // Preparation of random data to write
        for ( oC_MemorySize_t offset = 0; offset < cardInfo.SectorSize; offset++ )
        {
            randomSectorData[offset] = (uint8_t)((offset + ((oC_MemorySize_t)gettimestamp())) % 255);
        }

        printf("Random sector to write: \n");
        PrintBuffer(randomSectorData, cardInfo.SectorSize, NULL);
        printf("Start overwriting of sector %d ... ", sectorNumber);
        errorCode = oC_SDMMC_WriteSectors(context, diskId, sectorNumber, randomSectorData, cardInfo.SectorSize, s(3));
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);

        memset(sectorData, 0x00, cardInfo.SectorSize);

        printf("Reading of sector %d ... ", sectorNumber);
        errorCode = oC_SDMMC_ReadSectors(context, diskId, sectorNumber, sectorData, cardInfo.SectorSize, s(3));
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);
        PrintBuffer(sectorData, cardInfo.SectorSize, randomSectorData);

        printf("Restoring original values of sector %d ... ", sectorNumber);
        errorCode = oC_SDMMC_WriteSectors(context, diskId, sectorNumber, backupSectorData, cardInfo.SectorSize, s(3));
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);

        memset(sectorData, 0, cardInfo.SectorSize);
        printf("Reading of sector %d ... ", sectorNumber);
        errorCode = oC_SDMMC_ReadSectors(context, diskId, sectorNumber, sectorData, cardInfo.SectorSize, s(3));
        printf("%R\n", errorCode);
        oC_Procedure_ExitIfError(errorCode);
        PrintBuffer(sectorData, cardInfo.SectorSize, backupSectorData);

	}
	oC_Procedure_End;

	if ( sectorData != NULL )
	{
	    free(sectorData);
	}
	if ( backupSectorData != NULL )
	{
	    free(backupSectorData);
	}
	if ( randomSectorData != NULL )
	{
	    free(randomSectorData);
	}

    printf("Unconfiguration of the sdmmc: ");
    errorCode = oC_SDMMC_Unconfigure(&STM32F7Discovery_SDMMC, &context);
    printf("%R\n", errorCode);
    return 0;
}
