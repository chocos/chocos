/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the userspace_test program
 *
 * @author     Patryk Kubiak - (Created on: 2017-06-23 - 08:16:35) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_compiler.h>
#include <oc_stdio.h>
#include <oc_stdtypes.h>

static char *       SomeString POINTER  = "Hue hue, it is my string!";
static uint32_t     Variable            = 0xDEADBEEF;
static uint32_t *   Pointer             = NULL;
static uint32_t     Variable2           = 0xBEEFBABA;

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    printf("Hello World");

    printf("Hello ChocoOS - userspace World!\n");
    printf("It is very nice to be here!\n");

    printf("Here is string from data section: '%s'\n", SomeString);
    printf("And here variable from data section: 0x%08X\n", Variable);

    Pointer = &Variable2;

    printf("Variable2 = 0x%08X\n", Variable2);
    printf("Pointer to Variable2 = 0x%08X\n", Pointer);
    printf("Value from pointer = 0x%08X\n", *Pointer);
    printf("String pointer = 0x%08X\n", SomeString);

    return 0;
}
