#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#ifndef COMMON_H
#define COMMON_H

typedef uint8_t byte;
typedef uint16_t word;
typedef uint32_t dword;
typedef uint64_t qword;

#define MMC_MAX_PAGE_COUNT 8

struct Pixel {
    int x, y; // (x, y) coordinate
    int c; // RGB value of colors can be found in fce.h
};
typedef struct Pixel Pixel;

/* A buffer of pixels */
struct PixelBuf {
    Pixel buf[264 * 264];
    int size;
};
typedef struct PixelBuf PixelBuf;

typedef struct
{
    char rom[1048576];
    byte buf[1048576];
    byte CPU_RAM[0x8000];
    byte memory[0x10000];
    byte mmc_chr_pages[MMC_MAX_PAGE_COUNT][0x2000];
    byte PPU_SPRRAM[0x100];
    byte PPU_RAM[0x4000];
    // Screen State and Rendering

    // For sprite-0-hit checks
    byte ppu_screen_background[264][248];

    // Precalculated tile high and low bytes addition for pattern tables
    byte ppu_l_h_addition_table[256][256][8];
    byte ppu_l_h_addition_flip_table[256][256][8];
    PixelBuf bg___pixels, bbg___pixels, fg___pixels;
} Nes_t;

#define CPU_RAM         Nes->CPU_RAM
#define PPU_RAM         Nes->PPU_RAM
#define PPU_SPRRAM      Nes->PPU_SPRRAM

#define ppu_screen_background       Nes->ppu_screen_background
#define ppu_l_h_addition_table      Nes->ppu_l_h_addition_table
#define ppu_l_h_addition_flip_table Nes->ppu_l_h_addition_flip_table
#define bg___pixels     Nes->bg___pixels
#define bbg___pixels    Nes->bbg___pixels
#define fg___pixels     Nes->fg___pixels

// Binary Operations
bool common_bit_set(long long value, byte position);

// Byte Bit Operations
void common_set_bitb(byte *variable, byte position);
void common_unset_bitb(byte *variable, byte position);
void common_toggle_bitb(byte *variable, byte position);
void common_modify_bitb(byte *variable, byte position, bool set);

// Word Bit Operations
void common_set_bitw(word *variable, byte position);
void common_unset_bitw(word *variable, byte position);
void common_toggle_bitw(word *variable, byte position);
void common_modify_bitw(word *variable, byte position, bool set);

// Double Word Bit Operations
void common_set_bitd(dword *variable, byte position);
void common_unset_bitd(dword *variable, byte position);
void common_toggle_bitd(dword *variable, byte position);
void common_modify_bitd(dword *variable, byte position, bool set);

// Quad Word Bit Operations
void common_set_bitq(qword *variable, byte position);
void common_unset_bitq(qword *variable, byte position);
void common_toggle_bitq(qword *variable, byte position);
void common_modify_bitq(qword *variable, byte position, bool set);

extern Nes_t * Nes;

#endif
