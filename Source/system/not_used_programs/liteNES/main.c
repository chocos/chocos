/*
LiteNES originates from Stanislav Yaglo's mynes project:
  https://github.com/yaglo/mynes

LiteNES is a "more portable" version of mynes.
  all system(library)-dependent code resides in "hal.c" and "main.c"
  only depends on libc's memory moving utilities.

How does the emulator work?
  1) read file name at argv[1]
  2) load the rom file into array rom
  3) call fce_load_rom(rom) for parsing
  4) call fce_init for emulator initialization
  5) call fce_run(), which is a non-exiting loop simulating the NES system
  6) when SIGINT signal is received, it kills itself
*/

#include "fce.h"
#include <common.h>
#include <oc_stdio.h>
#include <oc_stdlib.h>
#include <unistd.h>

Nes_t * Nes = NULL;

#define exit(r)     return r

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Usage: mynes romfile.nes\n");
        exit(1);
    }
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL)
    {
        printf("Open rom file failed.\n");
        exit(1);
    }

    Nes = malloc(sizeof(Nes_t), AllocationFlags_ZeroFill | AllocationFlags_ExternalRamFirst);
    if(Nes == NULL)
    {
        printf("Cannot allocate %d bytes of memory!\n", sizeof(Nes_t));
        exit(1);
    }

    int nread = fread(Nes->rom, sizeof(Nes->rom), 1, fp);
    if (nread == 0) {
        printf("Read rom file failed.\n");
        exit(1);
    }
    if (fce_load_rom(Nes->rom) != 0)
    {
        printf("Invalid or unsupported rom.\n");
        exit(1);
    }
    fce_init();
    fce_run();
    return 0;
}
