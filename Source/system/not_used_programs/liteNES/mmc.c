#include "mmc.h"
#include "ppu.h"
#include <string.h>

int mmc_prg_pages_number, mmc_chr_pages_number;

#define memory              Nes->memory
#define mmc_chr_pages       Nes->mmc_chr_pages

inline byte mmc_read(word address)
{
    return memory[address];
}

inline void mmc_write(word address, byte data)
{
    switch (mmc_id) {
        case 0x3: {
            ppu_copy(0x0000, &mmc_chr_pages[data & 3][0], 0x2000);
        }
        break;
    }
    memory[address] = data;
}

inline void mmc_copy(word address, byte *source, int length)
{
    memcpy(&memory[address], source, length);
}

inline void mmc_append_chr_rom_page(byte *source)
{
    memcpy(&mmc_chr_pages[mmc_chr_pages_number++][0], source, 0x2000);
}
