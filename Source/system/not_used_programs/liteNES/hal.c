/*
This file present all abstraction needed to port LiteNES.
  (The current working implementation uses allegro library.)

To port this project, replace the following functions by your own:
1) nes_hal_init()
    Do essential initialization work, including starting a FPS HZ timer.

2) nes_set_bg_color(c)
    Set the back ground color to be the NES internal color code c.

3) nes_flush_buf(*buf)
    Flush the entire pixel buf's data to frame buffer.

4) nes_flip_display()
    Fill the screen with previously set background color, and
    display all contents in the frame buffer.

5) wait_for_frame()
    Implement it to make the following code is executed FPS times a second:
        while (1) {
            wait_for_frame();
            do_something();
        }

6) int nes_key_state(int b) 
    Query button b's state (1 to be pressed, otherwise 0).
    The correspondence of b and the buttons:
      0 - Power
      1 - A
      2 - B
      3 - SELECT
      4 - START
      5 - UP
      6 - DOWN
      7 - LEFT
      8 - RIGHT
*/
#include "hal.h"
#include "fce.h"
#include "common.h"
#include <oc_color.h>
#include <oc_screenman.h>
#include <oc_ictrlman.h>

oC_Color_t color_map[64];
static oC_ColorMap_t * ColorMap = NULL;
static oC_ICtrl_t ICtrl = NULL;

static struct
{
    oC_Pixel_Position_t     Position;
    oC_Pixel_Position_t     EndPosition;
    bool                    Pressed;
} Buttons[10];

//==========================================================================================================================================
//==========================================================================================================================================
static inline void DrawButton( int Index , oC_Pixel_ResolutionUInt_t X , oC_Pixel_ResolutionUInt_t Y , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height )
{
    Buttons[Index].Pressed          = false;
    Buttons[Index].Position.X       = X;
    Buttons[Index].Position.Y       = Y;
    Buttons[Index].EndPosition.X    = X + Width;
    Buttons[Index].EndPosition.Y    = Y + Height;

    oC_ColorMap_DrawRect(ColorMap,Buttons[Index].Position,Buttons[Index].EndPosition,1,oC_ColorMap_BorderStyle_Rounded,oC_Color_White, oC_Color_Red, oC_ColorFormat_RGB888,true);
}

//==========================================================================================================================================
//==========================================================================================================================================
static inline void SetColor(oC_Pixel_ResolutionUInt_t X,oC_Pixel_ResolutionUInt_t Y, oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height ,  uint8_t ColorMap[Height][Width][3] , pal Color )
{
    ColorMap[Y+27][X+100][0] = Color.b;
    ColorMap[Y+27][X+100][1] = Color.g;
    ColorMap[Y+27][X+100][2] = Color.r;
}

/* Wait until next allegro timer event is fired. */
void wait_for_frame()
{

}

/* Set background color. RGB value of c is defined in fce.h */
void nes_set_bg_color(int c)
{
    pal color = palette[c];

    for(int x = 0; x < SCREEN_WIDTH; x++)
    {
        for(int y = 0; y < SCREEN_HEIGHT; y++)
        {
            SetColor(x,y,ColorMap->Width,ColorMap->Height,ColorMap->GenericWriteMap,color);
        }
    }
}

/* Flush the pixel buffer */
void nes_flush_buf(PixelBuf *buf) {
    int i;
    for (i = 0; i < buf->size; i ++) {
        Pixel *p = &buf->buf[i];
        SetColor(p->x,p->y,ColorMap->Width,ColorMap->Height,ColorMap->GenericWriteMap,palette[p->c]);
    }
}

void nes_hal_init()
{
    oC_Screen_t screen = oC_ScreenMan_GetDefaultScreen();

    oC_Screen_ReadColorMap(screen,&ColorMap);

    ICtrl = oC_ICtrlMan_GetICtrl(screen);

    DrawButton(  3, 5, 30, 80 , 55 );  // SELECT
    DrawButton(  4, 5, 90, 80 , 55 );  // START
    DrawButton(  1, 5,150, 80 , 55 );  // A
    DrawButton(  2, 5,210, 80 , 55 );  // B

    DrawButton(  5,390, 60, 50 , 50 );  // UP
    DrawButton(  7,360,120, 50 , 50 );  // LEFT
    DrawButton(  6,390,180, 50 , 50 );  // DOWN
    DrawButton(  8,420,120, 50 , 50 );  // RIGHT

    DrawButton(  0,400,  0, 80 , 30 );  // RIGHT

}

/* Update screen at FPS rate by allegro's drawing function. 
   Timer ensures this function is called FPS times a second. */
void nes_flip_display()
{
    int i;
    for (i = 0; i < 64; i ++) {
        pal color = palette[i];
        color_map[i] = color.r << 16 | color.g << 8 | color.b;
    }
}

/* Query a button's state.
   Returns 1 if button #b is pressed. */
int nes_key_state(int b)
{
    oC_IDI_Event_t event;
    int pressed = false;

    if(b == 0)
    {
        if(oC_ICtrl_WaitForEvent(ICtrl,oC_IDI_EventId_AnyController | oC_IDI_EventId_Click, &event, ms(10) ) == oC_ErrorCode_None)
        {
            for(uint16_t i = 0; i < event.NumberOfPoints; i++)
            {
                for(int buttonIndex = 0; buttonIndex < 10 ; buttonIndex++)
                {
                    if(
                        event.Position[i].X >= Buttons[buttonIndex].Position.X
                     && event.Position[i].X <= Buttons[buttonIndex].EndPosition.X
                     && event.Position[i].Y >= Buttons[buttonIndex].Position.Y
                     && event.Position[i].Y <= Buttons[buttonIndex].EndPosition.Y
                        )
                    {
                        Buttons[buttonIndex].Pressed = true;
                        break;
                    }
                }
            }
        }
    }
    if(b < 10)
    {
        pressed = Buttons[b].Pressed;
        Buttons[b].Pressed = false;
    }

    return pressed;
}

