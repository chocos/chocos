/** ****************************************************************************************************************************************
 *
 * @file       spi_lld_test.c
 *
 * @brief      The file contains spi lld test program
 *
 * @author     Kamil Drobienko - (Created on: 18 10 2015 18:05:12)
 *             Krzysztof Chmielewski
 *             Krzysztof Dworzyński
 *
 * @note       Copyright (C) 2015 Kamil Drobienko <kamildrobienko@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_spi_lld.h>
#include <oc_stdio.h>
#include <inttypes.h>
#include <oc_stdlib.h>
#include <oc_vt100.h>
#include <oc_gpio_lld.h>

#ifdef LM4F120H5QR
typedef struct
{
    oC_SPI_Channel_t    channel;
    oC_SPI_Pin_t        clk;
    oC_SPI_Pin_t        fss;
    oC_SPI_Pin_t        Rx;
    oC_SPI_Pin_t        Tx;
}spi_Config_t;

oC_ErrorCode_t spi_channel_config(spi_Config_t *spiConfig)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    oC_Frequency_t checkoutFrequency = 0;

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_SetPower(spiConfig->channel, oC_Power_On));
    oC_PrintErrorMessage("SetPower = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_SetChannelUsed(spiConfig->channel) );
    oC_PrintErrorMessage("SetChannelUsed = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_DisableOperations(spiConfig->channel) );
    oC_PrintErrorMessage("DisableOperation = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_ConnectModulePin(spiConfig->clk) );
    oC_PrintErrorMessage("Connect clk = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_ConnectPeripheralPin(spiConfig->Rx)  );
    oC_PrintErrorMessage("Connect Rx = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_ConnectPeripheralPin(spiConfig->Tx)  );
    oC_PrintErrorMessage("Connect Tx = ", errorCode);

    oC_AssignErrorCode(&errorCode,  oC_SPI_LLD_ConnectPeripheralPin(spiConfig->fss) );
    oC_PrintErrorMessage("Connect fss = " , errorCode);

    oC_AssignErrorCode(&errorCode,  oC_SPI_LLD_SetMode(spiConfig->channel, oC_SPI_LLD_Mode_Master) );
    oC_PrintErrorMessage("Mode = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_SetFrequency(spiConfig->channel, oC_MHz(8) , 0 ));
    oC_PrintErrorMessage("SetFrequency = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_ReadFrequency(spiConfig->channel, &checkoutFrequency));
    oC_PrintErrorMessage("ReadFrequency = ", errorCode);

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_SetClockPolarity(spiConfig->channel, oC_SPI_LLD_Polarity_HighWhenActive) );
    oC_PrintErrorMessage("SetPolarity = ", errorCode);

    oC_AssignErrorCode(&errorCode ,oC_SPI_LLD_SetClockPhase(spiConfig->channel, oC_SPI_LLD_Phase_FirstEdge));
    oC_PrintErrorMessage("SetPhase = ", errorCode);

    oC_AssignErrorCode(&errorCode ,oC_SPI_LLD_SetFrameWidth(spiConfig->channel , oC_SPI_LLD_FrameWidth_16Bits) );
    oC_PrintErrorMessage("SetFrame = ",errorCode);

    oC_AssignErrorCode(&errorCode , oC_SPI_LLD_SetLoopback(spiConfig->channel, true));
    oC_PrintErrorMessage("SetLoopback = ", errorCode);

    oC_AssignErrorCode(&errorCode , oC_SPI_LLD_EnableOperations(spiConfig->channel));
    oC_PrintErrorMessage("Enable operations = ", errorCode);

    printf("\n\rClock frequency = %d \n\r", (int)checkoutFrequency);

    return errorCode;
}


int main( int Argc , char ** Argv )
{
    printf("This is spi lld test program\n\r");

    bool moduleWasEnabledByProgram = false;
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;

    spi_Config_t spiConfig = {
                    .channel = oC_SPI_Channel_SPI0,
                    .clk = oC_SPI_Pin_SSI0Clk_PA2,
                    .fss = oC_SPI_Pin_SSI0Fss_PA3,
                    .Rx = oC_SPI_Pin_SSI0Rx_PA4,
                    .Tx = oC_SPI_Pin_SSI0Tx_PA5};

    uint16_t dataTx=0;
    uint16_t dataRx=0;

    if(Argc>1)
    {
        for(int i=0; Argv[1][i] != '\0'; i++)
        {
            dataTx = dataTx*10 + ( (uint16_t)Argv[1][i] - '0' );
        }
    }

    oC_AssignErrorCode(&errorCode, oC_SPI_LLD_TurnOnDriver());
    oC_PrintErrorMessage("TurnOn = ", errorCode);

    errorCode = spi_channel_config(&spiConfig);

    moduleWasEnabledByProgram = (errorCode == oC_ErrorCode_ModuleIsTurnedOn);

    printf("\n\rTx= %d\n\r" ,    dataTx);

    oC_AssignErrorCode( &errorCode , oC_SPI_LLD_Write(   spiConfig.channel, &dataTx , 1, 2, oC_IoFlags_WaitForAllElements ) );
    oC_PrintErrorMessage("\n\rWrite = ", errorCode);
    oC_AssignErrorCode( &errorCode , oC_SPI_LLD_Read(    spiConfig.channel, &dataRx , 1, 2, oC_IoFlags_WaitForAllElements ) );
    oC_PrintErrorMessage("\n\rRead = ", errorCode);

    printf("\n\rRx= %d\n\r" ,    dataRx);

    if(moduleWasEnabledByProgram )
    {
        oC_AssignErrorCode(&errorCode, oC_SPI_LLD_TurnOffDriver());
        oC_PrintErrorMessage("\n\rTurnOff = ", errorCode);
    }
    else
    {
       printf("I don't disable SPI LLD module - it was not enabled by the program\n\r");
    }

    return 0;
}

#else
int main( int Argc , char ** Argv )
{
    printf("SPI TEST is not supported in the given machine\n\r");
    return -1;
}
#endif
