/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      The file contains gpio_test program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_vt100.h>
#include <oc_gpio.h>

//==========================================================================================================================================
/**
 * @brief performs loopback test (PinOut connected to the PinIn)
 */
//==========================================================================================================================================
static oC_ErrorCode_t PerformLoopbackTest( oC_Pin_t PinOut , oC_Pin_t PinIn )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    printf(oC_VT100_FG_WHITE "Performing GPIO loopback test - please ensure, that pin %s is connected with %s...\n\r" , oC_GPIO_GetPinName(PinOut), oC_GPIO_GetPinName(PinIn));

    printf(oC_VT100_FG_WHITE "Configuring pins...");

    if(
        oC_AssignErrorCode(&errorCode , oC_GPIO_QuickOutput(PinOut)) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_QuickInput(PinIn,oC_GPIO_Pull_Down,oC_GPIO_IntTrigger_Off))
        )
    {
        printf(oC_VT100_FG_GREEN "OK" oC_VT100_FG_WHITE "\n\r");

        do
        {
            errorCode = oC_ErrorCode_TestFailed;

            // //////////////////////////////////////
            /* Test High state */
            printf(oC_VT100_FG_WHITE "Testing high state...");

            oC_GPIO_SetPinsState(PinOut,oC_GPIO_PinsState_AllHigh);

            oC_Pin_t highState = oC_GPIO_GetHighStatePins(PinIn);

            if(highState != PinIn)
            {
                printf(oC_VT100_FG_RED "failed!" oC_VT100_FG_WHITE "\n\r"
                       "PinIn (%s) is not in high state! Loopback test failed!\n\r" , oC_GPIO_GetPinName(PinIn));
                oC_PrintErrorMessage("Pins state is not correct: " , errorCode);
                break;
            }
            else
            {
                printf(oC_VT100_FG_GREEN "OK" oC_VT100_FG_WHITE "\n\r");
            }

            // //////////////////////////////////////


            // //////////////////////////////////////
            /* Test Low state */
            printf(oC_VT100_FG_WHITE "Testing low state...");

            oC_GPIO_SetPinsState(PinOut,oC_GPIO_PinsState_AllLow);

            oC_Pin_t lowState = oC_GPIO_GetLowStatePins(PinIn);

            if(lowState != PinIn)
            {
                printf(oC_VT100_FG_RED "failed!" oC_VT100_FG_WHITE "\n\r"
                       "PinIn (%s) is not in low state! Loopback test failed!\n\r" , oC_GPIO_GetPinName(PinIn));
                oC_PrintErrorMessage("Pins state is not correct: " , errorCode);
                break;
            }
            else
            {
                printf(oC_VT100_FG_GREEN "OK" oC_VT100_FG_WHITE "\n\r");
            }
            // //////////////////////////////////////

            // //////////////////////////////////////
            /* Unconfiguration PinIn */
            printf(oC_VT100_FG_WHITE "Unconfiguration PinIn...");

            if(!oC_AssignErrorCode(&errorCode , oC_GPIO_QuickUnconfigure(PinIn)))
            {
                printf(oC_VT100_FG_RED "failed!" oC_VT100_FG_WHITE "\n\r");
                oC_PrintErrorMessage("Failure reason: " , errorCode);
                break;
            }
            else
            {
                printf(oC_VT100_FG_GREEN "OK" oC_VT100_FG_WHITE "\n\r");
            }
            // //////////////////////////////////////



            printf(oC_VT100_FG_WHITE "Reconfiguration of the pin %s for interrupts test..." , oC_GPIO_GetPinName(PinIn));

            oC_GPIO_Config_t gpioInputConfig = {
                            .Pins               = PinIn ,
                            .Mode               = oC_GPIO_Mode_Input ,
                            .InterruptTrigger   = oC_GPIO_IntTrigger_RisingEdge,
                            .Pull               = oC_GPIO_Pull_Down
            };
            oC_GPIO_Context_t context        = NULL;

            if(!oC_AssignErrorCode(&errorCode , oC_GPIO_Configure(&gpioInputConfig,&context)))
            {
                printf(oC_VT100_FG_RED "failed!" oC_VT100_FG_WHITE "\n\r");
                oC_PrintErrorMessage("Cannot reconfigure pin for interrupts test: " , errorCode);
                break;
            }
            else
            {
                printf(oC_VT100_FG_GREEN "OK" oC_VT100_FG_WHITE "\n\r");
            }

            printf(oC_VT100_FG_WHITE "Performing interrupts rising edge test...");

            oC_GPIO_SetPinsState(PinOut,oC_GPIO_PinsState_AllHigh);

            if(!oC_AssignErrorCode(&errorCode , oC_GPIO_WaitForPins(PinIn,oC_s(3))))
            {
                printf(oC_VT100_FG_RED "failed!" oC_VT100_FG_WHITE "\n\r");
                oC_PrintErrorMessage("Error while performing interrupt test: " , errorCode);
                break;
            }
            else
            {
                printf(oC_VT100_FG_GREEN "OK" oC_VT100_FG_WHITE "\n\r");
            }

            errorCode = oC_ErrorCode_None;
        } while(0);
    }
    else
    {
        printf(oC_VT100_FG_RED "failed!" oC_VT100_FG_WHITE "\n\r");
    }

    oC_GPIO_QuickUnconfigure(PinOut);
    oC_GPIO_QuickUnconfigure(PinIn);

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief this function performs GPIO tests
 */
//==========================================================================================================================================
static oC_ErrorCode_t PerformTests( oC_Pin_t PinOut , oC_Pin_t PinIn )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_GPIO_IsPinDefined(PinOut) == false)
    {
        printf("Cannot perform tests - pin out is not defined\n\r");
        errorCode = oC_ErrorCode_PinNotDefined;
    }
    else if(oC_GPIO_IsPinDefined(PinIn) == false)
    {
        printf("Cannot perform tests - pin in is not defined\n\r");
        errorCode = oC_ErrorCode_PinNotDefined;
    }
    else
    {
        errorCode = PerformLoopbackTest(PinOut,PinIn);

        if(oC_ErrorOccur(errorCode))
        {
            oC_PrintErrorMessage("Loopback test failed: " , errorCode);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief main gpio function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_Pin_t        pinOut      = oC_Pin_GPIO_TEST_PIN_OUT;
    oC_Pin_t        pinIn       = oC_Pin_GPIO_TEST_PIN_IN;
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_None;

    if(Argc == 1)
    {
        printf(oC_VT100_FG_WHITE
               "Pins are not given, using defaults:\n\r"
                        " Pin out: %s\n\r"
                        " Pin in: %s\n\r" , oC_GPIO_GetPinName(pinOut) , oC_GPIO_GetPinName(pinIn));
    }
    if(Argc >= 2)
    {
        if(!oC_AssignErrorCode(&errorCode , oC_GPIO_FindPinByName(Argv[1],&pinOut)))
        {
            oC_PrintErrorMessage("Cannot find output pin:" , errorCode);
        }
    }
    if(Argc >= 3)
    {
        if(!oC_AssignErrorCode(&errorCode , oC_GPIO_FindPinByName(Argv[2],&pinIn)))
        {
            oC_PrintErrorMessage("Cannot find input pin:" , errorCode);
        }
    }

    if(!oC_ErrorOccur(errorCode))
    {
        errorCode = PerformTests(pinOut,pinIn);
    }

    return errorCode;
}

