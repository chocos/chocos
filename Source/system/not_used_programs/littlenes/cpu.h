#ifndef SYSTEM_PROGRAMS_LITTLENES_CPU_H_
#define SYSTEM_PROGRAMS_LITTLENES_CPU_H_

#include <oc_stdtypes.h>
#include <oc_stdio.h>
#include <oc_memory.h>
#include <oc_tgui.h>
#include <joystick.h>
#include <ppu.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define NUMBER_OF_INSTRUCTIONS          256
#define ZERO_PAGE_SIZE                  0x0100
#define STACK_ADDRESS_START             0x0100
#define STACK_ADDRESS_END               0x0200
#define STACK_SIZE                      (STACK_ADDRESS_END - STACK_ADDRESS_START)

#define CPU_RAM_ADDRESS_START               0x0000
#define CPU_RAM_ADDRESS_END                 0x0800
#define CPU_RAM_ADDRESS_MASK                0x07FF
#define CPU_RAM_SIZE                        0x0800
#define CPU_RAM_MIRRORS_ADDRESS_START       0x0800
#define CPU_RAM_MIRRORS_ADDRESS_END         0x2000

#define PPU_REG_ADDRESS_START               0x2000
#define PPU_REG_ADDRESS_END                 0x2008
#define PPU_REG_ADDRESS_MASK                0x0007
#define PPU_REG_SIZE                        0x0008
#define PPU_REG_MIRRORS_ADDRESS_START       0x2008
#define PPU_REG_MIRRORS_ADDRESS_END         0x4000

#define APU_REG_ADDRESS_START               0x4000
#define APU_REG_ADDRESS_END                 0x4020
#define APU_REG_ADDRESS_MASK                0x001F
#define APU_REG_SIZE                        0x0020

#define CARTRIDGE_EXP_ROM_ADDRESS_START     0x4020
#define CARTRIDGE_EXP_ROM_ADDRESS_END       0x6000

#define SRAM_ADDRESS_START                  0x6000
#define SRAM_ADDRESS_END                    0x8000
#define SRAM_ADDRESS_MASK                   0x1FFF
#define SRAM_SIZE                           0x2000

#define PRG0_ROM_ADDRESS_START              0x8000
#define PRG0_ROM_ADDRESS_END                0xC000
#define PRG0_ROM_ADDRESS_MASK               0x3FFF

#define PRG1_ROM_ADDRESS_START              0xC000
#define PRG1_ROM_ADDRESS_END                0x10000
#define PRG1_ROM_ADDRESS_MASK               0x3FFF

#define PPU_OAMDMA_REGISTER_OFFSET          0x4014
#define JOYSTICK_0_REGISTER_OFFSET          0x4016
#define JOYSTICK_1_REGISTER_OFFSET          0x4017

//==========================================================================================================================================
/**
 * @brief List of CPU opcodes
 *
 * To add new OPCODE:
 *      ADD_OPCODE( OPCODE, NAME, ADDRESS_MODE, STRING_NAME, NO_BYTES, NO_CYCLES ) \
 */
//==========================================================================================================================================
#define CPU_OPCODES(ADD_OPCODE)             \
    ADD_OPCODE( 0x00 , BRK , IMPLIED        , "BRK              " , 1 , 7 ) \
    ADD_OPCODE( 0x20 , JSR , ABSOLUTE       , "JSR %02X%02X     " , 3 , 6 ) \
    ADD_OPCODE( 0x40 , RTI , IMPLIED        , "RTI              " , 1 , 6 ) \
    ADD_OPCODE( 0x60 , RTS , IMPLIED        , "RTS              " , 1 , 6 ) \
    ADD_OPCODE( 0x80 , SKB , IMMEDIATE      , "SKB #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xA0 , LDY , IMMEDIATE      , "LDY #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xC0 , CPY , IMMEDIATE      , "CPY *%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xE0 , CPX , IMMEDIATE      , "CPX *%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x01 , ORA , INDIRECT_X     , "ORA (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0x21 , AND , INDIRECT_X     , "AND (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0x41 , EOR , INDIRECT_X     , "EOR (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0x61 , ADC , INDIRECT_X     , "ADC (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0x81 , STA , INDIRECT_X     , "STA (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0xA1 , LDA , INDIRECT_X     , "LDA (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0xC1 , CMP , INDIRECT_X     , "CMP (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0xE1 , SBC , INDIRECT_X     , "SBC (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0x02 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x22 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x42 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x62 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x82 , SKB , IMMEDIATE      , "SKB #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xA2 , LDX , IMMEDIATE      , "LDX #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xC2 , SKB , IMMEDIATE      , "SKB #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xE2 , SKB , IMMEDIATE      , "SKB #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x03 , SLO , INDIRECT_X     , "SLO (%02X,X)     " , 2 , 8 ) \
    ADD_OPCODE( 0x23 , RLA , INDIRECT_X     , "RLA (%02X,X)     " , 2 , 8 ) \
    ADD_OPCODE( 0x43 , SRE , INDIRECT_X     , "SRE (%02X,X)     " , 2 , 8 ) \
    ADD_OPCODE( 0x63 , RRA , INDIRECT_X     , "RRA (%02X,X)     " , 2 , 8 ) \
    ADD_OPCODE( 0x83 , SAX , INDIRECT_X     , "SAX (%02X,X)     " , 2 , 8 ) \
    ADD_OPCODE( 0xA3 , LAX , INDIRECT_X     , "LAX (%02X,X)     " , 2 , 6 ) \
    ADD_OPCODE( 0xC3 , DCP , INDIRECT_X     , "DCP (%02X,X)     " , 2 , 8 ) \
    ADD_OPCODE( 0xE3 , ISB , INDIRECT_X     , "ISB (%02X,X)     " , 2 , 8 ) \
    ADD_OPCODE( 0x04 , IGN , ZERO_PAGE      , "IGN %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x24 , BIT , ZERO_PAGE      , "BIT %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x44 , IGN , ZERO_PAGE      , "IGN %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x64 , IGN , ZERO_PAGE      , "IGN %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x84 , STY , ZERO_PAGE      , "STY %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xA4 , LDY , ZERO_PAGE      , "LDY %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xC4 , CPY , ZERO_PAGE      , "CPY %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xE4 , CPX , ZERO_PAGE      , "CPX %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x05 , ORA , ZERO_PAGE      , "ORA %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x25 , AND , ZERO_PAGE      , "AND %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x45 , EOR , ZERO_PAGE      , "EOR %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x65 , ADC , ZERO_PAGE      , "ADC %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x85 , STA , ZERO_PAGE      , "STA %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xA5 , LDA , ZERO_PAGE      , "LDA %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xC5 , CMP , ZERO_PAGE      , "CMP %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xE5 , SBC , ZERO_PAGE      , "SBC %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0x06 , ASL , ZERO_PAGE      , "ASL %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x26 , ROL , ZERO_PAGE      , "ROL %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x46 , LSR , ZERO_PAGE      , "LSR %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x66 , ROR , ZERO_PAGE      , "ROR %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x86 , STX , ZERO_PAGE      , "STX %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xA6 , LDX , ZERO_PAGE      , "LDX %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xC6 , DEC , ZERO_PAGE      , "DEC %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0xE6 , INC , ZERO_PAGE      , "INC %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x07 , SLO , ZERO_PAGE      , "SLO %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x27 , RLA , ZERO_PAGE      , "RLA %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x47 , SRE , ZERO_PAGE      , "SRE %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x67 , RRA , ZERO_PAGE      , "RRA %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x87 , SAX , ZERO_PAGE      , "SAX %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0xA7 , LAX , ZERO_PAGE      , "LAX %02X         " , 2 , 3 ) \
    ADD_OPCODE( 0xC7 , DCP , ZERO_PAGE      , "DCP %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0xE7 , ISB , ZERO_PAGE      , "ISB %02X         " , 2 , 5 ) \
    ADD_OPCODE( 0x08 , PHP , IMPLIED        , "PHP              " , 1 , 3 ) \
    ADD_OPCODE( 0x28 , PLP , IMPLIED        , "PLP              " , 1 , 4 ) \
    ADD_OPCODE( 0x48 , PHA , IMPLIED        , "PHA              " , 1 , 3 ) \
    ADD_OPCODE( 0x68 , PLA , IMPLIED        , "PLA              " , 1 , 4 ) \
    ADD_OPCODE( 0x88 , DEY , IMPLIED        , "DEY              " , 1 , 2 ) \
    ADD_OPCODE( 0xA8 , TAY , IMPLIED        , "TAY              " , 1 , 2 ) \
    ADD_OPCODE( 0xC8 , INY , IMPLIED        , "INY              " , 1 , 2 ) \
    ADD_OPCODE( 0xE8 , INX , IMPLIED        , "INX              " , 1 , 2 ) \
    ADD_OPCODE( 0x09 , ORA , IMMEDIATE      , "ORA #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x29 , AND , IMMEDIATE      , "AND #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x49 , EOR , IMMEDIATE      , "EOR #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x69 , ADC , IMMEDIATE      , "ADC #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x89 , SKB , IMMEDIATE      , "SKB #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xA9 , LDA , IMMEDIATE      , "LDA #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xC9 , CMP , IMMEDIATE      , "CMP #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xE9 , SBC , IMMEDIATE      , "SBC #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x0A , ASL , ACCUMULATOR    , "ASL A            " , 1 , 2 ) \
    ADD_OPCODE( 0x2A , ROL , ACCUMULATOR    , "ROL A            " , 1 , 2 ) \
    ADD_OPCODE( 0x4A , LSR , ACCUMULATOR    , "LSR A            " , 1 , 2 ) \
    ADD_OPCODE( 0x6A , ROR , ACCUMULATOR    , "ROR A            " , 1 , 2 ) \
    ADD_OPCODE( 0x8A , TXA , IMPLIED        , "TXA              " , 1 , 2 ) \
    ADD_OPCODE( 0xAA , TAX , IMPLIED        , "TAX              " , 1 , 2 ) \
    ADD_OPCODE( 0xCA , DEX , IMPLIED        , "DEX              " , 1 , 2 ) \
    ADD_OPCODE( 0xEA , NOP , IMPLIED        , "NOP              " , 1 , 2 ) \
    ADD_OPCODE( 0x0B , ANC , IMMEDIATE      , "ANC #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x2B , ANC , IMMEDIATE      , "ANC #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x4B , ALR , IMMEDIATE      , "ALR #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x6B , ARR , IMMEDIATE      , "ARR #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x8B , XAA , IMMEDIATE      , "XAA #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xAB , LAX , IMMEDIATE      , "LAX #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xCB , AXS , IMMEDIATE      , "AXS #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0xEB , SBC , IMMEDIATE      , "SBC #%02X        " , 2 , 2 ) \
    ADD_OPCODE( 0x0C , IGN , ABSOLUTE       , "IGN %02X%02X     " , 2 , 4 ) \
    ADD_OPCODE( 0x2C , BIT , ABSOLUTE       , "BIT %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0x4C , JMP , ABSOLUTE       , "JMP %02X%02X     " , 3 , 3 ) \
    ADD_OPCODE( 0x6C , JMP , INDIRECT       , "JMP (%02X)       " , 3 , 5 ) \
    ADD_OPCODE( 0x8C , STY , ABSOLUTE       , "STY %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xAC , LDY , ABSOLUTE       , "LDY %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xCC , CPY , ABSOLUTE       , "CPY %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xEC , CPX , ABSOLUTE       , "CPX %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0x0D , ORA , ABSOLUTE       , "ORA %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0x2D , AND , ABSOLUTE       , "AND %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0x4D , EOR , ABSOLUTE       , "EOR %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0x6D , ADC , ABSOLUTE       , "ADC %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0x8D , STA , ABSOLUTE       , "STA %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xAD , LDA , ABSOLUTE       , "LDA %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xCD , CMP , ABSOLUTE       , "CMP %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xED , SBC , ABSOLUTE       , "SBC %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0x0E , ASL , ABSOLUTE       , "ASL %02X%02X     " , 3 , 6 ) \
    ADD_OPCODE( 0x2E , ROL , ABSOLUTE       , "ROL %02X%02X     " , 3 , 6 ) \
    ADD_OPCODE( 0x4E , LSR , ABSOLUTE       , "LSR %02X%02X     " , 3 , 6 ) \
    ADD_OPCODE( 0x6E , ROR , ABSOLUTE       , "ROR %02X%02X     " , 3 , 6 ) \
    ADD_OPCODE( 0x8E , STX , ABSOLUTE       , "STX %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xAE , LDX , ABSOLUTE       , "LDX %02X%02X     " , 3 , 4 ) \
    ADD_OPCODE( 0xCE , DEC , ABSOLUTE       , "DEC %02X%02X     " , 3 , 6 ) \
    ADD_OPCODE( 0xEE , INC , ABSOLUTE       , "INC %02X%02X     " , 3 , 6 ) \
    ADD_OPCODE( 0x0F , SLO , ABSOLUTE       , "SLO %02X%02X     " , 2 , 6 ) \
    ADD_OPCODE( 0x2F , RLA , ABSOLUTE       , "RLA %02X%02X     " , 2 , 6 ) \
    ADD_OPCODE( 0x4F , SRE , ABSOLUTE       , "SRE %02X%02X     " , 2 , 6 ) \
    ADD_OPCODE( 0x6F , RRA , ABSOLUTE       , "RRA %02X%02X     " , 2 , 6 ) \
    ADD_OPCODE( 0x8F , SAX , ABSOLUTE       , "SAX %02X%02X     " , 2 , 6 ) \
    ADD_OPCODE( 0xAF , LAX , ABSOLUTE       , "LAX %02X%02X     " , 2 , 4 ) \
    ADD_OPCODE( 0xCF , DCP , ABSOLUTE       , "DCP %02X%02X     " , 2 , 6 ) \
    ADD_OPCODE( 0xEF , ISB , ABSOLUTE       , "ISB %02X%02X     " , 2 , 6 ) \
    ADD_OPCODE( 0x10 , BPL , RELATIVE       , "BPL %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0x30 , BMI , RELATIVE       , "BMI %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0x50 , BVC , RELATIVE       , "BVC %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0x70 , BVS , RELATIVE       , "BVS %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0x90 , BCC , RELATIVE       , "BCC %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0xB0 , BCS , RELATIVE       , "BCS %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0xD0 , BNE , RELATIVE       , "BNE %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0xF0 , BEQ , RELATIVE       , "BEQ %02X         " , 2 , 2 ) \
    ADD_OPCODE( 0x11 , ORA , INDIRECT_Y     , "ORA (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0x31 , AND , INDIRECT_Y     , "AND (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0x51 , EOR , INDIRECT_Y     , "EOR (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0x71 , ADC , INDIRECT_Y     , "ADC (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0x91 , STA , INDIRECT_Y     , "STA (%02X),Y     " , 2 , 6 ) \
    ADD_OPCODE( 0xB1 , LDA , INDIRECT_Y     , "LDA (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0xD1 , CMP , INDIRECT_Y     , "CMP (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0xF1 , SBC , INDIRECT_Y     , "SBC (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0x12 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x32 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x52 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x72 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x92 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0xB2 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0xD2 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0xF2 , STP , IMPLIED        , "STP              " , 1 , 2 ) \
    ADD_OPCODE( 0x13 , SLO , INDIRECT_Y     , "SLO (%02X),Y     " , 2 , 8 ) \
    ADD_OPCODE( 0x33 , RLA , INDIRECT_Y     , "RLA (%02X),Y     " , 2 , 8 ) \
    ADD_OPCODE( 0x53 , SRE , INDIRECT_Y     , "SRE (%02X),Y     " , 2 , 8 ) \
    ADD_OPCODE( 0x73 , RRA , INDIRECT_Y     , "RRA (%02X),Y     " , 2 , 8 ) \
    ADD_OPCODE( 0x93 , AHX , INDIRECT_Y     , "AHX (%02X),Y     " , 2 , 8 ) \
    ADD_OPCODE( 0xB3 , LAX , INDIRECT_Y     , "LAX (%02X),Y     " , 2 , 5 ) \
    ADD_OPCODE( 0xD3 , DCP , INDIRECT_Y     , "DCP (%02X),Y     " , 2 , 8 ) \
    ADD_OPCODE( 0xF3 , ISB , INDIRECT_Y     , "ISB (%02X),Y     " , 2 , 8 ) \
    ADD_OPCODE( 0x14 , IGN , ZERO_PAGE_X    , "IGN %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x34 , IGN , ZERO_PAGE_X    , "IGN %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x54 , IGN , ZERO_PAGE_X    , "IGN %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x74 , IGN , ZERO_PAGE_X    , "IGN %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x94 , STY , ZERO_PAGE_Y    , "STY %02X,Y       " , 2 , 4 ) \
    ADD_OPCODE( 0xB4 , LDY , ZERO_PAGE_Y    , "LDY %02X,Y       " , 2 , 4 ) \
    ADD_OPCODE( 0xD4 , IGN , ZERO_PAGE_X    , "IGN %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0xF4 , IGN , ZERO_PAGE_X    , "IGN %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x15 , ORA , ZERO_PAGE_X    , "ORA %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x35 , AND , ZERO_PAGE_X    , "AND %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x55 , EOR , ZERO_PAGE_X    , "EOR %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x75 , ADC , ZERO_PAGE_X    , "ADC %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x95 , STA , ZERO_PAGE_X    , "STA %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0xB5 , LDA , ZERO_PAGE_X    , "LDA %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0xD5 , CMP , ZERO_PAGE_X    , "CMP %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0xF5 , SBC , ZERO_PAGE_X    , "SBC %02X,X       " , 2 , 4 ) \
    ADD_OPCODE( 0x16 , ASL , ZERO_PAGE_X    , "ASL %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x36 , ROL , ZERO_PAGE_X    , "ROL %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x56 , LSR , ZERO_PAGE_X    , "LSR %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x76 , ROR , ZERO_PAGE_X    , "ROR %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x96 , STX , ZERO_PAGE_Y    , "STX %02X,Y       " , 2 , 4 ) \
    ADD_OPCODE( 0xB6 , LDX , ZERO_PAGE_Y    , "LDX %02X,Y       " , 2 , 4 ) \
    ADD_OPCODE( 0xD6 , DEC , ZERO_PAGE_X    , "DEC %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0xF6 , INC , ZERO_PAGE_X    , "INC %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x17 , SLO , ZERO_PAGE_X    , "SLO %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x37 , RLA , ZERO_PAGE_X    , "RLA %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x57 , SRE , ZERO_PAGE_X    , "SRE %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x77 , RRA , ZERO_PAGE_X    , "RRA %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x97 , SAX , ZERO_PAGE_Y    , "SAX %02X,Y       " , 2 , 6 ) \
    ADD_OPCODE( 0xB7 , LAX , ZERO_PAGE_Y    , "LAX %02X,Y       " , 2 , 4 ) \
    ADD_OPCODE( 0xD7 , DCP , ZERO_PAGE_X    , "DCP %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0xF7 , ISB , ZERO_PAGE_X    , "ISB %02X,X       " , 2 , 6 ) \
    ADD_OPCODE( 0x18 , CLC , IMPLIED        , "CLC              " , 1 , 2 ) \
    ADD_OPCODE( 0x38 , SEC , IMPLIED        , "SEC              " , 1 , 2 ) \
    ADD_OPCODE( 0x58 , CLI , IMPLIED        , "CLI              " , 1 , 2 ) \
    ADD_OPCODE( 0x78 , SEI , IMPLIED        , "SEI              " , 1 , 2 ) \
    ADD_OPCODE( 0x98 , TYA , IMPLIED        , "TYA              " , 1 , 2 ) \
    ADD_OPCODE( 0xB8 , CLV , IMPLIED        , "CLV              " , 1 , 2 ) \
    ADD_OPCODE( 0xD8 , CLD , IMPLIED        , "CLD              " , 1 , 2 ) \
    ADD_OPCODE( 0xF8 , SED , IMPLIED        , "SED              " , 1 , 2 ) \
    ADD_OPCODE( 0x19 , ORA , ABSOLUTE_Y     , "ORA %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0x39 , AND , ABSOLUTE_Y     , "AND %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0x59 , EOR , ABSOLUTE_Y     , "EOR %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0x79 , ADC , ABSOLUTE_Y     , "ADC %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0x99 , STA , ABSOLUTE_Y     , "STA %02X%02X,Y   " , 3 , 5 ) \
    ADD_OPCODE( 0xB9 , LDA , ABSOLUTE_Y     , "LDA %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0xD9 , CMP , ABSOLUTE_Y     , "CMP %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0xF9 , SBC , ABSOLUTE_Y     , "SBC %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0x1A , NOP , IMPLIED        , "NOP              " , 1 , 2 ) \
    ADD_OPCODE( 0x3A , NOP , IMPLIED        , "NOP              " , 1 , 2 ) \
    ADD_OPCODE( 0x5A , NOP , IMPLIED        , "NOP              " , 1 , 2 ) \
    ADD_OPCODE( 0x7A , NOP , IMPLIED        , "NOP              " , 1 , 2 ) \
    ADD_OPCODE( 0x9A , TXS , IMPLIED        , "TXS              " , 1 , 2 ) \
    ADD_OPCODE( 0xBA , TSX , IMPLIED        , "TSX              " , 1 , 2 ) \
    ADD_OPCODE( 0xDA , NOP , IMPLIED        , "NOP              " , 1 , 2 ) \
    ADD_OPCODE( 0xFA , NOP , IMPLIED        , "NOP              " , 1 , 2 ) \
    ADD_OPCODE( 0x1B , SLO , ABSOLUTE_Y     , "SLO %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0x3B , RLA , ABSOLUTE_Y     , "RLA %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0x5B , SRE , ABSOLUTE_Y     , "SRE %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0x7B , RRA , ABSOLUTE_Y     , "RRA %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0x9B , TAS , ABSOLUTE_Y     , "TAS %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0xBB , LAS , ABSOLUTE_Y     , "LAS %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0xDB , DCP , ABSOLUTE_Y     , "DCP %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0xFB , ISB , ABSOLUTE_Y     , "ISB %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0x1C , IGN , ABSOLUTE_X     , "IGN %02X%02X,X   " , 3 , 5 ) \
    ADD_OPCODE( 0x3C , IGN , ABSOLUTE_X     , "IGN %02X%02X,X   " , 3 , 5 ) \
    ADD_OPCODE( 0x5C , IGN , ABSOLUTE_X     , "IGN %02X%02X,X   " , 3 , 5 ) \
    ADD_OPCODE( 0x7C , IGN , ABSOLUTE_X     , "IGN %02X%02X,X   " , 3 , 5 ) \
    ADD_OPCODE( 0x9C , SHY , ABSOLUTE_X     , "IGN %02X%02X,X   " , 3 , 5 ) \
    ADD_OPCODE( 0xBC , LDY , ABSOLUTE_Y     , "LDY %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0xDC , IGN , ABSOLUTE_X     , "IGN %02X%02X,X   " , 3 , 5 ) \
    ADD_OPCODE( 0xFC , IGN , ABSOLUTE_X     , "IGN %02X%02X,X   " , 3 , 5 ) \
    ADD_OPCODE( 0x1D , ORA , ABSOLUTE_X     , "ORA %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0x3D , AND , ABSOLUTE_X     , "AND %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0x5D , EOR , ABSOLUTE_X     , "AND %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0x7D , ADC , ABSOLUTE_X     , "AND %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0x9D , STA , ABSOLUTE_X     , "AND %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0xBD , LDA , ABSOLUTE_X     , "LDA %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0xDD , CMP , ABSOLUTE_X     , "CMP %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0xFD , SBC , ABSOLUTE_X     , "SBC %02X%02X,X   " , 3 , 4 ) \
    ADD_OPCODE( 0x1E , ASL , ABSOLUTE_X     , "ASL %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x3E , ROL , ABSOLUTE_X     , "ROL %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x5E , LSR , ABSOLUTE_X     , "LSR %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x7E , ROR , ABSOLUTE_X     , "ROR %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x9E , SHX , ABSOLUTE_Y     , "ROR %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0xBE , LDX , ABSOLUTE_Y     , "LDX %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0xDE , DEC , ABSOLUTE_X     , "DEC %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0xFE , INC , ABSOLUTE_X     , "INC %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x1F , SLO , ABSOLUTE_X     , "SLO %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x3F , RLA , ABSOLUTE_X     , "RLA %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x5F , SRE , ABSOLUTE_X     , "SRE %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x7F , RRA , ABSOLUTE_X     , "RRA %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0x9F , AHX , ABSOLUTE_Y     , "AHX %02X%02X,Y   " , 3 , 7 ) \
    ADD_OPCODE( 0xBF , LAX , ABSOLUTE_Y     , "LAX %02X%02X,Y   " , 3 , 4 ) \
    ADD_OPCODE( 0xDF , DCP , ABSOLUTE_X     , "DCP %02X%02X,X   " , 3 , 7 ) \
    ADD_OPCODE( 0xFF , ISB , ABSOLUTE_X     , "ISB %02X%02X,X   " , 3 , 7 ) \

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef uint8_t     Opcode_t;
typedef uint16_t    CpuAddress_t;

typedef struct
{
    union
    {
        struct
        {
            uint8_t     ZERO_PAGE[ZERO_PAGE_SIZE];
            uint8_t     STACK[STACK_SIZE];
        };
        uint8_t     CPU_RAM[CPU_RAM_SIZE];
    };
    uint8_t     APU_REG[APU_REG_SIZE];
    uint8_t     SRAM[SRAM_SIZE];
    struct
    {
        uint8_t *           ROM;
        uint16_t            Size;
    } PRG0 , PRG1;
} CpuMemoryMap_t;

typedef union
{
#ifdef BIG_ENDIAN
#   error The structure CpuFlagsDecoder_t is not defined for the BIG ENDIAN
#endif
    struct
    {
        uint8_t lu8;
        uint8_t hu8;
    };
    struct
    {
        int8_t  li8;
        int8_t  hi8;
    };
    struct
    {
        uint16_t    _lu8:7;
        uint16_t    NegativeFlag:1;
        uint16_t    CarryFlag:1;
        uint16_t    _rsv:7;
    };
    struct
    {
        uint16_t    b0:1;
        uint16_t    b1:1;
        uint16_t    b2:1;
        uint16_t    b3:1;
        uint16_t    b4:1;
        uint16_t    b5:1;
        uint16_t    b6:1;
        uint16_t    b7:1;
        uint16_t    b8:1;
        uint16_t    b9:1;
        uint16_t    b10:1;
        uint16_t    b11:1;
        uint16_t    b12:1;
        uint16_t    b13:1;
        uint16_t    b14:1;
        uint16_t    b15:1;
    };
    uint16_t    u16;
    int16_t     i16;
} CpuFlagsDecoder_t;

typedef struct
{
    struct
    {
        uint16_t    PC;
        uint8_t     A;
        uint8_t     X;
        uint8_t     Y;
        uint8_t     ST;
        uint8_t     SP;

#if defined(LITTLE_ENDIAN)
        union
        {
            struct
            {
                uint8_t         CarryFlag:1;
                uint8_t         ZeroFlag:1;
                uint8_t         InterruptDisable:1;
                uint8_t         DecimalMode:1;
                uint8_t         BreakCommand:1;
                uint8_t         Reserved:1;
                uint8_t         OverflowFlag:1;
                uint8_t         NegativeFlag:1;
            } ProcessorStatus;
            uint8_t     P;
        };
#elif defined(BIG_ENDIAN)
#   error structure is not defined for BIG_ENDIAN
#else
#   error   ENDIANESS not defined
#endif
    } Registers;

    CpuMemoryMap_t      MemoryMap;
    union
    {
        struct
        {
#ifdef BIG_ENDIAN
#   error Structure is not defined for the BIG_ENDIAN
#endif
            uint8_t         b0:1;
            uint8_t         Reserved:6;
            uint8_t         NegativeFlag:1;
        };
        uint8_t             OpcodeValue;
    };
    CpuAddress_t        OpcodeAddress;
    CpuFlagsDecoder_t   CpuFlagsDecoder;
    uint32_t            CyclesDone;
    uint32_t            CyclesToContinue;
    void *              NesReference;
    void *              PpuReference;
    JoystickKey_t       JoystickKeyToCheck;
} CpuContext_t;

typedef struct
{
    const char *  Name;
    uint16_t      Size;
    uint16_t      Cycles;
    void (*ReadValueFunction) ( CpuContext_t * Cpu );
    void (*PerformInstruction)( CpuContext_t * Cpu );
} InstructionDefinition_t;



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

extern const InstructionDefinition_t InstructionsDefinitions[NUMBER_OF_INSTRUCTIONS];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief reads CPU memory
 *
 * @param Cpu           Pointer to the CPU context structure
 * @param Address       Address to read
 *
 * @return value at the given address
 */
//==========================================================================================================================================
static inline uint8_t CpuMemoryReadByte( CpuContext_t * Cpu , CpuAddress_t Address )
{
    if(Address < CPU_RAM_MIRRORS_ADDRESS_END)
    {
        return Cpu->MemoryMap.CPU_RAM[Address & CPU_RAM_ADDRESS_MASK];
    }
    else if(Address < PPU_REG_MIRRORS_ADDRESS_END)
    {
        return PpuReadRegister(Cpu->PpuReference,Address & PPU_REG_ADDRESS_MASK);
    }
    else if(Address < APU_REG_ADDRESS_END)
    {
        if(Address == JOYSTICK_0_REGISTER_OFFSET)
        {
            return IsJoystickKeyPressed(Cpu->NesReference,JoystickId_Player1,Cpu->JoystickKeyToCheck);
        }
        else if(Address == JOYSTICK_1_REGISTER_OFFSET)
        {
            return IsJoystickKeyPressed(Cpu->NesReference,JoystickId_Player2,Cpu->JoystickKeyToCheck);
        }
        else
        {
            return Cpu->MemoryMap.APU_REG[Address & APU_REG_ADDRESS_MASK];
        }
    }
    else if(Address < CARTRIDGE_EXP_ROM_ADDRESS_END)
    {
        return 0;
    }
    else if(Address < SRAM_ADDRESS_END)
    {
        return Cpu->MemoryMap.SRAM[Address & SRAM_ADDRESS_MASK];
    }
    else if(Address < PRG0_ROM_ADDRESS_END)
    {
        uint16_t offset = Address & PRG0_ROM_ADDRESS_MASK;

        if(offset < Cpu->MemoryMap.PRG0.Size)
        {
            return Cpu->MemoryMap.PRG0.ROM[offset];
        }
        else
        {
            return 0;
        }
    }
    else if(Address < PRG1_ROM_ADDRESS_END)
    {
        uint16_t offset = Address & PRG1_ROM_ADDRESS_MASK;

        if(offset < Cpu->MemoryMap.PRG1.Size)
        {
            return Cpu->MemoryMap.PRG1.ROM[offset];
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}

//==========================================================================================================================================
/**
 * @brief writes CPU memory
 *
 * @param Cpu           Pointer to the CPU context structure
 * @param Address       Address to read
 *
 * @return value at the given address
 */
//==========================================================================================================================================
static inline void CpuMemoryWriteByte( CpuContext_t * Cpu , CpuAddress_t Address , uint8_t Value )
{
    if(Address < CPU_RAM_MIRRORS_ADDRESS_END)
    {
        Cpu->MemoryMap.CPU_RAM[Address & CPU_RAM_ADDRESS_MASK] = Value;
    }
    else if(Address < PPU_REG_MIRRORS_ADDRESS_END)
    {
        PpuWriteRegister(Cpu->PpuReference, Address & PPU_REG_ADDRESS_MASK, Value);
    }
    else if(Address < APU_REG_ADDRESS_END)
    {
        if(Address == PPU_OAMDMA_REGISTER_OFFSET)
        {
            uint16_t address = Value << 8;
            for(uint16_t i = 0 ; i <= 0xFF; i++)
            {
                PpuWriteOamRam(Cpu->PpuReference,i, CpuMemoryReadByte(Cpu,address + i) );
            }
        }
        else
        {
            Cpu->MemoryMap.APU_REG[Address & APU_REG_ADDRESS_MASK] = Value;
        }
    }
    else if(Address < CARTRIDGE_EXP_ROM_ADDRESS_END)
    {
        /* For future use */
    }
    else if(Address < SRAM_ADDRESS_END)
    {
        Cpu->MemoryMap.SRAM[Address & SRAM_ADDRESS_MASK] = Value;
    }
    else if(Address < PRG0_ROM_ADDRESS_END)
    {
        uint16_t offset = Address & PRG0_ROM_ADDRESS_MASK;

        if(offset < Cpu->MemoryMap.PRG0.Size)
        {
            Cpu->MemoryMap.PRG0.ROM[offset] = Value;
        }
    }
    else if(Address < PRG1_ROM_ADDRESS_END)
    {
        uint16_t offset = Address & PRG1_ROM_ADDRESS_MASK;

        if(offset < Cpu->MemoryMap.PRG1.Size)
        {
            Cpu->MemoryMap.PRG1.ROM[offset] = Value;
        }
    }
}


//==========================================================================================================================================
/**
 * @brief reads CPU memory
 *
 * @param Cpu           Pointer to the CPU context structure
 * @param Address       Address to read
 *
 * @return value at the given address
 */
//==========================================================================================================================================
static inline uint16_t CpuMemoryReadWord( CpuContext_t * Cpu , CpuAddress_t Address )
{
    uint16_t lowByte  = CpuMemoryReadByte(Cpu,Address    );
    uint16_t highByte = CpuMemoryReadByte(Cpu,Address + 1);
    return  lowByte | (highByte << 8);
}

//==========================================================================================================================================
/**
 * @brief writes CPU memory
 *
 * @param Cpu           Pointer to the CPU context structure
 * @param Address       Address to read
 *
 * @return value at the given address
 */
//==========================================================================================================================================
static inline void CpuMemoryWriteWord( CpuContext_t * Cpu , CpuAddress_t Address , uint16_t Value )
{
    uint16_t lowByte  = Value & 0xFF;
    uint16_t highByte = Value >> 8;
    CpuMemoryWriteByte( Cpu, Address    , lowByte  );
    CpuMemoryWriteByte( Cpu, Address + 1, highByte );
}

//==========================================================================================================================================
/**
 * @brief loads ROM into the CPU memory
 *
 * @param Cpu           Pointer to the CPU context structure
 * @param Bank0         true if program should be loaded into the Bank 0
 * @param Buffer        Buffer with ROM data
 * @param Size          Size of the buffer
 */
//==========================================================================================================================================
static inline void CpuMemoryLoadProgram( CpuContext_t * Cpu , bool Bank0 , uint8_t * Buffer , oC_MemorySize_t Size )
{
    if(Bank0)
    {
        Cpu->MemoryMap.PRG0.ROM  = Buffer;
        Cpu->MemoryMap.PRG0.Size = Size;
    }
    else
    {
        Cpu->MemoryMap.PRG1.ROM  = Buffer;
        Cpu->MemoryMap.PRG1.Size = Size;
    }
}

//==========================================================================================================================================
/**
 * @brief loads pointer to the PPU registers
 */
//==========================================================================================================================================
static inline void CpuSetPpuPointer( CpuContext_t * Cpu , void * Ppu )
{
    Cpu->PpuReference = Ppu;
}

//==========================================================================================================================================
/**
 * @brief sets NES pointer in the Cpu context
 */
//==========================================================================================================================================
static inline void CpuSetNesPointer( CpuContext_t * Cpu , void * Nes )
{
    Cpu->NesReference = Nes;
}

//==========================================================================================================================================
/**
 * @brief pushes byte into the stack
 */
//==========================================================================================================================================
static inline void CpuPushByteToStack( CpuContext_t * Cpu , uint8_t Value )
{
    Cpu->MemoryMap.STACK[Cpu->Registers.SP] = Value;
    Cpu->Registers.SP--;
}

//==========================================================================================================================================
/**
 * @brief pushes word into the stack
 */
//==========================================================================================================================================
static inline void CpuPushWordToStack( CpuContext_t * Cpu , uint16_t Value )
{
    CpuMemoryWriteWord(Cpu, STACK_ADDRESS_START + Cpu->Registers.SP - 1, Value );
    Cpu->Registers.SP -= 2;
}

//==========================================================================================================================================
/**
 * @brief pops byte from the stack
 */
//==========================================================================================================================================
static inline uint8_t CpuPopByteFromStack( CpuContext_t * Cpu )
{
    Cpu->Registers.SP++;
    return Cpu->MemoryMap.STACK[ Cpu->Registers.SP ];
}

//==========================================================================================================================================
/**
 * @brief pushes word into the stack
 */
//==========================================================================================================================================
static inline uint16_t CpuPopWordFromStack( CpuContext_t * Cpu )
{
    Cpu->Registers.SP += 2;
    return CpuMemoryReadWord(Cpu, STACK_ADDRESS_START + Cpu->Registers.SP - 1);
}

//==========================================================================================================================================
/**
 * @brief returns address of NMI interrupt handler
 */
//==========================================================================================================================================
static inline CpuAddress_t CpuGetNmiInterruptAddress( CpuContext_t * Cpu )
{
    return CpuMemoryReadWord(Cpu,0xFFFA);
}

//==========================================================================================================================================
/**
 * @brief returns address of Reset interrupt handler
 */
//==========================================================================================================================================
static inline CpuAddress_t CpuGetResetInterruptAddress( CpuContext_t * Cpu )
{
    return CpuMemoryReadWord(Cpu,0xFFFC);
}

//==========================================================================================================================================
/**
 * @brief returns address of Reset interrupt handler
 */
//==========================================================================================================================================
static inline CpuAddress_t CpuGetIrqInterruptAddress( CpuContext_t * Cpu )
{
    return CpuMemoryReadWord(Cpu,0xFFFE);
}

//==========================================================================================================================================
/**
 * @brief performs one CPU step
 */
//==========================================================================================================================================
static inline bool CpuPerformStep( CpuContext_t * Cpu )
{
    Opcode_t opcode  = CpuMemoryReadByte(Cpu,Cpu->Registers.PC++);

    InstructionsDefinitions[opcode].ReadValueFunction(Cpu);
    InstructionsDefinitions[opcode].PerformInstruction(Cpu);

    Cpu->CyclesDone += InstructionsDefinitions[opcode].Cycles;

    return true;
}

//==========================================================================================================================================
/**
 * @brief performs one CPU step
 */
//==========================================================================================================================================
static inline bool CpuPerformStepDebugMode( CpuContext_t * Cpu )
{
    oC_TGUI_Key_t key = 0;
    char        opname[30] = {0};
    uint16_t    startPC    = Cpu->Registers.PC;
    uint8_t     lowData    = CpuMemoryReadByte(Cpu,Cpu->Registers.PC + 1);
    uint8_t     highData   = CpuMemoryReadByte(Cpu,Cpu->Registers.PC + 2);
    Opcode_t    opcode     = CpuMemoryReadByte(Cpu,Cpu->Registers.PC++);

    InstructionsDefinitions[opcode].ReadValueFunction(Cpu);
    InstructionsDefinitions[opcode].PerformInstruction(Cpu);

    Cpu->CyclesDone += InstructionsDefinitions[opcode].Cycles;

    printf(oC_VT100_FG_BLUE "%04X: " oC_VT100_FG_WHITE, startPC);
    sprintf(opname,InstructionsDefinitions[opcode].Name, InstructionsDefinitions[opcode].Size == 2 ? lowData : highData, lowData);
    printf(oC_VT100_FG_WHITE "%-20s ", opname);
    printf(oC_VT100_FG_GREEN "; [%02X %02X %02X], PC = %04X, SP = %02X, A = %02X, X = %02X, Y = %02X, P = %02X\n" oC_VT100_FG_WHITE, opcode, lowData, highData,
           Cpu->Registers.PC , Cpu->Registers.SP, Cpu->Registers.A, Cpu->Registers.X , Cpu->Registers.Y, Cpu->Registers.P);

    if(Cpu->CyclesToContinue > 0)
    {
        if( InstructionsDefinitions[opcode].Cycles < Cpu->CyclesToContinue )
        {
            Cpu->CyclesToContinue -= InstructionsDefinitions[opcode].Cycles;
        }
        else
        {
            Cpu->CyclesToContinue = 0;
        }
    }
    else
    {
        do
        {
            oC_TGUI_WaitForKeyPress(&key);

            if(key == 'r')
            {
                unsigned int address = 0;

                printf("Please type address to read: ");
                if(scanf("%x" , &address) == oC_ErrorCode_None)
                {
                    printf("Memory at address %04X: %02X\n", address, CpuMemoryReadByte(Cpu,(uint16_t)address));
                }
            }
            else if(key == 'w')
            {
                unsigned int address = 0;
                unsigned int value   = 0;

                printf("Please type address to write: ");
                scanf("%x" , &address);
                printf("Please type value to write: ");
                scanf("%x" , &value);
                CpuMemoryWriteByte(Cpu,(uint16_t)address,(uint8_t)value);

            }
            else if(key == 'W')
            {
                unsigned int address = 0;
                unsigned int value   = 0;

                printf("Please type PPU address to write: ");
                scanf("%x" , &address);
                printf("Please type value to write: ");
                scanf("%x" , &value);
                PpuMemoryWrite(Cpu->PpuReference,(uint16_t)address,(uint8_t)value);

            }
            else if(key == 'R')
            {
                unsigned int address = 0;

                printf("Please type PPU address to read: ");
                if(scanf("%x" , &address) == oC_ErrorCode_None)
                {
                    printf("Memory at address %04X: %02X\n", address, PpuMemoryRead(Cpu->PpuReference,(uint16_t)address));
                }
            }
            else if(key == 'c')
            {
                do
                {
                    printf("Please type number of cycles to continue: ");
                } while( scanf("%x", &Cpu->CyclesToContinue) != oC_ErrorCode_None );
                break;
            }
            else if(key == 'h')
            {
                DrawPatternTableForDebug(Cpu->PpuReference);
            }
            else if(key == 'd')
            {
                PpuDrawScreen(Cpu->PpuReference);
            }
            else if(key == oC_TGUI_Key_ControlC)
            {
                return false;
            }
        } while(key != 's');
    }
    return true;
}

//==========================================================================================================================================
/**
 * @brief initializes CPU registers
 */
//==========================================================================================================================================
static inline void CpuInitializeRegisters( CpuContext_t * Cpu )
{
    Cpu->Registers.PC = CpuGetResetInterruptAddress(Cpu);
    Cpu->Registers.SP = 0x00;
    Cpu->Registers.SP-= 3;
    Cpu->Registers.A  = 0x00;
    Cpu->Registers.X  = 0x00;
    Cpu->Registers.Y  = 0x00;
    Cpu->Registers.P  = 0x24;
    Cpu->Registers.ProcessorStatus.InterruptDisable = true;

    Cpu->CyclesDone   = 0;
}

//==========================================================================================================================================
/**
 * @brief symulates interrupt
 */
//==========================================================================================================================================
static inline void CpuInterrupt( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.InterruptDisable = 1;
    Cpu->Registers.ProcessorStatus.BreakCommand     = 0;
    CpuPushWordToStack(Cpu,Cpu->Registers.PC);
    CpuPushByteToStack(Cpu,Cpu->Registers.P);
    Cpu->Registers.PC = CpuGetNmiInterruptAddress(Cpu);
}

#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________



#endif /* SYSTEM_PROGRAMS_LITTLENES_CPU_H_ */
