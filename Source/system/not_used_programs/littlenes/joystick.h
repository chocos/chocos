/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          joystick.h
 *
 * @author     Patryk Kubiak - (Created on: 10.05.2017 21:12:35) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PROGRAMS_LITTLENES_JOYSTICK_H_
#define SYSTEM_PROGRAMS_LITTLENES_JOYSTICK_H_

#include <stdbool.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    JoystickKey_Power   = 0 ,
    JoystickKey_A       = 1 ,
    JoystickKey_B       = 2 ,
    JoystickKey_Select  = 3 ,
    JoystickKey_Start   = 4 ,
    JoystickKey_Up      = 5 ,
    JoystickKey_Down    = 6 ,
    JoystickKey_Left    = 7 ,
    JoystickKey_Right   = 8 ,
    JoystickKey_NumberOfKeys = 9 ,
} JoystickKey_t;

typedef enum
{
    JoystickId_Player1 ,
    JoystickId_Player2 ,
    JoystickId_NumberOfPlayers ,
} JoystickId_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern bool IsJoystickKeyPressed( void * Nes, JoystickId_t Player , JoystickKey_t Key );
extern void UpdateKeysState( void * Nes );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________




#endif /* SYSTEM_PROGRAMS_LITTLENES_JOYSTICK_H_ */
