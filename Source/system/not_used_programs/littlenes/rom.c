/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          rom.c
 *
 * @author     Patryk Kubiak - (Created on: 07.05.2017 16:09:20) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <rom.h>
#include <cpu.h>
#include <nes.h>
#include <oc_stdio.h>
#include <oc_tgui.h>
#include <string.h>

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief disassemble the ROM
 */
//==========================================================================================================================================
void RomDisassemble( Rom_t * Rom, oC_MemorySize_t Size )
{
    if( memcmp(Rom->Header.Signature,"NES\x1A",4) != 0 )
    {
        printf("This is not NES file!\n");
    }
    else
    {
        char opname[30];
        printf("Recognized NES file\n"
               "    ROM type             : %04X\n"
               "    Number of PRG modules: %d\n"
               "    Number of CHR modules: %d\n\n"
               , Rom->Header.RomType
               , Rom->Header.PrgBlockCount
               , Rom->Header.ChrBlockCount
               );
        Size -= sizeof(RomHeader_t);

        for(oC_MemorySize_t offset = 0; offset < Size; )
        {
            memset(opname,0,sizeof(opname));

            if(offset % 0x0100 == 0 && offset > 0)
            {
                printf("Press any key to continue...\n");
                oC_TGUI_WaitForKeyPress(NULL);
            }

            uint8_t opcode = Rom->Data[offset];
            printf(oC_VT100_FG_BLUE "%04X " oC_VT100_FG_WHITE, offset);
            if(strlen(InstructionsDefinitions[opcode].Name) == 0 || InstructionsDefinitions[opcode].Size == 0)
            {
                printf(oC_VT100_FG_RED "UNKNOWN INSTRUCTION: %02X %02X %02X\n" oC_VT100_FG_WHITE, opcode, Rom->Data[offset + 1], Rom->Data[offset + 2]);
                offset++;
                printf("Press any key to continue...\n");
                oC_TGUI_WaitForKeyPress(NULL);
                continue;
            }
            else
            {
                sprintf(opname,InstructionsDefinitions[opcode].Name, Rom->Data[offset + 2], Rom->Data[offset + 1]);
            }

            printf(oC_VT100_FG_WHITE "%-20s ", opname);
            printf(oC_VT100_FG_GREEN "; [%02X %02X %02X]\n" oC_VT100_FG_WHITE, opcode, Rom->Data[offset + 1], Rom->Data[offset + 2]);
            offset += InstructionsDefinitions[opcode].Size;
        }
    }
}

//==========================================================================================================================================
/**
 * @brief loads the ROM into the CPU memory
 */
//==========================================================================================================================================
bool LoadRom( Rom_t * Rom , oC_MemorySize_t Size , void * Nes )
{
    Nes_t * nes     = Nes;
    bool    loaded  = false;

    if( memcmp(Rom->Header.Signature,"NES\x1A",4) != 0 )
    {
        printf("This is not NES file!\n");
    }
    else
    {
        printf("Recognized NES file\n"
               "    ROM type             : %04X\n"
               "    Number of PRG modules: %d\n"
               "    Number of CHR modules: %d\n\n"
               , Rom->Header.RomType
               , Rom->Header.PrgBlockCount
               , Rom->Header.ChrBlockCount
               );
        Size -= sizeof(RomHeader_t);

        if(Rom->Header.PrgBlockCount == 0)
        {
            printf("No PRG module detected!\n");
        }
        else if(Rom->Header.ChrBlockCount == 0)
        {
            printf("No CHR module detected!\n");
        }
        else if(Rom->Header.RomType != 0 && Rom->Header.RomType != 3 && Rom->Header.RomType != 1)
        {
            printf("The given ROM type is not supported!\n");
        }
        else
        {
            loaded = LoadRomPointers(&nes->RomLoader,Rom,Size);
            if(loaded == false)
            {
                printf("Cannot load ROM pointers!\n");
            }
            else
            {
                CpuMemoryLoadProgram(&nes->CpuContext, true , nes->RomLoader.PRG[0].Buffer, nes->RomLoader.PRG[0].Size);
                CpuMemoryLoadProgram(&nes->CpuContext, false, nes->RomLoader.PRG[1].Buffer, nes->RomLoader.PRG[1].Size);

                printf("PRG modules loaded\n");

                CpuSetNesPointer(&nes->CpuContext,nes);
                CpuSetPpuPointer(&nes->CpuContext,&nes->PpuContext);

                PpuLoadPatternTable(&nes->PpuContext,nes->RomLoader.CHR[0].Buffer,nes->RomLoader.CHR[0].Size);
                printf("PPU pattern table loaded\n");


                CpuInitializeRegisters(&nes->CpuContext);
                printf("CPU registers initialized\n");

                PpuInitializeRegisters(&nes->PpuContext);
                printf("PPU registers initializes\n");
            }

        }
    }
    return loaded;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________




