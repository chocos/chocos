#include <oc_stdio.h>
#include <oc_stdlib.h>

/* littleNES includes */
#include <cpu.h>
#include <rom.h>
#include <nes.h>

//==========================================================================================================================================
/**
 * @brief prints usage of the program
 */
//==========================================================================================================================================
static void PrintUsage( char * Name )
{
    printf("Usage:  %s path/to/rom.nes [--disassemble]\n", Name);
    printf("   where: \n");
    printf("            --disassemble           - prints list of instructions inside the ROM file\n");
    printf("            --debug                 - runs debug mode\n");
}

//==========================================================================================================================================
/**
 * @brief main function for littleNES
 */
//==========================================================================================================================================
int main(int Argc, char ** Argv )
{
    oC_ErrorCode_t      errorCode   = oC_ErrorCode_CommandNotCorrect;
    FILE *              fp          = NULL;
    oC_MemorySize_t     size        = 0;
    void*               rom         = NULL;
    Nes_t *             nes         = NULL;
    bool                debugMode   = oC_ArgumentOccur(Argc,Argv,"--debug");

    if(Argc < 2)
    {
        PrintUsage(Argv[0]);
        goto NoWithdraw;
    }

    if((fp = fopen(Argv[1],"r")) == NULL)
    {
        printf("Cannot open file %s\n", Argv[1]);
        goto NoWithdraw;
    }

    printf("File size: %d\n", size = fsize(fp));
    if(size == 0)
    {
        printf("File is empty!\n");
        goto CloseFile;
    }

    printf("Allocating memory for a ROM ...\n");
    if((rom = malloc( size, AllocationFlags_ExternalRamFirst )) == NULL)
    {
        printf("Error, cannot allocate %d bytes for ROM image!\n", size);
        goto CloseFile;
    }

    printf("Reading a ROM into the buffer ...\n");
    if(fread(rom,1,size,fp) != size)
    {
        printf("Error, cannot read all ROM data!\n");
        goto ReleaseRom;
    }

    if(oC_ArgumentOccur(Argc,Argv,"--disassemble"))
    {
        RomDisassemble(rom,size);
        errorCode = oC_ErrorCode_None;
        goto ReleaseRom;
    }
    else
    {
        printf("Allocating memory for the NES ...\n");
        Nes_t * nes = malloc(sizeof(Nes_t), AllocationFlags_ZeroFill | AllocationFlags_ExternalRamFirst);
        if(nes == NULL)
        {
            printf("Cannot allocate memory!\n");
            errorCode = oC_ErrorCode_AllocationError;
            goto ReleaseRom;
        }

        if(LoadRom(rom,size,nes) == false)
        {
            printf("Cannot load ROM!\n");
            goto ReleaseNes;
        }

        RunNes(nes,debugMode);
        errorCode = oC_ErrorCode_None;
    }

ReleaseNes:
    free(nes,0);
ReleaseRom:
    free(rom,0);
CloseFile:
    fclose(fp);
NoWithdraw:
    return errorCode;
}
