#include <cpu.h>
#include <oc_tgui.h>

/** ========================================================================================================================================
 *
 *              The section with addressing modes
 *
 *  ======================================================================================================================================*/
#define _________________________________________ADDRESSING_MODES_SECTION___________________________________________________________________

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_IMPLIED( CpuContext_t * Cpu )
{
    /* Nothing to do */
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_IMMEDIATE( CpuContext_t * Cpu )
{
    Cpu->OpcodeValue = CpuMemoryReadByte(Cpu,Cpu->Registers.PC);
    Cpu->Registers.PC++;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_ACCUMULATOR( CpuContext_t * Cpu )
{
    Cpu->OpcodeValue = Cpu->Registers.A;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_ZERO_PAGE( CpuContext_t * Cpu )
{
    Cpu->OpcodeAddress = CpuMemoryReadByte(Cpu,Cpu->Registers.PC);
    Cpu->OpcodeValue   = Cpu->MemoryMap.CPU_RAM[Cpu->OpcodeAddress];
    Cpu->Registers.PC++;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_ZERO_PAGE_X( CpuContext_t * Cpu )
{
    Cpu->OpcodeAddress = (CpuMemoryReadByte(Cpu,Cpu->Registers.PC) + Cpu->Registers.X) & 0xFF;
    Cpu->OpcodeValue   = Cpu->MemoryMap.CPU_RAM[Cpu->OpcodeAddress];
    Cpu->Registers.PC++;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_ZERO_PAGE_Y( CpuContext_t * Cpu )
{
    Cpu->OpcodeAddress = (CpuMemoryReadByte(Cpu,Cpu->Registers.PC) + Cpu->Registers.Y) & 0xFF;
    Cpu->OpcodeValue   = Cpu->MemoryMap.CPU_RAM[Cpu->OpcodeAddress];
    Cpu->Registers.PC++;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_ABSOLUTE( CpuContext_t * Cpu )
{
    Cpu->OpcodeAddress = CpuMemoryReadWord(Cpu,Cpu->Registers.PC);
    Cpu->OpcodeValue   = CpuMemoryReadByte(Cpu,Cpu->OpcodeAddress);
    Cpu->Registers.PC += 2;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_ABSOLUTE_X( CpuContext_t * Cpu )
{
    Cpu->OpcodeAddress = CpuMemoryReadWord(Cpu,Cpu->Registers.PC) + Cpu->Registers.X;
    Cpu->OpcodeValue   = CpuMemoryReadByte(Cpu,Cpu->OpcodeAddress);
    Cpu->Registers.PC += 2;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_ABSOLUTE_Y( CpuContext_t * Cpu )
{
    Cpu->OpcodeAddress = (CpuMemoryReadWord(Cpu,Cpu->Registers.PC) + Cpu->Registers.Y) & 0xFFFF;
    Cpu->OpcodeValue   = CpuMemoryReadByte(Cpu,Cpu->OpcodeAddress);
    Cpu->Registers.PC += 2;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_RELATIVE( CpuContext_t * Cpu )
{
    Cpu->OpcodeAddress = CpuMemoryReadByte(Cpu,Cpu->Registers.PC);
    Cpu->Registers.PC++;

    if(Cpu->OpcodeAddress & 0x80)
    {
        Cpu->OpcodeAddress -= 0x100;
    }
    Cpu->OpcodeAddress += Cpu->Registers.PC;
    Cpu->OpcodeValue   = CpuMemoryReadByte(Cpu,Cpu->OpcodeAddress);
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_INDIRECT( CpuContext_t * Cpu )
{
    uint16_t address = CpuMemoryReadWord(Cpu,Cpu->Registers.PC);

    // Some bug of 6502, when instead of reading $C0FF/$C100 it reads from $C0FF/$C000
    if((address & 0xFF) == 0xFF)
    {
        Cpu->OpcodeAddress = (CpuMemoryReadByte(Cpu,address & 0xFF00) << 8) + CpuMemoryReadByte(Cpu,address);
    }
    else
    {
        Cpu->OpcodeAddress = CpuMemoryReadWord(Cpu,address);
    }
    Cpu->Registers.PC += 2;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_INDIRECT_X( CpuContext_t * Cpu )
{
    CpuAddress_t address = CpuMemoryReadByte(Cpu,Cpu->Registers.PC);
    Cpu->OpcodeAddress  = (CpuMemoryReadByte(Cpu,(address + Cpu->Registers.X + 1) & 0xFF) << 8) | CpuMemoryReadByte(Cpu,(address + Cpu->Registers.X) & 0xFF);
    Cpu->OpcodeValue    = CpuMemoryReadByte(Cpu,Cpu->OpcodeAddress);
    Cpu->Registers.PC++;
}

//==========================================================================================================================================
/**
 * @brief reads value for opcode
 */
//==========================================================================================================================================
static void ReadValue_INDIRECT_Y( CpuContext_t * Cpu )
{
    CpuAddress_t address = CpuMemoryReadByte(Cpu,Cpu->Registers.PC);
    Cpu->OpcodeAddress  = (CpuMemoryReadByte(Cpu,(address + 1) & 0xFF) << 8) | CpuMemoryReadByte(Cpu,(address + Cpu->Registers.Y) & 0xFF);
    Cpu->OpcodeValue    = CpuMemoryReadByte(Cpu,Cpu->OpcodeAddress);
    Cpu->Registers.PC++;
}

#undef  _________________________________________ADDRESSING_MODES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define ShouldOverflowFlagBeSet( Cpu , R )      !!( ~(Cpu->Registers.R ^ Cpu->OpcodeValue) & (Cpu->Registers.R ^ Cpu->CpuFlagsDecoder.lu8) & 0x80 );
#define ShouldCarryFlagBeSet( Cpu )             Cpu->CpuFlagsDecoder.CarryFlag
#define ShouldZeroFlagBeSet( Cpu , R )          !!(Cpu->Registers.R == 0)
#define ShouldNegativeFlagBeSet( Cpu )          Cpu->CpuFlagsDecoder.NegativeFlag

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with instructions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INSTRUCTIONS_SECTION_______________________________________________________________________

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ADC( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.u16                    = Cpu->Registers.A + Cpu->OpcodeValue + Cpu->Registers.ProcessorStatus.CarryFlag;
    Cpu->Registers.ProcessorStatus.CarryFlag    = ShouldCarryFlagBeSet(Cpu);
    Cpu->Registers.ProcessorStatus.OverflowFlag = ShouldOverflowFlagBeSet(Cpu,A);
    Cpu->Registers.A                            = Cpu->CpuFlagsDecoder.lu8;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = ShouldZeroFlagBeSet(Cpu,A);
    Cpu->Registers.ProcessorStatus.NegativeFlag = ShouldNegativeFlagBeSet(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_AND( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.u16                    = Cpu->Registers.A & Cpu->OpcodeValue;
    Cpu->Registers.A                            = Cpu->CpuFlagsDecoder.lu8;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = ShouldZeroFlagBeSet(Cpu,A);
    Cpu->Registers.ProcessorStatus.NegativeFlag = ShouldNegativeFlagBeSet(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ASL( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.u16                    = ((uint16_t)Cpu->OpcodeValue) << 1;
    Cpu->Registers.ProcessorStatus.CarryFlag    = ShouldCarryFlagBeSet(Cpu);
    Cpu->Registers.A                            = Cpu->CpuFlagsDecoder.lu8;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = ShouldZeroFlagBeSet(Cpu,A);
    Cpu->Registers.ProcessorStatus.NegativeFlag = ShouldNegativeFlagBeSet(Cpu);
    CpuMemoryWriteByte(Cpu,Cpu->OpcodeAddress,Cpu->Registers.A);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BCC( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.CarryFlag == 0)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BCS( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.CarryFlag)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BEQ( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.ZeroFlag)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BIT( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8                        = Cpu->Registers.A & Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.ProcessorStatus.OverflowFlag     = Cpu->CpuFlagsDecoder.b6;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BMI( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.NegativeFlag)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BNE( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.ZeroFlag == 0)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BPL( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.NegativeFlag == 0)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BVC( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.OverflowFlag == 0)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BVS( CpuContext_t * Cpu )
{
    if(Cpu->Registers.ProcessorStatus.OverflowFlag)
    {
        Cpu->Registers.PC = Cpu->OpcodeAddress;
    }
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_CLC( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.CarryFlag = 0;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_CLD( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.DecimalMode = 0;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_CLI( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.InterruptDisable = 0;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_CLV( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.OverflowFlag = 0;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_CMP( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.A - Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.CarryFlag    = Cpu->CpuFlagsDecoder.NegativeFlag == 0 ? 1: 0;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_CPX( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.X - Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.CarryFlag    = Cpu->CpuFlagsDecoder.NegativeFlag == 0 ? 1: 0;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_CPY( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.Y - Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.CarryFlag    = Cpu->CpuFlagsDecoder.NegativeFlag == 0 ? 1: 0;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_DEC( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->OpcodeValue - 1;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;

    CpuMemoryWriteByte(Cpu,Cpu->OpcodeAddress,Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_DEX( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8                    = Cpu->Registers.X - 1;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.X                            = Cpu->CpuFlagsDecoder.lu8;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_DEY( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8                    = Cpu->Registers.Y - 1;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.Y                            = Cpu->CpuFlagsDecoder.lu8;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_EOR( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8                    = Cpu->Registers.A & Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.A                            = Cpu->CpuFlagsDecoder.lu8;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_INC( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->OpcodeValue + 1;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;

    CpuMemoryWriteByte(Cpu,Cpu->OpcodeAddress,Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_INX( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8                    = Cpu->Registers.X + 1;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.X                            = Cpu->CpuFlagsDecoder.lu8;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_INY( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8                    = Cpu->Registers.Y + 1;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.Y                            = Cpu->CpuFlagsDecoder.lu8;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_JMP( CpuContext_t * Cpu )
{
    Cpu->Registers.PC = Cpu->OpcodeAddress;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_JSR( CpuContext_t * Cpu )
{
    CpuPushWordToStack(Cpu,Cpu->Registers.PC - 1);
    Cpu->Registers.PC = Cpu->OpcodeAddress;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_LDA( CpuContext_t * Cpu )
{
    Cpu->Registers.A = Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->NegativeFlag;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->OpcodeValue == 0;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_LDX( CpuContext_t * Cpu )
{
    Cpu->Registers.X = Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->NegativeFlag;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->OpcodeValue == 0;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_LDY( CpuContext_t * Cpu )
{
    Cpu->Registers.Y = Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->NegativeFlag;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->OpcodeValue == 0;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_LSR( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->OpcodeValue >> 1;
    Cpu->Registers.ProcessorStatus.CarryFlag    = Cpu->b0;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = 0;

    CpuMemoryWriteByte(Cpu,Cpu->OpcodeAddress,Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
#define PerformInstruction_IGN      PerformInstruction_NOP
#define PerformInstruction_SKB      PerformInstruction_NOP
static void PerformInstruction_NOP( CpuContext_t * Cpu )
{
    // No Operation
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ORA( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.A |= Cpu->OpcodeValue;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.A = Cpu->CpuFlagsDecoder.lu8;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_PHA( CpuContext_t * Cpu )
{
    CpuPushByteToStack(Cpu,Cpu->Registers.A);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_PHP( CpuContext_t * Cpu )
{
    CpuPushByteToStack(Cpu,Cpu->Registers.P);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_PLA( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = CpuPopByteFromStack(Cpu);
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.A = Cpu->CpuFlagsDecoder.lu8;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_PLP( CpuContext_t * Cpu )
{
    Cpu->Registers.P = CpuPopByteFromStack(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ROL( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.u16                    = ((uint16_t)Cpu->OpcodeValue) << 1 | Cpu->Registers.ProcessorStatus.CarryFlag;
    Cpu->Registers.ProcessorStatus.CarryFlag    = Cpu->CpuFlagsDecoder.CarryFlag;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;

    Cpu->Registers.A = Cpu->CpuFlagsDecoder.lu8;

    CpuMemoryWriteByte(Cpu,Cpu->OpcodeAddress,Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ROR( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8                    = Cpu->OpcodeValue >> 1 | (Cpu->Registers.ProcessorStatus.CarryFlag << 7);
    Cpu->Registers.ProcessorStatus.CarryFlag    = Cpu->b0;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;

    Cpu->Registers.A = Cpu->CpuFlagsDecoder.lu8;

    CpuMemoryWriteByte(Cpu,Cpu->OpcodeAddress,Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_RTI( CpuContext_t * Cpu )
{
    Cpu->Registers.P    = CpuPopByteFromStack(Cpu);
    Cpu->Registers.PC   = CpuPopWordFromStack(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_RTS( CpuContext_t * Cpu )
{
    Cpu->Registers.PC   = CpuPopWordFromStack(Cpu) + 1;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SBC( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.u16                    = Cpu->Registers.A - Cpu->OpcodeValue - (Cpu->Registers.ProcessorStatus.CarryFlag) ? 0 : 1;
    Cpu->Registers.ProcessorStatus.CarryFlag    = (Cpu->CpuFlagsDecoder.CarryFlag == 1) ? 0 : 1;
    Cpu->Registers.ProcessorStatus.OverflowFlag = ShouldOverflowFlagBeSet(Cpu,A);

    Cpu->Registers.A = Cpu->CpuFlagsDecoder.lu8;
    Cpu->Registers.ProcessorStatus.ZeroFlag     = Cpu->Registers.A == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag = Cpu->CpuFlagsDecoder.NegativeFlag;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SEC( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.CarryFlag = 1;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SED( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.DecimalMode = 1;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SEI( CpuContext_t * Cpu )
{
    Cpu->Registers.ProcessorStatus.InterruptDisable = 1;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_STA( CpuContext_t * Cpu )
{
    CpuMemoryWriteByte(Cpu, Cpu->OpcodeAddress, Cpu->Registers.A);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_STX( CpuContext_t * Cpu )
{
    CpuMemoryWriteByte(Cpu, Cpu->OpcodeAddress, Cpu->Registers.X);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_STY( CpuContext_t * Cpu )
{
    CpuMemoryWriteByte(Cpu, Cpu->OpcodeAddress, Cpu->Registers.Y);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_TAX( CpuContext_t * Cpu )
{
    Cpu->Registers.X = Cpu->Registers.A;
    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->Registers.X == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = (Cpu->Registers.X) >> 7;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_TAY( CpuContext_t * Cpu )
{
    Cpu->Registers.Y = Cpu->Registers.A;
    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->Registers.Y == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = (Cpu->Registers.Y) >> 7;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_TSX( CpuContext_t * Cpu )
{
    Cpu->Registers.X = Cpu->Registers.SP;
    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->Registers.X == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = (Cpu->Registers.X) >> 7;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_TXA( CpuContext_t * Cpu )
{
    Cpu->Registers.A = Cpu->Registers.X;
    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->Registers.A == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = (Cpu->Registers.A) >> 7;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_TXS( CpuContext_t * Cpu )
{
    Cpu->Registers.SP = Cpu->Registers.X;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_TYA( CpuContext_t * Cpu )
{
    Cpu->Registers.A = Cpu->Registers.Y;
    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->Registers.A == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = (Cpu->Registers.A) >> 7;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_DCP( CpuContext_t * Cpu )
{
    PerformInstruction_DEC(Cpu);
    PerformInstruction_CMP(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ISB( CpuContext_t * Cpu )
{
    PerformInstruction_INC(Cpu);
    PerformInstruction_SBC(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_RLA( CpuContext_t * Cpu )
{
    PerformInstruction_ROL(Cpu);
    PerformInstruction_AND(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_RRA( CpuContext_t * Cpu )
{
    PerformInstruction_ROR(Cpu);
    PerformInstruction_ADC(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SLO( CpuContext_t * Cpu )
{
    PerformInstruction_ASL(Cpu);
    PerformInstruction_ORA(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SRE( CpuContext_t * Cpu )
{
    PerformInstruction_LSR(Cpu);
    PerformInstruction_EOR(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_LAX( CpuContext_t * Cpu )
{
    PerformInstruction_LDA(Cpu);
    PerformInstruction_TAX(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ALR( CpuContext_t * Cpu )
{
    PerformInstruction_AND(Cpu);
    PerformInstruction_LSR(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ANC( CpuContext_t * Cpu )
{
    PerformInstruction_AND(Cpu);
    Cpu->Registers.ProcessorStatus.CarryFlag = Cpu->Registers.ProcessorStatus.NegativeFlag;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_ARR( CpuContext_t * Cpu )
{
    PerformInstruction_AND(Cpu);
    PerformInstruction_ROR(Cpu);
    Cpu->Registers.ProcessorStatus.CarryFlag    = Cpu->CpuFlagsDecoder.b6;
    Cpu->Registers.ProcessorStatus.OverflowFlag = Cpu->CpuFlagsDecoder.b6 ^ Cpu->CpuFlagsDecoder.b5;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_AXS( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = (Cpu->Registers.A & Cpu->Registers.X) - Cpu->OpcodeValue;
    Cpu->Registers.X         = Cpu->CpuFlagsDecoder.lu8;
    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->CpuFlagsDecoder.lu8 == 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = Cpu->CpuFlagsDecoder.NegativeFlag;
    Cpu->Registers.ProcessorStatus.CarryFlag        = Cpu->CpuFlagsDecoder.CarryFlag;
    CpuMemoryWriteByte(Cpu ,Cpu->OpcodeAddress, Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SAX( CpuContext_t * Cpu )
{
    CpuMemoryWriteByte(Cpu ,Cpu->OpcodeAddress, Cpu->Registers.A & Cpu->Registers.X);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_AHX( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.A & Cpu->Registers.X & Cpu->OpcodeValue;
    CpuMemoryWriteByte(Cpu ,Cpu->OpcodeAddress, Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SHX( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.X & Cpu->OpcodeValue;
    CpuMemoryWriteByte(Cpu ,Cpu->OpcodeAddress, Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_SHY( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.Y & Cpu->OpcodeValue;
    CpuMemoryWriteByte(Cpu ,Cpu->OpcodeAddress, Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_LAS( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->OpcodeValue & Cpu->Registers.SP;

    Cpu->Registers.A = Cpu->Registers.X = Cpu->Registers.SP = Cpu->CpuFlagsDecoder.lu8;

    Cpu->Registers.ProcessorStatus.ZeroFlag         = Cpu->CpuFlagsDecoder.lu8 == 0 ? 1 : 0;
    Cpu->Registers.ProcessorStatus.NegativeFlag     = Cpu->CpuFlagsDecoder.NegativeFlag;
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_TAS( CpuContext_t * Cpu )
{
    Cpu->CpuFlagsDecoder.lu8 = Cpu->Registers.A & Cpu->Registers.X;
    Cpu->Registers.SP = Cpu->CpuFlagsDecoder.lu8;
    Cpu->CpuFlagsDecoder.lu8 &= ((Cpu->OpcodeAddress + 1) >> 8);
    CpuMemoryWriteByte(Cpu,Cpu->OpcodeAddress,Cpu->CpuFlagsDecoder.lu8);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_XAA( CpuContext_t * Cpu )
{
    Cpu->Registers.A = Cpu->Registers.X;
    PerformInstruction_AND(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_BRK( CpuContext_t * Cpu )
{
    CpuPushWordToStack(Cpu,Cpu->Registers.PC - 1);
    CpuPushByteToStack(Cpu,Cpu->Registers.P);

    Cpu->Registers.ProcessorStatus.BreakCommand = 1;

    Cpu->Registers.PC = CpuGetNmiInterruptAddress(Cpu);
}

//==========================================================================================================================================
/**
 * @brief performs instruction
 */
//==========================================================================================================================================
static void PerformInstruction_STP( CpuContext_t * Cpu )
{
    printf("STP instruction has been performed - Processor is stopped - Press any key to continue...\n");
    oC_TGUI_WaitForKeyPress(NULL);
}

#undef  _________________________________________INSTRUCTIONS_SECTION_______________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

const InstructionDefinition_t InstructionsDefinitions[NUMBER_OF_INSTRUCTIONS] = {
#define ADD_OPCODE( OPCODE, NAME, ADDRESS_MODE, STRING_NAME, NO_BYTES, NO_CYCLES )      \
    [ OPCODE ] = { .Name = STRING_NAME , .Size = NO_BYTES, .Cycles = NO_CYCLES , .ReadValueFunction = ReadValue_##ADDRESS_MODE, .PerformInstruction = PerformInstruction_##NAME } ,
                CPU_OPCODES(ADD_OPCODE)
#undef ADD_OPCODE
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

