/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          mem.h
 *
 * @author     Patryk Kubiak - (Created on: 07.05.2017 15:31:46) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PROGRAMS_LITTLENES_MEM_H_
#define SYSTEM_PROGRAMS_LITTLENES_MEM_H_

#include <oc_stdtypes.h>
#include <oc_memory.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define ZERO_PAGE_SIZE       0x0100
#define STACK_ADDRESS_START  0x0100
#define STACK_ADDRESS_END    0x0200
#define STACK_SIZE           (STACK_ADDRESS_END - STACK_ADDRESS_START)

#define CPU_RAM_ADDRESS_START               0x0000
#define CPU_RAM_ADDRESS_END                 0x0800
#define CPU_RAM_ADDRESS_MASK                0x07FF
#define CPU_RAM_SIZE                        0x0800
#define CPU_RAM_MIRRORS_ADDRESS_START       0x0800
#define CPU_RAM_MIRRORS_ADDRESS_END         0x2000

#define PPU_RAM_ADDRESS_START               0x2000
#define PPU_RAM_ADDRESS_END                 0x2008
#define PPU_RAM_ADDRESS_MASK                0x0007
#define PPU_RAM_SIZE                        0x0008
#define PPU_RAM_MIRRORS_ADDRESS_START       0x2008
#define PPU_RAM_MIRRORS_ADDRESS_END         0x4000

#define APU_RAM_ADDRESS_START               0x4000
#define APU_RAM_ADDRESS_END                 0x4020
#define APU_RAM_ADDRESS_MASK                0x001F
#define APU_RAM_SIZE                        0x0020

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    union
    {
        struct
        {
            uint8_t     ZeroPage[ZERO_PAGE_SIZE];
            uint8_t     SystemStack[STACK_SIZE];
        };
        uint8_t     RAM[CPU_RAM_SIZE];
    } CPU;

    union
    {
        struct
        {
            uint8_t  PPUCTRL;
            uint8_t  PPUMASK;
            uint8_t  PPUSTATUS;
            uint8_t  OAMADDR;
            uint8_t  OAMDATA;
            uint8_t  PPUSCROLL;
            uint8_t  PPUADDR;
            uint8_t  PPUDATA;
        };
        uint8_t     RAM[PPU_RAM_SIZE];
    } PPU;

    union
    {

    } APU;
} MemoryMap_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_SECTION_____________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static inline uint8_t ReadMemory( MemoryMap_t * Map , uint16_t Address )
{
    if(Address < CPU_RAM_MIRRORS_ADDRESS_END)
    {
        return Map->CPU.RAM[ Address & CPU_RAM_ADDRESS_MASK ];
    }
    else if(Address < PPU_RAM_MIRRORS_ADDRESS_END)
    {
        return Map->PPU.RAM[ Address & PPU_RAM_ADDRESS_MASK ];
    }
    else if(Address < APU_RAM_ADDRESS_END)
    {
        return Map->PPU.RAM[ Address & APU_RAM_ADDRESS_MASK ];
    }
    else
    {
        return 0;
    }
}

#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________




#endif /* SYSTEM_PROGRAMS_LITTLENES_MEM_H_ */
