/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          registers.h
 *
 * @author     Patryk Kubiak - (Created on: 07.05.2017 14:04:51) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PROGRAMS_LITTLENES_REGISTERS_H_
#define SYSTEM_PROGRAMS_LITTLENES_REGISTERS_H_



#endif /* SYSTEM_PROGRAMS_LITTLENES_REGISTERS_H_ */
