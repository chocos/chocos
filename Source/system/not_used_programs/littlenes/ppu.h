/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          ppu.h
 *
 * @author     Patryk Kubiak - (Created on: 07.05.2017 20:59:37) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PROGRAMS_LITTLENES_PPU_H_
#define SYSTEM_PROGRAMS_LITTLENES_PPU_H_

#include <oc_stdtypes.h>
#include <oc_memory.h>
#include <oc_screenman.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define PPU_SCREEN_WIDTH                        256
#define PPU_SCREEN_HEIGHT                       240

#define PPU_PATTERN_TABLE_ADDRESS_START         0x0000
#define PPU_PATTERN_TABLE_ADDRESS_END           0x2000
#define PPU_PATTERN_TABLE_ADDRESS_MASK          0x1FFF
#define PPU_PATTERN_TABLE_SIZE                  0x2000

#define PPU_NAME_TABLE_ADDRESS_START            0x2000
#define PPU_NAME_TABLE_ADDRESS_END              0x3000
#define PPU_NAME_TABLE_ADDRESS_MASK             0x0FFF
#define PPU_NAME_TABLE_SIZE                     0x1000

#define PPU_NAME_TABLE_MIRRORS_ADDRESS_START    0x3000
#define PPU_NAME_TABLE_MIRRORS_ADDRESS_END      0x3F00

#define PPU_IMAGE_PALLETE_RAM_ADDRESS_START     0x3F00
#define PPU_IMAGE_PALLETE_RAM_ADDRESS_END       0x3F10
#define PPU_IMAGE_PALLETE_RAM_ADDRESS_MASK      0x000F
#define PPU_IMAGE_PALLETE_RAM_SIZE              0x0010

#define PPU_SPRITE_PALLETE_RAM_ADDRESS_START    0x3F10
#define PPU_SPRITE_PALLETE_RAM_ADDRESS_END      0x3F20
#define PPU_SPRITE_PALLETE_RAM_ADDRESS_MASK     0x000F
#define PPU_SPRITE_PALLETE_RAM_SIZE             0x0010

#define PPU_PALLETES_RAM_ADDRESS_START          0x3F00
#define PPU_PALLETES_RAM_ADDRESS_END            0x3F20
#define PPU_PALLETES_RAM_ADDRESS_MASK           0x001F
#define PPU_PALLETES_RAM_SIZE                   0x0020

#define PPU_PALLETES_MIRRORS_ADDRESS_START      0x3F20
#define PPU_PALLETES_MIRRORS_ADDRESS_END        0x4000

#define PPU_RAM_SIZE                            (PPU_NAME_TABLE_SIZE + PPU_PALLETES_RAM_SIZE)

#define PPU_OAM_SIZE                            0x0100

#define PPU_RAM_MIRROR_ADDRESS_START            0x4000
#define PPU_RAM_MIRROR_ADDRESS_END              0x10000
#define PPU_RAM_MIRROR_ADDRESS_MASK             0x3FFF

#define PPU_REG_SIZE                            0x0008

#define PPUCTRL_REG_OFFSET                      0x0000
#define PPUMASK_REG_OFFSET                      0x0001
#define PPUSTATUS_REG_OFFSET                    0x0002
#define OAMADDR_REG_OFFSET                      0x0003
#define OAMDATA_REG_OFFSET                      0x0004
#define PPUSCROLL_REG_OFFSET                    0x0005
#define PPUADDR_REG_OFFSET                      0x0006
#define PPUDATA_REG_OFFSET                      0x0007

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef uint16_t PpuAddress_t;

typedef struct
{
    struct
    {
        uint8_t *           Buffer;
        oC_MemorySize_t     Size;
    } PATTERN_TABLE;

    union
    {
        struct
        {
            uint8_t     NAME_TABLE_RAM[PPU_NAME_TABLE_SIZE];
            union
            {
                struct
                {
                    uint8_t IMAGE_PALLETE[PPU_IMAGE_PALLETE_RAM_SIZE];
                    uint8_t SPRITE_PALLETE[PPU_SPRITE_PALLETE_RAM_SIZE];
                };
                uint8_t PALLETES_RAM[PPU_PALLETES_RAM_SIZE];
            };
            uint8_t     OAM_RAM[PPU_OAM_SIZE];
        };
        uint8_t         RAM[PPU_RAM_SIZE];
    };
} PpuMemoryMap_t;

typedef struct
{
    PpuMemoryMap_t              MemoryMap;
    oC_Screen_t                 Screen;
    oC_ColorMap_t *             ColorMap;
    oC_Pixel_Position_t         ScreenPosition;
    oC_Pixel_Position_t         ScreenEndPosition;

    struct
    {
        bool                LowByteAccess;
        union
        {
            struct
            {
#ifndef LITTLE_ENDIAN
#   error The structure is not defined for BIG_ENDIAN!
#endif
                uint8_t         LowByte;
                uint8_t         HighByte;
            };
            PpuAddress_t        Address;
        };
    } CpuMemoryAccess;

    struct
    {
        uint16_t    BaseNameTableOffset;
        bool        AutoIncrementAddress32;
        uint16_t    SpritePatternTableOffset;
        uint16_t    BackgroundPatternTableOffset;
        bool        BigSprite8x16;
    } PpuControl;

    uint8_t         PPUSTATUS;
    uint8_t         PpuRegisters[PPU_REG_SIZE];
} PpuContext_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with static inline functions section
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_FUNCTIONS_SECTION___________________________________________________________________

//==========================================================================================================================================
/**
 * @brief reads PPU memory
 *
 * @param Ppu           Context of the PPU
 * @param Address       Address to read
 *
 * @return value at the given PPU address
 */
//==========================================================================================================================================
static inline uint8_t PpuMemoryRead( PpuContext_t * Ppu , PpuAddress_t Address )
{
    if(Address < PPU_PATTERN_TABLE_ADDRESS_END)
    {
        uint16_t offset = Address & PPU_PATTERN_TABLE_ADDRESS_MASK;

        if(offset < Ppu->MemoryMap.PATTERN_TABLE.Size)
        {
            return Ppu->MemoryMap.PATTERN_TABLE.Buffer[offset];
        }
        else
        {
            return 0;
        }
    }
    else if(Address < PPU_NAME_TABLE_ADDRESS_END)
    {
        return Ppu->MemoryMap.NAME_TABLE_RAM[Address & PPU_NAME_TABLE_ADDRESS_MASK];
    }
    else if(Address < PPU_NAME_TABLE_MIRRORS_ADDRESS_END)
    {
        return Ppu->MemoryMap.NAME_TABLE_RAM[(Address & PPU_NAME_TABLE_ADDRESS_MASK)];
    }
    else if(Address < PPU_PALLETES_MIRRORS_ADDRESS_END)
    {
        return Ppu->MemoryMap.PALLETES_RAM[(Address & PPU_PALLETES_RAM_ADDRESS_MASK)];
    }
    else
    {
        /* The rest of the memory are just mirrors of the PPU memory */
        return PpuMemoryRead(Ppu,Address & PPU_RAM_MIRROR_ADDRESS_MASK);
    }
}

//==========================================================================================================================================
/**
 * @brief writes PPU memory
 *
 * @param Ppu           Context of the PPU
 * @param Address       Address to write
 * @param Value         Value to write
 */
//==========================================================================================================================================
static inline void PpuMemoryWrite( PpuContext_t * Ppu , PpuAddress_t Address , uint8_t Value )
{
//    printf("PPU write: 0x%04X %02X\n", Address, Value);

    if(Address < PPU_PATTERN_TABLE_ADDRESS_END)
    {
        uint16_t offset = Address & PPU_PATTERN_TABLE_ADDRESS_MASK;

        if(offset < Ppu->MemoryMap.PATTERN_TABLE.Size)
        {
            Ppu->MemoryMap.PATTERN_TABLE.Buffer[offset] = Value;
        }
    }
    else if(Address < PPU_NAME_TABLE_ADDRESS_END)
    {
        Ppu->MemoryMap.NAME_TABLE_RAM[Address & PPU_NAME_TABLE_ADDRESS_MASK] = Value;
    }
    else if(Address < PPU_NAME_TABLE_MIRRORS_ADDRESS_END)
    {
        Ppu->MemoryMap.NAME_TABLE_RAM[(Address & PPU_NAME_TABLE_ADDRESS_MASK)] = Value;
    }
    else if(Address < PPU_PALLETES_MIRRORS_ADDRESS_END)
    {
        Ppu->MemoryMap.PALLETES_RAM[(Address & PPU_PALLETES_RAM_ADDRESS_MASK)] = Value;
    }
    else
    {
        /* The rest of the memory are just mirrors of the PPU memory */
        PpuMemoryWrite(Ppu,Address & PPU_RAM_MIRROR_ADDRESS_MASK,Value);
    }
}

//==========================================================================================================================================
/**
 * @brief loads pattern table
 */
//==========================================================================================================================================
static inline void PpuLoadPatternTable( PpuContext_t * Ppu , void * Buffer, oC_MemorySize_t Size )
{
    Ppu->MemoryMap.PATTERN_TABLE.Buffer = Buffer;
    Ppu->MemoryMap.PATTERN_TABLE.Size   = Size;
}

//==========================================================================================================================================
/**
 * @brief initializes PPU registers
 */
//==========================================================================================================================================
static inline void PpuInitializeRegisters( PpuContext_t * Ppu )
{
    Ppu->PPUSTATUS = 0xA0;
    Ppu->Screen    = oC_ScreenMan_GetDefaultScreen();
    Ppu->ScreenPosition.X = 100;
    Ppu->ScreenPosition.Y = 27;
    Ppu->ScreenEndPosition.X = Ppu->ScreenPosition.X + PPU_SCREEN_WIDTH;
    Ppu->ScreenEndPosition.Y = Ppu->ScreenPosition.Y + PPU_SCREEN_HEIGHT;

    oC_Screen_ReadColorMap(Ppu->Screen,&Ppu->ColorMap);
}

//==========================================================================================================================================
/**
 * @brief writes data into the OAM RAM
 */
//==========================================================================================================================================
static inline void PpuWriteOamRam( PpuContext_t * Ppu , uint8_t Offset, uint8_t Data )
{
    Ppu->MemoryMap.OAM_RAM[Offset] = Data;
}


//==========================================================================================================================================
/**
 * @brief draws a tile at the given posistion
 */
//==========================================================================================================================================
static inline void PpuDrawTile( PpuContext_t * Ppu , PpuAddress_t Address, const oC_Color_t * ColorsLookUpTable, bool FullColor, uint16_t StartX, uint16_t StartY )
{
    if(FullColor)
    {
        for(uint8_t y = 0; y < 8 ; y++)
        {
            uint8_t lowByte    = PpuMemoryRead(Ppu,Address + y);
            uint8_t highByte   = PpuMemoryRead(Ppu,Address + y + 8);

            for(uint8_t x = 0; x < 8 ; x++)
            {
                oC_Pixel_Position_t pos;

                pos.X = StartX + x;
                pos.Y = StartY + y;

                uint8_t colorIndex = (( (lowByte  >> (7-x)) & 0x1 ) << 1)
                                   | (( (highByte >> (7-x)) & 0x1 )     );

                oC_ColorMap_SetColor(Ppu->ColorMap,pos,ColorsLookUpTable[colorIndex],Ppu->ColorMap->ColorFormat);
            }
        }
    }
    else
    {
        for(uint8_t y = 0; y < 8 ; y++)
        {
            uint8_t lowByte = PpuMemoryRead(Ppu,Address + y);

            for(uint8_t x = 0; x < 8 ; x++)
            {
                oC_Pixel_Position_t pos;

                pos.X = StartX + x;
                pos.Y = StartY + y;

                oC_ColorMap_SetColor(Ppu->ColorMap,pos,ColorsLookUpTable[((lowByte >> (7-x)) & 0x1 )],Ppu->ColorMap->ColorFormat);
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief draws content of the character table on the screen
 */
//==========================================================================================================================================
static inline void DrawPatternTableForDebug( PpuContext_t * Ppu )
{
    static const oC_Color_t debugColorsTable[4] = {
                    0x000000 ,
                    0xFFFFFF ,
                    0xFF0000 ,
                    0x00FF00
    };
    uint16_t startX = 0, startY = 30;

    for(uint16_t offset = 0; offset < Ppu->MemoryMap.PATTERN_TABLE.Size; offset+=16)
    {
        PpuDrawTile(Ppu,offset,debugColorsTable,true,startX,startY);

        startX += 8;

        if((startX + 8) > Ppu->ColorMap->Width)
        {
            startX  = 0;
            startY += 8;

            if((startY + 8) > Ppu->ColorMap->Height)
            {
                return;
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief clears the screen
 */
//==========================================================================================================================================
static inline void PpuClearScreen( PpuContext_t * Ppu , oC_Color_t Color )
{
    oC_Pixel_Position_t pos;

    for(pos.Y = Ppu->ScreenPosition.Y; pos.Y < Ppu->ScreenEndPosition.Y; pos.Y++)
    {
        for(pos.X = Ppu->ScreenPosition.X; pos.X < Ppu->ScreenEndPosition.X; pos.X++)
        {
            oC_ColorMap_SetColor(Ppu->ColorMap,pos,Color,Ppu->ColorMap->ColorFormat);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief draws screen using current data
 */
//==========================================================================================================================================
static inline void PpuDrawScreen( PpuContext_t * Ppu )
{
    static const oC_Color_t debugColorsTable[4] = {
                    0x000000 ,
                    0xFFFFFF ,
                    0xFF0000 ,
                    0x00FF00
    };

    PpuClearScreen(Ppu, oC_Color_Black);

    static uint16_t baseNameTableOffset = 0;

    for(uint16_t tile_x = 0; tile_x < 32 ; tile_x++)
    {
        for(uint16_t tile_y = 0; tile_y < 30; tile_y++)
        {
//            uint8_t         tile_index = PpuMemoryRead(Ppu,PPU_NAME_TABLE_ADDRESS_START + Ppu->PpuControl.BaseNameTableOffset + tile_x + (tile_y << 5));
            uint8_t         tile_index = PpuMemoryRead(Ppu,PPU_NAME_TABLE_ADDRESS_START + baseNameTableOffset + tile_x + (tile_y << 5));
            PpuAddress_t    address    = Ppu->PpuControl.BackgroundPatternTableOffset + (16 * tile_index);
            PpuDrawTile(Ppu,address,debugColorsTable,true,(tile_x * 8) + Ppu->ScreenPosition.X,(tile_y * 8) + Ppu->ScreenPosition.Y);
        }
    }

    baseNameTableOffset += 0x3E0 / 3;

    Ppu->PPUSTATUS |= 0x80;
}


//==========================================================================================================================================
/**
 * @brief reads PPU register
 */
//==========================================================================================================================================
static inline uint8_t PpuReadRegister( PpuContext_t * Ppu , uint16_t Offset )
{
    uint8_t value = 0xFF;

    if(Offset == PPUDATA_REG_OFFSET)
    {
        value = PpuMemoryRead(Ppu,Ppu->CpuMemoryAccess.Address);
        if(Ppu->PpuControl.AutoIncrementAddress32)
        {
            Ppu->CpuMemoryAccess.Address+=32;
        }
        else
        {
            Ppu->CpuMemoryAccess.Address++;
        }
    }
    else if(Offset == PPUSTATUS_REG_OFFSET)
    {
        Ppu->CpuMemoryAccess.LowByteAccess = false;
        value = Ppu->PPUSTATUS;

        Ppu->PPUSTATUS &= ~0x80;

        PpuDrawScreen(Ppu);
    }
    else
    {
        value = Ppu->PpuRegisters[Offset];
//        printf("PPU read unimplemented register: %02X with value %02X\n", Offset, value);
    }
//
//    printf("PPU read register: %02X: %02X\n", Offset, value);

    return value;
}

//==========================================================================================================================================
/**
 * @brief reads PPU register
 */
//==========================================================================================================================================
static inline void PpuWriteRegister( PpuContext_t * Ppu , uint16_t Offset , uint8_t Value )
{
    if(Offset == PPUCTRL_REG_OFFSET)
    {
        static const uint16_t baseNameTableOffsets[4] = {
                        0x0000 ,
                        0x0400 ,
                        0x0800 ,
                        0x0C00 ,
        };
        Ppu->PpuControl.BaseNameTableOffset             = baseNameTableOffsets[ Value & 0x3 ];
        Ppu->PpuControl.AutoIncrementAddress32          = (Value & 0x04) != 0;
        Ppu->PpuControl.BackgroundPatternTableOffset    = (Value & 0x10) != 0 ? 0x1000 : 0x0000;
        Ppu->PpuControl.BigSprite8x16                   = (Value & 0x20) != 0;
        Ppu->PpuControl.SpritePatternTableOffset        = (Value & 0x20) == 0 ? (Value & 0x08) != 0 ? 0x1000 : 0x0000 : 0x0000;
    }
    else if(Offset == PPUADDR_REG_OFFSET)
    {
        if(Ppu->CpuMemoryAccess.LowByteAccess)
        {
            Ppu->CpuMemoryAccess.LowByte = Value;
        }
        else
        {
            Ppu->CpuMemoryAccess.HighByte = Value;
        }
        Ppu->CpuMemoryAccess.LowByteAccess = Ppu->CpuMemoryAccess.LowByteAccess == false;
    }
    else if(Offset == PPUDATA_REG_OFFSET)
    {
        PpuMemoryWrite(Ppu,Ppu->CpuMemoryAccess.Address - 1,Value);

//        if(Ppu->PpuControl.AutoIncrementAddress32)
//        {
//            Ppu->CpuMemoryAccess.Address+=32;
//        }
//        else
//        {
//            Ppu->CpuMemoryAccess.Address++;
//        }
    }
//    else
//    {
//        printf("PPU write not implemented register: %02X with value %02X\n", Offset, Value);
//    }
//
//    printf("PPU write register: %02X: %02X\n", Offset, Value);
    Ppu->PpuRegisters[Offset] = Value;
}

#undef  _________________________________________INLINE_FUNCTIONS_SECTION___________________________________________________________________



#endif /* SYSTEM_PROGRAMS_LITTLENES_PPU_H_ */
