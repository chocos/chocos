/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          nes.c
 *
 * @author     Patryk Kubiak - (Created on: 10.05.2017 00:31:35) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <nes.h>
#include <oc_stdlib.h>
#include <oc_tgui.h>
#include <oc_frequency.h>

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief runs NES
 */
//==========================================================================================================================================
void RunNes( Nes_t * Nes , bool DebugMode )
{
    oC_TGUI_Key_t key = 0;

    printf("Press Ctrl+C to quit\n");

    oC_Timestamp_t startTimestamp       = gettimestamp();
    oC_Frequency_t frequency            = 0;
    oC_Timestamp_t lastCheckTimestamp   = startTimestamp;
    uint32_t       nextCyclesDraw       = 100000;
    bool (*CpuPerfomStepFunction)(CpuContext_t * Cpu) = DebugMode ? CpuPerformStepDebugMode : CpuPerformStep;

    while(1)
    {
        if(!CpuPerfomStepFunction(&Nes->CpuContext))
        {
            break;
        }
        if(Nes->CpuContext.CyclesDone >=  nextCyclesDraw)
        {
            UpdateKeysState(Nes);
            PpuDrawScreen(&Nes->PpuContext);
            nextCyclesDraw += 100000;
        }
        if(Nes->CpuContext.CyclesDone >= 1000000)
        {
            frequency                    = (oC_Frequency_t)Nes->CpuContext.CyclesDone;
            frequency                   /= gettimestamp() - lastCheckTimestamp;
            Nes->CpuContext.CyclesDone   = 0;
            nextCyclesDraw               = 100000;

            printf("Frequency: %f Hz\n", frequency);
            if(oC_TGUI_CheckKeyPressed(&key) && key == oC_TGUI_Key_ControlC)
            {
                break;
            }
            lastCheckTimestamp           = gettimestamp();
        }
    }

    oC_Timestamp_t endTimestamp         = gettimestamp();
    oC_Time_t      executionTime        = endTimestamp - startTimestamp;

    printf("Number of cycles performed: %lu\n", Nes->CpuContext.CyclesDone);
    printf("Execution time: %f s\n", executionTime);
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________



