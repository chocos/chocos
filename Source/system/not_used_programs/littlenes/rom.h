/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          rom.h
 *
 * @author     Patryk Kubiak - (Created on: 07.05.2017 15:25:06) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_PROGRAMS_LITTLENES_ROM_H_
#define SYSTEM_PROGRAMS_LITTLENES_ROM_H_

#include <oc_stdtypes.h>
#include <oc_memory.h>
#include <oc_stdlib.h>
#include <oc_math.h>
#include <stdio.h>
#include <string.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    char        Signature[4];
    uint8_t     PrgBlockCount;
    uint8_t     ChrBlockCount;
    uint16_t    RomType;
    uint8_t     Reserved[8];
} RomHeader_t;

typedef struct
{
    RomHeader_t         Header;
    uint8_t             Data[1];
} Rom_t;

typedef struct
{
    Rom_t *             Rom;
    oC_MemorySize_t     Size;
    oC_MemorySize_t     Offset;
    struct
    {
        uint8_t *           Buffer;
        oC_MemorySize_t     Size;
    } PRG[8];
    struct
    {
        uint8_t *           Buffer;
        oC_MemorySize_t     Size;
    } CHR[8];
} RomLoader_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________



#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

void RomDisassemble( Rom_t * Rom , oC_MemorySize_t Size );
bool LoadRom       ( Rom_t * Rom , oC_MemorySize_t Size , void * Nes );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief loads pointers into the ROM loader
 */
//==========================================================================================================================================
static inline bool LoadRomPointers( RomLoader_t * Loader, Rom_t * Rom , oC_MemorySize_t Size )
{
    bool success = false;

    memset(Loader,0,sizeof(RomLoader_t));

    Loader->Offset  = 0;
    Loader->Rom     = Rom;
    Loader->Size    = Size;

    if(Rom->Header.PrgBlockCount < oC_ARRAY_SIZE(Loader->PRG))
    {
        success = true;

        for(uint32_t i = 0; i < Rom->Header.PrgBlockCount; i++)
        {
            if(Size > Loader->Offset)
            {
                Loader->PRG[i].Buffer = &Rom->Data[Loader->Offset];
                Loader->PRG[i].Size   = oC_MIN(0x4000,Size - Loader->Offset);
                Loader->Offset       += 0x4000;
            }
            else
            {
                printf("The file is too small! (%d > %d)\n", Size, Loader->Offset);
                success = false;
            }
        }

        if(Rom->Header.PrgBlockCount == 1 && success)
        {
            Loader->PRG[1].Buffer = Loader->PRG[0].Buffer;
            Loader->PRG[1].Size   = Loader->PRG[0].Size;
        }

        if(Rom->Header.ChrBlockCount < oC_ARRAY_SIZE(Loader->CHR))
        {
            for(uint32_t i = 0; i < Rom->Header.ChrBlockCount; i++)
            {
                if(Size > Loader->Offset)
                {
                    Loader->CHR[i].Buffer = &Rom->Data[Loader->Offset];
                    Loader->CHR[i].Size   = oC_MIN(0x2000, Size - Loader->Offset);
                    Loader->Offset       += 0x2000;
                }
                else
                {
                    printf("The file is too small! (%d > %d)\n", Size, Loader->Offset);
                    success = false;
                }
            }
        }
        else
        {
            printf("The number of CHR blocks is too big: %d\n", Rom->Header.ChrBlockCount);
        }
    }
    else
    {
        printf("The number of PRG blocks is too big: %d\n", Rom->Header.PrgBlockCount);
    }

    return success;
}

#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________




#endif /* SYSTEM_PROGRAMS_LITTLENES_ROM_H_ */
