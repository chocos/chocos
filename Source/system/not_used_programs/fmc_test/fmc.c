/** ****************************************************************************************************************************************
 *
 * @file       echo.c
 *
 * @brief      The file contains echo program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_fmc.h>
#include <oc_fmc_chips.h>
#include <oc_mem_lld.h>
#include <oc_processman.h>
#include <oc_struct.h>
#include <oc_tgui.h>

#ifdef oC_FMC_LLD_AVAILABLE

static bool IsMemoryValid( void * Start , oC_UInt_t Size )
{
    bool        memoryOk      = true;
    uint8_t*    memoryArray   = Start;
    oC_UInt_t   sizeStepIndex = 0;
    oC_UInt_t   byteNumber    = 0;
    uint8_t     valueToWrite  = 0;
    bool        writeFF       = false;
    uint8_t     difference    = 0;
    oC_UInt_t   sizeSteps[]   = {
                    kB(100) ,
                    B(1) ,
                    B(1) ,
                    B(1) ,
                    B(1) ,
                    B(4) ,
                    B(4) ,
                    B(40) ,
    };

    for(uint32_t i = 0 ; i < Size ; i+= sizeSteps[sizeStepIndex++])
    {
        byteNumber      = i % 4;
        valueToWrite    = writeFF ? 0xFF : (uint8_t)(i & 0xFF);

        switch(byteNumber)
        {
            case 0: oC_TGUI_SetForegroundColor(oC_TGUI_Color_LightBlue  ); break;
            case 1: oC_TGUI_SetForegroundColor(oC_TGUI_Color_Blue       ); break;
            case 2: oC_TGUI_SetForegroundColor(oC_TGUI_Color_LightGreen ); break;
            case 3: oC_TGUI_SetForegroundColor(oC_TGUI_Color_Yellow     ); break;
        }

        printf("writing address %p with value 0x%02X..." , &memoryArray[i] , (unsigned int)valueToWrite);
        memoryArray[i] = valueToWrite;
        difference     = valueToWrite ^ memoryArray[i];

        if(valueToWrite != memoryArray[i])
        {
            oC_TGUI_SetBackgroundColor(oC_TGUI_Color_Red);
            oC_TGUI_SetForegroundColor(oC_TGUI_Color_White);
        }

        printf("wrote: 0x%02X (%u byte in word), difference = 0x%02X (%d data bit)" , (unsigned int)memoryArray[i] , (unsigned int)byteNumber , (unsigned int)difference , (unsigned int)oC_Bits_GetBitNumberU8(difference));

        oC_TGUI_SetBackgroundColor(oC_TGUI_Color_Black);

        printf("\n\r");

        if(sizeStepIndex >= oC_ARRAY_SIZE(sizeSteps))
        {
            sizeStepIndex = 0;
            writeFF       = !writeFF;
        }
    }

    for(int sec = 30 ; sec >= 0 ; sec-- )
    {
        printf(oC_VT100_SAVE_CURSOR_AND_ATTRS "Waiting %02d seconds before reading..." oC_VT100_RESTORE_CURSOR_AND_ATTRS , sec);
        sleep(1);
    }

    sizeStepIndex = 0;
    writeFF       = false;

    for(uint32_t i = 0 ; i < Size ; i+= sizeSteps[sizeStepIndex++])
    {
        valueToWrite    = writeFF ? 0xFF : (uint8_t)(i & 0xFF);

        printf("reading address %p..." , &memoryArray[i]);
        if(memoryArray[i] != valueToWrite)
        {
            printf(oC_VT100_SAVE_CURSOR_AND_ATTRS oC_VT100_FG_RED "FAILED (%02X != %02X)\n\r"oC_VT100_RESET_ALL_ATTRIBUTES , (unsigned int)memoryArray[i] , (unsigned int)(valueToWrite));
            memoryOk = false;
            break;
        }
        else
        {
            printf(oC_VT100_SAVE_CURSOR_AND_ATTRS oC_VT100_FG_GREEN "OK\n\r"oC_VT100_RESET_ALL_ATTRIBUTES);
        }

        if(sizeStepIndex >= oC_ARRAY_SIZE(sizeSteps))
        {
            sizeStepIndex = 0;
            writeFF       = !writeFF;
        }
    }

    printf(oC_VT100_FG_WHITE "SDRAM refresh verification loop\n");

    for(uint32_t offset = 0; memoryOk && offset < 0xFF; offset++)
    {
        printf("Saving 0x%02X value at address %p..." , (unsigned int)(offset & 0xFF) , &memoryArray[offset]);
        memoryArray[offset] = offset & 0xFF;
        printf("%s (stored 0x%02X)\n" , (memoryArray[offset] == (offset & 0xFF)) ? oC_VT100_FG_GREEN"OK"oC_VT100_FG_WHITE : oC_VT100_FG_RED"FAIL"oC_VT100_FG_WHITE);
        memoryOk = memoryArray[offset] == (offset & 0xFF);
    }

    for(uint32_t i = 0;memoryOk && i<120;i++)
    {
        printf(oC_VT100_SAVE_CURSOR_AND_ATTRS"Refresh verification (%u sec)\n",i);

        for(uint32_t offset = 0; offset < 0xFF; offset++)
        {
            printf("Verification of the 0x%02X value at address %p..." , (unsigned int)(offset & 0xFF) , &memoryArray[offset]);
            printf("%s (stored 0x%02X)\n" , (memoryArray[offset] == (offset & 0xFF)) ? oC_VT100_FG_GREEN"OK"oC_VT100_FG_WHITE : oC_VT100_FG_RED"FAIL"oC_VT100_FG_WHITE);
            memoryOk = memoryOk && memoryArray[offset] == (offset & 0xFF);
        }

        printf("%u seconds left\n" , 120 - i);
        puts(oC_VT100_RESTORE_CURSOR_AND_ATTRS);
        sleep(1);
    }


    printf(oC_VT100_FG_WHITE "2nd SDRAM refresh verification loop\n");

    for(uint32_t offset = 0; memoryOk && offset < 0xFF; offset++)
    {
        uint8_t valueToWrite = (0xFF - (0xFF & offset));
        printf("Saving 0x%02X value at address %p..." , valueToWrite , &memoryArray[offset]);
        memoryArray[offset] = valueToWrite;
        printf("%s (stored 0x%02X)\n" , (memoryArray[offset] == valueToWrite) ? oC_VT100_FG_GREEN"OK"oC_VT100_FG_WHITE : oC_VT100_FG_RED"FAIL"oC_VT100_FG_WHITE);
        memoryOk = memoryArray[offset] == valueToWrite;
    }

    for(uint32_t i = 0;memoryOk && i<120;i++)
    {
        printf(oC_VT100_SAVE_CURSOR_AND_ATTRS"Refresh verification (%u sec)\n",i);

        for(uint32_t offset = 0; offset < 0xFF; offset++)
        {
            uint8_t valueToWrite = (0xFF - (0xFF & offset));
            printf("Verification of the 0x%02X value at address %p..." , valueToWrite , &memoryArray[offset]);
            printf("%s (stored 0x%02X)\n" , (memoryArray[offset] == valueToWrite) ? oC_VT100_FG_GREEN"OK"oC_VT100_FG_WHITE : oC_VT100_FG_RED"FAIL"oC_VT100_FG_WHITE);
            memoryOk = memoryOk && memoryArray[offset] == valueToWrite;
        }

        printf("%u seconds left\n" , 120 - i);
        puts(oC_VT100_RESTORE_CURSOR_AND_ATTRS);
        sleep(1);
    }

    return memoryOk;
}


//==========================================================================================================================================
/**
 * @brief main fmc function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    static const oC_FMC_Config_t Config = {
                    .HeapUsage                           = oC_FMC_HeapUsage_UseAsHeapIfPossible,
                    .ChipInfo                            = &oC_FMC_ChipInfo_MT48LC4M32B2 ,
                    .MaximumTimeForConfiguration         = s(3) ,
                    .SDRAM.Protection                    = oC_FMC_Protection_Default ,
                    .SDRAM.Timeout                       = s(3) ,
                    .SDRAM.DataBusWidth                  = oC_FMC_LLD_DataBusWidth_16Bits , /* Forcing data bus width to 16 bits */
                    .SDRAM.Pins.SDCLK                    = oC_Pin_FMC_SDCLK,
                    .SDRAM.Pins.SDCKE[0]                 = oC_Pin_FMC_SDCKE0,
                    .SDRAM.Pins.SDCKE[1]                 = oC_Pin_FMC_SDCKE1,
                    .SDRAM.Pins.SDNE[0]                  = oC_Pin_FMC_SDNE0,
                    .SDRAM.Pins.SDNE[1]                  = oC_Pin_FMC_SDNE1,
                    .SDRAM.Pins.A[ 0]                    = oC_Pin_FMC_A0,
                    .SDRAM.Pins.A[ 1]                    = oC_Pin_FMC_A1,
                    .SDRAM.Pins.A[ 2]                    = oC_Pin_FMC_A2,
                    .SDRAM.Pins.A[ 3]                    = oC_Pin_FMC_A3,
                    .SDRAM.Pins.A[ 4]                    = oC_Pin_FMC_A4,
                    .SDRAM.Pins.A[ 5]                    = oC_Pin_FMC_A5,
                    .SDRAM.Pins.A[ 6]                    = oC_Pin_FMC_A6,
                    .SDRAM.Pins.A[ 7]                    = oC_Pin_FMC_A7,
                    .SDRAM.Pins.A[ 8]                    = oC_Pin_FMC_A8,
                    .SDRAM.Pins.A[ 9]                    = oC_Pin_FMC_A9,
                    .SDRAM.Pins.A[10]                    = oC_Pin_FMC_A10,
                    .SDRAM.Pins.A[11]                    = oC_Pin_FMC_A11,
                    .SDRAM.Pins.A[12]                    = oC_Pin_FMC_A12,
                    .SDRAM.Pins.D[ 0]                    = oC_Pin_FMC_DQ0,
                    .SDRAM.Pins.D[ 1]                    = oC_Pin_FMC_DQ1,
                    .SDRAM.Pins.D[ 2]                    = oC_Pin_FMC_DQ2,
                    .SDRAM.Pins.D[ 3]                    = oC_Pin_FMC_DQ3,
                    .SDRAM.Pins.D[ 4]                    = oC_Pin_FMC_DQ4,
                    .SDRAM.Pins.D[ 5]                    = oC_Pin_FMC_DQ5,
                    .SDRAM.Pins.D[ 6]                    = oC_Pin_FMC_DQ6,
                    .SDRAM.Pins.D[ 7]                    = oC_Pin_FMC_DQ7,
                    .SDRAM.Pins.D[ 8]                    = oC_Pin_FMC_DQ8,
                    .SDRAM.Pins.D[ 9]                    = oC_Pin_FMC_DQ9,
                    .SDRAM.Pins.D[10]                    = oC_Pin_FMC_DQ10,
                    .SDRAM.Pins.D[11]                    = oC_Pin_FMC_DQ11,
                    .SDRAM.Pins.D[12]                    = oC_Pin_FMC_DQ12,
                    .SDRAM.Pins.D[13]                    = oC_Pin_FMC_DQ13,
                    .SDRAM.Pins.D[14]                    = oC_Pin_FMC_DQ14,
                    .SDRAM.Pins.D[15]                    = oC_Pin_FMC_DQ15,
                    .SDRAM.Pins.D[16]                    = oC_Pin_FMC_DQ16,
                    .SDRAM.Pins.D[17]                    = oC_Pin_FMC_DQ17,
                    .SDRAM.Pins.D[18]                    = oC_Pin_FMC_DQ18,
                    .SDRAM.Pins.D[19]                    = oC_Pin_FMC_DQ19,
                    .SDRAM.Pins.D[20]                    = oC_Pin_FMC_DQ20,
                    .SDRAM.Pins.D[21]                    = oC_Pin_FMC_DQ21,
                    .SDRAM.Pins.D[22]                    = oC_Pin_FMC_DQ22,
                    .SDRAM.Pins.D[23]                    = oC_Pin_FMC_DQ23,
                    .SDRAM.Pins.D[24]                    = oC_Pin_FMC_DQ24,
                    .SDRAM.Pins.D[25]                    = oC_Pin_FMC_DQ25,
                    .SDRAM.Pins.D[26]                    = oC_Pin_FMC_DQ26,
                    .SDRAM.Pins.D[27]                    = oC_Pin_FMC_DQ27,
                    .SDRAM.Pins.D[28]                    = oC_Pin_FMC_DQ28,
                    .SDRAM.Pins.D[29]                    = oC_Pin_FMC_DQ29,
                    .SDRAM.Pins.D[30]                    = oC_Pin_FMC_DQ30,
                    .SDRAM.Pins.D[31]                    = oC_Pin_FMC_DQ31,
                    .SDRAM.Pins.BA[0]                    = oC_Pin_FMC_BA0,
                    .SDRAM.Pins.BA[1]                    = oC_Pin_FMC_BA1,
                    .SDRAM.Pins.BA[2]                    = oC_Pin_FMC_BA2,
                    .SDRAM.Pins.BA[3]                    = oC_Pin_FMC_BA3,
                    .SDRAM.Pins.NRAS                     = oC_Pin_FMC_SDNRAS,
                    .SDRAM.Pins.NCAS                     = oC_Pin_FMC_SDNCAS,
                    .SDRAM.Pins.SDNWE                    = oC_Pin_FMC_SDNWE,
                    .SDRAM.Pins.NBL[0]                   = oC_Pin_FMC_NBL0,
                    .SDRAM.Pins.NBL[1]                   = oC_Pin_FMC_NBL1,
                    .SDRAM.Pins.NBL[2]                   = oC_Pin_FMC_NBL2,
                    .SDRAM.Pins.NBL[3]                   = oC_Pin_FMC_NBL3,
    };
    oC_ErrorCode_t                  errorCode   = oC_ErrorCode_ImplementError;
    oC_FMC_Context_t                context     = NULL;
    uint8_t*                        externalRam = NULL;
    oC_MemorySize_t                 size        = 0;

    errorCode = oC_FMC_Configure(&Config,&context);

    if(oC_ErrorOccur(errorCode))
    {
        oC_PrintErrorMessage("Cannot configure FMC: " , errorCode);
    }
    else
    {
        printf("Configuration success!\n\r" oC_VT100_RESET_ALL_ATTRIBUTES);

        errorCode = oC_FMC_ReadDirectAddress(context,(void**)&externalRam,&size);

        if(oC_ErrorOccur(errorCode))
        {
            oC_PrintErrorMessage("Cannot read direct access: " , errorCode);
        }
        else
        {
            printf("Read direct access success!\n\r"
                   "Address = %p \n\r"
                   "Size    = %u B\n\r" ,
                   externalRam ,
                   (unsigned int)size
                   );

            IsMemoryValid(externalRam,size);
        }

//        oC_SaveIfErrorOccur("FMC: unconfiguration error: " , oC_FMC_Unconfigure(&Config,&context));
        printf("Press any key to continue...\n");
        oC_TGUI_WaitForKeyPress(NULL);
    }

    return 0;
}

#endif
