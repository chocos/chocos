/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      The tcolors program displays all available terminal colors
 *
 * @author     Patryk Kubiak - (Created on: 2016-05-08 - 13:02:05) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_tgui.h>
#include <oc_array.h>

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
	oC_TGUI_Style_t style;
	int maxInRow    = 10;
	int inRowIndex  = 0;

	style.DontDraw  = false;
    style.TextStyle = oC_TGUI_TextStyle_Default;

	oC_ARRAY_FOREACH_IN_ARRAY(oC_TGUI_Colors,backgroundColor)
	{
        inRowIndex = 0;

        style.Background = oC_TGUI_Color_Black;
        style.Foreground = oC_TGUI_Color_White;
        printf("\n\r");
        oC_TGUI_SetStyle(&style);
        printf("======================================================== Background: %13s ========================================================\n\r" , backgroundColor->Name );

        style.Background = backgroundColor->Color;

        oC_ARRAY_FOREACH_IN_ARRAY(oC_TGUI_Colors,foregroundColor)
        {
            style.Foreground = foregroundColor->Color;

            if(style.Background == style.Foreground)
            {
                continue;
            }

            oC_TGUI_SetStyle(&style);

            printf("%-13s " , foregroundColor->Name);

            inRowIndex++;
            if(inRowIndex == maxInRow)
            {
                printf("\n\r");
                inRowIndex = 0;
            }
        }
	}

    style.Background = oC_TGUI_Color_Black;
    oC_TGUI_SetStyle(&style);
    printf("\n\r");

    oC_TGUI_ResetDevice();

    return 0;
}
