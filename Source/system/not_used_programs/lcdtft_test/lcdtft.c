/** ****************************************************************************************************************************************
 *
 * @file       lcdtft.c
 *
 * @brief      The file contains lcdtft_test program source
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 16:16:18)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_lcdtft.h>
#include <oc_lcdtft_lld.h>
#include <oc_time.h>
#include <oc_system.h>
#include <oc_colormap.h>
#include <oc_string.h>
#include <oc_screenman.h>
#ifdef oC_LCDTFT_LLD_AVAILABLE

//==========================================================================================================================================
/**
 * @brief main echo function
 *
 * This is the main entry of the program. It is called by the system
 *
 * @param argc      Argument counter, number of elements in the argv array
 * @param argv      Array with program arguments
 *
 * @return
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t      errorCode       = oC_ErrorCode_ImplementError;
    oC_ColorMap_t*      colorMap        = NULL;
    oC_Color_t          color           = 0;
    oC_Screen_t         screen          = oC_ScreenMan_GetDefaultScreen();

    printf(oC_VT100_RESET_ALL_ATTRIBUTES "Searching of the LCD screen...");

    if(ErrorCondition(oC_Screen_IsCorrect(screen) , oC_ErrorCode_ObjectNotCorrect))
    {
        printf(oC_VT100_FG_GREEN " OK\n\r");
        printf(oC_VT100_RESET_ALL_ATTRIBUTES "Read color map...");

        if(oC_AssignErrorCode(&errorCode , oC_Screen_ReadColorMap(screen,&colorMap)))
        {
            printf(oC_VT100_FG_GREEN " OK\n\r");

            oC_Pixel_Position_t position = {0,0};

            color = oC_Color_ConvertToFormat(oC_Color_Blue,oC_ColorFormat_ARGB8888,colorMap->ColorFormat);

            if(Argc > 1)
            {
                int len = strlen(Argv[1]);
                for(int i = 0 ; i < len ; i++)
                {
                    oC_ColorMap_DrawChar(colorMap,position,color,colorMap->ColorFormat,Argv[1][i],oC_Font_(Consolas));
                    position.X += 10;
                    if(position.X >= colorMap->Width)
                    {
                        position.X = 0;
                        position.Y += 20;
                    }
                }
            }
            else
            {
                oC_ColorMap_DrawChar(colorMap,position,color,colorMap->ColorFormat,'H',oC_Font_(Consolas));
                position.X += 20;
                oC_ColorMap_DrawChar(colorMap,position,color,colorMap->ColorFormat,'E',oC_Font_(Consolas));
                position.X += 20;
                oC_ColorMap_DrawChar(colorMap,position,color,colorMap->ColorFormat,'L',oC_Font_(Consolas));
                position.X += 20;
                oC_ColorMap_DrawChar(colorMap,position,color,colorMap->ColorFormat,'L',oC_Font_(Consolas));
                position.X += 20;
                oC_ColorMap_DrawChar(colorMap,position,color,colorMap->ColorFormat,'O',oC_Font_(Consolas));
                position.X += 20;
            }

            color = oC_Color_ConvertToFormat(oC_Color_Red,oC_ColorFormat_ARGB8888,colorMap->ColorFormat);

            for(int8_t i = 10 ; i > 0 ; i--)
            {
                printf(oC_VT100_SAVE_CURSOR "Delay...%2d s\n" oC_VT100_RESTORE_CURSOR_AND_ATTRS  , i);
                sleep(s(1));
            }

            for(position.X = 0; position.X < colorMap->Width; position.X++)
            {
                for(position.Y = 0; position.Y < colorMap->Height; position.Y++)
                {
                    oC_ColorMap_SetColor(colorMap,position,color,colorMap->ColorFormat);
                }
            }

            for(int8_t i = 10 ; i > 0 ; i--)
            {
                printf(oC_VT100_SAVE_CURSOR "Delay...%2d s\n" oC_VT100_RESTORE_CURSOR_AND_ATTRS  , i);
                sleep(s(1));
            }

            color = oC_Color_ConvertToFormat(oC_Color_Black,oC_ColorFormat_ARGB8888,colorMap->ColorFormat);

            for(position.X = 0; position.X < colorMap->Width; position.X++)
            {
                for(position.Y = 0; position.Y < colorMap->Height; position.Y++)
                {
                    oC_ColorMap_SetColor(colorMap,position,color,colorMap->ColorFormat);
                }
            }
        }
        else
        {
            printf(oC_VT100_FG_RED "Failed!\n\r");
            oC_PrintErrorMessage("Cannot read color map: " , errorCode);

        }

    }
    else
    {
        printf(oC_VT100_FG_RED "Failed!\n\r");
        oC_PrintErrorMessage("screen not correct: " , errorCode);
    }

    return errorCode;
}

#endif
