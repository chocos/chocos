/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the http_server program
 *
 * @author     Patryk Kubiak - (Created on: 2016-12-13 - 18:35:52) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_tcp.h>
#include <oc_tgui.h>

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
	oC_Tcp_Server_t     server          = NULL;
	oC_Tcp_Connection_t connection      = NULL;
	oC_ErrorCode_t      errorCode       = oC_ErrorCode_ImplementError;
	char                tempString[2000]= {0};
	oC_Net_Address_t    remoteAddress;
	oC_Net_Address_t    address         = {
	                .Type           = oC_Net_AddressType_IPv4 ,
	                .IPv4           = 0 ,
	                .Port           = oC_Tcp_Port_HTTP ,
	                .Protocol       = oC_Net_Protocol_TCP
	};

	printf("Preparing server ...\n");

	if(ErrorCode(oC_Tcp_Listen( &address, &server, 6, min(5) )))
	{
	    oC_TGUI_Key_t key = 0;

	    while(oC_TGUI_CheckKeyPressed(&key) == false || key != oC_TGUI_Key_ControlC )
	    {
            printf("Waiting for connections...");

	        memset(tempString, 0, sizeof(tempString));

	        if(ErrorCode( oC_Tcp_Accept(server, &connection, min(5)) ))
	        {
                oC_Tcp_Connection_ReadRemote(connection, &remoteAddress);
                oC_Net_AddressToString(&remoteAddress, tempString, sizeof(tempString));
                printf("Received connection from %s\nWaiting for packet...", tempString);

                oC_MemorySize_t size = sizeof(tempString);
                if(ErrorCode(oC_Tcp_Receive(connection,tempString,&size, s(10))))
                {
                    printf("Packet has been received, opening index.html\n");
                    FILE * fp = fopen("/flash/index.html", "r");

                    if(ErrorCondition(fp != NULL, oC_ErrorCode_PathNotCorrect))
                    {
                        printf("file has been opened\nSending...");

                        memset(tempString,0,sizeof(tempString));

                        sprintf(tempString, "HTTP/1.0 200 OK \n"
                                            "Server: ChocoOS \n"
                                            "Content-Length: %d \n"
                                            "Content-Type: text/html \n\n",
                                            155);

                        fread(&tempString[strlen(tempString)], 1, sizeof(tempString), fp);

                        if(ErrorCode(oC_Tcp_Send(connection,tempString, strlen(tempString) + 1, s(10))))
                        {
                            printf("index has been sent\n");
                        }
                        else
                        {
                            printf(oC_VT100_FG_RED"%R\n"oC_VT100_FG_WHITE, errorCode);
                        }
                    }

                    fclose(fp);
                }
                else
                {
                    printf(oC_VT100_FG_RED"%R\n"oC_VT100_FG_WHITE, errorCode);
                }
	        }
	        else
	        {
	            printf(oC_VT100_FG_RED"%R\n"oC_VT100_FG_WHITE, errorCode);
	        }
	    }
	}
	else
	{
	    oC_PrintErrorMessage("Cannot start server - ", errorCode);
	}
	
    return errorCode;
}
