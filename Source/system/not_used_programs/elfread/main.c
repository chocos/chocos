/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the elfread program
 *
 * @author     Patryk Kubiak - (Created on: 2017-06-23 - 20:33:03) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_elf.h>
#include <oc_string.h>

#error This program needs to be re-implemented after changes in oc_elf.h module (the oC_Elf_t structure is not public anymore)

//==========================================================================================================================================
/**
 * @brief prints program's usage
 */
//==========================================================================================================================================
static void PrintUsage( int Argc, char ** Argv )
{
    printf("Usage: %s file_path\n", Argv[0]);
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
    oC_ErrorCode_t              errorCode          = oC_ErrorCode_ImplementError;
    char *                      filePath           = Argc >= 2 ? Argv[1] : NULL;
	oC_Elf_SectionHeader_t *    sectionHeader      = NULL;
	oC_Elf_SymbolTableEntry_t * symbolEntry        = NULL;
	oC_Elf_RelocationEntry_t *  relocationEntry    = NULL;
	oC_Elf_ProgramHeader_t *    programHeader      = NULL;
	oC_Elf_Word_t               symbolIndex        = 0;
	oC_Elf_Word_t               sectionIndex       = 0;
	oC_Elf_Word_t               relocationIndex    = 0;
	oC_Elf_Word_t               programIndex       = 0;
	oC_Elf_File_t               elfFile;
	oC_DefaultString_t          flagsString        = {0};


	if(Argc < 2)
	{
	    PrintUsage(Argc,Argv);
	}
	else
	{
        if( ErrorCode( oC_Elf_LoadFile(filePath,elfFile) ) )
        {
            printf("ELF Type:       %s (0x%X)\n"    , oC_Elf_GetTypeString(elfFile.FileHeader->Type)        , elfFile.FileHeader->Type      );
            printf("Target machine: %s (0x%X)\n"    , oC_Elf_GetMachineString(elfFile.FileHeader->Machine)  , elfFile.FileHeader->Machine   );
            printf("Version:        0x%X\n"         , elfFile.FileHeader->Version                           );
            printf("Entry address:  0x%X\n"         , elfFile.FileHeader->Entry                             );
            printf("Flags:          0x%08X\n"       , elfFile.FileHeader->Flags                             );
            printf("Header size:    %d\n"           , elfFile.FileHeader->HeaderSize                        );

            printf("\nProgram Header Table [PHT]: \n");
            printf("       Offset:                      0x%X\n"     , elfFile.FileHeader->ProgramHeaderTableOffset          );
            printf("       Entry-Size:                  %d\n"       , elfFile.FileHeader->ProgramHeaderTableEntrySize       );
            printf("       Number-Of-Entries:           %d\n"       , elfFile.FileHeader->ProgramHeaderTableNumberOfEntries );

            printf("\nSection Header Table [SHT]: \n");
            printf("       Offset:                      0x%X\n"     , elfFile.FileHeader->SectionHeaderTableOffset          );
            printf("       Entry-Size:                  %d\n"       , elfFile.FileHeader->SectionHeaderTableEntrySize       );
            printf("       Number-Of-Entries:           %d\n"       , elfFile.FileHeader->SectionHeaderTableNumberOfEntries );
            printf("       Name-String-Table Index:     %d\n"       , elfFile.FileHeader->SectionHeaderNameStringTableIndex );

            /* Printing section headers */
            while( (sectionHeader = oC_Elf_GetSectionHeader(&elfFile,sectionIndex++)) != NULL )
            {
                printf("====================================\n");
                printf(" Section IDX: %d\n", sectionIndex - 1);
                printf("    Name:           %s\n"           , oC_Elf_GetSectionEntry(&elfFile,elfFile.SectionHeaderNameString,sectionHeader->NameIndex));
                printf("    Type:           %s\n"           , oC_Elf_GetSectionTypeString(sectionHeader->Type));
                printf("    Flags:          %s (0x%08X)\n"  , oC_Elf_GetSectionFlagsString(sectionHeader->Flags,flagsString,sizeof(flagsString)), sectionHeader->Flags);
                printf("    Address:        0x%08X\n"       , sectionHeader->Address);
                printf("    Offset:         0x%X\n"         , sectionHeader->Offset);
                printf("    Size:           %d B\n"         , sectionHeader->Size);
                printf("    Link:           0x%08X\n"       , sectionHeader->Link);
                printf("    Info:           0x%08X\n"       , sectionHeader->Info);
                printf("    Align:          %d\n"           , sectionHeader->AddressAlign);
                printf("    Entry-Size:     %d\n"           , sectionHeader->EntrySize);

                if(
                    sectionHeader->Type == oC_Elf_SectionType_REL
                 || sectionHeader->Type == oC_Elf_SectionType_RELA
                    )
                {
                    printf("\n   Relocation entries: \n");

                    relocationIndex = 0;

                    printf("%-5s | %-10s | %-10s | %-10s | %-10s\n", "IDX", "Offset", "Symbol IDX", "Type", "Addend");

                    while( (relocationEntry = oC_Elf_GetSectionEntry(&elfFile,sectionHeader,relocationIndex++)) != NULL )
                    {
                        printf("%-10s | %-10s | %-10s \n",
                               relocationIndex - 1 ,
                               relocationEntry->Offset,
                               oC_ELF_GET_RELOCATION_SYMBOL_INDEX(relocationEntry->Info),
                               oC_ELF_GET_RELOCATION_TYPE(relocationEntry->Info));
                    }
                }
            }

            symbolIndex = 1;

            printf("----------------------------------------------\n");
            printf("\n\nSymbols in .symtab\n\n");

            printf("%-30s | %-10s | %-10s | %-11s | %-9s | %-9s | %-10s\n"
                  , "Name", "Type", "Bind","Value", "Size", "Other", "Index");

            while( (symbolEntry = oC_Elf_GetSectionEntry(elfFile,elfFile.symtab,symbolIndex++)) != NULL )
            {
                printf("%-30s |"   , oC_Elf_GetSectionEntry(elfFile,elfFile.strtab,symbolEntry->NameIndex));
                printf(" %-10s |"  , oC_Elf_GetSymbolTypeString( oC_ELF_GET_SYMBOL_TYPE( symbolEntry->Info) ), oC_ELF_GET_SYMBOL_TYPE( symbolEntry->Info) );
                printf(" %-10s |"  , oC_Elf_GetSymbolBindingString( oC_ELF_GET_SYMBOL_BIND( symbolEntry->Info) ), oC_ELF_GET_SYMBOL_BIND( symbolEntry->Info) );
                printf(" 0x%-8X |" , symbolEntry->Value );
                printf(" %-8d |"   , symbolEntry->Size  );
                printf(" %-8d |"   , symbolEntry->Other );
                printf(" 0x%08X\n" , symbolEntry->Index );
            }

            symbolIndex = 1;

            printf("----------------------------------------------\n");
            printf("\n\nSymbols in .dynsym\n\n");

            printf("%-30s | %-10s | %-10s | %-11s | %-9s | %-9s | %-10s\n"
                  , "Name", "Type", "Bind", "Value", "Size", "Other", "Index");

            while( (symbolEntry = oC_Elf_GetSectionEntry(elfFile,elfFile.dynsym,symbolIndex++)) != NULL )
            {
                printf("%-30s |"   , oC_Elf_GetSectionEntry(elfFile,elfFile.dynstr,symbolEntry->NameIndex));
                printf(" %-10s |"  , oC_Elf_GetSymbolTypeString( oC_ELF_GET_SYMBOL_TYPE( symbolEntry->Info) ), oC_ELF_GET_SYMBOL_TYPE( symbolEntry->Info) );
                printf(" %-10s |"  , oC_Elf_GetSymbolBindingString( oC_ELF_GET_SYMBOL_BIND( symbolEntry->Info) ), oC_ELF_GET_SYMBOL_BIND( symbolEntry->Info) );
                printf(" 0x%-8X |"  , symbolEntry->Value );
                printf(" %-8d |"    , symbolEntry->Size  );
                printf(" %-8d |"    , symbolEntry->Other );
                printf(" 0x%-08X\n" , symbolEntry->Index );
            }

            programIndex = 0;

            printf("\n----------------------------------------------\n");
            printf("\nProgram headers\n\n");
            printf(" %-5s | %-10s | %-11s |" " %-11s | %-11s |"" %-11s "  "| %-11s "   "| %-11s | %-5s \n",
                   "IDX" , "Type","Offset", "VADDR" , "PADDR", "FSIZE (B)", "MSIZE (B)", "Flags", "Align");

            while( (programHeader = oC_Elf_GetProgramHeader(elfFile,programIndex++)) != NULL )
            {
                printf("%5d |"      , programIndex - 1 );
                printf(" %-10s |"   , oC_Elf_GetProgramTypeString(programHeader->Type));
                printf(" 0x%-8X |"  , programHeader->Offset);
                printf(" 0x%-8X |"  , programHeader->VirtualAddress);
                printf(" 0x%-8X |"  , programHeader->PhysicalAddress);
                printf(" %-10d |"  , programHeader->FileSize);
                printf(" %-10d |"  , programHeader->MemorySize);
                printf(" 0x%-8X |"  , programHeader->Flags);
                printf(" %-5d |"  , programHeader->Align);
                printf("\n");
            }
        }
        else
        {
            printf("Cannot load file %s: %R\n", filePath, errorCode);
        }
	}


    return errorCode;
}
