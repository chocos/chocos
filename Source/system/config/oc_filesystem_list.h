/** ****************************************************************************************************************************************
 *
 * @file       oc_filesystem_list.h
 *
 * @brief      The file with configuration of the file systems.
 *
 * @author     Patryk Kubiak - (Created on: 4 10 2015 12:22:41)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CONFIG_OC_FILESYSTEM_LIST_H_
#define SYSTEM_CONFIG_OC_FILESYSTEM_LIST_H_

#include <oc_cfg.h>

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_PATH_DEFAULT_MEDIA_MOUNT_PATH       "/media/"

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_LIST_FILE_SYSTEMS(INCLUDE,DONT_INCLUDE) \
    INCLUDE(DevFs) \
    INCLUDE(RamFs) \
    INCLUDE(FlashFs) \

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_LIST_MOUNTED_FILE_SYSTEMS(MOUNT) \
    MOUNT("RamFs"  , "" , "/"         ) \
    MOUNT("DevFs"  , "" , "/dev/"     ) \
    MOUNT("FlashFs", "" , "/flash/"   ) \

#endif /* SYSTEM_CONFIG_OC_FILESYSTEM_LIST_H_ */
