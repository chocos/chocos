/** ****************************************************************************************************************************************
 *
 * @file       oc_cfg.h
 *
 * @brief      Helper macros for configurations files
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 22:29:15) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup CFG Configuration
 * @{
 * To see details check #CFG
 * @page CFG
 * The ChocoOS system contains many configure definitions, that can be set to some kind of variables by user. All configuration files
 * are placed in the <b>config</b> directory. <br />
 * <br />
 * There are many definitions to set, but following rules helps you to quickly learn how to use them. First of all note, that
 * each configuration definition begins from <b>CFG_</b> prefix, for example #CFG_ENABLE_ASSERTIONS. This is easy way to distinguish
 * the system configuration from other definitions. Second part of configuration names is a type of a definition, for example <b>ENABLE</b>
 * in the #CFG_ENABLE_DEBUG_PRINT definition. Thanks to that you will know, what values can you assign to the definition. <br />
 * Here you have list of configuration types:<br />
 *              Type    | Values            | Description
 *              --------|-------------------|------------------------------------------------------------------------
 *              ENABLE  | ON / OFF          | Enables or disables functionality.
 *              LEVEL   | LEVEL( Lvl )      | Determines level of a functionality. To set you should use macro LEVEL
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_OC_CFG_H_
#define SYSTEM_CORE_OC_CFG_H_

#include <stdbool.h>
#include <oc_frequency.h>
#include <oc_time.h>

/**
 * @brief Function is turned on
 */
#define ON      2
/**
 * @brief Function is turned off
 */
#define OFF     1

/**
 * @brief Function is set to LEVEL
 */
#define LEVEL(Lvl)          (Lvl)

/**
 * @brief Number of percent
 */
#define PERCENT(Percent)    ((float)Percent/(float)100)

/**
 * @brief Number of bytes
 */
#define B(Bytes)            (Bytes)

/**
 * @brief Number of kB
 */
#define kB(kBytes)          (kBytes * 1024)

/**
 * @brief Number of MB
 */
#define MB(MBytes)          (MBytes * kB(1024))

/**
 * @brief Number of GB
 */
#define GB(GBytes)          (GBytes * MB(1024ULL))

/**
 * @brief Number of TB
 */
#define TB(TBytes)          (TBytes * GB(1024))

/**
 * @brief Number of Hz
 */
#define Hz(Freq)            (oC_Hz(Freq))

/**
 * @brief Number of kHz
 */
#define kHz(Freq)           (oC_kHz(Freq))

/**
 * @brief Number of MHz
 */
#define MHz(Freq)           (oC_MHz(Freq))

/**
 * @brief Number of GHz
 */
#define GHz(Freq)           (oC_GHz(Freq))

/**
 * @brief Number of ns
 */
#define ns(time)            (oC_ns(time))

/**
 * @brief Number of us
 */
#define us(time)            (oC_us(time))

/**
 * @brief Number of ms
 */
#define ms(time)            (oC_ms(time))

/**
 * @brief Number of s
 */
#define s(time)             (oC_s(time))

/**
 * @brief Number of min
 */
#define min(time)           (oC_min(time))

/**
 * @brief Number of hour
 */
#define hour(time)          (oC_hour(time))

/**
 * @brief Number of day
 */
#define day(time)           (oC_day(time))


/**
 * @}
 */

#define UNIVERSAL_DONT_ADD(...)

#endif /* SYSTEM_CORE_OC_CFG_H_ */
