/** ****************************************************************************************************************************************
 *
 * @file       oc_pinsmap.h
 *
 * @brief      The file with configuration of pins for lm4f120h5qr machine
 *
 * @author     Patryk Kubiak - (Created on: 29 08 2015 17:10:43)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CONFIG_LM4F120H5QR_OC_PINSMAP_H_
#define SYSTEM_CONFIG_LM4F120H5QR_OC_PINSMAP_H_
#ifdef LM4F120H5QR


#define oC_CFG_PINS(ADD_PIN)    \
    ADD_PIN( RED_LED    , PF1 ) \
    ADD_PIN( BLUE_LED   , PF2 ) \
    ADD_PIN( GREEN_LED  , PF3 ) \
    ADD_PIN( SW1        , PF0 ) \
    ADD_PIN( SW2        , PF4 ) \
    ADD_PIN( UART_RX    , PC4 ) \
    ADD_PIN( UART_TX    , PC5 ) \
    ADD_PIN( GPIO_TEST_PIN_OUT  , NotUsed ) \
    ADD_PIN( GPIO_TEST_PIN_IN   , NotUsed ) \
    ADD_PIN( LCDTFT_R0          , NotUsed ) \
    ADD_PIN( LCDTFT_R1          , NotUsed ) \
    ADD_PIN( LCDTFT_R2          , NotUsed ) \
    ADD_PIN( LCDTFT_R3          , NotUsed ) \
    ADD_PIN( LCDTFT_R4          , NotUsed ) \
    ADD_PIN( LCDTFT_R5          , NotUsed ) \
    ADD_PIN( LCDTFT_R6          , NotUsed ) \
    ADD_PIN( LCDTFT_R7          , NotUsed ) \
    ADD_PIN( LCDTFT_G0          , NotUsed ) \
    ADD_PIN( LCDTFT_G1          , NotUsed ) \
    ADD_PIN( LCDTFT_G2          , NotUsed ) \
    ADD_PIN( LCDTFT_G3          , NotUsed ) \
    ADD_PIN( LCDTFT_G4          , NotUsed ) \
    ADD_PIN( LCDTFT_G5          , NotUsed ) \
    ADD_PIN( LCDTFT_G6          , NotUsed ) \
    ADD_PIN( LCDTFT_G7          , NotUsed ) \
    ADD_PIN( LCDTFT_B0          , NotUsed ) \
    ADD_PIN( LCDTFT_B1          , NotUsed ) \
    ADD_PIN( LCDTFT_B2          , NotUsed ) \
    ADD_PIN( LCDTFT_B3          , NotUsed ) \
    ADD_PIN( LCDTFT_B4          , NotUsed ) \
    ADD_PIN( LCDTFT_B5          , NotUsed ) \
    ADD_PIN( LCDTFT_B6          , NotUsed ) \
    ADD_PIN( LCDTFT_B7          , NotUsed ) \
    ADD_PIN( LCDTFT_VSYNC       , NotUsed ) \
    ADD_PIN( LCDTFT_HSYNC       , NotUsed ) \
    ADD_PIN( LCDTFT_CLK         , NotUsed ) \
    ADD_PIN( LCDTFT_DE          , NotUsed ) \
    ADD_PIN( LCDTFT_BL_CTRL     , NotUsed ) \
    ADD_PIN( LCDTFT_DISP        , NotUsed ) \
    ADD_PIN( FMC_A0             , NotUsed ) \
    ADD_PIN( FMC_A1             , NotUsed ) \
    ADD_PIN( FMC_A2             , NotUsed ) \
    ADD_PIN( FMC_A3             , NotUsed ) \
    ADD_PIN( FMC_A4             , NotUsed ) \
    ADD_PIN( FMC_A5             , NotUsed ) \
    ADD_PIN( FMC_A6             , NotUsed ) \
    ADD_PIN( FMC_A7             , NotUsed ) \
    ADD_PIN( FMC_A8             , NotUsed ) \
    ADD_PIN( FMC_A9             , NotUsed ) \
    ADD_PIN( FMC_A10            , NotUsed ) \
    ADD_PIN( FMC_A11            , NotUsed ) \
    ADD_PIN( FMC_A12            , NotUsed ) \
    ADD_PIN( FMC_BA0            , NotUsed ) \
    ADD_PIN( FMC_BA1            , NotUsed ) \
    ADD_PIN( FMC_BA2            , NotUsed ) \
    ADD_PIN( FMC_BA3            , NotUsed ) \
    ADD_PIN( FMC_SDNWE          , NotUsed ) \
    ADD_PIN( FMC_SDNCAS         , NotUsed ) \
    ADD_PIN( FMC_SDNRAS         , NotUsed ) \
    ADD_PIN( FMC_SDNE0          , NotUsed ) \
    ADD_PIN( FMC_SDNE1          , NotUsed ) \
    ADD_PIN( FMC_SDCKE0         , NotUsed ) \
    ADD_PIN( FMC_SDCKE1         , NotUsed ) \
    ADD_PIN( FMC_SDCLK          , NotUsed ) \
    ADD_PIN( FMC_NBL0           , NotUsed ) \
    ADD_PIN( FMC_NBL1           , NotUsed ) \
    ADD_PIN( FMC_NBL2           , NotUsed ) \
    ADD_PIN( FMC_NBL3           , NotUsed ) \
    ADD_PIN( FMC_DQ0            , NotUsed ) \
    ADD_PIN( FMC_DQ1            , NotUsed ) \
    ADD_PIN( FMC_DQ2            , NotUsed ) \
    ADD_PIN( FMC_DQ3            , NotUsed ) \
    ADD_PIN( FMC_DQ4            , NotUsed ) \
    ADD_PIN( FMC_DQ5            , NotUsed ) \
    ADD_PIN( FMC_DQ6            , NotUsed ) \
    ADD_PIN( FMC_DQ7            , NotUsed ) \
    ADD_PIN( FMC_DQ8            , NotUsed ) \
    ADD_PIN( FMC_DQ9            , NotUsed ) \
    ADD_PIN( FMC_DQ10           , NotUsed ) \
    ADD_PIN( FMC_DQ11           , NotUsed ) \
    ADD_PIN( FMC_DQ12           , NotUsed ) \
    ADD_PIN( FMC_DQ13           , NotUsed ) \
    ADD_PIN( FMC_DQ14           , NotUsed ) \
    ADD_PIN( FMC_DQ15           , NotUsed ) \
    ADD_PIN( FMC_DQ16           , NotUsed ) \
    ADD_PIN( FMC_DQ17           , NotUsed ) \
    ADD_PIN( FMC_DQ18           , NotUsed ) \
    ADD_PIN( FMC_DQ19           , NotUsed ) \
    ADD_PIN( FMC_DQ20           , NotUsed ) \
    ADD_PIN( FMC_DQ21           , NotUsed ) \
    ADD_PIN( FMC_DQ22           , NotUsed ) \
    ADD_PIN( FMC_DQ23           , NotUsed ) \
    ADD_PIN( FMC_DQ24           , NotUsed ) \
    ADD_PIN( FMC_DQ25           , NotUsed ) \
    ADD_PIN( FMC_DQ26           , NotUsed ) \
    ADD_PIN( FMC_DQ27           , NotUsed ) \
    ADD_PIN( FMC_DQ28           , NotUsed ) \
    ADD_PIN( FMC_DQ29           , NotUsed ) \
    ADD_PIN( FMC_DQ30           , NotUsed ) \
    ADD_PIN( FMC_DQ31           , NotUsed ) \
    ADD_PIN( RMII_REF_CLK       , NotUsed ) \
    ADD_PIN( RMII_MDIO          , NotUsed ) \
    ADD_PIN( RMII_RCS_DV        , NotUsed ) \
    ADD_PIN( RMII_MDC           , NotUsed ) \
    ADD_PIN( RMII_RXD0          , NotUsed ) \
    ADD_PIN( RMII_RXD1          , NotUsed ) \
    ADD_PIN( RMII_TX_EN         , NotUsed ) \
    ADD_PIN( RMII_TXD0          , NotUsed ) \
    ADD_PIN( RMII_TXD1          , NotUsed ) \
    ADD_PIN( RMII_RXER          , NotUsed ) \
    ADD_PIN( FT5336_SCL         , NotUsed ) \
    ADD_PIN( FT5336_SDA         , NotUsed ) \
    ADD_PIN( FT5336_IT          , NotUsed ) \
    ADD_PIN( I2C_TCAudio_SCL    , NotUsed ) \
    ADD_PIN( I2C_TCAudio_SDA    , NotUsed ) \

#endif
#endif /* SYSTEM_CONFIG_LM4F120H5QR_OC_PINSMAP_H_ */
