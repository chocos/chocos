/** ****************************************************************************************************************************************
 *
 * @file       oc_drivers_list.h
 *
 * @brief      The file with drivers list configuration
 *
 * @author     Patryk Kubiak - (Created on: 15 09 2015 18:51:43)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CONFIG_OC_DRIVERS_LIST_H_
#define SYSTEM_CONFIG_OC_DRIVERS_LIST_H_

#include <oc_cfg.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of drivers to include to the system
 */
//==========================================================================================================================================
#define CFG_LIST_DRIVERS(TURN_ON,TURN_OFF)    \
    TURN_OFF(TIMER) \
    TURN_ON(GPIO) \
    TURN_ON(UART) \
    TURN_OFF(PWM) \
    TURN_ON(LED) \
    TURN_ON(LCDTFT) \
    TURN_ON(FMC) \
    TURN_ON(ETH) \
    TURN_ON(GTD) \
    TURN_ON(I2C) \
    TURN_ON(FT5336) \
    TURN_ON(SPI) \
    TURN_ON(SDMMC) \

#endif /* SYSTEM_CONFIG_OC_DRIVERS_LIST_H_ */
