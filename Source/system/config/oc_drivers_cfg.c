/** ****************************************************************************************************************************************
 *
 * @file       oc_drivers_cfg.c
 *
 * @brief      File with configuration of drivers
 *
 * @author     Patryk Kubiak - (Created on: 13.07.2016 21:47:57) 
 *
 * @copyright       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_driverman.h>

/** ========================================================================================================================================
 *
 *              The section with configurations of drivers
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

#ifdef oC_FMC_LLD_AVAILABLE
//==========================================================================================================================================
/**
 * Structure with configuration of the SDRAM in the STM32F7Discovery board
 */
//==========================================================================================================================================
static const oC_FMC_Config_t STM32F7Discovery_SDRAMConfig = {
    .HeapUsage                           = oC_FMC_HeapUsage_UseAsHeapIfPossible ,
    .ChipInfo                            = &oC_FMC_ChipInfo_MT48LC4M32B2 ,
    .MaximumTimeForConfiguration         = s(3) ,
    .SDRAM.Protection                    = oC_FMC_Protection_Default ,
    .SDRAM.Timeout                       = s(3) ,
    .SDRAM.DataBusWidth                  = oC_FMC_LLD_DataBusWidth_16Bits , /* Forcing data bus width to 16 bits */
    .SDRAM.Pins.SDCLK                    = oC_Pin_FMC_SDCLK,
    .SDRAM.Pins.SDCKE[0]                 = oC_Pin_FMC_SDCKE0,
    .SDRAM.Pins.SDCKE[1]                 = oC_Pin_FMC_SDCKE1,
    .SDRAM.Pins.SDNE[0]                  = oC_Pin_FMC_SDNE0,
    .SDRAM.Pins.SDNE[1]                  = oC_Pin_FMC_SDNE1,
    .SDRAM.Pins.A[ 0]                    = oC_Pin_FMC_A0,
    .SDRAM.Pins.A[ 1]                    = oC_Pin_FMC_A1,
    .SDRAM.Pins.A[ 2]                    = oC_Pin_FMC_A2,
    .SDRAM.Pins.A[ 3]                    = oC_Pin_FMC_A3,
    .SDRAM.Pins.A[ 4]                    = oC_Pin_FMC_A4,
    .SDRAM.Pins.A[ 5]                    = oC_Pin_FMC_A5,
    .SDRAM.Pins.A[ 6]                    = oC_Pin_FMC_A6,
    .SDRAM.Pins.A[ 7]                    = oC_Pin_FMC_A7,
    .SDRAM.Pins.A[ 8]                    = oC_Pin_FMC_A8,
    .SDRAM.Pins.A[ 9]                    = oC_Pin_FMC_A9,
    .SDRAM.Pins.A[10]                    = oC_Pin_FMC_A10,
    .SDRAM.Pins.A[11]                    = oC_Pin_FMC_A11,
    .SDRAM.Pins.A[12]                    = oC_Pin_FMC_A12,
    .SDRAM.Pins.D[ 0]                    = oC_Pin_FMC_DQ0,
    .SDRAM.Pins.D[ 1]                    = oC_Pin_FMC_DQ1,
    .SDRAM.Pins.D[ 2]                    = oC_Pin_FMC_DQ2,
    .SDRAM.Pins.D[ 3]                    = oC_Pin_FMC_DQ3,
    .SDRAM.Pins.D[ 4]                    = oC_Pin_FMC_DQ4,
    .SDRAM.Pins.D[ 5]                    = oC_Pin_FMC_DQ5,
    .SDRAM.Pins.D[ 6]                    = oC_Pin_FMC_DQ6,
    .SDRAM.Pins.D[ 7]                    = oC_Pin_FMC_DQ7,
    .SDRAM.Pins.D[ 8]                    = oC_Pin_FMC_DQ8,
    .SDRAM.Pins.D[ 9]                    = oC_Pin_FMC_DQ9,
    .SDRAM.Pins.D[10]                    = oC_Pin_FMC_DQ10,
    .SDRAM.Pins.D[11]                    = oC_Pin_FMC_DQ11,
    .SDRAM.Pins.D[12]                    = oC_Pin_FMC_DQ12,
    .SDRAM.Pins.D[13]                    = oC_Pin_FMC_DQ13,
    .SDRAM.Pins.D[14]                    = oC_Pin_FMC_DQ14,
    .SDRAM.Pins.D[15]                    = oC_Pin_FMC_DQ15,
    .SDRAM.Pins.D[16]                    = oC_Pin_FMC_DQ16,
    .SDRAM.Pins.D[17]                    = oC_Pin_FMC_DQ17,
    .SDRAM.Pins.D[18]                    = oC_Pin_FMC_DQ18,
    .SDRAM.Pins.D[19]                    = oC_Pin_FMC_DQ19,
    .SDRAM.Pins.D[20]                    = oC_Pin_FMC_DQ20,
    .SDRAM.Pins.D[21]                    = oC_Pin_FMC_DQ21,
    .SDRAM.Pins.D[22]                    = oC_Pin_FMC_DQ22,
    .SDRAM.Pins.D[23]                    = oC_Pin_FMC_DQ23,
    .SDRAM.Pins.D[24]                    = oC_Pin_FMC_DQ24,
    .SDRAM.Pins.D[25]                    = oC_Pin_FMC_DQ25,
    .SDRAM.Pins.D[26]                    = oC_Pin_FMC_DQ26,
    .SDRAM.Pins.D[27]                    = oC_Pin_FMC_DQ27,
    .SDRAM.Pins.D[28]                    = oC_Pin_FMC_DQ28,
    .SDRAM.Pins.D[29]                    = oC_Pin_FMC_DQ29,
    .SDRAM.Pins.D[30]                    = oC_Pin_FMC_DQ30,
    .SDRAM.Pins.D[31]                    = oC_Pin_FMC_DQ31,
    .SDRAM.Pins.BA[0]                    = oC_Pin_FMC_BA0,
    .SDRAM.Pins.BA[1]                    = oC_Pin_FMC_BA1,
    .SDRAM.Pins.BA[2]                    = oC_Pin_FMC_BA2,
    .SDRAM.Pins.BA[3]                    = oC_Pin_FMC_BA3,
    .SDRAM.Pins.NRAS                     = oC_Pin_FMC_SDNRAS,
    .SDRAM.Pins.NCAS                     = oC_Pin_FMC_SDNCAS,
    .SDRAM.Pins.SDNWE                    = oC_Pin_FMC_SDNWE,
    .SDRAM.Pins.NBL[0]                   = oC_Pin_FMC_NBL0,
    .SDRAM.Pins.NBL[1]                   = oC_Pin_FMC_NBL1,
    .SDRAM.Pins.NBL[2]                   = oC_Pin_FMC_NBL2,
    .SDRAM.Pins.NBL[3]                   = oC_Pin_FMC_NBL3,
};
#endif

static const oC_I2C_Config_t STM32F7Discovery_I2C_TCAudio = {
                .Mode           = oC_I2C_Mode_Master,
                .SpeedMode      = oC_I2C_SpeedMode_Normal ,
                .AddressMode    = oC_I2C_AddressMode_7Bits ,
                .Pins.SCL       = oC_Pin_I2C_TCAudio_SCL ,
                .Pins.SDA       = oC_Pin_I2C_TCAudio_SDA ,
};

static const oC_SDMMC_Config_t STM32F7Discovery_SDMMC = {
                .Mode           = oC_SDMMC_Mode_LLD,
                .TransferMode   = oC_SDMMC_TransferMode_4Bit,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT0] = oC_Pin_uSD_D0,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT1] = oC_Pin_uSD_D1,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT2] = oC_Pin_uSD_D2,
                .Pins[oC_SDMMC_PinIndex_4BitMode_DAT3] = oC_Pin_uSD_D3,
                .Pins[oC_SDMMC_PinIndex_4BitMode_CMD]  = oC_Pin_uSD_CMD,
                .Pins[oC_SDMMC_PinIndex_4BitMode_CLK]  = oC_Pin_uSD_CLK,
                .Pins[oC_SDMMC_PinIndex_4BitMode_nIRQ] = oC_Pin_uSD_Detect,
                .SectorSize     = B(2048)
};

#undef  _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * To add configuration:
 *          ADD_CONFIGURATION( DRIVER_NAME , CONFIG_NAME )\
 */
//==========================================================================================================================================
#define CFG_AUTO_CONFIGURATION_LIST(ADD_CONFIGURATION,DONT_ADD_CONFIGURATION)   \
    ADD_CONFIGURATION( FMC      , STM32F7Discovery_SDRAMConfig      )\
    ADD_CONFIGURATION( I2C      , STM32F7Discovery_I2C_TCAudio      )\
    DONT_ADD_CONFIGURATION( SDMMC    , STM32F7Discovery_SDMMC            )\

