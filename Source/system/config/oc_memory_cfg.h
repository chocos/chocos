/** ****************************************************************************************************************************************
 *
 * @file          oc_memory_cfg.h
 *
 * @brief      The file with configuration of memory
 *
 * @author     Patryk Kubiak - (Created on: 20 06 2015 13:20:51)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CONFIG_OC_MEMORY_CFG_H_
#define SYSTEM_CONFIG_OC_MEMORY_CFG_H_

#include <oc_cfg.h>

//==========================================================================================================================================
/**
 * @brief sets memory exhausted event default limit
 *
 * The memory that is dynamically managed may become exhausted. When this event will not be detected and handled properly, it may cause that
 * the system will stop, or will work unpredictable. This definition helps to set the first level of detecting this event. It is given in
 * percents.
 */
//==========================================================================================================================================
#define CFG_PERCENT_DEFAULT_MEMORY_EXHAUSTED_LIMIT              PERCENT(20)

//==========================================================================================================================================
/**
 * @brief sets memory exhausted event default limit
 *
 * The memory that is dynamically managed may become exhausted. When this event will not be detected and handled properly, it may cause that
 * the system will stop, or will work unpredictable. This definition helps to set the last level of detecting this event. When this threshold
 * is reached, the system will be restarted.
 *
 * It is given in percents.
 */
//==========================================================================================================================================
#define CFG_PERCENT_PANIC_MEMORY_EXHAUSTED_LIMIT                PERCENT(10)

//==========================================================================================================================================
/**
 * @brief sets core memory size
 *
 * The size of the memory that is reserved only for core operations that should be protected by default.
 *
 * It is configured as percent part of the internal memory
 */
//==========================================================================================================================================
#define CFG_PERCENT_CORE_MEMORY_SIZE                            PERCENT(10)

#endif /* SYSTEM_CONFIG_OC_MEMORY_CFG_H_ */
