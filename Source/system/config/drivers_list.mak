############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/05/28
##  Description:    makefile with list of drivers to include
##
############################################################################################################################################

##============================================================================================================================
##                                          
##              LIST OF DRIVERS TO INCLUDE TO SYSTEM
##                                          
##============================================================================================================================
DRIVERS		= UART \
			  TIMER \
			  GPIO
