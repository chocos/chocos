/** ****************************************************************************************************************************************
 *
 * @file       oc_programs_list.h
 *
 * @brief      The file with list of programs that are included to the system
 *
 *
 * @author     Patryk Kubiak - (Created on: 30 05 2015 14:09:04)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef OC_PROGRAMS_LIST_H_
#define OC_PROGRAMS_LIST_H_

#include <oc_cfg.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of programs to include to the system
 *
 *
 */
//==========================================================================================================================================
#define CFG_PROGRAMS_LIST(INCLUDE,DONT_INCLUDE)\
    INCLUDE(chell)\
    INCLUDE(cpu)\
    INCLUDE(geterrors)\
    INCLUDE(echo)\
    INCLUDE(memory)\
    INCLUDE(ls)\
    INCLUDE(ps)\
    INCLUDE(getallocs)\
    DONT_INCLUDE(printf)\
    INCLUDE(reboot)\
    INCLUDE(time)\
    INCLUDE(mkdir)\
    INCLUDE(led)\
    DONT_INCLUDE(gpio_test)\
    DONT_INCLUDE(lcdtft_test)\
    INCLUDE(system)\
    DONT_INCLUDE(tcolors)\
    DONT_INCLUDE(TguiExample)\
    DONT_INCLUDE(fmc_test)\
    INCLUDE(cat)\
    DONT_INCLUDE(littlenes)\
    INCLUDE(ifconfig)\
    INCLUDE(ping)\
    INCLUDE(http_server)\
    INCLUDE(kill)\
    INCLUDE(getlogs)\
    INCLUDE(user)\
    INCLUDE(i2c)\
    INCLUDE(ft5336)\
    INCLUDE(touchscreen)\
    DONT_INCLUDE(liteNES)\
    INCLUDE(paint)\
    INCLUDE(ticket2ride)\
    DONT_INCLUDE(elfread)\
    INCLUDE(cbinread)\
    INCLUDE(spi)\
    INCLUDE(sdmmc)\
    INCLUDE(scanf)\
    INCLUDE(example_allocator)\
    INCLUDE(tictactoe)\
    INCLUDE(example_ws_radiobutton)\
    INCLUDE(example_ws_buttons)\
    INCLUDE(example_cm_draw)\
    INCLUDE(example_uart)\
    INCLUDE(example_tcp)\
    INCLUDE(example_udp)\

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief default program to run after start of the system
 */
//==========================================================================================================================================
#define CFG_PROGRAM_DEFAULT     chell

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief default graphics program to run after start of the system
 */
//==========================================================================================================================================
#define CFG_PROGRAM_DEFAULT_GRAPHICS        ticket2ride

#endif /* OC_PROGRAMS_LIST_H_ */
