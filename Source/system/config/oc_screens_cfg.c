/** ****************************************************************************************************************************************
 *
 * @file          oc_screen_cfg.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 21.07.2016 20:06:03) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_screen.h>
#include <oc_screenman.h>
#include <oc_system_cfg.h>

#if CFG_BOOL_GUI_ENABLED

/** ========================================================================================================================================
 *
 *              The section with configurations of drivers
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

#ifdef oC_LCDTFT_LLD_AVAILABLE
static const oC_LCDTFT_Config_t  lcdtft_config    = {
                    .Width              = 480 ,
                    .Height             = 272 ,
                    .ColorFormat        = oC_ColorFormat_RGB888 ,
                    .ClockFrequency     = kHz( 9600 ) ,
                    .PixelClockPolarity = oC_LCDTFT_PixelClockPolarity_InputPixelClock ,
                    .HSyncPolarity      = oC_LCDTFT_Polarity_ActiveLow ,
                    .VSyncPolarity      = oC_LCDTFT_Polarity_ActiveLow ,
                    .DESyncPolarity     = oC_LCDTFT_Polarity_ActiveLow ,
                    .ColorMap           = NULL,
                    .TimingParameters.HSYNC.PulseWidth = 41 ,
                    .TimingParameters.VSYNC.PulseWidth =  2 ,
                    .TimingParameters.HSYNC.BackPorch  = 26 ,
                    .TimingParameters.VSYNC.BackPorch  = 20 ,
                    .TimingParameters.HSYNC.FrontPorch = 32 ,
                    .TimingParameters.VSYNC.FrontPorch =  2 ,
                    .Pins.R[0]          = oC_Pin_LCDTFT_R0 ,
                    .Pins.R[1]          = oC_Pin_LCDTFT_R1 ,
                    .Pins.R[2]          = oC_Pin_LCDTFT_R2 ,
                    .Pins.R[3]          = oC_Pin_LCDTFT_R3 ,
                    .Pins.R[4]          = oC_Pin_LCDTFT_R4 ,
                    .Pins.R[5]          = oC_Pin_LCDTFT_R5 ,
                    .Pins.R[6]          = oC_Pin_LCDTFT_R6 ,
                    .Pins.R[7]          = oC_Pin_LCDTFT_R7 ,
                    .Pins.G[0]          = oC_Pin_LCDTFT_G0 ,
                    .Pins.G[1]          = oC_Pin_LCDTFT_G1 ,
                    .Pins.G[2]          = oC_Pin_LCDTFT_G2 ,
                    .Pins.G[3]          = oC_Pin_LCDTFT_G3 ,
                    .Pins.G[4]          = oC_Pin_LCDTFT_G4 ,
                    .Pins.G[5]          = oC_Pin_LCDTFT_G5 ,
                    .Pins.G[6]          = oC_Pin_LCDTFT_G6 ,
                    .Pins.G[7]          = oC_Pin_LCDTFT_G7 ,
                    .Pins.B[0]          = oC_Pin_LCDTFT_B0 ,
                    .Pins.B[1]          = oC_Pin_LCDTFT_B1 ,
                    .Pins.B[2]          = oC_Pin_LCDTFT_B2 ,
                    .Pins.B[3]          = oC_Pin_LCDTFT_B3 ,
                    .Pins.B[4]          = oC_Pin_LCDTFT_B4 ,
                    .Pins.B[5]          = oC_Pin_LCDTFT_B5 ,
                    .Pins.B[6]          = oC_Pin_LCDTFT_B6 ,
                    .Pins.B[7]          = oC_Pin_LCDTFT_B7 ,
                    .Pins.VSYNC         = oC_Pin_LCDTFT_VSYNC ,
                    .Pins.HSYNC         = oC_Pin_LCDTFT_HSYNC ,
                    .Pins.DE            = oC_Pin_LCDTFT_DE ,
                    .Pins.CLK           = oC_Pin_LCDTFT_CLK ,
                    .Pins.BL_CTRL       = oC_Pin_LCDTFT_BL_CTRL ,
                    .Pins.DISP          = oC_Pin_LCDTFT_DISP ,
};
#endif

#undef  _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

#ifdef oC_LCDTFT_LLD_AVAILABLE
//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * To add screen:
 *          ADD_SCREEN( NAME , DRIVER_NAME , CONFIG_NAME )\
 */
//==========================================================================================================================================
#define CFG_SCREENS_LIST(ADD_SCREEN)    \
                ADD_SCREEN(lcdtft  , LCDTFT , lcdtft_config  ) \

#else
#define CFG_SCREENS_LIST(ADD_SCREEN)
#endif

#define CFG_STRING_DEFAULT_SCREEN      "lcdtft"

#endif
