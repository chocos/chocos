/** ****************************************************************************************************************************************
 *
 * @file       oc_debug_cfg.h
 *
 * @brief      Configuration of the debug functions
 *
 * @author     Patryk Kubiak - (Created on: 24 mar 2015 22:17:07) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @addtogroup CFG
 * @{
 *
 * @page CFG
 * @section DEBUG_CFG Debug Configuration
 *
 *
 *
 ******************************************************************************************************************************************/


#ifndef APPLICATION_CONFIG_OC_DEBUG_CFG_H_
#define APPLICATION_CONFIG_OC_DEBUG_CFG_H_

#include <oc_cfg.h>

//==========================================================================================================================================
/**
 * @brief enabling assertions
 *
 * In computer programming, an assertion is a predicate (a true–false statement) placed in a program to indicate that the developer thinks
 * that the predicate is always true at that place. If an assertion evaluates to false at run-time, an assertion failure results,
 * which typically causes execution to abort. <br />
 * <br />
 * To enable assertions that are placed in the system, set it to ON
 * <br />
 * OPTIONS:<br />
 *              VALUE | Description
 *              ------|------------------------------
 *              ON    | assertions are turned on
 *              OFF   | assertions are turned off
 */
//==========================================================================================================================================
#define CFG_ENABLE_ASSERTIONS           ON

//==========================================================================================================================================
/**
 * @brief definition of assertions level
 *
 * In computer programming, an assertion is a predicate (a true–false statement) placed in a program to indicate that the developer thinks
 * that the predicate is always true at that place. If an assertion evaluates to false at run-time, an assertion failure results,
 * which typically causes execution to abort. <br />
 * <br />
 * If assertions are turned on you can select a level of assertions, that should be used.
 *
 * To turn on assertions use #CFG_ENABLE_ASSERTIONS definition<br />
 *<br />
 * OPTIONS:<br />
 *               VALUE    | Description
 *               ---------|-----------------------------------------
 *               LEVEL(0) | When some of assertions fails, a program will stack in infinite loop.
 *               LEVEL(1) | When some of assertions fails, a program will print information about it on the STDOUT
 */
//==========================================================================================================================================
#define CFG_LEVEL_ASSERTIONS            LEVEL(0)

//==========================================================================================================================================
/**
 * @brief enabling debug printing
 *
 * Turns on the oC_Debug function, that can be used to print messages visible only in this mode. Debug print is the similar function to printf
 * but the most important difference between them is that oC_Debug will not be called, if this definition is set to OFF.
 * <br />
 * To enable debug messages, set it to #ON<br />
 * <br />
 * OPTIONS:<br />
 *              VALUE | Description
 *              ------|----------------------------------------
 *              ON    | Debug messages are printed
 *              OFF   | all calls of #oC_Debug function are skipped
 *
 */
//==========================================================================================================================================
#define CFG_ENABLE_DEBUG_PRINT          ON

//==========================================================================================================================================
/**
 * @brief enabling implement failures
 *
 * Turns on the #oC_IMPLEMENT_FAILURE loops, that are for places in the code, that could not be achieved. The implement failure definition
 * is just infinity loop, that can be turned off using this definition.<br />
 * <br />
 * To enable implement failures, set it to #ON<br />
 * <br />
 * OPTIONS:<br />
 *              VALUE | Description
 *              ------|----------------------------------------------
 *              ON    | Implement failures are turned on
 *              OFF   | all calls of #oC_IMPLEMENT_FAILUIRE function are skipped
 */
//==========================================================================================================================================
#define CFG_ENABLE_IMPLEMENT_FAILURES   ON

//==========================================================================================================================================
/**
 * @brief enabling compile assertions
 *
 * Turns on assertions, that are checked during compile process. If the assertion condition is not true, then there will be compilation error
 * <br />
 * To enable compile assertions set it to #ON<br />
 * <br />
 * OPTIONS:<br />
 *              VALUE | Description
 *              ------|----------------------------------------------
 *              ON    | compile assertions are turned on
 *              OFF   | compile assertions are skipped
 */
//==========================================================================================================================================
#define CFG_ENABLE_COMPILE_ASSERTS      ON

#define CFG_UINT16_NUMBER_OF_DEBUG_LOGS         100

#define CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH   200

#define CFG_ENABLE_TCP_LOGS             ON

#define CFG_ENABLE_TELNET_LOGS          ON

#define CFG_UINT16_MAX_NEWS_STRING_LENGTH       50

#define CFG_UINT16_NUMBER_OF_NEWS               5

#define CFG_ENABLE_NEWS                 ON

/**
 * @}
 */
#endif /* APPLICATION_CONFIG_OC_DEBUG_CFG_H_ */
