/** ****************************************************************************************************************************************
 *
 * @file       oc_pinsmap.h
 *
 * @brief      The file with configuration of pins for lm4f120h5qr machine
 *
 * @author     Patryk Kubiak - (Created on: 29 08 2015 17:10:43)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CONFIG_STM32F746NGH6_OC_PINSMAP_H_
#define SYSTEM_CONFIG_STM32F746NGH6_OC_PINSMAP_H_

#define oC_CFG_PINS(ADD_PIN)    \
    ADD_PIN( RED_LED            , PA1 ) \
    ADD_PIN( BLUE_LED           , PA2 ) \
    ADD_PIN( GREEN_LED          , PA3 ) \
    ADD_PIN( SW1                , PA0 ) \
    ADD_PIN( SW2                , PA4 ) \
    ADD_PIN( UART_RX            , PB7 ) \
    ADD_PIN( UART_TX            , PA9 ) \
    ADD_PIN( UART_RX_ALT        , PC7 ) \
    ADD_PIN( UART_TX_ALT        , PC6 ) \
    ADD_PIN( GPIO_TEST_PIN_OUT  , PG6 ) \
    ADD_PIN( GPIO_TEST_PIN_IN   , PG7 ) \
    ADD_PIN( LCDTFT_R0          , PI15 ) \
    ADD_PIN( LCDTFT_R1          , PJ0 ) \
    ADD_PIN( LCDTFT_R2          , PJ1 ) \
    ADD_PIN( LCDTFT_R3          , PJ2 ) \
    ADD_PIN( LCDTFT_R4          , PJ3 ) \
    ADD_PIN( LCDTFT_R5          , PJ4 ) \
    ADD_PIN( LCDTFT_R6          , PJ5 ) \
    ADD_PIN( LCDTFT_R7          , PJ6 ) \
    ADD_PIN( LCDTFT_G0          , PJ7 ) \
    ADD_PIN( LCDTFT_G1          , PJ8 ) \
    ADD_PIN( LCDTFT_G2          , PJ9 ) \
    ADD_PIN( LCDTFT_G3          , PJ10 ) \
    ADD_PIN( LCDTFT_G4          , PJ11 ) \
    ADD_PIN( LCDTFT_G5          , PK0 ) \
    ADD_PIN( LCDTFT_G6          , PK1 ) \
    ADD_PIN( LCDTFT_G7          , PK2 ) \
    ADD_PIN( LCDTFT_B0          , PE4 ) \
    ADD_PIN( LCDTFT_B1          , PJ13 ) \
    ADD_PIN( LCDTFT_B2          , PJ14 ) \
    ADD_PIN( LCDTFT_B3          , PJ15 ) \
    ADD_PIN( LCDTFT_B4          , PG12 ) \
    ADD_PIN( LCDTFT_B5          , PK4 ) \
    ADD_PIN( LCDTFT_B6          , PK5 ) \
    ADD_PIN( LCDTFT_B7          , PK6 ) \
    ADD_PIN( LCDTFT_VSYNC       , PI9 ) \
    ADD_PIN( LCDTFT_HSYNC       , PI10 ) \
    ADD_PIN( LCDTFT_CLK         , PI14 ) \
    ADD_PIN( LCDTFT_DE          , PK7 ) \
    ADD_PIN( LCDTFT_BL_CTRL     , PK3 ) \
    ADD_PIN( LCDTFT_DISP        , PI12 ) \
    ADD_PIN( FMC_A0             , PF0 ) \
    ADD_PIN( FMC_A1             , PF1 ) \
    ADD_PIN( FMC_A2             , PF2 ) \
    ADD_PIN( FMC_A3             , PF3 ) \
    ADD_PIN( FMC_A4             , PF4 ) \
    ADD_PIN( FMC_A5             , PF5 ) \
    ADD_PIN( FMC_A6             , PF12 ) \
    ADD_PIN( FMC_A7             , PF13 ) \
    ADD_PIN( FMC_A8             , PF14 ) \
    ADD_PIN( FMC_A9             , PF15 ) \
    ADD_PIN( FMC_A10            , PG0 ) \
    ADD_PIN( FMC_A11            , PG1 ) \
    ADD_PIN( FMC_A12            , NotUsed ) \
    ADD_PIN( FMC_BA0            , PG4 ) \
    ADD_PIN( FMC_BA1            , PG5 ) \
    ADD_PIN( FMC_BA2            , NotUsed ) \
    ADD_PIN( FMC_BA3            , NotUsed ) \
    ADD_PIN( FMC_SDNWE          , PH5 ) \
    ADD_PIN( FMC_SDNCAS         , PG15 ) \
    ADD_PIN( FMC_SDNRAS         , PF11 ) \
    ADD_PIN( FMC_SDNE0          , PH3 ) \
    ADD_PIN( FMC_SDNE1          , NotUsed ) \
    ADD_PIN( FMC_SDCKE0         , PC3 ) \
    ADD_PIN( FMC_SDCKE1         , NotUsed ) \
    ADD_PIN( FMC_SDCLK          , PG8 ) \
    ADD_PIN( FMC_NBL0           , PE0 ) \
    ADD_PIN( FMC_NBL1           , PE1 ) \
    ADD_PIN( FMC_NBL2           , PI4 ) \
    ADD_PIN( FMC_NBL3           , PI5 ) \
    ADD_PIN( FMC_DQ0            , PD14 ) \
    ADD_PIN( FMC_DQ1            , PD15 ) \
    ADD_PIN( FMC_DQ2            , PD0 ) \
    ADD_PIN( FMC_DQ3            , PD1 ) \
    ADD_PIN( FMC_DQ4            , PE7 ) \
    ADD_PIN( FMC_DQ5            , PE8 ) \
    ADD_PIN( FMC_DQ6            , PE9 ) \
    ADD_PIN( FMC_DQ7            , PE10 ) \
    ADD_PIN( FMC_DQ8            , PE11 ) \
    ADD_PIN( FMC_DQ9            , PE12 ) \
    ADD_PIN( FMC_DQ10           , PE13 ) \
    ADD_PIN( FMC_DQ11           , PE14 ) \
    ADD_PIN( FMC_DQ12           , PE15 ) \
    ADD_PIN( FMC_DQ13           , PD8 ) \
    ADD_PIN( FMC_DQ14           , PD9 ) \
    ADD_PIN( FMC_DQ15           , PD10 ) \
    ADD_PIN( FMC_DQ16           , NotUsed ) \
    ADD_PIN( FMC_DQ17           , NotUsed ) \
    ADD_PIN( FMC_DQ18           , NotUsed ) \
    ADD_PIN( FMC_DQ19           , NotUsed ) \
    ADD_PIN( FMC_DQ20           , NotUsed ) \
    ADD_PIN( FMC_DQ21           , NotUsed ) \
    ADD_PIN( FMC_DQ22           , NotUsed ) \
    ADD_PIN( FMC_DQ23           , NotUsed ) \
    ADD_PIN( FMC_DQ24           , NotUsed ) \
    ADD_PIN( FMC_DQ25           , NotUsed ) \
    ADD_PIN( FMC_DQ26           , NotUsed ) \
    ADD_PIN( FMC_DQ27           , NotUsed ) \
    ADD_PIN( FMC_DQ28           , NotUsed ) \
    ADD_PIN( FMC_DQ29           , NotUsed ) \
    ADD_PIN( FMC_DQ30           , NotUsed ) \
    ADD_PIN( FMC_DQ31           , NotUsed ) \
    ADD_PIN( RMII_REF_CLK       , PA1 ) \
    ADD_PIN( RMII_MDIO          , PA2 ) \
    ADD_PIN( RMII_RCS_DV        , PA7 ) \
    ADD_PIN( RMII_MDC           , PC1 ) \
    ADD_PIN( RMII_RXD0          , PC4 ) \
    ADD_PIN( RMII_RXD1          , PC5 ) \
    ADD_PIN( RMII_TX_EN         , PG11 ) \
    ADD_PIN( RMII_TXD0          , PG13 ) \
    ADD_PIN( RMII_TXD1          , PG14 ) \
    ADD_PIN( RMII_RXER          , PG2  ) \
    ADD_PIN( FT5336_SCL         , PH7  ) \
    ADD_PIN( FT5336_SDA         , PH8  ) \
    ADD_PIN( FT5336_IT          , PI13 ) \
    ADD_PIN( I2C_TCAudio_SCL    , PH7  ) \
    ADD_PIN( I2C_TCAudio_SDA    , PH8  ) \
    ADD_PIN( ARD_D0             , PC7  ) \
    ADD_PIN( ARD_D1             , PC6  ) \
    ADD_PIN( ARD_D2             , PG6  ) \
    ADD_PIN( ARD_D3             , PB4  ) \
    ADD_PIN( ARD_D4             , PG7  ) \
    ADD_PIN( ARD_D5             , PI0  ) \
    ADD_PIN( ARD_D6             , PH6  ) \
    ADD_PIN( ARD_D7             , PI3  ) \
    ADD_PIN( ARD_D8             , PI2  ) \
    ADD_PIN( ARD_D9             , PA15 ) \
    ADD_PIN( ARD_D10            , PA8  ) \
    ADD_PIN( ARD_D11            , PB15 ) \
    ADD_PIN( ARD_D12            , PB14 ) \
    ADD_PIN( ARD_D13            , PI1  ) \
    ADD_PIN( ARD_D14            , PB9  ) \
    ADD_PIN( ARD_D15            , PB8  ) \
    ADD_PIN( uSD_D0             , PC8 ) \
    ADD_PIN( uSD_D1             , PC9 ) \
    ADD_PIN( uSD_D2             , PC10 ) \
    ADD_PIN( uSD_D3             , PC11 ) \
    ADD_PIN( uSD_CLK            , PC12 ) \
    ADD_PIN( uSD_Detect         , PC13 ) \
    ADD_PIN( uSD_CMD            , PD2 ) \

#endif /* SYSTEM_CONFIG_STM32F746NGH6_OC_PINSMAP_H_ */
