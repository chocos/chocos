/** ****************************************************************************************************************************************
 *
 * @file       oc_system_cfg.h
 *
 * @brief      The file with configuration of the system
 *
 * @author     Patryk Kubiak - (Created on: 30 08 2015 09:19:57)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CONFIG_OC_SYSTEM_CFG_H_
#define SYSTEM_CONFIG_OC_SYSTEM_CFG_H_

#include <oc_cfg.h>

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief default thread stack size in bytes
 *
 * Each thread needs own stack for variables, function pointers, function parameters, etc. This definition is for setting default size of the stack
 * for each thread after start of the system. Note, that this definition can be also changed dynamically in the thread module. Moreover, when
 * the stack is overflowed, the task is trying to extend it.
 */
//==========================================================================================================================================
#define CFG_BYTES_DEFAULT_THREAD_STACK_SIZE               B(1024)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief default size of the red zone
 *
 * When the stack in a thread is over, there is a risk that the system do not have time to detect it, and some other memory will be overwritten
 * by random data. For this reason, each stack has its own 'red zone'. When this zone is overwritten, the system is extending size of the stack
 * for the thread without loss of data.
 */
//==========================================================================================================================================
#define CFG_BYTES_DEFAULT_RED_ZONE_SIZE                   B(512)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum number of threads that can be run in the same time
 */
//==========================================================================================================================================
#define CFG_UINT32_MAXIMUM_NUMBER_OF_THREADS              300

#define CFG_BOOL_RERUN_THREAD_WHEN_STACK_IS_OVER          false

#define CFG_BYTES_STACK_INCREASE_STEP                     B(256)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief number of saved errors
 *
 * The system provides special error codes module (oc_errors.h), that is returned by most of functions. It allow to quickly find out, why
 * an operation fails, but there are some situations, when the error cannot be showed to the user - for example, when the STDOUT is not ready.
 * Thats why this definition exists - it specify number of errors, that can be stored in the memory for printing in the future.
 */
//==========================================================================================================================================
#define CFG_UINT8_MAXIMUM_NUMBER_OF_SAVED_ERRORS          20

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief default frequency of system timer
 *
 * The system timer is the special timer, that is used for switching system threads. This definition helps to choose frequency of switching
 * tasks. If this value is too big, it may cause high system impact for CPU load, but if it is too small, threads will not be switched very
 * often.
 */
//==========================================================================================================================================
#define CFG_FREQUENCY_DEFAULT_SYSTEM_TIMER_FREQUENCY      kHz(1)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief frequency of force changing thread
 */
//==========================================================================================================================================
#define CFG_FREQUENCY_DEFAULT_FORCE_CHANGE_THREAD         Hz(1)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum time for start of the system
 *
 * This definition helps to select maximum time for start of the system. When this time expired, then the system is restarted.
 */
//==========================================================================================================================================
#define CFG_TIME_MAXIMUM_TIME_FOR_INITIALIZATION_OF_SYSTEM      min(10)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief Time of incrementing timestamp
 *
 * The definition is for selecting timestamp increment time. It is the shortest time, that can be measured by the system.
 */
//==========================================================================================================================================
#define CFG_TIME_TIMESTAMP_INCREMENT_TIME                       ms(400)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief size of the heap map for each process
 *
 * This is definition that helps to set size of the 'heap map' - it is the special memory block, that is allocated for the process when it is
 * created. When process has allocated heap map, then all of system allocations in the process will try to use this heap instead of common system
 * heap. It also helps to protect other processes again buffer overflow, and some other memory-related problems. When the heap from the process
 * is finished, then allocations will just use common heap.
 */
//==========================================================================================================================================
#define CFG_BYTES_DEFAULT_PROCESS_HEAP_MAP_SIZE                 kB(1)

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_BOOL_EXTERNAL_CLOCK_SOURCE                          true

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_FREQUENCY_EXTERNAL_CLOCK                            MHz(25)

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief frequency of clock to configure
 *
 * This definition stores frequency to configure for system clock after start of the system. You can use macros from oc_cfg.h file:
 * `GHz` , `MHz` , `kHz` and `Hz`. In addition it is possible to set it to maximum by using `MAX` definition. Example:
 * @code{.c}
   // Definition set to 80 MHz
   #define CFG_FREQUENCY_TARGET_FREQUENCY                          MHz(80)

   // Definition set to maximum for the given machine (for example 216 MHz for STM32F7)
   #define CFG_FREQUENCY_TARGET_FREQUENCY                          MAX
   @endcode
 */
//==========================================================================================================================================
#define CFG_FREQUENCY_TARGET_FREQUENCY                          MHz(200)

#define CFG_FREQUENCY_PERMISSIBLE_FREQUENCY_DIFFERENCE          MHz(1)

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_STRING_ROOT_PASSWORD                                "root"

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_BYTES_STDIO_BUFFER_SIZE                             256

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_UINT8_MAX_LOGIN_LENGTH                              30

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_UINT8_MAX_PASSWORD_LENGTH                           50

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_BOOL_USE_TGUI_LOGIN                                 true

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_BYTES_DELETE_DEAMON_STACK_SIZE                      kB(2)

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_FREQUENCY_CPU_LOAD_MEASUREMENT_FREQUENCY            Hz(0.3)

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_PERCENT_CPU_LOAD_PANIC_MARGIN                       90

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_TIME_CPU_LOAD_PANIC_TIME                            min(360)

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_BYTES_MAX_CONFIGURATION_NAME_SIZE                   30

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_UINT16_MAX_SAVED_COMMANDS                           5

#define CFG_BOOL_GUI_ENABLED                                    true

#define CFG_BOOL_NETWORK_ENABLED                                true

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_UINT8_MAX_ACTIONS_TO_REVERT                         20

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_ENABLE_CBIN_DEBUG_LOGS                              ON

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_STRING_HOSTNAME                                     "ChocoOS_Device"

//==========================================================================================================================================
//==========================================================================================================================================
#define CFG_TIME_DEFAULT_LEASE_TIME                              min(10)

#endif /* SYSTEM_CONFIG_OC_SYSTEM_CFG_H_ */
