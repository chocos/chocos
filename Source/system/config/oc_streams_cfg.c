/** ****************************************************************************************************************************************
 *
 * @file       oc_streams_cfg.c
 *
 * @brief      The file with configurations of streams
 *
 * @author     Patryk Kubiak - (Created on: 21 09 2015 21:40:30)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stream.h>
#include <oc_driverman.h>

/** ========================================================================================================================================
 *
 *              The section with configurations of drivers
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

static const oC_UART_Config_t uart_stdio = {
                .Rx             = oC_Pin_UART_RX ,
                .Tx             = oC_Pin_UART_TX ,
                .WordLength     = oC_UART_WordLength_8Bits ,
                .BitRate        = 921600  /* 460800 */ ,
                .Parity         = oC_UART_Parity_None ,
                .StopBit        = oC_UART_StopBit_2Bits ,
                .BitOrder       = oC_UART_BitOrder_LSBFirst ,
                .Invert         = oC_UART_Invert_NotInverted ,
                .Dma            = oC_UART_Dma_UseIfPossible
};
static const oC_GTD_Config_t gtd_stderr = {
                .ScreenName          = "lcdtft" ,
                .Width               = 480 ,
                .Height              = 220 ,
                .Position.X          = 0 ,
                .Position.Y          = 50 ,
};
static const oC_GTD_Config_t gtd_syssprint = {
                .ScreenName          = "lcdtft" ,
                .Width               = 480 ,
                .Height              = 50 ,
                .Position.X          = 0 ,
                .Position.Y          = 0 ,
                .FontG1              = oC_Font_(Consolas) ,
};

#undef  _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * @brief standard error stream for fatal system errors
 */
//==========================================================================================================================================
#define CFG_STDERR_STREAM_NAME      "uart_stdio"

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * To add stream:
 *          ADD_STREAM( NAME , IN | OUT , DRIVER_NAME , CONFIG_NAME )\
 */
//==========================================================================================================================================
#define CFG_STREAMS_LIST(ADD_STREAM,DONT_ADD_STREAM)    \
                ADD_STREAM(uart_stdio   , IN | OUT , UART , uart_stdio      ) \
                ADD_STREAM(gtd_syssprint,      OUT , GTD  , gtd_syssprint   ) \
				DONT_ADD_STREAM(gtd_stderr,         OUT , GTD  , gtd_syssprint   ) \

