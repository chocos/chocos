/** ****************************************************************************************************************************************
 *
 * @brief      File with configuration of the network
 *
 * @file       oc_net_cfg.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_cfg.h>
#include <oc_driverman.h>

/** ========================================================================================================================================
 *
 *              The section with configurations of drivers
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

#ifdef oC_ETH_LLD_AVAILABLE
static const oC_ETH_Config_t STM32F7Discovery_EthernetConfig = {
                .PhyChipInfo    = &oC_ETH_PhyChipInfo_LAN8742A ,
                .PhyAddress     = 0x00 ,
                .MacAddress     = { 0x02 , 0x11 , 0x22 , 0x33 , 0x44 , 0x55 } ,
                .LLD            = {
                    .AutoGeneratePad  = true,
                    .AutoCalculateCrc = true,
                    .PhyAddress       = 0x00,
                    .BaudRate         = MBd(100),
                    .OperationMode    = oC_ETH_LLD_OperationMode_FullDuplex,
                    .Rmii           = {
                                    .Mdc     = oC_Pin_RMII_MDC,
                                    .Mdio    = oC_Pin_RMII_MDIO,
                                    .RefClk  = oC_Pin_RMII_REF_CLK,
                                    .RcsDv   = oC_Pin_RMII_RCS_DV,
                                    .RxD0    = oC_Pin_RMII_RXD0,
                                    .RxD1    = oC_Pin_RMII_RXD1,
                                    .TxEn    = oC_Pin_RMII_TX_EN,
                                    .TxD0    = oC_Pin_RMII_TXD0,
                                    .TxD1    = oC_Pin_RMII_TXD1,
                                    .RxEr    = oC_Pin_RMII_RXER,
                    },

                },
};
#endif

#undef  _________________________________________DRIVER_CONFIGURATIONS_SECTION______________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * To add network interface:
 *          ADD_NET( "FRIENDLY_NAME" , "IP" , "NETMASK" , DRIVER_NAME , CONFIG_NAME )\
 *
 *              where:
 *                      "FRIENDLY_NAME"     - String with friendly name of the interface
 *                      "IP"                - String with static IP (IPv4 or IPv6) or AUTO if DHCP is used
 *                      "NETMASK"           - String with static net mask IP (IPv4 or IPv6) or AUTO if DHCP is used
 */
//==========================================================================================================================================
#define CFG_NETIF_LIST(ADD_NET,DONT_ADD_NET)    \
                ADD_NET( "ethernet" , "192.168.1.101" , "255.255.255.0" , ETH , STM32F7Discovery_EthernetConfig  ) \

//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * To add entry to the routing table:
 *          ADD_ENTRY( "Network Destination" , "Netmask", COST , "Interface FRIENDLY_NAME" )\
 *
 *              where:
 *                      "Network Destination"       - String with IP of the network destination, e.x. "0.0.0.0" or "192.168.0.0"
 *                      "Netmask"                   - String with mask of network to route, e.x. "0.0.0.0" or "255.255.255.0"
 *                      "Interface FRIENDLY_NAME"   - String with FRIENDLY_NAME of the interface in the #CFG_NETIF_LIST
 *                      COST                        - uint32_t cost of the connection to the IP by using this entry. It can be used to choose
 *                                                    one interface more frequently than another
 */
//==========================================================================================================================================
#define CFG_ROUTING_TABLE(ADD_ENTRY,DONT_ADD_ENTRY)     \
                ADD_ENTRY( "0.0.0.0" , "0.0.0.0" , 1 , "ethernet" )\

