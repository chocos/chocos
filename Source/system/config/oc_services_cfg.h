/** ****************************************************************************************************************************************
 *
 * @brief      File with configuration of services
 * 
 * @file       oc_services_cfg.h
 *
 * @author     Patryk Kubiak - (Created on: 18.01.2017 21:28:01) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CONFIG_OC_SERVICES_CFG_H_
#define SYSTEM_CONFIG_OC_SERVICES_CFG_H_

#include <oc_cfg.h>


#define CFG_SYSSPRINT_STREAM_NAME   "gtd_syssprint"

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief list of services to include to the system
 */
//==========================================================================================================================================
#define CFG_LIST_SERVICES(TURN_ON,TURN_OFF)    \
    TURN_ON(Telnet) \
    TURN_ON(syssprint) \
    TURN_ON(ictrlman) \
    TURN_ON(diskman) \

#endif /* SYSTEM_CONFIG_OC_SERVICES_CFG_H_ */
