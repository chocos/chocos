/** ****************************************************************************************************************************************
 *
 * @brief      File with configuration of input controllers
 * 
 * @file          oc_ictrlman_cfg.c
 *
 * @author     Patryk Kubiak - (Created on: 05.05.2017 10:48:28) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_driverman.h>
#include <oc_ictrlman.h>

/** ========================================================================================================================================
 *
 *              The section with configuration
 *
 *  ======================================================================================================================================*/
#define _________________________________________CONFIGURATION_SECTION______________________________________________________________________

static const oC_FT5336_Config_t TouchScreenConfig = {
                .I2C.AutoConfigurationName   = "STM32F7Discovery_I2C_TCAudio" ,
                .Mode                        = oC_FT5336_Mode_InterruptMode ,
                .InterruptPin                = oC_Pin_FT5336_IT ,
                .PollingTime                 = s(0) ,
};

#undef  _________________________________________CONFIGURATION_SECTION______________________________________________________________________


//==========================================================================================================================================
/**
 * @hideinitializer
 *
 * To add Input Controller:
 *          ADD_ICTRL( NAME , DRIVER_NAME , CONFIG_NAME , DEFAULT_SCREEN_NAME , ROTATE )\
 *
 *          where:
 *                  NAME                    - string with the input controller name
 *                  DRIVER_NAME             - name of the driver (it is NOT a string)
 *                  CONFIG_NAME             - Name of the Input Controller Driver configuration (it should be inside this file)
 *                  DEFAULT_SCREEN_NAME     - default screen to associate with the input controller (set it to empty if all of them)
 *                  ROTATE                  - set it to NONE to not rotate or ROTATE_90, ROTATE_180, ROTATE_270 to rotate
 */
//==========================================================================================================================================
#define CFG_ICTRL_LIST(ADD_ICTRL)       \
    ADD_ICTRL("ft5336", FT5336, TouchScreenConfig , "lcdtft", ROTATE_90) \

