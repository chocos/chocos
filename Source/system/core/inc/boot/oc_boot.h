/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for bootloader
 *
 * @file       oc_boot.h
 *
 * @author     Patryk Kubiak - (Created on: 18 05 2015 20:12:54)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Boot Bootloader
 * @ingroup CoreSpace
 * @brief Mechanism for boot loading the system
 *
 *
 ******************************************************************************************************************************************/


#ifndef INC_BOOT_OC_BOOT_H_
#define INC_BOOT_OC_BOOT_H_

#include <oc_user.h>
#include <oc_time.h>

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @addtogroup Boot
//! @{

//==========================================================================================================================================
/**
 * @brief Boot reason
 *
 * The enumeration is for saving the reason why the system was booted
 */
//==========================================================================================================================================
typedef enum
{
    oC_Boot_Reason_Unknown ,            //!< Unknown reason
    oC_Boot_Reason_UserRequest ,        //!< User requested boot
    oC_Boot_Reason_DriverError ,        //!< Driver error
    oC_Boot_Reason_SystemException ,    //!< System exception
    oC_Boot_Reason_MemoryLackout        //!< Memory lackout
} oC_Boot_Reason_t;

//==========================================================================================================================================
/**
 * @brief Boot level
 *
 * The enumeration is for saving the level of system boot
 */
//==========================================================================================================================================
typedef enum
{
    oC_Boot_Level_RequireClock                  = (1<<0) ,
    oC_Boot_Level_RequireMemoryManager          = (1<<1) ,
    oC_Boot_Level_RequireDriversManager         = (1<<2) ,
    oC_Boot_Level_RequireProcessManager         = (1<<3) ,
    oC_Boot_Level_RequireStreamsManager         = (1<<4) ,
    oC_Boot_Level_RequireThreadManager          = (1<<5) ,
    oC_Boot_Level_RequireUsersManager           = (1<<6) ,
    oC_Boot_Level_RequireKernelTime             = (1<<7) ,
    oC_Boot_Level_RequireProgramManager         = (1<<8) ,
    oC_Boot_Level_RequireKernelPrint            = (1<<9) ,
    oC_Boot_Level_RequireDeleteDeamon           = (1<<10) ,
    oC_Boot_Level_RequireFileSystem             = (1<<11) ,
    oC_Boot_Level_RequireNetwork                = (1<<12) ,
    oC_Boot_Level_RequireIdleTask               = (1<<13) ,
    oC_Boot_Level_RequireExcHan                 = (1<<14) ,
    oC_Boot_Level_RequireScreenMan              = (1<<15) ,
    oC_Boot_Level_0                             = oC_Boot_Level_RequireClock            |
                                                  oC_Boot_Level_RequireMemoryManager    |
                                                  oC_Boot_Level_RequireExcHan           |
                                                  oC_Boot_Level_RequireScreenMan        |
                                                  oC_Boot_Level_RequireStreamsManager   |
                                                  oC_Boot_Level_RequireKernelPrint      |
                                                  oC_Boot_Level_RequireDriversManager   ,
    oC_Boot_Level_1                             = oC_Boot_Level_0                       |
                                                  oC_Boot_Level_RequireProcessManager   |
                                                  oC_Boot_Level_RequireThreadManager    |
                                                  oC_Boot_Level_RequireUsersManager     |
                                                  oC_Boot_Level_RequireKernelTime       |
                                                  oC_Boot_Level_RequireProgramManager   ,
    oC_Boot_Level_2                             = oC_Boot_Level_1                       |
                                                  oC_Boot_Level_RequireDeleteDeamon,
    oC_Boot_Level_3                             = oC_Boot_Level_2                       |
                                                  oC_Boot_Level_RequireFileSystem,
    oC_Boot_Level_4                             = oC_Boot_Level_3                       |
                                                  oC_Boot_Level_RequireNetwork,
    oC_Boot_Level_5                             = oC_Boot_Level_4,
    oC_Boot_Level_6                             = oC_Boot_Level_5,
    oC_Boot_Level_SystemRunning                 = oC_Boot_Level_6 | oC_Boot_Level_RequireIdleTask,
    oC_Boot_Level_DonotRequireAnything          = 0
} oC_Boot_Level_t;

#undef  _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface of boot loader
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________
//! @addtogroup Boot
//! @{

//==========================================================================================================================================
/**
 * @brief main function of the system
 *
 * The function is the main function of the system. It is called after the system starts.
 *
 */
//==========================================================================================================================================
extern void oC_Boot_Main(void);

//==========================================================================================================================================
/**
 * @brief returns current boot level
 *
 * Returns current level of system boot
 */
//==========================================================================================================================================
extern oC_Boot_Level_t oC_Boot_GetCurrentBootLevel (void);

//==========================================================================================================================================
/**
 * @brief returns timestamp of startup
 */
//==========================================================================================================================================
extern oC_Timestamp_t oC_Boot_GetStartupTimestamp( void );

//==========================================================================================================================================
/**
 * @brief restarts the system
 *
 * The function is for restarting the system
 *
 * @param Reason		The parameter is for saving the reason why the system was restarted
 * @param User  		This holds user, that calls restarting the system
 *
 */
//==========================================================================================================================================
extern void oC_Boot_Restart( oC_Boot_Reason_t Reason , oC_User_t User );

//==========================================================================================================================================
/**
 * @brief shutting down the system
 *
 * The function is for shutting down the system.
 *
 * @param Reason        The parameter is for saving the reason why the system was turned off
 * @param User          This holds user, that calls the function
 */
//==========================================================================================================================================
extern void oC_Boot_Shutdown( oC_Boot_Reason_t Reason , oC_User_t User );

//==========================================================================================================================================
/**
 * @brief returns reason of shutting down
 *
 * The function returns reason of last shutting down the system
 *
 * @return reason of last shutdown operation
 */
//==========================================================================================================================================
extern oC_Boot_Reason_t oC_Boot_GetLastShutdownReason( void );

//==========================================================================================================================================
/**
 * @brief returns id of user that shutdown the system last time
 *
 * The function is for reading the user, that cause shutdown operation for last time.
 *
 * @return user that caused shutdown operation
 */
//==========================================================================================================================================
extern oC_User_t oC_Boot_GetLastShutdownUser( void );

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________
//! @}

#endif /* INC_BOOT_OC_BOOT_H_ */
