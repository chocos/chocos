/** ****************************************************************************************************************************************
 *
 * @file       oc_core.h
 *
 * @brief      The file with an interface for the Core Space
 *
 * @author     Patryk Kubiak - (Created on: 29 kwi 2015 20:10:10) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup CoreSpace Core Space
 * 
 * @brief The core space of the system
 * 
 * @defgroup Kernel Kernel
 * @ingroup CoreSpace
 * @brief The kernel of the system
 * 
 * @defgroup CoreThreadsSync Threads' Synchronization 
 * @ingroup CoreSpace
 * @brief The synchronization primitives
 * 
 * @defgroup Drivers System Drivers
 * @ingroup CoreSpace
 * @brief The list of available system drivers
 * 
 * @defgroup GUI    Graphical User Interface
 * @ingroup CoreSpace
 * @brief Module for handling graphical user interface
 * 
 * @defgroup CoreNetwork Network
 * @ingroup CoreSpace
 * @brief The network module
 * 
 * @defgroup CoreFileSystem File System
 * @ingroup CoreSpace
 * @brief The file system module
 * 
 ******************************************************************************************************************************************/
#ifndef INC_OC_CORE_H_
#define INC_OC_CORE_H_

/** ========================================================================================================================================
 *
 *              The section with core description
 *
 *  ======================================================================================================================================*/
#define _________________________________________CORE_DESCRIPTION_SECTION__________________________________________________________________

/**
 * @page CoreSpace Core Space
 *
 * ![Core Architecture](core_architecture.jpg)
 *
 */

#undef  _________________________________________CORE_DESCRIPTION_SECTION__________________________________________________________________

#endif /* INC_OC_CORE_H_ */
