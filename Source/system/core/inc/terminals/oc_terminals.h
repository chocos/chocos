/** ****************************************************************************************************************************************
 *
 * @file       oc_terminals.h
 *
 * @brief      List of terminals standards
 *
 * @author     Patryk Kubiak - (Created on: 27 05 2015 18:03:26)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Terminals Terminals
 * @ingroup GUI
 * @brief The module for handling terminals
 * 
 ******************************************************************************************************************************************/


#ifndef INC_TERMINAL_STANDARDS_OC_TERMINALS_H_
#define INC_TERMINAL_STANDARDS_OC_TERMINALS_H_

#include <oc_vt100.h>

#endif /* INC_TERMINAL_STANDARDS_OC_TERMINALS_H_ */
