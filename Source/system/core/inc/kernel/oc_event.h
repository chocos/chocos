/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for event module
 *
 * @file       oc_event.h
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 18:37:47)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Event Event
 * @ingroup CoreThreadsSync
 * @brief Threads synchronization
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_EVENT_H_
#define INC_KERNEL_OC_EVENT_H_

#include <stdbool.h>
#include <oc_time.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Event
//! @{

//==========================================================================================================================================
/**
 * @brief Stores the event context
 */
//==========================================================================================================================================
typedef struct Event_t * oC_Event_t;

//==========================================================================================================================================
/**
 * @brief Event state
 */
//==========================================================================================================================================
typedef enum
{
    oC_Event_State_Active               = 0xFFFFFFFFU,  //!< Event is active
    oC_Event_State_Inactive             = 0 ,           //!< Event is inactive
} oC_Event_State_t;

//==========================================================================================================================================
/**
 * @brief Event state mask
 */
//==========================================================================================================================================
typedef enum
{
    oC_Event_StateMask_Full             = oC_Event_State_Active,    //!< Full mask
    oC_Event_StateMask_DifferentThan    = 0 ,                       //!< Different than
} oC_Event_StateMask_t;

//==========================================================================================================================================
/**
 * @brief Stores the event protection
 * 
 * If the event is protected, then it cannot be deleted
 */
//==========================================================================================================================================
typedef enum
{
    oC_Event_Protect_NotProtected   = 0 ,       //!< Event is not protected and can be deleted
    oC_Event_Protect_Protected      = 0xFF      //!< Event is protected and cannot be deleted
} oC_Event_Protect_t;

//==========================================================================================================================================
/**
 * @brief Stores the event compare type
 */
//==========================================================================================================================================
typedef enum
{
    oC_Event_CompareType_Equal ,            //!< Equal
    oC_Event_CompareType_Less ,             //!< Less
    oC_Event_CompareType_LessOrEqual ,      //!< Less or equal
    oC_Event_CompareType_Greater ,          //!< Greater
    oC_Event_CompareType_GreaterOrEqual ,   //!< Greater or equal
    oC_Event_CompareType_NumberOfElements   //!< Number of elements
} oC_Event_CompareType_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Event
//! @{

extern oC_Event_t       oC_Event_New            ( oC_Event_State_t InitState , Allocator_t Allocator , AllocationFlags_t Flags );
extern bool             oC_Event_Delete         ( oC_Event_t * Event , AllocationFlags_t Flags );
extern bool             oC_Event_ProtectDelete  ( oC_Event_t Event , Allocator_t Allocator , oC_Event_Protect_t Protection );
extern bool             oC_Event_IsCorrect      ( oC_Event_t Event );
extern oC_Event_State_t oC_Event_GetState       ( oC_Event_t Event );
extern bool             oC_Event_SetState       ( oC_Event_t Event , oC_Event_State_t State );
extern bool             oC_Event_ClearStateBits ( oC_Event_t Event , uint32_t StateMask );
extern bool             oC_Event_SetStateBits   ( oC_Event_t Event , uint32_t StateBits );
extern bool             oC_Event_ReadState      ( oC_Event_t Event , oC_Event_State_t * outState );
extern bool             oC_Event_WaitForState   ( oC_Event_t Event , oC_Event_State_t State , oC_Event_StateMask_t StateMask , oC_Time_t Timeout );
extern bool             oC_Event_WaitForValue   ( oC_Event_t Event , oC_Event_State_t Value , oC_Event_CompareType_t CompareType, oC_Time_t Timeout );
extern bool             oC_Event_WaitForBitSet  ( oC_Event_t Event , oC_Event_StateMask_t AvailableBitsMask, oC_Time_t Timeout );
extern bool             oC_Event_WaitForBitClear( oC_Event_t Event , oC_Event_StateMask_t AvailableBitsMask, oC_Time_t Timeout );


#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_EVENT_H_ */
