/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for thread managing
 *
 * @file       oc_thread.h
 *
 * @author     Patryk Kubiak - (Created on: 18 maj 2015 19:38:41) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup    Thread  Thread
 * @ingroup     Kernel
 * @brief     The object for handling the thread
 * 
 * @section Introduction
 * 
 * @par
 * The **Thread** object is for handling the thread. It is the main object for the thread management. It is possible to create, delete, run, 
 * cancel, block, unblock, sleep, and check the thread. The thread is the main object for the multithreading. 
 * 
 * @par
 * To create the thread, the function #oC_Thread_New should be called. It creates the thread object, but it does not start the thread. To start
 * the thread, the function #oC_Thread_Run should be called. 
 * 
 * 
 * @par
 * Example of usage:
 * @code{.c}
  
#include <oc_thread.h>
#include <stdio.h>

void ThreadFunction( void * Parameter )
{
    const char* hello = (const char*)Parameter;
    printf("Thread started\n");
    if(hello)
    {
        printf("Parameter: %s\n",hello); // should print "Hello World!"
    }
    while(1)
    {
        printf("Thread running\n");
    }
}

int main()
{
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , "Hello World!" );
    if(thread)
    {
        oC_Thread_Run(thread);
    }
    else
    {
        printf("Cannot create thread\n");
    }
    
    return 0;
}

   @endcode
 * 
 * 
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_THREAD_H_
#define INC_KERNEL_OC_THREAD_H_

#include <stdbool.h>
#include <stdint.h>
#include <oc_ktime.h>
#include <oc_memman.h>
#include <oc_list.h>
#include <oc_memory.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Thread
//! @{

//=========================================================================================================================================
/**
 * @brief Context structure for the thread
 */
//=========================================================================================================================================
typedef struct Thread_t * oC_Thread_t;

//=========================================================================================================================================
/**
 * @brief Stores priority of the thread
 */
//=========================================================================================================================================
typedef uint32_t oC_Thread_Priority_t;

//=========================================================================================================================================
/**
 * @brief The thread function
 * 
 * Main function of the thread
 * 
 * @param UserParameter - The parameter passed to the thread
 * 
 * @note The function should not return
 */
//=========================================================================================================================================
typedef void (*oC_Thread_Function_t)( void * UserParameter );

//=========================================================================================================================================
/**
 * @brief stores unblock state
 * 
 * The type stores the condition when the thread should be unblocked
 */
//=========================================================================================================================================
typedef enum
{
    oC_Thread_Unblock_WhenEqual ,                       //!< Unblock when the value is equal
    oC_Thread_Unblock_WhenNotEqual ,                    //!< Unblock when the value is not equal
    oC_Thread_Unblock_WhenSmaller ,                     //!< Unblock when the value is smaller
    oC_Thread_Unblock_WhenGreater ,                     //!< Unblock when the value is greater
    oC_Thread_Unblock_WhenSmallerOrEqual ,              //!< Unblock when the value is smaller or equal
    oC_Thread_Unblock_WhenGreaterOrEqual ,              //!< Unblock when the value is greater or equal
    oC_Thread_Unblock_WhenAtLeastOneBitIsSet ,          //!< Unblock when at least one bit is set
    oC_Thread_Unblock_WhenAtLeastOneBitIsCleared ,      //!< Unblock when at least one bit is cleared
} oC_Thread_Unblock_t;

//=========================================================================================================================================
/**
 * @brief stores unblock mask
 * 
 * The type stores the mask for unblocking the thread
 */
//=========================================================================================================================================
typedef enum
{
    oC_Thread_UnblockMask_None = 0 ,                //!< No mask
    oC_Thread_UnblockMask_All  = 0xFFFFFFFFULL      //!< All bits
}oC_Thread_UnblockMask_t;

//=========================================================================================================================================
/**
 * @brief function for reverting changes made by the thread
 * 
 * The function is called when the thread is being finished. The function should revert all changes made by the thread
 */
//=========================================================================================================================================
typedef bool (*oC_Thread_RevertFunction_t)( oC_Thread_t Thread , void * Object , uint32_t Parameter );

//=========================================================================================================================================
/**
 * @brief function for finishing the thread
 * 
 * The function is called when the thread is being finished. 
 */
//=========================================================================================================================================
typedef void (*oC_Thread_FinishedFunction_t)( oC_Thread_t Thread , void * Parameter );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Thread
//! @{

extern oC_Thread_t              oC_Thread_New               ( oC_Thread_Priority_t Priority , oC_Int_t StackSize , void * Process , const char * Name , oC_Thread_Function_t Function , void * Parameter );
extern bool                     oC_Thread_IsOwnedByStack    ( oC_Thread_t Thread , const void * Address , bool * outAddressInRedZone );
extern oC_Thread_t              oC_Thread_CloneWithNewStack ( oC_Thread_t Thread , oC_MemorySize_t NewStackSize );
extern bool                     oC_Thread_Delete            ( oC_Thread_t * Thread );
extern bool                     oC_Thread_IsCorrect         ( oC_Thread_t Thread );
extern bool                     oC_Thread_IsActive          ( oC_Thread_t Thread );
extern bool                     oC_Thread_SetBlocked        ( oC_Thread_t Thread , uint32_t * BlockingFlag , oC_Thread_Unblock_t Unblock , uint32_t StateToUnblock , oC_Thread_UnblockMask_t UnblockMask , oC_Time_t Timeout );
extern bool                     oC_Thread_SetUnblocked      ( oC_Thread_t Thread );
extern bool                     oC_Thread_IsBlocked         ( oC_Thread_t Thread );
extern bool                     oC_Thread_IsBlockedBy       ( oC_Thread_t Thread , uint32_t * BlockingFlag );
extern bool                     oC_Thread_Run               ( oC_Thread_t Thread );
extern bool                     oC_Thread_Cancel            ( oC_Thread_t * Thread );
extern void *                   oC_Thread_GetContext        ( oC_Thread_t Thread );
extern oC_Thread_Priority_t     oC_Thread_GetPriority       ( oC_Thread_t Thread );
extern void *                   oC_Thread_GetParameter      ( oC_Thread_t Thread );
extern const char *             oC_Thread_GetName           ( oC_Thread_t Thread );
extern bool                     oC_Thread_Sleep             ( oC_Thread_t Thread , oC_Time_t Time );
extern oC_Time_t                oC_Thread_GetExecutionTime  ( oC_Thread_t Thread );
extern bool                     oC_Thread_AddToExecutionTime( oC_Thread_t Thread , oC_Time_t Time );
extern uint64_t                 oC_Thread_GetLastExecutionTick(oC_Thread_t Thread );
extern bool                     oC_Thread_SetLastExecutionTick(oC_Thread_t Thread , uint64_t Tick );
extern oC_UInt_t                oC_Thread_GetStackSize      ( oC_Thread_t Thread );
extern oC_Int_t                 oC_Thread_GetFreeStackSize  ( oC_Thread_t Thread , bool Current );
extern bool                     oC_Thread_SaveToRevert      ( oC_Thread_t Thread , oC_Thread_RevertFunction_t Function, void * Object , uint32_t Parameter );
extern bool                     oC_Thread_RemoveFromRevert  ( oC_Thread_t Thread , oC_Thread_RevertFunction_t Function, void * Object );
extern bool                     oC_Thread_SetFinishedFunction( oC_Thread_t Thread , oC_Thread_FinishedFunction_t Function, void * Parameter );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_THREAD_H_ */
