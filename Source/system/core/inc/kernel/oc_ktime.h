/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface of kernel time module
 *
 * @file       oc_ktime.h
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 23:18:54)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup KTime Kernel Time
 * @ingroup Kernel
 * @brief The module for kernel time operations
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_KTIME_H_
#define INC_KERNEL_OC_KTIME_H_

#include <oc_time.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup KTime
//! @{

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup KTime
//! @{

extern oC_ErrorCode_t       oC_KTime_TurnOn         ( void );
extern oC_ErrorCode_t       oC_KTime_TurnOff        ( void );
extern oC_Timestamp_t       oC_KTime_GetTimestamp   ( void );
extern uint64_t             oC_KTime_GetCurrentTick ( void );
extern oC_Time_t            oC_KTime_TickToTime     ( uint64_t Ticks );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with static inlines functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @addtogroup KTime
//! @{

//==========================================================================================================================================
/**
 * @brief calculates timeout
 *
 * The function calculates timeout from the current time to the end timestamp
 *
 * @param EndTimestamp      The end timestamp
 *
 * @return timeout
 */
//==========================================================================================================================================
static inline oC_Time_t oC_KTime_CalculateTimeout( oC_Timestamp_t EndTimestamp )
{
    oC_Timestamp_t timestamp = oC_KTime_GetTimestamp();
    return (EndTimestamp > timestamp) ? EndTimestamp - timestamp : 0;
}

//==========================================================================================================================================
/**
 * @brief calculates timeout with limit
 *
 * The function calculates timeout from the current time to the end timestamp with limit
 *
 * @param EndTimestamp      The end timestamp
 * @param Limit             The limit of the timeout
 *
 * @return timeout
 */
//==========================================================================================================================================
static inline oC_Time_t oC_KTime_CalculateTimeoutWithLimit( oC_Timestamp_t EndTimestamp , oC_Time_t Limit )
{
    oC_Time_t timeout = oC_KTime_CalculateTimeout(EndTimestamp);

    return timeout > Limit ? Limit : timeout;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_KTIME_H_ */
