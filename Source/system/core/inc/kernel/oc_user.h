/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for user system.
 *
 * @file       oc_user.h
 *
 * @author     Patryk Kubiak - (Created on: 18 maj 2015 20:29:47) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup User User
 * @ingroup Kernel
 * @brief The module for handling users
 * 
 ******************************************************************************************************************************************/


#ifndef INC_USER_OC_USER_H_
#define INC_USER_OC_USER_H_

#include <stdbool.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup User
//! @{

typedef struct User_t * oC_User_t;

typedef enum
{
    oC_User_Permissions_Root = 0xFF ,
} oC_User_Permissions_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

extern oC_User_t                oC_User_New              ( Allocator_t Allocator , AllocationFlags_t Flags , const char * Name , const char * Password , oC_User_Permissions_t Permissions );
extern bool                     oC_User_Delete           ( oC_User_t * User );
extern bool                     oC_User_IsCorrect        ( oC_User_t User );
extern const char *             oC_User_GetName          ( oC_User_t User );
extern bool                     oC_User_Rename           ( oC_User_t User , const char * NewName );
extern bool                     oC_User_CheckPassword    ( oC_User_t User , const char * Password );
extern bool                     oC_User_CheckPermissions ( oC_User_t User , oC_User_Permissions_t Permissions );
extern oC_User_Permissions_t    oC_User_GetPermissions   ( oC_User_t User);
extern bool                     oC_User_ModifyPermissions( oC_User_t User , oC_User_Permissions_t Permissions );
extern bool                     oC_User_IsRoot           ( oC_User_t User);

#endif /* INC_USER_OC_USER_H_ */
