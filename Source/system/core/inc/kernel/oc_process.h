/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for process mechanism
 *
 * @file       oc_process.h
 *
 * @author     Patryk Kubiak - (Created on: 18 05 2015 19:53:22)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Process Process
 * @ingroup Kernel
 * @brief The module for managing processes
 * 
 * The module provides the interface for managing processes. The process is a program that is running in the system. The process has its own memory
 * space, its own threads, and its own resources. The module provides the interface for creating, deleting, and managing processes. 
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_PROCESS_H_
#define INC_KERNEL_OC_PROCESS_H_

#include <stdbool.h>
#include <stdint.h>
#include <oc_thread.h>
#include <oc_user.h>
#include <oc_stream.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________
//! @addtogroup Process
//! @{

#define oC_PROCESS_THREAD_PRIORITY_BITS     28
#define oC_PROCESS_PRIORITY_BITS            4

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Process
//! @{

//==========================================================================================================================================
/**
 * @brief Context of the process module
 */
//==========================================================================================================================================
typedef struct Process_t * oC_Process_t;

//==========================================================================================================================================
/**
 * @brief The priority of the process
 */
//==========================================================================================================================================
typedef enum
{
    oC_Process_Priority_IdleProcess             =  0 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The idle process
    oC_Process_Priority_UserSpaceProcess        =  1 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The user space process
    oC_Process_Priority_UserSpaceDaemon         =  2 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The user space daemon
    oC_Process_Priority_CoreSpaceProcess        =  4 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The core space process
    oC_Process_Priority_CoreSpaceDaemon         =  5 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The core space daemon
    oC_Process_Priority_SystemHelperProcess     =  6 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The system helper process
    oC_Process_Priority_SystemHelperDeamon      =  7 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The system helper daemon
    oC_Process_Priority_DefaultProcess          =  8 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The default process
    oC_Process_Priority_NetworkHandlerProcess   =  8 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The network handler process
    oC_Process_Priority_SystemHandlerProcess    =  8 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The system handler process
    oC_Process_Priority_SystemHandlerDeamon     =  9 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The system handler daemon
    oC_Process_Priority_SystemSecurityProcess   = 10 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The system security process
    oC_Process_Priority_SystemSecurityDeamon    = 11 << oC_PROCESS_THREAD_PRIORITY_BITS,    //!< The system security daemon
}oC_Process_Priority_t;

//==========================================================================================================================================
/**
 * @brief The state of the process
 */
//==========================================================================================================================================
typedef enum
{
    oC_Process_State_Invalid ,          //!< The invalid state
    oC_Process_State_Initialized ,      //!< The initialized state
    oC_Process_State_Run ,              //!< The run state
    oC_Process_State_Asleep ,           //!< The asleep state
    oC_Process_State_Zombie ,           //!< The zombie state (the process is killed, but the system still has some information about it)
    oC_Process_State_Killed             //!< The killed state
} oC_Process_State_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Process
//! @{

extern oC_Process_t          oC_Process_New              ( oC_Process_Priority_t Priority , const char * Name , oC_User_t User , oC_UInt_t HeapMapSize , oC_Stream_t InputStream , oC_Stream_t OutputStream , oC_Stream_t ErrorStream );
extern bool                  oC_Process_Delete           ( oC_Process_t * Process );
extern bool                  oC_Process_IsCorrect        ( oC_Process_t Process );
extern bool                  oC_Process_IsKilled         ( oC_Process_t Process );
extern bool                  oC_Process_ContainsThread   ( oC_Process_t Process , oC_Thread_t Thread );
extern const char *          oC_Process_GetName          ( oC_Process_t Process );
extern oC_Process_State_t    oC_Process_GetState         ( oC_Process_t Process );
extern oC_User_t             oC_Process_GetUser          ( oC_Process_t Process );
extern oC_Stream_t           oC_Process_GetInputStream   ( oC_Process_t Process );
extern oC_Stream_t           oC_Process_GetOutputStream  ( oC_Process_t Process );
extern oC_Stream_t           oC_Process_GetErrorStream   ( oC_Process_t Process );
extern oC_ErrorCode_t        oC_Process_SetInputStream   ( oC_Process_t Process , oC_Stream_t Stream );
extern oC_ErrorCode_t        oC_Process_SetOutputStream  ( oC_Process_t Process , oC_Stream_t Stream );
extern oC_ErrorCode_t        oC_Process_SetErrorStream   ( oC_Process_t Process , oC_Stream_t Stream );
extern oC_ErrorCode_t        oC_Process_SetPriority      ( oC_Process_t Process , oC_Process_Priority_t Priority );
extern oC_Process_Priority_t oC_Process_GetPriority      ( oC_Process_t Process );
extern oC_ErrorCode_t        oC_Process_LockStdioBuffer  ( oC_Process_t Process , char ** outStdioBuffer , oC_Time_t Timeout );
extern oC_ErrorCode_t        oC_Process_UnlockStdioBuffer( oC_Process_t Process );
extern bool                  oC_Process_IsActive         ( oC_Process_t Process );
extern bool                  oC_Process_Kill             ( oC_Process_t Process );
extern Allocator_t           oC_Process_GetAllocator     ( oC_Process_t Process );
extern oC_HeapMap_t          oC_Process_GetHeapMap       ( oC_Process_t Process );
extern oC_ErrorCode_t        oC_Process_Sleep            ( oC_Process_t Process , oC_Time_t Timeout );
extern oC_UInt_t             oC_Process_GetHeapMapSize   ( oC_Process_t Process );
extern oC_UInt_t             oC_Process_GetFreeHeapMapSize(oC_Process_t Process );
extern oC_UInt_t             oC_Process_GetThreadsStackSize(oC_Process_t Process );
extern oC_Int_t              oC_Process_GetFreeThreadsStackSize(oC_Process_t Process , bool Current );
extern oC_Time_t             oC_Process_GetExecutionTime ( oC_Process_t Process );
extern oC_UInt_t             oC_Process_GetUsedRamSize   ( oC_Process_t Process );
extern oC_IoFlags_t          oC_Process_GetIoFlags       ( oC_Process_t Process );
extern oC_ErrorCode_t        oC_Process_SetIoFlags       ( oC_Process_t Process , oC_IoFlags_t IoFlags );
extern oC_ErrorCode_t        oC_Process_WaitForFinish    ( oC_Process_t Process , oC_Time_t CheckPeriod, oC_Time_t Timeout );
extern char *                oC_Process_GetPwd           ( oC_Process_t Process );
extern oC_ErrorCode_t        oC_Process_SetPwd           ( oC_Process_t Process , const char * Pwd );
extern oC_UInt_t             oC_Process_GetPid           ( oC_Process_t Process );
extern bool                  oC_Process_SetAllocationLimit( oC_Process_t Process , oC_MemorySize_t Limit );
extern bool                  oC_Process_SetAllocationTracking( oC_Process_t Process, bool Enabled );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_PROCESS_H_ */
