/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for interrupt manager
 *
 * @file       oc_intman.h
 *
 * @author     Patryk Kubiak - (Created on: 18 maj 2015 21:36:05) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup IntMan Interrupt Manager
 * @ingroup Kernel
 * @brief The module for managing interrupts
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_INTMAN_H_
#define INC_KERNEL_OC_INTMAN_H_

#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with functions sections
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION_________________________________________________________________________
//! @addtogroup IntMan
//! @{

extern void oC_IntMan_EnterCriticalSection( void );
extern void oC_IntMan_ExitCriticalSection( void );
extern bool oC_IntMan_AreInterruptsTurnedOn( void );

#undef  _________________________________________INTERFACE_SECTION_________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_INTMAN_H_ */
