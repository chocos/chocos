/** ****************************************************************************************************************************************
 *
 * @brief      Manages news in the system
 * 
 * @file       oc_news.h
 *
 * @author     Patryk Kubiak - (Created on: 12.02.2017 10:58:07) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup News
 * @ingroup Kernel
 * @brief       The module is responsible for storing news in the system
 * 
 * The <b>News</b> module allows for managing news - the messages that are printed to the End-User in a screen as a popup or in a console.
 * The news are stored in a circular buffer, so the oldest news are overwritten by the newest ones. 
 * 
 * To save a news, use the #oC_News_Save function. To read the news, use the #oC_News_Read function.
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_KERNEL_OC_NEWS_H_
#define SYSTEM_CORE_INC_KERNEL_OC_NEWS_H_

#include <oc_memory.h>
#include <oc_time.h>
#include <stdbool.h>

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
//! @addtogroup News
//! @{

extern void oC_News_Save            ( const char * News );
extern bool oC_News_Read            ( char * outNews, oC_MemorySize_t Size, oC_Timestamp_t * outTimestamp );
extern void oC_News_SaveFormatted   ( const char * Format , ... );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
//! @}

#endif /* SYSTEM_CORE_INC_KERNEL_OC_NEWS_H_ */
