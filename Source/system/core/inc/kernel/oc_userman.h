/** ****************************************************************************************************************************************
 *
 * @file          oc_userman.h
 *
 * @brief      File with interface for user system manager
 *
 * @author     Patryk Kubiak - (Created on: 27 05 2015 18:43:20)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup UserMan User Manager
 * @ingroup Kernel
 * @brief The module for handling users
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_USERMAN_H_
#define INC_KERNEL_OC_USERMAN_H_

#include <oc_user.h>
#include <oc_list.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup UserMan
//! @{




#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup UserMan
//! @{

extern oC_ErrorCode_t        oC_UserMan_TurnOn         ( void );
extern oC_ErrorCode_t        oC_UserMan_TurnOff        ( void );
extern oC_ErrorCode_t        oC_UserMan_CreateAndAddUser(const char * Name, const char * Password ,  oC_User_Permissions_t Permissions);
extern oC_ErrorCode_t        oC_UserMan_AddUser        ( oC_User_t User );
extern oC_ErrorCode_t        oC_UserMan_RemoveUser     ( oC_User_t User );
extern oC_List(oC_User_t)    oC_UserMan_GetList        ( void );
extern oC_User_t             oC_UserMan_GetRootUser    ( void );
extern oC_User_t             oC_UserMan_GetUser        ( const char * Name );
extern oC_ErrorCode_t        oC_UserMan_Rename         ( oC_User_t User , const char * NewName );


#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_USERMAN_H_ */
