/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for semaphores
 *
 * @file       oc_semaphore.h
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 18:14:12)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Semaphore Semaphore
 * @ingroup CoreThreadsSync
 * @brief The module for managing semaphores
 * 
 * The module provides the interface for managing semaphores. The semaphore is a synchronization object that allows multiple threads to access shared data
 * in a controlled manner. The semaphore is used to protect the shared data from being accessed by more than one thread at a time. The module provides
 * the interface for creating, deleting, giving, and taking the semaphore. The semaphore can be binary or counting. 
 * 
 * Example:
 * @code{.c}

#include <oc_semaphore.h>
#include <oc_thread.h>
#include <oc_tgui.h>
#include <stdio.h>

void Producer( void * Arg )
{
    oC_Semaphore_t semaphore = (oC_Semaphore_t)Arg;
    while(1)
    {
        if(oC_Semaphore_Give(semaphore, oC_s(1)))
        {
            printf("Semaphore given\n");
        }
        else
        {
            printf("Semaphore not given\n");
        }
        sleep(oC_ms(200));
    }
}

int main( int Argc , char ** Argv )
{
    // Create the semaphore
    oC_Semaphore_t semaphore = oC_Semaphore_New(oC_Semaphore_Type_Binary, oC_Semaphore_InitialValue_GiveBinary, getcurallocator(), AllocationFlags_Default);
    if(semaphore != NULL)
    {
        // Create the thread
        oC_Thread_t thread = oC_Thread_New(0, 0, NULL, "Producer", Producer, semaphore);
        if(thread != NULL)
        {
            // Run the thread
            oC_Thread_Run(thread);
            if(oC_Semaphore_Take(semaphore, oC_s(1)))
            {
                printf("Semaphore taken\n");
            }
            else
            {
                printf("Semaphore not taken\n");
            }
            oC_TGUI_WaitForKeyPress(NULL); // Wait for any key press
        }
        // Delete the semaphore
        oC_Semaphore_Delete(&semaphore, AllocationFlags_Default);
    }

    return 0;

}
 
 * @endcode
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_SEMAPHORE_H_
#define INC_KERNEL_OC_SEMAPHORE_H_

#include <stdbool.h>
#include <stdint.h>
#include <oc_time.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Semaphore
//! @{

//==========================================================================================================================================
/**
 * @brief Stores the semaphore context
 */
//==========================================================================================================================================
typedef struct Semaphore_t * oC_Semaphore_t;

//==========================================================================================================================================
/**
 * @brief Stores the semaphore type
 * 
 * The enumeration is for saving the type of semaphore
 */
//==========================================================================================================================================
typedef enum
{
    oC_Semaphore_Type_Binary        = 1 ,           //!< The binary semaphore
    oC_Semaphore_Type_Counting      = 0xFFFFFFFF    //!< The counting semaphore
} oC_Semaphore_Type_t;

//==========================================================================================================================================
/**
 * @brief Stores the semaphore initial value
 * 
 * The enumeration is for saving the initial value of semaphore
 */
//==========================================================================================================================================
typedef enum
{
    oC_Semaphore_InitialValue_AllTaken      = 0,            //!< The semaphore is created with all taken
    oC_Semaphore_InitialValue_GiveBinary    = 1,            //!< The semaphore is created with given binary value
    oC_Semaphore_InitialValue_MaxValue      = 0xFFFFFFFF    //!< The semaphore is created with maximal value
} oC_Semaphore_InitialValue_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Semaphore
//! @{

extern oC_Semaphore_t   oC_Semaphore_New            ( oC_Semaphore_Type_t Type , oC_Semaphore_InitialValue_t InitialValue , Allocator_t Allocator , AllocationFlags_t Flags );
extern bool             oC_Semaphore_Delete         ( oC_Semaphore_t * Semaphore , AllocationFlags_t Flags );
extern bool             oC_Semaphore_IsCorrect      ( oC_Semaphore_t Semaphore );
extern bool             oC_Semaphore_Give           ( oC_Semaphore_t Semaphore );
extern bool             oC_Semaphore_GiveCounting   ( oC_Semaphore_t Semaphore , uint32_t Count );
extern bool             oC_Semaphore_Take           ( oC_Semaphore_t Semaphore , oC_Time_t Timeout );
extern bool             oC_Semaphore_TakeCounting   ( oC_Semaphore_t Semaphore , uint32_t Count , oC_Time_t Timeout );
extern bool             oC_Semaphore_TakeCountingSoft( oC_Semaphore_t Semaphore , uint32_t* Count, uint32_t Min , oC_Time_t Timeout );
extern bool             oC_Semaphore_ForceValue     ( oC_Semaphore_t Semaphore , uint32_t Value );
extern uint32_t         oC_Semaphore_Value          ( oC_Semaphore_t Semaphore );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_SEMAPHORE_H_ */
