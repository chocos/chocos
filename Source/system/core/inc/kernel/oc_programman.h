/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for program manager
 *
 * @file       oc_programman.h
 *
 * @author     Patryk Kubiak - (Created on: 21 05 2015 18:56:45)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ProgramMan Program Manager
 * @ingroup Kernel
 * @brief The module for managing programs
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_PROGRAMMAN_H_
#define INC_KERNEL_OC_PROGRAMMAN_H_

#include <stdbool.h>
#include <oc_errors.h>
#include <oc_program.h>
#include <oc_list.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup ProgramMan
//! @{




#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with function
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup ProgramMan
//! @{

extern oC_ErrorCode_t               oC_ProgramMan_TurnOn                ( void );
extern oC_ErrorCode_t               oC_ProgramMan_TurnOff               ( void );
extern oC_ErrorCode_t               oC_ProgramMan_AddProgram            ( oC_Program_t Program );
extern oC_ErrorCode_t               oC_ProgramMan_RemoveProgram         ( oC_Program_t Program );
extern oC_ErrorCode_t               oC_ProgramMan_SetDefaultProgram     ( oC_Program_t Program );
extern oC_List(oC_Program_t)        oC_ProgramMan_GetList               ( void );
extern oC_Program_t                 oC_ProgramMan_GetDefaultProgram     ( void );
extern oC_Program_t                 oC_ProgramMan_GetTerminalProgram    ( void );
extern oC_Program_t                 oC_ProgramMan_GetProgram            ( const char * Name );
extern oC_ErrorCode_t               oC_ProgramMan_RunDefaultProgram     ( oC_User_t User );
extern uint32_t                     oC_ProgramMan_RerunDefualtIfNotRun  ( oC_User_t User );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_PROGRAMMAN_H_ */
