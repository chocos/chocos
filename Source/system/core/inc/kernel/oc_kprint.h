/** ****************************************************************************************************************************************
 *
 * @file       oc_kprint.h
 *
 * @brief      The file with interface for kernel print operations
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 21:12:32)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup KPrint Kernel Print
 * @ingroup Kernel
 * @brief The module for kernel print operations
 *  
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_KPRINT_H_
#define INC_KERNEL_OC_KPRINT_H_

#include <oc_driver.h>
#include <stdarg.h>

//==========================================================================================================================================
/**
 * @brief Definition of EOF character
 */
//==========================================================================================================================================
#define oC_KPrint_EOF       (-1)

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup KPrint
//! @{

//==========================================================================================================================================
/**
 * @brief function pointer to function which returns character from input device
 * 
 * The function pointer to function which returns character from input device. It is used in scanf function.
 * 
 * @param UserData          pointer to user data
 * @param MovePosition      if true then move position in input device
 * 
 * @return character from input device
 */
//==========================================================================================================================================
typedef int (*oC_KPrint_GetChar_t)( void* UserData, bool MovePosition );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup KPrint
//! @{

extern oC_ErrorCode_t   oC_KPrint_TurnOn            ( void );
extern oC_ErrorCode_t   oC_KPrint_TurnOff           ( void );
extern int              oC_KPrint_GetCharFromString ( void* UserData, bool MovePosition );
extern oC_ErrorCode_t   oC_KPrint_Format            ( char * outBuffer , oC_UInt_t Size , const char * Format , va_list ArgumentList, oC_UInt_t* outSize );
extern oC_ErrorCode_t   oC_KPrint_Printf            ( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , ... );
extern oC_ErrorCode_t   oC_KPrint_VPrintf           ( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , va_list ArgumentList );
extern oC_ErrorCode_t   oC_KPrint_DriverPrintf      ( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , oC_Driver_t Driver , const char * Format , ... );
extern oC_ErrorCode_t   oC_KPrint_FormatScanf       ( const char * Buffer , const char * Format , va_list ArgumentList );
extern oC_ErrorCode_t   oC_KPrint_FormatScanfFromStream ( oC_KPrint_GetChar_t GetChar , void * UserData , const char * Format , va_list ArgumentList );
extern oC_ErrorCode_t   oC_KPrint_Scanf             ( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , ... );
extern oC_ErrorCode_t   oC_KPrint_VScanf            ( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , va_list ArgumentList );
extern oC_ErrorCode_t   oC_KPrint_WriteToStdOut     ( oC_IoFlags_t IoFlags , const char *    Buffer , oC_MemorySize_t Size );
extern oC_ErrorCode_t   oC_KPrint_ReadFromStdIn     ( oC_IoFlags_t IoFlags ,       char * outBuffer , oC_MemorySize_t Size );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_KPRINT_H_ */
