/** ****************************************************************************************************************************************
 *
 * @brief      The file with memory manager interface
 *
 * @file       oc_memman.h
 *
 * @author     Patryk Kubiak - (Created on: 18 maj 2015 19:37:25) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup MemMan Memory Manager
 * @ingroup Kernel
 * @brief Module for manage memory space
 *
 * @section Introduction
 *
 * @par
 * The **Memory Manager** module is for operations based on the machine memory. It is part of the **core space** and require the **MEM-LLD** to work.
 * It is important, that it also manage the power of this module. The **MemMan** must be turned on before usage, using function #oC_MemMan_TurnOn. It initialize
 * pointers, and submodules to work with it. When the Memory Manager is not needed anymore (it is not recommended but possible) it can be
 * turned off using #oC_MemMan_TurnOff function. Note, that this function release all allocated memory, and if there is some still used pointers
 * it can cause some bugs.
 *
 * @note When the **MemMan** module is turned off, the #oC_MemMan_EventFlags_ModuleTurningOff event is called
 *
 * @par
 * When the module is turned on, it is possible to set the **module event handler**. This is a pointer to a function, that will be executed,
 * when some of module events occurs. This pointer can be set only once until turning off the module by function #oC_MemMan_SetEventHandler.
 * Look at #oC_MemMan_EventFlags_t for list of available events.
 *
 * @par
 * The main purpose of this is managing the memory allocations. It helps to reserve memory on the machine heap, but it also protects
 * this memory. The first of all, to allocate, it is required to create the special structure, named as **Allocator**, that is
 * represented by the type #oC_MemMan_Allocator_t. Thanks to this type, none of allocations is anonymous. The **Allocator** should be created
 * per module as static constant definition, and the pointer to it should be given in each allocation in the module. The **Memory Manager**
 * provide the interface to get size of all memory allocated for the allocator, and release it all by functions #oC_MemMan_GetMemoryOfAllocatorSize
 * and #oC_MemMan_FreeAllMemoryOfAllocator. There are also some events **Allocator specific**. It is possible to set event handler and choose
 * events per allocator in the #oC_MemMan_Allocator_t struct. Some example of usage is given below:
 *
 * @code{.c}
 * static void EventHandlerFunction( void * Address , oC_MemMan_EventFlags_t Flags , const char * Function, uint32_t LineNumber )
 * {
 *      // handling of events
 * }
 *
 * static const oC_MemMan_Allocator_t Allocator = {
 *     .Name         = "Example allocator" ,
 *     .EventHandler = EventHandlerFunction ,
 *     .EventFlags   = oC_MemMan_EventFlags_BufferOverflow | oC_MemMan_EventFlags_PossibleMemoryLeak
 * };
 *
 * void main()
 * {
 *    uint32_t * Buffer = oC_MemMan_Allocate( sizeof(uint32_t) * 10 , &Allocator , __LINE__ , oC_MemMan_AllocationFlags_Default);
 *
 *    for(int i = 0; i <= 10 ; i++)
 *    {
 *      // This loop overwrite the buffer.
 *      Buffer[i] = 0;
 *    }
 *
 *    // When this function is called, the overflow will be detected, and EventHandlerFunction function will be called.
 *    oC_MemMan_CheckOverflow();
 *
 *    oC_MemMan_Free( Buffer );
 * }
 * @endcode
 *
 * @par
 * As in the example above, standard allocation of memory is possible by using the function #oC_MemMan_Allocate. It is standard, and the most
 * comfortable way, but it require a lot of memory for managing it (something about 20 bytes per each allocation). Because of that, the module
 * supports some alternative way for memory allocation, that is designed for taking minimum additional memory (only 1 bit for 1 memory word).
 * This way is better for cases, when some module needs a lot of little allocations with always known sizes (for example for the same structures),
 * and it is called **raw allocation**. It can be handled by functions #oC_MemMan_RawAllocate and #oC_MemMan_RawFree. To allocate a memory in the
 * raw way the **heap map** must be created. It can be done by function #oC_MemMan_AllocateHeapMap. It is a special memory block, where your
 * raw allocations will be placed. It is designed as follows:
 *
 * @code
 *  Start address of the memory block  _ _ +-----------------------------------+
 *                                         |                                   |   Bit map - each bit in this section is reserved for one word (oC_UInt_t)
 *                                         |              BIT MAP              |   in HEAP section. If bit is set, then the word is used (allocated)
 *                                         |                                   |
 *                                         +-----------------------------------+
 *                                         |                                   |   Heap - memory for allocations (this stores your data)
 *                                         |               HEAP                |
 *                                         |                                   |
 *    End address of the memory block  _ _ +-----------------------------------+
 * @endcode
 * @par
 * So the part of the heap map memory will be reserved for the bit map. Each bit in this map is for 1 word of the heap, and when it is set to 1,
 * then the word is already allocated. It is recommended to allocate just something about 3% more memory as needed. Here some example of usage:
 *
 * @code{.c}
 * static const oC_MemMan_Allocator_t Allocator = {"Example memory heap"};
 *
 * int main()
 * {
 *      // It allocates 100 bytes for this heap map, so if machine will be 32 bit, 3 bytes (it will be aligned to the word size) will be reserved for memory managing.
 *      // 3 bytes = 8 bits * 3 = 24 bits. It is enough for 24 words.
 *      oC_HeapMap_t HeapMap = oC_MemMan_AllocateHeapMap( sizeof(uint8_t) * 100, &Allocator , __FUNCTION__,  __LINE__ , oC_MemMan_AllocationFlags_Default);
 *
 *      // Now raw allocate 10 bytes
 *      uint8_t * buffer= oC_MemMan_RawAllocate( &HeapMap , 10 * sizeof(uint8_t) , __FUNCTION__,  __LINE__ , oC_MemMan_AllocationFlags_Default);
 *
 *      // Now release 10 bytes
 *      oC_MemMan_RawFree( &HeapMap, buffer , 10 * sizeof(uint8_t));
 * }
 * @endcode
 *
 * @par
 * The next feature of this module is diagnostic. It helps to check if some error occurs. The diagnostic functions should be called periodically.
 * Each of them checks one problem, and generate events for it. For example, the #oC_MemMan_CheckOverflow function checks, if the memory overflow
 * problem is not occurring, and when it is, the event is generated. Moreover there is one function, called #oC_MemMan_TestAndRepairMainHeap that
 * tries to repair the module without loosing of data. Note, that this way is not safe. The module can only try to restore stability of work, but
 * it does not provide, that data in allocated memory is correct. It is recommended to use it only for some special cases, when the machine cannot
 * be restarted.
 *
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_MEMMAN_H_
#define INC_KERNEL_OC_MEMMAN_H_

#include <oc_mem_lld.h>
#include <oc_errors.h>
#include <stdbool.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________
//! @addtogroup MemMan
//! @{

//==========================================================================================================================================
/**
 * @brief password for core-protected allocations
 */
//==========================================================================================================================================
#define oC_MemMan_CORE_PWD          0xfdabceaa

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @addtogroup MemMan
//! @{

//==========================================================================================================================================
/**
 * @brief stores pointer to function for dumping data
 */
//==========================================================================================================================================
typedef void (*oC_MemMan_DumpFunction_t)(oC_UInt_t Data);

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    Allocator_t Allocator;
    oC_UInt_t   Size;
} oC_MemMan_AllocatorsStats_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    void *          Address;
    oC_UInt_t       Size;
    const char *    Function;
    oC_UInt_t       LineNumber;
    bool            Overflowed;
} oC_MemMan_AllocationStats_t;

#undef  _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________
//! @addtogroup MemMan
//! @{

extern oC_ErrorCode_t  oC_MemMan_TurnOn                      ( void );
extern oC_ErrorCode_t  oC_MemMan_TurnOff                     ( void );
extern void            oC_MemMan_CheckOverflow               ( void );
extern void            oC_MemMan_CheckMemoryExhausted        ( void );
extern void            oC_MemMan_CheckMemoryLeak             ( void );
extern oC_ErrorCode_t  oC_MemMan_TestAndRepairMainHeap       ( oC_MemMan_DumpFunction_t DumpFunction );
extern oC_HeapMap_t    oC_MemMan_AllocateHeapMap             ( oC_UInt_t Size , Allocator_t Allocator , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags );
extern bool            oC_MemMan_FreeHeapMap                 ( oC_HeapMap_t * Map , AllocationFlags_t Flags);
extern void *          oC_MemMan_RawAllocate                 ( oC_HeapMap_t Map , oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags);
extern bool            oC_MemMan_RawFree                     ( oC_HeapMap_t Map , void * Address , oC_UInt_t Size );
extern void *          oC_MemMan_Allocate                    ( oC_UInt_t Size , Allocator_t Allocator , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags , oC_UInt_t Alignment );
extern bool            oC_MemMan_Free                        ( void * Address , AllocationFlags_t Flags);
extern bool            oC_MemMan_FreeAllMemoryOfAllocator    ( Allocator_t Allocator );
extern oC_ErrorCode_t  oC_MemMan_SetEventHandler             ( MemoryEventHandler_t EventHandler );
extern bool            oC_MemMan_ChangeAllocatorOfAddress    ( const void * Address , Allocator_t Allocator );
extern Allocator_t     oC_MemMan_GetAllocatorOfAddress       ( const void * Address );
extern oC_UInt_t       oC_MemMan_GetSizeOfAllocation         ( const void * Address );
extern void *          oC_MemMan_AlignAddress                ( const void * Address );
extern void *          oC_MemMan_AlignAddressTo              ( const void * Address , oC_UInt_t Alignment );
extern bool            oC_MemMan_IsAddressAligned            ( const void * Address );
extern bool            oC_MemMan_IsAddressAlignedTo          ( const void * Address , oC_UInt_t Alignment );
extern oC_UInt_t       oC_MemMan_AlignSize                   ( oC_UInt_t Size );
extern oC_UInt_t       oC_MemMan_AlignSizeTo                 ( oC_UInt_t Size , oC_UInt_t Alignment );
extern bool            oC_MemMan_IsSizeAligned               ( oC_UInt_t Size );
extern oC_UInt_t       oC_MemMan_GetMaximumAllocationSize    ( void );
extern oC_UInt_t       oC_MemMan_GetFlashSize                ( void );
extern oC_UInt_t       oC_MemMan_GetRamSize                  ( void );
extern oC_UInt_t       oC_MemMan_GetFreeFlashSize            ( void );
extern oC_UInt_t       oC_MemMan_GetFreeRamSize              ( void );
extern oC_UInt_t       oC_MemMan_GetExternalHeapSize         ( void );
extern oC_UInt_t       oC_MemMan_GetCoreHeapSize             ( void );
extern oC_UInt_t       oC_MemMan_GetDmaRamHeapSize           ( void );
extern oC_UInt_t       oC_MemMan_GetMemoryOfAllocatorSize    ( Allocator_t Allocator );
extern oC_UInt_t       oC_MemMan_GetAllocationBlockSize      ( void );
extern oC_UInt_t       oC_MemMan_GetHeapMapSize              ( oC_HeapMap_t Map );
extern oC_UInt_t       oC_MemMan_GetFreeHeapMapSize          ( oC_HeapMap_t Map );
extern bool            oC_MemMan_IsFlashAddress              ( const void * Address );
extern bool            oC_MemMan_IsUsedFlashAddress          ( const void * Address );
extern bool            oC_MemMan_IsRamAddress                ( const void * Address );
extern bool            oC_MemMan_IsDynamicAllocatedAddress   ( const void * Address );
extern bool            oC_MemMan_IsStaticRamAddress          ( const void * Address );
extern bool            oC_MemMan_IsAddressCorrect            ( const void * Address );
extern bool            oC_MemMan_IsAllocatorCorrect          ( Allocator_t Allocator );
extern oC_ErrorCode_t  oC_MemMan_ReadAllocatorsStats         ( oC_MemMan_AllocatorsStats_t * outAllocatorsArray  , oC_UInt_t * Size );
extern oC_ErrorCode_t  oC_MemMan_ReadAllocationsStats        ( Allocator_t Allocator , oC_MemMan_AllocationStats_t * outAllocationsArray , oC_UInt_t * Size , bool JoinSimilar );
extern bool            oC_MemMan_FindOverflowedAllocation    ( Allocator_t Allocator , oC_MemMan_AllocationStats_t * outAllocationStat );
extern oC_ErrorCode_t  oC_MemMan_ConfigureExternalHeapMap    ( void * StartAddress , oC_UInt_t Size );
extern oC_ErrorCode_t  oC_MemMan_UnconfigureExternalHeapMap  ( void );
extern float           oC_MemMan_GetMemoryExhaustedLimit     ( void );
extern float           oC_MemMan_GetPanicMemoryExhaustedLimit( void );
extern oC_ErrorCode_t  oC_MemMan_SetMemoryExhaustedLimit     ( float LimitPercent );
extern oC_ErrorCode_t  oC_MemMan_SetPanicMemoryExhaustedLimit( float LimitPercent );

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_MEMMAN_H_ */
