/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for programs
 *
 * @file       oc_program.h
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 19:40:34)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Program Program
 * @ingroup Kernel
 * @brief The module with interface for programs
 * 
 * The module contains the interface for programs. The programs are the basic units of the system. The programs are executed in the processes.
 * 
 * The programs can be created statically or dynamically by using the #oC_Program_New function. 
 * 
 * Example of usage:
 * @code{.c}
 * 
 * // The program
 * oC_Program_t program = oC_Program_New(oC_Program_Priority_UserSpaceProgram,"program",ProgramMainFunction,"stdin","stdout","stderr",1024);
 * 
 * // The process
 * oC_Process_t process = oC_Program_NewProcess(program,User);
 * 
 * // The program execution
 * oC_Thread_t mainThread;
 * void * context;
 * oC_ErrorCode_t errorCode = oC_Program_Exec(program,process,0,NULL,&mainThread,&context);
 * 
 * @endcode
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_PROGRAM_H_
#define INC_KERNEL_OC_PROGRAM_H_

#include <stdbool.h>
#include <oc_process.h>
#include <oc_program_types.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Program
//! @{

//==========================================================================================================================================
/**
 * @brief stores priority of the program
 */
//==========================================================================================================================================
typedef enum
{
    oC_Program_Priority_IdleProgram             = oC_Process_Priority_IdleProcess             , //!< Idle program
    oC_Program_Priority_UserSpaceProgram        = oC_Process_Priority_UserSpaceProcess        , //!< User space program
    oC_Program_Priority_UserSpaceDaemon         = oC_Process_Priority_UserSpaceDaemon         , //!< User space daemon
    oC_Program_Priority_CoreSpaceProgram        = oC_Process_Priority_CoreSpaceProcess        , //!< Core space program
    oC_Program_Priority_CoreSpaceDaemon         = oC_Process_Priority_CoreSpaceDaemon         , //!< Core space daemon
    oC_Program_Priority_SystemHelperProgram     = oC_Process_Priority_SystemHelperProcess     , //!< System helper program
    oC_Program_Priority_SystemHelperDeamon      = oC_Process_Priority_SystemHelperDeamon      , //!< System helper daemon
    oC_Program_Priority_SystemHandlerProgram    = oC_Process_Priority_SystemHandlerProcess    , //!< System handler program
    oC_Program_Priority_SystemHandlerDeamon     = oC_Process_Priority_SystemHandlerDeamon     , //!< System handler daemon
    oC_Program_Priority_SystemSecurityProgram   = oC_Process_Priority_SystemSecurityProcess   , //!< System security program
    oC_Program_Priority_SystemSecurityDeamon    = oC_Process_Priority_SystemSecurityDeamon      //!< System security daemon
} oC_Program_Priority_t;

//==========================================================================================================================================
/**
 * @brief stores registration of the program
 */
//==========================================================================================================================================
typedef const oC_Program_Registration_t * oC_Program_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Program
//! @{

extern oC_ErrorCode_t                       oC_Program_SetGotAddress    ( void* programContext, void * got );
extern oC_Process_t                         oC_Program_NewProcess       ( oC_Program_t Program , oC_User_t User );
extern oC_Program_t                         oC_Program_New              ( oC_Program_Priority_t Priority , const char * Name , oC_Program_MainFunction_t MainFunction , const char * StdInName , const char * StdOutName , const char * StdErrName , oC_UInt_t HeapMapSize );
extern bool                                 oC_Program_Delete           ( oC_Program_t Program );
extern bool                                 oC_Program_IsCorrect        ( oC_Program_t Program );
extern oC_ErrorCode_t                       oC_Program_Exec             ( oC_Program_t Program , oC_Process_t Process , int Argc , const char ** Argv , oC_Thread_t * outMainThread , void ** outContext );
extern oC_ErrorCode_t                       oC_Program_Exec2            ( oC_Program_t Program , oC_Process_t Process , int Argc , const char ** Argv , oC_Thread_t * outMainThread , void ** outContext, void* got );
extern const char *                         oC_Program_GetName          ( oC_Program_t Program );
extern const char *                         oC_Program_GetStdInName     ( oC_Program_t Program );
extern const char *                         oC_Program_GetStdOutName    ( oC_Program_t Program );
extern const char *                         oC_Program_GetStdErrName    ( oC_Program_t Program );
extern oC_Program_Priority_t                oC_Program_GetPriority      ( oC_Program_t Program );
extern oC_MemorySize_t                      oC_Program_GetHeapMapSize   ( oC_Program_t Program );
extern oC_ErrorCode_t                       oC_Program_WaitForFinish    ( void * Context , oC_Time_t CheckPeriod , oC_Time_t Timeout );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_PROGRAM_H_ */
