/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for signal module
 *
 * @file       oc_signal.h
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 18:28:24)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Signal Signal
 * @ingroup CoreThreadsSync
 * @brief The module for managing signals
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_SIGNAL_H_
#define INC_KERNEL_OC_SIGNAL_H_

#include <stdbool.h>
#include <oc_time.h>
#include <oc_memory.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Signal
//! @{

typedef struct Signal_t * oC_Signal_t;

typedef bool (*oC_Signal_Slot_t)( void * Context );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Signal
//! @{

extern oC_Signal_t      oC_Signal_New           ( void );
extern bool             oC_Signal_Delete        ( oC_Signal_t * Signal );
extern bool             oC_Signal_IsCorrect     ( oC_Signal_t Signal );
extern bool             oC_Signal_Connect       ( oC_Signal_t Signal , oC_Signal_Slot_t Slot , void * Context , oC_MemorySize_t StackSize );
extern bool             oC_Signal_Disconnect    ( oC_Signal_t Signal , oC_Signal_Slot_t Slot );
extern bool             oC_Signal_Emit          ( oC_Signal_t Signal , bool * outTaskWoken );
extern bool             oC_Signal_WaitFor       ( oC_Signal_t Signal , oC_Time_t Timeout );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_SIGNAL_H_ */
