/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for process manager
 *
 * @file       oc_processman.h
 *
 * @author     Patryk Kubiak - (Created on: 21 05 2015 18:29:56)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup   ProcessMan Process Manager
 * @ingroup    Kernel
 * @brief      The group of functions for process manager
 * 
 * The Process Manager is responsible for managing processes in the system. It allows for creating, deleting, and managing processes.
 * 
 * Every process in the system should be registered in the Process Manager. 
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_PROCESSMAN_H_
#define INC_KERNEL_OC_PROCESSMAN_H_

#include <stdbool.h>
#include <oc_errors.h>
#include <oc_list.h>
#include <oc_process.h>
#include <oc_memman.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________



#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @addtogroup ProcessMan
//! @{




#undef  _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup ProcessMan
//! @{

extern oC_ErrorCode_t           oC_ProcessMan_TurnOn                ( void );
extern oC_ErrorCode_t           oC_ProcessMan_TurnOff               ( void );
extern oC_ErrorCode_t           oC_ProcessMan_AddProcess            ( oC_Process_t Process );
extern oC_ErrorCode_t           oC_ProcessMan_RemoveProcess         ( oC_Process_t Process );
extern bool                     oC_ProcessMan_ContainsProcess       ( oC_Process_t Process );
extern oC_ErrorCode_t           oC_ProcessMan_GetList               ( oC_List(oC_Process_t) List );
extern oC_ErrorCode_t           oC_ProcessMan_GetUserProcesses      ( oC_User_t User , oC_List(oC_Process_t) List );
extern oC_Process_t             oC_ProcessMan_GetCurrentProcess     ( void );
extern oC_Process_t             oC_ProcessMan_GetProcess            ( const char * Name );
extern oC_Process_t             oC_ProcessMan_GetProcessById        ( oC_UInt_t PID );
extern Allocator_t              oC_ProcessMan_GetCurrentAllocator   ( void );
extern oC_Process_t             oC_ProcessMan_GetProcessOfThread    ( oC_Thread_t Thread );
extern void *                   oC_ProcessMan_RawAllocate           ( oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags );
extern bool                     oC_ProcessMan_RawFree               ( void * Address , oC_UInt_t Size );
extern void *                   oC_ProcessMan_Allocate              ( oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags );
extern bool                     oC_ProcessMan_Free                  ( void * Address , AllocationFlags_t Flags );
extern void *                   oC_ProcessMan_SmartAllocate         ( oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags );
extern bool                     oC_ProcessMan_SmartFree             ( void * Address , oC_UInt_t Size , AllocationFlags_t Flags );
extern void *                   oC_ProcessMan_KernelSmartAllocate   ( oC_UInt_t Size , Allocator_t Allocator, const char * Function, uint32_t LineNumber , AllocationFlags_t Flags );
extern bool                     oC_ProcessMan_KernelSmartFree       ( void * Address , oC_UInt_t Size , AllocationFlags_t Flags );
extern void                     oC_ProcessMan_DeleteDeamon          ( void * Parameter );
extern oC_ErrorCode_t           oC_ProcessMan_RunDeleteDaemon       ( void );
extern void                     oC_ProcessMan_ActivateDeleteDeamon  ( void );
extern oC_UInt_t                oC_ProcessMan_GetNextPid            ( void );
extern void                     oC_ProcessMan_KillAllZombies        ( void );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_PROCESSMAN_H_ */
