/** ****************************************************************************************************************************************
 *
 * @brief      interface for handling services
 * 
 * @file       oc_service.h
 *
 * @author     Patryk Kubiak - (Created on: 18.01.2017 19:09:18) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Service Service
 * @ingroup Kernel
 * @brief The module for handling services
 * 
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_INC_KERNEL_OC_SERVICE_H_
#define SYSTEM_CORE_INC_KERNEL_OC_SERVICE_H_

#include <oc_errors.h>
#include <oc_thread.h>
#include <oc_process.h>
#include <oc_module.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct Context_t * oC_Service_Context_t;

typedef oC_ErrorCode_t (*oC_Service_StartFunction_t)( oC_Service_Context_t * outContext );
typedef oC_ErrorCode_t (*oC_Service_StopFunction_t) ( oC_Service_Context_t *    Context );

typedef oC_ErrorCode_t (*oC_Service_MainFunction_t)( oC_Service_Context_t Context );

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    const char *                        Name;
    const char *                        StdoutStreamName;
    oC_Service_StartFunction_t          StartFunction;
    oC_Service_StopFunction_t           StopFunction;
    oC_Service_MainFunction_t           MainFunction;
    oC_MemorySize_t                     MainThreadStackSize;
    oC_MemorySize_t                     HeapMapSize;
    oC_MemorySize_t                     AllocationLimit;
    oC_Module_RequiredArray_t           RequiredModules;
    oC_Process_Priority_t               Priority;
} oC_Service_Registration_t;

//==========================================================================================================================================
/**
 * @brief object for storing service data
 *
 * The type is for storing service object data.
 */
//==========================================================================================================================================
typedef struct Service_t * oC_Service_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_Service_t   oC_Service_New            ( const oC_Service_Registration_t * Registration );
extern bool           oC_Service_Delete         ( oC_Service_t * Service );
extern bool           oC_Service_IsCorrect      ( oC_Service_t Service );
extern oC_ErrorCode_t oC_Service_Start          ( oC_Service_t Service );
extern oC_ErrorCode_t oC_Service_Stop           ( oC_Service_t Service );
extern bool           oC_Service_IsActive       ( oC_Service_t Service );
extern const char *   oC_Service_GetName        ( oC_Service_t Service );
extern oC_Process_t   oC_Service_GetProcess     ( oC_Service_t Service );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

#endif /* SYSTEM_CORE_INC_KERNEL_OC_SERVICE_H_ */
