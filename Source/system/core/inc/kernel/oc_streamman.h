/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for stream manager.
 *
 * @file       oc_streamman.h
 *
 * @author     Patryk Kubiak - (Created on: 28 05 2015 18:36:27)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup StreamMan Stream Manager
 * @ingroup Kernel
 * @brief The module for managing streams
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_STREAMMAN_H_
#define INC_KERNEL_OC_STREAMMAN_H_

#include <oc_errors.h>
#include <oc_stream.h>
#include <oc_list.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup StreamMan
//! @{



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup StreamMan
//! @{

extern oC_ErrorCode_t       oC_StreamMan_TurnOn             ( void );
extern oC_ErrorCode_t       oC_StreamMan_TurnOff            ( void );
extern oC_ErrorCode_t       oC_StreamMan_Add                ( oC_Stream_t Stream );
extern oC_ErrorCode_t       oC_StreamMan_Remove             ( oC_Stream_t Stream );
extern oC_List(oC_Stream_t) oC_StreamMan_GetList            ( void );
extern oC_Stream_t          oC_StreamMan_GetStream          ( const char * Name );
extern oC_Stream_t          oC_StreamMan_GetStdErrorStream  ( void );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_STREAMMAN_H_ */
