/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for Queue module
 *
 * @file       oc_queue.h
 *
 * @author     Patryk Kubiak - (Created on: 20 maj 2015 18:44:39) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Queue Queue
 * @ingroup CoreThreadsSync
 * @brief The module for managing queues
 * 
 * The module provides the interface for managing queues. The queue is a data structure that allows adding and removing elements in a FIFO (First In First Out) order.
 * 
 * @code{.c}
   
#include <oc_queue.h>
#include <oc_thread.h>
#include <oc_tgui.h>
#include <oc_system.h>
#include <stdio.h>

void Producer( void * Arg )
{
    oC_Queue_t queue = (oC_Queue_t)Arg;
    char buffer[100];
    while(1)
    {
        if(oC_Queue_Put(queue, "Hello World!", 12, oC_s(1)))
        {
            printf("Data put to the queue\n");
        }
        else
        {
            printf("Data not put to the queue\n");
        }
        sleep(oC_ms(200));
    }
}

void Consumer( void * Arg )
{
    oC_Queue_t queue = (oC_Queue_t)Arg;
    char buffer[100];
    while(1)
    {
        if(oC_Queue_Get(queue, buffer, 12, oC_s(1)))
        {
            printf("Data got from the queue\n");
        }
        else
        {
            printf("Data not got from the queue\n");
        }
    }
}

int main( int Argc , char ** Argv )
{
    oC_Queue_t queue = oC_Queue_New( getcurallocator() , 100 );
    if(queue)
    {
        oC_Thread_t producer = oC_Thread_New( 0, 0, NULL, "Producer", Producer, queue );
        oC_Thread_t consumer = oC_Thread_New( 0, 0, NULL, "Consumer", Consumer, queue );
        if(producer && consumer)
        {
            oC_Thread_Run(producer);
            oC_Thread_Run(consumer);
            oC_TGUI_WaitForKeyPress(NULL); // Wait for any key press
        }
        oC_Queue_Delete(&queue);
    }
    return 0;
}
    

   @endcode
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_QUEUE_H_
#define INC_KERNEL_OC_QUEUE_H_

#include <stdbool.h>
#include <stdint.h>
#include <oc_time.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Queue
//! @{

//==========================================================================================================================================
/**
 * @brief Stores the queue context
 * 
 * @param Type The  type of queue
 */
//==========================================================================================================================================
#define oC_Queue(Type)   Type*

//==========================================================================================================================================
/**
 * @brief Stores the queue type
 * 
 * The enumeration is for saving the type of queue
 */
//==========================================================================================================================================
typedef void * oC_Queue_t;


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION_________________________________________________________________________
//! @addtogroup Queue
//! @{

extern oC_Queue_t oC_Queue_New          ( Allocator_t Allocator , oC_MemorySize_t BufferSize );
extern bool       oC_Queue_Delete       ( oC_Queue_t * Queue );
extern bool       oC_Queue_IsCorrect    ( oC_Queue_t Queue );
extern bool       oC_Queue_Put          ( oC_Queue_t Queue , const void *    Data , uint32_t Size , oC_Time_t Timeout );
extern bool       oC_Queue_Get          ( oC_Queue_t Queue ,       void * outData , uint32_t Size , oC_Time_t Timeout );

#undef  _________________________________________FUNCTIONS_SECTION_________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_QUEUE_H_ */
