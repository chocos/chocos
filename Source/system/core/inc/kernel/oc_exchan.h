/** ****************************************************************************************************************************************
 *
 * @brief      File with interface of Exception Handler
 *
 * @file       oc_exchan.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ExcHan Exception Handler
 * @ingroup Kernel
 * @brief Module that handles system exceptions
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_KERNEL_OC_EXCHAN_H_
#define SYSTEM_CORE_INC_KERNEL_OC_EXCHAN_H_

#include <oc_errors.h>
#include <oc_thread.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup ExcHan
//! @{

//==========================================================================================================================================
/**
 * @brief Stores the exception handler type
 */
//==========================================================================================================================================
typedef enum
{
    oC_ExcHan_ExceptionType_KernelPanic             = (1<<0),       //!< Kernel panic
    oC_ExcHan_ExceptionType_HardFault               = (1<<1),       //!< Hard fault
    oC_ExcHan_ExceptionType_MemoryAccess            = (1<<2),       //!< Memory access error
    oC_ExcHan_ExceptionType_MemoryAllocationError   = (1<<3),       //!< Memory allocation error
    oC_ExcHan_ExceptionType_ProcessDamaged          = (1<<4),       //!< Process is damaged
    oC_ExcHan_ExceptionType_RequiredReboot          = (1<<8),       //!< Required reboot
} oC_ExcHan_ExceptionType_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t       oC_ExcHan_TurnOn            ( void );
extern oC_ErrorCode_t       oC_ExcHan_TurnOff           ( void );
extern void                 oC_ExcHan_PrintLoggedEvents ( void );
extern void                 oC_ExcHan_LogEvent          ( const char * Name , char * AdditionalInfo , void * Stack , oC_Thread_t Thread , oC_ExcHan_ExceptionType_t Type );
extern oC_ErrorCode_t       oC_ExcHan_RunDaemon         ( void );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* SYSTEM_CORE_INC_KERNEL_OC_EXCHAN_H_ */
