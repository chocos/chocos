/** ****************************************************************************************************************************************
 *
 * @brief      Manages modules in the system
 * 
 * @file       oc_moduleman.h
 *
 * @author     Patryk Kubiak - (Created on: 18.01.2017 21:35:40) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_KERNEL_OC_MODULEMAN_H_
#define SYSTEM_CORE_INC_KERNEL_OC_MODULEMAN_H_

#include <oc_module.h>

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern void                             oC_ModuleMan_TurnOnAllPossible    ( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) );
extern void                             oC_ModuleMan_TurnOffAllPossible   ( void );
extern oC_ErrorCode_t                   oC_ModuleMan_TurnOnModule         ( const char * Name );
extern oC_ErrorCode_t                   oC_ModuleMan_TurnOffModule        ( const char * Name );
extern const oC_Module_Registration_t * oC_ModuleMan_GetModuleRegistration( const char * Name );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

#endif /* SYSTEM_CORE_INC_KERNEL_OC_MODULEMAN_H_ */
