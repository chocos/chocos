/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for Thread Manager
 *
 * @file       oc_threadman.h
 *
 * @author     Patryk Kubiak - (Created on: 20 05 2015 19:58:27)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ThreadMan Thread Manager
 * @ingroup Kernel
 * @brief The module for managing threads
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_THREADMAN_H_
#define INC_KERNEL_OC_THREADMAN_H_

#include <oc_errors.h>
#include <oc_thread.h>
#include <oc_list.h>
#include <oc_memory.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup ThreadMan
//! @{

typedef enum
{
    oC_AutoStackMethod_Disabled ,
    oC_AutoStackMethod_RerunThread ,
} oC_AutoStackMethod_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup ThreadMan
//! @{

extern oC_ErrorCode_t           oC_ThreadMan_TurnOn                     ( void );
extern oC_ErrorCode_t           oC_ThreadMan_TurnOff                    ( void );
extern oC_ErrorCode_t           oC_ThreadMan_SetSystemTimerFrequency    ( oC_Frequency_t Frequency );
extern oC_Frequency_t           oC_ThreadMan_GetSystemTimerFrequency    ( void );
extern oC_ErrorCode_t           oC_ThreadMan_AddThread                  ( oC_Thread_t Thread );
extern oC_ErrorCode_t           oC_ThreadMan_RemoveThread               ( oC_Thread_t Thread );
extern bool                     oC_ThreadMan_ContainsThread             ( oC_Thread_t Thread );
extern oC_Thread_t              oC_ThreadMan_GetCurrentThread           ( void );
extern void                     oC_ThreadMan_UnblockAllBlockedBy        ( uint32_t * BlockingFlag , bool OnlyIfUnblocked );
extern void                     oC_ThreadMan_SwitchThread               ( void );
extern oC_List(oC_Thread_t)     oC_ThreadMan_GetList                    ( void );
extern oC_Thread_t              oC_ThreadMan_GetThread                  ( const char * Name );
extern oC_MemorySize_t          oC_ThreadMan_GetDefaultStackSize        ( void );
extern oC_ErrorCode_t           oC_ThreadMan_SetDefaultStackSize        ( oC_MemorySize_t Size );
extern oC_ErrorCode_t           oC_ThreadMan_SetAutoStackMethod         ( oC_AutoStackMethod_t Type );
extern oC_AutoStackMethod_t     oC_ThreadMan_GetAutoStackMethod         ( void );
extern oC_ErrorCode_t           oC_ThreadMan_SetRedZoneSize             ( oC_MemorySize_t Size );
extern oC_MemorySize_t          oC_ThreadMan_GetRedZoneSize             ( void );
extern oC_ErrorCode_t           oC_ThreadMan_SetStackIncreaseStep       ( oC_MemorySize_t Size );
extern oC_MemorySize_t          oC_ThreadMan_GetStackIncreaseStep       ( void );
extern double                   oC_ThreadMan_GetCpuLoad                 ( void );
extern double                   oC_ThreadMan_GetCurrentCpuLoad          ( void );
extern oC_Thread_t              oC_ThreadMan_GetThreadOfContext         ( void * Context );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_THREADMAN_H_ */
