/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for mutex managing
 *
 * @file       oc_mutex.h
 *
 * @author     Patryk Kubiak - (Created on: 18 05 2015 19:39:29) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup Mutex Mutex
 * @ingroup CoreThreadsSync
 * 
 * @brief The module for managing mutexes
 * 
 * The module provides the interface for managing mutexes. The mutex is a synchronization object that allows multiple threads to access shared data
 * in a controlled manner. The mutex is used to protect the shared data from being accessed by more than one thread at a time. The module provides
 * the interface for creating, deleting, taking, and giving the mutex. The mutex can be recursive or non-recursive. The recursive mutex allows the
 * same thread to take the mutex multiple times without blocking. The non-recursive mutex blocks the thread that tries to take the mutex that is already
 * taken by the same thread. The module provides the interface for checking if the mutex is taken by the current thread.
 * 
 * Example:
 * @code{.c}
 * 
 * #include <oc_mutex.h>
 * 
 * int main( int Argc , char ** Argv )
 * {
 *    // Create the mutex
 *    oC_Mutex_t Mutex = oC_Mutex_New(oC_Mutex_Type_Default, getcurallocator(), AllocationFlags_Default);
 *    if(Mutex != NULL)
 *    {
 *      // Take the mutex
 *      if( oC_Mutex_Take(Mutex, oC_s(1)) )
 *      {
 *         // The mutex is taken
 *         oC_Mutex_Give(Mutex);
 *      }
 * 
 *      // Delete the mutex
 *      oC_Mutex_Delete(&Mutex, AllocationFlags_Default);
 *    }
 * 
 *    return 0;
 * }
 * @endcode
 *
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_MUTEX_H_
#define INC_KERNEL_OC_MUTEX_H_

#include <stdbool.h>
#include <oc_time.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with interface types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Mutex
//! @{

//==========================================================================================================================================
/**
 * @brief Stores the mutex context
 */
//==========================================================================================================================================
typedef struct Mutex_t * oC_Mutex_t;

//==========================================================================================================================================
/**
 * @brief Stores the mutex type
 * 
 * The enumeration is for saving the type of mutex
 */
//==========================================================================================================================================
typedef enum
{
    oC_Mutex_Type_NonRecursive,                         //!< Non recursive mutex
    oC_Mutex_Type_Recursive,                            //!< Recursive mutex
    oC_Mutex_Type_Normal = oC_Mutex_Type_NonRecursive,  //!< Normal mutex @deprecated Use #oC_Mutex_Type_NonRecursive instead
    oC_Mutex_Type_Default = oC_Mutex_Type_Recursive,    //!< Default mutex type
} oC_Mutex_Type_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Mutex
//! @{

extern oC_Mutex_t           oC_Mutex_New                    ( oC_Mutex_Type_t Type , Allocator_t Allocator , AllocationFlags_t AllocationFlags );
extern bool                 oC_Mutex_Delete                 ( oC_Mutex_t * Mutex , AllocationFlags_t AllocationFlags );
extern bool                 oC_Mutex_Give                   ( oC_Mutex_t Mutex );
extern bool                 oC_Mutex_Take                   ( oC_Mutex_t Mutex , oC_Time_t Timeout );
extern bool                 oC_Mutex_IsTakenByCurrentThread ( oC_Mutex_t Mutex );
extern bool                 oC_Mutex_IsCorrect              ( oC_Mutex_t Mutex );
extern const char*          oC_Mutex_GetBlockedName         ( oC_Mutex_t Mutex );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_MUTEX_H_ */
