/** ****************************************************************************************************************************************
 *
 * @file       oc_stream.h
 *
 * @brief      The file with interface for stream
 *
 * @author     Patryk Kubiak - (Created on: 28 05 2015 18:30:21)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Stream Stream
 * @ingroup Kernel
 * @brief The module for handling streams
 * 
 ******************************************************************************************************************************************/


#ifndef INC_KERNEL_OC_STREAM_H_
#define INC_KERNEL_OC_STREAM_H_

#include <stdbool.h>
#include <oc_errors.h>
#include <oc_driver.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Stream
//! @{

typedef enum
{
    oC_Stream_Type_Input    = (1<<0),
    oC_Stream_Type_Output   = (1<<1)
} oC_Stream_Type_t;

typedef struct Stream_t * oC_Stream_t;

typedef struct
{
    oC_Stream_Type_t        Type;
    const char *            Name;
    oC_Driver_t             Driver;
    const void *            Config;
} oC_Stream_Cfg_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Stream
//! @{

extern oC_Stream_t      oC_Stream_New               ( Allocator_t Allocator , AllocationFlags_t Flags , oC_Stream_Type_t Type , const char * Name , oC_Driver_t Driver , const void * Config );
extern bool             oC_Stream_Delete            ( oC_Stream_t * Stream );
extern bool             oC_Stream_IsCorrect         ( oC_Stream_t Stream );
extern bool             oC_Stream_IsType            ( oC_Stream_t Stream , oC_Stream_Type_t Type );
extern oC_ErrorCode_t   oC_Stream_Write             ( oC_Stream_t Stream , const char * Buffer    , oC_MemorySize_t * Size , oC_IoFlags_t IoFlags );
extern oC_ErrorCode_t   oC_Stream_Read              ( oC_Stream_t Stream , char * outBuffer       , oC_MemorySize_t * Size , oC_IoFlags_t IoFlags );
extern oC_ErrorCode_t   oC_Stream_ClearReadBuffer   ( oC_Stream_t Stream );
extern const char *     oC_Stream_GetName           ( oC_Stream_t Stream );
extern oC_Driver_t      oC_Stream_GetDriver         ( oC_Stream_t Stream );
extern void*            oC_Stream_GetDriverContext  ( oC_Stream_t Stream );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_KERNEL_OC_STREAM_H_ */
