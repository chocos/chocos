/** ****************************************************************************************************************************************
 *
 * @brief      Interface of the CBIN module
 * 
 * @file       oc_cbin.h
 *
 * @author     Patryk Kubiak - (Created on: 08.07.2017 18:34:17) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup CBIN CBIN - The CBIN file format
 * @ingroup CoreFileSystem
 * @brief The module for handling CBIN file format
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_FS_OC_CBIN_H_
#define SYSTEM_CORE_INC_FS_OC_CBIN_H_

#include <oc_program.h>
#include <oc_compiler.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief      Stores information about the program sections
 */
//==========================================================================================================================================
typedef struct PACKED
{
    struct
    {
        void *           Address;                       /**< The address of the section */
        oC_MemorySize_t  Size;                          /**< The size of the section */
    } text , data , bss , syscalls, sections, got;
} oC_CBin_Sections_t;

//==========================================================================================================================================
/**
 * @brief stores information about CBIN file
 */
//========================================================================================================================================== 
typedef struct
{
    void *                  FileBuffer;         /**< The buffer with the file data */
    oC_MemorySize_t         FileSize;           /**< The size of the file */
    oC_Program_Header_t *   ProgramHeader;      /**< The pointer to the program header */
    oC_CBin_Sections_t      Sections;           /**< The structure with the sections information */
} oC_CBin_File_t;

//==========================================================================================================================================
/**
 * @brief      The structure with the context of the CBIN program
 */
//==========================================================================================================================================
typedef struct
{
    oC_Process_t                Process;            /**< The process of the program */
    oC_Process_Priority_t       Priority;           /**< The priority of the process */
    oC_Stream_t                 StreamIn;           /**< The stream for the input */
    oC_Stream_t                 StreamOut;          /**< The stream for the output */
    oC_Stream_t                 StreamErr;          /**< The stream for the errors */
    int                         Argc;               /**< The number of the arguments */
    char **                     Argv;               /**< The array with the arguments */
    oC_Program_Registration_t * Program;            /**< The pointer to the program registration */
    void *                      ProgramContext;     /**< The pointer to the program context */

    oC_CBin_Sections_t          Sections;           /**< The structure with the sections information */
    void *                      SectionsBuffer;     /**< The buffer with the sections data */
    oC_Thread_t                 MainThread;         /**< The main thread of the program */
} oC_CBin_ProgramContext_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

extern oC_ErrorCode_t   oC_CBin_LoadFile            ( const char * Path , oC_CBin_File_t * outFile );
extern oC_ErrorCode_t   oC_CBin_UnloadFile          ( oC_CBin_File_t * File );
extern oC_ErrorCode_t   oC_CBin_PrepareForExecution ( oC_CBin_File_t * File , oC_CBin_ProgramContext_t * ProgramContext );
extern oC_ErrorCode_t   oC_CBin_Execute             ( oC_CBin_ProgramContext_t * ProgramContext );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________



#endif /* SYSTEM_CORE_INC_FS_OC_CBIN_H_ */
