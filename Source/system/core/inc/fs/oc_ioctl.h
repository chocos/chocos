/** ****************************************************************************************************************************************
 *
 * @file       oc_ioctl.h
 *
 * @brief      The file with interface for IOCTL
 *
 * @author     Patryk Kubiak - (Created on: 27 05 2015 22:02:37)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Ioctl Ioctl
 * @ingroup CoreFileSystem
 * @brief The module for handling IOCTL
 * 
 ******************************************************************************************************************************************/


#ifndef INC_DRIVERS_OC_IOCTL_H_
#define INC_DRIVERS_OC_IOCTL_H_

#include <stdint.h>
#include <oc_null.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Ioctl
//! @{

#define oC_IOCTL_SPECIAL_COMMAND_ID                 0xF2
#define oC_IOCTL_NORMAL_COMMAND_ID                  0xA2


#define _oC_Ioctl_MakeCommand(GROUP,INDEX,SIZE)         (((GROUP) << 16) | ((SIZE & 0xFF) << 8) | ((INDEX) << 0))
#define oC_Ioctl_MakeCommand(GROUP,INDEX,TYPE)          ((oC_IOCTL_NORMAL_COMMAND_ID  << 24) | _oC_Ioctl_MakeCommand(GROUP,INDEX,sizeof(TYPE)) )
#define oC_Ioctl_MakeSpecialCommand(GROUP,INDEX,TYPE)   ((oC_IOCTL_SPECIAL_COMMAND_ID << 24) | _oC_Ioctl_MakeCommand(GROUP,INDEX,sizeof(TYPE)) )

#define oC_Ioctl_IsNormalCommand(COMMAND)               (((COMMAND) >> 24) == oC_IOCTL_NORMAL_COMMAND_ID)
#define oC_Ioctl_IsSpecialCommand(COMMAND)              (((COMMAND) >> 24) == oC_IOCTL_SPECIAL_COMMAND_ID)
#define oC_Ioctl_IsCorrectCommand(COMMAND)              oC_Ioctl_IsNormalCommand(COMMAND) || oC_Ioctl_IsSpecialCommand(COMMAND)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup Ioctl
//! @{

typedef uint32_t oC_Ioctl_Command_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with list of special commands
 *
 *  ======================================================================================================================================*/
#define _________________________________________SPECIAL_COMMANDS_SECTION___________________________________________________________________

#define oC_IOCTL_COMMAND_GET_DRIVER_VERSION             oC_Ioctl_MakeSpecialCommand(0x00,uint32_t)

typedef enum
{
    oC_Ioctl_SpecialGroup_Driver ,
    oC_Ioctl_SpecialGroup_DevFs ,
    oC_Ioctl_SpecialGroup_FileSystem ,
} oC_Ioctl_SpecialGroup_t;

typedef enum
{
    oC_Ioctl_Group_Configure ,
    oC_Ioctl_Group_GPIO ,
    oC_Ioctl_Group_DriverSpecific ,
} oC_Ioctl_Group_t;

typedef enum
{
    oC_IoCtl_SpecialCommand_GetDriverVersion = oC_Ioctl_MakeSpecialCommand(oC_Ioctl_SpecialGroup_Driver     , 0x01 , uint32_t) ,

    oC_IoCtl_SpecialCommand_SetDriverContext = oC_Ioctl_MakeSpecialCommand(oC_Ioctl_SpecialGroup_DevFs      , 0x01 , uint32_t) ,

    oC_IoCtl_SpecialCommand_SetFileOffset    = oC_Ioctl_MakeSpecialCommand(oC_Ioctl_SpecialGroup_FileSystem , 0x01 , uint32_t) ,
    oC_IoCtl_SpecialCommand_ClearRxFifo      = oC_Ioctl_MakeSpecialCommand(oC_Ioctl_SpecialGroup_FileSystem , 0x02 , uint32_t) ,
    oC_IoCtl_SpecialCommand_ClearTxFifo      = oC_Ioctl_MakeSpecialCommand(oC_Ioctl_SpecialGroup_FileSystem , 0x03 , uint32_t) ,
} oC_IoCtl_SpecialCommand_t;

typedef enum
{
    oC_Ioctl_NormalCommand_Enable             = oC_Ioctl_MakeCommand(oC_Ioctl_Group_Configure , 0x01 , NULL ) ,
    oC_Ioctl_NormalCommand_Disable            = oC_Ioctl_MakeCommand(oC_Ioctl_Group_Configure , 0x02 , NULL ) ,
} oC_Ioctl_NormalCommand_t;

#undef  _________________________________________SPECIAL_COMMANDS_SECTION___________________________________________________________________


#endif /* INC_DRIVERS_OC_IOCTL_H_ */
