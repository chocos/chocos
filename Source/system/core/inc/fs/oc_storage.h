/** ****************************************************************************************************************************************
 *
 * @brief      File with interface for a storage
 * 
 * @file       oc_storage.h
 *
 * @author     Patryk Kubiak - (Created on: 18.08.2017 19:15:02) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Storage Storage
 * @ingroup CoreFileSystem
 * @brief The module for handling storages
 * 
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_INC_FS_OC_STORAGE_H_
#define SYSTEM_CORE_INC_FS_OC_STORAGE_H_

#include <oc_stdtypes.h>
#include <oc_errors.h>
#include <oc_memory.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS________________________________________________________________________________

#define oC_STORAGE_MBR_SIZE     512

#undef  _________________________________________DEFINITIONS________________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_ErrorCode_t (*ReadSectors) ( void * Context, oC_SectorNumber_t SectorNumber, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
    oC_ErrorCode_t (*WriteSectors)( void * Context, oC_SectorNumber_t SectorNumber, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
    oC_ErrorCode_t (*EraseSectors)( void * Context, oC_SectorNumber_t SectorNumber, oC_SectorNumber_t NumberOfSectors, oC_Time_t Timeout );
} oC_Storage_DiskInterface_t;

typedef struct Storage_t * oC_Storage_t;
typedef uint8_t oC_Storage_MasterBootRecord_t[oC_STORAGE_MBR_SIZE];

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_Storage_t         oC_Storage_New                  ( const oC_Storage_DiskInterface_t * Interface, void * Context, const char * Name, oC_SectorNumber_t StartSector, oC_MemorySize_t SectorSize , oC_SectorNumber_t NumberOfSectors );
extern bool                 oC_Storage_Delete               ( oC_Storage_t * Storage );
extern bool                 oC_Storage_IsCorrect            ( oC_Storage_t Storage );
extern oC_MemorySize_t      oC_Storage_GetSectorSize        ( oC_Storage_t Storage );
extern oC_SectorNumber_t    oC_Storage_GetSectorCount       ( oC_Storage_t Storage );
extern oC_MemorySize_t      oC_Storage_GetSize              ( oC_Storage_t Storage );
extern const char *         oC_Storage_GetName              ( oC_Storage_t Storage );
extern oC_ErrorCode_t       oC_Storage_SetName              ( oC_Storage_t Storage, const char * Name );
extern oC_Timestamp_t       oC_Storage_GetTimestamp         ( oC_Storage_t Storage );
extern oC_ErrorCode_t       oC_Storage_ReadMasterBootRecord ( oC_Storage_t Storage, oC_Storage_MasterBootRecord_t outMBR, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Storage_WriteMasterBootRecord( oC_Storage_t Storage, oC_Storage_MasterBootRecord_t MBR, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Storage_Read                 ( oC_Storage_t Storage, oC_MemoryOffset_t Offset, void * outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Storage_Write                ( oC_Storage_t Storage, oC_MemoryOffset_t Offset, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Storage_ReadSectors          ( oC_Storage_t Storage, oC_SectorNumber_t StartSector, void * outBuffer, oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Storage_WriteSectors         ( oC_Storage_t Storage, oC_SectorNumber_t StartSector, const void * Buffer, oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Storage_EraseSectors         ( oC_Storage_t Storage, oC_SectorNumber_t StartSector, oC_SectorNumber_t SectorCount, oC_Time_t Timeout );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



#endif /* SYSTEM_CORE_INC_FS_OC_STORAGE_H_ */
