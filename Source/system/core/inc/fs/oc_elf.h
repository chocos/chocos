/** ****************************************************************************************************************************************
 *
 * @brief      Module for analyze of ELF files
 * 
 * @file          oc_elf.h
 *
 * @author     Patryk Kubiak - (Created on: 23.06.2017 18:10:28) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Elf Elf file analyzer
 * @ingroup CoreFileSystem
 * @brief Module for analyze of ELF files
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_KERNEL_OC_ELF_H_
#define SYSTEM_CORE_INC_KERNEL_OC_ELF_H_

#include <oc_stdtypes.h>
#include <oc_errors.h>
#include <oc_memory.h>
#include <oc_process.h>
#include <oc_stream.h>
#include <oc_program.h>
#include <oc_object.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define oC_ELF_ID_SIZE                              16
#define oC_ELF_SIGNATURE_SIZE                       4
#define oC_ELF_GET_SYMBOL_BIND(Info)                ((Info)>>4)
#define oC_ELF_GET_SYMBOL_TYPE(Info)                ((Info) & 0x0F)
#define oC_ELF_GET_RELOCATION_SYMBOL_INDEX(Info)    ((Info) >> 8)
#define oC_ELF_GET_RELOCATION_TYPE(Info)            ((Info) & 0x0F)

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________
//! @addtogroup Elf
//! @{


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Unsigned program address
 */
//==========================================================================================================================================
typedef uint32_t oC_Elf_Addr_t;

//==========================================================================================================================================
/**
 * @brief Unsigned medium integer
 */
//==========================================================================================================================================
typedef uint16_t oC_Elf_Half_t;

//==========================================================================================================================================
/**
 * @brief Unsigned file offset
 */
//==========================================================================================================================================
typedef uint32_t oC_Elf_Off_t;

//==========================================================================================================================================
/**
 * @brief Signed large integer
 */
//==========================================================================================================================================
typedef int32_t oC_Elf_Sword_t;

//==========================================================================================================================================
/**
 * @brief Unsigned large integer
 */
//==========================================================================================================================================
typedef uint32_t oC_Elf_Word_t;

//==========================================================================================================================================
/**
 * @brief type for elf header
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_Type_None                    = 0 ,     //!< No file type
    oC_Elf_Type_RelocatableFile         = 1 ,     //!< Relocatable File
    oC_Elf_Type_ExecutableFile          = 2 ,     //!< Executable File
    oC_Elf_Type_SharedObjectFile        = 3 ,     //!< Shared Object File
    oC_Elf_Type_CoreFile                = 4 ,     //!< Core File
    oC_Elf_Type_LoProc                  = 0xFF00 ,//!< Lo Proc
    oC_Elf_Type_HiProc                  = 0xFFFF ,//!< Hi Proc
} oC_Elf_Type_t;

//==========================================================================================================================================
/**
 * @brief machine type for ELF header
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_MachineType_NoMachine        = 0    ,   //!< No machine
    oC_Elf_MachineType_M32              = 1    ,   //!< AT&T WE 32100
    oC_Elf_MachineType_SPARC            = 2    ,   //!< SPARC
    oC_Elf_MachineType_386              = 3    ,   //!< Intel 80386
    oC_Elf_MachineType_68K              = 4    ,   //!< Motorola 68000
    oC_Elf_MachineType_88K              = 5    ,   //!< Motorola 88000
    oC_Elf_MachineType_860              = 7    ,   //!< Intel 80860
    oC_Elf_MachineType_MIPS             = 8    ,   //!< MIPS RS3000
    oC_Elf_MachineType_ArmCortexM7      = 0x28 ,   //!< ARM Cortex M7
} oC_Elf_MachineType_t;

//==========================================================================================================================================
/**
 * @brief stores ELF file class
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_FileClass_None   = 0 , //!< Invalid class
    oC_Elf_FileClass_32     = 1 , //!< 32-bits objects
    oC_Elf_FileClass_64     = 2 , //!< 64-bits objects
} oC_Elf_FileClass_t;

//==========================================================================================================================================
/**
 * @brief holds section type values
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_SectionType_NULL         =  0 ,               //!< Marks section as inactive
    oC_Elf_SectionType_PROGBITS     =  1 ,               //!< This section holds data defined by the program, whose format and meaning are determined by the program
    oC_Elf_SectionType_SYMTAB       =  2 ,               //!< Holds symbol table
    oC_Elf_SectionType_STRTAB       =  3 ,               //!< The section holds string table
    oC_Elf_SectionType_RELA         =  4 ,               //!< The section holds relocation table
    oC_Elf_SectionType_HASH         =  5 ,               //!< Symbol hash table
    oC_Elf_SectionType_DYNAMIC      =  6 ,               //!< Holds information for dynamic linking.
    oC_Elf_SectionType_NOTE         =  7 ,               //!< Holds the information that marks the file in some way
    oC_Elf_SectionType_NOBITS       =  8 ,               //!< The section of this type occupies no space in the file, but otherwise resembles PROGBITS.
    oC_Elf_SectionType_REL          =  9 ,               //!< Holds relocation entries without explicit addends
    oC_Elf_SectionType_SHLIB        = 10 ,               //!< The section is reserved, but has unspecified semantics.
    oC_Elf_SectionType_DYNSYM       = 11 ,               //!< Holds dynamic symbol table
    oC_Elf_SectionType_LOPROC       = 0x70000000 ,       //!< Values in this range are reserved for processor-specific semantics
    oC_Elf_SectionType_HIPROC       = 0x7fffffff ,       //!< Values in this range are reserved for processor-specific semantics
    oC_Elf_SectionType_LOUSER       = 0x80000000 ,       //!< The value specifies the lower bound of the range of indexes reserved for application programs
    oC_Elf_SectionType_HIUSER       = 0xffffffff ,       //!< The value specifies the upper bound of the range of indexes reserved for application programs
} oC_Elf_SectionType_t;

//==========================================================================================================================================
/**
 * @brief stores section header flags
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_SectionFlag_WRITE        = 0x1 ,              //!< The section contains data that should be writable during process execution
    oC_Elf_SectionFlag_ALLOC        = 0x2 ,              //!< Occupies memory during process execution. Some control sections do not reside in the memory image of an object file. This attribute is off for those sections
    oC_Elf_SectionFlag_EXECINSTR    = 0x4 ,              //!< The section contains executable machine insturctions
    oC_Elf_SectionFlag_MASKPROC     = 0xf0000000 ,       //!< bits reserved for processor specific semantics
} oC_Elf_SectionFlag_t;

//==========================================================================================================================================
/**
 * @brief stores symbol binding
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_SymbolBinding_LOCAL      = 0 ,               //!< Local symbols are not visible outside the object file containing their definition
    oC_Elf_SymbolBinding_GLOBAL     = 1 ,               //!< Global symbols are visible to all object files being combined
    oC_Elf_SymbolBinding_WEAK       = 2 ,               //!< Weak symbols resemble global symbols, but their definitions have lower precedence
    oC_Elf_SymbolBinding_LOPROC     = 13 ,              //!< Values in the range are reserved for processor specific semantics
    oC_Elf_SymbolBinding_HIPROC     = 15 ,              //!< Values in the range are reserved for processor specific semantics
} oC_Elf_SymbolBinding_t;

//==========================================================================================================================================
/**
 * @brief stores symbol type
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_SymbolType_NOTYPE        = 0 ,               //!< Type not specified
    oC_Elf_SymbolType_OBJECT        = 1 ,               //!< The symbol associated with a data object, such as variable, array, etc
    oC_Elf_SymbolType_FUNC          = 2 ,               //!< The symbol associated with a function or other executable code
    oC_Elf_SymbolType_SECTION       = 3 ,               //!< Associated with a section
    oC_Elf_SymbolType_FILE          = 4 ,               //!< Conventionally the symbol's name gives the name of the source file associated with the object file
    oC_Elf_SymbolType_LOPROC        = 13 ,              //!< Reserved for machine specific
    oC_Elf_SymbolType_HIPROC        = 15 ,              //!< Reserved for machine specific
} oC_Elf_SymbolType_t;

//==========================================================================================================================================
/**
 * @brief stores program type
 */
//==========================================================================================================================================
typedef enum
{
    oC_Elf_ProgramType_NULL         = 0 ,               //!< The entry is unused
    oC_Elf_ProgramType_LOAD         = 1 ,               //!< The array entry specifies a loadable segment, described by `FileSize` and `MemorySize`
    oC_Elf_ProgramType_DYNAMIC      = 2 ,               //!< The entry stores dynamic linking information
    oC_Elf_ProgramType_INTERP       = 3 ,               //!< The element specifies the location and size of a null-terminated path name to invoke as a interpreter
    oC_Elf_ProgramType_NOTE         = 4 ,               //!< The element stores location and size of auxiliary information
    oC_Elf_ProgramType_SHLIB        = 5 ,               //!< Segment is reserved
    oC_Elf_ProgramType_PHDR         = 6 ,               //!< The element, if present, specifies the location and size the program header table itself both - in the file and in the memory image of the program
} oC_Elf_ProgramType_t;

//==========================================================================================================================================
/**
 * @brief stores the header of the ELF file
 */
//==========================================================================================================================================
typedef struct
{
    union
    {
        struct
        {
            uint8_t Signature[oC_ELF_SIGNATURE_SIZE];       //!< Signature to recognize ELF file
            uint8_t FileClass;                              //!< Identifies file class, see #oC_Elf_FileClass_t for more
            uint8_t DataEncoding;                           //!< Specifies data encoding - LSB (1) or MSB (2)
            uint8_t ObjectFileVersion;                      //!< Again file version (?!)
        };
        uint8_t  ID[oC_ELF_ID_SIZE];                        //!< The initial bytes mark the file as an ELF object
    };
    oC_Elf_Half_t       Type;                               //!< Type of the ELF file, see #oC_Elf_Type_t for more information
    oC_Elf_Half_t       Machine;                            //!< Type of the target machine, see #oC_Elf_MachineType_t for more
    oC_Elf_Word_t       Version;                            //!< Object file version
    oC_Elf_Addr_t       Entry;                              //!< This member gives the virtual address to which the system first transfers control, thus starting the process. If the file has no associated entry point, this member holds zero.
    oC_Elf_Off_t        ProgramHeaderTableOffset;           //!< This member holds the program header table's file offset in bytes. If the file has no program header table, this member holds zero.
    oC_Elf_Off_t        SectionHeaderTableOffset;           //!< This member holds the section header table's file offset in bytes. If the file has no program header table, this member holds zero.
    oC_Elf_Word_t       Flags;                              //!< Processor specific flags
    oC_Elf_Half_t       HeaderSize;                         //!< ELF header's size in bytes
    oC_Elf_Half_t       ProgramHeaderTableEntrySize;        //!< Holds the size in bytes of one entry in program header table.
    oC_Elf_Half_t       ProgramHeaderTableNumberOfEntries;  //!< Number of entries in program header table
    oC_Elf_Half_t       SectionHeaderTableEntrySize;        //!< Holds the size in bytes of one entry in section header table
    oC_Elf_Half_t       SectionHeaderTableNumberOfEntries;  //!< Number of entries in section header table
    oC_Elf_Half_t       SectionHeaderNameStringTableIndex;  //!< Holds the section header table index of the entry associated with the section name string table
} oC_Elf_FileHeader_t;

//==========================================================================================================================================
/**
 * @brief holds header of the ELF sections
 */
//==========================================================================================================================================
typedef struct
{
    oC_Elf_Word_t       NameIndex;                          //!< Name of the section - its value is an index into the section header string table
    oC_Elf_Word_t       Type;                               //!< This member categorizes the section’s contents and semantics. See #oC_Elf_SectionType_t for more
    oC_Elf_Word_t       Flags;                              //!< Attributes flags. See #oC_Elf_SectionFlag_t for more
    oC_Elf_Addr_t       Address;                            //!< If the section appear in the memory image of a process, this member gives the address at which the section's first byte should reside
    oC_Elf_Off_t        Offset;                             //!< Byte offset from the beginning of the file to the first byte in the section
    oC_Elf_Word_t       Size;                               //!< The section size in bytes
    oC_Elf_Word_t       Link;                               //!< Section header table index link, interpretation depends on the section type
    oC_Elf_Word_t       Info;                               //!< Extra information that depends on the section type
    oC_Elf_Word_t       AddressAlign;                       //!< Alignment of the address of the section
    oC_Elf_Word_t       EntrySize;                          //!< Some sections holds table of symbols, and this is size of the entry in the table. If it is 0, the section does not hold a table of fixed size entries
} oC_Elf_SectionHeader_t;

//==========================================================================================================================================
/**
 * @brief holds symbol table entry
 */
//==========================================================================================================================================
typedef struct
{
    oC_Elf_Word_t       NameIndex;                          //!< Index of symbol name into the object file symbol string table
    oC_Elf_Addr_t       Value;                              //!< This member gives the value of the associated symbol
    oC_Elf_Word_t       Size;                               //!< Number of bytes contained in the object
    uint8_t             Info;                               //!< Symbol type and binding attributes
    uint8_t             Other;                              //!< Reserved for future use
    oC_Elf_Half_t       Index;                              //!< Every symbol table entry is defined in relation for some section. This member holds the relevant section header table index
} oC_Elf_SymbolTableEntry_t;

//==========================================================================================================================================
/**
 * @brief stores relocation entry (extended from RELA and not from REL)
 */
//==========================================================================================================================================
typedef struct
{
    oC_Elf_Addr_t       Offset;                             //!< Location at which to apply the relocation action.
    oC_Elf_Word_t       Info;                               //!< Symbol table index and type of the relocation
    oC_Elf_Sword_t      Addend;                             //!< Constant addend used to compute the value to be stored into the relocatable field
} oC_Elf_RelocationEntry_t;

//==========================================================================================================================================
/**
 * @brief stores program header entry
 */
//==========================================================================================================================================
typedef struct
{
    oC_Elf_Word_t       Type;                               //!< Type of the program header entry. See #oC_Elf_ProgramType_t for more
    oC_Elf_Off_t        Offset;                             //!< Offset in the file with the first byte of the segment
    oC_Elf_Addr_t       VirtualAddress;                     //!< This member gives the virtual address at which the first byte of the segment resides in memory
    oC_Elf_Addr_t       PhysicalAddress;                    //!< This member gives the physical address at which the first byte of the segment resides in memory
    oC_Elf_Word_t       FileSize;                           //!< Number of bytes in the file image of the segment. May be 0
    oC_Elf_Word_t       MemorySize;                         //!< Number of bytes in the memory image of the segment. May be 0
    oC_Elf_Word_t       Flags;                              //!< Flags for the segment.
    oC_Elf_Word_t       Align;                              //!< Alignment of the address of the program segment
} oC_Elf_ProgramHeader_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct File_t* oC_Elf_File_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct ProgramContext_t* oC_Elf_ProgramContext_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

extern oC_ErrorCode_t               oC_Elf_LoadFile                 ( const char * Path , oC_Elf_File_t * outFile );
extern oC_ErrorCode_t               oC_Elf_UnloadFile               ( oC_Elf_File_t * File );
extern void*                        oC_Elf_GetSectionData           ( oC_Elf_File_t File, oC_Elf_SectionHeader_t* SectionHeader );
extern void*                        oC_Elf_GetSectionEntry          ( oC_Elf_File_t File, oC_Elf_SectionHeader_t * SectionHeader, oC_Elf_Word_t EntryIndex );
extern const char*                  oC_Elf_GetString                ( oC_Elf_File_t File, oC_Elf_Word_t NameIndex );
extern oC_Elf_SymbolTableEntry_t*   oC_Elf_FindSymbol               ( oC_Elf_File_t File, const char* SymbolName );
extern void*                        oC_Elf_GetProgramEntryAddress   ( oC_Elf_File_t File );
extern oC_Elf_Addr_t                oC_Elf_GetProgramEntryOffset    ( oC_Elf_File_t File );
extern oC_ErrorCode_t               oC_Elf_LoadProgram              ( oC_Elf_File_t File );
extern oC_ErrorCode_t               oC_Elf_Execute                  ( oC_Elf_File_t File, int Argc, const char** Argv);
extern oC_ErrorCode_t               oC_Elf_Kill                     ( oC_Elf_File_t File );
extern oC_Process_t                 oC_Elf_GetProcess               ( oC_Elf_File_t File );
extern oC_Program_t                 oC_Elf_GetProgram               ( oC_Elf_File_t File );
extern void*                        oC_Elf_GetProgramContext        ( oC_Elf_File_t File );
extern oC_ErrorCode_t               oC_Elf_WaitForFinish            ( oC_Elf_File_t File, oC_Time_t CheckPeriod, oC_Time_t Timeout );
extern oC_Elf_ProgramHeader_t * oC_Elf_GetProgramHeader         ( oC_Elf_File_t File , oC_Elf_Word_t ProgramIndex );
extern oC_Elf_SectionHeader_t * oC_Elf_GetSectionHeader         ( oC_Elf_File_t File , oC_Elf_Word_t SectionIndex );
extern oC_Elf_SectionHeader_t * oC_Elf_GetSectionHeaderOfType   ( oC_Elf_File_t File , oC_Elf_SectionType_t Type );
extern oC_Elf_SectionHeader_t * oC_Elf_GetSectionHeaderByName   ( oC_Elf_File_t File , const char * SectionName );
extern const char *             oC_Elf_GetTypeString            ( oC_Elf_Half_t ElfType );
extern const char *             oC_Elf_GetMachineString         ( oC_Elf_Half_t MachineType );
extern const char *             oC_Elf_GetSectionTypeString     ( oC_Elf_SectionType_t SectionType );
extern char *                   oC_Elf_GetSectionFlagsString    ( oC_Elf_Word_t Flags, char * Buffer , oC_MemorySize_t BufferSize );
extern const char *             oC_Elf_GetSymbolBindingString   ( uint8_t SymbolBinding );
extern const char *             oC_Elf_GetSymbolTypeString      ( uint8_t SymbolType );
extern const char *             oC_Elf_GetProgramTypeString     ( uint8_t ProgramType );


#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________



#endif /* SYSTEM_CORE_INC_KERNEL_OC_ELF_H_ */
