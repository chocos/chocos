/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for Virtual File System
 *
 * @file       oc_vfs.h
 *
 * @author     Patryk Kubiak - (Created on: 2 06 2015 19:06:40)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup VFS VFS - Virtual File System
 * @ingroup CoreFileSystem
 * @brief The module for handling Virtual File System
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_USER_INC_OC_VFS_H_
#define SYSTEM_USER_INC_OC_VFS_H_

#include <oc_fs.h>
#include <oc_devfs.h>
#include <oc_ramfs.h>
#include <oc_flashfs.h>
#include <oc_process.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup VFS
//! @{



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup VFS
//! @{

extern oC_ErrorCode_t       oC_VirtualFileSystem_TurnOn        ( void );
extern oC_ErrorCode_t       oC_VirtualFileSystem_TurnOff       ( void );
extern oC_ErrorCode_t       oC_VirtualFileSystem_AddFileSystem      ( oC_FileSystem_t FileSystem );
extern oC_ErrorCode_t       oC_VirtualFileSystem_RemoveFileSystem   ( oC_FileSystem_t FileSystem );
extern oC_ErrorCode_t       oC_VirtualFileSystem_mount         ( const char * FileSystem , const char * DevicePath , const char * Path );
extern oC_ErrorCode_t       oC_VirtualFileSystem_umount        ( const char * Path );
extern oC_ErrorCode_t       oC_VirtualFileSystem_fopen         ( oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes );
extern oC_ErrorCode_t       oC_VirtualFileSystem_fclose             ( oC_File_t File );
extern oC_ErrorCode_t       oC_VirtualFileSystem_fcloseFromProcess  ( oC_Process_t Process );
extern oC_ErrorCode_t       oC_VirtualFileSystem_fread         ( oC_File_t File , void * outBuffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_VirtualFileSystem_fwrite        ( oC_File_t File , const void * Buffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_VirtualFileSystem_lseek         ( oC_File_t File , uint32_t Offset );
extern oC_ErrorCode_t       oC_VirtualFileSystem_ioctl         ( oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer);
extern oC_ErrorCode_t       oC_VirtualFileSystem_sync          ( oC_File_t File );
extern oC_ErrorCode_t       oC_VirtualFileSystem_getc          ( char * outCharacter , oC_File_t File );
extern oC_ErrorCode_t       oC_VirtualFileSystem_putc          ( char Character , oC_File_t File );
extern int32_t              oC_VirtualFileSystem_tell          ( oC_File_t File );
extern bool                 oC_VirtualFileSystem_eof           ( oC_File_t File );
extern uint32_t             oC_VirtualFileSystem_size          ( oC_File_t File );
extern oC_ErrorCode_t       oC_VirtualFileSystem_flush         ( oC_File_t File );
extern oC_ErrorCode_t       oC_VirtualFileSystem_error         ( oC_File_t File );

extern oC_ErrorCode_t       oC_VirtualFileSystem_opendir       ( oC_Dir_t * outDir , const char * Path );
extern oC_ErrorCode_t       oC_VirtualFileSystem_closedir      ( oC_Dir_t Dir );
extern oC_ErrorCode_t       oC_VirtualFileSystem_readdir       ( oC_Dir_t Dir , oC_FileInfo_t * outFileInfo );

extern oC_ErrorCode_t       oC_VirtualFileSystem_stat          ( const char * Path , oC_FileInfo_t * outFileInfo);
extern oC_ErrorCode_t       oC_VirtualFileSystem_unlink        ( const char * Path);
extern oC_ErrorCode_t       oC_VirtualFileSystem_rename        ( const char * OldName , const char * NewName);
extern oC_ErrorCode_t       oC_VirtualFileSystem_chmod         ( const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask);
extern oC_ErrorCode_t       oC_VirtualFileSystem_utime         ( const char * Path , oC_Timestamp_t Timestamp );
extern oC_ErrorCode_t       oC_VirtualFileSystem_mkdir         ( const char * Path);
extern oC_ErrorCode_t       oC_VirtualFileSystem_chdir         ( const char * Path);
extern oC_ErrorCode_t       oC_VirtualFileSystem_getcwd        ( char * outBuffer , uint32_t Size );

extern oC_ErrorCode_t       oC_VirtualFileSystem_getpwd        ( char * outPwd , uint32_t Size );

extern oC_ErrorCode_t       oC_VirtualFileSystem_ConvertRelativeToAbsolute( const char * Relative , char * outAbsolute , uint32_t * AbsoluteSize , bool TreatAsDir );
extern bool                 oC_VirtualFileSystem_DirExists     ( const char * Path );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* SYSTEM_USER_INC_OC_VFS_H_ */
