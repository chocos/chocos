/** ****************************************************************************************************************************************
 *
 * @file          oc_devfs.h
 *
 * @brief      The file with interface for device drivers file system
 *
 * @author     Patryk Kubiak - (Created on: 1 06 2015 19:46:31)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup DevFs DevFs - The device drivers file system
 * @ingroup CoreFileSystem
 * @brief The file system for device drivers
 * 
 ******************************************************************************************************************************************/


#ifndef INC_FS_DEVFS_OC_DEVFS_H_
#define INC_FS_DEVFS_OC_DEVFS_H_

#include <oc_fs.h>
#include <oc_storage.h>
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup DevFs
//! @{

typedef struct Context_t * oC_DevFs_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup DevFs
//! @{

extern oC_ErrorCode_t       oC_DevFs_init           ( oC_DevFs_Context_t * outContext , oC_Storage_t Storage );
extern oC_ErrorCode_t       oC_DevFs_deinit         ( oC_DevFs_Context_t Context );
extern oC_ErrorCode_t       oC_DevFs_fopen          ( oC_DevFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes );
extern oC_ErrorCode_t       oC_DevFs_fclose         ( oC_DevFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_DevFs_fread          ( oC_DevFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_DevFs_fwrite         ( oC_DevFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_DevFs_lseek          ( oC_DevFs_Context_t Context , oC_File_t File , uint32_t Offset );
extern oC_ErrorCode_t       oC_DevFs_ioctl          ( oC_DevFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer);
extern oC_ErrorCode_t       oC_DevFs_sync           ( oC_DevFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_DevFs_getc           ( oC_DevFs_Context_t Context , char * outCharacter , oC_File_t File );
extern oC_ErrorCode_t       oC_DevFs_putc           ( oC_DevFs_Context_t Context , char Character , oC_File_t File );
extern int32_t              oC_DevFs_tell           ( oC_DevFs_Context_t Context , oC_File_t File );
extern bool                 oC_DevFs_eof            ( oC_DevFs_Context_t Context , oC_File_t File );
extern uint32_t             oC_DevFs_size           ( oC_DevFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_DevFs_flush          ( oC_DevFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_DevFs_error          ( oC_DevFs_Context_t Context , oC_File_t File );

extern oC_ErrorCode_t       oC_DevFs_opendir        ( oC_DevFs_Context_t Context , oC_Dir_t * outDir , const char * Path );
extern oC_ErrorCode_t       oC_DevFs_closedir       ( oC_DevFs_Context_t Context , oC_Dir_t Dir );
extern oC_ErrorCode_t       oC_DevFs_readdir        ( oC_DevFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo );

extern oC_ErrorCode_t       oC_DevFs_stat           ( oC_DevFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo);
extern oC_ErrorCode_t       oC_DevFs_unlink         ( oC_DevFs_Context_t Context , const char * Path);
extern oC_ErrorCode_t       oC_DevFs_rename         ( oC_DevFs_Context_t Context , const char * OldName , const char * NewName);
extern oC_ErrorCode_t       oC_DevFs_chmod          ( oC_DevFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask);
extern oC_ErrorCode_t       oC_DevFs_utime          ( oC_DevFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp );
extern oC_ErrorCode_t       oC_DevFs_mkdir          ( oC_DevFs_Context_t Context , const char * Path);
extern bool                 oC_DevFs_DirExists      ( oC_DevFs_Context_t Context , const char * Path);

extern oC_ErrorCode_t       oC_DevFs_AddStorage     ( oC_Storage_t Storage , char * outPath , oC_MemorySize_t Size );
extern oC_ErrorCode_t       oC_DevFs_RemoveStorage  ( oC_Storage_t Storage );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

extern const oC_FileSystem_Registration_t DevFs;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


#endif /* INC_FS_DEVFS_OC_DEVFS_H_ */
