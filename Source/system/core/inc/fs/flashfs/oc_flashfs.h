/** ****************************************************************************************************************************************
 *
 * @file          oc_flashfs.h
 *
 * @brief      Interface for the Flash File System
 *
 * @author     Patryk Kubiak - (Created on: 15.07.2016 18:58:09) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This progFlash is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This progFlash is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this progFlash; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup FlashFs Flash File System
 * @ingroup CoreFileSystem
 * @brief The file system for the flash memory
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_FS_FLASHFS_OC_FLASHFS_H_
#define SYSTEM_CORE_INC_FS_FLASHFS_OC_FLASHFS_H_


#include <oc_fs.h>
#include <oc_memory.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup FlashFs
//! @{

typedef struct Context_t * oC_FlashFs_Context_t;

typedef struct
{
    const char *        Name;
    const uint8_t *     Data;
    oC_MemorySize_t     Size;
} oC_FlashFs_FileDefinition_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup FlashFs
//! @{

extern oC_ErrorCode_t       oC_FlashFs_init           ( oC_FlashFs_Context_t * outContext , oC_Storage_t Storage );
extern oC_ErrorCode_t       oC_FlashFs_deinit         ( oC_FlashFs_Context_t Context );
extern oC_ErrorCode_t       oC_FlashFs_fopen          ( oC_FlashFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes );
extern oC_ErrorCode_t       oC_FlashFs_fclose         ( oC_FlashFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_FlashFs_fread          ( oC_FlashFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_FlashFs_fwrite         ( oC_FlashFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_FlashFs_lseek          ( oC_FlashFs_Context_t Context , oC_File_t File , uint32_t Offset );
extern oC_ErrorCode_t       oC_FlashFs_ioctl          ( oC_FlashFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer);
extern oC_ErrorCode_t       oC_FlashFs_sync           ( oC_FlashFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_FlashFs_getc           ( oC_FlashFs_Context_t Context , char * outCharacter , oC_File_t File );
extern oC_ErrorCode_t       oC_FlashFs_putc           ( oC_FlashFs_Context_t Context , char Character , oC_File_t File );
extern int32_t              oC_FlashFs_tell           ( oC_FlashFs_Context_t Context , oC_File_t File );
extern bool                 oC_FlashFs_eof            ( oC_FlashFs_Context_t Context , oC_File_t File );
extern uint32_t             oC_FlashFs_size           ( oC_FlashFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_FlashFs_flush          ( oC_FlashFs_Context_t Context , oC_File_t File );

extern oC_ErrorCode_t       oC_FlashFs_opendir        ( oC_FlashFs_Context_t Context , oC_Dir_t * outDir , const char * Path );
extern oC_ErrorCode_t       oC_FlashFs_closedir       ( oC_FlashFs_Context_t Context , oC_Dir_t Dir );
extern oC_ErrorCode_t       oC_FlashFs_readdir        ( oC_FlashFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo );

extern oC_ErrorCode_t       oC_FlashFs_stat           ( oC_FlashFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo);
extern oC_ErrorCode_t       oC_FlashFs_unlink         ( oC_FlashFs_Context_t Context , const char * Path);
extern oC_ErrorCode_t       oC_FlashFs_rename         ( oC_FlashFs_Context_t Context , const char * OldName , const char * NewName);
extern oC_ErrorCode_t       oC_FlashFs_chmod          ( oC_FlashFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask);
extern oC_ErrorCode_t       oC_FlashFs_utime          ( oC_FlashFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp );
extern oC_ErrorCode_t       oC_FlashFs_mkdir          ( oC_FlashFs_Context_t Context , const char * Path);
extern bool                 oC_FlashFs_DirExists      ( oC_FlashFs_Context_t Context , const char * Path);

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

extern const oC_FileSystem_Registration_t FlashFs;
extern const oC_FlashFs_FileDefinition_t  oC_FlashFs_FileDefinitions[];
extern const uint32_t                     oC_FlashFs_NumberOfFiles;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________



#endif /* SYSTEM_CORE_INC_FS_FLASHFS_OC_FLASHFS_H_ */
