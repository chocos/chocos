/** ****************************************************************************************************************************************
 *
 * @brief       Module for handling disk
 * 
 * @file          oc_disk.h
 *
 * @author     Patryk Kubiak - (Created on: 20.08.2017 19:56:38) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Disk Disk
 * @ingroup CoreFileSystem
 * @brief The module for handling disk
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_FS_OC_DISK_H_
#define SYSTEM_CORE_INC_FS_OC_DISK_H_

#include <oc_stdtypes.h>
#include <oc_memory.h>
#include <oc_storage.h>
#include <oc_list.h>
#include <oc_partition.h>

#define oC_DISK_BOOT_CODE_SIZE          446
#define oC_DISK_MBR_SIZE                512

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct Disk_t * oC_Disk_t;

typedef uint8_t oC_Disk_BootstrapCode_t[oC_DISK_BOOT_CODE_SIZE];
typedef union
{
    uint8_t     Data[oC_DISK_MBR_SIZE];

    struct
    {
        oC_Disk_BootstrapCode_t     BootstrapCode;
        oC_Partition_Entry_t        PartitionEntries[4];
        uint16_t                    BootSignature;
    };
} oC_Disk_MasterBootRecord_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_Disk_t                oC_Disk_New                 ( const oC_Storage_DiskInterface_t * Interface , void * Context, const char * Name, oC_MemorySize_t SectorSize, oC_SectorNumber_t NumberOfSectors, oC_Time_t Timeout );
extern bool                     oC_Disk_Delete              ( oC_Disk_t * Disk );
extern bool                     oC_Disk_IsCorrect           ( oC_Disk_t Disk );
extern oC_MemorySize_t          oC_Disk_GetSize             ( oC_Disk_t Disk );
extern oC_MemorySize_t          oC_Disk_GetSectorSize       ( oC_Disk_t Disk );
extern oC_SectorNumber_t        oC_Disk_GetSectorCount      ( oC_Disk_t Disk );
extern const char *             oC_Disk_GetName             ( oC_Disk_t Disk );
extern oC_MemorySize_t          oC_Disk_GetFreeSize         ( oC_Disk_t Disk );
extern bool                     oC_Disk_IsBootable          ( oC_Disk_t Disk );
extern oC_ErrorCode_t           oC_Disk_ReadBootstrapCode   ( oC_Disk_t Disk, oC_Disk_BootstrapCode_t * outCode );
extern oC_ErrorCode_t           oC_Disk_ReadPartitions      ( oC_Disk_t Disk, oC_List(oC_Partition_t) outPartitions );
extern oC_ErrorCode_t           oC_Disk_AddPartition        ( oC_Disk_t Disk, oC_MemorySize_t Size, oC_Partition_Type_t Type, oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Disk_RemovePartition     ( oC_Disk_t Disk, oC_Partition_t Partition );
extern oC_ErrorCode_t           oC_Disk_ReadSectors         ( oC_Disk_t Disk, oC_SectorNumber_t StartSector, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Disk_WriteSectors        ( oC_Disk_t Disk, oC_SectorNumber_t StartSector, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Disk_Read                ( oC_Disk_t Disk, oC_MemoryOffset_t Offset, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Disk_Write               ( oC_Disk_t Disk, oC_MemoryOffset_t Offset, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_List(oC_Partition_t)  oC_Disk_GetPartitionsList   ( oC_Disk_t Disk );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



#endif /* SYSTEM_CORE_INC_FS_OC_DISK_H_ */
