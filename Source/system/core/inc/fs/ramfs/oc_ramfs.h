/** ****************************************************************************************************************************************
 *
 * @file       oc_ramfs.h
 *
 * @brief      The file with source for the RAM file system
 *
 * @author     Patryk Kubiak - (Created on: 10 10 2015 13:19:16)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup RamFs RamFs - The RAM file system
 * @ingroup CoreFileSystem
 * @brief The module for handling RAM file system
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_FS_RAMFS_OC_RAMFS_H_
#define SYSTEM_CORE_INC_FS_RAMFS_OC_RAMFS_H_

#include <oc_fs.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup RamFs
//! @{

typedef struct Context_t * oC_RamFs_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup RamFs
//! @{

extern oC_ErrorCode_t       oC_RamFs_init           ( oC_RamFs_Context_t * outContext , oC_Storage_t Storage );
extern oC_ErrorCode_t       oC_RamFs_deinit         ( oC_RamFs_Context_t Context );
extern oC_ErrorCode_t       oC_RamFs_fopen          ( oC_RamFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes );
extern oC_ErrorCode_t       oC_RamFs_fclose         ( oC_RamFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_RamFs_fread          ( oC_RamFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_RamFs_fwrite         ( oC_RamFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size );
extern oC_ErrorCode_t       oC_RamFs_lseek          ( oC_RamFs_Context_t Context , oC_File_t File , uint32_t Offset );
extern oC_ErrorCode_t       oC_RamFs_ioctl          ( oC_RamFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer);
extern oC_ErrorCode_t       oC_RamFs_sync           ( oC_RamFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_RamFs_getc           ( oC_RamFs_Context_t Context , char * outCharacter , oC_File_t File );
extern oC_ErrorCode_t       oC_RamFs_putc           ( oC_RamFs_Context_t Context , char Character , oC_File_t File );
extern bool                 oC_RamFs_eof            ( oC_RamFs_Context_t Context , oC_File_t File );
extern uint32_t             oC_RamFs_size           ( oC_RamFs_Context_t Context , oC_File_t File );
extern oC_ErrorCode_t       oC_RamFs_flush          ( oC_RamFs_Context_t Context , oC_File_t File );

extern oC_ErrorCode_t       oC_RamFs_opendir        ( oC_RamFs_Context_t Context , oC_Dir_t * outDir , const char * Path );
extern oC_ErrorCode_t       oC_RamFs_closedir       ( oC_RamFs_Context_t Context , oC_Dir_t Dir );
extern oC_ErrorCode_t       oC_RamFs_readdir        ( oC_RamFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo );

extern oC_ErrorCode_t       oC_RamFs_stat           ( oC_RamFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo);
extern oC_ErrorCode_t       oC_RamFs_unlink         ( oC_RamFs_Context_t Context , const char * Path);
extern oC_ErrorCode_t       oC_RamFs_rename         ( oC_RamFs_Context_t Context , const char * OldName , const char * NewName);
extern oC_ErrorCode_t       oC_RamFs_chmod          ( oC_RamFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask);
extern oC_ErrorCode_t       oC_RamFs_utime          ( oC_RamFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp );
extern oC_ErrorCode_t       oC_RamFs_mkdir          ( oC_RamFs_Context_t Context , const char * Path);
extern bool                 oC_RamFs_DirExists      ( oC_RamFs_Context_t Context , const char * Path);

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

extern const oC_FileSystem_Registration_t RamFs;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________



#endif /* SYSTEM_CORE_INC_FS_RAMFS_OC_RAMFS_H_ */
