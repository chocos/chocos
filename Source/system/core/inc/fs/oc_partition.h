/** ****************************************************************************************************************************************
 *
 * @brief        The file contains the partition interface
 * 
 * @file          oc_partition.h
 *
 * @author     Patryk Kubiak - (Created on: 18.08.2017 19:41:50) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Partition Partition
 * @ingroup CoreFileSystem
 * @brief The module for handling partitions
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_FS_OC_PARTITION_H_
#define SYSTEM_CORE_INC_FS_OC_PARTITION_H_

#include <oc_stdtypes.h>
#include <oc_errors.h>
#include <oc_list.h>
#include <oc_driver.h>
#include <oc_storage.h>

#define oC_PARTITION_ENTRY_SIZE     16
#define oC_PARTITION_MAX_LEVEL      5

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct Partition_t * oC_Partition_t;

typedef uint8_t oC_Partition_ChsAddress_t[3];

typedef union
{
    uint8_t      Data[oC_PARTITION_ENTRY_SIZE];
    struct
    {
        uint8_t                         _reservedStatus:7;
        uint8_t                         IsBootable:1;
        oC_Partition_ChsAddress_t       StartCHS;
        uint8_t                         Type;
        oC_Partition_ChsAddress_t       EndCHS;
        uint32_t                        LbaStartSector;
        uint32_t                        LbaSectorCount;
    };
} oC_Partition_Entry_t;

typedef enum
{
    oC_Partition_Type_Empty                     = 0 ,
    oC_Partition_Type_FAT16                     = 0x04 ,
    oC_Partition_Type_ExtendedLBA               = 0x0F ,
    oC_Partition_Type_XenixBadBlockTable        = 0xFF ,
} oC_Partition_Type_t;

typedef uint8_t oC_Partition_Index_t;
typedef uint8_t oC_Partition_Level_t;

typedef struct
{
    bool                    Filled;
    oC_Partition_Index_t    Indexes[oC_PARTITION_MAX_LEVEL];
    oC_Partition_Level_t    Level;
} oC_Partition_Location_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_Partition_t       oC_Partition_New                ( const oC_Partition_Entry_t * Entry, const oC_Partition_Location_t * Location, oC_Storage_t Storage );
extern bool                 oC_Partition_Delete             ( oC_Partition_t * Partition );
extern bool                 oC_Partition_IsCorrect          ( oC_Partition_t Partition );
extern oC_Storage_t         oC_Partition_GetStorage         ( oC_Partition_t Partition );
extern bool                 oC_Partition_IsBootable         ( oC_Partition_t Partition );
extern bool                 oC_Partition_IsPrimary          ( oC_Partition_t Partition );
extern bool                 oC_Partition_IsExtended         ( oC_Partition_t Partition );
extern oC_Partition_Index_t oC_Partition_GetPrimaryIndex    ( oC_Partition_t Partition );
extern oC_Partition_Type_t  oC_Partition_GetType            ( oC_Partition_t Partition );
extern const char *         oC_Partition_GetName            ( oC_Partition_t Partition );
extern oC_ErrorCode_t       oC_Partition_ReadLocation       ( oC_Partition_t Partition, oC_Partition_Location_t * outLocation );
extern oC_ErrorCode_t       oC_Partition_ReadEntry          ( oC_Partition_t Partition, oC_Partition_Entry_t * outEntry );
extern oC_ErrorCode_t       oC_Partition_Read               ( oC_Partition_t Partition, oC_MemoryOffset_t Offset, char *    outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Partition_Write              ( oC_Partition_t Partition, oC_MemoryOffset_t Offset, const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_MemorySize_t      oC_Partition_GetSectorSize      ( oC_Partition_t Partition );
extern oC_SectorNumber_t    oC_Partition_GetSectorCount     ( oC_Partition_t Partition );
extern oC_MemorySize_t      oC_Partition_GetSize            ( oC_Partition_t Partition );
extern oC_ErrorCode_t       oC_Partition_ReadSectors        ( oC_Partition_t Partition, oC_SectorNumber_t StartSector, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Partition_WriteSectors       ( oC_Partition_t Partition, oC_SectorNumber_t StartSector, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Partition_EraseSectors       ( oC_Partition_t Partition, oC_SectorNumber_t StartSector, oC_SectorNumber_t SectorCount, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Partition_Mount              ( oC_Partition_t Partition, const char * Path );
extern oC_ErrorCode_t       oC_Partition_Umount             ( oC_Partition_t Partition, const char * Path );
extern const char *         oC_Partition_GetDefaultMountPath( oC_Partition_t Partition );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



#endif /* SYSTEM_CORE_INC_FS_OC_PARTITION_H_ */
