/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          oc_storageman.h
 *
 * @author     Patryk Kubiak - (Created on: 20.08.2017 23:32:31) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup StorageMan StorageMan - The storage manager
 * @ingroup CoreFileSystem
 * @brief The module for handling storages
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_FS_OC_STORAGEMAN_H_
#define SYSTEM_CORE_INC_FS_OC_STORAGEMAN_H_

#include <oc_storage.h>
#include <oc_list.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t           oC_StorageMan_TurnOn            ( void );
extern oC_ErrorCode_t           oC_StorageMan_TurnOff           ( void );
extern oC_ErrorCode_t           oC_StorageMan_AddStorage        ( oC_Storage_t Storage );
extern oC_ErrorCode_t           oC_StorageMan_RemoveStorage     ( oC_Storage_t Storage );
extern oC_List(oC_Storage_t)    oC_StorageMan_GetList           ( void );
extern bool                     oC_StorageMan_IsNameAvailable   ( const char * Name );
extern oC_Storage_t             oC_StorageMan_GetStorage        ( const char * Name );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



#endif /* SYSTEM_CORE_INC_FS_OC_STORAGEMAN_H_ */
