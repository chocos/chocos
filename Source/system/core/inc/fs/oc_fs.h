/** ****************************************************************************************************************************************
 *
 * @file       oc_fs.h
 *
 * @brief      The file with interface for file systems
 *
 * @author     Patryk Kubiak - (Created on: 30 05 2015 18:23:08)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup FileSystem File System Interface
 * @ingroup CoreFileSystem
 * @brief The module for handling file systems
 * 
 ******************************************************************************************************************************************/


#ifndef INC_FS_OC_FS_H_
#define INC_FS_OC_FS_H_

#include <stdint.h>
#include <stdbool.h>
#include <oc_list.h>
#include <oc_ioctl.h>
#include <oc_time.h>
#include <oc_errors.h>
#include <oc_string.h>
#include <oc_storage.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup FileSystem
//! @{

typedef struct File_t * oC_File_t;

//==========================================================================================================================================
/**
 * Modes of opening files
 */
//==========================================================================================================================================
typedef enum
{
    oC_FileSystem_ModeFlags_Read            = (1<<0),   //!< specifies read access to the file
    oC_FileSystem_ModeFlags_Write           = (1<<1),   //!< specifies write access to the file
    oC_FileSystem_ModeFlags_SeekToTheEnd    = (1<<6),   //!< moves offset pointer to the end of the file
    oC_FileSystem_ModeFlags__FailIfExists   = (1<<2),
    oC_FileSystem_ModeFlags__FailIfNotExists= (1<<3),
    oC_FileSystem_ModeFlags__Open           = (1<<4),
    oC_FileSystem_ModeFlags__Create         = (1<<5),
    oC_FileSystem_ModeFlags_OpenExisting    = oC_FileSystem_ModeFlags__Open           |
                                              oC_FileSystem_ModeFlags__FailIfNotExists ,     //!< open file (fails if file does not exist)
    oC_FileSystem_ModeFlags_OpenAlways      = oC_FileSystem_ModeFlags__Open           |
                                              oC_FileSystem_ModeFlags__Create         ,      //!< open file (create new if it does not exist)
    oC_FileSystem_ModeFlags_CreateNew       = oC_FileSystem_ModeFlags__Create         |
                                              oC_FileSystem_ModeFlags__FailIfExists,         //!< create new file (fails if file exists)
    oC_FileSystem_ModeFlags_CreateNewAlways = oC_FileSystem_ModeFlags__Create                //!< create new file (overwrite if exists)
} oC_FileSystem_ModeFlags_t;

typedef enum
{
    oC_FileSystem_FileAttributes_ReadOnly = (1<<0),
    oC_FileSystem_FileAttributes_Archive  = (1<<1),
    oC_FileSystem_FileAttributes_System   = (1<<2),
    oC_FileSystem_FileAttributes_Hidden   = (1<<3)
} oC_FileSystem_FileAttributes_t;

typedef enum
{
    oC_FileSystem_FileType_File ,
    oC_FileSystem_FileType_Directory
} oC_FileSystem_FileType_t;

typedef struct Dir_t * oC_Dir_t;

typedef struct
{
    oC_Timestamp_t              Timestamp;
    const char *                Name;
    oC_MemorySize_t             Size;
    oC_FileSystem_FileType_t    FileType;
    oC_DefaultString_t          _nameBuffer;
} oC_FileInfo_t;

typedef struct Context_t * oC_FileSystem_Context_t;

typedef oC_ErrorCode_t (*oC_FileSystem_init_t)     ( oC_FileSystem_Context_t * outContext , oC_Storage_t Storage );
typedef oC_ErrorCode_t (*oC_FileSystem_deinit_t)   ( oC_FileSystem_Context_t Context );
typedef oC_ErrorCode_t (*oC_FileSystem_fopen_t)    ( oC_FileSystem_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes );
typedef oC_ErrorCode_t (*oC_FileSystem_fclose_t)   ( oC_FileSystem_Context_t Context , oC_File_t File );
typedef oC_ErrorCode_t (*oC_FileSystem_fread_t)    ( oC_FileSystem_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size );
typedef oC_ErrorCode_t (*oC_FileSystem_fwrite_t)   ( oC_FileSystem_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size );
typedef oC_ErrorCode_t (*oC_FileSystem_lseek_t)    ( oC_FileSystem_Context_t Context , oC_File_t File , uint32_t Offset );
typedef oC_ErrorCode_t (*oC_FileSystem_ioctl_t)    ( oC_FileSystem_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer);
typedef oC_ErrorCode_t (*oC_FileSystem_sync_t)     ( oC_FileSystem_Context_t Context , oC_File_t File );
typedef oC_ErrorCode_t (*oC_FileSystem_getc_t)     ( oC_FileSystem_Context_t Context , char * outCharacter , oC_File_t File );
typedef oC_ErrorCode_t (*oC_FileSystem_putc_t)     ( oC_FileSystem_Context_t Context , char Character , oC_File_t File );
typedef int32_t        (*oC_FileSystem_tell_t)     ( oC_FileSystem_Context_t Context , oC_File_t File );
typedef bool           (*oC_FileSystem_eof_t)      ( oC_FileSystem_Context_t Context , oC_File_t File );
typedef uint32_t       (*oC_FileSystem_size_t)     ( oC_FileSystem_Context_t Context , oC_File_t File );
typedef oC_ErrorCode_t (*oC_FileSystem_flush_t)    ( oC_FileSystem_Context_t Context , oC_File_t File );
typedef oC_ErrorCode_t (*oC_FileSystem_error_t)    ( oC_FileSystem_Context_t Context , oC_File_t File );

typedef oC_ErrorCode_t (*oC_FileSystem_opendir_t)  ( oC_FileSystem_Context_t Context , oC_Dir_t * outDir , const char * Path );
typedef oC_ErrorCode_t (*oC_FileSystem_closedir_t) ( oC_FileSystem_Context_t Context , oC_Dir_t Dir );
typedef oC_ErrorCode_t (*oC_FileSystem_readdir_t)  ( oC_FileSystem_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo );

typedef oC_ErrorCode_t (*oC_FileSystem_stat_t)     ( oC_FileSystem_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo);
typedef oC_ErrorCode_t (*oC_FileSystem_unlink_t)   ( oC_FileSystem_Context_t Context , const char * Path);
typedef oC_ErrorCode_t (*oC_FileSystem_rename_t)   ( oC_FileSystem_Context_t Context , const char * OldName , const char * NewName);
typedef oC_ErrorCode_t (*oC_FileSystem_chmod_t)    ( oC_FileSystem_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask);
typedef oC_ErrorCode_t (*oC_FileSystem_utime_t)    ( oC_FileSystem_Context_t Context , const char * Path , oC_Timestamp_t Timestamp );
typedef oC_ErrorCode_t (*oC_FileSystem_mkdir_t)    ( oC_FileSystem_Context_t Context , const char * Path);
typedef bool           (*oC_FileSystem_DirExists_t)( oC_FileSystem_Context_t Context , const char * Path );

typedef struct
{
    const char *                   Name;
    oC_FileSystem_init_t           init;
    oC_FileSystem_deinit_t         deinit;
    oC_FileSystem_fopen_t          fopen;
    oC_FileSystem_fclose_t         fclose;
    oC_FileSystem_fread_t          fread;
    oC_FileSystem_fwrite_t         fwrite;
    oC_FileSystem_lseek_t          lseek;
    oC_FileSystem_ioctl_t          ioctl;
    oC_FileSystem_sync_t           sync;
    oC_FileSystem_getc_t           Getc;
    oC_FileSystem_putc_t           Putc;
    oC_FileSystem_tell_t           tell;
    oC_FileSystem_eof_t            eof;
    oC_FileSystem_size_t           size;
    oC_FileSystem_flush_t          fflush;
    oC_FileSystem_error_t          error;

    oC_FileSystem_opendir_t        opendir;
    oC_FileSystem_closedir_t       closedir;
    oC_FileSystem_readdir_t        readdir;

    oC_FileSystem_stat_t           stat;
    oC_FileSystem_unlink_t         unlink;
    oC_FileSystem_rename_t         rename;
    oC_FileSystem_chmod_t          chmod;
    oC_FileSystem_utime_t          utime;
    oC_FileSystem_mkdir_t          mkdir;
    oC_FileSystem_DirExists_t      DirExists;

} oC_FileSystem_Registration_t;

typedef const oC_FileSystem_Registration_t * oC_FileSystem_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

#endif /* INC_FS_OC_FS_H_ */
