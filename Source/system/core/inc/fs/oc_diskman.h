/** ****************************************************************************************************************************************
 *
 * @file       oc_diskman.h
 *
 * @brief      contains interface for DiskMan module
 *
 * @author     Patryk Kubiak - (Created on: 26 09 2017 19:59:21)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup DiskMan DiskMan - Disks Manager
 * @ingroup CoreFileSystem
 * @brief Module for managing operations on disks
 *
 * The module is responsible for managing disks in the system. It consist of 2 main parts - the module and service. The service is responsible
 * for waiting for new disks and automatically mounting all found partitions. The module is responsible for storing disks. You can access
 * the full list by using function #oC_DiskMan_GetList
 *
 ******************************************************************************************************************************************/


#ifndef OC_DISKMAN_H_
#define OC_DISKMAN_H_

#include <oc_disk.h>
#include <oc_list.h>
#include <oc_service.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________
//! @addtogroup DiskMan
//! @{

//==========================================================================================================================================
/**
 * @brief list of type oC_List(oC_Disk_t) with disks
 *
 * The type is for storing list with disks. The type of the list stored on the list is: oC_List(oC_Disk_t)
 */
//==========================================================================================================================================
#ifndef DOXYGEN
typedef oC_List(oC_Disk_t) oC_DiskList_t;
#else
typedef oC_List_t oC_DiskList_t;
#endif

#undef  _________________________________________TYPES______________________________________________________________________________________
//! @}


/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_________________________________________________________________________________
//! @addtogroup DiskMan
//! @{

extern oC_ErrorCode_t       oC_DiskMan_TurnOn       ( void );
extern oC_ErrorCode_t       oC_DiskMan_TurnOff      ( void );
extern oC_DiskList_t        oC_DiskMan_GetList      ( void );
extern oC_Disk_t            oC_DiskMan_GetDisk      ( const char * DiskName );
extern oC_ErrorCode_t       oC_DiskMan_AddDisk      ( oC_Disk_t Disk );
extern oC_ErrorCode_t       oC_DiskMan_RemoveDisk   ( oC_Disk_t Disk );

#undef  _________________________________________PROTOTYPES_________________________________________________________________________________
//! @}

#endif /* OC_DISKMAN_H_ */
