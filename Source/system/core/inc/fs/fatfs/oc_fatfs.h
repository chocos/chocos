/** ****************************************************************************************************************************************
 *
 * @file       oc_fatfs.h
 *
 * @brief      Contains interface for the FAT file system
 *
 * @author     Patryk Kubiak - (Created on: 21 09 2017 18:03:31)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup FatFs FatFs
 * @ingroup CoreFileSystem
 * @brief The module for handling FAT file system
 * 
 ******************************************************************************************************************************************/
#ifndef OC_FATFS_H_
#define OC_FATFS_H_

#include <oc_fs.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________

typedef struct Context_t * oC_FatFs_Context_t;

#undef  _________________________________________TYPES______________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_________________________________________________________________________________

// Initializes file system
extern oC_ErrorCode_t       oC_FatFs_init           ( oC_FatFs_Context_t * outContext , oC_Storage_t Storage );
// destroys file system
extern oC_ErrorCode_t       oC_FatFs_deinit         ( oC_FatFs_Context_t Context );
// opens file
extern oC_ErrorCode_t       oC_FatFs_fopen          ( oC_FatFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes );
// closes file
extern oC_ErrorCode_t       oC_FatFs_fclose         ( oC_FatFs_Context_t Context , oC_File_t File );
// reads bytes from the file
extern oC_ErrorCode_t       oC_FatFs_fread          ( oC_FatFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size );
// writes byte to the file
extern oC_ErrorCode_t       oC_FatFs_fwrite         ( oC_FatFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size );
// sets offset in the file
extern oC_ErrorCode_t       oC_FatFs_lseek          ( oC_FatFs_Context_t Context , oC_File_t File , uint32_t Offset );
// performs special request to the file
extern oC_ErrorCode_t       oC_FatFs_ioctl          ( oC_FatFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer);
// see `sync` from posix
extern oC_ErrorCode_t       oC_FatFs_sync           ( oC_FatFs_Context_t Context , oC_File_t File );
// reads 1 char from file
extern oC_ErrorCode_t       oC_FatFs_getc           ( oC_FatFs_Context_t Context , char * outCharacter , oC_File_t File );
// puts 1 char to file
extern oC_ErrorCode_t       oC_FatFs_putc           ( oC_FatFs_Context_t Context , char Character , oC_File_t File );
// reads offset in file
extern int32_t              oC_FatFs_tell           ( oC_FatFs_Context_t Context , oC_File_t File );
// returns true if this is end of file
extern bool                 oC_FatFs_eof            ( oC_FatFs_Context_t Context , oC_File_t File );
// returns file size
extern uint32_t             oC_FatFs_size           ( oC_FatFs_Context_t Context , oC_File_t File );
// flushes file
extern oC_ErrorCode_t       oC_FatFs_flush          ( oC_FatFs_Context_t Context , oC_File_t File );
// reads last error
extern oC_ErrorCode_t       oC_FatFs_error          ( oC_FatFs_Context_t Context , oC_File_t File );
// opens directory
extern oC_ErrorCode_t       oC_FatFs_opendir        ( oC_FatFs_Context_t Context , oC_Dir_t * outDir , const char * Path );
// closes directory
extern oC_ErrorCode_t       oC_FatFs_closedir       ( oC_FatFs_Context_t Context , oC_Dir_t Dir );
// reads next file information from the directory (each execution returns info about next file)
extern oC_ErrorCode_t       oC_FatFs_readdir        ( oC_FatFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo );
// Returns info about the given file
extern oC_ErrorCode_t       oC_FatFs_stat           ( oC_FatFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo);
// deletes a file
extern oC_ErrorCode_t       oC_FatFs_unlink         ( oC_FatFs_Context_t Context , const char * Path);
// changes name of a file
extern oC_ErrorCode_t       oC_FatFs_rename         ( oC_FatFs_Context_t Context , const char * OldName , const char * NewName);
// changes attributes of the file
extern oC_ErrorCode_t       oC_FatFs_chmod          ( oC_FatFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask);
// sets timestamp of file
extern oC_ErrorCode_t       oC_FatFs_utime          ( oC_FatFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp );
// creates new directory
extern oC_ErrorCode_t       oC_FatFs_mkdir          ( oC_FatFs_Context_t Context , const char * Path);
// returns true if directory exists
extern bool                 oC_FatFs_DirExists      ( oC_FatFs_Context_t Context , const char * Path);

#undef  _________________________________________PROTOTYPES_________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

extern const oC_FileSystem_Registration_t FatFs;

#undef  _________________________________________VARIABLES__________________________________________________________________________________


#endif /* OC_FATFS_H_ */
