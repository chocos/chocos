/** ****************************************************************************************************************************************
 *
 * @file       oc_gtd.h
 * 
 * File based on driver.h Ver 1.1.0
 *
 * @brief      The file with interface for GTD driver
 *
 * @author     Patryk Kubiak - (Created on: 2017-02-06 - 19:07:41)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup GTD    Graphic Terminal Drivers
 * @ingroup Drivers
 * @brief GTD - managing and configuring graphic terminal drivers.
 *
 ******************************************************************************************************************************************/
#ifndef _OC_GTD_H
#define _OC_GTD_H
#define DRIVER_HEADER
#define DRIVER_NAME                 GTD

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_pixel.h>
#include <oc_color.h>
#include <oc_tgui.h>
#include <oc_font.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup GTD
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores definition of terminal color
 */
//==========================================================================================================================================
typedef struct
{
    oC_TGUI_Color_t     TerminalColorId;    //!< Color ID from the terminal GUI
    oC_Color_t          Color;              //!< Real color to use for displaying the given `TerminalColorId`
    oC_ColorFormat_t    ColorFormat;        //!< Format of the given `Color` (RGB888, RGB565, etc)
} oC_GTD_ColorDefinition_t;

//==========================================================================================================================================
/**
 * @brief type for indexing #oC_TGUI_Color_t inside the #oC_GTD_ColorsArray_t array
 */
//==========================================================================================================================================
typedef enum
{
    oC_GTD_TerminalColorIndex_DefaultBackground , //!< Default background
    oC_GTD_TerminalColorIndex_DefaultForeground , //!< Default foreground
    oC_GTD_TerminalColorIndex_Cursor            , //!< Cursor color
    oC_GTD_TerminalColorIndex_Black         ,     //!< Black
    oC_GTD_TerminalColorIndex_Red           ,     //!< Red
    oC_GTD_TerminalColorIndex_Green         ,     //!< Green
    oC_GTD_TerminalColorIndex_Yellow        ,     //!< Yellow
    oC_GTD_TerminalColorIndex_Blue          ,     //!< Blue
    oC_GTD_TerminalColorIndex_Magenda       ,     //!< Magenda
    oC_GTD_TerminalColorIndex_Cyan          ,     //!< Cyan
    oC_GTD_TerminalColorIndex_LightGray     ,     //!< LightGray
    oC_GTD_TerminalColorIndex_DarkGray      ,     //!< DarkGray
    oC_GTD_TerminalColorIndex_LightRed      ,     //!< LightRed
    oC_GTD_TerminalColorIndex_LightGreen    ,     //!< LightGreen
    oC_GTD_TerminalColorIndex_LightYellow   ,     //!< LightYellow
    oC_GTD_TerminalColorIndex_LightBlue     ,     //!< LightBlue
    oC_GTD_TerminalColorIndex_LightMagenda  ,     //!< LightMagenda
    oC_GTD_TerminalColorIndex_LightCyan     ,     //!< LightCyan
    oC_GTD_TerminalColorIndex_White         ,     //!< White
    oC_GTD_TerminalColorIndex_NumberOfElements    //!< Number of colors inside the array
} oC_GTD_TerminalColorIndex_t;

//==========================================================================================================================================
/**
 * @brief type stores definitions of terminal colors
 */
//==========================================================================================================================================
typedef oC_GTD_ColorDefinition_t oC_GTD_ColorsArray_t[oC_GTD_TerminalColorIndex_NumberOfElements];

//==========================================================================================================================================
/**
 * @brief GTD driver configuration structure
 *
 * This is the configuration structure for the GTD driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_GTD_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    const char *                ScreenName;     //!< Name of the configured screen to use for terminal drawing
    oC_Pixel_ResolutionUInt_t   Width;          //!< Width of the part of the screen that should be used to draw terminal
    oC_Pixel_ResolutionUInt_t   Height;         //!< Height of the part of the screen that should be used to draw terminal
    oC_Pixel_Position_t         Position;       //!< Position of the terminal window
    const oC_GTD_ColorsArray_t* ColorsArray;    //!< Array with definitions of terminal colors (Optional). Set it to #NULL to use default
    oC_Font_t                   FontG0;         //!< Pointer to the font to use as G0 (standard font). Set it to #NULL to use default
    oC_Font_t                   FontG1;         //!< Pointer to the font to use as G1. Set it to #NULL to use default
} oC_GTD_Config_t;

//==========================================================================================================================================
/**
 * @brief The GTD context structure.
 *
 * This is the structure with dynamic allocated data for the GTD. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_GTD_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_GTD_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_GTD_Context_t;

/** @} */
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/**
 * @addtogroup GTD
 * @{
 */

extern oC_ErrorCode_t oC_GTD_TurnOn            ( void );
extern oC_ErrorCode_t oC_GTD_TurnOff           ( void );
extern bool           oC_GTD_IsTurnedOn        ( void );
extern oC_ErrorCode_t oC_GTD_Configure         ( const oC_GTD_Config_t * Config , oC_GTD_Context_t * outContext );
extern oC_ErrorCode_t oC_GTD_Unconfigure       ( const oC_GTD_Config_t * Config , oC_GTD_Context_t * outContext );
extern oC_ErrorCode_t oC_GTD_Read              ( oC_GTD_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_GTD_Write             ( oC_GTD_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_GTD_Ioctl             ( oC_GTD_Context_t Context , oC_Ioctl_Command_t Command , void * Data );

/** @} */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* _OC_GTD_H */
