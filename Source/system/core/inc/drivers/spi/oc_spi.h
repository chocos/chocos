/** ****************************************************************************************************************************************
 *
 * @file       oc_spi.h
 * 
 * File based on driver.h Ver 1.1.1
 *
 * @brief      The file with interface for SPI driver
 *
 * @author     Patryk Kubiak - (Created on: 2017-07-20 - 00:42:25)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SPI    Serial Peripheral Interface
 * @ingroup Drivers
 * @brief SPI - managing and configuring SPI transmissions.
 * 
 ******************************************************************************************************************************************/
#ifndef _OC_SPI_H
#define _OC_SPI_H
#define DRIVER_HEADER
#define DRIVER_NAME                 SPI

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_spi_lld.h>
#include <oc_gpio.h>

#ifdef oC_SPI_LLD_AVAILABLE
#define oC_SPI_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Maximum number of CHIP select pins
 */
//==========================================================================================================================================
#define oC_SPI_MAX_CHIP_SELECT_PINS         10

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores mode of transmission
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_Mode_Master = oC_SPI_LLD_Mode_Master ,//!< Works as master
    oC_SPI_Mode_Slave  = oC_SPI_LLD_Mode_Slave , //!< Works as slave
} oC_SPI_Mode_t;

//==========================================================================================================================================
/**
 * @brief stores frame format
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_FrameFormat_MSB = oC_SPI_LLD_FrameFormat_MSB ,    //!< MSB is first
    oC_SPI_FrameFormat_LSB = oC_SPI_LLD_FrameFormat_LSB ,    //!< LSB is first
} oC_SPI_FrameFormat_t;


//==========================================================================================================================================
/**
 * @brief stores clock polarity
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_ClockPolarity_HighActive = oC_SPI_LLD_ClockPolarity_HighActive ,   //!< Clock is active when CLK is 1
    oC_SPI_ClockPolarity_LowActive  = oC_SPI_LLD_ClockPolarity_LowActive ,    //!< Clock is active when CLK is 0
} oC_SPI_ClockPolarity_t;


//==========================================================================================================================================
/**
 * @brief stores clock phase
 */
//==========================================================================================================================================
typedef enum
{
    oC_SPI_ClockPhase_FirstTransition  = oC_SPI_LLD_ClockPhase_FirstTransition ,  //!< The first clock transition is the first data capture edge
    oC_SPI_ClockPhase_SecondTransition = oC_SPI_LLD_ClockPhase_SecondTransition , //!< The second clock transition is the first data capture edge
} oC_SPI_ClockPhase_t;

//==========================================================================================================================================
/**
 * @brief stores size of SPI transmission data in bits
 */
//==========================================================================================================================================
typedef oC_SPI_LLD_DataSize_t oC_SPI_DataSize_t;

//==========================================================================================================================================
/**
 * @brief stores ID of Chip Select, cannot be grower than #oC_SPI_MAX_CHIP_SELECT_PINS
 */
//==========================================================================================================================================
typedef uint16_t oC_SPI_ChipSelectId_t;

//==========================================================================================================================================
/**
 * @brief stores data for the SPI transmissions
 */
//==========================================================================================================================================
typedef oC_SPI_LLD_Data_t oC_SPI_Data_t;

//==========================================================================================================================================
/**
 * @brief SPI driver configuration structure
 *
 * This is the configuration structure for the SPI driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_SPI_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    struct
    {
        oC_Pin_t ChipSelect[oC_SPI_MAX_CHIP_SELECT_PINS];
        oC_Pin_t CLK;
        oC_Pin_t MISO;
        oC_Pin_t MOSI;
    } Pins;

    oC_BaudRate_t           BaudRate;
    oC_BaudRate_t           Tolerance;
    oC_SPI_Mode_t           Mode;
    oC_SPI_FrameFormat_t    FrameFormat;
    oC_SPI_ClockPolarity_t  ClockPolarity;
    oC_SPI_ClockPhase_t     ClockPhase;
    oC_SPI_DataSize_t       DataSize;
    oC_SPI_ChipSelectId_t   DefaultChipId;
    bool                    ChipSelectActiveHigh;

    struct
    {
        oC_SPI_Channel_t    Channel;
    } Advanced;
} oC_SPI_Config_t;

//==========================================================================================================================================
/**
 * @brief The SPI context structure.
 *
 * This is the structure with dynamic allocated data for the SPI. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_SPI_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_SPI_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_SPI_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_ErrorCode_t oC_SPI_TurnOn                 ( void );
extern oC_ErrorCode_t oC_SPI_TurnOff                ( void );
extern bool           oC_SPI_IsTurnedOn             ( void );
extern oC_ErrorCode_t oC_SPI_Configure              ( const oC_SPI_Config_t * Config , oC_SPI_Context_t * outContext );
extern oC_ErrorCode_t oC_SPI_Unconfigure            ( const oC_SPI_Config_t * Config , oC_SPI_Context_t * outContext );
extern oC_ErrorCode_t oC_SPI_Read                   ( oC_SPI_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SPI_Write                  ( oC_SPI_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SPI_Ioctl                  ( oC_SPI_Context_t Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t oC_SPI_SetChip                ( oC_SPI_Context_t Context , oC_SPI_ChipSelectId_t ChipSelectId , bool Active );
extern oC_ErrorCode_t oC_SPI_Tranceive              ( oC_SPI_Context_t Context , const void * TxData , void * outRxData , uint32_t Count , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SPI_LockChannel            ( oC_SPI_Context_t Context , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SPI_UnlockChannel          ( oC_SPI_Context_t Context );
extern oC_BaudRate_t  oC_SPI_GetConfiguredBaudRate  ( oC_SPI_Context_t Context );
extern oC_ErrorCode_t oC_SPI_ReadChannel            ( oC_SPI_Context_t Context , oC_SPI_Channel_t * outChannel );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* oC_SPI_LLD_AVAILABLE */
#endif /* _OC_SPI_H */
