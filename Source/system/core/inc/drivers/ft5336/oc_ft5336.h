/** ****************************************************************************************************************************************
 *
 * @file       oc_ft5336.h
 * 
 * File based on driver.h Ver 1.1.1
 *
 * @brief      The file with interface for FT5336 driver
 *
 * @author     Patryk Kubiak - (Created on: 2017-05-02 - 18:34:20)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup FT5336 FT5336 Touch screen driver
 * @ingroup Drivers
 * @brief FT5336 - managing and configuring touch screen.
 * 
 ******************************************************************************************************************************************/
#ifndef _OC_FT5336_H
#define _OC_FT5336_H
#define DRIVER_HEADER
#define DRIVER_NAME                 FT5336

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_i2c.h>
#include <oc_pixel.h>
#include <oc_idi.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores mode of the work
 *
 * The type is for storing touch screen mode work.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FT5336_Mode_InterruptMode = 0 ,      //!< IT pin is connected and interrupt is generated when touch screen is touched
    oC_FT5336_Mode_PollingMode   = 1 ,      //!< FT driver should pull the touch screen about the state, IT pin is not connected
} oC_FT5336_Mode_t;

//==========================================================================================================================================
/**
 * @brief FT5336 driver configuration structure
 *
 * This is the configuration structure for the FT5336 driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_FT5336_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    //======================================================================================================================================
    /**
     * @brief configuration of the I2C bus
     *
     * The field is for choosing I2C configuration. FT5336 driver can use 'auto configuration' identified by a name. It will use the name to
     * get a I2C context from the driver manager. Note, that if also the FT driver is configured as autoconfiguration, it should be placed
     * in the 'AutoConfiguration' list after the I2C driver configuration.
     *
     * It is also possible to configure the I2C by manual - in this case you can give the I2C context directly by using `Context` parameter.
     *
     * If the FT is the only device on the I2C bus, you don't have to configure I2C by yourself. In this case you have to just set SCL and
     * SDA fields and the FT driver will do the rest.
     */
    //======================================================================================================================================
    struct
    {
        const char *        AutoConfigurationName;      //!< Name of the auto-configuration (optional). It is one of 3 possible ways to configure I2C
        oC_I2C_Context_t    Context;                    //!< Context of the I2C driver (optional). It is one of 3 possible ways to configure I2C
        oC_Pin_t            SCL;                        //!< SCL pin where the touch screen is connected (optional - use with SDA). It is one of 3 possible ways to configure I2C
        oC_Pin_t            SDA;                        //!< SDA pin where the touch screen is connected (optional - use with SCL). It is one of 3 possible ways to configure I2C
    } I2C;

    oC_FT5336_Mode_t    Mode;               //!< Mode of the touch screen work
    oC_Pin_t            InterruptPin;       //!< Interrupt pin (required in interrupt mode)
    oC_Time_t           PollingTime;        //!< Period of requesting about the state of the touch-screen (required in pull mode)
} oC_FT5336_Config_t;

//==========================================================================================================================================
/**
 * @brief The FT5336 context structure.
 *
 * This is the structure with dynamic allocated data for the FT5336. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_FT5336_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_FT5336_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_FT5336_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_ErrorCode_t oC_FT5336_TurnOn            ( void );
extern oC_ErrorCode_t oC_FT5336_TurnOff           ( void );
extern bool           oC_FT5336_IsTurnedOn        ( void );
extern oC_ErrorCode_t oC_FT5336_Configure         ( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t * outContext );
extern oC_ErrorCode_t oC_FT5336_Unconfigure       ( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t * outContext );
extern oC_ErrorCode_t oC_FT5336_WaitForInput      ( oC_FT5336_Context_t Context , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent , oC_Time_t Timeout );
extern bool           oC_FT5336_IsEventSupported  ( oC_FT5336_Context_t Context , oC_IDI_EventId_t EventId );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* _OC_FT5336_H */
