/** ****************************************************************************************************************************************
 *
 * @file       oc_timer.h
 *
 * @brief      The file with interface for the timer driver.
 *
 * @author     Patryk Kubiak - (Created on: 30 08 2015 13:30:25)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup TIMER Timers
 * @ingroup Drivers
 * @brief TIMER - managing and configuring HW timers.
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_DRIVERS_TIMER_OC_TIMER_H_
#define SYSTEM_CORE_INC_DRIVERS_TIMER_OC_TIMER_H_

#define DRIVER_HEADER
#define DRIVER_NAME                 TIMER

#include <oc_driver.h>
#include <oc_timer_lld.h>
#include <oc_time.h>

#undef DRIVER_HEADER
#undef DRIVER_NAME


typedef struct Context_t * oC_TIMER_Context_t;

typedef enum
{
    oC_TIMER_Mode_Periodic             = oC_TIMER_LLD_Mode_PeriodicTimer ,
    oC_TIMER_Mode_InputEdgeCount       = oC_TIMER_LLD_Mode_InputEdgeCount ,
    oC_TIMER_Mode_InputEdgeTime        = oC_TIMER_LLD_Mode_InputEdgeTime ,
    oC_TIMER_Mode_RTC                  = oC_TIMER_LLD_Mode_RealTimeClock ,
    oC_TIMER_Mode_PWM                  = oC_TIMER_LLD_Mode_PWM
} oC_TIMER_Mode_t;

typedef enum
{
    oC_TIMER_PwmState_Low   = oC_TIMER_LLD_PwmState_Low ,
    oC_TIMER_PwmState_High  = oC_TIMER_LLD_PwmState_High ,
} oC_TIMER_PwmState_t;

typedef enum
{
    oC_TIMER_InputTrigger_RisingEdge    = oC_TIMER_LLD_Trigger_RisingEdge ,
    oC_TIMER_InputTrigger_FallingEdge   = oC_TIMER_LLD_Trigger_FallingEdge ,
    oC_TIMER_InputTrigger_BothEdges     = oC_TIMER_LLD_Trigger_Both ,
} oC_TIMER_InputTrigger_t;

typedef enum
{
    oC_TIMER_CountDirection_DoesntMatter = oC_TIMER_LLD_CountDirection_DoesntMatter ,
    oC_TIMER_CountDirection_Up           = oC_TIMER_LLD_CountDirection_Up ,
    oC_TIMER_CountDirection_Down         = oC_TIMER_LLD_CountDirection_Down
} oC_TIMER_CountDirection_t;

typedef oC_TIMER_LLD_EventHandler_t oC_TIMER_EventHandler_t;

typedef oC_TIMER_LLD_EventFlags_t oC_TIMER_EventFlags_t;

typedef struct
{
    oC_TIMER_Mode_t             Mode;
    oC_Time_t                   MaximumTimeForWait;
    oC_Frequency_t              Frequency;
    oC_Frequency_t              PermissibleDifference;
    uint64_t                    MaximumValue;
    uint64_t                    MatchValue;
    oC_TIMER_EventFlags_t       EventFlags;
    oC_TIMER_EventHandler_t     EventHandler;
    oC_Pins_t                   Pin;
    oC_TIMER_CountDirection_t   CountDirection;

    union
    {
        oC_TIMER_PwmState_t         StartPwmState;
        oC_TIMER_InputTrigger_t     InputTrigger;
    };
} oC_TIMER_Config_t;

extern oC_ErrorCode_t oC_TIMER_TurnOn            ( void );
extern oC_ErrorCode_t oC_TIMER_TurnOff           ( void );
extern bool           oC_TIMER_IsTurnedOn        ( void );
extern oC_ErrorCode_t oC_TIMER_Configure         ( const oC_TIMER_Config_t * Config , oC_TIMER_Context_t * outContext );
extern oC_ErrorCode_t oC_TIMER_Unconfigure       ( const oC_TIMER_Config_t * Config , oC_TIMER_Context_t * outContext );
extern oC_ErrorCode_t oC_TIMER_ReadValue         ( oC_TIMER_Context_t Context , uint64_t * outValue );
extern oC_ErrorCode_t oC_TIMER_SetValue          ( oC_TIMER_Context_t Context , uint64_t Value );
extern oC_ErrorCode_t oC_TIMER_ReadMatchValue    ( oC_TIMER_Context_t Context , uint64_t * outValue );
extern oC_ErrorCode_t oC_TIMER_SetMatchValue     ( oC_TIMER_Context_t Context , uint64_t Value );
extern oC_ErrorCode_t oC_TIMER_ReadMaxValue      ( oC_TIMER_Context_t Context , uint64_t * outValue );
extern oC_ErrorCode_t oC_TIMER_SetMaxValue       ( oC_TIMER_Context_t Context , uint64_t Value );
extern oC_ErrorCode_t oC_TIMER_Start             ( oC_TIMER_Context_t Context );
extern oC_ErrorCode_t oC_TIMER_Stop              ( oC_TIMER_Context_t Context );

#endif /* SYSTEM_CORE_INC_DRIVERS_TIMER_OC_TIMER_H_ */
