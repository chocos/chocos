/** ****************************************************************************************************************************************
 *
 * @file       oc_driverman.h
 *
 * @brief      The file with drivers manager interface
 *
 * @author     Patryk Kubiak - (Created on: 18 05 2015 19:43:39)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup DriverMan Driver Manager (DriverMan)
 * @ingroup Drivers
 * @brief The module for managing drivers
 * 
 ******************************************************************************************************************************************/


#ifndef INC_DRIVERS_OC_DRIVERMAN_H_
#define INC_DRIVERS_OC_DRIVERMAN_H_

#include <oc_errors.h>
#include <oc_list.h>
#include <oc_driver.h>
#include <oc_system_cfg.h>
#include <oc_string.h>

// List of drivers
#include <oc_gpio.h>
#include <oc_uart.h>
#include <oc_timer.h>
#include <oc_led.h>
#include <oc_pwm.h>
#include <oc_lcdtft.h>
#include <oc_fmc.h>
#include <oc_fmc_chips.h>
#include <oc_eth.h>
#include <oc_eth_chips.h>
#include <oc_gtd.h>
#include <oc_i2c.h>
#include <oc_ft5336.h>
#include <oc_spi.h>
#include <oc_sdmmc.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup DriverMan
//! @{

typedef struct
{
    char                Name[CFG_BYTES_MAX_CONFIGURATION_NAME_SIZE];
    oC_Driver_t         Driver;
    void *              Config;
    void *              Context;
    bool                Configured;
} oC_AutoConfiguration_t;

typedef struct
{
    oC_DefaultString_t  Name;
    oC_Driver_t         Driver;
    oC_Driver_Config_t  Config;
    oC_Driver_Context_t Context;
} oC_DriverInstance_t;

typedef void (*oC_DriverInstanceAddedCallback_t)  ( void * UserCallback, oC_DriverInstance_t * Instance );
typedef void (*oC_DriverInstanceRemovedCallback_t)( void * UserCallback, oC_DriverInstance_t * Instance );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup DriverMan
//! @{

extern oC_ErrorCode_t                   oC_DriverMan_TurnOn                    ( void );
extern oC_ErrorCode_t                   oC_DriverMan_TurnOff                   ( void );
extern oC_List(oC_Driver_t)             oC_DriverMan_GetList                   ( void );
extern oC_ErrorCode_t                   oC_DriverMan_AddDriver                 ( oC_Driver_t Driver );
extern oC_ErrorCode_t                   oC_DriverMan_RemoveDriver              ( oC_Driver_t Driver );
extern oC_Driver_t                      oC_DriverMan_GetDriver                 ( const char * FileName );
extern void                             oC_DriverMan_TurnOnAllDrivers          ( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) );
extern void                             oC_DriverMan_TurnOffAllDrivers         ( void );
extern oC_ErrorCode_t                   oC_DriverMan_AddAutoConfiguration      ( const char * Name , oC_Driver_t Driver , const void * Config );
extern oC_ErrorCode_t                   oC_DriverMan_RemoveAutoConfiguration   ( const char * Name );
extern oC_ErrorCode_t                   oC_DriverMan_ReadAutoConfiguration     ( const char * Name , oC_AutoConfiguration_t * outAutoConfiguration );
extern void                             oC_DriverMan_ConfigureAllDrivers       ( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) );
extern oC_ErrorCode_t                   oC_DriverMan_ReadAutoConfigurationList ( oC_List(oC_AutoConfiguration_t*) * outList );
extern oC_ErrorCode_t                   oC_DriverMan_AddInstance               ( oC_Driver_t Driver , oC_Driver_Config_t Config , oC_Driver_Context_t Context );
extern oC_ErrorCode_t                   oC_DriverMan_RemoveInstance            ( oC_Driver_t Driver , oC_Driver_Context_t Context );
extern oC_ErrorCode_t                   oC_DriverMan_SetInstanceAddedCallback  ( oC_DriverInstanceAddedCallback_t Callback, void * UserPointer );
extern oC_ErrorCode_t                   oC_DriverMan_SetInstanceRemovedCallback( oC_DriverInstanceRemovedCallback_t Callback, void * UserPointer );
extern oC_List(oC_DriverInstance_t*)    oC_DriverMan_GetInstanceList           ( void );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_DRIVERS_OC_DRIVERMAN_H_ */
