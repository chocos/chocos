/** ****************************************************************************************************************************************
 *
 * @brief      Header with definitions for PHY handling
 *
 * @file       oc_eth_phy.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_DRIVERS_ETH_OC_ETH_PHY_H_
#define SYSTEM_CORE_INC_DRIVERS_ETH_OC_ETH_PHY_H_

#include <oc_eth.h>

#ifdef oC_ETH_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________


#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores PHY register address
 *
 * The function is for storing PHY register address
 */
//==========================================================================================================================================
typedef enum
{
    oC_ETH_PhyRegister_BasicControlRegister = 0x00 ,    //!< Basic control register
    oC_ETH_PhyRegister_BasicStatusRegister  = 0x01 ,    //!< Basic status register

    oC_ETH_PhyRegister_BCR                  = oC_ETH_PhyRegister_BasicControlRegister , //!< #oC_ETH_PhyRegister_BasicControlRegister
    oC_ETH_PhyRegister_BSR                  = oC_ETH_PhyRegister_BasicStatusRegister ,  //!< #oC_ETH_PhyRegister_BasicStatusRegister

    oC_ETH_PhyRegister_Max                  = 0xFFFF    //!< Maximum value for the register address
} oC_ETH_PhyRegister_t;

//==========================================================================================================================================
/**
 * @brief stores value of the BCR register
 *
 * The type is for encoding Basic Control Register (BCR) value. The `Value` field stores all value, and the other fields are just bits
 * definitions.
 */
//==========================================================================================================================================
typedef union
{
#if defined(LITTLE_ENDIAN)
    struct
    {
        uint32_t        Reserved0_7:8;              //!< Reserved byte.
        uint32_t        DuplexMode:1;               //!< Duplex mode: 0 = Half duplex, 1 = Full duplex
        uint32_t        RestartAutonegotiate:1;     //!< Restart Auto-Negotiate: 0 = Normal operation, 1 = Restart auto-negotiate process
        uint32_t        Isolate:1;                  //!< Isolate: 0 = Normal operation, 1 = electrical isolation of PHY from the RMII
        uint32_t        PowerDown:1;                //!< Power-Down: 0 = Normal operation, 1 = General power down mode
        uint32_t        AutoNegotiation:1;          //!< Auto-Negotiation Enable: 0 = disable auto-negotiate process, 1 = enable auto negotiate process
        uint32_t        SpeedSelect:1;              //!< Speed select: 0 = Slower, 1 = Faster
        uint32_t        Loopback:1;                 //!< Loopback mode: 0 = Normal operation, 1 = loopback mode
        uint32_t        SoftReset:1;                //!< Soft-Reset: 1 = Software reset.
        uint32_t        Reserved16_31:16;           //!< Reserved bytes for future use
    };
#elif defined(BIG_ENDIAN)
#   error The structure is not prepared for the BIG_ENDIAN
#else
#   error ENDIANNES not defined!
#endif
    uint32_t    Value;      //!< Stores value of the register
} oC_ETH_PhyRegister_BCR_t;

//==========================================================================================================================================
/**
 * @brief stores value of the BSR register
 *
 * The type is for encoding Basic Status Register (BCR) value. The `Value` field stores all value, and the other fields are just bits
 * definitions.
 */
//==========================================================================================================================================
typedef union
{
#if defined(LITTLE_ENDIAN)
    struct
    {
        uint32_t        ExtendedCapabilities:1;     //!< Extended capabilities: 0 = Does not support extended capabilities registers, 1 = supports extended capabilities registers
        uint32_t        JabberDetect:1;             //!< Jabber Detect: 0 = No jabber condition detected. 1 = Jabber condition detected.
        uint32_t        LinkStatus:1;               //!< Link Status: 0 = link is down, 1 = link is up
        uint32_t        AutoNegotiateAbility:1;     //!< Auto-Negotiate ability: 0 = Unable to perform auto-negotiation function, 1 = Able to perform auto-negotiation function
        uint32_t        RemoteFault:1;              //!< Remote Fault: 0 = No remote fault, 1 = Remote fault detected
        uint32_t        AutoNegotiateComplete:1;    //!< Auto-Negotiate Complete: 0 = auto negotiate process not completed, 1 = auto negotiate process completed
        uint32_t        Reserved6_7:2;              //!< Reserved fields
        uint32_t        ExtendedStatus:1;           //!< Extended status: 0 = No extended status information in register 15, 1 = Extended status information in register 15
        uint32_t        HalfDuplex100BASET2:1;      //!< 100BASE-T2 Half Duplex: 0 = PHY is not able to perform half duplex 100BASE-T2, 1 = PHY is able to perform half duplex 100BASE-T2
        uint32_t        FullDuplex100BASET2:1;      //!< 100BASE-T2 Full Duplex: 0 = PHY is not able to perform full duplex 100BASE-T2, 1 = PHY is able to perform full duplex 100BASE-T2
        uint32_t        HalfDuplex10BASET:1;        //!< 10BASE-T Half Duplex: 0 = PHY is not able to perform half duplex 10 Mbps, 1 = PHY is able to perform half duplex 10 Mbps
        uint32_t        FullDuplex10BASET:1;        //!< 10BASE-T Full Duplex: 0 = PHY is not able to perform full duplex 10 Mbps, 1 = PHY is able to perform full duplex 10 Mbps
        uint32_t        HalfDuplex100BASETX:1;      //!< 100BASE-TX Half Duplex: 0 = No TX half duplex ability, 1 = Tx with half duplex
        uint32_t        FullDuplex100BASETX:1;      //!< 100BASE-TX Full Duplex: 0 = No TX full duplex ability, 1 = Tx with full duplex
        uint32_t        T4100BASE:1;                //!< 100BASE-T4: 0 = No T4 ability, 1 = T4 able
        uint32_t        Reserved16_31:16;           //!< Reserved bytes for future use
    };
#elif defined(BIG_ENDIAN)
#   error The structure is not prepared for the BIG_ENDIAN
#else
#   error ENDIANNES not defined!
#endif
    uint32_t    Value;      //!< Stores value of the register
} oC_ETH_PhyRegister_BSR_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t   oC_ETH_WritePhyRegister     ( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t      Value );
extern oC_ErrorCode_t   oC_ETH_ReadPhyRegister      ( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t * outValue );
extern oC_ErrorCode_t   oC_ETH_PhyReset             ( oC_ETH_PhyAddress_t PhyAddress );
extern oC_ErrorCode_t   oC_ETH_ReadLinkStatus       ( oC_ETH_PhyAddress_t PhyAddress , oC_Net_LinkStatus_t * outLinkStatus );
extern oC_ErrorCode_t   oC_ETH_SetAutoNegotiation   ( oC_ETH_PhyAddress_t PhyAddress , bool Enabled , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_ETH_SetPhyLoopback       ( oC_ETH_PhyAddress_t PhyAddress , bool Enabled );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif
#endif /* SYSTEM_CORE_INC_DRIVERS_ETH_OC_ETH_PHY_H_ */
