/** ****************************************************************************************************************************************
 *
 * @brief      List of defined Ethernet chips
 *
 * @file       oc_eth_chips.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_DRIVERS_ETH_OC_ETH_CHIPS_H_
#define SYSTEM_CORE_INC_DRIVERS_ETH_OC_ETH_CHIPS_H_

#include <oc_eth.h>

#ifdef oC_ETH_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with chip variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________CHIPS_SECTION______________________________________________________________________________

extern const oC_ETH_PhyChipInfo_t oC_ETH_PhyChipInfo_LAN8742A;

#undef  _________________________________________CHIPS_SECTION______________________________________________________________________________


#endif
#endif /* SYSTEM_CORE_INC_DRIVERS_ETH_OC_ETH_CHIPS_H_ */
