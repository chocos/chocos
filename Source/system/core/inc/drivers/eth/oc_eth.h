/** ****************************************************************************************************************************************
 *
 * @file       oc_eth.h
 * 
 * File based on driver.h Ver 1.1.0
 *
 * @brief      The file with interface for ETH driver
 *
 * @author     Patryk Kubiak - (Created on: 2016-07-28 - 21:37:41)
 *
 * @copyright  Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ETH Ethernet
 * @ingroup Drivers
 * @brief ETH - Driver of the ethernet communication
 *
 ******************************************************************************************************************************************/
#ifndef _OC_ETH_H
#define _OC_ETH_H
#define DRIVER_HEADER
#define DRIVER_NAME                 ETH

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_eth_lld.h>
#include <oc_event.h>
#include <oc_diag.h>

#ifdef oC_ETH_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION_______________________________________________________________________
/**
 * @addtogroup ETH
 * @{
 */

//==========================================================================================================================================
/**
 * @hideinitializer
 * @brief maximum length for the chip name
 *
 * The definition stores maximum length of the chip name
 */
//==========================================================================================================================================
#define oC_ETH_CHIP_NAME_MAX_LENGTH             ( IFNAMSIZ / 2 )

/** @} */
#undef  _________________________________________DEFINITIONS_SECTION_______________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup ETH
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores name of the chip
 *
 * The type is for storing name of the chip. It is just to make it shorter and easier to manipulate.
 */
//==========================================================================================================================================
typedef char oC_ETH_ChipName_t[oC_ETH_CHIP_NAME_MAX_LENGTH];

//==========================================================================================================================================
/**
 * @brief stores address of a PHY
 *
 * The type is for storing address of an external PHY.
 */
//==========================================================================================================================================
typedef oC_ETH_LLD_PHY_Address_t oC_ETH_PhyAddress_t;

//==========================================================================================================================================
/**
 * @brief Informations about the selected chip
 *
 * The type stores definitions about the PHY chip. List of predefined chips you can find in the oc_eth_chips.h file.
 */
//==========================================================================================================================================
typedef struct
{
    oC_ETH_ChipName_t          Name;            //!< Name of the PHY chip
    oC_ETH_LLD_PHY_ChipInfo_t  LLD;             //!< Informations about the chip required by the LLD layer
} oC_ETH_PhyChipInfo_t;

//==========================================================================================================================================
/**
 * @brief ETH driver configuration structure
 *
 * This is the configuration structure for the ETH driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_ETH_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    const oC_ETH_PhyChipInfo_t *       PhyChipInfo;     //!< Pointer to the PHY chip definition (you can find it in the oc_eth_chips.h file)
    oC_ETH_LLD_Config_t                LLD;             //!< Configuration from the LLD layer (More you can find in the #oC_ETH_LLD_Config_t type description)
    oC_ETH_PhyAddress_t                PhyAddress;      //!< Address of the external PHY
    oC_Net_MacAddress_t                MacAddress;      //!< MAC address of the interface
} oC_ETH_Config_t;

//==========================================================================================================================================
/**
 * @brief The ETH context structure.
 *
 * This is the structure with dynamic allocated data for the ETH. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_ETH_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_ETH_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_ETH_Context_t;

/** @} */
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_ErrorCode_t oC_ETH_TurnOn            ( void );
extern oC_ErrorCode_t oC_ETH_TurnOff           ( void );
extern bool           oC_ETH_IsTurnedOn        ( void );
extern oC_ErrorCode_t oC_ETH_Configure         ( const oC_ETH_Config_t * Config , oC_ETH_Context_t * outContext );
extern oC_ErrorCode_t oC_ETH_Unconfigure       ( const oC_ETH_Config_t * Config , oC_ETH_Context_t * outContext );

extern oC_ErrorCode_t oC_ETH_SendFrame         ( oC_ETH_Context_t Context , const oC_Net_Frame_t * Frame , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_ETH_ReceiveFrame      ( oC_ETH_Context_t Context , oC_Net_Frame_t * outFrame , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_ETH_SetWakeOnLanEvent ( oC_ETH_Context_t Context , oC_Event_t WolEvent );
extern oC_ErrorCode_t oC_ETH_Flush             ( oC_ETH_Context_t Context );
extern oC_ErrorCode_t oC_ETH_SetLoopback       ( oC_ETH_Context_t Context , oC_Net_Layer_t Layer , bool Enabled );
extern oC_ErrorCode_t oC_ETH_PerformDiagnostics( oC_ETH_Context_t Context , oC_Diag_t * Diags , uint32_t * NumberOfDiags );
extern oC_ErrorCode_t oC_ETH_ReadNetInfo       ( oC_ETH_Context_t Context , oC_Net_Info_t * outInfo );
extern bool           oC_ETH_IsContextCorrect  ( oC_ETH_Context_t Context );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* oC_ETH_LLD_AVAILABLE */
#endif /* _OC_ETH_H */
