/** ****************************************************************************************************************************************
 *
 * @file       oc_neunet.h
 * 
 * File based on driver.h Ver 1.1.1
 *
 * @brief      The file with interface for NEUNET driver
 *
 * @author     Patryk Kubiak - (Created on: 2023-05-09 - 12:37:21)
 *
 * @copyright  Copyright (C) 2023 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup NEUNET Neural Network driver
 * @ingroup Drivers
 * @brief NEUNET - Driver for Neural Network
 * 
 ******************************************************************************************************************************************/
#ifndef _OC_NEUNET_H
#define _OC_NEUNET_H
#define DRIVER_HEADER
#define DRIVER_NAME                 NEUNET

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief NEUNET driver configuration structure
 *
 * This is the configuration structure for the NEUNET driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_NEUNET_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{

} oC_NEUNET_Config_t;

//==========================================================================================================================================
/**
 * @brief The NEUNET context structure.
 *
 * This is the structure with dynamic allocated data for the NEUNET. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_NEUNET_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_NEUNET_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_NEUNET_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_ErrorCode_t oC_NEUNET_TurnOn            ( void );
extern oC_ErrorCode_t oC_NEUNET_TurnOff           ( void );
extern bool           oC_NEUNET_IsTurnedOn        ( void );
extern oC_ErrorCode_t oC_NEUNET_Configure         ( const oC_NEUNET_Config_t * Config , oC_NEUNET_Context_t * outContext );
extern oC_ErrorCode_t oC_NEUNET_Unconfigure       ( const oC_NEUNET_Config_t * Config , oC_NEUNET_Context_t * outContext );
extern oC_ErrorCode_t oC_NEUNET_Read              ( oC_NEUNET_Context_t Context , char * outBuffer , uint32_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_NEUNET_Write             ( oC_NEUNET_Context_t Context , const char * Buffer , uint32_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_NEUNET_Ioctl             ( oC_NEUNET_Context_t Context , oC_Ioctl_Command_t Command , void * Data );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* _OC_NEUNET_H */
