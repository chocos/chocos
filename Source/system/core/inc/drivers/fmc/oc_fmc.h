/** ****************************************************************************************************************************************
 *
 * @file       oc_fmc.h
 * 
 * File based on driver.h Ver 1.1.0
 *
 * @brief      The file with interface for FMC driver
 *
 * @author     Patryk Kubiak - (Created on: 2016-04-02 - 16:12:02)
 *
 * @copyright  Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup FMC Flexible Memory Controller 
 * @ingroup Drivers
 * @brief FMC - managing and configuring external memory.
 *
 * The driver for managing and configuring external memory.
 *
 ******************************************************************************************************************************************/
#ifndef _OC_FMC_H
#define _OC_FMC_H
#define DRIVER_HEADER
#define DRIVER_NAME                 FMC

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_fmc_lld.h>
#include <oc_frequency.h>
#include <oc_memory.h>

#ifdef oC_FMC_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores memory protection
 *
 * @ingroup FMC
 *
 * The type is for storing allowed operations for the memory.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_Protection_Default       = oC_FMC_LLD_Protection_Default      , //!< Default protection for the memory type
    oC_FMC_Protection_AllowWrite    = oC_FMC_LLD_Protection_AllowWrite   , //!< Write operations are performed
    oC_FMC_Protection_AllowRead     = oC_FMC_LLD_Protection_AllowRead    , //!< Read operations are performed
    oC_FMC_Protection_AllowExecute  = oC_FMC_LLD_Protection_AllowExecute , //!< Execution operations are performed
} oC_FMC_Protection_t;

//==========================================================================================================================================
/**
 * @brief Heap should be used or not
 *
 * @ingroup FMC
 *
 * The type for storing heap usage - if it should be used or not
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_HeapUsage_UseAsHeapIfPossible = 0 ,          //!< Default option - if usage of heap is possible, then use it
    oC_FMC_HeapUsage_DontUseAsHeap       = 1 ,          //!< Never use the memory as heap
} oC_FMC_HeapUsage_t;

//==========================================================================================================================================
/**
 * @brief commands for SDRAM
 *
 * @ingroup FMC
 *
 * The type stores command that can be send to the SDRAM. It can be useful in the SDRAM initialization procedure. Look at the #oC_FMC_SendSDRAMCommand
 * function for more informations.
 */
//==========================================================================================================================================
typedef enum
{
    oC_FMC_SDRAM_Command_EnableClock      = oC_FMC_LLD_SDRAM_Command_EnableClock ,     //!< Enables the clock (provides a stable CLK signal)
    oC_FMC_SDRAM_Command_Inhibit          = oC_FMC_LLD_SDRAM_Command_Inhibit ,         //!< The COMMAND INHIBIT function prevents new commands from being executed by the device, regardless of whether the CLK signal is enabled. The device is effectively deselected. Operations already in progress are not affected.
    oC_FMC_SDRAM_Command_Nop              = oC_FMC_LLD_SDRAM_Command_Nop ,             //!< Simple no operation command
    oC_FMC_SDRAM_Command_LoadModeRegister = oC_FMC_LLD_SDRAM_Command_LoadModeRegister ,//!< Configuration command for loading modes registers
    oC_FMC_SDRAM_Command_Active           = oC_FMC_LLD_SDRAM_Command_Active ,          //!< The command used for activate a row in a bank for a subsequent access
    oC_FMC_SDRAM_Command_Read             = oC_FMC_LLD_SDRAM_Command_Read ,            //!< Read a burst to an active row
    oC_FMC_SDRAM_Command_Write            = oC_FMC_LLD_SDRAM_Command_Write ,           //!< Write a burst to an active row
    oC_FMC_SDRAM_Command_Precharge        = oC_FMC_LLD_SDRAM_Command_Precharge ,       //!< Deactivate a row in a bank (or in all banks)
    oC_FMC_SDRAM_Command_BurstTerminate   = oC_FMC_LLD_SDRAM_Command_BurstTerminate ,  //!< Terminates a burst (either fixed length or continuous page burst)
    oC_FMC_SDRAM_Command_AutoRefresh      = oC_FMC_LLD_SDRAM_Command_AutoRefresh ,     //!< Refreshes a SDRAM. It could be called each time, when the refresh is required
    oC_FMC_SDRAM_Command_SelfRefresh      = oC_FMC_LLD_SDRAM_Command_SelfRefresh ,     //!< Refreshes a ram, when the system is powered-down (it does not require a CLOCK)
} oC_FMC_SDRAM_Command_t;

//==========================================================================================================================================
/**
 * @brief stores data for SDRAM commands
 *
 * @ingroup FMC
 *
 * Stores data for the SDRAM commands. For more look at the type #oC_FMC_LLD_SDRAM_CommandData_t
 */
//==========================================================================================================================================
typedef oC_FMC_LLD_SDRAM_CommandData_t oC_FMC_SDRAM_CommandData_t;

//==========================================================================================================================================
/**
 * @brief The FMC context structure.
 *
 * @ingroup FMC
 *
 * This is the structure with dynamic allocated data for the FMC. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_FMC_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_FMC_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_FMC_Context_t;

//==========================================================================================================================================
/**
 * @brief function for initialization of the chip
 *
 * The type stores the pointer for the chip initialization function. The function will be called during memory configuration.
 *
 * @param Context           FMC context pointer
 * @param Timeout           Pointer to the maximum time for the initialization procedure (should be updated about the time that left after the initialization)
 *
 * @return code of error
 */
//==========================================================================================================================================
typedef oC_ErrorCode_t (*oC_FMC_InitializationFunction_t)( const void * Config , oC_FMC_Context_t Context , oC_Time_t * Timeout );

//==========================================================================================================================================
/**
 * @brief stores informations about chip
 *
 * The type is for storing informations about memory chips.
 */
//==========================================================================================================================================
typedef struct
{
    oC_FMC_LLD_ChipParameters_t         ChipParameters;             //!< Chip parameters
    oC_FMC_InitializationFunction_t     InitializationFunction;     //!< Function to call for initialize the chip. It can be set to NULL if initialization is not required
} oC_FMC_ChipInfo_t;

//==========================================================================================================================================
/**
 * @brief FMC driver configuration structure
 *
 * @ingroup FMC
 *
 * This is the configuration structure for the FMC driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_FMC_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct oC_FMC_Config_t
{
    oC_FMC_HeapUsage_t              HeapUsage;                      //!< Use or not the configured memory as heap
    const oC_FMC_ChipInfo_t*        ChipInfo;                       //!< Pointer to the informations about the selected chip. All defined chips you will find in the #oc_fmc_chips.h file.
    oC_Time_t                       MaximumTimeForConfiguration;    //!< Maximum time for the memory configuration
    union
    {
        oC_FMC_LLD_SDRAM_Config_t       SDRAM;                      //!< SDRAM configuration structure
        oC_FMC_LLD_PSRAM_Config_t       PSRAM;                      //!< PSRAM configuration structure
        oC_FMC_LLD_NANDFlash_Config_t   NANDFlash;                  //!< NAND Flash configuration structure
        oC_FMC_LLD_NORFlash_Config_t    NORFlash;                   //!< NOR Flash configuration structure
    };
} oC_FMC_Config_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_ErrorCode_t oC_FMC_TurnOn                 ( void );
extern oC_ErrorCode_t oC_FMC_TurnOff                ( void );
extern bool           oC_FMC_IsTurnedOn             ( void );
extern oC_ErrorCode_t oC_FMC_Configure              ( const oC_FMC_Config_t * Config , oC_FMC_Context_t * outContext );
extern oC_ErrorCode_t oC_FMC_Unconfigure            ( const oC_FMC_Config_t * Config , oC_FMC_Context_t * outContext );
extern oC_ErrorCode_t oC_FMC_Read                   ( oC_FMC_Context_t Context , char * outBuffer , oC_MemorySize_t Size , oC_IoFlags_t IoFlags );
extern oC_ErrorCode_t oC_FMC_Write                  ( oC_FMC_Context_t Context , const char * Buffer , oC_MemorySize_t Size , oC_IoFlags_t IoFlags );
extern oC_ErrorCode_t oC_FMC_Ioctl                  ( oC_FMC_Context_t Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t oC_FMC_SDRAM_SendCommand      ( oC_FMC_Context_t Context , oC_Time_t * Timeout , oC_FMC_SDRAM_Command_t Command , const oC_FMC_SDRAM_CommandData_t * Data );
extern oC_ErrorCode_t oC_FMC_ReadDirectAddress      ( oC_FMC_Context_t Context , void ** outAddress , oC_MemorySize_t * outMemorySize );
extern oC_ErrorCode_t oC_FMC_FinishInitialization   ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif
#endif /* _OC_FMC_H */
