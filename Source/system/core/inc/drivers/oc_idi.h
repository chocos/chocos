/** ****************************************************************************************************************************************
 *
 * @brief      IDI module interface
 * 
 * @file          oc_idi.h
 *
 * @author     Patryk Kubiak - (Created on: 02.05.2017 19:21:07) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup IDI IDI - Input Driver Interface
 * @ingroup Drivers
 * @brief Module for handling IDI events
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_GUI_OC_IDI_H_
#define SYSTEM_CORE_INC_GUI_OC_IDI_H_

#include <oc_pixel.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup IDI
 * @{
 */

typedef enum
{
    oC_IDI_EventId_MouseEvent               = 0x80000000 ,
    oC_IDI_EventId_KeyboardEvent            = 0x40000000 ,
    oC_IDI_EventId_TouchScreenEvent         = 0x20000000 ,
    oC_IDI_EventId_AnyController            = 0xFFFF0000 ,
    oC_IDI_EventId_AnyMove                  = 0x00000000 ,
    oC_IDI_EventId_AllMoves                 = 0x0000FFFF ,

    /* Generic events */
    oC_IDI_EventId_None                     = 0x00000000 ,
    oC_IDI_EventId_ClickWithoutRelease       ,
    oC_IDI_EventId_Click                     ,
    oC_IDI_EventId_DoubleClick               ,
    oC_IDI_EventId_TripleClick               ,
    oC_IDI_EventId_MoveUp                    ,
    oC_IDI_EventId_MoveDown                  ,
    oC_IDI_EventId_MoveLeft                  ,
    oC_IDI_EventId_MoveRight                 ,
    oC_IDI_EventId_MoveUpLeft                ,
    oC_IDI_EventId_MoveUpRight               ,
    oC_IDI_EventId_MoveDownLeft              ,
    oC_IDI_EventId_MoveDownRight             ,
    oC_IDI_EventId_RotateClockwise           ,
    oC_IDI_EventId_RotateCounterClockwise    ,
    oC_IDI_EventId_ZoomIn                    ,
    oC_IDI_EventId_ZoomOut                   ,
    oC_IDI_EventId_RightClick                ,
    oC_IDI_EventId_MiddleClick               ,
    oC_IDI_EventId_MoveCursor                ,

    /* Must be at the end of generic events */
    oC_IDI_EventId_NumberOfDefinedEvents    ,
    oC_IDI_EventId_IndexMask                = 0x0000FFFF ,


    /* Mouse events */
    oC_IDI_EventId_Mouse_Click                          = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_Click                  ,
    oC_IDI_EventId_Mouse_DoubleClick                    = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_DoubleClick            ,
    oC_IDI_EventId_Mouse_MoveUp                         = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_MoveUp                 ,
    oC_IDI_EventId_Mouse_MoveDown                       = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_MoveDown               ,
    oC_IDI_EventId_Mouse_MoveLeft                       = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_MoveLeft               ,
    oC_IDI_EventId_Mouse_MoveRight                      = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_MoveRight              ,
    oC_IDI_EventId_Mouse_RotateClockwise                = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_RotateClockwise        ,
    oC_IDI_EventId_Mouse_RotateCounterClockwise         = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_RotateCounterClockwise ,
    oC_IDI_EventId_Mouse_ZoomIn                         = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_ZoomIn                 ,
    oC_IDI_EventId_Mouse_ZoomOut                        = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_ZoomOut                ,
    oC_IDI_EventId_Mouse_RightClick                     = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_RightClick             ,
    oC_IDI_EventId_Mouse_MiddleClick                    = oC_IDI_EventId_MouseEvent | oC_IDI_EventId_MiddleClick            ,

    /* Touch-Screen events */
    oC_IDI_EventId_TouchScreen_Click                    = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_Click                  ,
    oC_IDI_EventId_TouchScreen_ClickWithoutRelease      = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_ClickWithoutRelease    ,
    oC_IDI_EventId_TouchScreen_DoubleClick              = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_DoubleClick            ,
    oC_IDI_EventId_TouchScreen_MoveUp                   = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_MoveUp                 ,
    oC_IDI_EventId_TouchScreen_MoveDown                 = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_MoveDown               ,
    oC_IDI_EventId_TouchScreen_MoveLeft                 = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_MoveLeft               ,
    oC_IDI_EventId_TouchScreen_MoveRight                = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_MoveRight              ,
    oC_IDI_EventId_TouchScreen_RotateClockwise          = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_RotateClockwise        ,
    oC_IDI_EventId_TouchScreen_RotateCounterClockwise   = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_RotateCounterClockwise ,
    oC_IDI_EventId_TouchScreen_ZoomIn                   = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_ZoomIn                 ,
    oC_IDI_EventId_TouchScreen_ZoomOut                  = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_ZoomOut                ,
    oC_IDI_EventId_TouchScreen_RightClick               = oC_IDI_EventId_TouchScreenEvent | oC_IDI_EventId_RightClick             ,

    oC_IDI_EventId_Keyboard_PrintedKeyPressed           = oC_IDI_EventId_KeyboardEvent | 0x00000001 ,
    oC_IDI_EventId_Keyboard_SpecialKeyPressed           = oC_IDI_EventId_KeyboardEvent | 0x00000002 ,
    oC_IDI_EventId_Keyboard_CapsLockTurnedOn            = oC_IDI_EventId_KeyboardEvent | 0x00000003 ,
    oC_IDI_EventId_Keyboard_CapsLockTurnedOff           = oC_IDI_EventId_KeyboardEvent | 0x00000004 ,
    oC_IDI_EventId_Keyboard_NumLockTurnedOn             = oC_IDI_EventId_KeyboardEvent | 0x00000005 ,
    oC_IDI_EventId_Keyboard_NumLockTurnedOff            = oC_IDI_EventId_KeyboardEvent | 0x00000006 ,
    oC_IDI_EventId_Keyboard_ScrollLockTurnedOn          = oC_IDI_EventId_KeyboardEvent | 0x00000007 ,
    oC_IDI_EventId_Keyboard_ScrollLockTurnedOff         = oC_IDI_EventId_KeyboardEvent | 0x00000008 ,

} oC_IDI_EventId_t;

typedef enum
{
    oC_IDI_Key_None                 = 0x00000000 ,
    oC_IDI_Key_MaxCharacter         = 0x000000FF ,

    oC_IDI_Key_StandardNotPrintedKey    = 0x01000000 ,
    oC_IDI_Key_ESC                      ,
    oC_IDI_Key_F1                       ,
    oC_IDI_Key_F2                       ,
    oC_IDI_Key_F3                       ,
    oC_IDI_Key_F4                       ,
    oC_IDI_Key_F5                       ,
    oC_IDI_Key_F6                       ,
    oC_IDI_Key_F7                       ,
    oC_IDI_Key_F8                       ,
    oC_IDI_Key_F9                       ,
    oC_IDI_Key_F10                      ,
    oC_IDI_Key_F11                      ,
    oC_IDI_Key_F12                      ,
    oC_IDI_Key_F13                      ,
    oC_IDI_Key_F14                      ,
    oC_IDI_Key_F15                      ,
    oC_IDI_Key_F16                      ,
    oC_IDI_Key_F17                      ,
    oC_IDI_Key_F18                      ,
    oC_IDI_Key_F19                      ,
    oC_IDI_Key_F20                      ,
    oC_IDI_Key_F21                      ,
    oC_IDI_Key_F22                      ,
    oC_IDI_Key_F23                      ,
    oC_IDI_Key_F24                      ,
    oC_IDI_Key_LeftShift                ,
    oC_IDI_Key_RightShift               ,
    oC_IDI_Key_RightCTRL                ,
    oC_IDI_Key_LeftALT                  ,
    oC_IDI_Key_RightALT                 ,
    oC_IDI_Key_TAB                      ,
    oC_IDI_Key_PrintScreen              ,
    oC_IDI_Key_ScrollLock               ,
    oC_IDI_Key_PauseBreak               ,
    oC_IDI_Key_Insert                   ,
    oC_IDI_Key_Delete                   ,
    oC_IDI_Key_PageUp                   ,
    oC_IDI_Key_PageDown                 ,
    oC_IDI_Key_Home                     ,
    oC_IDI_Key_End                      ,
    oC_IDI_Key_NumLock                  ,
    oC_IDI_Key_LeftEnter                ,
    oC_IDI_Key_RightEnter               ,
    oC_IDI_Key_Backspace                ,
    oC_IDI_Key_Numeric0                 ,
    oC_IDI_Key_Numeric1                 ,
    oC_IDI_Key_Numeric2                 ,
    oC_IDI_Key_Numeric3                 ,
    oC_IDI_Key_Numeric4                 ,
    oC_IDI_Key_Numeric5                 ,
    oC_IDI_Key_Numeric6                 ,
    oC_IDI_Key_Numeric7                 ,
    oC_IDI_Key_Numeric8                 ,
    oC_IDI_Key_Numeric9                 ,
    oC_IDI_Key_NumericPlus              ,
    oC_IDI_Key_NumericSlash             ,
    oC_IDI_Key_NumericStar              ,
    oC_IDI_Key_NumericMinus             ,
    oC_IDI_Key_ArrowUp                  ,
    oC_IDI_Key_ArrowDown                ,
    oC_IDI_Key_ArrowLeft                ,
    oC_IDI_Key_ArrowRight               ,
    oC_IDI_Key_Windows                  ,
    oC_IDI_Key_Space                    ,

    oC_IDI_Key_SpecialNotPrintedKey     = 0x02000000 ,
    oC_IDI_Key_EcoMode                  ,
    oC_IDI_Key_Sleep                    ,
    oC_IDI_Key_FlyMode                  ,
    oC_IDI_Key_Camera                   ,
    oC_IDI_Key_VolumePlus               ,
    oC_IDI_Key_VolumeMinus              ,
    oC_IDI_Key_BrightnessUp             ,
    oC_IDI_Key_BrightnessDown           ,
    oC_IDI_Key_Mute                     ,
    oC_IDI_Key_WiFi                     ,
    oC_IDI_Key_Bluetooth                ,
    oC_IDI_Key_TouchPad                 ,
    oC_IDI_Key_Screen                   ,
    oC_IDI_Key_Power                    ,
    oC_IDI_Key_Hibernate                ,
    oC_IDI_Key_MakeLifeEasy             ,

    oC_IDI_Key_DeviceSpecificNotPrintedKey      = 0x04000000 ,
} oC_IDI_Key_t;

typedef int oC_IDI_RotateAngle_t;

#define oC_IDI_MAX_POINTS           10

typedef struct
{
    oC_IDI_EventId_t            EventId;
    uint16_t                    NumberOfPoints;
    oC_Pixel_ResolutionInt_t    DeltaX[oC_IDI_MAX_POINTS];
    oC_Pixel_ResolutionInt_t    DeltaY[oC_IDI_MAX_POINTS];
    oC_Pixel_Position_t         Position[oC_IDI_MAX_POINTS];
    oC_Pixel_ResolutionUInt_t   Weight[oC_IDI_MAX_POINTS];
    oC_IDI_RotateAngle_t        RotateAngle;
    char                        PrintedKey;
    oC_IDI_Key_t                Key;
    oC_Timestamp_t              Timestamp;
} oC_IDI_Event_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
/** @} */


#endif /* SYSTEM_CORE_INC_GUI_OC_IDI_H_ */
