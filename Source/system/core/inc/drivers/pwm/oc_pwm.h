/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the PWM driver
 *
 * @file       oc_pwm.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup PWM Pulse Width Modulation
 * @ingroup Drivers
 * @brief PWM - Driver for handling PWM signals
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_DRIVERS_PWM_OC_PWM_H_
#define SYSTEM_CORE_INC_DRIVERS_PWM_OC_PWM_H_

#define DRIVER_HEADER
#define DRIVER_NAME                 PWM

#include <oc_driver.h>
#include <oc_gpio.h>
#include <oc_frequency.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    oC_PWM_ActiveState_Low  = oC_GPIO_PinsState_AllLow,
    oC_PWM_ActiveState_High = oC_GPIO_PinsState_AllHigh
} oC_PWM_ActiveState_t;

//==========================================================================================================================================
/**
 * @brief PWM driver configuration structure
 *
 * This is the configuration structure for the PWM driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_PWM_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pins_t               Pin;
    oC_PWM_ActiveState_t    ActiveState;
    uint64_t                MaximumValue;
    oC_Frequency_t          TickFrequency;
    oC_Frequency_t          PermissibleDiffernece;
} oC_PWM_Config_t;

//==========================================================================================================================================
/**
 * @brief The PWM context structure.
 *
 * This is the structure with dynamic allocated data for the PWM. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_PWM_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_PWM_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_PWM_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t oC_PWM_Configure         ( const oC_PWM_Config_t * Config , oC_PWM_Context_t * outContext );
extern oC_ErrorCode_t oC_PWM_Unconfigure       ( const oC_PWM_Config_t * Config , oC_PWM_Context_t * outContext );
extern oC_ErrorCode_t oC_PWM_SetDuty           ( oC_PWM_Context_t Context , float PercentDuty );
extern oC_ErrorCode_t oC_PWM_ReadDuty          ( oC_PWM_Context_t Context , float * outPercentDuty );
extern oC_ErrorCode_t oC_PWM_SetValue          ( oC_PWM_Context_t Context , uint64_t Value );
extern oC_ErrorCode_t oC_PWM_ReadValue         ( oC_PWM_Context_t Context , uint64_t * outValue );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* SYSTEM_CORE_INC_DRIVERS_PWM_OC_PWM_H_ */
