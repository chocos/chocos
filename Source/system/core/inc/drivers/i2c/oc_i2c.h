/** ****************************************************************************************************************************************
 *
 * @file       oc_i2c.h
 * 
 * File based on driver.h Ver 1.1.1
 *
 * @brief      The file with interface for I2C driver
 *
 * @author     Patryk Kubiak - (Created on: 2017-02-17 - 19:39:55)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup I2C    Inter-Integrated Circuit
 * @ingroup Drivers
 * @brief I2C - managing and configuring I2C communication.
 *
 ******************************************************************************************************************************************/
#ifndef _OC_I2C_H
#define _OC_I2C_H
#define DRIVER_HEADER
#define DRIVER_NAME                 I2C

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_i2c_lld.h>
#include <oc_baudrate.h>

#ifdef oC_I2C_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup I2C
 * @{
 */

//==========================================================================================================================================
/**
 * @brief Stores speed mode
 *
 * The type is for storing speed mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_SpeedMode_Normal     = oC_I2C_LLD_SpeedMode_Normal       , //!< First version of I2C from 1982, with the maximum speed of 100 kbps
    oC_I2C_SpeedMode_Fast       = oC_I2C_LLD_SpeedMode_Fast         , //!< Version 1 of I2C - defined in 1992 as Fast-Mode (Fm) with the maximum speed of 400 kbps
    oC_I2C_SpeedMode_FastPlus   = oC_I2C_LLD_SpeedMode_FastPlus     , //!< Defined in 2007 with version 3, with the maximum speed of 1 Mbps
    oC_I2C_SpeedMode_HighSpeed  = oC_I2C_LLD_SpeedMode_HighSpeed    , //!< Introduced with version 2 as High-Speed Mode (Hs) with the maximum speed of 3.4 Mbps
    oC_I2C_SpeedMode_UltraFast  = oC_I2C_LLD_SpeedMode_UltraFast    , //!< Ultra Fast Mode (UFm) - defined in 2012 with version 4 with the maximum speed of 5 Mbps
} oC_I2C_SpeedMode_t;

//==========================================================================================================================================
/**
 * @brief type for storing I2C addresses
 */
//==========================================================================================================================================
typedef oC_I2C_LLD_Address_t oC_I2C_Address_t;

//==========================================================================================================================================
/**
 * @brief Type for storing I2C Mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_Mode_Master ,//!< Device works as master
    oC_I2C_Mode_Slave   //!< Device works as slave
} oC_I2C_Mode_t;

//==========================================================================================================================================
/**
 * @brief Stores addressing mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_AddressMode_7Bits    = oC_I2C_LLD_AddressMode_7Bits ,    //!< Addresses are 7 bits length
    oC_I2C_AddressMode_10Bits   = oC_I2C_LLD_AddressMode_10Bits ,   //!< Addresses are 10 bits length
} oC_I2C_AddressMode_t;

//==========================================================================================================================================
/**
 * @brief stores IOCTL commands definitions
 */
//==========================================================================================================================================
typedef enum
{
    oC_I2C_IoctlCommand_SetRemoteAddress        = oC_Ioctl_MakeCommand( oC_Ioctl_Group_DriverSpecific, 0x1, oC_I2C_Address_t ), //!< Sets remote I2C address
    oC_I2C_IoctlCommand_SetLocalAddress         = oC_Ioctl_MakeCommand( oC_Ioctl_Group_DriverSpecific, 0x2, oC_I2C_Address_t ), //!< Sets local I2C address
    oC_I2C_IoctlCommand_ReadRemoteAddress       = oC_Ioctl_MakeCommand( oC_Ioctl_Group_DriverSpecific, 0x3, oC_I2C_Address_t ), //!< Reads remote I2C address
    oC_I2C_IoctlCommand_ReadLocalAddress        = oC_Ioctl_MakeCommand( oC_Ioctl_Group_DriverSpecific, 0x4, oC_I2C_Address_t ), //!< Reads local I2C address
} oC_I2C_IoctlCommand_t;

//==========================================================================================================================================
/**
 * @brief I2C driver configuration structure
 *
 * This is the configuration structure for the I2C driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_I2C_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    oC_I2C_Mode_t               Mode;                   //!< Mode of the local device - master or slave
    oC_I2C_SpeedMode_t          SpeedMode;              //!< Maximum speed of transmission
    oC_I2C_AddressMode_t        AddressMode;            //!< Mode of the address

    struct
    {
        oC_I2C_Address_t        DefaultSlaveAddress;    //!< Default remote address - the address that should be used for I2C communication as default
    } Master;

    struct
    {
        oC_I2C_Address_t        LocalAddress;                   //!< Local address of the device
        bool                    GeneralCallDetectionEnabled;    //!< Set it to 'true' if general call address detection should be enabled
        bool                    ClockStretchingEnabled;         //!< Set it to 'true' if clock stretching should be enabled
    } Slave;


    struct
    {
        oC_Pin_t                SDA;    //!< Serial Data Line Pin
        oC_Pin_t                SCL;    //!< Serial Clock Line
    } Pins;

    struct
    {
        oC_I2C_Channel_t        Channel;        //!< LLD channel to use
        oC_Time_t               DataHoldTime;   //!< delay between SCL falling edge and SDA edge
        oC_Time_t               DataSetupTime;  //!< delay between SDA edge and SCL rising edge
        oC_Frequency_t          ClockFrequency; //!< SCL frequency
        oC_Time_t               ClockLowTime;
        oC_Time_t               ClockHighTime;
    } Advanced;
} oC_I2C_Config_t;

typedef uint8_t oC_I2C_Register_t;

//==========================================================================================================================================
/**
 * @brief The I2C context structure.
 *
 * This is the structure with dynamic allocated data for the I2C. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_I2C_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_I2C_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_I2C_Context_t;

//! @}
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/**
 * @addtogroup I2C
 * @{
 */

extern oC_ErrorCode_t oC_I2C_TurnOn            ( void );
extern oC_ErrorCode_t oC_I2C_TurnOff           ( void );
extern bool           oC_I2C_IsTurnedOn        ( void );
extern bool           oC_I2C_IsContextCorrect  ( oC_I2C_Context_t Context );
extern oC_ErrorCode_t oC_I2C_Configure         ( const oC_I2C_Config_t * Config , oC_I2C_Context_t * outContext );
extern oC_ErrorCode_t oC_I2C_Unconfigure       ( const oC_I2C_Config_t * Config , oC_I2C_Context_t * outContext );
extern oC_ErrorCode_t oC_I2C_Read              ( oC_I2C_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_Write             ( oC_I2C_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_Ioctl             ( oC_I2C_Context_t Context , oC_I2C_IoctlCommand_t Command , void * Data );
extern oC_ErrorCode_t oC_I2C_Transmit          ( oC_I2C_Context_t Context , oC_I2C_Address_t RemoteAddress, const void * Data, oC_MemorySize_t   Size, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_Receive           ( oC_I2C_Context_t Context , oC_I2C_Address_t RemoteAddress, void *    outData, oC_MemorySize_t * Size, oC_Time_t Timeout );

extern oC_ErrorCode_t oC_I2C_WriteRegister     ( oC_I2C_Context_t Context, oC_I2C_Address_t RemoteAddress, oC_I2C_Register_t Register, const void * Data, oC_MemorySize_t   Size, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_ReadRegister      ( oC_I2C_Context_t Context, oC_I2C_Address_t RemoteAddress, oC_I2C_Register_t Register, void *    outData, oC_MemorySize_t * Size, oC_Time_t Timeout );

extern oC_ErrorCode_t oC_I2C_TakeBusOwnership   ( oC_I2C_Context_t Context , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_ReleaseBusOwnership( oC_I2C_Context_t Context );

extern oC_ErrorCode_t oC_I2C_SetSlaveAddress    ( oC_I2C_Context_t Context , oC_I2C_Address_t SlaveAddress );
extern oC_ErrorCode_t oC_I2C_ReadSlaveAddress   ( oC_I2C_Context_t Context , oC_I2C_Address_t * outSlaveAddress );
extern oC_ErrorCode_t oC_I2C_ReadChannel        ( oC_I2C_Context_t Context , oC_I2C_Channel_t * outChannel );

extern oC_ErrorCode_t oC_I2C_RawStartAsMaster   ( oC_I2C_Context_t Context , bool StartForRead, oC_MemorySize_t Size );
extern oC_ErrorCode_t oC_I2C_RawStopAsMaster    ( oC_I2C_Context_t Context , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_RawWriteAsMaster   ( oC_I2C_Context_t Context , const void * Data , oC_MemorySize_t   Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_RawReadAsMaster    ( oC_I2C_Context_t Context , void *    outData , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_RawWriteAsSlave    ( oC_I2C_Context_t Context, const void * Data  , oC_MemorySize_t   Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_I2C_RawReadAsSlave     ( oC_I2C_Context_t Context ,    void * outData , oC_MemorySize_t * Size , oC_Time_t Timeout );

/** @} */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* oC_I2C_LLD_AVAILABLE */
#endif /* _OC_I2C_H */
