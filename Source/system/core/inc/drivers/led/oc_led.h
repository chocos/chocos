/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the LED driver
 *
 * @file       oc_led.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup LED LED - Light Emitting Diode
 * @ingroup Drivers
 * @brief Driver for handling LEDs
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_DRIVERS_LED_OC_LED_H_
#define SYSTEM_CORE_INC_DRIVERS_LED_OC_LED_H_

#define DRIVER_HEADER
#define DRIVER_NAME                 LED

#include <oc_driver.h>
#include <oc_gpio.h>
#include <oc_color.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief      The enum with indexes of LEDs
 */
//==========================================================================================================================================
typedef enum
{
    oC_LED_Index_Red    = 0 ,               /**< The index of the red LED */
    oC_LED_Index_Single = 0 ,               /**< The index of the single LED (if the driver is used for a single LED) */
    oC_LED_Index_Green  = 1 ,               /**< The index of the green LED */
    oC_LED_Index_Blue   = 2 ,               /**< The index of the blue LED */

    oC_LED_Index_NumberOfElements           /**< The number of elements in the enum */
} oC_LED_Index_t;

//==========================================================================================================================================
/**
 * @brief      The enum with modes of LEDs
 */
//==========================================================================================================================================
typedef enum
{
    oC_LED_Mode_Red             = (1<<oC_LED_Index_Red),                    /**< The mode of the red LED */
    oC_LED_Mode_Green           = (1<<oC_LED_Index_Green),                  /**< The mode of the green LED */
    oC_LED_Mode_Blue            = (1<<oC_LED_Index_Blue),                   /**< The mode of the blue LED */
    oC_LED_Mode_UsePWM          = (1<<3),                                   /**< The mode of the LED driver, which uses PWM */
    oC_LED_Mode_RedActiveLow    = (1<<4),                                   /**< The mode of the red LED, which is active low */
    oC_LED_Mode_GreenActiveLow  = (1<<5),                                   /**< The mode of the green LED, which is active low */
    oC_LED_Mode_BlueActiveLow   = (1<<6),                                   /**< The mode of the blue LED, which is active low */
    oC_LED_Mode_Single          = oC_LED_Mode_Red,                          /**< The mode of the single LED */
    oC_LED_Mode_SingleActiveLow = oC_LED_Mode_RedActiveLow,                 /**< The mode of the single LED, which is active low */
    oC_LED_Mode_RGB             = oC_LED_Mode_Red | oC_LED_Mode_Green | oC_LED_Mode_Blue ,  /**< The mode of the RGB LED */
    oC_LED_Mode_RG              = oC_LED_Mode_Red | oC_LED_Mode_Green ,     /**< The mode of the RG LED */
    oC_LED_Mode_RB              = oC_LED_Mode_Red | oC_LED_Mode_Blue ,      /**< The mode of the RB LED */
    oC_LED_Mode_BG              = oC_LED_Mode_Blue | oC_LED_Mode_Green ,    /**< The mode of the BG LED */
} oC_LED_Mode_t;

//==========================================================================================================================================
/**
 * @brief LED driver configuration structure
 *
 * This is the configuration structure for the LED driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_LED_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pins_t       Pins[oC_LED_Index_NumberOfElements];           /**< The array with pins for the LEDs */
    oC_LED_Mode_t   Mode;                                          /**< The mode of the LED driver */                   
} oC_LED_Config_t;

//==========================================================================================================================================
/**
 * @brief The LED context structure.
 *
 * This is the structure with dynamic allocated data for the LED. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_LED_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_LED_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_LED_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t oC_LED_Configure         ( const oC_LED_Config_t * Config , oC_LED_Context_t * outContext );
extern oC_ErrorCode_t oC_LED_Unconfigure       ( const oC_LED_Config_t * Config , oC_LED_Context_t * outContext );
extern oC_ErrorCode_t oC_LED_Write             ( oC_LED_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_LED_Ioctl             ( oC_LED_Context_t Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t oC_LED_SetColor          ( oC_LED_Context_t Context , oC_Color_t Color );
extern oC_ErrorCode_t oC_LED_SetLight          ( oC_LED_Context_t Context , oC_LED_Index_t LedIndex , uint8_t Light );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* SYSTEM_CORE_INC_DRIVERS_LED_OC_LED_H_ */
