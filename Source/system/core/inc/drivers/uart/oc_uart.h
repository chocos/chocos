/** ****************************************************************************************************************************************
 *
 * @file       oc_uart.h
 *
 * @brief      The file with interface for UART driver
 *
 * @author     Patryk Kubiak - (Created on: 27 05 2015 22:30:48)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup UART Universal Asynchronous Receiver/Transmitter driver
 * @ingroup Drivers
 * @brief UART - managing and configuring UART transmissions.
 * 
 * This driver allows for handling UART transmissions. It provides the interface for configuring the UART module, 
 * sending and receiving data. 
 * 
 * The driver is based on the low-level driver, which is responsible for handling the hardware. The low-level driver
 * is defined in the module #UART-LLD.
 * 
 * To use the driver you have to configure the UART module with the function #oC_UART_Configure. The function requires
 * the configuration structure #oC_UART_Config_t and returns the context of the UART module. The context is used for
 * further operations on the UART module.
 * 
 * To send data via the UART module, you have to use the function #oC_UART_Write. 
 * 
 * To receive data via the UART module, you have to use the function #oC_UART_Read or #oC_UART_Receive. The 
 * difference between these functions is that #oC_UART_Read waits only for some data, while #oC_UART_Receive waits
 * for the whole buffer to be filled.
 * 
 * The driver provides also the function #oC_UART_TurnOn, which allows for turning on the UART module, but 
 * usually, it is not necessary to use it, because the UART module is turned on automatically when the system is
 * booted. However, you can always check if the UART module is turned on with the function #oC_UART_IsTurnedOn
 * and call the function #oC_UART_TurnOn if necessary.
 * 
 * <h1>Example</h1>
 * @include example_uart.c
 * 
 ******************************************************************************************************************************************/


#ifndef INC_DRIVERS_UART_OC_UART_H_
#define INC_DRIVERS_UART_OC_UART_H_

#define DRIVER_HEADER
#define DRIVER_NAME                 UART

#include <oc_driver.h>
#include <oc_gpio.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_uart_lld.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup UART
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores word length
 * 
 * The type is for storing the word length of the transmission
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_WordLength_5Bits = oC_UART_LLD_WordLength_5Bits ,  //!< transmission length is 5 bits
    oC_UART_WordLength_6Bits = oC_UART_LLD_WordLength_6Bits ,  //!< transmission length is 6 bits
    oC_UART_WordLength_7Bits = oC_UART_LLD_WordLength_7Bits ,  //!< transmission length is 7 bits
    oC_UART_WordLength_8Bits = oC_UART_LLD_WordLength_8Bits    //!< transmission length is 8 bits
} oC_UART_WordLength_t;

//==========================================================================================================================================
/**
 * @brief stores parity checking
 * 
 * The type is for storing the parity checking
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_Parity_None  = oC_UART_LLD_Parity_None ,    //!< none parity checking
    oC_UART_Parity_Odd   = oC_UART_LLD_Parity_Odd ,     //!< odd parity checking
    oC_UART_Parity_Even  = oC_UART_LLD_Parity_Even      //!< even parity checking
} oC_UART_Parity_t;

//==========================================================================================================================================
/**
 * @brief stores stop bit length
 * 
 * The type is for storing the stop bit length
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_StopBit_1Bit    = oC_UART_LLD_StopBit_1Bit    ,       //!< stop bit length set to 1 bit
    oC_UART_StopBit_1p5Bits = oC_UART_LLD_StopBit_1p5Bits ,       //!< stop bit length set to 1.5 bit
    oC_UART_StopBit_2Bits   = oC_UART_LLD_StopBit_2Bits           //!< stop bit length set to 2 bits
} oC_UART_StopBit_t;

//==========================================================================================================================================
/**
 * @brief stores bit order
 * 
 * The type is for storing the bit order
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_BitOrder_LSBFirst = oC_UART_LLD_BitOrder_LSBFirst ,//!< low significant bit first
    oC_UART_BitOrder_MSBFirst = oC_UART_LLD_BitOrder_MSBFirst  //!< most significant bit first
} oC_UART_BitOrder_t;

//==========================================================================================================================================
/**
 * @brief stores invert signals
 * 
 * The type is for storing the invert signals
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_Invert_NotInverted = oC_UART_LLD_Invert_NotInverted ,//!< not inverted signals
    oC_UART_Invert_Inverted    = oC_UART_LLD_Invert_Inverted     //!< signals will be inverted
} oC_UART_Invert_t;

//==========================================================================================================================================
/**
 * @brief stores DMA usage
 * 
 * The type is for storing the DMA usage
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_Dma_UseIfPossible ,     //!< use DMA if possible
    oC_UART_Dma_AlwaysUse ,         //!< always use DMA (force DMA usage)
    oC_UART_Dma_DontUse             //!< don't use DMA
} oC_UART_Dma_t;

//==========================================================================================================================================
/**
 * @brief stores UART mode
 * 
 * The type is for storing the UART mode
 */
//==========================================================================================================================================
typedef enum
{
    oC_UART_Mode_Both ,             //!< both input and output
    oC_UART_Mode_Output ,           //!< only output
    oC_UART_Mode_Input              //!< only input
} oC_UART_Mode_t;

//==========================================================================================================================================
/**
 * @brief stores configuration of UART
 * 
 * The type is for storing the configuration of UART
 * 
 * <b>Example:</b>
 * @snippet example_uart.c uart_configuration
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pins_t               Rx;                 //!< pin for receiving
    oC_Pins_t               Tx;                 //!< pin for transmitting
    oC_UART_WordLength_t    WordLength;         //!< length of the word
    uint32_t                BitRate;            //!< bit rate
    oC_UART_Parity_t        Parity;             //!< parity checking
    oC_UART_StopBit_t       StopBit;            //!< stop bit length
    oC_UART_BitOrder_t      BitOrder;           //!< bit order
    oC_UART_Invert_t        Invert;             //!< invert signals
    oC_UART_Dma_t           Dma;                //!< DMA usage
    bool                    Loopback;           //!< loop-back mode
} oC_UART_Config_t;

//==========================================================================================================================================
/**
 * @brief stores context of UART
 * 
 * The type is for storing the context of UART
 */
//==========================================================================================================================================
typedef struct Context_t * oC_UART_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
/** @} */

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/**
 * @addtogroup UART
 * @{
 */

extern oC_ErrorCode_t oC_UART_TurnOn                 ( void );
extern oC_ErrorCode_t oC_UART_TurnOff                ( void );
extern bool           oC_UART_IsTurnedOn             ( void );
extern oC_ErrorCode_t oC_UART_Configure              ( const oC_UART_Config_t * Config , oC_UART_Context_t * outContext );
extern oC_ErrorCode_t oC_UART_Unconfigure            ( const oC_UART_Config_t * Config , oC_UART_Context_t * outContext );
extern oC_ErrorCode_t oC_UART_Read                   ( oC_UART_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_UART_Receive                ( oC_UART_Context_t Context , char * outBuffer , oC_MemorySize_t Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_UART_Write                  ( oC_UART_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_UART_Ioctl                  ( oC_UART_Context_t Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t oC_UART_Disable                ( oC_UART_Context_t Context );
extern oC_ErrorCode_t oC_UART_Enable                 ( oC_UART_Context_t Context );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/** @} */

#endif /* INC_DRIVERS_UART_OC_UART_H_ */
