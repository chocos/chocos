/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_gpio.h
 *
 * @author     Patryk Kubiak - (Created on: 27 05 2015 21:03:16)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup GPIO General Purpose Input/Output driver
 * @ingroup Drivers
 * @brief GPIO - managing and configuring GPIO pins.
 * 
 ******************************************************************************************************************************************/


#ifndef INC_DRIVERS_GPIO_OC_GPIO_H_
#define INC_DRIVERS_GPIO_OC_GPIO_H_

#define DRIVER_HEADER
#define DRIVER_NAME                 GPIO

#include <oc_driver.h>
#include <oc_gpio_lld.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________


//==========================================================================================================================================
/**
 * @brief unlocking special pins
 *
 * The type is for protection for configure special pins, like JTAG and NMI pins
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_Protection_DontUnlockProtectedPins = oC_GPIO_LLD_Protection_DontUnlockProtectedPins,    /**< Do not configure special pins */
    oC_GPIO_Protection_UnlockProtectedPins     = oC_GPIO_LLD_Protection_UnlockProtectedPins         /**< Configure special pins */
} oC_GPIO_Protection_t;

//==========================================================================================================================================
/**
 * @brief speed of pins
 *
 * The type represents maximum speed, that GPIO pins can be switched. When you do not need to very often switch you pin, you can use #oC_GPIO_Speed_Minimum,
 * and thanks to that, you can save some current.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_Speed_Default           = oC_GPIO_LLD_Speed_Default,             /**< Default value for oC_GPIO_Speed_t */
    oC_GPIO_Speed_Minimum           = oC_GPIO_LLD_Speed_Minimum,             /**< Pin works with minimum speed */
    oC_GPIO_Speed_Medium            = oC_GPIO_LLD_Speed_Medium,              /**< Pin works with medium speed */
    oC_GPIO_Speed_Maximum           = oC_GPIO_LLD_Speed_Maximum,             /**< Pin works with maximum speed */
    oC_GPIO_Speed_NumberOfElements  = oC_GPIO_LLD_Speed_NumberOfElements     /**< Number of elements in oC_GPIO_Speed_t enum */
} oC_GPIO_Speed_t;

//==========================================================================================================================================
/**
 * @brief output current
 *
 * Type represents maximum output current for GPIO pin.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_Current_Default     = oC_GPIO_LLD_Current_Default,        /**< Pin works with default current (not changed) */
    oC_GPIO_Current_Minimum     = oC_GPIO_LLD_Current_Minimum,        /**< Pin works with minimum current */
    oC_GPIO_Current_Medium      = oC_GPIO_LLD_Current_Medium,         /**< Pin works with medium current */
    oC_GPIO_Current_Maximum     = oC_GPIO_LLD_Current_Maximum         /**< Pin works with maximum current */
} oC_GPIO_Current_t;

//==========================================================================================================================================
/**
 * @brief select In/Out mode
 *
 * Type represents mode of works for GPIO pin.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_Mode_Default    = oC_GPIO_LLD_Mode_Default,       /**< Pin works in default mode (not changed) */
    oC_GPIO_Mode_Input      = oC_GPIO_LLD_Mode_Input,         /**< Pin works as input */
    oC_GPIO_Mode_Output     = oC_GPIO_LLD_Mode_Output,        /**< Pin works as output */
    oC_GPIO_Mode_Alternate  = oC_GPIO_LLD_Mode_Alternate      /**< Pin works in alternative mode */
} oC_GPIO_Mode_t;

//==========================================================================================================================================
/**
 * @brief pull-up/pull-down in input mode
 *
 * Type represents GPIO pull mode, when the pin work as input.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_Pull_Default = oC_GPIO_LLD_Pull_Default,        /**< Pin works with default pull mode (not changed) */
    oC_GPIO_Pull_Up      = oC_GPIO_LLD_Pull_Up,             /**< Pull-up */
    oC_GPIO_Pull_Down    = oC_GPIO_LLD_Pull_Down            /**< Pull-down */
} oC_GPIO_Pull_t;

//==========================================================================================================================================
/**
 * @brief output circuit - open drain/push pull
 *
 * Type represents GPIO output circuit mode ( Open Drain / PushPull )
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_OutputCircuit_Default   = oC_GPIO_LLD_OutputCircuit_Default ,
    oC_GPIO_OutputCircuit_OpenDrain = oC_GPIO_LLD_OutputCircuit_OpenDrain,   /**< open drain mode */
    oC_GPIO_OutputCircuit_PushPull  = oC_GPIO_LLD_OutputCircuit_PushPull     /**< push-pull mode */
} oC_GPIO_OutputCircuit_t;


//==========================================================================================================================================
/**
 * @brief   interrupt trigger source
 *
 * Type represents GPIO interrupt trigger source.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_IntTrigger_Default      = oC_GPIO_LLD_IntTrigger_Default,     /**< Pin works in default interrupt mode (not changed) */
    oC_GPIO_IntTrigger_Off          = oC_GPIO_LLD_IntTrigger_Off,         /**< Interrupts on this pin are turned off */
    oC_GPIO_IntTrigger_RisingEdge   = oC_GPIO_LLD_IntTrigger_RisingEdge,  /**< Interrupt on rising edge */
    oC_GPIO_IntTrigger_FallingEdge  = oC_GPIO_LLD_IntTrigger_FallingEdge, /**< Interrupt on falling edge */
    oC_GPIO_IntTrigger_BothEdges    = oC_GPIO_LLD_IntTrigger_BothEdges,   /**< Interrupt on both edges */
    oC_GPIO_IntTrigger_HighLevel    = oC_GPIO_LLD_IntTrigger_HighLevel,   /**< Interrupt on high level */
    oC_GPIO_IntTrigger_LowLevel     = oC_GPIO_LLD_IntTrigger_LowLevel,    /**< Interrupt on low level */
    oC_GPIO_IntTrigger_BothLevels   = oC_GPIO_LLD_IntTrigger_BothLevels   /**< Interrupt on both levels */
} oC_GPIO_IntTrigger_t;

//==========================================================================================================================================
/**
 * @brief type for storing state of pins
 *
 * The type is for storing state of pins. This type is similar to the #oC_PinsMask_t type, and every bit in the variable is reserved for some
 * pins.
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_PinsState_AllLow  = oC_GPIO_LLD_PinsState_AllLow,    //!< all pins are set to low state
    oC_GPIO_PinsState_AllHigh = oC_GPIO_LLD_PinsState_AllHigh    //!< all pins are set to high state
} oC_GPIO_PinsState_t;

//==========================================================================================================================================
/**
 * @brief ioctl commands for GPIO
 */
//==========================================================================================================================================
typedef enum
{
    oC_GPIO_Ioctl_Command_TogglePins        = oC_Ioctl_MakeCommand(oC_Ioctl_Group_GPIO , 0x01 , NULL ) ,                            //!< Toggles pins state
    oC_GPIO_Ioctl_Command_SetPinsState      = oC_Ioctl_MakeCommand(oC_Ioctl_Group_GPIO , 0x02 , sizeof(oC_GPIO_LLD_PinsState_t*) ) ,//!< set new pins state
    oC_GPIO_Ioctl_Command_GetHighPinsState  = oC_Ioctl_MakeCommand(oC_Ioctl_Group_GPIO , 0x03 , sizeof(oC_GPIO_LLD_PinsState_t*) ) ,//!< reads pins in high state
    oC_GPIO_Ioctl_Command_GetLowPinsState   = oC_Ioctl_MakeCommand(oC_Ioctl_Group_GPIO , 0x04 , sizeof(oC_GPIO_LLD_PinsState_t*) ) ,//!< reads pins in low state
} oC_GPIO_Ioctl_Command_t;

//==========================================================================================================================================
/**
 * @brief GPIO driver configuration structure
 *
 * This is the configuration structure for the GPIO driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_GPIO_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pins_t               Pins;               /**< List of pins from single port, that should be configured. More pins can be joined by '|' operator */
    oC_GPIO_Protection_t    Protection;         /**< Take off protection if exists (for special pins) */
    oC_GPIO_Speed_t         Speed;              /**< Maximum speed of output pins toggling */
    oC_GPIO_Current_t       Current;            /**< Maximum current for input/output pins */
    oC_GPIO_Mode_t          Mode;               /**< Mode of pins work - input/output/alternative */
    oC_GPIO_Pull_t          Pull;               /**< For input pins - if it should be pull up or pull down by default */
    oC_GPIO_OutputCircuit_t OutputCircuit;      /**< For output pins - selection between open drain and push-pull modes */
    oC_GPIO_IntTrigger_t    InterruptTrigger;   /**< Active event of pins interrupt */
} oC_GPIO_Config_t;

//==========================================================================================================================================
/**
 * @brief The GPIO context structure.
 *
 * This is the structure with dynamic allocated data for the GPIO. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_GPIO_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_GPIO_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef oC_Pins_t * oC_GPIO_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t oC_GPIO_TurnOn            ( void );
extern oC_ErrorCode_t oC_GPIO_TurnOff           ( void );
extern bool           oC_GPIO_IsTurnedOn        ( void );
extern oC_ErrorCode_t oC_GPIO_Configure         ( const oC_GPIO_Config_t * Config , oC_GPIO_Context_t * outContext );
extern oC_ErrorCode_t oC_GPIO_Unconfigure       ( const oC_GPIO_Config_t * Config , oC_GPIO_Context_t * outContext );
extern oC_ErrorCode_t oC_GPIO_Ioctl             ( oC_GPIO_Context_t Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t oC_GPIO_QuickOutput       ( oC_Pins_t Pins );
extern oC_ErrorCode_t oC_GPIO_QuickInput        ( oC_Pins_t Pins , oC_GPIO_Pull_t Pull , oC_GPIO_IntTrigger_t InterruptTrigger );
extern oC_ErrorCode_t oC_GPIO_QuickUnconfigure  ( oC_Pins_t Pins );
extern oC_ErrorCode_t oC_GPIO_SetSpeed          ( oC_Pins_t Pins , oC_GPIO_Speed_t Speed );
extern oC_ErrorCode_t oC_GPIO_ReadSpeed         ( oC_Pins_t Pins , oC_GPIO_Speed_t * outSpeed );
extern oC_ErrorCode_t oC_GPIO_SetCurrent        ( oC_Pins_t Pins , oC_GPIO_Current_t Current );
extern oC_ErrorCode_t oC_GPIO_ReadCurrent       ( oC_Pins_t Pins , oC_GPIO_Current_t * outCurrent );
extern oC_ErrorCode_t oC_GPIO_SetMode           ( oC_Pins_t Pins , oC_GPIO_Mode_t Mode );
extern oC_ErrorCode_t oC_GPIO_ReadMode          ( oC_Pins_t Pins , oC_GPIO_Mode_t * outMode );
extern oC_ErrorCode_t oC_GPIO_SetPull           ( oC_Pins_t Pins , oC_GPIO_Pull_t Pull );
extern oC_ErrorCode_t oC_GPIO_ReadPull          ( oC_Pins_t Pins , oC_GPIO_Pull_t * outPull );
extern oC_ErrorCode_t oC_GPIO_SetOutputCircuit  ( oC_Pins_t Pins , oC_GPIO_OutputCircuit_t OutputCircuit );
extern oC_ErrorCode_t oC_GPIO_ReadOutputCircuit ( oC_Pins_t Pins , oC_GPIO_OutputCircuit_t * outOutputCircuit );
extern oC_ErrorCode_t oC_GPIO_SetInterruptTrigger   ( oC_Pins_t Pins , oC_GPIO_IntTrigger_t      InterruptTrigger );
extern oC_ErrorCode_t oC_GPIO_ReadInterruptTrigger  ( oC_Pins_t Pins , oC_GPIO_IntTrigger_t * outInterruptTrigger );
extern oC_ErrorCode_t oC_GPIO_FindPinByName     ( const char * Name , oC_Pin_t * outPin );
extern oC_ErrorCode_t oC_GPIO_WaitForPins       ( oC_Pins_t Pins , oC_Time_t Timeout, oC_Time_t Period );
extern oC_ErrorCode_t oC_GPIO_WaitForPinsState  ( oC_Pins_t Pins , oC_GPIO_PinsState_t PinsState , oC_Time_t Timeout, oC_Time_t Period );
extern const char *   oC_GPIO_GetCurrentNameFromEnum        (oC_GPIO_Current_t Current);
extern const char *   oC_GPIO_GetModeNameFromEnum           (oC_GPIO_Mode_t Mode);
extern const char *   oC_GPIO_GetOutputCircuitNameFromEnum  (oC_GPIO_OutputCircuit_t OutputCircuit);
extern const char *   oC_GPIO_GetPullNameFromEnum           (oC_GPIO_Pull_t Pull);
extern const char *   oC_GPIO_GetSpeedNameFromEnum          (oC_GPIO_Speed_t Speed);
extern const char *   oC_GPIO_GetIntTriggerNameFromEnum     (oC_GPIO_IntTrigger_t IntTrigger);

/* Redefinitions from the LLD */
//==========================================================================================================================================
/**
 * @brief returns name of the pin
 *
 * The function is for reading printable name of the given pin, or "unknown" if the pin is not correct. Note, that it works only for
 * single pin and will not return correct value for more than one pin.
 *
 * @param Pin   Variable with single pin
 *
 * @return constant string with name of a pin
 */
//==========================================================================================================================================
extern const char * oC_GPIO_GetPinName( oC_Pin_t Pin );
//==========================================================================================================================================
/**
 * @brief checks if the GPIO port is correct
 *
 * The function checks if the port is correct
 *
 * @param Port       Port of the GPIO (port)
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_IsPortCorrect( oC_Port_t Port );
//==========================================================================================================================================
/**
 * @brief checks if the GPIO port index is correct
 *
 * The function checks if the given port index is correct in the GPIO module.
 *
 * @param PortIndex  Index of the channel in the module
 *
 * @return true if correct
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_IsPortIndexCorrect( oC_PortIndex_t PortIndex );
//==========================================================================================================================================
/**
 * @brief returns pins mask of pins
 *
 * The function returns pins mask from the #oC_Pins_t type. The pins mask is a mask with bits of port, that are used in this variable.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 *
 * oC_PinsMask_t pinsMask = oC_GPIO_GetPinsMaskOfPins(pins); // pinsMask = 0b00001100
 *
 * ~~~~~~~~~~~~
 *
 * @param Pins      Variable with one or more pins
 *
 * @return bit mask
 */
//==========================================================================================================================================
extern oC_PinsMask_t oC_GPIO_GetPinsMaskOfPins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief returns port of pins
 *
 * The function returns port of pins that are stored in the pins variable.
 *
 * @param Pins      Variable with one or more pins
 *
 * @return port of pins
 */
//==========================================================================================================================================
extern oC_Port_t oC_GPIO_GetPortOfPins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief converts port to port index
 *
 * The function converts port to index of the port in the GPIO module
 *
 * @param Port       Port of the GPIO (port)
 *
 * @return index of the port in the GPIO
 */
//==========================================================================================================================================
extern oC_PortIndex_t oC_GPIO_PortToPortIndex( oC_Port_t Port );
//==========================================================================================================================================
/**
 * @brief converts port index to port
 *
 * The function converts index of the port to port in the GPIO module
 *
 * @param Channel       Channel of the GPIO (port)
 *
 * @return index of the port in the GPIO
 */
//==========================================================================================================================================
extern oC_Port_t oC_GPIO_PortIndexToPort( oC_PortIndex_t PortIndex );
//==========================================================================================================================================
/**
 * @brief checks if the pin is defined (only one pin)
 *
 * The function checks if the given pin is defined in the machine definition list for this processor.
 *
 * @warning this function is correct only for single selection pin link. It return false if the pin link will contain more than one pin.
 *
 * @param Pins          Pin to check if is defined
 *
 * @return true if defined.
 */
//==========================================================================================================================================
extern bool oC_GPIO_IsPinDefined( oC_Pins_t Pin );
//==========================================================================================================================================
/**
 * @brief checks if pins are defined
 *
 * The function checks if all pins that are stored in the Pins argument are defined. It is important, that each pin should be from the same
 * port. If any of pins will be from different port, then the function will return, that pins are not defined.
 *
 * @param Pins      variable with pins from the same port
 *
 * @return true if each pin stored in the Pins argument is defined
 */
//==========================================================================================================================================
extern bool oC_GPIO_ArePinsDefined( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if pins are correct
 *
 * The function checks if pins are correct, but not checks if each pin in the variable is defined.
 *
 * @param Pins      One or more pins from the same port
 *
 * @return true if pins are correct
 */
//==========================================================================================================================================
extern bool oC_GPIO_ArePinsCorrect( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if there is only one pin in pins variable
 *
 * The function checks if in the Pins variable is stored only one pin.
 *
 * @param Pins      Variable with pins from the same port
 *
 * @return true if there is only one pin
 */
//==========================================================================================================================================
extern bool oC_GPIO_IsSinglePin( oC_Pins_t Pins );

//==========================================================================================================================================
/**
 * @brief connect port and pins to one variable
 *
 * The function is for connecting pins definitions and port to the one, special type #oC_Pins_t that stores this both informations.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Port_t port = oC_Port_PORTA;
 * oC_Pins_Mask_t pinsMask = oC_Pins_Mask_All;
 * oC_Pins_t pins = oC_GPIO_GetPinsFor( port , pinsMask );
 * ~~~~~~~~~~~~
 *
 * @param Port      Port of the GPIO
 * @param Pins      Mask of pins that should be included to the #oC_Pins_t
 *
 * @return connected port and pins definitions
 */
//==========================================================================================================================================
extern oC_Pins_t oC_GPIO_GetPinsFor( oC_Port_t Port , oC_PinsMask_t Pins );

//==========================================================================================================================================
/**
 * @brief returns name of the port
 *
 * The function is for reading printable name of the given port. If the port is not correct, then it should return "unknown" string.
 *
 * @param Port      Variable with a port value
 *
 * @return constant string with name of a port
 */
//==========================================================================================================================================
extern const char * oC_GPIO_GetPortName( oC_Port_t Port );
//==========================================================================================================================================
/**
 * @brief returns name of the pin
 *
 * The function is for reading printable name of the given pin, or "unknown" if the pin is not correct. Note, that it works only for
 * single pin and will not return correct value for more than one pin.
 *
 * @param Pin   Variable with single pin
 *
 * @return constant string with name of a pin
 */
//==========================================================================================================================================
extern const char * oC_GPIO_GetPinName( oC_Pins_t Pin );
//==========================================================================================================================================
/**
 * @brief sets power for a port
 *
 * The function configures power for the port that is given in the Pins argument. This function should power up all registers that are needed
 * to configure GPIO pins.
 *
 * @note this function does not require calling of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and
 * after usage
 *
 * @param Pins      variable with single or more pins from the same port
 * @param Power     power to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_SetPower( oC_Pins_t Pins , oC_Power_t Power );
//==========================================================================================================================================
/**
 * @brief reads power state in a port
 *
 * The function reads power state in a port.
 *
 * @note this function does not require calling of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and
 * after usage
 *
 * @param Pins      variable with single or more pins from the same port
 * @param outPower  destination for the power state variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_ReadPower( oC_Pins_t Pins , oC_Power_t * outPower );

//==========================================================================================================================================
/**
 * @brief checks if the pin is used
 *
 * The function is for checking if the pin is in use. It is for protection before reconfiguration in the future. Note,
 * that none of **GPIO-LLD** functions checks if pin is used during configuration - on this layer it is only a feature. The main driver
 * should provide an interface to protect pins when it is necessary. To set pin as used see #oC_GPIO_SetPinUsed function.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outPinUsed            destination for the output variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_CheckIsPinUsed( oC_Pins_t Pins , bool * outPinUsed );
//==========================================================================================================================================
/**
 * @brief checks if all of pins are not used
 *
 * The function is for checking if the pin (or pins) are not in use. It is for protection before reconfiguration in the future. Note,
 * that none of **GPIO-LLD** functions checks if pin is used during configuration - on this layer it is only a feature. The main driver
 * should provide an interface to protect pins when it is necessary. To set pin as used see #oC_GPIO_SetPinUsed function.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outPinsUnused         destination for the output variable
 *
 * @see oC_GPIO_CheckIsPinUsed
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_ArePinsUnused( oC_Pins_t Pins , bool * outPinsUnused );
//==========================================================================================================================================
/**
 * @brief write data in port
 *
 * The function is for writing data on the selected pins. This function checks if arguments are correct, and it is useful for example, when
 * all port is configured and used, and there is a need to write data on it, because it sets both - high and low states on each pin from the
 * Pins argument. Note, that if you need to set all pins to the same state, the #oC_GPIO_SetPinsState function will be more useful and
 * faster.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param Data                  Data to write on port
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_WriteData( oC_Pins_t Pins , oC_PinsMask_t Data );
//==========================================================================================================================================
/**
 * @brief reads data from port
 *
 * The function is for reading data from the selected pins. This function checks if arguments are correct, and it is useful for example, when
 * all port is configured and used, and there is a need to read data from it (not pins state). Note, that there are also functions #oC_GPIO_GetHighStatePins,
 * #oC_GPIO_GetLowStatePins and #oC_GPIO_IsPinsState that should be little faster.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outData               Destination for the return data
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_ReadData( oC_Pins_t Pins , oC_PinsMask_t * outData );
//==========================================================================================================================================
/**
 * @brief reads reference to the GPIO data
 *
 * This function allows to read the pointer to the DATA of GPIO. It can be useful for speeding up operations based on the writing or reading
 * state of a GPIO port.
 *
 * Example of usage:
 * @code{.c}
 * // (with assumption, that it was correctly configured before)
 *
 * oC_PinsMask_t * keysPortData = NULL;
 * oC_PinsMask_t * ledsPortData = NULL;
 *
 * oC_GPIO_ReadDataReference( ledPins  , &ledsPortData );
 * oC_GPIO_ReadDataReference( keysPins , &keysPortData );
 *
 * while(1)
 * {
 *      // If at least one of keys is pressed
 *      if(*keysPortData)
 *      {
 *          // Turn on LEDs
 *          *ledsPortData = 0xff;
 *      }
 *      else
 *      {
 *          // Turn off LEDs if none of keys is turned on
 *          *ledsPortData = 0;
 *      }
 * }
 *
 * @endcode
 *
 * @param Pins
 * @param outDataReference
 * @return
 */
//==========================================================================================================================================
extern oC_ErrorCode_t oC_GPIO_ReadDataReference( oC_Pins_t Pins , oC_UInt_t ** outDataReference );
//==========================================================================================================================================
/**
 * @brief returns pins that are set to high state
 *
 * The function is for reading state of pins without arguments checking. It is little faster then the #oC_GPIO_ReadData function, and
 * can be more comfortable in use, but there is a risk, that if the Pins argument is not set correctly, then it can cause the hard fault on
 * the machine. This function should be used only when the pins argument is correct.
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * if ( oC_GPIO_GetHighStatePins( pins ) == oC_Pins_PA2 )
 * {
 *      // only the oC_Pins_PA2 pin is set to high!
 * }
 * ~~~~~~~~~~~~
 * @warning
 * This function returns list of pins, that are set to high state. If you want to simply check if all pins that are defined in the Pins argument
 * are set to the given state - low or high, use #oC_GPIO_IsPinsState function.
 *
 * @param Pins                  variable with single or more pins from the same port
 *
 * @return pins that set from the Pins argument
 */
//==========================================================================================================================================
extern oC_Pins_t oC_GPIO_GetHighStatePins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief returns pins that are set to low state
 *
 * The function is for reading state of pins without arguments checking. It is little faster then the #oC_GPIO_ReadData function, and
 * can be more comfortable in use, but there is a risk, that if the Pins argument is not set correctly, then it can cause the hard fault on
 * the machine. This function should be used only when the pins argument is correct.
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * if ( oC_GPIO_GetLowStatePins( pins ) == oC_Pins_PA2 )
 * {
 *      // only the oC_Pins_PA2 pin is set to low!
 * }
 * ~~~~~~~~~~~~
 * @warning
 * This function returns list of pins, that are set to low state. If you want to simply check if all pins that are defined in the Pins argument
 * are set to the given state - low or high, use #oC_GPIO_IsPinsState function.
 *
 *
 * @param Pins                  variable with single or more pins from the same port
 *
 * @return pins that set from the Pins argument
 */
//==========================================================================================================================================
extern oC_Pins_t oC_GPIO_GetLowStatePins( oC_Pins_t Pins );
//==========================================================================================================================================
/**
 * @brief checks if all pins are set to state
 *
 * The function checks if all pins that are set in the Pins argument are set to the selected state.
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 * if ( oC_GPIO_IsPinsState( pins , oC_GPIO_PinsState_AllHigh ) )
 * {
 *      // oC_Pins_PA2 and oC_Pins_PA3 pins are set to high state
 * }
 * else if ( oC_GPIO_IsPinsState( pins , oC_GPIO_PinsState_AllLow ) )
 * {
 *      // oC_Pins_PA2 and oC_Pins_PA3 pins are set to low state
 * }
 * else
 * {
 *      // pins are not in the same state
 * }
 * ~~~~~~~~~~~~
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param ExpectedPinsState     expected state of pins
 *
 * @return true if all pins are in the expected state
 */
//==========================================================================================================================================
extern bool oC_GPIO_IsPinsState( oC_Pins_t Pins , oC_GPIO_PinsState_t ExpectedPinsState );
//==========================================================================================================================================
/**
 * @brief sets pins to the selected state
 *
 * The function is for setting a state for all pins that are defined in the Pins argument.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 *
 * oC_GPIO_SetPinsState( pins , oC_GPIO_PinsState_AllHigh );
 * ~~~~~~~~~~~~
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param PinsState             state of pins to set
 */
//==========================================================================================================================================
extern void oC_GPIO_SetPinsState( oC_Pins_t Pins , oC_GPIO_PinsState_t PinsState );
//==========================================================================================================================================
/**
 * @brief toggles pins state
 *
 * The function toggles each pins state to opposite. This function is for make basic operation faster, so it not checks arguments.
 *
 * Example of usage:
 * ~~~~~~~~~~~~{.c}
 * oC_Pins_t pins = oC_Pins_PA2 | oC_Pins_PA3;
 *
 * oC_GPIO_SetPinsState( pins , oC_GPIO_PinsState_AllHigh );
 * // pins oC_Pins_PA2 and oC_Pins_PA3 are now in high state
 * oC_GPIO_TogglePinsState( pins );
 * // pins oC_Pins_PA2 and oC_Pins_PA3 are now in low state
 * oC_GPIO_SetPinsState( oC_Pins_PA2 , oC_GPIO_PinsState_AllHigh );
 * // pin oC_Pins_PA2 is in high state, but oC_Pins_PA3 is in low state
 * oC_GPIO_TogglePinsState( pins );
 * // pin oC_Pins_PA2 is in low state, but oC_Pins_PA3 is in high state
 * ~~~~~~~~~~~~
 *
 * @warning
 * This function does not check arguments, so it is important to set it properly, because if the port will be set incorrectly, there is a risk
 * that it will cause a hard fault. You must call #oC_GPIO_ArePinsCorrect function before it to ensure, that it will not happen.
 *
 * @param Pins                  variable with single or more pins from the same port
 */
//==========================================================================================================================================
extern void oC_GPIO_TogglePinsState( oC_Pins_t Pins );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* INC_DRIVERS_GPIO_OC_GPIO_H_ */
