/** ****************************************************************************************************************************************
 *
 * @brief      File with interface prototypes for SDMMC modes
 * 
 * @file       oc_sdmmc_mode.h
 *
 * @author     Patryk Kubiak - (Created on: 10.08.2017 19:50:34) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_MODE_H_
#define SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_MODE_H_
#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

#include <oc_errors.h>
#include <oc_sdmmc.h>
#include <oc_sdmmc_responses.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores index of the raw command
 */
//==========================================================================================================================================
typedef uint8_t oC_SDMMC_Mode_CommandIndex_t;

//==========================================================================================================================================
/**
 * @brief stores command argument
 */
//==========================================================================================================================================
typedef uint32_t oC_SDMMC_Mode_CommandArgument_t;

//==========================================================================================================================================
/**
 * @brief stores RAW command to send
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_Mode_CommandIndex_t            CommandIndex;
    oC_SDMMC_Mode_CommandArgument_t         CommandArgument;
    oC_SDMMC_Response_t                Response;
} oC_SDMMC_Mode_RawCommand_t;

//==========================================================================================================================================
/**
 * @brief stores interface of a mode
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_Mode_t Mode;       // ID of the mode
    const char *    ModeName;   // Name of the mode
    oC_ErrorCode_t (*TurnOn)                ( void );       // Turns on the mode
    oC_ErrorCode_t (*TurnOff)               ( void );       // Turns off the mode
    bool           (*IsSupported)           ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context );  // Checks if the given configuration is possible to configure in the given mode
    oC_ErrorCode_t (*Configure)             ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context );  // Configures the given mode in the given configuration
    oC_ErrorCode_t (*Unconfigure)           ( oC_SDMMC_Context_t Context );  // Unconfigures the given configuration

    /**
     * @brief stores function for sending of commands
     *
     * The type is for storing of a pointer to the function that allows for sending of SDIO commands.
     *
     * @param Context       Context of the driver from #oC_SDMMC_Configure function
     * @param Command       Command to send
     * @param Timeout       Maximum time for the operation
     *
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                       | Description
     * --------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                   | Operation success
     *  #oC_ErrorCode_WrongAddress           | Address of the Interface or Command is not correct
     *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
     *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
     *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
     *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
     *  #oC_ErrorCode_ResponseNotReceived    | The command was properly sent, but the card did not respond for the command
     *
     *  @note
     *  More error codes can be returned by function
     */
    oC_ErrorCode_t (*SendCommand)           ( oC_SDMMC_Context_t Context , oC_SDMMC_Mode_RawCommand_t * Command , oC_Time_t Timeout );
    oC_ErrorCode_t (*ReadData)              ( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, char * outBuffer, oC_MemorySize_t * Size , oC_Time_t Timeout ); // Reads data by using the given mode
    oC_ErrorCode_t (*WriteData)             ( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, const char * Buffer, oC_MemorySize_t * Size , oC_Time_t Timeout ); // Writes data by using the given mode
    oC_ErrorCode_t (*InitializeCard)        ( oC_SDMMC_Context_t Context , oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout ); // Initializes the given card to work (after card detection)
    oC_ErrorCode_t (*SetTransferMode)       ( oC_SDMMC_Context_t Context , oC_SDMMC_TransferMode_t TransferMode ); // Sets transfer mode
} oC_SDMMC_Mode_Interface_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t oC_SDMMC_Mode_TurnOn          ( const oC_SDMMC_Mode_Interface_t * Interface );
extern oC_ErrorCode_t oC_SDMMC_Mode_TurnOff         ( const oC_SDMMC_Mode_Interface_t * Interface );
extern bool           oC_SDMMC_Mode_IsSupported     ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context );
extern oC_ErrorCode_t oC_SDMMC_Mode_Configure       ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context );
extern oC_ErrorCode_t oC_SDMMC_Mode_Unconfigure     ( oC_SDMMC_Context_t Context );
extern oC_ErrorCode_t oC_SDMMC_Mode_SendCommand     ( oC_SDMMC_Context_t Context , oC_SDMMC_Mode_RawCommand_t * Command , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Mode_ReadData        ( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, char * outBuffer, oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Mode_WriteData       ( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, const char * Buffer, oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Mode_InitializeCard  ( oC_SDMMC_Context_t Context , oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Mode_SetTransferMode ( oC_SDMMC_Context_t Context, oC_SDMMC_TransferMode_t TransferMode );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



#endif /* SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_MODE_H_ */
