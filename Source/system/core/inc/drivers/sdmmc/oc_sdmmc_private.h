/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          oc_sdmmc_private.h
 *
 * @author     Patryk Kubiak - (Created on: 14.08.2017 10:37:58) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_PRIVATE_H_
#define SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_PRIVATE_H_

#include <oc_sdmmc.h>
#include <oc_sdmmc_spi.h>
#include <oc_sdmmc_mode.h>
#include <oc_mutex.h>
#include <oc_semaphore.h>
#include <oc_list.h>

#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define oC_SDMMC_DRIVER_NAME        SDMMC
#define oC_SDMMC_DRIVER_FILE_NAME   "sdmmc"

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores SDMMC context
 *
 * The structure is for storing context of the driver.
 */
//==========================================================================================================================================
struct oC_SDMMC_Context_t
{
    /** @brief The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t                  ObjectControl;
    /** @brief Size of the sector (block) - it is number of data that can be accessed at once. The field is copied from configuration by main SDMMC module */
    oC_MemorySize_t                     SectorSize;
    /** @brief Number of sectors in the card - thanks to that we can know size of the card. The field is filled during card initialization */
    oC_SectorNumber_t                   NumberOfSectors;
    /** @brief Module busy recursive mutex - the mutex is taken when the card is busy and the module cannot be accessed */
    oC_Mutex_t                          ModuleBusy;
    /** @brief Stores transfer mode type - filled during initialization */
    oC_SDMMC_TransferMode_t             TransferMode;
    /** @brief Stores pointer to the SDMMC Mode interface. It is filled during configuration by the main SDMMC module */
    const oC_SDMMC_Mode_Interface_t *   Interface;
    /** @brief Pin used for detection of the SD/MMC card insertion */
    oC_Pin_t                            DetectionPin;
    /** @brief Context for mode-specific data. Allocated and filled by the mode during mode configuration */
    struct ModeContext_t *              ModeContext;
    /** @brief number of retries for commands sending */
    uint8_t                             NumberOfRetries;
    /** @brief card initialization power mode */
    oC_SDMMC_PowerMode_t                PowerMode;
    /** @brief period of card detection polling */
    oC_Time_t                           DetectionPeriod;
    /** @brief List of detected cards */
    oC_List(oC_SDMMC_CardInfo_t*)       DetectedCards;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINES____________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the given SDMMC context is valid
 *
 * The function checks if the given context is correct and returns true if it is.
 *
 * @param Context   Pointer to the SDMMC context to check
 * @return true if context is correct
 */
//==========================================================================================================================================
static inline bool oC_SDMMC_IsContextCorrect( oC_SDMMC_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl( Context, oC_ObjectId_SDMMCContext, Context->ObjectControl );
}

#undef  _________________________________________INLINES____________________________________________________________________________________

#endif /* SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_PRIVATE_H_ */
