/** ****************************************************************************************************************************************
 *
 * @brief      Definitions for SDIO commands
 * 
 * @file       oc_sdmmc_cmd.h
 *
 * @author     Patryk Kubiak - (Created on: 05.07.2022 10:24:00)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SDMMC-Cmd SDIO commands
 * @ingroup SDMMC
 * @brief The module for handling SDIO commands
 * 
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_CMD_H_
#define SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_CMD_H_
#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

#include <oc_sdmmc.h>
#include <oc_1word.h>
#include <oc_sdmmc_mode.h>
#include <oc_sdmmc_responses.h>
#include <oc_sdmmc_arguments.h>
#include <oc_struct.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief makes command index entry
 */
//==========================================================================================================================================
#define oC_SDMMC_Cmd_Command_Make(App,Index,Class,Type,Response)     \
    ((App)      << oC_SDMMC_Cmd_Command__AppBitIndex) | \
    ((oC_1WORD_FROM_2(oC_SDMMC_Cmd_CommandType_  , Type    )) << oC_SDMMC_Cmd_Command__TypeBitIndex   ) | \
    ((oC_1WORD_FROM_2(oC_SDMMC_Cmd_CommandClass_ , Class   )) << oC_SDMMC_Cmd_Command__ClassBitIndex  )  | \
    ((oC_1WORD_FROM_2(oC_SDMMC_ResponseIndex_    , Response)) << oC_SDMMC_Cmd_Command__ResponseBitIndex) | \
    ((Index)    << oC_SDMMC_Cmd_Command__IndexBitIndex)

#undef _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________


//==========================================================================================================================================
/**
 * @brief stores command type
 */
//==========================================================================================================================================
typedef enum
{
    /**
     * Broadcast Commands (bc), no response—The broadcast feature is applicable only if all the CMD lines
     * are connected together in the host. If they are separated then each card will accept it separately on his turn.
     */
    oC_SDMMC_Cmd_CommandType_BroadcastNoResponse,

    /**
     * Broadcast Commands with Response (bcr)—response from all cards simultaneously. Since there is no
     * Open Drain mode in SD Card, this type of command is used only if all the CMD lines are separated.
     * The command will be accepted and responded to by every card separately
     */
    oC_SDMMC_Cmd_CommandType_BroadcastWithResponse,

    /**
     * Addressed (point-to-point) Commands (ac)—no data transfer on DAT.
     */
    oC_SDMMC_Cmd_CommandType_AddressedNoData,

    /**
     * Addressed (point-to-point) Data Transfer Commands (adtc)—data transfer on DAT.
     */
    oC_SDMMC_Cmd_CommandType_AddressedWithData,

    /**
     * Number of elements in this enum
     */
    oC_SDMMC_Cmd_CommandType_NumberOfElements,
    oC_SDMMC_Cmd_CommandType_Unknown
} oC_SDMMC_Cmd_CommandType_t;

//==========================================================================================================================================
/**
 * @brief stores class of the command
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_Cmd_CommandClass_Class0,               //!< Basic commends
    oC_SDMMC_Cmd_CommandClass_Class1,               //!< Stream Read
    oC_SDMMC_Cmd_CommandClass_Class2,               //!< Block Read
    oC_SDMMC_Cmd_CommandClass_Class3,               //!< Stream Write
    oC_SDMMC_Cmd_CommandClass_Class4,               //!< Block Write
    oC_SDMMC_Cmd_CommandClass_Class5,               //!< Erase
    oC_SDMMC_Cmd_CommandClass_Class6,               //!< Write protection
    oC_SDMMC_Cmd_CommandClass_Class7,               //!< Lock card
    oC_SDMMC_Cmd_CommandClass_Unknown,              //!< Unknown class
    oC_SDMMC_Cmd_CommandClass__NumberOfElements,

    oC_SDMMC_Cmd_CommandClass_Basic             = oC_SDMMC_Cmd_CommandClass_Class0, //!< Basic commends (need to be supported by all the cards)
    oC_SDMMC_Cmd_CommandClass_StreamRead        = oC_SDMMC_Cmd_CommandClass_Class1, //!< Stream Read
    oC_SDMMC_Cmd_CommandClass_BlockRead         = oC_SDMMC_Cmd_CommandClass_Class2, //!< Block Read
    oC_SDMMC_Cmd_CommandClass_StreamWrite       = oC_SDMMC_Cmd_CommandClass_Class3, //!< Stream Read
    oC_SDMMC_Cmd_CommandClass_BlockWrite        = oC_SDMMC_Cmd_CommandClass_Class4, //!< Block Write
    oC_SDMMC_Cmd_CommandClass_Erase             = oC_SDMMC_Cmd_CommandClass_Class5, //!< Erase
    oC_SDMMC_Cmd_CommandClass_WriteProtection   = oC_SDMMC_Cmd_CommandClass_Class6, //!< Write Protection
    oC_SDMMC_Cmd_CommandClass_LockCard          = oC_SDMMC_Cmd_CommandClass_Class7  //!< Lock card
} oC_SDMMC_Cmd_CommandClass_t;

//==========================================================================================================================================
/**
 *  Stores commands
 *
 *  @warning
 *  Please keep it aligned with #oC_SDMMC_Cmd_CommandData_t
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_Cmd_Command__TypeBitIndex        = 9,
    oC_SDMMC_Cmd_Command__TypeBitMask         = 0x7  << oC_SDMMC_Cmd_Command__TypeBitIndex,
    oC_SDMMC_Cmd_Command__ResponseBitIndex    = 12,
    oC_SDMMC_Cmd_Command__ResponseBitMask     = 0xF  << oC_SDMMC_Cmd_Command__ResponseBitIndex,
    oC_SDMMC_Cmd_Command__ClassBitIndex       = 16,
    oC_SDMMC_Cmd_Command__ClassBitMask        = 0xF,
    oC_SDMMC_Cmd_Command__IndexBitIndex       = 0,
    oC_SDMMC_Cmd_Command__IndexBitMask        = 0xFF << oC_SDMMC_Cmd_Command__IndexBitIndex,
    oC_SDMMC_Cmd_Command__AppBitIndex         = 8,
    oC_SDMMC_Cmd_Command__AppBitMask          = 0x1  << oC_SDMMC_Cmd_Command__AppBitIndex,
    oC_SDMMC_Cmd_Command__MaxIndex            = 60,
    oC_SDMMC_Cmd_Command_CMD0  = oC_SDMMC_Cmd_Command_Make(0, 0,Basic            , BroadcastNoResponse        , None),
    oC_SDMMC_Cmd_Command_CMD1  = oC_SDMMC_Cmd_Command_Make(0, 1,Basic            , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD2  = oC_SDMMC_Cmd_Command_Make(0, 2,Basic            , BroadcastWithResponse      , R2),
    oC_SDMMC_Cmd_Command_CMD3  = oC_SDMMC_Cmd_Command_Make(0, 3,Basic            , BroadcastWithResponse      , R6),
    oC_SDMMC_Cmd_Command_CMD4  = oC_SDMMC_Cmd_Command_Make(0, 4,Basic            , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD5  = oC_SDMMC_Cmd_Command_Make(0, 5,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD6  = oC_SDMMC_Cmd_Command_Make(0, 6,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD7  = oC_SDMMC_Cmd_Command_Make(0, 7,Basic            , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_CMD8  = oC_SDMMC_Cmd_Command_Make(0, 8,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD9  = oC_SDMMC_Cmd_Command_Make(0, 9,Basic            , AddressedNoData            , R2),
    oC_SDMMC_Cmd_Command_CMD10 = oC_SDMMC_Cmd_Command_Make(0,10,Basic            , AddressedNoData            , R2),
    oC_SDMMC_Cmd_Command_CMD11 = oC_SDMMC_Cmd_Command_Make(0,11,StreamRead       , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD12 = oC_SDMMC_Cmd_Command_Make(0,12,Basic            , AddressedNoData            , R1b),
    oC_SDMMC_Cmd_Command_CMD13 = oC_SDMMC_Cmd_Command_Make(0,13,Basic            , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_CMD15 = oC_SDMMC_Cmd_Command_Make(0,15,Basic            , AddressedNoData            , None),
    oC_SDMMC_Cmd_Command_CMD16 = oC_SDMMC_Cmd_Command_Make(0,16,BlockRead        , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_CMD17 = oC_SDMMC_Cmd_Command_Make(0,17,BlockRead        , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD18 = oC_SDMMC_Cmd_Command_Make(0,18,BlockRead        , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD22 = oC_SDMMC_Cmd_Command_Make(0,22,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD23 = oC_SDMMC_Cmd_Command_Make(0,23,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD24 = oC_SDMMC_Cmd_Command_Make(0,24,BlockWrite       , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD25 = oC_SDMMC_Cmd_Command_Make(0,25,BlockWrite       , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD27 = oC_SDMMC_Cmd_Command_Make(0,27,BlockWrite       , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD28 = oC_SDMMC_Cmd_Command_Make(0,28,WriteProtection  , AddressedNoData            , R1b),
    oC_SDMMC_Cmd_Command_CMD29 = oC_SDMMC_Cmd_Command_Make(0,29,WriteProtection  , AddressedNoData            , R1b),
    oC_SDMMC_Cmd_Command_CMD30 = oC_SDMMC_Cmd_Command_Make(0,30,WriteProtection  , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD32 = oC_SDMMC_Cmd_Command_Make(0,32,Erase            , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_CMD33 = oC_SDMMC_Cmd_Command_Make(0,33,Erase            , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_CMD35 = oC_SDMMC_Cmd_Command_Make(0,35,Erase            , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD36 = oC_SDMMC_Cmd_Command_Make(0,36,Erase            , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD38 = oC_SDMMC_Cmd_Command_Make(0,38,Erase            , AddressedNoData            , R1b),
    oC_SDMMC_Cmd_Command_CMD41 = oC_SDMMC_Cmd_Command_Make(0,41,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD42 = oC_SDMMC_Cmd_Command_Make(0,42,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD51 = oC_SDMMC_Cmd_Command_Make(0,51,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD52 = oC_SDMMC_Cmd_Command_Make(0,52,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD53 = oC_SDMMC_Cmd_Command_Make(0,53,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD55 = oC_SDMMC_Cmd_Command_Make(0,55,Unknown          , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_CMD56 = oC_SDMMC_Cmd_Command_Make(0,56,Unknown          , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_CMD58 = oC_SDMMC_Cmd_Command_Make(0,58,Unknown          , Unknown                    , None),
    oC_SDMMC_Cmd_Command_CMD59 = oC_SDMMC_Cmd_Command_Make(0,59,Unknown          , Unknown                    , None),

    oC_SDMMC_Cmd_Command_ACMD6    =
                    oC_SDMMC_Cmd_Command_Make(1,6,Unknown           , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_ACMD13   =
                    oC_SDMMC_Cmd_Command_Make(1,13,Unknown          , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_ACMD22   =
                    oC_SDMMC_Cmd_Command_Make(1,22,Unknown          , AddressedWithData          , R1),
    oC_SDMMC_Cmd_Command_ACMD23   =
                    oC_SDMMC_Cmd_Command_Make(1,23,Unknown          , AddressedNoData            , R1),

    oC_SDMMC_Cmd_Command_ACMD41   =
                    oC_SDMMC_Cmd_Command_Make(1,41,Unknown          , BroadcastWithResponse      , R3),
    oC_SDMMC_Cmd_Command_ACMD42   =
                    oC_SDMMC_Cmd_Command_Make(1,42,Unknown          , AddressedNoData            , R1),
    oC_SDMMC_Cmd_Command_ACMD51   =
                    oC_SDMMC_Cmd_Command_Make(1,51,Unknown          , AddressedWithData          , R1),

    /** Resets all cards to idle state */
    oC_SDMMC_Cmd_Command_GO_IDLE_STATE      = oC_SDMMC_Cmd_Command_CMD0,
    /** SEND_OP_COND */
    oC_SDMMC_Cmd_Command_SEND_OP_COND       = oC_SDMMC_Cmd_Command_CMD1,
    /** Asks any card to send the CID numbers on the CMD line (any card that is connected to the host will respond */
    oC_SDMMC_Cmd_Command_ALL_SEND_CID       = oC_SDMMC_Cmd_Command_CMD2,
    /** Ask the card to publish a new relative address (RCA) */
    oC_SDMMC_Cmd_Command_SEND_RELATIVE_ADDR = oC_SDMMC_Cmd_Command_CMD3,
    /** Program the DSR of all cards */
    oC_SDMMC_Cmd_Command_SET_DSR = oC_SDMMC_Cmd_Command_CMD4,
    /** IO_SEND_OP_COND */
    oC_SDMMC_Cmd_Command_IO_SEND_OP_COND = oC_SDMMC_Cmd_Command_CMD5,
    /**
     * Command toggles a card between the stand-by and transfer states or between the programming and disconnecting states.
     * In both cases, the card is selected by its own relative address, and gets de-selected by any other address; address 0 deselects all.
     * In the case that the RCA equals 0, then the host may do one of the following:
     *    - Use other RCA number to perform card de-selection
     *    - Resend CMD3 to change its RCA number to other than 0 and then use CMD7 with RCA=0 for card deselection
     */
    oC_SDMMC_Cmd_Command_SELECT_CARD = oC_SDMMC_Cmd_Command_CMD7,
    /**
     * Sends SD Memory Card interface condition, which includes host supply voltage information and asks the card whether card supports voltage
     * Reserved bits shall be set to 0
     */
    oC_SDMMC_Cmd_Command_SEND_IF_COND = oC_SDMMC_Cmd_Command_CMD8,
    /** Addressed card sends its card-specific data (CSD) on the CMD line */
    oC_SDMMC_Cmd_Command_SEND_CSD = oC_SDMMC_Cmd_Command_CMD9,
    /** Addressed card sends its card identification (CID) on the CMD line */
    oC_SDMMC_Cmd_Command_SEND_CID = oC_SDMMC_Cmd_Command_CMD10,
    /** Forces the card to stop transmission */
    oC_SDMMC_Cmd_Command_SEND_STOP_TRANSMISSION = oC_SDMMC_Cmd_Command_CMD12,
    /** Addressed card sends its status register */
    oC_SDMMC_Cmd_Command_SEND_STATUS = oC_SDMMC_Cmd_Command_CMD13,
    /**
     * Sends an addressed card into the Inactive State. This command is used when the host explicitly wants to deactivate a card.
     * Reserved bits shall be set to 0
     * */
    oC_SDMMC_Cmd_Command_GO_INACTIVE_STATE = oC_SDMMC_Cmd_Command_CMD15,
    /**
     * In the case of a Standard Capacity SD Memory Card, this command sets the block length (in bytes) for all following block commands
     * (read, write, lock). Default block length is fixed to 512 Bytes. Set length is valid for memory access commands only if partial block
     * read operation are allowed in CSD. In the case of a High Capacity SD Memory Card, block length set by CMD16 command does not affect
     * the memory read and write commands. Always 512 Bytes fixed block length is used. This command is effective for LOCK_UNLOCK command.
     * In both cases, if block length is set larger than 512Bytes, the card sets the BLOCK_LEN_ERROR bit.
     * */
    oC_SDMMC_Cmd_Command_SET_BLOCKLEN = oC_SDMMC_Cmd_Command_CMD16,
    /**
     * In the case of a Standard Capacity SD Memory Card, this command reads a block of the size selected by the SET_BLOCKLEN command.
     * In the case of a High Capacity Card, block length is fixed 512 Bytes regardless of the #oC_SDMMC_Cmd_Command_SET_BLOCKLEN command.
     */
    oC_SDMMC_Cmd_Command_READ_SINGLE_BLOCK = oC_SDMMC_Cmd_Command_CMD17,
    /**
     * Continuously transfers data blocks from card to host until interrupted by a STOP_TRANSMISSION command. Block length is specified
     * the same as #oC_SDMMC_Cmd_Command_READ_SINGLE_BLOCK command
     */
    oC_SDMMC_Cmd_Command_READ_MULTIPLE_BLOCK = oC_SDMMC_Cmd_Command_CMD18,
    /** Writes block */
    oC_SDMMC_Cmd_Command_WRITE_BLOCK = oC_SDMMC_Cmd_Command_CMD24,
    /** Writes multiple blocks */
    oC_SDMMC_Cmd_Command_WRITE_MULTIPLE_BLOCK = oC_SDMMC_Cmd_Command_CMD25,
    /** Programs CSD */
    oC_SDMMC_Cmd_Command_PROGRAM_CSD = oC_SDMMC_Cmd_Command_CMD27,
    /** Sets write protection */
    oC_SDMMC_Cmd_Command_SET_WRITE_PROT = oC_SDMMC_Cmd_Command_CMD28,
    /** Clears write protection */
    oC_SDMMC_Cmd_Command_CLR_WRITE_PROT = oC_SDMMC_Cmd_Command_CMD29,
    /** Sends write protection */
    oC_SDMMC_Cmd_Command_SEND_WRITE_PROT = oC_SDMMC_Cmd_Command_CMD30,
    /** Configures starting block for erasing */
    oC_SDMMC_Cmd_Command_ERASE_WR_BLOCK_START = oC_SDMMC_Cmd_Command_CMD32,
    /** Configures end block for erasing */
    oC_SDMMC_Cmd_Command_ERASE_WR_BLOCK_END = oC_SDMMC_Cmd_Command_CMD33,
    /** Erases the blocks */
    oC_SDMMC_Cmd_Command_ERASE = oC_SDMMC_Cmd_Command_CMD38,
    /** Configures lock/unlock */
    oC_SDMMC_Cmd_Command_LOCK_UNLOCK = oC_SDMMC_Cmd_Command_CMD42,
    /** Allows to access single register */
    oC_SDMMC_Cmd_Command_IO_RW_DIRECT = oC_SDMMC_Cmd_Command_CMD52,
    /** Allows to access multiple registers */
    oC_SDMMC_Cmd_Command_IO_RW_EXTENDED = oC_SDMMC_Cmd_Command_CMD53,
    /** Application command */
    oC_SDMMC_Cmd_Command_APP_CMD = oC_SDMMC_Cmd_Command_CMD55,
    /** General command */
    oC_SDMMC_Cmd_Command_GEN_CMD = oC_SDMMC_Cmd_Command_CMD56,
    /** Reads OCR */
    oC_SDMMC_Cmd_Command_READ_OCR = oC_SDMMC_Cmd_Command_CMD58,
    /** Enables/Disables CRC */
    oC_SDMMC_Cmd_Command_CRC_ON_OFF = oC_SDMMC_Cmd_Command_CMD59,
    /** Defines the data bus width (’00’=1bit or ’10’=4 bits bus) to be used for data transfer.*/
    oC_SDMMC_Cmd_Command_SET_BUS_WIDTH = oC_SDMMC_Cmd_Command_ACMD6,
    /** Send the SD Card status. */
    oC_SDMMC_Cmd_Command_SD_STATUS = oC_SDMMC_Cmd_Command_ACMD13,
    /** Send the number of the written (without errors) write blocks. Responds with 32bit+CRC data block. */
    oC_SDMMC_Cmd_Command_SEND_NUM_WR_BLOCKS = oC_SDMMC_Cmd_Command_ACMD22,
    /** Set the number of write blocks to be pre-erased before writing (to be used for faster Multiple Block WR command). “1”=default (one wr block)2.  */
    oC_SDMMC_Cmd_Command_SET_WR_BLK_ERASE_COUNT = oC_SDMMC_Cmd_Command_ACMD23,
    /** Asks the accessed card to send its operating condition register (OCR) content in the response on the CMD line.  */
    oC_SDMMC_Cmd_Command_SD_SEND_OP_COND = oC_SDMMC_Cmd_Command_ACMD41,
    /** Connect[1]/Disconnect[0] the 50KOhm pull-up resistor on CD/DAT3 (pin 1) of the card. The pull-up may be used for card detection. */
    oC_SDMMC_Cmd_Command_SET_CLR_CARD_DETECT = oC_SDMMC_Cmd_Command_ACMD42,
    /** Reads the SD Configuration Register (SCR). */
    oC_SDMMC_Cmd_Command_SEND_SCR = oC_SDMMC_Cmd_Command_ACMD51,

} oC_SDMMC_Cmd_Command_t;

//==========================================================================================================================================
/**
 * @brief stores index of the command
 */
//==========================================================================================================================================
typedef uint8_t oC_SDMMC_Cmd_CommandIndex_t;

//==========================================================================================================================================
/**
 * @brief type for storing command argument
 */
//==========================================================================================================================================
typedef oC_SDMMC_Mode_CommandArgument_t oC_SDMMC_Cmd_Argument_t;

//==========================================================================================================================================
/**
 * @brief stores request for SdSendOpCond command
 */
//==========================================================================================================================================
typedef struct
{
    /**
     * @brief required voltage range
     */
    struct
    {
        float Min;  //!< Minimum voltage required by the host
        float Max;  //!< Maximum voltage required by the host
    } VoltageRange;

    /**
     * If true, the switching to low voltage mode will be requested
     */
    bool    SwitchToLowVoltage;

    /**
     * Set by the host if High Capacity Cards (SDHC and SDXC) can be supported. If false, then only SDSC is supported by the host
     */
    bool    HighCapacitySupported;

    /**
     * (Only for SDXC) - If true, the SDXC works in maximum performance mode and it works in power saving mode otherwise
     */
    bool    MaximumPerformanceEnabled;
} oC_SDMMC_Cmd_Request_SdSendOpCond_t;

//==========================================================================================================================================
/**
 * @brief stores result of the SD_SEND_OP_COND command
 */
//==========================================================================================================================================
typedef struct
{
    bool    VoltageSupported;                   //!< If true, the requested voltage range is supported
    bool    SwitchingToLowVoltageAccepted;      //!< If true, the switching to low-voltage mode has been accepted
    bool    HighCapacityCard;                   //!< If true, the card is SDHC or SDXC. If false, the card is SDSC
    bool    InitializationCompleted;            //!< If true, the initialization process has been completed
} oC_SDMMC_Cmd_Result_SdSendOpCond_t;

//==========================================================================================================================================
/**
 * The SD Memory Card command set is divided into subsets (command classes). The card command class register CCC defines which command
 * classes are supported by this card.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_Cmd_Result_CardCommandClass_None    = 0,    /**< None class is supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class0  = 1<<0, /**< Class0  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class1  = 1<<1, /**< Class1  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class2  = 1<<2, /**< Class2  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class3  = 1<<3, /**< Class3  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class4  = 1<<4, /**< Class4  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class5  = 1<<5, /**< Class5  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class6  = 1<<6, /**< Class6  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class7  = 1<<7, /**< Class7  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class8  = 1<<8, /**< Class8  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class9  = 1<<9, /**< Class9  commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class10 = 1<<10,/**< Class10 commands are supported */
    oC_SDMMC_Cmd_Result_CardCommandClass_Class11 = 1<<11,/**< Class11 commands are supported */
} oC_SDMMC_Cmd_Result_CardCommandClass_t;

//==========================================================================================================================================
/**
 * Result of CSD register read cmd
 */
//==========================================================================================================================================
typedef struct
{
    /**
     * Size of the card capacity
     */
    oC_MemorySize_t         CardSize;

    /**
     * List of supported card command classes
     */
    oC_SDMMC_Cmd_Result_CardCommandClass_t  SupportedClasses;

    /**
     * Stores maximum transfer speed
     */
    oC_TransferSpeed_t TransferSpeed;

    /**
     * Maximum data block length
     */
    oC_MemorySize_t BlockLength;

    /**
     * If true, smaller blocks can be read as well, so the minimum block size will be one byte.
     */
    bool PartialBlockReadAllowed;

    /**
     * If true, smaller blocks can be write as well, so the minimum block size will be one byte.
     */
    bool PartialBlockWriteAllowed;

    /**
     * Defines if the data block to be written by one command can be spread over more than one physical
     * block of the memory device.
     *
     * The size of the memory block is defined in #BlockLength.
     *
     * If true, signals that crossing physical block boundaries is allowed.
     */
    bool WriteBlockMisalign;

    /**
     * Defines if the data block to be read by one command can be spread over more than one physical block
     * of the memory device.
     *
     * The size of the memory block is defined in #BlockLength.
     *
     * If true, signals that crossing physical block boundaries is allowed.
     */
    bool ReadBlockMisalign;

    /**
     * True if DSR is implemented in the card
     */
    bool DsrImplemented;

    /**
     * The size of an erasable sector.
     */
    oC_MemorySize_t SectorSize;

    /**
     * Indicates the file format on the card.
     */
    oC_SDMMC_FileFormat_t       FileFormat;

    /**
     * Permanently protects the entire card content against overwriting or erasing (all write and erase
     * commands for this card are permanently disabled). The default value is 0, i.e. not permanently write
     * protected
     */
    bool PermanentWriteProtection;

    /**
     * Temporarily protects the entire card content from being overwritten or erased (all write and erase
     * commands for this card are temporarily disabled). This bit can be set and reset. The default value is 0,
     * i.e. not write protected.
     */
    bool TemporaryWriteProtection;
} oC_SDMMC_Cmd_Result_CardSpecificData_t;

//==========================================================================================================================================
/**
 * @brief stores command data
 *
 * The type is for storing of data for commands
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_Cmd_Command_t          Command;            //!< Command to send
    /**
     * @brief stores argument of the function
     */
    union{
        oC_SDMMC_Cmd_Argument_t                 Generic;         //!< Generic command argument
        oC_SDMMC_Arguments_SendIfCond_t         SendIfCond;      //!< Field for accessing SEND_IF_COND command argument
        oC_SDMMC_Arguments_SdSendOpCond_t       SdSendOpCond;    //!< Field for accessing SD_SEND_OP_COND command argument
        oC_SDMMC_Arguments_SetBusWidth_t        SetBusWidth;     //!< Field for accessing SET_BUS_WIDTH command argument
        oC_SDMMC_Arguments_SelectCard_t         SelectCard;      //!< Field for accessing SELECT_CARD command argument
        oC_SDMMC_Arguments_SendCsd_t            SendCsd;         //!< Field for accessing SEND_CSD command argument
    } Argument;
    oC_SDMMC_Response_t             Response;           //!< Data related to the command response
} oC_SDMMC_Cmd_CommandData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t oC_SDMMC_Cmd_ReadCommandIndex ( const char* CommandName, oC_SDMMC_Cmd_Command_t * outCommandIndex );
extern oC_ErrorCode_t oC_SDMMC_Cmd_ReadCommandName  ( oC_SDMMC_Cmd_Command_t CommandIndex, size_t BufferLength, char outCommandName[BufferLength] );
extern oC_ErrorCode_t oC_SDMMC_Cmd_ReadClassIndex   ( const char* ClassName, oC_SDMMC_Cmd_CommandClass_t* outClass );
extern oC_ErrorCode_t oC_SDDMC_Cmd_ReadClassName    ( oC_SDMMC_Cmd_CommandClass_t Class, size_t BufferLength, char outClassName[BufferLength] );

extern oC_ErrorCode_t oC_SDMMC_Cmd_SendCommand      ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeCardAddress, oC_SDMMC_Cmd_CommandData_t* Command, oC_Time_t Timeout );

/* Commands */
extern oC_ErrorCode_t oC_SDMMC_Cmd_GoIdleState          ( oC_SDMMC_Context_t Context, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_SendIfCondition      ( oC_SDMMC_Context_t Context, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_SdSendOpCond         ( oC_SDMMC_Context_t Context, const oC_SDMMC_Cmd_Request_SdSendOpCond_t* Request, oC_SDMMC_Cmd_Result_SdSendOpCond_t* outResult, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_AllSendCardId        ( oC_SDMMC_Context_t Context, oC_SDMMC_CardId_t* outCardId, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_SendRelativeAddress  ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t* outRelativeAddress, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_SelectCard           ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_SetBusWidth          ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_SDMMC_TransferMode_t TransferMode, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_SendCardSpecificData ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_SetBlockLength       ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemorySize_t BlockSize, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_ReadMultipleBlock    ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_WriteMultipleBlock   ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Cmd_StopTransmission     ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Reads CommandType from CommandIndex
 *
 * The function converts #oC_SDMMC_Cmd_Command_t to #oC_SDMMC_Cmd_CommandType_t
 *
 * @param CommandIndex      Index to read
 * @return command type from the command index
 */
//==========================================================================================================================================
static inline oC_SDMMC_Cmd_CommandType_t oC_SDMMC_Cmd_CommandToType( oC_SDMMC_Cmd_Command_t CommandIndex )
{
    return (CommandIndex & oC_SDMMC_Cmd_Command__TypeBitMask) >> oC_SDMMC_Cmd_Command__TypeBitIndex;
}

//==========================================================================================================================================
/**
 * @brief Reads ResponseIndex from CommandIndex
 *
 * The function converts #oC_SDMMC_Cmd_Command_t to #oC_SDMMC_Mode_ResponseIndex_t
 *
 * @param CommandIndex      Index to read
 * @return response index from the command index
 */
//==========================================================================================================================================
static inline oC_SDMMC_ResponseIndex_t oC_SDMMC_Cmd_CommandToResponseIndex( oC_SDMMC_Cmd_Command_t CommandIndex )
{
    return (CommandIndex & oC_SDMMC_Cmd_Command__ResponseBitMask) >> oC_SDMMC_Cmd_Command__ResponseBitIndex;
}

//==========================================================================================================================================
/**
 * @brief Reads class from command
 *
 * The function converts #oC_SDMMC_Cmd_Command_t to #oC_SDMMC_Cmd_CommandClass_t
 * @param Command           Command to read
 * @return class from the command
 */
//==========================================================================================================================================
static inline oC_SDMMC_Cmd_CommandClass_t oC_SDMMC_Cmd_CommandToClass( oC_SDMMC_Cmd_Command_t Command )
{
    return (Command & oC_SDMMC_Cmd_Command__ClassBitMask) >> oC_SDMMC_Cmd_Command__ClassBitIndex;
}

//==========================================================================================================================================
/**
 * @brief converts command into command index
 * @param Command           Command to convert
 * @return command index
 */
//==========================================================================================================================================
static inline oC_SDMMC_Cmd_CommandIndex_t oC_SDMMC_Cmd_CommandToIndex( oC_SDMMC_Cmd_Command_t Command )
{
    return (Command & oC_SDMMC_Cmd_Command__IndexBitMask) >> oC_SDMMC_Cmd_Command__IndexBitIndex;
}

//==========================================================================================================================================
/**
 * @brief returns true if the command is application command
 * @param Command           Command to check
 * @return true if the command is application command
 */
//==========================================================================================================================================
static inline bool oC_SDMMC_Cmd_IsApplicationCommand( oC_SDMMC_Cmd_Command_t Command )
{
    return (Command & oC_SDMMC_Cmd_Command__AppBitMask) != 0;
}

//==========================================================================================================================================
/**
 * @brief checks if command index is valid
 *
 * The function verifies that the given command index stores correct value
 *
 * @param CommandIndex      command index to verify
 * @return true if valid
 */
//==========================================================================================================================================
static inline bool oC_SDMMC_Cmd_IsCommandIndexValid( oC_SDMMC_Cmd_CommandIndex_t CommandIndex )
{
    return CommandIndex < oC_SDMMC_Cmd_Command__MaxIndex;
}

//==========================================================================================================================================
/**
 * @brief checks if the command type is valid
 * @param CommandType       type of the command
 * @return true if valid
 */
//==========================================================================================================================================
static inline bool oC_SDMMC_Cmd_IsCommandTypeValid( oC_SDMMC_Cmd_CommandType_t CommandType )
{
    return CommandType < oC_SDMMC_Cmd_CommandType_NumberOfElements;
}

//==========================================================================================================================================
/**
 * @brief checks if the command class is valid
 * @param Class         class to verify
 * @return true if the class is valid
 */
//==========================================================================================================================================
static inline bool oC_SDMMC_Cmd_IsCommandClassValid( oC_SDMMC_Cmd_CommandClass_t Class )
{
    return Class < oC_SDMMC_Cmd_CommandClass__NumberOfElements;
}

//==========================================================================================================================================
/**
 * @brief checks if the command is valid
 * @param Command       command to verify
 * @return true if the command is valid
 */
//==========================================================================================================================================
static inline bool oC_SDMMC_Cmd_IsCommandValid( oC_SDMMC_Cmd_Command_t Command )
{
    return oC_SDMMC_Cmd_IsCommandIndexValid ( oC_SDMMC_Cmd_CommandToIndex(Command)          )
        || oC_SDMMC_Responses_IsResponseIndexValid( oC_SDMMC_Cmd_CommandToResponseIndex(Command)  )
        || oC_SDMMC_Cmd_IsCommandClassValid ( oC_SDMMC_Cmd_CommandToClass(Command)          )
        ;
}

#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________

#endif /* SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_CMD_H_ */
