/** ****************************************************************************************************************************************
 *
 * @brief      File with response definitions for SDMMC
 * 
 * @file       oc_sdmmc_responses.h
 *
 * @author     Patryk Kubiak - (Created on: 30.08.2022 12:27)
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SDMMC-Responses SDIO Responses
 * @ingroup SDMMC
 * @brief The module for handling responses from SDIO
 * 
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_RESPONSES_H_
#define SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_RESPONSES_H_

#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

#include <stdint.h>
#include <oc_sdmmc.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#undef _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores response type
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_Responses_ResponseType_None ,         /**< None */
    oC_SDMMC_Responses_ResponseType_Short ,        /**< Short */
    oC_SDMMC_Responses_ResponseType_Long ,         /**< Long */
    oC_SDMMC_Responses_ResponseType_NumberOfTypes ,/**< Number of elements in this enum (used for verification) */
} oC_SDMMC_Responses_ResponseType_t;

//==========================================================================================================================================
/**
 * @brief stores response index
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_ResponseIndex_None = 0,           /**< None response */
    oC_SDMMC_ResponseIndex_R1   = 1,           /**< R1 */
    oC_SDMMC_ResponseIndex_R1b  = 1,           /**< R1b */
    oC_SDMMC_ResponseIndex_R2   = 2,           /**< R2 */
    oC_SDMMC_ResponseIndex_R3   = 3,           /**< R3 */
    oC_SDMMC_ResponseIndex_R6   = 6,           /**< R6 */
    oC_SDMMC_ResponseIndex_R7   = 7,           /**< R7 */
    oC_SDMMC_ResponseIndex_MAX  = 10           //!< Maximum value for the register
} oC_SDMMC_ResponseIndex_t;

//==========================================================================================================================================
/**
 * The response format R1 contains a 32-bit field with the name card status. This field is intended to
 * transmit status information which is stored in a local status register of each card to the host. The
 * following table defines the status register structure. The Type and Clear-Condition fields in the table
 * are coded as follows:
 * - Type:
 * E: Error bit.
 * S: Status bit.
 * R: Detected and set for the actual command response.
 * X: Detected and set during command execution. The host must poll the card by sending status
 *
 * @note the description of the fields comes from MultiMediaCard Specification by Samsung Electronics Co., LTD ver 0.9
 */
//==========================================================================================================================================
typedef struct PACKED
{
    uint32_t                _Reserved:5;
    /**
     * The card will expect ACMD or indication
     * that the command has been interpreted
     * as ACMD.
     */
    uint32_t                AppCmd:1;
    uint32_t                _Reserved2:2;

    /**
     * corresponds to buffer empty signaling on the bus
     */
    uint32_t                ReadyForData:1;

    /**
     * Current state of the card.
     */
    oC_SDMMC_State_t        CurrentState:4;

    /**
     * An erase sequence was cleared before executing because an out of erase sequence command was received
     */
    uint32_t                EraseReset:1;

    /**
     * The command has been executed without using the internal ECC.
     */
    uint32_t                CardEccDisabled:1;

    /**
     * Only partial address space was erased due to existing WP blocks.
     */
    uint32_t                WpEraseSkip:1;

    /**
     * can be either one of the following errors :
     * - The CID register is already written and
     *   can not be overwritten.
     * - The read only section of the CSD does
     *   not match the card content.
     *   - An attempt to reversecopy (set as original)
     *   or permanent WP (unprotect) bits was done.
     */
    uint32_t                CIDCSDOverwrite:1;

    /**
     * The card could not sustain data programming in stream write mode.
     */
    uint32_t                Overrun:1;

    /**
     * The card could not sustain data transfer in stream read mode.
     */
    uint32_t                Underrun:1;

    /**
     * A general or an unknown error occurred during the operation.
     */
    uint32_t                Error:1;

    /**
     * Card internal ECC was applied but the correction of data is failed
     */
    uint32_t                CardEccFailed:1;

    /**
     * Command not legal for the current state
     */
    uint32_t                IllegalCommand:1;

    /**
     * The CRC check of the previous command failed.
     */
    uint32_t                ComCrcError:1;

    /**
     * Set when a sequence or password error has been detected in lock/unlock card command or it there was an attempt to
     * access a locked card.
     */
    uint32_t                LockUnlockFailed:1;

    /**
     * When set, signals that the card is locked by the host.
     */
    uint32_t                CardIsLocked:1;

    /**
     * The command tried to write a write protected block.
     */
    uint32_t                WpViolation:1;

    /**
     * An invalid selection, sectors or groups, for erase.
     */
    uint32_t                EraseParam:1;

    /**
     * An error in the sequence of erase commands occurred.
     */
    uint32_t                EraseSeqError:1;

    /**
     * The transferred block length is not allowed for this card or the number of transferred bytes does not match the block length
     */
    uint32_t                BlockLenError:1;

    /**
     * A misaligned address, which did not match the block length was used in the command.
     */
    uint32_t                AddressError:1;

    /**
     * The commands argument was out of allowed range for this card.
     */
    uint32_t                OutOfRange:1;
} oC_SDMMC_Responses_R1_t;

//==========================================================================================================================================
/**
 * @brief stores voltage window
 *
 * The type is for storing of voltage window required by the OCR register
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_Responses_VoltageWindow_2p7to2p8 = 1<<0,/**< 2.7V-2.8V */
    oC_SDMMC_Responses_VoltageWindow_2p8to2p9 = 1<<1,/**< 2.8V-2.9V */
    oC_SDMMC_Responses_VoltageWindow_2p9to3p0 = 1<<2,/**< 2.9V-3.0V */
    oC_SDMMC_Responses_VoltageWindow_3p0to3p1 = 1<<3,/**< 3.0V-3.1V */
    oC_SDMMC_Responses_VoltageWindow_3p1to3p2 = 1<<4,/**< 3.1V-3.2V */
    oC_SDMMC_Responses_VoltageWindow_3p2to3p3 = 1<<5,/**< 3.2V-3.3V */
    oC_SDMMC_Responses_VoltageWindow_3p3to3p4 = 1<<6,/**< 3.3V-3.4V */
    oC_SDMMC_Responses_VoltageWindow_3p4to3p5 = 1<<7,/**< 3.4V-3.5V */
    oC_SDMMC_Responses_VoltageWindow_3p5to3p6 = 1<<8,/**< 3.5V-3.6V */

    oC_SDMMC_Responses_VoltageWindow__MinBitIndex = 0,
    oC_SDMMC_Responses_VoltageWindow__MaxBitIndex = 8,
} oC_SDMMC_Responses_VoltageWindow_t;

//==========================================================================================================================================
/**
 * @brief stores R3 response
 */
//==========================================================================================================================================
typedef struct PACKED
{
    uint32_t                            _Reserved:15;

    /**
     * @brief Voltage Window Supported by the card
     */
    oC_SDMMC_Responses_VoltageWindow_t  VoltageWindow:9;

    /**
     * @brief Switching to 1.8V Accepted (S18A)
     */
    uint32_t SwitchingTo18Accepted:1;
    uint32_t _Reserved2:5;

    /**
     * @brief Card Capacity Status (CCS)
     */
    uint32_t CardCapacityStatus:1;
    /**
     * @brief Busy Status
     *
     * The bit is set when the card initialization is complete
     */
    uint32_t InitializationComplete:1;
} oC_SDMMC_Responses_R3_t;

//==========================================================================================================================================
/**
 * @brief stores R6 response
 */
//==========================================================================================================================================
typedef struct PACKED
{
    uint32_t                _Reserved:5;
    /**
     * The card will expect ACMD or indication
     * that the command has been interpreted
     * as ACMD.
     */
    uint32_t                AppCmd:1;

    uint32_t                _Reserved2:2;

    /**
     * corresponds to buffer empty signaling on the bus
     */
    uint32_t                ReadyForData:1;

    /**
     * Current state of the card.
     */
    oC_SDMMC_State_t        CurrentState:4;

    /**
     * A general or an unknown error occurred during the operation.
     */
    uint32_t                Error:1;

    /**
     * Command not legal for the current state
     */
    uint32_t                IllegalCommand:1;

    /**
     * The CRC check of the previous command failed.
     */
    uint32_t                ComCrcError:1;

    /**
     * Relative card address high bytes
     */
    uint16_t                RelativeCardAddress;
} oC_SDMMC_Responses_R6_t;

//==========================================================================================================================================
/**
 * @brief stores R7 response
 *
 * The card support voltage information is sent by the response of CMD8. `VoltageAccepted` field indicates the voltage range that
 * the card supports. The card that accepted the supplied voltage returns R7 response.
 * In the response, the card echoes back both the voltage range and check pattern set in the argument.
 */
//==========================================================================================================================================
typedef struct PACKED
{
    /**
     * Echo-back of check pattern
     */
    uint8_t Pattern;

    /**
     * Voltage accepted by the card
     */
    enum {
        oC_SDMMC_Responses_R7_VoltageAccepted_NotDefined = 0,/**< Supported voltage is not defined */
        oC_SDMMC_Responses_R7_VoltageAccepted_2p7to3p6   = 1,/**< 2.7V-3.6V */
        oC_SDMMC_Responses_R7_VoltageAccepted_LowVoltage = 2,/**< Reserved for low voltage */
    } VoltageAccepted:4;
} oC_SDMMC_Responses_R7_t;

//==========================================================================================================================================
/**
 * @brief stores responses for CID
 *
 * The Card IDentification (CID) register is 128 bits wide. It contains the card identification information
 * used during the card identification phase. Every individual Read/Write (RW) card shall have a unique
 * identification number. The structure of the CID register is defined in the following paragraphs:
 */
//==========================================================================================================================================
typedef struct PACKED
{
    /**
     * An 8-bit binary number that identifies the card manufacturer. The MID number is controlled, defined and allocated to a SD Memory Card
     * manufacturer by the SD-3C, LLC. This procedure is established to ensure uniqueness of the CID register.
     */
    uint8_t         ManufacturerId;

    /**
     * A 2-character ASCII string that identifies the card OEM and/or the card contents (when used as a
     * distribution media either on ROM or FLASH cards). The OID number is controlled, defined, and allo-
     * cated to a SD Memory Card manufacturer by the SD-3C, LLC. This procedure is established to ensure
     * uniqueness of the CID register.
     * Note: SD-3C, LLC licenses companies that wish to manufacture and/or sell SD Memory Cards, including but not limited
     * to flash memory, ROM, OTP, RAM, and SDIO Combo Cards.
     * SD-3C, LLC is a limited liability company established by Panasonic Corporation, SanDisk Corporation and Toshiba
     * Corporation.
     */
    char            OEMApplicationId[2];

    /**
     * Product name is a 5-character string
     */
    char            ProductName[5];

    /**
     * The product revision is composed of two Binary Coded Decimal (BCD) digits, four bits each, representing an "n.m" revision number.
     * The "n" is the most significant nibble and "m" is the least significant nibble.
     * As an example, the PRV binary value field for product revision "6.2" will be: 0110 0010b
     */
    uint8_t         ProductRevisionMajor:4;

    /**
     * The product revision is composed of two Binary Coded Decimal (BCD) digits, four bits each, representing an "n.m" revision number.
     * The "n" is the most significant nibble and "m" is the least significant nibble.
     * As an example, the PRV binary value field for product revision "6.2" will be: 0110 0010b
     */
    uint8_t         ProductRevisionMinor:4;


    /**
     * The Serial Number is 32 bits of binary number.
     */
    uint32_t        ProductSerialNumber;

    uint8_t         Reserved2:4;

    /**
     * The manufacturing date is composed of two hexadecimal digits, one is 8 bits representing the year(y)
     * and the other is 4 bits representing the month (m).
     * The "m" field [11:8] is the month code. 1 = January.
     * The "y" field [19:12] is the year code. 0 = 2000.
     * As an example, the binary value of the Date field for production date "April 2001" will be:
     * 00000001 0100.
     */
    uint16_t        ManufacturingDate:12;

    /** @brief CRC7 checksum (7 bits) */
    uint8_t         CRC7:7;

    uint8_t         Reserved:1;
} oC_SDMMC_Responses_CardId_t;

//==========================================================================================================================================
/**
 *  Stores transfer speed. For SDSC it should be 0x32 (25MHz), and for High-Speed 0x5A (50MHz)
 */
//==========================================================================================================================================
typedef union
{
    struct PACKED
    {
        /**
         * transfer rate unit
         *
         *  0x0     - 100kbps
         *  0x1     - 1Mbps
         *  0x2     - 10Mbps
         *  0x3     - 100Mbps
         *  0x4     - 1Gbps
         *  0x5     - 10Gbps
         *  0x6     - 100Gbps
         */
        uint8_t         Unit:3;
        /**
         * transfer value
         *
         *      0x0       - reserved
         *      0x1       - 1.0
         *      0x2       - 1.2
         *      0x3       - 1.3
         *      0x4       - 1.5
         *      0x5       - 2.0
         *      0x6       - 2.5
         *      0x7       - 3.0
         *      0x8       - 3.5
         *      0x9       - 4.0
         *      0xA       - 4.5
         *      0xB       - 5.0
         *      0xC       - 5.5
         *      0xD       - 6.0
         *      0xE       - 7.0
         *      0xF       - 8.0
         *
         */
        uint8_t         Value:4;
        uint8_t         Reserved:1;
    };
    uint8_t Generic;
} oC_SDMMC_Responses_TransferSpeed_t;

//==========================================================================================================================================
/**
 * Defines the asynchronous part of the data access time
 */
//==========================================================================================================================================
typedef struct PACKED
{
    /**
     *  time unit:
     *      0x0       - 1 ns
     *      0x1       - 10 ns
     *      0x2       - 100 ns
     *      0x3       - 1 us
     *      0x4       - 10 us
     *      0x5       - 100 us
     *      0x6       - 1 ms
     *      0x7       - 10 ms
     */
    uint8_t TimeUnit:3;

    /**
     * time value:
     *      0x0       - reserved
     *      0x1       - 1.0
     *      0x2       - 1.2
     *      0x3       - 1.3
     *      0x4       - 1.5
     *      0x5       - 2.0
     *      0x6       - 2.5
     *      0x7       - 3.0
     *      0x8       - 3.5
     *      0x9       - 4.0
     *      0xA       - 4.5
     *      0xB       - 5.0
     *      0xC       - 5.5
     *      0xD       - 6.0
     *      0xE       - 7.0
     *      0xF       - 8.0
     */
    uint8_t TimeValue:4;
    uint8_t Reserved:1;
} oC_SDMMC_Responses_TAAC_t;

//==========================================================================================================================================
/**
 * @brief Card Specific Data Register Version 1.0
 */
//==========================================================================================================================================
typedef struct PACKED
{
    /**
     * Maximum data transfer
     */
    uint32_t TransferSpeed:8;
    /**
     * Defines the worst case for the clock-independent factor of the data access time. The unit for NSAC is 100 clock cycles. Therefore
     * the maximum value for the clock-dependent part of the data access time is 25.5k clock cycles.
     */
    uint32_t     NSAC:8;
    /**
     * Defines the asynchronous part of the data access time
     */
    uint32_t   TAAC:8;
    uint32_t     Reserved:6;
    /**
     * For SDCv2 the values are as follows:
     *      0x0    - CSD Version 1.0
     *      0x1    - CSD Version 2.0
     *
     * For MMC:
     *      0x2    - CSD Version 1.2
     */
    uint32_t CsdStructure:2;
    /**
     * This parameter is used to compute the user's data card capacity (not include the security protected
     * area). The memory capacity of the card is computed from the entries C_SIZE, C_SIZE_MULT and
     * READ_BL_LEN as follows:
     *
     *          memory capacity = BLOCKNR * BlockLength
     *
     *          Where:
     *                  BLOCKNR     = (CardSize+1)*MULT
     *                  MULT        = 2^C_SIZE_MULT+2       (C_SIZE_MULT<8)
     *                  BlockLength = 2^ReadBlockLength     (ReadBlockLength<12)
     */
    uint32_t   CardSizeHigh:10;
    uint32_t    Reserved2:2;
    /**
     * Defines if the configurable driver stage is integrated on the card. If set, a driver stage register (DSR) shall be implemented
     */
    uint32_t    DSRImplemented:1;
    /**
     * Defines if the data block to be read by one command can be spread over more than one physical block of the memory device. The
     * size of the memory block is defined in ReadBlockLength
     */
    uint32_t    ReadBlockMisalign:1;
    /**
     * Defines if the data block to be written by one command can be spread over more than one physical block of the memory device. The
     * size of the memory block is defined in WriteBlockLength
     */
    uint32_t    WriteBlockMisalign:1;
    /**
     * If set, partial block read is allowed, so it means that smaller memory blocks can be used as well - the minimum block size will be
     * one byte.
     */
    uint32_t    ReadBlockPartial:1;
    /**
     * Maximum read block length 512...2048
     */
    uint32_t   ReadBlockLength:4;
    /**
     * The SD Memory Card command set is divided into subsets (command classes). The card command class register CCC defines which
     * command classes are supported by this card
     */
    uint32_t     CardCommandClasses:12;
    uint32_t     WriteProtectGroupSize:7;
    uint32_t     EraseSectorSize:7;
    uint32_t     EraseSingleBlockEnabled:1;
    /**
     * A factor MULT for computing the total device size (@see CardSize)
     *
     *      0x0     - 4
     *      0x1     - 8
     *      0x2     - 16
     *      0x3     - 32
     *      0x4     - 64
     *      0x5     - 128
     *      0x6     - 256
     *      0x7     - 512
     */
    uint32_t     CardSizeMultiplier:3;
    uint32_t     VddWCurrMax:3;
    uint32_t     VddWCurrMin:3;
    uint32_t     VddRCurrMax:3;
    uint32_t     VddRCurrMin:3;
    /**
     * This parameter is used to compute the user's data card capacity (not include the security protected
     * area). The memory capacity of the card is computed from the entries C_SIZE, C_SIZE_MULT and
     * READ_BL_LEN as follows:
     *
     *          memory capacity = BLOCKNR * BlockLength
     *
     *          Where:
     *                  BLOCKNR     = (CardSize+1)*MULT
     *                  MULT        = 2^C_SIZE_MULT+2       (C_SIZE_MULT<8)
     *                  BlockLength = 2^ReadBlockLength     (ReadBlockLength<12)
     */
    uint32_t     CardSizeLow:2;
    uint32_t     Reserved6:1;
    uint32_t     Crc7:7;
    uint32_t     Reserved5:2;
    uint32_t     FileFormat:2;
    uint32_t     TemporaryWriteProtection:1;
    uint32_t     PermanentWriteProtection:1;
    uint32_t     CopyFlag:1;
    uint32_t     FileFormatGroup:1;
    uint32_t     Reserved4:5;
    uint32_t     PartialBlocksForWriteAllowed:1;
    uint32_t     MaxWriteDataBlockLength:4;
    uint32_t     WriteSpeedFactor:3;
    uint32_t     Reserved3:2;
    uint32_t     WriteProtectGroupEnable:1;
} oC_SDMMC_Responses_CardSpecificData_V1p0_t;

//==========================================================================================================================================
/**
 * @brief card specific data register Version 2.0
 */
//==========================================================================================================================================
typedef struct PACKED
{
    /**
     * Maximum data transfer
     */
    uint32_t     TransferSpeed:8;
    uint32_t     NSAC:8;
    /**
     * Defines the worst case for the clock-independent factor of the data access time. The unit for NSAC is 100 clock cycles. Therefore
     * the maximum value for the clock-dependent part of the data access time is 25.5k clock cycles.
     */
    uint32_t     TAAC:8;
    uint32_t     Reserved:6;
    /**
     * For SDCv2 the values are as follows:
     *      0x1    - CSD Version 1.0
     *      0x2    - CSD Version 2.0
     *
     * For MMC:
     *      0x2    - CSD Version 1.2
     */
    uint32_t CsdStructure:2;

    /**
     * This parameter is used to compute the user's data card capacity (not include the security protected
     * area). The memory capacity of the card is computed from the entries C_SIZE, C_SIZE_MULT and
     * READ_BL_LEN as follows:
     *
     *          memory capacity = BLOCKNR * BlockLength
     *
     *          Where:
     *                  BLOCKNR     = (CardSize+1)*MULT
     *                  MULT        = 2^C_SIZE_MULT+2       (C_SIZE_MULT<8)
     *                  BlockLength = 2^ReadBlockLength     (ReadBlockLength<12)
     */
    uint32_t    CardSizeHigh:6;
    uint32_t    Reserved2:6;
    /**
     * Defines if the configurable driver stage is integrated on the card. If set, a driver stage register (DSR) shall be implemented
     */
    uint32_t    DSRImplemented:1;
    /**
     * Defines if the data block to be read by one command can be spread over more than one physical block of the memory device. The
     * size of the memory block is defined in ReadBlockLength
     */
    uint32_t    ReadBlockMisalign:1;
    /**
     * Defines if the data block to be written by one command can be spread over more than one physical block of the memory device. The
     * size of the memory block is defined in WriteBlockLength
     */
    uint32_t    WriteBlockMisalign:1;
    /**
     * If set, partial block read is allowed, so it means that smaller memory blocks can be used as well - the minimum block size will be
     * one byte.
     */
    uint32_t    ReadBlockPartial:1;
    /**
     * Maximum read block length 512...2048
     */
    uint32_t   ReadBlockLength:4;
    /**
     * The SD Memory Card command set is divided into subsets (command classes). The card command class register CCC defines which
     * command classes are supported by this card
     */
    uint32_t   CardCommandClasses:12;



    uint32_t   WriteProtectGroupSize:7;
    uint32_t   EraseSectorSize:7;
    uint32_t   EraseSingleBlockEnabled:1;
    uint32_t   Reserved3:1;
    /**
     * This parameter is used to compute the user's data card capacity (not include the security protected
     * area). The memory capacity of the card is computed from the entries C_SIZE, C_SIZE_MULT and
     * READ_BL_LEN as follows:
     *
     *          memory capacity = BLOCKNR * BlockLength
     *
     *          Where:
     *                  BLOCKNR     = (CardSize+1)*MULT
     *                  MULT        = 2^C_SIZE_MULT+2       (C_SIZE_MULT<8)
     *                  BlockLength = 2^ReadBlockLength     (ReadBlockLength<12)
     */
    uint32_t     CardSizeLow:16;
    uint32_t     Reserved7:1;
    uint32_t     Crc7:7;
    uint32_t     Reserved6:2;
    uint32_t     FileFormat:2;
    uint32_t     TemporaryWriteProtection:1;
    uint32_t     PermanentWriteProtection:1;
    uint32_t     CopyFlag:1;
    uint32_t     FileFormatGroup:1;
    uint32_t     Reserved5:5;
    uint32_t     PartialBlocksForWriteAllowed:1;
    uint32_t     MaxWriteDataBlockLength:4;
    uint32_t     WriteSpeedFactor:3;
    uint32_t     Reserved4:2;
    uint32_t     WriteProtectGroupEnable:1;
} oC_SDMMC_Responses_CardSpecificData_V2p0_t;

//==========================================================================================================================================
/**
 * @brief card specific data register
 */
//==========================================================================================================================================
typedef union
{
    /**
     * Stores fields for CSD Structure in version 1.0
     */
    oC_SDMMC_Responses_CardSpecificData_V1p0_t Version1p0;

    /**
     * Stores fields for CSD Structure in version 2.0
     */
    oC_SDMMC_Responses_CardSpecificData_V2p0_t Version2p0;
} oC_SDMMC_Responses_CardSpecificData_t;

//==========================================================================================================================================
/**
 * @brief type for storing of short response
 */
//==========================================================================================================================================
typedef uint32_t oC_SDMMC_ShortResponseData_t;
//==========================================================================================================================================
/**
 * @brief type for storing of long response
 */
//==========================================================================================================================================
typedef uint32_t oC_SDMMC_LongResponseData_t[oC_SDMMC_LONG_RESPONSE_LENGTH];

//==========================================================================================================================================
/**
 * @brief stores response of command
 *
 * The type is used for storing of response of a command. Please fill #Type field before sending of a command
 */
//==========================================================================================================================================
typedef struct
{
    union
    {
        oC_SDMMC_ShortResponseData_t            ShortResponse;      //!< Field for accessing generic short response
        oC_SDMMC_LongResponseData_t             LongResponse;       //!< Field for accessing generic long response
        oC_SDMMC_Responses_R1_t                 R1;                 //!< Field for accessing R1 response
        oC_SDMMC_Responses_R3_t                 R3;                 //!< Field for accessing R3 response
        oC_SDMMC_Responses_R6_t                 R6;                 //!< Field for accessing R6 response
        oC_SDMMC_Responses_R7_t                 R7;                 //!< Field for accessing R7 response
        oC_SDMMC_Responses_CardId_t             CardId;             //!< Field for accessing CardID response
        oC_SDMMC_Responses_CardSpecificData_t   CardSpecificData;   //!< Field for accessing CardSpecificData response
    };
    oC_SDMMC_Responses_ResponseType_t Type;           //!< Type of the expected response (should be filled before sending of a command)
} oC_SDMMC_Response_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t oC_SDMMC_Responses_ReadResponseIndex( const char* ResponseName, oC_SDMMC_ResponseIndex_t* outResponseIndex );
extern oC_ErrorCode_t oC_SDMMC_Responses_ReadResponseName( oC_SDMMC_ResponseIndex_t ResponseIndex, size_t BufferLength, char outResponseName[BufferLength] );
extern oC_ErrorCode_t oC_SDMMC_Responses_VerifyR1( const oC_SDMMC_Responses_R1_t* R1 );
extern oC_ErrorCode_t oC_SDMMC_Responses_Handle( oC_SDMMC_Context_t Context, oC_SDMMC_ResponseIndex_t ResponseIndex, const oC_SDMMC_Response_t* Response );
extern oC_ErrorCode_t oC_SDMMC_Responses_PrepareVoltageWindow( float VoltageMin, float VoltageMax, oC_SDMMC_Responses_VoltageWindow_t* outVoltageWindow );
extern oC_ErrorCode_t oC_SDMMC_Responses_SwapBytes( oC_SDMMC_Response_t* Response );
extern oC_ErrorCode_t oC_SDMMC_Responses_ReadTransferSpeed( const oC_SDMMC_Responses_TransferSpeed_t* TransferSpeed, oC_TransferSpeed_t* outTransferSpeed );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the response index is valid
 * @param ResponseIndex         index to validate
 * @return true if the response index is valid
 */
//==========================================================================================================================================
static inline bool oC_SDMMC_Responses_IsResponseIndexValid( oC_SDMMC_ResponseIndex_t ResponseIndex )
{
    return ResponseIndex < oC_SDMMC_ResponseIndex_MAX;
}

#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________

#endif
