/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_cardif_sdcv2.h
 *
 * @brief      Stores SDCv2 interface prototypes
 *
 * @author     Patryk Kubiak - (Created on: 09 4, 2022 1:53:41 PM)
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_INC_DRIVERS_SDMMC_CARDIF_OC_SDMMC_COMMIF_SDCV2_H_
#define SYSTEM_CORE_INC_DRIVERS_SDMMC_CARDIF_OC_SDMMC_COMMIF_SDCV2_H_

#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

#include "oc_sdmmc_commif.h"

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores definition of SDCv2 communication interface handlers
 */
//==========================================================================================================================================
extern const oC_SDMMC_CommIf_Handlers_t oC_SDMMC_CommIf_Handler_SDCv2;

#undef  _________________________________________VARIABLES__________________________________________________________________________________

#endif /* SYSTEM_CORE_INC_DRIVERS_SDMMC_CARDIF_OC_SDMMC_COMMIF_SDCV2_H_ */
