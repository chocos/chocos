/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_cardif.h
 *
 * @brief      Stores definitions and types for Card Interface handlers
 *
 * @author     Patryk Kubiak - (Created on: 09 4, 2022 12:20:44 PM)
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_DRIVERS_SDMMC_CARDIF_OC_SDMMC_COMMIF_H_
#define SYSTEM_CORE_INC_DRIVERS_SDMMC_CARDIF_OC_SDMMC_COMMIF_H_

#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

#include <oc_sdmmc.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores handlers for cards interface
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_CommunicationInterfaceId_t     InterfaceId;            //!< ID of Card Communication Interface
    const char*                             Name;                   //!< Printable name of the interface

    /**
     * @brief confirms identification of the card interface
     *
     * The function is used to confirm, that the given card supports this communication interface
     *
     * @param Context           Context of the driver
     * @param Timeout           Maximum time for the operation
     *
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                       | Description
     * --------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                   | Operation success
     *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
     *  #oC_ErrorCode_InterfaceNotSupported  | The card does not support this interface
     *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
     *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
     *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
     */
    oC_ErrorCode_t (*ConfirmIdentification)( oC_SDMMC_Context_t Context, oC_Time_t Timeout );

    /**
     * @brief initializes the card to work
     *
     * The function initializes the card to work and reads the Card Info
     *
     * @param Context           Context of the driver
     * @param outCardInfo       Destination for the read card info
     * @param Timeout           maximum time for the operation
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                       | Description
     * --------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                   | Operation success
     *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
     *  #oC_ErrorCode_InterfaceNotSupported  | The card does not support this interface
     *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
     *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
     *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
     */
    oC_ErrorCode_t (*InitializeCard)( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout );

    /**
     * @brief sets bus width
     *
     * The function sends the command SEND_BUS_WIDTH to change bus width
     *
     * @param Context               context of the driver
     * @param RelativeAddress       relative address of a card to access
     * @param TransferMode          bus width to set
     * @param Timeout               maximum time for the operation
     *
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                          | Description
     * -----------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                      | Operation success
     *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
     *  #oC_ErrorCode_WrongAddress              | `RelativeAddress` is not valid
     *  #oC_ErrorCode_TransferModeNotSupported  | The given `TransferMode` is not supported
     *
     * @note
     * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
     */
    oC_ErrorCode_t (*SetTransferMode)( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_SDMMC_TransferMode_t TransferMode, oC_Time_t Timeout );

    /**
     * @brief Initializes communication interface to read sectors
     *
     * The function is for initialization of reading sectors. Once all the required sectors are read, the user should call #FinishReadingSectors
     *
     * @param Context               context of the driver
     * @param RelativeAddress       relative address of the card to access
     * @param StartAddress          address of the starting sector to read
     * @param Timeout               maximum time for the operation
     *
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                          | Description
     * -----------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                      | Operation success
     *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
     *  #oC_ErrorCode_WrongAddress              | `RelativeAddress` is not valid
     *  #oC_ErrorCode_TransferModeNotSupported  | The given `TransferMode` is not supported
     *
     * @note
     * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
     */
    oC_ErrorCode_t (*StartReadingSectors)( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );

    /**
     * @brief Finishes reading of sectors begun by #StartReadingSectors
     *
     * The function is for finishing of reading of sectors begun by #StartReadingSectors function.
     *
     * @param Context               Context of the driver
     * @param RelativeAddress       relative address of the card to access
     * @param Timeout               maximum time for the operation
     *
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                          | Description
     * -----------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                      | Operation success
     *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
     *  #oC_ErrorCode_WrongAddress              | `RelativeAddress` is not valid
     *  #oC_ErrorCode_TimeNotCorrect            | `Timeout` is lower than 0
     *
     * @note
     * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
     */
    oC_ErrorCode_t (*FinishReadingSectors)( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );

    /**
     * @brief Initializes communication interface to write sectors
     *
     * The function is for initialization of writing sectors. Once all the required sectors are written, the user should call #FinishWritingSectors
     *
     * @param Context               context of the driver
     * @param RelativeAddress       relative address of the card to access
     * @param StartAddress          address of the starting sector to write
     * @param Timeout               maximum time for the operation
     *
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                          | Description
     * -----------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                      | Operation success
     *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
     *  #oC_ErrorCode_WrongAddress              | `RelativeAddress` is not valid
     *  #oC_ErrorCode_TransferModeNotSupported  | The given `TransferMode` is not supported
     *
     * @note
     * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
     */
    oC_ErrorCode_t (*StartWritingSectors)( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );

    /**
     * @brief Finishes writing of sectors begun by #StartWritingSectors
     *
     * The function is for finishing of writing of sectors begun by #StartWritingSectors function.
     *
     * @param Context               Context of the driver
     * @param RelativeAddress       relative address of the card to access
     * @param Timeout               maximum time for the operation
     *
     * @return
     * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
     * be set to #oC_ErrorCode_None value.
     * Possible codes:
     *      Error Code                          | Description
     * -----------------------------------------|---------------------------------------------------------------------------------------------------
     *  #oC_ErrorCode_None                      | Operation success
     *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
     *  #oC_ErrorCode_WrongAddress              | `RelativeAddress` is not valid
     *  #oC_ErrorCode_TimeNotCorrect            | `Timeout` is lower than 0
     *
     * @note
     * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
     */
    oC_ErrorCode_t (*FinishWritingSectors)( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );
} oC_SDMMC_CommIf_Handlers_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t oC_SDMMC_CommIf_DetectInterface( oC_SDMMC_Context_t Context, oC_SDMMC_CommunicationInterfaceId_t* outCardInterfaceId, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_CommIf_ReadHandlers( oC_SDMMC_CommunicationInterfaceId_t InterfaceId, const oC_SDMMC_CommIf_Handlers_t** outHandler );
extern oC_ErrorCode_t oC_SDMMC_CommIf_InitializeCard( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_CommIf_SetTransferMode( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SDMMC_TransferMode_t TransferMode, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_CommIf_StartReadingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SectorNumber_t StartSector, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_CommIf_FinishReadingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_CommIf_StartWritingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SectorNumber_t StartSector, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_CommIf_FinishWritingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_Time_t Timeout );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores pointers to communication interfaces handlers
 */
//==========================================================================================================================================
extern const oC_SDMMC_CommIf_Handlers_t* oC_SDMMC_CommIf_Handlers[ oC_SDMMC_CommunicationInterfaceId_NumberOfElements ];

#undef  _________________________________________VARIABLES__________________________________________________________________________________

#endif /* SYSTEM_CORE_INC_DRIVERS_SDMMC_CARDIF_OC_SDMMC_COMMIF_H_ */
