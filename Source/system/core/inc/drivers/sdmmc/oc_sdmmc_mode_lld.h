/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_mode_lld.h
 *
 * @brief      Contains interface for SDMMC LLD mode
 *
 * @author     Patryk Kubiak - (Created on: 3 10 2017 19:01:59)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SDMMC-LLD-Mode LLD Mode Manager
 * @ingroup SDMMC
 * @brief Handles SD/MMC card by using LLD
 *
 * The module SDMMC-LLD-Mode is responsible for managing LLD to work with SD/MMC cards. It passes requests from the main driver to the LLD,
 * handles LLD interrupts etc.
 *
 ******************************************************************************************************************************************/
#ifndef OC_SDMMC_MODE_LLD_H_
#define OC_SDMMC_MODE_LLD_H_
#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

#include <oc_sdmmc.h>
#include <oc_sdmmc_private.h>
#include <oc_sdmmc_lld.h>
#if defined(oC_SDMMC_LLD_AVAILABLE) || defined(DOXYGEN)

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________
//! @addtogroup SDMMC-LLD-Mode
//! @{

//==========================================================================================================================================
/**
 * @brief stores interface to SDMMC LLD mode
 *
 * The variable is internal SDMMC variable with interface of LLD mode
 */
//==========================================================================================================================================
extern const oC_SDMMC_Mode_Interface_t oC_SDMMC_Mode_Interface_LLD;

#undef  _________________________________________VARIABLES__________________________________________________________________________________
//!< @}

#endif /* oC_SDMMC_LLD_AVAILABLE */
#endif /* OC_SDMMC_MODE_LLD_H_ */
