/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc.h
 * 
 * File based on driver.h Ver 1.1.1
 *
 * @brief      The file with interface for SDMMC driver
 *
 * @author     Patryk Kubiak - (Created on: 2017-08-07 - 22:44:51)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SDMMC SD/MMC cards
 * @ingroup Drivers
 * @brief SDMMC - managing and configuring SD/MMC cards.
 *
 * ![SDMMC Architecture](SDMMC_Architecture.png)
 *
 ******************************************************************************************************************************************/
#ifndef _OC_SDMMC_H
#define _OC_SDMMC_H
#define DRIVER_HEADER
#define DRIVER_NAME                 SDMMC

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_sdmmc_lld.h>
#include <oc_spi.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define oC_SDMMC_DEBUGLOG_ENABLED       true

//==========================================================================================================================================
/**
 * @brief number of bytes in long response
 */
//==========================================================================================================================================
#define oC_SDMMC_LONG_RESPONSE_LENGTH        4

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup SDMMC
//! @{

//==========================================================================================================================================
/**
 * @brief stores mode of the driver
 *
 * The type is for storing working mode of the driver. The driver architecture allows for communication with the card by using few different
 * protocols and target resources. This type allows to choose the used mode.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_Mode_Auto ,          //!< If the `Auto` mode is set, the driver will try to configure and use first mode, that will support the given configuration
    oC_SDMMC_Mode_LLD ,           //!< The driver in the `LLD` mode, uses an embedded target machine module responsible for handling SD/MMC cards. Usually it is the fastest way to communicate with the card
    oC_SDMMC_Mode_SPI ,           //!< The driver in this mode, uses `SPI` driver to communicate with the card. It is usually slower than `LLD` but it allows to use the card also with the targets that does not support embedded SDMMC module
    oC_SDMMC_Mode_SW_1Bit ,       //!< The `SW1BIT` mode is the slowest of modes. The driver in this mode handles a card with the software emulation of the 1BIT interface that uses only GPIO driver.

    oC_SDMMC_Mode_NumberOfElements  //!< This definition represents number of defined modes. In fact, there is also a mode 'Auto' that is not real mode, but the `Auto` mode has to be 0 and taking care about the real number of elements would be to much complicated. It is just easier to define it in this way
} oC_SDMMC_Mode_t;

//==========================================================================================================================================
/**
 * @brief stores transmission protocol
 *
 * The type is for storing transfer mode (transmission protocol). It is for choosing a way the driver should communicate with the card. Each
 * of the modes needs different number of pins. Required pins for the mode you can check in the SD/MMC specification.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_TransferMode_Auto ,          //!< If the `Auto` mode is set, the driver tries to configure the transmission in the fastest possible protocol.
    oC_SDMMC_TransferMode_1Bit ,          //!< The mode uses only 1 bit of data
    oC_SDMMC_TransferMode_4Bit ,          //!< The mode uses 4 bits of data
    oC_SDMMC_TransferMode_8Bit ,          //!< The mode uses 8 bits of data (MMCPlus only)
    oC_SDMMC_TransferMode_SPI ,           //!< The mode uses `SPI` driver to communicate (#oC_SDMMC_Mode_t should be set to #oC_SDMMC_Mode_Auto or #oC_SDMMC_Mode_SPI)

    oC_SDMMC_TransferMode_NumberOfElements//!< This definition represents number of defined modes. In fact, there is also a mode 'Auto' that is not real mode, but the `Auto` mode has to be 0 and taking care about the real number of elements would be to much complicated. It is just easier to define it in this way
} oC_SDMMC_TransferMode_t;

//==========================================================================================================================================
/**
 * @brief stores type of detected card
 *
 * The type is for storing type of the card.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_CommunicationInterfaceId_Unknown ,         //!< Unknown card interface
    oC_SDMMC_CommunicationInterfaceId_SDCv2 ,           //!< Secure Digital Card version 2+
    oC_SDMMC_CommunicationInterfaceId_SDCv1 ,           //!< Secure Digital Card version 1
    oC_SDMMC_CommunicationInterfaceId_MMCPlus ,         //!< MultiMedia Card Plus
    oC_SDMMC_CommunicationInterfaceId_MMC ,             //!< MultiMedia Card

    oC_SDMMC_CommunicationInterfaceId_NumberOfElements ,//!< Number of card types
} oC_SDMMC_CommunicationInterfaceId_t;

//==========================================================================================================================================
/**
 * @brief stores card type
 *
 * The type is for storing type of the card
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_CardType_Unknown,        /**< Unknown card type */
    oC_SDMMC_CardType_SDSC,           /**< SDSC - Up to 2GB SD memory card using FAT12 and FAT16 FS  */
    oC_SDMMC_CardType_SDHC,           /**< SDHC - Over 2GB-32GB using FAT32 FS */
    oC_SDMMC_CardType_SDXC,           /**< SDXC - Over 32GB-2TB using exFAT FS */
    oC_SDMMC_CardType_SDHC_or_SDXC,   /**< SDHC or SDXC (recognition in progress) */
    oC_SDMMC_CardType_NumberOfElements/**< oC_SDMMC_CardType_NumberOfElements */
} oC_SDMMC_CardType_t;

//==========================================================================================================================================
/**
 * @brief stores index in the pins array
 *
 * The type is for storing index of the pin inside the #oC_SDMMC_Pins_t array.
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_PinIndex_P1 = 0 ,                                  //!< Pin number 1
    oC_SDMMC_PinIndex_P2  ,                                     //!< Pin number 2
    oC_SDMMC_PinIndex_P3  ,                                     //!< Pin number 3
    oC_SDMMC_PinIndex_P4  ,                                     //!< Pin number 4
    oC_SDMMC_PinIndex_P5  ,                                     //!< Pin number 5
    oC_SDMMC_PinIndex_P6  ,                                     //!< Pin number 6
    oC_SDMMC_PinIndex_P7  ,                                     //!< Pin number 7
    oC_SDMMC_PinIndex_P8  ,                                     //!< Pin number 8
    oC_SDMMC_PinIndex_P9  ,                                     //!< Pin number 9
    oC_SDMMC_PinIndex_P10 ,                                     //!< Pin number 10
    oC_SDMMC_PinIndex_P11 ,                                     //!< Pin number 11
    oC_SDMMC_PinIndex_P12 ,                                     //!< Pin number 12
    oC_SDMMC_PinIndex_P13 ,                                     //!< Pin number 13
    oC_SDMMC_PinIndex_NumberOfPins ,                            //!< Maximum number of pins in the #oC_SDMMC_Pins_t array

    /* SPI Mode pins */
    oC_SDMMC_PinIndex_SpiMode_nCS       = oC_SDMMC_PinIndex_P1 ,//!< Index of the pin nCS  in SPI transfer mode
    oC_SDMMC_PinIndex_SpiMode_DI        = oC_SDMMC_PinIndex_P2 ,//!< Index of the pin DI   in SPI transfer mode
    oC_SDMMC_PinIndex_SpiMode_MOSI      = oC_SDMMC_PinIndex_P2 ,//!< Index of the pin MOSI in SPI transfer mode
    oC_SDMMC_PinIndex_SpiMode_CLK       = oC_SDMMC_PinIndex_P5 ,//!< Index of the pin CLK  in SPI transfer mode
    oC_SDMMC_PinIndex_SpiMode_DO        = oC_SDMMC_PinIndex_P7 ,//!< Index of the pin DO   in SPI transfer mode
    oC_SDMMC_PinIndex_SpiMode_MISO      = oC_SDMMC_PinIndex_P7 ,//!< Index of the pin MISO in SPI transfer mode
    oC_SDMMC_PinIndex_SpiMode_nIRQ      = oC_SDMMC_PinIndex_P8 ,//!< Index of the pin nIRQ in SPI transfer mode

    /* One-Bit Mode */
    oC_SDMMC_PinIndex_1BitMode_CD     = oC_SDMMC_PinIndex_P1 ,  //!< Index of the pin CD   in 1-Bit transfer mode
    oC_SDMMC_PinIndex_1BitMode_CMD    = oC_SDMMC_PinIndex_P2 ,  //!< Index of the pin CMD  in 1-Bit transfer mode
    oC_SDMMC_PinIndex_1BitMode_CLK    = oC_SDMMC_PinIndex_P5 ,  //!< Index of the pin CLK  in 1-Bit transfer mode
    oC_SDMMC_PinIndex_1BitMode_DAT0   = oC_SDMMC_PinIndex_P7 ,  //!< Index of the pin DAT0 in 1-Bit transfer mode
    oC_SDMMC_PinIndex_1BitMode_nIRQ   = oC_SDMMC_PinIndex_P8 ,  //!< Index of the pin nIRQ in 1-Bit transfer mode

    /* Four-Bit Mode */
    oC_SDMMC_PinIndex_4BitMode_DAT3  = oC_SDMMC_PinIndex_P1 ,   //!< Index of the pin DAT3 in 4-Bits transfer mode
    oC_SDMMC_PinIndex_4BitMode_CMD   = oC_SDMMC_PinIndex_P2 ,   //!< Index of the pin CMD  in 4-Bits transfer mode
    oC_SDMMC_PinIndex_4BitMode_CLK   = oC_SDMMC_PinIndex_P5 ,   //!< Index of the pin CLK  in 4-Bits transfer mode
    oC_SDMMC_PinIndex_4BitMode_DAT0  = oC_SDMMC_PinIndex_P7 ,   //!< Index of the pin DAT0 in 4-Bits transfer mode
    oC_SDMMC_PinIndex_4BitMode_nIRQ  = oC_SDMMC_PinIndex_P10 ,  //!< Index of the pin nIRQ in 4-Bits transfer mode
    oC_SDMMC_PinIndex_4BitMode_DAT1  = oC_SDMMC_PinIndex_P8 ,   //!< Index of the pin DAT1 in 4-Bits transfer mode
    oC_SDMMC_PinIndex_4BitMode_DAT2  = oC_SDMMC_PinIndex_P9 ,   //!< Index of the pin DAT2 in 4-Bits transfer mode

    /* 8-Bit Mode */
    oC_SDMMC_PinIndex_8BitMode_DAT3  = oC_SDMMC_PinIndex_P1 ,   //!< Index of the pin DAT3 in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_CMD   = oC_SDMMC_PinIndex_P2 ,   //!< Index of the pin CMD  in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_CLK   = oC_SDMMC_PinIndex_P5 ,   //!< Index of the pin CLK  in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_DAT0  = oC_SDMMC_PinIndex_P7 ,   //!< Index of the pin DAT0 in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_DAT1  = oC_SDMMC_PinIndex_P8 ,   //!< Index of the pin DAT1 in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_DAT2  = oC_SDMMC_PinIndex_P9 ,   //!< Index of the pin DAT2 in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_DAT4  = oC_SDMMC_PinIndex_P10 ,  //!< Index of the pin DAT4 in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_DAT5  = oC_SDMMC_PinIndex_P11 ,  //!< Index of the pin DAT5 in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_DAT6  = oC_SDMMC_PinIndex_P12 ,  //!< Index of the pin DAT6 in 8-Bits transfer mode
    oC_SDMMC_PinIndex_8BitMode_DAT7  = oC_SDMMC_PinIndex_P13 ,  //!< Index of the pin DAT7 in 8-Bits transfer mode
} oC_SDMMC_PinIndex_t;

//==========================================================================================================================================
/**
 * @brief stores power mode for card initialization
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_PowerMode_MaxPerformance,      //!< Card is initialized in maximum performance mode
    oC_SDMMC_PowerMode_PowerSaving,         //!< Card is initialized in power saving mode
    oC_SDMMC_PowerMode_NumberOfElements     //!< Number of elements in this enum
} oC_SDMMC_PowerMode_t;

//==========================================================================================================================================
/**
 * @brief array with `SDMMC` pins
 *
 * The type is for storing pins of the `SDMMC` driver. Index of the pin can be identified with the type #oC_SDMMC_PinIndex_t
 */
//==========================================================================================================================================
typedef oC_Pin_t oC_SDMMC_Pins_t[oC_SDMMC_PinIndex_NumberOfPins];

//==========================================================================================================================================
/**
 * @brief type for storing of RCA
 *
 * The type is for storing relative card address. The default value is 0x0001
 *
 * The 16-bit relative card address register carries the card address assigned by the host during the card
 * identification. This address is used for the addressed host to card communication after the card
 * identification procedure. The default value of the RCA register is 0x0001. The value 0x0000 is reserved
 * to set all cards in Standby State with the command SELECT_DESELECT_CARD (CMD7). The RCA is
 * programmed with the command SET_RELATIVE_ADDRESS (CMD3) during the initialization
 * procedure. The content of this register is lost after power down. The default value is assigned when an
 * internal reset is applied by the power up detection unit of the MultiMediaCard.
 */
//==========================================================================================================================================
typedef uint16_t oC_SDMMC_RelativeCardAddress_t;

//==========================================================================================================================================
/**
 * @brief stores detection mode
 *
 * Stores information how the card should be detected
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_DetectionMode_Interrupt,   //!< The driver uses interrupt on nIRQ pin to detect card insertion
    oC_SDMMC_DetectionMode_Polling,     //!< The driver checks the nIRQ pin state periodically to detect card insertion
    oC_SDMMC_DetectionMode_NumberOfElements     //!< Number of elements in the enum
} oC_SDMMC_DetectionMode_t;

//==========================================================================================================================================
/**
 * @brief SDMMC driver configuration structure
 *
 * This is the configuration structure for the SDMMC driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_SDMMC_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_Mode_t                 Mode;           /**!< Mode of the driver - leave it as `Auto` if you don't know which mode you should use */
    oC_SDMMC_TransferMode_t         TransferMode;   /**!< Mode of the transfer - transmission protocol */
    oC_SDMMC_Pins_t                 Pins;           /**!< Array with pins for the card. You don't have to fill all pins, not connected pins you can set to `oC_Pin_NotUsed` */
    oC_MemorySize_t                 SectorSize;     /**!< Size of the block / sector. It should be power of 2 */
    oC_SDMMC_PowerMode_t            PowerMode;      /**!< Card initialization power mode */
    oC_SDMMC_DetectionMode_t        DetectionMode;  /**!< Card detection mode */
    struct
    {
        oC_SPI_Context_t            SpiContext;    /**!< Keep it #NULL also in SPI mode, if the SPI should be configured automatically */
#if defined(oC_SDMMC_LLD_AVAILABLE) || defined(DOXYGEN)
        oC_SDMMC_Channel_t          Channel;       /**!< Set it to correct SDMMC channel if you wish to configure specific channel */
#endif
        uint8_t                     NumberOfRetries;        //!< Number of retries when command response has not been received (Leave 0 to use default - 3)
        oC_Time_t                   DetectionPollingPeriod; //!< Period of checking if a card has been inserted
    } Advanced;
} oC_SDMMC_Config_t;

//==========================================================================================================================================
/**
 * @brief The SDMMC context structure.
 *
 * This is the structure with dynamic allocated data for the SDMMC. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_SDMMC_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_SDMMC_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct oC_SDMMC_Context_t * oC_SDMMC_Context_t;

//==========================================================================================================================================
/**
 * @brief stores card state
 *
 * @warning values comes from spec, do not change them
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_State_Idle             = 0x00,     /**< oC_SDMMC_Cmd_State_Idle */
    oC_SDMMC_State_Ready            = 0x01,     /**< oC_SDMMC_Cmd_State_Ready */
    oC_SDMMC_State_Identification   = 0x02,     /**< oC_SDMMC_Cmd_State_Identification */
    oC_SDMMC_State_StandBy          = 0x03,     /**< oC_SDMMC_Cmd_State_StandBy */
    oC_SDMMC_State_Transfer         = 0x04,     /**< oC_SDMMC_Cmd_State_Transfer */
    oC_SDMMC_State_SendingData      = 0x05,     /**< oC_SDMMC_Cmd_State_SendingData */
    oC_SDMMC_State_ReceiveData      = 0x06,     /**< oC_SDMMC_Cmd_State_ReceiveData */
    oC_SDMMC_State_Programming      = 0x07,     /**< oC_SDMMC_Cmd_State_Programming */
    oC_SDMMC_State_Disconnect       = 0x08      /**< oC_SDMMC_Cmd_State_Disconnect */
} oC_SDMMC_State_t;

//==========================================================================================================================================
/**
 * @brief type for storing Card ID (CID)
 */
//==========================================================================================================================================
typedef struct
{
    /**
     * An 8-bit binary number that identifies the card manufacturer. The MID number is controlled, defined and allocated to a SD Memory Card
     * manufacturer by the SD-3C, LLC. This procedure is established to ensure uniqueness of the CID register.
     */
    uint8_t             ManufacturerId;
    /**
     * A 2-character ASCII string that identifies the card OEM and/or the card contents (when used as a
     * distribution media either on ROM or FLASH cards). The OID number is controlled, defined, and allo-
     * cated to a SD Memory Card manufacturer by the SD-3C, LLC. This procedure is established to ensure
     * uniqueness of the CID register.
     * Note: SD-3C, LLC licenses companies that wish to manufacture and/or sell SD Memory Cards, including but not limited
     * to flash memory, ROM, OTP, RAM, and SDIO Combo Cards.
     * SD-3C, LLC is a limited liability company established by Panasonic Corporation, SanDisk Corporation and Toshiba
     * Corporation.
     */
    char                OemApplicationId[3];
    /**
     * Product name is a 5-character string
     */
    char                ProductName[6];
    /**
     * The product revision is composed of two Binary Coded Decimal (BCD) digits, four bits each, representing an "n.m" revision number.
     * The "n" is the most significant nibble and "m" is the least significant nibble.
     * As an example, the PRV binary value field for product revision "6.2" will be: 0110 0010b
     */
    struct
    {
        uint8_t  Major;
        uint8_t  Minor;
    } ProductRevision;
    /**
     * The Serial Number is 32 bits of binary number.
     */
    uint32_t            SerialNumber;
    /**
     * The manufacturing date is composed of two hexadecimal digits, one is 8 bits representing the year(y)
     * and the other is 4 bits representing the month (m).
     * The "m" field [11:8] is the month code. 1 = January.
     * The "y" field [19:12] is the year code. 0 = 2000.
     * As an example, the binary value of the Date field for production date "April 2001" will be:
     * 00000001 0100.
     */
    struct
    {
        uint8_t         Month;
        uint8_t         Year;
    } ManufacturingDate;
} oC_SDMMC_CardId_t;

//==========================================================================================================================================
/**
 * Stores file format
 */
//==========================================================================================================================================
typedef enum
{
    oC_SDMMC_FileFormat_Unknown,            /**< Unknown file format */
    oC_SDMMC_FileFormat_PartitionTable,     /**< Hard disk-like file system with partition table */
    oC_SDMMC_FileFormat_Fat,                /**< DOS FAT (floopy-like) with boot sector only (no partition table) */
    oC_SDMMC_FileFormat_UniversalFileFormat,/**< Universal File Format */
    oC_SDMMC_FileFormat_NumberOfElements    /**< Number of definitions in this enum */
} oC_SDMMC_FileFormat_t;

//==========================================================================================================================================
/**
 *  @brief stores information about the card
 */
//==========================================================================================================================================
typedef struct
{
    oC_DiskId_t                             DiskId;                 //!< ID of the disk in the system
    oC_SDMMC_RelativeCardAddress_t          RelativeCardAddress;    //!< Relative Card Address (RCA)
    oC_SDMMC_CommunicationInterfaceId_t     CardInterface;          //!< Supported interface
    oC_SDMMC_CardId_t                       CardId;                 //!< Card ID (CID)
    oC_SDMMC_CardType_t                     CardType;               //!< Type of the card
    oC_MemorySize_t                         CardSize;               //!< Size of the card
    oC_SDMMC_FileFormat_t                   FileFormat;             //!< Format of the file system
    oC_MemorySize_t                         SectorSize;             //!< Size of a memory sector
    oC_SectorNumber_t                       NumberOfSectors;        //!< Number of sectors in the card
} oC_SDMMC_CardInfo_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_ErrorCode_t oC_SDMMC_TurnOn               ( void );
extern oC_ErrorCode_t oC_SDMMC_TurnOff              ( void );
extern bool           oC_SDMMC_IsTurnedOn           ( void );
extern oC_ErrorCode_t oC_SDMMC_Configure            ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t * outContext );
extern oC_ErrorCode_t oC_SDMMC_Unconfigure          ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t * outContext );
extern oC_ErrorCode_t oC_SDMMC_WaitForNewDisk       ( oC_SDMMC_Context_t Context , oC_DiskId_t * outDiskId, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_ReadSectorSize       ( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_MemorySize_t * outSectorSize , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_ReadNumberOfSectors  ( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t * outSectorNumber , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_ReadSectors          ( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, void *    outBuffer , oC_MemorySize_t Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_WriteSectors         ( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, const void * Buffer , oC_MemorySize_t Size , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_EraseSectors         ( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, oC_MemorySize_t Size, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_WaitForDiskEject     ( oC_SDMMC_Context_t Context , oC_DiskId_t * outDiskId, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_SDMMC_Ioctl                ( oC_SDMMC_Context_t Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t oC_SDMMC_ReadCardInfo         ( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SDMMC_CardInfo_t* outCardInfo );
extern oC_ErrorCode_t oC_SDMMC_ReadCardType         ( oC_SDMMC_Context_t Context , oC_SDMMC_CommunicationInterfaceId_t * outCardType );
extern oC_ErrorCode_t oC_SDMMC_ReadMode             ( oC_SDMMC_Context_t Context , oC_SDMMC_Mode_t * outMode , const char ** outName );
extern oC_ErrorCode_t oC_SDMMC_ReadTransferMode     ( oC_SDMMC_Context_t Context , oC_SDMMC_TransferMode_t * outTransferMode , const char ** outName );
extern oC_ErrorCode_t oC_SDMMC_SetTransferMode      ( oC_SDMMC_Context_t Context , oC_SDMMC_TransferMode_t TransferMode );
extern oC_ErrorCode_t oC_SDMMC_ReadTransferModeName ( oC_SDMMC_TransferMode_t TransferMode, const char** outName );
extern oC_ErrorCode_t oC_SDMMC_ReadTransferModeByName( const char* Name, oC_SDMMC_TransferMode_t* outTransferMode );
extern const char*    oC_SDMMC_GetCardStateName    ( oC_SDMMC_State_t State );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* _OC_SDMMC_H */
