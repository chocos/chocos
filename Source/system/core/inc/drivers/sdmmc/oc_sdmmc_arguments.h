/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_arguments.h
 *
 * @brief      Stores arguments definitions
 *
 * @author     Patryk Kubiak - (Created on: 09 04, 2022 1:25:20 AM)
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_ARGUMENTS_H_
#define SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_ARGUMENTS_H_

#ifndef oC_SDMMC_PRIVATE
#   error This file is private and cannot be used out of the SDMMC driver!
#endif

#include <stdint.h>
#include <oc_sdmmc_responses.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores argument for SEND_IF_COND command
 */
//==========================================================================================================================================
typedef struct PACKED
{
    /**
     * Check pattern - a pattern that should be returned back by the card
     */
    uint32_t        Pattern:8;

    /**
     * Voltage accepted by the card
     */
    enum {
        oC_SDMMC_Arguments_VoltageAccepted_NotDefined = 0,/**< Supported voltage is not defined */
        oC_SDMMC_Arguments_VoltageAccepted_2p7to3p6   = 1,/**< 2.7V-3.6V */
        oC_SDMMC_Arguments_VoltageAccepted_LowVoltage = 2,/**< Reserved for low voltage */
    } VoltageAccepted:4;
} oC_SDMMC_Arguments_SendIfCond_t;

//==========================================================================================================================================
/**
 * @brief stores argument for SD_SEND_OP_COND command
 */
//==========================================================================================================================================
typedef struct PACKED
{
    uint32_t                _Reserved:15;

    /**
     * @brief Voltage window used by the host
     */
    oC_SDMMC_Responses_VoltageWindow_t VoltageWindow:9;
     /**
      * @brief Switching to 1.8 Request (S18R)
      *
      * 0b - Use current signal voltage
      * 1b - Switch to 1.8V signal voltage
      */
    uint32_t                SwitchTo18Request:1;
    uint32_t                _Reserved2:5;

    /**
     * @brief SDXC Power Control (XPC)
     *
     * 0b - Power Saving Mode
     * 1b - Maximum Performance
     */
    uint32_t                SDXCPowerControl:1;
    uint32_t                _ReservedForeSD:1;

    /**
     * Set by the host if High Capacity Cards (SDHC and SDXC) can be supported. If the bit is 0, then only SDSC is supported by the host
     */
    uint32_t                HostCapacitySupport:1;
    uint32_t                _Reserved3:1;
} oC_SDMMC_Arguments_SdSendOpCond_t;

//==========================================================================================================================================
/**
 * @brief stores argument for SEND_CSD and SELECT_CARD command
 */
//==========================================================================================================================================
typedef struct PACKED
{
    uint16_t                _Reserved;
    uint16_t                CardRelativeAddress;    //!< Relative Card Address (RCA)
} oC_SDMMC_Arguments_SendCsd_t, oC_SDMMC_Arguments_SelectCard_t;

//==========================================================================================================================================
/**
 * @brief stores argument for SET_BUS_WIDTH command
 */
//==========================================================================================================================================
typedef struct PACKED
{
    enum {
        oC_SDMMC_Arguments_SetBusWidth_1Bit = 0,
        oC_SDMMC_Arguments_SetBusWidth_4Bit = 2
    } BusWidth:2;
} oC_SDMMC_Arguments_SetBusWidth_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


#endif /* SYSTEM_CORE_INC_DRIVERS_SDMMC_OC_SDMMC_ARGUMENTS_H_ */
