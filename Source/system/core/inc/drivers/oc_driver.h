/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for driver creating
 *
 * @file       oc_driver.h
 *
 * @author     Patryk Kubiak - (Created on: 18 05 2015 19:41:07)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Driver Driver's Interface
 * @ingroup Drivers
 * @brief Common interface for all the system's drivers
 * 
 ******************************************************************************************************************************************/


#ifndef INC_OC_DRIVER_H_
#define INC_OC_DRIVER_H_

#include <oc_array.h>
#include <oc_version.h>
#include <oc_errors.h>
#include <oc_null.h>
#include <oc_ioctl.h>
#include <stdint.h>
#include "stdbool.h"
#include <oc_list.h>
#include <oc_stdlib.h>
#include <oc_boot.h>
#include <oc_compiler.h>
#include <oc_1word.h>
#include <oc_pixel.h>
#include <oc_colormap.h>
#include <oc_memory.h>
#include <oc_net.h>
#include <oc_diag.h>
#include <oc_event.h>
#include <oc_debug.h>
#include <oc_idi.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Driver
//! @{

#define oC_DRIVER_ARCHITECTURE_VERSION_01       0xF1
#define oC_DRIVER_ARCHITECTURE_VERSION_02       0xF2
#define oC_DRIVER_ARCHITECTURE_VERSION          oC_DRIVER_ARCHITECTURE_VERSION_02

#define oC_DRIVER_GET_ARCHITECTURE_FROM_VERSION(VERSION)    ((VERSION >> 24) & 0xFF)

#define oC_Driver_MakeVersion(MAJOR,MINOR,PATCH)    ( (PATCH<<0) | (MINOR<<8) | (MAJOR<<16) | (oC_DRIVER_ARCHITECTURE_VERSION<<24) )

#define oC_Driver_IsVersionCorrect(VERSION)         (oC_DRIVER_GET_ARCHITECTURE_FROM_VERSION(VERSION) == oC_DRIVER_ARCHITECTURE_VERSION)
#define oC_DRIVER_CONFIG_TYPE_NAME(DRIVER_NAME)     oC_1WORD_FROM_3(oC_,DRIVER_NAME,_Config_t)

#define oC_Driver_IsDriverType(TYPE)            ( (DRIVER_TYPE) == (TYPE) )

#define STANDARD_DRIVER                         1
#define COMMUNICATION_DRIVER                    2
#define GRAPHICS_DRIVER                         3
#define NETWORK_DRIVER                          4
#define INPUT_EVENT_DRIVER                      5
#define DISK_DRIVER                          6

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @addtogroup Driver
//! @{

typedef oC_ErrorCode_t  (*oC_Driver_ConfigureFunction_t)           ( const void * Config , void ** outContext );
typedef oC_ErrorCode_t  (*oC_Driver_UnconfigureFunction_t)         ( const void * Config , void ** outContext );
typedef oC_ErrorCode_t  (*oC_Driver_ReadFunction_t)                ( void * Context , char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout  );
typedef oC_ErrorCode_t  (*oC_Driver_WriteFunction_t)               ( void * Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_HandleIoctlFunction_t)         ( void * Context , oC_Ioctl_Command_t Command , void * Data );
typedef oC_ErrorCode_t  (*oC_Driver_ReadColorMapFunction_t)        ( void * Context , oC_ColorMap_t ** outColorMap );
typedef oC_ErrorCode_t  (*oC_Driver_SetResolutionFunction_t)       ( void * Context , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height );
typedef oC_ErrorCode_t  (*oC_Driver_ReadResolutionFunction_t)      ( void * Context , oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight );
typedef oC_ErrorCode_t  (*oC_Driver_TurnFunction_t)                ( void );
typedef bool            (*oC_Driver_IsTurnedOnFunction_t)          ( void );
typedef oC_ErrorCode_t  (*oC_Driver_SendFrame_t        )           ( void * Context , const oC_Net_Frame_t * Frame , oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_ReceiveFrame_t     )           ( void * Context , oC_Net_Frame_t * outFrame , oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_SetWakeOnLanEvent_t )          ( void * Context , oC_Event_t WolEvent );
typedef oC_ErrorCode_t  (*oC_Driver_Flush_t             )          ( void * Context );
typedef oC_ErrorCode_t  (*oC_Driver_SetLoopback_t       )          ( void * Context , oC_Net_Layer_t Layer , bool Enabled );
typedef oC_ErrorCode_t  (*oC_Driver_PerformDiagnostics_t)          ( void * Context , oC_Diag_t * outDiags , uint32_t * NumberOfDiags );
typedef oC_ErrorCode_t  (*oC_Driver_ReadNetInfo_t)                 ( void * Context , oC_Net_Info_t * outInfo );
typedef oC_ErrorCode_t  (*oC_Driver_WaitForInput_t)                ( void * Context , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent , oC_Time_t Timeout );
typedef bool            (*oC_Driver_IsEventSupported_t)            ( void * Context , oC_IDI_EventId_t EventId );
typedef oC_ErrorCode_t  (*oC_Driver_SwitchLayer_t)                 ( void * Context , oC_ColorMap_LayerIndex_t Layer );

typedef oC_ErrorCode_t  (*oC_Driver_WaitForNewDiskFunction_t)   ( void * Context , oC_DiskId_t * outDiskId, oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_ReadSectorSizeFunction_t)      ( void * Context , oC_DiskId_t DiskId , oC_MemorySize_t * outSectorSize , oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_ReadNumberOfSectorsFunction_t) ( void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t * outSectorNumber , oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_ReadSectorsFunction_t)         ( void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, void *    outBuffer , oC_MemorySize_t Size , oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_WriteSectorsFunction_t)        ( void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, const void * Buffer , oC_MemorySize_t Size , oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_EraseSectorsFunction_t)        ( void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, oC_MemorySize_t Size, oC_Time_t Timeout );
typedef oC_ErrorCode_t  (*oC_Driver_WaitForDiskEjectFunction_t) ( void * Context , oC_DiskId_t * outDiskId , oC_Time_t Timeout );

typedef enum
{
    oC_Driver_Type_Incorrect        = 0 ,
    oC_Driver_Type_Standard         = STANDARD_DRIVER,
    oC_Driver_Type_Communication    = COMMUNICATION_DRIVER,
    oC_Driver_Type_Graphics         = GRAPHICS_DRIVER,
    oC_Driver_Type_Network          = NETWORK_DRIVER,
    oC_Driver_Type_InputEvent       = INPUT_EVENT_DRIVER,
    oC_Driver_Type_DiskDriver    = DISK_DRIVER ,
} oC_Driver_Type_t;

typedef struct _oC_Driver_Registration_t
{
    char *                                              FileName;
    oC_MemorySize_t                                     ConfigurationSize;
    const struct _oC_Driver_Registration_t * const *    RequiredList;
    uint16_t                                            RequiredCount;
    oC_Boot_Level_t                                     RequiredBootLevel;
    uint32_t                                            Version;
    oC_Driver_Type_t                                    Type;
    oC_Driver_ConfigureFunction_t                       Configure;
    oC_Driver_UnconfigureFunction_t                     Unconfigure;
    oC_Driver_TurnFunction_t                            TurnOn;
    oC_Driver_TurnFunction_t                            TurnOff;
    oC_Driver_IsTurnedOnFunction_t                      IsTurnedOn;
    oC_Driver_ReadFunction_t                            Read;
    oC_Driver_WriteFunction_t                           Write;
    oC_Driver_HandleIoctlFunction_t                     HandleIoctl;
    oC_Driver_ReadColorMapFunction_t                    ReadColorMap;
    oC_Driver_SetResolutionFunction_t                   SetResolution;
    oC_Driver_ReadResolutionFunction_t                  ReadResolution;
    oC_Driver_SendFrame_t                               SendFrame;
    oC_Driver_ReceiveFrame_t                            ReceiveFrame;
    oC_Driver_SetWakeOnLanEvent_t                       SetWakeOnLanEvent;
    oC_Driver_Flush_t                                   Flush;
    oC_Driver_SetLoopback_t                             SetLoopback;
    oC_Driver_PerformDiagnostics_t                      PerformDiagnostics;
    oC_Driver_ReadNetInfo_t                             ReadNetInfo;
    oC_Driver_WaitForInput_t                            WaitForInput;
    oC_Driver_IsEventSupported_t                        IsEventSupported;
    oC_Driver_SwitchLayer_t                             SwitchLayer;

    oC_Driver_WaitForNewDiskFunction_t               WaitForNewDisk;
    oC_Driver_ReadSectorSizeFunction_t                  ReadSectorSize;
    oC_Driver_ReadNumberOfSectorsFunction_t             ReadNumberOfSectors;
    oC_Driver_ReadSectorsFunction_t                     ReadSectors;
    oC_Driver_WriteSectorsFunction_t                    WriteSectors;
    oC_Driver_EraseSectorsFunction_t                    EraseSectors;
    oC_Driver_WaitForDiskEjectFunction_t             WaitForDiskEject;
} oC_Driver_Registration_t;

typedef struct Context_t * oC_Driver_Context_t;
typedef const void * oC_Driver_Config_t;
typedef const oC_Driver_Registration_t * oC_Driver_t;
typedef const struct _oC_Driver_Registration_t * const * oC_Driver_RequiredList_t;
typedef oC_ErrorCode_t (*oC_Driver_InstanceAddedCallback_t)( oC_Driver_t Driver, oC_Driver_Config_t Config, oC_Driver_Context_t Context );
typedef oC_ErrorCode_t (*oC_Driver_InstanceRemovedCallback_t)( oC_Driver_t Driver, oC_Driver_Context_t Context );

#undef  _________________________________________TYPES_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup Driver
//! @{

#define oC_Driver_BeginRequireList      static const oC_Driver_Registration_t * const __RequiredList[] = {

#define oC_Driver_EndRequireList        }

#define oC_Driver_DefineDriver          const oC_Driver_Registration_t DRIVER_NAME = {\
                                                                                      .FileName          = DRIVER_FILE_NAME, \
                                                                                      .ConfigurationSize = sizeof(oC_DRIVER_CONFIG_TYPE_NAME(DRIVER_NAME)),\
                                                                                      .RequiredList      = __RequiredList, \
                                                                                      .RequiredCount     = oC_ARRAY_SIZE(__RequiredList), \
                                                                                      .RequiredBootLevel = REQUIRED_BOOT_LEVEL, \
                                                                                      .Version           = DRIVER_VERSION ,\
                                                                                      .TurnOn            = (oC_Driver_TurnFunction_t)DRIVER_TURN_ON  ,\
                                                                                      .TurnOff           = (oC_Driver_TurnFunction_t)DRIVER_TURN_OFF ,\
                                                                                      .IsTurnedOn        = (oC_Driver_IsTurnedOnFunction_t)IS_TURNED_ON, \
                                                                                      .Read              = (oC_Driver_ReadFunction_t)READ_FROM_DRIVER ,\
                                                                                      .Write             = (oC_Driver_WriteFunction_t)WRITE_TO_DRIVER ,\
                                                                                      .HandleIoctl       = (oC_Driver_HandleIoctlFunction_t)HANDLE_IOCTL ,\
                                                                                      .Configure         = (oC_Driver_ConfigureFunction_t)DRIVER_CONFIGURE ,\
                                                                                      .Unconfigure       = (oC_Driver_UnconfigureFunction_t)DRIVER_UNCONFIGURE ,\
                                                                                      .ReadColorMap      = (oC_Driver_ReadColorMapFunction_t)READ_COLOR_MAP, \
                                                                                      .SetResolution     = (oC_Driver_SetResolutionFunction_t)SET_RESOLUTION, \
                                                                                      .ReadResolution    = (oC_Driver_ReadResolutionFunction_t)READ_RESOLUTION , \
                                                                                      .SendFrame         = (oC_Driver_SendFrame_t          ) SEND_FRAME ,\
                                                                                      .ReceiveFrame      = (oC_Driver_ReceiveFrame_t       ) RECEIVE_FRAME ,\
                                                                                      .SetWakeOnLanEvent = (oC_Driver_SetWakeOnLanEvent_t   ) SET_WAKE_ON_LAN_EVENT ,\
                                                                                      .Flush             = (oC_Driver_Flush_t               ) FLUSH ,\
                                                                                      .SetLoopback       = (oC_Driver_SetLoopback_t         ) SET_LOOPBACK ,\
                                                                                      .PerformDiagnostics= (oC_Driver_PerformDiagnostics_t  ) PERFORM_DIAGNOSTIC ,\
                                                                                      .ReadNetInfo       = (oC_Driver_ReadNetInfo_t         ) READ_NET_INFO, \
                                                                                      .WaitForInput      = (oC_Driver_WaitForInput_t        ) WAIT_FOR_INPUT , \
                                                                                      .IsEventSupported  = (oC_Driver_IsEventSupported_t    ) IS_EVENT_SUPPORTED , \
                                                                                      .SwitchLayer       = (oC_Driver_SwitchLayer_t         ) SWITCH_LAYER , \
                                                                                      .WaitForNewDisk = (oC_Driver_WaitForNewDiskFunction_t) WAIT_FOR_NEW_DISK,\
                                                                                      .ReadSectorSize    = (oC_Driver_ReadSectorSizeFunction_t) READ_SECTOR_SIZE, \
                                                                                      .ReadNumberOfSectors=(oC_Driver_ReadNumberOfSectorsFunction_t) READ_NUMBER_OF_SECTORS , \
                                                                                      .ReadSectors       = (oC_Driver_ReadSectorsFunction_t) READ_SECTORS, \
                                                                                      .WriteSectors      = (oC_Driver_WriteSectorsFunction_t) WRITE_SECTORS , \
                                                                                      .EraseSectors      = (oC_Driver_EraseSectorsFunction_t) ERASE_SECTORS, \
                                                                                      .WaitForDiskEject=(oC_Driver_WaitForDiskEjectFunction_t) WAIT_FOR_DISK_EJECT, \
                                                                                      .Type              = DRIVER_TYPE , \
                                                                                     }


#define oC_Driver_DeclareDriver         extern const oC_Driver_Registration_t DRIVER_NAME
#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with function prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @addtogroup Driver
//! @{

extern bool                 oC_Driver_IsDriverRequiredBy( oC_Driver_t Driver , oC_Driver_t RequiredDriver );
extern oC_Driver_Type_t     oC_Driver_GetDriverType     ( oC_Driver_t Driver );
extern bool                 oC_Driver_ReadRequiredList  ( oC_Driver_t Driver , oC_Driver_RequiredList_t * outRequiredList , uint16_t * outRequiredListCount );
extern oC_ErrorCode_t       oC_Driver_TurnOn            ( oC_Driver_t Driver );
extern oC_ErrorCode_t       oC_Driver_TurnOff           ( oC_Driver_t Driver );
extern bool                 oC_Driver_IsTurnedOn        ( oC_Driver_t Driver );
extern oC_ErrorCode_t       oC_Driver_Configure         ( oC_Driver_t Driver , const void * Config , void ** outContext );
extern oC_ErrorCode_t       oC_Driver_Unconfigure       ( oC_Driver_t Driver , const void * Config , void ** outContext );
extern oC_ErrorCode_t       oC_Driver_Read              ( oC_Driver_t Driver , void * Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_Write             ( oC_Driver_t Driver , void * Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_HandleIoctl       ( oC_Driver_t Driver , void * Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t       oC_Driver_ReadColorMap      ( oC_Driver_t Driver , void * Context , oC_ColorMap_t ** outColorMap );
extern oC_ErrorCode_t       oC_Driver_SetResolution     ( oC_Driver_t Driver , void * Context , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height );
extern oC_ErrorCode_t       oC_Driver_ReadResolution    ( oC_Driver_t Driver , void * Context , oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight );
extern oC_ErrorCode_t       oC_Driver_SendFrame         ( oC_Driver_t Driver , void * Context , const oC_Net_Frame_t * Frame , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_ReceiveFrame      ( oC_Driver_t Driver , void * Context , oC_Net_Frame_t * outFrame , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_SetWakeOnLanEvent ( oC_Driver_t Driver , void * Context , oC_Event_t WolEvent );
extern oC_ErrorCode_t       oC_Driver_Flush             ( oC_Driver_t Driver , void * Context );
extern oC_ErrorCode_t       oC_Driver_SetLoopback       ( oC_Driver_t Driver , void * Context , oC_Net_Layer_t Layer , bool Enabled );
extern oC_ErrorCode_t       oC_Driver_PerformDiagnostics( oC_Driver_t Driver , void * Context , oC_Diag_t * outDiags , uint32_t * NumberOfDiags );
extern oC_ErrorCode_t       oC_Driver_ReadNetInfo       ( oC_Driver_t Driver , void * Context , oC_Net_Info_t * outInfo );
extern oC_ErrorCode_t       oC_Driver_WaitForInput      ( oC_Driver_t Driver , void * Context , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent , oC_Time_t Timeout );
extern bool                 oC_Driver_IsEventSupported  ( oC_Driver_t Driver , void * Context , oC_IDI_EventId_t EventId );
extern oC_ErrorCode_t       oC_Driver_SwitchLayer       ( oC_Driver_t Driver , void * Context , oC_ColorMap_LayerIndex_t Layer );
extern oC_ErrorCode_t       oC_Driver_WaitForNewDisk     ( oC_Driver_t Driver , void * Context , oC_DiskId_t * outDiskId , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_ReadSectorSize     ( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_MemorySize_t * outSectorSize , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_ReadNumberOfSectors( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t * outSectorNumber , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_ReadSectors        ( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, void *    outBuffer , oC_MemorySize_t Size , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_WriteSectors       ( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, const void * Buffer , oC_MemorySize_t Size , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_EraseSectors       ( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, oC_MemorySize_t Size, oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_WaitForDiskEject   ( oC_Driver_t Driver , void * Context , oC_DiskId_t * outDiskId , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Driver_SetInstanceAddedCallback      ( oC_Driver_InstanceAddedCallback_t     Callback );
extern oC_ErrorCode_t       oC_Driver_SetInstanceRemovedCallback    ( oC_Driver_InstanceRemovedCallback_t   Callback );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
//! @}

#endif /* INC_OC_DRIVER_H_ */

#if defined(DRIVER_HEADER) || defined(DRIVER_SOURCE) || defined(DRIVER_PRIVATE)
#   ifndef DRIVER_NAME
#       error  DRIVER_NAME is not defined! You must define it first
#   endif
#   if defined(DRIVER_HEADER) && defined(DRIVER_SOURCE)
#       error DRIVER_HEADER and DRIVER_SOURCE are both defined. You must define only one of them at once. Use #undef DRIVER_HEADER before second include of the oc_driver.h file.
#   elif defined(DRIVER_HEADER)
oC_Driver_DeclareDriver;
#undef DRIVER_HEADER
#   elif defined(DRIVER_PRIVATE)
#       if DRIVER_DEBUGLOG == true
#           define driverlog(LogType,...)       kdebuglog( LogType, DRIVER_FILE_NAME ": " __VA_ARGS__ )
#       else
#           define driverlog(LogType,...)
#       endif
#       undef DRIVER_PRIVATE
#       undef DRIVER_DEBUGLOG
#   elif defined(DRIVER_SOURCE)

#       if !defined(DRIVER_VERSION)
#           error DRIVER_VERSION is not defined! Define it using oC_Driver_MakeVersion definition
#       elif !oC_Driver_IsVersionCorrect(DRIVER_VERSION)
#           error DRIVER_VERSION is not correctly defined! You must use oC_Driver_MakeVersion macro to define the driver version
#       elif !defined(REQUIRED_DRIVERS)
#           error REQUIRED_DRIVERS is not defined! Define it as list of drivers, that is required by this driver separated by comma. If the driver does not require anything, just define it as empty
#       elif !defined(DRIVER_FILE_NAME)
#           error DRIVER_FILE_NAME is not define! Define it as string with name of the file that will be created for this driver. Note, that it is not a path!
#       elif !defined(REQUIRED_BOOT_LEVEL)
#           error REQUIRED_BOOT_LEVEL is not defined! Define it as a oC_Boot_Level_t type value (for example as oC_Boot_Level_0)
#       else

#       ifndef DRIVER_TYPE
#           define DRIVER_TYPE              oC_Driver_Type_Standard
#       endif

/* Checking driver interface */
#       ifndef DRIVER_CONFIGURE
#           error DRIVER_CONFIGURE is not defined! Each driver has to have defined configure function!
#           define DRIVER_CONFIGURE    NULL
#       endif
#       ifndef DRIVER_UNCONFIGURE
#           error DRIVER_UNCONFIGURE is not defined! Each driver has to have defined unconfigure function!
#           define DRIVER_UNCONFIGURE  NULL
#       endif
#       ifndef DRIVER_TURN_ON
#           define DRIVER_TURN_ON      NULL
#       elif !defined(IS_TURNED_ON)
#           error IS_TURNED_ON is not defined! When the DRIVER_TURN_ON function is set, it is required to define also IS_TURNED_ON function!
#       endif
#       ifndef DRIVER_TURN_OFF
#           define DRIVER_TURN_OFF     NULL
#       elif !defined(IS_TURNED_ON)
#           error IS_TURNED_ON is not defined! When the DRIVER_TURN_OFF function is set, it is required to define also IS_TURNED_ON function!
#       endif
#       ifndef READ_FROM_DRIVER
#           if oC_Driver_IsDriverType(COMMUNICATION_DRIVER)
#               error READ_FROM_DRIVER function is not defined (required for communication drivers)
#           endif
#           define READ_FROM_DRIVER    NULL
#       endif
#       ifndef WRITE_TO_DRIVER
#           if oC_Driver_IsDriverType(COMMUNICATION_DRIVER)
#               error WRITE_TO_DRIVER function is not defined (required for communication drivers)
#           endif
#           define WRITE_TO_DRIVER     NULL
#       endif
#       ifndef READ_COLOR_MAP
#           if oC_Driver_IsDriverType(GRAPHICS_DRIVER)
#               error READ_COLOR_MAP function required for graphics drivers is not defined!
#           endif
#           define READ_COLOR_MAP         NULL
#       endif
#       ifndef SET_RESOLUTION
#           if oC_Driver_IsDriverType(GRAPHICS_DRIVER)
#               error SET_RESOLUTION function required for graphics drivers is not defined!
#           endif
#           define SET_RESOLUTION         NULL
#       endif
#       ifndef READ_RESOLUTION
#           if oC_Driver_IsDriverType(GRAPHICS_DRIVER)
#               error READ_RESOLUTION function required for graphics drivers is not defined!
#           endif
#           define READ_RESOLUTION         NULL
#       endif
#       ifndef SWITCH_LAYER
#           if oC_Driver_IsDriverType(GRAPHICS_DRIVER)
#               error SWITCH_LAYER function required for graphics drivers is not defined!
#           endif
#           define SWITCH_LAYER         NULL
#       endif
#       ifndef HANDLE_IOCTL
#           define HANDLE_IOCTL        NULL
#       endif
#       ifndef IS_TURNED_ON
#           define IS_TURNED_ON        NULL
#       endif
#       ifndef SEND_FRAME
#           if oC_Driver_IsDriverType(NETWORK_DRIVER)
#               error SEND_FRAME function required for network drivers is not defined!
#           endif
#           define SEND_FRAME         NULL
#       endif
#       ifndef RECEIVE_FRAME
#           if oC_Driver_IsDriverType(NETWORK_DRIVER)
#               error RECEIVE_FRAME function required for network drivers is not defined!
#           endif
#           define RECEIVE_FRAME         NULL
#       endif
#       ifndef READ_NET_INFO
#           if oC_Driver_IsDriverType(NETWORK_DRIVER)
#               error READ_NET_INFO function required for network drivers is not defined!
#           endif
#           define READ_NET_INFO      NULL
#       endif
#       ifndef SET_WAKE_ON_LAN_EVENT
#           define SET_WAKE_ON_LAN_EVENT    NULL
#       endif
#       ifndef FLUSH
#           define FLUSH                    NULL
#       endif
#       ifndef SET_LOOPBACK
#           define SET_LOOPBACK             NULL
#       endif
#       ifndef PERFORM_DIAGNOSTIC
#           if oC_Driver_IsDriverType(NETWORK_DRIVER)
#               error PERFORM_DIAGNOSTIC function required for network drivers is not defined!
#           endif
#           define PERFORM_DIAGNOSTIC         NULL
#       endif
#       ifndef WAIT_FOR_INPUT
#           if oC_Driver_IsDriverType(INPUT_EVENT_DRIVER)
#               error WAIT_FOR_INPUT function required for input event drivers is not defined!
#           endif
#           define WAIT_FOR_INPUT             NULL
#       endif
#       ifndef IS_EVENT_SUPPORTED
#           if oC_Driver_IsDriverType(INPUT_EVENT_DRIVER)
#               error IS_EVENT_SUPPORTED function required for input event drivers is not defined!
#           endif
#           define IS_EVENT_SUPPORTED         NULL
#       endif
#       ifndef WAIT_FOR_NEW_DISK
#           if oC_Driver_IsDriverType(DISK_DRIVER)
#               error WAIT_FOR_NEW_DISK function required for disk drivers is not defined
#           endif
#           define WAIT_FOR_NEW_DISK     NULL
#       endif
#       ifndef READ_SECTOR_SIZE
#           if oC_Driver_IsDriverType(DISK_DRIVER)
#               error READ_SECTOR_SIZE function required for disk drivers is not defined
#           endif
#           define READ_SECTOR_SIZE     NULL
#       endif
#       ifndef READ_NUMBER_OF_SECTORS
#           if oC_Driver_IsDriverType(DISK_DRIVER)
#               error READ_NUMBER_OF_SECTORS function required for disk drivers is not defined
#           endif
#           define READ_NUMBER_OF_SECTORS NULL
#       endif
#       ifndef READ_SECTORS
#           if oC_Driver_IsDriverType(DISK_DRIVER)
#               error READ_SECTORS function required for disk drivers is not defined
#           endif
#           define READ_SECTORS NULL
#       endif
#       ifndef WRITE_SECTORS
#           if oC_Driver_IsDriverType(DISK_DRIVER)
#               error WRITE_SECTORS function required for disk drivers is not defined
#           endif
#           define WRITE_SECTORS    NULL
#       endif
#       ifndef ERASE_SECTORS
#           if oC_Driver_IsDriverType(DISK_DRIVER)
#               error ERASE_SECTORS function required for disk drivers is not defined
#           endif
#           define ERASE_SECTORS    NULL
#       endif
#       ifndef WAIT_FOR_DISK_EJECT
#           if oC_Driver_IsDriverType(DISK_DRIVER)
#               error WAIT_FOR_DISK_EJECT function required for disk drivers is not defined
#           endif
#           define WAIT_FOR_DISK_EJECT    NULL
#       endif
#       ifndef DRIVER_DEBUGLOG
#           define DRIVER_DEBUGLOG            false
#       endif
#       if DRIVER_DEBUGLOG == true
#           define driverlog(LogType,...)       kdebuglog( LogType, DRIVER_FILE_NAME ": " __VA_ARGS__ )
#       else
#           define driverlog(LogType,...)
#       endif

oC_Driver_BeginRequireList
REQUIRED_DRIVERS
oC_Driver_EndRequireList;
oC_Driver_DefineDriver;
#endif
#undef DRIVER_SOURCE
#undef DRIVER_VERSION
#undef REQUIRED_DRIVERS
//#undef DRIVER_FILE_NAME
#undef DRIVER_TURN_ON
#undef DRIVER_TURN_OFF
#undef READ_FROM_DRIVER
#undef WRITE_TO_DRIVER
#undef HANDLE_IOCTL
#undef READ_CONTEXT
#undef IS_TURNED_ON
#undef DRIVER_CONFIGURE
#undef READ_COLOR_MAP
#undef SET_RESOLUTION
#undef READ_RESOLUTION
#undef SEND_FRAME
#undef RECEIVE_FRAME
#undef SET_WAKE_ON_LAN_EVENT
#undef FLUSH
#undef SET_LOOPBACK
#undef PERFORM_DIAGNOSTIC
#undef DRIVER_TYPE
#undef DRIVER_DEBUGLOG
#undef WAIT_FOR_NEW_DISK
#undef READ_SECTOR_SIZE
#undef READ_NUMBER_OF_SECTORS
#undef READ_SECTORS
#undef WRITE_SECTORS
#undef ERASE_SECTORS
#undef WAIT_FOR_DISK_EJECT
#endif
#undef DRIVER_NAME
#endif
