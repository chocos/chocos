/** ****************************************************************************************************************************************
 *
 * @file       oc_lcdtft.h
 * 
 * File based on driver.h Ver 1.1.0
 *
 * @brief      The file with interface for LCDTFT driver
 *
 * @author     Patryk Kubiak - (Created on: 2016-02-06 - 11:43:03)
 *
 * @copyright  Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup LCDTFT LCD-TFT
 * @ingroup Drivers
 * @brief LCD-TFT - managing and configuring LCD-TFT displays.
 *
 *
 *
 ******************************************************************************************************************************************/
#ifndef _OC_LCDTFT_H
#define _OC_LCDTFT_H
#define DRIVER_HEADER
#define DRIVER_NAME                 LCDTFT

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>
#include <oc_lcdtft_lld.h>
#include <oc_stdtypes.h>
#include <oc_color.h>
#include <oc_font.h>
#include <oc_pixel.h>
#include <oc_frequency.h>

#ifdef oC_LCDTFT_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________



#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________


typedef enum
{
    oC_LCDTFT_PixelClockPolarity_InputPixelClock   = oC_LCDTFT_LLD_PixelClockPolarity_InputPixelClock,
    oC_LCDTFT_PixelClockPolarity_InvertedInputPixe = oC_LCDTFT_LLD_PixelClockPolarity_InvertedInputPixelClock
} oC_LCDTFT_PixelClockPolarity_t;

typedef enum
{
    oC_LCDTFT_Polarity_ActiveLow  = oC_LCDTFT_LLD_Polarity_ActiveLow ,
    oC_LCDTFT_Polarity_ActiveHigh = oC_LCDTFT_LLD_Polarity_ActiveHigh
} oC_LCDTFT_Polarity_t;

//==========================================================================================================================================
/**
 * @brief LCDTFT driver configuration structure
 *
 * This is the configuration structure for the LCDTFT driver. You should fill all fields or set to 0 all that are not required.
 * To use this structure call the #oC_LCDTFT_Configure function
 *
 * @note When the structure is defined as constant, the unused fields must not be filled, cause there will be set to 0 as default.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pixel_ResolutionUInt_t           Width;
    oC_Pixel_ResolutionUInt_t           Height;
    oC_ColorFormat_t                    ColorFormat;
    oC_Frequency_t                      ClockFrequency;
    oC_Frequency_t                      PermissibleDifference;
    oC_LCDTFT_Polarity_t                HSyncPolarity;
    oC_LCDTFT_Polarity_t                VSyncPolarity;
    oC_LCDTFT_Polarity_t                DESyncPolarity;
    oC_LCDTFT_PixelClockPolarity_t      PixelClockPolarity;
    oC_LCDTFT_LLD_TimingParameters_t    TimingParameters;
    oC_LCDTFT_LLD_Pins_t                Pins;
    const oC_ColorMap_t * const         ColorMap;
} oC_LCDTFT_Config_t;

//==========================================================================================================================================
/**
 * @brief The LCDTFT context structure.
 *
 * This is the structure with dynamic allocated data for the LCDTFT. It stores a HANDLE for a driver and it can be used to identify the driver
 * context. You should get this pointer from the #oC_LCDTFT_Configure function, but note, that not all drivers use it. In many cases it is just
 * not needed, and it just will store NULL then. You should keep this pointer as long as it is necessary for you, and when it will not be
 * anymore, you should call #oC_LCDTFT_Unconfigure function to destroy it.
 */
//==========================================================================================================================================
typedef struct Context_t * oC_LCDTFT_Context_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/** @addtogroup LCDTFT
 * @{ */

extern oC_ErrorCode_t oC_LCDTFT_TurnOn                  ( void );
extern oC_ErrorCode_t oC_LCDTFT_TurnOff                 ( void );
extern bool           oC_LCDTFT_IsTurnedOn              ( void );
extern oC_ErrorCode_t oC_LCDTFT_Configure               ( const oC_LCDTFT_Config_t * Config , oC_LCDTFT_Context_t * outContext );
extern oC_ErrorCode_t oC_LCDTFT_Unconfigure             ( const oC_LCDTFT_Config_t * Config , oC_LCDTFT_Context_t * outContext );
extern oC_ErrorCode_t oC_LCDTFT_Ioctl                   ( oC_LCDTFT_Context_t Context , oC_Ioctl_Command_t Command , void * Data );
extern oC_ErrorCode_t oC_LCDTFT_SetBackgroundColor      ( oC_LCDTFT_Context_t Context , oC_Color_t      Color , oC_ColorFormat_t ColorFormat );
extern oC_ErrorCode_t oC_LCDTFT_ReadBackgroundColor     ( oC_LCDTFT_Context_t Context , oC_Color_t * outColor , oC_ColorFormat_t ColorFormat );
extern oC_ErrorCode_t oC_LCDTFT_ReadColorMap            ( oC_LCDTFT_Context_t Context , oC_ColorMap_t ** outColorMap );
extern oC_ErrorCode_t oC_LCDTFT_SetResolution           ( oC_LCDTFT_Context_t Context , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height );
extern oC_ErrorCode_t oC_LCDTFT_ReadResolution          ( oC_LCDTFT_Context_t Context , oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight );
extern oC_ErrorCode_t oC_LCDTFT_SwitchLayer             ( oC_LCDTFT_Context_t Context , oC_ColorMap_LayerIndex_t Layer );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/** @} */

#endif
#endif /* _OC_LCDTFT_H */
