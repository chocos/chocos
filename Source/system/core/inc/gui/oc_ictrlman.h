/** ****************************************************************************************************************************************
 *
 * @brief      Contains interface for ICtrlMan module
 * 
 * @file          oc_ictrlman.h
 *
 * @author     Patryk Kubiak - (Created on: 04.05.2017 19:22:13) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ICtrlMan   ICtrlMan - Input Control Manager Module
 * @ingroup GUI
 * @brief Handles input controlers
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_GUI_OC_ICTRLMAN_H_
#define SYSTEM_CORE_INC_GUI_OC_ICTRLMAN_H_

#include <oc_ictrl.h>
#include <oc_screen.h>
#include <oc_list.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup ICtrlMan
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores information about the activity
 */
//==========================================================================================================================================
typedef struct oC_ICtrlMan_Activity_t
{
    oC_IDI_EventId_t            EventsMask;     //!< the mask of events
    oC_Screen_t                 Screen;         //!< the screen where the activity is
    oC_Pixel_Position_t         Position;       //!< the position of the activity
    oC_Pixel_ResolutionUInt_t   Width;          //!< the width of the activity
    oC_Pixel_ResolutionUInt_t   Height;         //!< the height of the activity
} oC_ICtrlMan_Activity_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
/** @} */


/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
/**
 * @addtogroup ICtrlMan
 * @{
 */

extern oC_ErrorCode_t               oC_ICtrlMan_TurnOnModule            ( void );
extern oC_ErrorCode_t               oC_ICtrlMan_TurnOffModule           ( void );
extern void                         oC_ICtrlMan_ConfigureAllPossible    ( void );
extern void                         oC_ICtrlMan_UnconfigureAll          ( void );
extern oC_List(oC_ICtrl_t)          oC_ICtrlMan_GetList                 ( void );
extern oC_ErrorCode_t               oC_ICtrlMan_AddICtrl                ( oC_ICtrl_t ICtrl );
extern oC_ErrorCode_t               oC_ICtrlMan_RemoveICtrl             ( oC_ICtrl_t ICtrl );
extern oC_ICtrl_t                   oC_ICtrlMan_GetICtrl                ( oC_Screen_t Screen );
extern oC_ErrorCode_t               oC_ICtrlMan_WaitForActivity         ( oC_ICtrlMan_Activity_t * Activity , oC_IDI_Event_t * outEvent, oC_Time_t Timeout );
extern oC_ErrorCode_t               oC_ICtrlMan_RegisterActivity        ( oC_ICtrlMan_Activity_t * Activity , oC_IDI_Event_t * outEvent );
extern oC_ErrorCode_t               oC_ICtrlMan_UnregisterActivity      ( oC_ICtrlMan_Activity_t * Activity );
extern bool                         oC_ICtrlMan_HasActivityOccurred     ( oC_ICtrlMan_Activity_t * Activity );
extern bool                         oC_ICtrlMan_WaitForActivityFinish   ( oC_ICtrlMan_Activity_t * Activity , oC_Time_t Timeout );
extern oC_ErrorCode_t               oC_ICtrlMan_ReadCursorPosition      ( oC_Screen_t * outScreen , oC_Pixel_Position_t * outPosition );
extern oC_ErrorCode_t               oC_ICtrlMan_WaitForAnyNewActivity   ( oC_Time_t Timeout );
extern oC_ErrorCode_t               oC_ICtrlMan_WaitForRawEvent         ( oC_Screen_t Screen , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent, oC_Time_t Timeout , oC_Time_t GesturesWindow );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
/** @} */


#endif /* SYSTEM_CORE_INC_GUI_OC_ICTRLMAN_H_ */
