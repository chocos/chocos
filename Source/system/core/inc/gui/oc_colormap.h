/** ****************************************************************************************************************************************
 *
 * @brief      The file contains the definition of the color map structure and functions for drawing on it.
 *
 * @file       oc_colormap.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ColorMap ColorMap
 * @ingroup GUI
 * 
 * @brief Allows for drawing on the color map
 * 
 * The module contains the structure for storing color maps. The color map is a two-dimensional array of colors, can have multiple layers. 
 * The active layer is the layer that is currently used for drawing. 
 * 
 * It is possible to both - use the static color map (created with #oC_ColorMap_Define macro) and the dynamic color map (created with #oC_ColorMap_New function).
 * When the color map is no longer needed, it should be deleted with the #oC_ColorMap_Delete function (applies only to the dynamic color maps).
 * 
 * The best way to handle the color map is to draw on a layer that is not the active layer, and then switch the layers - thanks to that 
 * the user experience will be better (the user will not see the drawing process).
 * 
 * @warning
 * To avoid blinking of the screen, it is recommended to draw on the layer that is not the active layer in the screen, and then switch the layers.
 * 
 * <b>Example:</b>
 * @code{.c}
  
#include <oc_colormap.h>
#include <oc_screen.h>
#include <stdio.h>

int main()
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;
    oC_ColorMap_t*  colorMap  = NULL;

    // get the default screen from the screen manager
    oC_Screen_t screen = oC_ScreenMan_GetDefaultScreen();

    // read the color map from the screen
    if(ErrorCode( oC_Screen_ReadColorMap(screen, &colorMap) ))
    {
        // Get an inactive layer for drawing
        oC_ColorMap_LayerIndex_t drawLayer      = oC_ColorMap_GetInactiveLayer(ColorMap);

        // Switch the active layer in the color map
        oC_ColorMap_SwitchLayer(ColorMap, drawLayer);

        // color map is ready to draw
        oC_ColorMap_DrawRectangle(colorMap, 10, 10, 50, 50, oC_Color_Red);

        // Switch the layer in the screen
        oC_Screen_SwitchLayer(screen, drawLayer);
    }
    
    return errorCode;
}
 * @endcode
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_GUI_OC_COLORMAP_H_
#define SYSTEM_CORE_INC_GUI_OC_COLORMAP_H_

#include <oc_color.h>
#include <oc_pixel.h>
#include <oc_stdtypes.h>
#include <oc_assert.h>
#include <oc_stdlib.h>
#include <oc_font.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores magic number
 *
 * The magic number is for checking if the color map is valid.
 */
//==========================================================================================================================================
#define oC_ColorMap_MagicNumber     0xFEADBABA

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup ColorMap
//! @{

//==========================================================================================================================================
/**
 * @brief stores index of the layer
 */
//=========================================================================================================================================
typedef uint8_t oC_ColorMap_LayerIndex_t;

//==========================================================================================================================================
/**
 * @brief stores color map structure
 *
 * The structure is for storing the color map. The color map is a two-dimensional array of colors. 
 * The color map can have multiple layers. The active layer is the layer that is currently used for drawing. 
 * The color map can be read-only or read-write. The color map can have different color formats.
 */
//==========================================================================================================================================
typedef struct
{
    union
    {
        const void *            Map;                    //!< The pointer to the color map
        const uint8_t *         ReadMap;                //!< The pointer to the read-only color map
        uint8_t *               WriteMap;               //!< The pointer to the read-write color map
        void *                  GenericWriteMap;        //!< The pointer to the generic write color map
    };
    void **                     Layers;                 //!< The array of layers
    oC_ColorMap_LayerIndex_t    NumberOfLayers;         //!< The number of layers
    oC_ColorMap_LayerIndex_t    ActiveLayer;            //!< The active layer index
    oC_Pixel_ResolutionUInt_t   Width;                  //!< The width of the color map
    oC_Pixel_ResolutionUInt_t   Height;                 //!< The height of the color map
    oC_ColorFormat_t            ColorFormat;            //!< The color format of the color map
    uint32_t                    MagicNumber;            //!< The magic number for checking if the color map is valid
    uint32_t                    FormatSize;             //!< The size of the color format
} oC_ColorMap_t;

//==========================================================================================================================================
/**
 * @brief stores text align
 *
 * The type is for storing text align, for example, in the function #oC_ColorMap_DrawString.
 */
//==========================================================================================================================================
typedef enum
{
    oC_ColorMap_TextAlign_Left ,            //!< Text align left
    oC_ColorMap_TextAlign_Right ,           //!< Text align right
    oC_ColorMap_TextAlign_Center            //!< Text align center
} oC_ColorMap_TextAlign_t;

//==========================================================================================================================================
/**
 * @brief stores vertical text align
 *
 * The type is for storing vertical text align, for example, in the function #oC_ColorMap_DrawString.
 */
//==========================================================================================================================================
typedef enum
{
    oC_ColorMap_VerticalTextAlign_Up ,          //!< Vertical text align up
    oC_ColorMap_VerticalTextAlign_Down ,        //!< Vertical text align down
    oC_ColorMap_VerticalTextAlign_Center        //!< Vertical text align center
} oC_ColorMap_VerticalTextAlign_t;

//==========================================================================================================================================
/**
 * @brief stores direction
 *
 * The type is for storing direction, for example, of the triangle in the function #oC_ColorMap_DrawArrow.
 */
//==========================================================================================================================================
typedef enum
{
    oC_ColorMap_Direction_Up ,      //!< Direction Up
    oC_ColorMap_Direction_Down ,    //!< Direction Down
    oC_ColorMap_Direction_Left ,    //!< Direction Left
    oC_ColorMap_Direction_Right,    //!< Direction Right

    oC_ColorMap_Direction_NumberOfElements          //!< Additional definition for type verification
} oC_ColorMap_Direction_t;

//==========================================================================================================================================
/**
 * @brief stores border style
 *
 * The type is for storing style of the border to draw
 */
//==========================================================================================================================================
typedef enum
{
    oC_ColorMap_BorderStyle_None        = 0,                //!< No border

    oC_ColorMap_BorderStyle_CornerMask  = 0xF ,             //!< Mask for corner style
    oC_ColorMap_BorderStyle_Rectangle   = (0x1 << 0) ,      //!< Rectangle border
    oC_ColorMap_BorderStyle_Rounded     = (0x2 << 0) ,      //!< Rounded border

    oC_ColorMap_BorderStyle_LineMask    = 0xF0 ,            //!< Mask for line style
    oC_ColorMap_BorderStyle_Solid       = (0x1 << 4),       //!< Solid border
    oC_ColorMap_BorderStyle_Dashed      = (0x2 << 4),       //!< Dashed border
    oC_ColorMap_BorderStyle_Dotted      = (0x2 << 4),       //!< Dotted border
} oC_ColorMap_BorderStyle_t;

//==========================================================================================================================================
/**
 * @brief reads line style
 * 
 * The macro reads line style from the border style
 */
//==========================================================================================================================================
#define oC_ColorMap_BorderStyle_Line(BorderStyle)           ( ((BorderStyle) & oC_ColorMap_BorderStyle_LineMask   ) >> 4 )

//==========================================================================================================================================
/**
 * @brief reads corner style
 * 
 * The macro reads corner style from the border style
 */
//==========================================================================================================================================
#define oC_ColorMap_BorderStyle_Corner(BorderStyle)         ( ((BorderStyle) & oC_ColorMap_BorderStyle_CornerMask ) >> 0 )

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @addtogroup ColorMap
//! @{

//==========================================================================================================================================
/**
 * @brief defines the color map
 *
 * The macro defines the color map. The macro checks if the array is big enough to store the color map.
 */
//==========================================================================================================================================
#define oC_ColorMap_Define( ColorMapName , COLORS_ARRAY , WIDTH , HEIGHT , COLOR_FORMAT )         \
                oC_COMPILE_ASSERT( sizeof(COLORS_ARRAY) >= (WIDTH * HEIGHT * oC_Color_FormatSize(COLOR_FORMAT)) , #ColorMapName " array size is too small! (Required " #WIDTH "*" #HEIGHT ")");\
                oC_ColorMap_t ColorMapName = {                 \
                     .Map         = COLORS_ARRAY ,             \
                     .Width       = WIDTH ,                    \
                     .Height      = HEIGHT ,                   \
                     .ColorFormat = COLOR_FORMAT ,             \
                     .MagicNumber = oC_ColorMap_MagicNumber ,  \
                }


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________
//! @addtogroup ColorMap
//! @{

extern oC_ColorMap_t *          oC_ColorMap_New              ( Allocator_t Allocator , AllocationFlags_t AllocationFlags , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_ColorFormat_t ColorFormat , oC_ColorMap_LayerIndex_t NumberOfLayers );
extern bool                     oC_ColorMap_Delete           ( oC_ColorMap_t * ColorMap , AllocationFlags_t AllocationFlags );
extern bool                     oC_ColorMap_SwitchLayer      ( oC_ColorMap_t * ColorMap , oC_ColorMap_LayerIndex_t Layer );
extern oC_ColorMap_LayerIndex_t oC_ColorMap_GetNumberOfLayers( oC_ColorMap_t * ColorMap );
extern oC_ColorMap_LayerIndex_t oC_ColorMap_GetActiveLayer   ( oC_ColorMap_t * ColorMap );
extern oC_ColorMap_LayerIndex_t oC_ColorMap_GetInactiveLayer ( oC_ColorMap_t * ColorMap );
extern oC_ErrorCode_t           oC_ColorMap_FromImage        ( const char * ImagePath , oC_ColorMap_t * outColorMap , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_ColorFormat_t ColorFormat );
extern bool                     oC_ColorMap_CloneMap         ( oC_ColorMap_t * ColorMapOut, const oC_ColorMap_t * ColorMapIn , oC_Pixel_Position_t StartPosition );
extern oC_Color_t               oC_ColorMap_GetColor         ( const oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_ColorFormat_t ColorFormat );
extern bool                     oC_ColorMap_SetColor         ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat );
extern bool                     oC_ColorMap_DrawChar         ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat , uint16_t C , oC_Font_t Font );
extern bool                     oC_ColorMap_DrawString       ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat , const char * String , oC_Font_t Font , oC_Pixel_ResolutionUInt_t BoxWidth , oC_Pixel_ResolutionUInt_t BoxHeight);
extern bool                     oC_ColorMap_DrawAlignedString( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat , const char * String , oC_Font_t Font , oC_Pixel_ResolutionUInt_t BoxWidth , oC_Pixel_ResolutionUInt_t BoxHeight, oC_ColorMap_TextAlign_t TextAlign , oC_ColorMap_VerticalTextAlign_t VerticalAlign , oC_Pixel_ResolutionUInt_t Margin );
extern bool                     oC_ColorMap_DrawLine         ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t StartPosition , oC_Pixel_Position_t EndPosition , oC_Color_t Color , oC_ColorFormat_t ColorFormat );
extern bool                     oC_ColorMap_DrawCircle       ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat , int Radius );
extern bool                     oC_ColorMap_DrawRect         ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t StartPosition , oC_Pixel_Position_t EndPosition , oC_Pixel_ResolutionUInt_t BorderWidth, oC_ColorMap_BorderStyle_t BorderStyle, oC_Color_t BorderColor, oC_Color_t FillColor, oC_ColorFormat_t ColorFormat , bool DrawFill );
extern bool                     oC_ColorMap_DrawTriangle     ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t EdgePositions[3], oC_Pixel_ResolutionUInt_t BorderWidth, oC_Color_t BorderColor, oC_Color_t FillColor, oC_ColorFormat_t ColorFormat , bool DrawFill );
extern bool                     oC_ColorMap_DrawArrow        ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t TopLeftPosition , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_ColorMap_Direction_t Direction , oC_Pixel_ResolutionUInt_t BorderWidth, oC_Color_t BorderColor , oC_Color_t FillColor, oC_ColorFormat_t ColorFormat , bool DrawFill );
extern bool                     oC_ColorMap_DrawBackground   ( oC_ColorMap_t * ColorMap , oC_Color_t BackgroundColor , oC_ColorFormat_t ColorFormat );
extern oC_ErrorCode_t           oC_ColorMap_DrawImage        ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t StartPosition, oC_Pixel_ResolutionUInt_t Width, oC_Pixel_ResolutionUInt_t Height , const char * FilePath );
extern bool                     oC_ColorMap_IsCorrect        ( const oC_ColorMap_t * ColorMap );
extern bool                     oC_ColorMap_IsPositionCorrect( const oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position );
extern bool                     oC_ColorMap_FitPosition      ( const oC_ColorMap_t * ColorMap , oC_Pixel_Position_t* Position );
extern bool                     oC_ColorMap_FillRectWithColor( oC_ColorMap_t * ColorMap, oC_Pixel_Position_t Position, oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_Color_t Color , oC_ColorFormat_t ColorFormat );
extern bool                     oC_ColorMap_FillCircleWithColor( oC_ColorMap_t * ColorMap, oC_Pixel_Position_t Center, oC_Pixel_ResolutionUInt_t Radius , oC_Color_t Color , oC_ColorFormat_t ColorFormat );

#undef  _________________________________________INTERFACE_PROTOTYPES_SECTION_______________________________________________________________
//! @}

#endif /* SYSTEM_CORE_INC_GUI_OC_COLORMAP_H_ */
