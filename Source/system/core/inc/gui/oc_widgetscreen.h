/** ****************************************************************************************************************************************
 *
 * @brief       The file for interface of WidgetScreen module
 * 
 * @file          oc_wscreen.h
 *
 * @author     Patryk Kubiak - (Created on: 02.06.2017 21:12:41) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * @defgroup WidgetScreen Widget Screen
 * @ingroup GUI
 * @brief Module for handling widgets on the screen
 * 
 * @par
 * The <b>WidgetScreen</b> is a special object that stores information about widgets on the screen. It helps you to 
 * create your own GUI in a very simple way - all you need to do, is to define static variables with definitions 
 * of the widgets and screens and just call the function #oC_WidgetScreen_HandleScreens. The function will handle
 * all the widgets and screens for you - it includes not only drawing the widgets, but also handling the user input.
 * 
 * @par
 * Please note, that the function will not return until the user will not close the screen, so to handle the widgets
 * you need to register the callback functions. The callback functions are called when the user interacts with the widget.
 * The callbacks are stored in the types: #oC_WidgetScreen_ScreenHandler_t and #oC_WidgetScreen_WidgetHandler_t. 
 * The first one is used for handling the screen events, the second one is used for handling the widget events.
 * 
 * @par
 * Every widget has its own state. The state is stored in the type #oC_WidgetScreen_WidgetState_t. You can register 
 * the callback for each state of the widget. 
 * 
 * @par
 * Before every screen is drawn, the <b>PrepareHandler</b> function is called (you can find it in the 
 * #oC_WidgetScreen_ScreenDefinition_t structure). Once the screen is drawn, the <b>Handler</b> function is called. 
 * It is a place where you can decide if you want to change the screen without interaction with the widgets. 
 * This callback can be used for example for changing the screen after some time.
 * 
 * @par 
 * Moreover you can react on the widgets events by using <b>Handlers</b> from the #oC_WidgetScreen_WidgetDefinition_t structure.
 * Whenever the widget state changes to the state that has a callback, the callback is called.
 * 
 * @par 
 * Every widget has its own Z position. The Z position is used for definition of the widgets drawing order. The widget
 * with the highest Z position is drawn as the last one. The Z position is stored in the type #oC_WidgetScreen_ZPosition_t.
 * 
 * @warning 
 * Z position with the value 0 is reserved for the screen background.
 * 
 * <h1>Example of the simple menu</h1>
 * @include example_ws_buttons.c
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_GUI_OC_WIDGETSCREEN_H_
#define SYSTEM_CORE_INC_GUI_OC_WIDGETSCREEN_H_

#include <oc_stdtypes.h>
#include <oc_color.h>
#include <oc_errors.h>
#include <oc_pixel.h>
#include <oc_font.h>
#include <oc_screen.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup WidgetScreen
 * @{
 */

//=========================================================================================================================================
/**
 * @brief The type for the widget screen
 */
//=========================================================================================================================================
typedef struct WidgetScreen_t * oC_WidgetScreen_t;

//=========================================================================================================================================
/**
 * @brief stores the screen id
 */
//=========================================================================================================================================
typedef uint32_t oC_WidgetScreen_ScreenId_t;

//=========================================================================================================================================
/**
 * @brief stores the widget index
 */
//=========================================================================================================================================
typedef uint32_t oC_WidgetScreen_WidgetIndex_t;

//=========================================================================================================================================
/**
 * @brief stores the widget value
 */
//=========================================================================================================================================
typedef uint32_t oC_WidgetScreen_Value_t;

//=========================================================================================================================================
/**
 * @brief z position of the widget
 *
 * The Z position is used for definition of the widgets drawing order
 */
//=========================================================================================================================================
typedef uint8_t oC_WidgetScreen_ZPosition_t;

//=========================================================================================================================================
/**
 * @brief stores text align
 * 
 * Helper enum for text align
 */
//=========================================================================================================================================
typedef enum
{
    oC_WidgetScreen_TextAlign_Left   = oC_ColorMap_TextAlign_Left   ,   /**< Align text to the left */
    oC_WidgetScreen_TextAlign_Right  = oC_ColorMap_TextAlign_Right  ,   /**< Align text to the right */
    oC_WidgetScreen_TextAlign_Center = oC_ColorMap_TextAlign_Center ,   /**< Align text to the center */
} oC_WidgetScreen_TextAlign_t;

//=========================================================================================================================================
/**
 * @brief stores vertical text align
 * 
 * Helper enum for vertical text align
 */
//=========================================================================================================================================
typedef enum
{
    oC_WidgetScreen_VerticalTextAlign_Up     = oC_ColorMap_VerticalTextAlign_Up     ,   /**< Align text to the top */
    oC_WidgetScreen_VerticalTextAlign_Down   = oC_ColorMap_VerticalTextAlign_Down   ,   /**< Align text to the bottom */
    oC_WidgetScreen_VerticalTextAlign_Center = oC_ColorMap_VerticalTextAlign_Center ,   /**< Align text to the center */
} oC_WidgetScreen_VerticalTextAlign_t;

//=========================================================================================================================================
/**
 * @brief stores the widget type
 */
//=========================================================================================================================================
typedef enum
{
    oC_WidgetScreen_WidgetType_None        = 0 ,    /**< No widget */
    oC_WidgetScreen_WidgetType_TextBox ,            /**< Text box */
    oC_WidgetScreen_WidgetType_PushButton ,         /**< Push button */
    oC_WidgetScreen_WidgetType_CheckBox ,           /**< Check box */
    oC_WidgetScreen_WidgetType_RadioButton ,        /**< Radio button */
    oC_WidgetScreen_WidgetType_ProgressBar ,        /**< Progress bar */

    oC_WidgetScreen_WidgetType_NumberOfTypes ,      /**< Number of widget types */
} oC_WidgetScreen_WidgetType_t;

//=========================================================================================================================================
/**
 * @brief stores the widget state
 */
//=========================================================================================================================================
typedef enum
{
    //=====================================================================================================================================
    /**
     *  @brief Widget is ready
     * 
     *  The ready state is the default state of the widget. It is used when the widget is not pressed, hovered or activated.
     */
    //=====================================================================================================================================
    oC_WidgetScreen_WidgetState_Ready ,         

    //=====================================================================================================================================
    /**
     *  @brief Widget is pressed
     * 
     *  The pressed state is used when the widget is pressed by the user. It is used for push buttons, check boxes and radio buttons.
     *
     *  <b>Example of Push Button Handler:</b>
     *  @snippet example_ws_buttons.c push_button_handler
     *
     *  <b>Example of handler assignment:</b>
     *  @snippet example_ws_buttons.c main_menu_widgets
     *
     */
    //=====================================================================================================================================
    oC_WidgetScreen_WidgetState_Pressed ,       

    //=====================================================================================================================================
    /**
     *  @brief Widget is activated
     * 
     *  The activated state is used when the widget is activated by the user. It is used for push buttons, check boxes and radio buttons.
     */
    //=====================================================================================================================================
    oC_WidgetScreen_WidgetState_Activated ,     

    //=====================================================================================================================================
    /**
     *  @brief Widget is hovered
     * 
     *  The hovered state is used when the widget is hovered by the user. It is used for push buttons, check boxes and radio buttons.
     */
    //=====================================================================================================================================
    oC_WidgetScreen_WidgetState_Hovered ,       

    //=====================================================================================================================================
    /**
     *  @brief Widget is disabled
     * 
     *  The disabled state is used when the widget is disabled. 
     */
    //=====================================================================================================================================
    oC_WidgetScreen_WidgetState_Disabled,

    //=====================================================================================================================================
    /**
     *  @brief Number of widget states
     * 
     *  The number of widget states is used for counting the number of widget states.
     */
    //=====================================================================================================================================      
    oC_WidgetScreen_WidgetState_NumberOfStates, /**< Number of widget states */
} oC_WidgetScreen_WidgetState_t;

//=========================================================================================================================================
/**
 * @brief stores the push button type
 *
 *  The push button type is used for defining the type of the push button. The type is used for drawing the push button.
 * 
 *  The types are only different in the way they are drawn.
 *
 *  <b>Example of handler assignment:</b>
 *  @snippet example_ws_buttons.c main_menu_widgets
 */
//=========================================================================================================================================
typedef enum
{
    oC_WidgetScreen_PushButtonType_Standard ,       /**< Standard push button */
    oC_WidgetScreen_PushButtonType_ArrowUp ,        /**< Arrow up push button */
    oC_WidgetScreen_PushButtonType_ArrowRight ,     /**< Arrow right push button */
    oC_WidgetScreen_PushButtonType_ArrowLeft ,      /**< Arrow left push button */
    oC_WidgetScreen_PushButtonType_ArrowDown ,      /**< Arrow down push button */

    oC_WidgetScreen_PushButtonType_NumberOfElements, /**< Number of push button types */
} oC_WidgetScreen_PushButtonType_t;

//=========================================================================================================================================
/**
 * @brief stores the border style
 */
//=========================================================================================================================================
typedef enum
{
    oC_WidgetScreen_BorderStyle_None        = 0,                /**< No border */

    oC_WidgetScreen_BorderStyle_CornerMask  = 0xF ,             /**< Mask for corner style */
    oC_WidgetScreen_BorderStyle_Rectangle   = (0x1 << 0) ,      /**< Rectangle corner style */
    oC_WidgetScreen_BorderStyle_Rounded     = (0x2 << 0) ,      /**< Rounded corner style */

    oC_WidgetScreen_BorderStyle_LineMask    = 0xF0 ,            /**< Mask for line style */
    oC_WidgetScreen_BorderStyle_Solid       = (0x1 << 4),       /**< Solid line style */
    oC_WidgetScreen_BorderStyle_Dashed      = (0x2 << 4),       /**< Dashed line style */
    oC_WidgetScreen_BorderStyle_Dotted      = (0x2 << 4),       /**< Dotted line style */
} oC_WidgetScreen_BorderStyle_t;

//=========================================================================================================================================
/**
 * @brief reads the line style from the border style
 */
//=========================================================================================================================================
#define oC_WidgetScreen_BorderStyle_Line(BorderStyle)           ( ((BorderStyle) & oC_WidgetScreen_BorderStyle_LineMask   ) >> 4 )

//=========================================================================================================================================
/**
 * @brief reads the corner style from the border style
 */
//=========================================================================================================================================
#define oC_WidgetScreen_BorderStyle_Corner(BorderStyle)         ( ((BorderStyle) & oC_WidgetScreen_BorderStyle_CornerMask ) >> 0 )

//=========================================================================================================================================
/**
 * @brief palette for the widget screen
 * 
 *   The variable stores information about colors used by the program.
 *   Thanks to that you don't have to define colors for each widget,
 *   but you can keep set of colors in one place and replace all occurrences
 *   in easy way if you need. Of course you are not obligated to use only
 *   one palette - you can define as many palettes as you want to.
 */
//=========================================================================================================================================
typedef struct
{
    oC_ColorFormat_t    ColorFormat;            /**< Color format */
    oC_Color_t          TextColor;              /**< Text color */
    oC_Color_t          FillColor;              /**< Fill color */
    oC_Color_t          BorderColor;            /**< Border color */
} oC_WidgetScreen_Palette_t[oC_WidgetScreen_WidgetState_NumberOfStates];

//=========================================================================================================================================
/**
 * @brief style of drawing for the widget screen
 * 
 * Array with a style for drawing of the widget screen. It is defined for every state of the widget
 */
//=========================================================================================================================================
typedef struct
{
    oC_WidgetScreen_BorderStyle_t       BorderStyle;            //!< Border style
    oC_Pixel_ResolutionUInt_t           BorderWidth;            //!< Border width
    bool                                DrawFill;               //!< Draw fill - if true, fill the widget with fill color
    bool                                DrawText;               //!< Draw text - if true, draw text
    uint8_t                             Opacity;                //!< Opacity of the widget
} oC_WidgetScreen_DrawStyle_t[oC_WidgetScreen_WidgetState_NumberOfStates];

//=========================================================================================================================================
/**
 * @brief Stores user context for the widget screen
 * 
 * Type for storing user context for the widget screen
 */
//=========================================================================================================================================
typedef struct Context_t * oC_WidgetScreen_UserContext_t;

//=========================================================================================================================================
/**
 * @brief stores handler for the widget screen
 * 
 * The type is for storing handler for the widget screen
 * 
 * @param Screen            the screen
 * @param Context           the context
 * @param ScreenID          ID of the current screen
 * 
 * @return ID of the next screen
 * 
 * @note The handler should return the ID of the next screen or number bigger than number of screens if the program should exit
 * 
 * @code{.c}
   
    oC_WidgetScreen_ScreenId_t MyScreenHandler( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_ScreenId_t ScreenID )
    {
        if( GameFinished() )
        {
            return ScreenID + 1;
        }
        
        return ScreenID;
    }

   @endcode
 */
//=========================================================================================================================================
typedef oC_WidgetScreen_ScreenId_t (*oC_WidgetScreen_ScreenHandler_t)( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_ScreenId_t ScreenID );

//=========================================================================================================================================
/**
 * @brief stores handler for the widget 
 * 
 * The type is for storing handler for the widget. The handler is called when the widget is activated.
 * 
 * @param Screen            the current widget screen
 * @param Context           the context
 * @param WidgetIndex       the index of the widget
 * @param ScreenID          ID of the current screen
 * @param outReprepareScreen if the screen should be reprepared
 * 
 * @return ID of the next screen (if the program should exit, then return number bigger than number of screens)
 * 
 * @note The handler should return the ID of the next screen
 * 
 * @code{.c}
   
    oC_WidgetScreen_ScreenId_t MyWidgetHandler( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_ScreenId_t ScreenID , bool * outReprepareScreen )
    {
        if( GameFinished() )
        {
            *outReprepareScreen = true;
            return ScreenID + 1;
        }
        
        return ScreenID;
    }

   @endcode
 */
//=======================================================================================================================================
typedef oC_WidgetScreen_ScreenId_t (*oC_WidgetScreen_WidgetHandler_t)( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_ScreenId_t ScreenID , bool * outReprepareScreen );

//=========================================================================================================================================
/**
 * @brief updates the string for the widget
 * 
 * The type is for updating the string for the widget. The handler should update the string for the widget.
 * 
 * @param Screen            the current widget screen
 * @param Context           the context
 * @param WidgetIndex       the index of the widget
 * @param outString         the output string
 * @param BufferLength      the length of the buffer
 * 
 * @note The handler should return the ID of the next screen
 * 
 * @code{.c}
   
    void MyWidgetStringHandler( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength )
    {
        snprintf( outString, BufferLength, "Hello World" );
    }

   @endcode
 */
//=========================================================================================================================================
typedef void (*oC_WidgetScreen_UpdateWidgetStringHandler_t)( oC_WidgetScreen_t Screen , oC_WidgetScreen_UserContext_t Context , oC_WidgetScreen_WidgetIndex_t WidgetIndex , char * outString, oC_MemorySize_t BufferLength );

//=========================================================================================================================================
/**
 * @brief array with the handlers for the widget
 * 
 * Array with the handlers for the widget. It is defined for every state of the widget
 */
//=========================================================================================================================================
typedef oC_WidgetScreen_WidgetHandler_t oC_WidgetScreen_WidgetHandlers_t[oC_WidgetScreen_WidgetState_NumberOfStates];

//=========================================================================================================================================
/**
 * @brief stores the definition of the widget
 * 
 * The type is for storing the definition of the widget. The definition is used for creating the widget.
 *
 * @snippet example_ws_radiobutton.c widgets
 */
//=========================================================================================================================================
typedef struct
{
    oC_WidgetScreen_WidgetType_t            Type;               //!< Type of the widget
    oC_Pixel_Position_t                     Position;           //!< Position of the widget (top left corner)
    oC_Pixel_ResolutionUInt_t               Height;             //!< Height of the widget
    oC_Pixel_ResolutionUInt_t               Width;              //!< Width of the widget
    oC_WidgetScreen_ZPosition_t             ZPosition;          //!< Z position of the widget (Z position equal to 0 will not be drawn)
    const oC_WidgetScreen_Palette_t *       Palette;            //!< Palette for the widget
    const oC_WidgetScreen_DrawStyle_t *     DrawStyle;          //!< Draw style for the widget
    oC_DefaultPathString_t                  ImagePath;          //!< Path to the image used for the widget
    oC_WidgetScreen_WidgetHandlers_t        Handlers;           //!< Array with handlers for the widget

    struct
    {
        oC_DefaultString_t                              DefaultString;          //!< Default string for the widget
        oC_Font_t                                       Font;                   //!< Font for the widget
        oC_WidgetScreen_TextAlign_t                     TextAlign;              //!< Text align for the widget
        oC_WidgetScreen_VerticalTextAlign_t             VerticalTextAlign;      //!< Vertical text align for the widget 
        oC_WidgetScreen_UpdateWidgetStringHandler_t     UpdateStringHandler;    //!< Handler for updating the string for the widget
    } String;

    union
    {
        /**
         * @brief data for push button
         */
        struct
        {
            oC_WidgetScreen_PushButtonType_t    Type;       //!< Type of the push button
        } PushButton;

        /**
         * @brief data for check box and radio button
         */
        struct
        {
            oC_Pixel_ResolutionUInt_t   OptionWidth;                //!< Width of the every option
            oC_Pixel_ResolutionUInt_t   OptionHeight;               //!< Height of the every option
            oC_Pixel_ResolutionUInt_t   HorizontalCellSpacing;      //!< Horizontal cell spacing between options
            oC_Pixel_ResolutionUInt_t   VerticalCellSpacing;        //!< Vertical cell spacing between options
        } CheckBox, RadioButton;
    } TypeSpecific;
} oC_WidgetScreen_WidgetDefinition_t;

//=========================================================================================================================================
/**
 * @brief stores the definition of the screen
 * 
 * The type is for storing the definition of the screen. The definition is used for creating the screen.
 *
 * @snippet example_ws_radiobutton.c screens
 */
//=========================================================================================================================================
typedef struct
{
    oC_Pixel_Position_t                                 Position;                       //!< Position of the screen (top left corner)
    oC_Pixel_ResolutionUInt_t                           Height;                         //!< Height of the screen
    oC_Pixel_ResolutionUInt_t                           Width;                          //!< Width of the screen
    oC_Color_t                                          BackgroundColor;                //!< Background color of the screen
    oC_ColorFormat_t                                    ColorFormat;                    //!< Color format of the screen
    oC_WidgetScreen_ScreenHandler_t                     PrepareHandler;                 //!< Prepares the screen for drawing
    oC_WidgetScreen_ScreenHandler_t                     Handler;                        //!< Handler for the screen (called before handling of the widgets)
    const oC_WidgetScreen_WidgetDefinition_t *          WidgetsDefinitions;             //!< Array with definitions of the widgets on the screen
    uint32_t                                            NumberOfWidgetsDefinitions;     //!< Size of the array #WidgetsDefinitions
} oC_WidgetScreen_ScreenDefinition_t;

//=========================================================================================================================================
/**
 * @brief option for RadioButton widget
 * 
 * The type is for storing the option for the RadioButton widget. To add the option to the widget, use the function oC_WidgetScreen_AddOption.
 */
//=========================================================================================================================================
typedef struct
{
    const char *                            Label;                  //!< String with label for the option
    oC_Font_t                               Font;                   //!< Font to use to draw the label
    oC_WidgetScreen_TextAlign_t             TextAlign;              //!< Text align for the label
    oC_WidgetScreen_VerticalTextAlign_t     VerticalTextAlign;      //!< Vertical text align for the label
    oC_Pixel_ResolutionUInt_t               Margin;                 //!< Margin for the label
    const char *                            Image;                  //!< Path to the image for the option (not supported yet)

    bool                ChangeFillColor;                            //!< If true, the fill color will be changed to the #FillColor
    oC_Color_t          FillColor;                                  //!< Fill color for the option
    union
    {
        void *          UserPointer;                                //!< User pointer - generic pointer associated with the option. Can be used to store additional data when this option is activated
        const void *    ConstUserPointer;                           //!< Const user pointer - generic pointer associated with the option. Can be used to store additional data when this option is activated
    };
} oC_WidgetScreen_Option_t;

//=========================================================================================================================================
//                                                          SHORT TYPES
//=========================================================================================================================================
#ifdef WIDGET_SCREEN_DEFINE_SHORT_TYPES
    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_TextAlign_t
     */
    //=====================================================================================================================================
    typedef enum
    {
        TextAlign_Left      = oC_WidgetScreen_TextAlign_Left,           //!< Align text to the left
        TextAlign_Right     = oC_WidgetScreen_TextAlign_Right,          //!< Align text to the right
        TextAlign_Center    = oC_WidgetScreen_TextAlign_Center ,        //!< Align text to the center
    } TextAlign_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_VerticalTextAlign_t
     */
    //=====================================================================================================================================
    typedef enum
    {
        VerticalTextAlign_Up        = oC_WidgetScreen_VerticalTextAlign_Up,         //!< Align text to the top
        VerticalTextAlign_Down      = oC_WidgetScreen_VerticalTextAlign_Down,       //!< Align text to the bottom
        VerticalTextAlign_Center    = oC_WidgetScreen_VerticalTextAlign_Center,     //!< Align text to the center
    } VerticalTextAlign_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_WidgetType_t
     */
    //=====================================================================================================================================
    typedef enum
    {
        WidgetType_TextBox          = oC_WidgetScreen_WidgetType_TextBox,           //!< Text box
        WidgetType_PushButton       = oC_WidgetScreen_WidgetType_PushButton,        //!< Push button
        WidgetType_CheckBox         = oC_WidgetScreen_WidgetType_CheckBox,          //!< Check box
        WidgetType_RadioButton      = oC_WidgetScreen_WidgetType_RadioButton,       //!< Radio button
        WidgetType_ProgressBar      = oC_WidgetScreen_WidgetType_ProgressBar,       //!< Progress bar

        WidgetType_NumberOfTypes    = oC_WidgetScreen_WidgetType_NumberOfTypes,
    } WidgetType_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_WidgetState_t
     */
    //=====================================================================================================================================
    typedef enum
    {
        WidgetState_Ready           = oC_WidgetScreen_WidgetState_Ready,            //!< Widget is ready
        WidgetState_Pressed         = oC_WidgetScreen_WidgetState_Pressed,          //!< Widget is pressed
        WidgetState_Activated       = oC_WidgetScreen_WidgetState_Activated,        //!< Widget is activated
        WidgetState_Hovered         = oC_WidgetScreen_WidgetState_Hovered,          //!< Widget is hovered
        WidgetState_Disabled        = oC_WidgetScreen_WidgetState_Disabled,         //!< Widget is disabled
        WidgetState_NumberOfStates  = oC_WidgetScreen_WidgetState_NumberOfStates,   //!< Number of widget states
    } WidgetState_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_PushButtonType_t
     */
    //=====================================================================================================================================
    typedef enum
    {
        PushButtonType_Standard         = oC_WidgetScreen_PushButtonType_Standard,
        PushButtonType_ArrowUp          = oC_WidgetScreen_PushButtonType_ArrowUp,
        PushButtonType_ArrowRight       = oC_WidgetScreen_PushButtonType_ArrowRight,
        PushButtonType_ArrowLeft        = oC_WidgetScreen_PushButtonType_ArrowLeft,
        PushButtonType_ArrowDown        = oC_WidgetScreen_PushButtonType_ArrowDown,
        PushButtonType_NumberOfElements = oC_WidgetScreen_PushButtonType_NumberOfElements
    } PushButtonType_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_BorderStyle_t
     */
    //=====================================================================================================================================
    typedef enum
    {
        BorderStyle_None        = oC_WidgetScreen_BorderStyle_None,

        BorderStyle_CornerMask  = oC_WidgetScreen_BorderStyle_CornerMask ,
        BorderStyle_Rectangle   = oC_WidgetScreen_BorderStyle_Rectangle ,
        BorderStyle_Rounded     = oC_WidgetScreen_BorderStyle_Rounded ,

        BorderStyle_LineMask    = oC_WidgetScreen_BorderStyle_LineMask ,
        BorderStyle_Solid       = oC_WidgetScreen_BorderStyle_Solid,
        BorderStyle_Dashed      = oC_WidgetScreen_BorderStyle_Dashed,
        BorderStyle_Dotted      = oC_WidgetScreen_BorderStyle_Dotted,
    } BorderStyle_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_BorderStyle_Line
     */
    //=====================================================================================================================================
    #define BorderStyle_Line(BorderStyle)           oC_WidgetScreen_BorderStyle_Line(BorderStyle)

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_BorderStyle_Corner
     */
    //=====================================================================================================================================
    #define BorderStyle_Corner(BorderStyle)         oC_WidgetScreen_BorderStyle_Corner(BorderStyle)

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_Palette_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_Palette_t Palette_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_DrawStyle_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_DrawStyle_t DrawStyle_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_WidgetIndex_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_WidgetIndex_t   WidgetIndex_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_t               Screen_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_ScreenHandler_t
     */
    //=====================================================================================================================================    
    typedef oC_WidgetScreen_ScreenHandler_t ScreenHandler_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_WidgetHandlers_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_WidgetHandlers_t WidgetHandlers_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_WidgetDefinition_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_WidgetDefinition_t WidgetDefinition_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_ScreenDefinition_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_ScreenDefinition_t ScreenDefinition_t;

    //=====================================================================================================================================
    /**
     * @copydoc oC_WidgetScreen_Option_t
     */
    //=====================================================================================================================================
    typedef oC_WidgetScreen_Value_t WidgetValue_t;
#endif

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
/** @} */

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
/**
 * @addtogroup WidgetScreen
 * @{
 */

extern oC_WidgetScreen_Value_t          oC_WidgetScreen_AddOption           ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , const oC_WidgetScreen_Option_t * Option );
extern void                             oC_WidgetScreen_RemoveOption        ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value );
extern const oC_WidgetScreen_Option_t * oC_WidgetScreen_GetOption           ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value );
extern const oC_WidgetScreen_Option_t * oC_WidgetScreen_GetCurrentOption    ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex );
extern void                             oC_WidgetScreen_RemoveCurrentOption ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex );
extern void                             oC_WidgetScreen_ClearOptions        ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex );
extern oC_WidgetScreen_Value_t          oC_WidgetScreen_GetValue            ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex );
extern void                             oC_WidgetScreen_SetVisible          ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , bool Visible );
extern void                             oC_WidgetScreen_SetValue            ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value );
extern oC_WidgetScreen_Value_t          oC_WidgetScreen_GetMaximumValue     ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex );
extern void                             oC_WidgetScreen_SetMaximumValue     ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value );
extern void                             oC_WidgetScreen_ScrollUp            ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value );
extern void                             oC_WidgetScreen_ScrollDown          ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value );
extern oC_WidgetScreen_Palette_t *      oC_WidgetScreen_GetPalette          ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex );

extern oC_WidgetScreen_WidgetIndex_t    oC_WidgetScreen_AddWidget           ( oC_WidgetScreen_t Screen, const oC_WidgetScreen_WidgetDefinition_t * Definition, void * UserParameter);
extern void                             oC_WidgetScreen_RemoveWidget        ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex );

extern oC_ErrorCode_t oC_WidgetScreen_HandleScreens( oC_Screen_t Screen , const oC_WidgetScreen_ScreenDefinition_t * Definitions , oC_WidgetScreen_ScreenId_t NumberOfScreens, void * UserParameter );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
/** @} */

#endif /* SYSTEM_CORE_INC_GUI_OC_WIDGETSCREEN_H_ */
