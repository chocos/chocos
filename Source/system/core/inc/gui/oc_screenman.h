/** ****************************************************************************************************************************************
 *
 * @file       oc_screenman.h
 *
 * @brief      The file with interface of the Screen Manager module
 *
 * @author     Patryk Kubiak - (Created on: 21.07.2016 20:07:14) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup ScreenMan Screens Manager
 * @ingroup GUI
 *
 * @brief Handles screens
 *
 * The screen manager module is designed for storing screens that are handled by the system. It has to be turned on before usage by calling
 * a function #oC_ScreenMan_TurnOn. If the turning operation is finished with success, the drivers that are associated with screens can be
 * configured by calling the function named #oC_ScreenMan_ConfigureAll.
 *
 * If you want draw something at the screen, you have to choose one. If you don't know which screen should you take, you can draw at the
 * default screen. It can be received by the function named #oC_ScreenMan_GetDefaultScreen
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_GUI_OC_SCREENMAN_H_
#define SYSTEM_CORE_INC_GUI_OC_SCREENMAN_H_

#include <oc_errors.h>
#include <oc_screen.h>
#include <oc_list.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
//! @addtogroup ScreenMan
//! @{

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_________________________________________________________________________________
//! @addtogroup ScreenMan
//! @{

extern oC_ErrorCode_t           oC_ScreenMan_TurnOn           ( void );
extern oC_ErrorCode_t           oC_ScreenMan_TurnOff          ( void );
extern void                     oC_ScreenMan_ConfigureAll     ( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) );
extern void                     oC_ScreenMan_UnconfigureAll   ( void );
extern oC_Screen_t              oC_ScreenMan_GetDefaultScreen ( void );
extern oC_List(oC_Screen_t)     oC_ScreenMan_GetList          ( void );
extern oC_Screen_t              oC_ScreenMan_GetScreen        ( const char * Name );
extern oC_ErrorCode_t           oC_ScreenMan_AddScreen        ( oC_Screen_t Screen );
extern oC_ErrorCode_t           oC_ScreenMan_RemoveScreen     ( oC_Screen_t Screen );

#undef  _________________________________________FUNCTIONS_________________________________________________________________________________
//! @}

#endif /* SYSTEM_CORE_INC_GUI_OC_SCREENMAN_H_ */
