/** ****************************************************************************************************************************************
 *
 * @file       oc_screen.h
 *
 * @brief      File with interface for the screen objects
 *
 * @author     Patryk Kubiak - (Created on: 21.07.2016 20:05:47) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Screen
 * @ingroup GUI
 * @brief Object for handling screen
 *
 * The "Screen" is a special object that stores information about a screen. I know, that it does not sounds very well, but it means exactly
 * what it does. You should use screens objects, when you want to use some graphics drivers or modules. Generally you should not create them -
 * they are created during startup of the system. Of course it also can be created during it's work, but remember, that it is not allocated
 * for the current process - so if you don't delete the screen, it will cause memory leak.
 *
 * Each screen object handles only one physical object. So, for example if you have 2 LCD displays, you will need 2 the Screen objects.
 *
 * @note
 * Each screen object should be added to the list in the screens manager (#ScreenMan).
 *
 * You always can check if the Screen object is correct by using function named #oC_Screen_IsCorrect. It returns true if the Screen was correctly
 * initialized.
 *
 * To create the screen you will need pointer to the graphics driver and its configuration. After creation of the object, you also have to
 * configure the graphics driver. You have to do it by using the function named #oC_Screen_Configure. You cannot configure a driver by your own,
 * because the Screen object requires context of the configuration to work.
 *
 * @warning
 * You should always use #oC_Screen_Configure function to configure the graphics drivers.
 *
 * To unconfigure the driver call #oC_Screen_Unconfigure function.
 *
 * To draw something on the screen, you should use the #oc_ColorMap_t. Pointer to the color map you can read by using the function
 * #oC_Screen_ReadColorMap. You can also read resolution of the screen by calling the function #oC_Screen_ReadResolution
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_GUI_OC_SCREEN_H_
#define SYSTEM_CORE_INC_GUI_OC_SCREEN_H_

#include <oc_errors.h>
#include <oc_driverman.h>
#include <oc_colormap.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief Screen object pointer
 *
 * This type stores all informations about the screen. It is a main module object. All data inside are private, and cannot be changed
 * without using an interface functions.
 *
 * The screen is an object, so it must be created before usage. You can do it by using a function called #oC_Screen_New. When the object is
 * not needed anymore, you can destroy it by calling a function named #oC_Screen_Delete.
 *
 * @warning
 * Screen objects are allocated with a special allocator. Because of that, you should not create a screen in programs unless you exactly
 * know what are you doing.
 *
 */
//==========================================================================================================================================
typedef struct Screen_t * oC_Screen_t;

#undef  _________________________________________TYPES______________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTION_SECTION___________________________________________________________________________

extern oC_Screen_t          oC_Screen_New           ( oC_Driver_t Driver , const void * Config , const char * Name );
extern bool                 oC_Screen_IsCorrect     ( oC_Screen_t Screen );
extern bool                 oC_Screen_Delete        ( oC_Screen_t * Screen );
extern bool                 oC_Screen_IsConfigured  ( oC_Screen_t Screen );
extern oC_ErrorCode_t       oC_Screen_Configure     ( oC_Screen_t Screen );
extern oC_ErrorCode_t       oC_Screen_Unconfigure   ( oC_Screen_t Screen );
extern oC_ErrorCode_t       oC_Screen_SwitchLayer   ( oC_Screen_t Screen , oC_ColorMap_LayerIndex_t Layer );
extern oC_ErrorCode_t       oC_Screen_ReadColorMap  ( oC_Screen_t Screen , oC_ColorMap_t ** outColorMap );
extern oC_ErrorCode_t       oC_Screen_ReadResolution( oC_Screen_t Screen , oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight );
extern const char *         oC_Screen_GetName       ( oC_Screen_t Screen );
extern oC_Driver_t          oC_Screen_GetDriver     ( oC_Screen_t Screen );

#undef  _________________________________________FUNCTION_SECTION___________________________________________________________________________


#endif /* SYSTEM_CORE_INC_GUI_OC_SCREEN_H_ */
