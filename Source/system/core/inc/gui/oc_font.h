/** ****************************************************************************************************************************************
 *
 * @brief      The file contains the definition of the font object
 *
 * @file       oc_font.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup Font Font
 * @ingroup GUI
 * @brief Module for handling fonts
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_FONT_H_
#define SYSTEM_LIBRARIES_INC_OC_FONT_H_

#include <oc_stdtypes.h>
#include <oc_1word.h>
#include <oc_null.h>
#include <stdbool.h>
#include <oc_stdlib.h>
#include <oc_pixel.h>
#include <oc_string.h>
#include <oc_math.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup Font
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores information about the character
 */
//==========================================================================================================================================
typedef struct
{
    const uint8_t   WidthBits;      //!< width of the character in bits
    const uint16_t  Offset;         //!< offset of the character in the font data
} oC_Font_CharacterInfo_t;

//==========================================================================================================================================
/**
 * @brief stores information about the font
 */
//==========================================================================================================================================
typedef struct
{
    const uint8_t                    HeightPages;           //!< height of the character in pages
    const uint16_t                   StartChar;             //!< start character
    const uint16_t                   EndChar;               //!< end character
    const oC_Font_CharacterInfo_t*   CharInfo;              //!< pointer to the character info
    const uint8_t*                   Data;                  //!< pointer to the font data
    const char *                     FontName;              //!< name of the font
} oC_FontInfo_t;

//==========================================================================================================================================
/**
 * @brief stores information about the character map
 */
//==========================================================================================================================================
typedef struct
{
    uint8_t                 Width;
    uint8_t                 Height;
    const uint8_t*          Data;
} oC_Font_CharacterMap_t;

//==========================================================================================================================================
/**
 * @brief stores pointer to the font
 */
//==========================================================================================================================================
typedef const oC_FontInfo_t * oC_Font_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________
/**
 * @addtogroup Font
 * @{
 */

//==========================================================================================================================================
/**
 * @brief checks if the character is printable with the font
 */
//==========================================================================================================================================
#define oC_Font_IsPrintableCharacter( Font , Ascii )       ( (Ascii) >= Font->StartChar && (Ascii) <= Font->EndChar )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with predefined fonts
 *
 *  ======================================================================================================================================*/
#define _________________________________________PREDEFINED_FONTS_SECTION___________________________________________________________________
/**
 * @addtogroup Font
 * @{
 */

//==========================================================================================================================================
/**
 * @brief returns the pointer to the font
 */
//==========================================================================================================================================
#define oC_Font_(FontName)          &oC_1WORD_FROM_2(oC_FontInfo_ , FontName)

/**
 * @defgroup PredefinedFonts Predefined Fonts
 * @brief collection of predefined fonts
 * @addtogroup PredefinedFonts
 * @ingroup Font
 * @{
 */

extern const oC_FontInfo_t oC_FontInfo_Consolas;                //!< Consolas font
extern const oC_FontInfo_t oC_FontInfo_Consolas6pt;             //!< Consolas 6pt font
extern const oC_FontInfo_t oC_FontInfo_Consolas7pt;             //!< Consolas 7pt font
extern const oC_FontInfo_t oC_FontInfo_Consolas8pt;             //!< Consolas 8pt font
extern const oC_FontInfo_t oC_FontInfo_NiagaraEngraved_36pt;    //!< Niagara Engraved 36pt font
extern const oC_FontInfo_t oC_FontInfo_OldEnglishTextMT_36pt;   //!< Old English Text MT 36pt font
extern const oC_FontInfo_t oC_FontInfo_ShowcardGothic_36pt;     //!< Showcard Gothic 36pt font
extern const oC_FontInfo_t oC_FontInfo_Algerian_28pt;           //!< Algerian 28pt font
extern const oC_FontInfo_t oC_FontInfo_Arial_10pt;              //!< Arial 10pt font
extern const oC_FontInfo_t oC_FontInfo_BlackadderITC_20pt;      //!< Blackadder ITC 20pt font
extern const oC_FontInfo_t oC_FontInfo_Consolas_20pt;           //!< Consolas 20pt font
extern const oC_FontInfo_t oC_FontInfo_CooperBlack_20pt;        //!< Cooper Black 20pt font
extern const oC_FontInfo_t oC_FontInfo_Castellar_28pt;          //!< Castellar 28pt font
//! @}

#undef  _________________________________________PREDEFINED_FONTS_SECTION___________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with static inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
/**
 * @addtogroup Font
 * @{
 */

//==========================================================================================================================================
/**
 * @brief returns the maximum width of the font
 * 
 * The function returns the maximum width of the font
 * 
 * @param Font      pointer to the font
 * 
 * @return maximum width of the font
 */
//==========================================================================================================================================
static inline uint8_t oC_Font_GetMaximumWidth( oC_Font_t Font )
{
    uint8_t width = 0;

    if(isaddresscorrect(Font))
    {
        uint32_t numberOfDefinedChars = Font->EndChar - Font->StartChar;

        for( uint32_t i = 0; i < numberOfDefinedChars; i++ )
        {
            if(Font->CharInfo[i].WidthBits > width)
            {
                width = Font->CharInfo[i].WidthBits;
            }
        }
    }

    return width;
}

//==========================================================================================================================================
/**
 * @brief reads the character map
 * 
 * The function reads the character map
 * 
 * @param Font      pointer to the font
 * @param C         character
 */
//==========================================================================================================================================
static inline bool oC_Font_ReadCharacterMap( oC_Font_t Font , uint16_t C , oC_Font_CharacterMap_t * outCharacterMap )
{
    bool success = false;

    if(isaddresscorrect(Font) && isram(outCharacterMap) && oC_Font_IsPrintableCharacter(Font,C))
    {
        const oC_Font_CharacterInfo_t characterInfo = Font->CharInfo[C - Font->StartChar];

        outCharacterMap->Data   = &Font->Data[characterInfo.Offset];
        outCharacterMap->Height = Font->HeightPages;
        outCharacterMap->Width  = characterInfo.WidthBits;

        success = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads the string size
 * 
 * The function reads the string size
 * 
 * @param Font              pointer to the font
 * @param String            string
 * @param outWidth[out]     pointer to the width
 * @param outHeight[out]    pointer to the height
 * @param ScreenWidth       screen width
 */
//==========================================================================================================================================
static inline bool oC_Font_ReadStringSize( oC_Font_t Font , const char * String , oC_Pixel_ResolutionUInt_t * outWidth, oC_Pixel_ResolutionUInt_t * outHeight , oC_Pixel_ResolutionUInt_t ScreenWidth )
{
    bool success = false;

    if(isaddresscorrect(Font) && isram(outWidth) && isram(outHeight) && isaddresscorrect(String))
    {
        int length = strlen(String);
        oC_Pixel_ResolutionUInt_t width         = 0;
        oC_Pixel_ResolutionUInt_t height        = Font->HeightPages;
        oC_Pixel_ResolutionUInt_t x             = 0;
        oC_Font_CharacterMap_t    characterMap  = {0};

        for(int i = 0; i < length; i++)
        {
            uint16_t C = String[i];

            if(C == '\n')
            {
                x       = 0;
                height += Font->HeightPages;
            }
            else if(oC_Font_ReadCharacterMap(Font,C,&characterMap))
            {
                if( (x + characterMap.Width) <= ScreenWidth )
                {
                    x += characterMap.Width;
                }
                else
                {
                    width   = x;
                    height += Font->HeightPages;
                }
            }

            width = oC_MAX(x,width);
        }

        *outWidth   = width;
        *outHeight  = height;
        success     = true;

    }

    return success;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________
//! @}

#endif /* SYSTEM_LIBRARIES_INC_OC_FONT_H_ */
