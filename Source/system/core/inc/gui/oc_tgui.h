/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for TGUI
 *
 * @file       oc_tgui.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup TGUI TGUI - Terminal GUI Module
 * @ingroup GUI
 * @brief Handles VT100 pseudo GUI operations
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_SRC_GUI_OC_TGUI_H_
#define SYSTEM_CORE_SRC_GUI_OC_TGUI_H_

#include <oc_stdtypes.h>
#include <stdbool.h>
#include <oc_stdlib.h>
#include <oc_vt100.h>
#include <oc_list.h>
#include <oc_null.h>
#include <oc_string.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define oC_TGUI_Color_IsCorrect(Color)              ( (Color) <= (oC_TGUI_Color_White) && (Color) >= (oC_TGUI_Color_Black) )
#define oC_TGUI_Position_IsCorrect(Pos)             ( (Pos.Line > 0) && (Pos.Column > 0) )
#define oC_TGUI_Position_AreEqual(P1,P2)            ( oC_TGUI_Position_Compare(P1,P2) == 0 )
#define oC_TGUI_Position_IsSmaller(P1,P2)           ( oC_TGUI_Position_Compare(P1,P2) <  0 )
#define oC_TGUI_Position_IsGreater(P1,P2)           ( oC_TGUI_Position_Compare(P1,P2) >  0 )
#define oC_TGUI_Position_IsLower(P1,P2)             ( (P1.Line > P2.Line) )
#define oC_TGUI_Position_IsHigher(P1,P2)            ( (P1.Line < P2.Line) )

#define oC_TGUI_DefaultScreenWidth                  80
#define oC_TGUI_DefaultScreenHeight                 24

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores pressed key
 *
 * @ingroup TGUI
 *
 * This is the type for storing keys. Special keys are defined as enumeration values (for example #oC_TGUI_Key_Enter).
 * All other characters are represented with their ASCII code.
 *
 * When you need to detect some key pressed with CTRL key, you should expect value: 'ASCII' - #oC_TGUI_Key_Control . For example:
 * @code{.c}
   oC_TGUI_WaitForKeyPress(&key);

   if(key == ( 'd' - oC_TGUI_Key_Control ))
   {
       printf("You pressed Ctrl+D!\n");
   }
   else if(key == ( 'c' - oC_TGUI_Key_Control ))
   {
       printf("You pressed Ctrl+C!\n");
   }
   @endcode
 *
 * List of predefined keys:
 */
//==========================================================================================================================================
typedef enum
{
    oC_TGUI_Key_Enter           = 13,                                                      //!< ENTER key
    oC_TGUI_Key_Backspace       = 127,                                                     //!< Backspace key
    oC_TGUI_Key_Tab             = 9,                                                       //!< Tab key
    oC_TGUI_Key_Control         = 0x60,                                                    //!< Special value for detecting CTRL
    oC_TGUI_Key_ControlC        = 'c' - oC_TGUI_Key_Control ,                              //!< Ctrl+C keys
    oC_TGUI_Key_SpecialKeysId   = 0x10000 ,                                                //!< Special value - minimum ID for special keys
    oC_TGUI_Key_ESC             = oC_TGUI_Key_SpecialKeysId ,                              //!< ESC key
    oC_TGUI_Key_ArrowUp         = oC_TGUI_Key_SpecialKeysId + '[' + 'A' ,                  //!< Arrow UP key
    oC_TGUI_Key_ArrowDown       = oC_TGUI_Key_SpecialKeysId + '[' + 'B' ,                  //!< Arrow DOWN key
    oC_TGUI_Key_ArrowRight      = oC_TGUI_Key_SpecialKeysId + '[' + 'C' ,                  //!< Arrow RIGHT key
    oC_TGUI_Key_ArrowLeft       = oC_TGUI_Key_SpecialKeysId + '[' + 'D' ,                  //!< Arrow LEFT key
    oC_TGUI_Key_F1              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '1' + '~' ,      //!< F1 key
    oC_TGUI_Key_F2              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '2' + '~' ,      //!< F2 key
    oC_TGUI_Key_F3              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '3' + '~' ,      //!< F3 key
    oC_TGUI_Key_F4              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '4' + '~' ,      //!< F4 key
    oC_TGUI_Key_F5              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '5' + '~' ,      //!< F5 key
    oC_TGUI_Key_F6              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '6' + '~' ,      //!< F6 key
    oC_TGUI_Key_F7              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '7' + '~' ,      //!< F7 key
    oC_TGUI_Key_F8              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '8' + '~' ,      //!< F8 key
    oC_TGUI_Key_F9              = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '9' + '~' ,      //!< F9 key
    oC_TGUI_Key_F10             = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '1' + '0' + '~' ,//!< F10 key
    oC_TGUI_Key_F11             = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '1' + '1' + '~' ,//!< F11 key
    oC_TGUI_Key_F12             = oC_TGUI_Key_SpecialKeysId + '[' + '1' + '1' + '2' + '~' ,//!< F12 key
} oC_TGUI_Key_t;

//==========================================================================================================================================
/**
 * @brief stores terminal colors
 *
 * @ingroup TGUI
 *
 * This is the type for storing terminal colors. You can use functions #oC_TGUI_SetBackgroundColor to set background color and
 * #oC_TGUI_SetForegroundColor to set foreground color. Look at the #oC_TGUI_Style_t type to see how to set them all with one function.
 *
 * Here is a list of available combinations of terminal colors:
 * ![Terminal Colors](terminal_colors.png)
 *
 * @see oC_TGUI_GetColorFromName oC_TGUI_GetNameFromColor
 *
 * List of available color definitions:
 */
//==========================================================================================================================================
typedef enum
{
    oC_TGUI_Color_Default     = 39 ,//!< <b style="color: #000000">Default     </b>
    oC_TGUI_Color_Black       = 30 ,//!< <b style="color: #000000">Black       </b>
    oC_TGUI_Color_Red         = 31 ,//!< <b style="color: #BB0000">Red         </b>
    oC_TGUI_Color_Green       = 32 ,//!< <b style="color: #00BB00">Green       </b>
    oC_TGUI_Color_Yellow      = 33 ,//!< <b style="color: #BBBB00">Yellow      </b>
    oC_TGUI_Color_Blue        = 34 ,//!< <b style="color: #0000BB">Blue        </b>
    oC_TGUI_Color_Magenda     = 35 ,//!< <b style="color: #BB00BB">Magenda     </b>
    oC_TGUI_Color_Cyan        = 36 ,//!< <b style="color: #00BBBB">Cyan        </b>
    oC_TGUI_Color_LightGray   = 37 ,//!< <b style="color: #BBBBBB">Light Gray   </b>
    oC_TGUI_Color_DarkGray    = 90 ,//!< <b style="color: #555555">Dark Gray    </b>
    oC_TGUI_Color_LightRed    = 91 ,//!< <b style="color: #FF5555">Light Red    </b>
    oC_TGUI_Color_LightGreen  = 92 ,//!< <b style="color: #55FF55">Light Green  </b>
    oC_TGUI_Color_LightYellow = 93 ,//!< <b style="color: #FFFF55">Light Yellow </b>
    oC_TGUI_Color_LightBlue   = 94 ,//!< <b style="color: #5555FF">Light Blue   </b>
    oC_TGUI_Color_LightMagenda= 95 ,//!< <b style="color: #FF55FF">Light Magenda</b>
    oC_TGUI_Color_LightCyan   = 96 ,//!< <b style="color: #55FFFF">Light Cyan   </b>
    oC_TGUI_Color_White       = 97 ,//!< <b style="color: #DDDDDD">White       </b>
    oC_TGUI_Color_NumberOfColors = 16   //!< Number of defined colors
} oC_TGUI_Color_t;

//==========================================================================================================================================
/**
 * @brief text attributes
 *
 * @ingroup TGUI
 *
 * The type for storing style of the text. If you want default style, set it as 0 or #oC_TGUI_TextStyle_Default.
 * Attributes can be joined by 'OR'.
 *
 * @note
 * Not all attributes are supported by all terminals. For example in PuTTY, blinking text must be enabled in the configuration before.
 *
 * To set text style use function #oC_TGUI_SetTextStyle
 *
 * List of defined attributes:
 */
//==========================================================================================================================================
typedef enum
{
    oC_TGUI_TextStyle_Default   = (1<<0) , //!< Default style for a text
    oC_TGUI_TextStyle_Bold      = (1<<1) , //!< <b>Text will be bold</b>
    oC_TGUI_TextStyle_Dim       = (1<<2) , //!< Foreground and background will be lighter
    oC_TGUI_TextStyle_Underline = (1<<3) , //!< Text will be underlined
    oC_TGUI_TextStyle_Blink     = (1<<4) , //!< Text will blink
    oC_TGUI_TextStyle_Inverted  = (1<<5) , //!< Invert foreground with background colors
    oC_TGUI_TextStyle_Hidden    = (1<<6)   //!< Text will be hidden
} oC_TGUI_TextStyle_t;

//==========================================================================================================================================
/**
 * @brief stores index for menu
 *
 * @ingroup TGUI
 *
 * The type is for storing index of menu entries.
 *
 * @see oC_TGUI_DrawMenu
 */
//==========================================================================================================================================
typedef int32_t oC_TGUI_EntryIndex_t;

//==========================================================================================================================================
/**
 * @brief stores style of elements
 *
 * @ingroup TGUI
 *
 * The type stores all terminal style parameters in one structure. Example of usage:
 * @code{.c}
   static const oC_TGUI_Style_t style = {
       .Background    = oC_TGUI_Color_Red ,
       .Foreground    = oC_TGUI_Color_Blue ,
       .TextStyle     = oC_TGUI_TextStyle_Default ,
       .DontDraw      = false ,
   };

   oC_TGUI_SetStyle(&style);
   printf("This text is written on red background with blue foreground.\n\r");
   @endcode
 *
 * This is result of the example code:
 * ![Result of example](set_tgui_style_example.png)
 *
 * @see oC_TGUI_SetStyle
 *
 * The structure fields:
 */
//==========================================================================================================================================
typedef struct
{
    oC_TGUI_Color_t         Background; //!< Color of the background
    oC_TGUI_Color_t         Foreground; //!< Color of the foreground
    oC_TGUI_TextStyle_t     TextStyle;  //!< Text attributes (bold/underline,etc)
    bool                    DontDraw;   //!< Additional field for TGUI functions. If this flag is set to 'true', then the element of this style will not be drawn
} oC_TGUI_Style_t;

//==========================================================================================================================================
/**
 * @brief style for progress bar
 *
 * @ingroup TGUI
 *
 * The type for storing style of the progress bar.
 *
 * @see oC_TGUI_DrawProgressBar
 *
 * Here is example of usage:
 * @code{.c}
    // Style of the progress bar
    static const oC_TGUI_ProgressBarStyle_t barStyle = {
                    .ActiveColor            = oC_TGUI_Color_Green ,      // Color of the filled part of progress bar
                    .NonActiveColor         = oC_TGUI_Color_LightGreen , // Color of the not filled part of progress bar
                    .BorderStyle.Foreground = oC_TGUI_Color_Black ,      // Foreground color for border
                    .BorderStyle.Background = oC_TGUI_Color_White ,      // Background color for border
                    .BorderStyle.DontDraw   = false ,                    // Set it to true if you don't need a border
    };
    // Position of progress bar
    static const oC_TGUI_Position_t position = { .Column = 1 , .Line = 1 };
    // Width of the progress bar
    static const oC_TGUI_Column_t   width    = 30;
    // current value of progress bar
    static const uint32_t           value    = 15;
    // Maximum value of the progress bar
    static const uint32_t           maxValue = 30;

    // The progress bar will have 50% filled (value/maxValue = 15/30 = 0.5)
    oC_TGUI_DrawProgressBar(position,width,value,maxValue,&barStyle);
   @endcode
 *
 * The code above will result with creation of the following progress bar:
 * ![Progress Bar Example](progress_bar_tgui_example.png)
 */
//==========================================================================================================================================
typedef struct
{
    oC_TGUI_Color_t         NonActiveColor; //!< Color of the not filled part of progress bar
    oC_TGUI_Color_t         ActiveColor;    //!< Color of the filled part of progress bar
    oC_TGUI_Style_t         BorderStyle;    //!< Style of the border
} oC_TGUI_ProgressBarStyle_t;

//==========================================================================================================================================
/**
 * @brief style for text box
 *
 * @ingroup TGUI
 *
 * The type stores style for the text box.
 *
 * @see oC_TGUI_DrawTextBox
 */
//==========================================================================================================================================
typedef struct
{
    oC_TGUI_Style_t         TextStyle;      //!< Style of the text inside the box
    oC_TGUI_Style_t         BorderStyle;    //!< Style of the border (set DontDraw to **true** if you do not want it)
} oC_TGUI_TextBoxStyle_t;

//==========================================================================================================================================
/**
 * @brief style for box
 *
 * @ingroup TGUI
 *
 * The type stores style for the Box.
 *
 * @see oC_TGUI_DrawBox
 *
 * Example of usage:
 * @code{.c}
    static const oC_TGUI_BoxStyle_t boxStyle = {
        .BorderStyle.Foreground      = oC_TGUI_Color_Black ,
        .BorderStyle.Background      = oC_TGUI_Color_White ,
        .TitleStyle.Foreground       = oC_TGUI_Color_Black ,
        .TitleStyle.Background       = oC_TGUI_Color_White ,
        .ShadowStyle.Background      = oC_TGUI_Color_Blue ,
        .InsideStyle.Background      = oC_TGUI_Color_LightBlue ,
        .InsideStyle.Foreground      = oC_TGUI_Color_Blue ,
    };
    static const oC_TGUI_Position_t position = { .Column = 1 , .Line = 1 };
    static const oC_TGUI_Column_t   width    = 60;
    static const oC_TGUI_Line_t     height   = 20;

    oC_TGUI_DrawBox( "[ TITLE ]"  , "Text inside box" , position , width , height , &boxStyle);

    oC_TGUI_WaitForKeyPress(NULL);
   @endcode
 *
 * Result of the example code:
 * ![Example TGUI Box](box_tgui_example.png)
 *
 * @note
 * If you do not want some part of the box, set the **DontDraw** flag in the style to **true**
 *
 * List of box parties:
 */
//==========================================================================================================================================
typedef struct
{
    oC_TGUI_Style_t         BorderStyle;    //!< Style of the box border
    oC_TGUI_Style_t         TitleStyle;     //!< Title style
    oC_TGUI_Style_t         ShadowStyle;    //!< Shadow (right down and right bottom)
    oC_TGUI_Style_t         InsideStyle;    //!< Style of text inside
} oC_TGUI_BoxStyle_t;

//==========================================================================================================================================
/**
 * @brief stores line or height of terminal cursor position
 *
 * The type for storing number of line or height of cursor position.
 *
 * @note This type cannot store 0 value!
 *
 * @ingroup TGUI
 */
//==========================================================================================================================================
typedef uint16_t oC_TGUI_Line_t;
//==========================================================================================================================================
/**
 * @brief stores column or width of terminal cursor position
 *
 * The type for storing number of column or width of cursor position.
 *
 * @note This type cannot store 0 value!
 *
 * @ingroup TGUI
 */
//==========================================================================================================================================
typedef uint16_t oC_TGUI_Column_t;

//==========================================================================================================================================
/**
 * @brief stores cursor position
 *
 * @ingroup TGUI
 *
 * The type for storing cursor position.
 *
 * @warning
 * The position {0,0} is not correct! The top left corner is {1,1}
 */
//==========================================================================================================================================
typedef struct
{
    oC_TGUI_Line_t      Line;       //!< Number of line
    oC_TGUI_Column_t    Column;     //!< Number of column
} oC_TGUI_Position_t;

//==========================================================================================================================================
/**
 * @brief Menu entry activated function
 *
 * @ingroup TGUI
 *
 * The type for storing pointer to a function, that should be called when one of menu entries is activated.
 *
 * @param Parameter     Optional parameter, that can be used by a user
 *
 * @see oC_TGUI_DrawMenu oC_TGUI_MenuEntry_t
 */
//==========================================================================================================================================
typedef void (*oC_TGUI_MenuHandler_t)( void * Parameter );
//==========================================================================================================================================
/**
 * @brief Function for drawing list menu element
 *
 * @ingroup TGUI
 *
 * The types stores pointer to a function, that draws a list element. It is called by the TGUI module, when element of the list is visible
 * on the screen.
 *
 * @param Position          Position of the list element, where it should be drawn
 * @param ElementHandle     Handler to a list element
 * @param Width             Maximum width for the element
 *
 * @see oC_TGUI_DrawListMenu
 *
 * @note
 * The terminal style is set before call of this function
 */
//==========================================================================================================================================
typedef void (*oC_TGUI_DrawListHandler_t)( oC_TGUI_Position_t Position , oC_List_ElementHandle_t ElementHandle , oC_TGUI_Column_t Width );
//==========================================================================================================================================
/**
 * @brief List element selected function
 *
 * @ingroup TGUI
 *
 * The type stores handler to the function, that is called, when the list element is selected.
 *
 * @see oC_TGUI_DrawListMenu
 *
 * @param ListElement           Selected element
 */
//==========================================================================================================================================
typedef void (*oC_TGUI_SelectListHandler_t)( oC_List_ElementHandle_t ListElement );
//==========================================================================================================================================
/**
 * @brief Function for saving value of edit box
 *
 * @ingroup TGUI
 *
 * The function is called, when the value in edit box is changed.
 *
 * @param Buffer            Buffer with string of EditBox
 * @param Size              Size of the Buffer
 * @param Parameter         Optional Parameter
 *
 * @see oC_TGUI_DrawEditBox
 */
//==========================================================================================================================================
typedef void (*oC_TGUI_EditBoxSaveValueHandler_t)( char * Buffer , uint32_t Size , void * Parameter );
//==========================================================================================================================================
/**
 * @brief Function for saving value of quick edit box
 *
 * @ingroup TGUI
 *
 * The function is called, when the value in quick edit box is changed.
 *
 * @param Buffer            Buffer with string of QuickEditBox
 * @param Size              Size of the Buffer
 * @param Parameter         Optional Parameter
 *
 * @see oC_TGUI_DrawQuickEditBox
 */
//==========================================================================================================================================
typedef void (*oC_TGUI_QuickEditBoxSaveValueHandler_t)( char * Buffer , uint32_t Size , void * Parameter );
//==========================================================================================================================================
/**
 * @brief Function called when push button is pressed
 *
 * @ingroup TGUI
 *
 * Pointer to the function that is called, when the push button is pressed.
 *
 * @param Parameter         Optional parameter
 *
 * @return true if waiting for push button should ends, false if the TGUI should still wait for a key
 */
//==========================================================================================================================================
typedef bool (*oC_TGUI_PushButtonHandler_t)( void * Parameter );
//==========================================================================================================================================
/**
 * @brief Function called when value is changed
 *
 * @ingroup TGUI
 *
 * Pointer of the function, that is called, when selected value in SelectionBox is changed.
 *
 * @param SelectedValue         String with selected value
 * @param SelectedIndex         Index of selected value
 * @param Parameter             Optional parameter
 */
//==========================================================================================================================================
typedef void (*oC_TGUI_SelectionHandler_t)( const char * SelectedValue , uint32_t SelectedIndex , void * Parameter );

//==========================================================================================================================================
/**
 * @brief Entry in menu
 *
 * @ingroup TGUI
 *
 * Entry in menu for #oC_TGUI_DrawMenu function.
 *
 * @see oC_TGUI_DrawMenu
 */
//==========================================================================================================================================
typedef struct
{
    const char *            Title;              //!< Title of the menu entry
    const char *            Help;               //!< Help string for the menu entry
    oC_TGUI_MenuHandler_t   Handler;            //!< Handler of function, that will be called, when the menu is selected. If it is set to #NULL, the #oC_TGUI_DrawMenu function will be broken
    void *                  Parameter;          //!< Optional parameter for the Handler
    oC_TGUI_MenuHandler_t   OnHoverHandler;     //!< Handler of the function, that will be called, when the menu is hovered
    void *                  OnHoverParameter;   //!< Optional parameter for the OnHoverHandler
} oC_TGUI_MenuEntry_t;

//==========================================================================================================================================
/**
 * @brief Style of the menu
 *
 * @ingroup TGUI
 *
 * The structure stores style of the menu.
 *
 * @see oC_TGUI_DrawMenu
 */
//==========================================================================================================================================
typedef struct
{
    oC_TGUI_Style_t     ActiveEntry;            //!< Style of selected item
    oC_TGUI_Style_t     NotActiveEntry;         //!< Style of not selected item
    oC_TGUI_Style_t     Border;                 //!< Style of border
} oC_TGUI_MenuStyle_t;

//==========================================================================================================================================
/**
 * @brief stores type of property value
 *
 * @ingroup TGUI
 *
 * @see oC_TGUI_Property_t
 */
//==========================================================================================================================================
typedef enum
{
    oC_TGUI_ValueType_UINT ,       //!< Value of type oC_UInt_t
    oC_TGUI_ValueType_U32 ,        //!< Value of type uint32_t
    oC_TGUI_ValueType_U64 ,        //!< Value of type uint64_t
    oC_TGUI_ValueType_I32 ,        //!< Value of type int32_t
    oC_TGUI_ValueType_I64 ,        //!< Value of type int64_t
    oC_TGUI_ValueType_ValueFloat , //!< Value of type float
    oC_TGUI_ValueType_ValueDouble ,//!< Value of type double
    oC_TGUI_ValueType_ValueChar ,  //!< Value of type char
    oC_TGUI_ValueType_ValueString ,//!< Value of type string
} oC_TGUI_ValueType_t;

typedef struct
{
    const char *        Description;
    const char *        Format;
    oC_TGUI_ValueType_t Type;
    union
    {
        oC_UInt_t           ValueUINT;
        uint32_t            ValueU32;
        uint64_t            ValueU64;
        int32_t             ValueI32;
        int64_t             ValueI64;
        float               ValueFloat;
        double              ValueDouble;
        char                ValueChar;
        const char *        ValueString;
    };
} oC_TGUI_Property_t;

typedef struct
{
    oC_TGUI_Style_t     DescriptionStyle;
    oC_TGUI_Style_t     ValueStyle;
} oC_TGUI_PropertyStyle_t;

typedef struct
{
    oC_TGUI_Position_t              StartPosition;
    oC_TGUI_Column_t                Width;
    oC_TGUI_Line_t                  Height;
    oC_TGUI_Column_t                DescriptionWidth;
    const oC_TGUI_Property_t *      Properties;
    oC_TGUI_EntryIndex_t            NumberOfProperties;
    const oC_TGUI_PropertyStyle_t * Style;
} oC_TGUI_DrawPropertyConfig_t;

typedef struct
{
    oC_TGUI_Position_t              TopLeft;
    oC_TGUI_Column_t                Width;
    oC_TGUI_Line_t                  Height;
    oC_List_t                       List;
    oC_TGUI_DrawListHandler_t       DrawHandler;
    oC_TGUI_SelectListHandler_t     SelectHandler;
    void *                          SelectHandlerParameter;
    const oC_TGUI_MenuStyle_t *     Style;
} oC_TGUI_DrawListMenuConfig_t;

typedef enum
{
    oC_TGUI_InputType_Disabled          = 0 ,
    oC_TGUI_InputType_Password          = (1<<0),
    oC_TGUI_InputType_Digits            = (1<<1),
    oC_TGUI_InputType_Characters        = (1<<2),
    oC_TGUI_InputType_SpecialChars      = (1<<3),
    oC_TGUI_InputType_Default           = oC_TGUI_InputType_Digits          |
                                          oC_TGUI_InputType_Characters      |
                                          oC_TGUI_InputType_SpecialChars ,
} oC_TGUI_InputType_t;

typedef enum
{
    oC_TGUI_Font_G0 ,
    oC_TGUI_Font_G1 ,
} oC_TGUI_Font_t;

typedef struct
{
    oC_TGUI_Style_t     ActiveBorder;
    oC_TGUI_Style_t     Text;
    oC_TGUI_Style_t     DisabledText;
    oC_TGUI_Style_t     NotActiveBorder;
} oC_TGUI_EditBoxStyle_t;

typedef struct
{
    oC_TGUI_Style_t     ActiveBorder;
    oC_TGUI_Style_t     NotActiveBorder;
    oC_TGUI_Style_t     ActiveText;
    oC_TGUI_Style_t     NotActiveText;
} oC_TGUI_QuickEditBoxStyle_t;

typedef struct
{
    char *                              Buffer;
    uint32_t                            BufferSize;
    oC_TGUI_InputType_t                 InputType;
    oC_TGUI_EditBoxSaveValueHandler_t   SaveHandler;
    void *                              SaveParameter;
    const oC_TGUI_EditBoxStyle_t*       Style;
} oC_TGUI_EditBox_t;

typedef struct
{
    char *                                    Buffer;
    uint32_t                                  BufferSize;
    oC_TGUI_InputType_t                       InputType;
    oC_TGUI_QuickEditBoxSaveValueHandler_t    SaveHandler;
    void *                                    SaveParameter;
    const oC_TGUI_QuickEditBoxStyle_t*        Style;
} oC_TGUI_QuickEditBox_t;

typedef enum
{
    oC_TGUI_ActiveObjecType_EditBox ,
    oC_TGUI_ActiveObjecType_PushButton ,
    oC_TGUI_ActiveObjecType_QuickEdit ,
    oC_TGUI_ActiveObjecType_SelectionBox ,
} oC_TGUI_ActiveObjecType_t;

typedef struct
{
    oC_TGUI_Style_t         ActiveBorder;
    oC_TGUI_Style_t         Active;
    oC_TGUI_Style_t         NotActive;
} oC_TGUI_PushButtonStyle_t;

typedef struct
{
    const char *                        Text;
    oC_TGUI_PushButtonHandler_t         PressHandler;
    void *                              PressParameter;
    const oC_TGUI_PushButtonStyle_t *   Style;
} oC_TGUI_PushButton_t;

typedef struct
{
    oC_TGUI_Style_t                     Arrow;
    oC_TGUI_Style_t                     Active;
    oC_TGUI_Style_t                     NotActive;
} oC_TGUI_SelectionBoxStyle_t;

typedef struct
{
    uint32_t*                           SelectedIndex;
    const oC_String_t*                  Options;
    uint32_t                            NumberOfOptions;
    const oC_TGUI_SelectionBoxStyle_t * Style;
    oC_TGUI_SelectionHandler_t          OnChangeHandler;
    void *                              OnChangeParameter;
} oC_TGUI_SelectionBox_t;

typedef struct
{
    oC_TGUI_ActiveObjecType_t   Type;
    oC_TGUI_Position_t          Position;
    oC_TGUI_Column_t            Width;
    oC_TGUI_Line_t              Height;
    oC_TGUI_Position_t          LabelPosition;
    const char *                LabelText;
    oC_TGUI_Column_t            LabelWidth;
    const oC_TGUI_Style_t *     LabelStyle;
    union
    {
        oC_TGUI_EditBox_t       EditBox;
        oC_TGUI_PushButton_t    PushButton;
        oC_TGUI_QuickEditBox_t  QuickEdit;
        oC_TGUI_SelectionBox_t  SelectionBox;
    };
} oC_TGUI_ActiveObject_t;

typedef struct
{
    uint8_t     SignIndex;
} oC_TGUI_Spiner_t;

typedef struct
{
    oC_TGUI_Color_t     Color;
    const char *        Name;
} oC_TGUI_ColorData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_TGUI_ColorData_t oC_TGUI_Colors[] = {
                { .Color = oC_TGUI_Color_Default      , .Name = "Default"       } ,
                { .Color = oC_TGUI_Color_Black        , .Name = "Black"         } ,
                { .Color = oC_TGUI_Color_Red          , .Name = "Red"           } ,
                { .Color = oC_TGUI_Color_Green        , .Name = "Green"         } ,
                { .Color = oC_TGUI_Color_Yellow       , .Name = "Yellow"        } ,
                { .Color = oC_TGUI_Color_Blue         , .Name = "Blue"          } ,
                { .Color = oC_TGUI_Color_Magenda      , .Name = "Magenda"       } ,
                { .Color = oC_TGUI_Color_Cyan         , .Name = "Cyan"          } ,
                { .Color = oC_TGUI_Color_LightGray    , .Name = "LightGray"     } ,
                { .Color = oC_TGUI_Color_DarkGray     , .Name = "DarkGray"      } ,
                { .Color = oC_TGUI_Color_LightRed     , .Name = "LightRed"      } ,
                { .Color = oC_TGUI_Color_LightGreen   , .Name = "LightGreen"    } ,
                { .Color = oC_TGUI_Color_LightYellow  , .Name = "LightYellow"   } ,
                { .Color = oC_TGUI_Color_LightBlue    , .Name = "LightBlue"     } ,
                { .Color = oC_TGUI_Color_LightMagenda , .Name = "LightMagenda"  } ,
                { .Color = oC_TGUI_Color_LightCyan    , .Name = "LightCyan"     } ,
                { .Color = oC_TGUI_Color_White        , .Name = "White"         } ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

extern oC_TGUI_Color_t       oC_TGUI_GetColorFromName       ( const char * Name );
extern const char *          oC_TGUI_GetNameFromColor       ( oC_TGUI_Color_t Color );

extern bool                  oC_TGUI_ClearScreen            ( void );
extern bool                  oC_TGUI_ResetDevice            ( void );
extern bool                  oC_TGUI_SetCursorPosition      ( oC_TGUI_Position_t CursorPosition );
extern bool                  oC_TGUI_ReadCursorPosition     ( oC_TGUI_Position_t * outCursorPosition );
extern bool                  oC_TGUI_SetBackgroundColor     ( oC_TGUI_Color_t Color );
extern bool                  oC_TGUI_SetForegroundColor     ( oC_TGUI_Color_t Color );
extern bool                  oC_TGUI_SetTextStyle           ( oC_TGUI_TextStyle_t TextStyle );
extern bool                  oC_TGUI_SetStyle               ( const oC_TGUI_Style_t * Style );
extern bool                  oC_TGUI_SaveAttributes         ( void );
extern bool                  oC_TGUI_RestoreAttributes      ( void );
extern int                   oC_TGUI_Position_Compare       ( oC_TGUI_Position_t P1 , oC_TGUI_Position_t P2 );
extern oC_TGUI_Position_t    oC_TGUI_Position_Increment     ( oC_TGUI_Position_t TopPosition , oC_TGUI_Position_t BottomRight , oC_TGUI_Position_t Position );
extern oC_TGUI_Position_t    oC_TGUI_Position_Decrement     ( oC_TGUI_Position_t TopPosition , oC_TGUI_Position_t BottomRight , oC_TGUI_Position_t Position );
extern bool                  oC_TGUI_SetFont                ( oC_TGUI_Font_t Font );
extern bool                  oC_TGUI_SetLineWrapping        ( bool Enabled );
extern bool                  oC_TGUI_DoesSupportRealLines   ( void );

extern bool                  oC_TGUI_DrawRealLine           ( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition );
extern bool                  oC_TGUI_DrawLine               ( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition );
extern bool                  oC_TGUI_DrawBorder             ( oC_TGUI_Position_t TopLeft       , oC_TGUI_Position_t BottomRight );
extern bool                  oC_TGUI_DrawAtPosition         ( oC_TGUI_Position_t StartPosition , const char * String );
extern bool                  oC_TGUI_DrawAtPositionWithSize ( oC_TGUI_Position_t StartPosition , const char * String , int Size );
extern bool                  oC_TGUI_DrawCharAtPosition     ( oC_TGUI_Position_t StartPosition , char C );
extern bool                  oC_TGUI_DrawNCharAtPosition    ( oC_TGUI_Position_t StartPosition , char C , int N );
extern bool                  oC_TGUI_DrawFillBetween        ( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition , char C );
extern bool                  oC_TGUI_ClearPartOfScreen      ( oC_TGUI_Position_t StartPosition , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , oC_TGUI_Color_t Color );
extern bool                  oC_TGUI_DrawMultiLineText      ( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition , const char * String , bool AsPassword );
extern bool                  oC_TGUI_DrawFormatAtPosition   ( oC_TGUI_Position_t StartPosition , const char * Format , ... );
extern bool                  oC_TGUI_FindPositionInBuffer   ( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t * Position ,oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const char * String , uint32_t * outIndex );
extern void                  oC_TGUI_DrawSpiner             ( oC_TGUI_Spiner_t * outSpanner );

extern bool                  oC_TGUI_DrawProgressBar        ( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , uint32_t CurrentValue , uint32_t MaximumValue , const oC_TGUI_ProgressBarStyle_t * Style );

extern bool                  oC_TGUI_DrawTextBox            ( const char * String , oC_TGUI_Position_t Position , oC_TGUI_Column_t FixedWidth , const oC_TGUI_TextBoxStyle_t * Style );
extern bool                  oC_TGUI_DrawBox                ( const char * Title  , const char * InsideText , oC_TGUI_Position_t TopLeft  , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const oC_TGUI_BoxStyle_t * Style );
extern bool                  oC_TGUI_DrawProperties         ( const oC_TGUI_DrawPropertyConfig_t * Config );

extern oC_TGUI_EntryIndex_t  oC_TGUI_DrawMenu               ( oC_TGUI_Position_t TopLeft , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const oC_TGUI_MenuEntry_t * MenuEntries , oC_TGUI_EntryIndex_t NumberOfEntries , const oC_TGUI_MenuStyle_t * Style );
extern bool                  oC_TGUI_DrawListMenu           ( const oC_TGUI_DrawListMenuConfig_t * Config );
extern oC_TGUI_Key_t         oC_TGUI_DrawPushButton         ( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const oC_TGUI_PushButton_t * PushButton , bool Active );
extern oC_TGUI_Key_t         oC_TGUI_DrawEditBox            ( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , oC_TGUI_EditBox_t * EditBox , bool Active );
extern oC_TGUI_Key_t         oC_TGUI_DrawQuickEditBox       ( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , oC_TGUI_QuickEditBox_t * QuickEditBox , bool Active );
extern oC_TGUI_Key_t         oC_TGUI_DrawSelectionBox       ( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , const oC_TGUI_SelectionBox_t * SelectionBox , bool Active );
extern oC_TGUI_Key_t         oC_TGUI_DrawActiveObject       ( oC_TGUI_ActiveObject_t * ActiveObject , bool Active );
extern bool                  oC_TGUI_DrawActiveObjects      ( oC_TGUI_ActiveObject_t * ActiveObjects, uint32_t NumberOfObjects );

extern bool                  oC_TGUI_WaitForKeyPress        ( oC_TGUI_Key_t * outKey );
extern bool                  oC_TGUI_CheckKeyPressed        ( oC_TGUI_Key_t * outKey );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________


#endif /* SYSTEM_CORE_SRC_GUI_OC_TGUI_H_ */
