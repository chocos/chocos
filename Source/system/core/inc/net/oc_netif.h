/** ****************************************************************************************************************************************
 *
 * @brief      Contains interface for netif object
 *
 * @file       oc_netif.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Netif  Netif - Network Interface object
 * @ingroup CoreNetwork
 * @brief stores informations about the network interface
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_OC_NETIF_H_
#define SYSTEM_CORE_INC_NET_OC_NETIF_H_

#include <oc_net.h>
#include <oc_driver.h>
#include <oc_thread.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup Netif
 * @{
 */

//==========================================================================================================================================
/**
 * @brief Netif main object
 *
 * The type is for storing **Netif** object. All data inside are private, you can only use it for identify the instance for **Netif** functions.
 * The object cannot be copied.
 *
 * Please use #oC_Netif_New function to create the object and #oC_Netif_Delete to destroy it.
 */
//==========================================================================================================================================
typedef struct Netif_t * oC_Netif_t;

//==========================================================================================================================================
/**
 * @brief type for storing friendly name string
 *
 * The type is for storing friendly name string of the network interface (the string that is easy to understand and remember for human).
 */
//==========================================================================================================================================
typedef oC_Net_FriendlyInterfaceName_t oC_Netif_FriendlyName_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________
/** @} */

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

extern oC_Netif_t               oC_Netif_New                    ( oC_Netif_FriendlyName_t FriendlyName, const char * Address, const char * NetMaskAddress , oC_Driver_t Driver , const void * Config );
extern bool                     oC_Netif_Delete                 ( oC_Netif_t * outNetif );
extern bool                     oC_Netif_IsCorrect              ( oC_Netif_t Netif );
extern bool                     oC_Netif_IsConfigured           ( oC_Netif_t Netif );
extern const char *             oC_Netif_GetFriendlyName        ( oC_Netif_t Netif );
extern oC_Driver_t              oC_Netif_GetDriver              ( oC_Netif_t Netif );
extern oC_ErrorCode_t           oC_Netif_Configure              ( oC_Netif_t Netif );
extern oC_ErrorCode_t           oC_Netif_Unconfigure            ( oC_Netif_t Netif );
extern oC_ErrorCode_t           oC_Netif_ReadNetInfo            ( oC_Netif_t Netif , oC_Net_Info_t * outInfo );
extern oC_ErrorCode_t           oC_Netif_SendPacket             ( oC_Netif_t Netif , oC_Net_Packet_t * Packet , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Netif_ReceivePacket          ( oC_Netif_t Netif , oC_Net_Packet_t * Packet , oC_MemorySize_t PacketSize , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Netif_SetWakeOnLanEvent      ( oC_Netif_t Netif , oC_Event_t WolEvent );
extern oC_ErrorCode_t           oC_Netif_Flush                  ( oC_Netif_t Netif );
extern oC_ErrorCode_t           oC_Netif_SetLoopback            ( oC_Netif_t Netif , oC_Net_Layer_t Layer , bool Enabled );
extern oC_ErrorCode_t           oC_Netif_PerformDiagnostics     ( oC_Netif_t Netif , oC_Diag_t * outDiags , uint32_t * NumberOfDiags );
extern oC_Net_LinkStatus_t      oC_Netif_GetLinkStatus          ( oC_Netif_t Netif );
extern oC_BaudRate_t            oC_Netif_GetBaudRate            ( oC_Netif_t Netif );
extern oC_Net_HardwareType_t    oC_Netif_GetHardwareType        ( oC_Netif_t Netif );
extern oC_ErrorCode_t           oC_Netif_ReadHardwareAddress    ( oC_Netif_t Netif , oC_Net_HardwareAddress_t * outHardwareAddress );
extern oC_ErrorCode_t           oC_Netif_ReadHardwareAddressOf  ( oC_Netif_t Netif , oC_Net_HardwareAddress_t * outHardwareAddress , const oC_Net_Address_t * Address , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Netif_SetIpv4Info            ( oC_Netif_t Netif , const oC_Net_Ipv4Info_t * Info );
extern oC_ErrorCode_t           oC_Netif_ReadIpv4Info           ( oC_Netif_t Netif , oC_Net_Ipv4Info_t * outInfo );
extern oC_ErrorCode_t           oC_Netif_SetIpv6Info            ( oC_Netif_t Netif , const oC_Net_Ipv6Info_t * Info );
extern oC_ErrorCode_t           oC_Netif_ReadIpv6Info           ( oC_Netif_t Netif , oC_Net_Ipv6Info_t * outInfo );
extern bool                     oC_Netif_IsAddressInSubnet      ( oC_Netif_t Netif , const oC_Net_Address_t * Address );
extern oC_Net_Ipv4_t            oC_Netif_GetIpv4Address         ( oC_Netif_t Netif );
extern bool                     oC_Netif_ReadIpv6Address        ( oC_Netif_t Netif , oC_Net_Ipv6_t * outAddress );
extern oC_ErrorCode_t           oC_Netif_UpdateLinkStatus       ( oC_Netif_t Netif );
extern bool                     oC_Netif_IsIpAssigned           ( oC_Netif_t Netif );
extern bool                     oC_Netif_IsStaticIpValid        ( oC_Netif_t Netif );
extern bool                     oC_Netif_IsIpStatic             ( oC_Netif_t Netif );
extern oC_ErrorCode_t           oC_Netif_WaitOnLink             ( oC_Netif_t Netif , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Netif_UpdateIp               ( oC_Netif_t Netif , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Netif_SetReceiveThread       ( oC_Netif_t Netif , oC_Thread_t Thread );
extern oC_ErrorCode_t           oC_Netif_SetListenMode          ( oC_Netif_t Netif , bool Enabled );
extern bool                     oC_Netif_GetListenMode          ( oC_Netif_t Netif );
extern oC_ErrorCode_t           oC_Netif_ReadReceiveThread      ( oC_Netif_t Netif , oC_Thread_t * outThread );
extern oC_ErrorCode_t           oC_Netif_SendAddressProbe       ( oC_Netif_t Netif , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Netif_AssignStaticIp         ( oC_Netif_t Netif , oC_Time_t Timeout );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_OC_NETIF_H_ */
