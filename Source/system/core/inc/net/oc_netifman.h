/** ****************************************************************************************************************************************
 *
 * @brief      Stores interface of netif manager module
 *
 * @file       oc_netifman.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup NetifMan   NetifMan - Network interface manager
 * @ingroup CoreNetwork
 * @brief Handles network interface objects
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_OC_NETIFMAN_H_
#define SYSTEM_CORE_INC_NET_OC_NETIFMAN_H_

#include <oc_netif.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup NetifMan
 * @brief stores entry of the routing table
 *
 * The type is for storing routing table entry. In computer networking the **Routing Table**, or **routing information base (RIB)**,
 * is a data table stored in a router or a networked computer that lists the routes to particular network destinations, and in some cases,
 * metrics (distances) associated with those routes. The routing table contains information about the topology of the network immediately around it.
 *
 * For more info see:
 * <b><a href="https://en.wikipedia.org/wiki/Routing_table">routing table in wikipedia</a></b>
 */
//==========================================================================================================================================
typedef struct
{
    oC_Net_Address_t            DestinationAddress;     //!< Destination address of the packet
    oC_Net_Address_t            Netmask;                //!< Subnetwork mask
    oC_Net_Cost_t               Cost;                   //!< Cost of interface usage (it is to allow choosing one interface more frequently than another)
    oC_Netif_t                  Netif;                  //!< Netif interface to use
} oC_NetifMan_RoutingTableEntry_t;

//==========================================================================================================================================
/**
 * @ingroup NetifMan
 * @brief stores pointer to the function to filter packets
 *
 * The function is used for filtering packets during receiving it from the Netif.
 *
 * @param ReceivedPacket        Packet received in the ring
 * @param Parameter             Optional parameter
 * @param Netif                 Netif that has received the packet
 *
 * @return true if this is the expected packet
 */
//==========================================================================================================================================
typedef bool (*oC_NetifMan_PacketFilterFunction_t)( oC_Net_Packet_t * ReceivedPacket , const void * Parameter , oC_Netif_t Netif );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

extern oC_ErrorCode_t       oC_NetifMan_TurnOn                 ( void );
extern oC_ErrorCode_t       oC_NetifMan_TurnOff                ( void );
extern void                 oC_NetifMan_ConfigureAll           ( void );
extern void                 oC_NetifMan_UnconfigureAll         ( void );
extern oC_Netif_t           oC_NetifMan_GetNetif               ( oC_Netif_FriendlyName_t FriendlyName );
extern oC_Netif_t           oC_NetifMan_GetNetifForAddress     ( const oC_Net_Address_t * Destination );
extern oC_ErrorCode_t       oC_NetifMan_AddNetifToList         ( oC_Netif_t Netif );
extern oC_ErrorCode_t       oC_NetifMan_RemoveNetifFromList    ( oC_Netif_t Netif );
extern oC_ErrorCode_t       oC_NetifMan_AddRoutingTableEntry   ( const oC_Net_Address_t * Destination, const oC_Net_Address_t * Netmask, oC_Net_Cost_t Cost , oC_Netif_t Netif );
extern oC_ErrorCode_t       oC_NetifMan_RemoveRoutingTableEntry( const oC_Net_Address_t * Destination, const oC_Net_Address_t * Netmask, oC_Netif_t Netif );
extern oC_List(oC_Netif_t)  oC_NetifMan_GetList                ( void );
extern oC_ErrorCode_t       oC_NetifMan_SendPacket             ( const oC_Net_Address_t * Address , oC_Net_Packet_t * Packet , oC_Time_t Timeout , oC_Netif_t * outNetif );
extern oC_ErrorCode_t       oC_NetifMan_ReceivePacket          ( const oC_Net_Address_t * Address , oC_Net_Packet_t * outPacket , oC_Time_t Timeout , oC_Netif_t * outNetif , oC_NetifMan_PacketFilterFunction_t FilterFunction , const void * Parameter);
extern void                 oC_NetifMan_UpdateRoutingTable     ( void );
extern oC_ErrorCode_t       oC_NetifMan_StartDaemon            ( void );


#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_OC_NETIFMAN_H_ */
