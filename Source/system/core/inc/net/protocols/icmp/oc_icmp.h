/** ****************************************************************************************************************************************
 *
 * @brief      File with interface for ICMP
 *
 * @file       oc_icmp.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Icmp   Internet Control Message Protocol
 * @ingroup CoreNetwork
 * @brief module for handling ICMP packets
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_PROTOCOLS_ICMP_OC_ICMP_H_
#define SYSTEM_CORE_INC_NET_PROTOCOLS_ICMP_OC_ICMP_H_

#include <oc_net.h>
#include <oc_process.h>
#include <oc_netif.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup Icmp
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores ICMP message type
 *
 * The type is for storing ICMP type for the `Type` field in the #oC_Icmp_Header_t struct.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Icmp_Type_EchoReply                      = 0 , //!< Echo reply
    oC_Icmp_Type_DestinationUnreachable         = 3 , //!< Destination unreachable message
    oC_Icmp_Type_SourceQuench                   = 4 , //!< Source Quench (depracated)
    oC_Icmp_Type_RedirectMessage                = 5 , //!< Redirect Message
    oC_Icmp_Type_EchoRequest                    = 8 , //!< Echo Request
    oC_Icmp_Type_RouterAdvertisement            = 9 , //!< Router Advertisement
    oC_Icmp_Type_RouterSolicitation             = 10 ,//!< Router Solicitation
    oC_Icmp_Type_TimeExceeded                   = 11 ,//!< Time Exceeded
    oC_Icmp_Type_ParameterProblemBadIpHeader    = 12 ,//!< Parameter Problem Bad Ip Header
    oC_Icmp_Type_Timestamp                      = 13 ,//!< Timestamp
    oC_Icmp_Type_TimestampReply                 = 14 ,//!< Timestamp Reply
    oC_Icmp_Type_InformationRequest             = 15 ,//!< Information Request
    oC_Icmp_Type_InformationReply               = 16 ,//!< Information Reply
    oC_Icmp_Type_AddressMaskRequest             = 17 ,//!< Address Mask Request
    oC_Icmp_Type_AddressMaskReply               = 18, //!< Address Mask Reply


    oC_Icmp_Type_Empty                          = 0xFF
} oC_Icmp_Type_t;

//==========================================================================================================================================
/**
 * @brief stores ICMP message header
 */
//==========================================================================================================================================
typedef struct
{
    uint8_t     Type;           //!< Type of the ICMP message. Use #oC_Icmp_Type_t type to assign a value to this field
    uint8_t     Code;           //!< Sub type of the ICMP message
    uint16_t    Checksum;       //!< Checksum with ICMP header and Data (with 0 for this field)
} oC_Icmp_Header_t;

//==========================================================================================================================================
/**
 * @brief stores ICMP echo message
 */
//==========================================================================================================================================
typedef struct
{
    uint16_t    ID;                 //!< Identification of the transaction
    uint16_t    SequenceNumber;     //!< Sequence number of the message
    uint32_t    Payload[40];        //!< Optional PayLoad
} oC_Icmp_Message_Echo_t;

//==========================================================================================================================================
/**
 * @brief stores ICMP timestamp message
 */
//==========================================================================================================================================
typedef struct
{
    uint16_t    ID;                     //!< Identification of the transaction
    uint16_t    SequenceNumber;         //!< Sequence number of the message
    uint32_t    OriginateTimestamp;
    uint32_t    ReceiveTimestamp;
    uint32_t    TransmitTimestamp;
} oC_Icmp_Message_Timestamp_t;

//==========================================================================================================================================
/**
 * @brief stores ICMP Address Mask message
 */
//==========================================================================================================================================
typedef struct
{
    uint16_t    ID;                     //!< Identification of the transaction
    uint16_t    SequenceNumber;         //!< Sequence number of the message
    uint32_t    AddressMask;            //!< Subnet mask
} oC_Icmp_Message_AddressMask_t;

//==========================================================================================================================================
/**
 * @brief stores ICMP Destination Unreachable message
 */
//==========================================================================================================================================
typedef struct
{
    uint16_t                    Unused;
    uint16_t                    NextHopMtu;
    oC_Net_Ipv4PacketHeader_t   IPv4Header;
    uint8_t                     Data[8];
} oC_Icmp_Message_DestinationUnreachable_t;

//==========================================================================================================================================
/**
 * @brief stores ICMP datagram
 */
//==========================================================================================================================================
typedef struct
{
    oC_Icmp_Header_t    Header;         //!< Header of the ICMP packet
    union
    {
        uint8_t                                     Payload[4];                 //!< Generic pointer to the ICMP message
        oC_Icmp_Message_Echo_t                      Echo;                       //!< Echo message data
        oC_Icmp_Message_Timestamp_t                 Timestamp;                  //!< Timestamp message data
        oC_Icmp_Message_AddressMask_t               AddressMask;                //!< Address mask message data
        oC_Icmp_Message_DestinationUnreachable_t    DestinationUnreachable;     //!< Destination unreachable message data
    } Message;
} oC_Icmp_Datagram_t;

//==========================================================================================================================================
/**
 * @brief stores ICMP packet (datagram + IP header)
 */
//==========================================================================================================================================
typedef union
{
    oC_Net_Packet_t         Packet;     //!< IP packet

    struct
    {
        oC_Net_Ipv4PacketHeader_t   Header;         //!< IPv4 header
        oC_Icmp_Datagram_t          IcmpDatagram;   //!< ICMP data in the IPv4 packet
    } IPv4;

    struct
    {
        oC_Net_Ipv6PacketHeader_t   Header;         //!< IPv6 header
        oC_Icmp_Datagram_t          IcmpDatagram;   //!< ICMP data in the IPv6 packet
    } IPv6;
} oC_Icmp_Packet_t;

/** @} */
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
/**
 * @addtogroup Icmp
 * @{
 */

extern oC_ErrorCode_t       oC_Icmp_TurnOn                      ( void );
extern oC_ErrorCode_t       oC_Icmp_TurnOff                     ( void );
extern bool                 oC_Icmp_IsTypeReserved              ( oC_Icmp_Type_t Type    , oC_Process_t Process );
extern oC_ErrorCode_t       oC_Icmp_ReserveType                 ( oC_Icmp_Type_t Type    , oC_Time_t    Timeout );
extern oC_ErrorCode_t       oC_Icmp_ReleaseType                 ( oC_Icmp_Type_t Type    , oC_Time_t    Timeout );
extern oC_ErrorCode_t       oC_Icmp_ReleaseAllTypesReservedBy   ( oC_Process_t   Process , oC_Time_t    Timeout );
extern oC_ErrorCode_t       oC_Icmp_CalculateChecksum           ( oC_Icmp_Packet_t * Packet , uint16_t * outChecksum );
extern oC_Icmp_Packet_t*    oC_Icmp_Packet_New                  ( Allocator_t PacketAllocator , oC_Net_PacketType_t Type , oC_Icmp_Type_t IcmpType , uint8_t Code , const void * Data , uint16_t Size );
extern bool                 oC_Icmp_Packet_SetSize              ( oC_Icmp_Packet_t * Packet , uint16_t Size );
extern bool                 oC_Icmp_Packet_Delete               ( oC_Icmp_Packet_t ** Packet );
extern void *               oC_Icmp_Packet_GetMessageReference  ( oC_Icmp_Packet_t * Packet );
extern oC_ErrorCode_t       oC_Icmp_Send                        ( oC_Netif_t Netif , const oC_Net_Address_t * Destination , oC_Icmp_Packet_t * Packet , oC_Time_t Timeout );
extern oC_ErrorCode_t       oC_Icmp_Receive                     ( oC_Netif_t Netif , oC_Icmp_Packet_t * outPacket , oC_Icmp_Type_t Type , oC_Time_t Timeout );

/** @} */
#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_PROTOCOLS_ICMP_OC_ICMP_H_ */
