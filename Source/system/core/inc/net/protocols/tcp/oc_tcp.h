/** ****************************************************************************************************************************************
 *
 * @brief      File with interface of the TCP protocol
 *
 * @file       oc_tcpip.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Tcp Tcp - Transmission Control Protocol
 * @ingroup CoreNetwork
 * @brief Module that handles transmission in the TCP IP
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_PROTOCOLS_TCP_OC_TCP_H_
#define SYSTEM_CORE_INC_NET_PROTOCOLS_TCP_OC_TCP_H_

#include <oc_errors.h>
#include <oc_time.h>
#include <oc_net.h>
#include <oc_process.h>
#include <oc_debug.h>
#include <oc_debug_cfg.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#if CFG_ENABLE_TCP_LOGS == ON
#   define tcplog( LogType, ... )       kdebuglog(LogType, __VA_ARGS__)
#else
#   define tcplog( LogType, ... )
#endif

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________




/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup Tcp
 * @{
 */

//==========================================================================================================================================
/**
 * @brief Stores Group of callback
 */
//==========================================================================================================================================
typedef enum
{
    oC_Tcp_CallbackGroup_User,                 //!< Callback ID used by TCP module user
    oC_Tcp_CallbackGroup_Internal,             //!< Callback ID used by TCP internal implementation
    oC_Tcp_CallbackGroup_NumberOfElements      //!< Number of elements in this enum
} oC_Tcp_CallbackGroup_t;

//==========================================================================================================================================
/**
 * @brief Stores ID of event
 */
//==========================================================================================================================================
typedef enum
{
    oC_Tcp_EventId_ConnectionFinished,          //!< Called when the TCP connection has been finished
    oC_Tcp_EventId_NewConnectionVerification,   //!< Called when new TCP connection is received and server needs to verify if this connection is known
    oC_Tcp_EventId_NumberOfElements             //!< Number of elements in this enum
} oC_Tcp_EventId_t;

//==========================================================================================================================================
/**
 * @brief stores TCP port
 */
//==========================================================================================================================================
typedef enum
{
    oC_Tcp_Port_Empty                                       = 0 ,                       //!< Special value for marks port as not filled
    oC_Tcp_Port_MAX                                         = UINT16_MAX ,              //!< maximum number of ports
    oC_Tcp_Port_IsSpecified                                 = 1 , //!< is specified.
    oC_Tcp_Port_RemoteJobEntry                              = 5 , //!<   Remote job entry
    oC_Tcp_Port_EchoProtocol                                = 7 , //!<     Echo Protocol
    oC_Tcp_Port_DiscardProtocol                             = 9 , //!<  Discard Protocol
    oC_Tcp_Port_ActiveUsers                                 = 11 , //!<     Active Users

    oC_Tcp_Port_DaytimeProtocol                             = 13 , //!<     Daytime Protocol

    oC_Tcp_Port_PreviouslyNetstatService                    = 15 , //!< Previously netstat service

    oC_Tcp_Port_QOTD                                        = 17 , //!<    Quote of the Day
    oC_Tcp_Port_MessageSendProtocol                         = 18 , //!<     Message Send Protocol
    oC_Tcp_Port_CHARGEN                                     = 19 , //!<     Character Generator Protocol
    oC_Tcp_Port_FTPDataTransfer                             = 20 , //!<     File Transfer Protocol  data transfer
    oC_Tcp_Port_FTPControl                                  = 21 , //!<  File Transfer Protocol  control
    oC_Tcp_Port_SSH                                         = 22 , //!<     Secure Shell , secure logins, file transfers  and port forwarding
    oC_Tcp_Port_TelnetProtocolUnencrypted                   = 23 , //!<   Telnet protocol—unencrypted text communications
    oC_Tcp_Port_SMTP                                        = 25 , //!<    Simple Mail Transfer Protocol , used for e-mail routing between mail servers

    oC_Tcp_Port_TimeProtocol                                = 37 , //!<    Time Protocol
    oC_Tcp_Port_RAP                                         = 38 , //!<     Route Access Protocol
    oC_Tcp_Port_RLPused                                     = 39 , //!<     Resource Location Protocol —used for determining the location of higher level services from hosts on a network

    oC_Tcp_Port_HostNameServerProtocol                      = 42 , //!<  Host Name Server Protocol
    oC_Tcp_Port_WHOISProtocol                               = 43 , //!<   WHOIS protocol
    oC_Tcp_Port_TACACS                                      = 49 , //!<  TACACS+ Login Host protocol
    oC_Tcp_Port_RemoteMailCheckingProtocol                  = 50 , //!<  Remote Mail Checking Protocol

    oC_Tcp_Port_PreviouslyInterfaceMessageProcessorlogical  = 51 , //!< Previously Interface Message Processor logical address management
    oC_Tcp_Port_XNSTimeProtocol                             = 52 , //!<     Xerox Network Systems  Time Protocol
    oC_Tcp_Port_DNS                                         = 53 , //!<     Domain Name System
    oC_Tcp_Port_XNSClearinghouse                            = 54 , //!<    Xerox Network Systems  clearinghouse
    oC_Tcp_Port_XNSAuthentication                           = 56 , //!<   Xerox Network Systems  authentication
    oC_Tcp_Port_AnyPrivateTerminalAccess                    = 57 , //!<    Any private terminal access
    oC_Tcp_Port_XNSMail                                     = 58 , //!<     Xerox Network Systems  Mail
    oC_Tcp_Port_BOOTPServer                                 = 67 , //!<     Bootstrap Protocol  server; also used by Dynamic Host Configuration Protocol
    oC_Tcp_Port_BOOTPClient                                 = 68 , //!<     Bootstrap Protocol  client; also used by Dynamic Host Configuration Protocol
    oC_Tcp_Port_TFTP                                        = 69 , //!<    Trivial File Transfer Protocol
    oC_Tcp_Port_GopherProtocol                              = 70 , //!<  Gopher protocol
    oC_Tcp_Port_NETRJSProtocol                              = 74 , //!<  NETRJS protocol
    oC_Tcp_Port_AnyPrivateDialOutService                    = 75 , //!<    Any private dial out service
    oC_Tcp_Port_AnyPrivateRemotejob                         = 77 , //!<     Any private Remote job entry
    oC_Tcp_Port_FingerProtocol                              = 79 , //!<  Finger protocol
    oC_Tcp_Port_HTTP                                        = 80 , //!<    Hypertext Transfer Protocol
    oC_Tcp_Port_TorParkOnionRouting                         = 81 , //!< TorPark onion routing
    oC_Tcp_Port_KerberosAuthenticationSystem                = 88 , //!<    Kerberos authentication system
    oC_Tcp_Port_DnsixSecuritAttributeTokenMap               = 90 , //!<   dnsix  Securit  Attribute Token Map
    oC_Tcp_Port_dotcom                                      = 90 , //!< PointCast
    oC_Tcp_Port_WIPMessageProtocol                          = 99 , //!< WIP message protocol
    oC_Tcp_Port_NICHostName                                 = 101 , //!<    NIC host name
    oC_Tcp_Port_TSAPClass0                                  = 102 , //!<     ISO Transport Service Access Point  Class 0 protocol;
    oC_Tcp_Port_DigitalImagingand                           = 104 , //!<  Digital Imaging and Communications in Medicine
    oC_Tcp_Port_CCSONameserver                              = 105 , //!<     CCSO Nameserver
    oC_Tcp_Port_RTelnet                                     = 107 , //!<    Remote User Telnet Service
    oC_Tcp_Port_SNAGatewayAccessServer                      = 108 , //!<     IBM Systems Network Architecture  gateway access server
    oC_Tcp_Port_PostOfficeProtocolVer2                      = 109 , //!<     Post Office Protocol, version 2
    oC_Tcp_Port_PostOfficeProtocolVer3                      = 110 , //!<     Post Office Protocol, version 3
    oC_Tcp_Port_OpenNetworkComputingRemoteProcedureCall     = 111 , //!<    Open Network Computing Remote Procedure Call
    oC_Tcp_Port_Ident                                       = 113 , //!< Ident, authentication service/identification protocol, used by IRC servers to identify users
    oC_Tcp_Port_Connection                                  = 113 , //!< connection.
    oC_Tcp_Port_SimpleFileTransferProtocol                  = 115 , //!<     Simple File Transfer Protocol
    oC_Tcp_Port_UUCPMappingProject                          = 117 , //!<     UUCP Mapping Project
    oC_Tcp_Port_SQLServices                                 = 118 , //!<    Structured Query Language  Services
    oC_Tcp_Port_NNTP                                        = 119 , //!<   Network News Transfer Protocol , retrieval of newsgroup messages
    oC_Tcp_Port_NTP                                         = 123 , //!<    Network Time Protocol , used for time synchronization
    oC_Tcp_Port_FormerlyUnisysUnitaryLogin                  = 126 , //!<     Formerly Unisys Unitary Login, renamed by Unisys to NXEdit. Used by Unisys Programmer's Workbench for Clearpath MCP, an IDE for Unisys MCP software development
    oC_Tcp_Port_DCEEndpointResolution                       = 135 , //!<  DCE endpoint resolution
    oC_Tcp_Port_MicrosoftEPMAP                              = 135 , //!<     Microsoft EPMAP , also known as DCE/RPC Locator service, used to remotely manage services including DHCP server, DNS server and WINS. Also used by DCOM
    oC_Tcp_Port_NetBIOSNameService                          = 137 , //!<     NetBIOS Name Service, used for name registration and resolution
    oC_Tcp_Port_NetBIOSDatagramService                      = 138 , //!<     NetBIOS Datagram Service
    oC_Tcp_Port_NetBIOSSessionService                       = 139 , //!<  NetBIOS Session Service
    oC_Tcp_Port_Imap                                        = 143 , //!<   Internet Message Access Protocol , management of electronic mail messages on a server
    oC_Tcp_Port_BFTP                                        = 152 , //!<   Background File Transfer Program
    oC_Tcp_Port_SGMP                                        = 153 , //!<   Simple Gateway Monitoring Protocol , a protocol for remote inspection and alteration of gateway management information
    oC_Tcp_Port_SQLService                                  = 156 , //!<     Structured Query Language  Service
    oC_Tcp_Port_DistributedMailSystemProtocol               = 158 , //!<  Distributed Mail System Protocol
    oC_Tcp_Port_SNMP                                        = 161 , //!<   Simple Network Management Protocol
    oC_Tcp_Port_SNMPTRAP                                    = 162 , //!<   Simple Network Management Protocol Trap
    oC_Tcp_Port_PrintServer                                 = 170 , //!<    Print server
    oC_Tcp_Port_XDMCP                                       = 177 , //!<  X Display Manager Control Protocol , used for remote logins to an X Display Manager server
    oC_Tcp_Port_BGP                                         = 179 , //!<    Border Gateway Protocol , used to exchange routing and reachability information among autonomous systems  on the Internet
    oC_Tcp_Port_IRC                                         = 194 , //!<    Internet Relay Chat
    oC_Tcp_Port_SMUX                                        = 199 , //!<   SNMP multiplexing protocol
    oC_Tcp_Port_AppleTalkRoutingMaintenance                 = 201 , //!<    AppleTalk Routing Maintenance
    oC_Tcp_Port_QuickMailTransferProtocol                   = 209 , //!<  Quick Mail Transfer Protocol
    oC_Tcp_Port_ANSIZ3950                                   = 210 , //!<  ANSI Z39.50
    oC_Tcp_Port_IPX                                         = 213 , //!<    Internetwork Packet Exchange
    oC_Tcp_Port_MPP                                         = 218 , //!<    Message posting protocol
    oC_Tcp_Port_ImapVer3                                    = 220 , //!<   Internet Message Access Protocol , version 3
    oC_Tcp_Port_ESRO                                        = 259 , //!<   Efficient Short Remote Operations
    oC_Tcp_Port_Arcisdms                                    = 262 , //!<   Arcisdms
    oC_Tcp_Port_BGMP                                        = 264 , //!<   Border Gateway Multicast Protocol
    oC_Tcp_Port_Httpmgmt                                    = 280 , //!<   http-mgmt
    oC_Tcp_Port_ThinLincWebAccess                           = 300 , //!< ThinLinc Web Access
    oC_Tcp_Port_NovastorOnlineBackup                        = 308 , //!< Novastor Online Backup
    oC_Tcp_Port_MacOSXServerAdmin                           = 311 , //!< Mac OS X Server Admin
    oC_Tcp_Port_TSP                                         = 318 , //!<    PKIX Time Stamp Protocol


    oC_Tcp_Port_MATIPTypeA                                  = 350 , //!<     Mapping of Airline Traffic over Internet Protocol  type A
    oC_Tcp_Port_MATIPTypeB                                  = 351 , //!<     MATIP type B
    oC_Tcp_Port_Cloantonet                                  = 356 , //!<     cloanto-net-1
    oC_Tcp_Port_OnDemand                                    = 366 , //!<   On-Demand Mail Relay
    oC_Tcp_Port_Rpc2portmap                                 = 369 , //!<    Rpc2portmap
    oC_Tcp_Port_Codaauth2                                   = 370 , //!< codaauth2, Coda authentication server

    oC_Tcp_Port_ClearCaseAlbd                               = 371 , //!<  ClearCase albd
    oC_Tcp_Port_HPDataAlarmManager                          = 383 , //!<     HP data alarm manager
    oC_Tcp_Port_ARemoteNetworkServerSystem                  = 384 , //!<     A Remote Network Server System
    oC_Tcp_Port_AURP                                        = 387 , //!<   AURP
    oC_Tcp_Port_LDAP                                        = 389 , //!<   Lightweight Directory Access Protocol
    oC_Tcp_Port_DigitalEquipmentCorporationDECnet           = 399 , //!<  Digital Equipment Corporation DECnet  over TCP/IP
    oC_Tcp_Port_UPS                                         = 401 , //!<    Uninterruptible power supply
    oC_Tcp_Port_SLP                                         = 427 , //!<    Service Location Protocol
    oC_Tcp_Port_NNSP                                        = 433 , //!<   NNSP, part of Network News Transfer Protocol
    oC_Tcp_Port_MobileIPAgent                               = 434 , //!<  Mobile IP Agent
    oC_Tcp_Port_HypertextTransferProtocolover               = 443 , //!<  Hypertext Transfer Protocol over TLS/SSL

    oC_Tcp_Port_SNPP                                        = 444 , //!<   Simple Network Paging Protocol , RFC 1568
    oC_Tcp_Port_WellKnownSlitherio                          = 444 , //!< Well known Slither.io port
    oC_Tcp_Port_MicrosoftDS                                 = 445 , //!< Microsoft-DS Active Directory, Windows shares
    oC_Tcp_Port_SMBFS                                       = 445 , //!< Microsoft-DS SMB file sharing
    oC_Tcp_Port_KerberosChangeSet                           = 464 , //!<  Kerberos Change/Set password
    oC_Tcp_Port_URLRendezvousDirectoryfor                   = 465 , //!< URL Rendezvous Directory for SSM
    oC_Tcp_Port_AuthenticatedSMTPover                       = 465 , //!< Authenticated SMTP over TLS/SSL
    oC_Tcp_Port_Tcpnethaspsrv                               = 475 , //!<  tcpnethaspsrv, Aladdin Knowledge Systems Hasp services
    oC_Tcp_Port_GOGlobal                                    = 491 , //!< GO-Global remote access and application publishing software
    oC_Tcp_Port_DantzRetrospect                             = 497 , //!< Dantz Retrospect
    oC_Tcp_Port_ISAKMPInternetKeyExchange                   = 500 , //!<  Internet Security Association and Key Management Protocol  / Internet Key Exchange
    oC_Tcp_Port_ModbusProtocol                              = 502 , //!<     Modbus Protocol
    oC_Tcp_Port_Citadel                                     = 504 , //!<    Citadel, multiservice protocol for dedicated clients for the Citadel groupware system
    oC_Tcp_Port_FCP                                         = 510 , //!<    FirstClass Protocol , used by FirstClass client/server groupware system
    oC_Tcp_Port_Rexec                                       = 512 , //!< Rexec, Remote Process Execution

    oC_Tcp_Port_Rlogin                                      = 513 , //!< rlogin

    oC_Tcp_Port_RemoteShell                                 = 514 , //!< Remote Shell, used to execute non-interactive commands on a remote system

    oC_Tcp_Port_LPD                                         = 515 , //!< Line Printer Daemon , print service


    oC_Tcp_Port_Efs                                         = 520 , //!< efs, extended file name server


    oC_Tcp_Port_NCPIsUsedForAVarietyThingsSuchAsAccessToPrimaryNetWareserver = 524 , //!<   NetWare Core Protocol  is used for a variety things such as access to primary NetWare server resources, Time Synchronization, etc.

    oC_Tcp_Port_RPC                                         = 530 , //!<    Remote procedure call
    oC_Tcp_Port_AOLInstantMessenger                         = 531 , //!< AOL Instant Messenger
    oC_Tcp_Port_Netnews                                     = 532 , //!< netnews

    oC_Tcp_Port_Unixto                                      = 540 , //!< Unix-to-Unix Copy Protocol
    oC_Tcp_Port_Commerce                                    = 542 , //!<   commerce
    oC_Tcp_Port_Klogin                                      = 543 , //!< klogin, Kerberos login
    oC_Tcp_Port_Kshell                                      = 544 , //!< kshell, Kerberos Remote shell
    oC_Tcp_Port_VMS                                         = 545 , //!< OSIsoft PI , OSISoft PI Server Client Access
    oC_Tcp_Port_DHCPv6Client                                = 546 , //!<   DHCPv6 client
    oC_Tcp_Port_DHCPv6Server                                = 547 , //!<   DHCPv6 server
    oC_Tcp_Port_AFPOverTCP                                  = 548 , //!< Apple Filing Protocol  over TCP
    oC_Tcp_Port_Newrwho                                     = 550 , //!<    new-rwho, new-who
    oC_Tcp_Port_RTSP                                        = 554 , //!<   Real Time Streaming Protocol
    oC_Tcp_Port_Remotefs                                    = 556 , //!< Remotefs, RFS, rfs_server


    oC_Tcp_Port_NNTPOverTLSSSL                              = 563 , //!<     NNTP over TLS/SSL
    oC_Tcp_Port_9P                                          = 564 , //!< 9P

    oC_Tcp_Port_Email                                       = 587 , //!< e-mail message submission
    oC_Tcp_Port_FileMaker6                                  = 591 , //!< FileMaker 6.0  Web Sharing
    oC_Tcp_Port_HTTPRPCEpMap                                = 593 , //!<   HTTP RPC Ep Map, Remote procedure call over Hypertext Transfer Protocol, often used by Distributed Component Object Model services and Microsoft Exchange Server
    oC_Tcp_Port_ReliableSyslogService                       = 601 , //!< Reliable Syslog Service — used for system logging
    oC_Tcp_Port_TUNNELProfile                               = 604 , //!< TUNNEL profile, a protocol for BEEP peers to form an application layer tunnel

    oC_Tcp_Port_ODProxy                                     = 625 , //!< Open Directory Proxy
    oC_Tcp_Port_IPP                                         = 631 , //!<    Internet Printing Protocol
    oC_Tcp_Port_CUPSAdministrationConsole                   = 631 , //!< Common Unix Printing System  administration console
    oC_Tcp_Port_RLZDBase                                    = 635 , //!<   RLZ DBase
    oC_Tcp_Port_LightweightDirectoryAccessProtocolover      = 636 , //!<     Lightweight Directory Access Protocol over TLS/SSL
    oC_Tcp_Port_MSDP                                        = 639 , //!<   MSDP, Multicast Source Discovery Protocol
    oC_Tcp_Port_SupportSoftNexusRemoteCommand               = 641 , //!<  SupportSoft Nexus Remote Command , a proxy gateway connecting remote control traffic
    oC_Tcp_Port_SANity                                      = 643 , //!<     SANity
    oC_Tcp_Port_LDP                                         = 646 , //!<    Label Distribution Protocol , a routing protocol used in MPLS networks
    oC_Tcp_Port_DHCPFailoverProtocol                        = 647 , //!< DHCP Failover protocol
    oC_Tcp_Port_RRP                                         = 648 , //!< Registry Registrar Protocol
    oC_Tcp_Port_IEEEMMS                                     = 651 , //!<    IEEE-MMS
    oC_Tcp_Port_data                                        = 653 , //!<   SupportSoft Nexus Remote Command , a proxy gateway connecting remote control traffic
    oC_Tcp_Port_MMSMediaManagementProtocol                  = 654 , //!< Media Management System  Media Management Protocol
    oC_Tcp_Port_TincVPNdaemon                               = 655 , //!<  Tinc VPN daemon
    oC_Tcp_Port_IBMRMC                                      = 657 , //!<     IBM RMC  protocol, used by System p5 AIX Integrated Virtualization Manager  and Hardware Management Console to connect managed logical partitions  to enable dynamic partition reconfiguration
    oC_Tcp_Port_MacOSXServeradministration                  = 660 , //!< Mac OS X Server administration, version 10.4 and earlier
    oC_Tcp_Port_Doom                                        = 666 , //!<   Doom, first online first-person shooter
    oC_Tcp_Port_Airservng                                   = 666 , //!< airserv-ng, aircrack-ng's server for remote-controlling wireless devices
    oC_Tcp_Port_ACAP                                        = 674 , //!< Application Configuration Access Protocol
    oC_Tcp_Port_REALMRUSD                                   = 688 , //!<  REALM-RUSD
    oC_Tcp_Port_VATP                                        = 690 , //!<   Velneo Application Transfer Protocol
    oC_Tcp_Port_MSExchangeRouting                           = 691 , //!< MS Exchange Routing
    oC_Tcp_Port_LinuxHA                                     = 694 , //!<    Linux-HA high-availability heartbeat
    oC_Tcp_Port_IEEEMediaManagementSystemover               = 695 , //!< IEEE Media Management System over SSL

    oC_Tcp_Port_EPP                                         = 700 , //!< Extensible Provisioning Protocol , a protocol for communication between domain name registries and registrars
    oC_Tcp_Port_LMP                                         = 701 , //!< Link Management Protocol , a protocol that runs between a pair of nodes and is used to manage traffic engineering  links
    oC_Tcp_Port_IRISOverBEEP                                = 702 , //!< IRIS  over BEEP
    oC_Tcp_Port_SILC                                        = 706 , //!< Secure Internet Live Conferencing
    oC_Tcp_Port_CiscoTagDistributionProtocolbeing           = 711 , //!< Cisco Tag Distribution Protocol—being replaced by the MPLS Label Distribution Protocol
    oC_Tcp_Port_TopologyBroadcastbased                      = 712 , //!< Topology Broadcast based on Reverse-Path Forwarding routing protocol
    oC_Tcp_Port_protocolAdministration                      = 749 , //!<     Kerberos  administration

    oC_Tcp_Port_Kerberos_master                             = 751 , //!< kerberos_master, Kerberos authentication

    oC_Tcp_Port_RRH                                         = 753 , //!< Reverse Routing Header


    oC_Tcp_Port_TellSend                                    = 754 , //!< tell send
    oC_Tcp_Port_Krb5_prop                                   = 754 , //!< krb5_prop, Kerberos v5 slave propagation

    oC_Tcp_Port_KrbupdateKerberosregistration               = 760 , //!< krbupdate , Kerberos registration
    oC_Tcp_Port_ConserverSerialconsole                      = 782 , //!< Conserver serial-console management server
    oC_Tcp_Port_SpamAssassinSpamdDaemon                     = 783 , //!< SpamAssassin spamd daemon
    oC_Tcp_Port_Mdbsdaemon                                  = 800 , //!<     mdbs-daemon
    oC_Tcp_Port_PortSharingService                          = 808 , //!< Port Sharing Service
    oC_Tcp_Port_CertificateManagementProtocol               = 829 , //!< Certificate Management Protocol
    oC_Tcp_Port_NETCONFOverSSH                              = 830 , //!<     NETCONF over SSH
    oC_Tcp_Port_NETCONFOverBEEP                             = 831 , //!<    NETCONF over BEEP
    oC_Tcp_Port_NETCONFForSOAPoverHttps                     = 832 , //!<     NETCONF for SOAP over HTTPS
    oC_Tcp_Port_NETCONFForSOAPoverBeep                      = 833 , //!<     NETCONF for SOAP over BEEP
    oC_Tcp_Port_AdobeFlash                                  = 843 , //!< Adobe Flash
    oC_Tcp_Port_DHCPFailoverProtocol2                       = 847 , //!< DHCP Failover protocol
    oC_Tcp_Port_GDOIProtocol                                = 848 , //!<   Group Domain Of Interpretation  protocol
    oC_Tcp_Port_DNSOverTLS                                  = 853 , //!<     DNS over TLS
    oC_Tcp_Port_ISCSI                                       = 860 , //!< iSCSI
    oC_Tcp_Port_OWAMPControl                                = 861 , //!<   OWAMP control
    oC_Tcp_Port_TWAMPControl                                = 862 , //!<   TWAMP control
    oC_Tcp_Port_RsyncFileSynchronizationProtocol            = 873 , //!< rsync file synchronization protocol
    oC_Tcp_Port_Cddbp                                       = 888 , //!< cddbp, CD DataBase  protocol
    oC_Tcp_Port_IBMEndpointManagerRemoteControl             = 888 , //!< IBM Endpoint Manager Remote Control
    oC_Tcp_Port_BrocadeSmisRpc                              = 897 , //!< Brocade SMI-S RPC
    oC_Tcp_Port_BrocadeSmisSsl                              = 898 , //!< Brocade SMI-S RPC SSL
    oC_Tcp_Port_VMwareESXi_902                              = 902 , //!< VMware ESXi
    oC_Tcp_Port_VMwareESXi_903                              = 903 , //!< VMware ESXi
    oC_Tcp_Port_RNDC                                        = 953 , //!< BIND remote name daemon control
    oC_Tcp_Port_RemoteHTTPSmanagement                       = 981 , //!< Remote HTTPS management for firewall devices running embedded Check Point VPN-1 software
    oC_Tcp_Port_MicrosoftWindowsSBSSharePoint               = 987 , //!< Microsoft Windows SBS SharePoint
    oC_Tcp_Port_FTPSData                                    = 989 , //!<   FTPS Protocol , FTP over TLS/SSL
    oC_Tcp_Port_FTPSControl                                 = 990 , //!<    FTPS Protocol , FTP over TLS/SSL
    oC_Tcp_Port_NAS                                         = 991 , //!<    Netnews Administration System
    oC_Tcp_Port_TelnetProtocolOverTLSSSL                    = 992 , //!<   Telnet protocol over TLS/SSL
    oC_Tcp_Port_InternetMessageAccessProtocolover           = 993 , //!< Internet Message Access Protocol over TLS/SSL

    oC_Tcp_Port_InternetRelayChatover                       = 994 , //!< Internet Relay Chat over TLS/SSL
    oC_Tcp_Port_PostOfficeProtocol3                         = 995 , //!< Post Office Protocol 3 over TLS/SSL
    oC_Tcp_Port_ScimoreDBDatabaseSystem                     = 999 , //!< ScimoreDB Database System
    oC_Tcp_Port_ThinLincWebbased                            = 1010 , //!< ThinLinc web-based administration interface

    oC_Tcp_Port_NumberOfSpecialPorts                        = 1024 ,
} oC_Tcp_Port_t;

//==========================================================================================================================================
/**
 * @brief stores TCP connection data
 */
//==========================================================================================================================================
typedef struct Tcp_Connection_t * oC_Tcp_Connection_t;

//==========================================================================================================================================
/**
 * @brief stores TCP header
 *
 * The type is for storing header of the TCP packet
 */
//==========================================================================================================================================
typedef struct
{
#ifdef LITTLE_ENDIAN
    uint16_t                SourcePort;
    uint16_t                DestinationPort;
    uint32_t                SequenceNumber;
    uint32_t                AcknowledgmentNumber;
    uint32_t                DataOffset:4;
    uint32_t                Reserved:3;
    uint32_t                NS:1;
    union PACKED
    {
        struct PACKED
        {
            uint32_t                CWR:1;
            uint32_t                ECE:1;
            uint32_t                URG:1;
            uint32_t                ACK:1;
            uint32_t                PSH:1;
            uint32_t                RST:1;
            uint32_t                SYN:1;
            uint32_t                FIN:1;
        };
        struct PACKED
        {
            uint32_t                FIN:1;
            uint32_t                SYN:1;
            uint32_t                RST:1;
            uint32_t                PSH:1;
            uint32_t                ACK:1;
            uint32_t                URG:1;
            uint32_t                ECE:1;
            uint32_t                CWR:1;
        } NetworkOrder;
        uint8_t             Flags;
    };
    uint16_t                WindowSize;
    uint16_t                Checksum;
    uint16_t                UrgentPointer;
#elif defined(BIG_ENDIAN)
#   error Structure is not defined for BIG_ENDIAN
#else
#   error Endianess is not defined
#endif
} oC_Tcp_Header_t;

//==========================================================================================================================================
/**
 * @brief stores TCP packet
 *
 * The type is for storing TCP packet
 */
//==========================================================================================================================================
typedef union
{
    oC_Net_Packet_t             Packet;         //!< Network Packet

    struct
    {
        oC_Net_Ipv4PacketHeader_t       IpHeader;       //!< IPv4 Header
        oC_Tcp_Header_t                 TcpHeader;      //!< TCP Header
        uint8_t                         Data[4];        //!< Data buffer
    } IPv4;

    struct
    {
        oC_Net_Ipv6PacketHeader_t       IpHeader;       //!< IPv6 Header
        oC_Tcp_Header_t                 TcpHeader;      //!< TCP Header
        uint8_t                         Data[4];        //!< Data buffer
    } IPv6;
} oC_Tcp_Packet_t;

//==========================================================================================================================================
/**
 * @brief stores TCP server object data
 *
 * The type is for storing TCP server data. It should be created by using the function #oC_Tcp_Server_New and deleted by the function
 * #oC_Tcp_Server_Delete. All data inside are private and should be changed only by TCP server interface functions.
 */
//==========================================================================================================================================
typedef struct Server_t * oC_Tcp_Server_t;

//==========================================================================================================================================
/**
 * @brief stores TCP Option kinds
 */
//==========================================================================================================================================
typedef enum
{
    oC_Tcp_OptionKind_End                               = 0x00 ,        //!< End of Option List
    oC_Tcp_OptionKind_Nop                               = 0x01 ,        //!< No-Operation
    oC_Tcp_OptionKind_MaxSegmentSize                    = 0x02 ,        //!< Maximum Segment Size
    oC_Tcp_OptionKind_WindowScale                       = 0x03 ,        //!< Window Scale
    oC_Tcp_OptionKind_SackPermitted                     = 0x04 ,        //!< Sack Permitted
    oC_Tcp_OptionKind_Sack                              = 0x05 ,        //!< Selective Acknowledgment
    oC_Tcp_OptionKind_Echo                              = 0x06 ,        //!< Echo
    oC_Tcp_OptionKind_EchoReplay                        = 0x07 ,        //!< Echo Replay
    oC_Tcp_OptionKind_Timestamps                        = 0x08 ,        //!< Timestamps
    oC_Tcp_OptionKind_PartialOrderConnectionPermitted   = 0x09 ,        //!< Partial Order Connection Permitted
    oC_Tcp_OptionKind_PartialOrderServiceProfile        = 0x0A ,        //!< Partial Order Service Profile
    oC_Tcp_OptionKind_CC                                = 0x0B ,        //!< Connection Count
    oC_Tcp_OptionKind_CCNew                             = 0x0C ,        //!< Connection Count New
    oC_Tcp_OptionKind_CCEcho                            = 0x0D ,        //!< Connection Count Echo
    oC_Tcp_OptionKind_TcpAlternateChecksumRequest       = 0x0E ,        //!< TCP Alternate Checksum Request
    oC_Tcp_OptionKind_TcpAlternateChecksumData          = 0x0F ,        //!< TCP Alternate Checksum Data
    oC_Tcp_OptionKind_Skeeter                           = 0x10 ,        //!< Skeeter
    oC_Tcp_OptionKind_Bubba                             = 0x11 ,        //!< Bubba
    oC_Tcp_OptionKind_TrailerChecksum                   = 0x12 ,        //!< Trailer Checksum
    oC_Tcp_OptionKind_Md5Signature                      = 0x13 ,        //!< MD5 Signature
    oC_Tcp_OptionKind_ScpsCapabilities                  = 0x14 ,        //!< SCPS Capabilities
    oC_Tcp_OptionKind_SelectiveNegativeAcknowledgements = 0x15 ,        //!< Selective Negative Acknowledgements
    oC_Tcp_OptionKind_RecordBoundaries                  = 0x16 ,        //!< Record Boundaries
    oC_Tcp_OptionKind_CorruptionExperencied             = 0x17 ,        //!< Corruption Experienced
    oC_Tcp_OptionKind_Snap                              = 0x18 ,        //!< SNAP
    oC_Tcp_OptionKind_TcpCompressionFilter              = 0x1A ,        //!< TCP Compression Filter
    oC_Tcp_OptionKind_QuickStartResponse                = 0x1B ,        //!< Quick-Start Response
    oC_Tcp_OptionKind_UserTimeout                       = 0x1C ,        //!< User Timeout
    oC_Tcp_OptionKind_TcpAuthentication                 = 0x1D ,        //!< TCP Authentication
    oC_Tcp_OptionKind_MultipathTcp                      = 0x1E ,        //!< Multipath TCP
    oC_Tcp_OptionKind_FastOpenCookie                    = 0x22 ,        //!< Fast Open Cookie
    oC_Tcp_OptionKind_Max                               = 0xFF ,        //!< Maximum Option Kind
} oC_Tcp_OptionKind_t;

//==========================================================================================================================================
/**
 * @brief callback for connection events
 * 
 * The type is for storing callback for connection events
 * 
 * @param Connection        connection object
 * @param Parameter         optional user parameter
 * @param EventParameter    Parameter specific to the event
 */
//==========================================================================================================================================
typedef bool (*oC_Tcp_ConnectionFunction_t)(oC_Tcp_Connection_t Connection , void * Parameter, void* EventParameter );

//==========================================================================================================================================
/**
 * @brief stores TCP connection event IDs
 */
//==========================================================================================================================================
typedef struct
{
    oC_Tcp_ConnectionFunction_t     Function;       //!< Function to call
    void *                          Parameter;      //!< Parameter to pass to the function
} oC_Tcp_Connection_Callback_t;

//==========================================================================================================================================
/**
 * @brief stores callbacks for the connection
 * 
 * The type is for storing callbacks for the connection for all events
 */
//==========================================================================================================================================
typedef oC_Tcp_Connection_Callback_t oC_Tcp_Connection_Callbacks_t[oC_Tcp_CallbackGroup_NumberOfElements][oC_Tcp_EventId_NumberOfElements];

//==========================================================================================================================================
/**
 * @brief stores TCP connection configuration
 * 
 * The type is for storing configuration for the TCP connection
 */
//==========================================================================================================================================
typedef struct
{
    uint32_t                            InitialSequenceNumber;       //!< Initial sequence number for the connection
    uint32_t                            InitialAcknowledgeNumber;    //!< Initial acknowledge number for the connection
    uint16_t                            LocalWindowSize;             //!< Local window size for the connection
    uint8_t                             LocalWindowScale;            //!< Local window scale factor for the connection
    oC_Time_t                           ConfirmationTimeout;         //!< Timeout for connection confirmation
    oC_Time_t                           ExpirationTimeout;           //!< Timeout for connection expiration
    oC_Time_t                           SendingAcknowledgeTimeout;   //!< Timeout for sending acknowledgements
    oC_Time_t                           ReadSegmentTimeout;          //!< Timeout for reading segments
    oC_Time_t                           SaveSegmentTimeout;          //!< Timeout for saving segments
    oC_Time_t                           ReceiveTimeout;              //!< Timeout for receiving data
    oC_Time_t                           ResendSegmentTime;           //!< Time interval for resending segments
    oC_Net_Address_t                    LocalAddress;                //!< Local address for the connection
    oC_Net_Address_t                    RemoteAddress;               //!< Remote address for the connection
    oC_MemorySize_t                     PacketSize;                  //!< Size of the packets for the connection
    oC_Tcp_Connection_Callbacks_t       Callbacks;                   //!< Callbacks for connection events
} oC_Tcp_Connection_Config_t;

/** @} */
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
/**
 * @addtogroup Tcp
 * @{
 */

extern oC_ErrorCode_t           oC_Tcp_TurnOn                       ( void );
extern oC_ErrorCode_t           oC_Tcp_TurnOff                      ( void );
extern oC_ErrorCode_t           oC_Tcp_CloseProcess                 ( oC_Process_t Process , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_ReservePort                  ( oC_Tcp_Port_t * Port , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_ReleasePort                  ( oC_Tcp_Port_t Port , oC_Time_t Timeout );
extern bool                     oC_Tcp_IsPortReserved               ( oC_Tcp_Port_t Port );
extern bool                     oC_Tcp_IsPortReservedBy             ( oC_Tcp_Port_t Port , oC_Process_t Process );

extern oC_ErrorCode_t           oC_Tcp_Connect                      ( const oC_Net_Address_t * Destination , oC_Tcp_Port_t LocalPort , oC_Tcp_Connection_t * outConnection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Disconnect                   ( oC_Tcp_Connection_t * Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Listen                       ( const oC_Net_Address_t * Source , oC_Tcp_Server_t * outServer , uint32_t MaxConnections , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_StopListen                   ( oC_Tcp_Server_t * Server , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Accept                       ( oC_Tcp_Server_t Server , oC_Tcp_Connection_t * outConnection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Send                         ( oC_Tcp_Connection_t Connection, const void *    Buffer, oC_MemorySize_t Size, oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Receive                      ( oC_Tcp_Connection_t Connection,       void * outBuffer, oC_MemorySize_t *Size, oC_Time_t Timeout );

extern oC_Tcp_Server_t          oC_Tcp_Server_New                   ( const oC_Net_Address_t * ListenAddress , uint32_t MaxConnections );
extern bool                     oC_Tcp_Server_Delete                ( oC_Tcp_Server_t * Server , oC_Time_t Timeout );
extern bool                     oC_Tcp_Server_IsCorrect             ( oC_Tcp_Server_t Server );
extern bool                     oC_Tcp_Server_IsRunning             ( oC_Tcp_Server_t Server );
extern oC_ErrorCode_t           oC_Tcp_Server_SetConnectionConfig   ( oC_Tcp_Server_t Server , const oC_Tcp_Connection_Config_t * Config );
extern oC_ErrorCode_t           oC_Tcp_Server_Run                   ( oC_Tcp_Server_t Server );
extern oC_ErrorCode_t           oC_Tcp_Server_Stop                  ( oC_Tcp_Server_t Server );
extern oC_ErrorCode_t           oC_Tcp_Server_WaitForConnection     ( oC_Tcp_Server_t Server , oC_Tcp_Connection_t * outConnection , oC_Time_t Timeout );
extern bool                     oC_Tcp_Server_ContainsConnection    ( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection );
extern oC_ErrorCode_t           oC_Tcp_Server_AcceptConnection      ( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Server_RejectConnection      ( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Server_AddConnection         ( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Server_RemoveConnection      ( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection );
extern oC_Process_t             oC_Tcp_Server_GetProcess            ( oC_Tcp_Server_t Server );
extern oC_Tcp_Port_t            oC_Tcp_Server_GetPort               ( oC_Tcp_Server_t Server );
extern oC_ErrorCode_t           oC_Tcp_Server_SetConnectionFinished ( oC_Tcp_Server_t Server , oC_Tcp_ConnectionFunction_t Function , void * Parameter );
extern oC_ErrorCode_t           oC_Tcp_Server_AddToKnown            ( oC_Tcp_Server_t Server, oC_Tcp_Connection_t Connection );
extern oC_ErrorCode_t           oC_Tcp_Server_RemoveFromKnown       ( oC_Tcp_Server_t Server, oC_Tcp_Connection_t Connection );
extern bool                     oC_Tcp_Server_IsKnown               ( oC_Tcp_Server_t Server, oC_Tcp_Connection_t Connection );

extern oC_Tcp_Packet_t *        oC_Tcp_Packet_New                   ( const oC_Net_Address_t * Source, const oC_Net_Address_t * Destination, uint16_t HeaderSize , const void * Data , uint16_t Size );
extern bool                     oC_Tcp_Packet_Delete                ( oC_Tcp_Packet_t ** outPacket );
extern oC_ErrorCode_t           oC_Tcp_Packet_SetData               ( oC_Tcp_Packet_t * Packet , const void * Data , uint16_t Size );
extern oC_ErrorCode_t           oC_Tcp_Packet_ReadData              ( oC_Tcp_Packet_t * Packet , void * outData    , oC_MemorySize_t Size );
extern void                     oC_Tcp_Packet_SetSize               ( oC_Tcp_Packet_t * Packet , uint16_t Size );
extern void *                   oC_Tcp_Packet_GetDataReference      ( oC_Tcp_Packet_t * Packet );
extern uint16_t                 oC_Tcp_Packet_GetDataSize           ( oC_Tcp_Packet_t * Packet );
extern bool                     oC_Tcp_Packet_AddOption             ( oC_Tcp_Packet_t * Packet , oC_Tcp_OptionKind_t OptionKind , const void * Data , uint8_t Size );
extern oC_ErrorCode_t           oC_Tcp_Packet_ReadOption            ( oC_Tcp_Packet_t * Packet , oC_Tcp_OptionKind_t OptionKind , void * outData , uint8_t * Size );
extern bool                     oC_Tcp_Packet_ClearOptions          ( oC_Tcp_Packet_t * Packet );
extern uint16_t                 oC_Tcp_Packet_CalculateChecksum     ( oC_Tcp_Packet_t * Packet );
extern oC_Tcp_Header_t *        oC_Tcp_Packet_GetHeaderReference    ( oC_Tcp_Packet_t * Packet );
extern bool                     oC_Tcp_Packet_ConvertFromNetworkEndianess   ( oC_Tcp_Packet_t * Packet );
extern bool                     oC_Tcp_Packet_ConvertToNetworkEndianess     ( oC_Tcp_Packet_t * Packet );

extern oC_Tcp_Connection_t      oC_Tcp_Connection_New               ( const oC_Tcp_Connection_Config_t * Config );
extern bool                     oC_Tcp_Connection_Delete            ( oC_Tcp_Connection_t * Connection , oC_Time_t Timeout );
extern bool                     oC_Tcp_Connection_AreTheSame        ( oC_Tcp_Connection_t ConnectionA,  oC_Tcp_Connection_t ConnectionB );
extern bool                     oC_Tcp_Connection_IsCorrect         ( oC_Tcp_Connection_t Connection );
extern oC_Thread_t              oC_Tcp_Connection_GetThread         ( oC_Tcp_Connection_t Connection );
extern bool                     oC_Tcp_Connection_IsConnected       ( oC_Tcp_Connection_t Connection );
extern bool                     oC_Tcp_Connection_IsDisconnected    ( oC_Tcp_Connection_t Connection );
extern oC_Process_t             oC_Tcp_Connection_GetProcess        ( oC_Tcp_Connection_t Connection );
extern const char *             oC_Tcp_Connection_GetName           ( oC_Tcp_Connection_t Connection );
extern oC_ErrorCode_t           oC_Tcp_Connection_Connect           ( oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Connection_Disconnect        ( oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Connection_WaitForConnection ( oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Connection_Accept            ( oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Connection_Reject            ( oC_Tcp_Connection_t Connection , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Connection_ReadRemote        ( oC_Tcp_Connection_t Connection , oC_Net_Address_t * outAddress );
extern oC_ErrorCode_t           oC_Tcp_Connection_ReadLocal         ( oC_Tcp_Connection_t Connection , oC_Net_Address_t * outAddress );
extern oC_ErrorCode_t           oC_Tcp_Connection_Send              ( oC_Tcp_Connection_t Connection , const void *    Buffer , oC_MemorySize_t Size , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Connection_Receive           ( oC_Tcp_Connection_t Connection ,       void * outBuffer , oC_MemorySize_t* Size , oC_Time_t Timeout );
extern oC_ErrorCode_t           oC_Tcp_Connection_SetCallback       ( oC_Tcp_Connection_t Connection , oC_Tcp_CallbackGroup_t Group, oC_Tcp_EventId_t EventId, const oC_Tcp_Connection_Callback_t* Callback );
extern oC_MemorySize_t          oC_Tcp_Connection_GetRemoteWindowSize   ( oC_Tcp_Connection_t Connection );

/** @} */
#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

#endif /* SYSTEM_CORE_INC_NET_PROTOCOLS_TCP_OC_TCP_H_ */
