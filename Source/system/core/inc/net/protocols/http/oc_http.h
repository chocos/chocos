/** ****************************************************************************************************************************************
 *
 * @brief      File with interface for HTTP
 *
 * @file       oc_http.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Http   Hypertext Transfer Protocol
 * @ingroup CoreNetwork
 * @brief module for handling HTTP packets
 *
 ******************************************************************************************************************************************/
#ifndef SYSTEM_CORE_INC_NET_PROTOCOLS_HTTP_OC_HTTP_H_
#define SYSTEM_CORE_INC_NET_PROTOCOLS_HTTP_OC_HTTP_H_

#include <oc_list.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define __________________________________________DEFINITIONS_SECTION_______________________________________________________________________
/**
 * @addtogroup Http
 * @{
 */

//==========================================================================================================================================
#ifndef oC_HTTP_MAX_HEADERS_COUNT
/**
 * @brief The maximum number of headers in the HTTP packet
 */
#   define oC_HTTP_MAX_HEADERS_COUNT           32
#endif
//==========================================================================================================================================

//==========================================================================================================================================

/** @} */
#undef  __________________________________________DEFINITIONS_SECTION_______________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup Http
 * @{
 */

//==========================================================================================================================================
/**
 * @brief HTTP method
 * 
 * The method is for storing HTTP method.
 */
//==========================================================================================================================================
typedef enum 
{
    oC_Http_Method_Get,         //!< GET method
    oC_Http_Method_Post,        //!< POST method
    oC_Http_Method_Put,         //!< PUT method
    oC_Http_Method_Delete,      //!< DELETE method
    oC_Http_Method_Head,        //!< HEAD method
    oC_Http_Method_Patch,       //!< PATCH method
    oC_Http_Method_Options,     //!< OPTIONS method
    oC_Http_Method_Trace,       //!< TRACE method
    oC_Http_Method_Connect,     //!< CONNECT method

    oC_Http_Method_NumberOfElements, //!< Number of elements in the enumeration
    oC_Http_Method_Unsupported = oC_Http_Method_NumberOfElements //!< Unsupported method
} oC_Http_Method_t;

//==========================================================================================================================================
/**
 * @brief HTTP version
 * 
 * The version is for storing HTTP version.
 */
//==========================================================================================================================================
typedef enum 
{
    oC_Http_Version_1_0,        //!< HTTP/1.0 version
    oC_Http_Version_1_1,        //!< HTTP/1.1 version
    oC_Http_Version_2_0,        //!< HTTP/2.0 version

    oC_Http_Version_NumberOfElements, //!< Number of elements in the enumeration
    oC_Http_Version_Unsupported = oC_Http_Version_NumberOfElements //!< Unsupported version
} oC_Http_Version_t;

//==========================================================================================================================================
/**
 * @brief HTTP content type
 * 
 * The content type is for storing HTTP content type.
 */
//==========================================================================================================================================
typedef enum 
{
    oC_Http_ContentType_TextPlain,        //!< text/plain
    oC_Http_ContentType_TextHtml,         //!< text/html
    oC_Http_ContentType_TextCss,          //!< text/css
    oC_Http_ContentType_TextJavascript,   //!< text/javascript
    oC_Http_ContentType_ApplicationJson,  //!< application/json
    oC_Http_ContentType_ApplicationXml,   //!< application/xml
    oC_Http_ContentType_ApplicationFormUrlEncoded, //!< application/x-www-form-urlencoded
    oC_Http_ContentType_MultipartFormData, //!< multipart/form-data
    oC_Http_ContentType_ImagePng,         //!< image/png
    oC_Http_ContentType_ImageJpeg,        //!< image/jpeg
    oC_Http_ContentType_ImageGif,         //!< image/gif
    oC_Http_ContentType_ImageSvgXml,      //!< image/svg+xml

    oC_Http_ContentType_NumberOfElements, //!< Number of elements in the enumeration
    oC_Http_ContentType_Unsupported = oC_Http_ContentType_NumberOfElements //!< Unsupported content type
} oC_Http_ContentType_t;

//==========================================================================================================================================
/**
 * @brief HTTP header ID
 * 
 * The header is for storing HTTP header ID.
 */
//==========================================================================================================================================
typedef enum 
{
    oC_Http_HeaderId_None,          //!< None
    oC_Http_HeaderId_Host,          //!< Host header
    oC_Http_HeaderId_Accept,        //!< Accept header
    oC_Http_HeaderId_AcceptLanguage,//!< Accept-Language header
    oC_Http_HeaderId_AcceptEncoding,//!< Accept-Encoding header
    oC_Http_HeaderId_AcceptCharset, //!< Accept-Charset header
    oC_Http_HeaderId_ContentType,   //!< Content-Type header
    oC_Http_HeaderId_ContentLength, //!< Content-Length header
    oC_Http_HeaderId_UserAgent,     //!< User-Agent header
    oC_Http_HeaderId_Referer,       //!< Referer header
    oC_Http_HeaderId_Connection,    //!< Connection header
    oC_Http_HeaderId_CacheControl,  //!< Cache-Control header
    oC_Http_HeaderId_Upgrade,       //!< Upgrade header
    oC_Http_HeaderId_Origin,        //!< Origin header
    oC_Http_HeaderId_Authorization, //!< Authorization header
    oC_Http_HeaderId_IfModifiedSince,//!< If-Modified-Since header
    oC_Http_HeaderId_IfNoneMatch,   //!< If-None-Match header
    oC_Http_HeaderId_AccessControlRequestMethod, //!< Access-Control-Request-Method header
    oC_Http_HeaderId_AccessControlRequestHeaders,//!< Access-Control-Request-Headers header
    oC_Http_HeaderId_TransferEncoding,//!< Transfer-Encoding header

    oC_Http_HeaderId_NumberOfElements, //!< Number of elements in the enumeration
    oC_Http_HeaderId_Unsupported = oC_Http_HeaderId_NumberOfElements //!< Unsupported header
} oC_Http_HeaderId_t;

//==========================================================================================================================================
/**
 * @brief HTTP connection
 * 
 * The connection is for storing HTTP connection.
 */
//==========================================================================================================================================
typedef enum 
{
    oC_Http_Connection_KeepAlive,   //!< Keep-Alive connection
    oC_Http_Connection_Close,       //!< Close connection
    oC_Http_Connection_Upgrade,     //!< Upgrade connection
    oC_Http_Connection_Unknown,     //!< Unknown connection

    oC_Http_Connection_NumberOfElements, //!< Number of elements in the enumeration
    oC_Http_Connection_Unsupported = oC_Http_Connection_NumberOfElements //!< Unsupported connection
} oC_Http_Connection_t;

//==========================================================================================================================================
/**
 * @brief HTTP status code
 * 
 * The status code is for storing HTTP status code.
 */
//==========================================================================================================================================
typedef enum 
{
    oC_Http_StatusCode_Continue                      = 100,  //!< Continue
    oC_Http_StatusCode_SwitchingProtocols            = 101,  //!< Switching Protocols
    oC_Http_StatusCode_Processing                    = 102,  //!< Processing
    oC_Http_StatusCode_EarlyHints                    = 103,  //!< Early Hints
    oC_Http_StatusCode_OK                            = 200,  //!< OK
    oC_Http_StatusCode_Created                       = 201,  //!< Created
    oC_Http_StatusCode_Accepted                      = 202,  //!< Accepted
    oC_Http_StatusCode_NonAuthoritativeInformation   = 203,  //!< Non-Authoritative Information
    oC_Http_StatusCode_NoContent                     = 204,  //!< No Content
    oC_Http_StatusCode_ResetContent                  = 205,  //!< Reset Content
    oC_Http_StatusCode_PartialContent                = 206,  //!< Partial Content
    oC_Http_StatusCode_MultiStatus                   = 207,  //!< Multi-Status
    oC_Http_StatusCode_AlreadyReported               = 208,  //!< Already Reported
    oC_Http_StatusCode_IMUsed                        = 226,  //!< IM Used
    oC_Http_StatusCode_MultipleChoices               = 300,  //!< Multiple Choices
    oC_Http_StatusCode_MovedPermanently              = 301,  //!< Moved Permanently
    oC_Http_StatusCode_Found                         = 302,  //!< Found
    oC_Http_StatusCode_SeeOther                      = 303,  //!< See Other
    oC_Http_StatusCode_NotModified                   = 304,  //!< Not Modified
    oC_Http_StatusCode_UseProxy                      = 305,  //!< Use Proxy
    oC_Http_StatusCode_TemporaryRedirect             = 307,  //!< Temporary Redirect
    oC_Http_StatusCode_PermanentRedirect             = 308,  //!< Permanent Redirect
    oC_Http_StatusCode_BadRequest                    = 400,  //!< Bad Request
    oC_Http_StatusCode_Unauthorized                  = 401,  //!< Unauthorized
    oC_Http_StatusCode_PaymentRequired               = 402,  //!< Payment Required
    oC_Http_StatusCode_Forbidden                     = 403,  //!< Forbidden
    oC_Http_StatusCode_NotFound                      = 404,  //!< Not Found
    oC_Http_StatusCode_MethodNotAllowed               = 405,  //!< Method Not Allowed
    oC_Http_StatusCode_NotAcceptable                 = 406,  //!< Not Acceptable
    oC_Http_StatusCode_ProxyAuthenticationRequired   = 407,  //!< Proxy Authentication Required
    oC_Http_StatusCode_RequestTimeout                = 408,  //!< Request Timeout
    oC_Http_StatusCode_Conflict                      = 409,  //!< Conflict
    oC_Http_StatusCode_Gone                          = 410,  //!< Gone
    oC_Http_StatusCode_LengthRequired                = 411,  //!< Length Required
    oC_Http_StatusCode_PreconditionFailed            = 412,  //!< Precondition Failed
    oC_Http_StatusCode_PayloadTooLarge               = 413,  //!< Payload Too Large
    oC_Http_StatusCode_URI_TooLong                   = 414,  //!< URI Too Long
    oC_Http_StatusCode_UnsupportedMediaType          = 415,  //!< Unsupported Media Type
    oC_Http_StatusCode_RangeNotSatisfiable           = 416,  //!< Range Not Satisfiable
    oC_Http_StatusCode_ExpectationFailed             = 417,  //!< Expectation Failed
    oC_Http_StatusCode_ImATeapot                     = 418,  //!< I'm a teapot
    oC_Http_StatusCode_MisdirectedRequest            = 421,  //!< Misdirected Request
    oC_Http_StatusCode_UnprocessableEntity           = 422,  //!< Unprocessable Entity
    oC_Http_StatusCode_Locked                        = 423,  //!< Locked
    oC_Http_StatusCode_FailedDependency              = 424,  //!< Failed Dependency
    oC_Http_StatusCode_TooEarly                      = 425,  //!< Too Early
    oC_Http_StatusCode_UpgradeRequired               = 426,  //!< Upgrade Required
    oC_Http_StatusCode_PreconditionRequired          = 428,  //!< Precondition Required
    oC_Http_StatusCode_TooManyRequests               = 429,  //!< Too Many Requests
    oC_Http_StatusCode_RequestHeaderFieldsTooLarge   = 431,  //!< Request Header Fields Too Large
    oC_Http_StatusCode_UnavailableForLegalReasons    = 451,  //!< Unavailable For Legal Reasons
    oC_Http_StatusCode_InternalServerError           = 500,  //!< Internal Server Error
    oC_Http_StatusCode_NotImplemented                = 501,  //!< Not Implemented
    oC_Http_StatusCode_BadGateway                    = 502,  //!< Bad Gateway
    oC_Http_StatusCode_ServiceUnavailable            = 503,  //!< Service Unavailable
    oC_Http_StatusCode_GatewayTimeout                = 504,  //!< Gateway Timeout
    oC_Http_StatusCode_HTTPVersionNotSupported       = 505,  //!< HTTP Version Not Supported
    oC_Http_StatusCode_VariantAlsoNegotiates         = 506,  //!< Variant Also Negotiates
    oC_Http_StatusCode_InsufficientStorage           = 507,  //!< Insufficient Storage
    oC_Http_StatusCode_LoopDetected                  = 508,  //!< Loop Detected
    oC_Http_StatusCode_NotExtended                   = 510,  //!< Not Extended
    oC_Http_StatusCode_NetworkAuthenticationRequired = 511   //!< Network Authentication Required
} oC_Http_StatusCode_t;

//==========================================================================================================================================
/**
 * @brief HTTP header
 * 
 * The header is for storing HTTP header.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Http_HeaderId_t  Id;         //!< ID of the header (can be #oC_Http_HeaderId_Unsupported if not recognized)
    char*               Name;       //!< Name of the header
    char*               Value;      //!< Value of the header
} oC_Http_Header_t;

//==========================================================================================================================================
/**
 * @brief HTTP packet
 * 
 * The packet is for storing HTTP packet.
 */
//==========================================================================================================================================
typedef struct 
{
    oC_Http_Header_t    Headers[oC_HTTP_MAX_HEADERS_COUNT];   //!< Headers of the HTTP packet
    char*               Body;                                 //!< Body of the HTTP packet
} oC_Http_Packet_t;

//==========================================================================================================================================
/**
 * @brief HTTP request
 * 
 * The request is for storing HTTP request.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Http_Method_t        Method;         //!< Method of the HTTP packet
    const char*             Path;           //!< Path of the HTTP packet
    oC_Http_Version_t       Version;        //!< Version of the HTTP packet
    oC_Http_Packet_t        Packet;         //!< Packet of the HTTP packet
    oC_MemorySize_t         ContentLength;  //!< Content length of the HTTP packet
    oC_Http_ContentType_t   ContentType;    //!< Content type of the HTTP packet
    oC_Http_Connection_t    Connection;     //!< Connection of the HTTP packet
} oC_Http_Request_t;

//==========================================================================================================================================
/**
 * @brief HTTP response
 * 
 * The response is for storing HTTP response.
 */
//==========================================================================================================================================
typedef struct 
{
    oC_Http_StatusCode_t    StatusCode;     //!< Status code of the HTTP packet
    oC_Http_Version_t       Version;        //!< Version of the HTTP packet
    oC_MemorySize_t         ContentLength;  //!< Content length of the HTTP packet
    oC_Http_ContentType_t   ContentType;    //!< Content type of the HTTP packet
    oC_Http_Packet_t        Packet;         //!< Packet of the HTTP packet
} oC_Http_Response_t;

//==========================================================================================================================================
/**
 * @brief HTTP context
 * 
 * The context is for storing HTTP data.
 */
//==========================================================================================================================================
typedef struct Context_t* oC_Http_Context_t;

/** @} */
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________
/**
 * @addtogroup Http
 * @{
 */

extern oC_ErrorCode_t      oC_Http_ParseRequest                ( oC_Http_Request_t* outRequest , char* Data, oC_MemorySize_t DataSize );
extern oC_ErrorCode_t      oC_Http_PrepareStatusLine           ( const oC_Http_Response_t* Response, char** outData, oC_MemorySize_t * DataSize );
extern oC_ErrorCode_t      oC_Http_PrepareHeader               ( const oC_Http_Header_t* Header, char** outData, oC_MemorySize_t * DataSize );
extern oC_ErrorCode_t      oC_Http_PrepareContentLength        ( oC_MemorySize_t ContentLength, char** outData, oC_MemorySize_t * DataSize );
extern oC_ErrorCode_t      oC_Http_PrepareContentType          ( oC_Http_ContentType_t ContentType, char** outData, oC_MemorySize_t * DataSize );
extern oC_ErrorCode_t      oC_Http_PrepareHeaders              ( const oC_Http_Header_t* Headers, uint16_t NumberOfHeaders, char** outData, oC_MemorySize_t * DataSize );
extern oC_ErrorCode_t      oC_Http_PrepareResponse             ( const oC_Http_Response_t* Response, char** outData, oC_MemorySize_t * DataSize );

/** @} */
#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_PROTOCOLS_HTTP_OC_HTTP_H_ */
