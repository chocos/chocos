/** ****************************************************************************************************************************************
 *
 * @brief      Stores UDP interface
 *
 * @file       oc_udp.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Udp Udp - User Datagram Protocol
 * @ingroup CoreNetwork
 * @brief Module for handling UDP protocol datagrams
 *
 * The module is for handling UDP protocol datagrams. It provides the interface for sending and receiving UDP datagrams. The module is
 * designed to be used with the network interface object.
 * 
 * If you want to use the module, reserve the UDP port by using the function #oC_Udp_ReservePort, create a packet by using the function
 * #oC_Udp_Packet_New, send the packet by using the function #oC_Udp_Send, and receive the packet by using the function #oC_Udp_Receive.
 * 
 * <h1>Example:</h1>
 * @code{.c}
   
//=============================================================================
//                   == UDP SERVER ==
//
//          This function shows how to create simple UDP server
//
//=============================================================================
static int ExampleUdpServer(int Argc, char ** Argv)
{
    //
    // Prepare source address
    //
    oC_Net_Address_t sourceAddress = {
                     .Type = oC_Net_AddressType_IPv4,
                     .IPv4 = IP(255,255,255,255),
                     .Protocol = oC_Net_Protocol_UDP
    };
    oC_Udp_Port_t udpPort = 3000;

    //
    //  Get network interface for this address
    //
    oC_Netif_t netif = oC_NetifMan_GetNetifForAddress(&sourceAddress);
    if(netif == NULL)
    {
        oC_PrintErrorMessage("Network interface not found: ", oC_ErrorCode_NoSuchFile);
        return -1;
    }

    //
    //  Reserve UDP port
    //
    oC_ErrorCode_t errorCode = oC_Udp_ReservePort(&udpPort, s(10));
    if(errorCode != oC_ErrorCode_None)
    {
        oC_PrintErrorMessage("Cannot reserve UDP port: ", errorCode);
        return -1;
    }

    //
    //  Allocates memory for UDP packet
    //
    oC_MemorySize_t bufferSize = 100;
    oC_Udp_Packet_t* udpPacket = oC_Udp_Packet_New(getcurallocator(), oC_Net_PacketType_IPv4, NULL, bufferSize);
    if(udpPacket == NULL)
    {
        oC_PrintErrorMessage("Cannot allocate memory for a packet: ", oC_ErrorCode_AllocationError);
        return -1;
    }

    //
    //  Waits for client data
    //
    printf("Listening on port %d\n", udpPort);
    while(ErrorCode( oC_Udp_Receive(netif, udpPort, udpPacket, day(365)) ))
    {
        //
        //  Read source IP
        //
        oC_Net_Address_t sourceAddress;
        memset(&sourceAddress, 0, sizeof(sourceAddress));
        errorCode = oC_Net_Packet_ReadSourceAddress(&udpPacket->Packet, &sourceAddress);

        //
        //  Convert source IP to string
        //
        char ipString[sizeof("255.255.255.255")];
        memset(ipString, 0, sizeof(ipString));
        oC_Net_Ipv4AddressToString(sourceAddress.IPv4, ipString, sizeof(ipString));

        //
        // Read source port
        //
        oC_Udp_Port_t sourcePort = udpPacket->IPv4.UdpDatagram.Header.SourcePort;
        sourceAddress.Port = (oC_Net_Port_t)sourcePort;

        //
        //  Read received data
        //
        char* dataRef = oC_Udp_Packet_GetDataReference(udpPacket);
        printf("Received data: %s from %s:%d\n", dataRef, ipString, sourcePort);

        //
        //  Prepare response
        //
        char buffer[100];
        memset(buffer, 0, sizeof(buffer));
        sprintf_s(buffer, sizeof(buffer), "Hello from ChocoOS. Current time: %.03g s. Your data: %s\n", gettimestamp(), dataRef);
        printf("Sending %s\n", buffer);

        //
        //  Restore original packet's size (the one from oC_Udp_Packet_New)
        //
        if(!oC_Udp_Packet_SetDataSize(udpPacket, bufferSize))
        {
            oC_PrintErrorMessage("Cannot change packet's size: ", oC_ErrorCode_SizeNotCorrect);
        }

        //
        //  Set our data to the packet
        //
        if(!oC_Udp_Packet_SetData(udpPacket, buffer, sizeof(buffer)))
        {
            oC_PrintErrorMessage("Cannot set packet's data: ", oC_ErrorCode_EndOfFile);
        }

        //
        // Prepare data to respond
        //
        errorCode = oC_Udp_Send(netif, udpPort, &sourceAddress, udpPacket, s(10));
        if(errorCode != oC_ErrorCode_None)
        {
            oC_PrintErrorMessage("Cannot send UDP packet: ", errorCode);
        }
    }

    return 0;
}

   @endcode
 * 
 * 
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_PROTOCOLS_UDP_OC_UDP_H_
#define SYSTEM_CORE_INC_NET_PROTOCOLS_UDP_OC_UDP_H_

#include <oc_net.h>
#include <oc_netif.h>
#include <limits.h>
#include <oc_process.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________
/**
 * @addtogroup Udp
 * @{
 */

/** @} */
#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION_____________________________________________________________________________
/**
 * @addtogroup Udp
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores UDP port number
 *
 * The type is for storing UDP port
 *
 * @see oC_Udp_Port_MAX
 */
//==========================================================================================================================================
typedef enum
{
    oC_Udp_Port_Empty                   = 0 ,                       //!< Special value for marks port as not filled
    oC_Udp_Port_MAX                     = UINT16_MAX ,              //!< maximum number of ports

    // ==================== WELL KNOWN PORTS ==================== //
    oC_Udp_Port_TCPMUX                          = 1  , //!< have been assigned to TCPMUX by IANA, but by design only TCP is specified.
    oC_Udp_Port_RemoteJobEntry                  = 5  , //!< Remote job entry
    oC_Udp_Port_Echo                            = 7  , //!< Echo Protocol
    oC_Udp_Port_Daytime                         = 13 , //!< Daytime Protocol
    oC_Udp_Port_QuoteOfTheDay                   = 17 , //!< Quote of the Day (QOTD)
    oC_Udp_Port_MessageSend                     = 18 , //!< Message Send Protocol
    oC_Udp_Port_CHARGEN                         = 19 , //!< Character Generator Protocol (CHARGEN)
    oC_Udp_Port_FTP                             = 20 , //!< File Transfer Protocol (FTP) data transfer
    oC_Udp_Port_Telnet                          = 23 , //!< Telnet protocol—unencrypted text communications
    oC_Udp_Port_SMTP                            = 25 , //!< Simple Mail Transfer Protocol (SMTP), used for e-mail routing between mail servers
    oC_Udp_Port_Time                            = 37 , //!< Time Protocol
    oC_Udp_Port_RAP                             = 38 , //!< Route Access Protocol (RAP)
    oC_Udp_Port_RLP                             = 39 , //!< Resource Location Protocol (RLP)[importance?]—used for determining the location of higher level services from hosts on a network
    oC_Udp_Port_HostNameServer                  = 42 , //!< Host Name Server Protocol
    oC_Udp_Port_WHOIS                           = 43 , //!< WHOIS protocol
    oC_Udp_Port_TACACS                          = 49 , //!< TACACS+ Login Host protocol
    oC_Udp_Port_RemoteMailChecking              = 50 , //!< Remote Mail Checking Protocol
    oC_Udp_Port_XNSTime                         = 52 , //!< Xerox Network Systems (XNS) Time Protocol
    oC_Udp_Port_DNS                             = 53 , //!< Domain Name System (DNS)
    oC_Udp_Port_XNSClearingHouse                = 54 , //!< Xerox Network Systems (XNS) clearinghouse
    oC_Udp_Port_XNSAuthentication               = 56 , //!< Xerox Network Systems (XNS) authentication
    oC_Udp_Port_AnyPrivateTerminalAccess        = 57 , //!< Any private terminal access
    oC_Udp_Port_XNSMail                         = 58 , //!< Xerox Network Systems (XNS) Mail
    oC_Udp_Port_BOOTPServer                     = 67 , //!< Bootstrap Protocol (BOOTP) server; also used by Dynamic Host Configuration Protocol (DHCP)
    oC_Udp_Port_BOOTPClient                     = 68 , //!< Bootstrap Protocol (BOOTP) client; also used by Dynamic Host Configuration Protocol (DHCP)
    oC_Udp_Port_TFTP                            = 69 , //!< Trivial File Transfer Protocol (TFTP)
    oC_Udp_Port_Gopher                          = 70 , //!< Gopher protocol
    oC_Udp_Port_NETRJS                          = 74 , //!< NETRJS protocol
    oC_Udp_Port_AnyPrivateDialOutService        = 75 , //!< Any private dial out service
    oC_Udp_Port_AnyPrivateRemoteJobEntry        = 77 , //!< Any private Remote job entry
    oC_Udp_Port_Finger                          = 79 , //!< Finger protocol
    oC_Udp_Port_KerberosAuthenticationSystem    = 88 , //!< Kerberos authentication system
    oC_Udp_Port_DNSIX                           = 90 , //!< dnsix (DoD Network Security for Information Exchange) Securit  Attribute Token Map
    oC_Udp_Port_NICHostName                     = 101 , //!< NIC host name
    oC_Udp_Port_TSAP                            = 102 , //!< ISO Transport Service Access Point (TSAP) Class 0 protocol;
    oC_Udp_Port_DICOM                           = 104 , //!< Digital Imaging and Communications in Medicine (DICOM; also port 11112)
    oC_Udp_Port_CCSONameserver                  = 105 , //!< CCSO Nameserver
    oC_Udp_Port_RTelnet                         = 107 , //!< Remote User Telnet Service (RTelnet)
    oC_Udp_Port_SNA                             = 108 , //!< IBM Systems Network Architecture (SNA) gateway access server
    oC_Udp_Port_POP2                            = 109 , //!< Post Office Protocol, version 2 (POP2)
    oC_Udp_Port_POP3                            = 110 , //!< Post Office Protocol, version 3 (POP3)
    oC_Udp_Port_OncRpc                          = 111 , //!< Open Network Computing Remote Procedure Call (ONC RPC, sometimes referred to as Sun RPC)
    oC_Udp_Port_Auth                            = 113 , //!< Authentication Service (auth), the precedessor to identification protocol. Used to determine an user's identity of a particular TCP connection.
    oC_Udp_Port_SFTP                            = 115 , //!< Simple File Transfer Protocol
    oC_Udp_Port_UUCPMappingProject              = 117 , //!< UUCP Mapping Project (path service)
    oC_Udp_Port_SQLServices                     = 118 , //!< Structured Query Language (SQL) Services
    oC_Udp_Port_NNTP                            = 119 , //!< Network News Transfer Protocol (NNTP), retrieval of newsgroup messages
    oC_Udp_Port_NTP                             = 123 , //!< Network Time Protocol (NTP), used for time synchronization
    oC_Udp_Port_FormerlyUnisysUnitaryLogin      = 126 , //!< Formerly Unisys Unitary Login, renamed by Unisys to NXEdit. Used by Unisys Programmer's Workbench for Clearpath MCP, an IDE for Unisys MCP software development
    oC_Udp_Port_DCEEndpointResolution           = 135 , //!< DCE endpoint resolution
    oC_Udp_Port_MicrosoftEPMAP                  = 135 , //!< Microsoft EPMAP (End Point Mapper), also known as DCE/RPC Locator service, used to remotely manage services including DHCP server, DNS server and WINS. Also used by DCOM
    oC_Udp_Port_NetBIOS                         = 137 , //!< NetBIOS Name Service, used for name registration and resolution
    oC_Udp_Port_NetBIOSDatagram                 = 138 , //!< NetBIOS Datagram Service
    oC_Udp_Port_NetBIOSSession                  = 139 , //!< NetBIOS Session Service
    oC_Udp_Port_IMAP                            = 143 , //!< Internet Message Access Protocol (IMAP), management of electronic mail messages on a server
    oC_Udp_Port_BFTP                            = 152 , //!< Background File Transfer Program (BFTP)
    oC_Udp_Port_SGMP                            = 153 , //!< Simple Gateway Monitoring Protocol (SGMP), a protocol for remote inspection and alteration of gateway management information
    oC_Udp_Port_SQL                             = 156 , //!< Structured Query Language (SQL) Service
    oC_Udp_Port_DMSP                            = 158 , //!< Distributed Mail System Protocol (DMSP, sometimes referred to as Pcmail)
    oC_Udp_Port_SNMP                            = 161 , //!< Simple Network Management Protocol (SNMP)
    oC_Udp_Port_SNMPTRAP                        = 162 , //!< Simple Network Management Protocol Trap (SNMPTRAP)
    oC_Udp_Port_PrintServer                     = 170 , //!< Print server
    oC_Udp_Port_XDMCP                           = 177 , //!< X Display Manager Control Protocol (XDMCP), used for remote logins to an X Display Manager server
    oC_Udp_Port_IRC                             = 194 , //!< Internet Relay Chat (IRC)
    oC_Udp_Port_SMUX                            = 199 , //!< SNMP multiplexing protocol (SMUX)
    oC_Udp_Port_AppleTalkRoutingMaintenance     = 201 , //!< AppleTalk Routing Maintenance
    oC_Udp_Port_QuickMailTransferProtocol       = 209 , //!< Quick Mail Transfer Protocol
    oC_Udp_Port_ANSIZ39_50                      = 210 , //!< ANSI Z39.50
    oC_Udp_Port_IPX                             = 213 , //!< Internetwork Packet Exchange (IPX)
    oC_Udp_Port_MPP                             = 218 , //!< Message posting protocol (MPP)
    oC_Udp_Port_IMAPVer3                        = 220 , //!< Internet Message Access Protocol (IMAP), version 3
    oC_Udp_Port_ESRO                            = 259 , //!< Efficient Short Remote Operations (ESRO)
    oC_Udp_Port_Arcisdms                        = 262 , //!< Arcisdms
    oC_Udp_Port_BGMP                            = 264 , //!< Border Gateway Multicast Protocol (BGMP)
    oC_Udp_Port_http_mgmt                       = 280 , //!< http-mgmt
    oC_Udp_Port_TSP                             = 318 , //!< PKIX Time Stamp Protocol (TSP)
    oC_Udp_Port_PTPEvent                        = 319 , //!< Precision Time Protocol (PTP) event messages
    oC_Udp_Port_PTPGeneral                      = 320 , //!< Precision Time Protocol (PTP) general messages
    oC_Udp_Port_MATIP                           = 350 , //!< Mapping of Airline Traffic over Internet Protocol (MATIP) type A
    oC_Udp_Port_MATIPTypeB                      = 351 , //!< MATIP type B
    oC_Udp_Port_Cloanto_net_1                   = 356 , //!< cloanto-net-1 (used by Cloanto Amiga Explorer and VMs)
    oC_Udp_Port_ODMR                            = 366 , //!< On-Demand Mail Relay (ODMR)
    oC_Udp_Port_Rpc2portmap                     = 369 , //!< Rpc2portmap
    oC_Udp_Port_CodaAuth2                       = 370 , //!< codaauth2, Coda authentication server
    oC_Udp_Port_SecureCast1                     = 370 , //!< securecast1, outgoing packets to NAI's SecureCast serversAs of 2000
    oC_Udp_Port_ClearCaseAlbd                   = 371 , //!< ClearCase albd
    oC_Udp_Port_HPDataAlarmManager              = 383 , //!< HP data alarm manager
    oC_Udp_Port_ARemoteNetworkServerSystem      = 384 , //!< A Remote Network Server System
    oC_Udp_Port_AURP                            = 387 , //!< AURP (AppleTalk Update-based Routing Protocol)
    oC_Udp_Port_LDAP                            = 389 , //!< Lightweight Directory Access Protocol (LDAP)
    oC_Udp_Port_DigitalEquipmentCorporationDEC  = 399 , //!< Digital Equipment Corporation DECnet (Phase V+) over TCP/IP
    oC_Udp_Port_UPS                             = 401 , //!< Uninterruptible power supply (UPS)
    oC_Udp_Port_SLP                             = 427 , //!< Service Location Protocol (SLP)
    oC_Udp_Port_NNSP                            = 433 , //!< NNSP, part of Network News Transfer Protocol
    oC_Udp_Port_MobileIpAgent                   = 434 , //!< Mobile IP Agent (RFC 5944)
    oC_Udp_Port_SNPP                            = 444 , //!< Simple Network Paging Protocol (SNPP), RFC 1568
    oC_Udp_Port_KerberosChangeSetPassword       = 464 , //!< Kerberos Change/Set password
    oC_Udp_Port_TcpNetHaspsrv                   = 475 , //!< tcpnethaspsrv, Aladdin Knowledge Systems Hasp services
    oC_Udp_Port_ISAKMP                          = 500 , //!< Internet Security Association and Key Management Protocol (ISAKMP) / Internet Key Exchange (IKE)
    oC_Udp_Port_Modbus                          = 502 , //!< Modbus Protocol
    oC_Udp_Port_Citadel                         = 504 , //!< Citadel, multiservice protocol for dedicated clients for the Citadel groupware system
    oC_Udp_Port_FCP                             = 510 , //!< FirstClass Protocol (FCP), used by FirstClass client/server groupware system
    oC_Udp_Port_ComSat                          = 512 , //!< comsat, together with biff
    oC_Udp_Port_Who                             = 513 , //!< Who
    oC_Udp_Port_Syslog                          = 514 , //!< Syslog, used for system logging
    oC_Udp_Port_Talk                            = 517 , //!< Talk
    oC_Udp_Port_NTalk                           = 518 , //!< NTalk
    oC_Udp_Port_RIP                             = 520 , //!< Routing Information Protocol (RIP)
    oC_Udp_Port_RIPng                           = 521 , //!< Routing Information Protocol Next Generation (RIPng)
    oC_Udp_Port_NCP                             = 524 , //!< NetWare Core Protocol (NCP) is used for a variety things such as access to primary NetWare server resources, Time Synchronization, etc.
    oC_Udp_Port_TimedTimeserver                 = 525 , //!< Timed, Timeserver
    oC_Udp_Port_RPC                             = 530 , //!< Remote procedure call (RPC)
    oC_Udp_Port_NetwallForEmergencyBroadcasts   = 533 , //!< netwall, For Emergency Broadcasts
    oC_Udp_Port_Commerce                        = 542 , //!< commerce (Commerce Applications)
    oC_Udp_Port_DHCPv6Client                    = 546 , //!< DHCPv6 client
    oC_Udp_Port_DHCPv6Server                    = 547 , //!< DHCPv6 server
    oC_Udp_Port_NewRwhoNewWho                   = 550 , //!< new-rwho, new-who
    oC_Udp_Port_RTSP                            = 554 , //!< Real Time Streaming Protocol (RTSP)
    oC_Udp_Port_RMonitorRemoteMonitor           = 560 , //!< rmonitor, Remote Monitor
    oC_Udp_Port_Monitor                         = 561 , //!< monitor
    oC_Udp_Port_NNTPS                           = 563 , //!< NNTP over TLS/SSL (NNTPS)
    oC_Udp_Port_HTTP                            = 593 , //!< HTTP RPC Ep Map, Remote procedure call over Hypertext Transfer Protocol, often used by Distributed Component Object Model services and Microsoft Exchange Server
    oC_Udp_Port_ASFRemoteManagementAndControl   = 623 , //!< ASF Remote Management and Control Protocol (ASF-RMCP) & IPMI Remote Management Protocol
    oC_Udp_Port_IPP                             = 631 , //!< Internet Printing Protocol (IPP)
    oC_Udp_Port_RLZDBase                        = 635 , //!< RLZ DBase
    oC_Udp_Port_LDAPS                           = 636 , //!< Lightweight Directory Access Protocol over TLS/SSL (LDAPS)
    oC_Udp_Port_MSDPMulticastSourceDiscovery    = 639 , //!< MSDP, Multicast Source Discovery Protocol
    oC_Udp_Port_SupportSoftNexusRemoteControl   = 641 , //!< SupportSoft Nexus Remote Command (control/listening), a proxy gateway connecting remote control traffic
    oC_Udp_Port_SANity                          = 643 , //!< SANity
    oC_Udp_Port_LDP                             = 646 , //!< Label Distribution Protocol (LDP), a routing protocol used in MPLS networks
    oC_Udp_Port_IEEE_MMS                        = 651 , //!< IEEE-MMS
    oC_Udp_Port_SupportSoftNexusRemoteData      = 653 , //!< SupportSoft Nexus Remote Command (data), a proxy gateway connecting remote control traffic
    oC_Udp_Port_TincVPNDaemon                   = 655 , //!< Tinc VPN daemon
    oC_Udp_Port_IBMRMC                          = 657 , //!< IBM RMC (Remote monitoring and Control) protocol, used by System p5 AIX Integrated Virtualization Manager (IVM) and Hardware Management Console to connect managed logical partitions (LPAR) to enable dynamic partition reconfiguration
    oC_Udp_Port_Doom                            = 666 , //!< Doom, first online first-person shooter
    oC_Udp_Port_REALM_RUSD                      = 688 , //!< REALM-RUSD (ApplianceWare Server Appliance Management Protocol)
    oC_Udp_Port_VATP                            = 690 , //!< Velneo Application Transfer Protocol (VATP)
    oC_Udp_Port_Linux                           = 694 , //!< Linux-HA high-availability heartbeat
    oC_Udp_Port_OLSR                            = 698 , //!< Optimized Link State Routing (OLSR)
    oC_Udp_Port_Kerberos                        = 749 , //!< Kerberos (protocol) administration
    oC_Udp_Port_kerberos_IvKerberosVersionIV    = 750 , //!< kerberos-iv, Kerberos version IV
    oC_Udp_Port_RRH                             = 753 , //!< Reverse Routing Header (RRH)
    oC_Udp_Port_TellSend                        = 754 , //!< tell send
    oC_Udp_Port_mdbs_daemon                     = 800 , //!< mdbs-daemon
    oC_Udp_Port_NETCONFOverSSH                  = 830 , //!< NETCONF over SSH
    oC_Udp_Port_NETCONFOverBEEP                 = 831 , //!< NETCONF over BEEP
    oC_Udp_Port_NETCONFHTTPS                    = 832 , //!< NETCONF for SOAP over HTTPS
    oC_Udp_Port_NETCONFBEEP                     = 833 , //!< NETCONF for SOAP over BEEP
    oC_Udp_Port_GDOI                            = 848 , //!< Group Domain Of Interpretation (GDOI) protocol
    oC_Udp_Port_DNSOverTLS                      = 853 , //!< DNS over TLS (RFC 7858)
    oC_Udp_Port_OWAMP                           = 861 , //!< OWAMP control (RFC 4656)
    oC_Udp_Port_TWAMP                           = 862 , //!< TWAMP control (RFC 5357)
    oC_Udp_Port_FTPSData                        = 989 , //!< FTPS Protocol (data), FTP over TLS/SSL
    oC_Udp_Port_FTPSControl                     = 990 , //!< FTPS Protocol (control), FTP over TLS/SSL
    oC_Udp_Port_NAS                             = 991 , //!< Netnews Administration System (NAS)
    oC_Udp_Port_TelnetOverTLS_SSL               = 992 , //!< Telnet protocol over TLS/SSL

    oC_Udp_Port_NumberOfSpecialPorts            = 1024 ,//!< number of special ports, that are reserved for special service

} oC_Udp_Port_t;

//==========================================================================================================================================
/**
 * @brief stores UDP header
 */
//==========================================================================================================================================
typedef struct PACKED
{
#if defined(LITTLE_ENDIAN)
    uint16_t            SourcePort;         //!< This field identifies the sender's port when meaningful and should be assumed to be the port to reply to if needed. If not used, then it should be zero. If the source host is the client, the port number is likely to be an ephemeral port number. If the source host is the server, the port number is likely to be a well-known port number
    uint16_t            DestinationPort;    //!< This field identifies the receiver's port and is required. Similar to source port number, if the client is the destination host then the port number will likely be an ephemeral port number and if the destination host is the server then the port number will likely be a well-known port number
    uint16_t            Length;             //!< A field that specifies the length in bytes of the UDP header and UDP data. The minimum length is 8 bytes because that is the length of the header. The field size sets a theoretical limit of 65,535 bytes (8 byte header + 65,527 bytes of data) for a UDP datagram. The practical limit for the data length which is imposed by the underlying IPv4 protocol is 65,507 bytes
    uint16_t            Checksum;           //!< The checksum field may be used for error-checking of the header and data. This field is optional in IPv4, and mandatory in IPv6.[6] The field carries all-zeros if unused
#elif defined(BIG_ENDIAN)
#   error oC_Udp_Header_t structure is not defined for BIG ENDIAN!
#else
#   error Unknown endianess
#endif
} oC_Udp_Header_t;

//==========================================================================================================================================
/**
 * @brief pseudo UDP header required only for checksum calculation
 */
//==========================================================================================================================================
typedef union PACKED
{
    struct
    {
        oC_Net_Ipv4_t       SourceIp;               //!< Source IP address
        oC_Net_Ipv4_t       DestinationIp;          //!< Destination IP address
        uint8_t             Zeroes;                 //!< Zeroes
        uint8_t             Protocol;               //!< Protocol
        uint16_t            UdpLength;              //!< UDP length
        uint16_t            SourcePort;             //!< Source port
        uint16_t            DestinationPort;        //!< Destination port
        uint16_t            Length;                 //!< Length
        uint16_t            Checksum;               //!< Checksum
        uint32_t            Data[2];                //!< Data
    } IPv4;

    struct
    {
        uint8_t             IPv6IsNotDefinedYet;    
    } IPv6;
} oC_Udp_PseudoHeader_t;

//==========================================================================================================================================
/**
 * @brief stores UDP datagram
 */
//==========================================================================================================================================
typedef struct PACKED
{
    oC_Udp_Header_t     Header;         //!< Header of the UDP packet
    uint8_t             Data[4];        //!< First byte of the UDP packet data
} oC_Udp_Datagram_t ;

//==========================================================================================================================================
/**
 * @brief stores UDP packet
 *
 * The type is for storing UDP packet. It is packet, that contains IP header + UDP datagram reference
 */
//==========================================================================================================================================
typedef union
{
    oC_Net_Packet_t     Packet;                         //!< Packet

    struct PACKED
    {
        oC_Net_Ipv4PacketHeader_t   Header;             //!< IPv4 header
        oC_Udp_Datagram_t           UdpDatagram;        //!< UDP datagram
    } IPv4;

    struct PACKED
    {
        oC_Net_Ipv6PacketHeader_t   Header;             //!< IPv6 header
        oC_Udp_Datagram_t           UdpDatagram;        //!< UDP datagram
    } IPv6;
} oC_Udp_Packet_t;

//==========================================================================================================================================
/**
 * @brief stores information about UDP state
 *
 * The type is for storing data of the UDP instance. It has to be created by #oC_Udp_New before usage and destroyed by #oC_Udp_Delete when
 * it is not needed anymore
 */
//==========================================================================================================================================
typedef struct Udp_t * oC_Udp_t;

/** @} */
#undef  _________________________________________TYPES_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t   oC_Udp_TurnOn                       ( void );
extern oC_ErrorCode_t   oC_Udp_TurnOff                      ( void );
extern bool             oC_Udp_IsPortReserved               ( oC_Udp_Port_t Port , oC_Process_t Process );
extern oC_ErrorCode_t   oC_Udp_ReservePort                  ( oC_Udp_Port_t * Port , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_Udp_ReleasePort                  ( oC_Udp_Port_t Port , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_Udp_ReleaseAllPortsReservedBy    ( oC_Process_t Process , oC_Time_t Timeout );
extern uint16_t         oC_Udp_Packet_GetMaximumDataSize    ( oC_Net_PacketType_t Type );
extern oC_Udp_Packet_t* oC_Udp_Packet_New                   ( Allocator_t PacketAllocator , oC_Net_PacketType_t Type , const void * Data , uint16_t Size );
extern bool             oC_Udp_Packet_Delete                ( oC_Udp_Packet_t ** Packet );
extern uint16_t         oC_Udp_Packet_GetDataSize           ( const oC_Udp_Packet_t * Packet );
extern bool             oC_Udp_Packet_SetDataSize           ( oC_Udp_Packet_t * Packet , uint16_t Size );
extern bool             oC_Udp_Packet_SetData               ( oC_Udp_Packet_t * Packet , const void * Data , uint16_t Size );
extern oC_ErrorCode_t   oC_Udp_Packet_ReadData              ( const oC_Udp_Packet_t * Packet , void * outData , uint16_t Size );
extern void *           oC_Udp_Packet_GetDataReference      ( oC_Udp_Packet_t * Packet );
extern oC_ErrorCode_t   oC_Udp_Send                         ( oC_Netif_t Netif , oC_Udp_Port_t LocalPort , const oC_Net_Address_t * Destination, oC_Udp_Packet_t * Packet , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_Udp_Receive                      ( oC_Netif_t Netif , oC_Udp_Port_t LocalPort , oC_Udp_Packet_t * Packet , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_Udp_CalculateChecksum            ( oC_Udp_Packet_t * Packet , bool * outChecksumCorrect , uint16_t * outExpectedChecksum );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_PROTOCOLS_UDP_OC_UDP_H_ */
