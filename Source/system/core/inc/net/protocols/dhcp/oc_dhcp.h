/** ****************************************************************************************************************************************
 *
 * @brief      Contains definitions of DHCP
 *
 * @file       oc_dhcp.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Dhcp   DHCP - Dynamic Host Configuration Protocol
 * @ingroup CoreNetwork
 * @brief    Dynamic Host Configuration Protocol
 *
 * The module is for communication with DHCP server (or client)
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_OC_DHCP_H_
#define SYSTEM_CORE_INC_NET_OC_DHCP_H_

#include <oc_stdtypes.h>
#include <oc_compiler.h>
#include <oc_net.h>
#include <oc_netif.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief Magic cookie for the DHCP message
 */
//==========================================================================================================================================
#define oC_DHCP_MAGIC_COOKIE        0x63825363

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief Value for marking last entry in the options array
 */
//==========================================================================================================================================
#define oC_DHCP_END_OPTION          0xFF

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief stores maximum number of bytes reserved for options in DHCP message
 */
//==========================================================================================================================================
#define oC_DHCP_OPTIONS_BUFFER_SIZE     600

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief stores operation code
 * The type is for storing operation code for field `Operation` in the #oC_Dhcp_Message_t type
 */
//==========================================================================================================================================
typedef enum
{
    oC_Dhcp_OperationCode_Request = 1 ,//!< Request to a server
    oC_Dhcp_OperationCode_Reply   = 2 ,//!< Reply from the server
} oC_Dhcp_OperationCode_t;

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief stores type of hardware for #oC_Dhcp_Message_t type
 *
 * The type is for storing hardware type used for the local network.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Dhcp_HardwareType_Ethernet                   = 1 ,  //!< Ethernet (10Mb)
    oC_Dhcp_HardwareType_IEEE802Networks            = 6 ,  //!< IEEE 802 Networks
    oC_Dhcp_HardwareType_ARCNET                     = 7 ,  //!< ARCNET
    oC_Dhcp_HardwareType_LocalTalk                  = 11 , //!< LocalTalk
    oC_Dhcp_HardwareType_LocalNet                   = 12 , //!< LocalNet (IBM PCNet or SYTEK LocalNET)
    oC_Dhcp_HardwareType_SMDS                       = 14 , //!< SMDS
    oC_Dhcp_HardwareType_FrameRelay                 = 15 , //!< Frame Relay
    oC_Dhcp_HardwareType_AsynchronousTransferMode   = 16 , //!< Asynchronous Transfer Mode (ATM)
    oC_Dhcp_HardwareType_HDLC                       = 17 , //!< HDLC
    oC_Dhcp_HardwareType_FibreChannel               = 18 , //!< Fibre Channel
    oC_Dhcp_HardwareType_SerialLine                 = 20 , //!< Serial Line
} oC_Dhcp_HardwareType_t;

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief stores flags for the DHCP message
 *
 * The type is for storing flags used in the #oC_Dhcp_Message_t type.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Dhcp_Flags_None          = 0 ,     //!< None flag is set
    oC_Dhcp_Flags_Broadcast     = 0x8000 ,//!< Broadcast flag - a client does not know its own IP address so it send the message as broadcast. The server receives the message and responds for it also as a broadcast
} oC_Dhcp_Flags_t;

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief Stores DHCP options that can be added into the DHCP message
 *
 * The type is for storing DHCP options that can be placed in the DHCP message. You should use #oC_Dhcp_OptionLength_t type to receive size
 * in bytes required for the given option.
 *
 * @see oC_Dhcp_OptionLength_t
 */
//==========================================================================================================================================
typedef enum
{
    oC_Dhcp_Option_Pad              = 0 ,                 //!< The PAD value used to align word bound
    oC_Dhcp_Option_End              = oC_DHCP_END_OPTION ,//!< The end option marks end of the options array
    oC_Dhcp_Option_SubnetMask       = 1 ,                 //!< Specifies subnetwork mask
    oC_Dhcp_Option_TimeOffset       = 2 ,                 //!< Offset of the client subnetwork in seconds from Coordinated Universal Time (UTC).
    oC_Dhcp_Option_Router           = 3 ,                 //!< The router option specifies a list of IP addresses on the client subnetwork.
    oC_Dhcp_Option_DnsServer        = 6 ,                 //!< List of DNS servers in the subnetwork
    oC_Dhcp_Option_Hostname         = 12 ,                //!< Specifies name of the client.
    oC_Dhcp_Option_DefaultTTL       = 23 ,                //!< Specifies default TTL value for packets
    oC_Dhcp_Option_MTU              = 26 ,                //!< Specifies MTU for the interface
    oC_Dhcp_Option_Broadcast        = 28 ,                //!< Address to use in client subnetwork for sending broadcast packets
    oC_Dhcp_Option_TcpTTL           = 37 ,                //!< Default TTL value for TCP segments
    oC_Dhcp_Option_Ntp              = 42 ,                //!< List of Network Time Protocol Servers available in the subnetwork

    oC_Dhcp_Option_RequestedIp      = 50 ,                //!< The option allows to request the specified static IP
    oC_Dhcp_Option_LeaseTime        = 51 ,                //!< Allows the client to request a lease time for the IP address. The time is in seconds
    oC_Dhcp_Option_Overload         = 52 ,                //!< Used by the DHCP server to mark, that `File` or `SName` or both of these fields are overloaded to store DHCP options
    oC_Dhcp_Option_ServerId         = 54 ,                //!< Destination IP address of DHCP server
    oC_Dhcp_Option_RequestedList    = 55 ,                //!< Used by the DHCP client to request values for specified configuration parameters
    oC_Dhcp_Option_MessageType      = 53 ,                //!< Used to mark type of the message - 1 for `Discover`, 2 - `Offer`, 3 - `Request`, 4 - `Declide`, 5 - `Pack` , 6 - `Pnak`, 7 - `Release`, 8 - `Inform`
    oC_Dhcp_Option_T1RenewalTime    = 58 ,                //!< Time interval from address assignment until the client have to renew the address
    oC_Dhcp_Option_T2RebingingTime  = 59 ,                //!< Time interval until the client transitions to the REBINDING state
    oC_Dhcp_Option_ClientId         = 61 ,                //!< Used by the DHCP clients to specify their unique ID.
    oC_Dhcp_Option_TftpServerName   = 66 ,                //!< When the `SName` field is used for options, this field specifies name of the TFTP server
    oC_Dhcp_Option_Bootfile         = 67 ,                //!< When the `File` field is used for options, this field specifies a boot file name.
} oC_Dhcp_Option_t;

//==========================================================================================================================================
/**
 * @brief stores length for options from #oC_Dhcp_Option_t type
 */
//==========================================================================================================================================
typedef enum
{
    oC_Dhcp_OptionLength_SubnetMask         = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_SubnetMask
    oC_Dhcp_OptionLength_TimeOffset         = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_TimeOffset
    oC_Dhcp_OptionLength_RouterMin          = 4 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_RouterMin
    oC_Dhcp_OptionLength_DnsServerMin       = 4 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_DnsServerMin
    oC_Dhcp_OptionLength_HostnameMin        = 1 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_HostnameMin
    oC_Dhcp_OptionLength_DefaultTTL         = 1 ,//!< Length in bytes for option #oC_Dhcp_Option_DefaultTTL
    oC_Dhcp_OptionLength_MTU                = 2 ,//!< Length in bytes for option #oC_Dhcp_Option_MTU
    oC_Dhcp_OptionLength_Broadcast          = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_Broadcast
    oC_Dhcp_OptionLength_TcpTTL             = 1 ,//!< Length in bytes for option #oC_Dhcp_Option_TcpTTL
    oC_Dhcp_OptionLength_NtpMin             = 4 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_NtpMin

    oC_Dhcp_OptionLength_RequestedIp        = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_RequestedIp
    oC_Dhcp_OptionLength_LeaseTime          = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_LeaseTime
    oC_Dhcp_OptionLength_Overload           = 1 ,//!< Length in bytes for option #oC_Dhcp_Option_Overload
    oC_Dhcp_OptionLength_ServerId           = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_ServerId
    oC_Dhcp_OptionLength_RequestedListMin   = 1 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_RequestedList
    oC_Dhcp_OptionLength_MessageType        = 1 ,//!< Length in bytes for option #oC_Dhcp_Option_MessageType
    oC_Dhcp_OptionLength_T1RenevalTime      = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_T1RenevalTime
    oC_Dhcp_OptionLength_T2RebindingTime    = 4 ,//!< Length in bytes for option #oC_Dhcp_Option_T2RebindingTime
    oC_Dhcp_OptionLength_ClientIdMin        = 2 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_ClientIdMin
    oC_Dhcp_OptionLength_TftpServerNameMin  = 1 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_TftpServerNameMin
    oC_Dhcp_OptionLength_BootfileMin        = 1 ,//!< Minimum length in bytes for option #oC_Dhcp_Option_Bootfile
} oC_Dhcp_OptionLength_t;

//==========================================================================================================================================
/**
 * @ingroup Dhcp
 * @brief stores DHCP message
 *
 * The type is for storing DHCP message format.
 */
//==========================================================================================================================================
typedef struct PACKED
{
#ifdef LITTLE_ENDIAN
    uint32_t        Operation:8;                          //!< Specifies type of the message. A value `1` indicates a request message, while a value 2 is a reply message. You can use #oC_Dhcp_OperationCode_t type to assign this value
    uint32_t        HardwareType:8;                       //!< Hardware Type - specifies type of the hardware used in the local network. You can assign values from the #oC_Dhcp_HardwareType_t type.
    uint32_t        HardwareAddressLength:8;              //!< Length of the hardware address in bytes
    uint32_t        Hops:8;                               //!< Set to 0 by a client when transmitting a request and used by relay agents to control the forwarding of BOOT and/or DHCP messages
    uint32_t        XID;                                  //!< `Transaction ID` Generated by the client to allow it to match up the request with replies received from DHCP servers
    uint32_t        Secs:16;                              //!< `Seconds` - In BOOTP this field was vaguely defined and not always used. For DHCP, it is defined as the number of seconds elapsed since a client began an attempt to acquire or renew a lease. This may be used by a busy DHCP server to prioritize replies when multiple client requests are outstanding.
    uint32_t        Flags:16;                             //!< Flags of the message, look at #oC_Dhcp_Flags_t type for more
    uint32_t        CIAddr;                               //!< `Client IP Address` - If a client has IP address and it is valid (while BOUND, RENEWING or REBINDING states) it has to fill this field by it, otherwise it should be set to 0.
    uint32_t        YIAddr;                               //!< `Your IP Address` - the server fills it as the proposed IP address for the client
    uint32_t        SIAddr;                               //!< `Server IP Address` - the server fills it with its own address, that should be used for the states after DISCOVER
    uint32_t        GIAddr;                               //!< `Gateway IP Address` - a router IP
    uint8_t         CHAddr[16];                           //!< `Client Hardware Address` - the hardware address of the client used for identification and communication
    char            SName[64];                            //!< `Server Name` - The server can optionally put its name in this field
    char            File[128];                            //!< `Boot Filename` - Optionally used by the client to request a particular type of boot file in a DISCOVER message
    uint32_t        MagicCookie;                          //!< `Magic cookie` - set to #oC_DHCP_MAGIC_COOKIE (0x63825363) to identify the information as vendor independent option fields
    uint8_t         Options[oC_DHCP_OPTIONS_BUFFER_SIZE]; //!< Additional options array

#elif defined(BIG_ENDIAN)
#   error   The structure is not defined for BIG_ENDIAN
#else
#   error   Endianess is not correct
#endif
} oC_Dhcp_Message_t ;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

extern oC_ErrorCode_t oC_Dhcp_SendMessage               ( oC_Netif_t Netif , oC_Dhcp_Message_t * Message    , uint16_t Size, oC_Time_t Timeout );
extern oC_ErrorCode_t oC_Dhcp_ReceiveMessage            ( oC_Netif_t Netif , oC_Dhcp_Message_t * outMessage , oC_Net_Address_t* outSrc , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_Dhcp_RequestIp                 ( oC_Netif_t Netif , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_Dhcp_SendDiscovery             ( oC_Netif_t Netif , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_Dhcp_ReceiveOffer              ( oC_Netif_t Netif , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_Dhcp_SendRequest               ( oC_Netif_t Netif , oC_Time_t Timeout );
extern oC_ErrorCode_t oC_Dhcp_ReceiveAcknowledge        ( oC_Netif_t Netif , oC_Time_t Timeout );

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_OC_DHCP_H_ */
