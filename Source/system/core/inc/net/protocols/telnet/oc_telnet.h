/** ****************************************************************************************************************************************
 *
 * @brief        File with interface for TELNET protocol
 * 
 * @file       oc_telnet.h
 *
 * @author     Patryk Kubiak - (Created on: 16.01.2017 21:31:19) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_CORE_INC_NET_PROTOCOLS_TELNET_OC_TELNET_H_
#define SYSTEM_CORE_INC_NET_PROTOCOLS_TELNET_OC_TELNET_H_

#include <oc_tcp.h>
#include <oc_driver.h>
#include <oc_debug_cfg.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#if CFG_ENABLE_TELNET_LOGS == ON
#   define telnetlog( LogType, ...)     kdebuglog( LogType , __VA_ARGS__ )
#else
#   define telnetlog( LogType, ...)
#endif

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________



#endif /* SYSTEM_CORE_INC_NET_PROTOCOLS_TELNET_OC_TELNET_H_ */
