/** ****************************************************************************************************************************************
 *
 * @brief      Stores interface of IP module
 *
 * @file       oc_ip.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup IP IP
 * @ingroup CoreNetwork
 * @brief The module for handling IP
 * 
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_OC_IP_H_
#define SYSTEM_CORE_INC_NET_OC_IP_H_

#include <oc_net.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

extern oC_ErrorCode_t       oC_Ip_TurnOn        ( void );
extern oC_ErrorCode_t       oC_Ip_TurnOff       ( void );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_OC_IP_H_ */
