/** ****************************************************************************************************************************************
 *
 * @brief      Module for managing port reservations
 *
 * @file       oc_portman.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup PortMan PortMan - Port Manager
 * @ingroup CoreNetwork
 * @brief Helps to managing network ports in different protocols
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_SRC_NET_OC_PORTMAN_H_
#define SYSTEM_CORE_SRC_NET_OC_PORTMAN_H_

#include <oc_errors.h>
#include <oc_module.h>
#include <oc_process.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________
/**
 * @addtogroup PortMan
 * @{
 */

//==========================================================================================================================================
/**
 * @brief stores the port number
 */
//==========================================================================================================================================
typedef uint32_t oC_PortMan_Port_t;

//==========================================================================================================================================
/**
 * @brief stores configuration of the module
 *
 * The type is for storing configuration of the network modules.
 */
//==========================================================================================================================================
typedef struct
{
    oC_PortMan_Port_t            MaximumPortNumber;          //!< Maximum number of correct port
    oC_PortMan_Port_t            FirstDynamicPortNumber;     //!< The first port, that can be dynamically reserved (when the port to reserve is 0). For more info see #oC_PortMan_ReservePort
    oC_PortMan_Port_t            LastDynamicPortNumber;      //!< Last port, that can be dynamically reserved (when the port to reserve is 0). For more info see #oC_PortMan_ReservePort
} oC_PortMan_Config_t;

/** @} */
#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________
/**
 * @addtogroup PortMan
 * @{
 */

extern oC_ErrorCode_t   oC_PortMan_TurnOn               ( void );
extern oC_ErrorCode_t   oC_PortMan_TurnOff              ( void );
extern oC_ErrorCode_t   oC_PortMan_RegisterModule       ( oC_Module_t Module , const oC_PortMan_Config_t * Config , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_PortMan_UnregisterModule     ( oC_Module_t Module , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_PortMan_ReservePort          ( oC_Module_t Module , oC_PortMan_Port_t * Port , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_PortMan_ReleasePort          ( oC_Module_t Module , oC_PortMan_Port_t Port , oC_Time_t Timeout );
extern oC_ErrorCode_t   oC_PortMan_ReleaseAllPortsOf    ( oC_Module_t Module , oC_Process_t Process , oC_Time_t Timeout );
extern bool             oC_PortMan_IsPortReserved       ( oC_Module_t Module , oC_PortMan_Port_t Port );
extern bool             oC_PortMan_IsPortReservedBy     ( oC_Module_t Module , oC_PortMan_Port_t Port , oC_Process_t Process );

/** @} */
#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

#endif /* SYSTEM_CORE_SRC_NET_OC_PORTMAN_H_ */
