/** ****************************************************************************************************************************************
 *
 * @brief      File with interface for the NET module
 *
 * @file       oc_net.h
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup NET Network
 * @ingroup CoreNetwork
 * @brief Network types, definitions and low level functions
 *
 * The module is for storing low level definitions of the network. You are probably looking for #Netif object
 *
 ******************************************************************************************************************************************/

#ifndef SYSTEM_CORE_INC_NET_OC_NET_H_
#define SYSTEM_CORE_INC_NET_OC_NET_H_

#include <oc_memory.h>
#include <oc_stdio.h>
#include <oc_baudrate.h>
#include <oc_compiler.h>
#include <oc_string.h>
#include <oc_stdlib.h>
#include <oc_array.h>
#include <oc_debug.h>
#include <oc_memman.h>
#include <oc_math.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

// +----------------------------------------------------------------+
// |                            IEEE 802.3                          |
// +----------------------------------------------------------------+
#define IEEE_802_3_MAC_ADDRESS_LENGTH           6
#define IEEE_802_3_ETHERNET_MTU                 1500
#define IEEE_802_3_MINIMUM_PAYLOAD              42

// +----------------------------------------------------------------+
// |                            SELECTED STD                        |
// +----------------------------------------------------------------+
#define MAC_ADDRESS_LENGTH                      IEEE_802_3_MAC_ADDRESS_LENGTH
#define NETWORK_MTU                             IEEE_802_3_ETHERNET_MTU
#define MINIMUM_PAYLOAD                         IEEE_802_3_MINIMUM_PAYLOAD
#define IPv4_MAXIMUM_DATA_LENGTH                (NETWORK_MTU - sizeof(oC_Net_Ipv4PacketHeader_t))
#define IPv6_MAXIMUM_DATA_LENGTH                (NETWORK_MTU - sizeof(oC_Net_Ipv6PacketHeader_t))

// +----------------------------------------------------------------+
// |                      OTHER NET DEFINITIONS                     |
// +----------------------------------------------------------------+
#define IFNAMSIZ                                64
#define MAXIMUM_NETWORK_INTERFACES              0xFFFFFFFF
#define MAXIMUM_FRIENDLY_INTERFACE_NAME         100
#define DHCP_SERVER_NAME_LENGTH                 64
#define MAXIMUM_PACKET_SIZE                     UINT16_MAX

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with function-like macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTION_LIKE_MACROS_SECTION_______________________________________________________________

#define IP(A,B,C,D)             ( ( (D) << 0 ) | ( (C) << 8 ) | ( (B) << 16 ) | ( (A) << 24 ))

#undef  _________________________________________FUNCTION_LIKE_MACROS_SECTION_______________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores MAC address
 */
//==========================================================================================================================================
typedef uint8_t oC_Net_MacAddress_t[MAC_ADDRESS_LENGTH];

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores hardware address
 */
//==========================================================================================================================================
typedef struct
{
    union
    {
        uint8_t                 Address;        //!< Universal HW Address pointer
        oC_Net_MacAddress_t     MacAddress;     //!< MAC address
    };
    bool                    Filled;         //!< True if the address is filled
} oC_Net_HardwareAddress_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores IPv4 address
 *
 * The type is for storing IP address in ver. 4
 */
//==========================================================================================================================================
typedef uint32_t oC_Net_Ipv4_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores IPv6 address
 *
 * The type is for storing IP address in ver. 6
 */
//==========================================================================================================================================
typedef struct PACKED
{
    uint64_t    LowPart;        //!< Low 8 bytes of the IPv6 address
    uint64_t    HighPart;       //!< High 8 bytes of the IPv6 address
} oC_Net_Ipv6_t ;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores the protocol number for the IP headers
 * The type is for storing protocol number. The values can be written indirectly to the IP headers.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Net_Protocol_ICMP    = 1 ,  //!< Internet Control Message Protocol
    oC_Net_Protocol_IGMP    = 2 ,  //!< Internet Group Management Protocol
    oC_Net_Protocol_TCP     = 6 ,  //!< Transmission Control Protocol
    oC_Net_Protocol_UDP     = 17 , //!< User Datagram Protocol
    oC_Net_Protocol_ENCAP   = 41 , //!< IPv6 encapsulation protocol
    oC_Net_Protocol_OSPF    = 89 , //!< Open Shortest Path First
    oC_Net_Protocol_SCTP    = 132 ,//!< Stream Control Transmission Protocol
} oC_Net_Protocol_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores type of the packet
 *
 * The type is for storing packet type. The values can be used to the `Version` field in IP headers.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Net_PacketType_Invalid   = 0,    //!< Invalid value
    oC_Net_PacketType_IPv4      = 4,    //!< Packet in type IPv4
    oC_Net_PacketType_IPv6      = 6,    //!< Packet in type IPv6
} oC_Net_PacketType_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores type of the frame
 *
 * The type is for storing frame type. The values can be used to the `EtherType` field in ethernet headers.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Net_FrameType_IPv4       = 0x0800 , //!< Frame stores IPv4 packet
    oC_Net_FrameType_IPv6       = 0x86DD , //!< Frame stores IPv6 packet
    oC_Net_FrameType_ARP        = 0x0806 , //!< Address resolution packet
} oC_Net_FrameType_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores hardware type of the network interface
 */
//==========================================================================================================================================
typedef enum
{
    oC_Net_HardwareType_Invalid  = 0 ,  //!< Invalid type
    oC_Net_HardwareType_Ethernet = 1 ,  //!< Ethernet
} oC_Net_HardwareType_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief contains IPv4 packet header
 *
 * The type stores IPv4 packet header.
 */
//==========================================================================================================================================
typedef struct PACKED
{
#ifdef LITTLE_ENDIAN
    uint32_t            Version:4;                  //!< 4 bits, that contain the version, that specified if it's an IPv4 or IPv6 packet
    uint32_t            IHL:4;                      //!< 4 bits, that contain Internet Header Length, which is the length of the header in multiples of 4 (number of words - NOT BYTES!!!)
    uint32_t            QoS:8;                      //!< `Quality of Service` or `Type of Service` - describes the priority of the packet
    uint32_t            Length:16;                  //!< Length of the packet in bytes
    uint32_t            ID:16;                      //!< ID value for reconstruction the packet from segments
    uint32_t            FragmentOffset:13;          //!< Field to identify position of fragment within original packet
    uint32_t            MF:1;                       //!< `More Fragments` - whether more fragments of a packet follow
    uint32_t            DF:1;                       //!< `Don't Fragment` - packet can be fragmented or not
    uint32_t            Reserved:1;                 //!< This is always 0
    uint32_t            TTL:8;                      //!< `Time to live` - number of hops the packet is allowed to pass before it dies. It is to prevent circuits in the network.
    uint32_t            Protocol:8;                 //!< Contains selected protocol (TCP/UDP/ICMP,etc). You can use #oC_Net_Protocol_t type to assign a value to this field
    uint32_t            Checksum:16;                //!< Header checksum - number used for errors detection
    oC_Net_Ipv4_t       SourceIp;                   //!< Source IP address
    oC_Net_Ipv4_t       DestinationIp;              //!< Destination IP address
#elif defined(BIG_ENDIAN)
#   error The structure is not defined for BIG_ENDIAN
#else
#   error Endianess is not defined
#endif
} oC_Net_Ipv4PacketHeader_t ;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief contains IPv6 packet header
 *
 * The type is for storing IPv6 packet header
 */
//==========================================================================================================================================
typedef struct PACKED
{
#ifdef LITTLE_ENDIAN
    uint32_t            Version:4;                  //!< The constant 6 (0b0110)
    uint32_t            TrafficClass:8;             //!< The bits of this field hold two values. The 6 most-significant bits are used for differentiated services, which is used to classify packets.[2][3] The remaining two bits are used for ECN;[4] priority values subdivide into ranges: traffic where the source provides congestion control and non-congestion control traffic.
    uint32_t            FlowLabel:20;               //!< Originally created for tracking, now it's only a hint for routers and switches.
    uint32_t            PayloadLength:16;           //!< Size of the payload in octets (bytes)
    uint32_t            NextHeader:8;               //!< Specifies the type of the next header in the packet (TCP/UDP,etc)
    uint32_t            HopLimit:8;                 //!< The same as TTL from the IPv4 header - time of packet's live.
    oC_Net_Ipv6_t       Source;                     //!< Source IP address
    oC_Net_Ipv6_t       Destination;                //!< Destination IP address
#elif defined(BIG_ENDIAN)
#   error The structure is not defined for BIG_ENDIAN
#else
#   error Endianess is not defined
#endif
} oC_Net_Ipv6PacketHeader_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores IPv4 packet
 *
 * The type is for storing packets in IPv4 format
 */
//==========================================================================================================================================
typedef struct PACKED
{
    oC_Net_Ipv4PacketHeader_t   Header;                             //!< Header of the IPv4 packet
    uint8_t                     Data[IPv4_MAXIMUM_DATA_LENGTH];     //!< Data of the packet
} oC_Net_Ipv4Packet_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores IPv6 packet
 *
 * The type is for storing packets in IPv6 format
 */
//==========================================================================================================================================
typedef struct PACKED
{
    oC_Net_Ipv6PacketHeader_t   Header;                             //!< Header of the IPv6 packet
    uint8_t                     Data[IPv6_MAXIMUM_DATA_LENGTH];     //!< Data of the packet
} oC_Net_Ipv6Packet_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores network packet
 *
 * The type is for storing network packets
 */
//==========================================================================================================================================
typedef union
{
    oC_Net_Ipv4Packet_t         IPv4;   //!< Packet in format IPv4
    oC_Net_Ipv6Packet_t         IPv6;   //!< Packet in format IPv6
} oC_Net_Packet_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores informations about the frame to send
 */
//==========================================================================================================================================
typedef struct
{
    oC_Net_HardwareAddress_t     Destination;           //!< Hardware Destination Address
    oC_Net_HardwareAddress_t     Source;                //!< Hardware Source Address
    union
    {
        oC_Net_Packet_t*             Packet;                //!< Pointer to the packet to send
        const oC_Net_Packet_t*       ConstPacket;           //!< Pointer to the packet to send (for sending)
    };
    uint16_t                     Size;                  //!< Size Data in frame
    oC_Net_FrameType_t           FrameType;             //!< Type of the frame
} oC_Net_Frame_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores layer of the OSI model
 */
//==========================================================================================================================================
typedef enum
{
    oC_Net_Layer_Physical       = 1 << 4,                      //!< In the seven-layer OSI model of computer networking, the physical layer or layer 1 is the first and lowest layer.[1] The implementation of this layer is often termed PHY.
    oC_Net_Layer_DataLink       = 2 << 4,                      //!< The data link layer or layer 2 is the second layer of the seven-layer OSI model of computer networking. This layer is the protocol layer that transfers data between adjacent network nodes in a wide area network (WAN) or between nodes on the same local area network (LAN) segment. The data link layer provides the functional and procedural means to transfer data between network entities and might provide the means to detect and possibly correct errors that may occur in the physical layer.
    oC_Net_Layer_Network        = 3 << 4,                      //!< In the seven-layer OSI model of computer networking, the network layer is layer 3. The network layer is responsible for packet forwarding including routing through intermediate routers, since it knows the address of neighboring network nodes, and it also manages quality of service (QoS), and recognizes and forwards local host domain messages to the Transport layer (layer 4). The data link layer (layer 2) is responsible for media access control, flow control and error checking.
    oC_Net_Layer_Transport      = 4 << 4,                      //!< In computer networking, the transport layer is a conceptual division of methods in the layered architecture of protocols in the network stack in the Internet Protocol Suite and the Open Systems Interconnection (OSI). The protocols of the layer provide host-to-host communication services for applications. It provides services such as connection-oriented data stream support, reliability, flow control, and multiplexing.
    oC_Net_Layer_Session        = 5 << 4,                      //!< In the seven-layer OSI model of computer networking, the session layer is layer 5. The session layer provides the mechanism for opening, closing and managing a session between end-user application processes, i.e., a semi-permanent dialogue. Communication sessions consist of requests and responses that occur between applications. Session-layer services are commonly used in application environments that make use of remote procedure calls (RPCs).
    oC_Net_Layer_Presentation   = 6 << 4,                      //!< In the seven-layer OSI model of computer networking, the presentation layer is layer 6 and serves as the data translator for the network. It is sometimes called the syntax layer.
    oC_Net_Layer_Application    = 7 << 4,                      //!< An application layer is an abstraction layer that specifies the shared protocols and interface methods used by hosts in a communications network. The application layer abstraction is used in both of the standard models of computer networking: the Internet Protocol Suite (TCP/IP) and the Open Systems Interconnection model (OSI model).

    oC_Net_Layer_MainLayerMask  = 0xF << 4 ,                   //!< Mask to get the main layer of the OSI model
    oC_Net_Layer_SubLayerMask   = 0xF ,                        //!< Mask to get the sublayer of the OSI model

    oC_Net_Layer_PHY            = oC_Net_Layer_Physical | 0x0 ,//!< PHY layer

    oC_Net_Layer_LLC            = oC_Net_Layer_DataLink | 0x0 ,//!< Logical link control sublayer - The uppermost sublayer, LLC, multiplexes protocols running atop the data link layer, and optionally provides flow control, acknowledgment, and error notification.
    oC_Net_Layer_MAC            = oC_Net_Layer_DataLink | 0x1 ,//!< MAC may refer to the sublayer that determines who is allowed to access the media at any one time (e.g. CSMA/CD). Other times it refers to a frame structure delivered based on MAC addresses inside.

    oC_Net_Layer_Netif          = oC_Net_Layer_Network  | 0x0 ,//!< Network interface layer - sublayer of the Network layer.
} oC_Net_Layer_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores network interface link status
 */
//==========================================================================================================================================
typedef enum
{
    oC_Net_LinkStatus_Down ,//!< Network interface is UP   (cable is connected)
    oC_Net_LinkStatus_Up    //!< Network interface is DOWN (cable is not connected)
} oC_Net_LinkStatus_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores index of the interface
 *
 * The type is for storing index of the given interface in the network driver. It can be used for creating the interface name
 */
//==========================================================================================================================================
typedef uint32_t oC_Net_InterfaceIndex_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores name of the network interface
 *
 * The type is for storing name of the interface. It is just to make it shorter and easier to manipulate
 */
//==========================================================================================================================================
typedef char oC_Net_InterfaceName_t[IFNAMSIZ];

//==========================================================================================================================================
/**
 * @brief type for storing friendly name string
 *
 * The type is for storing friendly name string of the network interface (the string that is easy to understand and remember for human).
 */
//==========================================================================================================================================
typedef char oC_Net_FriendlyInterfaceName_t[MAXIMUM_FRIENDLY_INTERFACE_NAME];

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores status of network queue
 *
 * The type is for storing status of the Queue.
 */
//==========================================================================================================================================
typedef struct
{
    uint32_t        NumberOfElementsInTxQueue;  //!< Number of elements in queue to send
    uint32_t        NumberOfElementsInRxQueue;  //!< Number of elements in queue ready to receive
} oC_Net_QueueStatus_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief Port of the network address
 *
 * The type is for storing port of the network address (e.x. 192.168.1.1:80, where 80 is the port)
 */
//==========================================================================================================================================
typedef uint16_t oC_Net_Port_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores type of the network address
 *
 * The type is for storing type of a network address for #oC_Net_Address_t structure.
 */
//==========================================================================================================================================
typedef enum
{
    oC_Net_AddressType_IPv4 = oC_Net_PacketType_IPv4 ,   //!< IP address in version 4
    oC_Net_AddressType_IPv6 = oC_Net_PacketType_IPv6 ,   //!< IP address in version 6
} oC_Net_AddressType_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores network address
 *
 * The type is for storing all types of network address.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Net_AddressType_t    Type;       //!< Type of the address stored inside
    union
    {
        oC_Net_Ipv4_t   IPv4;           //!< Address in IPv4 version
        oC_Net_Ipv6_t   IPv6;           //!< Address in IPv6 version
    };
    oC_Net_Protocol_t   Protocol;           //!< IP Protocol
    oC_Net_Port_t       Port;               //!< Port of the address
} oC_Net_Address_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores cost of the network connection
 */
//==========================================================================================================================================
typedef uint32_t oC_Net_Cost_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores network interface IPv4 informations
 *
 * The type is for storing network interface informations of IPv4
 */
//==========================================================================================================================================
typedef struct
{
    oC_Net_Ipv4_t              IP;                                          //!< IP of the network interface
    oC_Net_Ipv4_t              NetIP;                                       //!< IP of the local network (router IP)
    oC_Net_Ipv4_t              Netmask;                                     //!< Local network mask
    oC_Net_Ipv4_t              DhcpIP;                                      //!< IP of the local DHCP server
    char                       DhcpServerName[DHCP_SERVER_NAME_LENGTH];     //!< Name of the local DHCP server
    oC_Net_Ipv4_t              GatewayIP;                                   //!< Default gateway IP
    oC_Net_Ipv4_t              DnsIP;                                       //!< IP of the local DNS server
    oC_MemorySize_t            MTU;                                         //!< MTU (Maximum Transmission Unit) for the local network
    oC_Net_Ipv4_t              BroadcastIP;                                 //!< IP that should be used for broadcasting
    uint8_t                    DefaultTTL;                                  //!< Default TTL value for the IPv4 packets
    oC_Net_Ipv4_t              NtpIP;                                       //!< Main Network Time Protocol Server available in the subnetwork
    oC_Time_t                  LeaseTime;                                   //!< IP lease time
    oC_Timestamp_t             IpExpiredTimestamp;                          //!< Timestamp of IP expiration
    oC_Net_HardwareAddress_t   HardwareRouterAddress;                       //!< HW address of the local router
    bool                       StaticIP;                                    //!< If true, the IP is assigned as static
} oC_Net_Ipv4Info_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores network interface IPv6 informations
 *
 * The type is for storing network interface informations of IPv6
 */
//==========================================================================================================================================
typedef struct
{
    oC_Net_Ipv6_t      IPv6;    //!< IP of the network interface
} oC_Net_Ipv6Info_t;

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief stores network interface informations
 *
 * The type is for storing informations about the network interface
 */
//==========================================================================================================================================
typedef struct
{
    oC_Net_InterfaceName_t      InterfaceName;              //!< Name of the interface
    oC_Net_InterfaceIndex_t     InterfaceIndex;             //!< Index of the interface in the driver
    const char *                DriverName;                 //!< Name of the driver that handles the network interface
    oC_Net_LinkStatus_t         LinkStatus;                 //!< Network interface link status (UP/DOWN)
    oC_MemorySize_t             TransmittedBytes;           //!< Number of transmitted bytes
    oC_MemorySize_t             ReceivedBytes;              //!< Number of received bytes
    oC_Net_HardwareType_t       HardwareType;               //!< Type of the hardware device
    oC_Net_HardwareAddress_t    HardwareAddress;            //!< Hardware address
    oC_Net_HardwareAddress_t    HardwareBroadcastAddress;   //!< Hardware address that should be used for broadcast
    uint32_t                    HardwareAddressLength;      //!< Number of bytes in the HW address
    oC_BaudRate_t               BaudRate;                   //!< Baud rate of the interface
    oC_Net_QueueStatus_t        QueueStatus;                //!< Current status of network interface's queue

    struct
    {
        oC_Net_Ipv4Info_t       IPv4;   //!< IPv4 informations
        oC_Net_Ipv6Info_t       IPv6;   //!< IPv6 informations
    } NetworkLayer;                     //!< Struct with data from the network layer
} oC_Net_Info_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INLINE_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts uint16_t to network byte order
 */
//==========================================================================================================================================
static inline uint16_t oC_Net_ConvertUint16ToNetworkEndianess(uint16_t v)
{
#if defined(LITTLE_ENDIAN)
  return (v >> 8) | (v << 8);
#elif defined(BIG_ENDIAN)
  return v;
#else
#   error Unknown endianess
#endif
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts uint32_t to network byte order
 */
//==========================================================================================================================================
static inline uint32_t oC_Net_ConvertUint32ToNetworkEndianess(uint32_t v)
{
#if defined(LITTLE_ENDIAN)
  return oC_Net_ConvertUint16ToNetworkEndianess(v >> 16) | (oC_Net_ConvertUint16ToNetworkEndianess((uint16_t) v) << 16);
#elif defined(BIG_ENDIAN)
  return v;
#else
#   error Unknown endianess
#endif
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts uint16_t from network byte order
 */
//==========================================================================================================================================
static inline uint16_t oC_Net_ConvertUint16FromNetworkEndianess(uint16_t v)
{
    return oC_Net_ConvertUint16ToNetworkEndianess(v);
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts uint32_t from network byte order
 */
//==========================================================================================================================================
static inline uint32_t oC_Net_ConvertUint32FromNetworkEndianess(uint32_t v)
{
  return oC_Net_ConvertUint32ToNetworkEndianess(v);
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts buffer to network endianess
 */
//==========================================================================================================================================
static inline void oC_Net_ConvertBufferToNetworkEndianess( void * Buffer , uint16_t Size  )
{
    uint16_t * array      = Buffer;
    uint16_t   arraySize  = Size / sizeof(uint16_t);

    if(
        oC_SaveIfFalse("oC_Net_ConvertBufferToNetworkEndianess: "                               , isram(Buffer)                     , oC_ErrorCode_AddressNotInRam    )
     && oC_SaveIfFalse("oC_Net_ConvertBufferToNetworkEndianess: (size is 0)"                    , Size > 0                          , oC_ErrorCode_SizeNotCorrect     )
     && oC_SaveIfFalse("oC_Net_ConvertBufferToNetworkEndianess: (size not aligned to 2 bytes)"  , Size % sizeof(uint16_t) == 0      , oC_ErrorCode_SizeNotAligned     )
        )
    {
        for(uint16_t i = 0 ; i < arraySize ; i++)
        {
            array[i] = oC_Net_ConvertUint16ToNetworkEndianess(array[i]);
        }
    }
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts buffer from network endianess
 */
//==========================================================================================================================================
static inline void oC_Net_ConvertBufferFromNetworkEndianess( void * Buffer , uint16_t Size  )
{
    uint16_t * array      = Buffer;
    uint16_t   arraySize  = Size / sizeof(uint16_t);

    if(
        oC_SaveIfFalse("oC_Net_ConvertBufferFromNetworkEndianess: "                               , isram(Buffer)                     , oC_ErrorCode_AddressNotInRam    )
     && oC_SaveIfFalse("oC_Net_ConvertBufferFromNetworkEndianess: (size is 0)"                    , Size > 0                          , oC_ErrorCode_SizeNotCorrect     )
     && oC_SaveIfFalse("oC_Net_ConvertBufferFromNetworkEndianess: (size not aligned to 2 bytes)"  , Size % sizeof(uint16_t) == 0      , oC_ErrorCode_SizeNotAligned     )
        )
    {
        for(uint16_t i = 0 ; i < arraySize ; i++)
        {
            array[i] = oC_Net_ConvertUint16FromNetworkEndianess(array[i]);
        }
    }
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief prepares interface name string
 *
 * The function is for preparation of the interface name according to prefix and index. In general it is designed for drivers usage, but
 * it can also be useful in the other network related modules and programs. The function is defined to provide the same network interface
 * name standard in each network driver.
 *
 * @param outName       Name destination buffer
 * @param Prefix        Prefix of the interface name (for example 'eth')
 * @param Index         Unique index of the adapter in the network driver
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
static inline int oC_Net_PrepareInterfaceName( oC_Net_InterfaceName_t *outName  , const char * Prefix , oC_Net_InterfaceIndex_t Index )
{
    return sprintf_s( *outName , sizeof(*outName) , "%s#%X" , Prefix , Index);
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief reads MAC address from the string
 *
 * The function is for reading MAC address in format 'FF:FF:FF:FF:FF:FF' from string to the oC_Net_MacAddress_t type array.
 *
 * @param MacAddressString      String with MAC address
 * @param outMacAddress         Destination for the macAddress
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
static inline int oC_Net_MacAddressFromString( const char * MacAddressString , oC_Net_MacAddress_t outMacAddress )
{
    return sscanf(MacAddressString,"%x:%x:%x:%x:%x:%x" , &outMacAddress[0], &outMacAddress[1], &outMacAddress[2],
                                                         &outMacAddress[3], &outMacAddress[4], &outMacAddress[5]);
}

//==========================================================================================================================================
/**
 * @ingroup
 * @brief returns user friendly name of the protocol
 */
//==========================================================================================================================================
static inline const char * oC_Net_GetProtocolName( oC_Net_Protocol_t Protocol )
{
    const char * name = "unknown";

    switch(Protocol)
    {
        case oC_Net_Protocol_ICMP    : name = "[ICMP ] Internet Control Message Protocol   "; break;
        case oC_Net_Protocol_IGMP    : name = "[IGMP ] Internet Group Management Protocol  "; break;
        case oC_Net_Protocol_TCP     : name = "[TCP  ] Transmission Control Protocol       "; break;
        case oC_Net_Protocol_UDP     : name = "[UDP  ] User Datagram Protocol              "; break;
        case oC_Net_Protocol_ENCAP   : name = "[ENCAP] IPv6 encapsulation protocol         "; break;
        case oC_Net_Protocol_OSPF    : name = "[OSPF ] Open Shortest Path First            "; break;
        case oC_Net_Protocol_SCTP    : name = "[SCTP ] Stream Control Transmission Protocol"; break;
        default:
            break;
    }


    return name;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief prints HW address to the string
 *
 * The function is for printing HW address to the string
 *
 * @param HardwareAddress       Hardware address to print
 * @param Type                  Type of the address filled in the HardwareAddress structure
 * @param outString             Destination buffer for a HW-Address string
 * @param Size                  Size of the destination buffer
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Net_HardwareAddressToString( const oC_Net_HardwareAddress_t * HardwareAddress , oC_Net_HardwareType_t Type , char * outString , oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(HardwareAddress)   , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( HardwareAddress->Filled             , oC_ErrorCode_HardwareAddressIsEmpty   )
     && ErrorCondition( isram(outString)                    , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Size > 0                            , oC_ErrorCode_SizeNotCorrect           )
        )
    {
        switch(Type)
        {
            case oC_Net_HardwareType_Ethernet:
                sprintf_s(outString,Size,"%02X:%02X:%02X:%02X:%02X:%02X",
                          HardwareAddress->MacAddress[0],
                          HardwareAddress->MacAddress[1],
                          HardwareAddress->MacAddress[2],
                          HardwareAddress->MacAddress[3],
                          HardwareAddress->MacAddress[4],
                          HardwareAddress->MacAddress[5]
                          );
                errorCode = oC_ErrorCode_None;
                break;
            default:
                errorCode = oC_ErrorCode_UnknownHardwareType;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief prints IPv4 address to the string
 *
 * The function is for printing IP to the string
 *
 * @param Address       Address to print
 * @param outString     Destination for a string
 * @param Size          size of the destination buffer
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Net_Ipv4AddressToString( oC_Net_Ipv4_t Address, char * outString , oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outString)                , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( Size > 0                        , oC_ErrorCode_SizeNotCorrect               )
        )
    {
        sprintf_s(outString, Size, "%d.%d.%d.%d",
                  (Address & 0xFF000000) >> 24,
                  (Address & 0x00FF0000) >> 16,
                  (Address & 0x0000FF00) >> 8,
                  (Address & 0x000000FF) >> 0
                  );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief prints IP to the string
 *
 * The function is for printing IP to the string
 *
 * @param Address       Address to print
 * @param outString     Destination for a string
 * @param Size          size of the destination buffer
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Net_AddressToString( const oC_Net_Address_t * Address, char * outString , oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Address)       , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( isram(outString)                , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( Size > 0                        , oC_ErrorCode_SizeNotCorrect               )
        )
    {
        if(Address->Type == oC_Net_AddressType_IPv4)
        {
            errorCode = oC_Net_Ipv4AddressToString(Address->IPv4,outString,Size);
        }
        else if(Address->Type == oC_Net_AddressType_IPv6)
        {
            errorCode = oC_ErrorCode_NotImplemented;
        }
        else
        {
            errorCode = oC_ErrorCode_UnknownAddressType;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief reads IP address from the string
 *
 * The function is for reading IP address from a string to a #oC_Net_Address_t structure.
 *
 * @param Address               String with IP address in format IPv4 or IPv6
 * @param outAddress            Destination for the IP address
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Net_AddressFromString( const char * Address , oC_Net_Address_t * outAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Address) , oC_ErrorCode_WrongAddress           )
     && ErrorCondition( isram(outAddress)         , oC_ErrorCode_OutputAddressNotInRAM  )
        )
    {
        uint8_t         bytesArray[16]  = {0};
        oC_Net_Port_t   port            = 0;

        if(
            ErrorCode( sscanf(Address, "%d.%d.%d.%d",    &bytesArray[0], &bytesArray[1], &bytesArray[2], &bytesArray[3]             ) )
         || ErrorCode( sscanf(Address, "%d.%d.%d.%d:%d", &bytesArray[0], &bytesArray[1], &bytesArray[2], &bytesArray[3], &port      ) )
            )
        {
            outAddress->Type    = oC_Net_AddressType_IPv4;
            outAddress->Port    = port;
            outAddress->IPv4    = (bytesArray[0] << 24)
                                | (bytesArray[1] << 16)
                                | (bytesArray[2] <<  8)
                                | (bytesArray[3] <<  0);
            errorCode = oC_ErrorCode_None;
        }
        else if(
            ErrorCode( sscanf(Address, "%x:%x:%x:%x:%x:%x:%x:%x", &bytesArray[0], &bytesArray[1], &bytesArray[2], &bytesArray[3],
                                                                  &bytesArray[4], &bytesArray[5], &bytesArray[6], &bytesArray[7]) )
            )
        {
            outAddress->Type            = oC_Net_AddressType_IPv6;
            outAddress->Port            = port;
            outAddress->IPv6.LowPart    = (((uint64_t)bytesArray[0] ) << 0 )
                                        | (((uint64_t)bytesArray[1] ) << 8 )
                                        | (((uint64_t)bytesArray[2] ) << 16)
                                        | (((uint64_t)bytesArray[3] ) << 24)
                                        | (((uint64_t)bytesArray[4] ) << 32)
                                        | (((uint64_t)bytesArray[5] ) << 40)
                                        | (((uint64_t)bytesArray[6] ) << 48)
                                        | (((uint64_t)bytesArray[7] ) << 56);
            outAddress->IPv6.HighPart   = (((uint64_t)bytesArray[8] ) << 0 )
                                        | (((uint64_t)bytesArray[9] ) << 8 )
                                        | (((uint64_t)bytesArray[10]) << 16)
                                        | (((uint64_t)bytesArray[11]) << 24)
                                        | (((uint64_t)bytesArray[12]) << 32)
                                        | (((uint64_t)bytesArray[13]) << 40)
                                        | (((uint64_t)bytesArray[14]) << 48)
                                        | (((uint64_t)bytesArray[15]) << 56);
            errorCode                   = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_IpAddressNotCorrect;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns true if the given address is correct
 *
 * The function returns true if the IP address is correct
 *
 * @param Address       Pointer to the structure with network IP address
 *
 * @return true if the address is correct
 */
//==========================================================================================================================================
static inline bool oC_Net_IsAddressCorrect( const oC_Net_Address_t * Address )
{
    bool correct = false;

    if(isaddresscorrect(Address))
    {
        if(Address->Type == oC_Net_AddressType_IPv4)
        {
            correct = true;
        }
        else if(Address->Type == oC_Net_AddressType_IPv6)
        {
            correct = true;
        }
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns true if the given address is empty
 *
 * The function returns true if the IP address is empty (0)
 *
 * @param Address       Pointer to the structure with network IP address
 *
 * @return true if the address is empty
 */
//==========================================================================================================================================
static inline bool oC_Net_IsAddressEmpty( const oC_Net_Address_t * Address )
{
    bool empty = true;

    if(isaddresscorrect(Address))
    {
        if(Address->Type == oC_Net_AddressType_IPv4)
        {
            empty = Address->IPv4 == 0;
        }
        else if(Address->Type == oC_Net_AddressType_IPv6)
        {
            empty = Address->IPv6.LowPart == 0 && Address->IPv6.HighPart == 0;
        }
    }

    return empty;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief checks if the IPv4 address belongs to the given subnetwork
 *
 * The function checks if the given IPv4 address belongs to the given subnetwork
 *
 * @param IP           IP to check
 * @param Subnet       Subnetwork IP
 * @param Mask         Subnetwork mask
 *
 * @return true if the IP address belongs to the subnetwork
 */
//==========================================================================================================================================
static inline bool oC_Net_Ipv4_IsAddressInSubnet( oC_Net_Ipv4_t IP , oC_Net_Ipv4_t Subnet , oC_Net_Ipv4_t Mask )
{
    return (IP & Mask) == (Subnet & Mask);
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief checks if the address is in subnet
 *
 * The function checks if the given address belongs to the subnet.
 *
 * @param Address       Address to check
 * @param Subnet        Address of the network
 * @param Mask          network mask
 *
 * @return true if address belongs to the given network
 */
//==========================================================================================================================================
static inline bool oC_Net_IsAddressInSubnet( const oC_Net_Address_t * Address, const oC_Net_Address_t * Subnet , const oC_Net_Address_t * Mask )
{
    bool belong = false;

    if(isaddresscorrect(Address) && isaddresscorrect(Subnet) && isaddresscorrect(Mask) && Address->Type == Subnet->Type)
    {
        if(Address->Type == oC_Net_AddressType_IPv4)
        {
            belong = oC_Net_Ipv4_IsAddressInSubnet(Address->IPv4,Subnet->IPv4,Mask->IPv4);
        }
        else if(Address->Type == oC_Net_AddressType_IPv6)
        {
            oC_SaveError("Net::IsAddressInSubnet - IPv6 version - ", oC_ErrorCode_NotImplemented);
        }
    }

    return belong;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns true if both pointers stores the same address
 *
 * The function compares 2 addresses and returns true if they are the same.
 *
 * @param Address1      Address 1 to compare
 * @param Address2      Address 2 to compare
 *
 * @return true if `Address1` is the same as `Address2`
 */
//==========================================================================================================================================
static inline bool oC_Net_AreAddressesTheSame( const oC_Net_Address_t * Address1 , const oC_Net_Address_t * Address2 )
{
    bool same = false;

    if(isaddresscorrect(Address1) && isaddresscorrect(Address2) && Address1->Type == Address2->Type)
    {
        if(Address1->Type == oC_Net_AddressType_IPv4)
        {
            same = Address1->IPv4 == Address2->IPv4;
        }
        else if(Address1->Type == oC_Net_AddressType_IPv6)
        {
            same = memcmp(&Address1->IPv6,&Address2->IPv6,sizeof(oC_Net_Ipv6_t)) == 0;
        }
    }

    return same;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns maximum size of the packet data
 */
//==========================================================================================================================================
static inline uint16_t oC_Net_GetMaximumPacketDataSize( oC_Net_PacketType_t Type )
{
    return Type == oC_Net_PacketType_IPv4 ? (MAXIMUM_PACKET_SIZE - sizeof(oC_Net_Ipv4PacketHeader_t)) :
           Type == oC_Net_PacketType_IPv6 ? (MAXIMUM_PACKET_SIZE - sizeof(oC_Net_Ipv6PacketHeader_t)) : 0;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns size of the packet
 *
 * The function returns size of the packet (with header size)
 *
 * @param Packet    Pointer to the packet to check
 *
 * @return size that is required to allocate for the packet
 */
//==========================================================================================================================================
static inline uint16_t oC_Net_GetPacketSize( const oC_Net_Packet_t * Packet , bool WithHeader )
{
    uint16_t size = 0;

    if(isaddresscorrect(Packet))
    {
        if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv4)
        {
            size = (uint16_t)Packet->IPv4.Header.Length;

            if( WithHeader == false )
            {
                size -= Packet->IPv4.Header.IHL * sizeof(uint32_t);
            }
        }
        else if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv6)
        {
            size = (uint16_t)Packet->IPv6.Header.PayloadLength + sizeof(oC_Net_Ipv6PacketHeader_t);
            if(WithHeader)
            {
                size += sizeof(oC_Net_Ipv6PacketHeader_t);
            }
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts header to network endianess
 */
//==========================================================================================================================================
static inline bool oC_Net_ConvertHeaderToNetworkEndianess( oC_Net_Packet_t * Packet )
{
    bool success = false;

    if(isaddresscorrect(Packet))
    {
        if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv4)
        {
            success = true;

            uint32_t   ihl        = Packet->IPv4.Header.IHL;

            Packet->IPv4.Header.IHL     = Packet->IPv4.Header.Version;
            Packet->IPv4.Header.Version = ihl;
            Packet->IPv4.Header.Length  = oC_Net_ConvertUint16ToNetworkEndianess(Packet->IPv4.Header.Length);

            Packet->IPv4.Header.SourceIp        = oC_Net_ConvertUint32FromNetworkEndianess(Packet->IPv4.Header.SourceIp);
            Packet->IPv4.Header.DestinationIp   = oC_Net_ConvertUint32FromNetworkEndianess(Packet->IPv4.Header.DestinationIp);

            uint16_t * word = (void*)Packet;

            word[3] = oC_Net_ConvertUint16FromNetworkEndianess(word[3]); /* Offset & Flags */
        }
        else if(Packet->IPv6.Header.Version == oC_Net_PacketType_IPv6)
        {
            kdebuglog(oC_LogType_Error, "oC_Net_ConvertHeaderToNetworkEndianess: not implemented for IPv6 yet\n");
        }
        else
        {
            kdebuglog(oC_LogType_Error, "oC_Net_ConvertHeaderToNetworkEndianess: incorrect packet type %d\n", Packet->IPv6.Header.Version);
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief converts header from network endianess
 */
//==========================================================================================================================================
static inline bool oC_Net_ConvertHeaderFromNetworkEndianess( oC_Net_Packet_t * Packet )
{
    bool success = false;

    if(isaddresscorrect(Packet))
    {
        /* When header is not converted, IHL stores version of the header */
        if(Packet->IPv4.Header.IHL == oC_Net_PacketType_IPv4)
        {
            success = true;

            /* When header is not converted Version stores IHL of the header */
            uint32_t   ihl        = Packet->IPv4.Header.Version;

            Packet->IPv4.Header.Version         = Packet->IPv4.Header.IHL;
            Packet->IPv4.Header.IHL             = ihl;
            Packet->IPv4.Header.Length          = oC_Net_ConvertUint16ToNetworkEndianess(Packet->IPv4.Header.Length);

            Packet->IPv4.Header.SourceIp        = oC_Net_ConvertUint32FromNetworkEndianess(Packet->IPv4.Header.SourceIp);
            Packet->IPv4.Header.DestinationIp   = oC_Net_ConvertUint32FromNetworkEndianess(Packet->IPv4.Header.DestinationIp);

            uint16_t * word = (void*)Packet;

            word[3] = oC_Net_ConvertUint16FromNetworkEndianess(word[3]); /* Offset & Flags */
        }
        /* When header is not converted, IHL stores version of the header */
        else if(Packet->IPv4.Header.IHL == oC_Net_PacketType_IPv6)
        {
            kdebuglog(oC_LogType_Track, "not implemented for IPv6 yet\n");
        }
        else
        {
            kdebuglog(oC_LogType_Error, "oC_Net_ConvertHeaderFromNetworkEndianess: incorrect packet type %d Version: %d QoS: %d Length: %d\n"
                      , Packet->IPv4.Header.IHL
                      , Packet->IPv4.Header.Version
                      , Packet->IPv4.Header.QoS
                      , Packet->IPv4.Header.Length
                      );
            kdebuglog(oC_LogType_Error, "oC_Net_ConvertHeaderFromNetworkEndianess: 0x%08X\n", *(uint32_t*)Packet);
        }
    }

    return success;
}



//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns size of the hardware address (in bytes)
 */
//==========================================================================================================================================
static inline oC_MemorySize_t oC_Net_GetHardwareAddressSize( oC_Net_HardwareType_t Type )
{
    oC_MemorySize_t size = 0;

    switch(Type)
    {
        case oC_Net_HardwareType_Ethernet: size = sizeof(oC_Net_MacAddress_t); break;
        default: size = 0; break;
    }

    return size;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns size of IP address
 */
//==========================================================================================================================================
static inline oC_MemorySize_t oC_Net_GetAddressSize( const oC_Net_Address_t * Address )
{
    oC_MemorySize_t size = 0;

    if(isaddresscorrect(Address))
    {
        switch(Address->Type)
        {
            case oC_Net_AddressType_IPv4: size = sizeof(oC_Net_Ipv4_t); break;
            case oC_Net_AddressType_IPv6: size = sizeof(oC_Net_Ipv6_t); break;
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns name of the hardware type
 */
//==========================================================================================================================================
static inline const char * oC_Net_GetHardwareTypeName( oC_Net_HardwareType_t Type )
{
    const char * name = "unknown";

    switch(Type)
    {
        case oC_Net_HardwareType_Ethernet: name = "ethernet"; break;
        default: name = "unhandled in oC_Net_GetHardwareTypeName";
    }

    return name;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief reads network layer name
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Net_ReadLayerName( oC_Net_Layer_t Layer , char * outName, oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(isram(outName), oC_ErrorCode_OutputAddressNotInRAM))
    {
        oC_Net_Layer_t mainLayer = Layer & oC_Net_Layer_MainLayerMask;
        oC_Net_Layer_t subLayer  = Layer & oC_Net_Layer_SubLayerMask;

        const char * mainLayerName = "unknownMainLayer";

        switch(mainLayer)
        {
            default:    break;
            case oC_Net_Layer_Physical    : mainLayerName = "Physical";     break;
            case oC_Net_Layer_DataLink    : mainLayerName = "DataLink";     break;
            case oC_Net_Layer_Network     : mainLayerName = "Network";      break;
            case oC_Net_Layer_Transport   : mainLayerName = "Transport";    break;
            case oC_Net_Layer_Session     : mainLayerName = "Session";      break;
            case oC_Net_Layer_Presentation: mainLayerName = "Presentation"; break;
            case oC_Net_Layer_Application : mainLayerName = "Application";  break;
        }

        errorCode = sprintf_s(outName,Size,"%s:%d", mainLayerName, subLayer);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns network OSI layer according to its name
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Net_ReadLayerByName( oC_Net_Layer_t * outLayer, const char * Name )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outLayer)         , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( isaddresscorrect(Name)  , oC_ErrorCode_WrongAddress          )
        )
    {
        uint32_t     subLayer = 0;
        static const struct
        {
                const char *    Name;
                oC_Net_Layer_t  Layer;
        } layerStrings[] = {
                        { .Name = "PHY"          , .Layer = oC_Net_Layer_PHY          } ,
                        { .Name = "LLC"          , .Layer = oC_Net_Layer_LLC          } ,
                        { .Name = "MAC"          , .Layer = oC_Net_Layer_MAC          } ,
                        { .Name = "Netif"        , .Layer = oC_Net_Layer_Netif        } ,
                        { .Name = "Physical"     , .Layer = oC_Net_Layer_Physical     } ,
                        { .Name = "DataLink"     , .Layer = oC_Net_Layer_DataLink     } ,
                        { .Name = "Network"      , .Layer = oC_Net_Layer_Network      } ,
                        { .Name = "Transport"    , .Layer = oC_Net_Layer_Transport    } ,
                        { .Name = "Session"      , .Layer = oC_Net_Layer_Session      } ,
                        { .Name = "Presentation" , .Layer = oC_Net_Layer_Presentation } ,
                        { .Name = "Application"  , .Layer = oC_Net_Layer_Application  } ,
        };


        errorCode = oC_ErrorCode_UnknownLayer;

        oC_ARRAY_FOREACH_IN_ARRAY(layerStrings,layer)
        {
            if(string_contains(Name,layer->Name,false))
            {
                *outLayer = layer->Layer;
                errorCode = oC_ErrorCode_None;
            }
        }

        if(
            string_contains(Name,":",true)
         && ErrorCode(sscanf(Name,"%*s:%u", &subLayer))
            )
        {
            *outLayer |= subLayer;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 *
 * @brief returns true if Layer is part of the `MainLayer`
 */
//==========================================================================================================================================
static inline bool oC_Net_IsMainLayer( oC_Net_Layer_t Layer , oC_Net_Layer_t MainLayer )
{
    return (Layer & oC_Net_Layer_MainLayerMask) == MainLayer;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 *
 * @brief returns true if the given packet type is valid
 */
//==========================================================================================================================================
static inline bool oC_Net_IsPacketTypeValid( oC_Net_PacketType_t packetType )
{
    return packetType == oC_Net_PacketType_IPv4 || packetType == oC_Net_PacketType_IPv6;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns type of the packet
 *
 * The function returns type of the packet.
 *
 * @param Packet    Pointer to the packet to check
 *
 * @return type of the given packet
 */
//==========================================================================================================================================
static inline oC_Net_PacketType_t oC_Net_Packet_GetType( const oC_Net_Packet_t * Packet )
{
    oC_Net_PacketType_t type = oC_Net_PacketType_Invalid;

    if(isaddresscorrect(Packet))
    {
        type = (oC_Net_PacketType_t)Packet->IPv4.Header.Version;
    }

    return type;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 *
 * @brief returns true if the given packet type is valid
 */
//==========================================================================================================================================
static inline bool oC_Net_Packet_IsTypeValid( const oC_Net_Packet_t* Packet )
{
    return isaddresscorrect(Packet) && oC_Net_IsPacketTypeValid((oC_Net_PacketType_t)Packet->IPv4.Header.Version);
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief sets size of the packet
 */
//==========================================================================================================================================
static inline bool oC_Net_Packet_SetSize( oC_Net_Packet_t * Packet , uint16_t Size )
{
    bool success = false;

    if(isram(Packet) && Size > 0 && Size <= oC_Net_GetMaximumPacketDataSize(oC_Net_Packet_GetType(Packet)))
    {
        if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv4)
        {
            Packet->IPv4.Header.Length = Size + (Packet->IPv4.Header.IHL * sizeof(uint32_t));
            success                    = true;
        }
        else if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv6)
        {

            kdebuglog(oC_LogType_Error, "oC_Net_SetPacketSize:: IPv6 is not ready yet\n", Packet->IPv4.Header.Version);
        }
        else
        {
            kdebuglog(oC_LogType_Error, "oC_Net_SetPacketSize:: incorrect packet version: %d\n", Packet->IPv4.Header.Version);
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief reads source address from the packet
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t oC_Net_Packet_ReadSourceAddress( const oC_Net_Packet_t* Packet, oC_Net_Address_t* outSourceAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( isaddresscorrect(Packet)            , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isram(outSourceAddress)             , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( oC_Net_Packet_IsTypeValid(Packet)   , oC_ErrorCode_PacketNotCorrect      )
        )
    {
        outSourceAddress->Type = Packet->IPv4.Header.Version;
        switch(Packet->IPv4.Header.Version){
            case oC_Net_PacketType_IPv4:
                outSourceAddress->Protocol = Packet->IPv4.Header.Protocol;
                outSourceAddress->IPv4 = Packet->IPv4.Header.SourceIp;
                break;
            case oC_Net_PacketType_IPv6:
                memcpy(&outSourceAddress->IPv6, &Packet->IPv6.Header.Source, sizeof(outSourceAddress->IPv6));
                break;
            default:
                break;
        }
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief checks if source address is set to the given one
 */
//==========================================================================================================================================
static inline bool oC_Net_Packet_IsSourceAddress( const oC_Net_Packet_t * Packet, const oC_Net_Address_t* SourceAddress)
{
    bool addressTheSame = false;
    oC_Net_Address_t packetSourceAddress;
    if (
            isaddresscorrect(Packet)
         && isaddresscorrect(SourceAddress)
         && oC_SaveIfErrorOccur("Cannot read source address", oC_Net_Packet_ReadSourceAddress(Packet, &packetSourceAddress) )
         )
    {
        addressTheSame = oC_Net_AreAddressesTheSame(&packetSourceAddress, SourceAddress);
    }
    return addressTheSame;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief returns reference to the payload in the packet
 */
//==========================================================================================================================================
static inline void * oC_Net_Packet_GetDataReference( oC_Net_Packet_t * Packet )
{
    void * data = NULL;

    if(isaddresscorrect(Packet))
    {
        oC_Net_PacketType_t type = oC_Net_Packet_GetType(Packet);

        if(type == oC_Net_PacketType_IPv4)
        {
            data = Packet->IPv4.Data;
        }
        else if(type == oC_Net_PacketType_IPv6)
        {
            data = Packet->IPv6.Data;
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief sets addresses in the packet
 */
//==========================================================================================================================================
static inline bool oC_Net_Packet_SetAddress( oC_Net_Packet_t * Packet, const oC_Net_Address_t * Source, const oC_Net_Address_t * Destination )
{
    bool success = false;
    if(isram(Packet))
    {
        if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv4)
        {
            success = true;
            Packet->IPv4.Header.Protocol = Source->Protocol;

            if(isaddresscorrect(Source) && Source->Type == oC_Net_AddressType_IPv4)
            {
                Packet->IPv4.Header.SourceIp = Source->IPv4;
            }
            else
            {
                success = false;
            }
            if(isaddresscorrect(Destination) && Destination->Type == oC_Net_AddressType_IPv4)
            {
                Packet->IPv4.Header.DestinationIp = Destination->IPv4;
            }
            else
            {
                success = false;
            }
        }
        else if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv6)
        {
            success = true;
            if(isaddresscorrect(Source) && Source->Type == oC_Net_AddressType_IPv6)
            {
                Packet->IPv6.Header.Source.HighPart = Source->IPv6.HighPart;
                Packet->IPv6.Header.Source.LowPart  = Source->IPv6.LowPart;
            }
            else
            {
                success = false;
            }
            if(isaddresscorrect(Destination) && Destination->Type == oC_Net_AddressType_IPv6)
            {
                Packet->IPv6.Header.Destination.HighPart = Destination->IPv6.HighPart;
                Packet->IPv6.Header.Destination.LowPart  = Destination->IPv6.LowPart;
            }
            else
            {
                success = false;
            }
        }
    }
    return success;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief sets payload in the packet
 */
//==========================================================================================================================================
static inline void oC_Net_Packet_SetData( oC_Net_Packet_t * Packet , const void * Data , uint16_t Size )
{
    oC_Net_PacketType_t type = oC_Net_Packet_GetType(Packet);

    if(isram(Packet) && isaddresscorrect(Data) && Size > 0 && Size <= oC_Net_GetMaximumPacketDataSize(type))
    {
        if(type == oC_Net_PacketType_IPv4)
        {
            oC_Net_Packet_SetSize(Packet,Size);
            memcpy( Packet->IPv4.Data, Data, Size );
        }
        else if(type == oC_Net_PacketType_IPv6)
        {
            Packet->IPv6.Header.PayloadLength = Size;
            memcpy( Packet->IPv6.Data, Data, Size );
        }

    }
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief allocates memory for a packet
 */
//==========================================================================================================================================
static inline oC_Net_Packet_t* oC_Net_Packet_New( Allocator_t Allocator , oC_Net_PacketType_t Type , void * Data , uint16_t Size )
{
    oC_Net_Packet_t * packet = NULL;

    if(
        oC_SaveIfFalse( "Net::New-Packet - Allocator is not correct - "    , isaddresscorrect(Allocator)                    , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse( "Net::New-Packet - Type is not correct - "         ,    Type == oC_Net_PacketType_IPv4
                                                                             || Type == oC_Net_PacketType_IPv6              , oC_ErrorCode_TypeNotCorrect   )
     && oC_SaveIfFalse( "Net::New-Packet - Size is not correct - "         , Size > 0                                       , oC_ErrorCode_SizeNotCorrect   )
     && oC_SaveIfFalse( "Net::New-Packet - Size is not correct - "         , Size <= oC_Net_GetMaximumPacketDataSize(Type)  , oC_ErrorCode_SizeTooBig       )
     && oC_SaveIfFalse( "Net::New-Packet - Data address is not correct - " , Data == NULL || isaddresscorrect(Data)         , oC_ErrorCode_SizeNotCorrect   )
        )
    {
        uint16_t headerSize = Type == oC_Net_PacketType_IPv4 ? sizeof(oC_Net_Ipv4PacketHeader_t) :
                              Type == oC_Net_PacketType_IPv6 ? sizeof(oC_Net_Ipv6PacketHeader_t) : 0;
        uint16_t packetSize = oC_MAX(headerSize + Size, sizeof(oC_Net_Packet_t));

        if(oC_SaveIfFalse("Net::New-Packet - Fatal error - ", packetSize > Size , oC_ErrorCode_SizeTooBig))
        {
            packet = kmalloc( packetSize , Allocator, AllocationFlags_ZeroFill );

            if(oC_SaveIfFalse("Net::New-Packet - cannot allocate memory for a packet - ", packet != NULL , oC_ErrorCode_AllocationError ))
            {
                packet->IPv4.Header.Version = Type;

                if(Type == oC_Net_PacketType_IPv4)
                {
                    packet->IPv4.Header.IHL = 5;
                }

                oC_Net_Packet_SetSize(packet,Size);

                if(Data != NULL)
                {
                    oC_Net_Packet_SetData(packet,Data,Size);
                }
            }

        }
    }

    return packet;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief releases memory allocated for a packet
 */
//==========================================================================================================================================
static inline bool oC_Net_Packet_Delete( oC_Net_Packet_t ** Packet )
{
    bool success = false;

    if(isram(Packet) && isram(*Packet))
    {
        success = kfree(*Packet,AllocationFlags_Default);
        *Packet = NULL;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 *
 * Calculates network checksum for a buffer
 */
//==========================================================================================================================================
static inline uint16_t oC_Net_CalculateChecksum( const void * Buffer , uint16_t Size , uint16_t InitialValue , bool ConvertEndianess , bool PrepareChecksumToSend )
{
    uint32_t checksum = InitialValue;

    if(isaddresscorrect(Buffer) && Size > 0)
    {
        const uint16_t * array = Buffer;
        uint16_t         count = Size / sizeof(uint16_t);

        for(uint16_t i = 0 ; i < count ; i++)
        {
            uint16_t valueInNetworkEndianess = ConvertEndianess ? oC_Net_ConvertUint16ToNetworkEndianess( array[i] ) : array[i];

            checksum += valueInNetworkEndianess;

            while(checksum >> 16)
            {
                checksum = (checksum & 0xFFFF) + (checksum >> 16);
            }
        }

        if(Size % sizeof(uint16_t))
        {
            if(ConvertEndianess)
            {
                checksum += (((uint8_t*)Buffer)[Size-1]) << 8;
            }
            else
            {
                checksum += ((uint8_t*)Buffer)[Size-1];
            }
        }

        while(checksum >> 16)
        {
            checksum = (checksum & 0xFFFF) + (checksum >> 16);
        }

        if(PrepareChecksumToSend)
        {
            checksum = (~checksum) & 0xFFFF;

            if(checksum == 0)
            {
                checksum = 0xFFFF;
            }
        }
    }

    return (uint16_t)checksum;
}

//==========================================================================================================================================
/**
 * @ingroup NET
 * @brief calculates checksum for a packet (it only calculates it and does not fill it!)
 */
//==========================================================================================================================================
static inline uint16_t oC_Net_Packet_CalculateChecksum( oC_Net_Packet_t * Packet )
{
    uint16_t checksum = 0;

    if(isaddresscorrect(Packet))
    {
        oC_Net_PacketType_t type = oC_Net_Packet_GetType(Packet);

        if(type == oC_Net_PacketType_IPv4)
        {
            uint16_t storedChecksum = Packet->IPv4.Header.Checksum;
            uint16_t headerSize     = Packet->IPv4.Header.IHL * sizeof(uint32_t);
            Packet->IPv4.Header.Checksum = 0;

            oC_Net_ConvertHeaderToNetworkEndianess(Packet);
            checksum = oC_Net_CalculateChecksum(Packet, headerSize, 0, false, true);
            oC_Net_ConvertHeaderFromNetworkEndianess(Packet);

            Packet->IPv4.Header.Checksum = storedChecksum;

        }
    }

    return checksum;
}

#undef  _________________________________________INLINE_SECTION_____________________________________________________________________________


#endif /* SYSTEM_CORE_INC_NET_OC_NET_H_ */
