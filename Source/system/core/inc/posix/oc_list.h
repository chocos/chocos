/** ****************************************************************************************************************************************
 *
 * @file       oc_list.h
 *
 * @brief      The file with list library
 *
 * @author     Patryk Kubiak - (Created on: 3 09 2015 18:16:31)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * @defgroup List List
 * @ingroup CoreSpace
 * @brief The module for handling lists
 * 
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_LIST_H_
#define SYSTEM_LIBRARIES_INC_OC_LIST_H_

#include <oc_object.h>
#include <oc_stdlib.h>
#include <oc_system.h>
#include <oc_assert.h>

#if !defined(oC_CORE_SPACE) && !defined(oC_USER_SPACE) && !defined(oC_LIBRARIES_SPACE)
#   error   List library can be used only in CORE , USER or LIBRARIES space!
#endif

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________


//==========================================================================================================================================
/**
 * @brief callback to call whenever an element is deleted
 *
 * @param Object            Pointer to the deleted object
 */
//==========================================================================================================================================
typedef void (*oC_List_DestructorCallback_t)( void* Object );

//==========================================================================================================================================
//==========================================================================================================================================
struct List_ElementHandle_t
{
    struct List_ElementHandle_t *   Previous;
    struct List_ElementHandle_t *   Next;
    void *                          Object;
    oC_UInt_t                       Size;
};

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct List_ElementHandle_t * oC_List_ElementHandle_t;

//==========================================================================================================================================
//==========================================================================================================================================
struct List_t
{
    oC_ObjectControl_t              ObjectControl;
    uint32_t                        Count;
    oC_List_ElementHandle_t         First;
    oC_List_ElementHandle_t         Last;
    oC_List_DestructorCallback_t    DestructorCallback;
};

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct List_t * oC_List_t;


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief      The macro for definition of the type of the list
*/
//==========================================================================================================================================
#define oC_List(Type)                       Type*

//==========================================================================================================================================
/**
 * @brief     The macro verifies that the given variable is in correct type
 * 
 * @param     List        The list
 * @param     Variable    The variable to verify
 * 
 * The static assert is used to verify that the given variable is in correct type
 */
//==========================================================================================================================================
#define oC_List_TypeAssert( List , Variable )                          oC_STATIC_ASSERT( sizeof(__typeof__(*List)) == sizeof(__typeof__(Variable)) , "Object " #Variable " is not in correct type" );

#ifdef oC_CORE_SPACE
//==========================================================================================================================================
/**
 * @brief     The macro for creation of the list
 * 
 * @param     Allocator    The allocator
 * @param     Flags        The allocation flags
 * 
 * @return    The pointer to the list
 * 
 * The macro creates the list in the given allocator with the given flags
 */
//==========================================================================================================================================
#define oC_List_New(...)                (void*)List_New( __VA_ARGS__ , 0 )
//==========================================================================================================================================
/**
 * @brief     The macro for deletion of the list
*/
//==========================================================================================================================================
#define oC_List_Delete(List,...)          List_Delete((oC_List_t*)(&(List)), 0 )
#else
//==========================================================================================================================================
/**
 * @brief     The macro for creation of the list
 * 
 * @return    The pointer to the list
*/
//==========================================================================================================================================
#define oC_List_New()                       List_New( getcurallocator() , oC_MemMan_AllocationFlags_CanWaitForever )
//==========================================================================================================================================
/**
 * @brief    The macro for deletion of the list
 */
//==========================================================================================================================================
#define oC_List_Delete(List)                List_Delete((oC_List_t*)&List , oC_MemMan_AllocationFlags_CanWaitForever)
#endif

//==========================================================================================================================================
/**
 * @brief    The macro verifies that the given list is correct
 */
//==========================================================================================================================================
#define oC_List_IsCorrect(List)                         List_IsCorrect((oC_List_t)List)

//==========================================================================================================================================
/**
 * @brief    The macro checks that the given list is empty
 */
//==========================================================================================================================================
#define oC_List_IsEmpty(List)                           (oC_List_Count((oC_List_t)List) == 0)

//==========================================================================================================================================
/**
 * @brief    The macro clears the given list
 */
//==========================================================================================================================================
#define oC_List_Clear(List)                             List_Clear((oC_List_t)List)

//==========================================================================================================================================
/**
 * @brief    The macro counts the number of elements in the given list
 */
//==========================================================================================================================================
#define oC_List_Count(List)                             List_Count((oC_List_t)List)

//==========================================================================================================================================
/**
 * @brief checks if the list contains a given variable
 */
//==========================================================================================================================================
#define oC_List_Contains(List,Variable)                 List_Contains((oC_List_t)List,&(Variable),sizeof(Variable));\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_CountObjects(List,Variable)             List_CountObjects((oC_List_t)List,&(Variable),sizeof(Variable));\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_IndexOf(List,Variable)                  List_IndexOf((oC_List_t)List,&(Variable),sizeof(Variable));\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_ElementHandleOf(List,Variable)          List_ElementHandleOf((oC_List_t)List,&(Variable),sizeof(Variable));\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_PushBack(List,Variable,Allocator)       List_PushBack((oC_List_t)List,&(Variable),sizeof(Variable),Allocator);\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_PushFront(List,Variable,Allocator)      List_PushFront((oC_List_t)List,&(Variable),sizeof(Variable),Allocator);\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_PushCopyBack(List,Variable,Allocator)   List_PushBack((oC_List_t)List,&(Variable),sizeof(Variable),Allocator);\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_PushCopyFront(List,Variable,Allocator)  List_PushFront((oC_List_t)List,&(Variable),sizeof(Variable),Allocator);\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_InsertAfter(List,Variable,Index,Allocator)  List_InsertAfter((oC_List_t)List,&(Variable),sizeof(Variable),List_ElementOfIndex((oC_List_t)List,Index),Allocator);\
                                                            oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_InsertBefore(List,Variable,Index,Allocator) List_InsertBefore((oC_List_t)List,&(Variable),sizeof(Variable),List_ElementOfIndex((oC_List_t)List,Index),Allocator);\
                                                            oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_InsertCopyAfter(List,Variable,Index,Allocator)    List_InsertAfter((oC_List_t)List,&(Variable),sizeof(Variable),List_ElementOfIndex((oC_List_t)List,Index),Allocator);\
                                                                  oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_InsertCopyBefore(List,Variable,Index,Allocator)     List_InsertBefore((oC_List_t)List,$(Variable),sizeof(Variable),List_ElementOfIndex((oC_List_t)List,Index),Allocator);\
                                                                    oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_Replace(List,Variable,Index,Allocator)  List_Replace((oC_List_t)List,&(Variable),sizeof(Variable),List_ElementOfIndex(List,Index),Allocator);\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_ReplaceByCopy(List,Variable,Index,Allocator)      List_Replace((oC_List_t)List,&(Variable),sizeof(Variable),List_ElementOfIndex((oC_List_t)List,Index),Allocator);\
                                                                oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_Remove(List,Index)                      List_Remove((oC_List_t)List,List_ElementOfIndex((oC_List_t)List,Index))

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_RemoveAll(List,Variable)                List_RemoveAll((oC_List_t)List,&(Variable),sizeof(Variable));\
                                                        oC_List_TypeAssert(List,Variable)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_At(List,Index,Output)                   List_At((oC_List_t)List,List_ElementOfIndex((oC_List_t)List,Index),&(Output),sizeof(Output));\
                                                        oC_List_TypeAssert(List,Output)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_AtWithDefault(List,Index,Default,Output)    List_AtWithDefault((oC_List_t)List,&Default,sizeof(Default),List_ElementOfIndex((oC_List_t)List,Index),&(Output),sizeof(Output));\
                                                            oC_List_TypeAssert(List,Default);\
                                                            oC_List_TypeAssert(List,Output)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_Move(List,From,To,Allocator)            List_Move((oC_List_t)List,List_ElementOfIndex((oC_List_t)List,From),List_ElementOfIndex((oC_List_t)List,To),Allocator)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_PopBack(List)                           List_PopBack((oC_List_t)List)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_PopFront(List)                          List_PopBack((oC_List_t)List)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_Swap(List,I,J)                          List_Swap((oC_List_t)List,List_ElementHandleOf((oC_List_t)List,&I,sizeof(I)),List_ElementHandleOf((oC_List_t)List,&J,sizeof(J)))

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_TakeAt(List,Index,Output)               List_TakeAt((oC_List_t)List,List_ElementOfIndex((oC_List_t)List,Index),&(Output),sizeof(Output));\
                                                        oC_List_TypeAssert(List,Output)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_Take(List,Variable,Output)              List_TakeAt((oC_List_t)List,List_ElementHandleOf((oC_List_t)List,&(Variable),sizeof(Variable)),&(Output),sizeof(Output));\
                                                        oC_List_TypeAssert(List,Variable);\
                                                        oC_List_TypeAssert(List,Output)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_TakeFirst(List,Output)                  List_TakeFirst((oC_List_t)List,&(Output),sizeof(Output));\
                                                        oC_List_TypeAssert(List,Output)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_TakeLast(List,Output)                   List_TakeLast((oC_List_t)List,&(Output),sizeof(Output));\
                                                        oC_List_TypeAssert(List,Output)

#define oC_List_Verify(List)                            List_Verify((oC_List_t)List)

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_AttachDestructor(List,Destructor)       List_AttachDestructor((oC_List_t)List, (oC_List_DestructorCallback_t)Destructor)

//==========================================================================================================================================
//==========================================================================================================================================
#define List_Foreach_ElementHandle( ElementName )           ElementName##Handle

//==========================================================================================================================================
//==========================================================================================================================================
#define List_Foreach(List,ElementName)      if(List_IsCorrect((oC_List_t)List) && List_Count((oC_List_t)List)) \
                                            for(bool ElementName##Execute=true;ElementName##Execute;ElementName##Execute=false)\
                                            for(oC_List_ElementHandle_t List_Foreach_ElementHandle(ElementName) = ((oC_List_t)List)->First ; (List_Foreach_ElementHandle(ElementName) != NULL) && ElementName##Execute ; ElementName##Execute = false ) \
                                            for(__typeof__(*List) ElementName = *((__typeof__(List))List_Foreach_ElementHandle(ElementName)->Object);(List_Foreach_ElementHandle(ElementName) != NULL) && (ElementName != NULL);ElementName = NULL , List_Foreach_ElementHandle(ElementName) = List_Foreach_ElementHandle(ElementName)->Next , ElementName =(List_Foreach_ElementHandle(ElementName) == NULL) ? NULL : *((__typeof__(List))List_Foreach_ElementHandle(ElementName)->Object))

//==========================================================================================================================================
//==========================================================================================================================================
#define oC_List_Foreach(List,VariableName)  List_Foreach(List,VariableName)

#define oC_List_GetFirstElementHandle(List)                     ((oC_List_IsCorrect(List)) ? ((oC_List_t)List)->First : NULL)
#define oC_List_GetLastElementHandle(List)                      ((oC_List_IsCorrect(List)) ? ((oC_List_t)List)->Last  : NULL)
#define oC_List_ElementHandle_Value(ElementHandle,Type)         (*((Type*)((ElementHandle)->Object)))
#define oC_List_ElementHandle_Next(ElementHandle)               ( (ElementHandle != NULL) ? (ElementHandle)->Next     : NULL )
#define oC_List_ElementHandle_Previous(ElementHandle)           ( (ElementHandle != NULL) ? (ElementHandle)->Previous : NULL )

#define oC_List_RemoveCurrentElement(List,VariableName)         \
                        { \
                            oC_List_ElementHandle_t current = List_Foreach_ElementHandle(VariableName);\
                            List_Remove((oC_List_t)List,current);\
                            continue;\
                        }

#ifndef foreach
#define foreach(list,variable)              List_Foreach(list,variable)
#endif

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern oC_List_t                List_New                ( Allocator_t Allocator , AllocationFlags_t Flags , ... );
extern bool                     List_IsCorrect          ( oC_List_t List );
extern bool                     List_Delete             ( oC_List_t * List , AllocationFlags_t Flags );
extern bool                     List_DeleteElement      ( oC_List_t List, oC_List_ElementHandle_t Element );
extern oC_List_ElementHandle_t  List_NewElement         ( const void * Object , oC_UInt_t Size , Allocator_t Allocator);
extern bool                     List_AttachDestructor   ( oC_List_t List, oC_List_DestructorCallback_t Destructor );
extern bool                     List_Clear              ( oC_List_t List );
extern uint32_t                 List_Count              ( oC_List_t List );
extern bool                     List_Contains           ( oC_List_t List , void * Object , oC_UInt_t Size );
extern uint32_t                 List_CountObjects       ( oC_List_t List , void * Object , oC_UInt_t Size );
extern int                      List_IndexOf            ( oC_List_t List , void * Object , oC_UInt_t Size );
extern int                      List_IndexOfElement     ( oC_List_t List , oC_List_ElementHandle_t ElementHandle );
extern oC_List_ElementHandle_t  List_ElementHandleOf    ( oC_List_t List , void * Object , oC_UInt_t Size );
extern oC_List_ElementHandle_t  List_ElementOfIndex     ( oC_List_t List , int Index );
extern bool                     List_PushBack           ( oC_List_t List , const void * Object , oC_UInt_t Size , Allocator_t Allocator);
extern bool                     List_PushFront          ( oC_List_t List , const void * Object , oC_UInt_t Size , Allocator_t Allocator);
extern bool                     List_InsertAfter        ( oC_List_t List , void * Object , oC_UInt_t Size , oC_List_ElementHandle_t ElementHandle , Allocator_t Allocator);
extern bool                     List_InsertBefore       ( oC_List_t List , void * Object , oC_UInt_t Size , oC_List_ElementHandle_t ElementHandle , Allocator_t Allocator);
extern bool                     List_Replace            ( oC_List_t List , void * Object , oC_UInt_t Size , oC_List_ElementHandle_t ElementHandle , Allocator_t Allocator , AllocationFlags_t Flags );
extern bool                     List_Remove             ( oC_List_t List , oC_List_ElementHandle_t ElementHandle );
extern uint32_t                 List_RemoveAll          ( oC_List_t List , void * Object , oC_UInt_t Size );
extern void *                   List_At                 ( oC_List_t List , oC_List_ElementHandle_t ElementHandle , void * outObject , oC_UInt_t Size );
extern void *                   List_AtWithDefault      ( oC_List_t List , void * DefaultObject , oC_UInt_t DefaultSize , oC_List_ElementHandle_t ElementHandle , void * outObject , oC_UInt_t Size );
extern bool                     List_Move               ( oC_List_t List , oC_List_ElementHandle_t From , oC_List_ElementHandle_t To , Allocator_t Allocator );
extern bool                     List_PopBack            ( oC_List_t List );
extern bool                     List_PopFront           ( oC_List_t List );
extern bool                     List_Swap               ( oC_List_t List , oC_List_ElementHandle_t I , oC_List_ElementHandle_t J );
extern bool                     List_TakeAt             ( oC_List_t List , oC_List_ElementHandle_t ElementHandle , void * outObject , oC_UInt_t Size );
extern bool                     List_TakeFirst          ( oC_List_t List , void * outObject , oC_UInt_t Size );
extern bool                     List_TakeLast           ( oC_List_t List , void * outObject , oC_UInt_t Size );
extern bool                     List_Verify             ( oC_List_t List );
extern bool                     List_Repair             ( oC_List_t List , void * SavedListAddress , uint32_t MaximumNumberOfObjects );
extern bool                     List_Erase              ( oC_List_t List , oC_List_ElementHandle_t ElementHandle );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with static inline functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________


#endif /* SYSTEM_LIBRARIES_INC_OC_LIST_H_ */
