/** ****************************************************************************************************************************************
 *
 * @file       stdlib.h
 *
 * @brief      Standard library
 *
 * @author     Patryk Kubiak - (Created on: 31 08 2015 19:14:26)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_STDLIB_H_
#define SYSTEM_LIBRARIES_INC_OC_STDLIB_H_

#include <oc_stdtypes.h>
#include <stdbool.h>
#include <oc_memory.h>
#include <oc_compiler.h>
#include <oc_posix_ptrs.h>

#ifdef oC_CORE_SPACE

#define malloc_array(type,array_size,flags)     _malloc(oC_FUNCTION, __LINE__,AllocationFlags_ReleaseOnlyInCore,sizeof(type)*array_size,flags)

#define malloc(...)                             _malloc(oC_FUNCTION, __LINE__,AllocationFlags_ReleaseOnlyInCore, __VA_ARGS__, 0)
#define free(...)                               _free(AllocationFlags_ReleaseOnlyInCore,__VA_ARGS__, 0)

#define rawmalloc(size,flags)                   _rawmalloc(size,oC_FUNCTION, __LINE__,flags | AllocationFlags_ReleaseOnlyInCore)
#define rawfree(address,size)                   _rawfree(address,size)

#define smartalloc(size,flags)                  _smartalloc(size,oC_FUNCTION, __LINE__,flags | AllocationFlags_ReleaseOnlyInCore)
#define smartfree(address,size,flags)           _smartfree(address,size,flags)

#define kmalloc(size,allocator,flags)           _kmalloc(size,allocator,oC_FUNCTION, __LINE__,flags | AllocationFlags_ReleaseOnlyInCore,0)
#define kfree(address,flags)                    _kfree(address,flags)

#define kamalloc(size,allocator,flags,alignment)    _kmalloc(size,allocator,oC_FUNCTION, __LINE__,flags | AllocationFlags_ReleaseOnlyInCore,alignment)
#define kafree(address,flags)                       _kfree(address,flags)

#define krawmalloc(map,size,flags)              _krawmalloc(map,size,oC_FUNCTION, __LINE__,flags | AllocationFlags_ReleaseOnlyInCore)
#define krawfree(map,address,size)              _krawfree(map,address,size)

#define ksmartalloc(size,allocator,flags)       _ksmartalloc(size,allocator,oC_FUNCTION, __LINE__,flags | AllocationFlags_ReleaseOnlyInCore)
#define ksmartfree(address,size,flags)          _ksmartfree(address,size,flags | AllocationFlags_ReleaseOnlyInCore)

#elif defined(oC_USER_SPACE)

#define malloc_array(type,array_size,flags)     _malloc(oC_FUNCTION, __LINE__,0,sizeof(type)*array_size,flags)

#define malloc(...)                             _malloc(oC_FUNCTION, __LINE__,0, __VA_ARGS__, 0)
#define free(...)                               _free(0,__VA_ARGS__)

#define rawmalloc(size,flags)                   _rawmalloc(size,oC_FUNCTION, __LINE__,flags )
#define rawfree(address,size)                   _rawfree(address,size)

#define smartalloc(size,flags)                  _smartalloc(size,oC_FUNCTION, __LINE__,flags )
#define smartfree(address,size,flags)           _smartfree(address,size,flags)

#endif

#define isarrayinram(array,size)                isbufferinram(array,size*sizeof(array[0]))
#define isarrayinrom(array,size)                isbufferinrom(array,size*sizeof(array[0]))
#define isarraycorrect(array,size)              isarrayinram(array,size) || isarrayinrom(array,size)

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________


//==========================================================================================================================================
/**
 * @brief flags for events
 *
 * This type stores event flags. Each flag is reserved for different event. Flags can be joined with 'OR' operations, for example:
 *
 * @code{.c}
 * oC_MemMan_EventFlags_t flags = oC_MemMan_EventFlags_AllocationError | oC_MemMan_EventFlags_BufferOverflow;
 * @endcode
 */
//==========================================================================================================================================
typedef enum
{
    MemoryEventFlags_NotError                      = 0x8000 ,   //!< Flag set if the event is not error

    MemoryEventFlags_MemoryFault                   =                             (1<<0), //!< Memory fault flag. Reason is unknown
    MemoryEventFlags_BusFault                      =                             (1<<1), //!< Bus fault flag.
    MemoryEventFlags_AllocationError               =                             (1<<2), //!< Error while allocation of memory
    MemoryEventFlags_ReleaseError                  =                             (1<<3), //!< Error while releasing memory (address and allocation line number given in the event arguments)
    MemoryEventFlags_PossibleMemoryLeak            =                             (1<<4), //!< There is a possibility of memory leak
    MemoryEventFlags_BufferOverflow                =                             (1<<5), //!< Allocated memory at the given address achieved 'red-zone'
    MemoryEventFlags_MemoryAllocated               = MemoryEventFlags_NotError | (1<<6), //!< The memory has been allocated without an error
    MemoryEventFlags_MemoryReleased                = MemoryEventFlags_NotError | (1<<7), //!< The memory was released without error
    MemoryEventFlags_RawMemoryAllocated            = MemoryEventFlags_NotError | (1<<8), //!< The memory was allocated in the heap map without error
    MemoryEventFlags_RawMemoryReleased             = MemoryEventFlags_NotError | (1<<9), //!< The memory was released from the heap map without error
    MemoryEventFlags_ModuleTurningOff              =                             (1<<10),//!< The MemMan module is turning off
    MemoryEventFlags_MemoryExhausted               =                             (1<<11),//!< The memory on the main machine heap is going to be exhausted (achieved minimum level)
    MemoryEventFlags_PanicMemoryExhausted          =                             (1<<12),//!< The memory on the main machine heap is already exhausted. Achieved panic level
    MemoryEventFlags_DataSectionOverflow           =                             (1<<13),//!< Some buffer, that is in the data section was overwritten.
    MemoryEventFlags_ExternalMemoryExhausted       =                             (1<<14),//!< The memory on the external machine heap is going to be exhausted (achieved minimum level)
    MemoryEventFlags_PanicExternalMemoryExhausted  =                             (1<<15),//!< The memory on the external machine heap is already exhausted. Achieved panic level

    MemoryEventFlags_AllErrors                     = 0x7FFF ,    //!< All error events
    MemoryEventFlags_All                           = 0xFFFF ,    //!< All events
} MemoryEventFlags_t;

//==========================================================================================================================================
/**
 * @brief flags for allocations
 *
 * This is the type, that helps to configure memory allocation. Flags can be joined by 'OR' operations.
 */
//==========================================================================================================================================
typedef enum
{
    AllocationFlags_Default           = 0 ,   //!< Default memory allocation
    AllocationFlags_ZeroFill          = 1<<0 ,//!< Fill the allocated memory with 0 values
    AllocationFlags_NoWait            = 1<<1 ,//!< Don't wait for allocation. Allocate it now or never (allocate function will check if there is a possibility for allocation and return NULL if not).
    AllocationFlags_CanWait500Msecond = 1<<2 ,//!< Wait for memory or module available but maximum for 500 ms
    AllocationFlags_CanWait1Second    = 1<<3 ,//!< Wait for memory or module available but maximum for 1 s
    AllocationFlags_CanWaitForever    = 1<<4 ,//!< Wait for memory as much as needed
    AllocationFlags_ForceBlock        = 1<<5 ,//!< Do not use this flag if you are not sure. It forces allocation of memory. It is only for really special cases. There is a high probability, that module or some data will be damaged when this flag is set.
    AllocationFlags_UseExternalRam    = 1<<6 ,//!< Allocate in external ram if possible
    AllocationFlags_UseInternalRam    = 1<<7 ,//!< Allocate in internal ram if possible
    AllocationFlags_UseDmaRam         = 1<<8 ,//!< Use special memory that is reserved for DMA
    AllocationFlags_DmaRamFirst       = 1<<8 ,//!< At the begin try to allocate in internal ram
    AllocationFlags_InternalRamFirst  = 0<<9 ,//!< At the begin try to allocate in internal ram
    AllocationFlags_ExternalRamFirst  = 1<<9 ,//!< At the begin try to allocate in external ram
    AllocationFlags_ReleaseOnlyInCore = 1<<10,//!< Releasing the memory is possible only in the core
    AllocationFlags_DontRepair        = 1<<11,//!< If the buffer is damaged, don't repair it (mainly used for demonstration purposes)
} AllocationFlags_t;

//==========================================================================================================================================
/**
 * @brief handler function type for events
 *
 * The type is for storing pointers to the functions that handles memory events. It is for allocator events and module events. Note, that
 * this function is like interrupt, and there cannot be any 'thread wait' operations, because none of threads is active while execution of it.
 * Moreover, the system is stopped for handling this operations, so this should be as short as possible.
 *
 * @param Address           Address of event. It can be NULL, when the address is not used in this event.
 * @param Event             Flags of the event, that already occurs. It can be set to more than one flag.
 * @param LineNumber        Number of line where the allocation was called. It can be set to 0, if it is not used.
 */
//==========================================================================================================================================
typedef void (*MemoryEventHandler_t)( void * Address , MemoryEventFlags_t Event, const char * Function, uint32_t LineNumber );

//==========================================================================================================================================
/**
 * @brief identifier for allocations
 *
 * The type is for storing informations about module, that want to allocate memory. It should be defined as constant variable to save the memory.
 * Each allocation of memory needs the allocator of this type with correct values.
 */
//==========================================================================================================================================
typedef struct
{
    const char *          Name;           /**< Printable name of the allocator (for printing to user) */
    MemoryEventHandler_t  EventHandler;   /**< Function, that should be called, when some event occurs. It can be set to NULL if not used */
    MemoryEventFlags_t    EventFlags;     /**< List of events, that should be turned on for this allocator. Flags are joined with 'OR' operation. */
    oC_MemorySize_t       Limit;          /**< Limit of size of memory allocations - set it to 0 if not used */
    uint32_t              CorePwd;        /**< Password for usage of core-protected heap */
} oC_Allocator_t;

//==========================================================================================================================================
/**
 * @brief type to storing heap map
 *
 * This is the special type, that stores information about *Heap Map*. It is only the handle. To create a heap map use function #oC_MemMan_AllocateHeapMap, and use
 * #oC_MemMan_FreeHeapMap to release it. Read the module description for more informations about usage of heap maps.
 */
//==========================================================================================================================================
typedef struct _HeapMap_t * oC_HeapMap_t;

//==========================================================================================================================================
/**
 * @brief type to store allocator pointer
 *
 * The type is just for short name of the allocator type pointer
 */
//==========================================================================================================================================
typedef const oC_Allocator_t * Allocator_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

EXTERNAL_PREFIX void * EXTERNAL_FUNC( _malloc        , ( const char * Function , uint32_t LineNumber , AllocationFlags_t CoreFlags, oC_UInt_t Size, AllocationFlags_t Flags , ... ) );
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( _free          , ( AllocationFlags_t CoreFlags, void * Address , AllocationFlags_t Flags , ... )                                              );

EXTERNAL_PREFIX void * EXTERNAL_FUNC( _rawmalloc     , ( oC_UInt_t Size , const char * Function , uint32_t LineNumber , AllocationFlags_t Flags)    );
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( _rawfree       , ( void * Address , oC_UInt_t Size)                                                           );

EXTERNAL_PREFIX void * EXTERNAL_FUNC( _kmalloc       , ( oC_UInt_t Size , Allocator_t Allocator , const char * Function , uint32_t LineNumber , AllocationFlags_t Flags , oC_UInt_t Alignment ) );
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( _kfree         , ( void * Address , AllocationFlags_t Flags )                                                  );

EXTERNAL_PREFIX void * EXTERNAL_FUNC( _krawmalloc    , ( oC_HeapMap_t Map , oC_UInt_t Size , const char * Function , uint32_t LineNumber , AllocationFlags_t Flags ));
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( _krawfree      , ( oC_HeapMap_t Map , void * Address , oC_UInt_t Size ) );

EXTERNAL_PREFIX void * EXTERNAL_FUNC( _smartalloc    , ( oC_UInt_t Size , const char * Function , uint32_t LineNumber , AllocationFlags_t Flags )   );
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( _smartfree     , ( void * Address , oC_UInt_t Size , AllocationFlags_t Flags )                                );

EXTERNAL_PREFIX void * EXTERNAL_FUNC( _ksmartalloc   , ( oC_UInt_t Size , Allocator_t Allocator , const char * Function , uint32_t LineNumber , AllocationFlags_t Flags));
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( _ksmartfree    , ( void * Address , oC_UInt_t Size , AllocationFlags_t Flags) );

EXTERNAL_PREFIX bool   EXTERNAL_FUNC( isram          , ( const void * Address ) );
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( isrom          , ( const void * Address ) );

EXTERNAL_PREFIX bool   EXTERNAL_FUNC( isbufferinram  , ( const void * Buffer , oC_UInt_t Size ) );
EXTERNAL_PREFIX bool   EXTERNAL_FUNC( isbufferinrom  , ( const void * Buffer , oC_UInt_t Size ) );

EXTERNAL_PREFIX bool   EXTERNAL_FUNC( isaddresscorrect , ( const void * Address ) );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

#endif /* SYSTEM_LIBRARIES_INC_OC_STDLIB_H_ */
