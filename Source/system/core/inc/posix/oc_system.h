/** ****************************************************************************************************************************************
 *
 * @file       oc_system.h
 *
 * @brief      __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 6 wrz 2015 12:30:36) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/


#ifndef SYSTEM_LIBRARIES_INC_OC_SYSTEM_H_
#define SYSTEM_LIBRARIES_INC_OC_SYSTEM_H_

#include <oc_stdlib.h>
#include <oc_time.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef void * Process_t;

typedef void * Thread_t;

typedef void * User_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

extern Allocator_t      getcurallocator     (void);
extern Process_t        getcurprocess       (void);
extern Thread_t         getcurthread        (void);
extern User_t           getcuruser          (void);
extern User_t           getrootuser         (void);
extern bool             iscurroot           (void);
extern oC_Timestamp_t   gettimestamp        (void);
extern oC_Time_t        gettimeout          ( oC_Timestamp_t Timestamp );
extern oC_Timestamp_t   timeouttotimestamp  ( oC_Time_t Timeout );
extern bool             sleep               ( oC_Time_t Time );

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


#endif /* SYSTEM_LIBRARIES_INC_OC_SYSTEM_H_ */
