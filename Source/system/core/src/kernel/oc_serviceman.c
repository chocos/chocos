/** ****************************************************************************************************************************************
 *
 * @brief      Source of service manager
 * 
 * @file       oc_serviceman.c
 *
 * @author     Patryk Kubiak - (Created on: 18.01.2017 21:29:35) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_service.h>
#include <oc_serviceman.h>
#include <oc_services_cfg.h>
#include <oc_intman.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define MODULE_NAME                        "Service Manager"
#define TURN_ON_VERIFICATION(errorCode)    oC_Module_TurnOnVerification ( &errorCode, oC_Module_ServiceMan )
#define TURN_OFF_VERIFICATION(errorCode)   oC_Module_TurnOffVerification( &errorCode, oC_Module_ServiceMan )
#define IS_TURNED_ON                       oC_SaveIfFalse(oC_FUNCTION, oC_Module_IsTurnedOn(oC_Module_ServiceMan), oC_ErrorCode_ModuleNotStartedYet)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_Service_t                        Service;
    const oC_Service_Registration_t*    Registration;
} ServiceData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

       ServiceData_t*   ServiceData_New     ( const oC_Service_Registration_t * Registration );
static bool             ServiceData_Delete  ( ServiceData_t * ServiceData );
static ServiceData_t*   GetServiceData      ( const char * Name );
static oC_ErrorCode_t   StartService        ( ServiceData_t * ServiceData, void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) );
static oC_ErrorCode_t   StopService         ( ServiceData_t * ServiceData );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with globals
 *
 *  ======================================================================================================================================*/
#define _________________________________________GLOBALS_SECTION____________________________________________________________________________

#define TURN_OFF(...)
#define TURN_ON(ServiceName)        \
extern const oC_Service_Registration_t ServiceName;

CFG_LIST_SERVICES(TURN_ON,TURN_OFF);

#undef TURN_ON

static oC_List(ServiceData_t*) Services   = NULL;
const oC_Module_Registration_t ServiceMan = {
                .Name                   = "ServiceMan" ,
                .LongName               = MODULE_NAME ,
                .Module                 = oC_Module_ServiceMan ,
                .TurnOnFunction         = oC_ServiceMan_TurnOn ,
                .TurnOffFunction        = oC_ServiceMan_TurnOff ,
                .RequiredModules        = { oC_Module_ProcessMan , oC_Module_ThreadMan }
};
static const oC_Allocator_t    Allocator  = {
                .Name    = MODULE_NAME,
                .CorePwd = oC_MemMan_CORE_PWD
};

#undef  _________________________________________GLOBALS_SECTION____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on Service Manager
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ServiceMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(TURN_OFF_VERIFICATION(errorCode))
    {
        Services = oC_List_New( &Allocator, AllocationFlags_Default );

        if(ErrorCondition( Services != NULL , oC_ErrorCode_AllocationError ))
        {
#define TURN_ON(ServiceName)            \
                {   \
                    ServiceData_t * serviceData = ServiceData_New(&ServiceName);    \
                    \
                    if(oC_SaveIfFalse(ServiceName.Name, serviceData != NULL, oC_ErrorCode_AllocationError )) \
                    { \
                        oC_List_PushBack(Services, serviceData, &Allocator);    \
                    } \
                }

            CFG_LIST_SERVICES(TURN_ON,TURN_OFF);

#undef TURN_ON

            oC_Module_TurnOn(oC_Module_ServiceMan);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off Service Manager
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ServiceMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(TURN_ON_VERIFICATION(errorCode))
    {
        bool allDeleted = true;

        oC_IntMan_EnterCriticalSection();

        oC_Module_TurnOff(oC_Module_ServiceMan);

        foreach(Services, serviceData)
        {
            if(ServiceData_Delete(serviceData) == false)
            {
                allDeleted = false;
            }
        }

        bool listDeleted = oC_List_Delete(Services, AllocationFlags_Default);

        if(ErrorCondition( listDeleted && allDeleted, oC_ErrorCode_ReleaseError ))
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief starts all services
 */
//==========================================================================================================================================
void oC_ServiceMan_StartAllPossible( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) )
{
    if(oC_Module_IsTurnedOn(oC_Module_ServiceMan)
        && ( PrintWaitMessage == NULL || isaddresscorrect(PrintWaitMessage)    )
        && ( PrintResult == NULL      || isaddresscorrect(PrintResult)         )
        )
    {
        foreach(Services,serviceData)
        {
            if(oC_Service_IsActive(serviceData->Service) == false)
            {
                oC_ErrorCode_t errorCode = StartService(serviceData, PrintWaitMessage, PrintResult );

                if(errorCode != oC_ErrorCode_RequiredModuleNotEnabled)
                {
                    oC_SaveIfErrorOccur(serviceData->Registration->Name,errorCode);
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief stops all services
 */
//==========================================================================================================================================
void oC_ServiceMan_StopAllPossible ( void )
{
    if(IS_TURNED_ON)
    {
        foreach(Services,serviceData)
        {
            if(oC_Service_IsActive(serviceData->Service) == false)
            {
                oC_ErrorCode_t errorCode = StopService(serviceData);

                if(errorCode != oC_ErrorCode_ModuleUsedByDifferentModule)
                {
                    oC_SaveIfErrorOccur(serviceData->Registration->Name,errorCode);
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief checks if the given service is activated
 */
//==========================================================================================================================================
bool oC_ServiceMan_IsServiceActive( const char * Name )
{
    bool active = false;

    if(IS_TURNED_ON && oC_SaveIfFalse("ServiceMan::IsActive - Name is not correct - ", isaddresscorrect(Name), oC_ErrorCode_WrongAddress))
    {
        ServiceData_t * serviceData = GetServiceData(Name);

        if(oC_SaveIfFalse("ServiceMan::IsActive: ", serviceData != NULL , oC_ErrorCode_ObjectNotFoundOnList))
        {
            active = oC_Service_IsActive(serviceData->Service);
        }
    }

    return active;
}

//==========================================================================================================================================
/**
 * @brief starts the given service
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ServiceMan_StartService( const char * Name )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(TURN_ON_VERIFICATION(errorCode) && ErrorCondition( isaddresscorrect(Name), oC_ErrorCode_WrongAddress ))
    {
        ServiceData_t * serviceData = GetServiceData(Name);
        if(
            ErrorCondition( serviceData != NULL                                 , oC_ErrorCode_ObjectNotFoundOnList     )
         && ErrorCondition( oC_Service_IsActive(serviceData->Service) == false  , oC_ErrorCode_ServiceAlreadyStarted    )
            )
        {
            errorCode = StartService(serviceData, NULL, NULL);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief stops the given service
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ServiceMan_StopService( const char * Name )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(TURN_ON_VERIFICATION(errorCode) && ErrorCondition( isaddresscorrect(Name), oC_ErrorCode_WrongAddress ))
    {
        ServiceData_t * serviceData = GetServiceData(Name);
        if(
            ErrorCondition( serviceData != NULL                                 , oC_ErrorCode_ObjectNotFoundOnList     )
         && ErrorCondition( oC_Service_IsActive(serviceData->Service) == true   , oC_ErrorCode_ServiceNotStarted        )
            )
        {
            errorCode = StopService(serviceData);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns service with the given name
 */
//==========================================================================================================================================
oC_Service_t oC_ServiceMan_GetService( const char * Name )
{
    oC_Service_t service = NULL;

    if(IS_TURNED_ON && oC_SaveIfFalse("ServiceMan::GetService - Name is not correct ", isaddresscorrect(Name), oC_ErrorCode_WrongAddress))
    {
        ServiceData_t * data = GetServiceData(Name);

        if(
            oC_SaveIfFalse("ServiceMan::GetService: ", data != NULL         , oC_ErrorCode_ObjectNotFoundOnList     )
         && oC_SaveIfFalse("ServiceMan::GetService: ", data->Service != NULL, oC_ErrorCode_InternalDataAreDamaged   )
            )
        {
            service = data->Service;
        }
    }

    return service;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief allocates memory for a new service data
 */
//==========================================================================================================================================
ServiceData_t* ServiceData_New( const oC_Service_Registration_t * Registration )
{
    ServiceData_t * serviceData = NULL;

    if( oC_SaveIfFalse("ServiceManager::NewServiceData - Registration incorrect ", isaddresscorrect(Registration), oC_ErrorCode_WrongAddress ) )
    {
        serviceData = kmalloc( sizeof(ServiceData_t), &Allocator, AllocationFlags_Default );

        if( oC_SaveIfFalse("ServiceManger::NewServiceData - data ", serviceData != NULL, oC_ErrorCode_AllocationError) )
        {
            serviceData->Service = oC_Service_New( Registration );

            if( oC_SaveIfFalse("ServiceManger::NewServiceData - service ", serviceData->Service != NULL, oC_ErrorCode_AllocationError) )
            {
                serviceData->Registration = Registration;
            }
            else
            {
                kfree(serviceData, AllocationFlags_Default);
                serviceData = NULL;
            }
        }
    }

    return serviceData;
}
//==========================================================================================================================================
/**
 * @brief deletes service data
 */
//==========================================================================================================================================
static bool ServiceData_Delete( ServiceData_t * ServiceData )
{
    bool serviceStopped = oC_Service_IsActive(ServiceData->Service) == false
                       || oC_SaveIfErrorOccur("ServiceManager::Delete - Cannot Stop Service   - " , oC_Service_Stop(ServiceData->Service) );
    bool serviceDeleted = oC_SaveIfFalse("ServiceManager::Delete - Cannot Delete Service - " , oC_Service_Delete(&ServiceData->Service), oC_ErrorCode_ReleaseError );
    bool deleted        = kfree( ServiceData, AllocationFlags_Default );

    return deleted && serviceStopped && serviceDeleted;
}

//==========================================================================================================================================
/**
 * @brief searches for a service data
 */
//==========================================================================================================================================
static ServiceData_t* GetServiceData( const char * Name )
{
    ServiceData_t* foundServiceData = NULL;

    foreach(Services,serviceData)
    {
        if(strcmp(serviceData->Registration->Name, Name) == 0)
        {
            foundServiceData = serviceData;
            break;
        }
    }

    return foundServiceData;
}

//==========================================================================================================================================
/**
 * @brief starts service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartService( ServiceData_t * ServiceData, void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) )
{
#define PrintWait(...)      if( PrintWaitMessage != NULL ) PrintWaitMessage( __VA_ARGS__ )
#define PrintResult( err )  if( PrintResult      != NULL ) PrintResult( err )
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Service_IsActive(ServiceData->Service) == false, oC_ErrorCode_ServiceAlreadyStarted))
    {
        bool allRequiredEnabled = true;

        for( uint32_t i = 0 ; i < MAX_REQUIRED_MODULES && allRequiredEnabled ; i++)
        {
            oC_Module_t requiredModule = ServiceData->Registration->RequiredModules[i];
            if(requiredModule > oC_Module_None && requiredModule < oC_Module_NumberOfModules)
            {
                allRequiredEnabled = oC_Module_IsTurnedOn(requiredModule);
            }
        }
        if(ErrorCondition(allRequiredEnabled , oC_ErrorCode_RequiredModuleNotEnabled))
        {
            PrintWait( "Staring service %s", oC_Service_GetName(ServiceData->Service) );
            errorCode = oC_Service_Start(ServiceData->Service);
            PrintResult( errorCode );
        }
    }

    return errorCode;
#undef PrintWait
#undef PrintResult
}

//==========================================================================================================================================
/**
 * @brief stops service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StopService( ServiceData_t * ServiceData )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Service_IsActive(ServiceData->Service) == false, oC_ErrorCode_ServiceNotStarted))
    {
        errorCode = oC_Service_Stop(ServiceData->Service);
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


