/** ****************************************************************************************************************************************
 *
 * @file       oc_signal.c
 *
 * @brief      The file with source for signal module functions
 *
 * @author     Patryk Kubiak - (Created on: 2 09 2015 18:15:57)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_signal.h>
#include <oc_object.h>
#include <oc_stdlib.h>
#include <oc_threadman.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

struct Signal_t
{
    oC_ObjectControl_t  ObjectControl;
};

#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________



#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
