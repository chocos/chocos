/** ****************************************************************************************************************************************
 *
 * @brief      The file contains the implementation of the news module
 * 
 * @file          oc_news.c
 *
 * @author     Patryk Kubiak - (Created on: 12.02.2017 11:00:05) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_news.h>
#include <oc_debug_cfg.h>
#include <oc_intman.h>
#include <oc_system.h>
#include <stdarg.h>
#include <oc_kprint.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    char                String[CFG_UINT16_MAX_NEWS_STRING_LENGTH];
    oC_Timestamp_t      Timestamp;
    bool                Used;
} News_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static News_t   NewsArray[CFG_UINT16_NUMBER_OF_NEWS] = {{{0}, 0, 0}};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCITONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Saves the news
 *
 * Saves the news in the news array. If there is no free space in the array, the oldest news will be replaced.
 *
 * @param News      the news to save
 */
//==========================================================================================================================================
void oC_News_Save( const char * News )
{
#if CFG_ENABLE_NEWS == ON
    oC_IntMan_EnterCriticalSection();

    if(isaddresscorrect(News) && strlen(News) > 0)
    {
        oC_Timestamp_t currentTimestamp = gettimestamp();
        oC_Timestamp_t oldestTimestamp  = currentTimestamp;
        uint16_t       putIndex         = 0;

        for(uint16_t i = 0; i < CFG_UINT16_NUMBER_OF_NEWS; i++)
        {
            if(NewsArray[i].Used == false)
            {
                putIndex = i;
                break;
            }
            else if(NewsArray[i].Timestamp <= oldestTimestamp)
            {
                oldestTimestamp = NewsArray[i].Timestamp;
                putIndex        = i;
            }
        }

        NewsArray[putIndex].Used       = true;
        NewsArray[putIndex].Timestamp  = currentTimestamp;
        strcpy(NewsArray[putIndex].String,News);
    }

    oC_IntMan_ExitCriticalSection();
#endif
}

//==========================================================================================================================================
/**
 * @brief reads the news
 *
 * Reads the news from the news array. It will read the oldest news entry.
 *
 * @param outNews       Destination buffer for the news
 * @param Size          the size of the news
 * @param outTimestamp  Destination for the timestamp of the news
 *
 * @return true if news was read (false if there is no news)
 */
//==========================================================================================================================================
bool oC_News_Read( char * outNews, oC_MemorySize_t Size, oC_Timestamp_t * outTimestamp )
{
    bool found = false;
#if CFG_ENABLE_NEWS == ON

    oC_IntMan_EnterCriticalSection();

    if(isram(outNews) && Size > 0 && isram(outTimestamp))
    {
        oC_Timestamp_t oldestTimestamp = gettimestamp();
        uint16_t       getIndex        = 0;

        for(uint16_t i = 0; i < CFG_UINT16_NUMBER_OF_NEWS; i++)
        {
            if(NewsArray[i].Used == true && NewsArray[i].Timestamp <= oldestTimestamp)
            {
                found           = true;
                oldestTimestamp = NewsArray[i].Timestamp;
                getIndex        = i;
            }
        }

        if(found)
        {
            *outTimestamp = NewsArray[getIndex].Timestamp;
            strncpy(outNews,NewsArray[getIndex].String,Size);
            NewsArray[getIndex].Used = false;
        }
    }

    oC_IntMan_ExitCriticalSection();

#endif
    return found;
}

//==========================================================================================================================================
/**
 * @brief saves the formatted news
 *
 * Saves the formatted news in the news array
 *
 * @param Format        the format of the news (For more information see #oC_KPrint_Format)
 */
//==========================================================================================================================================
void oC_News_SaveFormatted( const char * Format , ... )
{
#if CFG_ENABLE_NEWS == ON
    oC_IntMan_EnterCriticalSection();

    if(isaddresscorrect(Format) && strlen(Format) > 0)
    {
        oC_Timestamp_t currentTimestamp = gettimestamp();
        oC_Timestamp_t oldestTimestamp  = currentTimestamp;
        uint16_t       putIndex         = 0;
        va_list        argumentsList;


        for(uint16_t i = 0; i < CFG_UINT16_NUMBER_OF_NEWS; i++)
        {
            if(NewsArray[i].Used == false)
            {
                putIndex = i;
                break;
            }
            else if(NewsArray[i].Timestamp <= oldestTimestamp)
            {
                oldestTimestamp = NewsArray[i].Timestamp;
                putIndex        = i;
            }
        }

        NewsArray[putIndex].Used       = true;
        NewsArray[putIndex].Timestamp  = currentTimestamp;

        va_start(argumentsList,Format);
        oC_KPrint_Format(NewsArray[putIndex].String,sizeof(NewsArray[putIndex].String),Format,argumentsList, NULL);
        va_end(argumentsList);
    }

    oC_IntMan_ExitCriticalSection();
#endif
}


#undef  _________________________________________FUNCITONS_SECTION__________________________________________________________________________


