/** ****************************************************************************************************************************************
 *
 * @file       oc_event.c
 *
 * @brief      The source file for the event module
 *
 * @author     Patryk Kubiak - (Created on: 2 09 2015 17:44:16)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_event.h>
#include <oc_object.h>
#include <oc_stdlib.h>
#include <oc_threadman.h>
#include <oc_intman.h>
#include <oc_null.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Event_t
{
    oC_ObjectControl_t      ObjectControl;
    uint32_t                State;
    oC_Event_Protect_t      Protection;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup Event
//! @{

//==========================================================================================================================================
/**
 * @brief Creates new event
 *
 * The function creates new event and returns pointer to it. The event is created in the state given as parameter.
 *
 * @param InitState     Initial state of the event
 * @param Allocator     Allocator for the event
 * @param Flags         Flags for the allocation
 *
 * @return pointer to the event or NULL if error
 */
//==========================================================================================================================================
oC_Event_t oC_Event_New( oC_Event_State_t InitState , Allocator_t Allocator , AllocationFlags_t Flags )
{
    oC_Event_t event = ksmartalloc( sizeof(struct Event_t) , Allocator , Flags);

    if(event)
    {
        event->State         = InitState;
        event->ObjectControl = oC_CountObjectControl(event,oC_ObjectId_Event);
        event->Protection    = oC_Event_Protect_NotProtected;
    }
    else
    {
        oC_SaveError("Event: Creating new event" , oC_ErrorCode_AllocationError);
    }

    return event;
}


//==========================================================================================================================================
/**
 * @brief Deletes event
 *
 * The function deletes event. If the event is protected, then it cannot be deleted.
 *
 * @param Event     Pointer to the event
 * @param Flags     Flags for the deallocation
 *
 * @return true if event was deleted, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_Delete( oC_Event_t * Event , AllocationFlags_t Flags )
{
    bool deleted = false;

    if(oC_Event_IsCorrect(*Event))
    {
        if((*Event)->Protection == oC_Event_Protect_NotProtected)
        {
            oC_IntMan_EnterCriticalSection();

            oC_ThreadMan_UnblockAllBlockedBy(&(*Event)->State , false);
            (*Event)->ObjectControl = 0;

            if(ksmartfree(*Event,sizeof(struct Event_t),Flags))
            {
                *Event  = NULL;
                deleted = true;
            }
            else
            {
                oC_SaveError("Event: Delete" , oC_ErrorCode_ReleaseError);
            }

            oC_IntMan_ExitCriticalSection();
        }
        else
        {
            oC_SaveError("Event: Delete - " , oC_ErrorCode_ObjectProtected);
        }
    }
    else
    {
        oC_SaveError("Event: Delete - " , oC_ErrorCode_ObjectNotCorrect);
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief Protects event from deleting
 *
 * The function protects event from deleting. The event can be protected only by the owner of the event.
 *
 * @param Event         Pointer to the event
 * @param Allocator     Allocator of the event
 * @param Protection    Protection value
 *
 * @return true if event was protected, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_ProtectDelete( oC_Event_t Event , Allocator_t Allocator , oC_Event_Protect_t Protection )
{
    bool success = false;

    if(oC_Event_IsCorrect(Event))
    {
        if(isaddresscorrect(Allocator))
        {
            if(Allocator != oC_MemMan_GetAllocatorOfAddress(Event))
            {
                oC_IntMan_EnterCriticalSection();

                Event->Protection = Protection;

                success = true;

                oC_IntMan_ExitCriticalSection();
            }
            else
            {
                oC_SaveError("Event: Protect delete - Allocator not correct (you are not owner of this event)" , oC_ErrorCode_WrongAddress );
            }
        }
        else
        {
            oC_SaveError("Event: Protect delete - Allocator not correct " , oC_ErrorCode_WrongAddress );
        }
    }
    else
    {
        oC_SaveError("Event: Protect delete - " , oC_ErrorCode_ObjectNotCorrect);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief Checks if the event is correct
 *
 * The function checks if the event is correct. The event is correct if it is in the RAM and the object control value is correct.
 *
 * @param Event     Pointer to the event
 *
 * @return true if the event is correct, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_IsCorrect( oC_Event_t Event )
{
    return oC_MemMan_IsRamAddress(Event) && oC_CheckObjectControl(Event,oC_ObjectId_Event,Event->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief Gets the state of the event
 * 
 * The function gets the state of the event
 * 
 * @param Event     Pointer to the event
 * 
 * @return State of the event
 */
//==========================================================================================================================================
oC_Event_State_t oC_Event_GetState( oC_Event_t Event )
{
    return oC_Event_IsCorrect(Event) ? Event->State : 0;
}

//==========================================================================================================================================
/**
 * @brief Sets the state of the event
 * 
 * The function sets the state of the event
 * 
 * @param Event     Pointer to the event
 * @param State     New state of the event
 * 
 * @return true if the state was set, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_SetState( oC_Event_t Event , oC_Event_State_t State )
{
    bool set = false;

    if(oC_Event_IsCorrect(Event))
    {
        oC_IntMan_EnterCriticalSection();
        Event->State = State;

        // TODO: Uncomment line below when list will be faster
        // The line below unblocks all threads blocked by the event, but
        // it takes a lot of time and it must be called in an interrupt :/
//        oC_ThreadMan_UnblockAllBlockedBy(&Event->State , true);
        set          = true;
        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        oC_SaveError("Event: set state" , oC_ErrorCode_ObjectNotCorrect);
    }

    return set;
}


//==========================================================================================================================================
/**
 * @brief Clears state bits
 * 
 * The function clears state bits
 * 
 * @param Event         Pointer to the event
 * @param StateMask     Mask with bits to clear
 * 
 * @return true if the bits were cleared, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_ClearStateBits( oC_Event_t Event , uint32_t StateMask )
{
    bool cleared = false;

    if(oC_Event_IsCorrect(Event))
    {
        oC_IntMan_EnterCriticalSection();
        Event->State &= ~StateMask;
        cleared       = true;
        oC_IntMan_ExitCriticalSection();
    }

    return cleared;
}

//==========================================================================================================================================
/**
 * @brief Sets state bits
 * 
 * The function sets state bits
 * 
 * @param Event         Pointer to the event
 * @param StateBits     Bits to set
 * 
 * @return true if the bits were set, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_SetStateBits( oC_Event_t Event , uint32_t StateBits )
{
    bool cleared = false;

    if(oC_Event_IsCorrect(Event))
    {
        oC_IntMan_EnterCriticalSection();
        Event->State |= StateBits;
        cleared       = true;
        oC_IntMan_ExitCriticalSection();
    }

    return cleared;
}

//==========================================================================================================================================
/**
 * @brief Reads state of the event
 * 
 * The function reads state of the event
 * 
 * @param Event         Pointer to the event
 * @param outState      Destination for the state
 * 
 * @return true if the state was read, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_ReadState( oC_Event_t Event , oC_Event_State_t * outState )
{
    bool read = false;

    if(oC_Event_IsCorrect(Event))
    {
        oC_IntMan_EnterCriticalSection();
        *outState = Event->State;
        read      = true;
        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        oC_SaveError("Event: read state" , oC_ErrorCode_ObjectNotCorrect);
    }

    return read;
}

//==========================================================================================================================================
/**
 * @brief Waits for the state
 * 
 * The function waits for the state of the event
 * 
 * @param Event         Pointer to the event
 * @param State         State to wait for
 * @param StateMask     Mask for the state
 * @param Timeout       Timeout for the waiting
 * 
 * @return true if the state is correct, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_WaitForState( oC_Event_t Event , oC_Event_State_t State , oC_Event_StateMask_t StateMask , oC_Time_t Timeout )
{
    bool stateOk = false;

    if(oC_Event_IsCorrect(Event))
    {
        oC_Thread_t thread = oC_ThreadMan_GetCurrentThread();

        if(StateMask != oC_Event_StateMask_DifferentThan)
        {
            oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenEqual , State , StateMask , Timeout);
        }
        else
        {
            oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenNotEqual , State , oC_Event_StateMask_Full , Timeout);
        }

        while(oC_Thread_IsBlocked(thread));

        oC_Thread_SetUnblocked(thread);

        oC_IntMan_EnterCriticalSection();

        if(oC_Event_IsCorrect(Event))
        {
            if(StateMask != oC_Event_StateMask_DifferentThan)
            {
                if((Event->State & StateMask) == (State & StateMask))
                {
                    stateOk = true;
                }
            }
            else
            {
                stateOk = (Event->State & oC_Event_StateMask_Full) != (State & oC_Event_StateMask_Full);
            }
        }
        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        oC_SaveError("Event: wait for state" , oC_ErrorCode_ObjectNotCorrect);
    }

    return stateOk;
}

//==========================================================================================================================================
/**
 * @brief Waits for the value
 * 
 * The function waits for the value of the event
 * 
 * @param Event         Pointer to the event
 * @param Value         Value to wait for
 * @param CompareType   Type of the compare
 * @param Timeout       Timeout for the waiting
 * 
 * @return true if the value is correct, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_WaitForValue( oC_Event_t Event , oC_Event_State_t Value , oC_Event_CompareType_t CompareType, oC_Time_t Timeout )
{
    bool stateOk = false;

    if(oC_Event_IsCorrect(Event) && CompareType < oC_Event_CompareType_NumberOfElements)
    {
        oC_Thread_t thread = oC_ThreadMan_GetCurrentThread();

        if(CompareType == oC_Event_CompareType_Equal)
        {
            oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenEqual , Value, oC_Event_StateMask_Full, Timeout);
        }
        else if(CompareType == oC_Event_CompareType_Greater)
        {
            oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenGreater, Value, oC_Event_StateMask_Full, Timeout);
        }
        else if(CompareType == oC_Event_CompareType_GreaterOrEqual)
        {
            oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenGreaterOrEqual, Value, oC_Event_StateMask_Full, Timeout);
        }
        else if(CompareType == oC_Event_CompareType_Less)
        {
            oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenSmaller, Value, oC_Event_StateMask_Full, Timeout);
        }
        else if(CompareType == oC_Event_CompareType_LessOrEqual)
        {
            oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenSmallerOrEqual, Value, oC_Event_StateMask_Full, Timeout);
        }

        while(oC_Thread_IsBlocked(thread));

        oC_Thread_SetUnblocked(thread);

        oC_IntMan_EnterCriticalSection();

        if(oC_Event_IsCorrect(Event))
        {
            if(CompareType == oC_Event_CompareType_Equal)
            {
                stateOk = Event->State == Value;
            }
            else if(CompareType == oC_Event_CompareType_Greater)
            {
                stateOk = Event->State > Value;
            }
            else if(CompareType == oC_Event_CompareType_GreaterOrEqual)
            {
                stateOk = Event->State >= Value;
            }
            else if(CompareType == oC_Event_CompareType_Less)
            {
                stateOk = Event->State < Value;
            }
            else if(CompareType == oC_Event_CompareType_LessOrEqual)
            {
                stateOk = Event->State <= Value;
            }
        }
        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        oC_SaveError("Event: wait for state" , oC_ErrorCode_ObjectNotCorrect);
    }

    return stateOk;
}

//==========================================================================================================================================
/**
 * @brief Waits for the bit to be set
 * 
 * The function waits for the bit to be set
 * 
 * @param Event             Pointer to the event
 * @param AvailableBitsMask Mask for the bits
 * @param Timeout           Timeout for the waiting
 * 
 * @return true if the bit is set, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_WaitForBitSet( oC_Event_t Event , oC_Event_StateMask_t AvailableBitsMask, oC_Time_t Timeout )
{
    bool stateOk = false;

    if(oC_Event_IsCorrect(Event))
    {
        oC_Timestamp_t  endTimestamp    = timeouttotimestamp(Timeout);
        oC_Thread_t     thread          = oC_ThreadMan_GetCurrentThread();

        if(oC_SaveIfFalse("thread cannot be NULL!", thread != NULL , oC_ErrorCode_ObjectNotCorrect))
        {
            while(stateOk == false && oC_Event_IsCorrect(Event) == true && endTimestamp >= gettimestamp())
            {
                oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenAtLeastOneBitIsSet, AvailableBitsMask, AvailableBitsMask, Timeout);

                while(oC_Thread_IsBlocked(thread));

                oC_Thread_SetUnblocked(thread);

                oC_IntMan_EnterCriticalSection();

                if(oC_Event_IsCorrect(Event))
                {
                    stateOk = (Event->State & AvailableBitsMask) != 0;
                }
                oC_IntMan_ExitCriticalSection();
            }
        }

    }
    else
    {
        oC_SaveError("Event: wait for state" , oC_ErrorCode_ObjectNotCorrect);
    }

    return stateOk;
}

//==========================================================================================================================================
/**
 * @brief Waits for the bit to be cleared
 * 
 * The function waits for the bit to be cleared
 * 
 * @param Event             Pointer to the event
 * @param AvailableBitsMask Mask for the bits
 * @param Timeout           Timeout for the waiting
 * 
 * @return true if the bit is cleared, false otherwise
 */
//==========================================================================================================================================
bool oC_Event_WaitForBitClear( oC_Event_t Event , oC_Event_StateMask_t AvailableBitsMask, oC_Time_t Timeout )
{
    bool stateOk = false;

    if(oC_Event_IsCorrect(Event))
    {
        oC_Thread_t thread = oC_ThreadMan_GetCurrentThread();

        oC_Thread_SetBlocked(thread, &Event->State , oC_Thread_Unblock_WhenAtLeastOneBitIsCleared, AvailableBitsMask, AvailableBitsMask, Timeout);

        while(oC_Thread_IsBlocked(thread));

        oC_Thread_SetUnblocked(thread);

        oC_IntMan_EnterCriticalSection();

        if(oC_Event_IsCorrect(Event))
        {
            stateOk = (Event->State & AvailableBitsMask) != AvailableBitsMask;
        }
        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        oC_SaveError("Event: wait for state" , oC_ErrorCode_ObjectNotCorrect);
    }

    return stateOk;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}
