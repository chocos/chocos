/** ****************************************************************************************************************************************
 *
 * @brief      Interface for module manager
 * 
 * @file       oc_moduleman.c
 *
 * @author     Patryk Kubiak - (Created on: 18.01.2017 21:51:50) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_moduleman.h>
#include <oc_modules_cfg.h>
#include <oc_stdlib.h>
#include <oc_array.h>
#include <oc_debug.h>
#include <oc_intman.h>
#include <oc_string.h>

/** ========================================================================================================================================
 *
 *              The section with globals
 *
 *  ======================================================================================================================================*/
#define _________________________________________GLOBALS_SECTION____________________________________________________________________________

#define TURN_OFF(...)
#define TURN_ON(Name) extern const oC_Module_Registration_t Name;

CFG_LIST_MODULES(TURN_ON,TURN_OFF);

#undef TURN_ON

#define TURN_ON(ModuleName)     &ModuleName ,
static const oC_Module_Registration_t * ModuleRegistrations[] = {

    CFG_LIST_MODULES(TURN_ON,TURN_OFF)

};
#undef TURN_ON

#undef  _________________________________________GLOBALS_SECTION____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on all modules that turning on is possible
 */
//==========================================================================================================================================
void oC_ModuleMan_TurnOnAllPossible( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) )
{
#define PrintWait(...)      if( PrintWaitMessage != NULL ) PrintWaitMessage( __VA_ARGS__ )
#define PrintResult( err )  if( PrintResult      != NULL ) PrintResult( err )
    oC_ARRAY_FOREACH_IN_ARRAY(ModuleRegistrations,registrationReference)
    {
        const oC_Module_Registration_t * registration = *registrationReference;

        if(
            oC_SaveIfFalse(registration->Name, registration->Module != oC_Module_None, oC_ErrorCode_ModuleNotCorrect)
         && oC_Module_IsTurnedOn(registration->Module) == false
          )
        {
            bool allRequiredEnabled = true;

            for( uint32_t i = 0; allRequiredEnabled && i < MAX_REQUIRED_MODULES; i++ )
            {
                if(registration->RequiredModules[i] != oC_Module_None)
                {
                    allRequiredEnabled = oC_Module_IsTurnedOn(registration->RequiredModules[i]);
                }
            }

            if(allRequiredEnabled)
            {
                if(oC_SaveIfFalse("ModuleManager: Turn on function is not set - ", isaddresscorrect(registration->TurnOnFunction), oC_ErrorCode_WrongAddress))
                {
                    PrintWait( "Turning on module %s", registration->Name );
                    oC_ErrorCode_t errorCode = registration->TurnOnFunction();
                    PrintResult( errorCode );
                    if(oC_SaveIfErrorOccur(registration->Name, errorCode ))
                    {
                        oC_SaveIfFalse(registration->Name, oC_Module_IsTurnedOn(registration->Module), oC_ErrorCode_ModuleHasNotStartedCorrectly);
                    }
                }
            }
        }
    }
#undef PrintWait
#undef PrintResult
}

//==========================================================================================================================================
/**
 * @brief turns off all possible modules
 */
//==========================================================================================================================================
void oC_ModuleMan_TurnOffAllPossible( void )
{
    for( uint32_t moduleToTurnOffIndex = 0; moduleToTurnOffIndex < oC_ARRAY_SIZE(ModuleRegistrations); moduleToTurnOffIndex++ )
    {
        bool                             someOfModulesRequireThisModule = false;
        const oC_Module_Registration_t * moduleToTurnOff                = ModuleRegistrations[moduleToTurnOffIndex];

        if(
            oC_SaveIfFalse(moduleToTurnOff->Name, moduleToTurnOff->Module != oC_Module_None, oC_ErrorCode_ModuleNotCorrect)
         && oC_Module_IsTurnedOn(moduleToTurnOff->Module) == true
          )
        {
            for( uint32_t moduleToCheckRequirements = 0 ; someOfModulesRequireThisModule == false && moduleToCheckRequirements < oC_ARRAY_SIZE(ModuleRegistrations) ; moduleToCheckRequirements++ )
            {
                const oC_Module_Registration_t * moduleToCheck = ModuleRegistrations[moduleToCheckRequirements];

                for( uint32_t i = 0; someOfModulesRequireThisModule == false && i < MAX_REQUIRED_MODULES; i++ )
                {
                    if(moduleToCheck->RequiredModules[i] == moduleToTurnOff->Module)
                    {
                        someOfModulesRequireThisModule = oC_Module_IsTurnedOn(moduleToCheck->RequiredModules[i]);
                    }
                }
            }

            if(someOfModulesRequireThisModule == false)
            {
                if(oC_SaveIfFalse("ModuleManager: Turn off function is not set - ", isaddresscorrect(moduleToTurnOff->TurnOffFunction), oC_ErrorCode_WrongAddress))
                {
                    if(oC_SaveIfErrorOccur(moduleToTurnOff->Name, moduleToTurnOff->TurnOffFunction()))
                    {
                        oC_SaveIfFalse(moduleToTurnOff->Name, oC_Module_IsTurnedOn(moduleToTurnOff->Module) == false, oC_ErrorCode_ModuleHasNotBeenStoppedCorrectly );
                    }
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief turns on the module
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ModuleMan_TurnOnModule( const char * Name )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition( isaddresscorrect(Name), oC_ErrorCode_WrongAddress))
    {
        errorCode = oC_ErrorCode_ObjectNotFoundOnList;

        oC_ARRAY_FOREACH_IN_ARRAY(ModuleRegistrations,registrationReference)
        {
            const oC_Module_Registration_t * registration = *registrationReference;
            if(
                strcmp(Name, registration->Name) == 0
             && ErrorCondition(registration->Module != oC_Module_None               , oC_ErrorCode_ModuleNotCorrect     )
             && ErrorCondition(oC_Module_IsTurnedOn(registration->Module) == false  , oC_ErrorCode_ModuleIsTurnedOn     )
              )
            {
                bool allRequiredEnabled = true;

                for( uint32_t i = 0; allRequiredEnabled && i < MAX_REQUIRED_MODULES; i++ )
                {
                    if(registration->RequiredModules[i] != oC_Module_None)
                    {
                        allRequiredEnabled = oC_Module_IsTurnedOn(registration->RequiredModules[i]);
                    }
                }

                if(ErrorCondition( allRequiredEnabled, oC_ErrorCode_RequiredModuleNotEnabled ))
                {
                    if(ErrorCondition( isaddresscorrect(registration->TurnOnFunction), oC_ErrorCode_WrongAddress ))
                    {
                        if( ErrorCode( registration->TurnOnFunction() ) )
                        {
                            errorCode = oC_ErrorCode_None;
                            ErrorCondition( oC_Module_IsTurnedOn(registration->Module), oC_ErrorCode_ModuleHasNotStartedCorrectly );
                        }
                    }
                }
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off the module
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ModuleMan_TurnOffModule( const char * Name )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition( isaddresscorrect(Name), oC_ErrorCode_WrongAddress))
    {
        errorCode = oC_ErrorCode_ObjectNotFoundOnList;

        for( uint32_t moduleToTurnOffIndex = 0; moduleToTurnOffIndex < oC_ARRAY_SIZE(ModuleRegistrations); moduleToTurnOffIndex++ )
        {
            bool                             someOfModulesRequireThisModule = false;
            const oC_Module_Registration_t * moduleToTurnOff                = ModuleRegistrations[moduleToTurnOffIndex];

            if(
                    strcmp(Name, moduleToTurnOff->Name) == 0
                 && ErrorCondition(moduleToTurnOff->Module != oC_Module_None               , oC_ErrorCode_ModuleNotCorrect      )
                 && ErrorCondition(oC_Module_IsTurnedOn(moduleToTurnOff->Module) == true   , oC_ErrorCode_ModuleNotStartedYet   )
              )
            {
                for( uint32_t moduleToCheckRequirements = 0 ; someOfModulesRequireThisModule == false && moduleToCheckRequirements < oC_ARRAY_SIZE(ModuleRegistrations) ; moduleToCheckRequirements++ )
                {
                    const oC_Module_Registration_t * moduleToCheck = ModuleRegistrations[moduleToCheckRequirements];

                    for( uint32_t i = 0; someOfModulesRequireThisModule == false && i < MAX_REQUIRED_MODULES; i++ )
                    {
                        if(moduleToCheck->RequiredModules[i] == moduleToTurnOff->Module)
                        {
                            someOfModulesRequireThisModule = oC_Module_IsTurnedOn(moduleToCheck->RequiredModules[i]);
                        }
                    }
                }

                if(ErrorCondition(someOfModulesRequireThisModule == false, oC_ErrorCode_ModuleUsedByDifferentModule))
                {
                    if(ErrorCondition( isaddresscorrect(moduleToTurnOff->TurnOffFunction), oC_ErrorCode_WrongAddress ))
                    {
                        if( ErrorCode( moduleToTurnOff->TurnOffFunction() ) )
                        {
                            errorCode = oC_ErrorCode_None;
                            ErrorCondition( oC_Module_IsTurnedOn(moduleToTurnOff->Module) == false, oC_ErrorCode_ModuleHasNotBeenStoppedCorrectly );
                        }
                    }
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns module registration
 */
//==========================================================================================================================================
const oC_Module_Registration_t * oC_ModuleMan_GetModuleRegistration( const char * Name )
{
    const oC_Module_Registration_t * foundRegistration = NULL;

    oC_ARRAY_FOREACH_IN_ARRAY(ModuleRegistrations,registrationReference)
    {
        const oC_Module_Registration_t * registration = *registrationReference;

        if(strcmp(Name,registration->Name) == 0)
        {
            foundRegistration = registration;
            break;
        }
    }

    return foundRegistration;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________




