/** ****************************************************************************************************************************************
 *
 * @file       oc_program.c
 *
 * @brief      The file with source for program module
 *
 * @author     Patryk Kubiak - (Created on: 8 09 2015 18:03:54)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_program.h>
#include <oc_stdlib.h>
#include <string.h>
#include <oc_streamman.h>
#include <oc_processman.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_Program_t        Program;
    oC_Process_t        Process;
    int                 Argc;
    char **             Argv;
    int                 Result;
    void*               GotAddress;
} ProgramContext_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

void ProgramThread( void * UserPointer );
bool CloseProcess( oC_Thread_t Thread, void * Process, uint32_t Parameter );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t Allocator = {
                .Name           = "program" ,
                .EventFlags     = 0 ,
                .EventHandler   = NULL
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief sets the address of the GOT
 * 
 * The function sets the address of the GOT (Global Offset Table) for the program.
 * 
 * @param programContext    context of the program
 * @param got               address of the GOT
 * 
 * @return error code
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Program_SetGotAddress( void* programContext, void* got )
{
    ProgramContext_t* context = programContext;
    context->GotAddress = got;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief creates a new process for the program
 * 
 * The function creates a new process for the given program.
 * 
 * @param Program           program
 * @param User              user
 * 
 * @return process
 * 
 * @note 
 * In case of error check the error code from #oC_ReadLastError
 */
//==========================================================================================================================================
oC_Process_t oC_Program_NewProcess( oC_Program_t Program , oC_User_t User )
{
    oC_Process_t process = NULL;

    if(oC_Program_IsCorrect(Program) && oC_User_IsCorrect(User))
    {
        oC_Stream_t _stdin  = oC_StreamMan_GetStream(Program->StdInName);
        oC_Stream_t _stdout = oC_StreamMan_GetStream(Program->StdOutName);
        oC_Stream_t _stderr = oC_StreamMan_GetStream(Program->StdErrName);
        process = oC_Process_New(Program->Priority,Program->Name,User,Program->HeapMapSize,_stdin,_stdout,_stderr);

        if(process != NULL)
        {
            oC_SaveIfFalse( "Program::NewProcess - cannot set memory limit "    , oC_Process_SetAllocationLimit   (process, Program->AllocationLimit ) , oC_ErrorCode_ObjectNotCorrect);
            oC_SaveIfFalse( "Program::NewProcess - cannot set memory tracking " , oC_Process_SetAllocationTracking(process, Program->TrackAllocations) , oC_ErrorCode_ObjectNotCorrect);
        }
    }

    return process;
}

//==========================================================================================================================================
/**
 * @brief creates a new program
 * 
 * The function creates a new program.
 * 
 * @param Priority          priority of the program
 * @param Name              name of the program
 * @param MainFunction      main function of the program
 * @param StdInName         name of the standard input stream
 * @param StdOutName        name of the standard output stream
 * @param StdErrName        name of the standard error stream
 * @param HeapMapSize       size of the heap map
 * 
 * @return program
 */
//==========================================================================================================================================
oC_Program_t oC_Program_New( oC_Program_Priority_t Priority , const char * Name , oC_Program_MainFunction_t MainFunction , const char * StdInName , const char * StdOutName , const char * StdErrName , oC_UInt_t HeapMapSize )
{
    oC_Program_Registration_t * program = NULL;

    if(
        isaddresscorrect(Name) &&
        isaddresscorrect(MainFunction) &&
        isaddresscorrect(StdInName) &&
        isaddresscorrect(StdOutName) &&
        isaddresscorrect(StdOutName)
        )
    {
        program = ksmartalloc(sizeof(oC_Program_Registration_t),&Allocator,AllocationFlags_NoWait);

        if(program)
        {
            program->Priority       = Priority;
            program->MainFunction   = MainFunction;
            program->HeapMapSize    = HeapMapSize;
            program->ThreadStackSize= 0;

            strncpy(program->Name,Name,sizeof(program->Name));

            program->StdInName = ksmartalloc(strlen(StdInName)+1 , &Allocator , AllocationFlags_NoWait);
            strcpy(program->StdInName,StdInName);
            program->StdOutName = ksmartalloc(strlen(StdOutName)+1 , &Allocator , AllocationFlags_NoWait);
            strcpy(program->StdOutName,StdOutName);
            program->StdErrName = ksmartalloc(strlen(StdErrName)+1 , &Allocator , AllocationFlags_NoWait);
            strcpy(program->StdErrName,StdErrName);
        }
    }

    return program;
}

//==========================================================================================================================================
/**
 * @brief deletes the program
 * 
 * The function deletes the program.
 * 
 * @param Program           program
 * 
 * @return true if deleted
 * 
 * @warning It can be used only for a programs created by the #oC_Program_New function (does not work for static programs)
 */
//==========================================================================================================================================
bool oC_Program_Delete( oC_Program_t Program )
{
    bool deleted = false;

    if(oC_MemMan_IsDynamicAllocatedAddress(Program))
    {
        deleted = true;
        if(oC_MemMan_IsDynamicAllocatedAddress(Program->Name))
        {
            if(!ksmartfree((void*)Program->Name,strlen(Program->Name)+1,AllocationFlags_CanWaitForever))
            {
                deleted = false;
            }
        }

        if(oC_MemMan_IsDynamicAllocatedAddress(Program->StdInName))
        {
            if(!ksmartfree((void*)Program->StdInName,strlen(Program->StdInName)+1,AllocationFlags_CanWaitForever))
            {
                deleted = false;
            }
        }

        if(oC_MemMan_IsDynamicAllocatedAddress(Program->StdOutName))
        {
            if(!ksmartfree((void*)Program->StdOutName,strlen(Program->StdOutName)+1,AllocationFlags_CanWaitForever))
            {
                deleted = false;
            }
        }

        if(oC_MemMan_IsDynamicAllocatedAddress(Program->StdErrName))
        {
            if(!ksmartfree((void*)Program->StdErrName,strlen(Program->StdErrName)+1,AllocationFlags_CanWaitForever))
            {
                deleted = false;
            }
        }

        if(!ksmartfree((void*)Program,sizeof(oC_Program_Registration_t),AllocationFlags_CanWaitForever))
        {
            deleted = false;
        }
    }

    return deleted;
}
//==========================================================================================================================================
/**
 * @brief checks if the program is correct
 */
//==========================================================================================================================================
bool oC_Program_IsCorrect( oC_Program_t Program )
{
    return isaddresscorrect(Program);
}

//==========================================================================================================================================
/**
 * @brief alternative version of the function for executing the program
 * 
 * The function executes the program.
 * 
 * @note
 * The function is an alternative version of the #oC_Program_Exec function with the additional parameter for the GOT address.
 * 
 * @param Program           program
 * @param Process           process
 * @param Argc              number of arguments
 * @param Argv              arguments
 * @param outMainThread     Destination for the main thread
 * @param outProgramContext Destination for the program context
 * @param got               address of the GOT
 * 
 * @return error code or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ObjectNotCorrect     | Program or process object is not correct
 *  #oC_ErrorCode_WrongAddress         | Wrong address
 *  #oC_ErrorCode_OutputAddressNotInRAM| Output address is not in RAM (outMainThread or outProgramContext)
 *  #oC_ErrorCode_AllocationError      | Memory could not be allocated (or #oC_Thread_New failed)
 *  #oC_ErrorCode_CannotRunThread      | Main thread could not be run
 * 
 * @note
 * More error codes can be returned by the #oC_ProcessMan_AddProcess function
 * 
 * @see oC_Program_Exec, oC_ProcessMan_AddProcess 
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Program_Exec2( oC_Program_t Program , oC_Process_t Process , int Argc , const char ** Argv , oC_Thread_t * outMainThread , void ** outProgramContext, void* got )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Program_IsCorrect(Program)                         , oC_ErrorCode_ObjectNotCorrect      ) 
     && ErrorCondition( isaddresscorrect(Argv)                                , oC_ErrorCode_WrongAddress          ) 
     && ErrorCondition( oC_Process_IsCorrect(Process)                         , oC_ErrorCode_ObjectNotCorrect      ) 
     && ErrorCondition( outProgramContext == NULL || isram(outProgramContext) , oC_ErrorCode_OutputAddressNotInRAM ) 
     && ErrorCondition( outMainThread     == NULL || isram(outMainThread)     , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        ProgramContext_t *  programContext  = kmalloc(sizeof(ProgramContext_t), oC_Process_GetAllocator(Process) ,AllocationFlags_CanWait1Second);
        Allocator_t         allocator       = oC_Process_GetAllocator(Process);

        if(ErrorCondition(programContext, oC_ErrorCode_AllocationError))
        {
            programContext->Process = Process;
            programContext->Program = Program;
            programContext->Argv    = kmalloc(Argc * sizeof(char*),allocator,AllocationFlags_CanWait1Second);
            programContext->Argc    = Argc;
            programContext->GotAddress = got;

            /*
             * This is the loop without repeats - just for simply assignments of errors
             */
            do
            {
                errorCode = oC_ErrorCode_None;

                if(isram(programContext->Argv)==false)
                {
                    errorCode = oC_ErrorCode_AllocationError;
                    break;
                }

                for(int argumentIndex = 0; argumentIndex < Argc && !oC_ErrorOccur(errorCode) ; argumentIndex++ )
                {
                    if(ErrorCondition(isaddresscorrect(Argv[argumentIndex]), oC_ErrorCode_WrongAddress))
                    {
                        programContext->Argv[argumentIndex] = kmalloc(strlen(Argv[argumentIndex])+1,allocator,AllocationFlags_CanWait500Msecond);

                        if(ErrorCondition(isram(programContext->Argv[argumentIndex]), oC_ErrorCode_AllocationError))
                        {
                            strcpy(programContext->Argv[argumentIndex],Argv[argumentIndex]);
                        }
                    }
                }

                if(oC_ErrorOccur(errorCode))
                {
                    break;
                }

                oC_Thread_t thread = oC_Thread_New( oC_Process_GetPriority(Process) ,Program->ThreadStackSize,Process,Program->Name,ProgramThread,programContext);

                if(thread==NULL)
                {
                    kfree(programContext,AllocationFlags_CanWaitForever);
                    errorCode = oC_ErrorCode_AllocationError;
                    break;
                }

                if(oC_ProcessMan_ContainsProcess(Process) == false)
                {
                    errorCode = oC_ProcessMan_AddProcess(Process);
                }

                if(!oC_ErrorOccur(errorCode) && oC_ProcessMan_ContainsProcess(Process))
                {
                    if(ErrorCondition(oC_Thread_Run(thread), oC_ErrorCode_CannotRunThread))
                    {
                        if(outMainThread != NULL)
                        {
                            *outMainThread  = thread;
                        }
                        if(outProgramContext != NULL)
                        {
                            *outProgramContext = programContext;
                        }

                        oC_SaveIfFalse("Cannot save CloseProcess to revert", oC_Thread_SaveToRevert(thread,CloseProcess,Process,0), oC_ErrorCode_InternalDataAreDamaged);
                        errorCode       = oC_ErrorCode_None;
                    }
                }
                else
                {
                    oC_SaveIfFalse("Thread", oC_Thread_Delete(&thread) , oC_ErrorCode_ReleaseError);
                }
            } while(0);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Executes the program
 * 
 * The function executes the program.
 * 
 * @param Program           program
 * @param Process           process
 * @param Argc              number of arguments
 * @param Argv              arguments
 * @param outMainThread     Destination for the main thread
 * @param outProgramContext Destination for the program context
 * 
 * @return error code or #oC_ErrorCode_None if success
 * 
 * @note List of possible error codes is the same as for the #oC_Program_Exec2 function
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Program_Exec( oC_Program_t Program , oC_Process_t Process , int Argc , const char ** Argv , oC_Thread_t * outMainThread , void ** outProgramContext )
{
    return oC_Program_Exec2(Program,Process,Argc,Argv,outMainThread, outProgramContext, NULL);
}

//==========================================================================================================================================
/**
 * @brief gets the name of the program
 * 
 * The function gets the name of the program.
 * 
 * @param Program           program
 * 
 * @return name of the program
 */
//==========================================================================================================================================
const char * oC_Program_GetName( oC_Program_t Program )
{
    const char * name = "program not correct";

    if(oC_Program_IsCorrect(Program))
    {
        name = Program->Name;
    }

    return name;
}
//==========================================================================================================================================
/**
 * @brief gets the name of the standard input stream
 * 
 * The function gets the name of the standard input stream.
 * 
 * @param Program           program
 * 
 * @return name of the standard input stream
 */
//==========================================================================================================================================
const char * oC_Program_GetStdInName( oC_Program_t Program )
{
    const char * name = "program not correct";

    if(oC_Program_IsCorrect(Program))
    {
        name = Program->StdInName;
    }

    return name;
}
//==========================================================================================================================================
/**
 * @brief gets the name of the standard output stream
 * 
 * The function gets the name of the standard output stream.
 * 
 * @param Program           program
 * 
 * @return name of the standard output stream
 */
//==========================================================================================================================================
const char * oC_Program_GetStdOutName( oC_Program_t Program )
{
    const char * name = "program not correct";

    if(oC_Program_IsCorrect(Program))
    {
        name = Program->StdOutName;
    }

    return name;
}
//==========================================================================================================================================
/**
 * @brief gets the name of the standard error stream
 * 
 * The function gets the name of the standard error stream.
 * 
 * @param Program           program
 * 
 * @return name of the standard error stream
 */
//==========================================================================================================================================
const char * oC_Program_GetStdErrName( oC_Program_t Program )
{
    const char * name = "program not correct";

    if(oC_Program_IsCorrect(Program))
    {
        name = Program->StdErrName;
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief gets the priority of the program
 * 
 * The function gets the priority of the program.
 * 
 * @param Program           program
 * 
 * @return priority of the program
 */
//==========================================================================================================================================
oC_Program_Priority_t oC_Program_GetPriority( oC_Program_t Program )
{
    oC_Program_Priority_t priority = 0;

    if(oC_Program_IsCorrect(Program))
    {
        priority = Program->Priority;
    }

    return priority;
}

//==========================================================================================================================================
/**
 * @brief gets the size of the heap map
 * 
 * The function gets the size of the heap map.
 * 
 * @param Program           program
 * 
 * @return size of the heap map
 */
//==========================================================================================================================================
oC_MemorySize_t oC_Program_GetHeapMapSize( oC_Program_t Program )
{
    oC_MemorySize_t memorySize = 0;

    if(oC_Program_IsCorrect(Program))
    {
        memorySize = Program->HeapMapSize;
    }

    return memorySize;
}

//==========================================================================================================================================
/**
 * @brief waits for the program to finish
 * 
 * The function waits for the program to finish.
 * 
 * @param Context           context of the program
 * @param CheckPeriod       period of checking
 * @param Timeout           timeout
 * 
 * @return Code of error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_AddressNotInRam      | Context is not in RAM
 *  #oC_ErrorCode_ImplementError       | Error in implementation
 * 
 * @note
 * More error codes can be returned by the #oC_Process_WaitForFinish function or by the program itself
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Program_WaitForFinish( void * Context , oC_Time_t CheckPeriod , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(isram(Context), oC_ErrorCode_AddressNotInRam))
    {
        ProgramContext_t * context = Context;

        if(ErrorCode(oC_Process_WaitForFinish(context->Process,CheckPeriod,Timeout)))
        {
            errorCode = (oC_ErrorCode_t)context->Result;
        }
    }

    return errorCode;
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief The function for the main thread of the program
 * 
 * The function is the main thread of the program.
 * 
 * @param UserPointer       user pointer
 */
//==========================================================================================================================================
void ProgramThread( void * UserPointer )
{
    ProgramContext_t * programContext = UserPointer;

    if(isaddresscorrect(programContext) && isaddresscorrect(programContext->Program) && isaddresscorrect(programContext->Program->MainFunction))
    {
        if (programContext->GotAddress != NULL)
        {
            oC_UInt_t r;

            __asm volatile (
                  "MOV r9,     %1           \n\t" /* Move the pointer to the r9, because it will be overwritten */
                        : "=r" (r)
                        : "r"  (programContext->GotAddress)
            );
        }
        programContext->Result = programContext->Program->MainFunction(programContext->Argc,(const char **)programContext->Argv);

        kdebuglog( oC_LogType_Track, "Process %s has been finished with result: %d\n", oC_Process_GetName(programContext->Process), programContext->Result);

        if(programContext->Result != 0)
        {
            oC_SaveError(programContext->Program->Name,(oC_ErrorCode_t)programContext->Result);
        }

        if(oC_Process_Kill(programContext->Process)==false)
        {
            oC_SaveError("Cannot kill process " , oC_ErrorCode_ReleaseError);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief The function for closing the process
 * 
 * The function closes the process.
 * 
 * @param Thread            thread
 * @param Process           process
 * @param Parameter         parameter
 * 
 * @return true if success
 */
//==========================================================================================================================================
bool CloseProcess( oC_Thread_t Thread, void * Process, uint32_t Parameter )
{
    return oC_Process_Kill(Process);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
