/** ****************************************************************************************************************************************
 *
 * @file       oc_memman.c
 *
 * @brief      The file with sources for memory manager.
 *
 * @author     Patryk Kubiak - (Created on: 6 06 2015 12:15:56)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_memman.h>
#include <oc_intman.h>
#include <oc_mem_lld.h>
#include <oc_sys_lld.h>
#include <oc_bits.h>
#include <oc_memory_cfg.h>
#include <oc_threadman.h>
#include <oc_array.h>
#include <oc_null.h>
#include <oc_ktime.h>
#include <oc_ifnot.h>
#include <oc_debug.h>
#include <string.h>

/** ========================================================================================================================================
 *
 *              The section with local definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_DEFINITIONS_SECTION_________________________________________________________________

//==========================================================================================================================================
/**
 * @hideinitializer
 * This is the special number, that helps to protect allocated memory before overflow.
 */
//==========================================================================================================================================
#define OVERFLOW_PROTECTION_MAGIC_NUMBER    ((oC_UInt_t)0xfeedface)

//==========================================================================================================================================
/**
 * @hideinitializer
 * This is the macro that helps to prepare loop for each block in the global block list.
 *
 * @param block     name of the block variable, that will be defined for each iteration
 *
 * Example of usage:
 * @code{.c}
 * foreach_block(block)
 * {
 *     if(block->Allocator == Allocator)
 *     {
 *         found = true;
 *         break;
 *     }
 * }
 * @endcode
 */
//==========================================================================================================================================
#define foreach_block(block)                for(Block_t * block = BlockListHead; (block != NULL) ; block = block->Next)

//==========================================================================================================================================
/**
 * @hideinitializer
 * Reference to the last word of buffer on heap.
 *
 * @param Buffer    Buffer pointer
 * @param Size      Size of the buffer in bytes
 *
 * Example of usage:
 * @code{.c}
 * uint8_t Buffer[10];
 *
 * LAST_WORD_OF_BUFFER(Buffer,10) = 0xFF;
 *
 * @endcode
 */
//==========================================================================================================================================
#define LAST_WORD_OF_BUFFER(Buffer,Size)    ((oC_UInt_t*)Buffer)[ALIGN_SIZE(Size,oC_MEM_LLD_MEMORY_ALIGNMENT)/sizeof(oC_UInt_t)]

//==========================================================================================================================================
/**
 * The macro returns size aligned to the current memory alignment
 */
//==========================================================================================================================================
#define ALIGN_SIZE(SIZE,ALIGNMENT)          ((((oC_UInt_t)SIZE) + ALIGNMENT - 1) & ~(ALIGNMENT-1))

#undef  _________________________________________LOCAL_DEFINITIONS_SECTION_________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

//==========================================================================================================================================
/**
 * The structure that is created for each allocation.
 */
//==========================================================================================================================================
typedef struct _Block_t
{
    struct _Block_t *               Next;               /**< Pointer to the next block on the list */
    Allocator_t                     Allocator;          /**< Pointer to the allocator */
    void *                          Address;            /**< Allocated address */
    oC_UInt_t                       Size;               /**< Size of allocation */
    const char *                    Function;           /**< Name of the function that called allocation */
    uint32_t                        LineNumber;         /**< Allocation function call line number */
    AllocationFlags_t               AllocationFlags;    /**< Flags used for allocation */
} Block_t;

typedef struct FreeArea_t
{
    struct FreeArea_t *             Next;
    oC_UInt_t                       Size;
    void *                          Address;
} FreeArea_t;

//==========================================================================================================================================
/**
 * Variable that stores information about usage of heap
 */
//==========================================================================================================================================
typedef enum
{
    HeapState_NotUsed  = false, //!< heap is not used
    HeapState_Used     = true   //!< heap is used
} HeapState_t;

//==========================================================================================================================================
/**
 * Structure that stores pointers for handling heap
 *
 * @code
 * Real start of machine heap _ _ +-----------------------------------+
 *                                |                                   |   Bit map - each bit in this section is reserved for one word (oC_UInt_t)
 *                                |              BIT MAP              |   in HEAP section. If bit is set, then the word is used (allocated)
 *                                |                                   |
 *                                +-----------------------------------+
 *                                |                                   |   Heap - memory for allocations.
 *                                |               HEAP                |
 *                                |                                   |
 * Real end of machine heap   _ _ +-----------------------------------+
 * @endcode
 */
//==========================================================================================================================================
typedef struct _HeapMap_t
{
    struct _HeapMap_t * Self;                   /**< Self reference for protection again not initialized pointers */
    uint8_t *           BitMapStart;            /**< Pointer to the start of bit map (for mapping heap) */
    uint8_t *           BitMapEnd;              /**< Pointer to the end of bit map (for mapping heap)*/
    oC_UInt_t *         HeapStart;              /**< Pointer to the start of heap (for allocations) */
    oC_UInt_t *         HeapEnd;                /**< Pointer to the end of heap (for allocations) */
    struct
    {
        FreeArea_t *    First;
        FreeArea_t *    Last;
    } FreeArea;
} HeapMap_t;

#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________

static bool             BlockModule                 ( AllocationFlags_t Flags );
static bool             UnblockModule               ( void );
static bool             WaitForMemory               ( HeapMap_t * Map , oC_UInt_t Size , oC_UInt_t Alignment , AllocationFlags_t Flags );
static oC_ErrorCode_t   InitializePointers          ( bool FillZero );
static oC_ErrorCode_t   PrepareCoreHeapMap          ( void );
static oC_ErrorCode_t   InitializeHeapMap           ( HeapMap_t * Map , void * HeapStartPointer , void * HeapEndPointer , oC_UInt_t HeapSize , bool FillZero );
static Block_t *        FindBlockOfAddress          ( const void * Address );
static Block_t *        FindBlockContainsAddress    ( const void * Address );
static oC_UInt_t        GetIndexOfHeapPointer       ( HeapMap_t * Map , oC_UInt_t * HeapPointer );
static inline bool      IsAddressOfHeapMap          ( HeapMap_t * Map , const void * Address );
static inline bool      IsBlockCorrect              ( Block_t * Block );
static inline bool      IsHeapUsed                  ( HeapMap_t * Map , oC_UInt_t * HeapPointer );
static void             DumpHeapMap                 ( HeapMap_t * Map , oC_MemMan_DumpFunction_t DumpFunction);
static inline bool      IsHeapMapInitialized        ( HeapMap_t * Map );
static inline void      SetHeapState                ( HeapMap_t * Map , oC_UInt_t * HeapPointer , HeapState_t HeapState );
static bool             CheckIfHeapInRangeIsInState ( HeapMap_t * Map , oC_UInt_t * HeapFrom , oC_UInt_t * HeapTo , HeapState_t HeapState );
static inline void      SetHeapStateInRange         ( HeapMap_t * Map , oC_UInt_t * HeapFrom , oC_UInt_t * HeapTo , HeapState_t HeapState );
static oC_UInt_t *      FindNextHeapPointerInState  ( HeapMap_t * Map , oC_UInt_t * HeapPointer , oC_UInt_t * HeapEndLimit , HeapState_t HeapState );
static oC_UInt_t        GetSizeOfHeapInState        ( HeapMap_t * Map , HeapState_t HeapState );
static oC_UInt_t        GetSizeOfHeap               ( HeapMap_t * Map );
static void             AddBufferToFreeAreaList     ( HeapMap_t * Map, void * HeapPointer, oC_UInt_t Size );
static void *           FindBufferOnFreeAreaList    ( HeapMap_t * Map, oC_UInt_t Size , oC_UInt_t Alignment , bool Take );
static void             UpdateFreeAreaList          ( HeapMap_t * Map );
static void *           FindFreeHeap                ( HeapMap_t * Map , oC_UInt_t Size , oC_UInt_t Alignment );
static void *           RawAlloc                    ( HeapMap_t * Map , oC_UInt_t Size , oC_UInt_t Alignment );
static bool             RawFree                     ( HeapMap_t * Map , void * Address , oC_UInt_t Size);
static bool             IsMemoryValid               ( void * Start , oC_UInt_t Size );
static void             CallAllocatorEvent          ( Allocator_t Allocator , void * Address , MemoryEventFlags_t EventFlags , const char * Function, uint32_t LineNumber );
static void             CallEvent                   ( void * Address , MemoryEventFlags_t EventFlags , const char * Function, uint32_t LineNumber );
static void             MemoryFaultInterrupt        ( void );
static void             BusFaultInterrupt           ( void );
static bool             CanReleaseMemory            ( Block_t * Block );

#undef  _________________________________________LOCAL_FUNCTIONS_PROTOTYPES_SECTION_________________________________________________________
/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

static bool                     ModuleTestModeEnabledFlag      = false; /**< The flag that is set, when test mode is active */
static bool                     ModuleEnabledFlag              = false; /**< This flag stores information if the module is turned on. */
static uint32_t                 ModuleBusyFlag                 = false; /**< Module is already busy, no operation is possible */
static bool                     FirstPowerOn                   = false;  /**< The module is turned on the first time after power on, so the memory is filled with 0 */
static uint32_t *               DataOverflowProtection         = NULL;  /**< Stores magic number at the start of machine heap. If it is overwritten, then it means, that data section was overwritten  */
static Block_t *                BlockListHead                  = NULL;  /**< Head element of the block list */
static Block_t *                BlockListTail                  = NULL;  /**< Tail element of the block list */
static MemoryEventHandler_t     ModuleEventHandler             = NULL;  /**< Pointer to the function that is handling module events */
static HeapMap_t                HeapMap;                                /**< Main heap map */
static HeapMap_t                ExternalHeapMap;                        /**< External heap map */
static HeapMap_t                DmaHeapMap;                             /**< DMA heap map */
static HeapMap_t                CoreHeapMap;                            /**< Heap map for core important processes */
static float                    PanicMemoryExhaustedLimit      = CFG_PERCENT_PANIC_MEMORY_EXHAUSTED_LIMIT;
static float                    DefaultMemoryExhaustedLimit    = CFG_PERCENT_DEFAULT_MEMORY_EXHAUSTED_LIMIT;

#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup MemMan
//! @{

//==========================================================================================================================================
/**
 * @brief turns on memory manager
 *
 * The function is for turning on memory manager module. Note, that it also turns on #MEM-LLD module. The function protects before returning
 * on this module. If module is already turned on, the `oC_ErrorCode_ModuleIsTurnedOn` will be returned. The module must be turned on before
 * usage. It initialized pointers and interrupts that are assigned for the module.
 *
 * @return one of following codes should be expected:
 * Code                                 | Description
 * -------------------------------------|----------------------------------------
 * oC_ErrorCode_None                    | There is no error
 * oC_ErrorCode_ModuleIsTurnedOn        | The MemMan was enabled before
 * oC_ErrorCode_ModuleIsTestedAlready   | Tests of module are active now
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(!ModuleEnabledFlag)
    {
        if(!ModuleTestModeEnabledFlag)
        {
            errorCode = oC_MEM_LLD_TurnOnDriver();

            if(errorCode == oC_ErrorCode_None || errorCode == oC_ErrorCode_ModuleIsTurnedOn)
            {
                if(
                    oC_AssignErrorCode(&errorCode , InitializePointers(!FirstPowerOn)) &&
                    oC_AssignErrorCode(&errorCode , PrepareCoreHeapMap()) &&
                    oC_AssignErrorCode(&errorCode , oC_MEM_LLD_SetBusFaultInterrupt(BusFaultInterrupt)) &&
                    oC_AssignErrorCode(&errorCode , oC_MEM_LLD_TurnOnBusFaultInterrupt()) &&
                    oC_AssignErrorCode(&errorCode , oC_MEM_LLD_SetMemoryFaultInterrupt(MemoryFaultInterrupt)) &&
                    oC_AssignErrorCode(&errorCode , oC_MEM_LLD_TurnOnMemoryFaultInterrupt())
                    )
                {
                    ModuleBusyFlag              = false;
                    ModuleEnabledFlag           = true;
                    ModuleTestModeEnabledFlag   = false;
                    FirstPowerOn                = false;

                    errorCode            = oC_ErrorCode_None;
                }
            }
        }
        else
        {
            errorCode   = oC_ErrorCode_ModuleIsTestedAlready;
        }
    }
    else
    {
        errorCode = oC_ErrorCode_ModuleIsTurnedOn;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off the module
 *
 * The function is for turning off the module. If the module is already not turned on, the `oC_ErrorCode_ModuleNotStartedYet` code will be
 * returned. Note, that also in case of error, the module is treated as turned off. All allocated memory will be released, and the
 * #MemoryEventFlags_MemoryReleased event will be called. While this function, also the #MemoryEventFlags_ModuleTurningOff event is generated
 * to inform the system, that memory allocation is not possible anymore.
 *
 * @see oC_MemMan_TurnOn
 *
 * @return one of following codes should be expected:
 * Code                              | Description
 * ----------------------------------|----------------------------------------
 * oC_ErrorCode_None                 | There is no error
 * oC_ErrorCode_ModuleNotStartedYet  | The MemMan was not enabled before
 *
 * Other error codes are from the #oC_MEM_LLD_TurnOffDriver function.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ModuleEnabledFlag)
    {
        if(!ModuleTestModeEnabledFlag)
        {
            ModuleEnabledFlag   = false;

            foreach_block(block)
            {
                CallAllocatorEvent(block->Allocator, block->Address, MemoryEventFlags_MemoryReleased, block->Function, block->LineNumber);
            }

            CallEvent(NULL,MemoryEventFlags_ModuleTurningOff,"",0);

            ModuleEventHandler  = NULL;
            errorCode           = oC_MEM_LLD_TurnOffDriver();
        }
        else
        {
            errorCode   = oC_ErrorCode_ModuleIsTestedAlready;
        }
    }
    else
    {
        errorCode = oC_ErrorCode_ModuleNotStartedYet;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief scan each block to check if the overflow event not occurs.
 *
 * The function is for checking if there was an overflow event. It scans each allocated block in the memory to check if the overflow protection number
 * was not overwritten. Of course it is not perfect way, but still better, than nothing. When the memory was overwritten, the protection magic
 * number is written again, and buffer overflow event is generated for both: module and allocator handlers. The function only works with enabled module.
 */
//==========================================================================================================================================
void oC_MemMan_CheckOverflow(void)
{
    if(ModuleEnabledFlag)
    {
        if(*DataOverflowProtection != OVERFLOW_PROTECTION_MAGIC_NUMBER)
        {
            CallEvent(NULL,MemoryEventFlags_DataSectionOverflow,"",0);
        }

        foreach_block(block)
        {
            if(LAST_WORD_OF_BUFFER(block->Address,block->Size) != OVERFLOW_PROTECTION_MAGIC_NUMBER)
            {
                if ((block->AllocationFlags & AllocationFlags_DontRepair) == 0)
                {
                    LAST_WORD_OF_BUFFER(block->Address,block->Size) = OVERFLOW_PROTECTION_MAGIC_NUMBER;
                    CallEvent(block->Address,MemoryEventFlags_BufferOverflow,block->Function,block->LineNumber);
                }
                CallAllocatorEvent(block->Allocator,block->Address,MemoryEventFlags_BufferOverflow,block->Function,block->LineNumber);
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief checks if memory exhausted event not occurs.
 *
 * The function is for checking and generating the memory exhausted event. It checks if size of free memory is greater than minimum. If not,
 * the Memory Exhausted event is generated. Moreover, when the size of free memory is smaller than panic limit, then the #MemoryEventFlags_PanicMemoryExhausted
 * event is generated. The function only works with enabled module.
 */
//==========================================================================================================================================
void oC_MemMan_CheckMemoryExhausted( void )
{
    if(ModuleEnabledFlag)
    {
        float freeSize         = ((float)GetSizeOfHeapInState(&HeapMap,HeapState_NotUsed)) + ((float)GetSizeOfHeapInState(&CoreHeapMap,HeapState_NotUsed));
        float size             = (float)GetSizeOfHeap(&HeapMap);

        if((freeSize/size) < PanicMemoryExhaustedLimit)
        {
            CallEvent(NULL,MemoryEventFlags_PanicMemoryExhausted,"",0);
        }
        else if((freeSize/size) < DefaultMemoryExhaustedLimit)
        {
            CallEvent(NULL,MemoryEventFlags_MemoryExhausted,"",0);
        }
        if(IsHeapMapInitialized(&ExternalHeapMap))
        {
            float externalFreeSize = (float)GetSizeOfHeapInState(&ExternalHeapMap,HeapState_NotUsed);
            float externalSize     = (float)GetSizeOfHeap(&ExternalHeapMap);

            if((externalFreeSize/externalSize) < PanicMemoryExhaustedLimit)
            {
                CallEvent(NULL,MemoryEventFlags_PanicExternalMemoryExhausted,"",0);
            }
            else if((externalFreeSize/externalSize) < DefaultMemoryExhaustedLimit)
            {
                CallEvent(NULL,MemoryEventFlags_ExternalMemoryExhausted,"",0);
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief checks if memory is not leaking
 *
 * The function is for checking and generating the memory leak fault. It try to find memory leakage, and generate a allocator event, when it
 * suspects something.
 */
//==========================================================================================================================================
void oC_MemMan_CheckMemoryLeak( void )
{
    if(ModuleEnabledFlag)
    {

    }
}

//==========================================================================================================================================
/**
 * @brief diagnoses main heap
 *
 * This function is created for special situations, when some error event occurs during work of module. It tests module state, and try to
 * repair it, if it is possible. The function tests all pointers, checks ranges, and try to establish what is not correct. It try to repair
 * module without loosing any data, but when it is not possible, the memory can be dumped using function given as argument `DumpFunction`.
 * The function will return value `oC_ErrorCode_None`, when module is not damaged, or when the repair operation was successful without lost
 * of any data. When during repair operation some data were lost, but module is repaired, the `oC_ErrorCode_SomeDataLost` code will be returned.
 * When repair is not possible, then `oC_ErrorCode_ModuleNeedRestart` will be returned.
 *
 * @param DumpFunction      [optional - NULL if not used]   Pointer to the function for dumping data
 *
 * @return code of error (look at description for more details)
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_TestAndRepairMainHeap( oC_MemMan_DumpFunction_t DumpFunction )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ModuleEnabledFlag)
    {
        BlockModule(AllocationFlags_ForceBlock);
        MemoryEventHandler_t EventHandlerBackup = ModuleEventHandler;
        Block_t *            BlockHeadBackup    = BlockListHead;
        ModuleTestModeEnabledFlag               = true;
        errorCode                               = InitializePointers(false);

        if(errorCode == oC_ErrorCode_None)
        {
            ModuleEventHandler  = EventHandlerBackup;

            for(Block_t * block=BlockHeadBackup;IsAddressOfHeapMap(&HeapMap,block);block = block->Next)
            {
                if(IsBlockCorrect(block))
                {
                    if(BlockListTail)
                    {
                        BlockListTail->Next = block;
                    }
                    if(BlockListHead == NULL)
                    {
                        BlockListHead = block;
                    }
                    BlockListTail = block;
                }
                else
                {
                    errorCode = oC_ErrorCode_SomeDataLost;
                    CallAllocatorEvent(block->Allocator, block->Address, MemoryEventFlags_MemoryFault,block->Function, block->LineNumber);
                    CallAllocatorEvent(block->Allocator, block->Address, MemoryEventFlags_MemoryReleased, block->Function, block->LineNumber);
                }
            }


            if( errorCode == oC_ErrorCode_ModuleNeedRestart || errorCode == oC_ErrorCode_SomeDataLost )
            {
                DumpHeapMap(&HeapMap,DumpFunction);
            }

            if ( errorCode == oC_ErrorCode_None || errorCode == oC_ErrorCode_SomeDataLost)
            {
                for(uint8_t * bitMapPointer = HeapMap.BitMapStart;bitMapPointer<HeapMap.BitMapEnd;bitMapPointer++)
                {
                    *bitMapPointer = 0;
                }

                foreach_block(block)
                {
                    SetHeapStateInRange(&HeapMap,(oC_UInt_t*)block,(oC_UInt_t*)block+sizeof(Block_t),HeapState_Used);
                    SetHeapStateInRange(&HeapMap,block->Address,block->Address+block->Size+sizeof(uint32_t),HeapState_Used);
                }
            }
        }
        else
        {
            DumpHeapMap(&HeapMap,DumpFunction);
        }

        ModuleTestModeEnabledFlag    = false;
        UnblockModule();
    }
    else
    {
        errorCode = oC_ErrorCode_ModuleNotStartedYet;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief allocates memory for new heap map
 *
 * The function is for creating and initializing new heap map. The heap map can be used for saving memory in some cases (for example, when size
 * of allocation is known every time during free'ing memory, and it is really small). This function reserve some heap space for future use in functions
 * #oC_MemMan_RawAllocate and #oC_MemMan_RawFree. Note, that all memory in the heap map is allocated in the system for the **Allocator** given
 * in this function. Look at description of the **MemMan** module for more informations. Remember, that each heap map, that was allocated using
 * this function, must be free by #oC_MemMan_FreeHeapMap when it is not needed anymore.
 *
 * @param Size              Size of the heap map to reserve. It must be more than 0. Remember, that some of this memory will be reserved for
 *                          bit map, so you should set this to value `(x + x/(8*sizeof(oC_UInt_t))) = Size`, where the `x` is the size, that
 *                          you will need for raw allocations.
 * @param Allocator         Pointer to the allocator structure. It cannot be set to NULL!
 * @param LineNumber        Number of line, where the function is called. It is recommended to set it to __LINE__
 * @param Flags             Flags of this allocation.
 *
 * @return address on heap to the heap map, when success, or #NULL pointer, when there was some problem.
 */
//==========================================================================================================================================
oC_HeapMap_t oC_MemMan_AllocateHeapMap( oC_UInt_t Size , Allocator_t Allocator , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    oC_HeapMap_t heapMap = NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && (Size > 0) && oC_MemMan_IsAllocatorCorrect(Allocator) && (LineNumber > 0))
    {
        void * heapBuffer   = oC_MemMan_Allocate(Size , Allocator , Function, LineNumber , Flags , oC_MEM_LLD_MEMORY_ALIGNMENT );

        if(heapBuffer != NULL)
        {
            heapMap = oC_MemMan_Allocate(sizeof(HeapMap_t) , Allocator , Function, LineNumber , Flags , oC_MEM_LLD_MEMORY_ALIGNMENT );
            void * heapBufferEnd  = heapBuffer + Size;

            if(heapMap != NULL)
            {
                if(oC_ErrorOccur( InitializeHeapMap(heapMap , heapBuffer , heapBufferEnd , Size , true) ))
                {
                    oC_MemMan_Free(heapBuffer,Flags);
                    oC_MemMan_Free(heapMap,Flags);

                    heapMap = NULL;
                }
            }
            else
            {
                oC_MemMan_Free(heapBuffer,Flags);
            }
        }
    }

    return heapMap;
}

//==========================================================================================================================================
/**
 * @brief release heap map
 *
 * This is the function that should be called, when the heap map is not needed anymore. It release all memory allocated for the heap map.
 * The module must be enabled for this function.
 *
 * @param MapPointer        Pointer to the heap map
 * @param Flags             Additional flags of allocation. It can be set to #AllocationFlags_Default if it is not important to you.
 * List of flags:
 * Flag name                                    | Description
 * ---------------------------------------------|-----------------------------------------------
 * AllocationFlags_NoWait             | If cannot do it know, do not wait
 * AllocationFlags_CanWait500Msecond  | If cannot do it know, wait maximum 500 ms
 * AllocationFlags_CanWait1Second     | If cannot do it know, wait maximum 1 second
 * AllocationFlags_CanWaitForever     | If cannot do it know, wait as much as needed
 *
 * @return true if all memory allocated for the heap map was released.
 */
//==========================================================================================================================================
bool oC_MemMan_FreeHeapMap( oC_HeapMap_t * MapPointer , AllocationFlags_t Flags)
{
    bool       allMemoryReleased = false;
    HeapMap_t* heapMap           = IsAddressOfHeapMap(&HeapMap,*MapPointer) ? &HeapMap : IsAddressOfHeapMap(&ExternalHeapMap,*MapPointer) ? &ExternalHeapMap : NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && heapMap != NULL && IsHeapMapInitialized(*MapPointer))
    {
        HeapMap_t * HeapMap = *MapPointer;
        void * heapBuffer = HeapMap->BitMapStart;

        allMemoryReleased = true;

        if(!oC_MemMan_Free(heapBuffer,Flags))
        {
            allMemoryReleased = false;
        }

        if(!oC_MemMan_Free(HeapMap,Flags))
        {
            allMemoryReleased = false;
        }
        *MapPointer = NULL;
    }

    return allMemoryReleased;
}


//==========================================================================================================================================
/**
 * @brief allow to allocate memory in heap map
 *
 * This function is for allocating a memory on the previously created by #oC_MemMan_AllocateHeapMap function heap map. This allow to save some
 * memory, that is needed for allocation handling, but it is less save than allocation by #oC_MemMan_Allocate function. Moreover it is needed
 * to give correct size of this allocation during releasing by #oC_MemMan_RawFree. This function is recommended only in cases, when the memory
 * allocation size is constant, and always known. The module must be enabled for this function.
 *
 * @see oC_MemMan_RawFree , oC_MemMan_AllocateHeapMap
 *
 * @param Map           Pointer to the heap map created by function #oC_MemMan_AllocateHeapMap
 * @param Size          Size of the memory to allocate.
 * @param LineNumber    Number of line, where this function is called, it should be set to __LINE__ value
 * @param Flags         Flags of allocation
 *
 * @return pointer to the allocated memory from the `Map` heap.
 */
//==========================================================================================================================================
void * oC_MemMan_RawAllocate( oC_HeapMap_t Map , oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    void *     Address = NULL;
    HeapMap_t* heapMap = IsAddressOfHeapMap(&HeapMap,Map) ? &HeapMap : IsAddressOfHeapMap(&ExternalHeapMap,Map) ? &ExternalHeapMap : NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && (Size > 0) && heapMap != NULL && IsHeapMapInitialized(Map) && BlockModule(Flags))
    {
        Block_t * block = FindBlockOfAddress( Map );

        if(block)
        {
            Address = RawAlloc( Map , Size , oC_MEM_LLD_MEMORY_ALIGNMENT );

            if(Address)
            {
                block->AllocationFlags = Flags;

                CallAllocatorEvent( block->Allocator,Address , MemoryEventFlags_RawMemoryAllocated, Function, LineNumber );
            }
            else
            {
                CallAllocatorEvent( block->Allocator,Address , MemoryEventFlags_AllocationError , Function, LineNumber );
            }

        }

        UnblockModule();
    }

    return Address;
}

//==========================================================================================================================================
/**
 * @brief release memory in heap map
 *
 * The function is for releasing memory allocated on the heap map by function #oC_MemMan_RawAllocate. Note, that raw allocations does not
 * control the memory, so it is needed to give the correct size to release. If it will be not correct, there is a risk, that it the function
 * release also the memory that is already taken by other allocation.
 *
 * @see oC_MemMan_AllocateHeapMap , oC_MemMan_RawAllocate
 *
 * @param Map           Pointer to the heap map created by function #oC_MemMan_AllocateHeapMap
 * @param Address       Address received from the #oC_MemMan_RawAllocate function
 * @param Size          Size of the allocated memory.
 *
 * @return true if all memory was released
 */
//==========================================================================================================================================
bool oC_MemMan_RawFree( oC_HeapMap_t Map , void * Address , oC_UInt_t Size )
{
    bool       result = false;
    HeapMap_t* heapMap = IsAddressOfHeapMap(&HeapMap,Map) ? &HeapMap : IsAddressOfHeapMap(&ExternalHeapMap,Map) ? &ExternalHeapMap : NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && (Size > 0) && heapMap != NULL && IsHeapMapInitialized(Map) && IsAddressOfHeapMap(Map,Address) && BlockModule(AllocationFlags_CanWaitForever))
    {
        Block_t * block = FindBlockOfAddress(Map);

        if(block && CanReleaseMemory(block))
        {
            if(RawFree( Map , Address , Size))
            {
                result = true;
                CallAllocatorEvent(block->Allocator,Address , MemoryEventFlags_RawMemoryReleased , block->Function, block->LineNumber);
            }
            else
            {
                CallAllocatorEvent(block->Allocator,Address , MemoryEventFlags_ReleaseError , block->Function, block->LineNumber);
            }
        }
        UnblockModule();
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief allocates memory on heap
 *
 * This is `malloc()` type function for allocation of memory on the machine heap. Note, that every allocation require some additional memory
 * to handle it on a list. It is something about 4 words, and moreover each allocation is aligned to the machine alignment. If you want to
 * allocate more memory with the same size, it is recommended to use #oC_MemMan_RawAllocate. Thanks to that you can save some memory. To use
 * this function, the **MemMan** module must be correctly turned on. Moreover you must to prepare an allocator, which should be given as the
 * function argument, and it cannot be NULL. If you do not know, if your allocator is correct, you can use function #oC_MemMan_IsAllocatorCorrect.
 * A pointer to the allocator should be given as `Allocator` argument. Remember to release all memory allocated, when it is not needed anymore
 * by using a function #oC_MemMan_Free.
 *
 * @note It is possible to fill allocated buffer with 0 values, by setting the #AllocationFlags_ZeroFill flag
 *
 * Example of usage:
 * @code{.c}
 * static const oC_MemMan_Allocator_t Allocator = { .Name = "Example" };
 *
 * void main()
 * {
 *    uint8_t count = 100;
 *    uint8_t * buffer = oC_MemMan_Allocate( sizeof(uint8_t) * count , &Allocator , __LINE__ , AllocationFlags_NoWait );
 *
 *    for(uint8_t i = 0; i < count ; i++)
 *    {
 *       buffer[i] = i;
 *    }
 * }
 *
 * @endcode
 *
 * @param Size          Size of the memory that is required
 * @param Allocator     Pointer to the allocator structure
 * @param LineNumber    Number of line where the function is called (it should be set to __LINE__ value )
 * @param Flags         Additional flags of allocation. It can be set to #AllocationFlags_Default if it is not important to you.
 * List of flags:
 * Flag name                                    | Description
 * ---------------------------------------------|-----------------------------------------------
 * AllocationFlags_ZeroFill                     | Allocated memory will be filled by 0
 * AllocationFlags_NoWait                       | If cannot do it now, do not wait
 * AllocationFlags_CanWait500Msecond            | If cannot do it now, wait maximum 500 ms
 * AllocationFlags_CanWait1Second               | If cannot do it now, wait maximum 1 second
 * AllocationFlags_CanWaitForever               | If cannot do it now, wait as much as needed
 *
 * @param Alignment     Alignment of the allocation. Can be set to 0 for default. Note, that this variable must be multiply of the memory
 *                      alignment (for example multiply of 4 on 32-bits architectures)
 *
 * @return address of allocated memory on success, or NULL, if there was some error.
 */
//==========================================================================================================================================
void * oC_MemMan_Allocate( oC_UInt_t Size , Allocator_t Allocator , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags , oC_UInt_t Alignment )
{
    void * Address = NULL;

    if(Alignment == 0)
    {
        Alignment = oC_MEM_LLD_MEMORY_ALIGNMENT;
    }

    if( ModuleEnabledFlag && !ModuleTestModeEnabledFlag && (Size > 0) && ((Alignment % oC_MEM_LLD_MEMORY_ALIGNMENT) == 0) && oC_MemMan_IsAllocatorCorrect(Allocator) && (LineNumber > 0))
    {
        HeapMap_t * heapMap = NULL;

        /* If none of flags is set, then use both RAM - internal and external */
        ifnot(Flags & (AllocationFlags_UseExternalRam | AllocationFlags_UseInternalRam | AllocationFlags_UseDmaRam))
        {
            Flags |= AllocationFlags_UseExternalRam | AllocationFlags_UseInternalRam;
        }


        do
        {
            if(Allocator->Limit != 0 && Allocator->Limit < (oC_MemMan_GetMemoryOfAllocatorSize(Allocator) + Size))
            {
                CallAllocatorEvent(Allocator, NULL, MemoryEventFlags_MemoryExhausted, Function, LineNumber);
                kdebuglog(oC_LogType_Error, "MemMan::Allocate - Allocator named '%s' has exhausted the limit (%d B)\n", Allocator->Name, Allocator->Limit);
                break;
            }

            if(oC_Bits_AreBitsSetU32(Flags, AllocationFlags_UseDmaRam) && IsHeapMapInitialized(&DmaHeapMap))
            {
                if(WaitForMemory(&DmaHeapMap, Size + sizeof(oC_UInt_t) + sizeof(Block_t) , Alignment ,Flags))
                {
                    heapMap = &DmaHeapMap;
                    break;
                }
            }

            if(Allocator->CorePwd == oC_MemMan_CORE_PWD && IsHeapMapInitialized(&CoreHeapMap))
            {
                if(WaitForMemory(&CoreHeapMap, Size + sizeof(oC_UInt_t) + sizeof(Block_t) , Alignment ,Flags))
                {
                    heapMap = &CoreHeapMap;
                    break;
                }
            }

            if(oC_Bits_AreBitsSetU32(Flags , AllocationFlags_UseExternalRam | AllocationFlags_ExternalRamFirst) && IsHeapMapInitialized(&ExternalHeapMap))
            {
                if(WaitForMemory(&ExternalHeapMap, Size + sizeof(oC_UInt_t) + sizeof(Block_t) , Alignment ,Flags))
                {
                    // TODO: Debug, find out why it helps and remove it
                    Size   += 1; // workaround for blinking screen in LCD

                    heapMap = &ExternalHeapMap;
                    break;
                }
            }

            if((Flags & AllocationFlags_UseInternalRam) && IsHeapMapInitialized(&HeapMap))
            {
                if(WaitForMemory(&HeapMap, Size + sizeof(oC_UInt_t) + sizeof(Block_t) , Alignment ,Flags))
                {
                    heapMap = &HeapMap;
                    break;
                }
            }

            if((Flags & AllocationFlags_UseExternalRam) && IsHeapMapInitialized(&ExternalHeapMap))
            {
                if(WaitForMemory(&ExternalHeapMap, Size + sizeof(oC_UInt_t) + sizeof(Block_t) , Alignment ,Flags))
                {
                    heapMap = &ExternalHeapMap;
                    break;
                }
            }
        } while(0);

        if(heapMap != NULL && BlockModule(Flags))
        {
            Address = RawAlloc(heapMap , Size + sizeof(oC_UInt_t) , Alignment );

            if(IsAddressOfHeapMap(heapMap,Address))
            {
                Block_t * block = RawAlloc(heapMap , sizeof(Block_t) , oC_MEM_LLD_MEMORY_ALIGNMENT);

                if(IsAddressOfHeapMap(heapMap,block))
                {
                    block->Address          = Address;
                    block->Allocator        = Allocator;
                    block->Function         = Function;
                    block->LineNumber       = LineNumber;
                    block->Next             = NULL;
                    block->Size             = Size;
                    block->AllocationFlags  = Flags;

                    if(Flags & AllocationFlags_ZeroFill)
                    {
                        for(oC_UInt_t BufferIndex = 0 ; BufferIndex < Size ; BufferIndex++ )
                        {
                            ((uint8_t*)Address)[BufferIndex] = 0;
                        }
                    }

                    // write a magic number at the end of the buffer for protection against overflow
                    LAST_WORD_OF_BUFFER(Address,Size) = OVERFLOW_PROTECTION_MAGIC_NUMBER;

                    if(BlockListHead == NULL && BlockListTail == NULL)
                    {
                        BlockListHead = block;
                        BlockListTail = block;
                    }
                    else
                    {
                        BlockListTail->Next = block;
                        BlockListTail       = block;
                    }
                }
                else
                {
                    RawFree(heapMap,Address,Size);
                }
            }

            UnblockModule();
        }
    }

    return Address;
}

//==========================================================================================================================================
/**
 * @brief release allocated memory
 *
 * The function is for releasing memory that was allocated by function #oC_MemMan_Allocate. The module must be enabled.
 *
 * @param Address       Address from the #oC_MemMan_Allocate function to release.
 * @param Flags         Additional flags of allocation. It can be set to #AllocationFlags_Default if it is not important to you.
 * List of flags:
 * Flag name                                    | Description
 * ---------------------------------------------|-----------------------------------------------
 * AllocationFlags_NoWait                       | If cannot do it know, do not wait
 * AllocationFlags_CanWait500Msecond            | If cannot do it know, wait maximum 500 ms
 * AllocationFlags_CanWait1Second               | If cannot do it know, wait maximum 1 second
 * AllocationFlags_CanWaitForever               | If cannot do it know, wait as much as needed
 *
 * @return
 */
//==========================================================================================================================================
bool oC_MemMan_Free( void * Address , AllocationFlags_t Flags)
{
    bool        memoryReleased = false;
    HeapMap_t * heapMap        = IsAddressOfHeapMap(&HeapMap,Address) ? &HeapMap : IsAddressOfHeapMap(&ExternalHeapMap,Address) ? &ExternalHeapMap : NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag  && heapMap != NULL && BlockModule(Flags))
    {
        Block_t * previousBlock = BlockListHead;

        foreach_block(block)
        {
            if(block->Address == Address && CanReleaseMemory(block))
            {
                CallAllocatorEvent(block->Allocator , Address , MemoryEventFlags_MemoryReleased , block->Function, block->LineNumber );

                if(block != BlockListHead)
                {
                    // this is not the first block
                    previousBlock->Next = block->Next;

                    if(block == BlockListTail)
                    {
                        // this is not the first block but the last
                        BlockListTail = previousBlock;
                    }
                }
                else
                {
                    // this is the first block
                    BlockListHead = block->Next;

                    if(block == BlockListTail)
                    {
                        // this is the first and the last block
                        BlockListTail = NULL;
                    }
                }

                memoryReleased = true;

                if(!RawFree(heapMap , Address , block->Size + sizeof(oC_UInt_t)))
                {
                    memoryReleased = false;
                }

                if(!RawFree(heapMap , block   , sizeof(Block_t)))
                {
                    memoryReleased = false;
                }

                if(memoryReleased == false)
                {
                    CallAllocatorEvent(block->Allocator, Address, MemoryEventFlags_ReleaseError, block->Function, block->LineNumber);
                }

                break;
            }
            previousBlock = block;
        }
        UnblockModule();
    }

    return memoryReleased;
}

//==========================================================================================================================================
/**
 * @brief release all memory of allocator
 *
 * The function is for releasing all memory that was allocated for the **Allocator**. It is designed for situations, when for example some
 * module will be turned off. It is not possible to stop this operation. It will be performed until all memory will be released.
 *
 * @param Allocator     Pointer to the allocator structure, that memory should be released
 *
 * @return true if success, false if there was some error while releasing memory.
 */
//==========================================================================================================================================
bool oC_MemMan_FreeAllMemoryOfAllocator( Allocator_t Allocator )
{
    bool result = false;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && oC_MemMan_IsAllocatorCorrect(Allocator))
    {
        bool found_block;

        result = true;

        do
        {
            found_block = false;

            foreach_block(block)
            {
                if(block->Allocator == Allocator)
                {
                    found_block = true;

                    if(!oC_MemMan_Free(block->Address,AllocationFlags_CanWaitForever))
                    {
                        result = false;
                    }
                    break;
                }
            }
        } while(found_block);
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief sets event handler function
 *
 * The function is for setting the handler of events. It can be called only once after turning on the module.
 *
 * @param EventHandler      Pointer to the function, that should be called, when some module event occurs.
 *
 * @return One of following codes of errors:
 * Code                                     | Description
 * -----------------------------------------|----------------------------------------
 * oC_ErrorCode_None                        | There is no error
 * oC_ErrorCode_ModuleNotStartedYet         | The MemMan was not enabled before
 * oC_ErrorCode_WrongAddress                | Event handler pointer is not correct
 * oC_ErrorCode_InterruptHandlerAlreadySet  | The event handler is already set
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_SetEventHandler( MemoryEventHandler_t EventHandler )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode, ModuleEnabledFlag                         , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode, !ModuleTestModeEnabledFlag                , oC_ErrorCode_ModuleIsTestedAlready) &&
        oC_AssignErrorCodeIfFalse(&errorCode, oC_MemMan_IsAddressCorrect(EventHandler)  , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode, ModuleEventHandler == NULL                , oC_ErrorCode_InterruptHandlerAlreadySet)
        )
    {
        ModuleEventHandler  = EventHandler;
        errorCode           = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief changes allocator of the given address
 *
 * The function is for changing allocator of the given address.
 *
 * @param Address       Address to change allocator
 * @param Allocator     New allocator of the object
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_MemMan_ChangeAllocatorOfAddress( const void * Address , Allocator_t Allocator )
{
    bool success = false;

    if(ModuleEnabledFlag && oC_MemMan_IsAddressCorrect(Address) && oC_MemMan_IsAddressCorrect(Allocator))
    {
        foreach_block(block)
        {
            if(block->Address == Address)
            {
                success          = true;
                block->Allocator = Allocator;
                break;
            }
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief returns allocator of address
 *
 * The function is for getting information, about the **Allocator** that reserve the given address.
 *
 * @note The address must not be the start of the allocation. It also can be somewhere in the middle of the buffer
 *
 * Example of usage:
 * @code{.c}
 * const oC_MemMan_Allocator_t MyAllocator =   {.Name = "MyAllocator"};
 * const oC_MemMan_Allocator_t YourAllocator = {.Name = "YourAllocator"};
 *
 * bool IsMyBuffer( uint8_t * Address )
 * {
 *     return oC_MemMan_GetAllocatorOfAddress( Address ) == &MyAllocator;
 * }
 *
 * bool IsYourBuffer( uint8_t * Address )
 * {
 *     return oC_MemMan_GetAllocatorOfAddress( Address ) == &YourAllocator;
 * }
 *
 * void main()
 * {
 *     uint8_t * myBuffer   = oC_MemMan_Allocate( sizeof(uint8_t)*100,&MyAllocator,__LINE__,0);
 *     uint8_t * yourBuffer = oC_MemMan_Allocate( sizeof(uint8_t)*100,&YourAllocator,__LINE__,0);
 *
 *     if(IsMyBuffer(&myBuffer[98]))
 *     {
 *          printf("This is my buffer!\n");
 *     }
 *
 *     if(IsYourBuffer(&yourBuffer[98]))
 *     {
 *          printf("This is your buffer!\n");
 *     }
 * }
 * @endcode
 *
 * @param Address   Address to find allocator
 *
 * @return pointer to the allocator structure or NULL if not found (or module is not enabled)
 */
//==========================================================================================================================================
Allocator_t oC_MemMan_GetAllocatorOfAddress( const void * Address )
{
    Allocator_t allocator = NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && BlockModule(AllocationFlags_Default))
    {
        Block_t * block = FindBlockOfAddress(Address);

        if(block == NULL)
        {
            block = FindBlockContainsAddress(Address);
        }

        if(block)
        {
            allocator = block->Allocator;
        }
        UnblockModule();
    }

    return allocator;
}

//==========================================================================================================================================
/**
 * @brief returns size of allocation
 *
 * The function returns size of memory, that was allocated at the given address. Note, that this function works only when memory was allocated
 * by #oC_MemMan_Allocate function, and there is not possible to get size of raw allocations from the #oC_MemMan_RawAllocate function. Moreover,
 * the address must point exactly to the start of the buffer.
 *
 * @param Address       Allocated address from the #oC_MemMan_Allocate function.
 *
 * @return size of allocation or 0 if not found or module is not turned on
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetSizeOfAllocation( const void * Address )
{
    oC_UInt_t size = 0;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && BlockModule(AllocationFlags_Default))
    {
        Block_t * block = FindBlockOfAddress(Address);

        if(block)
        {
            size = block->Size;
        }
        UnblockModule();
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief returns address aligned to the machine alignment.
 *
 * The function is for align the address to the machine alignment. It works also when the module is disabled.
 *
 * @param Address   Address to align
 */
//==========================================================================================================================================
void * oC_MemMan_AlignAddress( const void * Address )
{
    return (void*)ALIGN_SIZE(Address,oC_MEM_LLD_MEMORY_ALIGNMENT);
}

//==========================================================================================================================================
/**
 * @brief returns address aligned to the given alignment.
 *
 * The function is for align the address to the given alignment. It works also when the module is disabled.
 *
 * @param Address   Address to align
 * @param Alignment Alignment to align the address
 */
//==========================================================================================================================================
void * oC_MemMan_AlignAddressTo( const void * Address , oC_UInt_t Alignment )
{
    return (void*)ALIGN_SIZE(Address,Alignment);
}

//==========================================================================================================================================
/**
 * @brief checks if address is aligned
 *
 * Checks if the address given as argument is aligned to the machine alignment. It works also when the module is disabled.
 *
 * @param Address   Address to check
 *
 * @return true if aligned
 */
//==========================================================================================================================================
bool oC_MemMan_IsAddressAligned( const void * Address )
{
    return ((void*)ALIGN_SIZE(Address,oC_MEM_LLD_MEMORY_ALIGNMENT) == Address);
}

//==========================================================================================================================================
/**
 * @brief checks if address is aligned to given alignment
 *
 * Checks if the address given as argument is aligned to the given alignment. It works also when the module is disabled.
 *
 * @param Address   Address to check
 *
 * @return true if aligned
 */
//==========================================================================================================================================
bool oC_MemMan_IsAddressAlignedTo( const void * Address , oC_UInt_t Alignment )
{
    return ((void*)ALIGN_SIZE(Address,Alignment) == Address);
}

//==========================================================================================================================================
/**
 * @brief returns size aligned to the machine alignment
 *
 * The function is for align the size to the machine alignment. It works also when the module is disabled.
 *
 * @param Size  Size to align
 *
 * @return aligned size
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_AlignSize( oC_UInt_t Size )
{
    return ALIGN_SIZE(Size,oC_MEM_LLD_MEMORY_ALIGNMENT);
}

//==========================================================================================================================================
/**
 * @brief returns size aligned to the given alignment
 *
 * The function is for align the size to the given alignment. It works also when the module is disabled.
 *
 * @param Size          Size to align
 * @param Alignment     Alignment to align the size
 *
 * @return aligned size
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_AlignSizeTo( oC_UInt_t Size , oC_UInt_t Alignment )
{
    return ALIGN_SIZE(Size,Alignment);
}

//==========================================================================================================================================
/**
 * @brief checks if a size is aligned
 *
 * The function is for checking if a size given as argument is aligned to the machine alignment. Works also with disabled module.
 *
 * @param Size          The size to check
 *
 * @return true if the Size is aligned to the machine alignment.
 */
//==========================================================================================================================================
bool oC_MemMan_IsSizeAligned(oC_UInt_t Size)
{
    return ALIGN_SIZE(Size,oC_MEM_LLD_MEMORY_ALIGNMENT) == Size;
}

//==========================================================================================================================================
/**
 * @brief returns maximum size of allocation
 *
 * The function returns maximum size that can be allocated by function #oC_MemMan_Allocate. The module must be enabled to use this function.
 *
 * @return size of maximum possible allocation in bytes.
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetMaximumAllocationSize( void )
{
    oC_UInt_t maximumAllocationSize = 0;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag)
    {
        oC_UInt_t * heapPointer     = HeapMap.HeapStart;
        oC_UInt_t   size            = 0;
        HeapMap_t * heapMaps[]      = {
                        &HeapMap ,
                        &ExternalHeapMap,
                        &DmaHeapMap,
        };

        oC_ARRAY_FOREACH_IN_ARRAY(heapMaps,heapMap)
        {
            do
            {
                if(IsHeapMapInitialized(*heapMap) == false)
                {
                    break;
                }

                heapPointer = FindNextHeapPointerInState(*heapMap,heapPointer,NULL,HeapState_NotUsed);
                size        = 0;

                for(;(heapPointer < (*heapMap)->HeapEnd) && !IsHeapUsed(*heapMap,heapPointer);heapPointer++)
                {
                    size += sizeof(oC_UInt_t);
                }

                if(size > sizeof(Block_t))
                {
                    size -= sizeof(Block_t);

                    if(size > maximumAllocationSize)
                    {
                        maximumAllocationSize = size;
                    }
                }
            } while(heapPointer < (*heapMap)->HeapEnd);
        }
    }

    return maximumAllocationSize;
}

//==========================================================================================================================================
/**
 * @brief returns size of the machine flash
 *
 * The function returns size of the machine flash memory. It works also when the module is not enabled.
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetFlashSize( void )
{
    return oC_MEM_LLD_GetFlashSize();
}

//==========================================================================================================================================
/**
 * @brief returns size of the machine ram
 *
 * The function returns size of the machine ram memory. It works also when the module is not enabled.
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetRamSize( void )
{
    oC_UInt_t ramSize = oC_MEM_LLD_GetRamSize();

    if(IsHeapMapInitialized(&ExternalHeapMap))
    {
        ramSize += GetSizeOfHeap(&ExternalHeapMap);
    }

    if(IsHeapMapInitialized(&DmaHeapMap))
    {
        ramSize += GetSizeOfHeap(&DmaHeapMap);
    }

    return ramSize;
}

//==========================================================================================================================================
/**
 * @brief returns size of the machine not used flash
 *
 * The function returns size of the machine not used flash memory. It works also when the module is not enabled.
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetFreeFlashSize( void )
{
    return oC_MEM_LLD_GetFlashSize() - oC_MEM_LLD_GetUsedFlashSize();
}

//==========================================================================================================================================
/**
 * @brief returns size of free ram
 *
 * The function returns number of bytes that are already not used or 0 if module is not enabled.
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetFreeRamSize( void )
{
    oC_UInt_t freeRamSize = 0;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag)
    {
        freeRamSize = GetSizeOfHeapInState(&HeapMap,HeapState_NotUsed);

        if(IsHeapMapInitialized(&ExternalHeapMap))
        {
            freeRamSize += GetSizeOfHeapInState(&ExternalHeapMap,HeapState_NotUsed);
        }

        if(IsHeapMapInitialized(&DmaHeapMap))
        {
            freeRamSize += GetSizeOfHeapInState(&DmaHeapMap,HeapState_NotUsed);
        }

        if(IsHeapMapInitialized(&CoreHeapMap))
        {
            freeRamSize += GetSizeOfHeapInState(&CoreHeapMap,HeapState_NotUsed);
        }
    }

    return freeRamSize;
}

//==========================================================================================================================================
/**
 * @brief returns size of external ram
 *
 * The function returns size of external ram or 0 if not connected
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetExternalHeapSize( void )
{
    oC_UInt_t externalSize = 0;

    if(IsHeapMapInitialized(&ExternalHeapMap))
    {
        externalSize += GetSizeOfHeap(&ExternalHeapMap);
    }

    return externalSize;
}

//==========================================================================================================================================
/**
 * @brief returns size of core heap map ram
 *
 * The function returns size of external ram or 0 if not connected
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetCoreHeapSize( void )
{
    oC_UInt_t externalSize = 0;

    if(IsHeapMapInitialized(&CoreHeapMap))
    {
        externalSize += GetSizeOfHeap(&CoreHeapMap);
    }

    return externalSize;
}

//==========================================================================================================================================
/**
 * @brief returns size of external ram
 *
 * The function returns size of external ram or 0 if not connected
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetDmaRamHeapSize( void )
{
    oC_UInt_t dmaSize = 0;

    if(IsHeapMapInitialized(&DmaHeapMap))
    {
        dmaSize += GetSizeOfHeap(&DmaHeapMap);
    }

    return dmaSize;
}

//==========================================================================================================================================
/**
 * @brief returns size of allocations per allocator
 *
 * The function is for reading size of memory that was allocated for the Allocator. Note, that each allocation require some additional bytes.
 * This function returns real size of alloc
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetMemoryOfAllocatorSize( Allocator_t Allocator )
{
    oC_UInt_t size = 0;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag)
    {
        foreach_block(block)
        {
            if(block->Allocator == Allocator)
            {
                size += sizeof(Block_t) + block->Size;
            }
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief returns size of block needed for allocation
 *
 * The function returns size of the block, that is needed for each allocation. It is important, that this size should be added to size of each
 * allocation to get real size of the needed ram memory. This function can be useful to choose between allocation via standard allocation function
 * or raw allocations (by using 'heap map').
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetAllocationBlockSize( void )
{
    return sizeof(Block_t);
}


//==========================================================================================================================================
/**
 * @brief returns size of heap map
 *
 * The function returns size of the heap map allocated by function #oC_MemMan_AllocateHeapMap.
 *
 * @param Map           Pointer to the heap map created by function #oC_MemMan_AllocateHeapMap
 *
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetHeapMapSize( oC_HeapMap_t Map )\
{
    oC_UInt_t   size    = 0;
    HeapMap_t * heapMap = IsAddressOfHeapMap(&HeapMap,Map) ? &HeapMap : IsAddressOfHeapMap(&ExternalHeapMap,Map) ? &ExternalHeapMap : NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && heapMap != NULL && IsHeapMapInitialized(Map))
    {
        size = GetSizeOfHeap(Map);
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief returns size of free memory in heap map
 *
 * The function returns size of the free heap.
 *
 * @param Map           Pointer to the heap map created by function #oC_MemMan_AllocateHeapMap
 */
//==========================================================================================================================================
oC_UInt_t oC_MemMan_GetFreeHeapMapSize( oC_HeapMap_t Map )
{
    oC_UInt_t   size    = 0;
    HeapMap_t * heapMap = IsAddressOfHeapMap(&HeapMap,Map) ? &HeapMap : IsAddressOfHeapMap(&ExternalHeapMap,Map) ? &ExternalHeapMap : NULL;

    if(ModuleEnabledFlag && !ModuleTestModeEnabledFlag && heapMap != NULL && IsHeapMapInitialized(Map))
    {
        size = GetSizeOfHeapInState(Map,HeapState_NotUsed);
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief checks if address is placed in the flash section
 */
//==========================================================================================================================================
bool oC_MemMan_IsFlashAddress( const void * Address )
{
    return oC_MEM_LLD_IsFlashAddress(Address);
}

//==========================================================================================================================================
/**
 * @brief checks if address is in used flash section
 */
//==========================================================================================================================================
bool oC_MemMan_IsUsedFlashAddress( const void * Address )
{
    return oC_MEM_LLD_IsUsedFlashAddress(Address);
}

//==========================================================================================================================================
/**
 * @brief checks if address is in ram section
 */
//==========================================================================================================================================
bool oC_MemMan_IsRamAddress( const void * Address )
{
    return oC_MEM_LLD_IsRamAddress(Address);
}

//==========================================================================================================================================
/**
 * @brief checks if address is in dynamic allocated section
 */
//==========================================================================================================================================
bool oC_MemMan_IsDynamicAllocatedAddress( const void * Address )
{
    bool dynamicAllocatedAddress = IsAddressOfHeapMap(&HeapMap , Address);
    if(!dynamicAllocatedAddress && IsHeapMapInitialized(&ExternalHeapMap))
    {
        dynamicAllocatedAddress = IsAddressOfHeapMap(&ExternalHeapMap,Address);
    }
    return dynamicAllocatedAddress;
}

//==========================================================================================================================================
/**
 * @brief checks if address is in static used ram section (data section)
 */
//==========================================================================================================================================
bool oC_MemMan_IsStaticRamAddress( const void * Address )
{
    return Address >= oC_MEM_LLD_GetDataStartAddress() && Address < oC_MEM_LLD_GetDataEndAddress();
}

//==========================================================================================================================================
/**
 * @brief checks if address is correct - in RAM or in FLASH
 */
//==========================================================================================================================================
bool oC_MemMan_IsAddressCorrect( const void * Address )
{
    return oC_MEM_LLD_IsRamAddress(Address) || oC_MEM_LLD_IsFlashAddress(Address);
}

//==========================================================================================================================================
/**
 * @brief checks if allocator is correct
 */
//==========================================================================================================================================
bool oC_MemMan_IsAllocatorCorrect( Allocator_t Allocator )
{
    return oC_MemMan_IsAddressCorrect(Allocator) && oC_MemMan_IsAddressCorrect(Allocator->Name);
}

//==========================================================================================================================================
/**
 * @brief Reads allocators statistics array
 *
 * Reads statistics of memory
 *
 * @param AllocatorsArray   Array for statistics
 * @param Size              Size of the array
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_ReadAllocatorsStats( oC_MemMan_AllocatorsStats_t * outAllocatorsArray , oC_UInt_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag                            , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MemMan_IsRamAddress(outAllocatorsArray)   , oC_ErrorCode_OutputAddressNotInRAM) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(Size)                                  , oC_ErrorCode_AddressNotInRam) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (*Size) > 0                                  , oC_ErrorCode_SizeNotCorrect)
        )
    {
        oC_UInt_t arrayIndex = 0;

        BlockModule(AllocationFlags_Default);

        errorCode = oC_ErrorCode_None;

        foreach_block(block)
        {
            bool existOnArray = false;

            oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(outAllocatorsArray,arrayIndex,stats)
            {
                if(stats->Allocator == block->Allocator)
                {
                    existOnArray = true;
                    break;
                }
            }

            if(!existOnArray)
            {
                if(arrayIndex < *Size)
                {
                    outAllocatorsArray[arrayIndex].Allocator = block->Allocator;
                    outAllocatorsArray[arrayIndex].Size      = oC_MemMan_GetMemoryOfAllocatorSize(block->Allocator);
                    arrayIndex++;
                }
                else
                {
                    errorCode = oC_ErrorCode_OutputArrayToSmall;
                }
            }
        }

        UnblockModule();

        *Size = arrayIndex;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Reads allocations statistics of the allocator
 *
 * The function is for reading array of allocations for the given allocator
 *
 * @param Allocator                 Allocator to read allocations
 * @param outAllocationsArray       Array for statistics
 * @param Size                      Size of the array
 * @param JoinSimilar               Set it to true if it should join similar allocations into 1 entry
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_ReadAllocationsStats( Allocator_t Allocator , oC_MemMan_AllocationStats_t * outAllocationsArray , oC_UInt_t * Size , bool JoinSimilar )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag                            , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MemMan_IsRamAddress(outAllocationsArray)  , oC_ErrorCode_OutputAddressNotInRAM) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(Size)                                  , oC_ErrorCode_AddressNotInRam) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (*Size) > 0                                  , oC_ErrorCode_SizeNotCorrect)
        )
    {
        oC_UInt_t arrayIndex = 0;

        BlockModule(AllocationFlags_Default);

        errorCode = oC_ErrorCode_None;

        foreach_block(block)
        {
            if(block->Allocator == Allocator)
            {
                bool found = false;

                if(JoinSimilar == true)
                {
                    oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(outAllocationsArray,arrayIndex,stat)
                    {
                        if(stat->Function == block->Function && stat->LineNumber == block->LineNumber)
                        {
                            stat->Size += block->Size;
                            found = true;
                        }
                    }
                }
                if(found == false)
                {
                    outAllocationsArray[arrayIndex].Address     = block->Address;
                    outAllocationsArray[arrayIndex].Function    = block->Function;
                    outAllocationsArray[arrayIndex].LineNumber  = block->LineNumber;
                    outAllocationsArray[arrayIndex].Size        = block->Size;
                    outAllocationsArray[arrayIndex].Overflowed  = LAST_WORD_OF_BUFFER(block->Address,block->Size) != OVERFLOW_PROTECTION_MAGIC_NUMBER;

                    arrayIndex++;
                }

                if(arrayIndex >= (*Size))
                {
                    break;
                }
            }
        }

        *Size = arrayIndex;

        UnblockModule();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief searches for a overflowed buffer
 *
 * The function searches for a buffer that has been overflowed
 *
 * @param Allocator             Allocator to filter the results (or #NULL if not used)
 * @param outAllocationStat     Destination for a data of allocation
 *
 * @return true if found
 */
//==========================================================================================================================================
bool oC_MemMan_FindOverflowedAllocation( Allocator_t Allocator , oC_MemMan_AllocationStats_t * outAllocationStat )
{
    bool found = false;

    if(ModuleEnabledFlag && isram(outAllocationStat))
    {
        foreach_block(block)
        {
            if(block->Allocator == Allocator || Allocator == NULL)
            {
                if(LAST_WORD_OF_BUFFER(block->Address,block->Size) != OVERFLOW_PROTECTION_MAGIC_NUMBER)
                {
                    outAllocationStat->Address          = block->Address;
                    outAllocationStat->Function         = block->Function;
                    outAllocationStat->LineNumber       = block->LineNumber;
                    outAllocationStat->Overflowed       = true;
                    outAllocationStat->Size             = block->Size;
                    found = true;
                    break;
                }
            }
        }
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief prepares HeapMap stored in external RAM
 *
 * The function is for configuration of HeapMap that is stored in external RAM. It can be called only once after turning on module.
 *
 * @param StartAddress      Address of the external RAM
 * @param Size              Size of the external RAM
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_ConfigureExternalHeapMap( void * StartAddress , oC_UInt_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag                                , oC_ErrorCode_ModuleNotStartedYet        ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MEM_LLD_IsExternalAddress(StartAddress)       , oC_ErrorCode_AddressNotInExternalMemory ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size > 0                                         , oC_ErrorCode_SizeNotCorrect             ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsHeapMapInitialized(&ExternalHeapMap) == false  , oC_ErrorCode_HeapMapAlreadyConfigured   ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsMemoryValid(StartAddress,Size)                 , oC_ErrorCode_CannotAccessMemory         )
        )
    {
        errorCode = InitializeHeapMap(&ExternalHeapMap , StartAddress , StartAddress + Size , Size , true);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures external heap
 *
 * The function is for deconfiguration of the external heap
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_UnconfigureExternalHeapMap( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag                                , oC_ErrorCode_ModuleNotStartedYet        ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsHeapMapInitialized(&ExternalHeapMap) == true   , oC_ErrorCode_HeapMapNotConfigured       )
        )
    {
        errorCode = oC_ErrorCode_None;

        foreach_block(block)
        {
            if(IsAddressOfHeapMap(&ExternalHeapMap,block->Address))
            {
                ErrorCondition(oC_MemMan_Free(block->Address,AllocationFlags_CanWaitForever),oC_ErrorCode_ReleaseError);
            }
        }

        memset(&ExternalHeapMap,0,sizeof(ExternalHeapMap));
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * The function returns current percent limit of exhausted memory
 * @return percent value of limit
 */
//==========================================================================================================================================
float oC_MemMan_GetMemoryExhaustedLimit( void )
{
    return DefaultMemoryExhaustedLimit * 100;
}

//==========================================================================================================================================
/**
 * The function returns current percent panic limit of exhausted memory
 * @return percent value of panic limit
 */
//==========================================================================================================================================
float oC_MemMan_GetPanicMemoryExhaustedLimit( void )
{
    return PanicMemoryExhaustedLimit * 100;
}

//==========================================================================================================================================
/**
 * Configures limit of memory exhausted event
 *
 * @param LimitPercent  Limit in percent
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_SetMemoryExhaustedLimit( float LimitPercent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(ModuleEnabledFlag                        , oC_ErrorCode_ModuleNotStartedYet) &&
        ErrorCondition(LimitPercent >= 0 && LimitPercent <= 100 , oC_ErrorCode_WrongParameters)
        )
    {
        DefaultMemoryExhaustedLimit = LimitPercent / 100;
        errorCode                   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Configures limit of memory exhausted event
 *
 * @param LimitPercent  Limit in percent
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_MemMan_SetPanicMemoryExhaustedLimit( float LimitPercent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(ModuleEnabledFlag                        , oC_ErrorCode_ModuleNotStartedYet) &&
        ErrorCondition(LimitPercent >= 0 && LimitPercent <= 100 , oC_ErrorCode_WrongParameters)
        )
    {
        PanicMemoryExhaustedLimit   = LimitPercent / 100;
        errorCode                   = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * Blocks module to be unavailable for the others threads (mutex lock)
 *
 * @return true if block is blocked for current thread (if success)
 */
//==========================================================================================================================================
static bool BlockModule(AllocationFlags_t Flags)
{
    bool success = false;

    if(Flags & AllocationFlags_ForceBlock)
    {
        oC_IntMan_EnterCriticalSection();
        ModuleBusyFlag = true;
        success        = true;
        oC_IntMan_ExitCriticalSection();
    }
    else if(Flags & AllocationFlags_NoWait)
    {
        oC_IntMan_EnterCriticalSection();
        if(ModuleBusyFlag == false)
        {
            ModuleBusyFlag = true;
            success        = true;
        }
        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        oC_IntMan_EnterCriticalSection();
        if(ModuleBusyFlag)
        {
            oC_IntMan_ExitCriticalSection();

            oC_Thread_t currentThread = oC_ThreadMan_GetCurrentThread();

            if(currentThread)
            {
                oC_Time_t timeout = 0;

                if(Flags & AllocationFlags_CanWaitForever)
                {
                    timeout = oC_day(365);
                }
                else if(Flags & AllocationFlags_CanWait1Second)
                {
                    timeout = oC_s(1);
                }
                else
                {
                    timeout = oC_ms(500);
                }

                if(oC_Thread_SetBlocked(currentThread,(uint32_t*)&ModuleBusyFlag,oC_Thread_Unblock_WhenEqual,false,oC_Thread_UnblockMask_All,timeout))
                {
                    while(oC_Thread_IsBlocked(currentThread));

                    oC_Thread_SetUnblocked(currentThread);

                    oC_IntMan_EnterCriticalSection();
                    success = true;
                    ModuleBusyFlag    = true;
                    oC_IntMan_ExitCriticalSection();
                }
            }
        }
        else
        {

            ModuleBusyFlag = true;
            success        = true;
            oC_IntMan_ExitCriticalSection();
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * Unblocks module to be available for the other threads (mutex unlock)
 *
 * @return true if success
 */
//==========================================================================================================================================
static bool UnblockModule(void)
{
    bool success = false;

    oC_IntMan_EnterCriticalSection();
    if(ModuleBusyFlag)
    {
        ModuleBusyFlag = false;
        success = true;
    }
    oC_IntMan_ExitCriticalSection();

    return success;
}

//==========================================================================================================================================
/**
 * Sleep current thread during there is no memory to allocate (or it is to small)
 */
//==========================================================================================================================================
static bool WaitForMemory( HeapMap_t * Map , oC_UInt_t Size , oC_UInt_t Alignment ,  AllocationFlags_t Flags )
{
    bool        memoryAvailable = false;
    oC_Thread_t thread          = oC_ThreadMan_GetCurrentThread();

    if(thread)
    {
        oC_Time_t timeout           = 0;
        oC_Time_t checkingTimeStep  = 0;
        oC_Time_t startTime         = oC_KTime_GetTimestamp();
        oC_Time_t currentTime       = startTime;

        if(Flags & AllocationFlags_CanWaitForever)
        {
            timeout             = startTime + oC_hour(1);
            checkingTimeStep    = oC_min(1);
        }
        else if(Flags & AllocationFlags_CanWait1Second)
        {
            timeout             = startTime + oC_s(1);
            checkingTimeStep    = oC_ms(100);
        }
        else if(Flags & AllocationFlags_NoWait)
        {
            timeout             = startTime;
            checkingTimeStep    = 0;
        }
        else
        {
            timeout             = startTime + oC_ms(500);
            checkingTimeStep    = oC_ms(100);
        }

        do
        {
            memoryAvailable = FindBufferOnFreeAreaList( Map, Size, Alignment, false) != NULL
                           || FindFreeHeap            ( Map, Size, Alignment)        != NULL;

            if(!memoryAvailable)
            {
                oC_Thread_Sleep(thread,checkingTimeStep);
            }

            currentTime = oC_KTime_GetTimestamp();
        } while(currentTime < timeout && memoryAvailable == false);
    }
    else
    {
        memoryAvailable = FindFreeHeap(Map,Size,Alignment) != NULL;
    }

    return memoryAvailable;
}

//==========================================================================================================================================
/**
 * Initialize pointers for the module
 *
 * @param FillZero      true if heap map should be filled with 0 after initialize
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t InitializePointers( bool FillZero )
{
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;
    void *          heapPointer     = oC_MEM_LLD_GetHeapStartAddress();
    oC_UInt_t       heapSize        = oC_MEM_LLD_GetHeapSize() - sizeof(oC_UInt_t);
    void *          heapEndPointer  = oC_MEM_LLD_GetHeapEndAddress();
    void *          dmaRamStart     = oC_MEM_LLD_GetDmaRamStartAddress();
    oC_UInt_t       dmaRamSize      = oC_MEM_LLD_GetDmaRamSize();
    void *          dmaRamEnd       = oC_MEM_LLD_GetDmaRamEndAddress();

    if(ErrorCondition( oC_MEM_LLD_IsHeapAddress(heapPointer)    , oC_ErrorCode_MachineHeapError ))
    {
        DataOverflowProtection = heapPointer;
        heapPointer           += sizeof(uint32_t);
        BlockListHead          = NULL;
        BlockListTail          = NULL;
        ModuleEventHandler     = NULL;
        *DataOverflowProtection= OVERFLOW_PROTECTION_MAGIC_NUMBER;

        if(ErrorCode( InitializeHeapMap( &HeapMap, heapPointer, heapEndPointer, heapSize, FillZero )) )
        {
            if(dmaRamStart == NULL)
            {
                errorCode = oC_ErrorCode_None;
            }
            else if(
                    ErrorCondition( oC_MEM_LLD_IsDmaRamAddress(dmaRamStart) , oC_ErrorCode_NotDmaAddress        )
                 && ErrorCondition( dmaRamSize > 0                          , oC_ErrorCode_SizeNotCorrect       )
                    )
            {
                errorCode = InitializeHeapMap( &DmaHeapMap, dmaRamStart, dmaRamEnd, dmaRamSize, FillZero );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Prepares and initializes heap map for core operations
 * @return
 */
//==========================================================================================================================================
static oC_ErrorCode_t PrepareCoreHeapMap( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_MemorySize_t size = (oC_MemorySize_t)(((float)oC_MEM_LLD_GetRamSize()) * CFG_PERCENT_CORE_MEMORY_SIZE);

    void* heapStartPointer = RawAlloc(&HeapMap, size, oC_MEM_LLD_MEMORY_ALIGNMENT);
    void* heapEndPointer   = heapStartPointer + size;
    if (
         ErrorCondition(heapStartPointer != NULL, oC_ErrorCode_AllocationError)
      && ErrorCode( InitializeHeapMap(&CoreHeapMap, heapStartPointer, heapEndPointer, size, false) )
         )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Initializes heap map
 *
 * @param Map                   Pointer to the heap map to initialize
 * @param HeapStartPointer      Start of the heap, where heap map should be placed
 * @param HeapEndPointer        End of the heap map, where heap map should be placed
 * @param HeapSize              Heap size (HeapEndPointer - HeapStartPointer)
 * @param FillZero              Fills memory with 0
 *
 * @return code of error
 */
//==========================================================================================================================================
static oC_ErrorCode_t InitializeHeapMap( HeapMap_t * Map , void * HeapStartPointer , void * HeapEndPointer , oC_UInt_t HeapSize , bool FillZero )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    if(oC_MEM_LLD_IsHeapAddress(HeapStartPointer) || oC_MEM_LLD_IsExternalAddress(HeapStartPointer) || oC_MEM_LLD_IsDmaRamAddress(HeapStartPointer))
    {
        Map->BitMapStart            = HeapStartPointer;
        Map->BitMapEnd              = Map->BitMapStart + ALIGN_SIZE( HeapSize / ( sizeof(oC_UInt_t) * 8 ) , oC_MEM_LLD_MEMORY_ALIGNMENT);
        Map->HeapStart              = (oC_UInt_t*)Map->BitMapEnd;
        Map->HeapEnd                = HeapEndPointer;
        Map->Self                   = Map;

        for(uint8_t * heapPointer = Map->BitMapStart; heapPointer < (uint8_t*)Map->BitMapEnd;heapPointer++)
        {
            *heapPointer = 0;
        }

        if(FillZero)
        {
            for(uint8_t * heapPointer = HeapStartPointer;heapPointer < (uint8_t*)HeapEndPointer; heapPointer++)
            {
                *heapPointer = 0;
            }
        }

        UpdateFreeAreaList(Map);

        errorCode = oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_MachineHeapError;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Find block on the list, that was created for the address during allocation
 *
 * @param Address       Address from allocation (start of a buffer)
 *
 * @return pointer reference or NULL if not found
 */
//==========================================================================================================================================
static Block_t * FindBlockOfAddress(const void * Address)
{
    Block_t * blockToReturn = NULL;

    foreach_block(block)
    {
        if(block->Address == Address)
        {
            blockToReturn = block;
            break;
        }
    }

    return blockToReturn;
}

//==========================================================================================================================================
/**
 * Find block on the list, that contains this address (the address must not be start of the allocated buffer)
 *
 * @param Address       Address on heap (must not be at the start of allocated buffer)
 *
 * @return block if found, NULL otherwise
 */
//==========================================================================================================================================
static Block_t * FindBlockContainsAddress(const void * Address)
{
    Block_t * blockToReturn = NULL;

    foreach_block(block)
    {
        if( (Address >= block->Address) && (Address < (block->Address + block->Size)))
        {
            blockToReturn = block;
            break;
        }
    }

    return blockToReturn;
}

//==========================================================================================================================================
/**
 * Returns index of heap word in the heap section in the heap map.
 *
 * @param Map           Reference to the heap map
 * @param HeapPointer   Pointer to the word from heap section in map
 *
 * @return index
 */
//==========================================================================================================================================
static oC_UInt_t GetIndexOfHeapPointer( HeapMap_t * Map , oC_UInt_t * HeapPointer )
{
    oC_ASSERT(Map->HeapStart <= HeapPointer);
    return (oC_UInt_t)(HeapPointer - Map->HeapStart);
}

//==========================================================================================================================================
/**
 * Checks if address is on heap of map.
 *
 * @param Map          Reference to the heap map
 * @param Address      Address to check
 *
 * @return true if address belong to the heap map
 */
//==========================================================================================================================================
static inline bool IsAddressOfHeapMap( HeapMap_t * Map , const void * Address )
{
    return (Address >= ((void*)Map->HeapStart)) && (Address < ((void*)Map->HeapEnd));
}

//==========================================================================================================================================
/**
 * Checks if block is correct
 */
//==========================================================================================================================================
static inline bool IsBlockCorrect( Block_t * Block )
{
    return IsAddressOfHeapMap(&HeapMap,Block)             &&
           oC_MemMan_IsAllocatorCorrect(Block->Allocator) &&
           IsAddressOfHeapMap(&HeapMap,Block->Address)    &&
           CheckIfHeapInRangeIsInState(&HeapMap,Block->Address,(Block->Address + Block->Size),HeapState_Used);
}

//==========================================================================================================================================
/**
 * Checks if heap word is used
 */
//==========================================================================================================================================
static inline bool IsHeapUsed( HeapMap_t * Map , oC_UInt_t * HeapPointer )
{
    oC_UInt_t heapIndex  = GetIndexOfHeapPointer(Map,HeapPointer);
    oC_UInt_t bitMapIndex= heapIndex / 8;
    oC_UInt_t bitIndex   = heapIndex % 8;
    return oC_Bits_IsBitSetU8( Map->BitMapStart[bitMapIndex] , bitIndex );
}
//==========================================================================================================================================
/**
 * Checks if heap map is initialized
 */
//==========================================================================================================================================
static inline bool IsHeapMapInitialized( HeapMap_t * Map )
{
    return (Map->Self == Map );
}

//==========================================================================================================================================
/**
 * The function is for dumping heap map using function that is given as argument.
 */
//==========================================================================================================================================
static void DumpHeapMap(HeapMap_t * Map , oC_MemMan_DumpFunction_t DumpFunction)
{
    if(oC_MemMan_IsAddressCorrect(DumpFunction))
    {
        for(oC_UInt_t * heapPointer = Map->HeapStart ; heapPointer < Map->HeapEnd ; heapPointer++ )
        {
            DumpFunction(*heapPointer);
        }
    }
}

//==========================================================================================================================================
/**
 * Checks if heap word is in state
 */
//==========================================================================================================================================
static inline void SetHeapState( HeapMap_t * Map , oC_UInt_t * HeapPointer , HeapState_t HeapState )
{
    oC_UInt_t heapIndex  = GetIndexOfHeapPointer(Map,HeapPointer);
    oC_UInt_t bitMapIndex= heapIndex / 8;
    oC_UInt_t bitIndex   = heapIndex % 8;

    if(HeapState == HeapState_Used)
    {
        oC_Bits_SetBitU8(&Map->BitMapStart[bitMapIndex] , bitIndex);
    }
    else
    {
        oC_Bits_ClearBitU8(&Map->BitMapStart[bitMapIndex] , bitIndex);
    }
}

//==========================================================================================================================================
/**
 * Checks if heap in range is in state
 */
//==========================================================================================================================================
static bool CheckIfHeapInRangeIsInState( HeapMap_t * Map , oC_UInt_t * HeapFrom , oC_UInt_t * HeapTo , HeapState_t HeapState )
{
    bool stateCorrect = true;

    for(oC_UInt_t * heapPointer = HeapFrom; heapPointer < HeapTo ; heapPointer++)
    {
        if(HeapState != IsHeapUsed(Map,heapPointer))
        {
            stateCorrect = false;
            break;
        }
    }

    return stateCorrect;
}

//==========================================================================================================================================
/**
 * Sets heap in range to state
 */
//==========================================================================================================================================
static inline void SetHeapStateInRange( HeapMap_t * Map , oC_UInt_t * HeapFrom , oC_UInt_t * HeapTo , HeapState_t HeapState )
{
    for(oC_UInt_t * heapPointer = HeapFrom; (heapPointer < HeapTo) && (heapPointer < Map->HeapEnd) ; heapPointer++)
    {
        SetHeapState( Map, heapPointer , HeapState);
    }
}

//==========================================================================================================================================
/**
 * Finds next heap pointer in the heap map that is in the given state, starting from the heap pointer, that is given as the argument
 */
//==========================================================================================================================================
static oC_UInt_t * FindNextHeapPointerInState( HeapMap_t * Map , oC_UInt_t * HeapPointer , oC_UInt_t * HeapEndLimit , HeapState_t HeapState )
{
    if(HeapEndLimit == NULL)
    {
        HeapEndLimit = Map->HeapEnd;
    }

    while((HeapPointer < Map->HeapEnd) && (HeapPointer < HeapEndLimit) && (HeapState != IsHeapUsed( Map , HeapPointer)))
    {
        HeapPointer++;
    }

    return HeapPointer;
}

//==========================================================================================================================================
/**
 * Returns size of memory that is in the given heap state in the heap map.
 */
//==========================================================================================================================================
static oC_UInt_t GetSizeOfHeapInState( HeapMap_t * Map , HeapState_t HeapState )
{
    oC_UInt_t size = 0;

    for(oC_UInt_t * heapPointer = Map->HeapStart;heapPointer<Map->HeapEnd;heapPointer++)
    {
        if(HeapState == IsHeapUsed(Map,heapPointer))
        {
            size += sizeof(oC_UInt_t);
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * Returns size of heap map
 */
//==========================================================================================================================================
static oC_UInt_t GetSizeOfHeap( HeapMap_t * Map )
{
    return ((oC_UInt_t)Map->HeapEnd) - ((oC_UInt_t)Map->HeapStart);
}

//==========================================================================================================================================
/**
 * Adds new buffer to the free area list
 */
//==========================================================================================================================================
static void AddBufferToFreeAreaList( HeapMap_t * Map, void * HeapPointer, oC_UInt_t Size )
{
    FreeArea_t * area = oC_MemMan_AlignAddress(HeapPointer);

    oC_ASSERT( ((void*)area) >= HeapPointer );

    if(area != NULL && Size > sizeof(FreeArea_t))
    {
        area->Address = HeapPointer;
        area->Size    = Size;
        area->Next    = NULL;

        if(Map->FreeArea.First == NULL)
        {
            Map->FreeArea.First = area;
        }
        if(Map->FreeArea.Last != NULL)
        {
            Map->FreeArea.Last->Next = area;
        }
        Map->FreeArea.Last = area;
    }
}

//==========================================================================================================================================
/**
 * Searches for buffer on free area list
 */
//==========================================================================================================================================
static void * FindBufferOnFreeAreaList( HeapMap_t * Map, oC_UInt_t Size , oC_UInt_t Alignment , bool Take )
{
    void *       buffer                     = NULL;
    FreeArea_t * area                       = NULL;
    FreeArea_t * previous                   = NULL;
    oC_UInt_t    sizeForAlignment           = 0;
    oC_UInt_t    areaSizeAfterAlignment     = 0;
    oC_UInt_t    leftAreaSize               = 0;
    void *       newHeapPointer             = NULL;

    Size = ALIGN_SIZE(Size,Alignment);

    for(area = Map->FreeArea.First; area != NULL; previous = area, area = area->Next)
    {
        void * alignedAddress = oC_MemMan_AlignAddressTo( area->Address, Alignment );

        sizeForAlignment        = (oC_UInt_t)(alignedAddress - area->Address);
        areaSizeAfterAlignment  = area->Size >= sizeForAlignment ? area->Size - sizeForAlignment : 0;

        if(areaSizeAfterAlignment >= Size)
        {
            buffer = alignedAddress;

            if(Take == false)
            {
                break;
            }

            if(previous == NULL)
            {
                Map->FreeArea.First = area->Next;
            }
            else
            {
                previous->Next = area->Next;
            }
            if(area == Map->FreeArea.Last)
            {
                Map->FreeArea.Last = previous;
            }

            if(area->Address != alignedAddress)
            {
                AddBufferToFreeAreaList(Map,area->Address,sizeForAlignment);
            }
            leftAreaSize = areaSizeAfterAlignment - Size;

            if(leftAreaSize > sizeof(FreeArea_t))
            {
                newHeapPointer = alignedAddress + Size;
                AddBufferToFreeAreaList(Map,newHeapPointer,leftAreaSize);
            }
            break;
        }
    }

    return buffer;
}

//==========================================================================================================================================
/**
 * Updates list with free areas
 */
//==========================================================================================================================================
static void UpdateFreeAreaList( HeapMap_t * Map )
{
    oC_IntMan_EnterCriticalSection();

    void *      heapFrom    = Map->HeapStart;
    void *      heapTo      = NULL;
    void *      heapMapEnd  = Map->HeapEnd;
    oC_UInt_t   foundSize   = 0;

    Map->FreeArea.First = NULL;
    Map->FreeArea.Last  = NULL;

    while((heapFrom = FindNextHeapPointerInState(Map,heapFrom,NULL,HeapState_NotUsed)) < heapMapEnd)
    {
        heapTo      = FindNextHeapPointerInState(Map,heapFrom,NULL,HeapState_Used);
        foundSize   = (oC_UInt_t)(heapTo - heapFrom);

        if(foundSize > sizeof(FreeArea_t))
        {
            AddBufferToFreeAreaList(Map,heapFrom,foundSize);
        }
        heapFrom    = heapTo;
    }

    oC_IntMan_ExitCriticalSection();
}

//==========================================================================================================================================
/**
 * Searches for not used heap in size
 */
//==========================================================================================================================================
static void * FindFreeHeap( HeapMap_t * Map , oC_UInt_t Size , oC_UInt_t Alignment )
{
    void * address      = NULL;
    void * heapFrom     = Map->HeapStart;
    void * heapTo       = NULL;
    void * heapToLimit  = NULL;
    oC_UInt_t foundSize = 0;

    while((heapFrom < ((void*)Map->HeapEnd)) && (heapToLimit < ((void*)Map->HeapEnd)) && (address == NULL))
    {
        heapFrom = FindNextHeapPointerInState( Map , heapFrom , NULL , HeapState_NotUsed);

        if(oC_MemMan_IsAddressAlignedTo(heapFrom,Alignment))
        {
            heapToLimit = heapFrom + ALIGN_SIZE(Size,Alignment) + sizeof(oC_UInt_t);
            heapTo      = FindNextHeapPointerInState( Map , heapFrom , heapToLimit , HeapState_Used);

            /* If not found used heap, set it to the end of the heap map */
            if(heapTo == Map->HeapEnd || heapTo == heapToLimit)
            {
                heapTo-= sizeof(oC_UInt_t);
            }

            foundSize = (oC_UInt_t)(heapTo - heapFrom);

            if(foundSize >= ALIGN_SIZE(Size,Alignment))
            {
                address = heapFrom;
                break;
            }
            else
            {
                heapFrom = heapTo;
            }
        }
        else
        {
            heapFrom += sizeof(oC_UInt_t);
        }
    }

    return address;
}

//==========================================================================================================================================
/**
 * Allow to allocate memory in the heap map in the raw way.
 */
//==========================================================================================================================================
static void * RawAlloc( HeapMap_t * Map , oC_UInt_t Size , oC_UInt_t Alignment )
{
    void *      address             = NULL;
    void *      heapFrom            = NULL;
    void *      heapTo              = NULL;
    bool        updateFreeAreaList  = false;

    address = FindBufferOnFreeAreaList(Map,Size,Alignment,true);

    if(address == NULL)
    {
        address             = FindFreeHeap(Map,Size,Alignment);
        updateFreeAreaList  = true;
    }

    heapFrom = address;
    heapTo   = address + ALIGN_SIZE(Size,Alignment);

    if(address != NULL && CheckIfHeapInRangeIsInState( Map , heapFrom, heapTo , HeapState_NotUsed))
    {
        SetHeapStateInRange( Map , heapFrom , heapTo , HeapState_Used );
    }
    else
    {
        updateFreeAreaList = true;
    }

    if(updateFreeAreaList)
    {
        UpdateFreeAreaList(Map);
    }

    return address;
}

//==========================================================================================================================================
/**
 * Allow to release memory in the heap map in the raw way.
 */
//==========================================================================================================================================
static bool RawFree( HeapMap_t * Map , void * Address , oC_UInt_t Size)
{
    bool memoryReleased = false;
    void * endAddress = Address + ALIGN_SIZE(Size,oC_MEM_LLD_MEMORY_ALIGNMENT);

    if(IsAddressOfHeapMap(Map,Address))
    {
        SetHeapStateInRange(Map , Address , endAddress , HeapState_NotUsed);

        if(IsAddressOfHeapMap(Map,endAddress))
        {
            memoryReleased = true;
        }
    }

    return memoryReleased;
}

//==========================================================================================================================================
/**
 * @brief tests memory for the heap usage
 */
//==========================================================================================================================================
static bool IsMemoryValid( void * Start , oC_UInt_t Size )
{
    bool        memoryOk      = true;
    uint8_t*    memoryArray   = Start;
    oC_UInt_t   sizeStepIndex = 0;
    uint8_t     valueToWrite  = 0;
    bool        writeFF       = false;
    oC_UInt_t   sizeSteps[]   = {
                    kB(100) ,
                    B(1) ,
                    B(1) ,
                    B(1) ,
                    B(1) ,
                    B(4) ,
                    B(4) ,
                    B(40) ,
    };

    for(uint32_t i = 0 ; i < Size ; i+= sizeSteps[sizeStepIndex++])
    {
        valueToWrite   = writeFF ? 0xFF : (uint8_t)(i & 0xFF);
        memoryArray[i] = valueToWrite;

        if(sizeStepIndex >= oC_ARRAY_SIZE(sizeSteps))
        {
            sizeStepIndex = 0;
            writeFF = !writeFF;
        }
    }

    sizeStepIndex = 0;
    writeFF       = false;

    for(uint32_t i = 0 ; i < Size ; i+= sizeSteps[sizeStepIndex++])
    {
        valueToWrite   = writeFF ? 0xFF : (uint8_t)(i & 0xFF);

        if(memoryArray[i] != valueToWrite)
        {
            kdebuglog(oC_LogType_Error,"MemMan: External memory access error - (%p) %u != %u\n", &memoryArray[i] , (unsigned int)memoryArray[i] , valueToWrite );
            memoryOk = false;
            break;
        }

        if(sizeStepIndex >= oC_ARRAY_SIZE(sizeSteps))
        {
            sizeStepIndex = 0;
            writeFF = !writeFF;
        }
    }

    return memoryOk;
}

//==========================================================================================================================================
/**
 * Calls event of the allocator.
 */
//==========================================================================================================================================
static void CallAllocatorEvent( Allocator_t Allocator , void * Address , MemoryEventFlags_t EventFlags , const char * Function, uint32_t LineNumber )
{
    if(
            oC_MemMan_IsAllocatorCorrect(Allocator) &&
            (oC_MEM_LLD_IsFlashAddress(Allocator->EventHandler) || oC_MEM_LLD_IsRamAddress(Allocator->EventHandler)) &&
            (oC_Bits_AreBitsSetU32(Allocator->EventFlags , EventFlags))
            )
    {
        Allocator->EventHandler( Address, EventFlags ,Function ,LineNumber );
    }
}

//==========================================================================================================================================
/**
 * Calls event of the module.
 */
//==========================================================================================================================================
static void CallEvent(void * Address , MemoryEventFlags_t EventFlags , const char * Function, uint32_t LineNumber)
{
    if(ModuleEventHandler)
    {
        ModuleEventHandler( Address, EventFlags, Function, LineNumber );
    }
}

//==========================================================================================================================================
/**
 * Interrupt that is called when the memory fault occurs
 */
//==========================================================================================================================================
static void MemoryFaultInterrupt(void)
{
    CallEvent(NULL,MemoryEventFlags_MemoryFault,"",0);
}

//==========================================================================================================================================
/**
 * Interrupt that is called when the bus fault occurs
 */
//==========================================================================================================================================
static void BusFaultInterrupt(void)
{
    CallEvent(NULL,MemoryEventFlags_BusFault,"",0);
}

//==========================================================================================================================================
/**
 * Checks if current thread is allowed for releasing the memory
 */
//==========================================================================================================================================
static bool CanReleaseMemory( Block_t * Block )
{
    bool releaseAllowed = false;

    if(Block->AllocationFlags & AllocationFlags_ReleaseOnlyInCore)
    {
        releaseAllowed = oC_MEM_LLD_GetMemoryAccessMode() == oC_MEM_LLD_MemoryAccessMode_Privileged;
    }
    else
    {
        releaseAllowed = true;
    }

    return releaseAllowed;
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
