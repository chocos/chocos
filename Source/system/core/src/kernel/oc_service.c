/** ****************************************************************************************************************************************
 *
 * @brief      File with source for Service object interface
 * 
 * @file       oc_service.c
 *
 * @author     Patryk Kubiak - (Created on: 19.01.2017 22:48:18) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_service.h>
#include <oc_object.h>
#include <oc_process.h>
#include <oc_string.h>
#include <oc_streamman.h>
#include <oc_intman.h>
#include <oc_processman.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Service_t
{
    oC_ObjectControl_t          ObjectControl;
    oC_Process_t                Process;
    oC_Thread_t                 MainThread;
    oC_DefaultString_t          Name;
    oC_Service_MainFunction_t   MainFunction;
    oC_Service_StartFunction_t  StartFunction;
    oC_Service_StopFunction_t   StopFunction;
    oC_MemorySize_t             MainThreadStackSize;
    oC_Process_Priority_t       Priority;
    oC_MemorySize_t             HeapMapSize;
    oC_Service_Context_t        Context;
    oC_Module_RequiredArray_t   RequiredModules;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool         AreRequiredModulesEnabled   ( oC_Service_t Service );
static void         MainThreadFinished          ( oC_Thread_t Thread , void * Service );
static void         ServiceThread               ( oC_Service_t Service );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with globals
 *
 *  ======================================================================================================================================*/
#define _________________________________________GLOBALS_SECTION____________________________________________________________________________

static const oC_Allocator_t Allocator = {
                .Name   = "Service",
                .CorePwd = oC_MemMan_CORE_PWD
};

#undef  _________________________________________GLOBALS_SECTION____________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief allocates memory for a new service
 *
 * The function is for allocating memory for a service object.
 *
 * @note
 * If the function returns #NULL, you will find an error description in the errors stack.
 *
 * @param Registration          Structure with registration of the service
 *
 * @return pointer to the allocated and initialized service object or NULL in case of error
 */
//==========================================================================================================================================
oC_Service_t oC_Service_New( const oC_Service_Registration_t * Registration )
{
    oC_Service_t service        = NULL;
    oC_Stream_t  stream         = oC_StreamMan_GetStdErrorStream();
    oC_Stream_t  stdoutStream   = NULL;

    if(
        oC_SaveIfFalse("Service::New: Registration - "      , isaddresscorrect(Registration)                    , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse("Service::New: Name - "              , isaddresscorrect(Registration->Name)              , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse("Service::New: Name - "              , strlen(Registration->Name) > 0                    , oC_ErrorCode_StringIsEmpty    )
     && oC_SaveIfFalse("Service::New: StartFunction - "     , isaddresscorrect(Registration->StartFunction)     , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse("Service::New: StopFunction - "      , isaddresscorrect(Registration->StopFunction)      , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse("Service::New: MainFunction - "      , isaddresscorrect(Registration->MainFunction)      , oC_ErrorCode_WrongAddress     )
        )
    {
        stdoutStream = oC_StreamMan_GetStream(Registration->StdoutStreamName);
        stdoutStream = stdoutStream == NULL ? stream : stdoutStream;
        service = kmalloc( sizeof(struct Service_t), &Allocator, AllocationFlags_ZeroFill );

        if(oC_SaveIfFalse("Service::New: Main object - ", service != NULL, oC_ErrorCode_AllocationError))
        {
            oC_User_t user = getcuruser();
            strncpy(service->Name, Registration->Name, sizeof(service->Name));

            user = user == NULL ? getrootuser() : user;

            service->Process                = oC_Process_New( Registration->Priority, service->Name, user, Registration->HeapMapSize, NULL, stdoutStream, stream);
            service->MainThread             = oC_Thread_New ( Registration->Priority, Registration->MainThreadStackSize, service->Process, service->Name, (oC_Thread_Function_t)ServiceThread, service );
            service->MainFunction           = Registration->MainFunction;
            service->StartFunction          = Registration->StartFunction;
            service->StopFunction           = Registration->StopFunction;
            service->MainThreadStackSize    = Registration->MainThreadStackSize;
            service->Priority               = Registration->Priority;
            service->HeapMapSize            = Registration->HeapMapSize;
            service->Context                = NULL;

            memcpy(service->RequiredModules,Registration->RequiredModules,sizeof(service->RequiredModules));

            if(
                oC_SaveIfFalse("Service::New: Process - ", oC_Process_IsCorrect(service->Process)   , oC_ErrorCode_AllocationError )
             && oC_SaveIfFalse("Service::New: Thread - " , oC_Thread_IsCorrect(service->MainThread) , oC_ErrorCode_AllocationError )
                )
            {
                service->ObjectControl = oC_CountObjectControl(service, oC_ObjectId_Service);

                oC_Thread_SetFinishedFunction(service->MainThread,MainThreadFinished,service);

                bool allocationLimitSet = oC_Process_SetAllocationLimit(service->Process, Registration->AllocationLimit);
                oC_SaveIfFalse("Service::New: Cannot set allocation limit ", allocationLimitSet, oC_ErrorCode_InternalDataAreDamaged);
            }
        }
    }

    return service;
}

//==========================================================================================================================================
/**
 * @brief releases memory allocated for a service
 *
 * The function is for releasing memory allocated for the service object. It also kills the service process and service thread.
 *
 * @param Service       Pointer to the memory where the service object is stored.
 *
 * @return true if the memory has been released
 */
//==========================================================================================================================================
bool oC_Service_Delete( oC_Service_t * Service )
{
    bool deleted = false;

    if(
        oC_SaveIfFalse("Service::Delete ", isram(Service)                   , oC_ErrorCode_AddressNotInRam  )
     && oC_SaveIfFalse("Service::Delete ", oC_Service_IsCorrect(*Service)   , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        oC_IntMan_EnterCriticalSection();
        oC_Service_t service        = *Service;

        service->ObjectControl = 0;

        oC_Process_t process        = service->Process;
        oC_Thread_t  thread         = service->MainThread;
        bool         memoryReleased = kfree(service, AllocationFlags_Default);
        bool         processKilled  = oC_Process_Kill( process );
        bool         threadDeleted  = oC_Thread_Delete(&thread);
        bool         processDeleted = oC_Process_Delete(&process);


        *Service = NULL;

        if(
            oC_SaveIfFalse("Service::Delete: Main object - ", memoryReleased, oC_ErrorCode_ReleaseError         )
         && oC_SaveIfFalse("Service::Delete: Process - "    , processKilled , oC_ErrorCode_CannotKillProcess    )
         && oC_SaveIfFalse("Service::Delete: Thread - "     , threadDeleted , oC_ErrorCode_ReleaseError         )
         && oC_SaveIfFalse("Service::Delete: Process - "    , processDeleted, oC_ErrorCode_ReleaseError         )
            )
        {
            deleted = true;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief returns true if the service object is correct
 */
//==========================================================================================================================================
bool oC_Service_IsCorrect( oC_Service_t Service )
{
    return isram(Service) && oC_CheckObjectControl(Service, oC_ObjectId_Service, Service->ObjectControl );
}

//==========================================================================================================================================
/**
 * @brief starts the service
 *
 * The function starts the given service. The service cannot be already started. If it is, you have to stop it first by calling #oC_Service_Stop.
 *
 * @param Service       Service object created by the function #oC_Service_New
 *
 * @return code of error or `oC_ErrorCode_None` in case of success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Service_Start( oC_Service_t Service )
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;
    oC_Stream_t     stream    = oC_StreamMan_GetStdErrorStream();

    if(
        ErrorCondition( oC_Service_IsCorrect(Service)       == true  , oC_ErrorCode_ObjectNotCorrect            )
     && ErrorCondition( oC_Service_IsActive(Service)        == false , oC_ErrorCode_ServiceAlreadyStarted       )
     && ErrorCondition( AreRequiredModulesEnabled(Service)  == true  , oC_ErrorCode_RequiredModuleNotEnabled    )
        )
    {
        if( oC_Process_IsCorrect(Service->Process) == false || oC_Thread_IsCorrect(Service->MainThread) == false )
        {
            Service->Process    = oC_Process_New( Service->Priority, Service->Name, getcuruser(), Service->HeapMapSize, NULL, stream, stream);
            Service->MainThread = oC_Thread_New ( Service->Priority, Service->MainThreadStackSize, Service->Process, Service->Name, (oC_Thread_Function_t)ServiceThread, Service );
        }
        if(
            ErrorCondition( oC_Process_IsCorrect(Service->Process)   , oC_ErrorCode_AllocationError )
         && ErrorCondition( oC_Thread_IsCorrect(Service->MainThread) , oC_ErrorCode_AllocationError )
         && ErrorCode     ( oC_ProcessMan_AddProcess(Service->Process)                              )
         && ErrorCondition( oC_Thread_Run( Service->MainThread )     , oC_ErrorCode_CannotRunThread )
            )
        {
            errorCode        = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief stops the service
 *
 * The function stops the given service. The service has to be already started. If it is not, you have to start it first by calling #oC_Service_Start.
 *
 * @param Service       Service object created by the function #oC_Service_New
 *
 * @return code of error or `oC_ErrorCode_None` in case of success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Service_Stop( oC_Service_t Service )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Service_IsCorrect(Service) == true  , oC_ErrorCode_ObjectNotCorrect      )
     && ErrorCondition( oC_Service_IsActive(Service)  == true  , oC_ErrorCode_ServiceNotStarted     )
        )
    {
        oC_IntMan_EnterCriticalSection();

        if(
            ErrorCondition( isaddresscorrect(Service->StopFunction), oC_ErrorCode_WrongAddress  )
         && ErrorCode     ( Service->StopFunction( &Service->Context )                          )
            )
        {
            if( ErrorCode( oC_Process_Kill(Service->Process) ) )
            {
                Service->Process    = NULL;
                Service->MainThread = NULL;
                errorCode           = oC_ErrorCode_None;
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns true if the service is correct and active
 */
//==========================================================================================================================================
bool oC_Service_IsActive( oC_Service_t Service )
{
    return oC_Service_IsCorrect(Service) && oC_Process_IsActive(Service->Process);
}

//==========================================================================================================================================
/**
 * @brief returns name of the service
 */
//==========================================================================================================================================
const char * oC_Service_GetName( oC_Service_t Service )
{
    const char * name = "incorrect service";

    if(oC_Service_IsCorrect(Service))
    {
        name = Service->Name;
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief returns process associated with the service
 */
//==========================================================================================================================================
oC_Process_t oC_Service_GetProcess( oC_Service_t Service )
{
    oC_Process_t process = NULL;

    if(oC_Service_IsCorrect(Service))
    {
        process = Service->Process;
    }

    return process;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


//==========================================================================================================================================
/**
 * @brief checks if all required modules are enabled
 */
//==========================================================================================================================================
static bool AreRequiredModulesEnabled( oC_Service_t Service )
{
    bool allEnabled = true;

    oC_ARRAY_FOREACH_IN_ARRAY(Service->RequiredModules,moduleReference)
    {
        if((*moduleReference) > oC_Module_None && (*moduleReference) < oC_Module_NumberOfModules)
        {
            if(oC_Module_IsTurnedOn(*moduleReference) == false)
            {
                kdebuglog( oC_LogType_Info, "Cannot start service %s - module ID = %d is not enabled", Service->Name, *moduleReference );
                allEnabled = false;
                break;
            }
        }
    }

    return allEnabled;
}

//==========================================================================================================================================
/**
 * @brief main thread for services
 */
//==========================================================================================================================================
static void ServiceThread( oC_Service_t Service )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_SaveIfFalse("ServiceThread: Service is not correct - ", oC_Service_IsCorrect(Service)            , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse("ServiceThread: StartFunction - "         , isaddresscorrect(Service->StartFunction) , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse("ServiceThread: MainFunction - "          , isaddresscorrect(Service->MainFunction)  , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse("ServiceThread: StopFunction - "          , isaddresscorrect(Service->StopFunction)  , oC_ErrorCode_WrongAddress     )
        )
    {
        if(oC_SaveIfErrorOccur(Service->Name, errorCode = Service->StartFunction( &Service->Context )))
        {
            kdebuglog( oC_LogType_Track , "The service '%s' has been started", Service->Name );
            oC_SaveIfErrorOccur(Service->Name, errorCode = Service->MainFunction (  Service->Context ));
            kdebuglog( oC_ErrorOccur(errorCode) ? oC_LogType_Error : oC_LogType_Track, "The service '%s' has finished with status: '%R'", Service->Name, errorCode);
            oC_SaveIfErrorOccur(Service->Name, errorCode = Service->StopFunction ( &Service->Context ));
            kdebuglog( oC_ErrorOccur(errorCode) ? oC_LogType_Error : oC_LogType_Track, "The service '%s' has been stopped with status: '%R'", Service->Name, errorCode);
        }
        else
        {
            kdebuglog( oC_LogType_Error, "Cannot start service '%s' because of error '%R'", Service->Name , errorCode);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief function called when the main thread has been finished
 */
//==========================================================================================================================================
static void MainThreadFinished( oC_Thread_t Thread , void * Service )
{
    oC_Service_t service = Service;

    if(oC_SaveIfFalse("Service", oC_Service_IsCorrect(service), oC_ErrorCode_ObjectNotCorrect))
    {
        oC_SaveIfErrorOccur("Service Stop"    , oC_Service_Stop  (service)  );
        oC_SaveIfFalse     ("Service Delete"  , oC_Service_Delete(&service) , oC_ErrorCode_ReleaseError );
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________




