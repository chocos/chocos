/** ****************************************************************************************************************************************
 *
 * @file       oc_ktime.c
 *
 * @brief      The file with kernel time functions
 *
 * @author     Patryk Kubiak - (Created on: 30 08 2015 10:54:53)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_ktime.h>
#include <oc_timer_lld.h>
#include <oc_system_cfg.h>
#include <oc_timer.h>
#include <oc_errors.h>
#include <oc_intman.h>

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

static void TimerHandler(oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags );
static void MemoryFaultHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber );

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool                 ModuleEnabledFlag       = false;
static oC_Timestamp_t       TimeStamp               = 0;
static oC_TIMER_Context_t   Context                 = NULL;
extern uint64_t             oC_ThreadManTickCounter;

static const oC_TIMER_Config_t TimerConfig = {
                .Mode                   = oC_TIMER_Mode_Periodic ,
                .MaximumTimeForWait     = CFG_TIME_MAXIMUM_TIME_FOR_INITIALIZATION_OF_SYSTEM ,
                .Frequency              = oC_Frequency_FromTime(CFG_TIME_TIMESTAMP_INCREMENT_TIME) ,
                .PermissibleDifference  = 0 ,
                .MaximumValue           = 10000 ,
                .EventHandler           = TimerHandler ,
                .EventFlags             = oC_TIMER_LLD_EventFlags_TimeoutInterrupt
};

static const oC_Allocator_t Allocator = {
                .Name           = "ktime" ,
                .EventHandler   = MemoryFaultHandler ,
                .EventFlags     = MemoryEventFlags_BufferOverflow |
                                  MemoryEventFlags_PossibleMemoryLeak
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Turns on the module
 * 
 * The function turns on the module
 * 
 * @return code of error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 *  Error code                     | Description
 * --------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ModuleIsTurnedOn | Module is turned on already
 *  #oC_ErrorCode_ImplementError   | Unexpected error
 * 
 * @note More error codes can be returned from the #oC_TIMER_Configure, #oC_TIMER_Start functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KTime_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn)
        )
    {
        TimeStamp         = 0;
        ModuleEnabledFlag = true;
        errorCode         = oC_ErrorCode_None;

        if(
           oC_TIMER_IsTurnedOn() &&
           oC_AssignErrorCode(&errorCode , oC_TIMER_Configure(&TimerConfig , &Context)) &&
           oC_AssignErrorCode(&errorCode , oC_TIMER_Start(Context))
           )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the module
 * 
 * The function turns off the module
 * 
 * @return code of error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 *  Error code                        | Description
 * -----------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ModuleNotStartedYet | Module is turned off. Please call #oC_KTime_TurnOn function
 *  #oC_ErrorCode_ImplementError      | Unexpected error
 * 
 * @note More error codes can be returned from the #oC_TIMER_Stop, #oC_TIMER_Unconfigure functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KTime_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCode(&errorCode , oC_TIMER_Stop(Context)) &&
        oC_AssignErrorCode(&errorCode , oC_TIMER_Unconfigure(&TimerConfig , &Context))
        )
    {
        ModuleEnabledFlag = false;
        errorCode         = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Gets the timestamp
 * 
 * The function gets the timestamp
 * 
 * @return timestamp
 */
//==========================================================================================================================================
oC_Timestamp_t oC_KTime_GetTimestamp( void )
{
    oC_Timestamp_t timeStamp = 0;

    if(oC_TIMER_IsTurnedOn() && Context != NULL)
    {
        uint64_t timerValue = 0;

        oC_TIMER_ReadValue(Context,&timerValue);

        timeStamp = TimeStamp + (timerValue * CFG_TIME_TIMESTAMP_INCREMENT_TIME);
    }
    else
    {
        oC_IntMan_EnterCriticalSection();
        timeStamp = oC_KTime_TickToTime(oC_ThreadManTickCounter);
        oC_IntMan_ExitCriticalSection();
    }

    return timeStamp;
}

//==========================================================================================================================================
/**
 * @brief Gets the current tick
 * 
 * The function gets the current tick
 * 
 * @return current tick
 */
//==========================================================================================================================================
uint64_t oC_KTime_GetCurrentTick ( void )
{
    return oC_ThreadManTickCounter;
}

//==========================================================================================================================================
/**
 * @brief Converts ticks to time
 * 
 * The function converts ticks to time
 * 
 * @param Ticks     ticks
 * 
 * @return time
 */
//==========================================================================================================================================
oC_Time_t oC_KTime_TickToTime( uint64_t Ticks )
{
    return oC_Frequency_ToTime(CFG_FREQUENCY_DEFAULT_SYSTEM_TIMER_FREQUENCY) * Ticks;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Handler for timer
 */
//==========================================================================================================================================
static void TimerHandler(oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , oC_TIMER_LLD_EventFlags_t EventFlags )
{
    TimeStamp += CFG_TIME_TIMESTAMP_INCREMENT_TIME * 10000;
}

//==========================================================================================================================================
/**
 * @brief Handler for memory fault
 */
//==========================================================================================================================================
static void MemoryFaultHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber )
{
    while(1);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
