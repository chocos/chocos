/** ****************************************************************************************************************************************
 *
 * @file       oc_thread.c
 *
 * @brief      The file with source of thread functions
 *
 * @author     Patryk Kubiak - (Created on: 30 08 2015 08:30:03)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_thread.h>
#include <oc_object.h>
#include <oc_memman.h>
#include <oc_sys_lld.h>
#include <oc_system_cfg.h>
#include <oc_stdlib.h>
#include <oc_processman.h>
#include <oc_threadman.h>
#include <oc_intman.h>
#include <oc_mem_lld.h>
#include <oc_debug.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

/**
 * @brief Action to revert
 */
typedef struct
{
    oC_Thread_RevertFunction_t      Function;       //!< Function to call to revert action
    void *                          Object;         //!< Object to revert
    uint32_t                        Parameter;      //!< Parameter to the function
} RevertAction_t;

/**
 * @brief Function to call after thread finish
 */
typedef oC_Thread_FinishedFunction_t FinishedFunction_t;

//==========================================================================================================================================
/**
 * Thread object structure
 */
//==========================================================================================================================================
struct Thread_t
{
    oC_ObjectControl_t       ObjectControl;          ///!< Control value for checking if object is correct
    oC_Process_t             Process;                ///!< Process owner
    oC_Thread_Priority_t     Priority;               ///!< Priority of this thread
    const char *             Name;                   ///!< Name of the thread
    oC_UInt_t *              RedZone;                ///!< Pointer to the red zone
    oC_UInt_t *              RedZoneEnd;             ///!< Pointer to end of the RedZone (start of the real stack)
    void *                   RealStackStart;         ///!< Pointer to the real stack (real allocated memory)
    oC_SYS_LLD_Context_t *   Context;                ///!< Machine context (stack)
    void *                   StackStart;             ///!< Pointer to the normal stack (not red zone)
    oC_MemorySize_t          RealStackSize;          ///!< Size of the stack with red zone
    uint32_t *               BlockedFlag;            ///!< Reference to the flag of the blocked context
    uint32_t                 StateToUnblock;         ///!< State to unblock thread
    uint32_t                 UnblockMask;            ///!< Mask with bits important for unblocking
    oC_Thread_Unblock_t      Unblock;                ///!< Type of operation to unblock thread
    oC_Timestamp_t           TimeoutTimestamp;       ///!< Timeout for the blocked state
    oC_Time_t                ExecutionTime;          ///!< Time of execution of this thread
    uint32_t                 LastExecutionTick;      ///!< Tick of last execution
    oC_Int_t                 StackSize;              ///!< Size of the stack
    oC_Thread_Function_t     Function;               ///!< Main function to execute
    void *                   Parameter;              ///!< Parameter to main function
    FinishedFunction_t       FinishedFunction;       ///!< Function to call after thread finish
    void *                   FinishedParameter;      ///!< Parameter to give to the thread finish
    RevertAction_t           ActionsToRevert[CFG_UINT8_MAX_ACTIONS_TO_REVERT];  //!< Array with actions to revert
};

/**
 * @brief Function to check if thread is unblocked
 */
typedef bool (*CheckFunction_t)( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void ThreadExitHandler( void );
static bool CheckUnblockedWhenEqual                 ( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );
static bool CheckUnblockedWhenNotEqual              ( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );
static bool CheckUnblockedWhenSmaller               ( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );
static bool CheckUnblockedWhenGreater               ( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );
static bool CheckUnblockedWhenSmallerOrEqual        ( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );
static bool CheckUnblockedWhenGreaterOrEqual        ( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );
static bool CheckUnblockedWhenAtLeastOneBitIsSet    ( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );
static bool CheckUnblockedWhenAtLeastOneBitIsCleared( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const uint16_t           NotUsedByteValue = 0xbeef;
static const CheckFunction_t    CheckFunctions[] = {
                CheckUnblockedWhenEqual ,
                CheckUnblockedWhenNotEqual,
                CheckUnblockedWhenSmaller ,
                CheckUnblockedWhenGreater ,
                CheckUnblockedWhenSmallerOrEqual ,
                CheckUnblockedWhenGreaterOrEqual ,
                CheckUnblockedWhenAtLeastOneBitIsSet ,
                CheckUnblockedWhenAtLeastOneBitIsCleared ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup Thread
//! @{


//==========================================================================================================================================
/**
 * @brief creates new thread object
 * 
 * The function creates new thread object. The thread is not started yet - to start it, the function #oC_Thread_Run should be called.
 * 
 * To delete the thread, the function #oC_Thread_Delete should be called.
 * 
 * @param Priority              Priority of the thread (if 0, the priority is the same as the process' priority)
 * @param StackSize             Size of the stack (if 0, the default size is used)
 * @param Process               Process owner of the thread (if NULL, the current process is used)
 * @param Name                  Name of the thread
 * @param Function              Function to execute by the thread
 * @param Parameter             Parameter to the function - please note, that the parameter should be allocated in the same process as the thread (and cannot be on the stack)
 * 
 * @return pointer to the thread object or NULL if error occurs
 * 
 * @see oC_Thread_Delete oC_Thread_Run
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Thread_Run(thread);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
oC_Thread_t oC_Thread_New( oC_Thread_Priority_t Priority , oC_Int_t StackSize , void * Process , const char * Name , oC_Thread_Function_t Function , void * Parameter )
{
    oC_Thread_t thread = NULL;

    if(oC_SYS_LLD_IsMachineSupportMultithreadMode())
    {
        Allocator_t     allocator = NULL;
        oC_Process_t    process   = Process;

        if(process == NULL)
        {
            process   = oC_ProcessMan_GetCurrentProcess();
        }

        if(oC_Process_IsCorrect(process) == false)
        {
            kdebuglog(oC_LogType_Error,"thread: process 0x%08X is not correct" , Process);
        }

        allocator = oC_Process_GetAllocator(process);

        if(StackSize == 0)
        {
            StackSize = oC_ThreadMan_GetDefaultStackSize();
        }

        StackSize = oC_MemMan_AlignSizeTo(StackSize,oC_MEM_LLD_STACK_ALIGNMENT);

        if(allocator)
        {
            thread = kmalloc(sizeof(struct Thread_t) , allocator , AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill );

            if(thread)
            {
                oC_MemorySize_t redZoneSize        = oC_ThreadMan_GetRedZoneSize();
                oC_MemorySize_t minimumContextSize = oC_SYS_LLD_GetMinimumContextSize(StackSize + redZoneSize);
                void * realStack = kamalloc(minimumContextSize , allocator , AllocationFlags_CanWait1Second , oC_MEM_LLD_STACK_ALIGNMENT );
                void * endStack  = (void*)(((oC_UInt_t)realStack) + (oC_UInt_t)minimumContextSize);

                /* Filling a stack with special value for checking if all stack is used */
                for(uint16_t * stack = (uint16_t*)realStack; stack < ((uint16_t*)endStack) ; stack++ )
                {
                    *stack = NotUsedByteValue;
                }

                if(realStack)
                {
                    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

                    thread->ObjectControl       = oC_CountObjectControl(thread,oC_ObjectId_Thread);
                    thread->Name                = Name;
                    thread->Priority            = Priority + oC_Process_GetPriority(process);
                    thread->BlockedFlag         = NULL;
                    thread->TimeoutTimestamp    = 0;
                    thread->ExecutionTime       = 0;
                    thread->RealStackSize       = minimumContextSize;
                    thread->StackSize           = StackSize;
                    thread->Process             = process;
                    thread->Function            = Function;
                    thread->Parameter           = Parameter;
                    thread->RealStackStart      = realStack;

                    if(oC_SYS_LLD_IsStackPushDecrementPointer())
                    {
                        // 0x00000000 - ,----------------------------------------.
                        //              |                RED ZONE                |
                        //              |----------------------------------------|
                        //              |                 STACK                  |
                        // 0xFFFFFFFF - '----------------------------------------'
                        thread->RedZone   = realStack;
                        thread->Context   = oC_MemMan_AlignAddressTo((void*)(((oC_UInt_t)realStack) + (oC_UInt_t)redZoneSize),oC_MEM_LLD_STACK_ALIGNMENT);
                    }
                    else
                    {
                        // 0x00000000 - ,----------------------------------------.
                        //              |                 STACK                  |
                        //              |----------------------------------------|
                        //              |                RED ZONE                |
                        // 0xFFFFFFFF - '----------------------------------------'
                        thread->RedZone   = (void*)(((oC_UInt_t)realStack) + (oC_UInt_t)StackSize);
                        thread->Context   = realStack;
                    }

                    thread->StackStart = thread->Context;
                    thread->RedZoneEnd = oC_MemMan_AlignAddressTo((void*)(((oC_UInt_t)thread->RedZone) + (oC_UInt_t)redZoneSize),oC_MEM_LLD_STACK_ALIGNMENT);

                    errorCode = oC_SYS_LLD_InitializeContext(&thread->Context,StackSize,Function,Parameter,ThreadExitHandler);

                    if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
                    {
                        oC_SYS_LLD_TurnOnDriver();
                        errorCode = oC_SYS_LLD_InitializeContext(&thread->Context,StackSize,Function,Parameter,ThreadExitHandler);
                    }

                    if(oC_ErrorOccur(errorCode))
                    {
                        oC_SaveError("Thread: Cannot initialize context" , errorCode);

                        if(!kfree(realStack , AllocationFlags_CanWait1Second) ||
                           !kfree(thread , AllocationFlags_CanWait1Second)
                           )
                        {
                            oC_SaveError("Thread: Cannot release memory" , oC_ErrorCode_AllocationError);
                        }
                        thread = NULL;
                    }
                }
                else
                {
                    bool stackReleased  = kfree(realStack,0);

                    oC_SaveIfFalse("Stack"      , stackReleased || realStack          == NULL, oC_ErrorCode_ReleaseError);
                    if(!kfree(thread,AllocationFlags_CanWait1Second))
                    {
                        oC_SaveError("Thread: Cannot release memory" , oC_ErrorCode_AllocationError);
                    }
                    thread = NULL;
                }
            }
        }

    }

    return thread;
}

//==========================================================================================================================================
/**
 * @brief checks if the given address is owned by the thread stack
 */
//==========================================================================================================================================
bool oC_Thread_IsOwnedByStack( oC_Thread_t Thread , const void * Address , bool * outAddressInRedZone )
{
    bool owned = false;

    if(oC_Thread_IsCorrect(Thread) && (outAddressInRedZone == NULL || isram(outAddressInRedZone)))
    {
        void * stackEnd     = Thread->StackStart;
        void * stackStart   = Thread->StackStart;

        stackEnd += Thread->StackSize;

        if(outAddressInRedZone != NULL)
        {
            void * redZoneStart = Thread->RedZone;
            void * redZoneEnd   = Thread->RedZoneEnd;
            *outAddressInRedZone = Address >= redZoneStart && Address <= redZoneEnd;
        }

        owned = Address >= stackStart && Address < stackEnd;
    }

    return owned;
}

//==========================================================================================================================================
/**
 * @brief clones the thread with new stack
 * 
 * The function clones the thread with new stack. The new stack is created with the given size. 
 * The new thread is not started yet - to start it, the function #oC_Thread_Run should be called.
 * 
 * To delete the thread, the function #oC_Thread_Delete should be called.
 * 
 * @param Thread                Thread to clone
 * @param NewStackSize          Size of the new stack
 * 
 * @return pointer to the new thread object or NULL if error occurs
 * 
 * @see oC_Thread_Delete oC_Thread_Run
 * 
 * @code{.c}
  
    oC_Thread_t clonedThread = oC_Thread_CloneWithNewStack( thread, 1024 );
    if(clonedThread)
    {
        oC_Thread_Run(clonedThread);
    }
    else
    {
        printf("Cannot clone thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
oC_Thread_t oC_Thread_CloneWithNewStack( oC_Thread_t Thread , oC_MemorySize_t NewStackSize )
{
    oC_Thread_t newThread = NULL;

    if(oC_Thread_IsCorrect(Thread) && NewStackSize > 0)
    {
        newThread = oC_Thread_New(Thread->Priority,NewStackSize,Thread->Process,Thread->Name,Thread->Function,Thread->Parameter);
    }

    return newThread;
}

//==========================================================================================================================================
/**
 * @brief deletes the thread
 * 
 * The function deletes the thread. The thread is stopped and all resources are released.
 * 
 * @param Thread                Reference to the thread to delete. For safety reasons the reference is set to NULL after deleting the thread.
 * 
 * @return true if thread has been deleted
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Thread_Run(thread);
        oC_Thread_Delete(&thread);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_Delete( oC_Thread_t * Thread )
{
    bool deleted = false;

    if(isram(Thread) && oC_Thread_IsCorrect(*Thread))
    {
        if(oC_ThreadMan_GetCurrentThread() != (*Thread))
        {
            uint32_t blockingFlag  = 0;
            bool     threadRemoved = false;

            oC_Thread_SetBlocked(*Thread,&blockingFlag,oC_Thread_Unblock_WhenEqual,0xff,oC_Thread_UnblockMask_All,oC_hour(1));

            oC_IntMan_EnterCriticalSection();

            if(oC_Thread_IsActive(*Thread))
            {
                threadRemoved               = oC_SaveIfErrorOccur("Thread - Removing from list: ", oC_ThreadMan_RemoveThread(*Thread));
                deleted                     = threadRemoved;
                (*Thread)->ObjectControl    = 0;
            }
            else
            {
                void * realStack = (*Thread)->RealStackStart;

                bool     stackReleased = false;
                bool     objectReleased= false;

                oC_ARRAY_FOREACH_IN_ARRAY((*Thread)->ActionsToRevert,action)
                {
                    if(action->Function != NULL)
                    {
                        if(oC_SaveIfFalse("cannot revert action", isaddresscorrect(action->Function), oC_ErrorCode_WrongAddress))
                        {
                            oC_Thread_RevertFunction_t function = action->Function;
                            action->Function = NULL;
                            oC_SaveIfFalse("cannot revert action",  function(*Thread,action->Object,action->Parameter), oC_ErrorCode_InternalDataAreDamaged );
                        }
                    }
                }

                oC_Thread_t thread = *Thread;

                if(thread->FinishedFunction != NULL)
                {
                    if(oC_SaveIfFalse("Finished Function", isaddresscorrect(thread->FinishedFunction), oC_ErrorCode_WrongAddress))
                    {
                        oC_Thread_FinishedFunction_t function = thread->FinishedFunction;
                        thread->FinishedFunction = NULL;
                        function(thread, thread->FinishedParameter);
                    }
                }

                threadRemoved = true;
                (*Thread)->ObjectControl = 0;

                stackReleased  = kfree(realStack, AllocationFlags_CanWaitForever);
                objectReleased = kfree(*Thread,   AllocationFlags_CanWaitForever);

                if(
                    threadRemoved
                 && oC_SaveIfFalse("Thread - Stack  - ", stackReleased  , oC_ErrorCode_ReleaseError )
                 && oC_SaveIfFalse("Thread - Object - ", objectReleased , oC_ErrorCode_ReleaseError )
                    )
                {
                    *Thread = NULL;
                    deleted = true;
                }

            }
            oC_IntMan_ExitCriticalSection();

        }
        else
        {
            oC_IntMan_EnterCriticalSection();
            /* This is current thread, we cannot delete it now */
            oC_ErrorCode_t errorCode = oC_ThreadMan_RemoveThread(*Thread);

            if(oC_ErrorOccur(errorCode))
            {
                oC_SaveError((*Thread)->Name , errorCode);
            }
            else
            {
                deleted = true;
            }

            oC_IntMan_ExitCriticalSection();

            if(oC_IntMan_AreInterruptsTurnedOn())
            {
                while(deleted);
            }
        }
    }


    return deleted;
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is correct
 * 
 * The function checks if the thread is correct. The thread is correct if it is allocated in the dynamic memory and the object control is correct.
 * 
 * @param Thread                Thread to check
 * 
 * @return true if the thread is correct
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(oC_Thread_IsCorrect(thread))
    {
        printf("Thread is correct\n");
    }
    else
    {
        printf("Thread is not correct\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_IsCorrect( oC_Thread_t Thread )
{
    return oC_MemMan_IsDynamicAllocatedAddress(Thread) && oC_CheckObjectControl(Thread,oC_ObjectId_Thread,Thread->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is active
 * 
 * The function checks if the thread is active. The thread is active if it is in the thread list.
 * 
 * @param Thread                Thread to check
 * 
 * @return true if the thread is active
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(oC_Thread_IsActive(thread))
    {
        printf("Thread is active\n");
    }
    else
    {
        printf("Thread is not active\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_IsActive( oC_Thread_t Thread )
{
    return oC_Thread_IsCorrect(Thread) && oC_ThreadMan_ContainsThread(Thread);
}

//==========================================================================================================================================
/**
 * @brief sets the thread as blocked
 * 
 * The function sets the thread as blocked. The thread is blocked until the BlockingFlag is equal to StateToUnblock or the Timeout is reached.
 * 
 * @param Thread                Thread to block
 * @param BlockingFlag          Flag to check if the thread should be unblocked
 * @param Unblock               Type of unblocking
 * @param StateToUnblock        State to unblock the thread
 * @param UnblockMask           Mask for unblocking
 * @param Timeout               Timeout for the blocked state
 * 
 * @return true if the thread is blocked
 * 
 * @see oC_Thread_SetUnblocked
 * 
 * @code{.c}
  
    uint32_t flag = 0;
    if(oC_Thread_SetBlocked(thread,&flag,oC_Thread_Unblock_WhenEqual,0xff,oC_Thread_UnblockMask_All,oC_hour(1)))
    {
        printf("Thread is blocked\n");
    }
    else
    {
        printf("Thread is not blocked\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_SetBlocked( oC_Thread_t Thread , uint32_t * BlockingFlag , oC_Thread_Unblock_t Unblock , uint32_t StateToUnblock , oC_Thread_UnblockMask_t UnblockMask , oC_Time_t Timeout )
{
    bool blocked = false;

    if(
        oC_Thread_IsCorrect(Thread) &&
        oC_MemMan_IsRamAddress(BlockingFlag) &&
        Timeout >= 0
        )
    {
        oC_IntMan_EnterCriticalSection();
        Thread->BlockedFlag         = BlockingFlag;
        Thread->StateToUnblock      = StateToUnblock;
        Thread->TimeoutTimestamp    = oC_KTime_GetTimestamp() + Timeout;
        Thread->Unblock             = Unblock;
        Thread->UnblockMask         = UnblockMask;
        oC_IntMan_ExitCriticalSection();

        oC_ThreadMan_SwitchThread();

        blocked = *BlockingFlag != StateToUnblock;
    }

    return blocked;
}

//==========================================================================================================================================
/**
 * @brief sets the thread as unblocked
 * 
 * The function sets the thread as unblocked. The thread is unblocked and can be executed.
 * 
 * @param Thread                Thread to unblock
 * 
 * @return true if the thread is unblocked
 * 
 * @see oC_Thread_SetBlocked
 * 
 * @code{.c}
  
    if(oC_Thread_SetUnblocked(thread))
    {
        printf("Thread is unblocked\n");
    }
    else
    {
        printf("Thread is not unblocked\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_SetUnblocked( oC_Thread_t Thread )
{
    bool unblocked = false;

    if(oC_Thread_IsCorrect(Thread))
    {
        oC_IntMan_EnterCriticalSection();
        Thread->BlockedFlag         = NULL;
        Thread->TimeoutTimestamp    = 0;
        unblocked                   = true;
        oC_IntMan_ExitCriticalSection();

        oC_ThreadMan_SwitchThread();
    }

    return unblocked;
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is blocked
 * 
 * The function checks if the thread is blocked. The thread is blocked if the BlockingFlag is not equal to StateToUnblock or the Timeout is not reached.
 * 
 * @param Thread                Thread to check
 * 
 * @return true if the thread is blocked
 * 
 * @see oC_Thread_SetBlocked
 * 
 * @code{.c}
  
    if(oC_Thread_IsBlocked(thread))
    {
        printf("Thread is blocked\n");
    }
    else
    {
        printf("Thread is not blocked\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_IsBlocked( oC_Thread_t Thread )
{
    bool blocked = true;

    if(oC_Thread_IsCorrect(Thread))
    {
        oC_IntMan_EnterCriticalSection();
        oC_Timestamp_t currentTimestamp = oC_KTime_GetTimestamp();
        oC_Timestamp_t timeout          = Thread->TimeoutTimestamp;
        uint32_t       blockedFlag      = *Thread->BlockedFlag;
        uint32_t       stateToUnblock   = Thread->StateToUnblock;
        uint32_t       unblockMask      = Thread->UnblockMask;
        uint32_t*      blockedFlagRef   = Thread->BlockedFlag;
        oC_IntMan_ExitCriticalSection();

        if(
            ((blockedFlagRef == NULL) && (currentTimestamp < Thread->TimeoutTimestamp)) ||
            ((blockedFlagRef != NULL) && (CheckFunctions[Thread->Unblock](blockedFlag,stateToUnblock,unblockMask)) && (currentTimestamp < timeout))
            )
        {
            blocked = true;
        }
        else
        {
            blocked = false;
        }
    }

    return blocked;
}


//==========================================================================================================================================
/**
 * @brief checks if the thread is blocked by the given flag
 * 
 * The function checks if the thread is blocked by the given flag. The thread is blocked if the BlockingFlag is not equal to StateToUnblock or the Timeout is not reached.
 * 
 * @param Thread                Thread to check
 * @param BlockingFlag          Flag to check
 * 
 * @return true if the thread is blocked
 * 
 * @see oC_Thread_SetBlocked
 * 
 * @code{.c}
  
    uint32_t flag = 0;
    if(oC_Thread_SetBlocked(thread,&flag,oC_Thread_Unblock_WhenEqual,0xff,oC_Thread_UnblockMask_All,oC_hour(1)))
    {
        printf("Thread is blocked\n");
    }
    else
    {
        printf("Thread is not blocked\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_IsBlockedBy( oC_Thread_t Thread , uint32_t * BlockingFlag )
{
    return oC_Thread_IsCorrect(Thread) && isram(BlockingFlag) && Thread->BlockedFlag == BlockingFlag;
}

//==========================================================================================================================================
/**
 * @brief runs the thread
 * 
 * The function runs the thread. The thread is added to the thread list and can be executed.
 * 
 * @param Thread                Thread to run
 * 
 * @return true if the thread is runned
 * 
 * @see oC_Thread_Delete, oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(oC_Thread_Run(thread))
    {
        printf("Thread is runned\n");
    }
    else
    {
        printf("Thread is not runned\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_Run( oC_Thread_t Thread )
{
    bool runned = false;

    if(oC_Thread_IsCorrect(Thread))
    {
        oC_ErrorCode_t errorCode = oC_ThreadMan_AddThread(Thread);

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError(Thread->Name,errorCode);
        }
        else
        {
            runned = true;
        }
    }

    return runned;
}

//==========================================================================================================================================
/**
 * @brief cancels the thread
 * 
 * The function cancels the thread. The thread is removed from the thread list and all resources are released.
 * 
 * @param Thread                Thread to cancel
 * 
 * @return true if the thread is canceled
 * 
 * @see oC_Thread_Delete, oC_ThreadMan_RemoveThread
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(oC_Thread_Run(thread))
    {
        oC_Thread_Cancel(thread);
    }
    else
    {
        printf("Thread is not runned\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_Cancel( oC_Thread_t * Thread )
{
    bool canceled = false;

    if(oC_Thread_IsCorrect(*Thread))
    {
        oC_Thread_t thread = *Thread;

        if(thread->FinishedFunction != NULL)
        {
            if(oC_SaveIfFalse("Finished Function", isaddresscorrect(thread->FinishedFunction), oC_ErrorCode_WrongAddress))
            {
                oC_Thread_FinishedFunction_t function = thread->FinishedFunction;
                thread->FinishedFunction = NULL;
                function(thread, thread->FinishedParameter);
            }
        }

        oC_ErrorCode_t errorCode = oC_ThreadMan_RemoveThread(*Thread);

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError((*Thread)->Name , errorCode);
        }
        else
        {
            canceled = true;
            *Thread  = NULL;
        }
    }

    return canceled;
}

//==========================================================================================================================================
/**
 * @brief gets the thread context
 * 
 * The function gets the thread context - the machine context (stack) of the thread.
 * 
 * @param Thread                Thread to get context
 * 
 * @return pointer to the thread context or NULL if error occurs
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        void * context = oC_Thread_GetContext(thread);
        if(context)
        {
            printf("Thread context is 0x%08X\n",context);
        }
        else
        {
            printf("Cannot get thread context\n");
        }
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
void * oC_Thread_GetContext( oC_Thread_t Thread )
{
    void * context = NULL;

    if(oC_Thread_IsCorrect(Thread))
    {
        context = Thread->Context;
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief gets the thread priority
 * 
 * The function gets the thread priority.
 * 
 * @param Thread                Thread to get priority
 * 
 * @return priority of the thread
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Thread_Priority_t priority = oC_Thread_GetPriority(thread);
        printf("Thread priority is %d\n",priority);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
oC_Thread_Priority_t oC_Thread_GetPriority( oC_Thread_t Thread )
{
    oC_Thread_Priority_t priority = 0;

    if(oC_Thread_IsCorrect(Thread))
    {
        priority = Thread->Priority;
    }

    return priority;
}

//==========================================================================================================================================
/**
 * @brief gets the thread parameter
 * 
 * The function gets the thread parameter - the parameter to the main function of the thread.
 * 
 * @param Thread                Thread to get parameter
 * 
 * @return pointer to the thread parameter or NULL if error occurs
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
    
    int parameter = 13;
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , &parameter );
    if(thread)
    {
        void * parameter = oC_Thread_GetParameter(thread);
        if(parameter)
        {
            printf("Thread parameter is 0x%08X\n",parameter); // should be pointer to the 'parameter' variable
        }
        else
        {
            printf("Cannot get thread parameter\n");
        }
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
void * oC_Thread_GetParameter( oC_Thread_t Thread )
{
    void * parameter = NULL;

    if(oC_Thread_IsCorrect(Thread))
    {
        parameter = Thread->Parameter;
    }

    return parameter;
}

//==========================================================================================================================================
/**
 * @brief gets the thread process
 * 
 * The function gets the thread process - the process owner of the thread.
 * 
 * @param Thread                Thread to get process
 * 
 * @return pointer to the thread process or NULL if error occurs
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Process_t process = oC_Thread_GetProcess(thread);
        if(process)
        {
            printf("Thread process is 0x%08X\n",process);
        }
        else
        {
            printf("Cannot get thread process\n");
        }
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
const char * oC_Thread_GetName( oC_Thread_t Thread )
{
    const char * name = "incorrect thread";

    if(oC_Thread_IsCorrect(Thread))
    {
        name = Thread->Name;
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief sleeps the thread
 * 
 * The function sleeps the thread for the given time. The thread is blocked for the given time.
 * 
 * @param Thread                Thread to sleep
 * @param Time                  Time to sleep
 * 
 * @return true if the thread is asleep
 * 
 * @see oC_Thread_SetBlocked
 * @code{.c}
      
     if(oC_Thread_Sleep(thread,oC_hour(1)))
     {
          printf("Thread is asleep\n");
     }
     else
     {
          printf("Thread is not asleep\n");
     }   
    
    @endcode
 */
//==========================================================================================================================================
bool oC_Thread_Sleep( oC_Thread_t Thread , oC_Time_t Time )
{
    bool asleep = false;

    if(oC_Thread_IsCorrect(Thread))
    {
        oC_IntMan_EnterCriticalSection();

        Thread->BlockedFlag         = NULL;
        Thread->TimeoutTimestamp    = oC_KTime_GetTimestamp() + Time;
        asleep                      = true;
        oC_IntMan_ExitCriticalSection();

        while(oC_Thread_IsBlocked(Thread));
    }

    return asleep;
}

//==========================================================================================================================================
/**
 * @brief gets the thread execution time
 * 
 * The function gets the thread execution time - the time when the thread was executed.
 * 
 * @param Thread                Thread to get execution time
 * 
 * @return execution time of the thread
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Time_t executionTime = oC_Thread_GetExecutionTime(thread);
        printf("Thread execution time is %d\n",executionTime);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
oC_Time_t oC_Thread_GetExecutionTime  ( oC_Thread_t Thread )
{
    oC_Time_t executionTime = 0;

    if(oC_Thread_IsCorrect(Thread))
    {
        executionTime = Thread->ExecutionTime;
    }

    return executionTime;
}

//==========================================================================================================================================
/**
 * @brief adds the time to the thread execution time
 * 
 * The function adds the time to the thread execution time.
 * 
 * @param Thread                Thread to add execution time
 * @param Time                  Time to add
 * 
 * @return true if the time is added
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Thread_AddToExecutionTime(thread,oC_hour(1));
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_AddToExecutionTime( oC_Thread_t Thread , oC_Time_t Time )
{
    bool added = false;

    if(oC_Thread_IsCorrect(Thread))
    {
        Thread->ExecutionTime += Time;
    }

    return added;
}

//==========================================================================================================================================
/**
 * @brief gets the last execution tick of the thread
 * 
 * The function gets the last execution tick of the thread - the tick when the thread was executed.
 * 
 * @param Thread                Thread to get last execution tick
 * 
 * @return last execution tick of the thread
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Time_t lastExecutionTick = oC_Thread_GetLastExecutionTick(thread);
        printf("Thread last execution tick is %d\n",lastExecutionTick);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
uint64_t oC_Thread_GetLastExecutionTick(oC_Thread_t Thread )
{
    uint64_t tick = 0;

    if(oC_Thread_IsCorrect(Thread))
    {
        tick = Thread->LastExecutionTick;
    }

    return tick;
}

//==========================================================================================================================================
/**
 * @brief sets the last execution tick of the thread
 * 
 * The function sets the last execution tick of the thread - the tick when the thread was executed.
 * 
 * @param Thread                Thread to set last execution tick
 * @param Tick                  Last execution tick
 * 
 * @return true if the last execution tick is set
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_Thread_SetLastExecutionTick(thread,1000);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
bool oC_Thread_SetLastExecutionTick(oC_Thread_t Thread , uint64_t Tick )
{
    bool set = false;

    if(oC_Thread_IsCorrect(Thread))
    {
        Thread->LastExecutionTick = Tick;
    }

    return set;
}

//==========================================================================================================================================
/**
 * @brief gets the thread stack size
 * 
 * The function gets the thread stack size.
 * 
 * @param Thread                Thread to get stack size
 * 
 * @return stack size of the thread
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_UInt_t stackSize = oC_Thread_GetStackSize(thread);
        printf("Thread stack size is %d\n",stackSize);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
oC_UInt_t oC_Thread_GetStackSize( oC_Thread_t Thread )
{
    oC_UInt_t size = 0;

    if(oC_Thread_IsCorrect(Thread))
    {
        size = oC_SYS_LLD_GetContextStackSize(Thread->Context);

        if(size != Thread->StackSize)
        {
            size = Thread->StackSize;
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief gets the thread free stack size
 * 
 * The function gets the thread free stack size.
 * 
 * @param Thread                Thread to get free stack size
 * @param Current               If true, the function gets the current free stack size. If false, the function gets the free stack size without the red zone.
 * 
 * @return free stack size of the thread
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(thread)
    {
        oC_UInt_t freeStackSize = oC_Thread_GetFreeStackSize(thread,true);
        printf("Thread free stack size is %d\n",freeStackSize);
    }
    else
    {
        printf("Cannot create thread\n");
    }   

   @endcode
 */
//==========================================================================================================================================
oC_Int_t oC_Thread_GetFreeStackSize( oC_Thread_t Thread , bool Current )
{
    oC_Int_t size = 0;

    if(oC_Thread_IsCorrect(Thread))
    {
        if(Current)
        {
            size = oC_SYS_LLD_GetContextFreeStackSize(Thread->Context);
        }
        else
        {
            oC_UInt_t contextSize = oC_SYS_LLD_GetContextStackSize(Thread->Context);
            uint16_t* redZoneEnd  = (void*)(((oC_UInt_t)Thread->RedZoneEnd));

            //==========================================================================================================================
            /*
             * Checking if red zone is was used
             */
            //==========================================================================================================================
            for(uint16_t * stack = (uint16_t*)Thread->RedZone ; stack < redZoneEnd ; stack++ )
            {
                if(*stack != NotUsedByteValue)
                {
                    size-=sizeof(uint16_t);
                }
            }

            if(size == 0)
            {
                uint16_t * stackEnd = (Thread->StackStart + contextSize);

                //======================================================================================================================
                /*
                 * If 'Red-Zone' was not used, then check if there is any stack free
                 */
                //======================================================================================================================
                for(uint16_t * stack = Thread->StackStart ; stack < stackEnd ; stack++ )
                {
                    if(*stack == NotUsedByteValue)
                    {
                        size+=sizeof(uint16_t);
                    }
                }
            }
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief saves the action to revert
 * 
 * The function saves the action to revert. The action is called when the thread is deleted.
 * 
 * @param Thread                Thread to save action
 * @param Function              Function to revert
 * @param Object                Object to revert
 * @param Parameter             Parameter to revert
 * 
 * @return true if the action is saved
 * 
 * @see oC_Thread_RemoveFromRevert
 * 
 * @code{.c}
      
     void RevertFunction( oC_Thread_t Thread , void * Object , uint32_t Parameter )
     {
          printf("Reverting action\n");
     }
    
     oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
     if(thread)
     {
          if(oC_Thread_SaveToRevert(thread,RevertFunction,NULL,0))
          {
                printf("Action saved\n");
          }
          else
          {
                printf("Action not saved\n");
          }
     }
     else
     {
          printf("Cannot create thread\n");
     }   
    
    @endcode
 */
//==========================================================================================================================================
bool oC_Thread_SaveToRevert( oC_Thread_t Thread , oC_Thread_RevertFunction_t Function, void * Object , uint32_t Parameter )
{
    bool success = false;

    if(
        oC_SaveIfFalse("Thread object", oC_Thread_IsCorrect(Thread), oC_ErrorCode_ObjectNotCorrect  )
     && oC_SaveIfFalse("Function"     , isaddresscorrect(Function) , oC_ErrorCode_WrongAddress      )
        )
    {
        oC_IntMan_EnterCriticalSection();
        oC_ARRAY_FOREACH_IN_ARRAY(Thread->ActionsToRevert,action)
        {
            if(action->Function == NULL)
            {
                action->Function = Function;
                action->Object   = Object;
                action->Parameter= Parameter;

                success = true;
                break;
            }
        }
        oC_IntMan_ExitCriticalSection();
        oC_SaveIfFalse("Cannot add function", success, oC_ErrorCode_NoFreeSlots);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief removes the action to revert
 * 
 * The function removes the action to revert.
 * 
 * @param Thread                Thread to remove action
 * @param Function              Function to revert
 * @param Object                Object to revert
 * 
 * @return true if the action is removed
 * 
 * @see oC_Thread_SaveToRevert
 */
//==========================================================================================================================================
bool oC_Thread_RemoveFromRevert( oC_Thread_t Thread , oC_Thread_RevertFunction_t Function, void * Object )
{
    bool success = false;

    if(
        oC_SaveIfFalse("Thread object", oC_Thread_IsCorrect(Thread), oC_ErrorCode_ObjectNotCorrect  )
     && oC_SaveIfFalse("Function"     , isaddresscorrect(Function) , oC_ErrorCode_WrongAddress      )
        )
    {
        oC_IntMan_EnterCriticalSection();
        oC_ARRAY_FOREACH_IN_ARRAY(Thread->ActionsToRevert,action)
        {
            if(action->Function == Function && action->Object == Object)
            {
                action->Function    = NULL;
                action->Parameter   = 0;
                action->Object      = NULL;
                success = true;
                break;
            }
        }
        oC_IntMan_ExitCriticalSection();
        oC_SaveIfFalse("Cannot remove function", success, oC_ErrorCode_ObjectNotFoundOnList);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief sets the thread finished function
 * 
 * The function sets the thread finished function. The function is called when the thread is finished.
 * 
 * @param Thread                Thread to set finished function
 * @param Function              Function to call
 * @param Parameter             Parameter to call
 * 
 * @return true if the function is set
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
      
     void FinishedFunction( oC_Thread_t Thread , void * Parameter )
     {
          printf("Thread finished\n");
     }
    
     oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
     if(thread)
     {
          if(oC_Thread_SetFinishedFunction(thread,FinishedFunction,NULL))
          {
                printf("Function set\n");
          }
          else
          {
                printf("Function not set\n");
          }
     }
     else
     {
          printf("Cannot create thread\n");
     }   
    
    @endcode
 */
//==========================================================================================================================================
bool oC_Thread_SetFinishedFunction( oC_Thread_t Thread , oC_Thread_FinishedFunction_t Function, void * Parameter )
{
    bool success = false;

    if(
        oC_SaveIfFalse("Thread object", oC_Thread_IsCorrect(Thread), oC_ErrorCode_ObjectNotCorrect  )
     && oC_SaveIfFalse("Function"     , isaddresscorrect(Function) , oC_ErrorCode_WrongAddress      )
        )
    {
        Thread->FinishedFunction    = Function;
        Thread->FinishedParameter   = Parameter;
        success                     = true;
    }

    return success;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the thread is correct
 * 
 * The function checks if the thread is correct. The thread is correct if it is allocated in the dynamic memory and the object control is correct.
 * 
 * @param Thread                Thread to check
 * 
 * @return true if the thread is correct
 * 
 * @see oC_Thread_New
 * 
 * @code{.c}
  
    oC_Thread_t thread = oC_Thread_New( 0 , 0 , NULL , "Thread" , ThreadFunction , NULL );
    if(oC_Thread_IsCorrect(thread))
    {
        printf("Thread is correct\n");
    }
    else
    {
        printf("Thread is not correct\n");
    }   

   @endcode
 */
//==========================================================================================================================================
static void ThreadExitHandler( void )
{
    oC_Thread_t          thread   = oC_ThreadMan_GetCurrentThread();
    oC_List(oC_Thread_t) threads  = oC_ThreadMan_GetList();
    bool                 contains = oC_List_Contains(threads,thread);

    if(thread->FinishedFunction != NULL)
    {
        if(oC_SaveIfFalse("Finished Function", isaddresscorrect(thread->FinishedFunction), oC_ErrorCode_WrongAddress))
        {
            FinishedFunction_t function = thread->FinishedFunction;

            thread->FinishedFunction = NULL;

            function(thread, thread->FinishedParameter);
        }
    }

    if(oC_Thread_IsCorrect(thread) && contains)
    {
        oC_Thread_Delete(&thread);
    }
    while(1);
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when equal)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenEqual( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    return !((Value & UnblockMask) == (Expected & UnblockMask));
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when not equal)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenNotEqual( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    return !((Value & UnblockMask) != (Expected & UnblockMask));
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when smaller)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenSmaller( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    return !((Value & UnblockMask) < (Expected & UnblockMask));
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when greater)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenGreater( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    return !((Value & UnblockMask) >= (Expected & UnblockMask));
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when smaller or equal)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenSmallerOrEqual( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    return !((Value & UnblockMask) <= (Expected & UnblockMask));
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when greater or equal)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenGreaterOrEqual( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    return !((Value & UnblockMask ) >= (Expected & UnblockMask));
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when at least one bit is set)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenAtLeastOneBitIsSet( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    bool blocked = (Value & UnblockMask) == 0;

    return blocked;
}

//==========================================================================================================================================
/**
 * @brief checks if the thread is unblocked (when at least one bit is cleared)
 */
//==========================================================================================================================================
static bool CheckUnblockedWhenAtLeastOneBitIsCleared( uint32_t Value , uint32_t Expected , oC_Thread_UnblockMask_t UnblockMask )
{
    bool blocked = (Value & UnblockMask) == UnblockMask;

    return blocked;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

