/** ****************************************************************************************************************************************
 *
 * @file       oc_streamman.c
 *
 * @brief      The file with source for stream manager functions
 *
 * @author     Patryk Kubiak - (Created on: 7 09 2015 18:49:28)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_streamman.h>
#include <string.h>
#include <oc_streams_cfg.c>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool                 ModuleEnabledFlag = false;
static oC_List(oC_Stream_t) Streams           = NULL;
static const oC_Allocator_t Allocator  = {
                .Name           = "streams manager" ,
                .EventFlags     = 0 ,
                .EventHandler   = NULL,
                .CorePwd        = oC_MemMan_CORE_PWD
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StreamMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn))
    {
        Streams = oC_List_New(&Allocator,AllocationFlags_NoWait);

        if(Streams)
        {
            ModuleEnabledFlag = true;
            errorCode = oC_ErrorCode_None;

#define IN  oC_Stream_Type_Input
#define OUT oC_Stream_Type_Output
#define ADD_STREAM( NAME , TYPE , DRIVER_NAME , CONFIG_NAME ) oC_AssignErrorCode(&errorCode , oC_StreamMan_Add(oC_Stream_New(&Allocator,AllocationFlags_NoWait,TYPE,#NAME,&DRIVER_NAME,&CONFIG_NAME)));
#define DONT_ADD_STREAM( NAME,...)        (void)&NAME;
            CFG_STREAMS_LIST(ADD_STREAM, DONT_ADD_STREAM)
#undef IN
#undef OUT
#undef ADD_STREAM
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StreamMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        errorCode = oC_ErrorCode_None;

        oC_List_Foreach(Streams,stream)
        {
            if(!oC_Stream_Delete(&stream))
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }

        if(!oC_List_Delete(Streams,AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StreamMan_Add( oC_Stream_t Stream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true   , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Stream_IsCorrect(Stream) , oC_ErrorCode_ObjectNotCorrect)
        )
    {
        bool added = oC_List_PushCopyBack(Streams,Stream,&Allocator);

        if(added)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotAddObjectToList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StreamMan_Remove( oC_Stream_t Stream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true   , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Stream_IsCorrect(Stream) , oC_ErrorCode_ObjectNotCorrect)
        )
    {
        bool contains = oC_List_Contains(Streams,Stream);

        if(contains)
        {
            bool removed = oC_List_RemoveAll(Streams,Stream);

            if(removed)
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_CannotRemoveObjectFromList;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_ObjectNotFoundOnList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_Stream_t) oC_StreamMan_GetList( void )
{
    oC_List(oC_Stream_t) list = NULL;

    if(ModuleEnabledFlag)
    {
        list = Streams;
    }

    return list;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Stream_t oC_StreamMan_GetStream( const char * Name )
{
    oC_Stream_t streamToReturn = NULL;

    if(ModuleEnabledFlag)
    {
        oC_List_Foreach(Streams,stream)
        {
            if(strcmp(oC_Stream_GetName(stream),Name)==0)
            {
                streamToReturn = stream;
            }
        }
    }

    return streamToReturn;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Stream_t oC_StreamMan_GetStdErrorStream( void )
{
    return oC_StreamMan_GetStream(CFG_STDERR_STREAM_NAME);
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


