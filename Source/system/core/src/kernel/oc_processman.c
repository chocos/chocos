/** ****************************************************************************************************************************************
 *
 * @file       oc_processman.c
 *
 * @brief      The file with source for process manager.
 *
 * @author     Patryk Kubiak - (Created on: 7 09 2015 19:27:42)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_processman.h>
#include <oc_threadman.h>
#include <string.h>
#include <oc_system_cfg.h>
#include <oc_userman.h>
#include <oc_debug.h>
#include <oc_intman.h>
#include <oc_module.h>

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION_________________________________________________________________________

static oC_UInt_t                    NextPid            = 0;
static oC_UInt_t                    SomeProcessToDelete= false;
static oC_Thread_t                  DeleteDeamonThread = NULL;
static oC_Process_t                 DeleteDeamonProcess= NULL;
static oC_List(oC_Process_t)        Processes          = NULL;
static const oC_Allocator_t         Allocator          = {
                .Name           = "Process Manager" ,
                .EventFlags     = 0 ,
                .EventHandler   = NULL,
                .CorePwd        = oC_MemMan_CORE_PWD
};
const oC_Module_Registration_t ProcessMan = {
                .Name                   = "ProcessMan" ,
                .LongName               = "Process Manager" ,
                .Module                 = oC_Module_ProcessMan ,
                .TurnOnFunction         = oC_ProcessMan_TurnOn ,
                .TurnOffFunction        = oC_ProcessMan_TurnOff ,
                .RequiredModules        = { oC_Module_ThreadMan }
};

#undef  _________________________________________VARIABLES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief function for turning on the module
 *
 * The function for turning on the module
 *
 * @return code of the error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ModuleIsTurnedOn     | Module is turned on already
 *  #oC_ErrorCode_AllocationError      | Cannot allocate memory (out of RAM?)
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ProcessMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_ProcessMan))
    {
        Processes = oC_List_New(&Allocator,AllocationFlags_NoWait);

        if(ErrorCondition(Processes != NULL, oC_ErrorCode_AllocationError))
        {
            SomeProcessToDelete = false;
            DeleteDeamonThread  = NULL;
            DeleteDeamonProcess = NULL;
            NextPid             = 0;
            errorCode           = oC_ErrorCode_None;
            oC_Module_TurnOn(oC_Module_ProcessMan);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function for turning off the module
 *
 * The function for turning off the module
 *
 * @return code of the error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ModuleNotStartedYet  | Module is not started yet
 *  #oC_ErrorCode_CannotDeleteProcess  | Cannot delete process
 *  #oC_ErrorCode_ReleaseError         | Error when releasing memory
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ProcessMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ProcessMan))
    {
        oC_Module_TurnOff(oC_Module_ProcessMan);
        errorCode = oC_ErrorCode_None;

        oC_List_Foreach(Processes,process)
        {
            if(oC_Process_Delete(&process) == false)
            {
                errorCode = oC_ErrorCode_CannotDeleteProcess;
            }
        }
        if(oC_List_Delete(Processes,AllocationFlags_CanWaitForever) == false)
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief function for adding process
 * 
 * The function allows for adding process to the system.
 * 
 * @param Process           process
 * 
 * @return code of the error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|--------------------------------------------------------------------------------------------------- 
 *  #oC_ErrorCode_ObjectNotCorrect     | Process object is not correct
 *  #oC_ErrorCode_ObjectAlreadyExist   | Process object already exists
 *  #oC_ErrorCode_CannotAddObjectToList| Cannot add object to the list
 *  #oC_ErrorCode_None                 | No error
 * 
 * @see oC_ProcessMan_RemoveProcess
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ProcessMan_AddProcess( oC_Process_t Process )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ProcessMan)
     && ErrorCondition( oC_Process_IsCorrect(Process)           , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( !oC_ProcessMan_ContainsProcess(Process) , oC_ErrorCode_ObjectAlreadyExist   )
        )
    {
        bool added = oC_List_PushBack(Processes,Process,&Allocator);
        if(ErrorCondition(added, oC_ErrorCode_CannotAddObjectToList))
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function for removing process
 * 
 * The function allows for removing process from the system.
 * 
 * @warning 
 * The process is not deleted nor killed. It is only removed from the list of processes.
 * 
 * @param Process           process
 * 
 * @return code of the error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                              | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ObjectNotCorrect          | Process object is not correct
 *  #oC_ErrorCode_ObjectNotFoundOnList      | Process object not found on the list
 *  #oC_ErrorCode_CannotRemoveObjectFromList| Cannot remove object from the list
 *  #oC_ErrorCode_None                      | No error
 * 
 * @see oC_ProcessMan_AddProcess
 * 
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ProcessMan_RemoveProcess( oC_Process_t Process )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    bool contains =  oC_ProcessMan_ContainsProcess(Process);
    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ProcessMan)
     && ErrorCondition( oC_Process_IsCorrect(Process) || contains   , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( contains                                    , oC_ErrorCode_ObjectNotFoundOnList )
        )
    {
        bool removed = oC_List_RemoveAll(Processes,Process);
        if( ErrorCondition(removed, oC_ErrorCode_CannotRemoveObjectFromList) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief function for checking if process is on the list
 * 
 * The function allows for checking if process is on the list of processes.
 * 
 * @param Process           process
 * 
 * @return true if process is on the list
 * 
 * @see oC_ProcessMan_AddProcess
 * @see oC_ProcessMan_RemoveProcess
 */
//==========================================================================================================================================
bool oC_ProcessMan_ContainsProcess( oC_Process_t Process )
{
    bool contains = false;

    if(
        oC_SaveIfFalse("ProcessMan", oC_Module_IsTurnedOn(oC_Module_ProcessMan) , oC_ErrorCode_ModuleNotStartedYet  )
     && oC_SaveIfFalse("Process"   , oC_Process_IsCorrect(Process)              , oC_ErrorCode_ObjectNotCorrect     )
        )
    {
        contains = oC_List_Contains(Processes,Process);
    }

    return contains;
}

//==========================================================================================================================================
/**
 * @brief function for getting list of processes
 * 
 * The function allows for getting list of processes.
 * 
 * @param List              list of processes
 * 
 * @return code of the error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ListNotCorrect       | List is not correct
 *  #oC_ErrorCode_CannotAddObjectToList| Cannot add object to the list (the list still can be filled with some objects)
 *  #oC_ErrorCode_None                 | No error
 * 
 * @note 
 * Even if the error code is not #oC_ErrorCode_None the list still can be filled with some objects.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ProcessMan_GetList( oC_List(oC_Process_t) List )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ProcessMan))
    {
        errorCode = oC_ErrorCode_None;

        oC_List_Clear(List);

        oC_List_Foreach(Processes,process)
        {
            bool added = oC_List_PushBack(List,process,&Allocator);

            if(added == false)
            {
                errorCode = oC_ErrorCode_CannotAddObjectToList;
            }
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief function for getting list of processes of the user
 * 
 * The function allows for getting list of processes of the user.
 * 
 * @param User              user
 * @param List              list of processes
 * 
 * @return code of the error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ListNotCorrect       | List is not correct
 *  #oC_ErrorCode_None                 | No error
 * 
 * @note 
 * Even if the error code is not #oC_ErrorCode_None the list still can be filled with some objects.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ProcessMan_GetUserProcesses( oC_User_t User , oC_List(oC_Process_t) List )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ProcessMan) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_List_IsCorrect(List) ,    oC_ErrorCode_ListNotCorrect)
        )
    {
        errorCode = oC_ErrorCode_None;

        oC_List_Clear(List);

        oC_List_Foreach(Processes,process)
        {
            if(oC_Process_GetUser(process) == User)
            {
                bool added = oC_List_PushBack(List,process,&Allocator);

                if(added == false)
                {
                    errorCode = oC_ErrorCode_CannotAddObjectToList;
                }
            }
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief function for getting current process
 * 
 * The function allows for getting current process.
 * 
 * @return current process
 */
//==========================================================================================================================================
oC_Process_t oC_ProcessMan_GetCurrentProcess( void )
{
    oC_Process_t currentProcess = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        oC_Thread_t currentThread = oC_ThreadMan_GetCurrentThread();

        oC_List_Foreach(Processes,process)
        {
            if(oC_Process_ContainsThread(process,currentThread))
            {
                currentProcess = process;
                break;
            }
        }
    }

    return currentProcess;
}

//==========================================================================================================================================
/**
 * @brief function for getting process by name
 * 
 * The function allows for getting process by name.
 * 
 * @param Name              name of the process
 * 
 * @return process
 */
//==========================================================================================================================================
oC_Process_t oC_ProcessMan_GetProcess( const char * Name )
{
    oC_Process_t processToReturn = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        oC_List_Foreach(Processes,process)
        {
            if(strcmp(oC_Process_GetName(process),Name) == 0)
            {
                processToReturn = process;
                break;
            }
        }
    }

    return processToReturn;
}

//==========================================================================================================================================
/**
 * @brief function for getting process by PID
 * 
 * The function allows for getting process by PID.
 * 
 * @param PID               PID of the process
 * 
 * @return process
 */
//==========================================================================================================================================
oC_Process_t oC_ProcessMan_GetProcessById( oC_UInt_t PID )
{
    oC_Process_t processToReturn = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        oC_List_Foreach(Processes,process)
        {
            if(oC_Process_GetPid(process) == PID)
            {
                processToReturn = process;
                break;
            }
        }
    }

    return processToReturn;
}

//==========================================================================================================================================
/**
 * @brief gets allocator of the current process
 * 
 * The function allows for getting allocator of the current process.
 * 
 * @return allocator of the current process
 * 
 * @see oC_Allocator_t
 */
//==========================================================================================================================================
Allocator_t oC_ProcessMan_GetCurrentAllocator( void )
{
    Allocator_t allocator = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        oC_IntMan_EnterCriticalSection();
        oC_Process_t currentProcess = oC_ProcessMan_GetCurrentProcess();
        oC_IntMan_ExitCriticalSection();

        if(currentProcess)
        {
            oC_IntMan_EnterCriticalSection();
            allocator = oC_Process_GetAllocator(currentProcess);
            oC_IntMan_ExitCriticalSection();
        }
    }

    return allocator;
}

//==========================================================================================================================================
/**
 * @brief gets process of the thread
 * 
 * The function allows for getting process of the thread.
 * 
 * @param Thread            thread
 * 
 * @return process of the thread
 * 
 * @note 
 * In case of error check the error code from #oC_ReadLastError
 */
//==========================================================================================================================================
oC_Process_t oC_ProcessMan_GetProcessOfThread( oC_Thread_t Thread )
{
    oC_Process_t process = NULL;

    if(oC_Thread_IsCorrect(Thread))
    {
        oC_List_Foreach(Processes,proc)
        {
            if(oC_Process_ContainsThread(proc,Thread))
            {
                process = proc;
                break;
            }
        }
    }
    else
    {
        oC_SaveError("Process manager" , oC_ErrorCode_ObjectNotCorrect);
    }

    return process;
}

//==========================================================================================================================================
/**
 * @brief allocates raw memory using current process' heap map
 * 
 * The function allows for allocating raw memory using current process' heap map.
 * 
 * @see oC_MemMan_RawAllocate, HeapMap_t
 * 
 * @param Size              size of the memory to allocate
 * @param Function          name of the function where the function is called
 * @param LineNumber        number of the line where the function is called
 * @param Flags             flags for allocation
 * 
 * @return pointer to the allocated memory
 * 
 * @note
 * Remember to free the memory using the #oC_ProcessMan_RawFree function
 */
//==========================================================================================================================================
void * oC_ProcessMan_RawAllocate( oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    void * address = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        oC_Process_t currentProcess = oC_ProcessMan_GetCurrentProcess();

        if(currentProcess)
        {
            oC_HeapMap_t heapMap = oC_Process_GetHeapMap(currentProcess);

            if(heapMap)
            {
                address = oC_MemMan_RawAllocate(heapMap,Size,Function,LineNumber,Flags);
            }
        }
    }

    return address;
}

//==========================================================================================================================================
/**
 * @brief frees raw memory using current process' heap map
 * 
 * The function allows for freeing raw memory using current process' heap map.
 * 
 * @see oC_MemMan_RawFree, HeapMap_t
 * 
 * @param Address           address of the memory to free
 * @param Size              size of the memory to free
 * 
 * @return true if memory was released
 * 
 * @see oC_ProcessMan_RawAllocate, oC_ProcessMan_Allocate, oC_ProcessMan_Free
 */
//==========================================================================================================================================
bool oC_ProcessMan_RawFree( void * Address , oC_UInt_t Size )
{
    bool released = false;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        oC_Process_t currentProcess = oC_ProcessMan_GetCurrentProcess();

        if(currentProcess)
        {
            oC_HeapMap_t heapMap = oC_Process_GetHeapMap(currentProcess);

            if(heapMap)
            {
                released = oC_MemMan_RawFree(heapMap,Address,Size);
            }
        }
    }

    return released;
}
//==========================================================================================================================================
/**
 * @brief allocates memory using current process' allocator
 * 
 * The function allows for allocating memory using current process' allocator.
 * 
 * @see oC_MemMan_Allocate, Allocator_t
 * 
 * @param Size              size of the memory to allocate
 * @param Function          name of the function where the function is called
 * @param LineNumber        number of the line where the function is called
 * @param Flags             flags for allocation
 * 
 * @return pointer to the allocated memory
 * 
 * @note
 * Remember to free the memory using the #oC_ProcessMan_Free function
 */
//==========================================================================================================================================
void * oC_ProcessMan_Allocate( oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    void * address = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        Allocator_t allocator = oC_ProcessMan_GetCurrentAllocator();

        if(allocator)
        {
            address = oC_MemMan_Allocate(Size,allocator,Function,LineNumber,Flags,0);
        }
    }

    return address;
}
//==========================================================================================================================================
/**
 * @brief frees memory using current process' allocator
 * 
 * The function allows for freeing memory using current process' allocator.
 * 
 * @see oC_MemMan_Free, Allocator_t
 * 
 * @param Address           address of the memory to free
 * @param Flags             flags for allocation
 * 
 * @return true if memory was released
 * 
 * @see oC_ProcessMan_RawFree, oC_ProcessMan_RawAllocate, oC_ProcessMan_Allocate
 */
//==========================================================================================================================================
bool oC_ProcessMan_Free( void * Address , AllocationFlags_t Flags )
{
    bool released = false;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        released = oC_MemMan_Free(Address,Flags);
    }

    return released;
}

//==========================================================================================================================================
/**
 * @brief smart allocation of memory
 * 
 * The function allows for smart allocation of memory. It checks which way of allocation is better for the given size and uses it.
 * 
 * For example, if the size is smaller than the block size of the allocator, the function uses the #oC_ProcessMan_RawAllocate - because it 
 * uses less memory for the allocation. If the size is bigger than the block size of the allocator, the function uses the #oC_ProcessMan_Allocate
 * 
 * @warning Memory allocated by this function should be freed by the #oC_ProcessMan_SmartFree function
 * 
 * @param Size              size of the memory to allocate
 * @param Function          name of the function where the function is called
 * @param LineNumber        number of the line where the function is called
 * @param Flags             flags for allocation
 * 
 * @return pointer to the allocated memory
 * 
 * @see oC_ProcessMan_SmartFree
 */
//==========================================================================================================================================
void * oC_ProcessMan_SmartAllocate( oC_UInt_t Size, const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    void * address = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        oC_UInt_t realRawAllocationSize = Size + (Size/8) + ((Size%8) > 0) ? 1 : 0;
        oC_UInt_t realAllocationSize    = Size + oC_MemMan_GetAllocationBlockSize();

        if(realAllocationSize >= realRawAllocationSize)
        {
            address = oC_ProcessMan_RawAllocate(Size,Function,LineNumber,Flags);
        }
        if(address == NULL)
        {
            address = oC_ProcessMan_Allocate(Size,Function,LineNumber,Flags);
        }
    }

    return address;
}

//==========================================================================================================================================
/**
 * @brief smart freeing of memory
 * 
 * The function allows for freeing of memory allocated by the #oC_ProcessMan_SmartAllocate function. 
 * 
 * @param Address           address of the memory to free
 * @param Size              size of the memory to free
 * @param Flags             flags for allocation
 * 
 * @return true if memory was released
 * 
 * @see oC_ProcessMan_SmartAllocate
 */
//==========================================================================================================================================
bool oC_ProcessMan_SmartFree( void * Address , oC_UInt_t Size , AllocationFlags_t Flags )
{
    bool released = false;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        released = oC_ProcessMan_RawFree(Address,Size);

        if(!released)
        {
            released = oC_ProcessMan_Free(Address,Flags);
        }
    }

    return released;
}

//==========================================================================================================================================
/**
 * @brief smart allocation of memory using kernel allocator
 * 
 * The function allows for smart allocation of memory using kernel allocator. It checks which way of allocation is better for the given size and uses it.
 * 
 * For example, if the size is smaller than the block size of the allocator, the function uses the #_kmalloc - because it uses less memory for the allocation. If the size is bigger than the block size of the allocator, the function uses the #oC_ProcessMan_KernelSmartAllocate
 * 
 * @warning Memory allocated by this function should be freed by the #oC_ProcessMan_KernelSmartFree function
 * 
 * @param Size              size of the memory to allocate
 * @param Allocator         allocator
 * @param Function          name of the function where the function is called
 * @param LineNumber        number of the line where the function is called
 * @param Flags             flags for allocation
 * 
 * @return pointer to the allocated memory
 * 
 * @see oC_ProcessMan_KernelSmartFree
 */
//==========================================================================================================================================
void * oC_ProcessMan_KernelSmartAllocate( oC_UInt_t Size , Allocator_t Allocator , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    void * address = oC_ProcessMan_SmartAllocate(Size,Function,LineNumber,Flags);

    if(address == NULL)
    {
        address = _kmalloc(Size,Allocator,Function,LineNumber,Flags,0);
    }

    return address;
}

//==========================================================================================================================================
/**
 * @brief smart freeing of memory using kernel allocator
 * 
 * The function allows for freeing of memory allocated by the #oC_ProcessMan_KernelSmartAllocate function. 
 * 
 * @param Address           address of the memory to free
 * @param Size              size of the memory to free
 * @param Flags             flags for allocation
 * 
 * @return true if memory was released
 * 
 * @see oC_ProcessMan_KernelSmartAllocate
 */
//==========================================================================================================================================
bool oC_ProcessMan_KernelSmartFree( void * Address , oC_UInt_t Size , AllocationFlags_t Flags )
{
    bool released = oC_ProcessMan_SmartFree(Address,Size,Flags);

    if(released == false)
    {
        released = kfree(Address,Flags);
    }

    return released;
}

//==========================================================================================================================================
/**
 * @brief deamon for deleting processes
 * 
 * The function is a deamon for deleting processes. It deletes processes which are killed.
 * 
 * @param Parameter         parameter (not used)
 */
//==========================================================================================================================================
void oC_ProcessMan_DeleteDeamon( void * Parameter )
{
    oC_Thread_t currentThread = oC_ThreadMan_GetCurrentThread();

    while(1)
    {
        oC_IntMan_EnterCriticalSection();
        SomeProcessToDelete = false;
        if(oC_Thread_SetBlocked(currentThread,&SomeProcessToDelete,oC_Thread_Unblock_WhenEqual,true,oC_Thread_UnblockMask_All,oC_hour(1))==false)
        {
            kdebuglog(oC_LogType_Error , "DeleteDeamon: cannot block current thread\n");
        }
        oC_IntMan_ExitCriticalSection();

        while(oC_Thread_IsBlocked(currentThread));

        oC_Thread_SetUnblocked(currentThread);

        oC_List_Foreach(Processes,process)
        {
            if(oC_Process_IsKilled(process))
            {
                oC_Process_t    copy      = process;
                oC_ErrorCode_t  errorCode = oC_ProcessMan_RemoveProcess(process);

                if(oC_ErrorOccur(errorCode))
                {
                    oC_SaveError("DeleteDeamon: Cannot delete process from the list: " , errorCode );
                }

                if(oC_Process_Delete(&process)==false)
                {
                    oC_SaveError(oC_Process_GetName(copy),oC_ErrorCode_CannotDeleteProcess);
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief runs delete deamon
 * 
 * The function runs delete deamon.
 * 
 * @return code of the error or #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                               | Description
 * ------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_DaemonThreadAlreadyExists  | Daemon thread already exists
 *  #oC_ErrorCode_DaemonProcessAlreadyExists | Daemon process already exists
 *  #oC_ErrorCode_CannotCreateProcess        | Cannot create process
 *  #oC_ErrorCode_CannotCreateThread         | Cannot create thread
 *  #oC_ErrorCode_CannotRunThread            | Cannot run thread
 *  #oC_ErrorCode_CannotDeleteProcess        | Cannot delete process
 *  #oC_ErrorCode_ReleaseError               | Error when releasing memory
 * 
 * @note 
 * More error codes can be returned by the #oC_ProcessMan_AddProcess function
 * 
 * @see oC_ProcessMan_ActivateDeleteDeamon
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ProcessMan_RunDeleteDaemon( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ProcessMan)      
     && ErrorCondition( oC_Thread_IsCorrect(DeleteDeamonThread)  == false , oC_ErrorCode_DaemonThreadAlreadyExists  ) 
     && ErrorCondition( oC_Process_IsCorrect(DeleteDeamonProcess)== false , oC_ErrorCode_DaemonProcessAlreadyExists )
        )
    {
        DeleteDeamonProcess = oC_Process_New(oC_Process_Priority_SystemHandlerDeamon,"delete daemon",oC_UserMan_GetRootUser(),0,NULL,NULL,NULL);

        if( ErrorCondition(oC_Process_IsCorrect(DeleteDeamonProcess), oC_ErrorCode_CannotCreateProcess) ) 
        {
            DeleteDeamonThread = oC_Thread_New(oC_Process_Priority_SystemHandlerDeamon,CFG_BYTES_DELETE_DEAMON_STACK_SIZE,DeleteDeamonProcess,"delete daemon",oC_ProcessMan_DeleteDeamon,NULL);

            if( ErrorCondition( oC_Thread_IsCorrect(DeleteDeamonThread), oC_ErrorCode_CannotCreateThread) )
            {
                if( ErrorCondition(oC_Thread_Run(DeleteDeamonThread), oC_ErrorCode_CannotRunThread) )
                {
                    errorCode = oC_ProcessMan_AddProcess(DeleteDeamonProcess);
                }
                if( oC_ErrorOccur(errorCode) )
                {
                    if(oC_Thread_Delete(&DeleteDeamonThread)==false)
                    {
                        oC_SaveError("Cannot delete daemon thread: ",oC_ErrorCode_ReleaseError);
                    }
                }
            }
            if(oC_ErrorOccur(errorCode))
            {
                if(oC_Process_Delete(&DeleteDeamonProcess)==false)
                {
                    oC_SaveError("Cannot delete daemon process: ",oC_ErrorCode_ReleaseError);
                }
            }
        }
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief activates delete deamon
 * 
 * The function activates delete deamon. The deamon will be activated to delete processes which are killed.
 * 
 * @see oC_ProcessMan_RunDeleteDaemon
 */
//==========================================================================================================================================
void oC_ProcessMan_ActivateDeleteDeamon( void )
{
    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan) && oC_Process_IsCorrect(DeleteDeamonProcess))
    {
        SomeProcessToDelete = true;
    }
}

//==========================================================================================================================================
/**
 * @brief gets next PID
 * 
 * The function gets next free PID (process ID).
 * 
 * @return next PID
 */
//==========================================================================================================================================
oC_UInt_t oC_ProcessMan_GetNextPid( void )
{
    oC_UInt_t pid = 0;

    if(oC_Module_IsTurnedOn(oC_Module_ProcessMan))
    {
        do
        {
            pid = ++NextPid;

            oC_List_Foreach(Processes,process)
            {
                if(oC_Process_GetPid(process)==pid)
                {
                    pid = 0;
                    break;
                }
            }
        } while(pid == 0);
    }

    return pid;
}

//==========================================================================================================================================
/**
 * @brief kills all zombie processes
 * 
 * The function kills all processes that are in zombie state.
 */
//==========================================================================================================================================
void oC_ProcessMan_KillAllZombies( void )
{
    oC_List_Foreach(Processes,process)
    {
        if(oC_Process_GetState(process) == oC_Process_State_Zombie)
        {
            oC_Process_Kill(process);
        }
    }
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________



