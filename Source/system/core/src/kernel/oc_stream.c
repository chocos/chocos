/** ****************************************************************************************************************************************
 *
 * @file       oc_stream.c
 *
 * @brief      __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 6 wrz 2015 17:22:27) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stream.h>
#include <oc_object.h>
#include <oc_streamman.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Stream_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_Stream_Type_t        Type;
    oC_Driver_t             Driver;
    const void *            Config;
    void *                  DriverContext;
    const char *            Name;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with if functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_Stream_t oC_Stream_New( Allocator_t Allocator , AllocationFlags_t Flags , oC_Stream_Type_t Type , const char * Name , oC_Driver_t Driver , const void * Config )
{
    oC_Stream_t stream = NULL;

    if(
        ((Type & oC_Stream_Type_Input) || (Type & oC_Stream_Type_Output)) &&
        isaddresscorrect(Config) &&
        isaddresscorrect(Name)
        )
    {
        oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
        void *         context   = NULL;

        errorCode = oC_Driver_Configure(Driver,Config,&context);

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError(Driver->FileName , errorCode);
        }
        else
        {
            stream    = kmalloc(sizeof(struct Stream_t) , Allocator , Flags);

            if(stream)
            {
                stream->ObjectControl = oC_CountObjectControl(stream,oC_ObjectId_Stream);
                stream->Type          = Type;
                stream->Driver        = Driver;
                stream->Config        = Config;
                stream->DriverContext = context;
                stream->Name          = Name;
            }
            else
            {
                oC_SaveError(Driver->FileName,oC_ErrorCode_AllocationError);
            }
        }
    }

    return stream;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Stream_Delete( oC_Stream_t * Stream )
{
    bool deleted = false;

    if(oC_Stream_IsCorrect(*Stream))
    {
        (*Stream)->ObjectControl = 0;

        deleted = kfree(*Stream,AllocationFlags_CanWaitForever);

        if(deleted)
        {
            *Stream = NULL;
        }
        else
        {
            (*Stream)->ObjectControl = oC_CountObjectControl(*Stream,oC_ObjectId_Stream);
        }
    }

    return deleted;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Stream_IsCorrect( oC_Stream_t Stream )
{
    return isram(Stream) && oC_CheckObjectControl(Stream,oC_ObjectId_Stream,Stream->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Stream_IsType( oC_Stream_t Stream , oC_Stream_Type_t Type )
{
    return oC_Stream_IsCorrect(Stream) && ((Stream->Type & Type) == Type);
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Stream_Write( oC_Stream_t Stream , const char * Buffer , oC_MemorySize_t * Size , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Stream_IsCorrect(Stream) == false && (IoFlags & oC_IoFlags_WriteToStdError))
    {
        Stream = oC_StreamMan_GetStdErrorStream();
    }

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Stream_IsCorrect(Stream) ,            oC_ErrorCode_ObjectNotCorrect       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(Size)  ,                           oC_ErrorCode_OutputAddressNotInRAM  ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , *Size > 0  ,                             oC_ErrorCode_SizeNotCorrect         ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Stream->Type & oC_Stream_Type_Output ,   oC_ErrorCode_StreamTypeNotCorrect   )
        )
    {
        oC_Time_t timeout               = oC_IoFlags_GetTimeout(IoFlags);
        oC_MemorySize_t  bytesToSend    = *Size;
        oC_MemorySize_t  sentBytes      = 0;

        while(sentBytes < (*Size))
        {
            errorCode   = oC_Driver_Write(Stream->Driver,Stream->DriverContext,&Buffer[sentBytes],&bytesToSend,timeout);

            sentBytes  += bytesToSend;
            bytesToSend = (*Size) - sentBytes;

            if(oC_ErrorOccur(errorCode)                         &&
               errorCode != oC_ErrorCode_Timeout                &&
               errorCode != oC_ErrorCode_NoAllBytesRead         &&
               errorCode != oC_ErrorCode_NotAllSent
               )
            {
                break;
            }
            else
            {
                errorCode = oC_ErrorCode_None;
            }

            if(oC_Bits_AreBitsSetU32(IoFlags,oC_IoFlags_WaitForAllElements))
            {
                continue;
            }

            if(sentBytes == 0 && oC_Bits_IsAtLeastOneBitSetU32(IoFlags,oC_IoFlags_WaitForSomeElements | oC_IoFlags_WaitForAllElements))
            {
                continue;
            }
            else if(oC_Bits_AreBitsSetU32(IoFlags,oC_IoFlags_WaitForAllElements))
            {
                continue;
            }
            else if(oC_Bits_AreBitsSetU32(IoFlags,oC_IoFlags_NoTimeout))
            {
                continue;
            }
        }

        *Size = sentBytes;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Stream_Read( oC_Stream_t Stream , char * outBuffer , oC_MemorySize_t * Size , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Stream_IsCorrect(Stream) ,            oC_ErrorCode_ObjectNotCorrect       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Stream->Type & oC_Stream_Type_Input ,    oC_ErrorCode_StreamTypeNotCorrect   ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(Size)  ,                           oC_ErrorCode_OutputAddressNotInRAM  ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , *Size > 0  ,                             oC_ErrorCode_SizeNotCorrect         )
        )
    {
        oC_Time_t        timeout       = oC_IoFlags_GetTimeout(IoFlags);
        oC_MemorySize_t  bytesToRead   = *Size;
        oC_MemorySize_t  readBytes     = 0;

        while(readBytes < (*Size))
        {
            errorCode   = oC_Driver_Read(Stream->Driver,Stream->DriverContext,&outBuffer[readBytes],&bytesToRead,timeout);

            readBytes   += bytesToRead;
            bytesToRead = (*Size) - readBytes;

            if(oC_ErrorOccur(errorCode)                         &&
               errorCode != oC_ErrorCode_Timeout                &&
               errorCode != oC_ErrorCode_NoAllBytesRead         &&
               errorCode != oC_ErrorCode_NoneElementReceived
               )
            {
                break;
            }
            else if(errorCode != oC_ErrorCode_Timeout)
            {
                errorCode = oC_ErrorCode_None;
            }

            if(readBytes == 0 && oC_Bits_IsAtLeastOneBitSetU32(IoFlags,oC_IoFlags_WaitForSomeElements | oC_IoFlags_WaitForAllElements | oC_IoFlags_NoTimeout))
            {
                continue;
            }
            else if(oC_Bits_AreBitsSetU32(IoFlags,oC_IoFlags_WaitForAllElements))
            {
                continue;
            }
            else
            {
                break;
            }
        }

        *Size = readBytes;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Stream_ClearReadBuffer( oC_Stream_t Stream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Stream_IsCorrect(Stream)         , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( Stream->Type & oC_Stream_Type_Input , oC_ErrorCode_StreamTypeNotCorrect     )
     && ErrorCondition( isaddresscorrect(Stream->Driver)    , oC_ErrorCode_WrongAddress             )
        )
    {
        errorCode = oC_Driver_HandleIoctl(Stream->Driver, Stream->DriverContext, oC_IoCtl_SpecialCommand_ClearRxFifo, NULL);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_Stream_GetName( oC_Stream_t Stream )
{
    const char * name = "unknown";

    if(oC_Stream_IsCorrect(Stream))
    {
        name = Stream->Name;
    }

    return name;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Driver_t oC_Stream_GetDriver( oC_Stream_t Stream )
{
    oC_Driver_t driver = NULL;

    if(oC_Stream_IsCorrect(Stream))
    {
        driver = Stream->Driver;
    }

    return driver;
}

//==========================================================================================================================================
//==========================================================================================================================================
void* oC_Stream_GetDriverContext( oC_Stream_t Stream )
{
    oC_Driver_Context_t context = NULL;

    if(oC_Stream_IsCorrect(Stream))
    {
        context = Stream->DriverContext;
    }

    return context;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
