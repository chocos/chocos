/** ****************************************************************************************************************************************
 *
 * @file       oc_semaphore.c
 *
 * @brief      The file with source code for semaphores
 *
 * @author     Patryk Kubiak - (Created on: 1 09 2015 19:01:05)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_null.h>
#include <oc_object.h>
#include <oc_semaphore.h>
#include <oc_threadman.h>
#include <oc_stdlib.h>
#include <oc_intman.h>
#include <oc_math.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Semaphore_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_Semaphore_Type_t     Type;
    uint32_t                Value;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup Semaphore
//! @{

//==========================================================================================================================================
/**
 * @brief Creates the semaphore
 * 
 * The function creates the semaphore with the given type and initial value. The semaphore can be binary or counting. 
 * 
 * @param Type                  The type of semaphore (binary or counting), can be also set to any value for counting semaphore
 * @param InitialValue          The initial value of semaphore 
 * @param Allocator             The allocator for semaphore (use getcurallocator() for current allocator)
 * @param Flags                 The flags for the allocation of the semaphore (use AllocationFlags_Default for default flags)
 * 
 * @return                      The created semaphore or NULL if the semaphore cannot be created
 * 
 * @see oC_Semaphore_Delete
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 , getcurallocator() , AllocationFlags_Default );
    if(semaphore)
    {
        // The semaphore is created
    }
    else
    {
        // The semaphore is not created
    }
    
    @endcode
 */
//==========================================================================================================================================
oC_Semaphore_t oC_Semaphore_New( oC_Semaphore_Type_t Type , oC_Semaphore_InitialValue_t InitialValue , Allocator_t Allocator , AllocationFlags_t Flags )
{
    oC_Semaphore_t semaphore = ksmartalloc( sizeof(struct Semaphore_t) , Allocator , Flags );

    if(semaphore)
    {
        semaphore->Type          = Type;
        semaphore->Value         = InitialValue;
        semaphore->ObjectControl = oC_CountObjectControl(semaphore,oC_ObjectId_Semaphore);
    }

    return semaphore;
}

//==========================================================================================================================================
/**
 * @brief Deletes the semaphore
 * 
 * The function deletes the semaphore and frees the memory
 * 
 * @param Semaphore             The pointer to the semaphore to delete (the pointer is set to NULL after the deletion)
 * @param Flags                 The flags for the deallocation of the semaphore (use AllocationFlags_Default for default flags)
 * 
 * @return                      true if the semaphore is deleted
 * 
 * @see oC_Semaphore_New
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 , getcurallocator() , AllocationFlags_Default );
    if(semaphore)
    {
        // The semaphore is created
        if(oC_Semaphore_Delete(&semaphore, AllocationFlags_Default))
        {
            // The semaphore is deleted
        }
        else
        {
            // The semaphore is not deleted
        }
    }
    else
    {
        // The semaphore is not created
    }
   @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_Delete( oC_Semaphore_t * Semaphore , AllocationFlags_t Flags  )
{
    bool deleted = false;

    if(oC_Semaphore_IsCorrect(*Semaphore))
    {
        oC_IntMan_EnterCriticalSection();
        (*Semaphore)->ObjectControl = 0;
        oC_ThreadMan_UnblockAllBlockedBy(&(*Semaphore)->Value,false);

        if(!ksmartfree(*Semaphore,sizeof(struct Semaphore_t),Flags))
        {
            oC_SaveError("Semaphore" , oC_ErrorCode_ReleaseError);
        }
        else
        {
            *Semaphore  = NULL;
            deleted     = true;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief checks if the semaphore is correct
 * 
 * The function checks if the semaphore is correct
 * 
 * @param Semaphore             The pointer to the semaphore
 * 
 * @return                      true if the semaphore is correct
 * 
 * @see oC_Semaphore_New
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 , getcurallocator() , AllocationFlags_Default );
    if(oC_Semaphore_IsCorrect(semaphore))
    {
        // The semaphore is correct
    }
    else
    {
        // The semaphore is not correct
    }
   @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_IsCorrect( oC_Semaphore_t Semaphore )
{
    return oC_MemMan_IsRamAddress(Semaphore) && oC_CheckObjectControl(Semaphore,oC_ObjectId_Semaphore , Semaphore->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief Gives the semaphore
 * 
 * The function gives the semaphore. For binary semaphore the value is set to 1. For counting semaphore the value is incremented by 1. 
 * 
 * @param Semaphore             The pointer to the semaphore
 * 
 * @return                      true if the semaphore is given
 * 
 * @see oC_Semaphore_Take
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 , getcurallocator() , AllocationFlags_Default );
    if(semaphore)
    {
        // The semaphore is created
        if(oC_Semaphore_Give(semaphore))
        {
            // The semaphore is given
        }
        else
        {
            // The semaphore is not given
        }
    }
    else
    {
        // The semaphore is not created
    }
   @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_Give( oC_Semaphore_t Semaphore )
{
    bool given = false;

    if(oC_Semaphore_IsCorrect(Semaphore))
    {
        oC_IntMan_EnterCriticalSection();

        if(Semaphore->Value < Semaphore->Type)
        {
            Semaphore->Value++;
        }
        oC_IntMan_ExitCriticalSection();
        given = true;
    }

    return given;
}

//==========================================================================================================================================
/**
 * @brief Gives the semaphore
 * 
 * The function gives the semaphore with the given count. For binary semaphore the count should be set to 1. 
 * For counting semaphore the value is incremented by the given count.
 * 
 * @param Semaphore             The pointer to the semaphore
 * @param Count                 The count of semaphore to give
 * 
 * @return                      true if the semaphore is given
 * 
 * @see oC_Semaphore_Take
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( 10 , 0 , getcurallocator() , AllocationFlags_Default ); // Create counting semaphore with 10 set as maximal value
    if(semaphore)
    {
        // The semaphore is created
        if(oC_Semaphore_GiveCounting(semaphore, 5))
        {
            // The semaphore is given with count 5
        }
        else
        {
            // The semaphore is not given
        }
    }
    else
    {
        // The semaphore is not created
    }

    @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_GiveCounting( oC_Semaphore_t Semaphore , uint32_t Count )
{
    bool given = false;

    if(oC_Semaphore_IsCorrect(Semaphore))
    {
        oC_IntMan_EnterCriticalSection();
        Semaphore->Value += Count;

        if(Semaphore->Value > Semaphore->Type)
        {
            Semaphore->Value = Semaphore->Type;
        }

        oC_IntMan_ExitCriticalSection();
        given = true;
    }

    return given;
}

//==========================================================================================================================================
/**
 * @brief Takes the semaphore
 * 
 * The function takes the semaphore. For binary semaphore the value is set to 0. For counting semaphore the value is decremented by 1. 
 * 
 * @param Semaphore             The pointer to the semaphore
 * @param Timeout               The timeout for the operation
 * 
 * @return                      true if the semaphore is taken
 * 
 * @see oC_Semaphore_Give
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary , 0 , getcurallocator() , AllocationFlags_Default );
    if(semaphore)
    {
        // The semaphore is created
        if(oC_Semaphore_Take(semaphore, oC_s(1)))
        {
            // The semaphore is taken
        }
        else
        {
            // The semaphore is not taken
        }
    }
    else
    {
        // The semaphore is not created
    }
   @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_Take( oC_Semaphore_t Semaphore , oC_Time_t Timeout )
{
    bool taken = false;

    if(oC_Semaphore_IsCorrect(Semaphore))
    {
        if(Semaphore->Value == 0)
        {
            oC_Thread_t thread = oC_ThreadMan_GetCurrentThread();

            if(oC_Thread_SetBlocked(thread,&Semaphore->Value,oC_Thread_Unblock_WhenGreaterOrEqual,1,oC_Thread_UnblockMask_All,Timeout))
            {
                while(oC_Thread_IsBlocked(thread));

                oC_Thread_SetUnblocked(thread);

                oC_IntMan_EnterCriticalSection();
                if(oC_Semaphore_IsCorrect(Semaphore) && (Semaphore->Value >= 1))
                {
                    Semaphore->Value        = 0;
                    taken                   = true;
                }
                oC_IntMan_ExitCriticalSection();
            }
            else
            {
                if ( Semaphore->Value != 0 )
                {
                    Semaphore->Value = 0;
                    taken = true;
                }
            }
        }
        else
        {
            oC_IntMan_EnterCriticalSection();
            Semaphore->Value        = 0;
            taken                   = true;
            oC_IntMan_ExitCriticalSection();
        }
    }

    return taken;
}

//==========================================================================================================================================
/**
 * @brief Takes the semaphore
 * 
 * The function takes the semaphore with the given count. For binary semaphore the count should be set to 1. 
 * For counting semaphore the value is decremented by the given count.
 * 
 * @param Semaphore             The pointer to the semaphore
 * @param Count                 The count of semaphore to take
 * @param Timeout               The timeout for the operation
 * 
 * @return                      true if the semaphore is taken
 * 
 * @see oC_Semaphore_Give
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( 10 , 0 , getcurallocator() , AllocationFlags_Default ); // Create counting semaphore with 10 set as maximal value
    if(semaphore)
    {
        // The semaphore is created
        if(oC_Semaphore_TakeCounting(semaphore, 5, oC_s(1)))
        {
            // The semaphore is taken with count 5
        }
        else
        {
            // The semaphore is not taken
        }
    }
    else
    {
        // The semaphore is not created
    }

    @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_TakeCounting( oC_Semaphore_t Semaphore , uint32_t Count , oC_Time_t Timeout )
{
    bool taken = false;

    if(oC_Semaphore_IsCorrect(Semaphore) && Count <= Semaphore->Type)
    {
        oC_IntMan_EnterCriticalSection();
        if(Semaphore->Value < Count)
        {
            oC_IntMan_ExitCriticalSection();
            oC_Thread_t thread = oC_ThreadMan_GetCurrentThread();
            oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
            while(oC_Thread_SetBlocked(thread,&Semaphore->Value, oC_Thread_Unblock_WhenGreaterOrEqual , Count , oC_Thread_UnblockMask_All , gettimeout(endTimestamp)))
            {
                while(oC_Thread_IsBlocked(thread));
                oC_IntMan_EnterCriticalSection();

                oC_Thread_SetUnblocked(thread);

                if(oC_Semaphore_IsCorrect(Semaphore) && (Semaphore->Value >= Count))
                {
                    Semaphore->Value       -= Count;
                    taken                   = true;
                    oC_IntMan_ExitCriticalSection();
                    break;
                }
                else
                {
                    oC_IntMan_ExitCriticalSection();
                }
            }
        }
        else
        {
            Semaphore->Value       -= Count;
            taken                   = true;
            oC_IntMan_ExitCriticalSection();
        }
    }

    return taken;
}

//==========================================================================================================================================
/**
 * @brief Takes the semaphore
 * 
 * The function is designed to take the semaphore with the given count. For binary semaphore the count should be set to 1. 
 * 
 * For the counting semaphores it will try to take the semaphore with the given count. If the semaphore has less than the given count, but 
 * more than the minimal count, it will take the semaphore with the available count.
 * 
 * The number of taken semaphore is returned in the #Count variable.
 * 
 * @param Semaphore             The pointer to the semaphore
 * @param Count                 The count of semaphore to take          (the maximal count to take)
 * @param Min                   The minimal count of semaphore to take  
 * @param Timeout               The timeout for the operation
 * 
 * @return                      true if the semaphore is taken
 * 
 * @see oC_Semaphore_Give, oC_Semaphore_TakeCounting
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( 10 , 0 , getcurallocator() , AllocationFlags_Default ); // Create counting semaphore with 10 set as maximal value
    if(semaphore)
    {
        // The semaphore is created
        uint32_t count = 5; // maximal count to take
        if(oC_Semaphore_TakeCountingSoft(semaphore, &count, 1, oC_s(1)))
        {
            printf("The semaphore is taken with count %d\n", count);
        }
        else
        {
            // The semaphore is not taken (minimal count is not available in the given time)
        }
    }
    else
    {
        // The semaphore is not created
    }

    @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_TakeCountingSoft( oC_Semaphore_t Semaphore , uint32_t* Count, uint32_t Min, oC_Time_t Timeout )
{
    bool taken = false;

    if(oC_Semaphore_IsCorrect(Semaphore) && isram(Count) &&  (*Count) <= Semaphore->Type)
    {
        oC_IntMan_EnterCriticalSection();
        if(Semaphore->Value >= *Count)
        {
            Semaphore->Value -= *Count;
            taken = true;
            oC_IntMan_ExitCriticalSection();
        }
        else if(Semaphore->Value >= Min)
        {
            uint32_t takenCount     = oC_MIN( oC_MAX(Min, Semaphore->Value), *Count );
            Semaphore->Value -= takenCount;
            taken = true;
            *Count = takenCount;
            oC_IntMan_ExitCriticalSection();
        }
        else
        {
            oC_IntMan_ExitCriticalSection();
            oC_Thread_t thread = oC_ThreadMan_GetCurrentThread();

            if(oC_Thread_SetBlocked(thread,&Semaphore->Value, oC_Thread_Unblock_WhenGreaterOrEqual , Min , oC_Thread_UnblockMask_All , Timeout))
            {
                oC_ASSERT( oC_IntMan_AreInterruptsTurnedOn() );
                while(oC_Thread_IsBlocked(thread));

                oC_Thread_SetUnblocked(thread);

                oC_IntMan_EnterCriticalSection();
                if(oC_Semaphore_IsCorrect(Semaphore) && (Semaphore->Value >= Min))
                {
                    uint32_t takenCount     = oC_MIN( oC_MAX(Min, Semaphore->Value), *Count );
                    Semaphore->Value       -= takenCount;
                    taken                   = true;
                    *Count                  = takenCount;
                }
                oC_IntMan_ExitCriticalSection();
            }
        }
    }

    return taken;
}

//==========================================================================================================================================
/**
 * @brief Forces the semaphore value
 * 
 * The function forces the semaphore value to the given value. It was created for the internal usage.
 * 
 * @warning                    The function should be used with caution. It can lead to the unexpected behavior of the system. 
 * 
 * @param Semaphore             The pointer to the semaphore
 * @param Value                 The value to set
 * 
 * @return                      true if the semaphore value is set
 * 
 * @see oC_Semaphore_Give, oC_Semaphore_Take
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( 10 , 0 , getcurallocator() , AllocationFlags_Default ); // Create counting semaphore with 10 set as maximal value
    if(semaphore)
    {
        // The semaphore is created
        if(oC_Semaphore_ForceValue(semaphore, 5))
        {
            // The semaphore value is set to 5
        }
        else
        {
            // The semaphore value is not set
        }
    }
    else
    {
        // The semaphore is not created
    }

    @endcode
*/
//==========================================================================================================================================
bool oC_Semaphore_ForceValue( oC_Semaphore_t Semaphore , uint32_t Value )
{
    bool success = false;

    oC_IntMan_EnterCriticalSection();

    if(oC_Semaphore_IsCorrect(Semaphore))
    {
        Semaphore->Value = Value;
        success = true;
    }

    oC_IntMan_ExitCriticalSection();

    return success;
}

//==========================================================================================================================================
/**
 * @brief Gets the semaphore value
 * 
 * The function gets the semaphore value. It was created for the internal usage.
 * 
 * @param Semaphore             The pointer to the semaphore
 * 
 * @return                      The value of the semaphore
 * 
 * @see oC_Semaphore_Give, oC_Semaphore_Take
 * 
 * @code{.c}
    
    oC_Semaphore_t semaphore = oC_Semaphore_New( 10 , 0 , getcurallocator() , AllocationFlags_Default ); // Create counting semaphore with 10 set as maximal value
    if(semaphore)
    {
        // The semaphore is created
        uint32_t value = oC_Semaphore_Value(semaphore);
        printf("The semaphore value is %d\n", value);
    }
    else
    {
        // The semaphore is not created
    }

    @endcode
*/
//==========================================================================================================================================
uint32_t oC_Semaphore_Value( oC_Semaphore_t Semaphore )
{
    uint32_t value = 0;

    oC_IntMan_EnterCriticalSection();

    if(oC_Semaphore_IsCorrect(Semaphore))
    {
        value = Semaphore->Value;
    }

    oC_IntMan_ExitCriticalSection();

    return value;
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_SECTION______________________________________________________________________________


#undef  _________________________________________LOCAL_SECTION______________________________________________________________________________


