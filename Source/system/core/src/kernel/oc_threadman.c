/** ****************************************************************************************************************************************
 *
 * @file       oc_threadman.c
 *
 * @brief      __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 6 wrz 2015 13:29:22) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_threadman.h>
#include <oc_sys_lld.h>
#include <oc_stdlib.h>
#include <oc_system_cfg.h>
#include <oc_ktime.h>
#include <string.h>
#include <oc_processman.h>
#include <oc_intman.h>
#include <oc_debug.h>
#include <oc_userman.h>
#include <oc_exchan.h>
#include <oc_module.h>
#include <oc_gpio.h>

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

static void MemoryEventHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber );
static void SysTickReturnToIdle( void );
static void CountCpuLoad( void );
static void SysTickHandler(void);

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static void *               SavedThreadsListAddress = NULL;
static oC_Thread_t          CurrentThread           = NULL;
static oC_Time_t            InIdleTime              = 0;
static oC_Time_t            CurrentInIdleTime       = 0;
static double               CpuLoad                 = 0;
static double               CurrentCpuLoad          = 0;
static double               CpuLoadPanicMargin      = CFG_PERCENT_CPU_LOAD_PANIC_MARGIN;
static oC_Time_t            CpuLoadPanicTimeLimit   = CFG_TIME_CPU_LOAD_PANIC_TIME;
static oC_Time_t            CpuLoadPanicTimeout     = 0;
static oC_Time_t            CpuLoadMeasureTimeout   = oC_Frequency_ToTime(CFG_FREQUENCY_CPU_LOAD_MEASUREMENT_FREQUENCY);
static oC_Time_t            CpuLoadMeasureTime      = 0;
static oC_Thread_t          ThreadToRemove          = NULL;
static oC_Thread_t          NextThread              = NULL;
static oC_MemorySize_t      RedZoneSize             = CFG_BYTES_DEFAULT_RED_ZONE_SIZE;
static oC_Frequency_t       SystemFrequency         = CFG_FREQUENCY_DEFAULT_SYSTEM_TIMER_FREQUENCY;
static oC_AutoStackMethod_t AutoStackMethod         = CFG_BOOL_RERUN_THREAD_WHEN_STACK_IS_OVER ? oC_AutoStackMethod_RerunThread : oC_AutoStackMethod_Disabled;
static oC_MemorySize_t      StackIncreaseStep       = CFG_BYTES_STACK_INCREASE_STEP;
static oC_Time_t            LastSwitchTime          = 0;
       uint64_t             oC_ThreadManTickCounter = 0;
static oC_Time_t            ForceChangeThreadTimeout= 0;
static oC_List(oC_Thread_t) ThreadList              = NULL;
static oC_MemorySize_t      DefaultStackSize        = CFG_BYTES_DEFAULT_THREAD_STACK_SIZE;

static const oC_Allocator_t Allocator = {
                .Name           = "thread manager" ,
                .EventHandler   = MemoryEventHandler ,
                .EventFlags     = MemoryEventFlags_BufferOverflow       |
                                  MemoryEventFlags_DataSectionOverflow  |
                                  MemoryEventFlags_PossibleMemoryLeak   |
                                  MemoryEventFlags_ModuleTurningOff     |
                                  MemoryEventFlags_PanicMemoryExhausted,
                .CorePwd        = oC_MemMan_CORE_PWD
};
const oC_Module_Registration_t ThreadMan = {
                .Name                   = "ThreadMan" ,
                .LongName               = "Thread Manager" ,
                .Module                 = oC_Module_ThreadMan ,
                .TurnOnFunction         = oC_ThreadMan_TurnOn ,
                .TurnOffFunction        = oC_ThreadMan_TurnOff ,
                .RequiredModules        = { 0 }
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_ThreadMan))
    {
        errorCode = oC_SYS_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn || errorCode == oC_ErrorCode_None)
        {
            CurrentThread           = NULL;
            ThreadToRemove          = NULL;
            SystemFrequency         = CFG_FREQUENCY_DEFAULT_SYSTEM_TIMER_FREQUENCY;
            ThreadList              = oC_List_New(&Allocator,AllocationFlags_NoWait);
            LastSwitchTime          = 0;
            oC_ThreadManTickCounter = 0;
            ForceChangeThreadTimeout= 0;

            if(ThreadList)
            {
                SavedThreadsListAddress = ThreadList;

                if(oC_AssignErrorCode(&errorCode , oC_SYS_LLD_ConfigureSystemTimer(SystemFrequency , SysTickHandler)))
                {
                    oC_Module_TurnOn(oC_Module_ThreadMan);
                    errorCode = oC_ErrorCode_None;
                }
            }
            else
            {
                errorCode = oC_ErrorCode_AllocationError;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan))
    {
        oC_Module_TurnOff(oC_Module_ThreadMan);
        errorCode = oC_SYS_LLD_TurnOffDriver();

        oC_List_Delete(ThreadList,AllocationFlags_CanWaitForever);

        if(errorCode == oC_ErrorCode_ModuleNotStartedYet || errorCode == oC_ErrorCode_None)
        {
            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_SetSystemTimerFrequency( oC_Frequency_t Frequency )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan))
    {
        errorCode = oC_SYS_LLD_ConfigureSystemTimer(Frequency,SysTickHandler);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Frequency_t oC_ThreadMan_GetSystemTimerFrequency( void )
{
    return SystemFrequency;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_AddThread( oC_Thread_t Thread )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Thread_IsCorrect(Thread) , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        bool added = false;

        oC_IntMan_EnterCriticalSection();
        added = oC_List_PushBack(ThreadList,Thread,&Allocator);
        oC_IntMan_ExitCriticalSection();

        if(added)
        {
            oC_ThreadMan_SwitchThread();
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotAddObjectToList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_RemoveThread( oC_Thread_t Thread )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Thread_IsCorrect(Thread) , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        oC_IntMan_EnterCriticalSection();
        bool objectFound = oC_List_Contains(ThreadList,Thread);
        oC_Thread_t takenThread = NULL;
        oC_List_Take(ThreadList,Thread,takenThread);
        oC_IntMan_ExitCriticalSection();

        if(takenThread == Thread)
        {
            oC_IntMan_EnterCriticalSection();
            if(CurrentThread == Thread)
            {
                ThreadToRemove = Thread;
                errorCode      = oC_ErrorCode_None;
            }
            else
            {
                if(oC_Thread_Delete(&Thread))
                {
                    oC_ThreadMan_SwitchThread();
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    errorCode = oC_ErrorCode_CannotDeleteObject;
                }
            }
            oC_IntMan_ExitCriticalSection();
        }
        else if(objectFound == false)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotRemoveObjectFromList;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_ThreadMan_ContainsThread( oC_Thread_t Thread )
{
    bool contains = false;

    if(oC_SaveIfFalse( "Module is not enabled", oC_Module_IsTurnedOn(oC_Module_ThreadMan), oC_ErrorCode_ModuleNotStartedYet ))
    {
        contains = oC_List_Contains(ThreadList,Thread);
    }

    return contains;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Thread_t oC_ThreadMan_GetCurrentThread( void )
{
    return (oC_Module_IsTurnedOn(oC_Module_ThreadMan)) ? CurrentThread : NULL;
}

//==========================================================================================================================================
//==========================================================================================================================================
void oC_ThreadMan_UnblockAllBlockedBy( uint32_t * BlockingFlag , bool OnlyIfUnblocked )
{
    if(oC_Module_IsTurnedOn(oC_Module_ThreadMan))
    {
        oC_List_Foreach(ThreadList,thread)
        {
            if(oC_Thread_IsBlockedBy(thread,BlockingFlag))
            {
                oC_IntMan_EnterCriticalSection();
                if(!OnlyIfUnblocked || !oC_Thread_IsBlocked(thread))
                {
                    oC_Thread_SetUnblocked(thread);
                }
                oC_IntMan_ExitCriticalSection();
            }
        }

        oC_ThreadMan_SwitchThread();
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
void oC_ThreadMan_SwitchThread( void )
{
    static bool                 repairInProgress = false;
    oC_MemMan_AllocationStats_t allStat;
    char                        tempString[100]  = {0};
    bool                        foundOverflow    = false;

    if(oC_Module_IsTurnedOn(oC_Module_ThreadMan))
    {
        NextThread = NULL;

        oC_List_Foreach(ThreadList,thread)
        {
            if(oC_Thread_IsCorrect(thread) == false)
            {
                kdebuglog(oC_LogType_Error , "switch-thread: Thread [%p] '%s' is not correct!\n", thread , oC_Thread_GetName(thread));

                if(oC_List_Verify(ThreadList) == false )
                {
                    foundOverflow = oC_MemMan_FindOverflowedAllocation(NULL,&allStat);

                    if(foundOverflow)
                    {
                        sprintf(tempString, "Overflowed buffer: [%p] with size: %d Allocated at: %s:%d", allStat.Address, allStat.Size, allStat.Function,allStat.LineNumber);
                    }
                    else
                    {
                        sprintf(tempString, "(Damaged thread: %p - %s)", thread, oC_Thread_GetName(thread));
                    }

                    if(repairInProgress == false)
                    {
                        repairInProgress = true;

                        if(List_Repair((oC_List_t)ThreadList,SavedThreadsListAddress,CFG_UINT32_MAXIMUM_NUMBER_OF_THREADS))
                        {
                            oC_ThreadMan_SwitchThread();
                            repairInProgress = false;
                            oC_ExcHan_LogEvent("Restored stable state after fatal exception. Threads list was damaged. There is a possibility of memory leakage.", tempString, NULL, CurrentThread, oC_ExcHan_ExceptionType_KernelPanic);
                            break;
                        }
                        else
                        {
                            if(isram(oC_SYS_LLD_GetSystemContext()))
                            {
                                oC_ExcHan_LogEvent("Cannot restore stable state after fatal exception. Threads list is damaged.", tempString, NULL, CurrentThread, oC_ExcHan_ExceptionType_KernelPanic | oC_ExcHan_ExceptionType_RequiredReboot);
                                SysTickReturnToIdle();
                                break;
                            }
                            else
                            {
                                oC_Boot_Restart( oC_Boot_Reason_SystemException, oC_UserMan_GetRootUser() );
                            }
                        }
                    }
                    else
                    {
                        if(isram(oC_SYS_LLD_GetSystemContext()))
                        {
                            oC_ExcHan_LogEvent("Cannot restore stable state after fatal exception. Threads list is damaged.", tempString, NULL, CurrentThread, oC_ExcHan_ExceptionType_KernelPanic | oC_ExcHan_ExceptionType_RequiredReboot);
                            SysTickReturnToIdle();
                            break;
                        }
                        else
                        {
                            oC_Boot_Restart( oC_Boot_Reason_SystemException, oC_UserMan_GetRootUser() );
                        }
                    }
                }
                else
                {
                    oC_List_RemoveCurrentElement(ThreadList,thread);
                }
            }
            else if(oC_Thread_IsBlocked(thread) == false)
            {
                oC_Thread_Priority_t nextPriority = oC_Thread_GetPriority(NextThread);
                oC_Thread_Priority_t priority     = oC_Thread_GetPriority(thread);
                if(
                    (NextThread == NULL) ||
                    (priority >  nextPriority) ||
                    (priority == nextPriority && oC_Thread_GetLastExecutionTick(thread) < oC_Thread_GetLastExecutionTick(NextThread))
                                )
                {
                    NextThread = thread;
                }
            }
        }
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_Thread_t) oC_ThreadMan_GetList( void )
{
    return ThreadList;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Thread_t oC_ThreadMan_GetThread( const char * Name )
{
    oC_Thread_t threadToReturn = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ThreadMan))
    {
        oC_List_Foreach(ThreadList,thread)
        {
            if(strcmp(oC_Thread_GetName(thread),Name) == 0)
            {
                threadToReturn = thread;
            }
        }
    }

    return threadToReturn;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_ThreadMan_GetDefaultStackSize( void )
{
    return DefaultStackSize;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_SetDefaultStackSize( oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan)
     && ErrorCondition(Size > 0          , oC_ErrorCode_SizeNotCorrect      )
        )
    {
        DefaultStackSize = oC_MemMan_AlignSizeTo(Size,oC_MEM_LLD_STACK_ALIGNMENT);
        errorCode        = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_SetAutoStackMethod( oC_AutoStackMethod_t Method )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan)
     && ErrorCondition( Method >= 0 && Method <= oC_AutoStackMethod_RerunThread , oC_ErrorCode_AutoStackMethodNotCorrect   )
        )
    {
        AutoStackMethod  = Method;
        errorCode        = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_AutoStackMethod_t oC_ThreadMan_GetAutoStackMethod( void )
{
    return AutoStackMethod;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_SetRedZoneSize( oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan)
     && ErrorCondition(Size > 0          , oC_ErrorCode_SizeNotCorrect      )
        )
    {
        RedZoneSize      = oC_MemMan_AlignSize(Size);
        errorCode        = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_ThreadMan_GetRedZoneSize( void )
{
    return RedZoneSize;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ThreadMan_SetStackIncreaseStep( oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_ThreadMan)
     && ErrorCondition(Size > 0                 , oC_ErrorCode_SizeNotCorrect      )
        )
    {
        StackIncreaseStep = oC_MemMan_AlignSizeTo(Size,oC_MEM_LLD_STACK_ALIGNMENT);
        errorCode         = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_ThreadMan_GetStackIncreaseStep( void )
{
    return StackIncreaseStep;
}

//==========================================================================================================================================
//==========================================================================================================================================
double oC_ThreadMan_GetCpuLoad( void )
{
    return CpuLoad;
}

//==========================================================================================================================================
//==========================================================================================================================================
double oC_ThreadMan_GetCurrentCpuLoad( void )
{
    return CurrentCpuLoad;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Thread_t oC_ThreadMan_GetThreadOfContext( void * Context )
{
    oC_Thread_t foundThread = NULL;

    foreach(ThreadList, thread)
    {
        bool inRedZone = false;

        if(oC_Thread_IsOwnedByStack(thread,Context,&inRedZone) || inRedZone)
        {
            foundThread = thread;
        }
    }

    return foundThread;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * ONLY FOR SYSTICK!!!
 */
//==========================================================================================================================================
static void SysTickReturnToIdle( void )
{
    oC_SYS_LLD_ReturnToSystemContext();
    CurrentThread = NULL;
}

//==========================================================================================================================================
//==========================================================================================================================================
static void CountCpuLoad( void )
{
    oC_Timestamp_t currentTimestamp   = oC_KTime_GetTimestamp();
    oC_Time_t      timeFromLastSwitch = currentTimestamp - LastSwitchTime;
    CpuLoadMeasureTime += timeFromLastSwitch;

    if(oC_SYS_LLD_GetCurrentContext() == oC_SYS_LLD_GetSystemContext())
    {
        InIdleTime          += timeFromLastSwitch;
        CurrentInIdleTime   += timeFromLastSwitch;
    }
    CpuLoad = (100.0 - (InIdleTime / currentTimestamp) * 100);

    if(CpuLoadMeasureTime >= CpuLoadMeasureTimeout || (CurrentCpuLoad == 0 && CpuLoadMeasureTime > 0))
    {
        CurrentCpuLoad      = (100.0 - (CurrentInIdleTime / CpuLoadMeasureTime) * 100);
        CpuLoadMeasureTime  = 0;
        CurrentInIdleTime   = 0;
    }

    if(CurrentCpuLoad > CpuLoadPanicMargin)
    {
        if(CpuLoadPanicTimeout > 0)
        {
            if(CpuLoadPanicTimeout < currentTimestamp)
            {
                char string[60];

                sprintf_s(string,sizeof(string), "margin = %f, thread name = %s", CurrentCpuLoad, oC_Thread_GetName(CurrentThread));
                oC_ExcHan_LogEvent("PANIC CPU LOAD limit exhausted", string, NULL, NULL, oC_ExcHan_ExceptionType_ProcessDamaged);
                CpuLoadPanicTimeout = 0;
                ForceChangeThreadTimeout = currentTimestamp;
                oC_Thread_Delete(&CurrentThread);
            }
        }
        else
        {
            CpuLoadPanicTimeout = currentTimestamp + CpuLoadPanicTimeLimit;
        }
    }
    else
    {
        CpuLoadPanicTimeout = 0;
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void SysTickHandler(void)
{
    if(oC_Module_IsTurnedOn(oC_Module_ThreadMan))
    {
        oC_IntMan_EnterCriticalSection();
        oC_Time_t               currentTime    = oC_KTime_GetTimestamp();
        oC_SYS_LLD_Context_t *  currentContext = oC_SYS_LLD_GetCurrentContext();
        oC_SYS_LLD_Context_t *  threadContext  = oC_Thread_GetContext(CurrentThread);

        CountCpuLoad();

        if(CurrentThread)
        {
            if(currentContext != threadContext)
            {
                // TODO: Why this error happens so often?!
//                kdebuglog(oC_LogType_Error , "systick: current context is not current thread! %p != %p", currentContext, threadContext);
//                kdebuglog(oC_LogType_Error , "systick: Searching new thread...");
                SysTickReturnToIdle();
                oC_ThreadMan_SwitchThread();
            }
        }

        if(ThreadToRemove)
        {
            if(ThreadToRemove == CurrentThread)
            {
                CurrentThread = NULL;
            }
            oC_Thread_Delete(&ThreadToRemove);
            ThreadToRemove = NULL;
        }

        if(oC_Thread_IsBlocked(CurrentThread) || currentTime >= ForceChangeThreadTimeout)
        {

            if(CurrentThread)
            {
                oC_Thread_AddToExecutionTime(CurrentThread, currentTime - LastSwitchTime);
                oC_Thread_SetLastExecutionTick(CurrentThread,oC_ThreadManTickCounter);
            }

            LastSwitchTime = currentTime;

            if(NextThread == NULL || oC_Thread_IsBlocked(NextThread))
            {
                oC_ThreadMan_SwitchThread();
            }

            if(NextThread != CurrentThread)
            {
                if(NextThread == NULL && currentTime >= ForceChangeThreadTimeout && CurrentThread != NULL && oC_Thread_IsBlocked(CurrentThread) == false)
                {
                    oC_ErrorCode_t errorCode = oC_SYS_LLD_SetNextContext(oC_Thread_GetContext(CurrentThread));

                    if(errorCode != oC_ErrorCode_None)
                    {
                        oC_Int_t freeStackSize = oC_Thread_GetFreeStackSize(CurrentThread,true);
                        if(freeStackSize <= 0)
                        {
                            kdebuglog(oC_LogType_Error,"systick: Stack of the thread %s is too small! Closing..." , oC_Thread_GetName(CurrentThread));
                            kdebuglog(oC_LogType_Error,"%s: lack of %d stack bytes..." , oC_Thread_GetName(CurrentThread) , freeStackSize);

                            char string[60];

                            sprintf(string,"Stack of the thread %s is too small! Lack of %d bytes", oC_Thread_GetName(CurrentThread), freeStackSize);

                            oC_ExcHan_LogEvent("Stack of the thread is too small!\n", string, NULL, CurrentThread, oC_ExcHan_ExceptionType_ProcessDamaged);
                        }
                        oC_SaveError("ThreadMan : SysTickHandler - cannot set next context (after timeout): " , errorCode );
                        oC_ThreadMan_RemoveThread(CurrentThread);
                        SysTickReturnToIdle();
                    }
                }
                else if(NextThread != NULL)
                {
                    ForceChangeThreadTimeout = oC_Frequency_ToTime(CFG_FREQUENCY_DEFAULT_FORCE_CHANGE_THREAD) + currentTime;

                    oC_ErrorCode_t errorCode = oC_SYS_LLD_SetNextContext(oC_Thread_GetContext(NextThread));

                    if(errorCode == oC_ErrorCode_None)
                    {
                        CurrentThread            = NextThread;
                        NextThread               = NULL;
                    }
                    else
                    {
                        oC_Int_t    freeStackSize = oC_Thread_GetFreeStackSize(NextThread,true);
                        oC_Thread_t oldThread     = NextThread;

                        if(freeStackSize <= 0)
                        {
                            kdebuglog(oC_LogType_Error,"systick: Stack of the thread %s is too small! Closing..." , oC_Thread_GetName(oldThread));
                            kdebuglog(oC_LogType_Error,"%s: lack of %d stack bytes..." , oC_Thread_GetName(oldThread) , freeStackSize);
                            char string[60];

                            sprintf(string,"Stack of the thread `%s` is too small! Lack of %d bytes", oC_Thread_GetName(oldThread), freeStackSize);

                            oC_ExcHan_LogEvent("Stack of the thread is too small!\n", string, NULL, oldThread, oC_ExcHan_ExceptionType_ProcessDamaged);

                            if(AutoStackMethod == oC_AutoStackMethod_RerunThread)
                            {
                                oC_MemorySize_t  newStackSize = oC_Thread_GetStackSize(oldThread) + StackIncreaseStep;
                                oC_Thread_t      newThread    = oC_Thread_CloneWithNewStack(oldThread,newStackSize);

                                kdebuglog(oC_LogType_Error , "%s-rerun: new stack size: %uB" , oC_Thread_GetName(oldThread) , newStackSize);

                                errorCode = oC_ThreadMan_AddThread(newThread);

                                if(oC_ErrorOccur(errorCode))
                                {
                                    kdebuglog(oC_LogType_Error , "systick: cannot rerun thread '%s'" , oC_Thread_GetName(oldThread));
                                    kdebuglog(oC_LogType_Error , "systick: rerun error: %R" , errorCode);
                                }
                            }
                        }
                        oC_SaveError("ThreadMan : SysTickHandler - cannot set next context: " , errorCode );
                        oC_ThreadMan_RemoveThread(oldThread);
                        SysTickReturnToIdle();
                    }
                }
                else
                {
                    SysTickReturnToIdle();
                }
            }
            else if(CurrentThread == NULL)
            {
                SysTickReturnToIdle();
            }
        }
        oC_IntMan_ExitCriticalSection();
    }

    oC_ThreadManTickCounter++;
}

//==========================================================================================================================================
//==========================================================================================================================================
static void MemoryEventHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber )
{
    char string[100];

    sprintf(string,"Event flags: 0x%X, Address = %p, %s:%d", Event, Address, Function, LineNumber);

    oC_ExcHan_LogEvent("Memory event in thread manager\n",string, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAccess);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
