/** ****************************************************************************************************************************************
 *
 * @file       oc_kprint.c
 *
 * @brief      The file with source for the kprint module
 *
 * @author     Patryk Kubiak - (Created on: 16 09 2015 18:31:19)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_kprint.h>
#include <oc_streamman.h>
#include <oc_processman.h>
#include <oc_stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define ZEROPAD                 1
#define SIGN                    2
#define PLUS                    4
#define SPACE                   8
#define LEFT                    16
#define SPECIAL                 32
#define LARGE                   64
#define IsDigit(c)              ((c) >= '0' && (c) <= '9')
#define IsBinary(c)             ((c) >= '0' && (c) <= '1')
#define IsOctal(c)              ((c) >= '0' && (c) <= '8')
#define IsHex(c)                ( IsDigit(c) || ( (c) >= 'a' && (c) <= 'f' ) || ( (c) >= 'A' && (c) <= 'F' ))


#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    /* d / i specifiers */
    PointerType_Int             = 0 ,
    PointerType_SignedChar      = 1 ,
    PointerType_ShortInt        = 2 ,
    PointerType_LongInt         = 3 ,
    PointerType_LongLongInt     = 4 ,
    PointerType_IntMax          = 5 ,
    PointerType_Size            = 6 ,
    PointerType_PtrDiff         = 7 ,

    /* u o x */
    PointerType_UnsignedInt         = 0 ,
    PointerType_UnsignedChar        = 1 ,
    PointerType_UnsignedShortInt    = 2 ,
    PointerType_UnsignedLongInt     = 3 ,
    PointerType_UnsignedLongLongInt = 4 ,
    PointerType_UnsignedIntMax      = 5 ,

    /* f e g a */
    PointerType_Float               = 0 ,
    PointerType_Double              = 3 ,
    PointerType_LongDouble          = 8 ,

    /* c s [] [^] */
    PointerType_Char                = 0 ,
    PointerType_WChar               = 3 ,

    /* p */
    PointerType_Void                = 0 ,

} PointerType_t;

typedef enum
{
    ScanfFlags_Ignored      = (1<<0) ,
    ScanfFlags_Signed       = (1<<1) ,
    ScanfFlags_Unsigned     = (1<<2) ,
} ScanfFlags_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static oC_ErrorCode_t ReadInputStream( oC_Stream_t * outStream );
static oC_ErrorCode_t ReadOutputStream( oC_Stream_t * outStream );
static bool           PutcToBuffer( char ** outBuffer , char * EndPointer , char C );
static int            SkipAToI(const char ** Format );
static oC_ErrorCode_t PutNumber( char ** outBuffer , char * EndPointer , long long Number , int Base , int FieldWidth , int Precision, int Type );
static oC_ErrorCode_t PutLongFloatInHex( char ** outBuffer , char * EndPointer , unsigned char * Address, int FieldWidth, int Precision, int Type );
static oC_ErrorCode_t PutFloatInHex( char ** outBuffer , char * EndPointer , unsigned char * Address, int FieldWidth, int Precision, int Type );
static oC_ErrorCode_t PutFloat( char ** outBuffer , char * EndPointer , double Number , int FieldWidth, int Precision, char Format, int Flags );
static oC_ErrorCode_t ReadNumberFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , int Base , ScanfFlags_t Flags , int FieldWidth , PointerType_t PointerType );
static oC_ErrorCode_t ReadFloatFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth , PointerType_t PointerType );
static oC_ErrorCode_t ReadCharFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth );
static oC_ErrorCode_t ReadStringFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth );
static oC_ErrorCode_t ReadPointerFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags );
static oC_ErrorCode_t ReadScansetFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth , const char * ScansetBegin , const char * ScansetEnd , bool ScansetInverted );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool ModuleEnabledFlag = false;
static const char *Digits = "0123456789abcdefghijklmnopqrstuvwxyz";
static const char *UpperDigits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Turns on the module
 *
 * The function turns on the module. It must be called before using the module.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * Standard possible error codes:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleIsTurnedOn                | Module is already turned on
 *  oC_ErrorCode_ImplementError                  | Unexpected error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode,ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn))
    {
        ModuleEnabledFlag = true;
        errorCode         = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the module
 *
 * The function turns off the module. It should be called when the module is no longer needed.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * Standard possible error codes:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_KPrint_TurnOn function
 *  oC_ErrorCode_ImplementError                  | Unexpected error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode,ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;
        errorCode         = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Helper function for getting a character from a string
 * 
 * The function returns the character from the string and moves the pointer to the next character. It can be used for oC_KPrint_Scanf function.
 * 
 * @param UserData          - pointer to the string
 * @param MovePosition      - if true the pointer will be moved to the next character
 * 
 * @return character from the string
 */
//==========================================================================================================================================
int oC_KPrint_GetCharFromString( void* UserData, bool MovePosition )
{
    int result = oC_KPrint_EOF;
    const char** buffer = (const char**)UserData;

    if( isaddresscorrect(buffer) && isaddresscorrect(*buffer) && **buffer != 0 )
    {
        result = (*buffer)[0];
        if(MovePosition)
        {
            (*buffer)++;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief Prints formatted string to the output buffer
 *
 * The function prints formatted string to the output buffer.
 *
 * <b>Supported format specifiers:</b>
 *
 *  Format Specifier    | Description
 * ---------------------|---------------------------------------------------------------------------------------------------
 *   %%c                | Character
 *   %%s                | String
 *   %%d %%i            | Signed decimal integer
 *   %%u                | Unsigned decimal integer
 *   %%o                | Unsigned octal
 *   %%x %%X            | Unsigned hexadecimal
 *   %%f                | Decimal floating point
 *   %%e %%E            | Scientific notation
 *   %%g %%G            | Use the shortest representation: %e or %f
 *   %%a %%A            | Hexadecimal floating point
 *   %%p                | Pointer address
 *   %%n                | Nothing printed
 *   %%M                | Content of #oC_MemorySize_t type (size of memory with units)
 *   %%R                | Error code (from the #oC_ErrorCode_t type)
 *   %%b                | Binary
 *   %%T                | Transfer Speed from the #oC_TransferSpeed_t type (with units)
 *   %%t                | Printing of time (oC_Time_t structure)
 *
 * <b>Supported flags:</b>
 *
 *  Flag    | Description
 * ---------|---------------------------------------------------------------------------------------------------
 *      -   | Left-justify within the given field width; Right justification is the default
 *      +   | Forces to precede the result with a plus or minus sign (+ or -) even for positive numbers
 *  (space) | If no sign is going to be written, a blank space is inserted before the value
 *      #   | Used with o, x or X specifiers the value is preceded with 0, 0x or 0X for values different than zero
 *      0   | Left-pads the number with zeroes (0) instead of spaces when padding is specified
 *      *   | The width is not specified in the format string, but as an additional integer value argument preceding the argument that has to be formatted
 *
 * <b>Supported length modifiers:</b>
 *
 *  Length Modifier   | Description
 * -------------------|---------------------------------------------------------------------------------------------------
 *         hh         | For integer specifiers (d, i, o, u, x, X) the argument is a signed char or unsigned char
 *          h         | For integer specifiers (d, i, o, u, x, X) the argument is a short int or unsigned short int
 *          l         | For integer specifiers (d, i, o, u, x, X) the argument is a long int or unsigned long int
 *         ll         | For integer specifiers (d, i, o, u, x, X) the argument is a long long int or unsigned long long int
 *          L         | For floating point specifiers (f, e, E, g, G, a, A) the argument is a long double
 *
 *
 * <b>Example:</b>
 * @code{.c}
 * char buffer[100];
 * oC_KPrint_Printf(buffer,sizeof(buffer),"Hello %s! %d + %d = %d\n","World",2,3,2+3);
 * @endcode
 *
 * @param outBuffer       pointer to the output buffer
 * @param Size            size of the output buffer
 * @param Format          format string
 * @param ArgumentList    list of arguments
 * @param outSize         destination for size of the string
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 *
 * @note
 * More error codes can be returned from the #PutcToBuffer function
 *
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_OutputBufferTooSmall | Output buffer is too small
 *  #oC_ErrorCode_ModuleNotStartedYet  | Module is turned off. Please call #oC_KPrint_TurnOn function
 *  #oC_ErrorCode_OutputAddressNotInRAM| `outBuffer` is not in RAM section
 *  #oC_ErrorCode_WrongAddress         | Address of the `Format` parameter is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_Format( char * outBuffer , oC_UInt_t Size , const char * Format , va_list ArgumentList, oC_UInt_t* outSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ModuleEnabledFlag == true           , oC_ErrorCode_ModuleNotStartedYet)
     && ErrorCondition( isram(outBuffer)                    , oC_ErrorCode_OutputAddressNotInRAM)
     && ErrorCondition( isaddresscorrect(Format)            , oC_ErrorCode_WrongAddress)
     && ErrorCondition( outSize == NULL || isram(outSize)   , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        int       flags;
        int       fieldWidth;
        int       precision;
        int       qualifier;
        int       base;
        char *    endPointer = &outBuffer[Size];
        char *    startBuffer= outBuffer;
        char *    string     = NULL;

        errorCode = oC_ErrorCode_None;

        for( ;*Format != '\0';Format++)
        {
#define _putc(c)            if(PutcToBuffer(&outBuffer,endPointer,c)==false){ errorCode = oC_ErrorCode_OutputBufferTooSmall; break; }
            if(*Format != '%')
            {
                _putc(*Format);
                continue;
            }

            //==============================================================================================================================
            /*
             * Read format flags
             */
            //==============================================================================================================================
            flags  = 0;
            string = NULL;

repeat:
            Format++;
            switch (*Format)
            {
              case '-': flags |= LEFT;      goto repeat;
              case '+': flags |= PLUS;      goto repeat;
              case ' ': flags |= SPACE;     goto repeat;
              case '#': flags |= SPECIAL;   goto repeat;
              case '0': flags |= ZEROPAD;   goto repeat;
            }

            //==============================================================================================================================
            /*
             * Read field width
             */
            //==============================================================================================================================
            fieldWidth = -1;

            if( IsDigit(*Format) )
            {
                fieldWidth = SkipAToI(&Format);
            }
            else if(*Format == '*')
            {
              Format++;
              fieldWidth = va_arg(ArgumentList, int);

              if(fieldWidth < 0)
              {
                  fieldWidth = -fieldWidth;
                  flags     |= LEFT;
              }
            }

            //==============================================================================================================================
            /*
             * Read precision
             */
            //==============================================================================================================================
            precision = -1;
            if (*Format == '.')
            {
                ++Format;

                if( IsDigit(*Format) )
                {
                    precision = SkipAToI(&Format);
                }
                else if (*Format == '*')
                {
                    ++Format;
                    precision = va_arg(ArgumentList, int);
                }
                if (precision < 0)
                {
                    precision = 0;
                }
            }

            //==============================================================================================================================
            /*
             * Read the conversion qualifier
             */
            //==============================================================================================================================
            qualifier = -1;
            if (*Format == 'h' || *Format == 'l' || *Format == 'L')
            {
                qualifier = *Format;
                Format++;

                if(*Format == 'l')
                {
                    qualifier = 'L';
                    Format++;
                }
            }

            // Default base
            base = 10;

            //==============================================================================================================================
            /*
             * Recognize the identifier
             */
            //==============================================================================================================================
            switch(*Format)
            {
                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Character identifier section */
                case 'c':
                {
                    if(!(flags & LEFT))
                    {
                        while (--fieldWidth > 0)
                        {
                            _putc(' ');
                        }
                    }

                    _putc((unsigned char) va_arg(ArgumentList, int));

                    while (--fieldWidth > 0)
                    {
                        _putc(' ');
                    }
                    continue;
                }
                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Error identifier section */
                case 'R':
                {
                    oC_ErrorCode_t errorCodeArgument = (oC_ErrorCode_t) va_arg(ArgumentList, int);

                    string = (char *)oC_GetErrorString(errorCodeArgument);
                }
                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* String identifier section */
                    // no break
                case 's':
                {
                    int    length = 0;
                    int    index  = 0;

                    /* If string is not NULL it means that 'R' option was used */
                    if( string == NULL )
                    {
                        string = va_arg(ArgumentList, char *);
                    }

                    if( string == NULL )
                    {
                        string = "<NULL>";
                    }
                    else if (isaddresscorrect(string)==false)
                    {
                        string    = "<string address not correct>";
                        errorCode = oC_ErrorCode_WrongAddress;
                    }

                    length = strnlen(string, precision);

                    if (!(flags & LEFT))
                    {
                        while (--fieldWidth >= length )
                        {
                            _putc(' ');
                        }
                    }
                    for (index = 0; index < length; ++index)
                    {
                        fieldWidth--;
                        _putc(*string++);
                    }

                    while (--fieldWidth > 0)
                    {
                        _putc(' ');
                    }

                    continue;
                }

                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Memory section */
                case 'M':
                {
                    int    length = 0;
                    int    index  = 0;
                    oC_MemorySize_t size = (oC_MemorySize_t) va_arg(ArgumentList, unsigned long long);
                    double sizeConverted = (oC_MemorySize_t)size;
                    string = "B";
                    if ( sizeConverted > oC_TB(1ULL) )
                    {
                        sizeConverted /= oC_TB(1ULL);
                        string = "TB";
                    }
                    else if ( sizeConverted > oC_GB(1ULL) )
                    {
                        sizeConverted /= oC_GB(1ULL);
                        string = "GB";
                    }
                    else if ( sizeConverted > oC_MB(1ULL) )
                    {
                        sizeConverted /= oC_MB(1ULL);
                        string = "MB";
                    }
                    else if ( sizeConverted > oC_KB(1ULL) )
                    {
                        sizeConverted /= oC_KB(1ULL);
                        string = "KB";
                    }
                    oC_AssignErrorCode(&errorCode , PutFloat(&outBuffer , endPointer , sizeConverted , fieldWidth, precision, *Format, flags | SIGN));

                    length = strnlen(string, 2);
                    for ( index = 0; index < length; index++ )
                    {
                        _putc(*string++);
                    }
                    continue;
                }
                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Transfer Speed section */
                case 'T':
                {
                    int    length = 0;
                    int    index  = 0;
                    oC_TransferSpeed_t speed = (oC_TransferSpeed_t) va_arg(ArgumentList, unsigned long long);
                    double speedConverted = (oC_TransferSpeed_t)speed;
                    string = "Bbps";
                    if ( speedConverted > oC_TransferSpeed_Tbps(1ULL) )
                    {
                        speedConverted /= oC_TransferSpeed_Tbps(1ULL);
                        string = "Tbps";
                    }
                    else if ( speedConverted > oC_TransferSpeed_Gbps(1ULL) )
                    {
                        speedConverted /= oC_TransferSpeed_Gbps(1ULL);
                        string = "Gbps";
                    }
                    else if ( speedConverted > oC_TransferSpeed_Mbps(1ULL) )
                    {
                        speedConverted /= oC_TransferSpeed_Mbps(1ULL);
                        string = "Mbps";
                    }
                    else if ( speedConverted > oC_TransferSpeed_kbps(1ULL) )
                    {
                        speedConverted /= oC_TransferSpeed_kbps(1ULL);
                        string = "kbps";
                    }
                    oC_AssignErrorCode(&errorCode , PutFloat(&outBuffer , endPointer , speedConverted , fieldWidth, precision, *Format, flags | SIGN));

                    length = strnlen(string, 4);
                    for ( index = 0; index < length; index++ )
                    {
                        _putc(*string++);
                    }
                    continue;
                }
                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Pointer identifier section */
                case 'p':
                {
                    if( fieldWidth == -1 )
                    {
                      fieldWidth = 2 * sizeof(void *) + 2; // + 2 for a 0x
                      flags |= ZEROPAD | SPECIAL;
                    }
                    oC_AssignErrorCode(&errorCode , PutNumber(&outBuffer , endPointer , (unsigned long) va_arg(ArgumentList, void *), 16, fieldWidth, precision, flags));
                    continue;
                }
                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Nothing printed identifier section */
                case 'n':
                {
                    if (qualifier == 'l')
                    {
                        long *ip = va_arg(ArgumentList, long *);
                        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(ip) , oC_ErrorCode_OutputAddressNotInRAM))
                        {
                            *ip = (outBuffer - startBuffer);
                        }
                    }
                    else
                    {
                        int *ip = va_arg(ArgumentList, int *);
                        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(ip) , oC_ErrorCode_OutputAddressNotInRAM))
                        {
                            *ip = (outBuffer - startBuffer);
                        }
                    }
                }
                    continue;
                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Hexadecimal floating point */
                case 'A':
                {
                    flags |= LARGE;
                }
                    // no break
                case 'a':
                {
                    if (qualifier == 'l')
                    {
                        oC_AssignErrorCode(&errorCode , PutLongFloatInHex(&outBuffer , endPointer , va_arg(ArgumentList, unsigned char *), fieldWidth, precision, flags));
                    }
                    else
                    {
                        oC_AssignErrorCode(&errorCode , PutFloatInHex(&outBuffer , endPointer , va_arg(ArgumentList, unsigned char *), fieldWidth, precision, flags));
                    }
                    continue;
                }

                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Integer numbers */
                case 'o':
                    base = 8;
                    break;

                case 'b':
                    base = 2;
                    break;

                case 'X':
                    flags |= LARGE;
                    // no break
                case 'x':
                    base = 16;
                    break;

                case 'd':
                case 'i':
                    flags |= SIGN;
                    // no break
                case 'u':
                    break;

                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* Floating numbers */
                case 'E':
                case 'G':
                case 'e':
                case 'f':
                case 'g':
                {
                  double dnumber = va_arg(ArgumentList, double);
                  oC_AssignErrorCode(&errorCode , PutFloat(&outBuffer , endPointer , dnumber , fieldWidth, precision, *Format, flags | SIGN));
                }
                  continue;

                // /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /* oC_Time_t */
                case 't':
                {
                  oC_Time_t dnumber = va_arg(ArgumentList, double);
                  oC_AssignErrorCode(&errorCode , PutFloat(&outBuffer , endPointer , dnumber , fieldWidth, precision, *Format, flags | SIGN));
                }

                  continue;
                default:
                        if( *Format != '%' )
                        {
                            _putc('%');
                        }
                        if (*Format)
                        {
                            _putc(*Format);
                        }
                        else
                        {
                          --Format;
                        }
                        continue;
            }
            long long number = 0;

            if (qualifier == 'L')
            {
                if (flags & SIGN)
                {
                    number = va_arg(ArgumentList, long long);
                }
                else
                {
                    number = va_arg(ArgumentList, unsigned long long);
                }
            }
            else if (qualifier == 'l')
            {
                if (flags & SIGN)
                {
                    number = va_arg(ArgumentList, long);
                }
                else
                {
                    number = va_arg(ArgumentList, unsigned long);
                }
            }
            else if (qualifier == 'h')
            {
                if (flags & SIGN)
                {
                    number = va_arg(ArgumentList, int);
                }
                else
                {
                    number = va_arg(ArgumentList, unsigned int);
                }
            }
            else if (flags & SIGN)
            {
                number = va_arg(ArgumentList, int);
            }
            else
            {
                number = va_arg(ArgumentList, unsigned int);
            }
            oC_AssignErrorCode(&errorCode , PutNumber(&outBuffer , endPointer , number , base , fieldWidth , precision , flags));
            if(errorCode == oC_ErrorCode_OutputBufferTooSmall)
            {
                endPointer--;
                *endPointer = '\0';
            }
#undef _putc
        }
        if(outSize != NULL && outBuffer > startBuffer)
        {
            *outSize = outBuffer - startBuffer;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prints formatted string to the standard output
 *
 * The function prints formatted string to the standard output.
 *
 * @param outBuffer       pointer to the output buffer
 * @param Size            size of the output buffer
 * @param IoFlags         flags for the input/output operations
 * @param Format          format string (For more information see #oC_KPrint_Format)
 * @param ...             list of arguments
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 *
 * @note
 * More error codes can be returned from the #oC_KPrint_Format function
 *
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_OutputBufferTooSmall | Output buffer is too small
 *  #oC_ErrorCode_ModuleNotStartedYet  | Module is turned off. Please call #oC_KPrint_TurnOn function
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_Printf( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , ... )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode,ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        va_list argumentList;

        va_start(argumentList, Format);
        errorCode = oC_KPrint_VPrintf(outBuffer,Size,IoFlags,Format,argumentList);
        va_end(argumentList);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prints formatted string to the standard output
 *
 * The function prints formatted string to the standard output.
 *
 * @param outBuffer       pointer to the output buffer
 * @param Size            size of the output buffer
 * @param IoFlags         flags for the input/output operations
 * @param Format          format string (For more information see #oC_KPrint_Format)
 * @param ...             list of arguments
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 *
 * @note
 * More error codes can be returned from the #oC_KPrint_Format function
 *
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_OutputBufferTooSmall | Output buffer is too small
 *  #oC_ErrorCode_ModuleNotStartedYet  | Module is turned off. Please call #oC_KPrint_TurnOn function
 *  #oC_ErrorCode_RecursiveDriverUsage | Recursive driver usage
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_VPrintf( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , va_list ArgumentList )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode,ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        errorCode = oC_KPrint_Format(outBuffer,Size,Format,ArgumentList, NULL);

        if(!oC_ErrorOccur(errorCode) || errorCode == oC_ErrorCode_OutputBufferTooSmall )
        {
            errorCode = oC_KPrint_WriteToStdOut(IoFlags,outBuffer,strlen(outBuffer));
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prints formatted string to the standard output (for drivers)
 * 
 * The function prints formatted string to the standard output. It is used by drivers. It prevents recursive usage of the same driver.
 * 
 * @param outBuffer       pointer to the output buffer
 * @param Size            size of the output buffer
 * @param IoFlags         flags for the input/output operations
 * @param Driver          driver that want to print the data
 * @param Format          format string (For more information see #oC_KPrint_Format)
 * @param ...             list of arguments
 * 
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * 
 * @note
 * More error codes can be returned from the #oC_KPrint_Format and #oC_KPrint_WriteToStdOut functions
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_OutputBufferTooSmall | Output buffer is too small
 *  #oC_ErrorCode_ModuleNotStartedYet  | Module is turned off. Please call #oC_KPrint_TurnOn function
 *  #oC_ErrorCode_RecursiveDriverUsage | Recursive driver usage
 *  #oC_ErrorCode_WrongAddress         | Wrong address of buffer or format string
 *  #oC_ErrorCode_ImplementError       | Unexpected error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_DriverPrintf( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , oC_Driver_t Driver , const char * Format , ... )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode,ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        va_list     argumentList;
        oC_Stream_t stream = NULL;

        va_start(argumentList, Format);
        if(
                oC_AssignErrorCode(       &errorCode , ReadOutputStream(&stream) ) &&
                oC_AssignErrorCodeIfFalse(&errorCode , oC_Stream_GetDriver(stream) != Driver , oC_ErrorCode_RecursiveDriverUsage) &&
                oC_AssignErrorCode(       &errorCode , oC_KPrint_Format(outBuffer,Size,Format,argumentList, NULL))
                )
        {
            errorCode = oC_KPrint_WriteToStdOut(IoFlags,outBuffer,Size);
        }
        va_end(argumentList);
    }

    return errorCode;
}

//==========================================================================================================================================
/** 
 * @brief Formatted scanf from string
 * 
 * The function reads data from string and stores them according to the parameter format into the locations given by the additional arguments.
 * 
 * @param Buffer    Pointer to the string to be read
 * @param Format    Format string that contains the text to be written to stdout. It can optionally contain embedded format tags that are replaced by the values specified in subsequent additional arguments and formatted as requested.
 * @param ...       Additional arguments
 * 
 * @return 
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module not started yet
 *  #oC_ErrorCode_WrongAddress           | Wrong address of buffer or format string
 * 
 * More error codes can be returned from the #oC_KPrint_FormatScanfFromStream function.
 * 
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_FormatScanf( const char * Buffer , const char * Format , va_list ArgumentList )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode, ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode, isaddresscorrect(Buffer)  , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode, isaddresscorrect(Format)  , oC_ErrorCode_WrongAddress)
        )
    {
        errorCode = oC_KPrint_FormatScanfFromStream(oC_KPrint_GetCharFromString, (void*)&Buffer, Format , ArgumentList);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief  Formatted scanf from stream
 * 
 * The function reads data from stream and stores them according to the parameter format into the locations given by the additional arguments.
 * 
 * Supported format specifiers:
 *  Format Specifier    | Description
 * ---------------------|---------------------------------------------------------------------------------------------------
 *   %%c                | Character
 *   %%s                | String
 *   %%d %%i            | Signed decimal integer
 *   %%u                | Unsigned decimal integer
 *   %%o                | Unsigned octal
 *   %%x %%X            | Unsigned hexadecimal
 *   %%f                | Decimal floating point
 *   %%e %%E            | Scientific notation
 *   %%g %%G            | Use the shortest representation: %e or %f
 *   %%a %%A            | Hexadecimal floating point
 *   %%p                | Pointer address
 * 
 * Supported flags:
 *  Flag    | Description
 * ---------|---------------------------------------------------------------------------------------------------
 *      -   | Left-justify within the given field width; Right justification is the default
 *      +   | Forces to precede the result with a plus or minus sign (+ or -) even for positive numbers
 *  (space) | If no sign is going to be written, a blank space is inserted before the value
 *      #   | Used with o, x or X specifiers the value is preceded with 0, 0x or 0X for values different than zero
 *      0   | Left-pads the number with zeroes (0) instead of spaces when padding is specified
 * 
 *  Supported length modifiers:
 *  Length Modifier   | Description
 * -------------------|---------------------------------------------------------------------------------------------------
 *       hh         | For integer specifiers (d, i, o, u, x, X) the argument is a signed char or unsigned char
 * 
 * @param  GetChar  Pointer to function which will be used to get character from stream
 * @param  UserData User data which will be passed to GetChar function
 * @param  Format   Format string that specifies how subsequent arguments are converted for input
 * @param  ...      Additional arguments
 * 
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module was not started yet
 *  #oC_ErrorCode_FunctionNotExists      | Function `GetChar` does not exists
 *  #oC_ErrorCode_WrongAddress           | Address of the `Format` parameter is not correct
 *  #oC_ErrorCode_StringNotAsExpected    | String from stream is not as expected
 *  #oC_ErrorCode_SpecifierNotHandled    | Specifier is not handled
 *  #oC_ErrorCode_WrongArgument          | Wrong argument was passed to function 
 *  
 *  @note
 *  More error codes can be returned by function #ReadNumber, #ReadFloat, #ReadChar, #ReadString, #ReadScanset
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_FormatScanfFromStream( oC_KPrint_GetChar_t GetChar , void * UserData , const char * Format , va_list ArgumentList )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode, ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode, isaddresscorrect(GetChar) , oC_ErrorCode_FunctionNotExists) &&
        oC_AssignErrorCodeIfFalse(&errorCode, isaddresscorrect(Format)  , oC_ErrorCode_WrongAddress)
        )
    {
#define _getc()     GetChar(UserData,true)
#define _peek()     GetChar(UserData,false)
#define _eof()      (_peek() == oC_KPrint_EOF)
        errorCode = oC_ErrorCode_None;


        for(;oC_ErrorOccur(errorCode) == false && isaddresscorrect(Format) && (*Format) && !_eof(); Format++ )
        {
            if(*Format == '%')
            {
                ScanfFlags_t    flags      = 0;
                oC_UInt_t       fieldWidth = 0;
                PointerType_t   pointerType= 0;
                int             base       = 0;

                Format++; /* Skip '%' */

                /* Check if argument is ignored */
                if(*Format == '*')
                {
                    flags |= ScanfFlags_Ignored;
                    Format++;
                }

                /* Read field width */
                while(IsDigit(*Format) && isaddresscorrect(Format))
                {
                    fieldWidth *= 10;
                    fieldWidth += (int)(*Format - '0');
                    Format++;
                }

                /* Read length = pointer type */
                switch(*Format)
                {
                    case 'h':
                        Format++;
                        if(*Format == 'h')
                        {
                            Format++;
                            pointerType = 1;
                        }
                        else
                        {
                            pointerType = 2;
                        }
                        break;
                    case 'l':
                        Format++;
                        if(*Format == 'l')
                        {
                            Format++;
                            pointerType = 4;
                        }
                        else
                        {
                            pointerType = 3;
                        }
                        break;
                    case 'j':
                        Format++;
                        pointerType = 5;
                        break;
                    case 'z':
                        Format++;
                        pointerType = 6;
                        break;
                    case 't':
                        Format++;
                        pointerType = 7;
                        break;
                    case 'L':
                        Format++;
                        pointerType = 8;
                        break;
                }

                switch(*Format)
                {
                    case '%':
                        if(_getc() != *Format)
                        {
                            errorCode = oC_ErrorCode_StringNotAsExpected;
                        }
                        break;
                    /* Auto-base integer */
                    case 'i':
                        flags |= ScanfFlags_Signed;
                        base   = 0;

                        errorCode = ReadNumberFromStream( GetChar, UserData, &ArgumentList, base , flags , fieldWidth , pointerType );
                        break;
                    /* Binary integer */
                    case 'b':
                        flags |= ScanfFlags_Unsigned;
                        base   = 2;

                        errorCode = ReadNumberFromStream(GetChar, UserData, &ArgumentList , base , flags , fieldWidth , pointerType );
                        break;
                    /* Decimal integer */
                    case 'd':
                    case 'u':
                        flags |= ScanfFlags_Signed;
                        base   = 10;

                        errorCode = ReadNumberFromStream( GetChar, UserData, &ArgumentList , base , flags , fieldWidth , pointerType );
                        break;
                    /* Octal integer */
                    case 'o':
                        flags |= ScanfFlags_Unsigned;
                        base   = 8;

                        errorCode = ReadNumberFromStream( GetChar, UserData, &ArgumentList , base , flags , fieldWidth , pointerType );
                        break;
                    /* Hexadecimal integer */
                    case 'x':
                        flags |= ScanfFlags_Unsigned;
                        base   = 16;

                        errorCode = ReadNumberFromStream( GetChar, UserData, &ArgumentList , base , flags , fieldWidth , pointerType );
                        break;
                    /* Floating-point */
                    case 'f':
                    case 'e':
                    case 'g':
                    case 'a':
                        flags |= ScanfFlags_Signed;
                        base   = 0;

                        errorCode = ReadFloatFromStream( GetChar, UserData, &ArgumentList , flags , fieldWidth , pointerType );
                        break;
                    /* Character */
                    case 'c':
                        errorCode = ReadCharFromStream( GetChar, UserData, &ArgumentList , flags , fieldWidth );
                        break;
                    /* String */
                    case 's':
                        errorCode = ReadStringFromStream( GetChar, UserData, &ArgumentList, flags , fieldWidth );
                        break;
                    case 'p':
                        errorCode = ReadPointerFromStream( GetChar, UserData, &ArgumentList , flags );
                        break;
                    case '[':
                    {
                        bool scansetInverted = false;

                        Format++;

                        if(*Format == '^')
                        {
                            Format++;
                            scansetInverted = true;
                        }

                        const char * scansetBegin = Format;
                        const char * scansetEnd   = Format;

                        while( (isaddresscorrect(scansetEnd)) && (*scansetEnd != 0) && (*scansetEnd != ']'))
                        {
                            scansetEnd++;
                            Format++;
                        }

                        errorCode = ReadScansetFromStream( GetChar, UserData, &ArgumentList , flags , fieldWidth , scansetBegin , scansetEnd , scansetInverted );
                    }
                        break;

                    case 'n':
                    default:
                        errorCode = oC_ErrorCode_SpecifierNotHandled;
                        break;

                }
            }
            else if(*Format == _peek())
            {
                _getc();
                continue;
            }
            else
            {
                oC_SaveError("scanf - reading string error", oC_ErrorCode_StringNotAsExpected);
            }
        }

#undef _getc
#undef _peek
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief scans a string for a format string and fills the argument list
 * 
 * The function scans a string for a format string and fills the argument list
 * 
 * @param outBuffer     pointer to the buffer where the string is stored
 * @param Size          size of the buffer
 * @param IoFlags       flags for the io operation
 * @param Format        format string
 * @param ...           variable argument list
 * 
 * @return oC_ErrorCode_t error code
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_Scanf( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , ... )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode, ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        va_list argumentList;
        va_start(argumentList,Format);
        errorCode = oC_KPrint_VScanf(outBuffer,Size,IoFlags,Format,argumentList);
        va_end(argumentList);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief scans a string for a format string and fills the argument list
 * 
 * The function scans a string for a format string and fills the argument list
 * 
 * @param outBuffer pointer to the buffer where the string is stored
 * @param Size size of the buffer
 * @param IoFlags flags for the input
 * @param Format format string
 * @param ArgumentList argument list
 * 
 * @return oC_ErrorCode_t error code
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_VScanf( char * outBuffer , oC_UInt_t Size , oC_IoFlags_t IoFlags , const char * Format , va_list ArgumentList )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode, ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode, isaddresscorrect(Format) , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCode(&errorCode, oC_KPrint_ReadFromStdIn(IoFlags,outBuffer,Size))
        )
    {
        errorCode = oC_KPrint_FormatScanf(outBuffer,Format,ArgumentList);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief  Writes string to the standard output
 * 
 * The function writes string to the standard output
 * 
 * @param IoFlags flags for the input/output operations
 * @param Buffer  pointer to the buffer with the string
 * @param Size    size of the buffer
 * 
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * 
 * @note
 * More error codes can be returned from the #oC_Stream_Write and #ReadOutputStream functions
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet  | Module is turned off. Please call #oC_KPrint_TurnOn function
 *  #oC_ErrorCode_WrongAddress         | Wrong address of buffer
 *  #oC_ErrorCode_SizeNotCorrect       | Size of the buffer is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_WriteToStdOut( oC_IoFlags_t IoFlags , const char * Buffer , oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet    )
     && ErrorCondition( isaddresscorrect(Buffer)  , oC_ErrorCode_WrongAddress           )
     && ErrorCondition( Size > 0                  , oC_ErrorCode_SizeNotCorrect         )
        )
    {
        oC_Stream_t stream = NULL;

        if(oC_AssignErrorCode(&errorCode , ReadOutputStream(&stream)))
        {
            char *       eolRef     = strchr(Buffer,'\n');
            const char   newline[]  = "\r";

            if(eolRef != NULL)
            {
                oC_MemorySize_t sentBytes   = 0;
                do
                {
                    oC_MemorySize_t newLineSize = 1;
                    oC_MemorySize_t bytesToSend = 0;
                    bool     sendEol     = false;

                    for( oC_MemorySize_t i = sentBytes ; i < Size ; i++ )
                    {
                        bytesToSend++;

                        if(Buffer[i] == '\n')
                        {
                            sendEol = true;
                            break;
                        }
                    }

                    if(
                        ErrorCode( oC_Stream_Write( stream, &Buffer[sentBytes], &bytesToSend, IoFlags ))
                     && (
                           sendEol == false
                        || ErrorCode( oC_Stream_Write( stream, newline, &newLineSize, IoFlags ))
                           )
                        )
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                    else
                    {
                        break;
                    }
                    sentBytes += bytesToSend;
                } while( sentBytes < Size);
            }
            else
            {
                errorCode = oC_Stream_Write(stream,Buffer,&Size,IoFlags);
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief  Reads string from the standard input
 * 
 * The function reads string from the standard input
 * 
 * @param IoFlags flags for the input/output operations
 * @param outBuffer pointer to the buffer where the string will be stored
 * @param Size size of the buffer
 * 
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * 
 * @note
 * More error codes can be returned from the #oC_Stream_Read and #ReadInputStream functions
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet  | Module is turned off. Please call #oC_KPrint_TurnOn function
 *  #oC_ErrorCode_OutputAddressNotInRAM| `outBuffer` is not in RAM
 *  #oC_ErrorCode_SizeNotCorrect       | Size of the buffer is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_KPrint_ReadFromStdIn( oC_IoFlags_t IoFlags , char * outBuffer , oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ModuleEnabledFlag == true, oC_ErrorCode_ModuleNotStartedYet     )
     && ErrorCondition( isram(outBuffer)         , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( Size > 0                 , oC_ErrorCode_SizeNotCorrect          )
        )
    {
        oC_Stream_t stream      = NULL;
        oC_Stream_t outStream   = NULL;
        char        passwordChar= '*';

        if(
            oC_AssignErrorCode(&errorCode , ReadInputStream(&stream))
         && ( !( IoFlags & oC_IoFlags_EchoWhenRead      ) || oC_AssignErrorCode(&errorCode , ReadOutputStream(&outStream))          )
         && ( !( IoFlags & oC_IoFlags_ClearRxBeforeRead ) || oC_AssignErrorCode(&errorCode , oC_Stream_ClearReadBuffer(stream))     )
            )
        {
            if(IoFlags & oC_IoFlags_EchoWhenRead)
            {
                oC_MemorySize_t bytesToRead = Size;
                oC_MemorySize_t bytesToSend = 1;
                oC_MemorySize_t readBytes   = 0;
                oC_MemorySize_t sentBytes   = 0;
                char     backspace   = 127;

                IoFlags &= ~oC_IoFlags_WaitForAllElements;
                IoFlags |= oC_IoFlags_WaitForSomeElements;

                errorCode = oC_ErrorCode_None;

                while(readBytes < Size && !oC_ErrorOccur(errorCode))
                {
                    if(oC_AssignErrorCode(&errorCode , oC_Stream_Read(stream,&outBuffer[readBytes],&bytesToRead,IoFlags)))
                    {
                        if(oC_Bits_AreBitsSetU32(IoFlags,oC_IoFlags_ReadOneLine) && (outBuffer[readBytes] == '\r'))
                        {
                            const char * newLine = "\n\r";
                            bytesToSend          = 2;
                            outBuffer[readBytes] = 0;
                            errorCode            = oC_Stream_Write(stream,newLine,&bytesToSend,oC_IoFlags_WaitForAllElements | oC_IoFlags_Default);
                            readBytes--;
                            break;
                        }
                        else if(outBuffer[readBytes] != '\033')
                        {
                            /* Removing characters according to backspaces */
                            while(readBytes >= 0 && outBuffer[readBytes + bytesToRead - 1] == backspace)
                            {
                                outBuffer[readBytes] = 0;

                                if(readBytes > 0)
                                {
                                    bytesToSend          = 1;
                                    errorCode            = oC_Stream_Write(stream,&backspace,&bytesToSend,oC_IoFlags_WaitForAllElements | oC_IoFlags_Default);
                                    readBytes--;
                                    outBuffer[readBytes] = 0;
                                }
                                bytesToRead--;
                            }

                            if(oC_Bits_AreBitsSetU32(IoFlags,oC_IoFlags_EchoAsPassword))
                            {
                                sentBytes = 0;

                                while(sentBytes < bytesToRead && !oC_ErrorOccur(errorCode))
                                {
                                    bytesToSend = 1;
                                    errorCode   = oC_Stream_Write(stream,&passwordChar,&bytesToSend,oC_IoFlags_WaitForAllElements | oC_IoFlags_Default);
                                    sentBytes  += bytesToSend;
                                }
                            }
                            else if(bytesToRead > 0)
                            {
                                bytesToSend = bytesToRead;
                                errorCode   = oC_Stream_Write(stream,&outBuffer[readBytes],&bytesToSend,oC_IoFlags_WaitForAllElements | oC_IoFlags_Default);
                            }

                            readBytes   += bytesToRead;
                            bytesToRead  = Size - readBytes;
                        }

                    }
                }
            }
            else
            {
                errorCode = oC_Stream_Read(stream,outBuffer,&Size,IoFlags);
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief reads input stream of the current process
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadInputStream( oC_Stream_t * outStream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Process_t currentProcess = oC_ProcessMan_GetCurrentProcess();

    if(ErrorCondition(oC_Process_IsCorrect(currentProcess) , oC_ErrorCode_ProcessNotCorrect))
    {
        errorCode  = oC_ErrorCode_None;
        *outStream = oC_Process_GetInputStream(currentProcess);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads output stream of the current process
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadOutputStream( oC_Stream_t * outStream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Process_t currentProcess = oC_ProcessMan_GetCurrentProcess();

    if(ErrorCondition( oC_Process_IsCorrect(currentProcess), oC_ErrorCode_ProcessNotCorrect ))
    {
        errorCode  = oC_ErrorCode_None;
        *outStream = oC_Process_GetOutputStream(currentProcess);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief skips format to the next specifier
 */
//==========================================================================================================================================
static int SkipAToI(const char ** Format )
{
  int index = 0;

  while(IsDigit(**Format))
  {
      index = index * 10 + *((*Format)++) - '0';
  }

  return index;
}

//==========================================================================================================================================
/**
 * @brief Puts character to the buffer
 */
//==========================================================================================================================================
static bool PutcToBuffer( char ** outBuffer , char * EndPointer , char C )
{
    bool sizeOk = false;

    if(*outBuffer < EndPointer)
    {
        *(*outBuffer) = C;
        (*outBuffer)++;
        sizeOk        = true;
    }

    return sizeOk;
}

//==========================================================================================================================================
/**
 * @brief Puts number to the buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t PutNumber( char ** outBuffer , char * EndPointer , long long Number , int Base , int FieldWidth , int Precision, int Type )
{
  oC_ErrorCode_t errorCode = oC_ErrorCode_None;
  char emptyChar = ' '; /* Character given before number */
  char sign      = 0;   /* Sign ('-'/'+'/' ') of the number */
  char digitBuffer[66];
  const char *digitPointer = Digits;
  int digitIndex = 0;

#define _putc(c)            if(PutcToBuffer(outBuffer,EndPointer,c)==false){ errorCode = oC_ErrorCode_OutputBufferTooSmall;}

  if(Type & LARGE)
  {
      digitPointer = UpperDigits;
  }

  if(Type & LEFT)
  {
      Type &= ~ZEROPAD;
  }

  if(oC_AssignErrorCodeIfFalse(&errorCode , (Base == 8) || (Base == 10) || (Base == 16) || (Base == 2) , oC_ErrorCode_DigitBaseNotCorrect))
  {
      emptyChar = (Type & ZEROPAD) ? '0' : ' ';
      sign      = 0;

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Get a sign */
      if (Type & SIGN)
      {
          if(Number < 0)
          {
              sign = '-';
              Number = -Number;
              FieldWidth--;
          }
          else if(Type & PLUS)
          {
              sign = '+';
              FieldWidth--;
          }
          else if (Type & SPACE)
          {
              sign = ' ';
              FieldWidth--;
          }
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Decrement field width for '0x' or for '0' in case of Hex/Octal bases */
      if (Type & SPECIAL)
      {
          if( Base == 16 )
          {
              FieldWidth -= 2;
          }
          else if( Base == 8 )
          {
              FieldWidth--;
          }
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Print variable to temporary digit buffer */
      if( Number == 0 )
      {
          digitBuffer[digitIndex++] = '0';
      }
      else
      {
          while( Number != 0 )
          {
              digitBuffer[digitIndex++] = digitPointer[((unsigned long long) Number) % (unsigned) Base];
              Number = ((unsigned long long) Number) / (unsigned) Base;
          }
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Update precision if it was too small */
      if( digitIndex > Precision )
      {
          Precision = digitIndex;
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Decrement field width for digits */
      FieldWidth -= Precision;

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* If there will be place, insert spaces */
      if (!(Type & (ZEROPAD | LEFT)))
      {
          while(FieldWidth-- > 0)
          {
              _putc(' ');
          }
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Put sign after spaces */
      if(sign)
      {
          _putc(sign);
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Put digit base */
      if(Type & SPECIAL)
      {
          if (Base == 8)
          {
              _putc('0');
          }
          else if (Base == 16)
          {
              _putc('0');
              _putc('x');
          }
          else if (Base == 2)
          {
              _putc('0');
              _putc('b');
          }
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* If should not be padding to the left, insert special char */
      if(!(Type & LEFT))
      {
          while(FieldWidth-- > 0)
          {
              _putc(emptyChar);
          }
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Insert zeros */
      while(digitIndex < Precision--)
      {
          _putc('0');
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Insert digits */
      while(digitIndex-- > 0)
      {
          _putc(digitBuffer[digitIndex]);
      }

      // ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      /* Insert spaces for the end of the string */
      while(FieldWidth-- > 0)
      {
          _putc(' ');
      }
  }

#undef _putc
  return errorCode;
}

//==========================================================================================================================================
/**
 * @brief puts long number to the buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t PutLongFloatInHex( char ** outBuffer , char * EndPointer , unsigned char * Address, int FieldWidth, int Precision, int Type )
{
#define _putc(c)            if(PutcToBuffer(outBuffer,EndPointer,c)==false){ errorCode = oC_ErrorCode_OutputBufferTooSmall; }

    oC_ErrorCode_t  errorCode = oC_ErrorCode_None;
    char            digitsBuffer[24];
    const char *    digits = Digits;
    int             digitIndex;
    int             length;

    if( Type & LARGE )
    {
        digits = UpperDigits;
    }

    length = 0;

    for (digitIndex = 0; digitIndex < 6; digitIndex++)
    {
        if (digitIndex != 0)
        {
            digitsBuffer[length++] = ':';
        }

        digitsBuffer[length++] = digits[Address[digitIndex] >> 4];
        digitsBuffer[length++] = digits[Address[digitIndex] & 0x0F];
    }

    if (!(Type & LEFT))
    {
        while (length < FieldWidth--)
        {
            _putc(' ');
        }
    }

    for (digitIndex = 0; digitIndex < length; ++digitIndex)
    {
        _putc(digitsBuffer[digitIndex]);
    }

    while(length < FieldWidth--)
    {
        _putc(' ');
    }

    return errorCode;
#undef _putc
}

//==========================================================================================================================================
/**
 * @brief puts float in hex to the buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t PutFloatInHex( char ** outBuffer , char * EndPointer , unsigned char * Address, int FieldWidth, int Precision, int Type )
{
#define _putc(c)            if(PutcToBuffer(outBuffer,EndPointer,c)==false){ errorCode = oC_ErrorCode_OutputBufferTooSmall; }
    oC_ErrorCode_t  errorCode = oC_ErrorCode_None;
    char digitsBuffer[24];
    int digitIndex;
    int n;
    int length;

    length = 0;

    for( digitIndex = 0 ; digitIndex < 4 ; digitIndex++ )
    {
        if( digitIndex != 0 )
        {
            digitsBuffer[length++] = '.';
        }

        n = Address[digitIndex];

        if (n == 0)
        {
            digitsBuffer[length++] = Digits[0];
        }
        else
        {
            if (n >= 100)
            {
                digitsBuffer[length++] = Digits[n / 100];
                n = n % 100;
                digitsBuffer[length++] = Digits[n / 10];
                n = n % 10;
            }
            else if (n >= 10)
            {
                digitsBuffer[length++] = Digits[n / 10];
                n = n % 10;
            }

            digitsBuffer[length++] = Digits[n];
        }
    }

    if (!(Type & LEFT))
    {
        while( length < FieldWidth-- )
        {
            _putc(' ');
        }
    }
    for( digitIndex = 0 ; digitIndex < length ; ++digitIndex )
    {
        _putc(digitsBuffer[digitIndex]);
    }

    while( length < FieldWidth-- )
    {
        _putc(' ');
    }

    return errorCode;
#undef _putc
}

//==========================================================================================================================================
/**
 * @brief puts float to the buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t PutFloat( char ** outBuffer , char * EndPointer , double Number , int FieldWidth, int Precision, char Format, int Flags )
{
#define _putc(c)            if(PutcToBuffer(outBuffer,EndPointer,c)==false){ errorCode = oC_ErrorCode_OutputBufferTooSmall; }
    oC_ErrorCode_t errorCode                = oC_ErrorCode_None;
    char           emptyChar                = 0;
    char           sign                     = 0;
    int            numberOfIntegerDigits    = 1;
    int            numberOfFractionalDigits = 0;
    int            numberOfPointChars       = 0;
    int            numberOfSignChars        = 0;
    int            minimumBufferSize        = 0;
    double         integerPart              = 0;
    double         fractionalPart           = 0;
    char           integerDigits[16];

    /* Left align means no zeropadding */
    if( Flags & LEFT )
    {
        Flags &= ~ZEROPAD;
    }

    emptyChar = ( Flags & ZEROPAD ) ? '0' : ' ';

    sign = 0;
    if( Flags & SIGN )
    {
        if (Number < 0.0)
        {
            sign = '-';
            Number = -Number;
            FieldWidth--;
        }
        else if (Flags & PLUS)
        {
            sign = '+';
            FieldWidth--;
        }
        else if (Flags & SPACE)
        {
            sign = ' ';
            FieldWidth--;
        }
    }

    integerPart    = floor(Number);
    fractionalPart = Number - integerPart;

    // Compute the precision value
    if( Precision < 0 )
    {
        Precision = 6; // Default precision: 6
    }
    else if( Precision == 0 && Format == 'g' )
    {
        Precision = 1; // ANSI specified
    }

    if(Precision == 0 && !(Flags & SPECIAL))
    {
        numberOfPointChars = 0;
    }
    else
    {
        numberOfPointChars = 1;
    }

    if(sign)
    {
        numberOfSignChars = 1;
    }

    /* Count number of integer part digits */
    for(double testNumber = 10 ; testNumber <= Number ; numberOfIntegerDigits++ , testNumber *= 10 );

    numberOfFractionalDigits = Precision;

    minimumBufferSize = numberOfFractionalDigits + numberOfIntegerDigits + numberOfPointChars + numberOfSignChars;

    while( !(Flags & LEFT) && FieldWidth > minimumBufferSize)
    {
        _putc(emptyChar);
        FieldWidth--;
    }

    if(sign)
    {
        _putc(sign);
    }

    for(int digitIndex = (numberOfIntegerDigits-1); digitIndex >= 0 ; digitIndex-- )
    {
        integerDigits[digitIndex] = Digits[((unsigned long) integerPart) % (unsigned) 10];
        integerPart /= 10;
    }

    for(int digitIndex = 0 ; digitIndex < numberOfIntegerDigits ; digitIndex++ )
    {
        _putc(integerDigits[digitIndex]);
    }

    if(numberOfPointChars)
    {
        _putc('.');
    }

    for(int digitIndex = 0; digitIndex < numberOfFractionalDigits ; digitIndex++ )
    {
        fractionalPart *= 10;
        char digit      = Digits[((unsigned long) fractionalPart) % (unsigned) 10];
        _putc(digit);
    }

    while( (Flags & LEFT) && FieldWidth > minimumBufferSize)
    {
        _putc(' ');
        FieldWidth--;
    }

    return errorCode;
#undef _putc
}

//==========================================================================================================================================
/**
 * @brief reads number from the stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadNumberFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , int Base , ScanfFlags_t Flags , int FieldWidth , PointerType_t PointerType )
{
#define _getc()                 GetChar(UserData,true)
#define _peek()                 GetChar(UserData,false)
#define _eof()                  (_peek() == oC_KPrint_EOF)

    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    uint64_t       value     = 0;
    bool           filled    = false;
    bool           negative  = false;
    int            realBase  = 0;
    int            c         = -1;

    //======================================================================================================================================
    /*
     * Read sign
     */
    //======================================================================================================================================
    if(_peek() == '-')
    {
        _getc();
        negative = true;
    }
    else if(_peek() == '+')
    {
        _getc();
        negative = false;
    }

    //======================================================================================================================================
    /*
     * Read base
     */
    //======================================================================================================================================
    if(_peek() == '0')
    {
        _getc();

        if(_peek() == 'x')
        {
            _getc();
            realBase = 16;
        }
        else if(_peek() == 'b')
        {
            _getc();
            realBase = 2;
        }
        else if(IsDigit(_peek()) && Base == 0)
        {
            realBase = 8;
        }
        else
        {
            c = '0';    // It is just 0, so save it for later
            realBase = Base;
        }
    }
    else
    {
        realBase = Base;
    }

    if(realBase == 0)
    {
        realBase = 10;
    }

    //======================================================================================================================================
    /*
     * Check if base is correct
     */
    //======================================================================================================================================
    if(Base != 0 && realBase != Base)
    {
        errorCode = oC_ErrorCode_WrongNumberFormat;
    }
    else
    {
        //==================================================================================================================================
        /*
         * Read value
         */
        //==================================================================================================================================
        while(!_eof() && !oC_ErrorOccur(errorCode))
        {
            bool readNext = c == -1;
            switch(realBase)
            {
                case 2:
                    if(IsBinary(c))
                    {
                        filled = true;
                        value *= realBase;
                        value += c - '0';
                        c = -1;
                    }
                    else if(IsBinary(_peek()))
                    {
                        filled = true;
                        value *= realBase;
                        value += _peek() - '0';
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_WrongNumberFormat;
                    }
                    break;
                case 8:
                    if(IsOctal(c))
                    {
                        filled = true;
                        value *= realBase;
                        value += c - '0';
                        c = -1;
                    }
                    else if(IsOctal(_peek()))
                    {
                        filled = true;
                        value *= realBase;
                        value += _peek() - '0';
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_WrongNumberFormat;
                    }
                    break;
                case 10:
                    if(IsDigit(c))
                    {
                        filled = true;
                        value *= realBase;
                        value += c - '0';
                        c = -1;
                    }
                    else if(IsDigit(_peek()))
                    {
                        filled = true;
                        value *= realBase;
                        value += _peek() - '0';
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_WrongNumberFormat;
                    }
                    break;
                case 16:
                    if(IsHex(c))
                    {
                        filled = true;
                        value *= realBase;
                        if(IsDigit(c))
                        {
                            value += c - '0';
                        }
                        else
                        {
                            value += (uint64_t)tolower(c) - (uint64_t)('a') + 10;
                        }
                        c = -1;
                    }
                    else if(IsHex(_peek()))
                    {
                        filled = true;
                        value *= realBase;
                        if(IsDigit(_peek()))
                        {
                            value += _peek() - '0';
                        }
                        else
                        {
                            value += (uint64_t)tolower(_peek()) - (uint64_t)('a') + 10;
                        }
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_WrongNumberFormat;
                    }
                    break;
            }
            if(!oC_ErrorOccur(errorCode) && readNext)
            {
                _getc();
            }
        }

        /*
         * Value is filled or buffer is not at the start, this means, that value is correct
         * 
         * TODO: I have removed checking if the buffer is at the start, because I think it is not needed, but I don't remember why I have added it. 
         * If you find out why it is needed, please add it back and add comment why it is needed.
         */
        if(filled /*|| buffer > (*Buffer) */)
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    //==================================================================================================================================
    /*
     * Save value to the pointer
     */
    //==================================================================================================================================
    if(!(Flags & ScanfFlags_Ignored))
    {
        void * address = va_arg(*ArgumentList, void *);

        if(isram(address) == false)
        {
            errorCode = oC_ErrorCode_OutputAddressNotInRAM;
        }
        else if((Flags & ScanfFlags_Unsigned) && negative == true)
        {
            errorCode = oC_ErrorCode_WrongNumberFormat;
        }
        else
        {
#define SaveValueAsType(Type)      *((Type*)address) = (negative) ? (-((Type)value)) : ((Type)value)
            if(Flags & ScanfFlags_Signed)
            {
                switch(PointerType)
                {
                    case PointerType_Int:
                        SaveValueAsType(int);
                        break;
                    case PointerType_SignedChar:
                        SaveValueAsType(signed char);
                        break;
                    case PointerType_ShortInt:
                        SaveValueAsType(short int);
                        break;
                    case PointerType_LongInt:
                        SaveValueAsType(long int);
                        break;
                    case PointerType_LongLongInt:
                        SaveValueAsType(long long int);
                        break;
                    case PointerType_IntMax:
                        SaveValueAsType(intmax_t);
                        break;
                    case PointerType_Size:
                        SaveValueAsType(size_t);
                        break;
                    case PointerType_PtrDiff:
                        SaveValueAsType(ptrdiff_t);
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch(PointerType)
                {
                    case PointerType_Int:
                        SaveValueAsType(unsigned int);
                        break;
                    case PointerType_SignedChar:
                        SaveValueAsType(unsigned char);
                        break;
                    case PointerType_ShortInt:
                        SaveValueAsType(unsigned short int);
                        break;
                    case PointerType_LongInt:
                        SaveValueAsType(unsigned long int);
                        break;
                    case PointerType_LongLongInt:
                        SaveValueAsType(unsigned long long int);
                        break;
                    case PointerType_IntMax:
                        SaveValueAsType(uintmax_t);
                        break;
                    case PointerType_Size:
                        SaveValueAsType(size_t);
                        break;
                    case PointerType_PtrDiff:
                        SaveValueAsType(ptrdiff_t);
                        break;
                    default:
                        break;
                }
            }
#undef SaveValueAsType
        }
    }

#undef _getc
#undef _peek
#undef _eof

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads float from the stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadFloatFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth , PointerType_t PointerType )
{
#define _getc()                 GetChar(UserData,true)
#define _peek()                 GetChar(UserData,false)
#define _eof()                  (_peek() == oC_KPrint_EOF)

    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    long double    value     = 0;
    long double    fractional= 0;
    int            precision = 0;
    bool           negative  = false;

    //======================================================================================================================================
    /*
     * Read sign
     */
    //======================================================================================================================================
    if(_peek() == '-')
    {
        _getc();
        negative = true;
    }
    else if(_peek() == '+')
    {
        _getc();
        negative = false;
    }

    //======================================================================================================================================
    /*
     * Read integer part
     */
    //======================================================================================================================================
    while(!_eof() && !oC_ErrorOccur(errorCode))
    {

        if(IsDigit(_peek()))
        {
            value *= 10;
            value += (long double)((_peek()) - '0');
        }
        else if(_peek() == '.')
        {
            _getc();
            break;
        }
        else
        {
            break;
        }

        _getc();
    }

    //======================================================================================================================================
    /*
     * Read fractional part
     */
    //======================================================================================================================================
    while(!_eof() && !oC_ErrorOccur(errorCode))
    {

        if(IsDigit(_peek()))
        {
            fractional *= 10;
            fractional += (long double)((_peek()) - '0');
            precision++;
        }
        else
        {
            break;
        }

        _getc();
    }

    //======================================================================================================================================
    /*
     * Convert fractional to fractional part
     */
    //======================================================================================================================================
    while(precision > 0)
    {
        fractional /= 10;
        precision--;
    }

    //======================================================================================================================================
    /*
     * Add integer to fractional part
     */
    //======================================================================================================================================
    value += fractional;

    //======================================================================================================================================
    /*
     * Save value to the pointer
     */
    //======================================================================================================================================
    if(!(Flags & ScanfFlags_Ignored))
    {
        void * address = va_arg(*ArgumentList, void *);

        if(isram(address) == false)
        {
            errorCode = oC_ErrorCode_OutputAddressNotInRAM;
        }
        else
        {
#define SaveValueAsType(Type)      *((Type*)address) = (negative) ? (-(Type)value):((Type)value)
            switch(PointerType)
            {
                case PointerType_Float:
                    SaveValueAsType(float);
                    break;
                case PointerType_Double:
                    SaveValueAsType(double);
                    break;
                case PointerType_LongDouble:
                    SaveValueAsType(long double);
                    break;
                default:
                    break;
            }
#undef SaveValueAsType
        }
    }

#undef _getc
#undef _peek
#undef _eof

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads character from the stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadCharFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth )
{
#define _getc()     GetChar(UserData,true)
#define _peek()     GetChar(UserData,false)
#define _eof()      (_peek() == oC_KPrint_EOF)

    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    char *outPointer = NULL;

    //======================================================================================================================================
    /*
     * Set default values
     */
    //======================================================================================================================================
    if ( FieldWidth == 0 )
    {
        FieldWidth = 1;
    }

    //======================================================================================================================================
    /*
     * Read and check pointer
     */
    //======================================================================================================================================
    if ( !(Flags & ScanfFlags_Ignored) )
    {
        outPointer = va_arg( *ArgumentList, char* );
    }

    //======================================================================================================================================
    /*
     * Read characters
     */
    //======================================================================================================================================
    while( !_eof() && !oC_ErrorOccur( errorCode ) && (FieldWidth > 0) )
    {
        if (!isspace(_peek()) )
        {
            if ( isram( outPointer ) )
            {
                *outPointer = (char)_peek();
                outPointer++;
            }
            else if ( outPointer != NULL )
            {
                errorCode = oC_ErrorCode_OutputAddressNotInRAM;
            }
            FieldWidth--;
        }
        _getc();
    }

#undef _getc
#undef _peek
#undef _eof
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads string from the stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadStringFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth )
{
#define _getc()                 GetChar(UserData,true)
#define _peek()                 GetChar(UserData,false)
#define _eof()                  (_peek() == oC_KPrint_EOF)
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    char *         outPointer= NULL;
    bool           widthSet  = FieldWidth != 0;

    //======================================================================================================================================
    /*
     * Read pointer
     */
    //======================================================================================================================================
    if(!(Flags & ScanfFlags_Ignored))
    {
        outPointer = va_arg(*ArgumentList, char *);
    }

    //======================================================================================================================================
    /*
     * Read string
     */
    //======================================================================================================================================
    while(!_eof() && !oC_ErrorOccur(errorCode) && (isspace(_peek()) == false))
    {
        if(isram(outPointer))
        {
            *outPointer = _peek();
            outPointer++;
        }
        else if(outPointer != NULL)
        {
            errorCode = oC_ErrorCode_OutputAddressNotInRAM;
        }

        if(widthSet)
        {
            if(FieldWidth > 0)
            {
                FieldWidth--;
            }
            else
            {
                break;
            }
        }
        _getc();
    }
    if ( !oC_ErrorOccur(errorCode) && isram(outPointer) )
    {
        *outPointer = 0;
    }

#undef _getc
#undef _peek
#undef _eof

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads pointer from the stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadPointerFromStream( oC_KPrint_GetChar_t GetChar, void* UserData, va_list * ArgumentList , ScanfFlags_t Flags )
{
#define _getc()                 GetChar(UserData,true)
#define _peek()                 GetChar(UserData,false)
#define _eof()                  (_peek() == oC_KPrint_EOF)

    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    void **        outPointer= NULL;
    oC_UInt_t      value     = 0;

    //======================================================================================================================================
    /*
     * Read pointer
     */
    //======================================================================================================================================
    if(!(Flags & ScanfFlags_Ignored))
    {
        outPointer = va_arg(*ArgumentList, void **);
    }

    //======================================================================================================================================
    /*
     * Read base
     */
    //======================================================================================================================================
    if(_peek() == '0')
    {
        _getc();

        if(_peek() == 'x')
        {
            _getc();
        }
        else
        {
            errorCode = oC_ErrorCode_WrongNumberFormat;
        }
    }

    //======================================================================================================================================
    /*
     * Read value
     */
    //======================================================================================================================================
    while(!_eof() && !oC_ErrorOccur(errorCode))
    {
        int c = tolower(_peek());
        if(IsHex(c))
        {
            value *= 16;
            if(IsDigit(c))
            {
                value += c - '0';
            }
            else
            {
                value += (oC_UInt_t)((oC_UInt_t)(c) - (oC_UInt_t)('a')) + 0xa;
            }
        }
        else
        {
            break;
        }
        _getc();
    }

    if(isram(outPointer))
    {
        *outPointer = (void*)value;
    }
    else if(outPointer != NULL)
    {
        errorCode = oC_ErrorCode_OutputAddressNotInRAM;
    }

#undef _getc
#undef _peek
#undef _eof
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads scanset from the stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadScansetFromStream( oC_KPrint_GetChar_t GetChar, void* UserData , va_list * ArgumentList , ScanfFlags_t Flags , int FieldWidth , const char * ScansetBegin , const char * ScansetEnd , bool ScansetInverted )
{
#define _getc()                 GetChar(UserData,true)
#define _peek()                 GetChar(UserData,false)
#define _eof()                  (_peek() == oC_KPrint_EOF)
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    char *         outPointer= NULL;
    bool           widthSet  = FieldWidth != 0;

    //======================================================================================================================================
    /*
     * Read pointer
     */
    //======================================================================================================================================
    if(!(Flags & ScanfFlags_Ignored))
    {
        outPointer = va_arg(*ArgumentList, char *);
    }

    //======================================================================================================================================
    /*
     * Read string
     */
    //======================================================================================================================================
    while(!_eof() && !oC_ErrorOccur(errorCode))
    {
        bool signIsInScanset = false;
        /* Searching if this character is one of ScansetBuffer buffer */
        for(const char * scanset = ScansetBegin ; scanset < ScansetEnd ; scanset++)
        {
            if(*scanset == _peek())
            {
                signIsInScanset = true;
                break;
            }
        }

        /* Check if we are looking for characters or not */
        if(signIsInScanset == ScansetInverted)
        {
            break;
        }

        if(isram(outPointer))
        {
            *outPointer = _peek();
            outPointer++;
        }
        else if(outPointer != NULL)
        {
            errorCode = oC_ErrorCode_OutputAddressNotInRAM;
        }

        if(widthSet)
        {
            if(FieldWidth > 0)
            {
                FieldWidth--;
            }
            else
            {
                break;
            }
        }
        _getc();
    }

#undef _getc
#undef _peek
#undef _eof
    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________


