/** ****************************************************************************************************************************************
 *
 * @file       oc_user.c
 *
 * @brief      __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 6 wrz 2015 16:42:35) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_user.h>
#include <oc_stdlib.h>
#include <oc_object.h>
#include <oc_md5.h>
#include <string.h>
#include <oc_memman.h>
#include <oc_system.h>
#include <oc_system_cfg.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct User_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_User_Permissions_t   Permissions;
    char                    Name[CFG_UINT8_MAX_LOGIN_LENGTH];
    char                    Password[CFG_UINT8_MAX_PASSWORD_LENGTH];
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with in. functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_User_t oC_User_New( Allocator_t Allocator , AllocationFlags_t Flags , const char * Name , const char * Password , oC_User_Permissions_t Permissions )
{
    oC_User_t user = NULL;

    if(oC_MemMan_IsAddressCorrect(Name) && oC_MemMan_IsAddressCorrect(Password))
    {
        user = kmalloc(sizeof(struct User_t) , Allocator , Flags);

        if(user)
        {
            user->ObjectControl = oC_CountObjectControl(user,oC_ObjectId_User );
            user->Permissions   = Permissions;
            strncpy(user->Password  , md5(Password) , CFG_UINT8_MAX_PASSWORD_LENGTH);
            strncpy(user->Name      , Name          , CFG_UINT8_MAX_LOGIN_LENGTH);
        }
    }

    return user;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_User_Delete( oC_User_t * User )
{
    bool deleted = false;

    if(isram(User) && oC_User_IsCorrect(*User))
    {
        (*User)->ObjectControl = 0;

        if(kfree(*User,AllocationFlags_CanWaitForever))
        {
            deleted = true;
            *User   = NULL;
        }
    }

    return deleted;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_User_IsCorrect( oC_User_t User )
{
    return isram(User) && oC_CheckObjectControl(User,oC_ObjectId_User,User->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_User_GetName( oC_User_t User )
{
    const char * name = "unknown";

    if(oC_User_IsCorrect(User))
    {
        name = User->Name;
    }

    return name;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_User_Rename( oC_User_t User , const char * NewName )
{
    bool result = false;

    if(oC_User_IsCorrect(User)  &&
       !oC_User_IsRoot(User)    &&
       isaddresscorrect(NewName)&&
       oC_User_CheckPermissions(getcuruser(), User->Permissions)
       )
    {
        strncpy(User->Name, NewName, CFG_UINT8_MAX_LOGIN_LENGTH);
        result = true;
    }

    return result;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_User_CheckPassword( oC_User_t User , const char * Password )
{
    bool correct = false;

    if(oC_User_IsCorrect(User) && strcmp(md5(Password),User->Password) == 0)
    {
        correct = true;
    }

    return correct;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_User_CheckPermissions( oC_User_t User , oC_User_Permissions_t Permissions )
{
    bool correct = false;

    if(oC_User_IsCorrect(User) &&
      (Permissions & ~User->Permissions) == 0 )
    {
        correct = true;
    }

    return correct;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_User_Permissions_t oC_User_GetPermissions( oC_User_t User)
{
    oC_User_Permissions_t permissions = 0;

    if(oC_User_IsCorrect(User))
    {
        permissions = User->Permissions;
    }

    return permissions;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_User_ModifyPermissions( oC_User_t User , oC_User_Permissions_t Permissions )
{
    bool result = false;

    if(oC_User_IsCorrect(User)            &&
       0 != strcmp(User->Name, "root")    &&
       oC_User_CheckPermissions(getcuruser(), Permissions) &&
       !oC_User_IsRoot(User)
       )
    {
        User->Permissions = Permissions;
        result = true;
    }

    return result;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_User_IsRoot( oC_User_t User)
{
    return 0 == strcmp(oC_User_GetName(User), "root");
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

