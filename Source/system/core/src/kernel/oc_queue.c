/** ****************************************************************************************************************************************
 *
 * @brief      Stores source for queues object
 *
 * @file       oc_queue.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_object.h>
#include <oc_queue.h>
#include <oc_list.h>
#include <oc_stdlib.h>
#include <oc_null.h>
#include <oc_thread.h>
#include <oc_semaphore.h>
#include <oc_intman.h>
#include <oc_debug.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct Queue_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_MemorySize_t         GetIndex;
    oC_MemorySize_t         PutIndex;
    oC_Semaphore_t          UsedSlotsSemaphore;
    oC_Semaphore_t          FreeSlotsSemaphore;
    oC_MemorySize_t         BufferSize;
    uint8_t                 Buffer[1];
} * Queue_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
/**
 * @addtogroup Queue
 * @{
 */

//==========================================================================================================================================
/**
 * @brief allocates memory for a queue
 *
 * The function allocates memory for a new queue object
 *
 * @param Allocator                 Allocator to use for the allocation (you can use #getcurallocator to get current allocator of the process)
 * @param BufferSize                The size of the buffer for the queue
 *
 * @return pointer to the queue or #NULL if error
 * 
 * @see oC_Queue_Delete
 * 
 * @code{.c}
   
    oC_Queue_t queue = oC_Queue_New( getcurallocator() , 100 );
    if(queue)
    {
            // The queue is created
    }
    else
    {
            // The queue is not created
    }

   @endcode
 */
//==========================================================================================================================================
oC_Queue_t oC_Queue_New( Allocator_t Allocator , oC_MemorySize_t BufferSize )
{
    Queue_t queue = NULL;

    if(
        oC_SaveIfFalse( "Allocator" , isaddresscorrect(Allocator), oC_ErrorCode_WrongAddress    )
     && oC_SaveIfFalse( "BufferSize", BufferSize > 0             , oC_ErrorCode_SizeNotCorrect  )
        )
    {
        queue = kmalloc( sizeof(struct Queue_t) + BufferSize, Allocator, AllocationFlags_ZeroFill );

        if( oC_SaveIfFalse( "queue" , queue != NULL, oC_ErrorCode_AllocationError ) )
        {
            queue->UsedSlotsSemaphore = oC_Semaphore_New(BufferSize,          0, Allocator, AllocationFlags_Default);
            queue->FreeSlotsSemaphore = oC_Semaphore_New(BufferSize, BufferSize, Allocator, AllocationFlags_Default);

            if(
                oC_SaveIfFalse( "used slots semaphore", queue->UsedSlotsSemaphore != NULL, oC_ErrorCode_AllocationError )
             && oC_SaveIfFalse( "free slots semaphore", queue->FreeSlotsSemaphore != NULL, oC_ErrorCode_AllocationError )
                )
            {
                queue->ObjectControl = oC_CountObjectControl( queue, oC_ObjectId_Queue );
                queue->BufferSize    = BufferSize;
                queue->GetIndex      = 0;
                queue->PutIndex      = 0;
            }
            else
            {
                if(queue->UsedSlotsSemaphore != NULL)
                {
                    oC_SaveIfFalse( "used slots", oC_Semaphore_Delete(&queue->UsedSlotsSemaphore,0), oC_ErrorCode_ReleaseError );
                }
                if(queue->FreeSlotsSemaphore != NULL)
                {
                    oC_SaveIfFalse( "free slots", oC_Semaphore_Delete(&queue->FreeSlotsSemaphore,0), oC_ErrorCode_ReleaseError );
                }
                oC_SaveIfFalse( "queue"     , kfree(queue,AllocationFlags_Default), oC_ErrorCode_ReleaseError );
                queue = NULL;
            }
        }
    }

    return queue;
}

//==========================================================================================================================================
/**
 * @brief deletes the queue
 *
 * The function deletes the queue and frees the memory
 *
 * @param Queue                     The pointer to the queue to delete (the pointer is set to #NULL after the deletion)
 *
 * @return true if the queue is deleted
 * 
 * @see oC_Queue_New
 * 
 * @code{.c}
   
    oC_Queue_t queue = oC_Queue_New( getcurallocator() , 100 );
    if(queue)
    {
            // The queue is created
            if(oC_Queue_Delete(&queue))
            {
                // The queue is deleted
            }
            else
            {
                // The queue is not deleted
            }
    }
    else
    {
            // The queue is not created
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_Queue_Delete( oC_Queue_t * Queue )
{
    bool deleted = false;

    if(
        oC_SaveIfFalse("Queue", isram(Queue)                , oC_ErrorCode_AddressNotInRam  )
     && oC_SaveIfFalse("Queue", oC_Queue_IsCorrect(*Queue)  , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        Queue_t    queue             = *Queue;
        bool       semaphore1deleted = oC_Semaphore_Delete(&queue->FreeSlotsSemaphore, 0);
        bool       semaphore2deleted = oC_Semaphore_Delete(&queue->UsedSlotsSemaphore, 0);
        bool       mainReleased      = kfree( queue, AllocationFlags_Default );

        deleted = oC_SaveIfFalse("Free slots semaphore", semaphore1deleted , oC_ErrorCode_ReleaseError)
               && oC_SaveIfFalse("Used slots semaphore", semaphore2deleted , oC_ErrorCode_ReleaseError)
               && oC_SaveIfFalse("Main object memory"  , mainReleased      , oC_ErrorCode_ReleaseError);
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief checks if the queue is correct
 *
 * The function checks if the queue is correct
 *
 * @param Queue                     The pointer to the queue
 *
 * @return true if the queue is correct
 * 
 * @see oC_Queue_New
 * 
 * @code{.c}
   
    oC_Queue_t queue = oC_Queue_New( getcurallocator() , 100 );
    if(queue)
    {
            // The queue is created
            if(oC_Queue_IsCorrect(queue))
            {
                // The queue is correct
            }
            else
            {
                // The queue is not correct
            }
    }
    else
    {
            // The queue is not created
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_Queue_IsCorrect( oC_Queue_t Queue )
{
    return isram(Queue) && oC_CheckObjectControl(Queue, oC_ObjectId_Queue, ((Queue_t)Queue)->ObjectControl);
}
//==========================================================================================================================================
/**
 * @brief puts data to the queue
 *
 * The function puts data to the queue
 *
 * @param Queue                     The pointer to the queue
 * @param Data                      The pointer to the data to put
 * @param Size                      The size of the data to put
 * @param Timeout                   The timeout for the operation
 *
 * @return true if the data is put to the queue
 * 
 * @see oC_Queue_Get
 * 
 * @code{.c}
   
    oC_Queue_t queue = oC_Queue_New( getcurallocator() , 100 );
    if(queue)
    {
            // The queue is created
            if(oC_Queue_Put(queue, "Hello World!", 12, oC_s(1)))
            {
                // The data is put to the queue
            }
            else
            {
                // The data is not put to the queue
            }
    }
    else
    {
            // The queue is not created
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_Queue_Put( oC_Queue_t Queue , const void * Data , uint32_t Size , oC_Time_t Timeout )
{
    bool    dataPushed = false;
    Queue_t queue      = Queue;

    if(
        oC_SaveIfFalse( "Queue"  , oC_Queue_IsCorrect(Queue), oC_ErrorCode_ObjectNotCorrect         )
     && oC_SaveIfFalse( "data"   , isaddresscorrect(Data)   , oC_ErrorCode_WrongAddress             )
     && oC_SaveIfFalse( "Size"   , Size > 0                 , oC_ErrorCode_SizeNotCorrect           )
     && oC_SaveIfFalse( "Timeout", Timeout >= 0             , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        const uint8_t * buffer = Data;
        uint32_t offset = 0;
        oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
        uint32_t putSize = Size;

        while(oC_Semaphore_TakeCountingSoft(queue->FreeSlotsSemaphore,&putSize, 1,gettimeout(endTimestamp)))
        {
            kdebuglog( oC_LogType_Info, "put: %d\n", putSize );
            oC_IntMan_EnterCriticalSection();
            for(uint32_t i = 0; i < putSize; i++)
            {
                queue->Buffer[queue->PutIndex++] = buffer[offset++];
                if(queue->PutIndex >= queue->BufferSize)
                {
                    queue->PutIndex = 0;
                }
            }

            oC_Semaphore_GiveCounting(queue->UsedSlotsSemaphore,putSize);
            oC_IntMan_ExitCriticalSection();
            if(offset >= Size)
            {
                dataPushed = true;
                break;
            }
            putSize = Size - offset;
        }
    }

    return dataPushed;
}

//==========================================================================================================================================
/**
 * @brief gets data from the queue
 *
 * The function gets data from the queue
 *
 * @param Queue                     The pointer to the queue
 * @param outData                   The pointer to the buffer for the data
 * @param Size                      The size of the data to get
 * @param Timeout                   The timeout for the operation
 *
 * @return true if the data is got from the queue
 * 
 * @see oC_Queue_Put
 * 
 * @code{.c}
   
    oC_Queue_t queue = oC_Queue_New( getcurallocator() , 100 );
    if(queue)
    {
            // The queue is created
            char buffer[100];
            if(oC_Queue_Put(queue, "Hello World!", 12, oC_s(1)))
            {
                // The data is put to the queue
                if(oC_Queue_Get(queue, buffer, 12, oC_s(1)))
                {
                    // The data is got from the queue
                }
                else
                {
                    // The data is not got from the queue
                }
            }
            else
            {
                // The data is not put to the queue
            }
    }
    else
    {
            // The queue is not created
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_Queue_Get( oC_Queue_t Queue , void * outData , uint32_t Size , oC_Time_t Timeout )
{
    bool    dataReady = false;
    Queue_t queue     = Queue;

    if(
        oC_SaveIfFalse( "Queue"  , oC_Queue_IsCorrect(Queue)            , oC_ErrorCode_ObjectNotCorrect         )
     && oC_SaveIfFalse( "outData", isram(outData) || outData == NULL    , oC_ErrorCode_OutputAddressNotInRAM    )
     && oC_SaveIfFalse( "Size"   , Size > 0                             , oC_ErrorCode_SizeNotCorrect           )
     && oC_SaveIfFalse( "Timeout", Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        uint8_t * outBuffer = outData;
        oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
        uint32_t offset = 0;
        uint32_t takenSize = Size;

        while(oC_Semaphore_TakeCountingSoft(queue->UsedSlotsSemaphore,&takenSize,1,gettimeout(endTimestamp)))
        {
            oC_IntMan_EnterCriticalSection();

            for(uint32_t i = 0; i < takenSize; i++)
            {
                if(outBuffer != NULL)
                {
                    outBuffer[offset++] = queue->Buffer[queue->GetIndex];
                }
                queue->GetIndex++;
                if(queue->GetIndex >= queue->BufferSize)
                {
                    queue->GetIndex = 0;
                }
            }
            oC_Semaphore_GiveCounting(queue->FreeSlotsSemaphore,takenSize);
            oC_IntMan_ExitCriticalSection();

            if(offset >= Size)
            {
                dataReady = true;
                break;
            }
            takenSize = Size - offset;
        }
    }

    return dataReady;
}

/** @] */
#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
