/** ****************************************************************************************************************************************
 *
 * @file       oc_intman.c
 *
 * @brief      The file with interface for interrupts manager
 *
 * @author     Patryk Kubiak - (Created on: 15 06 2015 20:51:43)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_intman.h>
#include <oc_sys_lld.h>

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________
//! @addtogroup IntMan
//! @{

//==========================================================================================================================================
/**
 * @brief Enter critical section
 */
//==========================================================================================================================================
void oC_IntMan_EnterCriticalSection( void )
{
    oC_SYS_LLD_EnterCriticalSection();
}

//==========================================================================================================================================
/**
 * @brief Exit critical section
 */
//==========================================================================================================================================
void oC_IntMan_ExitCriticalSection( void )
{
    oC_SYS_LLD_ExitCriticalSection();
}

//==========================================================================================================================================
/**
 * @brief Check if interrupts are turned on
 *
 * @return true if interrupts are turned on
 */
//==========================================================================================================================================
bool oC_IntMan_AreInterruptsTurnedOn( void )
{
    return oC_SYS_LLD_AreInterruptsEnabled();
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________
//! @}

