/** ****************************************************************************************************************************************
 *
 * @file       oc_process.c
 *
 * @brief      The file with source for process
 *
 * @author     Patryk Kubiak - (Created on: 6 09 2015 16:28:50)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_process.h>
#include <oc_memman.h>
#include <oc_user.h>
#include <oc_object.h>
#include <oc_list.h>
#include <oc_system_cfg.h>
#include <oc_threadman.h>
#include <oc_intman.h>
#include <oc_stdtypes.h>
#include <oc_system.h>
#include <oc_ktime.h>
#include <oc_processman.h>
#include <string.h>
#include <oc_vfs.h>
#include <oc_mutex.h>
#include <oc_udp.h>
#include <oc_icmp.h>
#include <oc_tcp.h>
#include <oc_exchan.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Process_t
{
    oC_ObjectControl_t          ObjectControl;
    oC_Process_Priority_t       Priority;
    const char *                Name;
    oC_User_t                   User;
    oC_Stream_t                 InputStream;
    oC_Stream_t                 OutputStream;
    oC_Stream_t                 ErrorStream;
    oC_Allocator_t              Allocator;
    bool                        Killed;
    oC_HeapMap_t                HeapMap;
    oC_IoFlags_t                IoFlags;
    char *                      Pwd;
    oC_UInt_t                   Pid;
    void *                      StdioBuffer;
    oC_Mutex_t                  StdioMutex;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static void MemoryFaultHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t Allocator = {
                .Name           = "process" ,
                .EventHandler   = MemoryFaultHandler ,
                .EventFlags     = MemoryEventFlags_BufferOverflow       |
                                  MemoryEventFlags_DataSectionOverflow  |
                                  MemoryEventFlags_MemoryExhausted      |
                                  MemoryEventFlags_PanicMemoryExhausted |
                                  MemoryEventFlags_PossibleMemoryLeak
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Creates a new process
 * 
 * The function creates a new process
 * 
 * @param Priority          priority of process
 * @param Name              name of process
 * @param User              user of process
 * @param HeapMapSize       size of heap map
 * @param InputStream       default input stream for the process
 * @param OutputStream      default output stream for the process
 * @param ErrorStream       default error stream for the process
 * 
 * @return Created process or #NULL if error
 * 
 * @note In case of error you can check the details by using the #oC_ReadLastError function
 */
//==========================================================================================================================================
oC_Process_t oC_Process_New( oC_Process_Priority_t Priority , const char * Name , oC_User_t User , oC_UInt_t HeapMapSize, oC_Stream_t InputStream , oC_Stream_t OutputStream , oC_Stream_t ErrorStream )
{
    oC_Process_t process = NULL;

    if(
        oC_MemMan_IsAddressCorrect(Name) &&
        oC_User_IsCorrect(User)
        )
    {
        if(InputStream != NULL && !oC_Stream_IsType(InputStream , oC_Stream_Type_Input))
        {
            oC_SaveError(Name,oC_ErrorCode_StreamTypeNotCorrect);
        }
        if(OutputStream != NULL && !oC_Stream_IsType(OutputStream , oC_Stream_Type_Output))
        {
            oC_SaveError(Name,oC_ErrorCode_StreamTypeNotCorrect);
        }
        if(ErrorStream != NULL && !oC_Stream_IsType(ErrorStream , oC_Stream_Type_Output))
        {
            oC_SaveError(Name,oC_ErrorCode_StreamTypeNotCorrect);
        }

        process = kmalloc(sizeof(struct Process_t) , &Allocator , AllocationFlags_CanWait1Second);

        if(process)
        {
            oC_Process_t currentProcess     = oC_ProcessMan_GetCurrentProcess();
            process->ObjectControl          = oC_CountObjectControl(process,oC_ObjectId_Process);
            process->ErrorStream            = ErrorStream;
            process->InputStream            = InputStream;
            process->Name                   = Name;
            process->OutputStream           = OutputStream;
            process->Priority               = Priority;
            process->User                   = User;
            process->Allocator.Name         = Name;
            process->Allocator.EventHandler = MemoryFaultHandler;
            process->Allocator.EventFlags   = MemoryEventFlags_AllErrors;
            process->Killed                 = false;
            process->IoFlags                = oC_IoFlags_Default;
            process->HeapMap                = oC_MemMan_AllocateHeapMap(HeapMapSize,&process->Allocator,oC_FUNCTION,__LINE__,AllocationFlags_CanWait1Second);
            process->Pwd                    = oC_Process_GetPwd(currentProcess);
            process->Pid                    = oC_ProcessMan_GetNextPid();
            process->StdioBuffer            = kmalloc(CFG_BYTES_STDIO_BUFFER_SIZE * sizeof(char) , &process->Allocator , AllocationFlags_CanWait1Second);
            process->StdioMutex             = oC_Mutex_New(oC_Mutex_Type_Normal,&process->Allocator,AllocationFlags_CanWait1Second);
        }
    }

    return process;
}

//==========================================================================================================================================
/**
 * @brief Deletes the process
 * 
 * The function deletes the process
 * 
 * @param Process           process
 * 
 * @return true if deleted
 * 
 * @note In case of error you can check the details by using the #oC_ReadLastError function
 */
//==========================================================================================================================================
bool oC_Process_Delete( oC_Process_t * Process )
{
    bool deleted = false;

    if(oC_Process_IsCorrect(*Process) && oC_Process_Kill(*Process))
    {
        (*Process)->ObjectControl = 0;
        deleted                   = true;

        oC_ErrorCode_t errorCode = oC_VirtualFileSystem_fcloseFromProcess(*Process);

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError("Process delete - VFS error" , errorCode );
            deleted = false;
        }

        errorCode = oC_Udp_ReleaseAllPortsReservedBy(*Process,s(2));

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError("Process delete - UDP error - " , errorCode );
            deleted = false;
        }

        errorCode = oC_Icmp_ReleaseAllTypesReservedBy(*Process,s(2));

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError("Process delete - ICMP error - " , errorCode );
            deleted = false;
        }

        errorCode = oC_Tcp_CloseProcess(*Process,s(2));

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveError("Process delete - TCP error - " , errorCode );
            deleted = false;
        }

        if(!oC_Mutex_Delete(&((*Process)->StdioMutex),AllocationFlags_CanWait1Second))
        {
            oC_SaveError("Process: Cannot delete StdioMutex",oC_ErrorCode_CannotDeleteObject);
            deleted = false;
        }

        if(oC_MemMan_FreeAllMemoryOfAllocator(&(*Process)->Allocator)==false)
        {
            oC_SaveError("Process delete - Error when releasing memory of allocator: " , oC_ErrorCode_ReleaseError);
            deleted = false;
        }

        if(kfree(*Process,AllocationFlags_CanWaitForever)==false)
        {
            oC_SaveError("Process delete - cannot release process memory" , oC_ErrorCode_ReleaseError);
            deleted  = false;
        }
        else
        {
            *Process = NULL;
        }

    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief Checks if the process object is correct
 * 
 * The function checks if the process object is correct
 * 
 * @param Process           process to check 
 * 
 * @return true if correct
 */
//==========================================================================================================================================
bool oC_Process_IsCorrect( oC_Process_t Process )
{
    return isram(Process) && oC_CheckObjectControl(Process,oC_ObjectId_Process,Process->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief Checks if the process is killed
 * 
 * The function checks if the process is killed
 * 
 * @param Process           process to check
 * 
 * @return true if killed
 */
//==========================================================================================================================================
bool oC_Process_IsKilled( oC_Process_t Process )
{
    return oC_Process_IsCorrect(Process) && Process->Killed;
}

//==========================================================================================================================================
/**
 * @brief Checks if the process contains the thread
 * 
 * The function checks if the process contains the thread
 * 
 * @param Process           process
 * @param Thread            thread
 * 
 * @return true if contains
 */
//==========================================================================================================================================
bool oC_Process_ContainsThread( oC_Process_t Process , oC_Thread_t Thread )
{
    bool contains = false;

    if(oC_Process_IsCorrect(Process) && oC_Thread_IsCorrect(Thread))
    {
        contains = oC_MemMan_GetAllocatorOfAddress(Thread) == &Process->Allocator;
    }

    return contains;
}

//==========================================================================================================================================
/**
 * @brief Gets the name of the process
 * 
 * The function gets the name of the process
 * 
 * @param Process           process
 * 
 * @return name of the process
 */
//==========================================================================================================================================
const char * oC_Process_GetName( oC_Process_t Process )
{
    const char * name = "unknown";

    if(oC_Process_IsCorrect(Process))
    {
        name = Process->Name;
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief Gets the state of the process
 * 
 * The function gets the state of the process
 * 
 * @param Process           process
 * 
 * @return state of the process
 */
//==========================================================================================================================================
oC_Process_State_t oC_Process_GetState( oC_Process_t Process )
{
    oC_Process_State_t state = oC_Process_State_Invalid;

    if(oC_Process_IsCorrect(Process))
    {
        oC_IntMan_EnterCriticalSection();
        oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

        if(threads)
        {
            if(Process->Killed)
            {
                state = oC_Process_State_Killed;
            }
            else
            {
                state = oC_Process_State_Initialized;
            }

            uint32_t numberOfthreads = 0;

            oC_List_Foreach(threads,thread)
            {
                if(oC_Process_ContainsThread(Process,thread))
                {
                    numberOfthreads++;

                    if(Process->Killed)
                    {
                        state = oC_Process_State_Zombie;
                    }
                    else if(oC_Thread_IsBlocked(thread))
                    {
                        state = oC_Process_State_Asleep;
                    }
                    else
                    {
                        state = oC_Process_State_Run;
                        break;
                    }
                }
            }

            if(numberOfthreads == 0)
            {
                state = oC_Process_State_Zombie;
            }
        }
        oC_IntMan_ExitCriticalSection();
    }

    return state;
}

//==========================================================================================================================================
/**
 * @brief Gets the user of the process
 * 
 * The function gets the user of the process
 * 
 * @param Process           process
 * 
 * @return user of the process
 */
//==========================================================================================================================================
oC_User_t oC_Process_GetUser( oC_Process_t Process )
{
    oC_User_t user = NULL;

    if(oC_Process_IsCorrect(Process))
    {
        user = Process->User;
    }

    return user;
}

//==========================================================================================================================================
/**
 * @brief Gets input stream of the process
 * 
 * The function gets input stream of the process
 * 
 * @param Process           process
 * 
 * @return input stream of the process
 */
//==========================================================================================================================================
oC_Stream_t oC_Process_GetInputStream( oC_Process_t Process )
{
    oC_Stream_t stream = NULL;

    if(oC_Process_IsCorrect(Process))
    {
        stream = Process->InputStream;
    }

    return stream;
}

//==========================================================================================================================================
/**
 * @brief Gets output stream of the process
 * 
 * The function gets output stream of the process
 * 
 * @param Process           process
 * 
 * @return output stream of the process
 */
//==========================================================================================================================================
oC_Stream_t oC_Process_GetOutputStream( oC_Process_t Process )
{
    oC_Stream_t stream = NULL;

    if(oC_Process_IsCorrect(Process))
    {
        stream = Process->OutputStream;
    }

    return stream;
}

//==========================================================================================================================================
/**
 * @brief Gets error stream of the process
 * 
 * The function gets error stream of the process
 * 
 * @param Process           process
 * 
 * @return error stream of the process
 */
//==========================================================================================================================================
oC_Stream_t oC_Process_GetErrorStream( oC_Process_t Process )
{
    oC_Stream_t stream = NULL;

    if(oC_Process_IsCorrect(Process))
    {
        stream = Process->ErrorStream;
    }

    return stream;
}

//==========================================================================================================================================
/**
 * @brief Sets input stream of the process
 * 
 * The function sets input stream of the process
 * 
 * @param Process           process
 * @param Stream            input stream
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 *  #oC_ErrorCode_StreamNotCorrect     | Stream object is not correct
 *  #oC_ErrorCode_StreamTypeNotCorrect | Stream type is not input
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_SetInputStream( oC_Process_t Process , oC_Stream_t Stream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Process_IsCorrect(Process)                   , oC_ErrorCode_ProcessNotCorrect    )
     && ErrorCondition( oC_Stream_IsCorrect(Stream)                     , oC_ErrorCode_StreamNotCorrect     )
     && ErrorCondition( oC_Stream_IsType(Stream,oC_Stream_Type_Input)   , oC_ErrorCode_StreamTypeNotCorrect )
        )
    {
        errorCode            = oC_ErrorCode_None;
        Process->InputStream = Stream;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets output stream of the process
 * 
 * The function sets output stream of the process
 * 
 * @param Process           process
 * @param Stream            output stream
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 *  #oC_ErrorCode_StreamNotCorrect     | Stream object is not correct
 *  #oC_ErrorCode_StreamTypeNotCorrect | Stream type is not output
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_SetOutputStream( oC_Process_t Process , oC_Stream_t Stream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Process_IsCorrect(Process)                   , oC_ErrorCode_ProcessNotCorrect    )
     && ErrorCondition( oC_Stream_IsCorrect(Stream)                     , oC_ErrorCode_StreamNotCorrect     )
     && ErrorCondition( oC_Stream_IsType(Stream,oC_Stream_Type_Output)  , oC_ErrorCode_StreamTypeNotCorrect )
        )
    {
        errorCode            = oC_ErrorCode_None;
        Process->OutputStream= Stream;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets error stream of the process
 * 
 * The function sets error stream of the process
 * 
 * @param Process           process
 * @param Stream            error stream
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 *  #oC_ErrorCode_StreamNotCorrect     | Stream object is not correct
 *  #oC_ErrorCode_StreamTypeNotCorrect | Stream type is not output
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_SetErrorStream( oC_Process_t Process , oC_Stream_t Stream )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Process_IsCorrect(Process)                   , oC_ErrorCode_ProcessNotCorrect    )
     && ErrorCondition( oC_Stream_IsCorrect(Stream)                     , oC_ErrorCode_StreamNotCorrect     )
     && ErrorCondition( oC_Stream_IsType(Stream,oC_Stream_Type_Output)  , oC_ErrorCode_StreamTypeNotCorrect )
        )
    {
        errorCode            = oC_ErrorCode_None;
        Process->ErrorStream = Stream;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets priority of the process
 * 
 * The function sets priority of the process. 
 * 
 * @param Process           process
 * @param Priority          priority
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 * 
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_SetPriority( oC_Process_t Process , oC_Process_Priority_t Priority )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition( oC_Process_IsCorrect(Process) , oC_ErrorCode_ProcessNotCorrect    ))
    {
        Process->Priority   = Priority;
        errorCode           = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Gets priority of the process
 * 
 * The function gets priority of the process
 * 
 * @param Process           process
 * 
 * @return priority of the process
 */
//==========================================================================================================================================
oC_Process_Priority_t oC_Process_GetPriority( oC_Process_t Process )
{
    return oC_Process_IsCorrect(Process) ? Process->Priority : 0;
}

//==========================================================================================================================================
/**
 * @brief Locks the stdio buffer
 * 
 * The function locks the stdio buffer (this allows for synchronous access to the buffer). You should call the #oC_Process_UnlockStdioBuffer
 * function after you finish working with the buffer.
 * 
 * @param Process           process
 * @param outStdioBuffer    pointer to the stdio buffer
 * @param Timeout           timeout
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM| Address of the `outStdioBuffer` parameter is not in RAM section
 *  #oC_ErrorCode_TimeoutError         | Timeout error
 * 
 * @see #oC_Process_UnlockStdioBuffer
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_LockStdioBuffer( oC_Process_t Process , char ** outStdioBuffer , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Process_IsCorrect(Process)            , oC_ErrorCode_ProcessNotCorrect     ) 
     && ErrorCondition( oC_MemMan_IsRamAddress(outStdioBuffer)   , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        if(oC_Mutex_Take(Process->StdioMutex,Timeout))
        {
            *outStdioBuffer = Process->StdioBuffer;
            errorCode       = oC_ErrorCode_None;

            memset(Process->StdioBuffer,0,CFG_BYTES_STDIO_BUFFER_SIZE * sizeof(char));
        }
        else
        {
            errorCode = oC_ErrorCode_TimeoutError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unlocks the stdio buffer
 * 
 * The function unlocks the stdio buffer after you finish working with the buffer. You should call this function 
 * after you call the #oC_Process_LockStdioBuffer
 * 
 * @param Process           process
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 *  #oC_ErrorCode_ReleasingMutexError  | Releasing mutex error (the buffer is not locked)
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_UnlockStdioBuffer( oC_Process_t Process )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Process_IsCorrect(Process) ,oC_ErrorCode_ProcessNotCorrect))
    {
        if(oC_Mutex_Give(Process->StdioMutex))
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ReleasingMutexError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Checks if the process is active
 * 
 * The function checks if the process is active (The Threads of the process are registered in the Threads Manager)
 * 
 * @param Process           process
 * 
 * @return true if active
 */
//==========================================================================================================================================
bool oC_Process_IsActive( oC_Process_t Process )
{
    bool active = false;

    if(oC_Process_IsCorrect(Process))
    {
        oC_IntMan_EnterCriticalSection();
        oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

        foreach(threads,thread)
        {
            if(oC_Process_ContainsThread(Process,thread))
            {
                active = true;
                break;
            }
        }
        oC_IntMan_ExitCriticalSection();
    }

    return active;
}

//==========================================================================================================================================
/**
 * @brief Kills the process
 * 
 * The function kills the process
 * 
 * @param Process           process to kill
 * 
 * @return true if all threads of the process are killed
 */
//==========================================================================================================================================
bool oC_Process_Kill( oC_Process_t Process )
{
    bool allKilled = false;

    if(oC_Process_IsCorrect(Process))
    {
        oC_IntMan_EnterCriticalSection();
        Process->Killed = true;
        allKilled       = true;

        oC_ProcessMan_ActivateDeleteDeamon();

        oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

        if(threads)
        {
            oC_List_Foreach(threads,thread)
            {
                if(oC_Process_ContainsThread(Process,thread))
                {
                    if(oC_Thread_Cancel(&thread)==false)
                    {
                        allKilled = false;
                    }
                }
            }

            oC_List_Foreach(threads,thread)
            {
                if(oC_Process_ContainsThread(Process,thread))
                {
                    if(oC_Thread_IsCorrect(thread))
                    {
                        oC_List_RemoveAll(threads,thread);
                    }
                }
            }
        }
        oC_IntMan_ExitCriticalSection();

    }

    return allKilled;
}

//==========================================================================================================================================
/**
 * @brief returns allocator of the process
 * 
 * The function returns allocator of the process. To know more about the allocator see the #oC_Allocator_t structure
 * 
 * @param Process           process
 * 
 * @return allocator of the process
 */
//==========================================================================================================================================
Allocator_t oC_Process_GetAllocator( oC_Process_t Process )
{
    Allocator_t allocator = NULL;

    if(oC_Process_IsCorrect(Process))
    {
        allocator = &Process->Allocator;
    }

    return allocator;
}

//==========================================================================================================================================
/**
 * @brief returns heap map of the process
 * 
 * The function returns heap map of the process. To know more about the heap map see the #HeapMap_t structure
 * 
 * @param Process           process
 * 
 * @return heap map of the process
 * 
 * @see HeapMap_t
 */
//==========================================================================================================================================
oC_HeapMap_t oC_Process_GetHeapMap( oC_Process_t Process )
{
    oC_HeapMap_t heapMap = NULL;

    if(oC_Process_IsCorrect(Process))
    {
        heapMap = Process->HeapMap;
    }

    return heapMap;
}

//==========================================================================================================================================
/**
 * @brief Puts the process to sleep
 * 
 * The function puts the process to sleep. All the threads of the process are put to sleep unless they are already blocked.
 * 
 * @param Process           process
 * @param Timeout           timeout
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_Sleep( oC_Process_t Process , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Process_IsCorrect(Process) , oC_ErrorCode_ProcessNotCorrect))
    {
        errorCode = oC_ErrorCode_None;

        oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

        if(threads)
        {
            oC_List_Foreach(threads,thread)
            {
                oC_IntMan_EnterCriticalSection();
                if(oC_Process_ContainsThread(Process,thread))
                {
                    if(oC_Thread_IsBlocked(thread) == false)
                    {
                        oC_Thread_Sleep(thread,Timeout);
                    }
                }
                oC_IntMan_ExitCriticalSection();
            }
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief gets size of the heap map
 * 
 * The function gets size of the heap map of the process. 
 * 
 * @param Process           process
 * 
 * @return size of the heap map
 * 
 * @see HeapMap_t
 */
//==========================================================================================================================================
oC_UInt_t oC_Process_GetHeapMapSize( oC_Process_t Process )
{
    oC_UInt_t size = 0;

    if(oC_Process_IsCorrect(Process))
    {
        size = oC_MemMan_GetHeapMapSize(Process->HeapMap);
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief gets size of free memory in the heap map
 * 
 * The function gets size of free memory in the heap map of the process.
 * 
 * @param Process           process
 * 
 * @return size of free memory in the heap map
 */
//==========================================================================================================================================
oC_UInt_t oC_Process_GetFreeHeapMapSize( oC_Process_t Process )
{
    oC_UInt_t size = 0;

    if(oC_Process_IsCorrect(Process))
    {
        size = oC_MemMan_GetFreeHeapMapSize(Process->HeapMap);
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief Gets size of the threads' stack
 * 
 * The function gets size of the stack of all threads of the process
 * 
 * @param Process           process
 * 
 * @return size of the threads' stack
 */
//==========================================================================================================================================
oC_UInt_t oC_Process_GetThreadsStackSize( oC_Process_t Process )
{
    oC_UInt_t size = 0;

    if(oC_Process_IsCorrect(Process))
    {
        oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

        if(threads)
        {
            oC_List_Foreach(threads,thread)
            {
                if(oC_Process_ContainsThread(Process,thread))
                {
                    size += oC_Thread_GetStackSize(thread);
                }
            }
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief Gets size of free threads' stack
 * 
 * The function gets size of free stack of all threads of the process
 * 
 * @param Process           process
 * @param Current           if true the function returns the current free stack size (using the stack pointer), otherwise the minimum free stack size is returned.
 * 
 * @return size of the free threads' stack
 * 
 * @see oC_Thread_GetFreeStackSize
 */
//==========================================================================================================================================
oC_Int_t oC_Process_GetFreeThreadsStackSize(oC_Process_t Process , bool Current )
{
    oC_Int_t size = 0;

    if(oC_Process_IsCorrect(Process))
    {
        oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

        if(threads)
        {
            oC_List_Foreach(threads,thread)
            {
                if(oC_Process_ContainsThread(Process,thread))
                {
                    oC_Int_t threadFreeSize = oC_Thread_GetFreeStackSize(thread,Current);

                    if(threadFreeSize > 0 && size >= 0)
                    {
                        size += threadFreeSize;
                    }
                    else if(threadFreeSize < size)
                    {
                        size = threadFreeSize;
                    }
                }
            }
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief Gets the execution time of the process
 * 
 * The function gets the execution time of the process by summing the execution time of all threads of the process. 
 * 
 * @param Process           process
 * 
 * @return execution time of the process
 * 
 * @see oC_Thread_GetExecutionTime
 */
//==========================================================================================================================================
oC_Time_t oC_Process_GetExecutionTime( oC_Process_t Process )
{
    oC_Time_t executionTime = 0;

    if(oC_Process_IsCorrect(Process))
    {
        oC_List(oC_Thread_t) threads = oC_ThreadMan_GetList();

        if(threads)
        {
            oC_List_Foreach(threads,thread)
            {
                if(oC_Process_ContainsThread(Process,thread))
                {
                    executionTime += oC_Thread_GetExecutionTime(thread);
                }
            }
        }
    }

    return executionTime;
}

//==========================================================================================================================================
/**
 * @brief Gets the used RAM size of the process
 * 
 * The function gets the used RAM size of the process - it includes all allocations done for this process' allocator. 
 * 
 * @param Process           process
 * 
 * @return used RAM size of the process
 */
//==========================================================================================================================================
oC_UInt_t oC_Process_GetUsedRamSize( oC_Process_t Process )
{
    oC_UInt_t size = 0;

    if(oC_Process_IsCorrect(Process))
    {
        size = oC_MemMan_GetMemoryOfAllocatorSize(&Process->Allocator) + sizeof(struct Process_t);
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief gets IO flags of the process
 * 
 * The function gets IO flags of the process
 * 
 * @param Process           process
 * 
 * @return IO flags of the process
 * 
 * @see oC_IoFlags_t
 */
//==========================================================================================================================================
oC_IoFlags_t oC_Process_GetIoFlags( oC_Process_t Process )
{
    oC_IoFlags_t flags = oC_IoFlags_Default;

    if(oC_Process_IsCorrect(Process))
    {
        flags = Process->IoFlags;
    }

    return flags;
}

//==========================================================================================================================================
/**
 * @brief Sets IO flags of the process
 * 
 * The function sets IO flags of the process
 * 
 * @param Process           process
 * @param IoFlags           IO flags
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_SetIoFlags( oC_Process_t Process , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Process_IsCorrect(Process),oC_ErrorCode_ProcessNotCorrect))
    {
        Process->IoFlags = IoFlags;
        errorCode        = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Waits for the process to finish
 * 
 * The function waits for the process to finish. The function checks the state of the process every `CheckPeriod` seconds. 
 * If the process does not finish within the `Timeout` seconds, the function returns with the #oC_ErrorCode_Timeout error code.
 * 
 * @param Process           process
 * @param CheckPeriod       check period in milliseconds
 * @param Timeout           timeout in milliseconds
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 *  #oC_ErrorCode_Timeout              | Timeout error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_WaitForFinish( oC_Process_t Process , oC_Time_t CheckPeriod , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Process_IsCorrect(Process),oC_ErrorCode_ProcessNotCorrect))
    {
        errorCode                = oC_ErrorCode_None;
        oC_Process_State_t state = oC_Process_State_Run;
        oC_Time_t timeoutTime    = oC_KTime_GetTimestamp() + Timeout;
        while( state == oC_Process_State_Run || state == oC_Process_State_Asleep || state == oC_Process_State_Initialized )
        {
            sleep(CheckPeriod);

            state = oC_Process_GetState(Process);

            if(oC_KTime_GetTimestamp() >= timeoutTime)
            {
                errorCode = oC_ErrorCode_Timeout;
                break;
            }
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Gets PWD of the process
 * 
 * The function gets PWD of the process - the current working directory of the process
 * 
 * @param Process           process
 * 
 * @return PWD of the process
 */
//==========================================================================================================================================
char * oC_Process_GetPwd( oC_Process_t Process )
{
    static char defaultPwd[] = "/";
    char * pwd = defaultPwd;

    if(oC_Process_IsCorrect(Process))
    {
        pwd = Process->Pwd;
    }

    return pwd;
}

//==========================================================================================================================================
/**
 * @brief Sets PWD of the process
 * 
 * The function sets PWD of the process - the current working directory of the process
 * 
 * @param Process           process
 * @param Pwd               PWD
 * 
 * @return #oC_ErrorCode_None if success
 * 
 * Possible error codes:
 * 
 *  Error code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ProcessNotCorrect    | Process object is not correct
 *  #oC_ErrorCode_WrongAddress         | Wrong address
 *  #oC_ErrorCode_AllocationError      | Allocation error
 *  #oC_ErrorCode_DirectoryNotExists   | Directory does not exist
 *  #oC_ErrorCode_ReleaseError         | Error when releasing memory
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Process_SetPwd( oC_Process_t Process , const char * Pwd )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Process_IsCorrect(Process)   , oC_ErrorCode_ProcessNotCorrect    )
     && ErrorCondition( isaddresscorrect(Pwd)           , oC_ErrorCode_WrongAddress         )
        )
    {
        uint32_t requiredSize   = 0;
        char *   newRelativePwd = NULL;

        errorCode = oC_VirtualFileSystem_ConvertRelativeToAbsolute(Pwd,NULL,&requiredSize,true);

        if(!oC_ErrorOccur(errorCode))
        {
            newRelativePwd = smartalloc(requiredSize,AllocationFlags_CanWait1Second);

            if(ErrorCondition(newRelativePwd != NULL, oC_ErrorCode_AllocationError))
            {
                errorCode = oC_VirtualFileSystem_ConvertRelativeToAbsolute(Pwd,newRelativePwd,&requiredSize,true);

                if(!oC_ErrorOccur(errorCode))
                {
                    errorCode = oC_ErrorCode_None;

                    if(oC_VirtualFileSystem_DirExists(newRelativePwd))
                    {
                        if(oC_MemMan_IsDynamicAllocatedAddress(Process->Pwd))
                        {
                            if(smartfree(Process->Pwd,strlen(Process->Pwd)+1,AllocationFlags_CanWaitForever)==false)
                            {
                                errorCode = oC_ErrorCode_ReleaseError;
                            }
                        }
                        Process->Pwd                 = newRelativePwd;
                    }
                    else
                    {
                        if(smartfree(newRelativePwd,requiredSize,AllocationFlags_CanWait1Second) == false)
                        {
                            oC_SaveError("Process: Set pwd, cannot release newRelativePwd: " , oC_ErrorCode_ReleaseError);
                        }

                        errorCode = oC_ErrorCode_DirectoryNotExists;
                    }

                }
                else
                {
                    if(smartfree(newRelativePwd,requiredSize,AllocationFlags_CanWait1Second) == false)
                    {
                        oC_SaveError("Process: Set pwd, cannot release newRelativePwd: " , oC_ErrorCode_ReleaseError);
                    }
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Gets the PID of the process
 * 
 * The function gets the PID of the process
 * 
 * @param Process           process
 * 
 * @return PID of the process
 */
//==========================================================================================================================================
oC_UInt_t oC_Process_GetPid( oC_Process_t Process )
{
    oC_UInt_t pid = 0;

    if(oC_Process_IsCorrect(Process))
    {
        pid = Process->Pid;
    }

    return pid;
}

//==========================================================================================================================================
/**
 * @brief Sets allocation limit of the process
 * 
 * The function sets allocation limit of the process (the maximum amount of memory that can be allocated by the process)
 * 
 * @param Process           process
 * @param Limit             allocation limit
 * 
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Process_SetAllocationLimit( oC_Process_t Process , oC_MemorySize_t Limit )
{
    bool success = false;

    if(oC_Process_IsCorrect(Process))
    {
        Process->Allocator.Limit = Limit;
        success                  = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief enables/disables allocation tracking of the process
 * 
 * The function enables/disables allocation tracking of the process. If enabled, the process will log all memory allocation and release events.
 */
//==========================================================================================================================================
bool oC_Process_SetAllocationTracking( oC_Process_t Process, bool Enabled )
{
    bool success = false;

    if(oC_Process_IsCorrect(Process))
    {
        if(Enabled)
        {
            Process->Allocator.EventFlags |= MemoryEventFlags_MemoryAllocated | MemoryEventFlags_MemoryReleased;
        }
        else
        {
            Process->Allocator.EventFlags &= ~(MemoryEventFlags_MemoryAllocated | MemoryEventFlags_MemoryReleased);
        }
        success = true;
    }

    return success;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

static void MemoryFaultHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber )
{
    char tempString[150] = {0};

    sprintf(tempString, "Error in allocation of address '%p' allocated in function %s at line %d (%d bytes)\n", Address, Function, LineNumber, oC_MemMan_GetSizeOfAllocation(Address));

    if(Event & MemoryEventFlags_MemoryFault                  ) oC_ExcHan_LogEvent( "Memory fault flag. Reason is unknown                                                            " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );
    if(Event & MemoryEventFlags_BusFault                     ) oC_ExcHan_LogEvent( "Bus fault flag.                                                                                 " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );
    if(Event & MemoryEventFlags_AllocationError              ) oC_ExcHan_LogEvent( "Error while allocation of memory                                                                " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );
    if(Event & MemoryEventFlags_ReleaseError                 ) oC_ExcHan_LogEvent( "Error while releasing memory (address and allocation line number given in the event arguments)  " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );
    if(Event & MemoryEventFlags_PossibleMemoryLeak           ) oC_ExcHan_LogEvent( "There is a possibility of memory leak                                                           " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );
    if(Event & MemoryEventFlags_BufferOverflow               ) oC_ExcHan_LogEvent( "Allocated memory at the given address achieved 'red-zone'                                       " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );

    if(Event & MemoryEventFlags_MemoryAllocated              ) kdebuglog(oC_LogType_Track, "allocation - %s:%d (%d bytes)", Function,LineNumber, oC_MemMan_GetSizeOfAllocation(Address));
    if(Event & MemoryEventFlags_MemoryReleased               ) kdebuglog(oC_LogType_Track, "release    - %s:%d (%d bytes)", Function,LineNumber, oC_MemMan_GetSizeOfAllocation(Address));
    if(Event & MemoryEventFlags_RawMemoryAllocated           ) kdebuglog(oC_LogType_Track, "allocation - %s:%d (%d bytes)", Function,LineNumber, oC_MemMan_GetSizeOfAllocation(Address));
    if(Event & MemoryEventFlags_RawMemoryReleased            ) kdebuglog(oC_LogType_Track, "release    - %s:%d (%d bytes)", Function,LineNumber, oC_MemMan_GetSizeOfAllocation(Address));

    if(Event & MemoryEventFlags_ModuleTurningOff             ) oC_ExcHan_LogEvent( "The MemMan module is turning off                                                                " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );
    if(Event & MemoryEventFlags_DataSectionOverflow          ) oC_ExcHan_LogEvent( "Some buffer, that is in the data section was overwritten.                                       " , tempString, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAllocationError );

}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
