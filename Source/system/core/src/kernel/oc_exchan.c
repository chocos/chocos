/** ****************************************************************************************************************************************
 *
 * @brief      Source file of Exception Handler module
 *
 * @file       oc_exchan.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_exchan.h>
#include <oc_module.h>
#include <oc_sys_lld.h>
#include <oc_ktime.h>
#include <oc_userman.h>
#include <oc_debug.h>
#include <oc_process.h>
#include <oc_processman.h>
#include <oc_intman.h>
#include <oc_kprint.h>
#include <oc_streamman.h>
#include <oc_threadman.h>
#include <oc_clock_lld.h>
#include <oc_memman.h>
#include <oc_dynamic_config.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

//! The maximum number of logged events
#define MAX_LOGGED_EVENTS                   30

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    bool                        Used;
    const char *                EventName;
    char                        AdditionalInfo[150];
    oC_Timestamp_t              Timestamp;
    oC_Thread_t                 ExpectedThread;
    oC_Thread_t                 RealThread;
    bool                        AchievedRedZone;
    bool                        StackAddressCorrect;
    bool                        InterruptsEnabled;
    oC_ExcHan_ExceptionType_t   ExceptionType;
    bool                        ErrorInSystemContext;
    void *                      StackAddress;
    oC_Process_t                Process;
    oC_Int_t                    FreeStackBytes;
} EventLog_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static void         SystemEventHandler      ( oC_SYS_LLD_EventFlags_t EventFlags , oC_SYS_LLD_Context_t * Context , void * MemoryAddress , const char * Details );
static void         MemoryEventHandler      ( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber );
static void         LogEvent                ( const char * Name , char * AdditionalInfo , oC_Thread_t Thread , oC_SYS_LLD_Context_t * Context , oC_ExcHan_ExceptionType_t Type );
static void         DaemonThreadHandler     ( void * Context );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_SYS_LLD_EventFlags_t EnabledSystemEvents = oC_SYS_LLD_EventFlags_DivideBy0
                                                         | oC_SYS_LLD_EventFlags_HardFault
                                                         | oC_SYS_LLD_EventFlags_ProcessStackOverflow
                                                         | oC_SYS_LLD_EventFlags_UnalignedLoadOrStore
                                                         | oC_SYS_LLD_EventFlags_UndefinedInstruction
                                                         | oC_SYS_LLD_EventFlags_UsageFault;

static const oC_Allocator_t Allocator = {
                .Name = "ExcHan",
                .CorePwd = oC_MemMan_CORE_PWD
};

static EventLog_t           LoggedEvents[MAX_LOGGED_EVENTS]        = {{ 0 }};
static oC_Event_t           EventLogged                            = NULL;
static oC_Process_t         DaemonProcess                          = NULL;
static oC_Thread_t          DaemonThread                           = NULL;
uintptr_t                   __stack_chk_guard                      = 0xfdecbaba;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/**
 * @addtogroup ExcHan
 * @{
 */

//==========================================================================================================================================
/**
 * @brief turns on exception handler
 * 
 * The function turns on exception handler
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * Standard possible error codes:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleIsTurnedOn                | Module is already turned on
 *  oC_ErrorCode_AllocationError                 | Memory cannot be allocated
 *  oC_ErrorCode_ImplementError                  | Unexpected error
 * 
 * @note
 * More error codes can be returned from the #oC_SYS_LLD_TurnOnDriver, #oC_SYS_LLD_SetEventInterruptHandler, #oC_MemMan_TurnOn, #oC_MemMan_SetEventHandler functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ExcHan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_ExcHan))
    {
        errorCode = oC_SYS_LLD_TurnOnDriver();

        if(
            ( !oC_ErrorOccur(errorCode) || errorCode == oC_ErrorCode_ModuleIsTurnedOn               )
         && ErrorCode(oC_SYS_LLD_SetEventInterruptHandler(SystemEventHandler , EnabledSystemEvents ))
            )
        {
            errorCode = oC_MemMan_TurnOn();

            if(
                ( !oC_ErrorOccur(errorCode) || errorCode == oC_ErrorCode_ModuleIsTurnedOn )
             && ErrorCode( oC_MemMan_SetEventHandler( MemoryEventHandler ) )
                )
            {
                EventLogged         = NULL;
                DaemonProcess       = NULL;
                __stack_chk_guard   = 0xfeedbaba;
                oC_Module_TurnOn(oC_Module_ExcHan);
                errorCode       = oC_ErrorCode_None;
            }
        }

    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off exception handler
 * 
 * The function turns off exception handler
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * Standard possible error codes:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_ExcHan_TurnOn function
 *  oC_ErrorCode_ImplementError                  | Unexpected error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ExcHan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ExcHan))
    {
        oC_Module_TurnOff(oC_Module_ExcHan);
        errorCode = oC_ErrorCode_None;
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief prints logged events in the STD out
 *
 * The function is called periodically in the IDLE task to print logged events
 */
//==========================================================================================================================================
void oC_ExcHan_PrintLoggedEvents ( void )
{
    oC_ARRAY_FOREACH_IN_ARRAY(LoggedEvents,log)
    {
        oC_IntMan_EnterCriticalSection();
        if(log->Used)
        {
            char string[250];

            bzero(string,sizeof(string));

            oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForAllElements | oC_IoFlags_WriteToStdError, oC_VT100_FG_RED oC_VT100_BG_BLACK);

            kdebuglog(oC_LogType_Error, "ExcHan - %s: 0x%X", log->EventName, log->ExceptionType);

            sprintf(string, "EXCEPTION: %s: 0x%X\n", log->EventName, log->ExceptionType);

            oC_SYS_LLD_PrintToDebugger(string);

            if(log->ExceptionType & oC_ExcHan_ExceptionType_HardFault)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"\n--- HARD FAULT --- \n");

            }

            if(log->ExceptionType & oC_ExcHan_ExceptionType_KernelPanic)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"\n--- KERNEL PANIC --- \n");

            }

            if(log->ExceptionType & oC_ExcHan_ExceptionType_MemoryAccess)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"\n--- MEMORY ACCESS --- \n");

            }

            if(log->ExceptionType & oC_ExcHan_ExceptionType_MemoryAllocationError)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"\n--- MEMORY ALLOCATION ERROR --- \n");

            }

            if(log->ExceptionType & oC_ExcHan_ExceptionType_RequiredReboot)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"\n--- REQUIRE REBOOT --- \n");

            }

            if(log->ExceptionType & oC_ExcHan_ExceptionType_ProcessDamaged)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"\n--- PROCESS DAMAGED --- \n");

            }

            bzero(string,sizeof(string));

            oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"System Event [%f s]: %s\n", log->Timestamp, log->EventName);


            bzero(string,sizeof(string));

            oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"Additional info: %s\n", log->AdditionalInfo);




            bzero(string,sizeof(string));

            oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Harmful process: [%p] %s\n", log->Process, oC_Process_GetName(log->Process));



            bzero(string,sizeof(string));

            oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Expected thread: [%p] %s\n", log->ExpectedThread, oC_Thread_GetName(log->ExpectedThread));


            bzero(string,sizeof(string));

            oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Real thread: [%p] %s\n", log->RealThread, oC_Thread_GetName(log->RealThread));


            if(log->AchievedRedZone)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Thread has achieved the red zone - stack is exhausted (lack of %d bytes)\n",
                                 0 - oC_Thread_GetFreeStackSize( log->RealThread == NULL ? log->ExpectedThread : log->RealThread, true));
            }

            if(log->StackAddressCorrect == false)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Stack pointer (%p) is out of the thread stack! - lack of %d bytes\n", log->StackAddress, log->FreeStackBytes);
            }
            else
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Stack pointer (%p) is correct. Free stack: %d bytes\n", log->StackAddress, log->FreeStackBytes);
            }

            bzero(string,sizeof(string));

            oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Interrupts %s\n", log->InterruptsEnabled ? "enabled" : "disabled");

            if(log->ErrorInSystemContext)
            {
                bzero(string,sizeof(string));

                oC_KPrint_Printf(string,sizeof(string),oC_IoFlags_WaitForSomeElements | oC_IoFlags_WriteToStdError,"    Error occurs in the system context\n");
            }

            if(log->ExceptionType & oC_ExcHan_ExceptionType_RequiredReboot)
            {
                oC_Boot_Restart(oC_Boot_Reason_SystemException,oC_UserMan_GetRootUser());
            }

            log->Used = false;

        }
        oC_IntMan_ExitCriticalSection();
    }
}

//==========================================================================================================================================
/**
 * @brief logs event
 *
 * The function logs event
 *
 * @param Name              Name of the event (has to be in ROM or allocated on the heap)
 * @param AdditionalInfo    Additional information
 * @param Stack             Stack pointer
 * @param Thread            Thread
 * @param Type              Type of the exception
 */
//==========================================================================================================================================
void oC_ExcHan_LogEvent( const char * Name , char * AdditionalInfo , void * Stack , oC_Thread_t Thread , oC_ExcHan_ExceptionType_t Type )
{
    if(isaddresscorrect(Name))
    {
        LogEvent(Name,AdditionalInfo,Stack,Thread,Type);
    }
}

//==========================================================================================================================================
/**
 * @brief starts daemon of the Exception Handler
 *
 * The daemon prints logs with the highest system priority
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ExcHan_RunDaemon( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ExcHan))
    {
        DaemonProcess = oC_Process_New( oC_Process_Priority_SystemSecurityDeamon, "exc-han", oC_UserMan_GetRootUser(), 0, NULL, oC_StreamMan_GetStdErrorStream(), oC_StreamMan_GetStdErrorStream());

        if(ErrorCondition(DaemonProcess != NULL, oC_ErrorCode_AllocationError))
        {
            DaemonThread = oC_Thread_New(0, oC_DynamicConfig_GetValue(ExcHan, StackSize), DaemonProcess,"events handler", DaemonThreadHandler, NULL);

            if(ErrorCondition(DaemonThread != NULL , oC_ErrorCode_AllocationError))
            {
                EventLogged = oC_Event_New(oC_Event_State_Inactive, &Allocator, AllocationFlags_Default);

                if(
                    ErrorCondition  ( EventLogged != NULL, oC_ErrorCode_AllocationError)
                 && ErrorCode       ( oC_ProcessMan_AddProcess(DaemonProcess)                    )
                 && ErrorCondition  ( oC_Thread_Run(DaemonThread) , oC_ErrorCode_CannotRunThread )
                    )
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    oC_SaveIfFalse("ExcHan - cannot delete daemon thread"   , oC_Thread_Delete(&DaemonThread)                                               , oC_ErrorCode_ReleaseError);
                    oC_SaveIfFalse("ExcHan - cannot delete daemon process"  , oC_Process_Delete(&DaemonProcess)                                             , oC_ErrorCode_ReleaseError);
                    oC_SaveIfFalse("ExcHan - cannot delete event"           , EventLogged == NULL || oC_Event_Delete(&EventLogged,AllocationFlags_Default)  , oC_ErrorCode_ReleaseError);
                }
            }
            else
            {
                oC_SaveIfFalse("ExcHan - cannot delete daemon process", oC_Process_Delete(&DaemonProcess) , oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

/** @} */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief handler for system events
 */
//==========================================================================================================================================
static void SystemEventHandler( oC_SYS_LLD_EventFlags_t EventFlags , oC_SYS_LLD_Context_t * Context , void * MemoryAddress , const char * Details )
{
    if(EventFlags & oC_SYS_LLD_EventFlags_HardFault)
    {
        char string[60] = {0};

        sprintf(string, "Error during accessing address [%p]\n", MemoryAddress);

        LogEvent(Details ? Details: "Hard Fault", string, NULL, Context, oC_ExcHan_ExceptionType_HardFault);

        oC_ExcHan_PrintLoggedEvents();

        oC_CLOCK_LLD_DelayForMicroseconds(3*1000);

        oC_MCS_HALT(string);

        if(Context == NULL)
        {
            oC_Boot_Restart(oC_Boot_Reason_SystemException, oC_UserMan_GetRootUser());
        }
        else
        {
            oC_Process_t process = oC_ProcessMan_GetCurrentProcess();

            oC_Process_Kill( process );
            oC_ProcessMan_RemoveProcess(process);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief handler for memory manager events
 */
//==========================================================================================================================================
static void MemoryEventHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber )
{
    if(Event & MemoryEventFlags_ModuleTurningOff)
    {
        LogEvent("Memory Manager has been disabled", NULL, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAccess);
    }
    if(Event & MemoryEventFlags_DataSectionOverflow)
    {
        LogEvent("Overflow in the DATA ram section", NULL, NULL, NULL, oC_ExcHan_ExceptionType_KernelPanic);
    }
    if(Event & MemoryEventFlags_BufferOverflow)
    {
        Allocator_t allocator = oC_MemMan_GetAllocatorOfAddress(Address);

        char string[100] = {0};

        if(allocator != NULL)
        {
            sprintf(string, "Buffer overflow at address %p of allocator %s (allocated in %s at line %d)\n", Address, allocator->Name, Function, LineNumber);
        }
        else
        {
            sprintf(string, "Buffer overflow at address %p of unknown allocator\n", Address);
        }

        LogEvent("Overflow in the DATA ram section", string, NULL, NULL, oC_ExcHan_ExceptionType_ProcessDamaged);
    }
    if(Event & MemoryEventFlags_PanicMemoryExhausted)
    {
        static oC_Timestamp_t nextLogTimestamp = 0;

        if(nextLogTimestamp <= oC_KTime_GetTimestamp())
        {
            nextLogTimestamp = oC_KTime_GetTimestamp() + oC_DynamicConfig_GetValue(ExcHan,MemoryEventsLoggingPeriod);
            LogEvent("Panic memory limit exhausted", "Check your configuration in the command 'system'", NULL, NULL, oC_ExcHan_ExceptionType_KernelPanic);
        }
    }
    if(Event & MemoryEventFlags_MemoryExhausted)
    {
        static oC_Timestamp_t nextLogTimestamp = 0;

        if(nextLogTimestamp <= oC_KTime_GetTimestamp())
        {
            nextLogTimestamp = oC_KTime_GetTimestamp() + oC_DynamicConfig_GetValue(ExcHan,MemoryEventsLoggingPeriod);
            LogEvent("First memory limit exhausted", "Check your configuration in the command 'system'", NULL, NULL, oC_ExcHan_ExceptionType_MemoryAccess);
        }
    }
    if(Event & MemoryEventFlags_MemoryFault)
    {
        LogEvent("Memory access fault", NULL, NULL, NULL, oC_ExcHan_ExceptionType_MemoryAccess);
    }
    if(Event & MemoryEventFlags_BusFault)
    {
        LogEvent("Bus Fault", NULL, NULL, NULL, oC_ExcHan_ExceptionType_HardFault);

        oC_ExcHan_PrintLoggedEvents();

        oC_CLOCK_LLD_DelayForMicroseconds(3*1000);

        oC_Boot_Restart(oC_Boot_Reason_SystemException, oC_UserMan_GetRootUser());
    }
}

//==========================================================================================================================================
/**
 * @brief logs event
 */
//==========================================================================================================================================
static void LogEvent( const char * Name , char * AdditionalInfo , oC_Thread_t Thread , oC_SYS_LLD_Context_t * Context , oC_ExcHan_ExceptionType_t Type )
{
    bool interruptsTurnedOn = oC_IntMan_AreInterruptsTurnedOn();
    oC_IntMan_EnterCriticalSection();

    oC_Event_SetState(EventLogged, oC_Event_State_Active);

    oC_ARRAY_FOREACH_IN_ARRAY(LoggedEvents,log)
    {
        if(log->Used == false)
        {
            if(Context == NULL)
            {
                Context = oC_SYS_LLD_GetCurrentContext();
            }
            if(Thread == NULL)
            {
                Thread = oC_ThreadMan_GetCurrentThread();
            }

            void * stackPointer        = oC_SYS_LLD_GetLastProcessStackPointer();
            log->Used                  = true;
            log->StackAddress          = stackPointer;
            log->EventName             = Name;
            log->ExpectedThread        = oC_ThreadMan_GetCurrentThread();
            log->RealThread            = oC_ThreadMan_GetThreadOfContext( Context );
            log->Timestamp             = oC_KTime_GetTimestamp();
            log->InterruptsEnabled     = interruptsTurnedOn;
            log->ExceptionType         = Type;
            log->ErrorInSystemContext  = oC_SYS_LLD_GetSystemContext() == Context;

            if(AdditionalInfo)
            {
                strncpy(log->AdditionalInfo,AdditionalInfo,sizeof(log->AdditionalInfo));
                log->AdditionalInfo[sizeof(log->AdditionalInfo) - 1] = 0;
            }
            else
            {
                memset(log->AdditionalInfo,0,sizeof(log->AdditionalInfo));
            }

            if(log->ErrorInSystemContext == false)
            {
                log->Process           = oC_ProcessMan_GetCurrentProcess();
            }
            else
            {
                log->Process           = NULL;
            }

            if(oC_Thread_IsCorrect(log->RealThread))
            {
                log->StackAddressCorrect = oC_Thread_IsOwnedByStack(log->RealThread, stackPointer, &log->AchievedRedZone);
                log->FreeStackBytes      = oC_Thread_GetFreeStackSize(log->RealThread,true);
            }
            else
            {
                log->StackAddressCorrect = oC_Thread_IsOwnedByStack(log->ExpectedThread, stackPointer, &log->AchievedRedZone);
                log->FreeStackBytes      = oC_Thread_GetFreeStackSize(log->ExpectedThread,true);
            }
            break;
        }
    }

    oC_IntMan_ExitCriticalSection();
}

//==========================================================================================================================================
/**
 * @brief thread for printing logs
 */
//==========================================================================================================================================
static void DaemonThreadHandler( void * Context )
{
    while(1)
    {
        if(oC_Event_WaitForState(EventLogged, oC_Event_State_Active, oC_Event_StateMask_Full, min(20)))
        {
            oC_ExcHan_PrintLoggedEvents();

            oC_Event_SetState(EventLogged, oC_Event_State_Inactive);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief handler called when stack has been overflowed
 */
//==========================================================================================================================================
__attribute__((noreturn)) void __stack_chk_fail(void)
{
    oC_Thread_t thread      = oC_ThreadMan_GetCurrentThread();
    char        info[100]   = {0};

    sprintf(info, "Behavior of the thread `%s` [%p] was unexpected - closing", oC_Thread_GetName(thread), thread);

    LogEvent("Segmentation fault", info, thread, NULL, oC_ExcHan_ExceptionType_ProcessDamaged );

    if(__stack_chk_guard != 0)
    {
        oC_Thread_Cancel(&thread);
    }

    while(1);
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

