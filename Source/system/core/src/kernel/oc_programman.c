/** ****************************************************************************************************************************************
 *
 * @file       oc_programman.c
 *
 * @brief      The file with program list source
 *
 * @author     Patryk Kubiak - (Created on: 8 09 2015 20:27:54)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_programman.h>
#include <oc_programs_list.h>
#include <string.h>
#include <oc_processman.h>
#include <oc_debug.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool                     ModuleEnabledFlag   = false;
static oC_Program_t             DefaultProgram      = NULL;
static oC_List(oC_Program_t)    Programs            = NULL;
static const oC_Allocator_t     Allocator           = {
                .Name           = "program manager" ,
                .EventFlags     = 0 ,
                .EventHandler   = NULL,
                .CorePwd        = oC_MemMan_CORE_PWD
};

#define DONT_DECLARE(...)
#define DECLARE_PROGRAM_REGISTRATION(ProgramName)   \
    extern const oC_Program_Registration_t ProgramName##Registration;
    CFG_PROGRAMS_LIST(DECLARE_PROGRAM_REGISTRATION,DONT_DECLARE);
#undef DECLARE_PROGRAM_REGISTRATION
#undef DONT_DECLARE

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ProgramMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn))
    {
        DefaultProgram    = NULL;
        Programs          = oC_List_New(&Allocator,AllocationFlags_NoWait);

        if(Programs)
        {
            ModuleEnabledFlag = true;
            errorCode         = oC_ErrorCode_None;
#define DONT_ADD(...)
#define ADD_PROGRAM_TO_LIST(ProgramName)        oC_AssignErrorCode(&errorCode , oC_ProgramMan_AddProgram(&ProgramName##Registration));
            CFG_PROGRAMS_LIST(ADD_PROGRAM_TO_LIST,DONT_ADD)
#undef ADD_PROGRAM_TO_LIST
#undef DONT_ADD
#define GET_PROGRAM_REGISTRATION(ProgramName)   ProgramName##Registration
#define SET_DEFAULT_PROGRAM(ProgramName)        oC_AssignErrorCode(&errorCode,oC_ProgramMan_SetDefaultProgram(&GET_PROGRAM_REGISTRATION(ProgramName)))
            SET_DEFAULT_PROGRAM(CFG_PROGRAM_DEFAULT);
#undef SET_DEFAULT_PROGRAM
#undef GET_PROGRAM_REGISTRATION
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ProgramMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;

        if(oC_List_Delete(Programs,AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }

    }

    return errorCode;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ProgramMan_AddProgram( oC_Program_t Program )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        bool added = oC_List_PushBack(Programs,Program,&Allocator);

        if(added)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotAddObjectToList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ProgramMan_RemoveProgram( oC_Program_t Program )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        bool contains = oC_List_Contains(Programs,Program);

        if(contains)
        {
            bool removed = oC_List_RemoveAll(Programs,Program);

            if(removed)
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_CannotRemoveObjectFromList;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_ObjectNotFoundOnList;
        }
    }

    return errorCode;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ProgramMan_SetDefaultProgram( oC_Program_t Program )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true ,     oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Program_IsCorrect(Program) , oC_ErrorCode_ObjectNotCorrect)
        )
    {
        DefaultProgram = Program;
        errorCode      = oC_ErrorCode_None;
    }

    return errorCode;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_Program_t) oC_ProgramMan_GetList( void )
{
    oC_List(oC_Program_t) programs = NULL;

    if(ModuleEnabledFlag)
    {
        programs = Programs;
    }

    return programs;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_Program_t oC_ProgramMan_GetDefaultProgram( void )
{
    oC_Program_t program = NULL;

    if(ModuleEnabledFlag)
    {
        program = DefaultProgram;
    }

    return program;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Program_t oC_ProgramMan_GetTerminalProgram( void )
{
    oC_Program_t program = NULL;

    if(ModuleEnabledFlag)
    {
        program = DefaultProgram;
    }

    return program;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Program_t oC_ProgramMan_GetProgram( const char * Name )
{
    oC_Program_t program = NULL;

    if(ModuleEnabledFlag)
    {
        oC_List_Foreach(Programs,prog)
        {
            if(strcmp(oC_Program_GetName(prog),Name) == 0)
            {
                program = prog;
                break;
            }
        }
    }

    return program;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ProgramMan_RunDefaultProgram( oC_User_t User )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true ,            oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Program_IsCorrect(DefaultProgram) , oC_ErrorCode_DefaultProgramNotSet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_User_IsCorrect(User) ,              oC_ErrorCode_WrongUser)
        )
    {
        oC_Process_t process = oC_Program_NewProcess(DefaultProgram,User);

        if(oC_AssignErrorCodeIfFalse(&errorCode , oC_Process_IsCorrect(process) , oC_ErrorCode_CannotCreateProcess))
        {
            if(
                oC_AssignErrorCode(&errorCode , oC_Process_SetPriority  ( process , oC_Process_Priority_DefaultProcess  ))
             && oC_AssignErrorCode(&errorCode , oC_ProcessMan_AddProcess( process                                       ))
                )
            {
                const char * name = oC_Program_GetName(DefaultProgram);
                errorCode = oC_Program_Exec(DefaultProgram,process,1,&name,NULL,NULL);
            }
            else
            {
                if(oC_Process_Delete(&process))
                {
                    oC_SaveError("RunDefaultProgram: cannot delete process" , oC_ErrorCode_ReleaseError);
                }
            }
        }
    }


    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
uint32_t oC_ProgramMan_RerunDefualtIfNotRun( oC_User_t User )
{
    static uint32_t rerunNumber    = 0;
    oC_Program_t    defaultProgram = oC_ProgramMan_GetDefaultProgram();

    if(isaddresscorrect(defaultProgram))
    {
        oC_Process_t        defaultProcess = oC_ProcessMan_GetProcess(oC_Program_GetName(defaultProgram));
        oC_Process_State_t  state          = oC_Process_GetState(defaultProcess);

        if(state == oC_Process_State_Invalid || state == oC_Process_State_Killed || state == oC_Process_State_Zombie)
        {
            oC_ErrorCode_t errorCode = oC_ProgramMan_RunDefaultProgram(User);

            if(oC_ErrorOccur(errorCode))
            {
                kdebuglog(oC_LogType_Error , "rerundefaultprogram: Cannot run default program %s" , oC_Program_GetName(defaultProgram));
                oC_SaveError("programman: rerun of default program failed!: " , errorCode);
            }
            else
            {
                rerunNumber++;
            }
        }

    }
    else
    {
        oC_SaveError("ProgramMan: Default program is not correct!" , oC_ErrorCode_WrongAddress);
        kdebuglog(oC_LogType_Error , "defaultprogram: the pointer of default program is not correct (0x%x)!" , defaultProgram);
    }

    return rerunNumber;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________


