/** ****************************************************************************************************************************************
 *
 * @file       oc_userman.c
 *
 * @brief      The file with interface for user manager module
 *
 * @author     Patryk Kubiak - (Created on: 13 09 2015 19:45:17)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_userman.h>
#include <oc_null.h>
#include <oc_system_cfg.h>
#include <string.h>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool                 ModuleEnabledFlag = false;
static oC_List(oC_User_t)   Users             = NULL;
static const oC_Allocator_t Allocator         = {
                .Name = "user manager" ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_UserMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn))
    {
        Users = oC_List_New(&Allocator,AllocationFlags_NoWait);

        if(Users)
        {
            ModuleEnabledFlag = true;
            errorCode         = oC_ErrorCode_None;
            oC_User_t root    = oC_User_New(&Allocator , AllocationFlags_NoWait , "root" , CFG_STRING_ROOT_PASSWORD , oC_User_Permissions_Root );

            if(root)
            {
                bool added    = oC_List_PushBack(Users,root,&Allocator);
                oC_AssignErrorCodeIfFalse(&errorCode,added,oC_ErrorCode_CannotAddObjectToList);
            }
            else
            {
                errorCode = oC_ErrorCode_AllocationError;
            }

        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_UserMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;
        errorCode         = oC_ErrorCode_None;

        oC_List_Foreach(Users,user)
        {
            if(oC_User_Delete(&user)==false)
            {
                errorCode = oC_ErrorCode_CannotDeleteObject;
            }
        }
        if(oC_List_Delete(Users,AllocationFlags_CanWaitForever) == false)
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_UserMan_AddUser( oC_User_t User )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ModuleEnabledFlag == true                                  ,   oC_ErrorCode_ModuleNotStartedYet) &&
        ErrorCondition( oC_User_IsCorrect(User)                                    ,   oC_ErrorCode_ObjectNotCorrect   ) &&
        ErrorCondition( !List_Contains((oC_List_t)Users, &User, sizeof(oC_User_t)) ,   oC_ErrorCode_UserExists         ) &&
        ErrorCondition( iscurroot() || !oC_User_IsRoot(User)                       ,   oC_ErrorCode_PermissionDenied   ) &&
        ErrorCondition( 0 != strcmp(oC_User_GetName(User) , "unknown")             ,   oC_ErrorCode_InvalidUserName    )
        )
    {
        bool added = oC_List_PushBack(Users, User, &Allocator);

        if(added)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotAddObjectToList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_UserMan_CreateAndAddUser( const char * Name, const char * Password, oC_User_Permissions_t Permissions )
{
    oC_User_t      user = oC_User_New(&Allocator , AllocationFlags_CanWait1Second , Name , Password , Permissions );
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( user == NULL )
    {
        errorCode = oC_ErrorCode_WrongParameters;
    }
    else
    {
        errorCode = oC_UserMan_AddUser(user);
    }

    if(oC_ErrorOccur(errorCode) && user != NULL)
    {
        oC_User_Delete(&user);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_UserMan_RemoveUser( oC_User_t User )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ModuleEnabledFlag == true                                               , oC_ErrorCode_ModuleNotStartedYet) &&
        ErrorCondition( oC_User_IsCorrect(User)                                                 , oC_ErrorCode_ObjectNotCorrect)    &&
        ErrorCondition( User != getcuruser()                                                    , oC_ErrorCode_UserInUse)           &&
        ErrorCondition( !oC_User_IsRoot(User)                                                   , oC_ErrorCode_CannotRemoveRoot)    &&
        ErrorCondition( oC_User_CheckPermissions(getcuruser(), oC_User_GetPermissions(User))    , oC_ErrorCode_PermissionDenied)    &&
        ErrorCondition( List_Contains((oC_List_t)Users, &User, sizeof(oC_User_t))               , oC_ErrorCode_UserNotExists)
        )
    {
        bool removed = oC_List_RemoveAll(Users,User);

        if(removed)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotRemoveObjectFromList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_User_t) oC_UserMan_GetList( void )
{
    return Users;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_User_t oC_UserMan_GetRootUser( void )
{
    oC_User_t root = NULL;

    if(ModuleEnabledFlag)
    {
        oC_List_Foreach(Users,user)
        {
            if(oC_User_IsRoot(user))
            {
                root = user;
            }
        }
    }

    return root;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_User_t oC_UserMan_GetUser( const char * Name )
{
    oC_User_t userToReturn = NULL;

    if(ModuleEnabledFlag && isaddresscorrect(Name))
    {
        oC_List_Foreach(Users,user)
        {
            if(strcmp(oC_User_GetName(user),Name)==0)
            {
                userToReturn = user;
                break;
            }
        }
    }

    return userToReturn;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_UserMan_Rename( oC_User_t User , const char * NewName )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

   if(
       ErrorCondition( ModuleEnabledFlag == true                                            , oC_ErrorCode_ModuleNotStartedYet) &&
       ErrorCondition( oC_User_IsCorrect(User)                                              , oC_ErrorCode_ObjectNotCorrect)    &&
       ErrorCondition( User != getcuruser()                                                 , oC_ErrorCode_UserInUse)           &&
       ErrorCondition( !oC_User_IsRoot(User)                                                , oC_ErrorCode_PermissionDenied)    &&
       ErrorCondition( oC_User_CheckPermissions(getcuruser(), oC_User_GetPermissions(User)) , oC_ErrorCode_PermissionDenied )   &&
       ErrorCondition( List_Contains((oC_List_t)Users, &User, sizeof(oC_User_t))            , oC_ErrorCode_UserNotExists)       &&
       ErrorCondition( isaddresscorrect(NewName)                                            , oC_ErrorCode_InvalidUserName)     &&
       ErrorCondition( 0 != strcmp(oC_User_GetName(User), "root")                           , oC_ErrorCode_PermissionDenied)    &&
       ErrorCondition( 0 != strcmp(NewName              , "root")                           , oC_ErrorCode_PermissionDenied)    &&
       ErrorCondition( 0 != strcmp(NewName              , "unknown")                        , oC_ErrorCode_InvalidUserName)
       )
       {
           if(oC_User_Rename(User, NewName))
           {
               errorCode = oC_ErrorCode_None;
           }
           else
           {
               errorCode = oC_ErrorCode_CannotRenameUser;
           }
       }

    return errorCode;
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
