/** ****************************************************************************************************************************************
 *
 * @file       oc_mutex.c
 *
 * @brief      The file with source for mutex object functions
 *
 * @author     Patryk Kubiak - (Created on: 31 08 2015 18:59:41)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_mutex.h>
#include <oc_thread.h>
#include <oc_object.h>
#include <oc_stdlib.h>
#include <oc_thread.h>
#include <oc_intman.h>
#include <oc_threadman.h>
#include <oc_null.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION_______________________________________________________________________

struct Mutex_t
{
    oC_ObjectControl_t      ObjectControl;
    int16_t                 RecursiveCount;
    oC_Mutex_Type_t         Type;
    uint32_t                LockedFlag;
    oC_Thread_t             BlockedThread;
};

#undef  _________________________________________LOCAL_TYPES_SECTION_______________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool RevertFunction( oC_Thread_t Thread, void * Mutex, uint32_t Parameter );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup Mutex
//! @{

//==========================================================================================================================================
/**
 * @brief Creates a new mutex
 * 
 * The function creates a new mutex
 * 
 * @param Type              type of mutex
 * @param Allocator         allocator
 * @param AllocationFlags   allocation flags
 * 
 * @return mutex
 */
//==========================================================================================================================================
oC_Mutex_t oC_Mutex_New( oC_Mutex_Type_t Type , Allocator_t Allocator , AllocationFlags_t AllocationFlags )
{
    oC_Mutex_t mutex = kmalloc(sizeof(struct Mutex_t), Allocator , AllocationFlags_NoWait | AllocationFlags_ZeroFill);

    if(mutex)
    {
        mutex->LockedFlag        = false;
        mutex->BlockedThread     = NULL;
        mutex->Type              = Type;
        mutex->ObjectControl     = oC_CountObjectControl(mutex,oC_ObjectId_Mutex);
    }

    return mutex;
}

//==========================================================================================================================================
/**
 * @brief Deletes the mutex
 * 
 * The function deletes the mutex
 * 
 * @param Mutex             mutex
 * @param AllocationFlags   allocation flags
 * 
 * @return true if deleted
 */
//==========================================================================================================================================
bool oC_Mutex_Delete( oC_Mutex_t * Mutex , AllocationFlags_t AllocationFlags )
{
    bool deleted = false;

    if(oC_Mutex_IsCorrect(*Mutex))
    {
        oC_IntMan_EnterCriticalSection();
        (*Mutex)->ObjectControl = 0;
        oC_ThreadMan_UnblockAllBlockedBy(&((*Mutex)->LockedFlag),false);
        if(!kfree(*Mutex,AllocationFlags))
        {
            oC_SaveError("Mutex" , oC_ErrorCode_ReleaseError);
        }
        else
        {
            *Mutex  = NULL;
            deleted = true;
        }
        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        oC_SaveError("Mutex" , oC_ErrorCode_ObjectNotCorrect);
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief Gives the mutex
 * 
 * The function gives the mutex
 * 
 * @param Mutex             mutex
 * 
 * @return true if given
 */
//==========================================================================================================================================
bool oC_Mutex_Give( oC_Mutex_t Mutex )
{
    bool given = false;

    if(oC_Mutex_IsCorrect(Mutex))
    {
        if(oC_ThreadMan_GetCurrentThread() == Mutex->BlockedThread)
        {
            oC_IntMan_EnterCriticalSection();
            Mutex->RecursiveCount++;
            if(
                (Mutex->Type == oC_Mutex_Type_NonRecursive) ||
                (Mutex->RecursiveCount >= 0)
                )
            {
                oC_Thread_RemoveFromRevert(getcurthread(),RevertFunction, Mutex);
                Mutex->LockedFlag = false;
            }
            given = true;
            oC_IntMan_ExitCriticalSection();
        }
        else
        {
            oC_SaveError("Mutex" , oC_ErrorCode_WrongThread);
        }
    }
    else
    {
        oC_SaveError("Mutex" , oC_ErrorCode_ObjectNotCorrect);
    }

    return given;
}

//==========================================================================================================================================
/**
 * @brief Takes the mutex
 * 
 * The function takes the mutex
 * 
 * @param Mutex             mutex
 * @param Timeout           timeout
 * 
 * @return true if taken
 */
//==========================================================================================================================================
bool oC_Mutex_Take( oC_Mutex_t Mutex , oC_Time_t Timeout )
{
    bool taken = false;

    if(oC_Mutex_IsCorrect(Mutex))
    {
        oC_Thread_t currentThread = oC_ThreadMan_GetCurrentThread();
        if(Mutex->LockedFlag)
        {
            if(currentThread != Mutex->BlockedThread)
            {
                if( oC_Thread_SetBlocked(currentThread , &Mutex->LockedFlag , oC_Thread_Unblock_WhenEqual , false , oC_Thread_UnblockMask_All , Timeout ) )
                {
                    while(oC_Thread_IsBlocked(currentThread));

                    oC_Thread_SetUnblocked(currentThread);

                    oC_IntMan_EnterCriticalSection();
                    if(oC_Mutex_IsCorrect(Mutex) && (Mutex->LockedFlag == 0))
                    {
                        Mutex->LockedFlag    = true;
                        Mutex->BlockedThread = currentThread;
                        Mutex->RecursiveCount--;
                        taken                = true;

                        oC_Thread_SaveToRevert(currentThread,RevertFunction,Mutex,0);
                    }
                    oC_IntMan_ExitCriticalSection();
                }
            }
            else
            {
                oC_IntMan_EnterCriticalSection();
                Mutex->RecursiveCount--;
                taken                = true;
                oC_IntMan_ExitCriticalSection();
            }
        }
        else
        {
            oC_IntMan_EnterCriticalSection();
            Mutex->LockedFlag    = true;
            Mutex->BlockedThread = currentThread;
            Mutex->RecursiveCount--;
            taken                = true;

            oC_Thread_SaveToRevert(currentThread,RevertFunction,Mutex,0);
            oC_IntMan_ExitCriticalSection();
        }
    }
    else
    {
        oC_SaveError("Mutex" , oC_ErrorCode_ObjectNotCorrect);
    }

    return taken;
}

//==========================================================================================================================================
/**
 * @brief Checks if the mutex is taken by the current thread
 * 
 * The function checks if the mutex is taken by the current thread
 * 
 * @param Mutex             mutex
 * 
 * @return true if taken
 */
//==========================================================================================================================================
bool oC_Mutex_IsTakenByCurrentThread( oC_Mutex_t Mutex )
{
    return oC_Mutex_IsCorrect(Mutex) && Mutex->LockedFlag == true && getcurthread() == Mutex->BlockedThread;
}

//==========================================================================================================================================
/**
 * @brief Checks if the mutex is correct
 * 
 * The function checks if the mutex is correct
 * 
 * @param Mutex             mutex
 * 
 * @return true if correct
 */
//==========================================================================================================================================
bool oC_Mutex_IsCorrect( oC_Mutex_t Mutex )
{
    return oC_MemMan_IsRamAddress(Mutex) && oC_CheckObjectControl(Mutex , oC_ObjectId_Mutex , Mutex->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief Gets the name of the blocked thread
 * 
 * The function gets the name of the blocked thread
 * 
 * @param Mutex             mutex
 * 
 * @return name of the blocked thread
 */
//==========================================================================================================================================
const char* oC_Mutex_GetBlockedName( oC_Mutex_t Mutex )
{
    const char* name = "invalid mutex";
    if(oC_Mutex_IsCorrect(Mutex))
    {
        if(Mutex->LockedFlag)
        {
            if(Mutex->BlockedThread)
            {
                name = oC_Thread_GetName(Mutex->BlockedThread);
            }
            else
            {
                name = "thread is null";
            }
        }
        else
        {
            name = "mutex is not locked";
        }
    }
    return name;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief called to revert changes made by the thread
 * 
 * @param Thread            thread that made changes
 * @param Mutex             mutex to revert
 * @param Parameter         parameter (not used)
 * 
 * @return true if reverted
 */
//==========================================================================================================================================
static bool RevertFunction( oC_Thread_t Thread, void * Mutex, uint32_t Parameter )
{
    bool        reverted = false;
    oC_Mutex_t  mutex    = Mutex;

    oC_IntMan_EnterCriticalSection();
    if(oC_SaveIfFalse("Mutex", oC_Mutex_IsCorrect(mutex), oC_ErrorCode_ObjectNotCorrect))
    {
        mutex->BlockedThread    = NULL;
        mutex->LockedFlag       = false;
        mutex->RecursiveCount   = 0;
        reverted = true;
    }
    oC_IntMan_ExitCriticalSection();

    return reverted;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

