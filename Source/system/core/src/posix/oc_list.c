/** ****************************************************************************************************************************************
 *
 * @file       oc_list.c
 *
 * @brief      The file with source for the list library
 *
 * @author     Patryk Kubiak - (Created on: 2 09 2015 18:22:13)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_list.h>
#include <oc_math.h>
#include <string.h>
#include <oc_null.h>
#include <oc_stdlib.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void PushToEmpty( oC_List_t List , oC_List_ElementHandle_t Handle );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/** @brief      The function creates new list
 *
 *  @param      Allocator   The allocator for the list
 *  @param      Flags       The allocation flags
 *  @param      ...         The arguments for the allocator
 *
 *  @return     The pointer to the list
 */
//==========================================================================================================================================
oC_List_t List_New( Allocator_t Allocator , AllocationFlags_t Flags , ...)
{
    oC_List_t list = NULL;

    list = kmalloc(sizeof(struct List_t) , Allocator , Flags );

    if(list)
    {
        list->ObjectControl = oC_CountObjectControl(list,oC_ObjectId_List);
        list->Count         = 0;
        list->First         = NULL;
        list->Last          = NULL;
    }

    return list;
}


//==========================================================================================================================================
/**
 * @brief The function verifies that the list is valid
 * 
 * @param List The list to verify
 * 
 * @return true if the list is valid
 */
//==========================================================================================================================================
bool List_IsCorrect( oC_List_t List )
{
    return isram(List) && oC_CheckObjectControl(List,oC_ObjectId_List,List->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief The function deletes the list and releases the memory allocated for it
 * 
 * @param List  The list to delete
 * @param Flags The allocation flags
 * 
 * @return true if the list was deleted
 */
//==========================================================================================================================================
bool List_Delete( oC_List_t * List , AllocationFlags_t Flags )
{
    bool deleted = false;

    if(List_IsCorrect(*List))
    {
        if(List_Clear(*List))
        {
            (*List)->ObjectControl = 0;

            if(kfree(*List,Flags))
            {
                *List   = NULL;
                deleted = true;
            }
            else
            {
                oC_SaveError("List-delete: cannot release memory" , oC_ErrorCode_ReleaseError);
            }
        }
        else
        {
            oC_SaveError("List-delete: cannot clear the list" , oC_ErrorCode_ReleaseError);
        }
    }


    return deleted;
}

//==========================================================================================================================================
/**
 * @brief The function deletes element from the list
 * 
 * @param List      The list from which the element will be deleted
 * @param Element   The element to delete
 * 
 * @return true if the element was deleted
 */
//==========================================================================================================================================
bool List_DeleteElement( oC_List_t List, oC_List_ElementHandle_t Element )
{
    bool success = false;

    if(isram(Element))
    {
        if ( List_IsCorrect(List) && isaddresscorrect(List->DestructorCallback) )
        {
            List->DestructorCallback(Element->Object);
        }
        success = kfree(Element->Object,AllocationFlags_CanWaitForever) && kfree(Element,AllocationFlags_CanWaitForever);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief The function allocates memory for a new element and copies the object to it
 * 
 * @param Object    The object to copy
 * @param Size      The size of the object
 * @param Allocator The allocator for the element
 * 
 * @return The pointer to the new element
 */
//==========================================================================================================================================
oC_List_ElementHandle_t List_NewElement( const void * Object , oC_UInt_t Size , Allocator_t Allocator )
{
    oC_List_ElementHandle_t handle = NULL;

    if(isaddresscorrect(Object))
    {
        handle = kmalloc(sizeof(struct List_ElementHandle_t),Allocator,AllocationFlags_CanWaitForever);

        if(handle)
        {
            handle->Object   = kmalloc(Size, Allocator , AllocationFlags_CanWaitForever);
            memcpy(handle->Object,Object,Size);
            handle->Size     = Size;
            handle->Next     = NULL;
            handle->Previous = NULL;
        }
    }

    return handle;
}

//==========================================================================================================================================
/**
 * @brief The function attaches a destructor to the list
 *
 * The destructor callback will be called whenever an element is deleted on the list
 *
 * @param List          The list to attach
 * @param Destructor    The destructor to attach
 *
 * @return true if the destructor was attached
 */
//==========================================================================================================================================
bool List_AttachDestructor( oC_List_t List, oC_List_DestructorCallback_t Destructor )
{
    bool success = false;

    if ( List_IsCorrect( List ) && isaddresscorrect(Destructor) )
    {
        List->DestructorCallback = Destructor;
        success = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief The function verifies that the list contains the object
 * 
 * @param List      The list to verify
 * @param Object    The object to verify
 * 
 * @return true if the list contains the object
 */
//==========================================================================================================================================
bool List_Contains( oC_List_t List , void * Object , oC_UInt_t Size )
{
    return List_CountObjects(List,Object,Size) > 0;
}

//==========================================================================================================================================
/**
 * @brief The function counts the number of objects in the list
 * 
 * @param List      The list to count
 * 
 * @return The number of objects in the list
 */ 
//==========================================================================================================================================
uint32_t List_Count( oC_List_t List )
{
    uint32_t count = 0;

    if(List_IsCorrect(List))
    {
        count = List->Count;
    }

    return count;
}

//==========================================================================================================================================
/**
 * @brief The function clears the list
 * 
 * @param List  The list to clear
 * 
 * @return true if the list was cleared
 */
//==========================================================================================================================================
bool List_Clear( oC_List_t List )
{
    bool success = false;

    if(List_IsCorrect(List))
    {
        success = true;
        oC_List_ElementHandle_t element     = List->Last;
        oC_List_ElementHandle_t previous    = element->Previous;

        while(List_DeleteElement(List, element))
        {
            element     = previous;
            previous    = previous->Previous;
            List->Count--;
        }

        List->First = NULL;
        List->Last  = NULL;
        List->Count = 0;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief The function counts the number of objects in the list
 * 
 * @param List      The list to count
 * @param Object    The object to count
 * @param Size      The size of the object
 * 
 * @return The number of objects in the list
 */
//==========================================================================================================================================
uint32_t List_CountObjects( oC_List_t List , void * Object , oC_UInt_t Size )
{
    uint32_t count = 0;

    if(List_IsCorrect(List) && (Size > 0) && (isaddresscorrect(Object)))
    {
        for(oC_List_ElementHandle_t element = List->First ; element != NULL ; element = element->Next )
        {
            if((Size == element->Size) && (memcmp(Object,element->Object,element->Size) == 0))
            {
               count++;
            }
        }
    }

    return count;
}

//==========================================================================================================================================
/**
 * @brief The function returns the index of the object in the list
 * 
 * @param List      The list to search
 * @param Object    The object to search
 * @param Size      The size of the object
 * 
 * @return The index of the object in the list
 */
//==========================================================================================================================================
int List_IndexOf( oC_List_t List , void * Object , oC_UInt_t Size )
{
    int index = -1;

    if(List_IsCorrect(List) && (Size > 0) && isaddresscorrect(Object))
    {
        int currentIndex = 0;
        for(oC_List_ElementHandle_t element = List->First ; element != NULL ; element = element->Next )
        {
            if((Size == element->Size) && (memcmp(Object,element->Object,element->Size) == 0))
            {
                index = currentIndex;
                break;
            }
            currentIndex++;
        }
    }

    return index;
}

//==========================================================================================================================================
/**
 * @brief The function returns the index of the element in the list
 * 
 * @param List              The list to search
 * @param ElementHandle     The element to search
 * 
 * @return The index of the element in the list
 */
//==========================================================================================================================================
int List_IndexOfElement( oC_List_t List , oC_List_ElementHandle_t ElementHandle )
{
    int index = -1;

    if(List_IsCorrect(List))
    {
        int currentIndex = 0;
        for(oC_List_ElementHandle_t element = List->First ; element != NULL ; element = element->Next )
        {
            if(ElementHandle == element)
            {
                index = currentIndex;
                break;
            }
            currentIndex++;
        }
    }

    return index;
}

//==========================================================================================================================================
/**
 * @brief The function returns the element of the object in the list
 * 
 * @param List      The list to search
 * @param Object    The object to search
 * @param Size      The size of the object
 * 
 * @return The element of the object in the list
 */
//==========================================================================================================================================
oC_List_ElementHandle_t List_ElementHandleOf( oC_List_t List , void * Object , oC_UInt_t Size )
{
    oC_List_ElementHandle_t handle = NULL;

    if(List_IsCorrect(List) && (Size > 0) && isaddresscorrect(Object))
    {
        for(oC_List_ElementHandle_t element = List->First ; element != NULL ; element = element->Next )
        {
            if((Size == element->Size) && (memcmp(Object,element->Object,element->Size) == 0))
            {
                handle = element;
                break;
            }
        }
    }

    return handle;
}

//==========================================================================================================================================
/**
 * @brief The function returns the element of the index in the list
 * 
 * @param List      The list to search
 * @param Index     The index to search
 * 
 * @return The element of the index in the list
 */
//==========================================================================================================================================
oC_List_ElementHandle_t List_ElementOfIndex( oC_List_t List , int Index )
{
    oC_List_ElementHandle_t handle = NULL;

    if(List_IsCorrect(List) && (Index >= 0))
    {
        int currentIndex = 0;
        for(oC_List_ElementHandle_t element = List->First ; element != NULL ; element = element->Next , currentIndex++ )
        {
            if(currentIndex == Index)
            {
                handle = element;
                break;
            }
        }
    }

    return handle;
}

//==========================================================================================================================================
/**
 * @brief The function pushes back an object to the list
 * 
 * @param List          The list to push back
 * @param Object        The object to push back
 * @param Size          The size of the object
 * @param Allocator     The allocator to use
 * 
 * @return true if the object was pushed back
 */
//==========================================================================================================================================
bool List_PushBack( oC_List_t List , const void * Object , oC_UInt_t Size , Allocator_t Allocator)
{
    bool pushed = false;

    if(List_IsCorrect(List) && (Size > 0) && isaddresscorrect(Object))
    {
        oC_List_ElementHandle_t element = List_NewElement(Object,Size,Allocator);

        if(element)
        {
            if(List->Last && List->First)
            {
                element->Previous   = List->Last;
                element->Next       = NULL;
                List->Last->Next    = element;
                List->Last          = element;
            }
            else
            {
                PushToEmpty(List,element);
            }

            pushed = true;
            List->Count++;
        }
        else
        {
            oC_SaveError("List - push back element error" , oC_ErrorCode_AllocationError);
        }
    }

    return pushed;
}

//==========================================================================================================================================
/**
 * @brief The function pushes front an object to the list
 * 
 * @param List          The list to push front
 * @param Object        The object to push front
 * @param Size          The size of the object
 * @param Allocator     The allocator to use
 * 
 * @return true if the object was pushed front
 */
//==========================================================================================================================================
bool List_PushFront( oC_List_t List , const void * Object , oC_UInt_t Size , Allocator_t Allocator)
{
    bool pushed = false;

    if(List_IsCorrect(List) && (Size > 0) && isaddresscorrect(Object))
    {
        oC_List_ElementHandle_t element = List_NewElement(Object,Size,Allocator);

        if(element)
        {
            if(List->Last && List->First)
            {
                element->Next         = List->First;
                element->Previous     = NULL;
                List->First->Previous = element;
                List->First           = element;
            }
            else
            {
                PushToEmpty(List,element);
            }

            pushed = true;
            List->Count++;
        }
        else
        {
            oC_SaveError("List - push front element error" , oC_ErrorCode_AllocationError);
        }
    }

    return pushed;
}

//==========================================================================================================================================
/**
 * @brief The function inserts an object after the element handle
 * 
 * @param List              The list to insert
 * @param Object            The object to insert
 * @param Size              The size of the object
 * @param ElementHandle     The element handle to insert after
 * @param Allocator         The allocator to use
 * 
 * @return true if the object was inserted
 */
//==========================================================================================================================================
bool List_InsertAfter( oC_List_t List , void * Object , oC_UInt_t Size , oC_List_ElementHandle_t ElementHandle , Allocator_t Allocator)
{
    bool inserted = false;

    if(List_IsCorrect(List) && (Size > 0) && isaddresscorrect(Object))
    {
        if(List->First == NULL && List->Last == NULL)
        {
            inserted = List_PushFront(List,Object,Size,Allocator);
        }
        else if(isram(ElementHandle)==false)
        {
            oC_SaveError("List-insert-after: ElementHandle is not in ram " , oC_ErrorCode_WrongAddress);
        }
        else if(List->Last == ElementHandle)
        {
            inserted = List_PushBack(List,Object,Size,Allocator);
        }
        else
        {
            oC_List_ElementHandle_t element = List_NewElement(Object,Size,Allocator);

            if(element)
            {
                element->Next       = ElementHandle->Next;
                element->Previous   = ElementHandle;

                ElementHandle->Next->Previous   = element;
                ElementHandle->Next             = element;

                inserted = true;
                List->Count++;
            }
            else
            {
                oC_SaveError("List - insert after element error" , oC_ErrorCode_AllocationError);
            }
        }
    }

    return inserted;
}

//==========================================================================================================================================
/**
 * @brief The function inserts an object before the element handle
 * 
 * @param List              The list to insert
 * @param Object            The object to insert
 * @param Size              The size of the object
 * @param ElementHandle     The element handle to insert before
 * @param Allocator         The allocator to use
 * 
 * @return true if the object was inserted
 */
//==========================================================================================================================================
bool List_InsertBefore( oC_List_t List , void * Object , oC_UInt_t Size , oC_List_ElementHandle_t ElementHandle , Allocator_t Allocator)
{
    bool inserted = false;

    if(List_IsCorrect(List) && (Size > 0) && isaddresscorrect(Object))
    {
        if( (List->First == ElementHandle) || (List->First == NULL && List->Last == NULL))
        {
            inserted = List_PushFront(List,Object,Size,Allocator);
        }
        else if(isram(ElementHandle)==false)
        {
            oC_SaveError("List-insert-after: ElementHandle is not in ram " , oC_ErrorCode_WrongAddress);
        }
        else
        {
            oC_List_ElementHandle_t element = List_NewElement(Object,Size,Allocator);

            if(element)
            {
                element->Next       = ElementHandle;
                element->Previous   = ElementHandle->Previous;

                ElementHandle->Previous->Next   = element;
                ElementHandle->Previous         = element;

                inserted = true;
                List->Count++;
            }
            else
            {
                oC_SaveError("List - insert before element error" , oC_ErrorCode_AllocationError);
            }
        }
    }

    return inserted;
}

//==========================================================================================================================================
/**
 * @brief The function replaces an object in the list
 * 
 * The function replaces an object in the list. The object is replaced by the object given in the function.
 * 
 * @param List              The list to replace
 * @param Object            The object to replace
 * @param Size              The size of the object
 * @param ElementHandle     The element handle to replace
 * @param Allocator         The allocator to use
 * @param Flags             The allocation flags
 * 
 * @return true if the object was replaced
 */
//==========================================================================================================================================
bool List_Replace( oC_List_t List , void * Object , oC_UInt_t Size , oC_List_ElementHandle_t ElementHandle , Allocator_t Allocator , AllocationFlags_t Flags )
{
    bool replaced = false;

    if(List_IsCorrect(List) && (Size > 0) && isram(ElementHandle) && isaddresscorrect(Object))
    {
        if( List->First == NULL && List->Last == NULL )
        {
            replaced = false;
        }
        else
        {
            if(!kfree(ElementHandle->Object,Flags))
            {
                oC_SaveError("List replace" , oC_ErrorCode_ReleaseError);
            }

            ElementHandle->Object = kmalloc(Size,Allocator,Flags);

            if(ElementHandle->Object)
            {
                memcpy(ElementHandle->Object,Object,Size);
                ElementHandle->Size = Size;
                replaced            = true;
            }

        }
    }

    return replaced;
}

//==========================================================================================================================================
/**
 * @brief The function removes an object from the list
 * 
 * The function removes an object from the list. The object is removed by the element handle given in the function.
 * 
 * @param List              The list to remove
 * @param ElementHandle     The element handle to remove
 * 
 * @return true if the object was removed
 */
//==========================================================================================================================================
bool List_Remove( oC_List_t List , oC_List_ElementHandle_t ElementHandle )
{
    bool removed = false;

    if(List_IsCorrect(List) && isram(ElementHandle) && (List->Count > 0))
    {
        if(ElementHandle == List->First)
        {
            removed = List_PopFront(List);
        }
        else if (ElementHandle == List->Last)
        {
            removed = List_PopBack(List);
        }
        else
        {
            oC_List_ElementHandle_t previous = ElementHandle->Previous;
            oC_List_ElementHandle_t next     = ElementHandle->Next;

            previous->Next = next;
            next->Previous = previous;

            removed = List_DeleteElement(List, ElementHandle);
        }
    }

    return removed;
}

//==========================================================================================================================================
/**
 * @brief The function removes all objects from the list
 * 
 * The function removes all objects from the list. The objects are removed by the object given in the function.
 * 
 * @param List              The list to remove
 * @param Object            The object to remove
 * @param Size              The size of the object
 * 
 * @return true if the object was removed
 */
//==========================================================================================================================================
uint32_t List_RemoveAll( oC_List_t List , void * Object , oC_UInt_t Size )
{
    uint32_t removedCount = 0;

    if(List_IsCorrect(List) && (Size > 0) && isaddresscorrect(Object))
    {
        oC_List_ElementHandle_t handle = List->First;
        oC_List_ElementHandle_t next   = handle->Next;
        while(handle != NULL)
        {
            if(Size == handle->Size && (memcmp(Object,handle->Object,Size) == 0))
            {
                if(List_Remove(List,handle))
                {
                    removedCount++;
                }
            }
            handle = next;
            next   = next->Next;
        }
    }

    return removedCount;
}

//==========================================================================================================================================
/**
 * @brief The function reads an object from the element handle
 * 
 * The function reads an object from the element handle. The object is read by the element handle given in the function.
 * 
 * @param List              The list to read
 * @param ElementHandle     The element handle to read
 * @param outObject         Destination for the object read from the list
 * @param Size              The size of the destination object
 * 
 * @return The object read from the list
 */
//==========================================================================================================================================
void * List_At( oC_List_t List , oC_List_ElementHandle_t ElementHandle , void * outObject , oC_UInt_t Size )
{
    void * Object = NULL;

    if(List_IsCorrect(List) && isram(ElementHandle) && isram(outObject))
    {
        if(Size == ElementHandle->Size)
        {
            Object = outObject;
            memcpy(outObject,ElementHandle->Object,Size);
        }
    }

    return Object;
}

//==========================================================================================================================================
/**
 * @brief The function reads an object from the element handle and returns a default object if the element handle is not correct
 * 
 * The function reads an object from the element handle and returns a default object if the element handle is not correct. The object is read by the element handle given in the function.
 * 
 * @param List              The list to read
 * @param DefaultObject     The default object to return if the element handle is not correct
 * @param DefaultSize       The size of the default object
 * @param ElementHandle     The element handle to read
 * @param outObject         Destination for the object read from the list
 * @param Size              The size of the destination object
 * 
 * @return The object read from the list
 */
//==========================================================================================================================================
void * List_AtWithDefault( oC_List_t List , void * DefaultObject , oC_UInt_t DefaultSize , oC_List_ElementHandle_t ElementHandle , void * outObject , oC_UInt_t Size )
{
    void * AtObject = DefaultObject;

    if(isram(outObject) && isaddresscorrect(DefaultObject))
    {
        if(Size == DefaultSize)
        {
            memcpy(outObject,DefaultObject,Size);
        }

        if(List_IsCorrect(List) && isram(ElementHandle))
        {
            if(Size == ElementHandle->Size)
            {
                AtObject = outObject;
                memcpy(outObject,ElementHandle->Object,Size);
            }
        }
    }

    return AtObject;
}

//==========================================================================================================================================
/**
 * @brief The function moves an object from one element handle to another
 * 
 * The function moves an object from one element handle to another. The object is moved by the element handle given in the function.
 * 
 * @param List              The list to move
 * @param From              The element handle to move from
 * @param To                The element handle to move to
 * @param Allocator         The allocator to use
 * 
 * @return true if the object was moved
 */
//==========================================================================================================================================
bool List_Move( oC_List_t List , oC_List_ElementHandle_t From , oC_List_ElementHandle_t To , Allocator_t Allocator )
{
    bool moved = false;

    if(List_IsCorrect(List) && isram(From) && isram(To))
    {
        oC_UInt_t size = From->Size;
        void * object  = ksmartalloc(size, Allocator, AllocationFlags_Default);
        if (
            oC_SaveIfFalse("List_Move: temporary object", isaddresscorrect(object)                                                , oC_ErrorCode_AllocationError      )
         && oC_SaveIfFalse("List_Move: From", List_TakeAt(List,From,object,size)                                                  , oC_ErrorCode_CannotTakeElement    )
         && oC_SaveIfFalse("List_Move: From", List_Replace(List, To->Object, To->Size, From, Allocator, AllocationFlags_Default)  , oC_ErrorCode_CannotPushElement    )
         && oC_SaveIfFalse("List_Move: To"  , List_Replace(List, object, size, From, Allocator, AllocationFlags_Default)          , oC_ErrorCode_CannotPushElement    )
            )
        {
            moved = true;
        }
        if (object)
        {
            moved = oC_SaveIfFalse("List_Move: temporary object", ksmartfree(object, size, AllocationFlags_Default), oC_ErrorCode_ReleaseError) && moved;
        }
    }

    return moved;
}

//==========================================================================================================================================
/**
 * @brief The function removes an object from the back of the list
 * 
 * The function removes an object from the back of the list. The object is removed by the list given in the function.
 * 
 * @param List              The list to remove
 * 
 * @return true if the object was removed
 */
//==========================================================================================================================================
bool List_PopBack( oC_List_t List )
{
    bool poped = false;

    if(List_IsCorrect(List) && (List->Last != NULL))
    {
        oC_List_ElementHandle_t handle = List->Last;

        List->Last = handle->Previous;

        if(List->Last == NULL)
        {
            List->First = NULL;
        }
        else
        {
            List->Last->Next = NULL;
        }

        poped = List_DeleteElement(List, handle);
    }

    return poped;
}

//==========================================================================================================================================
/**
 * @brief The function removes an object from the front of the list
 * 
 * The function removes an object from the front of the list. The object is removed by the list given in the function.
 * 
 * @param List              The list to remove
 * 
 * @return true if the object was removed
 */
//==========================================================================================================================================
bool List_PopFront( oC_List_t List )
{
    bool poped = false;

    if(List_IsCorrect(List) && (List->Last != NULL))
    {
        oC_List_ElementHandle_t handle = List->First;

        List->First = handle->Next;

        if(List->First == NULL)
        {
            List->Last = NULL;
        }
        else
        {
            List->First->Previous = NULL;
        }

        poped = List_DeleteElement(List, handle);
    }

    return poped;
}

//==========================================================================================================================================
/**
 * @brief The function swaps two objects in the list
 * 
 * The function swaps two objects in the list. The objects are swapped by the element handles given in the function.
 * 
 * @param List              The list to swap
 * @param I                 The first element handle to swap
 * @param J                 The second element handle to swap
 * 
 * @return true if the objects were swapped
 */
//==========================================================================================================================================
bool List_Swap( oC_List_t List , oC_List_ElementHandle_t I , oC_List_ElementHandle_t J )
{
    bool swaped = false;

    if(List_IsCorrect(List) && isram(I) && isram(J))
    {
        void *      object = I->Object;
        oC_UInt_t   size   = I->Size;

        I->Object = J->Object;
        I->Size   = J->Size;

        J->Object = object;
        J->Size   = size;

        swaped = true;
    }

    return swaped;
}

//==========================================================================================================================================
/**
 * @brief The function takes an object from the list at the given position
 * 
 * The function takes an object from the list at the given position. The object is not longer in the list after the function.
 * 
 * @param List              The list to take
 * @param Position          The position to take
 * @param outObject         Destination for the object taken from the list
 * @param Size              The size of the destination object
 * 
 * @return true if the object was taken
 */
//==========================================================================================================================================
bool List_TakeAt( oC_List_t List , oC_List_ElementHandle_t ElementHandle , void * outObject , oC_UInt_t Size )
{
    bool result = false;

    if(List_IsCorrect(List) && isram(ElementHandle) && ElementHandle->Size == Size && isram(outObject))
    {
        if(ElementHandle == List->First)
        {
            result = List_TakeFirst(List,outObject,Size);
        }
        else if (ElementHandle == List->Last)
        {
            result = List_TakeLast(List,outObject,Size);
        }
        else
        {
            result = true;

            memcpy(outObject,ElementHandle->Object,Size);

            oC_List_ElementHandle_t previous = ElementHandle->Previous;
            oC_List_ElementHandle_t next     = ElementHandle->Next;

            if(previous)
            {
                previous->Next = next;
            }

            if(next)
            {
                next->Previous = previous;
            }

            if(!List_DeleteElement(List, ElementHandle))
            {
                oC_SaveError("List , takeAt, delete element" , oC_ErrorCode_ReleaseError);
            }
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief The function takes a first a object from the of the list
 * 
 * The function takes a first a object from the of the list. The object is not longer in the list after the function.
 * 
 * @param List              The list to take
 * @param outObject         Destination for the object taken from the list
 * @param Size              The size of the destination object
 * 
 * @return true if the object was taken
 */
//==========================================================================================================================================
bool List_TakeFirst( oC_List_t List , void * outObject , oC_UInt_t Size )
{
    bool result = false;

    if(List_IsCorrect(List) && List->First != NULL && List->First->Size == Size && isram(outObject))
    {
        oC_List_ElementHandle_t first = List->First;

        result      = true;

        memcpy(outObject,List->First->Object,Size);

        List->First = List->First->Next;

        if(List->Last == first)
        {
            List->Last = List->First;
        }

        if(List->First != NULL)
        {
            List->First->Previous = NULL;
        }

        if(!List_DeleteElement(List, first))
        {
            oC_SaveError("List , take first, delete element" , oC_ErrorCode_ReleaseError);
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief The function takes a last a object from the of the list
 * 
 * The function takes a last a object from the of the list. The object is not longer in the list after the function.
 * 
 * @param List              The list to take
 * @param outObject         Destination for the object taken from the list
 * @param Size              The size of the destination object
 * 
 * @return true if the object was taken
 */
//==========================================================================================================================================
bool List_TakeLast( oC_List_t List , void * outObject , oC_UInt_t Size )
{
    bool result = false;

    if(List_IsCorrect(List) && List->Last != NULL && isram(outObject))
    {
        oC_List_ElementHandle_t last = List->Last;

        result      = true;

        memcpy(outObject,List->Last->Object,Size);

        List->Last  = List->Last->Previous;

        if(List->First == last)
        {
            List->First = List->Last;
        }

        if(List->Last != NULL)
        {
            List->Last->Next = NULL;
        }

        if(!List_DeleteElement(List, last))
        {
            oC_SaveError("List , take last, delete element" , oC_ErrorCode_ReleaseError);
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief The function verifies that the list is correct
 * 
 * The function verifies that the list is correct. It can be used as a last resort if the list is corrupted.
 * 
 * @param List              The list to verify
 * 
 * @return true if the list is correct
 */
//==========================================================================================================================================
bool List_Verify( oC_List_t List )
{
    bool correct = false;

    if(List_IsCorrect(List))
    {
       uint32_t count = 0;

       for(oC_List_ElementHandle_t element = List->First ; isram(element) && element != NULL ; element = element->Next )
       {
           count++;
       }

       correct = count == List->Count;
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief The function repairs the list
 * 
 * The function repairs the list. It can be used as a last resort if the list is corrupted.
 * 
 * @param List              The list to repair
 * @param SavedListAddress  The address of the list in the saved memory
 * @param MaximumNumberOfObjects The maximum number of objects in the list
 * 
 * @example
 * ```c
 * oC_List<int> list = oC_List_New();
 * 
 * // Save the list to the saved memory
 * void* savedListAddress = list;
 * 
 * // Do something with the list that can corrupt it
 * 
 * // Repair the list
 * List_Repair(list,savedListAddress,100);
 * 
 * // Now the list is repaired and can be used again, but please note, that there can be a memory leak, because 
 * // the list did contain some objects that are not in the list anymore
 * ```
 * 
 * @return true if the list was repaired
 */
//==========================================================================================================================================
bool List_Repair( oC_List_t List , void * SavedListAddress , uint32_t MaximumNumberOfObjects )
{
    bool repaired = false;

    /* Check list pointer first and try to restore it if possible */
    if(List_IsCorrect(List) == false)
    {
        if(isram(SavedListAddress) && List != SavedListAddress)
        {
            List = SavedListAddress;
        }
        if(List_IsCorrect(List) == false && isram(List) && SavedListAddress == List)
        {
            List->ObjectControl = oC_CountObjectControl(SavedListAddress,oC_ObjectId_List);
        }
    }

    /* Now, if we repaired list address, we can try to restore elements inside */
    if(List_IsCorrect(List))
    {
        uint32_t                countFromHead       = 0;
        uint32_t                countFromTail       = 0;
        oC_List_ElementHandle_t lastElementFromHead = NULL;
        oC_List_ElementHandle_t firstElementFromTail= NULL;
        bool                    repairFromHead      = false;
        bool                    repairFromTail      = false;

        // Count valid elements from head and tail
        for(oC_List_ElementHandle_t element = List->First ; isram(element) && element != NULL ; element = element->Next )
        {
            lastElementFromHead = element;
            countFromHead++;
        }

        // Count valid elements from tail
        for(oC_List_ElementHandle_t element = List->Last; isram(element) && element != NULL ; element = element->Previous )
        {
            firstElementFromTail = element;
            countFromTail++;
        }

        if(List->Count < MaximumNumberOfObjects)
        {
            // If the number of elements counting from head is valid start from head
            if( countFromHead == List->Count && isram(lastElementFromHead))
            {
                repairFromHead = true;
            }
            // If the number of elements counting from tail is valid start from tail
            else if( countFromTail == List->Count && isram(firstElementFromTail))
            {
                repairFromTail = true;
            }
            else
            {
                if(countFromHead < MaximumNumberOfObjects)
                {
                    repairFromHead = true;
                }
                if(countFromTail < MaximumNumberOfObjects)
                {
                    repairFromTail = true;
                }
            }
        }
        // If the list counter is damaged, we cannot rely on it and have to count from both sides
        else
        {
            repairFromHead = true;
            repairFromTail = true;
        }

        if(repairFromHead)
        {
            oC_List_ElementHandle_t previous = NULL;

            countFromHead = 0;

            List->Last = lastElementFromHead;

            // Repair the list from head by setting the previous pointer of each element
            for(oC_List_ElementHandle_t element = List->First; isram(element) && element != NULL ; element = element->Next )
            {
                element->Previous = previous;
                previous          = element;
                countFromHead++;
            }
        }

        if(repairFromTail)
        {
            oC_List_ElementHandle_t next = NULL;

            countFromTail = 0;

            List->First = firstElementFromTail;

            // Repair the list from tail by setting the next pointer of each element
            for(oC_List_ElementHandle_t element = List->Last; isram(element) && element != NULL ; element = element->Previous )
            {
                element->Next     = next;
                next              = element;
                countFromTail++;
            }
        }

        if(repairFromHead)
        {
            oC_List_ElementHandle_t previous = NULL;

            countFromHead = 0;

            List->Last = lastElementFromHead;

            for(oC_List_ElementHandle_t element = List->First; isram(element) && element != NULL ; element = element->Next )
            {
                element->Previous = previous;
                previous          = element;
                countFromHead++;
            }
        }

        if(countFromHead == countFromTail && countFromHead < MaximumNumberOfObjects)
        {
            List->Count = countFromHead;
            repaired    = true;
        }
    }

    return repaired;
}

//==========================================================================================================================================
/**
 * @brief   Erase element from list
 * @param   List            List handle
 * @param   ElementHandle   Element handle
 * @return  true if element was erased
 * @note    Element handle will be invalid after this operation
 * 
*/
//==========================================================================================================================================
bool List_Erase(oC_List_t List, oC_List_ElementHandle_t ElementHandle)
{
    bool result = false;

    if(List_IsCorrect(List) && isram(ElementHandle))
    {
        if(ElementHandle->Previous != NULL)
        {
            ElementHandle->Previous->Next = ElementHandle->Next;
        }
        else
        {
            List->First = ElementHandle->Next;
        }

        if(ElementHandle->Next != NULL)
        {
            ElementHandle->Next->Previous = ElementHandle->Previous;
        }
        else
        {
            List->Last = ElementHandle->Previous;
        }

        if(!List_DeleteElement(List, ElementHandle))
        {
            oC_SaveError("List , erase, delete element" , oC_ErrorCode_ReleaseError);
        }
        else
        {
            List->Count--;
            result = true;
        }
    }

    return result;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief pushes element to empty list
 * @param List      List handle
 * @param Handle    Element handle
 */
//==========================================================================================================================================
static void PushToEmpty( oC_List_t List , oC_List_ElementHandle_t Handle )
{
    List->Last = Handle;
    List->First= Handle;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

