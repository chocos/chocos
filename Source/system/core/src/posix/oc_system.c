/** ****************************************************************************************************************************************
 *
 * @file       oc_system.c
 *
 * @brief      __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 6 wrz 2015 13:25:54) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_processman.h>
#include <oc_threadman.h>
#include <oc_clock_lld.h>
#include <oc_userman.h>
#include <oc_sys_lld.h>

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

Allocator_t getcurallocator(void)
{
    return oC_ProcessMan_GetCurrentAllocator();
}

Process_t getcurprocess(void)
{
    return oC_ProcessMan_GetCurrentProcess();
}

User_t getcuruser(void)
{
    return oC_Process_GetUser(oC_ProcessMan_GetCurrentProcess());
}

bool iscurroot(void)
{
    bool iscurroot = getcuruser() == getrootuser();

    if(iscurroot == false)
    {
        iscurroot = oC_SYS_LLD_GetCurrentContext() == oC_SYS_LLD_GetSystemContext();
    }

    return iscurroot;
}

oC_Timestamp_t gettimestamp(void)
{
    return oC_KTime_GetTimestamp();
}


oC_Time_t gettimeout( oC_Timestamp_t Timestamp )
{
    return oC_KTime_CalculateTimeout(Timestamp);
}

oC_Timestamp_t timeouttotimestamp( oC_Time_t Timeout )
{
    return oC_KTime_GetTimestamp() + Timeout;
}

User_t getrootuser(void)
{
    return oC_UserMan_GetRootUser();
}

Thread_t getcurthread(void)
{
    return oC_ThreadMan_GetCurrentThread();
}

bool sleep( oC_Time_t Time )
{
    bool        slept           = false;
    oC_Thread_t currentThread   = oC_ThreadMan_GetCurrentThread();

    if(oC_Thread_IsCorrect(currentThread))
    {
        slept = oC_Thread_Sleep(currentThread,Time);
    }
    else
    {
        slept = oC_CLOCK_LLD_DelayForMicroseconds(oC_Time_ToMicroseconds(Time));
    }

    return slept;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
