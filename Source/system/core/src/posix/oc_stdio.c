/** ****************************************************************************************************************************************
 *
 * @file       oc_stdio.c
 *
 * @brief      With source for the standard IO library
 *
 * @author     Patryk Kubiak - (Created on: 22 09 2015 20:33:32)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#include <oc_stdio.h>
#include <oc_stdlib.h>
#include <oc_kprint.h>
#include <oc_streamman.h>
#include <oc_system_cfg.h>
#include <oc_processman.h>
#include <string.h>
#include <ctype.h>
#include <oc_debug.h>
#include <oc_fs.h>
#include <oc_vfs.h>
#include <limits.h>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t Allocator = {
                .Name = "stdio module"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief      The function for printing formatted string to the standard output
 * 
 * @param      Format            The format string
 * @param      ...               The arguments
 * 
 * @return     The function returns the number of characters printed or negative value if error occured
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters printed, 
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will 
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
int vprintf( const char * Format , va_list ArgumentList )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    oC_Process_t   process          = oC_ProcessMan_GetCurrentProcess();
    bool           bufferAllocated  = false;

    if(oC_AssignErrorCodeIfFalse(&errorCode , oC_Process_IsCorrect(process) , oC_ErrorCode_ProcessNotCorrect))
    {
        char * buffer = NULL;

        if(!oC_AssignErrorCode(&errorCode , oC_Process_LockStdioBuffer(process,&buffer,ms(10))))
        {
            oC_SaveError("vprintf:Cannot lock stdio buffer: " , errorCode);

            buffer          = ksmartalloc(CFG_BYTES_STDIO_BUFFER_SIZE,&Allocator,AllocationFlags_CanWaitForever | AllocationFlags_ZeroFill );
            bufferAllocated = true;
        }

        if(buffer)
        {
            errorCode = oC_KPrint_VPrintf(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,oC_Process_GetIoFlags(process),Format,ArgumentList);

            if(!bufferAllocated)
            {
                errorCode = oC_Process_UnlockStdioBuffer(process);
            }
            else if(ksmartfree(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,AllocationFlags_CanWaitForever))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief      The function for printing formatted string to the standard output
 * 
 * @param      Format            The format string
 * @param      ...               The arguments
 * 
 * @return     The function returns the number of characters printed or negative value if error occured
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters printed, 
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will 
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
int printf( const char * Format , ... )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    oC_Process_t   process          = oC_ProcessMan_GetCurrentProcess();
    bool           bufferAllocated  = false;

    if(oC_AssignErrorCodeIfFalse(&errorCode , oC_Process_IsCorrect(process) , oC_ErrorCode_ProcessNotCorrect))
    {
        char * buffer = NULL;

        if(!oC_AssignErrorCode(&errorCode , oC_Process_LockStdioBuffer(process,&buffer,ms(10))))
        {
            oC_SaveError("Cannot lock stdio buffer" , errorCode);

            buffer          = ksmartalloc(CFG_BYTES_STDIO_BUFFER_SIZE,&Allocator,AllocationFlags_CanWaitForever | AllocationFlags_ZeroFill );
            bufferAllocated = true;
        }

        if(buffer)
        {
            va_list argumentList;
            va_start(argumentList, Format);
            errorCode = oC_KPrint_VPrintf(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,oC_Process_GetIoFlags(process),Format,argumentList);
            va_end(argumentList);

            if(!bufferAllocated)
            {
                oC_SaveIfErrorOccur("Cannot unlock STDIO buffer", oC_Process_UnlockStdioBuffer(process));
            }
            else if(ksmartfree(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,AllocationFlags_CanWaitForever))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief     The function prints a string to the standard output
 * 
 * @param     String            The string to print
 * 
 * @return    The function returns the number of characters printed or negative value if error occured
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters printed,
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
int puts( const char * String )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    oC_Process_t   process          = oC_ProcessMan_GetCurrentProcess();
    bool           bufferAllocated  = false;
    oC_UInt_t      bufferSize       = 0;

    if(oC_AssignErrorCodeIfFalse(&errorCode , oC_Process_IsCorrect(process) , oC_ErrorCode_ProcessNotCorrect))
    {
        char * buffer = NULL;

        bufferSize = strlen(String) + strlen("\n\r") + 1;

        if(!oC_AssignErrorCode(&errorCode , oC_Process_LockStdioBuffer(process,&buffer,ms(10))))
        {
            oC_SaveError("printf:Cannot lock stdio buffer: " , errorCode);

            buffer          = ksmartalloc(bufferSize,&Allocator,AllocationFlags_CanWaitForever | AllocationFlags_ZeroFill );
            bufferAllocated = true;
        }

        if(buffer)
        {
            strcpy(buffer,String);
            strcpy(&buffer[strlen(String)],"\n\r");

            errorCode = oC_KPrint_WriteToStdOut(oC_Process_GetIoFlags(process),buffer,bufferSize-1);

            if(!bufferAllocated)
            {
                errorCode = oC_Process_UnlockStdioBuffer(process);
            }
            else if(ksmartfree(buffer,bufferSize,AllocationFlags_CanWaitForever))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief     prints a character to the standard output
 * 
 * @param     C                 The character to print
 * @param     stream            The stream to print to
 * 
 * @return    The function returns the number of characters printed or negative value if error occured
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters printed,
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
int putc(int C, FILE *stream)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(stream == stdout || stream == stderr)
    {
        errorCode = oC_KPrint_WriteToStdOut(oC_Process_GetIoFlags(oC_ProcessMan_GetCurrentProcess()),(char*)&C,1);
    }
    else
    {
        errorCode = oC_VirtualFileSystem_putc(C,stream);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief prints a formatted string to the standard output
 * 
 * @param outString            The string to print
 * @param Size                 The size of the string
 * @param Format               The format string
 * 
 * @return The function returns the number of characters printed or negative value if error occured
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters printed,
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
int sprintf_s( char * outString , oC_UInt_t Size , const char * Format, ... )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(isram(outString)         , oC_ErrorCode_OutputAddressNotInRAM) &&
        ErrorCondition(isaddresscorrect(Format) , oC_ErrorCode_WrongAddress)
        )
    {
        va_list argumentList;
        va_start(argumentList, Format);
        errorCode = oC_KPrint_Format(outString,Size,Format,argumentList, NULL);
        va_end(argumentList);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief prints a formatted string to the standard output
 * 
 * @param outString            The string to print
 * @param Size                 The size of the string
 * @param Format               The format string
 * 
 * @return The function returns the number of characters printed or negative value if error occured
 */
//==========================================================================================================================================
int snprintf( char * outString , oC_UInt_t Size , const char * Format, ... )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    int length = 0;

    if(
        ErrorCondition(isram(outString)         , oC_ErrorCode_OutputAddressNotInRAM) &&
        ErrorCondition(isaddresscorrect(Format) , oC_ErrorCode_WrongAddress)
        )
    {
        va_list argumentList;
        va_start(argumentList, Format);
        oC_UInt_t stringLength = 0;
        errorCode = oC_KPrint_Format(outString,Size,Format,argumentList, &stringLength);
        if(errorCode == oC_ErrorCode_None)
        {
            length = (int)stringLength;
        }
        else 
        {
            length = -errorCode;
        }
        va_end(argumentList);
    }

    return length;
}

//==========================================================================================================================================
/**
 * @brief scans a formatted string from the standard input
 * 
 * @param Format               The format string
 * 
 * @return The function returns the number of characters read or negative value if error occured
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters read,
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will
 * return true if the result operation was successful and will protect you from the future changes.
 */ 
//==========================================================================================================================================
int scanf( const char * Format , ... )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Process_t   process   = oC_ProcessMan_GetCurrentProcess();

    if(oC_AssignErrorCodeIfFalse(&errorCode , oC_Process_IsCorrect(process) , oC_ErrorCode_ProcessNotCorrect))
    {
        char * buffer = ksmartalloc(CFG_BYTES_STDIO_BUFFER_SIZE,&Allocator,AllocationFlags_CanWaitForever | AllocationFlags_ZeroFill );

        if(buffer)
        {
            va_list argumentList;
            va_start(argumentList, Format);
            errorCode = oC_KPrint_VScanf(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,oC_Process_GetIoFlags(process),Format,argumentList);
            va_end(argumentList);

            if(!ksmartfree(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,AllocationFlags_CanWaitForever))
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief scans a formatted string from the string input
 * 
 * @param String               The string to scan
 * @param Format               The format string
 * 
 * @return The function returns the number of characters read or negative value if error occured
 * 
 * @warning
 * The current implementation is not following the standard C library, because it does not return the number of characters read,
 * but the error code instead. It will be changed in the future, but it requires adaptation of the whole system. Sorry for that.
 * If you write a new application and you need to verify the result of the function, please use #oC_VerifyStdioResult() function - it will
 * return true if the result operation was successful and will protect you from the future changes.
 */
//==========================================================================================================================================
extern int sscanf( const char * String , const char * Format , ...)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    va_list argumentList;
    va_start(argumentList, Format);
    errorCode = oC_KPrint_FormatScanf(String,Format,argumentList);
    va_end(argumentList);

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns the number of the argument in the argument list
 * 
 * @param Argc                 The number of arguments
 * @param Argv                 The argument list
 * @param Argument             The argument to find
 * 
 * @return The function returns the number of the argument in the argument list or -1 if the argument didn't occur
 */
//==========================================================================================================================================
int oC_GetArgumentNumber( int Argc , char ** Argv , const char * Argument )
{
    bool argumentOccur  = false;
    int  index          = 0;

    if(isaddresscorrect(Argv) || isaddresscorrect(Argument) )
    {
        int argumentLength = strlen(Argument);

        for( index = 0 ; index < Argc && !argumentOccur ; index++)
        {
            int length = strlen(Argv[index]);

            argumentOccur = length == argumentLength;

            for(int signIndex = 0; signIndex < length && argumentOccur ; signIndex++)
            {
                argumentOccur = tolower((int)Argv[index][signIndex]) == tolower((int)Argument[signIndex]);
            }
        }

    }

    return (argumentOccur == true) ? index - 1 : -1;
}

//==========================================================================================================================================
/**
 * @brief returns the argument after the given argument
 * 
 * @param Argc                 The number of arguments
 * @param Argv                 The argument list
 * @param Argument             The argument to find
 * 
 * @return The function returns the argument after the given argument or NULL if the argument didn't occur or the argument is the last one
 * 
 * @example{.c}
   int main(int argc, char ** argv)
   {
       char * argument = oC_GetArgumentAfter(argc,argv,"--my-argument");

       if(argument)
       {
           printf("The argument after --my-argument is %s",argument);
        }
        else
        {
            printf("The argument --my-argument didn't occur or is the last one");
        }

        return 0;
    }
 * @endexample
 */
//==========================================================================================================================================
char * oC_GetArgumentAfter( int Argc, char ** Argv , const char * Argument )
{
    char * argument = NULL;
    int    position = -1;

    if(isaddresscorrect(Argv) && Argc > 0 && isaddresscorrect(Argument))
    {
        position = oC_GetArgumentNumber(Argc,Argv,Argument);

        if(position > 0 && (position + 1) < Argc)
        {
            argument = Argv[position + 1];
        }
    }

    return argument;
}

//==========================================================================================================================================
/**
 * @brief verifies if the argument occur in the argument list
 * 
 * @param Argc                 The number of arguments
 * @param Argv                 The argument list
 * @param Argument             The argument to find
 * 
 * @return The function returns true if the argument occur in the argument list
 */
//==========================================================================================================================================
bool oC_ArgumentOccur( int Argc , char ** Argv , const char * Argument )
{
   return oC_GetArgumentNumber(Argc, Argv, Argument) >= 0;
}

//==========================================================================================================================================
/**
 * @brief checks if all the bytes in the memory are set to the given value
 * 
 * @param Buffer               The buffer to check
 * @param Size                 The size of the buffer
 * @param Value                The value to check
 * 
 * @return The function returns true if all the bytes in the memory are set to the given value
 */
//==========================================================================================================================================
bool oC_IsMemSetTo( void * Buffer , oC_UInt_t Size , uint8_t Value )
{
    bool        set     = true;
    uint8_t*    buffer  = Buffer;

    for(oC_UInt_t i = 0 ; i < Size ; i++ )
    {
        if(buffer[i] != Value)
        {
            set = false;
            break;
        }
    }

    return set;
}

//==========================================================================================================================================
/**
 * @brief opens a file
 * 
 * @param FileName             The name of the file to open
 * @param Mode                 The mode of the file
 * 
 * @return The function returns the pointer to the file or NULL if the file cannot be opened
 */
//==========================================================================================================================================
FILE * fopen ( const char * FileName, const char * Mode )
{
    FILE *                      file        = NULL;
    oC_FileSystem_ModeFlags_t   mode        = 0;
    oC_ErrorCode_t              errorCode   = oC_ErrorCode_ImplementError;
    bool                        modeCorrect = false;

    if(
        ErrorCondition(isaddresscorrect(FileName) , oC_ErrorCode_WrongAddress)
     && ErrorCondition(isaddresscorrect(Mode)     , oC_ErrorCode_WrongAddress)
        )
    {
        if(strcmp(Mode,"r") == 0 || strcmp(Mode,"rb") == 0)
        {
            mode = oC_FileSystem_ModeFlags_OpenExisting |
                   oC_FileSystem_ModeFlags_Read;
            modeCorrect = true;
        }
        else if(strcmp(Mode,"w") == 0 || strcmp(Mode,"wb") == 0)
        {
            mode = oC_FileSystem_ModeFlags_CreateNewAlways |
                   oC_FileSystem_ModeFlags_Write;
            modeCorrect = true;
        }
        else if(strcmp(Mode,"a") == 0)
        {
            mode = oC_FileSystem_ModeFlags_OpenAlways   |
                   oC_FileSystem_ModeFlags_Write        |
                   oC_FileSystem_ModeFlags_SeekToTheEnd;
            modeCorrect = true;
        }
        else if(strcmp(Mode,"r+") == 0)
        {
            mode = oC_FileSystem_ModeFlags_OpenExisting |
                   oC_FileSystem_ModeFlags_Read         |
                   oC_FileSystem_ModeFlags_Write        ;
            modeCorrect = true;
        }
        else if(strcmp(Mode,"w+") == 0)
        {
            mode = oC_FileSystem_ModeFlags_CreateNewAlways |
                   oC_FileSystem_ModeFlags_Read            |
                   oC_FileSystem_ModeFlags_Write           ;
            modeCorrect = true;
        }
        else if(strcmp(Mode,"a+") == 0)
        {
            mode = oC_FileSystem_ModeFlags_OpenAlways      |
                   oC_FileSystem_ModeFlags_Read            |
                   oC_FileSystem_ModeFlags_Write           ;
            modeCorrect = true;
        }
        else
        {
            kdebuglog(oC_LogType_Error , "fopen: mode %s is not correct" , Mode);
        }

        if(modeCorrect)
        {
            errorCode = oC_VirtualFileSystem_fopen((oC_File_t*)&file,FileName,mode,0);
            if(oC_ErrorOccur(errorCode))
            {
                kdebuglog(oC_LogType_Error,"fopen: Cannot open file %s - %R" , FileName , errorCode);
                file = NULL;
            }
        }
    }
    else
    {
        oC_SaveError("fopen: cannot open file - " , errorCode);
    }

    return file;
}

//==========================================================================================================================================
/**
 * @brief closes a file
 * 
 * @param stream               The file to close
 * 
 * @return The function returns 0 if the file is closed
 */
//==========================================================================================================================================
int fclose ( FILE * stream )
{
    return oC_VirtualFileSystem_fclose(stream);
}

//==========================================================================================================================================
/**
 * @brief reads a character from a file
 * 
 * @param stream               The file to read
 * 
 * @return The function returns the character read or EOF if the end of the file is reached
 */
//==========================================================================================================================================
size_t fread( void * Buffer, size_t ElementSize, size_t Count, FILE * File )
{
    oC_MemorySize_t readSize = ElementSize * Count;

    oC_SaveIfErrorOccur("fread: cannot read file" , oC_VirtualFileSystem_fread(File,Buffer,&readSize));

    return (size_t)readSize;
}

//==========================================================================================================================================
/**
 * @brief writes a string to a file
 * 
 * @param Buffer               The buffer to write
 * @param ElementSize          The size of the element to write
 * @param Count                The number of elements to write
 * @param File                 The file to write
 * 
 * @return The function returns the number of elements written
 */
//==========================================================================================================================================
size_t fwrite(const void * Buffer, size_t ElementSize, size_t Count, FILE * File )
{
    oC_MemorySize_t writtenSize = ElementSize * Count;

    oC_SaveIfErrorOccur("fread: cannot read file" , oC_VirtualFileSystem_fwrite(File,Buffer,&writtenSize));

    return (size_t)writtenSize;
}

//==========================================================================================================================================
/**
 * @brief writes a string to a file
 * 
 * @param File                 The file to write
 * @param Format               The format of the string to write
 * @param ArgumentList         The list of arguments
 * 
 * @return The function returns the number of characters written
 * 
 * @note The function uses a buffer of size CFG_BYTES_STDIO_BUFFER_SIZE
 */
//==========================================================================================================================================
int vfprintf( FILE * File, const char * Format , va_list ArgumentList )
{
    int bufferLength = 0;
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    // TODO: try to do it without the buffer
    char * buffer = ksmartalloc(CFG_BYTES_STDIO_BUFFER_SIZE,&Allocator,AllocationFlags_CanWaitForever | AllocationFlags_ZeroFill );
    if( ErrorCondition(isram( buffer), oC_ErrorCode_AllocationError) )
    {
        oC_MemorySize_t bufferLength = strlen(buffer);
        if ( 
             ErrorCode( oC_KPrint_VPrintf(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,oC_IoFlags_Default,Format,ArgumentList)        ) 
          && ErrorCode( oC_VirtualFileSystem_fwrite(File,buffer,&bufferLength)                                              ) 
             ) 
        {
            if ( 
                ErrorCondition( ksmartfree(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,AllocationFlags_CanWaitForever)      , oC_ErrorCode_ReleaseError ) 
                )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
        else 
        {
            oC_SaveIfFalse("vfprintf: cannot release buffer", ksmartfree(buffer,CFG_BYTES_STDIO_BUFFER_SIZE,AllocationFlags_CanWaitForever), oC_ErrorCode_ReleaseError);
        }
    }
    else
    {
        errorCode = oC_ErrorCode_AllocationError;
    }

    return oC_ErrorOccur(errorCode) ? (-errorCode) : bufferLength;
}

//==========================================================================================================================================
/**
 * @brief writes a string to a file
 * 
 * @param File                 The file to write
 * @param Format               The format of the string to write
 * @param ...                  The list of arguments
 * 
 * @return The function returns the number of characters written
 * 
 * @note The function uses a buffer of size CFG_BYTES_STDIO_BUFFER_SIZE
 */
//==========================================================================================================================================
int fprintf( FILE * File, const char * Format, ... )
{
    va_list     args;
    int         writtenSize;

    va_start(args,Format);
    writtenSize = vfprintf(File,Format,args);
    va_end(args);

    return writtenSize;
}

//==========================================================================================================================================
/**
 * @brief checks if the end of the file is reached
 * 
 * @param File                 The file to check
 * 
 * @return This function returns a non-zero value when End-of-File indicator associated with the stream is set, else zero is returned.
 */
//==========================================================================================================================================
int feof( FILE * File )
{
    return oC_VirtualFileSystem_eof(File);
}

//==========================================================================================================================================
/**
 * @brief returns the size of a file
 * 
 * @param File                 The file to check
 * 
 * @return This function returns the size of the file
 */
//==========================================================================================================================================
uint32_t fsize( FILE * File )
{
    return oC_VirtualFileSystem_size(File);
}

//==========================================================================================================================================
/**
 * @brief reads a character from a file
 * 
 * @param File                 The file to read
 * 
 * @return The function returns the character read or EOF if the end of the file is reached
 */
//==========================================================================================================================================
int fgetc( FILE * File )
{
    int c = 0;

    if(File == stdin)
    {
        if(oC_KPrint_ReadFromStdIn( oC_Process_GetIoFlags(oC_ProcessMan_GetCurrentProcess()), (char*)&c, 1 ) != oC_ErrorCode_None)
        {
            c = EOF;
        }
    }
    else if(isaddresscorrect(File))
    {
        if(oC_VirtualFileSystem_getc((char*)&c,File) != oC_ErrorCode_None)
        {
            c = EOF;
        }
    }

    return c;
}

//==========================================================================================================================================
/**
 * @brief writes a character to a file
 * 
 * @param c                    The character to write
 * @param File                 The file to write
 * 
 * @return The function returns the character written or EOF if the end of the file is reached
 * 
 * @warning
 * stderr is not supported yet
 */
//==========================================================================================================================================
int fputc( int c, FILE * File )
{
    if(File == stdout)
    {
        if(oC_KPrint_WriteToStdOut( oC_Process_GetIoFlags(oC_ProcessMan_GetCurrentProcess()), (char*)&c, 1 ) != oC_ErrorCode_None)
        {
            c = EOF;
        }
    }
    else if(isaddresscorrect(File))
    {
        if(oC_VirtualFileSystem_putc((char)c,File) != oC_ErrorCode_None)
        {
            c = EOF;
        }
    }

    return c;
}

//==========================================================================================================================================
/**
 * @brief reads a string from a file
 * 
 * @param Buffer               The buffer to read
 * @param Size                 The size of the buffer
 * @param File                 The file to read
 * 
 * @return The function returns the string read or NULL if the end of the file is reached
 */
//==========================================================================================================================================
char * fgets( char * Buffer, int Size , FILE * File )
{
    char * line = NULL;

    if(isram(Buffer) && Size > 0)
    {
        for(int i = 0; i < Size; i++)
        {
            int c = fgetc(File);

            if(c == EOF)
            {
                break;
            }
            else
            {
                line      = Buffer;
                Buffer[i] = (char)c;

                if(c == '\n')
                {
                    break;
                }
            }
        }
    }

    return line;
}

//==========================================================================================================================================
/**
 * @brief flushes the output buffer of a stream
 * 
 * @param File                 The file to flush
 * 
 * @return The function returns 0 if the file is flushed
 */
//==========================================================================================================================================
int fflush( FILE * File )
{
    return oC_VirtualFileSystem_flush(File);
}

//==========================================================================================================================================
/**
 * @brief Sets the file position of the stream to the given offset. The argument offset signifies the number of bytes to seek from the given whence position.
 * 
 * @param File                 The file to seek
 * @param Offset               The offset to seek
 * @param Whence               The position to seek from
 * 
 * @return The function returns 0 if the file is seeked
 * 
 * @warning
 */
//==========================================================================================================================================
int fseek( FILE * File, long Offset, int Whence )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( Whence == SEEK_SET )
    {
        errorCode = oC_VirtualFileSystem_lseek(File,Offset);
    }
    else if ( Whence == SEEK_CUR )
    {
        errorCode = oC_VirtualFileSystem_lseek(File,oC_VirtualFileSystem_tell(File) + Offset);
    }
    else if ( Whence == SEEK_END )
    {
        errorCode = oC_VirtualFileSystem_lseek(File,oC_VirtualFileSystem_size(File) + Offset);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns the current position of the file pointer of the stream
 * 
 * @param File                 The file to get the position
 * 
 * @return The function returns the current position of the file pointer of the stream
 */
//==========================================================================================================================================
long ftell( FILE * File )
{
    return oC_VirtualFileSystem_tell(File);
}

//==========================================================================================================================================
/**
 * @brief The C library function int remove(const char *filename) deletes the given filename so that it is no longer accessible.
 * 
 * @param File                 The file to remove
 * 
 * @return The function returns 0 if the file is removed
 */
//==========================================================================================================================================
int remove( const char * File )
{
    return oC_VirtualFileSystem_unlink(File);
}

//==========================================================================================================================================
/**
 * @brief Causes the filename referred to, by old_filename to be changed to new_filename.
 * 
 * @param OldFile              The old file name
 * @param NewFile              The new file name
 * 
 * @return The function returns 0 if the file is renamed
 */
//==========================================================================================================================================
int rename( const char * OldFile, const char * NewFile )
{
    return oC_VirtualFileSystem_rename(OldFile,NewFile);
}

//==========================================================================================================================================
/**
 * @brief The C library function int ferror(FILE *stream) tests the error indicator for the given stream.
 * 
 * @param File                 The file to test
 * 
 * @return The function returns 0 if the file is error free
 */
//==========================================================================================================================================
int ferror( FILE * File )
{
    return oC_VirtualFileSystem_error(File);
}

//==========================================================================================================================================
/**
 * @brief checks if a file exist
 * 
 * @param File                 The file to check
 * 
 * @return This function returns true if the file exist
 */
//==========================================================================================================================================
bool file_exist( const char * File )
{
    bool exist = false;

    if(isaddresscorrect(File))
    {
        oC_FileInfo_t fileInfo;

        exist = oC_VirtualFileSystem_stat(File,&fileInfo) == oC_ErrorCode_None;
    }

    return exist;
}

//==========================================================================================================================================
/**
 * Verifies that the STDIO function returned success
 * @param Result    result from the stdio operation (for example printf)
 * @return true if success
 */
//==========================================================================================================================================
bool oC_VerifyStdioResult( int Result )
{
    return !oC_ErrorOccur((oC_ErrorCode_t)Result);
}

//==========================================================================================================================================
/**
 * @brief reads a character from the standard input
 * 
 * @return The function returns the character read or EOF if the end of the file is reached
 */
//==========================================================================================================================================
int getchar( void )
{
    return fgetc(stdin);
}

//==========================================================================================================================================
/**
 * @brief prints a character to the standard output
 * 
 * @param c                     The character to print
 * 
 * @return The function returns the character printed
 */
//==========================================================================================================================================
int putchar( int c )
{
    return fputc(c,stdout);
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


