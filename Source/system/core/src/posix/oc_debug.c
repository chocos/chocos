/** ****************************************************************************************************************************************
 *
 * @file          oc_debug.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 25 mar 2015 08:25:21) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_debug.h>
#include <stdio.h>
#include <oc_kprint.h>
#include <oc_intman.h>
#include <string.h>
#include <oc_system.h>
#include <stdarg.h>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static char         DebugLogs[CFG_UINT16_NUMBER_OF_DEBUG_LOGS][CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH];
static uint16_t     PutIndex        = 0;
static uint16_t     GetIndex        = 0;
static uint16_t     GetOldestIndex  = 0;
static uint16_t     NumberOfLogs    = 0;
static oC_LogType_t EnabledLogs     = oC_LogType_Default;
static bool         Locked          = false;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ****************************************************************************************************************************************
 * The section interface functions
 */
#define _________________________________________INTERFACE_FUNCTIOS_SECTION_________________________________________________________________

//==========================================================================================================================================
/**
 * Enables logs types
 */
//==========================================================================================================================================
void enkdebuglog( oC_LogType_t LogType )
{
    EnabledLogs = LogType;
}

//==========================================================================================================================================
/**
 * Locks kernel debug logs
 */
//==========================================================================================================================================
void lockkdebuglog( void )
{
    Locked = true;
}

//==========================================================================================================================================
/**
 * Unlocks kernel debug logs
 */
//==========================================================================================================================================
void unlockkdebuglog( void )
{
    Locked = false;
}

//==========================================================================================================================================
/**
 * The function is for printing debug messages, only in case when the #CFG_ENABLE_DEBUG_PRINT definition in #oc_debug_cfg.h is set to ON
 *
 * @param Format        Format to print
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
void _kdebuglog( oC_LogType_t LogType , const char * Function, char * Format , ...)
{
#if CFG_ENABLE_DEBUG_PRINT == ON
    if((EnabledLogs & LogType) && Locked == false)
    {
        oC_IntMan_EnterCriticalSection();
        char * buffer = DebugLogs[PutIndex++];
        if(NumberOfLogs < CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            NumberOfLogs++;
        }
        GetIndex++;
        if(GetIndex == CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            GetIndex = 0;
        }
        if(PutIndex == CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            PutIndex = 0;
        }
        oC_IntMan_ExitCriticalSection();
        va_list argumentList;
        oC_MemorySize_t size = CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH;
        oC_MemorySize_t len  = 0;
        va_start(argumentList, Format);
        memset(buffer,0,CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH);
        if(LogType & oC_LogType_Error)
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_RED , gettimestamp(), Function);
        }
        else if(LogType & oC_LogType_Warning)
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_YELLOW , gettimestamp(), Function);
        }
        else if(LogType & oC_LogType_GoodNews)
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_GREEN , gettimestamp(), Function);
        }
        else
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_WHITE , gettimestamp(), Function);
        }
        len   = strlen(buffer);
        size -= len;
        oC_KPrint_Format(&buffer[len],size,Format,argumentList, NULL);
        va_end(argumentList);
    }
#else
#endif
}

//==========================================================================================================================================
/**
 * The function is for printing debug messages, only in case when the #CFG_ENABLE_DEBUG_PRINT definition in #oc_debug_cfg.h is set to ON
 *
 * @param Format        Format to print
 *
 * @return number of sent bytes
 */
//==========================================================================================================================================
void _vkdebuglog( oC_LogType_t LogType , const char * Function, const char * Format , va_list Arguments )
{
#if CFG_ENABLE_DEBUG_PRINT == ON
    if((EnabledLogs & LogType) && Locked == false)
    {
        oC_IntMan_EnterCriticalSection();
        char * buffer = DebugLogs[PutIndex++];
        if(NumberOfLogs < CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            NumberOfLogs++;
        }
        GetIndex++;
        if(GetIndex == CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            GetIndex = 0;
        }
        if(PutIndex == CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            PutIndex = 0;
        }
        oC_IntMan_ExitCriticalSection();
        oC_MemorySize_t size = CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH;
        oC_MemorySize_t len  = 0;
        memset(buffer,0,CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH);

        if(LogType & oC_LogType_Error)
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_RED , gettimestamp(), Function);
        }
        else if(LogType & oC_LogType_Warning)
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_YELLOW , gettimestamp(), Function);
        }
        else if(LogType & oC_LogType_GoodNews)
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_GREEN , gettimestamp(), Function);
        }
        else
        {
            sprintf_s(buffer,size, oC_VT100_FG_GREEN "%09.3f " oC_VT100_FG_WHITE "|" oC_VT100_FG_BLUE " %-30s" oC_VT100_FG_WHITE ": " oC_VT100_FG_WHITE , gettimestamp(), Function);
        }
        len   = strlen(buffer);
        size -= len;
        oC_KPrint_Format(&buffer[len],size,Format,Arguments, NULL);
    }
#else
#endif
}

//==========================================================================================================================================
/**
 * Returns last kernel debug log
 */
//==========================================================================================================================================
bool readlastkdebuglog( char * outString , oC_UInt_t Size )
{
    bool available = false;

    if(isram(outString) && NumberOfLogs > 0 && Size > 0)
    {
        oC_IntMan_EnterCriticalSection();
        available  = true;
        NumberOfLogs--;

        if(GetIndex == 0)
        {
            GetIndex = CFG_UINT16_NUMBER_OF_DEBUG_LOGS - 1;
        }
        else
        {
            GetIndex--;
        }
        char * buffer = DebugLogs[GetIndex];
        oC_IntMan_ExitCriticalSection();
        strncpy(outString,buffer,Size);
    }

    return available;
}

//==========================================================================================================================================
/**
 * Returns last kernel debug log
 */
//==========================================================================================================================================
bool readoldestkdebuglog( char * outString , oC_UInt_t Size )
{
    bool available = false;

    if(isram(outString) && NumberOfLogs > 0 && Size > 0)
    {
        oC_IntMan_EnterCriticalSection();
        available  = true;
        NumberOfLogs--;

        char * buffer = DebugLogs[GetOldestIndex++];

        if(GetOldestIndex >= CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            GetOldestIndex = 0;
        }
        oC_IntMan_ExitCriticalSection();
        strncpy(outString,buffer,Size);
    }

    return available;
}

//==========================================================================================================================================
/**
 * Saves log
 */
//==========================================================================================================================================
void savekdebuglog( char * Log )
{
    if(isaddresscorrect(Log) && strlen(Log) > 0)
    {
        oC_IntMan_EnterCriticalSection();
        char * buffer = DebugLogs[PutIndex++];
        if(NumberOfLogs < CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            NumberOfLogs++;
        }
        GetIndex++;
        if(GetIndex == CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            GetIndex = 0;
        }
        if(PutIndex == CFG_UINT16_NUMBER_OF_DEBUG_LOGS)
        {
            PutIndex = 0;
        }
        oC_IntMan_ExitCriticalSection();
        strncpy(buffer,Log,CFG_UINT16_MAX_DEBUGLOG_STRING_LENGTH);
    }
}

/* END OF SECTION */
#undef  _________________________________________INTERFACE_FUNCTIOS_SECTION_________________________________________________________________
