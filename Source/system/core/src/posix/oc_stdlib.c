/** ****************************************************************************************************************************************
 *
 * @file       oc_stdlib.c
 *
 * @brief      The file with source for stdlib functions
 *
 * @author     Patryk Kubiak - (Created on: 9 09 2015 21:36:21)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdlib.h>
#include <oc_memman.h>
#include <oc_processman.h>

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
void * _malloc( const char * Function , uint32_t LineNumber , AllocationFlags_t CoreFlags, oC_UInt_t Size, AllocationFlags_t Flags , ... )
{
    void * address = NULL;

    Allocator_t allocator = oC_ProcessMan_GetCurrentAllocator();

    if(allocator)
    {
        address = oC_MemMan_Allocate(Size,allocator,Function,LineNumber,Flags | CoreFlags,0);
    }

    return address;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool _free( AllocationFlags_t CoreFlags, void * Address , AllocationFlags_t Flags , ... )
{
    return oC_MemMan_Free(Address,CoreFlags | Flags);
}

//==========================================================================================================================================
//==========================================================================================================================================
void * _rawmalloc( oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags)
{
    void * address = NULL;

    oC_Process_t process = oC_ProcessMan_GetCurrentProcess();

    if(oC_Process_IsCorrect(process))
    {
        oC_HeapMap_t map = oC_Process_GetHeapMap(process);

        if(map)
        {
            address = oC_MemMan_RawAllocate(map,Size,Function,LineNumber,Flags);
        }
    }

    return address;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool _rawfree( void * Address , oC_UInt_t Size)
{
    bool released = false;

    oC_Process_t process = oC_ProcessMan_GetCurrentProcess();

    if(oC_Process_IsCorrect(process))
    {
        oC_HeapMap_t map = oC_Process_GetHeapMap(process);

        if(map)
        {
            released = oC_MemMan_RawFree(map,Address,Size);
        }
    }

    return released;
}

//==========================================================================================================================================
//==========================================================================================================================================
void * _kmalloc( oC_UInt_t Size , Allocator_t Allocator , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags , oC_UInt_t Alignment )
{
    return oC_MemMan_Allocate(Size,Allocator,Function,LineNumber,Flags,Alignment);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool _kfree( void * Address , AllocationFlags_t Flags )
{
    return oC_MemMan_Free(Address,Flags);
}

//==========================================================================================================================================
//==========================================================================================================================================
void * _krawmalloc( oC_HeapMap_t Map , oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    return oC_MemMan_RawAllocate(Map,Size,Function,LineNumber,Flags);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool _krawfree( oC_HeapMap_t Map , void * Address , oC_UInt_t Size )
{
    return oC_MemMan_RawFree(Map,Address,Size);
}

//==========================================================================================================================================
//==========================================================================================================================================
void * _smartalloc( oC_UInt_t Size , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags )
{
    void * address = NULL;

    oC_UInt_t realRawAllocationSize = Size + ((Size/8) + (((Size%8) > 0) ? 1 : 0));
    oC_UInt_t realAllocationSize    = Size + oC_MemMan_GetAllocationBlockSize();

    if(realAllocationSize >= realRawAllocationSize)
    {
        address = _rawmalloc(Size,Function,LineNumber,Flags);
    }
    if(address == NULL)
    {
        address = _malloc(Function,LineNumber,0,Size,Flags);
    }

    return address;
}
//==========================================================================================================================================
//==========================================================================================================================================
bool _smartfree( void * Address , oC_UInt_t Size , AllocationFlags_t Flags )
{
    bool released = _rawfree(Address,Size);

    if(!released)
    {
        released = _free(0,Address,Flags);
    }

    return released;
}

//==========================================================================================================================================
//==========================================================================================================================================
void * _ksmartalloc( oC_UInt_t Size , Allocator_t Allocator , const char * Function, uint32_t LineNumber , AllocationFlags_t Flags)
{
    void * address = _smartalloc(Size,Function,LineNumber,Flags);

    if(address == NULL)
    {
        address = _kmalloc(Size,Allocator,Function,LineNumber,Flags,0);
    }

    return address;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool _ksmartfree( void * Address , oC_UInt_t Size , AllocationFlags_t Flags)
{
    bool released = _smartfree(Address,Size,Flags);

    if(released == false)
    {
        released = _kfree(Address,Flags);
    }

    return released;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool isram( const void * Address )
{
    return oC_MemMan_IsRamAddress(Address);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool isrom( const void * Address )
{
    return oC_MemMan_IsFlashAddress(Address);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool isbufferinram( const void * Buffer , oC_UInt_t Size )
{
    return isram(Buffer) && isram(&((uint8_t*)Buffer)[Size-1]);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool isbufferinrom( const void * Buffer , oC_UInt_t Size )
{
    return isrom(Buffer) && isrom(&((uint8_t*)Buffer)[Size-1]);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool isaddresscorrect( const void * Address )
{
    return oC_MemMan_IsAddressCorrect(Address);
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION_______________________________________________________________

