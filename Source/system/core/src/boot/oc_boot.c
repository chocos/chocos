/** ****************************************************************************************************************************************
 *
 * @file       oc_boot.c
 *
 * @brief      The file with source of interface functions for boot manager
 *
 * @author     Patryk Kubiak - (Created on: 3 06 2015 16:54:14)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_boot.h>
#include <oc_memman.h>
#include <oc_threadman.h>
#include <oc_programman.h>
#include <oc_processman.h>
#include <oc_streamman.h>
#include <oc_userman.h>
#include <oc_driverman.h>
#include <oc_moduleman.h>

#include <oc_clock_lld.h>
#include <oc_sys_lld.h>
#include <oc_gpio_lld.h>
#include <oc_timer_lld.h>
#include <oc_dma_lld.h>

#include <oc_threadman.h>
#include <oc_processman.h>
#include <oc_intman.h>
#include <oc_semaphore.h>
#include <oc_timer.h>
#include <oc_system_cfg.h>
#include <oc_userman.h>
#include <oc_driverman.h>
#include <oc_kprint.h>
#include <oc_stdio.h>
#include <oc_vfs.h>
#include <oc_stdio.h>
#include <oc_screenman.h>
#include <oc_netifman.h>
#include <oc_udp.h>
#include <oc_exchan.h>
#include <oc_icmp.h>
#include <oc_tcp.h>
#include <oc_portman.h>
#include <oc_serviceman.h>
#include <oc_news.h>
#include <oc_ictrlman.h>

#include <oc_posix.h>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________

static oC_Boot_Level_t CurrentBootLevel = oC_Boot_Level_0;
static oC_Timestamp_t  StartupTimestamp = 0;

#undef  _________________________________________LOCAL_VARIABLES_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local functions prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________
//! @addtogroup Boot
//! @{

static void InitializeSystem(void);
static void InitializeLevel0(void);
static void InitializeLevel1(void);
static void InitializeLevel2(void);
static void InitializeLevel3(void);
static void InitializeLevel4(void);
static void InitializeLevel5(void);
static void InitializeLevel6(void);

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with interface functions sources
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup Boot
//! @{

//==========================================================================================================================================
/**
 *
 */
//==========================================================================================================================================
void oC_Boot_Main(void)
{
    InitializeSystem();

    CurrentBootLevel = oC_Boot_Level_SystemRunning;

    oC_News_Save("The system has started");

    while(1)
    {
        /* IDLE Task */
        oC_MemMan_CheckMemoryExhausted();
        oC_MemMan_CheckMemoryLeak();
        oC_MemMan_CheckOverflow();
        oC_ProcessMan_KillAllZombies();
        oC_ProgramMan_RerunDefualtIfNotRun(oC_UserMan_GetRootUser());
        oC_ExcHan_PrintLoggedEvents();
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Boot_Level_t oC_Boot_GetCurrentBootLevel( void )
{
    return CurrentBootLevel;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Timestamp_t oC_Boot_GetStartupTimestamp( void )
{
    return StartupTimestamp;
}

//==========================================================================================================================================
//==========================================================================================================================================
void oC_Boot_Restart( oC_Boot_Reason_t Reason , oC_User_t User )
{
    oC_SYS_LLD_Reset();
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
//! @addtogroup Boot
//! @{

static void VPrint( const char * Format, va_list ArgumentsList )
{
    char buffer[200] = {0};

    oC_KPrint_Format( buffer, sizeof(buffer), Format, ArgumentsList, NULL );

    oC_Stream_t stream = oC_StreamMan_GetStdErrorStream();

    oC_MemorySize_t size = strlen(buffer);
    oC_Stream_Write( stream, buffer, &size, oC_IoFlags_Default );
}

static void Print( const char * Format, ... )
{
    va_list argumentList;
    va_start(argumentList, Format);
    VPrint( Format, argumentList );
    va_end(argumentList);
}

static void PrintCurrentBootLevel( void )
{
    Print( oC_VT100_FG_BLUE "Booting level: 0x%08X\n\r", CurrentBootLevel );
}

static void PrintWaitMessage( const char * Format, ... )
{
    va_list argumentList;
    va_start(argumentList, Format);
    Print( oC_VT100_FG_WHITE "%50s ... " oC_VT100_SAVE_CURSOR_AND_ATTRS "[      ]\r", " " );
    VPrint( Format, argumentList );
    va_end( argumentList );
}

static void PrintResult( oC_ErrorCode_t ErrorCode )
{
    if(oC_ErrorOccur(ErrorCode))
    {
        Print( oC_VT100_RESTORE_CURSOR_AND_ATTRS "[ " oC_VT100_FG_RED "FAIL" oC_VT100_FG_WHITE " ]\n\r");
        Print( "\tError: " oC_VT100_FG_RED "%R" oC_VT100_FG_WHITE "\n\r", ErrorCode );
    }
    else
    {
        Print( oC_VT100_RESTORE_CURSOR_AND_ATTRS "[ " oC_VT100_FG_GREEN " OK " oC_VT100_FG_WHITE " ]\n\r");
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void TurnOnAndPrint( const char * WaitMessage, oC_ErrorCode_t (*TurnOnFunction)(void) )
{
    PrintWaitMessage( WaitMessage );

    oC_ErrorCode_t errorCode = TurnOnFunction();
    PrintResult( errorCode );

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError( WaitMessage , errorCode );
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
static void InitializeSystem(void)
{
    InitializeLevel0();
    InitializeLevel1();
    InitializeLevel2();
    InitializeLevel3();
    InitializeLevel4();
    InitializeLevel5();
    InitializeLevel6();

    StartupTimestamp = oC_KTime_GetTimestamp();
    Print( oC_VT100_FG_GREEN "System has started after %5.2f seconds\n\r", StartupTimestamp );

    //======================================================================================================================================
    /*
     * Run default program
     */
    //======================================================================================================================================
    PrintWaitMessage( "Running default program %s", oC_Program_GetName( oC_ProgramMan_GetDefaultProgram() )  );
    oC_ErrorCode_t errorCode = oC_ProgramMan_RunDefaultProgram(oC_UserMan_GetRootUser());
    PrintResult( errorCode );

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Run default program error: ", errorCode);
    }
}

#define ADD_FUNCTION(NAME)      extern void * _##NAME;
    POSIX_FUNCTIONS_LIST(ADD_FUNCTION);
#undef ADD_FUNCTION

//==========================================================================================================================================
//==========================================================================================================================================
static void InitializePosixPointers( void )
{
#define ADD_FUNCTION(NAME)      _##NAME = NAME;
    POSIX_FUNCTIONS_LIST(ADD_FUNCTION);
#undef ADD_FUNCTION
}

//==========================================================================================================================================
//==========================================================================================================================================
static void InitializeLevel0(void)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    //======================================================================================================================================
    /*
     * Prepare level
     */
    //======================================================================================================================================
    CurrentBootLevel = oC_Boot_Level_0;

    //======================================================================================================================================
    /*
     * Initializes pointers to posix functions
     */
    //======================================================================================================================================
    InitializePosixPointers();

    //======================================================================================================================================
    /*
     * Clock configuration
     */
    //======================================================================================================================================
    errorCode = oC_CLOCK_LLD_TurnOnDriver();

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Turn on clock driver error: " , errorCode);
    }

    /* Prepare target frequency */
#define MAX 0
    oC_Frequency_t targetFrequency = CFG_FREQUENCY_TARGET_FREQUENCY;
#undef MAX

    if(targetFrequency == 0)
    {
        targetFrequency = oC_CLOCK_LLD_GetMaximumClockFrequency();
    }

    /* Configuration of clock */
#if CFG_BOOL_EXTERNAL_CLOCK_SOURCE
    errorCode = oC_CLOCK_LLD_ConfigureExternalClock(targetFrequency,CFG_FREQUENCY_PERMISSIBLE_FREQUENCY_DIFFERENCE,CFG_FREQUENCY_EXTERNAL_CLOCK);
#else
    errorCode = oC_CLOCK_LLD_ConfigureInternalClock(targetFrequency,CFG_FREQUENCY_PERMISSIBLE_FREQUENCY_DIFFERENCE);
#endif

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Clock configuration error: " , errorCode );
    }

    //======================================================================================================================================
    /*
     * Memory manager configuration
     */
    //======================================================================================================================================
    errorCode = oC_MemMan_TurnOn();

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Turn on memory manager error: " , errorCode );
    }

    //======================================================================================================================================
    /*
     * Turning on Exception Handler module
     */
    //======================================================================================================================================
    errorCode = oC_ExcHan_TurnOn();

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Turn on exception handler error: " , errorCode );
    }

    //======================================================================================================================================
    /*
     * Turning on drivers manager module
     */
    //======================================================================================================================================
    errorCode = oC_DriverMan_TurnOn();

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Driver manager error: " , errorCode );
    }

    //======================================================================================================================================
    /*
     * Turning on drivers from current level and configuration of all possible drivers
     */
    //======================================================================================================================================
    oC_DriverMan_TurnOnAllDrivers( NULL, NULL );
    oC_DriverMan_ConfigureAllDrivers( NULL, NULL );

#if CFG_BOOL_GUI_ENABLED
    //======================================================================================================================================
    /*
     * Turning on screens manager module
     */
    //======================================================================================================================================
    errorCode = oC_ScreenMan_TurnOn();

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Screen manager error: " , errorCode );
    }
#endif

#if CFG_BOOL_GUI_ENABLED
    oC_ScreenMan_ConfigureAll( NULL, NULL );
#endif

    //======================================================================================================================================
    /*
     * Turning on stream manager
     */
    //======================================================================================================================================
    errorCode = oC_StreamMan_TurnOn();

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Turn on stream manager error: " , errorCode);
    }

    //======================================================================================================================================
    /*
     * Turning on kernel print module
     */
    //======================================================================================================================================
    errorCode = oC_KPrint_TurnOn();

    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveError("Turn on kernel print module error: " , errorCode);
    }

    oC_ModuleMan_TurnOnAllPossible( NULL, NULL );
    oC_ServiceMan_StartAllPossible( NULL, NULL );

    oC_News_Save("Level 0 initialized");
}

//==========================================================================================================================================
//==========================================================================================================================================
static void InitializeLevel1(void)
{
    //======================================================================================================================================
    /*
     * Prepare level
     */
    //======================================================================================================================================
    CurrentBootLevel = oC_Boot_Level_1;

    PrintCurrentBootLevel();

    TurnOnAndPrint( "Turning on user manager"       , oC_UserMan_TurnOn         );
    TurnOnAndPrint( "Turning on ktime module"       , oC_KTime_TurnOn           );
    TurnOnAndPrint( "Turning on program manager"    , oC_ProgramMan_TurnOn      );

    //======================================================================================================================================
    /*
     * Turning on drivers from current level and configuration of all possible drivers
     */
    //======================================================================================================================================
    oC_DriverMan_TurnOnAllDrivers( PrintWaitMessage, PrintResult );
    oC_DriverMan_ConfigureAllDrivers( PrintWaitMessage, PrintResult );

#if CFG_BOOL_GUI_ENABLED
    oC_ScreenMan_ConfigureAll( PrintWaitMessage, PrintResult );
#endif

    oC_ModuleMan_TurnOnAllPossible( PrintWaitMessage, PrintResult );
    oC_ServiceMan_StartAllPossible( PrintWaitMessage, PrintResult );

    oC_News_Save("Level 2 initialized");
}

//==========================================================================================================================================
//==========================================================================================================================================
static void InitializeLevel2(void)
{
    //======================================================================================================================================
    /*
     * Prepare level
     */
    //======================================================================================================================================
    CurrentBootLevel = oC_Boot_Level_2;
    PrintCurrentBootLevel();

    TurnOnAndPrint( "Running delete daemon"             , oC_ProcessMan_RunDeleteDaemon     );
    TurnOnAndPrint( "Running exception handler daemon"  , oC_ExcHan_RunDaemon               );

    //======================================================================================================================================
    /*
     * Turning on drivers from current level and configuration of all possible drivers
     */
    //======================================================================================================================================
    oC_DriverMan_TurnOnAllDrivers( PrintWaitMessage, PrintResult );
    oC_DriverMan_ConfigureAllDrivers( PrintWaitMessage, PrintResult );

#if CFG_BOOL_GUI_ENABLED
    oC_ScreenMan_ConfigureAll( PrintWaitMessage, PrintResult );
#endif

    oC_ModuleMan_TurnOnAllPossible( PrintWaitMessage, PrintResult );
    oC_ServiceMan_StartAllPossible( PrintWaitMessage, PrintResult );

    oC_News_Save("Level 2 initialized");
}

static void InitializeLevel3(void)
{
    //======================================================================================================================================
    /*
     * Prepare level
     */
    //======================================================================================================================================
    CurrentBootLevel = oC_Boot_Level_3;
    PrintCurrentBootLevel();

    TurnOnAndPrint( "Turning on VFS"            , oC_VirtualFileSystem_TurnOn               );

    //======================================================================================================================================
    /*
     * Turning on drivers from current level and configuration of all possible drivers
     */
    //======================================================================================================================================
    oC_DriverMan_TurnOnAllDrivers( PrintWaitMessage, PrintResult );
    oC_DriverMan_ConfigureAllDrivers( PrintWaitMessage, PrintResult );

#if CFG_BOOL_GUI_ENABLED
    oC_ScreenMan_ConfigureAll( PrintWaitMessage, PrintResult );
#endif

    oC_ModuleMan_TurnOnAllPossible( PrintWaitMessage, PrintResult );
    oC_ServiceMan_StartAllPossible( PrintWaitMessage, PrintResult );

    oC_News_Save("Level 3 initialized");
}

static void InitializeLevel4(void)
{
    //======================================================================================================================================
    /*
     * Prepare level
     */
    //======================================================================================================================================
    CurrentBootLevel = oC_Boot_Level_4;

    //======================================================================================================================================
    /*
     * Starting network manager
     */
    //======================================================================================================================================
#if CFG_BOOL_NETWORK_ENABLED == true
    TurnOnAndPrint( "Turning on NetifMan"            , oC_NetifMan_TurnOn                     );
    TurnOnAndPrint( "Starting NetifMan daemon"       , oC_NetifMan_StartDaemon                );
    TurnOnAndPrint( "Turning on UDP"                 , oC_Udp_TurnOn                          );
    TurnOnAndPrint( "Turning on ICMP"                , oC_Icmp_TurnOn                         );
    TurnOnAndPrint( "Turning on PortMan"             , oC_PortMan_TurnOn                      );
    TurnOnAndPrint( "Turning on TCP"                 , oC_Tcp_TurnOn                          );
#endif

    //======================================================================================================================================
    /*
     * Turning on drivers from current level and configuration of all possible drivers
     */
    //======================================================================================================================================
    oC_DriverMan_TurnOnAllDrivers( PrintWaitMessage, PrintResult );
    oC_DriverMan_ConfigureAllDrivers( PrintWaitMessage, PrintResult );

#if CFG_BOOL_GUI_ENABLED
    oC_ScreenMan_ConfigureAll( PrintWaitMessage, PrintResult );
#endif

#if CFG_BOOL_NETWORK_ENABLED
    oC_NetifMan_ConfigureAll();
#endif

    oC_ModuleMan_TurnOnAllPossible( PrintWaitMessage, PrintResult );
    oC_ServiceMan_StartAllPossible( PrintWaitMessage, PrintResult );

    oC_News_Save("Level 4 initialized");
}

static void InitializeLevel5(void)
{
    //======================================================================================================================================
    /*
     * Prepare level
     */
    //======================================================================================================================================
    CurrentBootLevel = oC_Boot_Level_5;
    PrintCurrentBootLevel();

    //======================================================================================================================================
    /*
     * Turning on drivers from current level and configuration of all possible drivers
     */
    //======================================================================================================================================
    oC_DriverMan_TurnOnAllDrivers( PrintWaitMessage, PrintResult );
    oC_DriverMan_ConfigureAllDrivers( PrintWaitMessage, PrintResult );

#if CFG_BOOL_GUI_ENABLED
    oC_ScreenMan_ConfigureAll( PrintWaitMessage, PrintResult );
#endif

#if CFG_BOOL_NETWORK_ENABLED
    oC_NetifMan_ConfigureAll();
#endif

    oC_ModuleMan_TurnOnAllPossible( PrintWaitMessage, PrintResult );
    oC_ServiceMan_StartAllPossible( PrintWaitMessage, PrintResult );

    oC_News_Save("Level 5 initialized");
}

static void InitializeLevel6(void)
{
    //======================================================================================================================================
    /*
     * Prepare level
     */
    //======================================================================================================================================
    CurrentBootLevel = oC_Boot_Level_6;
    PrintCurrentBootLevel();

    //======================================================================================================================================
    /*
     * Turning on drivers from current level and configuration of all possible drivers
     */
    //======================================================================================================================================
    oC_DriverMan_TurnOnAllDrivers( PrintWaitMessage, PrintResult );
    oC_DriverMan_ConfigureAllDrivers( PrintWaitMessage, PrintResult );

#if CFG_BOOL_GUI_ENABLED
    oC_ScreenMan_ConfigureAll( PrintWaitMessage, PrintResult );
#endif

#if CFG_BOOL_NETWORK_ENABLED
    oC_NetifMan_ConfigureAll();
#endif

    oC_ModuleMan_TurnOnAllPossible( PrintWaitMessage, PrintResult );
    oC_ServiceMan_StartAllPossible( PrintWaitMessage, PrintResult );

    oC_News_Save("Level 6 initialized");
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
//! @}
