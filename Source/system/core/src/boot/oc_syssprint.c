/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          oc_syssprint.c
 *
 * @author     Patryk Kubiak - (Created on: 11.02.2017 11:30:53) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup SYSSPRINT System Status Printer
 * @ingroup CoreSpace
 *
 ******************************************************************************************************************************************/

#include <oc_service.h>
#include <oc_module.h>
#include <oc_tgui.h>
#include <oc_threadman.h>
#include <oc_netifman.h>
#include <math.h>
#include <oc_dynamic_config.h>
#include <oc_array.h>
#include <oc_memman.h>
#include <oc_news.h>
#include <oc_string.h>
#include <oc_services_cfg.h>

#define NUMBER_OF_CPU_LOAD_MEASUREMENTS         50

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct Context_t
{
    double              CpuLoadArray[NUMBER_OF_CPU_LOAD_MEASUREMENTS];
    uint32_t            PutCpuLoadIndex;
    oC_MemorySize_t     LastFreeRamSize;
    char                LastNews[100];
    oC_Timestamp_t      NewsTimestamp;

    struct
    {
        oC_Timestamp_t      NextRefreshTimestamp;
    } Clock, CpuLoad, NetInfo, Version, GraphBorder, Graph, RamInfo, News;
} Context_t;

typedef struct
{
    oC_TGUI_Position_t          Position;
    oC_TGUI_Column_t            Width;
    oC_TGUI_Line_t              Height;
    oC_TGUI_Style_t             BorderStyle;
    oC_TGUI_Style_t             LabelStyle;
    oC_TGUI_Style_t             DotsStyle;
    char                        DotsChar;
} GraphStyle_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static oC_ErrorCode_t           StartService            ( oC_Service_Context_t * outContext );
static oC_ErrorCode_t           StopService             ( oC_Service_Context_t *    Context );
static oC_ErrorCode_t           MainFunction            ( oC_Service_Context_t      Context );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with constant definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________CONSTANT_SECTION___________________________________________________________________________

static const oC_TGUI_Position_t     ClockPosition = { .Column = 1 , .Line = 1 };
static const oC_TGUI_Style_t        ClockStyle    = {
                .Background    = oC_TGUI_Color_DarkGray ,
                .Foreground    = oC_TGUI_Color_White ,
                .TextStyle     = oC_TGUI_TextStyle_Default ,
                .DontDraw      = false ,
};
static const oC_TGUI_Position_t     CpuLoadPosition = { .Column = 15 , .Line = 1 };
static const oC_TGUI_Style_t        CpuLoadStyle    = {
                .Background    = oC_TGUI_Color_LightGray ,
                .Foreground    = oC_TGUI_Color_Black ,
                .TextStyle     = oC_TGUI_TextStyle_Default ,
                .DontDraw      = false ,
};
static const oC_TGUI_Position_t     ShortNetInfoPosition = { .Column = 23 , .Line = 1 };
static const oC_TGUI_Style_t        ShortNetInfoStyle    = {
                .Background    = oC_TGUI_Color_Green ,
                .Foreground    = oC_TGUI_Color_White ,
                .TextStyle     = oC_TGUI_TextStyle_Default ,
                .DontDraw      = false ,
};
static const oC_TGUI_Position_t     SystemVersionPosition = { .Column = 57 , .Line = 1 };
static const oC_TGUI_Style_t        SystemVersionStyle    = {
                .Background    = oC_TGUI_Color_LightBlue ,
                .Foreground    = oC_TGUI_Color_White ,
                .TextStyle     = oC_TGUI_TextStyle_Default ,
                .DontDraw      = false ,
};
static const oC_TGUI_Position_t     SystemNewsPosition = { .Column = 1 , .Line = 2 };
static const oC_TGUI_Column_t       SystemNewsWidth    = 56;
static const oC_TGUI_Style_t        SystemNewsStyle    = {
                .Background    = oC_TGUI_Color_White,
                .Foreground    = oC_TGUI_Color_Black,
                .TextStyle     = oC_TGUI_TextStyle_Default ,
                .DontDraw      = false ,
};
static const oC_TGUI_Style_t        SystemNewsTimestampStyle  = {
                .Background    = oC_TGUI_Color_Red,
                .Foreground    = oC_TGUI_Color_White,
                .TextStyle     = oC_TGUI_TextStyle_Default ,
                .DontDraw      = false ,
};
//static const oC_TGUI_Position_t     CursorPosition        = { .Column = 1 , .Line = 13 };
static const GraphStyle_t           GraphStyle            = {
                .Position.Column        = 1 ,
                .Position.Line          = 4 ,
                .Width                  = 28 ,
                .Height                 = 7 ,
                .DotsChar               = '*',

                .BorderStyle.Background = oC_TGUI_Color_Black,
                .BorderStyle.Foreground = oC_TGUI_Color_White,
                .BorderStyle.TextStyle  = oC_TGUI_TextStyle_Default ,
                .BorderStyle.DontDraw   = false,

                .LabelStyle.Background  = oC_TGUI_Color_Black,
                .LabelStyle.Foreground  = oC_TGUI_Color_Red,
                .LabelStyle.TextStyle   = oC_TGUI_TextStyle_Default ,
                .LabelStyle.DontDraw    = false,

                .DotsStyle.Background   = oC_TGUI_Color_White,
                .DotsStyle.Foreground   = oC_TGUI_Color_Red,
                .DotsStyle.TextStyle    = oC_TGUI_TextStyle_Default ,
                .DotsStyle.DontDraw     = false,
};
static const oC_TGUI_Column_t           RamInfoWidth         = 30;
static const oC_TGUI_Line_t             RamInfoHeight        = 5;
static const oC_TGUI_Position_t         RamInfoPosition      = { .Column = 30 , .Line = 4 };
static const oC_TGUI_PropertyStyle_t    RamInfoPropertyStyle = {
                .DescriptionStyle.Background    = oC_TGUI_Color_Black ,
                .DescriptionStyle.Foreground    = oC_TGUI_Color_White ,
                .DescriptionStyle.TextStyle     = oC_TGUI_TextStyle_Default ,
                .DescriptionStyle.DontDraw      = false ,

                .ValueStyle.Background          = oC_TGUI_Color_Black ,
                .ValueStyle.Foreground          = oC_TGUI_Color_Yellow ,
                .ValueStyle.TextStyle           = oC_TGUI_TextStyle_Default ,
                .ValueStyle.DontDraw            = false ,
};

#undef  _________________________________________CONSTANT_SECTION___________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with service definition
 *
 *  ======================================================================================================================================*/
#define _________________________________________SERVICE_DEF_SECTION________________________________________________________________________

const oC_Service_Registration_t syssprint = {
                .Name                   = "syssprint" ,
                .StdoutStreamName       = CFG_SYSSPRINT_STREAM_NAME ,
                .StartFunction          = StartService ,
                .StopFunction           = StopService ,
                .MainFunction           = MainFunction ,
                .MainThreadStackSize    = B(1200),
                .HeapMapSize            = 0,
                .AllocationLimit        = 0,
                .RequiredModules        = { oC_Module_ScreenMan, oC_Module_GTD } ,
                .Priority               = oC_Process_Priority_SystemHandlerDeamon
};

#undef  _________________________________________SERVICE_DEF_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief function called to initialize during starting the service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartService( oC_Service_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( isram(outContext) , oC_ErrorCode_OutputAddressNotInRAM ) )
    {
        *outContext = malloc( sizeof(Context_t) , AllocationFlags_ZeroFill );

        if( ErrorCondition( *outContext != NULL , oC_ErrorCode_AllocationError ) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function called during stopping the service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StopService( oC_Service_Context_t *    Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( isram(Context) , oC_ErrorCode_AddressNotInRam) )
    {
        if( ErrorCondition( free(*Context,0), oC_ErrorCode_ReleaseError ) )
        {
            *Context  = NULL;
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief draws a clock
 */
//==========================================================================================================================================
static void DrawClock( oC_Service_Context_t Context )
{
    if(oC_DynamicConfig_GetValue(syssprint,ClockEnabled) && Context->Clock.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes() == true)
    {
        oC_Time_Divided_t divided;

        oC_TGUI_SetFont(oC_TGUI_Font_G1);
        oC_TGUI_SetStyle( &ClockStyle );

        oC_Time_Divide( gettimestamp(), &divided );
        oC_TGUI_DrawFormatAtPosition(ClockPosition, "%3dd %02d:%02d:%02d ", divided.Days, divided.Hours, divided.Minutes, divided.Seconds);

        Context->Clock.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,ClockRefreshTime);

        oC_TGUI_RestoreAttributes();
    }
}

//==========================================================================================================================================
/**
 * @brief draws current CPU load
 */
//==========================================================================================================================================
static void DrawCpuLoad( oC_Service_Context_t Context )
{
    double cpuLoad = oC_ThreadMan_GetCurrentCpuLoad();

    if(oC_DynamicConfig_GetValue(syssprint,CpuLoadEnabled) && Context->CpuLoad.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes())
    {
        oC_TGUI_SetFont(oC_TGUI_Font_G0);
        oC_TGUI_SetStyle( &CpuLoadStyle );

        oC_TGUI_DrawFormatAtPosition(CpuLoadPosition, " %5.1f%% ", cpuLoad);

        if(Context->PutCpuLoadIndex >= NUMBER_OF_CPU_LOAD_MEASUREMENTS)
        {
            Context->PutCpuLoadIndex = 0;
        }
        Context->CpuLoadArray[Context->PutCpuLoadIndex++] = cpuLoad;

        Context->CpuLoad.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,CpuLoadRefreshTime);

        oC_TGUI_RestoreAttributes();
    }
}

//==========================================================================================================================================
/**
 * @brief draws IP of the first network interface, that is already connected
 */
//==========================================================================================================================================
static void DrawShortNetInfo( oC_Service_Context_t Context )
{
    oC_List(oC_Netif_t) list          = oC_NetifMan_GetList();
    oC_Netif_t          selectedNetif = NULL;
    oC_DefaultString_t  tempString    = {0};
    oC_Net_LinkStatus_t linkStatus    = oC_Net_LinkStatus_Down;
    oC_Net_Ipv4Info_t   ipv4Info;
    oC_TGUI_Position_t  position;

    oC_List_Foreach(list,netif)
    {
        selectedNetif = netif;

        if(oC_Netif_GetLinkStatus(netif) == oC_Net_LinkStatus_Up)
        {
            linkStatus = oC_Net_LinkStatus_Up;
            break;
        }
    }

    if(oC_DynamicConfig_GetValue(syssprint,NetInfoEnabled) && Context->NetInfo.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes())
    {
        oC_TGUI_SetFont(oC_TGUI_Font_G0);
        oC_TGUI_SetStyle( &ShortNetInfoStyle);

        if(selectedNetif != NULL)
        {
            memset(&ipv4Info,0,sizeof(ipv4Info));

            oC_Netif_ReadIpv4Info(selectedNetif,&ipv4Info);
            oC_Net_Ipv4AddressToString(ipv4Info.IP,tempString,sizeof(tempString));

            position.Column = ShortNetInfoPosition.Column;
            position.Line   = ShortNetInfoPosition.Line;

            oC_TGUI_DrawFormatAtPosition(position," %10s: ", oC_Netif_GetFriendlyName(selectedNetif));
            position.Column += 13;

            oC_TGUI_SetFont(oC_TGUI_Font_G1);

            oC_TGUI_DrawFormatAtPosition(position,"%15s ", tempString);

            oC_TGUI_SetFont(oC_TGUI_Font_G0);
            position.Column += 16;

            oC_TGUI_DrawFormatAtPosition(position,"%4s ", linkStatus == oC_Net_LinkStatus_Up ? "UP" : "DOWN");

            if(linkStatus == oC_Net_LinkStatus_Up && oC_Netif_IsIpAssigned(selectedNetif))
            {
                Context->NetInfo.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,NetInfoRefreshTime);
            }
        }
        else
        {
            oC_TGUI_SetFont(oC_TGUI_Font_G1);
            oC_TGUI_DrawFormatAtPosition(ShortNetInfoPosition, " %32s ", " Network not configured ");
        }
        oC_TGUI_RestoreAttributes();

    }
}

//==========================================================================================================================================
/**
 * @brief draws the system version
 */
//==========================================================================================================================================
static void DrawSystemVersion( oC_Service_Context_t Context )
{
    oC_TGUI_Position_t  position;

    if(oC_DynamicConfig_GetValue(syssprint,VersionEnabled) && Context->Version.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes())
    {
        oC_TGUI_SetFont(oC_TGUI_Font_G0);
        oC_TGUI_SetStyle( &SystemVersionStyle);

        position.Column = SystemVersionPosition.Column;
        position.Line   = SystemVersionPosition.Line;

        oC_TGUI_DrawFormatAtPosition(position," %01d.%02d.%02d.%02d ", oC_BUILD_MAJOR, oC_BUILD_YEAR, oC_BUILD_MONTH, oC_BUILD_PATCH);

        oC_TGUI_SetFont(oC_TGUI_Font_G1);
        position.Line   += 1;

        oC_TGUI_DrawFormatAtPosition(position," %10s ", oC_TO_STRING(oC_BUILD_NAME));
        oC_TGUI_RestoreAttributes();

        Context->Version.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,VersionRefreshTime);
    }

}

//==========================================================================================================================================
/**
 * @brief draws graph border
 */
//==========================================================================================================================================
static void DrawGraphBorders( oC_Service_Context_t Context )
{
    oC_TGUI_Position_t  startPosition;
    oC_TGUI_Position_t  endPosition;

    if(oC_DynamicConfig_GetValue(syssprint,GraphEnabled) && Context->GraphBorder.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes())
    {
        oC_TGUI_SetFont(oC_TGUI_Font_G1);
        oC_TGUI_SetStyle( &GraphStyle.BorderStyle );

        startPosition.Column        = GraphStyle.Position.Column + 3;
        startPosition.Line          = GraphStyle.Position.Line + 1;
        endPosition.Column          = startPosition.Column;
        endPosition.Line            = startPosition.Line + GraphStyle.Height - 1;

        oC_TGUI_DrawLine(startPosition,endPosition);

        startPosition.Column        = GraphStyle.Position.Column + 4;
        startPosition.Line          = GraphStyle.Position.Line   + GraphStyle.Height;
        endPosition.Column          = GraphStyle.Position.Column + GraphStyle.Width;
        endPosition.Line            = GraphStyle.Position.Line   + GraphStyle.Height;

        oC_TGUI_DrawLine(startPosition,endPosition);

        oC_TGUI_SetFont(oC_TGUI_Font_G0);
        oC_TGUI_SetStyle( &GraphStyle.LabelStyle );

        startPosition.Column        = GraphStyle.Position.Column;
        startPosition.Line          = GraphStyle.Position.Line;

        oC_TGUI_DrawAtPosition(startPosition, "100");

        startPosition.Column        = GraphStyle.Position.Column;
        startPosition.Line         += GraphStyle.Height;

        oC_TGUI_DrawAtPosition(startPosition, "  0");

        oC_TGUI_RestoreAttributes();

        Context->GraphBorder.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,GraphBorderRefreshTime);
    }

}

//==========================================================================================================================================
/**
 * @brief draws a graph
 */
//==========================================================================================================================================
static void DrawGraph( oC_Service_Context_t Context )
{
    oC_TGUI_Position_t  position , endPosition, lastPosition;
    oC_TGUI_Column_t    column;
    uint32_t            cpuLoadIndex        = Context->PutCpuLoadIndex;
    double              dy                  = 100.0 / ((double)GraphStyle.Height - 1);
    double              cpuLoad             = 0;
    bool                realLinesSupported  = oC_TGUI_DoesSupportRealLines();

    if(oC_DynamicConfig_GetValue(syssprint,GraphEnabled) && Context->Graph.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes())
    {
        oC_TGUI_SetStyle(&GraphStyle.DotsStyle);
        oC_TGUI_SetFont(oC_TGUI_Font_G0);

        position.Column     = 5 + GraphStyle.Position.Column;
        position.Line       = GraphStyle.Position.Line;
        endPosition.Column  = GraphStyle.Position.Column + GraphStyle.Width  - 1;
        endPosition.Line    = GraphStyle.Position.Line   + GraphStyle.Height - 1;

        oC_TGUI_DrawFillBetween(position,endPosition,' ');

        lastPosition.Column = 0;
        lastPosition.Line   = 0;

        for(column = GraphStyle.Width - 1; column >= 5; column--, cpuLoadIndex--)
        {
            if(cpuLoadIndex >= NUMBER_OF_CPU_LOAD_MEASUREMENTS)
            {
                cpuLoadIndex = 0;
            }

            cpuLoad = Context->CpuLoadArray[cpuLoadIndex];

            position.Column     = column + GraphStyle.Position.Column;
            position.Line       = floor((100.0 - cpuLoad) / dy) + GraphStyle.Position.Line;

            if(realLinesSupported)
            {
                if(lastPosition.Column != 0 && lastPosition.Line != 0)
                {
                    oC_TGUI_DrawRealLine(lastPosition,position);
                }
            }
            else
            {
                oC_TGUI_DrawCharAtPosition(position,GraphStyle.DotsChar);
            }

            lastPosition.Column = position.Column;
            lastPosition.Line   = position.Line;
        }

        oC_TGUI_RestoreAttributes();

        Context->Graph.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,GraphRefreshTime);
    }

}

//==========================================================================================================================================
/**
 * @brief draws memory information
 */
//==========================================================================================================================================
static void DrawRamInfo( oC_Service_Context_t Context )
{
    if(oC_DynamicConfig_GetValue(syssprint,RamInfoEnabled) && Context->RamInfo.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes())
    {
        oC_MemorySize_t freeRamSize = oC_MemMan_GetFreeRamSize();
        int32_t ramChange = (int32_t)(oC_ABS(Context->LastFreeRamSize,freeRamSize));

        if(Context->LastFreeRamSize > freeRamSize)
        {
            ramChange = -ramChange;
        }

        oC_TGUI_Property_t  properties[] = {
                        { .Description = "RAM Size: "               , .Format = "%d", .ValueUINT = oC_MemMan_GetRamSize()                 } ,
                        { .Description = "External Heap Size: "     , .Format = "%d", .ValueUINT = oC_MemMan_GetExternalHeapSize()        } ,
                        { .Description = "DMA Heap Size: "          , .Format = "%d", .ValueUINT = oC_MemMan_GetDmaRamHeapSize()          } ,
                        { .Description = "Free RAM Size: "          , .Format = "%d", .ValueUINT = freeRamSize                            } ,
                        { .Description = "Free RAM change: "        , .Format = "%d", .ValueI32  = ramChange                              } ,
        };
        oC_TGUI_DrawPropertyConfig_t propertyConfig = {
                        .StartPosition          = RamInfoPosition ,
                        .Properties             = properties ,
                        .NumberOfProperties     = oC_ARRAY_SIZE(properties) ,
                        .Style                  = &RamInfoPropertyStyle ,
                        .DescriptionWidth       = 20,
                        .Width                  = RamInfoWidth ,
                        .Height                 = RamInfoHeight ,
        };
        Context->LastFreeRamSize = freeRamSize;

        oC_TGUI_SetFont(oC_TGUI_Font_G0);

        oC_TGUI_DrawProperties(&propertyConfig);

        oC_TGUI_RestoreAttributes();

        Context->RamInfo.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,RamInfoRefreshTime);
    }
}

//==========================================================================================================================================
/**
 * @brief draws system news
 */
//==========================================================================================================================================
static void DrawNews( oC_Service_Context_t Context )
{
    if(oC_DynamicConfig_GetValue(syssprint,NewsEnabled) && Context->News.NextRefreshTimestamp <= gettimestamp() && oC_TGUI_SaveAttributes())
    {
        oC_TGUI_Column_t   stringWidth      = oC_MIN(sizeof(Context->LastNews),SystemNewsWidth);
        bool               drawTimestamp    = false;
        bool               newsRead         = false;

        if(stringWidth > 14)
        {
            stringWidth     -= 14;
            drawTimestamp    = true;
        }

        while(oC_News_Read(Context->LastNews,oC_MIN(sizeof(Context->LastNews),stringWidth),&Context->NewsTimestamp))
        {
            newsRead = true;
        }

        if(newsRead)
        {
            oC_TGUI_Position_t drawPosition;

            if(drawTimestamp)
            {
                oC_Time_Divided_t divided;

                drawPosition.Column   = SystemNewsPosition.Column;
                drawPosition.Line     = SystemNewsPosition.Line;

                oC_TGUI_SetFont(oC_TGUI_Font_G0);
                oC_TGUI_SetStyle(&SystemNewsTimestampStyle);

                oC_Time_Divide( Context->NewsTimestamp, &divided );
                oC_TGUI_DrawFormatAtPosition(drawPosition, "%3dd %02d:%02d:%02d ", divided.Days, divided.Hours, divided.Minutes, divided.Seconds);

                oC_TGUI_SetFont(oC_TGUI_Font_G1);
                oC_TGUI_SetStyle(&SystemNewsStyle);

                drawPosition.Column  += 14;

                oC_TGUI_DrawAtPositionWithSize(drawPosition, Context->LastNews, stringWidth);
            }
            else
            {
                oC_TGUI_DrawAtPositionWithSize(SystemNewsPosition, Context->LastNews, stringWidth);
            }

            oC_TGUI_RestoreAttributes();
        }

        Context->News.NextRefreshTimestamp = gettimestamp() + oC_DynamicConfig_GetValue(syssprint,NewsRefreshTime);
    }
}

//==========================================================================================================================================
/**
 * @brief main function thread
 */
//==========================================================================================================================================
static oC_ErrorCode_t MainFunction( oC_Service_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    sleep(s(2));

    while(1)
    {
        sleep( oC_DynamicConfig_GetValue(syssprint,SleepTime) );

        oC_TGUI_SetLineWrapping(false);

        DrawClock(Context);
        DrawCpuLoad(Context);
        DrawShortNetInfo(Context);
        DrawGraph(Context);
        DrawGraphBorders(Context);
        DrawSystemVersion(Context);
        DrawRamInfo(Context);
        DrawNews(Context);
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________



