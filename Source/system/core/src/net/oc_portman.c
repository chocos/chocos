/** ****************************************************************************************************************************************
 *
 * @brief      Interface functions for PortManager
 *
 * @file       oc_portman.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_portman.h>
#include <oc_list.h>
#include <oc_intman.h>
#include <oc_mutex.h>
#include <oc_event.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores port reservation
 */
//==========================================================================================================================================
typedef struct
{
    oC_PortMan_Port_t           Port;       //!< Reserved port
    oC_Process_t                Process;    //!< Process that has reserved the port
} PortReservation_t;

//==========================================================================================================================================
/**
 * @brief stores module registration
 */
//==========================================================================================================================================
typedef struct
{
    oC_Module_t                     Module;             //!< Module ID
    oC_Event_t                      PortReleasedEvent;  //!< Port Released Event
    oC_PortMan_Config_t             Config;             //!< Configuration of the module
    oC_List(PortReservation_t*)     PortList;           //!< List of ports reservations
} ModuleRegistration_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static ModuleRegistration_t*    ModuleRegistration_New      ( oC_Module_t Module , const oC_PortMan_Config_t * Config );
static bool                     ModuleRegistration_Delete   ( ModuleRegistration_t * Registration );
static ModuleRegistration_t*    GetModuleRegistration       ( oC_Module_t Module );
static PortReservation_t*       GetPortReservation          ( ModuleRegistration_t * Registration , oC_PortMan_Port_t Port );
static PortReservation_t*       GetNextProcessReservation   ( ModuleRegistration_t * Registration , oC_Process_t Process );
static oC_PortMan_Port_t        GetFreePort                 ( ModuleRegistration_t * Registration );
static bool                     WaitForFreePortRelease      ( ModuleRegistration_t * Registration , oC_PortMan_Port_t * Port , oC_Time_t Timeout );
static bool                     WaitForPortRelease          ( ModuleRegistration_t * Registration , oC_PortMan_Port_t * Port , oC_Time_t Timeout );
static PortReservation_t*       PortReservation_New         ( oC_PortMan_Port_t Port );
static bool                     PortReservation_Delete      ( PortReservation_t * Reservation );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_Mutex_t                           ModuleBusy              = NULL;
static oC_List(ModuleRegistration_t*)       RegistrationsList       = NULL;
static oC_Allocator_t                       Allocator               = {
                .Name = "PortMan"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function turns on the module and initialize it to work.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleIsTurnedOn                | The module is already turned on
 *  oC_ErrorCode_PermissionDenied                | Only root user can enable the module
 *  oC_ErrorCode_AllocationError                 | There was a problem with memory allocation
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_PortMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_PortMan))
    {
        if(ErrorCondition( iscurroot() , oC_ErrorCode_PermissionDenied ))
        {
            RegistrationsList = oC_List_New( &Allocator, AllocationFlags_Default );
            ModuleBusy        = oC_Mutex_New( oC_Mutex_Type_Recursive, &Allocator, AllocationFlags_Default );

            if(
                ErrorCondition( RegistrationsList != NULL, oC_ErrorCode_AllocationError )
             && ErrorCondition( ModuleBusy        != NULL, oC_ErrorCode_AllocationError )
                )
            {
                oC_Module_TurnOn(oC_Module_PortMan);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                bool listDeleted    = RegistrationsList == NULL || oC_List_Delete( RegistrationsList, AllocationFlags_Default );
                bool mutexDeleted   = ModuleBusy        == NULL || oC_Mutex_Delete( &ModuleBusy, AllocationFlags_Default );

                oC_SaveIfFalse( "PortMan::TurnOn list - "  , listDeleted  , oC_ErrorCode_ReleaseError );
                oC_SaveIfFalse( "PortMan::TurnOn mutex - " , mutexDeleted , oC_ErrorCode_ReleaseError );
            }
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off the module
 *
 * The function is for turning off the module.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_PermissionDenied                | Only root user can turn off the module
 *  oC_ErrorCode_ReleaseError                    | There was a problem with releasing the memory
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_PortMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_PortMan))
    {
        if(ErrorCondition( iscurroot() , oC_ErrorCode_PermissionDenied ))
        {
            bool allModulesDeleted = true;

            oC_Module_TurnOff(oC_Module_PortMan);

            foreach(RegistrationsList, registration)
            {
                allModulesDeleted = ModuleRegistration_Delete(registration) && allModulesDeleted;
            }

            bool listDeleted  = oC_List_Delete( RegistrationsList, AllocationFlags_Default );
            bool mutexDeleted = oC_Mutex_Delete( &ModuleBusy, AllocationFlags_Default );

            if(ErrorCondition( listDeleted && mutexDeleted && allModulesDeleted, oC_ErrorCode_ReleaseError ))
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief registers module in the port manager
 *
 * Each module, that uses the `Port Manager` for ports reservations has to register itself by using this function. It allocates memory required
 * for handling reservations, creates required objects and puts the registration into the registrations list.
 *
 * @warning
 * Only root user can register modules!
 *
 * @param Module            ID of the module to register
 * @param Config            Pointer to the structure with configuration of the module to register.
 * @param Timeout           Maximum time to wait for the registration
 *
 * Here is an example of usage:
 * @code{.c}
   static const oC_PortMan_Config_t config = {
        .MaximumPortNumber          = oC_uint16_MAX,
        .FirstDynamicPortNumber     = 1024 , // The first port number, that can be used as random port
        .LastDynamicPortNumber      = 2048   // The last port number, that can be used as random port
   };

   oC_SaveIfErrorOccur("Example module registration ", oC_PortMan_RegisterModule(oC_Module_ExampleModule, &config, s(1)));
   @endcode
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_PermissionDenied                | Only root user can register modules
 *  oC_ErrorCode_ModuleNotCorrect                | The given `Module` is not correct (please check if it is defined in the oc_module.h file
 *  oC_ErrorCode_WrongAddress                    | Address of the `Config` parameter is not correct
 *  oC_ErrorCode_MaximumValueNotCorrect          | `Config->MaximumPortNumber` cannot be 0!
 *  oC_ErrorCode_PortNotCorrect                  | `Config->FirstDynamicPortNumber` or `Config->LastDynamicPortNumber` is incorrect
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below 0
 *  oC_ErrorCode_ModuleAlreadyRegistered         | The given module has been registered before
 *  oC_ErrorCode_ModuleIsBusy                    | Maximum time for the operation has expired and the module is still busy
 *  oC_ErrorCode_AllocationError                 | There is a problem with memory allocation
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_PortMan_RegisterModule( oC_Module_t Module , const oC_PortMan_Config_t * Config , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_PortMan))
    {
        if(
            ErrorCondition( iscurroot()                                                         , oC_ErrorCode_PermissionDenied         )
         && ErrorCondition( Module < oC_Module_NumberOfModules                                  , oC_ErrorCode_ModuleNotCorrect         )
         && ErrorCondition( isaddresscorrect(Config)                                            , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( Config->MaximumPortNumber > 0                                       , oC_ErrorCode_MaximumValueNotCorrect   )
         && ErrorCondition( Config->FirstDynamicPortNumber > 0                                  , oC_ErrorCode_PortNotCorrect           )
         && ErrorCondition( Config->FirstDynamicPortNumber <= Config->MaximumPortNumber         , oC_ErrorCode_PortNotCorrect           )
         && ErrorCondition( Config->LastDynamicPortNumber  >  Config->FirstDynamicPortNumber    , oC_ErrorCode_PortNotCorrect           )
         && ErrorCondition( Config->LastDynamicPortNumber  <= Config->MaximumPortNumber         , oC_ErrorCode_PortNotCorrect           )
         && ErrorCondition( Timeout >= 0                                                        , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( GetModuleRegistration(Module) == NULL                               , oC_ErrorCode_ModuleAlreadyRegistered  )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout)                                   , oC_ErrorCode_ModuleIsBusy             )
            )
        {
            ModuleRegistration_t * registration = ModuleRegistration_New( Module, Config );

            if(ErrorCondition( registration , oC_ErrorCode_AllocationError ))
            {
                bool registrationAdded = oC_List_PushBack(RegistrationsList,registration,&Allocator);

                if(ErrorCondition(registrationAdded, oC_ErrorCode_CannotAddObjectToList))
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    oC_SaveIfFalse("PortMan::RegisterModule ", ModuleRegistration_Delete(registration), oC_ErrorCode_ReleaseError);
                }
            }

            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unregisters module in the port manager
 *
 * Each module, that uses the `Port Manager` for ports reservations has to register itself by using #oC_PortMan_RegisterModule function.
 * When the module is not needed anymore, it should unregister itself in the Port Manager by using this function.
 *
 * @warning
 * Only root user can register modules!
 *
 * @param Module            ID of the module to unregister
 * @param Timeout           Maximum time to wait for the unregistration
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_PermissionDenied                | Only root user can unregister modules
 *  oC_ErrorCode_ModuleNotCorrect                | The given `Module` is not correct (please check if it is defined in the oc_module.h file
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below 0
 *  oC_ErrorCode_ModuleNotRegistered             | The given module has not been registered before
 *  oC_ErrorCode_ModuleIsBusy                    | Maximum time for the operation has expired and the module is still busy
 *  oC_ErrorCode_ReleaseError                    | There is a problem with memory releasing
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_PortMan_UnregisterModule( oC_Module_t Module , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode       = oC_ErrorCode_ImplementError;
    ModuleRegistration_t*   registration    = NULL;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_PortMan))
    {
        if(
            ErrorCondition( iscurroot()                                                     , oC_ErrorCode_PermissionDenied         )
         && ErrorCondition( Module < oC_Module_NumberOfModules                              , oC_ErrorCode_ModuleNotCorrect         )
         && ErrorCondition( Timeout >= 0                                                    , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( (registration = GetModuleRegistration(Module)) != NULL          , oC_ErrorCode_ModuleNotRegistered      )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout)                               , oC_ErrorCode_ModuleIsBusy             )
            )
        {
            bool removedFromList = oC_List_RemoveAll(RegistrationsList,registration);
            bool deleted         = ModuleRegistration_Delete(registration);

            if(
                ErrorCondition( removedFromList , oC_ErrorCode_CannotRemoveObjectFromList   )
             && ErrorCondition( deleted         , oC_ErrorCode_ReleaseError                 )
                )
            {
                errorCode = oC_ErrorCode_None;
            }

            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reserves a port of the given module
 *
 * The function reserves a port of the given module. The module has to be registered before by the function #oC_PortMan_RegisterModule.
 * If the `Port` argument will point to 0, then the port manager will try to find a first available dynamic port of the given module, that
 * matches ID from the configuration of module registration (value between `FirstDynamicPortNumber` and `LastDynamicPortNumber`)
 *
 * @param Module        Registered module ID
 * @param Port          Address to the port on input or address to zero if you want to find a first free port
 * @param Timeout       Maximum time for operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_ModuleNotCorrect                | The given `Module` is not correct (please check if it is defined in the oc_module.h file
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below 0
 *  oC_ErrorCode_ModuleNotRegistered             | The given module has not been registered before
 *  oC_ErrorCode_ModuleIsBusy                    | Maximum time for the operation has expired and the module is still busy
 *  oC_ErrorCode_AllocationError                 | There is a problem with memory allocation
 *  oC_ErrorCode_PortNotCorrect                  | The given port is too big for the given module
 *  oC_ErrorCode_PortBusy                        | The specified port or all ports (if the `Port` points to 0) are reserved
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_PortMan_ReservePort( oC_Module_t Module , oC_PortMan_Port_t * Port , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode       = oC_ErrorCode_ImplementError;
    ModuleRegistration_t*   registration    = NULL;
    oC_Timestamp_t          endTimestamp    = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_PortMan))
    {
        if(
            ErrorCondition( Module < oC_Module_NumberOfModules                                                  , oC_ErrorCode_ModuleNotCorrect      )
         && ErrorCondition( isram(Port)                                                                         , oC_ErrorCode_AddressNotInRam       )
         && ErrorCondition( Timeout >= 0                                                                        , oC_ErrorCode_TimeNotCorrect        )
         && ErrorCondition( (registration = GetModuleRegistration(Module)) != NULL                              , oC_ErrorCode_ModuleNotRegistered   )
         && ErrorCondition( (*Port) <= registration->Config.MaximumPortNumber                                   , oC_ErrorCode_PortNotCorrect        )
         && ErrorCondition( WaitForPortRelease(registration, Port, oC_KTime_CalculateTimeout(endTimestamp))     , oC_ErrorCode_PortBusy              )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp))                  , oC_ErrorCode_ModuleIsBusy          )
            )
        {
            PortReservation_t * reservation = PortReservation_New(*Port);

            if(ErrorCondition( reservation != NULL , oC_ErrorCode_AllocationError ))
            {
                bool pushed = oC_List_PushBack(registration->PortList, reservation, &Allocator);

                if(ErrorCondition( pushed , oC_ErrorCode_CannotAddObjectToList ))
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    oC_SaveIfFalse("PortMan::ReservePort - ", PortReservation_Delete(reservation), oC_ErrorCode_ReleaseError);
                }
            }

            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases port
 *
 * The function is for releasing a port that has been reserved by the function #oC_PortMan_ReservePort.
 *
 * @param Module            Module of the port to release
 * @param Port              Port to release
 * @param Timeout           Maximum time to wait for the port manager
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_ModuleNotCorrect                | The given `Module` is not correct (please check if it is defined in the oc_module.h file
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below 0
 *  oC_ErrorCode_ModuleNotRegistered             | The given module has not been registered before
 *  oC_ErrorCode_ModuleIsBusy                    | Maximum time for the operation has expired and the module is still busy
 *  oC_ErrorCode_ReleaseError                    | There is a problem with memory releasing
 *  oC_ErrorCode_CannotRemoveObjectFromList      | Internal error related with removing port reservation from list
 *  oC_ErrorCode_PortNotCorrect                  | The given port is too big for the given module
 *  oC_ErrorCode_PortNotReserved                 | The specified port has not been reserved
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_PortMan_ReleasePort( oC_Module_t Module , oC_PortMan_Port_t Port , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode       = oC_ErrorCode_ImplementError;
    ModuleRegistration_t*   registration    = NULL;
    PortReservation_t *     reservation     = NULL;
    oC_Timestamp_t          endTimestamp    = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_PortMan))
    {
        if(
            ErrorCondition( Module < oC_Module_NumberOfModules                                  , oC_ErrorCode_ModuleNotCorrect                 )
         && ErrorCondition( Timeout >= 0                                                        , oC_ErrorCode_TimeNotCorrect                   )
         && ErrorCondition( (registration = GetModuleRegistration(Module)) != NULL              , oC_ErrorCode_ModuleNotRegistered              )
         && ErrorCondition( (reservation  = GetPortReservation(registration,Port)) != NULL      , oC_ErrorCode_PortNotReserved                  )
         && ErrorCondition( Port <= registration->Config.MaximumPortNumber                      , oC_ErrorCode_PortNotCorrect                   )
         && ErrorCondition( reservation->Process == getcurprocess()                             , oC_ErrorCode_PortReservedByDifferentProcess   )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ModuleIsBusy                     )
            )
        {
            bool removedFromList = oC_List_RemoveAll(registration->PortList,reservation);
            bool deleted         = PortReservation_Delete(reservation);

            if(
                ErrorCondition( removedFromList , oC_ErrorCode_CannotRemoveObjectFromList )
             && ErrorCondition( deleted         , oC_ErrorCode_ReleaseError               )
                )
            {
                oC_Event_SetState(registration->PortReleasedEvent,Port);
                errorCode = oC_ErrorCode_None;
            }
            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases all ports reserved by a given process
 *
 * The function releases all ports that has been reserved by the given process. It is designed to provide a possibility to quickly and easy
 * close a process by the kernel.
 *
 * @param Module        Index of the module registered earlier
 * @param Process       Process to release
 * @param Timeout       Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_ModuleNotCorrect                | The given `Module` is not correct (please check if it is defined in the oc_module.h file
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below 0
 *  oC_ErrorCode_ModuleNotRegistered             | The given module has not been registered before
 *  oC_ErrorCode_ModuleIsBusy                    | Maximum time for the operation has expired and the module is still busy
 *  oC_ErrorCode_ReleaseError                    | There is a problem with memory releasing
 *  oC_ErrorCode_CannotRemoveObjectFromList      | Internal error related with removing port reservation from list
 *
 *  The function can also return an error code from the function #oC_PortMan_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_PortMan_ReleaseAllPortsOf( oC_Module_t Module , oC_Process_t Process , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode       = oC_ErrorCode_ImplementError;
    ModuleRegistration_t*   registration    = NULL;
    oC_Timestamp_t          endTimestamp    = oC_KTime_GetTimestamp() + Timeout;
    PortReservation_t *     reservation     = NULL;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_PortMan))
    {
        if(
            ErrorCondition( Module < oC_Module_NumberOfModules                                  , oC_ErrorCode_ModuleNotCorrect      )
         && ErrorCondition( Timeout >= 0                                                        , oC_ErrorCode_TimeNotCorrect        )
         && ErrorCondition( iscurroot()                                                         , oC_ErrorCode_PermissionDenied      )
         && ErrorCondition( (registration = GetModuleRegistration(Module)) != NULL              , oC_ErrorCode_ModuleNotRegistered   )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ModuleIsBusy          )
            )
        {
            errorCode = oC_ErrorCode_None;

            while((reservation = GetNextProcessReservation(registration,Process)) != NULL )
            {
                reservation->Process = getcurprocess();

                ErrorCode( oC_PortMan_ReleasePort( Module, reservation->Port, oC_KTime_CalculateTimeout(endTimestamp)) );
            }

            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns true if port is reserved
 *
 * The function checks if the given port is reserved at the given module. '
 *
 * @param Module    Module registered earlier
 * @param Port      Port to check
 *
 * @return true if the port is reserved
 */
//==========================================================================================================================================
bool oC_PortMan_IsPortReserved( oC_Module_t Module , oC_PortMan_Port_t Port )
{
    bool reserved = false;

    if(oC_SaveIfFalse("PortMan::IsPortReserved ", oC_Module_IsTurnedOn(oC_Module_PortMan), oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleRegistration_t* registration = NULL;

        if(
            oC_SaveIfFalse("PortMan::IsPortReserved ", Module < oC_Module_NumberOfModules                       , oC_ErrorCode_ModuleNotCorrect     )
         && oC_SaveIfFalse("PortMan::IsPortReserved ", (registration = GetModuleRegistration(Module)) != NULL   , oC_ErrorCode_ModuleNotRegistered  )
         && oC_SaveIfFalse("PortMan::IsPortReserved ", Port <= registration->Config.MaximumPortNumber           , oC_ErrorCode_PortNotCorrect       )
         && oC_SaveIfFalse("PortMan::IsPortReserved ", Port >  0                                                , oC_ErrorCode_PortNotCorrect       )
            )
        {
            reserved = GetPortReservation(registration,Port) != NULL;
        }
    }

    return reserved;
}

//==========================================================================================================================================
/**
 * @brief checks if the given port is reserved by the given process
 *
 * The function is for checking if the given port is reserved by the given process.
 *
 * @param Module        Registered module
 * @param Port          Port of the module to check
 * @param Process       Process object to check
 *
 * @return true if port is reserved by the given process
 */
//==========================================================================================================================================
bool oC_PortMan_IsPortReservedBy( oC_Module_t Module , oC_PortMan_Port_t Port , oC_Process_t Process )
{
    bool reserved = false;

    if(oC_SaveIfFalse("PortMan::IsPortReservedBy ", oC_Module_IsTurnedOn(oC_Module_PortMan), oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleRegistration_t* registration = NULL;

        if(
            oC_SaveIfFalse("PortMan::IsPortReservedBy ", Module < oC_Module_NumberOfModules                       , oC_ErrorCode_ModuleNotCorrect     )
         && oC_SaveIfFalse("PortMan::IsPortReservedBy ", (registration = GetModuleRegistration(Module)) != NULL   , oC_ErrorCode_ModuleNotRegistered  )
         && oC_SaveIfFalse("PortMan::IsPortReservedBy ", Port <= registration->Config.MaximumPortNumber           , oC_ErrorCode_PortNotCorrect       )
         && oC_SaveIfFalse("PortMan::IsPortReservedBy ", Port >  0                                                , oC_ErrorCode_PortNotCorrect       )
            )
        {
            PortReservation_t * reservation = GetPortReservation(registration,Port);

            if(oC_SaveIfFalse("PortMan::IsPortReservedBy ", reservation , oC_ErrorCode_PortNotReserved))
            {
                reserved = reservation->Process == Process;
            }
        }
    }

    return reserved;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief allocates memory for new module registration
 */
//==========================================================================================================================================
static ModuleRegistration_t* ModuleRegistration_New( oC_Module_t Module , const oC_PortMan_Config_t * Config )
{
    ModuleRegistration_t * registration = kmalloc( sizeof(ModuleRegistration_t), &Allocator, AllocationFlags_ZeroFill );

    if(registration != NULL)
    {
        registration->PortReleasedEvent = oC_Event_New(oC_Event_State_Inactive, &Allocator, AllocationFlags_Default);
        registration->PortList          = oC_List_New( &Allocator, AllocationFlags_Default );

        if(registration->PortReleasedEvent != NULL && registration->PortList != NULL)
        {
            registration->Module    = Module;

            memcpy(&registration->Config, Config, sizeof(oC_PortMan_Config_t));
        }
        else
        {
            bool eventDeleted           = registration->PortReleasedEvent == NULL || oC_Event_Delete(&registration->PortReleasedEvent, AllocationFlags_Default);
            bool listDeleted            = registration->PortList          == NULL || oC_List_Delete (registration->PortList          , AllocationFlags_Default);
            bool registrationDeleted    = kfree( registration, AllocationFlags_Default );

            oC_SaveIfFalse("PortMan::ModuleRegistration_New ", eventDeleted && listDeleted && registrationDeleted, oC_ErrorCode_ReleaseError);

            registration = NULL;
        }
    }

    return registration;
}

//==========================================================================================================================================
/**
 * @brief releases memory for the given module registration
 */
//==========================================================================================================================================
static bool ModuleRegistration_Delete( ModuleRegistration_t * Registration )
{
    bool eventDeleted           = oC_Event_Delete(&Registration->PortReleasedEvent, AllocationFlags_Default);
    bool listDeleted            = oC_List_Delete(Registration->PortList, AllocationFlags_Default);
    bool registrationDeleted    = kfree( Registration, AllocationFlags_Default );

    return eventDeleted && listDeleted && registrationDeleted;
}

//==========================================================================================================================================
/**
 * @brief looking for a registration of the given module
 */
//==========================================================================================================================================
static ModuleRegistration_t* GetModuleRegistration( oC_Module_t Module )
{
    ModuleRegistration_t * foundRegistration = NULL;

    foreach( RegistrationsList, registration )
    {
        if(registration->Module == Module)
        {
            foundRegistration = registration;
            break;
        }
    }

    return foundRegistration;
}
//==========================================================================================================================================
/**
 * @brief looking for a port reservation in the given module registration
 */
//==========================================================================================================================================
static PortReservation_t* GetPortReservation( ModuleRegistration_t * Registration , oC_PortMan_Port_t Port )
{
    PortReservation_t * foundReservation = NULL;

    foreach(Registration->PortList, reservation)
    {
        if(Port == reservation->Port)
        {
            foundReservation = reservation;
            kdebuglog(oC_LogType_Track, "Port %d is used by %s\n", Port, oC_Process_GetName(foundReservation->Process));
            break;
        }
    }

    return foundReservation;
}

//==========================================================================================================================================
/**
 * @brief looking for the next reservation of the port that is related with the given process
 */
//==========================================================================================================================================
static PortReservation_t* GetNextProcessReservation( ModuleRegistration_t * Registration , oC_Process_t Process )
{
    PortReservation_t * foundReservation = NULL;

    foreach(Registration->PortList, reservation)
    {
        if(Process == reservation->Process)
        {
            foundReservation = reservation;
            break;
        }
    }

    return foundReservation;
}

//==========================================================================================================================================
/**
 * @brief returns first free dynamic port
 */
//==========================================================================================================================================
static oC_PortMan_Port_t GetFreePort( ModuleRegistration_t * Registration )
{
    oC_PortMan_Port_t freePort = 0;

    for( oC_PortMan_Port_t port = Registration->Config.FirstDynamicPortNumber; port < Registration->Config.LastDynamicPortNumber; port++ )
    {
        if(GetPortReservation(Registration,port) == NULL)
        {
            freePort = port;
            break;
        }
    }

    /* If we still don't have a free port, we can try also the last dynamic port number (it is not in the loop to protect
     * against infinit loops ) */
    if(freePort == 0)
    {
        if(GetPortReservation(Registration,Registration->Config.LastDynamicPortNumber) == NULL)
        {
            freePort = Registration->Config.LastDynamicPortNumber;
        }
    }

    return freePort;
}

//==========================================================================================================================================
/**
 * @brief waits for release of free port
 */
//==========================================================================================================================================
static bool WaitForFreePortRelease( ModuleRegistration_t * Registration , oC_PortMan_Port_t * Port , oC_Time_t Timeout )
{
    bool             foundFreePort = false;
    oC_Timestamp_t   endTimestamp  = oC_KTime_GetTimestamp() + Timeout;
    oC_Event_State_t state         = 0;

    while(foundFreePort == false)
    {
        if(oC_Mutex_Take(ModuleBusy,oC_KTime_CalculateTimeout(endTimestamp)))
        {
            *Port = GetFreePort(Registration);

            oC_Mutex_Give(ModuleBusy);
            oC_Event_ReadState(Registration->PortReleasedEvent,&state);

            if(*Port != 0)
            {
                foundFreePort = true;
                break;
            }
        }
        else
        {
            oC_SaveError("WaitForFreePortRelease ", oC_ErrorCode_ModuleBusy);
            break;
        }
        if(!oC_Event_WaitForState(Registration->PortReleasedEvent, state, oC_Event_StateMask_DifferentThan, oC_KTime_CalculateTimeout(endTimestamp)))
        {
            oC_SaveError("WaitForFreePortRelease ", oC_ErrorCode_NoFreeSlots);
            break;
        }
    }

    return foundFreePort;
}

//==========================================================================================================================================
/**
 * @brief waits for a port release
 */
//==========================================================================================================================================
static bool WaitForPortRelease( ModuleRegistration_t * Registration , oC_PortMan_Port_t * Port , oC_Time_t Timeout )
{
    bool            portReady       = false;
    oC_Timestamp_t  endTimestamp    = oC_KTime_GetTimestamp() + Timeout;

    if(*Port == 0)
    {
        kdebuglog(oC_LogType_Info, "Waiting for free port for module 0x%08x...\n", Registration->Module);
        portReady = WaitForFreePortRelease(Registration, Port, oC_KTime_CalculateTimeout(endTimestamp));
        kdebuglog(oC_LogType_Info, "Port %u is available for module 0x%08X\n", *Port, Registration->Module);
    }
    else
    {
        kdebuglog(oC_LogType_Info, "Waiting for port %u for module 0x%08X\n", *Port, Registration->Module);
        while(portReady == false)
        {
            if(oC_SaveIfFalse("PortMan::WaitForPortRelease ", oC_Mutex_Take( ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp) ), oC_ErrorCode_ModuleBusy))
            {
                portReady = GetPortReservation(Registration, *Port) == NULL;
                oC_Mutex_Give(ModuleBusy);
            }
            else
            {
                kdebuglog(oC_LogType_Error, "The Port Manager module is busy by %s and the port %u cannot be reserved\n", oC_Mutex_GetBlockedName(ModuleBusy), *Port);
                break;
            }
            if(portReady == false)
            {
                if(!oC_Event_WaitForState(Registration->PortReleasedEvent, *Port, oC_Event_StateMask_Full, oC_KTime_CalculateTimeout(endTimestamp)))
                {
                    oC_SaveError("PortMan::WaitForPortRelease ", oC_ErrorCode_PortBusy);
                    break;
                }
            }
        }
        kdebuglog(oC_LogType_Error, "Port %u cannot be reserved for 0x%08X\n", *Port, Registration->Module);
    }

    return portReady;
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a port reservation
 */
//==========================================================================================================================================
static PortReservation_t* PortReservation_New( oC_PortMan_Port_t Port )
{
    PortReservation_t * reservation = kmalloc( sizeof(PortReservation_t), &Allocator, AllocationFlags_Default );

    if(reservation != NULL)
    {
        reservation->Port       = Port;
        reservation->Process    = getcurprocess();
    }

    return reservation;
}

//==========================================================================================================================================
/**
 * @brief releases memory of port reservation
 */
//==========================================================================================================================================
static bool PortReservation_Delete( PortReservation_t * Reservation )
{
    return kfree(Reservation,AllocationFlags_Default);
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

