/** ****************************************************************************************************************************************
 *
 * @brief      The file with source for the TCP connection interface
 * 
 * @file       oc_tcp_connection.c
 *
 * @author     Patryk Kubiak - (Created on: 22.01.2017 16:22:18) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_tcp.h>
#include <oc_thread.h>
#include <oc_ktime.h>
#include <oc_mutex.h>
#include <oc_math.h>
#include <oc_dynamic_config.h>
#include <oc_semaphore.h>
#include <oc_event.h>
#include <oc_intman.h>
#include <oc_netifman.h>
#include <oc_queue.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsConnected(C)              ((C)->State == State_Established)
#define IsDisconnected(C)           ((C)->State == State_Disconnecting || (C)->State == State_NotConnected)
#define IsCorrect(C)                (isram(C) && oC_CheckObjectControl(C,oC_ObjectId_TcpConnection,(C)->ObjectControl))

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    State_NotConnected   = 0 ,
    State_WaitForSyn     = 1 ,
    State_NeedsToAccept  = 3 ,
    State_Connecting     = 4 ,
    State_Established    = 5 ,
    State_Disconnecting  = 6 ,
} State_t;

typedef enum
{
    PredefinedPacket_SYN ,
    PredefinedPacket_SYN_ACK ,
    PredefinedPacket_SYN_RST ,
    PredefinedPacket_RST_ACK ,
    PredefinedPacket_ACK ,
    PredefinedPacket_FIN_ACK ,
    PredefinedPacket_FIN ,
    PredefinedPacket_NumberOfElements ,
} PredefinedPacket_t;

typedef struct
{
    union
    {
        struct
        {
    #if defined(LITTLE_ENDIAN)
            uint32_t                DataOffset:4;
            uint32_t                Reserved:3;
            uint32_t                NS:1;
            uint32_t                CWR:1;
            uint32_t                ECE:1;
            uint32_t                URG:1;
            uint32_t                ACK:1;
            uint32_t                PSH:1;
            uint32_t                RST:1;
            uint32_t                SYN:1;
            uint32_t                FIN:1;
    #elif defined(BIG_ENDIAN)
    #   error Structure is not defined for BIG_ENDIAN
    #else
    #   error Endianess is not defined
    #endif
        };
        struct
        {
    #if defined(LITTLE_ENDIAN)
            uint8_t         LowByte;
            uint8_t         HighByte;
    #elif defined(BIG_ENDIAN)
    #   error Structure is not defined for BIG_ENDIAN
    #else
    #   error Endianess is not defined
    #endif
        };

    };
    uint32_t        AddToSequence;
    const char *    Name;
} PacketFlags_t;

typedef oC_Tcp_Packet_t * Packet_t;

typedef struct
{
    union
    {
        uint8_t *           Buffer;
        const uint8_t *     ConstBuffer;
    };
    oC_MemorySize_t     Size;
    oC_MemorySize_t     ConfirmedBytes;
    union
    {
        oC_MemorySize_t     ReceivedBytes;
        oC_MemorySize_t     SentBytes;
    };
} Segment_t;

typedef oC_Tcp_ConnectionFunction_t ConnectionFinishedHandler_t;

//==========================================================================================================================================
/**
 * @brief stores TCP connection data
 */
//==========================================================================================================================================
struct Tcp_Connection_t
{
    oC_ObjectControl_t              ObjectControl;
    oC_DefaultString_t              Name;
    State_t                         State;
    oC_Process_t                    Process;
    oC_Thread_t                     ReceiveThread;
    oC_Tcp_Connection_Callbacks_t   Callbacks;

    // ///////////////////////////////////////////////////////////
    //                CONNECTION INFORMATION                    //
    // ///////////////////////////////////////////////////////////
    uint32_t                    InitialSequenceNumber;
    uint32_t                    RemoteInitialSequenceNumber;
    oC_Net_Address_t            LocalAddress;
    oC_Net_Address_t            RemoteAddress;

    // ///////////////////////////////////////////////////////////
    //                  TIMING PARAMETERS                       //
    // ///////////////////////////////////////////////////////////
    oC_Time_t                   ConfirmationTimeout;
    oC_Time_t                   ExpirationTimeout;
    oC_Time_t                   ReceiveTimeout;
    oC_Time_t                   SendingAcknowledgeTimeout;
    oC_Time_t                   ReadSegmentTimeout;
    oC_Time_t                   SaveSegmentTimeout;
    oC_Time_t                   ResendSegmentTime;
    oC_Timestamp_t              ExpirationTimestamp;

    // ///////////////////////////////////////////////////////////
    //             FIELDS REQUIRED FOR SENDING                  //
    // ///////////////////////////////////////////////////////////
    oC_Mutex_t                  ReadyForSendingMutex;
    oC_Mutex_t                  SendingSegmentMutex;
    oC_Event_t                  RemoteWindowSizeChangedEvent;
    Packet_t                    PacketToSend;
    uint16_t                    PacketSize;
    oC_MemorySize_t             RemoteWindowSize;
    uint8_t                     RemoteWindowScale;
    uint32_t                    NextSequenceNumberToSend;
    uint32_t                    LastConfirmedSequenceNumber;

    // ///////////////////////////////////////////////////////////
    //             FIELDS REQUIRED FOR RECEIVING                //
    // ///////////////////////////////////////////////////////////
    uint8_t                     LocalWindowScale;
    Segment_t                   SegmentToReceive;
    Packet_t                    LastReceivedPacket;
    oC_Mutex_t                  ReadyForReceivingMutex;
    oC_Semaphore_t              NewPacketReceivedSemaphore;
    oC_Event_t                  AcknowledgeReceivedEvent;
    uint32_t                    NextSequenceNumberToReceive;
    PacketFlags_t               ReceivedPacketFlags;
    bool                        AcknowledgePacketShouldBeSend;
    bool                        FinishConnectionPacketShouldBeSend;
    oC_Queue_t                  ReceiveQueue;

    // ///////////////////////////////////////////////////////////
    //             TRANSMISSION SPEED MEASUREMENT               //
    // ///////////////////////////////////////////////////////////
    oC_Timestamp_t              LastMeasureTimestamp;
    oC_BaudRate_t               BaudRate;
    oC_MemorySize_t             SentBytesFromLastMeasure;
};


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static oC_ErrorCode_t SendPredefinedPacket        ( oC_Tcp_Connection_t Connection , PredefinedPacket_t PredefinedPacket , oC_Time_t Timeout );
static oC_ErrorCode_t WaitForPredefinedPacket     ( oC_Tcp_Connection_t Connection , PredefinedPacket_t PredefinedPacket , oC_Time_t Timeout );
static oC_ErrorCode_t ReadConnectionDataFromPacket( oC_Tcp_Connection_t Connection , Packet_t Packet );
static oC_ErrorCode_t SendSegment                 ( oC_Tcp_Connection_t Connection , Segment_t * Segment , oC_Time_t Timeout  );
static bool           PacketFilterFunction        ( oC_Net_Packet_t * ReceivedPacket, const void * Parameter , oC_Netif_t Netif );
static void           ReceiveThread               ( oC_Tcp_Connection_t Connection );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with globals
 *
 *  ======================================================================================================================================*/
#define _________________________________________GLOBALS_SECTION____________________________________________________________________________

static const PacketFlags_t PredefinedPacketFlags[PredefinedPacket_NumberOfElements] = {
                [PredefinedPacket_SYN    ] = { .Name = "SYN"     , .AddToSequence = 1 , .DataOffset = 5, .SYN = 1  } ,
                [PredefinedPacket_SYN_ACK] = { .Name = "SYN ACK" , .AddToSequence = 1 , .DataOffset = 5, .SYN = 1 , .ACK = 1 } ,
                [PredefinedPacket_SYN_RST] = { .Name = "SYN RST" , .AddToSequence = 1 , .DataOffset = 5, .SYN = 1 , .RST = 1 } ,
                [PredefinedPacket_RST_ACK] = { .Name = "RST ACK" , .AddToSequence = 1 , .DataOffset = 5, .RST = 1 , .ACK = 1 } ,
                [PredefinedPacket_ACK    ] = { .Name = "ACK"     , .AddToSequence = 0 , .DataOffset = 5, .ACK = 1 } ,
                [PredefinedPacket_FIN_ACK] = { .Name = "FIN ACK" , .AddToSequence = 1 , .DataOffset = 5, .FIN = 1 , .ACK = 1 } ,
                [PredefinedPacket_FIN    ] = { .Name = "FIN"     , .AddToSequence = 1 , .DataOffset = 5, .FIN = 1 , .ACK = 1 } ,
};

#undef  _________________________________________GLOBALS_SECTION____________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief allocates memory for a new TCP connection object
 *
 * The function is designed for allocating a memory for a TCP connection object. The object is required for handling TCP connection with
 * the remote TCP client or server. The function initializes sub-objects required by the
 *
 * @param
 *
 * @return
 */
//==========================================================================================================================================
oC_Tcp_Connection_t oC_Tcp_Connection_New( const oC_Tcp_Connection_Config_t * Config )
{
    oC_Tcp_Connection_t connection = NULL;

    if(
        oC_SaveIfFalse( "Config"      , isaddresscorrect(Config)                       , oC_ErrorCode_WrongConfigAddress   )
     && oC_SaveIfFalse( "Local "      , oC_Net_IsAddressCorrect(&Config->LocalAddress) , oC_ErrorCode_IpAddressNotCorrect  )
     && oC_SaveIfFalse( "Remote "     , oC_Net_IsAddressCorrect(&Config->RemoteAddress), oC_ErrorCode_IpAddressNotCorrect  )
     && oC_SaveIfFalse( "Local Port"  , Config->LocalAddress.Port != oC_Tcp_Port_Empty , oC_ErrorCode_PortNotCorrect       )
     && oC_SaveIfFalse( "Confirmation", Config->ConfirmationTimeout >= 0               , oC_ErrorCode_TimeNotCorrect       )
     && oC_SaveIfFalse( "Expiration"  , Config->ExpirationTimeout >= 0                 , oC_ErrorCode_TimeNotCorrect       )
     && oC_SaveIfFalse( "ACK Time"    , Config->SendingAcknowledgeTimeout >= 0         , oC_ErrorCode_TimeNotCorrect       )
     && oC_SaveIfFalse( "Read Segment", Config->ReadSegmentTimeout >= 0                , oC_ErrorCode_TimeNotCorrect       )
     && oC_SaveIfFalse( "Packet Size" , Config->PacketSize  > 0                        , oC_ErrorCode_SizeNotCorrect       )
        )
    {
        connection = malloc( sizeof(struct Tcp_Connection_t), AllocationFlags_ZeroFill );

        if( oC_SaveIfFalse( "Main Object", connection != NULL, oC_ErrorCode_AllocationError ) )
        {
            oC_MemorySize_t stackSize  = oC_DynamicConfig_GetValue(Tcp,StackSize);
            Allocator_t     allocator  = getcurallocator();

            sprintf(connection->Name, "TCP:%08x", Config->InitialSequenceNumber);

            connection->ObjectControl                 = oC_CountObjectControl(connection,oC_ObjectId_TcpConnection);
            connection->State                         = State_NotConnected;
            connection->Process                       = getcurprocess();
            connection->ReceiveThread                 = oC_Thread_New( 0, stackSize , connection->Process, connection->Name, (oC_Thread_Function_t)ReceiveThread, connection);
            connection->InitialSequenceNumber         = Config->InitialSequenceNumber;
            connection->RemoteInitialSequenceNumber   = Config->InitialAcknowledgeNumber;
            memcpy(connection->Callbacks, Config->Callbacks, sizeof(connection->Callbacks));

            // ///////////////////////////////////////////////////////////////////////////// //
            //                       ADDRESS INITIALIZATION                                  //
            // ///////////////////////////////////////////////////////////////////////////// //
            connection->LocalAddress.Protocol         = oC_Net_Protocol_TCP;
            connection->LocalAddress.Type             = Config->LocalAddress.Type;
            connection->LocalAddress.IPv4             = Config->LocalAddress.IPv4;
            connection->LocalAddress.IPv6.LowPart     = Config->LocalAddress.IPv6.LowPart;
            connection->LocalAddress.IPv6.HighPart    = Config->LocalAddress.IPv6.HighPart;
            connection->LocalAddress.Port             = Config->LocalAddress.Port;

            connection->RemoteAddress.Protocol        = oC_Net_Protocol_TCP;
            connection->RemoteAddress.Type            = Config->RemoteAddress.Type;
            connection->RemoteAddress.IPv4            = Config->RemoteAddress.IPv4;
            connection->RemoteAddress.IPv6.LowPart    = Config->RemoteAddress.IPv6.LowPart;
            connection->RemoteAddress.IPv6.HighPart   = Config->RemoteAddress.IPv6.HighPart;
            connection->RemoteAddress.Port            = Config->RemoteAddress.Port;

            // ///////////////////////////////////////////////////////////////////////////// //
            //                          TIMING PARAMETERS                                    //
            // ///////////////////////////////////////////////////////////////////////////// //
            connection->ConfirmationTimeout           = Config->ConfirmationTimeout;
            connection->ExpirationTimeout             = Config->ExpirationTimeout;
            connection->SendingAcknowledgeTimeout     = Config->SendingAcknowledgeTimeout;
            connection->ReadSegmentTimeout            = Config->ReadSegmentTimeout;
            connection->SaveSegmentTimeout            = Config->SaveSegmentTimeout;
            connection->ReceiveTimeout                = Config->ReceiveTimeout;
            connection->ResendSegmentTime             = Config->ResendSegmentTime;
            connection->ExpirationTimestamp           = Config->ExpirationTimeout + gettimestamp();

            // ///////////////////////////////////////////////////////////////////////////// //
            //                          FIELDS REQUIRED FOR SENDING                          //
            // ///////////////////////////////////////////////////////////////////////////// //
            connection->ReadyForSendingMutex          = oC_Mutex_New(oC_Mutex_Type_Normal,allocator,0);
            connection->SendingSegmentMutex           = oC_Mutex_New(oC_Mutex_Type_Normal,allocator,0);
            connection->RemoteWindowSizeChangedEvent  = oC_Event_New(0,allocator,0);
            connection->PacketToSend                  = oC_Tcp_Packet_New(&connection->LocalAddress,&connection->RemoteAddress,sizeof(oC_Tcp_Header_t) + sizeof(uint32_t),NULL,Config->PacketSize);
            connection->PacketSize                    = Config->PacketSize;
            connection->RemoteWindowSize              = 0;
            connection->RemoteWindowScale             = 0;
            connection->NextSequenceNumberToSend      = Config->InitialSequenceNumber;

            // ///////////////////////////////////////////////////////////////////////////// //
            //                          FIELDS REQUIRED FOR RECEIVING                        //
            // ///////////////////////////////////////////////////////////////////////////// //
            connection->LocalWindowScale                    = Config->LocalWindowScale;
            connection->SegmentToReceive.Size               = Config->LocalWindowSize << Config->LocalWindowScale;
            connection->SegmentToReceive.Buffer             = malloc( connection->SegmentToReceive.Size, 0 );
            connection->SegmentToReceive.ReceivedBytes      = 0;
            connection->SegmentToReceive.ConfirmedBytes     = 0;
            connection->LastReceivedPacket                  = malloc( sizeof(oC_Tcp_Packet_t), AllocationFlags_ZeroFill );
            connection->ReadyForReceivingMutex              = oC_Mutex_New(oC_Mutex_Type_Normal, allocator, 0);
            connection->NewPacketReceivedSemaphore          = oC_Semaphore_New(oC_Semaphore_Type_Binary, 0, allocator, 0);
            connection->AcknowledgeReceivedEvent            = oC_Event_New(0,allocator,0);
            connection->NextSequenceNumberToReceive         = Config->InitialAcknowledgeNumber;
            connection->ReceivedPacketFlags.LowByte         = 0;
            connection->ReceivedPacketFlags.HighByte        = 0;
            connection->AcknowledgePacketShouldBeSend       = false;
            connection->FinishConnectionPacketShouldBeSend  = false;
            connection->ReceiveQueue                        = oC_Queue_New(allocator,connection->SegmentToReceive.Size);

            // ///////////////////////////////////////////////////////////////////////////// //
            //                          FIELDS REQUIRED FOR BAUD RATE                        //
            // ///////////////////////////////////////////////////////////////////////////// //
            connection->LastMeasureTimestamp            = gettimestamp();
            connection->BaudRate                        = 0;
            connection->SentBytesFromLastMeasure        = 0;

            if(
                oC_SaveIfFalse( "Thread"     , oC_Thread_IsCorrect    (connection->ReceiveThread)                   , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Mutex"      , oC_Mutex_IsCorrect     (connection->ReadyForSendingMutex)            , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Mutex"      , oC_Mutex_IsCorrect     (connection->SendingSegmentMutex)             , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Event"      , oC_Event_IsCorrect     (connection->RemoteWindowSizeChangedEvent)    , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Packet"     , connection->PacketToSend != NULL                                     , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Packet"     , connection->LastReceivedPacket != NULL                               , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Buffer"     , connection->SegmentToReceive.Buffer != NULL                          , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Mutex"      , oC_Mutex_IsCorrect     (connection->ReadyForReceivingMutex)          , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Semaphore"  , oC_Semaphore_IsCorrect (connection->NewPacketReceivedSemaphore)      , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Event"      , oC_Event_IsCorrect     (connection->AcknowledgeReceivedEvent)        , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Queue"      , oC_Queue_IsCorrect     (connection->ReceiveQueue)                    , oC_ErrorCode_AllocationError   )
             && oC_SaveIfFalse( "Run Thread" , oC_Thread_Run(connection->ReceiveThread)                             , oC_ErrorCode_CannotRunThread   )
                )
            {
                tcplog(oC_LogType_Track, "[%p] 0x%08X: New connection allocated", connection, Config->InitialSequenceNumber);
            }
            else
            {
                connection->ObjectControl = 0;
                oC_Thread_Delete    (&connection->ReceiveThread);
                oC_Mutex_Delete     (&connection->ReadyForSendingMutex , 0);
                oC_Mutex_Delete     (&connection->SendingSegmentMutex  , 0);
                oC_Event_Delete     (&connection->RemoteWindowSizeChangedEvent,0);
                oC_Tcp_Packet_Delete(&connection->PacketToSend);
                free(connection->LastReceivedPacket,0);
                free(connection->SegmentToReceive.Buffer,0);
                oC_Mutex_Delete     (&connection->ReadyForReceivingMutex,0);
                oC_Semaphore_Delete (&connection->NewPacketReceivedSemaphore,0);
                oC_Event_Delete     (&connection->AcknowledgeReceivedEvent,0);
                oC_Queue_Delete     (&connection->ReceiveQueue);
                free(connection,0);
                connection = NULL;
            }
        }
    }

    return connection;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Tcp_Connection_Delete( oC_Tcp_Connection_t * Connection , oC_Time_t Timeout )
{
    bool deleted = false;

    if(
        oC_SaveIfFalse( "Connection", isram(Connection)             , oC_ErrorCode_AddressNotInRam      )
     && oC_SaveIfFalse( "Connection", IsCorrect(*Connection)        , oC_ErrorCode_ObjectNotCorrect     )
     && oC_SaveIfFalse( "Timeout"   , Timeout >= 0                  , oC_ErrorCode_TimeNotCorrect       )
        )
    {
        oC_IntMan_EnterCriticalSection();
        oC_Tcp_Connection_t connection = *Connection;

        connection->ObjectControl = 0;
        uint32_t initialSequenceNumber               = connection->InitialSequenceNumber;
        bool deletedReceiveThread                    = oC_Thread_Delete    (&connection->ReceiveThread                         );
        bool deletedReadyForSendingMutex             = oC_Mutex_Delete     (&connection->ReadyForSendingMutex               , 0);
        bool deletedSendingSegmentMutex              = oC_Mutex_Delete     (&connection->SendingSegmentMutex                , 0);
        bool deletedRemoteWindowSizeChangedEvent     = oC_Event_Delete     (&connection->RemoteWindowSizeChangedEvent       , 0);
        bool deletedPacketToSend                     = oC_Tcp_Packet_Delete(&connection->PacketToSend                          );
        bool deletedBuffer                           = free(connection->SegmentToReceive.Buffer                             , 0);
        bool deletedReceiveBuffer                    = free(connection->LastReceivedPacket                                  , 0);
        bool deletedReadyForReceivingMutex           = oC_Mutex_Delete     (&connection->ReadyForReceivingMutex             , 0);
        bool deletedNewPacketReceivedSemaphore       = oC_Semaphore_Delete (&connection->NewPacketReceivedSemaphore         , 0);
        bool deletedAcknowledgeReceivedEvent         = oC_Event_Delete     (&connection->AcknowledgeReceivedEvent           , 0);
        bool deletedReceiveQueue                     = oC_Queue_Delete     (&connection->ReceiveQueue);
        bool deletedObject                           = free(connection,0);

        if(
            oC_SaveIfFalse( "ReceiveThread                      ",  deletedReceiveThread                    , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "ReadyForSendingMutex               ",  deletedReadyForSendingMutex             , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "SendingSegmentMutex                ",  deletedSendingSegmentMutex              , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "RemoteWindowSizeChangedEvent       ",  deletedRemoteWindowSizeChangedEvent     , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "PacketToSend                       ",  deletedPacketToSend                     , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "PacketToReceive                    ",  deletedReceiveBuffer                    , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "Buffer                             ",  deletedBuffer                           , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "ReadyForReceivingMutex             ",  deletedReadyForReceivingMutex           , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "NewPacketReceivedSemaphore         ",  deletedNewPacketReceivedSemaphore       , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "AcknowledgeReceivedEvent           ",  deletedAcknowledgeReceivedEvent         , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "ReceiveQueue"                       ,  deletedReceiveQueue                     , oC_ErrorCode_ReleaseError )
         && oC_SaveIfFalse( "Object"                             ,  deletedObject                           , oC_ErrorCode_ReleaseError )
            )
        {
            deleted = true;
        }
        tcplog(oC_LogType_Info, "[%p] 0x%08X Connection deleted\n", connection, initialSequenceNumber);
        *Connection = NULL;

        oC_IntMan_ExitCriticalSection();
    }

    return deleted;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Tcp_Connection_AreTheSame( oC_Tcp_Connection_t ConnectionA,  oC_Tcp_Connection_t ConnectionB )
{
    bool theSame = false;

    if(
        oC_SaveIfFalse( "Connection A", IsCorrect(ConnectionA) , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "Connection B", IsCorrect(ConnectionB) , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        theSame = ConnectionA->RemoteAddress.Port == ConnectionB->RemoteAddress.Port
               && oC_Net_AreAddressesTheSame(&ConnectionA->RemoteAddress, &ConnectionB->RemoteAddress);
    }

    return theSame;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Tcp_Connection_IsCorrect( oC_Tcp_Connection_t Connection )
{
    return isram(Connection) && oC_CheckObjectControl(Connection,oC_ObjectId_TcpConnection,Connection->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Tcp_Connection_IsConnected( oC_Tcp_Connection_t Connection )
{
    return IsCorrect(Connection) && IsConnected(Connection);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Tcp_Connection_IsDisconnected( oC_Tcp_Connection_t Connection )
{
    return IsCorrect(Connection) && IsDisconnected(Connection);
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Process_t oC_Tcp_Connection_GetProcess( oC_Tcp_Connection_t Connection )
{
    oC_Process_t process = NULL;

    if(oC_SaveIfFalse("Connection", IsCorrect(Connection), oC_ErrorCode_ObjectNotCorrect))
    {
        process = Connection->Process;
    }

    return process;
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_Tcp_Connection_GetName( oC_Tcp_Connection_t Connection )
{
    const char * name = "incorrect connection";

    if(oC_SaveIfFalse("connection", IsCorrect(Connection), oC_ErrorCode_ObjectNotCorrect))
    {
        name = Connection->Name;
    }

    return name;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_Connect( oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( IsCorrect(Connection)       , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( Timeout >= 0                , oC_ErrorCode_TimeNotCorrect   )
     && ErrorCondition( !IsConnected(Connection)    , oC_ErrorCode_AlreadyConnected )
        )
    {
        Connection->State = State_Connecting;

        while(ErrorCode( SendPredefinedPacket(Connection, PredefinedPacket_SYN, gettimeout(endTimestamp)) ))
        {
            if( ErrorCode( WaitForPredefinedPacket(Connection, PredefinedPacket_SYN_ACK, Connection->ConfirmationTimeout) ) )
            {
                if( ErrorCode( SendPredefinedPacket(Connection, PredefinedPacket_ACK    , gettimeout(endTimestamp)) ) )
                {
                    Connection->State   = State_Established;
                    errorCode           = oC_ErrorCode_None;
                }
                break;
            }
            else if(errorCode != oC_ErrorCode_Timeout)
            {
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_Disconnect( oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( IsCorrect(Connection)    , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( Timeout >= 0             , oC_ErrorCode_TimeNotCorrect   )
     && ErrorCondition( IsConnected(Connection)  , oC_ErrorCode_NotConnected     )
        )
    {
        tcplog( oC_LogType_Track, "%s: Sending disconnect packets...", Connection->Name );

        Connection->State = State_Disconnecting;

        if(
            ErrorCode( SendPredefinedPacket(Connection, PredefinedPacket_FIN_ACK, gettimeout(endTimestamp)) )
         && ErrorCode( SendPredefinedPacket(Connection, PredefinedPacket_ACK    , gettimeout(endTimestamp)) )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_WaitForConnection ( oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsCorrect(Connection)    , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( Timeout >= 0             , oC_ErrorCode_TimeNotCorrect       )
     && ErrorCondition( !IsConnected(Connection) , oC_ErrorCode_AlreadyConnected     )
        )
    {
        Connection->State = State_WaitForSyn;

        oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

        if(
            ErrorCode( WaitForPredefinedPacket( Connection, PredefinedPacket_SYN, gettimeout(endTimestamp) ))
            )
        {
            tcplog(oC_LogType_Info, "received connection from: %s\n", Connection->Name);
            Connection->State = State_NeedsToAccept;
            errorCode         = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_Accept( oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( IsCorrect(Connection)                    , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Timeout >= 0                             , oC_ErrorCode_TimeNotCorrect         )
     && ErrorCondition( Connection->State == State_NeedsToAccept , oC_ErrorCode_ConnectionNotRequested )
        )
    {
        oC_Time_t confirmationTimeout = Connection->ConfirmationTimeout;
        while(ErrorCode( SendPredefinedPacket(Connection, PredefinedPacket_SYN_ACK, gettimeout(endTimestamp)) ))
        {
            if(ErrorCode( WaitForPredefinedPacket(Connection, PredefinedPacket_ACK, confirmationTimeout) ))
            {
                tcplog( oC_LogType_Info, "%s: Connection has been accepted", Connection->Name );
                Connection->State   = State_Established;
                errorCode           = oC_ErrorCode_None;
                break;
            }
            else if(errorCode != oC_ErrorCode_Timeout)
            {
                Connection->NextSequenceNumberToReceive = Connection->RemoteInitialSequenceNumber + 1;
                Connection->NextSequenceNumberToSend    = Connection->InitialSequenceNumber;
                break;
            }
            else
            {
                confirmationTimeout *= 2;
            }
        }
        if(oC_ErrorOccur(errorCode))
        {
            tcplog(oC_LogType_Error, "Cannot accept TCP connection: %R", errorCode);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_Reject( oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( IsCorrect(Connection)                    , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Timeout >= 0                             , oC_ErrorCode_TimeNotCorrect         )
     && ErrorCondition( Connection->State == State_NeedsToAccept , oC_ErrorCode_ConnectionNotRequested )
        )
    {
        if( ErrorCode( SendPredefinedPacket(Connection, PredefinedPacket_RST_ACK, gettimeout(endTimestamp)) ) )
        {
            Connection->State   = State_NotConnected;
            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_ReadRemote( oC_Tcp_Connection_t Connection , oC_Net_Address_t * outAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsCorrect(Connection)       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outAddress)           , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        memcpy(outAddress, &Connection->RemoteAddress, sizeof(*outAddress));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_ReadLocal( oC_Tcp_Connection_t Connection , oC_Net_Address_t * outAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsCorrect(Connection)       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsConnected(Connection)     , oC_ErrorCode_NotConnected             )
     && ErrorCondition( isram(outAddress)           , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        memcpy(outAddress, &Connection->LocalAddress, sizeof(*outAddress));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_Send( oC_Tcp_Connection_t Connection , const void * Buffer , oC_MemorySize_t Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsCorrect(Connection)     , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( isaddresscorrect(Buffer)  , oC_ErrorCode_WrongAddress         )
     && ErrorCondition( Size > 0                  , oC_ErrorCode_SizeNotCorrect       )
     && ErrorCondition( Timeout >= 0              , oC_ErrorCode_TimeNotCorrect       )
     && ErrorCondition( IsConnected(Connection)   , oC_ErrorCode_NotConnected         )
        )
    {
        Segment_t segment = {
                        .ConstBuffer    = Buffer ,
                        .Size           = Size
        };
        errorCode = SendSegment(Connection,&segment,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_Receive( oC_Tcp_Connection_t Connection, void * outBuffer, oC_MemorySize_t* Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( IsCorrect(Connection)     , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( isram(outBuffer)          , oC_ErrorCode_OutputAddressNotInRAM  )
     && ErrorCondition( isram(Size)               , oC_ErrorCode_OutputAddressNotInRAM  )
     && ErrorCondition( *Size > 0                 , oC_ErrorCode_SizeNotCorrect         )
     && ErrorCondition( Timeout >= 0              , oC_ErrorCode_TimeNotCorrect         )
     && ErrorCondition( IsConnected(Connection)   , oC_ErrorCode_NotConnected           )
        )
    {
        if( ErrorCondition( oC_Mutex_Take( Connection->ReadyForReceivingMutex, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout ) )
        {
            oC_MemorySize_t segmentSize = 0;

            if( ErrorCondition( oC_Queue_Get(Connection->ReceiveQueue,&segmentSize,sizeof(oC_MemorySize_t), gettimeout(endTimestamp)), oC_ErrorCode_Timeout ) )
            {
                oC_MemorySize_t sizeToCopy = oC_MIN(*Size,segmentSize);
                oC_MemorySize_t sizeToDrop = segmentSize - sizeToCopy;

                if(
                    ErrorCondition(                    oC_Queue_Get(Connection->ReceiveQueue,outBuffer,sizeToCopy,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout )
                 && ErrorCondition( sizeToDrop == 0 || oC_Queue_Get(Connection->ReceiveQueue,NULL     ,sizeToDrop,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout )
                    )
                {
                    *Size = sizeToCopy;
                    if(sizeToDrop > 0)
                    {
                        tcplog(oC_LogType_Warning, "%s: Output buffer is too small. Dropping %d bytes", Connection->Name, sizeToDrop);
                    }
                    errorCode = oC_ErrorCode_None;
                }
            }
            oC_Mutex_Give( Connection->ReadyForReceivingMutex );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connection_SetCallback( oC_Tcp_Connection_t Connection , oC_Tcp_CallbackGroup_t Group, oC_Tcp_EventId_t EventId, const oC_Tcp_Connection_Callback_t* Callback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection)                             , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( Group   < oC_Tcp_CallbackGroup_NumberOfElements                     , oC_ErrorCode_GroupNotCorrect      )
     && ErrorCondition( EventId < oC_Tcp_EventId_NumberOfElements                           , oC_ErrorCode_EventNotSupported    )
     && ErrorCondition( isaddresscorrect(Callback)                                          , oC_ErrorCode_WrongAddress         )
     && ErrorCondition( Callback->Function == NULL || isaddresscorrect(Callback->Function)  , oC_ErrorCode_WrongAddress         )
        )
    {
        memcpy(&Connection->Callbacks[Group][EventId], Callback, sizeof(Connection->Callbacks[Group][EventId]));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_Tcp_Connection_GetRemoteWindowSize( oC_Tcp_Connection_t Connection )
{
    oC_MemorySize_t segmentSize = 0;

    if(oC_Tcp_Connection_IsCorrect(Connection))
    {
        segmentSize = Connection->RemoteWindowSize;
    }

    return segmentSize;
}
#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with locals
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief initializes Packet to send via TCP connection
 */
//==========================================================================================================================================
static oC_ErrorCode_t PreparePacketToSend( oC_Tcp_Connection_t Connection, Packet_t Packet, const PacketFlags_t * Flags, const uint8_t * Data, oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsCorrect(Connection)  , oC_ErrorCode_ObjectNotCorrect  )
     && ErrorCondition( isram(Packet)          , oC_ErrorCode_AddressNotInRam   )
     && ErrorCondition(
                     oC_Net_Packet_SetAddress(
                         &Packet->Packet,
                         &Connection->LocalAddress,
                         &Connection->RemoteAddress ), oC_ErrorCode_CannotSetAddress )
        )
    {
        oC_Tcp_Header_t * header = oC_Tcp_Packet_GetHeaderReference(Packet);

        if( ErrorCondition( header != NULL, oC_ErrorCode_PacketNotCorrect )  )
        {
            oC_MemorySize_t localWindowSize = Connection->SegmentToReceive.Size - Connection->SegmentToReceive.ReceivedBytes;
            header->SourcePort              = Connection->LocalAddress.Port;
            header->DestinationPort         = Connection->RemoteAddress.Port;
            header->SequenceNumber          = Connection->NextSequenceNumberToSend;
            header->AcknowledgmentNumber    = Connection->NextSequenceNumberToReceive;
            header->DataOffset              = Flags->DataOffset;
            header->Reserved                = 0;
            header->Flags                   = Flags->HighByte;
            header->NS                      = Flags->NS;
            header->WindowSize              = localWindowSize >> Connection->LocalWindowScale;
            header->UrgentPointer           = 0;

            if(Data != NULL && Size != NULL && (*Size) > 0)
            {
                oC_IntMan_EnterCriticalSection();
                uint16_t        sizeToSend      = oC_MIN(Connection->PacketSize,*Size);
                oC_MemorySize_t leftBytesToSend = 0;

                sizeToSend      = oC_MIN(sizeToSend,Connection->RemoteWindowSize);
                leftBytesToSend = *Size - sizeToSend;

                oC_Tcp_Packet_SetData(Packet,Data,sizeToSend);

                *Size = sizeToSend;

                if(leftBytesToSend == 0)
                {
                    header->PSH = 1;
                }
                else
                {
                    header->PSH = 0;
                }

                oC_IntMan_ExitCriticalSection();
            }
            else
            {
                oC_Tcp_Packet_SetSize(Packet,0);
            }

            header->Checksum    = oC_Tcp_Packet_CalculateChecksum(Packet);
            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends packet via TCP connection
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendPacket( oC_Tcp_Connection_t Connection, Packet_t Packet, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    uint16_t       dataSize     = oC_Tcp_Packet_GetDataSize(Packet);
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    while( dataSize > Connection->RemoteWindowSize )
    {
        if(!oC_Event_WaitForState(Connection->RemoteWindowSizeChangedEvent,
                                  Connection->RemoteWindowSize,
                                  oC_Event_StateMask_DifferentThan,
                                  gettimeout(endTimestamp)
                                  )
           )
        {
            break;
        }
    }
    if(
        ErrorCondition( dataSize <= Connection->RemoteWindowSize                        , oC_ErrorCode_NoSpaceOnRemoteServer         )
     && ErrorCondition( oC_Net_Packet_IsSourceAddress(&Packet->Packet, &Connection->LocalAddress), oC_ErrorCode_InvalidSourceAddress )
     && ErrorCondition( oC_Tcp_Packet_ConvertToNetworkEndianess(Packet)                 , oC_ErrorCode_CannotConvertHeaderEndianess  )
     && ErrorCode     ( oC_NetifMan_SendPacket( &Connection->RemoteAddress, &Packet->Packet, gettimeout(endTimestamp), NULL)         )
     && ErrorCondition( oC_Tcp_Packet_ConvertFromNetworkEndianess(Packet)               , oC_ErrorCode_CannotConvertHeaderEndianess  )
        )
    {
        oC_IntMan_EnterCriticalSection();

        tcplog(oC_LogType_Info, "Send packet to remote port %d\n", Connection->RemoteAddress.Port);
        Connection->NextSequenceNumberToSend     += dataSize;
        Connection->RemoteWindowSize             -= dataSize;

        errorCode = oC_ErrorCode_None;

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief prepares and sends TCP packet
 */
//==========================================================================================================================================
static oC_ErrorCode_t PrepareAndSendPacket( oC_Tcp_Connection_t Connection, Packet_t Packet, const PacketFlags_t * Flags, const uint8_t * Data, oC_MemorySize_t * Size , uint32_t * outSequenceNumber , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( Timeout >= 0                                                                , oC_ErrorCode_TimeNotCorrect          )
     && ErrorCondition( oC_Mutex_Take(Connection->ReadyForSendingMutex,gettimeout(endTimestamp))    , oC_ErrorCode_Timeout                 )
        )
    {
        if(
            ErrorCode( PreparePacketToSend( Connection, Packet, Flags, Data, Size   ) )
         && ErrorCode( SendPacket( Connection, Packet, gettimeout(endTimestamp)     ) )
            )
        {
            Connection->NextSequenceNumberToSend += Flags->AddToSequence;
            if(outSequenceNumber != NULL)
            {
                *outSequenceNumber = Connection->NextSequenceNumberToSend;
            }
            errorCode = oC_ErrorCode_None;
        }
        oC_Mutex_Give(Connection->ReadyForSendingMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief the function for filtering packets
 */
//==========================================================================================================================================
static bool PacketFilterFunction( oC_Net_Packet_t * ReceivedPacket , const void * Parameter , oC_Netif_t Netif )
{
    bool                packetForUs = false;
    oC_Tcp_Header_t *   header      = oC_Tcp_Packet_GetHeaderReference((oC_Tcp_Packet_t *)ReceivedPacket);
    oC_Tcp_Connection_t connection  = (oC_Tcp_Connection_t)Parameter;

    if( header != NULL
     && header->DestinationPort == oC_Net_ConvertUint16FromNetworkEndianess(connection->LocalAddress.Port)
     && ReceivedPacket->IPv4.Header.Protocol == oC_Net_Protocol_TCP
         )
    {
        oC_Net_Address_t remoteAddress = {
                .Type = oC_Net_AddressType_IPv4,
                .IPv4 = ReceivedPacket->IPv4.Header.SourceIp,
                .Protocol = ReceivedPacket->IPv4.Header.Protocol,
                .Port = oC_Net_ConvertUint16FromNetworkEndianess(header->SourcePort)
        };
        if(connection->State == State_WaitForSyn)
        {
            packetForUs = header->NetworkOrder.SYN == 1 && header->NetworkOrder.ACK == 0;
            oC_Tcp_Connection_Callback_t* callback = &connection->Callbacks[oC_Tcp_CallbackGroup_Internal][oC_Tcp_EventId_NewConnectionVerification];
            if(callback->Function != NULL)
            {
                packetForUs = packetForUs && !callback->Function(connection, callback->Parameter, &remoteAddress);
            }
        }
        else
        {
            packetForUs = remoteAddress.IPv4 == connection->RemoteAddress.IPv4 && remoteAddress.Port == connection->RemoteAddress.Port;
        }
    }

    return packetForUs;
}

//==========================================================================================================================================
/**
 * @brief receives packet via TCP connection
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReceivePacket( oC_Tcp_Connection_t Connection, Packet_t Packet, oC_Time_t Timeout )
{
    oC_ErrorCode_t    errorCode   = oC_ErrorCode_ImplementError;
    oC_Net_Packet_t * localPacket = (oC_Net_Packet_t*)Packet;
    if(
        ErrorCode     ( oC_NetifMan_ReceivePacket(&Connection->RemoteAddress, localPacket   , Timeout, NULL, PacketFilterFunction, Connection)  )
     && ErrorCondition( oC_Tcp_Packet_ConvertFromNetworkEndianess((Packet_t)localPacket)    , oC_ErrorCode_CannotConvertHeaderEndianess         )
        )
    {
        Connection->RemoteAddress.Type = oC_Net_AddressType_IPv4;
        Connection->RemoteAddress.IPv4 = Packet->IPv4.IpHeader.SourceIp;
        Connection->RemoteAddress.Protocol = oC_Net_Protocol_TCP;
        Connection->RemoteAddress.Port = Packet->IPv4.TcpHeader.SourcePort;
        tcplog(oC_LogType_Info, "[%p] Received packet for %s (remote port: %d)\n", Connection, Connection->Name, Connection->RemoteAddress.Port);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief simply sends one of standard (predefined) packets
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendPredefinedPacket( oC_Tcp_Connection_t Connection , PredefinedPacket_t PredefinedPacket , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;

    oC_STATIC_ASSERT( sizeof(PredefinedPacketFlags) <= (sizeof(PacketFlags_t) * 10),
                      "Size of 'PredefinedPacketFlags' array is too big! (Have you changed the PredefinedPacket_t definition? - you should not do it!)" );

    if(
        ErrorCondition( Timeout >= 0                                         , oC_ErrorCode_TimeNotCorrect          )
     && ErrorCondition( PredefinedPacket < PredefinedPacket_NumberOfElements , oC_ErrorCode_InternalDataAreDamaged  )
        )
    {

        errorCode = PrepareAndSendPacket(Connection,Connection->PacketToSend, &PredefinedPacketFlags[PredefinedPacket], NULL, NULL, NULL, Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for one of predefined packets
 */
//==========================================================================================================================================
static oC_ErrorCode_t WaitForPredefinedPacket( oC_Tcp_Connection_t Connection , PredefinedPacket_t PredefinedPacket , oC_Time_t Timeout )
{
    oC_ErrorCode_t   errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t   endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( IsCorrect(Connection)                                , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( PredefinedPacket < PredefinedPacket_NumberOfElements , oC_ErrorCode_InternalDataAreDamaged  )
     && ErrorCondition( Timeout >= 0                                         , oC_ErrorCode_TimeNotCorrect          )
        )
    {
        while( ErrorCondition(oC_Semaphore_Take(Connection->NewPacketReceivedSemaphore,gettimeout(endTimestamp)), oC_ErrorCode_Timeout) )
        {
            if(oC_Bits_AreBitsSetU8( Connection->ReceivedPacketFlags.HighByte ,PredefinedPacketFlags[PredefinedPacket].HighByte ))
            {
                Connection->NextSequenceNumberToReceive += PredefinedPacketFlags[PredefinedPacket].AddToSequence;
                errorCode = oC_ErrorCode_None;
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for the acknowledge of the given packet
 */
//==========================================================================================================================================
static oC_ErrorCode_t WaitForAcknowledge( oC_Tcp_Connection_t Connection, uint32_t AcknowledgeNumber, oC_Time_t Timeout )
{
    oC_ErrorCode_t   errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t   endTimestamp = gettimestamp() + Timeout;
    if(
        ErrorCondition( IsCorrect(Connection) , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( Timeout >= 0          , oC_ErrorCode_TimeNotCorrect          )
        )
    {
        if( ErrorCondition(oC_Event_WaitForValue(Connection->AcknowledgeReceivedEvent, AcknowledgeNumber,oC_Event_CompareType_GreaterOrEqual,gettimeout(endTimestamp)), oC_ErrorCode_Timeout) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads connection data from the given packet
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadConnectionDataFromPacket( oC_Tcp_Connection_t Connection , Packet_t Packet )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsCorrect(Connection) , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( isram(Packet)         , oC_ErrorCode_AddressNotInRam    )
        )
    {
        uint8_t             optionSize = sizeof(Connection->RemoteWindowScale);
        oC_DefaultString_t  tempString = {0};
        oC_Tcp_Packet_ReadOption(Packet,oC_Tcp_OptionKind_WindowScale, &Connection->RemoteWindowScale, &optionSize);

        if(oC_Net_Packet_GetType(&Packet->Packet) == oC_Net_PacketType_IPv4)
        {
            /* IP Header */
            Connection->LocalAddress.Type            = oC_Net_AddressType_IPv4;
            Connection->RemoteAddress.Type           = oC_Net_AddressType_IPv4;
            Connection->LocalAddress.IPv4            = Packet->IPv4.IpHeader.DestinationIp;
            Connection->RemoteAddress.IPv4           = Packet->IPv4.IpHeader.SourceIp;

            /* TCP Header */
            Connection->RemoteAddress.Port           = Packet->IPv4.TcpHeader.SourcePort;
            Connection->LocalAddress.Port            = Packet->IPv4.TcpHeader.DestinationPort;
            Connection->NextSequenceNumberToReceive  = Packet->IPv4.TcpHeader.SequenceNumber;
            Connection->RemoteWindowSize             = Packet->IPv4.TcpHeader.WindowSize << Connection->RemoteWindowScale;
            Connection->RemoteInitialSequenceNumber  = Packet->IPv4.TcpHeader.SequenceNumber;

            Connection->ReceivedPacketFlags.DataOffset = Packet->IPv4.TcpHeader.DataOffset;
            Connection->ReceivedPacketFlags.NS         = Packet->IPv4.TcpHeader.NS;
            Connection->ReceivedPacketFlags.HighByte   = Packet->IPv4.TcpHeader.Flags;

            oC_Net_AddressToString(&Connection->RemoteAddress,tempString,sizeof(tempString));
            sprintf(Connection->Name, "%s:%d", tempString, Connection->RemoteAddress.Port);

            tcplog(oC_LogType_Info, "Read connection data. Port: %d\n", Connection->RemoteAddress.Port);

            errorCode = oC_ErrorCode_None;
        }
        else if(oC_Net_Packet_GetType(&Packet->Packet) == oC_Net_PacketType_IPv6)
        {
            Connection->LocalAddress.Type   = oC_Net_AddressType_IPv6;
            Connection->RemoteAddress.Type  = oC_Net_AddressType_IPv6;

            errorCode = oC_ErrorCode_NotImplemented;
        }
        else
        {
            errorCode = oC_ErrorCode_PacketNotCorrect;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends data segment by using TCP connection
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendSegment( oC_Tcp_Connection_t Connection, Segment_t * Segment, oC_Time_t Timeout )
{
    oC_ErrorCode_t  errorCode               = oC_ErrorCode_ImplementError;
    oC_Timestamp_t  endTimestamp            = gettimestamp() + Timeout;
    uint8_t *       dataToSend              = NULL;
    oC_MemorySize_t dataToSendSize          = 0;
    uint32_t        sequenceNumberToConfirm = 0;

    if( ErrorCondition( oC_Mutex_Take(Connection->SendingSegmentMutex, gettimeout(endTimestamp)), oC_ErrorCode_Timeout ) )
    {
        Segment->ConfirmedBytes = 0;
        Segment->SentBytes      = 0;
        oC_Time_t confirmationTimeout = Connection->ConfirmationTimeout;
        oC_Time_t resendSegmnetTime = Connection->ResendSegmentTime;

        while(gettimestamp() <= endTimestamp)
        {
            dataToSend      = &Segment->Buffer[Segment->SentBytes];
            dataToSendSize  = Segment->Size - Segment->SentBytes;
            errorCode       = PrepareAndSendPacket(Connection,Connection->PacketToSend,&PredefinedPacketFlags[PredefinedPacket_ACK],dataToSend,&dataToSendSize,&sequenceNumberToConfirm,gettimeout(endTimestamp));

            if(oC_ErrorOccur(errorCode))
            {
                tcplog( oC_LogType_Error, "%s: Cannot send packet - %R", Connection->Name, errorCode );
                sleep( resendSegmnetTime );
                resendSegmnetTime *= 2;
                continue;
            }
            else
            {
                resendSegmnetTime = Connection->ResendSegmentTime;
            }

            if( ErrorCode( WaitForAcknowledge(Connection, sequenceNumberToConfirm, confirmationTimeout) ) )
            {
                Segment->SentBytes                      += dataToSendSize;
                Connection->SentBytesFromLastMeasure    += dataToSendSize;
                confirmationTimeout                     = Connection->ConfirmationTimeout;

                if(Segment->SentBytes >= Segment->Size)
                {
                    errorCode = oC_ErrorCode_None;
                    break;
                }
            }
            else if(IsDisconnected(Connection))
            {
                tcplog( oC_LogType_Info, "%s: Connection has been closed. Skipping packet sending\n", Connection->Name );
                errorCode = oC_ErrorCode_ConnectionExpired;
                break;
            }
            else
            {
                Connection->NextSequenceNumberToSend = Connection->LastConfirmedSequenceNumber;
                confirmationTimeout *= 2;
                tcplog( oC_LogType_Error, "%s: Packet SEQ 0x%08X not confirmed, sending once again. Timeout: %t\n", Connection->Name, Connection->NextSequenceNumberToSend, confirmationTimeout);
                if(errorCode != oC_ErrorCode_Timeout)
                {
                    break;
                }
            }
        }

        oC_Mutex_Give(Connection->SendingSegmentMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief saves data from the queue
 */
//==========================================================================================================================================
static void SaveReceivedSegmentInQueue( oC_Tcp_Connection_t Connection )
{
    oC_Timestamp_t  endTimestamp = gettimestamp() + Connection->SaveSegmentTimeout;
    void *          buffer       = Connection->SegmentToReceive.Buffer;
    oC_MemorySize_t size         = Connection->SegmentToReceive.ReceivedBytes;
    oC_Queue_t      queue        = Connection->ReceiveQueue;


    if(
        !oC_SaveIfFalse("Size of segment", oC_Queue_Put(queue, &size , sizeof(size), gettimeout(endTimestamp)), oC_ErrorCode_Timeout )
     || !oC_SaveIfFalse("Data of segment", oC_Queue_Put(queue, buffer, size        , gettimeout(endTimestamp)), oC_ErrorCode_Timeout )
        )
    {
        tcplog( oC_LogType_Error, "%s: Cannot put %d bytes of data into the queue", Connection->Name, size );
    }
    Connection->SegmentToReceive.ReceivedBytes  = 0;
    Connection->SegmentToReceive.ConfirmedBytes = 0;
}

//==========================================================================================================================================
/**
 * @brief analyzes received data
 */
//==========================================================================================================================================
static void AnalyzeReceivedPacket( oC_Tcp_Connection_t Connection , Packet_t Packet )
{
//    oC_IntMan_EnterCriticalSection();

    oC_Tcp_Header_t * header = oC_Tcp_Packet_GetHeaderReference(Packet);

    if(oC_SaveIfFalse("Header", header != NULL, oC_ErrorCode_PacketNotCorrect))
    {
        uint16_t  receivedDataSize  = oC_Tcp_Packet_GetDataSize(Packet);
        uint8_t * data              = receivedDataSize > 0 ? oC_Tcp_Packet_GetDataReference(Packet) : NULL;

        if(receivedDataSize > 0 && data != NULL)
        {
            if(oC_SaveIfFalse("Receive buffer is full", Connection->SegmentToReceive.ReceivedBytes < Connection->SegmentToReceive.Size, oC_ErrorCode_NoSpaceAvailable))
            {
                uint8_t *       buffer   = &Connection->SegmentToReceive.Buffer[ Connection->SegmentToReceive.ReceivedBytes ];
                oC_MemorySize_t leftSize = Connection->SegmentToReceive.Size - Connection->SegmentToReceive.ReceivedBytes;
                oC_MemorySize_t copySize = oC_MIN(leftSize,receivedDataSize);

                memcpy(buffer, data, copySize);

                Connection->SegmentToReceive.ConfirmedBytes += copySize;
                Connection->SegmentToReceive.ReceivedBytes  += receivedDataSize;
                Connection->AcknowledgePacketShouldBeSend    = true;
            }

            if(header->PSH == 1)
            {
                SaveReceivedSegmentInQueue(Connection);
            }
        }

        Connection->LastConfirmedSequenceNumber     = header->AcknowledgmentNumber;
        Connection->NextSequenceNumberToReceive     = header->SequenceNumber + receivedDataSize;
        Connection->RemoteWindowSize                = header->WindowSize << Connection->RemoteWindowScale;
        Connection->ReceivedPacketFlags.DataOffset  = header->DataOffset;
        Connection->ReceivedPacketFlags.NS          = header->NS;
        Connection->ReceivedPacketFlags.HighByte    = header->Flags;

        if(header->SYN == 1 && header->ACK == 0)
        {
            tcplog( oC_LogType_Info, "Received SYN packet. Reading connection data\n");
            ReadConnectionDataFromPacket(Connection,Packet);
        }
        if((header->FIN == 1 || header->RST == 1) && Connection->State != State_Disconnecting)
        {
            Connection->AcknowledgePacketShouldBeSend = true;
            Connection->FinishConnectionPacketShouldBeSend = true;
            Connection->State = State_Disconnecting;
            tcplog( oC_LogType_Info, "%s: Connection has been closed by the peer\n", Connection->Name );
        }

        oC_Event_SetState(Connection->RemoteWindowSizeChangedEvent, Connection->RemoteWindowSize);
        oC_Event_SetState(Connection->AcknowledgeReceivedEvent    , Connection->LastConfirmedSequenceNumber);
        oC_Semaphore_Give(Connection->NewPacketReceivedSemaphore);
    }
//    oC_IntMan_ExitCriticalSection();
}

//==========================================================================================================================================
/**
 * @brief calculates current Baud Rate of TCP connection
 */
//==========================================================================================================================================
static void CalculateBaudRate( oC_Tcp_Connection_t Connection )
{
    oC_Timestamp_t currentTimestamp = gettimestamp();
    oC_Time_t      measureTime      = currentTimestamp - Connection->LastMeasureTimestamp;

    if(measureTime >= s(1))
    {
        Connection->BaudRate                 = (oC_BaudRate_t)(((oC_Time_t)Connection->SentBytesFromLastMeasure) / measureTime);
        Connection->LastMeasureTimestamp     = currentTimestamp;
        Connection->SentBytesFromLastMeasure = 0;

        tcplog(oC_LogType_Track, "%s: Connection baud rate = %u B", Connection->Name, Connection->BaudRate);

    }
}

//==========================================================================================================================================
/**
 * @brief thread for receiving data
 */
//==========================================================================================================================================
static void ReceiveThread( oC_Tcp_Connection_t Connection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    while( oC_Tcp_Connection_IsCorrect(Connection) )
    {
        if( ErrorCode( ReceivePacket( Connection, Connection->LastReceivedPacket, Connection->ReceiveTimeout)) )
        {
            Connection->ExpirationTimestamp = gettimestamp() + Connection->ExpirationTimeout;

            AnalyzeReceivedPacket(Connection,Connection->LastReceivedPacket);
            CalculateBaudRate(Connection);

            if(Connection->FinishConnectionPacketShouldBeSend)
            {
                break;
            }
            if(Connection->AcknowledgePacketShouldBeSend)
            {
                oC_SaveIfErrorOccur("Cannot send ACK", SendPredefinedPacket(Connection, PredefinedPacket_ACK, Connection->SendingAcknowledgeTimeout));
                Connection->AcknowledgePacketShouldBeSend = false;
            }
        }
        else if(errorCode != oC_ErrorCode_Timeout)
        {
            oC_SaveError("Cannot receive packet", errorCode);
            tcplog( oC_LogType_Error, "%s: Cannot receive packet - %R", Connection->Name, errorCode );
            break;
        }
        else if(Connection->State != State_WaitForSyn)
        {
            if(gettimestamp() >= Connection->ExpirationTimestamp)
            {
                tcplog( oC_LogType_Warning, "Connection '%s' has expired, closing...", Connection->Name );
                break;
            }
            else if(Connection->State == State_Disconnecting)
            {
                tcplog( oC_LogType_Track, "%s: Detect disconnect request - disconnecting...", Connection->Name );
                break;
            }
        }
    }

    if(Connection->FinishConnectionPacketShouldBeSend)
    {
        tcplog( oC_LogType_Info, "Sending FIN ACK: %R\n" , SendPredefinedPacket(Connection, PredefinedPacket_FIN_ACK, Connection->SendingAcknowledgeTimeout));
        tcplog( oC_LogType_Info, "Sending ACK: %R\n", SendPredefinedPacket(Connection, PredefinedPacket_ACK    , Connection->SendingAcknowledgeTimeout));
    }

    tcplog( oC_LogType_Info, "%s: Connection changed it's status to 'not connected'", Connection->Name );
    Connection->State = State_NotConnected;

    for(oC_Tcp_CallbackGroup_t group = 0; group < oC_Tcp_CallbackGroup_NumberOfElements; group++)
    {
        oC_Tcp_Connection_Callback_t* callback = &Connection->Callbacks[group][oC_Tcp_EventId_ConnectionFinished];
        if(isaddresscorrect(callback->Function))
        {
            callback->Function(Connection, callback->Parameter, NULL);
        }
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


