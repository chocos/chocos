/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface of TCP module
 *
 * @file       oc_tcp.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_tcp.h>
#include <oc_module.h>
#include <oc_portman.h>
#include <oc_list.h>
#include <oc_mutex.h>
#include <oc_dynamic_config.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static oC_Tcp_Server_t          GetServerWithConnection     ( oC_Tcp_Connection_t Connection );
static oC_Tcp_Server_t          GetServerCreatedBy          ( oC_Process_t Process );
static oC_Tcp_Connection_t      GetConnectionCreatedBy      ( oC_Process_t Process );
static oC_ErrorCode_t           CloseServersCreatedBy       ( oC_Process_t Process , oC_Time_t Timeout );
static oC_ErrorCode_t           CloseConnectionsCreatedBy   ( oC_Process_t Process , oC_Time_t Timeout );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_List(oC_Tcp_Server_t)     Servers     = NULL;     //!< List of TCP servers
static oC_List(oC_Tcp_Connection_t) Connections = NULL;     //!< List of TCP connections
static oC_Mutex_t                   ModuleBusy  = NULL;     //!< Module busy Mutex
static const oC_Allocator_t         Allocator   = {         //!< Allocator of the module
                .Name = "TCP"
};
static const oC_PortMan_Config_t    PortManConfig = {       //!< Configuration for the `PortMan` module
                .MaximumPortNumber      = oC_uint16_MAX ,
                .FirstDynamicPortNumber = oC_Tcp_Port_NumberOfSpecialPorts ,
                .LastDynamicPortNumber  = oC_uint16_MAX
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on TCP module
 *
 * The function is for turning on the TCP module. It has to be called before any other TCP call. The module requires also `PortMan` module
 * to work and it should enabled before all of this function.
 *
 * @note
 * Only root user can enable the module
 *
 * @warning
 * Remember to enable `PortMan` module before!
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleIsTurnedOn                | Module is already turned on - please turn it off first if you want to restart it.
 *  oC_ErrorCode_AllocationError                 | There is a problem with memory allocation
 *  oC_ErrorCode_RequiredModuleNotEnabled        | `PortMan` module has not been enabled before
 *  oC_ErrorCode_PermissionDenied                | Only `root` user can enable the module
 *
 * More error codes can be returned by the function #oC_PortMan_RegisterModule
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_Tcp))
    {
        oC_Time_t timeout = oC_DynamicConfig_GetValue(Tcp, TurnTimeout);

        if(
            ErrorCondition( oC_Module_IsTurnedOn(oC_Module_PortMan) , oC_ErrorCode_RequiredModuleNotEnabled )
         && ErrorCondition( iscurroot()                             , oC_ErrorCode_PermissionDenied         )
         && ErrorCode     ( oC_PortMan_RegisterModule(oC_Module_Tcp, &PortManConfig, timeout)               )
            )
        {
            Servers     = oC_List_New(&Allocator, AllocationFlags_Default);
            Connections = oC_List_New(&Allocator, AllocationFlags_Default);
            ModuleBusy  = oC_Mutex_New( oC_Mutex_Type_Recursive, &Allocator, AllocationFlags_Default );

            if(
                ErrorCondition( Servers     != NULL , oC_ErrorCode_AllocationError )
             && ErrorCondition( Connections != NULL , oC_ErrorCode_AllocationError )
             && ErrorCondition( ModuleBusy  != NULL , oC_ErrorCode_AllocationError )
                )
            {
                oC_Module_TurnOn(oC_Module_Tcp);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                bool listServerListDeleted      = Servers     == NULL || oC_List_Delete ( Servers     , AllocationFlags_Default );
                bool listConnectionsListDeleted = Connections == NULL || oC_List_Delete ( Connections , AllocationFlags_Default );
                bool mutexDeleted               = ModuleBusy  == NULL || oC_Mutex_Delete( &ModuleBusy , AllocationFlags_Default );

                oC_SaveIfFalse( "TCP::TurnOn ", listServerListDeleted && listConnectionsListDeleted && mutexDeleted , oC_ErrorCode_ReleaseError );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off TCP module
 *
 * The function is for turning off the TCP module.
 *
 * @warning
 * Only root user can turn off the module!
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - call of this function is not required.
 *  oC_ErrorCode_ReleaseError                    | There was a problem with releasing memory
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below 0
 *  oC_ErrorCode_PermissionDenied                | Currently logged user is not root
 *  oC_ErrorCode_ModuleBusy                      | Module is busy and maximum time for the operation has expired
 *
 *  More error codes can be returned by the function #oC_PortMan_UnregisterModule
 *
 *  @see oC_Tcp_TurnOn
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_TurnOff( void )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Time_t      timeout      = oC_DynamicConfig_GetValue(Tcp, TurnTimeout);
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        if(
            ErrorCondition( timeout >= 0                                                         , oC_ErrorCode_TimeNotCorrect       )
         && ErrorCondition( iscurroot()                                                          , oC_ErrorCode_PermissionDenied     )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp))   , oC_ErrorCode_ModuleBusy           )
         && ErrorCode     ( oC_PortMan_UnregisterModule(oC_Module_Tcp, oC_KTime_CalculateTimeout(endTimestamp))                      )
            )
        {
            oC_Module_TurnOff(oC_Module_Tcp);

            bool allServersDeleted          = true;
            bool allConnectionsDeleted      = true;

            foreach(Servers,server)
            {
                allServersDeleted = oC_Tcp_Server_Delete( &server, oC_KTime_CalculateTimeout(endTimestamp) ) && allServersDeleted;
            }

            foreach(Connections,connection)
            {
                allConnectionsDeleted = oC_Tcp_Connection_Delete(&connection, oC_KTime_CalculateTimeout(endTimestamp)) && allConnectionsDeleted;
            }

            bool serversListDeleted     = oC_List_Delete ( Servers     , AllocationFlags_Default );
            bool connectionsListDeleted = oC_List_Delete ( Connections , AllocationFlags_Default );
            bool mutexDeleted           = oC_Mutex_Delete( &ModuleBusy , AllocationFlags_Default );

            if(
                ErrorCondition( serversListDeleted
                             && connectionsListDeleted
                             && allServersDeleted
                             && allConnectionsDeleted
                             && mutexDeleted                , oC_ErrorCode_ReleaseError)
                )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief closes all objects related with the process
 *
 * The function is for closing all TCP servers and connections that are related with the given process.
 *
 * @param Process           Pointer to the process to close
 * @param Timeout           Maximum time to wait for the closing operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_ProcessNotCorrect               | `Process` is not correct or not valid anymore
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below zero
 *  oC_ErrorCode_PermissionDenied                | You are not logged as `root` user
 *  oC_ErrorCode_ModuleBusy                      | Module is currently in use
 *  oC_ErrorCode_CannotRemoveObjectFromList      | a `Server` or a `Connection` cannot be removed from servers or connections list
 *  oC_ErrorCode_ReleaseError                    | There was some memory release error
 *
 *  @note
 *  More error codes can be returned by the function #oC_PortMan_ReleaseAllPortsOf
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_CloseProcess( oC_Process_t Process , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        if(
            ErrorCondition( Timeout >= 0                                                        , oC_ErrorCode_TimeNotCorrect       )
         && ErrorCondition( iscurroot()                                                         , oC_ErrorCode_PermissionDenied     )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ModuleBusy           )
            )
        {
            if(
                ErrorCode( CloseServersCreatedBy        ( Process, oC_KTime_CalculateTimeout(endTimestamp))                 )
             && ErrorCode( CloseConnectionsCreatedBy    ( Process, oC_KTime_CalculateTimeout(endTimestamp))                 )
             && ErrorCode( oC_PortMan_ReleaseAllPortsOf ( oC_Module_Tcp, Process, oC_KTime_CalculateTimeout(endTimestamp))  )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reserves TCP port
 *
 * To prevent using the same TCP port by two different processes at the same time, each process has to reserve the TCP port before using it.
 * This function allows to do by manual. It is also possible to reserve a first available TCP port by setting a `Port` variable to 0.
 *
 * @warning
 * If you are using functions #oC_Tcp_Connect or #oC_Tcp_Listen you should not reserve the port because these functions reserve it by
 * their own.
 *
 * @param Port          Pointer to the variable where the TCP port is stored. If it points to 0, the module will find a first available
 *                      TCP port and set it inside this variable
 * @param Timeout       Maximum time for the reservation operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_AddressNotInRam                 | The `Port` is not correct RAM address
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below zero
 *
 *  More error codes can be returned by the function #oC_PortMan_ReservePort
 *
 * @see oC_PortMan_ReservePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_ReservePort( oC_Tcp_Port_t * Port , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        if(
            ErrorCondition( isram(Port)  , oC_ErrorCode_AddressNotInRam )
         && ErrorCondition( Timeout >= 0 , oC_ErrorCode_TimeNotCorrect  )
            )
        {
            oC_PortMan_Port_t port = (oC_PortMan_Port_t)*Port;

            if(ErrorCode( oC_PortMan_ReservePort(oC_Module_Tcp, &port, Timeout) ))
            {
                *Port     = port;
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases TCP port
 *
 * The function is for releasing TCP ports that has been previously reserved by the function #oC_Tcp_ReservePort and are not required
 * any more.
 *
 * @warning
 * Only the process that has reserved the port earlier can release it. Exception from this rule is the root user that can release each port.
 *
 * @param Port              TCP port to release (reserved by the function #oc_Tcp_ReservePort)
 * @param Timeout           Maximum time to wait for releasing the port
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_PortNotCorrect                  | The `Port` is not correct (it cannot be 0)
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is below zero
 *
 *  More error codes can be returned by the function #oC_PortMan_ReleasePort
 *
 * @see oC_PortMan_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_ReleasePort( oC_Tcp_Port_t Port , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        oC_PortMan_Port_t port = (oC_PortMan_Port_t)Port;

        if(
            ErrorCondition( port > 0        , oC_ErrorCode_PortNotCorrect   )
         && ErrorCondition( Timeout >= 0    , oC_ErrorCode_TimeNotCorrect   )
            )
        {
            errorCode = oC_PortMan_ReleasePort(oC_Module_Tcp, Port, Timeout);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the port is reserved
 */
//==========================================================================================================================================
bool oC_Tcp_IsPortReserved( oC_Tcp_Port_t Port )
{
    return oC_PortMan_IsPortReserved(oC_Module_Tcp , Port);
}

//==========================================================================================================================================
/**
 * @brief checks if the port is reserved by the given process
 */
//==========================================================================================================================================
bool oC_Tcp_IsPortReservedBy( oC_Tcp_Port_t Port , oC_Process_t Process )
{
    return oC_PortMan_IsPortReservedBy(oC_Module_Tcp, Port, Process);
}

//==========================================================================================================================================
/**
 * @brief connects to the remote TCP server
 *
 * The function creates new TCP connection object, connects it into the remote TCP server and push it to the local TCP connections list.
 *
 * @param Destination       Destination IP address and destination TCP port to connect
 * @param LocalPort         Local TCP port to reserve for this connection or 0 if first free port number should be used
 * @param outConnection     Destination for the connection object
 * @param Timeout           Maximum time to wait for the connection
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_WrongAddress                    | The `Destination` does not point to the correct RAM or ROM address
 *  oC_ErrorCode_IpAddressNotCorrect             | The `Destination` structure stores incorrect IP address (the field `Type` is not set?)
 *  oC_ErrorCode_PortNotCorrect                  | The field `Port` inside the `Destination` structure cannot be 0!
 *  oC_ErrorCode_OutputAddressNotInRAM           | The `outConnection` is not correct RAM address
 *  oC_ErrorCode_TimeNotCorrect                  | The `Timeout` is below 0
 *  oC_ErrorCode_ModuleBusy                      | Maximum time to wait has expired and the module is still busy
 *  oC_ErrorCode_AllocationError                 | There was some problem with allocation memory for the connection
 *  oC_ErrorCode_CannotAddObjectToList           | Connection cannot be added to the local connections list
 *
 *  More error codes can be returned by the function #oC_Tcp_ReservePort and #oC_Tcp_Connection_Connect
 *
 * @see oC_Tcp_ReservePort, oC_Tcp_Connection_Connect, oC_Tcp_Send, oC_Tcp_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Connect( const oC_Net_Address_t * Destination , oC_Tcp_Port_t LocalPort , oC_Tcp_Connection_t * outConnection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        if(
            ErrorCondition( isaddresscorrect(Destination)                                               , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( oC_Net_IsAddressCorrect(Destination)                                        , oC_ErrorCode_IpAddressNotCorrect      )
         && ErrorCondition( Destination->Port > 0                                                       , oC_ErrorCode_PortNotCorrect           )
         && ErrorCondition( isram(outConnection)                                                        , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Timeout >= 0                                                                , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCode     ( oC_Tcp_ReservePort( &LocalPort, oC_KTime_CalculateTimeout(endTimestamp) )                                           )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy,oC_KTime_CalculateTimeout(endTimestamp))           , oC_ErrorCode_ModuleBusy               )
            )
        {
            oC_Tcp_Connection_Config_t config = {
                            .InitialSequenceNumber      = oC_DynamicConfig_GetValue( Tcp, InitialSequenceNumber ) ,
                            .InitialAcknowledgeNumber   = 0 ,
                            .LocalWindowSize            = oC_DynamicConfig_GetValue( Tcp, WindowSize            ) ,
                            .LocalWindowScale           = oC_DynamicConfig_GetValue( Tcp, WindowScale           ) ,
                            .ConfirmationTimeout        = oC_DynamicConfig_GetValue( Tcp, ConfirmationTimeout   ) ,
                            .LocalAddress.Type          = Destination->Type ,
                            .LocalAddress.IPv6.HighPart = 0 ,
                            .LocalAddress.IPv6.LowPart  = 0 ,
                            .LocalAddress.Port          = LocalPort ,
                            .LocalAddress.Protocol      = oC_Net_Protocol_TCP ,
            };
            oC_Tcp_Connection_t connection  = oC_Tcp_Connection_New( &config );
            bool                added       = oC_List_PushBack( Connections, connection, &Allocator );

            if(
                ErrorCondition( connection != NULL , oC_ErrorCode_AllocationError                               )
             && ErrorCondition( added , oC_ErrorCode_CannotAddObjectToList                                      )
             && ErrorCode     ( oC_Tcp_Connection_Connect(connection, oC_KTime_CalculateTimeout(endTimestamp))  )
                )
            {
                *outConnection  = connection;
                errorCode       = oC_ErrorCode_None;
            }
            else
            {
                bool removedFromList = added      == false || oC_List_RemoveAll( Connections, connection );
                bool deleted         = connection == NULL  || oC_Tcp_Connection_Delete( &connection, oC_KTime_CalculateTimeout(endTimestamp) );

                oC_SaveIfFalse     ("Tcp::Connect ", removedFromList , oC_ErrorCode_CannotRemoveObjectFromList                  );
                oC_SaveIfFalse     ("Tcp::Connect ", deleted         , oC_ErrorCode_ReleaseError                                );
                oC_SaveIfErrorOccur("Tcp::Connect ", oC_Tcp_ReleasePort(LocalPort, oC_KTime_CalculateTimeout(endTimestamp))     );
            }

            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disconnects TCP connection
 *
 * The function disconnects TCP connection, removes object from the local TCP connections list (or from TCP servers connection list) and
 * releases memory allocated for the connection. It can be used for connections received from functions #oC_Tcp_Connect and #oC_Tcp_Accept
 *
 * @param Connection        An address to variable with `Connection` object received from function #oC_Tcp_Connect or #oC_Tcp_Accept
 * @param Timeout           Maximum time for disconnection process
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_AddressNotInRam                 | The `Connection` does not point to the correct RAM address
 *  oC_ErrorCode_ObjectNotCorrect                | The `Connection` points to incorrect connection object
 *  oC_ErrorCode_NotConnected                    | The `Connection` is currently not connected
 *  oC_ErrorCode_TimeNotCorrect                  | The `Timeout` is below 0
 *  oC_ErrorCode_ModuleBusy                      | Maximum time to wait has expired and the module is still busy
 *  oC_ErrorCode_ObjectNotFoundOnList            | The `Connection` is not from function #oC_Tcp_Connect nor #oC_Tcp_Accept
 *  oC_ErrorCode_CannotRemoveObjectFromList      | Connection cannot be removed from the local connections list
 *  oC_ErrorCode_ReleaseError                    | The was a problem with releasing memory allocated for the `Connection` object
 *
 *  More error codes can be returned by the functions called internally
 *
 * @see oC_Tcp_Connect, oC_Tcp_Accept
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Disconnect( oC_Tcp_Connection_t * Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        if(
            ErrorCondition( isram(Connection)                                                   , oC_ErrorCode_AddressNotInRam      )
         && ErrorCondition( oC_Tcp_Connection_IsCorrect(*Connection)                            , oC_ErrorCode_ObjectNotCorrect     )
         && ErrorCondition( oC_Tcp_Connection_IsConnected(*Connection)                          , oC_ErrorCode_NotConnected         )
         && ErrorCondition( Timeout >= 0                                                        , oC_ErrorCode_TimeNotCorrect       )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ModuleBusy           )
            )
        {
            oC_Tcp_Connection_t connection   = *Connection;
            bool                foundOnList  = oC_List_Contains( Connections, *Connection );
            oC_Tcp_Server_t     server       = GetServerWithConnection(connection);
            oC_Net_Address_t    localAddress;

            bzero(&localAddress,sizeof(localAddress));

            if(ErrorCondition( foundOnList || server != NULL, oC_ErrorCode_ObjectNotFoundOnList ))
            {
                errorCode   = oC_ErrorCode_None;
                *Connection = NULL;

                bool removedFromList = foundOnList == false || oC_List_RemoveAll( Connections, connection );

                server == NULL
             || ErrorCode( oC_Tcp_Server_RemoveConnection( server, connection ) );

                ErrorCondition( removedFromList , oC_ErrorCode_CannotRemoveObjectFromList );
                ErrorCode( oC_Tcp_Connection_ReadLocal( connection, &localAddress)                           )
             && ErrorCode( oC_Tcp_ReleasePort( localAddress.Port , oC_KTime_CalculateTimeout(endTimestamp) ) );

                ErrorCode     ( oC_Tcp_Connection_Disconnect( connection, oC_KTime_CalculateTimeout(endTimestamp))                                      );
                ErrorCondition( oC_Tcp_Connection_Delete    ( Connection, oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ReleaseError         );
            }


            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief starts a server that listen at the given address
 *
 * The function starts a new TCP server that will listen at the given IP address (and the given TCP port). After call of the function it is
 * possible to accept connections by calling the function #oC_Tcp_Accept. Remember, that the function also reserves a TCP port for the
 * current process and it has to be closed by function #oC_Tcp_StopListen
 *
 * @param Source            Source IP address (with the `Port` field filled)
 * @param outServer         Destination address to return the new created server
 * @param MaxConnections    Maximum number of connections that the server can handle at once
 * @param Timeout           Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_WrongAddress                    | `Source` is not correct address of RAM or ROM
 *  oC_ErrorCode_IpAddressNotCorrect             | `Source` points to incorrect IP address (is `Type` field filled?)
 *  oC_ErrorCode_PortNotCorrect                  | `Port` in the `Source` structure is not filled (it is 0!)
 *  oC_ErrorCode_OutputAddressNotInRAM           | The `outServer` does not point to the correct RAM address
 *  oC_ErrorCode_MaximumValueNotCorrect          | The `MaxConnections` has to be grower than 0
 *  oC_ErrorCode_TimeNotCorrect                  | The `Timeout` is below 0
 *  oC_ErrorCode_ModuleBusy                      | Maximum time to wait has expired and the module is still busy
 *  oC_ErrorCode_AllocationError                 | There was a problem with memory allocation
 *  oC_ErrorCode_CannotAddObjectToList           | Some error occurred during adding server to the servers list
 *
 *  More error codes can be returned by the function #oC_Tcp_ReservePort
 *
 * @see oC_Tcp_Accept, oC_Tcp_Send, oC_Tcp_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Listen( const oC_Net_Address_t * Source , oC_Tcp_Server_t * outServer , uint32_t MaxConnections , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp     = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        oC_Tcp_Port_t port = 0;

        if(
            ErrorCondition( isaddresscorrect(Source)                                             , oC_ErrorCode_WrongAddress              )
         && ErrorCondition( oC_Net_IsAddressCorrect(Source)                                      , oC_ErrorCode_IpAddressNotCorrect       )
         && ErrorCondition( (port = Source->Port) > 0                                            , oC_ErrorCode_PortNotCorrect            )
         && ErrorCondition( isram(outServer)                                                     , oC_ErrorCode_OutputAddressNotInRAM     )
         && ErrorCondition( MaxConnections > 0                                                   , oC_ErrorCode_MaximumValueNotCorrect    )
         && ErrorCondition( Timeout >= 0                                                         , oC_ErrorCode_TimeNotCorrect            )
         && ErrorCode     ( oC_Tcp_ReservePort( &port, oC_KTime_CalculateTimeout(endTimestamp))                                           )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy , oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ModuleBusy                )
            )
        {
            oC_Tcp_Server_t server = oC_Tcp_Server_New( Source, MaxConnections );
            bool            pushed = oC_List_PushBack( Servers, server, &Allocator );

            if(
                ErrorCondition( server != NULL  , oC_ErrorCode_AllocationError          )
             && ErrorCondition( pushed          , oC_ErrorCode_CannotAddObjectToList    )
             && ErrorCode     ( oC_Tcp_Server_Run(server)                               )
                )
            {
                *outServer  = server;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                bool removedFromList    = pushed == false || oC_List_RemoveAll( Servers, server );
                bool deleted            = server == NULL  || oC_Tcp_Server_Delete( &server, oC_KTime_CalculateTimeout(endTimestamp) );

                oC_SaveIfFalse      ( "TCP::Listen ", removedFromList , oC_ErrorCode_CannotRemoveObjectFromList         );
                oC_SaveIfFalse      ( "TCP::Listen ", deleted         , oC_ErrorCode_ReleaseError                       );
                oC_SaveIfErrorOccur ( "TCP::Listen ", oC_Tcp_ReleasePort(port, oC_KTime_CalculateTimeout(endTimestamp)) );
            }
            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief stops listen at the given TCP port
 *
 * The function is for stopping listening at the given TCP port. It should be called for servers received from the function #oC_Tcp_Listen.
 * The function releases memory allocated for the TCP server and releases port reserved for it.
 *
 * @param Server            Pointer to server object received from the function #oC_Tcp_Listen, it will be set to #NULL on output
 * @param Timeout           Maximum time for server stopping
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_AddressNotInRam                 | `Server` is not correct RAM address
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` stores incorrect object pointer
 *  oC_ErrorCode_TimeNotCorrect                  | The `Timeout` is below 0
 *  oC_ErrorCode_ModuleBusy                      | Maximum time to wait has expired and the module is still busy
 *  oC_ErrorCode_ServerNotStarted                | Server has not started yet
 *  oC_ErrorCode_ReleaseError                    | There was a problem with releasing memory of TCP server
 *  oC_ErrorCode_CannotRemoveObjectFromList      | The given server cannot be removed from servers list
 *
 *  More error codes can be returned by the functions #oC_PortMan_ReleasePort and #oC_Tcp_Server_Stop
 *
 * @see oC_Tcp_Listen, oC_Tcp_Send, oC_Tcp_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_StopListen( oC_Tcp_Server_t * Server , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode        = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp     = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        if(
            ErrorCondition( isram(Server)                                                        , oC_ErrorCode_AddressNotInRam             )
         && ErrorCondition( oC_Tcp_Server_IsCorrect(*Server)                                     , oC_ErrorCode_ObjectNotCorrect            )
         && ErrorCondition( Timeout >= 0                                                         , oC_ErrorCode_TimeNotCorrect              )
         && ErrorCondition( oC_Tcp_Server_IsRunning(*Server)                                     , oC_ErrorCode_ServerNotStarted            )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy , oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ModuleBusy                  )
            )
        {
            oC_Tcp_Port_t   port        = oC_Tcp_Server_GetPort(*Server);
            bool            foundOnList = oC_List_Contains(Servers,*Server);

            if( ErrorCondition( foundOnList , oC_ErrorCode_ObjectNotFoundOnList ) )
            {
                errorCode = oC_ErrorCode_None;

                ErrorCode( oC_Tcp_Server_Stop(*Server)                                                           );
                ErrorCode( oC_PortMan_ReleasePort(oC_Module_Tcp, port, oC_KTime_CalculateTimeout(endTimestamp))  );

                bool removedFromList = oC_List_RemoveAll(Servers, *Server);

                ErrorCondition  ( oC_Tcp_Server_Delete(Server, oC_KTime_CalculateTimeout(endTimestamp))  , oC_ErrorCode_ReleaseError                );
                ErrorCondition  ( removedFromList                                                        , oC_ErrorCode_CannotRemoveObjectFromList  );
            }

            oC_Mutex_Give(ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for new connection and accepts it
 *
 * The function waits for the new TCP connection, accepts it and returns it for the user by using `outConnection` parameter.
 *
 * @param Server                Server object received from the function #oC_Tcp_Listen
 * @param outConnection         Destination address for a new TCP connection
 * @param Timeout               Maximum time to wait for a connection
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` stores incorrect object pointer
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outConnection` is not correct RAM address
 *  oC_ErrorCode_TimeNotCorrect                  | The `Timeout` is below 0
 *  oC_ErrorCode_ModuleBusy                      | Maximum time to wait has expired and the module is still busy
 *  oC_ErrorCode_ServerNotStarted                | Server has not started yet
 *
 *  More error codes can be returned by the functions #oC_Tcp_Server_WaitForConnection and #oC_Tcp_Server_AcceptConnection
 *
 * @see oC_Tcp_Listen, oC_Tcp_Send, oC_Tcp_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Accept( oC_Tcp_Server_t Server , oC_Tcp_Connection_t * outConnection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        if(
            ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                                     , oC_ErrorCode_ObjectNotCorrect         )
         && ErrorCondition( isram(outConnection)                                                , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Timeout >= 0                                                        , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( oC_Tcp_Server_IsRunning(Server)                                     , oC_ErrorCode_ServerNotStarted         )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy , oC_KTime_CalculateTimeout(endTimestamp)) , oC_ErrorCode_ModuleBusy               )
            )
        {
            oC_Mutex_Give(ModuleBusy);
            while(
                   ErrorCode( oC_Tcp_Server_WaitForConnection(Server, outConnection, oC_KTime_CalculateTimeout(endTimestamp)) )
                   )
            {
                tcplog(oC_LogType_Info, "[%p] New TCP connection detected, accepting\n", *outConnection);
                if(ErrorCode( oC_Tcp_Server_AcceptConnection(Server, *outConnection, s(2)) ))
                {
                    errorCode = oC_ErrorCode_None;
                    break;
                }
            }
            if(oC_ErrorOccur(errorCode))
            {
                tcplog(oC_LogType_Error, "Cannot accept connection: %R\n", errorCode);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends data by using TCP connection
 *
 * The function is for sending data by using TCP connection received from functions #oC_Tcp_Accept or #oC_Tcp_Connect.
 *
 * @note
 * You can also use directly the function #oC_Tcp_Connection_Send
 *
 * @param Connection            TCP connection object received from #oC_Tcp_Accept or #oC_Tcp_Connect
 * @param Buffer                Buffer with data to send
 * @param Size                  Size of the buffer to send
 * @param Timeout               Maximum time to wait for sending
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *
 *  More error codes can be returned by the function #oC_Tcp_Connection_Send
 *
 * @see oC_Tcp_Accept, oC_Tcp_Connect, oC_Tcp_Connection_Send, oC_Tcp_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Send( oC_Tcp_Connection_t Connection, const void *    Buffer, oC_MemorySize_t Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        errorCode = oC_Tcp_Connection_Send(Connection, Buffer, Size, Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives data by using TCP connection
 *
 * The function is for receiving data by using TCP connection received from functions #oC_Tcp_Accept or #oC_Tcp_Connect.
 *
 * @note
 * You can also use directly the function #oC_Tcp_Connection_Receive
 *
 * @param Connection            TCP connection object received from #oC_Tcp_Accept or #oC_Tcp_Connect
 * @param outBuffer             Buffer for received data
 * @param Size                  Size of the buffer
 * @param Timeout               Maximum time to wait for sending
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off - you should enable the module first
 *
 *  More error codes can be returned by the function #oC_Tcp_Connection_Receive
 *
 * @see oC_Tcp_Accept, oC_Tcp_Connect, oC_Tcp_Connection_Receive, oC_Tcp_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Receive( oC_Tcp_Connection_t Connection,       void * outBuffer, oC_MemorySize_t* Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Tcp))
    {
        errorCode = oC_Tcp_Connection_Receive(Connection, outBuffer, Size, Timeout);
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCALS_SECTION_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief searches for a server that contains the given connection
 */
//==========================================================================================================================================
static oC_Tcp_Server_t GetServerWithConnection( oC_Tcp_Connection_t Connection )
{
    oC_Tcp_Server_t foundServer = NULL;

    foreach(Servers,server)
    {
        if(oC_Tcp_Server_ContainsConnection(server,Connection))
        {
            foundServer = server;
            break;
        }
    }

    return foundServer;
}

//==========================================================================================================================================
/**
 * @brief returns first server that has been created by the given process
 */
//==========================================================================================================================================
static oC_Tcp_Server_t GetServerCreatedBy( oC_Process_t Process )
{
    oC_Tcp_Server_t foundServer = NULL;

    foreach(Servers, server)
    {
        if(oC_Tcp_Server_GetProcess(server) == Process)
        {
            foundServer = server;
            break;
        }
    }

    return foundServer;
}

//==========================================================================================================================================
/**
 * @brief returns first connection that has been created by the given process
 */
//==========================================================================================================================================
static oC_Tcp_Connection_t GetConnectionCreatedBy( oC_Process_t Process )
{
    oC_Tcp_Connection_t foundConnection = NULL;

    foreach(Connections,connection)
    {
        if(oC_Tcp_Connection_GetProcess(connection) == Process)
        {
            foundConnection = connection;
            break;
        }
    }

    return foundConnection;
}

//==========================================================================================================================================
/**
 * @brief closes all servers that has been created by the given process
 */
//==========================================================================================================================================
static oC_ErrorCode_t CloseServersCreatedBy( oC_Process_t Process , oC_Time_t Timeout )
{
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_None;
    oC_Tcp_Server_t server          = NULL;
    oC_Timestamp_t  endTimestamp    = oC_KTime_GetTimestamp() + Timeout;

    while( (server = GetServerCreatedBy(Process)) != NULL )
    {
        if(oC_Tcp_Server_IsRunning(server))
        {
            ErrorCode( oC_Tcp_Server_Stop(server) );
        }
        bool removedFromList = oC_List_RemoveAll(Servers,server);

        ErrorCondition( removedFromList                                                         , oC_ErrorCode_CannotRemoveObjectFromList   );
        ErrorCondition( oC_Tcp_Server_Delete( &server, oC_KTime_CalculateTimeout(endTimestamp) ), oC_ErrorCode_ReleaseError                 );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief closes all connections that has been created by the given process
 */
//==========================================================================================================================================
static oC_ErrorCode_t CloseConnectionsCreatedBy( oC_Process_t Process , oC_Time_t Timeout )
{
    oC_ErrorCode_t      errorCode       = oC_ErrorCode_None;
    oC_Tcp_Connection_t connection      = NULL;
    oC_Timestamp_t      endTimestamp    = oC_KTime_GetTimestamp() + Timeout;

    while( (connection = GetConnectionCreatedBy(Process)) != NULL )
    {
        if(oC_Tcp_Connection_IsConnected(connection))
        {
            ErrorCode( oC_Tcp_Connection_Disconnect(connection, oC_KTime_CalculateTimeout(endTimestamp)) );
        }
        bool removedFromList = oC_List_RemoveAll(Connections,connection);

        ErrorCondition( removedFromList                                                                 , oC_ErrorCode_CannotRemoveObjectFromList   );
        ErrorCondition( oC_Tcp_Connection_Delete( &connection, oC_KTime_CalculateTimeout(endTimestamp) ), oC_ErrorCode_ReleaseError                 );
    }

    return errorCode;
}

#undef  _________________________________________LOCALS_SECTION_____________________________________________________________________________

