/** ****************************************************************************************************************************************
 *
 * @brief      ChocoOS
 *
 * @file       oc_tcp_packet.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_tcp.h>

/** ========================================================================================================================================
 *
 *              The section with macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsPacketTypeCorrect( PacketType )           ( (PacketType) == oC_Net_PacketType_IPv4 || (PacketType) == oC_Net_PacketType_IPv6 )

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_Tcp_OptionKind_t     Kind;
    uint8_t                 Length;
    uint8_t                 Data[4];
} Option_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static Option_t *           GetNextOption       ( Option_t * Option );
static Option_t *           GetFirstOption      ( oC_Tcp_Packet_t * Packet );
static Option_t *           GetLastOption       ( oC_Tcp_Packet_t * Packet );
static Option_t *           GetOption           ( oC_Tcp_Packet_t * Packet , oC_Tcp_OptionKind_t OptionKind );
static uint16_t             CalculateChecksum   ( oC_Tcp_Packet_t * Packet );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Allocates memory for a TCP packet
 *
 * The function is for creating new TCP packet. It allocates memory for a TCP header, user data and IP header. It also fills basic required
 * fields.
 *
 * @param Source            Source IP address (sender)
 * @param Destination       Destination IP address (receiver)
 * @param HeaderSize        Size of the TCP header (minimum sizeof(oC_Tcp_Header_t) ). It is required, because TCP allows for adding options to the header
 * @param Data              #NULL if not used or pointer to the buffer with data to copy inside TCP packet
 * @param Size              Size of the data to put inside the TCP packet
 *
 * @return pointer to the allocated packet or #NULL in case of error
 *
 * @see oC_Tcp_Packet_Delete
 */
//==========================================================================================================================================
oC_Tcp_Packet_t * oC_Tcp_Packet_New( const oC_Net_Address_t * Source, const oC_Net_Address_t * Destination, uint16_t HeaderSize , const void * Data , uint16_t Size )
{
    oC_Tcp_Packet_t * packet        = NULL;
    uint16_t          packetSize    = HeaderSize + Size + ( sizeof(uint32_t) - (HeaderSize % sizeof(uint32_t)));

    if(
        oC_SaveIfFalse("TCP::Packet::New ", Data == NULL || isaddresscorrect(Data)          , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("TCP::Packet::New ", Data == NULL || Size > 0                        , oC_ErrorCode_SizeNotCorrect       )
     && oC_SaveIfFalse("TCP::Packet::New ", isaddresscorrect(Source)                        , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("TCP::Packet::New ", isaddresscorrect(Destination)                   , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("TCP::Packet::New ", oC_Net_IsAddressCorrect(Source)                 , oC_ErrorCode_IpAddressNotCorrect  )
     && oC_SaveIfFalse("TCP::Packet::New ", oC_Net_IsAddressCorrect(Destination)            , oC_ErrorCode_IpAddressNotCorrect  )
     && oC_SaveIfFalse("TCP::Packet::New ", Source->Type == Destination->Type               , oC_ErrorCode_TypeNotCorrect       )
     && oC_SaveIfFalse("TCP::Packet::New ", packetSize > Size && packetSize >= HeaderSize   , oC_ErrorCode_SizeTooBig           )
     && oC_SaveIfFalse("TCP::Packet::New ", HeaderSize >= sizeof(oC_Tcp_Header_t)           , oC_ErrorCode_SizeNotCorrect       )
        )
    {
        packet = (oC_Tcp_Packet_t*)oC_Net_Packet_New( getcurallocator(), (oC_Net_PacketType_t)Source->Type, NULL, packetSize );

        if(oC_SaveIfFalse("TCP::Packet::New ", packet != NULL, oC_ErrorCode_AllocationError))
        {
            if(Source->Type == oC_Net_AddressType_IPv4)
            {
                packet->IPv4.IpHeader.Checksum              = 0;
                packet->IPv4.IpHeader.DF                    = 0;
                packet->IPv4.IpHeader.FragmentOffset        = 0;
                packet->IPv4.IpHeader.IHL                   = 5;
                packet->IPv4.IpHeader.SourceIp              = Source->IPv4;
                packet->IPv4.IpHeader.DestinationIp         = Destination->IPv4;
                packet->IPv4.IpHeader.Protocol              = oC_Net_Protocol_TCP;

                packet->IPv4.TcpHeader.SourcePort           = Source->Port;
                packet->IPv4.TcpHeader.DestinationPort      = Destination->Port;
                packet->IPv4.TcpHeader.DataOffset           = HeaderSize / sizeof(uint32_t) + ( HeaderSize % sizeof(uint32_t) != 0 ? 1 : 0 );

                if(Data != NULL)
                {
                    memcpy( packet->IPv4.Data, Data , Size);
                }
            }
            else
            {
                packet->IPv6.IpHeader.Source.HighPart       = Source->IPv6.HighPart;
                packet->IPv6.IpHeader.Source.LowPart        = Source->IPv6.LowPart;

                packet->IPv6.IpHeader.Destination.HighPart  = Destination->IPv6.HighPart;
                packet->IPv6.IpHeader.Destination.LowPart   = Destination->IPv6.LowPart;

                if(Data != NULL)
                {
                    memcpy( packet->IPv6.Data, Data , Size);
                }
            }
        }
    }

    return packet;
}

//==========================================================================================================================================
/**
 * @brief releases memory allocated for a packet
 *
 * The function is for releasing memory allocated by the function #oC_Tcp_Packet_New.
 *
 * @param Packet        Pointer to the variable that keeps allocated packet - it will be set to #NULL after usage
 *
 * @return true if packet was deleted
 *
 * @see oC_Tcp_Packet_New
 */
//==========================================================================================================================================
bool oC_Tcp_Packet_Delete( oC_Tcp_Packet_t ** Packet )
{
    bool deleted = false;

    if(
        oC_SaveIfFalse("TCP::Packet::Delete ", isram(Packet)  , oC_ErrorCode_OutputAddressNotInRAM           )
     && oC_SaveIfFalse("TCP::Packet::Delete ", isram(*Packet) , oC_ErrorCode_AddressNotInRam                 )
        )
    {
        deleted = oC_Net_Packet_Delete((oC_Net_Packet_t**)Packet);
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief sets data in the TCP packet
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Packet_SetData( oC_Tcp_Packet_t * Packet , const void * Data , uint16_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(Packet)           , oC_ErrorCode_AddressNotInRam  )
     && ErrorCondition( isaddresscorrect(Data)  , oC_ErrorCode_WrongAddress     )
        )
    {
        oC_Tcp_Header_t * header = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if( ErrorCondition( header != NULL, oC_ErrorCode_PacketNotCorrect ) )
        {
            oC_Net_Packet_SetSize(&Packet->Packet,Size + (header->DataOffset * sizeof(uint32_t)));

            void *      data            = NULL;
            uint32_t *  tcpPacketArray  = (uint32_t*)header;

            data = &tcpPacketArray[header->DataOffset];

            memcpy(data, Data, Size);

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads data of the TCP packet
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Packet_ReadData( oC_Tcp_Packet_t * Packet , void * outData    , oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
       ErrorCondition( isram(Packet)    , oC_ErrorCode_AddressNotInRam          )
    && ErrorCondition( isram(outData)   , oC_ErrorCode_OutputAddressNotInRAM    )
       )
    {
       oC_Tcp_Header_t * header = oC_Net_Packet_GetDataReference(&Packet->Packet);

       if( ErrorCondition( header != NULL, oC_ErrorCode_PacketNotCorrect ) )
       {
           uint32_t    dataSize        = oC_Tcp_Packet_GetDataSize(Packet);
           void *      data            = NULL;
           uint32_t *  tcpPacketArray  = (uint32_t*)header;

           data = &tcpPacketArray[header->DataOffset];

           if(ErrorCondition( Size >= dataSize , oC_ErrorCode_OutputBufferTooSmall ))
           {
               memcpy(outData, data, dataSize);
               errorCode = oC_ErrorCode_None;
           }
       }
    }

   return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets size of the TCP packet
 */
//==========================================================================================================================================
void oC_Tcp_Packet_SetSize( oC_Tcp_Packet_t * Packet , uint16_t Size )
{
    if(isram(Packet))
    {
        oC_Tcp_Header_t * header = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if( header != NULL )
        {
            oC_Net_Packet_SetSize(&Packet->Packet,Size + (header->DataOffset * sizeof(uint32_t)));
        }
    }
}

//==========================================================================================================================================
/**
 * @brief returns pointer to the data inside the packet
 *
 * The function returns pointer to the data inside TCP packet.
 *
 * @param Packet    TCP packet allocated by the function #oC_Tcp_Packet_New
 *
 * @return pointer to the data section inside TCP packet
 */
//==========================================================================================================================================
void * oC_Tcp_Packet_GetDataReference( oC_Tcp_Packet_t * Packet )
{
    void * data = NULL;

    if(oC_SaveIfFalse("TCP::Packet::GetDataRef " , isram(Packet) , oC_ErrorCode_AddressNotInRam ))
    {
        uint16_t            packetSize  = oC_Net_GetPacketSize(&Packet->Packet,false);
        oC_Tcp_Header_t *   header      = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if(
            oC_SaveIfFalse("TCP::Packet::GetDataRef ", header != NULL                                           , oC_ErrorCode_PacketNotCorrect )
         && oC_SaveIfFalse("TCP::Packet::GetDataRef ", (packetSize > (header->DataOffset * sizeof(uint32_t)))   , oC_ErrorCode_SizeNotCorrect   )
            )
        {
            uint32_t * tcpPacketArray = (uint32_t*)header;

            data = &tcpPacketArray[header->DataOffset];
        }
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief returns size of data section inside TCP packet
 *
 * The function returns size of the data inside TCP packet
 *
 * @param Packet    TCP packet allocated by the function #oC_Tcp_Packet_New
 *
 * @return size of data inside TCP packet
 */
//==========================================================================================================================================
uint16_t oC_Tcp_Packet_GetDataSize( oC_Tcp_Packet_t * Packet )
{
    uint16_t size = 0;

    if(oC_SaveIfFalse("Reference to the Packet", isram(Packet) , oC_ErrorCode_AddressNotInRam))
    {
        uint16_t            packetSize  = oC_Net_GetPacketSize(&Packet->Packet,false);
        oC_Tcp_Header_t *   header      = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if(
            oC_SaveIfFalse("header is NULL" , header != NULL                                           , oC_ErrorCode_PacketNotCorrect )
         && oC_SaveIfFalse("packet size"    , (packetSize >= (header->DataOffset * sizeof(uint32_t)))  , oC_ErrorCode_SizeNotCorrect   )
            )
        {
            size = oC_Net_GetPacketSize(&Packet->Packet, false) - (header->DataOffset * sizeof(uint32_t));
        }
        else
        {
            kdebuglog(oC_LogType_Error, "TCP::Packet::GetDataSize - header = %p, packetSize = %d Bytes (data offset = %d)", header, packetSize, (header->DataOffset * sizeof(uint32_t)));
        }
    }
    else
    {
        kdebuglog(oC_LogType_Error, "TCP::Packet::GetDataSize - Packet = %p", Packet);
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief Adds TCP option to the packet
 *
 * The function is for adding options to the TCP packet. Please check TCP RFC 2780 for more informations.
 *
 * @param Packet            TCP packet allocated by the function #oC_Tcp_Packet_New
 * @param OptionKind        Kind of the option to add
 * @param Data              Pointer to data to put inside the option or #NULL if not used (in this case `Size` must be set to 0)
 * @param Size              Size (Length) of the option's data (please check the RFC to find out what size can be given for the option - the function does not check if the size is correct)
 *
 * @return true if option has been added
 */
//==========================================================================================================================================
bool oC_Tcp_Packet_AddOption( oC_Tcp_Packet_t * Packet , oC_Tcp_OptionKind_t OptionKind , const void * Data , uint8_t Size )
{
    bool success = false;

    if(
        oC_SaveIfFalse("TCP::Packet::AddOption ", isram(Packet)                         , oC_ErrorCode_AddressNotInRam   )
     && oC_SaveIfFalse("TCP::Packet::AddOption ", OptionKind <= oC_Tcp_OptionKind_Max   , oC_ErrorCode_ValueTooBig       )
     && oC_SaveIfFalse("TCP::Packet::AddOption ", Size == 0 || isaddresscorrect(Data)   , oC_ErrorCode_DataNotAvailable  )
        )
    {
        oC_Tcp_Header_t * header        = oC_Net_Packet_GetDataReference((oC_Net_Packet_t*)Packet);
        uint16_t          packetSize    = oC_Net_GetPacketSize((oC_Net_Packet_t*)Packet,false);
        uint8_t           optionLength  = Size + 2;
        uint32_t          oldDataOffset = header->DataOffset;

        // Update DataOffset to reflect the new header size
        header->DataOffset += (optionLength + sizeof(uint32_t) - 1) / sizeof(uint32_t);
        Option_t* lastOption = GetLastOption(Packet);

        if(
            oC_SaveIfFalse("TCP::Packet::AddOption ", header != NULL                                           , oC_ErrorCode_PacketNotCorrect  )
         && oC_SaveIfFalse("TCP::Packet::AddOption ", (packetSize > (header->DataOffset * sizeof(uint32_t)))   , oC_ErrorCode_SizeNotCorrect    )
         && oC_SaveIfFalse("TCP::Packet::AddOption ", lastOption != NULL                                       , oC_ErrorCode_NoSpaceAvailable  )
            )
        {
            lastOption->Kind    = OptionKind;
            lastOption->Length  = Size + 2;

            if(Data != NULL)
            {
                memcpy(lastOption->Data, Data, Size);
            }

            success = true;
        }
        else
        {
            header->DataOffset = oldDataOffset;
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads option from the TCP packet
 *
 * The function reads option's data from the TCP packet. Please check TCP RFC 2780 for more informations.
 *
 * @param Packet            TCP packet allocated by the function #oC_Tcp_Packet_New
 * @param OptionKind        Kind of the TCP option to find
 * @param outData           Destination for option data or #NULL if not used
 * @param Size              Destination for the option size on output and size of the `outData` buffer on input
 *
 * @return code of error or `oC_ErrorCode_None` if success.
 *
 * Here is the list of standard error codes that can be returned by the function:
 *
 * ErrorCode                                   | Description
 * --------------------------------------------|--------------------------------------------------------------------------------------------
 * oC_ErrorCode_AddressNotInRam                | `Packet` or `Size` are not correct RAM address
 * oC_ErrorCode_ValueTooBig                    | `OptionKind` is too big
 * oC_ErrorCode_OptionNotFound                 | The given `OptionKind` has not been found in the TCP packet
 * oC_ErrorCode_OutputAddressNotInRAM          | `outData` is not in RAM but it is required (size of the option is not 0)
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Packet_ReadOption( oC_Tcp_Packet_t * Packet , oC_Tcp_OptionKind_t OptionKind , void * outData , uint8_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(Packet)                         , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( OptionKind <= oC_Tcp_OptionKind_Max   , oC_ErrorCode_ValueTooBig              )
     && ErrorCondition( isram(Size)                           , oC_ErrorCode_AddressNotInRam          )
        )
    {
        Option_t * option = GetOption(Packet,OptionKind);

        if(
            ErrorCondition( option != NULL                          , oC_ErrorCode_OptionNotFound        )
         && ErrorCondition( option->Length >= 2                     , oC_ErrorCode_OptionNotCorrect      )
         && ErrorCondition( option->Length == 2 || isram(outData)   , oC_ErrorCode_OutputAddressNotInRAM )
            )
        {
            if(ErrorCondition( *Size >= (option->Length - 2)  , oC_ErrorCode_OutputArrayToSmall))
            {
                if(option->Length > 2)
                {
                    memcpy(outData, option->Data, option->Length - 2);
                }
                errorCode = oC_ErrorCode_None;
            }
            *Size = option->Length;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief clears options in the TCP packet
 *
 * The function clears options list in the TCP packet
 *
 * @param Packet        TCP packet allocated by the function #oC_Tcp_Packet_New
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Tcp_Packet_ClearOptions( oC_Tcp_Packet_t * Packet )
{
    bool cleared = false;

    if(oC_SaveIfFalse("TCP::Packet::ClearOptions " , isram(Packet) , oC_ErrorCode_AddressNotInRam ))
    {
        oC_Tcp_Header_t * header        = oC_Net_Packet_GetDataReference((oC_Net_Packet_t*)Packet);
        uint16_t          packetSize    = oC_Net_GetPacketSize((oC_Net_Packet_t*)Packet,false);
        Option_t *        firstOption   = GetFirstOption(Packet);
        void *            dataRef       = oC_Tcp_Packet_GetDataReference(Packet);

        if(
            oC_SaveIfFalse("TCP::Packet::ClearOption ", header != NULL                                           , oC_ErrorCode_PacketNotCorrect  )
         && oC_SaveIfFalse("TCP::Packet::ClearOption ", (packetSize > (header->DataOffset * sizeof(uint32_t)))   , oC_ErrorCode_SizeNotCorrect    )
         && oC_SaveIfFalse("TCP::Packet::ClearOption ", firstOption != NULL                                      , oC_ErrorCode_OptionNotFound    )
            )
        {
            for(uint8_t * byte = (uint8_t*)firstOption; byte < ((uint8_t*)dataRef) ; byte++)
            {
                *byte = 0;
            }

            cleared = true;
        }

    }

    return cleared;
}

//==========================================================================================================================================
/**
 * @brief calculates checksum for TCP packet
 */
//==========================================================================================================================================
uint16_t oC_Tcp_Packet_CalculateChecksum( oC_Tcp_Packet_t * Packet )
{
    uint16_t checksum = 0;

    if(isram(Packet))
    {
        checksum = CalculateChecksum(Packet);
    }

    return checksum;
}

//==========================================================================================================================================
/**
 * @brief returns TCP header reference
 */
//==========================================================================================================================================
oC_Tcp_Header_t * oC_Tcp_Packet_GetHeaderReference( oC_Tcp_Packet_t * Packet )
{
    return oC_Net_Packet_GetDataReference((oC_Net_Packet_t*)Packet);
}

//==========================================================================================================================================
/**
 * @brief converts packet from network endianess
 */
//==========================================================================================================================================
bool oC_Tcp_Packet_ConvertFromNetworkEndianess( oC_Tcp_Packet_t * Packet )
{
    bool converted = false;

    if(isram(Packet))
    {
        oC_Tcp_Header_t * header = oC_Tcp_Packet_GetHeaderReference(Packet);

        if(isram(header))
        {
            header->SourcePort          = oC_Net_ConvertUint16FromNetworkEndianess(header->SourcePort);
            header->DestinationPort     = oC_Net_ConvertUint16FromNetworkEndianess(header->DestinationPort);
            header->SequenceNumber      = oC_Net_ConvertUint32FromNetworkEndianess(header->SequenceNumber);
            header->AcknowledgmentNumber= oC_Net_ConvertUint32FromNetworkEndianess(header->AcknowledgmentNumber);
            header->WindowSize          = oC_Net_ConvertUint16FromNetworkEndianess(header->WindowSize);
            header->Checksum            = oC_Net_ConvertUint16FromNetworkEndianess(header->Checksum);
            header->UrgentPointer       = oC_Net_ConvertUint16FromNetworkEndianess(header->UrgentPointer);

            uint8_t * array = (uint8_t*)header;

            array[12] = ((array[12] << 4) & 0xF0) | ((array[12] >> 4) & 0x0F);
            array[13] = ((array[13] & 0x80) >> 7)
                      | ((array[13] & 0x40) >> 5)
                      | ((array[13] & 0x20) >> 3)
                      | ((array[13] & 0x10) >> 1)
                      | ((array[13] & 0x08) << 1)
                      | ((array[13] & 0x04) << 3)
                      | ((array[13] & 0x02) << 5)
                      | ((array[13] & 0x01) << 7);

            converted = true;
        }
    }

    return converted;
}

//==========================================================================================================================================
/**
 * @brief converts packet to network endianess
 */
//==========================================================================================================================================
bool oC_Tcp_Packet_ConvertToNetworkEndianess( oC_Tcp_Packet_t * Packet )
{
    bool converted = false;

    if(isram(Packet))
    {
        oC_Tcp_Header_t * header = oC_Tcp_Packet_GetHeaderReference(Packet);

        if(isram(header))
        {
            header->SourcePort          = oC_Net_ConvertUint16ToNetworkEndianess(header->SourcePort);
            header->DestinationPort     = oC_Net_ConvertUint16ToNetworkEndianess(header->DestinationPort);
            header->SequenceNumber      = oC_Net_ConvertUint32ToNetworkEndianess(header->SequenceNumber);
            header->AcknowledgmentNumber= oC_Net_ConvertUint32ToNetworkEndianess(header->AcknowledgmentNumber);
            header->WindowSize          = oC_Net_ConvertUint16ToNetworkEndianess(header->WindowSize);
            header->Checksum            = oC_Net_ConvertUint16ToNetworkEndianess(header->Checksum);
            header->UrgentPointer       = oC_Net_ConvertUint16ToNetworkEndianess(header->UrgentPointer);

            uint8_t * array = (uint8_t*)header;

            array[12] = ((array[12] << 4) & 0xF0) | ((array[12] >> 4) & 0x0F);
            array[13] = ((array[13] & 0x80) >> 7)
                      | ((array[13] & 0x40) >> 5)
                      | ((array[13] & 0x20) >> 3)
                      | ((array[13] & 0x10) >> 1)
                      | ((array[13] & 0x08) << 1)
                      | ((array[13] & 0x04) << 3)
                      | ((array[13] & 0x02) << 5)
                      | ((array[13] & 0x01) << 7);

            converted = true;
        }
    }

    return converted;
}


#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief returns pointer to the next TCP option
 */
//==========================================================================================================================================
static Option_t * GetNextOption( Option_t * Option )
{
    Option_t * nextOption = NULL;

    if(Option != NULL)
    {
        uint8_t * array = (uint8_t*)Option;

        nextOption = (Option_t*)&array[Option->Length + 2];
    }

    return nextOption;
}

//==========================================================================================================================================
/**
 * @brief returns pointer to the first TCP option inside TCP packet
 */
//==========================================================================================================================================
static Option_t * GetFirstOption( oC_Tcp_Packet_t * Packet )
{
    Option_t *          firstOption = NULL;
    uint8_t *           array       = oC_Net_Packet_GetDataReference(&Packet->Packet);
    void *              dataRef     = oC_Tcp_Packet_GetDataReference(Packet);

    if(array != NULL && dataRef != NULL)
    {
        firstOption = (Option_t*)&array[sizeof(oC_Tcp_Header_t)];

        if(firstOption >= ((Option_t*)dataRef))
        {
            firstOption = NULL;
        }
    }

    return firstOption;
}

//==========================================================================================================================================
/**
 * @brief returns pointer to the last TCP option in the packet
 */
//==========================================================================================================================================
static Option_t * GetLastOption( oC_Tcp_Packet_t * Packet )
{
    Option_t *  lastOption      = NULL;
    Option_t *  dataReference   = oC_Tcp_Packet_GetDataReference(Packet);

    if(dataReference != NULL)
    {
        lastOption = GetFirstOption(Packet);

        while(lastOption != NULL && lastOption < dataReference && lastOption->Kind != oC_Tcp_OptionKind_End)
        {
            lastOption = GetNextOption(lastOption);
        }

        if(lastOption >= dataReference)
        {
            lastOption = NULL;
        }
    }

    return lastOption;
}

//==========================================================================================================================================
/**
 * @brief searches for the given option in the TCP packet
 */
//==========================================================================================================================================
static Option_t * GetOption( oC_Tcp_Packet_t * Packet , oC_Tcp_OptionKind_t OptionKind )
{
    Option_t *  option          = NULL;
    Option_t *  dataReference   = oC_Tcp_Packet_GetDataReference(Packet);

    if(dataReference != NULL)
    {
        option = GetFirstOption(Packet);

        while(option != NULL && option < dataReference && option->Kind != oC_Tcp_OptionKind_End && option->Kind != OptionKind)
        {
            option = GetNextOption(option);
        }

        /* Checking against NULL is done for prevention against reading option->Kind when option is NULL */
        if(option >= dataReference || option == NULL || option->Kind != OptionKind)
        {
            option = NULL;
        }
    }

    return option;
}

//==========================================================================================================================================
/**
 * @brief calculates checksum for a TCP packet
 */
//==========================================================================================================================================
static uint16_t CalculateChecksum( oC_Tcp_Packet_t * Packet )
{
    uint32_t               checksum         = 0;
    oC_Net_PacketType_t    packetType       = oC_Net_Packet_GetType(&Packet->Packet);
    uint16_t               savedChecksum    = 0;

    if(packetType == oC_Net_PacketType_IPv4)
    {
        savedChecksum = Packet->IPv4.TcpHeader.Checksum;

        uint16_t  tcpPacketSize      = oC_Net_GetPacketSize(&Packet->Packet, false);
        uint32_t  sourceAddress      = oC_Net_ConvertUint32ToNetworkEndianess(Packet->IPv4.IpHeader.SourceIp);
        uint32_t  destinationAddress = oC_Net_ConvertUint32ToNetworkEndianess(Packet->IPv4.IpHeader.DestinationIp);
        uint16_t  protocol           = oC_Net_ConvertUint16ToNetworkEndianess(oC_Net_Protocol_TCP);

        checksum   += (sourceAddress      >> 16) & 0xFFFF;
        checksum   += (sourceAddress      >>  0) & 0xFFFF;

        checksum   += (destinationAddress >> 16) & 0xFFFF;
        checksum   += (destinationAddress >>  0) & 0xFFFF;

        checksum   += protocol;

        checksum   += oC_Net_ConvertUint16ToNetworkEndianess(tcpPacketSize);

        Packet->IPv4.TcpHeader.Checksum = 0;

        oC_Tcp_Packet_ConvertToNetworkEndianess(Packet);

        while(checksum >> 16)
        {
            checksum = (checksum & 0xFFFF) + (checksum >> 16);
        }

        checksum = oC_Net_CalculateChecksum(&Packet->IPv4.TcpHeader, tcpPacketSize, checksum, false, true);

        oC_Tcp_Packet_ConvertFromNetworkEndianess(Packet);

        Packet->IPv4.TcpHeader.Checksum = savedChecksum;
    }

    return oC_Net_ConvertUint16FromNetworkEndianess((uint16_t)checksum);
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

