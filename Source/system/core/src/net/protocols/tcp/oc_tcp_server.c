/** ****************************************************************************************************************************************
 *
 * @brief      Stores implementation of TCP server functions
 *
 * @file       oc_tcp_server.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak@chocoos.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_tcp.h>
#include <oc_object.h>
#include <oc_stdio.h>
#include <oc_thread.h>
#include <oc_dynamic_config.h>
#include <oc_semaphore.h>
#include <oc_intman.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores data for handling connections array
 *
 * The type is for storing data of connections slots.
 */
//==========================================================================================================================================
typedef struct
{
    oC_Tcp_Connection_t     Connection;     //!< Connection object or #NULL if it is not used
    bool                    Handled;        //!< `true` if the slot is already handled by an application
} ConnectionSlot_t;

//==========================================================================================================================================
/**
 * @brief redefinition of type for storing pointer to the connection finished function
 */
//==========================================================================================================================================
typedef oC_Tcp_ConnectionFunction_t ConnFinishedFunction_t;

//==========================================================================================================================================
/**
 * @brief stores TCP server data
 */
//==========================================================================================================================================
struct Server_t
{
    oC_ObjectControl_t              ObjectControl;                  //!< Special field for storing magic cookie that helps to recognize correct TCP server object
    oC_Tcp_Connection_Config_t      ConnectionConfig;               //!< Configuration of connections of the server
    oC_Process_t                    Process;                        //!< Process assiociated with the server
    oC_Thread_t                     Thread;                         //!< Thread for handling the server
    oC_Semaphore_t                  NewConnectionSemaphore;         //!< Semaphore that counts new connections - connections that are currently not handled
    oC_Semaphore_t                  FreeSlotsSemaphore;             //!< Semaphore that counts free slots for connections
    char                            Name[30];                       //!< Name of the TCP server
    ConnFinishedFunction_t          ConnectionFinishedFunction;     //!< Pointer to the function to call when the connection has finished
    void *                          ConnectionFinishedParameter;    //!< Parameter to give to the connection finished function
    bool                            Running;                        //!< The flag is set to true if the server is already running
    uint32_t                        MaxConnections;                 //!< Maximum number of connections that server can handle
    oC_List(oC_Tcp_Connection_t)    KnownConnections;               //!< List of known connections
    ConnectionSlot_t                Connections[1];                 //!< Array with TCP connections slots @warning IT ALWAYS HAS TO BE THE LAST FIELD IN THIS STRUCTURE!
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool ConnectionFinished  ( oC_Tcp_Connection_t Connection, oC_Tcp_Server_t Server, void* EventParameter );
static bool IsConnectionKnown   ( oC_Tcp_Connection_t Connection, oC_Tcp_Server_t Server, void* EventParameter );
static void ServerThread        ( oC_Tcp_Server_t Server );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief creates TCP server
 *
 * The function is for allocating memory for the new TCP server.
 *
 * @note
 * The function will associate the current process with the server. It will be destroyed when the process will be killed
 *
 * @warning
 * The TCP port has to be reserved before by calling the function #oC_Tcp_ReservePort !
 *
 * @param ListenAddress     Address to listen (with with filled field `Port`)
 * @param MaxConnections    Maximum number of connections handled by the server
 *
 * @return allocated TCP server
 */
//==========================================================================================================================================
oC_Tcp_Server_t oC_Tcp_Server_New( const oC_Net_Address_t * ListenAddress , uint32_t MaxConnections )
{
    oC_Tcp_Server_t server = NULL;

    if(
        oC_SaveIfFalse("TCP::Server::New " , isaddresscorrect(ListenAddress)                                , oC_ErrorCode_WrongAddress                     )
     && oC_SaveIfFalse("TCP::Server::New " , oC_Net_IsAddressCorrect(ListenAddress)                         , oC_ErrorCode_IpAddressNotCorrect              )
     && oC_SaveIfFalse("TCP::Server::New " , ListenAddress->Port > 0                                        , oC_ErrorCode_PortNotCorrect                   )
     && oC_SaveIfFalse("TCP::Server::New " , oC_Tcp_IsPortReserved(ListenAddress->Port)                     , oC_ErrorCode_PortNotReserved                  )
     && oC_SaveIfFalse("TCP::Server::New " , oC_Tcp_IsPortReservedBy(ListenAddress->Port,getcurprocess())   , oC_ErrorCode_PortReservedByDifferentProcess   )
     && oC_SaveIfFalse("TCP::Server::New " , MaxConnections > 0                                             , oC_ErrorCode_MaximumValueNotCorrect           )
        )
    {
        server = malloc( sizeof(struct Server_t) + (sizeof(ConnectionSlot_t) * MaxConnections) , AllocationFlags_ZeroFill );

        if( oC_SaveIfFalse("TCP::Server::New ", server != NULL , oC_ErrorCode_AllocationError) )
        {

            sprintf(server->Name, "TCP Server:%d/%d", ListenAddress->Port, MaxConnections);

            server->Thread                      = oC_Thread_New( 0, oC_DynamicConfig_GetValue(Tcp,StackSize), NULL, server->Name, (oC_Thread_Function_t)ServerThread, server );
            server->NewConnectionSemaphore      = oC_Semaphore_New( MaxConnections, 0             , getcurallocator(), AllocationFlags_ZeroFill );
            server->FreeSlotsSemaphore          = oC_Semaphore_New( MaxConnections, MaxConnections, getcurallocator(), AllocationFlags_ZeroFill );
            server->KnownConnections            = oC_List_New( getcurallocator(), AllocationFlags_ZeroFill );

            if(
                oC_SaveIfFalse("TCP::Server::New ", server->Thread != NULL                      , oC_ErrorCode_CannotCreateThread )
             && oC_SaveIfFalse("TCP::Server::New ", server->NewConnectionSemaphore!= NULL       , oC_ErrorCode_AllocationError    )
             && oC_SaveIfFalse("TCP::Server::New ", server->FreeSlotsSemaphore!= NULL           , oC_ErrorCode_AllocationError    )
             && oC_SaveIfFalse("TCP::Server::New ", server->KnownConnections != NULL            , oC_ErrorCode_AllocationError    )
                )
            {
                server->ObjectControl                       = oC_CountObjectControl( server, oC_ObjectId_TcpServer );
                server->MaxConnections                      = MaxConnections;
                server->Process                             = getcurprocess();
                server->ConnectionConfig.RemoteAddress.Type = ListenAddress->Type;

                memcpy(&server->ConnectionConfig.LocalAddress, ListenAddress, sizeof(oC_Net_Address_t));

                server->ConnectionConfig.LocalWindowSize            = oC_DynamicConfig_GetValue(Tcp,WindowSize);
                server->ConnectionConfig.LocalWindowScale           = oC_DynamicConfig_GetValue(Tcp,WindowScale);
                server->ConnectionConfig.ConfirmationTimeout        = oC_DynamicConfig_GetValue(Tcp,ConfirmationTimeout);
                server->ConnectionConfig.ExpirationTimeout          = oC_DynamicConfig_GetValue(Tcp,ExpirationTimeout);
                server->ConnectionConfig.SendingAcknowledgeTimeout  = oC_DynamicConfig_GetValue(Tcp,SendingAcknowledgeTimeout);
                server->ConnectionConfig.ReadSegmentTimeout         = oC_DynamicConfig_GetValue(Tcp,ReadSegmentTimeout);
                server->ConnectionConfig.SaveSegmentTimeout         = oC_DynamicConfig_GetValue(Tcp,SaveSegmentTimeout);
                server->ConnectionConfig.ReceiveTimeout             = oC_DynamicConfig_GetValue(Tcp,ReceiveTimeout);
                server->ConnectionConfig.ResendSegmentTime          = oC_DynamicConfig_GetValue(Tcp,ResendSegmentTime);
                server->ConnectionConfig.PacketSize                 = oC_DynamicConfig_GetValue(Tcp,PacketSize);
                server->ConnectionConfig.InitialAcknowledgeNumber   = 0;
                server->ConnectionConfig.InitialSequenceNumber      = 0;
                server->ConnectionConfig.Callbacks[oC_Tcp_CallbackGroup_Internal][oC_Tcp_EventId_ConnectionFinished].Function   = (oC_Tcp_ConnectionFunction_t)ConnectionFinished;
                server->ConnectionConfig.Callbacks[oC_Tcp_CallbackGroup_Internal][oC_Tcp_EventId_ConnectionFinished].Parameter  = server;
                server->ConnectionConfig.Callbacks[oC_Tcp_CallbackGroup_Internal][oC_Tcp_EventId_NewConnectionVerification].Function   = (oC_Tcp_ConnectionFunction_t)IsConnectionKnown;
                server->ConnectionConfig.Callbacks[oC_Tcp_CallbackGroup_Internal][oC_Tcp_EventId_NewConnectionVerification].Parameter  = server;
            }
            else
            {
                oC_SaveIfFalse("TCP::Server::New Thread "   , server->Thread                    == NULL || oC_Thread_Delete   ( &server->Thread )                   , oC_ErrorCode_ReleaseError );
                oC_SaveIfFalse("TCP::Server::New Semaphore ", server->NewConnectionSemaphore    == NULL || oC_Semaphore_Delete( &server->NewConnectionSemaphore,0)  , oC_ErrorCode_ReleaseError );
                free(server, AllocationFlags_Default);
                server = NULL;
            }
        }
    }

    return server;
}

//==========================================================================================================================================
/**
 * @brief deletes TCP server
 *
 * The function release memory allocated for the TCP Server object and deletes all children TCP connections.
 *
 * @param Server        Pointer to the variable that stores TCP server (it will be cleared in case of success
 * @param Timeout       Maximum time for operation
 *
 * @return true if the TCP server has been deleted
 */
//==========================================================================================================================================
bool oC_Tcp_Server_Delete( oC_Tcp_Server_t * Server , oC_Time_t Timeout )
{
    bool            deleted         = false;
    oC_Timestamp_t  endTimestamp    = oC_KTime_GetTimestamp() + Timeout;

    if(
        oC_SaveIfFalse("Server reference" , isram(Server)                        , oC_ErrorCode_AddressNotInRam      )
     && oC_SaveIfFalse("Server object"    , oC_Tcp_Server_IsCorrect(*Server)     , oC_ErrorCode_ObjectNotCorrect     )
     && oC_SaveIfFalse("Timeout"          , Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect       )
        )
    {
        (*Server)->ObjectControl = 0;

        bool allConnectionsDeleted = true;
        bool threadDeleted         = false;
        bool eventDeleted          = false;
        bool event2Deleted         = false;

        for(uint32_t i = 0; i < (*Server)->MaxConnections; i++)
        {
            if((*Server)->Connections[i].Connection != NULL)
            {
                allConnectionsDeleted = oC_Tcp_Connection_Delete(&((*Server)->Connections[i].Connection), oC_KTime_CalculateTimeout(endTimestamp)) && allConnectionsDeleted;
            }
        }

        threadDeleted = oC_Thread_Delete(&(*Server)->Thread);
        eventDeleted  = oC_Semaphore_Delete(&(*Server)->NewConnectionSemaphore   , AllocationFlags_Default);
        event2Deleted = oC_Semaphore_Delete(&(*Server)->FreeSlotsSemaphore       , AllocationFlags_Default);

        if(
            oC_SaveIfFalse("Main object"             , free( *Server, AllocationFlags_Default ) , oC_ErrorCode_ReleaseError         )
         && oC_SaveIfFalse("One of connections"      , allConnectionsDeleted                    , oC_ErrorCode_CannotDeleteObject   )
         && oC_SaveIfFalse("Server Thread"           , threadDeleted                            , oC_ErrorCode_CannotDeleteThread   )
         && oC_SaveIfFalse("New Connection Semaphore", eventDeleted                             , oC_ErrorCode_ReleaseError         )
         && oC_SaveIfFalse("Free Slot Semaphore"     , event2Deleted                            , oC_ErrorCode_ReleaseError         )
            )
        {
            *Server = NULL;
            deleted = true;
        }
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief checks if the TCP server object is correct
 *
 * The function checks if the TCP server object is correct (if it is allocated by the function #oC_Tcp_Server_New and if it has not been
 * deleted by the function #oC_Tcp_Server_Delete)
 *
 * @param Server        TCP Server object created by the function #oC_Tcp_Server_New
 *
 * @return true if the given TCP Server object is correct
 */
//==========================================================================================================================================
bool oC_Tcp_Server_IsCorrect( oC_Tcp_Server_t Server )
{
    return isram(Server) && oC_CheckObjectControl( Server, oC_ObjectId_TcpServer, Server->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief checks if the TCP server is already running
 *
 * The function checks if the TCP server is running (if the function #oC_Tcp_Server_Run has been called).
 *
 * @param Server        TCP Server object created by the function #oC_Tcp_Server_New
 *
 * @return true if the given TCP Server is running
 */
//==========================================================================================================================================
bool oC_Tcp_Server_IsRunning( oC_Tcp_Server_t Server )
{
    return oC_Tcp_Server_IsCorrect(Server) && Server->Running;
}

//==========================================================================================================================================
/**
 * @brief sets connection configuration
 *
 * The function is for changing configuration of the connections related with the server. The change will be applied only to new connections
 *
 * @param Server        TCP Server object created by the function #oC_Tcp_Server_New
 * @param Config        Pointer to the configuration to copy
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_WrongAddress                    | `Config` address is not correct
 *  oC_ErrorCode_TimeNotCorrect                  | `ConfirmationTimeout` field in `Config` is not correct
 *  oC_ErrorCode_PortNotCorrect                  | `LocalAddress.Port` field in `Config` is not correct
 *  oC_ErrorCode_SizeNotCorrect                  | `LocalWindowSize` field in `Config` is not correct
 *  oC_ErrorCode_PortNotReserved                 | `LocalAddress.Port` has to be reserved before
 *  oC_ErrorCode_PortReservedByDifferentProcess  | `LocalAddress.Port` has been reserved by different process
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_SetConnectionConfig( oC_Tcp_Server_t Server , const oC_Tcp_Connection_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                                             , oC_ErrorCode_ObjectNotCorrect                 )
     && ErrorCondition( isaddresscorrect(Config)                                                    , oC_ErrorCode_WrongAddress                     )
     && ErrorCondition( Config->ConfirmationTimeout > 0                                             , oC_ErrorCode_TimeNotCorrect                   )
     && ErrorCondition( Config->LocalAddress.Port > 0                                               , oC_ErrorCode_PortNotCorrect                   )
     && ErrorCondition( Config->LocalWindowSize > 0                                                 , oC_ErrorCode_SizeNotCorrect                   )
     && ErrorCondition( oC_Tcp_IsPortReserved( Config->LocalAddress.Port )                          , oC_ErrorCode_PortNotReserved                  )
     && ErrorCondition( oC_Tcp_IsPortReservedBy( Config->LocalAddress.Port, getcurprocess() )       , oC_ErrorCode_PortReservedByDifferentProcess   )
        )
    {
        memcpy(&Server->ConnectionConfig, Config, sizeof(oC_Tcp_Connection_Config_t));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief starts the TCP server
 *
 * The function is for running the TCP server. It starts the TCP Server thread.
 *
 * @param Server        TCP Server object created by the function #oC_Tcp_Server_New
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_AlreadyRunning                  | The function has been called before and the server is running already
 *  oC_ErrorCode_CannotRunThread                 | There was a problem with running the thread
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_Run( oC_Tcp_Server_t Server )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                 , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( Server->Running == false                        , oC_ErrorCode_AlreadyRunning           )
     && ErrorCondition( oC_Thread_Run(Server->Thread) == true           , oC_ErrorCode_CannotRunThread          )
        )
    {
        Server->Running = true;
        errorCode       = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief stops the TCP server
 *
 * The function is for stopping accepting new connections by the TCP server. After call of the function no new connection will be handled
 * at the server. Connections, that are currently active will work until the server will be deleted by the function #oC_Tcp_Server_Delete.
 *
 * @see oC_Tcp_Server_Run
 *
 * @param Server        TCP Server object created by the function #oC_Tcp_Server_New
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotRunning                      | The server is not running (the function #oC_Tcp_Server_Run has not been called)
 *  oC_ErrorCode_CannotDeleteThread              | There was a problem with deleting the thread
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_Stop( oC_Tcp_Server_t Server )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                 , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( Server->Running == true                         , oC_ErrorCode_NotRunning           )
        )
    {
        oC_Thread_t thread = oC_Thread_CloneWithNewStack(Server->Thread, oC_Thread_GetStackSize(Server->Thread));

        if( ErrorCondition( thread != NULL , oC_ErrorCode_AllocationError ) )
        {
            if(
                ErrorCondition( oC_Thread_Cancel(&Server->Thread)      , oC_ErrorCode_CannotDeleteThread )
                )
            {
                Server->Running = false;
                Server->Thread  = thread;
                errorCode       = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfFalse( "TCP::Server::Stop ", oC_Thread_Delete(&thread), oC_ErrorCode_ReleaseError );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Waits for new TCP connection
 *
 * The function waits for a new TCP connection.
 *
 * @warning
 * The connection has to be accepted or rejected as soon as possible, otherwise it can expire.
 *
 * @see oC_Tcp_Server_AcceptConnection, oC_Tcp_Server_RejectConnection
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 * @param outConnection     Pointer to the variable where new TCP connection should be stored
 * @param Timeout           Maximum time to wait for the connection
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotRunning                      | The server is not running (the function #oC_Tcp_Server_Run has not been called)
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outConnection` is not the correct RAM address
 *  oC_ErrorCode_TimeNotCorrect                  | The given `Timeout` is below zero
 *  oC_ErrorCode_Timeout                         | Maximum time to wait has expired
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_WaitForConnection( oC_Tcp_Server_t Server , oC_Tcp_Connection_t * outConnection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(  oC_Tcp_Server_IsCorrect(Server)                                          , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition(  Server->Running == true                                                  , oC_ErrorCode_NotRunning               )
     && ErrorCondition(  isram(outConnection)                                                     , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition(  Timeout >= 0                                                             , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition(  oC_Semaphore_TakeCounting( Server->NewConnectionSemaphore, 1, Timeout )  , oC_ErrorCode_Timeout                  )
        )
    {
        oC_IntMan_EnterCriticalSection();

        errorCode = oC_ErrorCode_InternalDataAreDamaged;
        *outConnection = NULL;

        for(uint32_t i = 0; i < Server->MaxConnections ; i++)
        {
            if(Server->Connections[i].Connection != NULL && Server->Connections[i].Handled == false)
            {
                *outConnection                  = Server->Connections[i].Connection;
                Server->Connections[i].Handled  = true;
                errorCode                       = oC_ErrorCode_None;
                break;
            }
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the server contains the connection
 *
 * The function is for checking if the given TCP server handles the given TCP connection
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 * @param Connection        TCP Connection object received from the function #oC_Tcp_Server_WaitForConnection
 *
 * @return true if the given connection is related with the given server
 */
//==========================================================================================================================================
bool oC_Tcp_Server_ContainsConnection( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection )
{
    bool contains = false;

    if(
        oC_SaveIfFalse("TCP::Server::ContainsConnection - Server "      , oC_Tcp_Server_IsCorrect(Server)           , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse("TCP::Server::ContainsConnection - Connection "  , oC_Tcp_Connection_IsCorrect(Connection)   , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        for(uint32_t i = 0; i < Server->MaxConnections; i++)
        {
            if(Server->Connections[i].Connection != NULL)
            {
                if(Server->Connections[i].Connection == Connection || oC_Tcp_Connection_AreTheSame(Connection,Server->Connections[i].Connection))
                {
                    contains = true;
                    break;
                }
            }
        }
    }

    return contains;
}

//==========================================================================================================================================
/**
 * @brief accepts TCP connection
 *
 * The function is for accepting the TCP connections received from the function #oC_Tcp_Server_WaitForConnection. After call of this function,
 * data can be sending and receiving by using #oC_Tcp_Connection_Send and #oC_Tcp_Connection_Receive
 *
 * @see oC_Tcp_Server_WaitForConnection, oC_Tcp_Server_RejectConnection, oC_Tcp_Connection_Send, oC_Tcp_Connection_Receive
 *
 * @warning
 * Remember, that in case of failure of this function removes the given TCP connection from the servers connections list, otherwise it would
 * block the slot in the list. To prevent memory leakage, it also has to delete the connection, so it cannot be used anymore.
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 * @param Connection        TCP Connection object received from the function #oC_Tcp_Server_WaitForConnection
 * @param Timeout           Maximum time to wait for the connection
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` or `Connection` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotRunning                      | The server is not running (the function #oC_Tcp_Server_Run has not been called)
 *  oC_ErrorCode_TimeNotCorrect                  | The given `Timeout` is below zero
 *  oC_ErrorCode_ConnectionFromDifferentServer   | The given `Connection` is not related with the given `Server`
 *  oC_ErrorCode_Timeout                         | Maximum time to wait has expired
 *
 *  Note, that more error code can be received from the function #oC_Tcp_Connection_Accept
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_AcceptConnection( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                     , oC_ErrorCode_ObjectNotCorrect                 )
     && ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection)             , oC_ErrorCode_ObjectNotCorrect                 )
     && ErrorCondition( Server->Running == true                             , oC_ErrorCode_NotRunning                       )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect                   )
     && ErrorCondition( oC_Tcp_Server_ContainsConnection(Server,Connection) , oC_ErrorCode_ConnectionFromDifferentServer    )
        )
    {
        if( ErrorCode( oC_Tcp_Connection_Accept( Connection, oC_KTime_CalculateTimeout(endTimestamp) ) ) )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            tcplog( oC_LogType_Error, "Cannot accept connection: %R", errorCode );
            oC_SaveIfErrorOccur ( "TCP::Server::Accept ", oC_Tcp_Server_RemoveConnection( Server,  Connection ) );
            oC_SaveIfFalse      ( "TCP::Server::Accept ", oC_Tcp_Connection_Delete(&Connection, Timeout), oC_ErrorCode_ReleaseError );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief rejects TCP connection
 *
 * The function is for rejecting TCP connection received from the function #oC_Tcp_Server_WaitForConnection.
 *
 * @warning
 * The function also removes the given connection from the TCP server's connections list and deletes the TCP connection object. It causes that
 * it cannot be used anymore.
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 * @param Connection        TCP Connection object received from the function #oC_Tcp_Server_WaitForConnection
 * @param Timeout           Maximum time to wait for the connection
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` or `Connection` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotRunning                      | The server is not running (the function #oC_Tcp_Server_Run has not been called)
 *  oC_ErrorCode_TimeNotCorrect                  | The given `Timeout` is below zero
 *  oC_ErrorCode_ConnectionFromDifferentServer   | The given `Connection` is not related with the given `Server`
 *  oC_ErrorCode_Timeout                         | Maximum time to wait has expired
 *
 *  Note, that more error code can be received from the functions #oC_Tcp_Connection_Reject and #oC_Tcp_Server_RemoveConnection
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_RejectConnection( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                     , oC_ErrorCode_ObjectNotCorrect                 )
     && ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection)             , oC_ErrorCode_ObjectNotCorrect                 )
     && ErrorCondition( Server->Running == true                             , oC_ErrorCode_NotRunning                       )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect                   )
     && ErrorCondition( oC_Tcp_Server_ContainsConnection(Server,Connection) , oC_ErrorCode_ConnectionFromDifferentServer    )
        )
    {
        if(
            ErrorCode( oC_Tcp_Server_RemoveConnection( Server,  Connection                                                              )  )
         && ErrorCode( oC_Tcp_Connection_Reject( Connection, oC_KTime_CalculateTimeout(endTimestamp)                                    )  )
         && ErrorCondition( oC_Tcp_Connection_Delete( &Connection, oC_KTime_CalculateTimeout(endTimestamp) ) , oC_ErrorCode_ReleaseError   )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        else if(oC_Tcp_Connection_IsCorrect(Connection))
        {
            oC_SaveIfFalse("TCP::Server::Reject ", oC_Tcp_Connection_Delete(&Connection, Timeout), oC_ErrorCode_ReleaseError );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief adds connection to server's connections list
 *
 * The function is for adding TCP connection to the TCP Sever's connections list.
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 * @param Connection        TCP Connection object received from the function #oC_Tcp_Server_WaitForConnection
 * @param Timeout           Maximum time for operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` or `Connection` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_FoundOnList                     | The given `Server` is already associated with the given `Connection`
 *  oC_ErrorCode_NoFreeSlots                     | There is no more free slots for TCP connections in the server
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_AddConnection( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                                     , oC_ErrorCode_ObjectNotCorrect                 )
     && ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection)                             , oC_ErrorCode_ObjectNotCorrect                 )
     && ErrorCondition( oC_Tcp_Server_ContainsConnection(Server,Connection) == false        , oC_ErrorCode_FoundOnList                      )
     && ErrorCondition( oC_Semaphore_TakeCounting(Server->FreeSlotsSemaphore,1,Timeout)     , oC_ErrorCode_NoFreeSlots                      )
        )
    {
        uint32_t freeIndex = Server->MaxConnections;

        oC_IntMan_EnterCriticalSection();
        for(uint32_t i = 0; i < Server->MaxConnections; i++)
        {
            if(Server->Connections[i].Connection == NULL)
            {
                freeIndex = i;
                break;
            }
        }

        if( ErrorCondition( freeIndex < Server->MaxConnections  , oC_ErrorCode_NoFreeSlots ) )
        {
            Server->Connections[freeIndex].Connection = Connection;
            Server->Connections[freeIndex].Handled    = false;
            errorCode                                 = oC_ErrorCode_None;
            tcplog(oC_LogType_Info, "Connection added: %p", Connection);
        }
        oC_IntMan_ExitCriticalSection();
        if(!oC_ErrorOccur(errorCode))
        {
            oC_Semaphore_GiveCounting(Server->NewConnectionSemaphore, 1);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief removes connection from server's connections list
 *
 * The function removes TCP connection from TCP server's connection list.
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 * @param Connection        TCP Connection object received from the function #oC_Tcp_Server_WaitForConnection
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Server` or `Connection` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_ObjectNotFoundOnList            | The given `Server` is not associated with the given `Connection`
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_RemoveConnection( oC_Tcp_Server_t Server , oC_Tcp_Connection_t Connection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)                     , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection)             , oC_ErrorCode_ObjectNotCorrect     )
        )
    {
        errorCode = oC_ErrorCode_ObjectNotFoundOnList;

        oC_IntMan_EnterCriticalSection();
        for(uint32_t i = 0; i < Server->MaxConnections; i++)
        {
            if(Server->Connections[i].Connection == Connection)
            {
                if(Server->Connections[i].Handled == false)
                {
                    oC_Semaphore_TakeCounting(Server->NewConnectionSemaphore,1,0);
                }
                oC_Semaphore_GiveCounting(Server->FreeSlotsSemaphore,1);
                Server->Connections[i].Connection   = NULL;
                Server->Connections[i].Handled      = false;
                errorCode                           = oC_ErrorCode_None;
                break;
            }
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns process associated with the server
 *
 * The function returns process associated with the given TCP server - it is the process, that called the function #oC_Tcp_Server_New.
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 *
 * @return process associated with the given TCP server
 */
//==========================================================================================================================================
oC_Process_t oC_Tcp_Server_GetProcess( oC_Tcp_Server_t Server )
{
    oC_Process_t process = NULL;

    if(oC_SaveIfFalse("Server object", oC_Tcp_Server_IsCorrect(Server), oC_ErrorCode_ObjectNotCorrect))
    {
        process = Server->Process;
    }

    return process;
}

//==========================================================================================================================================
/**
 * @brief returns local port of the server
 *
 * The function returns local TCP port, that the server listen to.
 *
 * @param Server            TCP Server object created by the function #oC_Tcp_Server_New
 *
 * @return local TCP port
 */
//==========================================================================================================================================
oC_Tcp_Port_t oC_Tcp_Server_GetPort( oC_Tcp_Server_t Server )
{
    oC_Tcp_Port_t port = 0;

    if(oC_SaveIfFalse("TCP::Server::GetPort ", oC_Tcp_Server_IsCorrect(Server), oC_ErrorCode_ObjectNotCorrect))
    {
        port = Server->ConnectionConfig.LocalAddress.Port;
    }

    return port;
}

//==========================================================================================================================================
/**
 * @brief sets a pointer for 'connection finished function'
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_SetConnectionFinished( oC_Tcp_Server_t Server , oC_Tcp_ConnectionFunction_t Function , void * Parameter )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)   , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( isaddresscorrect(Function)        , oC_ErrorCode_WrongAddress       )
        )
    {
        Server->ConnectionFinishedFunction  = Function;
        Server->ConnectionFinishedParameter = Parameter;
        errorCode                           = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 *
 * @param Server
 * @param Connection
 * @param Timeout
 * @return
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_AddToKnown( oC_Tcp_Server_t Server, oC_Tcp_Connection_t Connection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)             , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection)     , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        oC_IntMan_EnterCriticalSection();
        bool found = false;
        foreach(Server->KnownConnections, conn)
        {
            if(oC_Tcp_Connection_AreTheSame(Connection, conn))
            {
                found = true;
                break;
            }
        }
        bool pushed = false;
        if(!found)
        {
            pushed = oC_List_PushBack(Server->KnownConnections, Connection, getcurallocator());
        }
        if(ErrorCondition( found || pushed, oC_ErrorCode_CannotAddObjectToList ))
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief removes connection from known connections list
 * 
 * @param Server        TCP Server object created by the function #oC_Tcp_Server_New
 * @param Connection    TCP Connection object received from the function #oC_Tcp_Server_WaitForConnection
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Tcp_Server_RemoveFromKnown( oC_Tcp_Server_t Server, oC_Tcp_Connection_t Connection )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Tcp_Server_IsCorrect(Server)         , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection) , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        oC_IntMan_EnterCriticalSection();
        bool found = false;
        foreach(Server->KnownConnections, conn)
        {
            if(oC_Tcp_Connection_AreTheSame(Connection, conn))
            {
                found = true;
                break;
            }
        }
        bool removed = false;
        if(found)
        {
            removed = oC_List_RemoveAll(Server->KnownConnections, Connection);
        }
        if(ErrorCondition( !found || removed, oC_ErrorCode_CannotRemoveObjectFromList ))
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the connection is known by the server
 * 
 * @param Server        TCP Server object created by the function #oC_Tcp_Server_New
 * @param Connection    TCP Connection object received from the function #oC_Tcp_Server_WaitForConnection
 * 
 * @return true if the connection is known by the server
 */
//==========================================================================================================================================
bool oC_Tcp_Server_IsKnown( oC_Tcp_Server_t Server, oC_Tcp_Connection_t Connection )
{
    bool known = false;

    if(
        oC_SaveIfFalse("TCP::Server::IsKnown - Server "      , oC_Tcp_Server_IsCorrect(Server)           , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse("TCP::Server::IsKnown - Connection "  , oC_Tcp_Connection_IsCorrect(Connection)   , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        oC_IntMan_EnterCriticalSection();
        foreach(Server->KnownConnections, conn)
        {
            if(oC_Tcp_Connection_AreTheSame(Connection, conn))
            {
                known = true;
                break;
            }
        }
        oC_IntMan_ExitCriticalSection();
    }

    return known;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief function called, when the connection has been finished
 */
//==========================================================================================================================================
static bool ConnectionFinished( oC_Tcp_Connection_t Connection , oC_Tcp_Server_t Server, void* EventParameter )
{
    if(
        oC_SaveIfFalse("TCP::Server::ConnectionFinished (Connection)", oC_Tcp_Connection_IsCorrect(Connection), oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse("TCP::Server::ConnectionFinished (Server)"    , oC_Tcp_Server_IsCorrect(Server)        , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        if(isaddresscorrect(Server->ConnectionFinishedFunction))
        {
            Server->ConnectionFinishedFunction(Connection,Server->ConnectionFinishedParameter, EventParameter);
        }
        oC_SaveIfErrorOccur("TCP::Server::ConnectionFinished ", oC_Tcp_Server_RemoveConnection(Server,Connection)                       );
        oC_SaveIfFalse     ("TCP::Server::ConnectionFinished ", oC_Tcp_Connection_Delete(&Connection, s(1)), oC_ErrorCode_ReleaseError  );
    }
    return true;
}

//==========================================================================================================================================
/**
 * @brief checks if the connection is known
 */
//==========================================================================================================================================
static bool IsConnectionKnown( oC_Tcp_Connection_t Connection, oC_Tcp_Server_t Server, void* EventParameter )
{
    oC_Net_Address_t* remoteAddress = (oC_Net_Address_t*)EventParameter;
    if(remoteAddress)
    {
        for(uint32_t i = 0; i < Server->MaxConnections; i++)
        {
            oC_Net_Address_t connectionAddress;
            memset(&connectionAddress, 0, sizeof(connectionAddress));
            if(oC_Tcp_Connection_ReadRemote(Server->Connections[i].Connection, &connectionAddress) == oC_ErrorCode_None)
            {
                if(Connection != Server->Connections[i].Connection)
                {

                    bool known = oC_Net_AreAddressesTheSame(&connectionAddress, remoteAddress)
                              && connectionAddress.Port == remoteAddress->Port;
                    if(known)
                    {
                        return true;
                    }
                }
            }
        }
    }

    return false;
}


//==========================================================================================================================================
/**
 * @brief main thread of TCP servers
 */
//==========================================================================================================================================
static void ServerThread( oC_Tcp_Server_t Server )
{
    oC_Tcp_Connection_t connection      = NULL;
    uint32_t            connectionId    = 0;

    while(oC_Tcp_Server_IsCorrect(Server))
    {
        Server->ConnectionConfig.InitialSequenceNumber = connectionId += 0xFFFF;
        connectionId = connectionId << 1;

        if(connection == NULL)
        {
            connection = oC_Tcp_Connection_New(&Server->ConnectionConfig);
        }
        oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

        if(oC_SaveIfFalse("TCP::Server::ServerThread ", connection != NULL, oC_ErrorCode_AllocationError))
        {
            if(
                ErrorCode     ( oC_Tcp_Connection_WaitForConnection(connection, oC_DynamicConfig_GetValue( Tcp, WaitForConnectionTimeout    )))
             && ErrorCode     ( oC_Tcp_Server_AddConnection(Server, connection, oC_DynamicConfig_GetValue( Tcp, WaitForFreeSlotTimeout      )))
                )
            {
                oC_Net_Address_t address;
                char             addressString[30]    = {0};

                oC_Tcp_Connection_ReadRemote(connection,&address);
                oC_Net_AddressToString(&address,addressString,sizeof(addressString));

                tcplog(oC_LogType_Info, "TCP::Server:%d - received new connection from %s. Added: %p\n", Server->ConnectionConfig.LocalAddress.Port, addressString, connection);
            }
            else
            {
                tcplog(oC_LogType_Warning, "Cannot accept connection: %R\n", errorCode);
                if(oC_Tcp_Connection_IsCorrect(connection) && errorCode != oC_ErrorCode_FoundOnList)
                {
                    tcplog(oC_LogType_Warning, "Rejecting connection %s\n", oC_Tcp_Connection_GetName(connection));
                    if(ErrorCode(oC_Tcp_Connection_Reject(connection, oC_DynamicConfig_GetValue( Tcp, RejectConnectionTimeout ))))
                    {
                        oC_Tcp_Connection_Delete(&connection, oC_DynamicConfig_GetValue(Tcp,DeleteConnectionTimeout));
                    }
                    else
                    {
                        tcplog(oC_LogType_Error, "Cannot reject connection: %R\n", errorCode);
                    }
                }
            }
            connection = NULL;
        }
        else
        {
            break;
        }
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

