/** ****************************************************************************************************************************************
 *
 * @brief      ChocoOS
 *
 * @file       oc_udp.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_udp.h>
#include <oc_intman.h>
#include <oc_list.h>
#include <oc_event.h>
#include <oc_mutex.h>
#include <oc_module.h>
#include <oc_processman.h>
#include <oc_memman.h>
#include <oc_netifman.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_Udp_Port_t       Port;
    oC_Process_t        Process;
    oC_Mutex_t          Busy;
} ReservationData_t;

typedef struct
{
    oC_Udp_Packet_t*    Packet;
    oC_Netif_t          Netif;
} SavedPacket_t;

typedef enum
{
    PortPurpose_Source ,
    PortPurpose_Destination
} PortPurpose_t;

typedef struct
{
    oC_Udp_Port_t       DestinationPort;
    oC_Netif_t          ExpectedNetif;
} PacketFilterData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool                         PacketFilterFunction              ( oC_Net_Packet_t * Packet , const void * Parameter , oC_Netif_t Netif );
static ReservationData_t*           FindReservation                   ( oC_Udp_Port_t Port , oC_Process_t Process );
static uint16_t                     CalculateChecksum                 ( oC_Udp_Packet_t * Packet );
static bool                         IsSpecialPort                     ( oC_Udp_Port_t Port );
static bool                         IsPortReserved                    ( oC_Udp_Port_t Port );
static void                         ConvertHeaderToNetworkEndianess   ( oC_Udp_Header_t * Header );
static void                         ConvertHeaderFromNetworkEndianess ( oC_Udp_Header_t * Header );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_List(ReservationData_t*)  ReservedPorts   = NULL;                 //!< List of already used ports
static oC_Event_t                   PortReleased    = NULL;                 //!< Event that is active, when some port is released
static oC_Mutex_t                   ModuleBusy      = NULL;                 //!< Mutex taken, when the module is busy
static const oC_Allocator_t         Allocator       = { .Name = "UDP" };    //!< Allocator for the module

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION_________________________________________________________________________
/**
 * @addtogroup Udp
 * @{
 */

//==========================================================================================================================================
/**
 * @brief turns on UDP module
 *
 * The function is for turning on UDP module. The module has to be turned on before usage.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleIsTurnedOn                | UDP module is already turned on.
 *  oC_ErrorCode_AllocationError                 | Cannot allocate memory
 *
 *  @see oC_Udp_TurnOff
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();
    if(
        oC_Module_TurnOffVerification(&errorCode, oC_Module_Udp)
        )
    {
        ReservedPorts   = oC_List_New(&Allocator, AllocationFlags_Default);
        PortReleased    = oC_Event_New( oC_Event_State_Inactive, &Allocator, AllocationFlags_Default );
        ModuleBusy      = oC_Mutex_New( oC_Mutex_Type_Normal   , &Allocator, AllocationFlags_Default);

        if(
            ErrorCondition( ReservedPorts   != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( ModuleBusy      != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( PortReleased    != NULL , oC_ErrorCode_AllocationError )
            )
        {
            oC_Module_TurnOn(oC_Module_Udp);
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            bool listDeleted        = oC_List_Delete( ReservedPorts     , AllocationFlags_Default);
            bool mutexDeleted       = oC_Mutex_Delete(&ModuleBusy       , AllocationFlags_Default);
            bool eventDeleted       = oC_Event_Delete(&PortReleased     , AllocationFlags_Default);

            oC_SaveIfFalse("UDP:TurnOn - Cannot delete list "         , listDeleted         , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("UDP:TurnOn - Cannot delete mutex "        , mutexDeleted        , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("UDP:TurnOn - Cannot delete event "        , eventDeleted        , oC_ErrorCode_ReleaseError );
        }
    }
    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off UDP module
 *
 * The function is for turning off UDP module. The module has to be turned off before usage and turned off when is not needed.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_ReleaseError                    | Cannot release memory
 *
 *  @see oC_Udp_TurnOn
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();
    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_Udp)
        )
    {
        oC_Module_TurnOff(oC_Module_Udp);

        bool allPacketsReleased = true;

        bool listDeleted        = oC_List_Delete ( ReservedPorts   , AllocationFlags_Default );
        bool eventDeleted       = oC_Event_Delete( &PortReleased   , AllocationFlags_Default );
        bool mutexDeleted       = oC_Mutex_Delete( &ModuleBusy     , AllocationFlags_Default );

        if(
            ErrorCondition( listDeleted                                    , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( allPacketsReleased                             , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( eventDeleted                                   , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( mutexDeleted                                   , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( oC_MemMan_FreeAllMemoryOfAllocator(&Allocator) , oC_ErrorCode_ReleaseError  )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }
    oC_IntMan_ExitCriticalSection();

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief checks if the given UDP port is reserved
 *
 * The function checks if the given port is reserved. If `Process` parameter is not #NULL, the function will check if the given port
 * is reserved by this process.
 *
 * @param Port          Port to check
 * @param Process       Process which check if it is an owner of the port
 *
 * @return true if the port is reserved
 */
//==========================================================================================================================================
bool oC_Udp_IsPortReserved( oC_Udp_Port_t Port , oC_Process_t Process )
{
    bool reserved = false;

    if(
        oC_Module_IsTurnedOn(oC_Module_Udp)
     && oC_SaveIfFalse("UDP::IsPortReserved - ", Port != oC_Udp_Port_Empty                          , oC_ErrorCode_PortNotCorrect    )
     && oC_SaveIfFalse("UDP::IsPortReserved - ", Process == NULL || oC_Process_IsCorrect(Process)   , oC_ErrorCode_ProcessNotCorrect )
     )
    {
        reserved = IsPortReserved(Port);
    }

    return reserved;
}

//==========================================================================================================================================
/**
 * @brief reserves a UDP port
 *
 * Only one application can use a port at once, otherwise it can cause loosing of datagrams. This function allows to reserve a port before
 * usage. Reservation is performed for a current process and only this process will be able to use the reserved port.
 *
 * @warning
 * Only root user can use special ports
 *
 * @param Port          Address to the reserved port on output. On input it is port to reserve. It can be #oC_Udp_Port_Empty if you want to find first available port
 * @param Timeout       Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_OutputAddressNotInRAM           | `Port` does not point to RAM section
 *  oC_ErrorCode_PortNotAvailable                | The are no free ports or the selected one is already reserved
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Port is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_ModuleBusy                      | Module is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_PermissionDenied                | You are not allowed to use the selected UDP port (only root can use special ports)
 *
 *  @see oC_Udp_Receive, oC_Udp_Send, oC_Udp_TurnOn, oC_Udp_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_ReservePort( oC_Udp_Port_t * Port , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_Udp)
     && ErrorCondition( isram(Port)                                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( Timeout >= 0                                                            , oC_ErrorCode_TimeNotCorrect               )
     && ErrorCondition( *Port == 0 || IsSpecialPort(*Port) == false || iscurroot()              , oC_ErrorCode_PermissionDenied             )
        )
    {
        oC_Timestamp_t  endTimestamp    = oC_KTime_GetTimestamp() + Timeout;
        bool            freePortFound   = false;

        errorCode = oC_ErrorCode_PortNotAvailable;

        while(
               freePortFound == false
            && errorCode     == oC_ErrorCode_PortNotAvailable
            && ErrorCondition( oC_KTime_GetTimestamp() <= endTimestamp, oC_ErrorCode_PortNotAvailable )
            )
        {
            Timeout = endTimestamp - oC_KTime_GetTimestamp();

            if( ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout) , oC_ErrorCode_ModuleBusy  ) )
            {
                ReservationData_t * reservation     = NULL;
                oC_Udp_Port_t       port            = *Port;

                if(port == oC_Udp_Port_Empty)
                {
                    /* As we are looking for free port - we cannot use ports that are reserved as specials */
                    for(port = oC_Udp_Port_NumberOfSpecialPorts; port > 0 && freePortFound == false; port++)
                    {
                        reservation     = FindReservation(port,NULL);
                        freePortFound   = reservation == NULL;
                    }
                }
                else
                {
                    reservation     = FindReservation(port,NULL);
                    freePortFound   = reservation == NULL;
                }

                if(ErrorCondition( freePortFound , oC_ErrorCode_PortNotAvailable ))
                {
                    if(ErrorCondition( port != oC_Udp_Port_Empty   , oC_ErrorCode_InternalDataAreDamaged   ))
                    {
                        reservation = kmalloc( sizeof(ReservationData_t), &Allocator, AllocationFlags_Default );

                        if(ErrorCondition( reservation != NULL , oC_ErrorCode_AllocationError ))
                        {
                            reservation->Busy = oC_Mutex_New( oC_Mutex_Type_Normal , &Allocator, AllocationFlags_Default );

                            if( ErrorCondition( reservation->Busy != NULL , oC_ErrorCode_AllocationError ) )
                            {
                                reservation->Port       = port;
                                reservation->Process    = getcurprocess();

                                bool addedToList = oC_List_PushBack(ReservedPorts,reservation,&Allocator);

                                if( ErrorCondition( addedToList, oC_ErrorCode_CannotAddObjectToList ) )
                                {
                                    errorCode = oC_ErrorCode_None;
                                }
                                else
                                {
                                    oC_SaveIfFalse("UDP:Reserve - cannot release reservation memory ", kfree(reservation, AllocationFlags_Default), oC_ErrorCode_ReleaseError);
                                }
                            }
                            else
                            {
                                oC_SaveIfFalse("UDP:Reserve - cannot release reservation memory ", kfree(reservation, AllocationFlags_Default), oC_ErrorCode_ReleaseError);
                            }

                        }
                    }
                }
                else
                {
                    oC_Event_ClearStateBits( PortReleased, oC_Event_StateMask_Full );

                    Timeout = endTimestamp - oC_KTime_GetTimestamp();

                    if(*Port == oC_Udp_Port_Empty)
                    {
                        oC_Event_WaitForState( PortReleased, oC_Udp_Port_Empty, oC_Event_StateMask_DifferentThan, Timeout );
                    }
                    else
                    {
                        oC_Event_WaitForState( PortReleased, port, oC_Event_StateMask_Full, Timeout );
                    }
                }

                oC_Mutex_Give( ModuleBusy );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases a port
 *
 * Only one application can use a port at once, otherwise it can cause loosing of datagrams. This function allows to release reserved port
 * when it is not needed anymore. Reservation is performed for a current process and only this process will be able to use the reserved port.
 * Usually all reserved ports are automatically released, when the process is killed.
 *
 * @param Port              Local port reserved by calling a function #oC_Udp_ReservePort
 * @param Timeout           Maximum time to wait for the module readiness
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_PortNotCorrect                  | `Port` is not correct UDP port
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Module is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_PortNotReserved                 | The given `LocalPort` is not reserved by using the function #oC_Udp_ReservePort
 *  oC_ErrorCode_PortReservedByDifferentProcess  | The given `LocalPort` is reserved by different process.
 *  oC_ErrorCode_PortBusy                        | The given `LocalPort` is currently in use.
 *  oC_ErrorCode_CannotDeleteObject              | Cannot delete Mutex related with the local port.
 *
 *  @see oC_Udp_Receive, oC_Udp_Send, oC_Udp_TurnOn, oC_Udp_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_ReleasePort( oC_Udp_Port_t Port , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        oC_Module_TurnOnVerification( &errorCode , oC_Module_Udp )
     && ErrorCondition( Port != oC_Udp_Port_Empty           , oC_ErrorCode_PortNotCorrect       )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect       )
     && ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout)   , oC_ErrorCode_ModuleBusy           )
        )
    {
        ReservationData_t * reservation = FindReservation(Port,NULL);

        if(
            ErrorCondition( reservation != NULL                                                         , oC_ErrorCode_PortNotReserved                  )
         && ErrorCondition( reservation->Process == getcurprocess()                                     , oC_ErrorCode_PortReservedByDifferentProcess   )
         && ErrorCondition( oC_Mutex_Take(reservation->Busy,oC_KTime_CalculateTimeout(endTimestamp))    , oC_ErrorCode_PortBusy                         )
            )
        {
            bool mutexDeleted      = oC_Mutex_Delete(&reservation->Busy, AllocationFlags_Default);
            bool deletedFromList   = oC_List_RemoveAll(ReservedPorts,reservation);
            bool deleted           = kfree(reservation,AllocationFlags_Default);

            if(
                ErrorCondition( mutexDeleted                , oC_ErrorCode_CannotDeleteObject           )
             && ErrorCondition( deletedFromList             , oC_ErrorCode_CannotRemoveObjectFromList   )
             && ErrorCondition( deleted                     , oC_ErrorCode_ReleaseError                 )
                )
            {
                oC_Event_SetState(PortReleased,Port);
                errorCode = oC_ErrorCode_None;
            }
        }

        oC_Mutex_Give(ModuleBusy);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases all ports reserved by the process
 *
 * Only one application can use a port at once, otherwise it can cause loosing of datagrams. This function allows to release all reserved
 * port that are owned by the given process when they are not needed anymore.
 *
 * @note
 * This function can be called only by the root user. It is not enough to have admin privileges - you have to be a root.
 *
 * @param Process           Process that reserved some UDP ports
 * @param Timeout           Maximum time to wait for the module readiness
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_ProcessNotCorrect               | `Process` is not correct or not valid anymore
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Module is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_PossibleOnlyForRoot             | Function can be called only by the root user. You are not permitted to perform this
 *
 *  @see oC_Udp_Receive, oC_Udp_Send, oC_Udp_TurnOn, oC_Udp_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_ReleaseAllPortsReservedBy( oC_Process_t Process , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        oC_Module_TurnOnVerification( &errorCode, oC_Module_Udp )
     && ErrorCondition( getcuruser() == getrootuser()       , oC_ErrorCode_PossibleOnlyForRoot  )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect       )
     && ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout)   , oC_ErrorCode_ModuleBusy           )
        )
    {
        ReservationData_t * reservation = FindReservation(oC_Udp_Port_Empty, Process);

        errorCode = oC_ErrorCode_None;

        while(
                reservation != NULL
             && ErrorCondition( oC_KTime_GetTimestamp() <= endTimestamp , oC_ErrorCode_Timeout )
                )
        {
            reservation->Process = getcurprocess();

            Timeout = endTimestamp - oC_KTime_GetTimestamp();

            ErrorCode( oC_Udp_ReleasePort( reservation->Port, Timeout ) );

            reservation = FindReservation( oC_Udp_Port_Empty, Process );
        }

        oC_Mutex_Give(ModuleBusy);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns maximum size of the data for UDP packet
 */
//==========================================================================================================================================
uint16_t oC_Udp_Packet_GetMaximumDataSize( oC_Net_PacketType_t Type )
{
    return oC_Net_GetMaximumPacketDataSize(Type) - sizeof(oC_Udp_Header_t);
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a UDP packet
 *
 * The function is for allocating memory for the UDP packet. It calculates memory required for the packet including all headers of sub-layers.
 * Memory should be released, when is not needed anymore by using the function #oC_Udp_Packet_Delete
 *
 * @note
 * In case of failure, error can be read from the errors stack
 *
 * @param PacketAllocator       Allocator for the Memory Manager
 * @param Type                  Type of the packet (IPv4 or IPv6)
 * @param Data                  Address of the data to put inside the UDP packet. It can be NULL if you dont want to fill it now
 * @param Size                  Size of the data inside UDP packet
 *
 * @return pointer to the allocated memory
 *
 * @see oC_Udp_Packet_Delete
 */
//==========================================================================================================================================
oC_Udp_Packet_t* oC_Udp_Packet_New( Allocator_t PacketAllocator , oC_Net_PacketType_t Type , const void * Data , uint16_t Size )
{
    oC_Udp_Packet_t * packet        = NULL;
    uint16_t          packetSize    = Size + sizeof(oC_Udp_Header_t);

    if(
        oC_SaveIfFalse("Udp::PacketNew: "                           , oC_Module_IsTurnedOn(oC_Module_Udp)               , oC_ErrorCode_ModuleNotStartedYet  )
     && oC_SaveIfFalse("Udp::PacketNew: Packet Type incorrect - "   ,    Type == oC_Net_PacketType_IPv4
                                                                      || Type == oC_Net_PacketType_IPv6                 , oC_ErrorCode_TypeNotCorrect       )
     && oC_SaveIfFalse("Udp::PacketNew: Size is not correct - "     , Size > 0                                          , oC_ErrorCode_SizeNotCorrect       )
     && oC_SaveIfFalse("Udp::PacketNew: Size is not correct - "     , Size <= oC_Udp_Packet_GetMaximumDataSize(Type)    , oC_ErrorCode_SizeTooBig           )
     && oC_SaveIfFalse("Udp::PacketNew: Data address incorrect - "  , Data == NULL || isaddresscorrect(Data)            , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("Udp::PacketNew: Allocator incorrect - "     , isaddresscorrect(PacketAllocator)                 , oC_ErrorCode_WrongAddress         )
        )
    {
        packet = (oC_Udp_Packet_t*) oC_Net_Packet_New( PacketAllocator, Type, NULL, packetSize );

        if(oC_SaveIfFalse("Udp::PacketNew: Cannot allocate memory for a packet - ", packet != NULL , oC_ErrorCode_AllocationError))
        {
            oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&packet->Packet);

            datagram->Header.Length = Size + sizeof(oC_Udp_Header_t);

            if(Data != NULL)
            {
                void * dataDestination = datagram;

                dataDestination += sizeof(oC_Udp_Header_t);

                memcpy(dataDestination,Data,Size);
            }
        }
    }

    return packet;
}

//==========================================================================================================================================
/**
 * @brief release memory allocated for a UDP packet
 */
//==========================================================================================================================================
bool oC_Udp_Packet_Delete( oC_Udp_Packet_t ** Packet )
{
    bool success = false;

    if(isram(Packet) && isram(*Packet))
    {
        success = kfree(*Packet,AllocationFlags_Default);
        *Packet = NULL;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief returns size of the data inside UDP packet
 */
//==========================================================================================================================================
uint16_t oC_Udp_Packet_GetDataSize( const oC_Udp_Packet_t * Packet )
{
    uint16_t size = 0;

    if(isaddresscorrect(Packet))
    {
        oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference((void*)&Packet->Packet);

        if(isaddresscorrect(datagram) && datagram->Header.Length >= sizeof(oC_Udp_Header_t) )
        {
            size = datagram->Header.Length - sizeof(oC_Udp_Header_t);
        }
    }

    return size;
}

//==========================================================================================================================================
/**
 * @brief sets size of the data inside UDP packet
 * 
 * The function sets size of the data inside UDP packet. It is useful when you want to change the size of the data inside the packet.
 * 
 * @param Packet        UDP packet
 * @param Size          New size of the data inside the packet
 * 
 * @return true if success
 * 
 * @warning 
 * The function does not check if the new size is not bigger than the maximum size of the data inside the packet. It is your responsibility
 * to check it before calling this function. The #Size should not be bigger than the maximum given in the function #oC_Udp_Packet_New
 */
//==========================================================================================================================================
bool oC_Udp_Packet_SetDataSize( oC_Udp_Packet_t * Packet , uint16_t Size )
{
    bool success = false;

    if(isram(Packet))
    {
        oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if(isram(datagram))
        {
            datagram->Header.Length = Size + sizeof(oC_Udp_Header_t);
            success = oC_Net_Packet_SetSize(&Packet->Packet, datagram->Header.Length);
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief sets data inside UDP packet
 */
//==========================================================================================================================================
bool oC_Udp_Packet_SetData( oC_Udp_Packet_t * Packet , const void * Data , uint16_t Size )
{
    bool success = false;

    if(isram(Packet) && isaddresscorrect(Data) && Size > 0 && Size <= oC_Udp_Packet_GetDataSize( Packet ) )
    {
        oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if(isram(datagram))
        {
            memcpy(datagram->Data,Data,Size);
            success = true;
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads data of the UDP packet
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_Packet_ReadData( const oC_Udp_Packet_t * Packet , void * outData , uint16_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Packet)                    , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outData)                              , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Size >= oC_Udp_Packet_GetDataSize( Packet ) , oC_ErrorCode_OutputBufferTooSmall     )
        )
    {
        oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference((void*)&Packet->Packet);

        if( ErrorCondition( isram(datagram) , oC_ErrorCode_PacketNotCorrect) )
        {
            memcpy( outData, datagram->Data, Size );
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the data inside UDP packet
 */
//==========================================================================================================================================
void * oC_Udp_Packet_GetDataReference( oC_Udp_Packet_t * Packet )
{
    void *              data     = NULL;
    oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

    if(isaddresscorrect(datagram))
    {
        data = datagram->Data;
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief sends UDP packet
 *
 * The function prepares UDP packet and sends it to the recipient. Remember that you have to reserve a local port by using function #oC_Udp_ReservePort
 * before usage.
 *
 * @param Netif             Pointer to the configured network interface or NULL if not used
 * @param LocalPort         Local port of the UDP for receiving (the port on which the recipient should respond). It should be reserved by the function #oC_Udp_ReservePort before usage
 * @param Destination       Destination address where the packet should be sent (with the Port filled as destination port)
 * @param Packet            UDP packet to send allocated by the function #oC_Udp_Packet_New
 * @param Timeout           Maximum time for the send request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_NetifNotCorrect                 | `Netif` is not correct (address is not correct or not valid anymore) and it is not NULL
 *  oC_ErrorCode_PortNotCorrect                  | `Destination->Port` or `LocalPort` is not correct
 *  oC_ErrorCode_OutputAddressNotInRAM           | `Packet` does not point to the RAM section
 *  oC_ErrorCode_WrongAddress                    | `Destination` is not correct memory address
 *  oC_ErrorCode_IpAddressNotCorrect             | `Destination` stores IP address that is not correct (probably type is not set)
 *  oC_ErrorCode_SizeNotCorrect                  | Size of the UDP packet is set to 0
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *  oC_ErrorCode_PortNotReserved                 | The given `LocalPort` is not reserved by using the function #oC_Udp_ReservePort
 *  oC_ErrorCode_PortReservedByDifferentProcess  | The given `LocalPort` is already reserved by different process. Please use another one
 *  oC_ErrorCode_SizeTooBig                      | Size of the UDP packet is set to too big value (it exceed maximum size of the packet)
 *  oC_ErrorCode_PacketNotCorrect                | UDP packet is not correct (probably IP type is not set correctly)
 *  oC_ErrorCode_TypeNotCorrect                  | UDP packet type and destination address type are not the same
 *  oC_ErrorCode_CannotFindNetifForTheAddress    | Netif is not given (#NULL) but we cannot find any that can achieve the given address
 *  oC_ErrorCode_ModuleBusy                      | Module is busy (Maximum time for the request elapsed)
 *  oC_ErrorCode_PortBusy                        | The given local port is busy (Maximum time for the request elapsed)
 *
 *  @see oC_Udp_Receive, oC_Udp_TurnOn, oC_Udp_ReservePort, oC_Udp_Packet_New
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_Send( oC_Netif_t Netif , oC_Udp_Port_t LocalPort , const oC_Net_Address_t * Destination, oC_Udp_Packet_t * Packet , oC_Time_t Timeout )
{
    oC_ErrorCode_t       errorCode    = oC_ErrorCode_ImplementError;
    ReservationData_t*   reservation  = FindReservation(LocalPort,NULL);
    uint16_t             packetSize   = oC_Udp_Packet_GetDataSize(Packet);
    oC_Net_PacketType_t  packetType   = oC_Net_Packet_GetType((oC_Net_Packet_t*)Packet);
    oC_Net_AddressType_t addressType  = isaddresscorrect(Destination) ? Destination->Type : 0;
    oC_Timestamp_t       timestamp    = oC_KTime_GetTimestamp();
    oC_Timestamp_t       endTimestamp = timestamp + Timeout;

    if(
        oC_Module_TurnOnVerification( &errorCode , oC_Module_Udp )
     && ErrorCondition( Netif == NULL || oC_Netif_IsCorrect(Netif)                          , oC_ErrorCode_NetifNotCorrect                  )
     && ErrorCondition( LocalPort > 0                                                       , oC_ErrorCode_PortNotCorrect                   )
     && ErrorCondition( isram(Packet)                                                       , oC_ErrorCode_OutputAddressNotInRAM            )
     && ErrorCondition( isaddresscorrect(Destination)                                       , oC_ErrorCode_WrongAddress                     )
     && ErrorCondition( oC_Net_IsAddressCorrect(Destination)                                , oC_ErrorCode_IpAddressNotCorrect              )
     && ErrorCondition( Destination->Port > 0                                               , oC_ErrorCode_PortNotCorrect                   )
     && ErrorCondition( ((oC_Net_PacketType_t)addressType) == packetType                    , oC_ErrorCode_TypeNotCorrect                   )
     && ErrorCondition( packetSize > 0                                                      , oC_ErrorCode_SizeNotCorrect                   )
     && ErrorCondition( Timeout > 0                                                         , oC_ErrorCode_TimeNotCorrect                   )
     && ErrorCondition( reservation != NULL                                                 , oC_ErrorCode_PortNotReserved                  )
     && ErrorCondition( reservation->Process == getcurprocess()                             , oC_ErrorCode_PortReservedByDifferentProcess   )
     && ErrorCondition( packetSize <= oC_Udp_Packet_GetMaximumDataSize(Destination->Type)   , oC_ErrorCode_SizeTooBig                       )
     && ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout)                                   , oC_ErrorCode_ModuleBusy                       )
        )
    {
        oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

        Netif = Netif == NULL ? oC_NetifMan_GetNetifForAddress(Destination) : Netif;

        oC_Mutex_Give(ModuleBusy);

        if(
            ErrorCondition( oC_Netif_IsCorrect(Netif)                                                   , oC_ErrorCode_CannotFindNetifForTheAddress  )
         && ErrorCondition( oC_Mutex_Take(reservation->Busy, oC_KTime_CalculateTimeout(endTimestamp))   , oC_ErrorCode_PortBusy                      )
         && ErrorCondition( isram(datagram)                                                             , oC_ErrorCode_PacketNotCorrect              )
         && ErrorCondition( packetType == oC_Net_PacketType_IPv4                                        , oC_ErrorCode_NotImplemented                )
            )
        {
            if(packetType == oC_Net_PacketType_IPv4)
            {
                Packet->Packet.IPv4.Header.DF               = 0;
                Packet->Packet.IPv4.Header.DestinationIp    = Destination->IPv4;
                Packet->Packet.IPv4.Header.SourceIp         = oC_Netif_GetIpv4Address(Netif);
                Packet->Packet.IPv4.Header.FragmentOffset   = 0;
                Packet->Packet.IPv4.Header.ID               = 0;
                Packet->Packet.IPv4.Header.IHL              = 5;
                Packet->Packet.IPv4.Header.MF               = 0;
                Packet->Packet.IPv4.Header.Protocol         = oC_Net_Protocol_UDP;
                Packet->Packet.IPv4.Header.QoS              = 0;
                Packet->Packet.IPv4.Header.TTL              = 0xFF;
                Packet->Packet.IPv4.Header.Checksum         = 0;

            }
            else
            {
                memcpy(&Packet->Packet.IPv6.Header.Destination,&Destination->IPv6,sizeof(oC_Net_Ipv6_t));
                oC_Netif_ReadIpv6Address(Netif,&Packet->Packet.IPv6.Header.Source);

                oC_SaveError("UDP::Send - Function is not implemented for IPv6!", oC_ErrorCode_NotImplemented);
            }

            datagram->Header.DestinationPort = Destination->Port;
            datagram->Header.SourcePort      = LocalPort;
            datagram->Header.Checksum        = CalculateChecksum(Packet);

            ConvertHeaderToNetworkEndianess(&datagram->Header);

            errorCode = oC_Netif_SendPacket( Netif, &Packet->Packet, oC_KTime_CalculateTimeout(endTimestamp) );

            ConvertHeaderFromNetworkEndianess(&datagram->Header);
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives UDP packet
 *
 * The function is for receiving UDP packets at the selected UDP port. The port has to be reserved for the current process, otherwise
 * receiving is not possible.
 *
 * @note
 * Remember that you have to reserve a local port by using function #oC_Udp_ReservePort before usage.
 *
 * @warning
 * Function will receive only packets that matches `Packet` type, so if the type of the `Packet` is set to IPv4, the function will receive
 * only packets that contains IPv4 address!
 *
 * @param Netif             Network interface to receive packet from or #NULL if not used
 * @param LocalPort         Local UDP port to which at receive a packet
 * @param Packet            Destination for a received packet. It should be allocated by the function #oC_Udp_Packet_New
 * @param Timeout           Maximum time for the send request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_NetifNotCorrect                 | `Netif` is not correct (address is not correct or not valid anymore) and it is not NULL
 *  oC_ErrorCode_PortNotCorrect                  | `LocalPort` is not correct
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_OutputAddressNotInRAM           | `Packet` does not point to the RAM section
 *  oC_ErrorCode_PortNotReserved                 | The given `LocalPort` is not reserved by using the function #oC_Udp_ReservePort
 *  oC_ErrorCode_PortReservedByDifferentProcess  | The given `LocalPort` is already reserved by different process. Please use another one
 *  oC_ErrorCode_SizeNotCorrect                  | Size of the UDP packet is set to 0
 *  oC_ErrorCode_PortBusy                        | The given local port is busy (Maximum time for the request elapsed)
 *  oC_ErrorCode_ModuleBusy                      | Module is busy (Maximum time for the request elapsed)
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *  oC_ErrorCode_OutputBufferTooSmall            | `Packet` buffer is too small to receive a packet
 *
 *  @see oC_Udp_Receive, oC_Udp_TurnOn, oC_Udp_ReservePort, oC_Udp_Packet_New
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_Receive( oC_Netif_t Netif , oC_Udp_Port_t LocalPort , oC_Udp_Packet_t * Packet, oC_Time_t Timeout )
{
    oC_ErrorCode_t      errorCode    = oC_ErrorCode_ImplementError;
    ReservationData_t*  reservation  = FindReservation(LocalPort,NULL);
    uint16_t            packetSize   = oC_Udp_Packet_GetDataSize(Packet);
    oC_Timestamp_t      endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        oC_Module_TurnOnVerification( &errorCode, oC_Module_Udp )
     && ErrorCondition( Netif == NULL || oC_Netif_IsCorrect(Netif)                                  , oC_ErrorCode_NetifNotCorrect                  )
     && ErrorCondition( LocalPort > 0                                                               , oC_ErrorCode_PortNotCorrect                   )
     && ErrorCondition( Timeout > 0                                                                 , oC_ErrorCode_TimeNotCorrect                   )
     && ErrorCondition( isram(Packet)                                                               , oC_ErrorCode_OutputAddressNotInRAM            )
     && ErrorCondition( reservation != NULL                                                         , oC_ErrorCode_PortNotReserved                  )
     && ErrorCondition( reservation->Process == getcurprocess()                                     , oC_ErrorCode_PortReservedByDifferentProcess   )
     && ErrorCondition( packetSize > 0                                                              , oC_ErrorCode_SizeNotCorrect                   )
     && ErrorCondition( oC_Mutex_Take(reservation->Busy,oC_KTime_CalculateTimeout(endTimestamp))    , oC_ErrorCode_PortBusy                         )
     && ErrorCondition( oC_Mutex_Take(ModuleBusy,oC_KTime_CalculateTimeout(endTimestamp))           , oC_ErrorCode_ModuleBusy                       )
        )
    {
        oC_Net_Address_t addressFilter = {
                        .Type           = oC_Net_Packet_GetType(&Packet->Packet) ,
                        .Protocol       = oC_Net_Protocol_UDP,
                        .Port           = 0 ,
                        .IPv4           = 0 ,
                        .IPv6.LowPart   = 0 ,
                        .IPv6.HighPart  = 0 ,
        };
        oC_Udp_Datagram_t * datagram          = NULL;
        oC_Netif_t          receivedFromNetif = NULL;
        PacketFilterData_t  filterData        = {
                        .DestinationPort = LocalPort ,
                        .ExpectedNetif   = Netif ,
        };

        oC_Mutex_Give(ModuleBusy);

        if(ErrorCode(oC_NetifMan_ReceivePacket(&addressFilter, &Packet->Packet, gettimeout(endTimestamp), &receivedFromNetif, PacketFilterFunction, &filterData)))
        {
            datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

            if(ErrorCondition( isram(datagram) , oC_ErrorCode_PacketNotCorrect ))
            {
                ConvertHeaderFromNetworkEndianess(&datagram->Header);
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Calculates checksum for UDP packet
 *
 * The function is for calculating checksum for UDP packets. The packet has to be in the host byte endianess.
 *
 * @param Packet                    Packet to calculate a checksum
 * @param outChecksumCorrect        Destination for a flag that will be set if checksum is correct (or #NULL if not used)
 * @param outExpectedChecksum       Destination for a checksum (or #NULL if not used)
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_AddressNotInRam                 | `Packet` does not point to the RAM section
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outChecksumCorrect` or `outExpectedChecksum` does not point to the RAM section
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Udp_CalculateChecksum( oC_Udp_Packet_t * Packet , bool * outChecksumCorrect , uint16_t * outExpectedChecksum )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_Udp)
     && ErrorCondition( isram(Packet)                                                   , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( outChecksumCorrect  == NULL || isram(outChecksumCorrect)        , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( outExpectedChecksum == NULL || isram(outExpectedChecksum)       , oC_ErrorCode_OutputAddressNotInRAM        )
        )
    {
        oC_Udp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if( ErrorCondition(isram(datagram), oC_ErrorCode_PacketNotCorrect) )
        {
            uint16_t checksum = CalculateChecksum(Packet);

            if(checksum == 0)
            {
                checksum = 0xFFFF;
            }

            if(outChecksumCorrect != NULL)
            {
                *outChecksumCorrect = checksum == datagram->Header.Checksum || datagram->Header.Checksum == 0;
            }

            if(outExpectedChecksum)
            {
                *outExpectedChecksum = checksum;
            }

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

/** @} */
#undef  _________________________________________FUNCTIONS_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief function used in NetifMan for filtering packets
 */
//==========================================================================================================================================
static bool PacketFilterFunction( oC_Net_Packet_t * Packet , const void * Parameter , oC_Netif_t Netif )
{
    bool                      packetForUs = false;
    oC_Udp_Header_t *         header      = oC_Net_Packet_GetDataReference(Packet);
    const PacketFilterData_t* filterData  = Parameter;

    if(header != NULL && Parameter != NULL)
    {
        oC_Udp_Port_t destinationPort = filterData->DestinationPort;

        if(oC_Net_ConvertUint16FromNetworkEndianess(header->DestinationPort) == destinationPort )
        {
            packetForUs = filterData->ExpectedNetif == NULL || filterData->ExpectedNetif == Netif;
        }
    }

    return packetForUs;
}

//==========================================================================================================================================
/**
 * @brief finds reservation of port or process
 */
//==========================================================================================================================================
static ReservationData_t* FindReservation( oC_Udp_Port_t Port , oC_Process_t Process )
{
    ReservationData_t * foundReservation = NULL;

    foreach(ReservedPorts,reservation)
    {
        if(
            (reservation->Port      == Port    && Port    != oC_Udp_Port_Empty  )
         || (reservation->Process   == Process && Process != NULL               )
            )
        {
            foundReservation = reservation;
        }
    }

    return foundReservation;
}

//==========================================================================================================================================
/**
 * @brief calculates checksum for a packet
 */
//==========================================================================================================================================
static uint16_t CalculateChecksum( oC_Udp_Packet_t * Packet )
{
    uint32_t               checksum         = 0;
    oC_Net_PacketType_t    packetType       = oC_Net_Packet_GetType(&Packet->Packet);

    if(packetType == oC_Net_PacketType_IPv4)
    {
        uint16_t * srcAddr = (uint16_t*)&Packet->IPv4.Header.SourceIp;
        uint16_t * dstAddr = (uint16_t*)&Packet->IPv4.Header.DestinationIp;

        checksum += srcAddr[1];
        checksum += srcAddr[0];

        checksum += dstAddr[1];
        checksum += dstAddr[0];

        checksum += oC_Net_Protocol_UDP;

        checksum += Packet->IPv4.UdpDatagram.Header.Length;

        checksum += Packet->IPv4.UdpDatagram.Header.SourcePort;

        checksum += Packet->IPv4.UdpDatagram.Header.DestinationPort;

        checksum += Packet->IPv4.UdpDatagram.Header.Length;

        while(checksum >> 16)
        {
            checksum = (checksum & 0xFFFF) + (checksum >> 16);
        }

        checksum = oC_Net_CalculateChecksum(Packet->IPv4.UdpDatagram.Data, oC_Udp_Packet_GetDataSize(Packet), checksum, true, true);
    }

    return (uint16_t)checksum;
}

//==========================================================================================================================================
/**
 * @brief returns true if port is special port
 */
//==========================================================================================================================================
static bool IsSpecialPort( oC_Udp_Port_t Port )
{
    return Port < oC_Udp_Port_NumberOfSpecialPorts;
}

//==========================================================================================================================================
/**
 * @brief checks if port is reserved
 */
//==========================================================================================================================================
static bool IsPortReserved( oC_Udp_Port_t Port )
{
    ReservationData_t * reservationData = FindReservation(Port,NULL);
    return reservationData != NULL;
}

//==========================================================================================================================================
/**
 * @brief converts UDP header to network endianess
 */
//==========================================================================================================================================
static void ConvertHeaderToNetworkEndianess( oC_Udp_Header_t * Header )
{
    Header->Checksum        = oC_Net_ConvertUint16ToNetworkEndianess( Header->Checksum         );
    Header->DestinationPort = oC_Net_ConvertUint16ToNetworkEndianess( Header->DestinationPort  );
    Header->SourcePort      = oC_Net_ConvertUint16ToNetworkEndianess( Header->SourcePort       );
    Header->Length          = oC_Net_ConvertUint16ToNetworkEndianess( Header->Length           );
}

//==========================================================================================================================================
/**
 * @brief converts UDP header from network endianess
 */
//==========================================================================================================================================
static void ConvertHeaderFromNetworkEndianess( oC_Udp_Header_t * Header )
{
    Header->Checksum        = oC_Net_ConvertUint16FromNetworkEndianess( Header->Checksum         );
    Header->DestinationPort = oC_Net_ConvertUint16FromNetworkEndianess( Header->DestinationPort  );
    Header->SourcePort      = oC_Net_ConvertUint16FromNetworkEndianess( Header->SourcePort       );
    Header->Length          = oC_Net_ConvertUint16FromNetworkEndianess( Header->Length           );
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

