/** ****************************************************************************************************************************************
 *
 * @brief      File with interface for HTTP
 *
 * @file       oc_http.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2024 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @defgroup Http   Hypertext Transfer Protocol
 * @brief module for handling HTTP packets
 *
 ******************************************************************************************************************************************/

#include <string.h>
#include <stdlib.h>

#include <oc_http.h>
#include <oc_array.h>
#include <oc_stdlib.h>
#include <oc_stdio.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief HTTP status code
 * 
 * The status code is for storing HTTP status code messages.
 */
//==========================================================================================================================================
typedef struct 
{
    oC_Http_StatusCode_t    Code;       //!< Status code
    const char*             Message;    //!< Status message
} HttpStatusTranslation_t;


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static oC_Http_Version_t    ParseVersion    ( char* Data );
static oC_Http_Method_t     ParseMethod     ( char* Data, char** outEnd );
static const char*          ParsePath       ( char* Data, char** outEnd );
static oC_ErrorCode_t       ParseRequestLine( oC_Http_Request_t* outRequest , char* Data, char* DataEnd );
static oC_ErrorCode_t       ParseHeader     ( oC_Http_Header_t* outHeader   , char* Data, char* DataEnd );
static oC_ErrorCode_t       ParseHeaders    ( oC_Http_Request_t *outRequest, char *Data, char* DataEnd );
static void                 AnalyzeHeader   ( oC_Http_Header_t *header, oC_Http_Request_t *outRequest );
static const char*          GetStatusMessage( oC_Http_StatusCode_t StatusCode );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//! List of HTTP methods
static const char* oC_Http_Methods[oC_Http_Method_NumberOfElements] = {
    [oC_Http_Method_Get] = "GET",
    [oC_Http_Method_Post] = "POST",
    [oC_Http_Method_Put] = "PUT",
    [oC_Http_Method_Delete] = "DELETE",
    [oC_Http_Method_Head] = "HEAD",
    [oC_Http_Method_Patch] = "PATCH",
    [oC_Http_Method_Options] = "OPTIONS",
    [oC_Http_Method_Trace] = "TRACE",
    [oC_Http_Method_Connect] = "CONNECT"
};

//! List of HTTP versions
static const char* oC_Http_Versions[oC_Http_Version_NumberOfElements] = {
    [oC_Http_Version_1_0] = "HTTP/1.0",
    [oC_Http_Version_1_1] = "HTTP/1.1",
    [oC_Http_Version_2_0] = "HTTP/2.0"
};

//! End of the HTTP header
static const char* oC_Http_HeaderEnd = "\r\n";

//! List of HTTP header IDs
static const char* oC_Http_HeaderIds[oC_Http_HeaderId_NumberOfElements] = {
    [oC_Http_HeaderId_Host] = "Host",
    [oC_Http_HeaderId_UserAgent] = "User-Agent",
    [oC_Http_HeaderId_Accept] = "Accept",
    [oC_Http_HeaderId_AcceptLanguage] = "Accept-Language",
    [oC_Http_HeaderId_AcceptEncoding] = "Accept-Encoding",
    [oC_Http_HeaderId_Connection] = "Connection",
    [oC_Http_HeaderId_CacheControl] = "Cache-Control",
    [oC_Http_HeaderId_ContentType] = "Content-Type",
    [oC_Http_HeaderId_ContentLength] = "Content-Length",
    [oC_Http_HeaderId_TransferEncoding] = "Transfer-Encoding",
    [oC_Http_HeaderId_Upgrade] = "Upgrade",
    [oC_Http_HeaderId_Origin] = "Origin",
    [oC_Http_HeaderId_Referer] = "Referer",
    [oC_Http_HeaderId_Authorization] = "Authorization",
    [oC_Http_HeaderId_IfModifiedSince] = "If-Modified-Since",
    [oC_Http_HeaderId_IfNoneMatch] = "If-None-Match",
    [oC_Http_HeaderId_AccessControlRequestMethod] = "Access-Control-Request-Method",
    [oC_Http_HeaderId_AccessControlRequestHeaders] = "Access-Control-Request-Headers",
};

//! List of HTTP content types
static const char* oC_Http_ContentTypes[oC_Http_ContentType_NumberOfElements] = {
    [oC_Http_ContentType_TextPlain] = "text/plain",
    [oC_Http_ContentType_TextHtml] = "text/html",
    [oC_Http_ContentType_TextCss] = "text/css",
    [oC_Http_ContentType_TextJavascript] = "text/javascript",
    [oC_Http_ContentType_ApplicationJson] = "application/json",
    [oC_Http_ContentType_ApplicationXml] = "application/xml",
    [oC_Http_ContentType_ApplicationFormUrlEncoded] = "application/x-www-form-urlencoded",
    [oC_Http_ContentType_MultipartFormData] = "multipart/form-data",
    [oC_Http_ContentType_ImagePng] = "image/png",
    [oC_Http_ContentType_ImageJpeg] = "image/jpeg",
    [oC_Http_ContentType_ImageGif] = "image/gif",
    [oC_Http_ContentType_ImageSvgXml] = "image/svg+xml",
};

//! List of HTTP connection types
static const char* oC_Http_Connections[oC_Http_Connection_NumberOfElements] = {
    [oC_Http_Connection_KeepAlive] = "keep-alive",
    [oC_Http_Connection_Close] = "close",
    [oC_Http_Connection_Upgrade] = "upgrade",
};

//! List of HTTP status codes
static const HttpStatusTranslation_t oC_Http_StatusMessages[] = {
    { .Code = oC_Http_StatusCode_Continue, .Message = "Continue" },
    { .Code = oC_Http_StatusCode_SwitchingProtocols, .Message = "Switching Protocols" },
    { .Code = oC_Http_StatusCode_Processing, .Message = "Processing" },
    { .Code = oC_Http_StatusCode_EarlyHints, .Message = "Early Hints" },
    { .Code = oC_Http_StatusCode_OK, .Message = "OK" },
    { .Code = oC_Http_StatusCode_Created, .Message = "Created" },
    { .Code = oC_Http_StatusCode_Accepted, .Message = "Accepted" },
    { .Code = oC_Http_StatusCode_NonAuthoritativeInformation, .Message = "Non-Authoritative Information" },
    { .Code = oC_Http_StatusCode_NoContent, .Message = "No Content" },
    { .Code = oC_Http_StatusCode_ResetContent, .Message = "Reset Content" },
    { .Code = oC_Http_StatusCode_PartialContent, .Message = "Partial Content" },
    { .Code = oC_Http_StatusCode_MultiStatus, .Message = "Multi-Status" },
    { .Code = oC_Http_StatusCode_AlreadyReported, .Message = "Already Reported" },
    { .Code = oC_Http_StatusCode_IMUsed, .Message = "IM Used" },
    { .Code = oC_Http_StatusCode_MultipleChoices, .Message = "Multiple Choices" },
    { .Code = oC_Http_StatusCode_MovedPermanently, .Message = "Moved Permanently" },
    { .Code = oC_Http_StatusCode_Found, .Message = "Found" },
    { .Code = oC_Http_StatusCode_SeeOther, .Message = "See Other" },
    { .Code = oC_Http_StatusCode_NotModified, .Message = "Not Modified" },
    { .Code = oC_Http_StatusCode_UseProxy, .Message = "Use Proxy" },
    { .Code = oC_Http_StatusCode_TemporaryRedirect, .Message = "Temporary Redirect" },
    { .Code = oC_Http_StatusCode_PermanentRedirect, .Message = "Permanent Redirect" },
    { .Code = oC_Http_StatusCode_BadRequest, .Message = "Bad Request" },
    { .Code = oC_Http_StatusCode_Unauthorized, .Message = "Unauthorized" },
    { .Code = oC_Http_StatusCode_PaymentRequired, .Message = "Payment Required" },
    { .Code = oC_Http_StatusCode_Forbidden, .Message = "Forbidden" },
    { .Code = oC_Http_StatusCode_NotFound, .Message = "Not Found" },
    { .Code = oC_Http_StatusCode_MethodNotAllowed, .Message = "Method Not Allowed" },
    { .Code = oC_Http_StatusCode_NotAcceptable, .Message = "Not Acceptable" },
    { .Code = oC_Http_StatusCode_ProxyAuthenticationRequired, .Message = "Proxy Authentication Required" },
    { .Code = oC_Http_StatusCode_RequestTimeout, .Message = "Request Timeout" },
    { .Code = oC_Http_StatusCode_Conflict, .Message = "Conflict" },
    { .Code = oC_Http_StatusCode_Gone, .Message = "Gone" },
    { .Code = oC_Http_StatusCode_LengthRequired, .Message = "Length Required" },
    { .Code = oC_Http_StatusCode_PreconditionFailed, .Message = "Precondition Failed" },
    { .Code = oC_Http_StatusCode_PayloadTooLarge, .Message = "Payload Too Large" },
    { .Code = oC_Http_StatusCode_UnsupportedMediaType, .Message = "Unsupported Media Type" },
    { .Code = oC_Http_StatusCode_RangeNotSatisfiable, .Message = "Range Not Satisfiable" },
    { .Code = oC_Http_StatusCode_ExpectationFailed, .Message = "Expectation Failed" },
    { .Code = oC_Http_StatusCode_ImATeapot, .Message = "I'm a teapot" },
    { .Code = oC_Http_StatusCode_MisdirectedRequest, .Message = "Misdirected Request" },
    { .Code = oC_Http_StatusCode_UnprocessableEntity, .Message = "Unprocessable Entity" },
    { .Code = oC_Http_StatusCode_Locked, .Message = "Locked" },
    { .Code = oC_Http_StatusCode_FailedDependency, .Message = "Failed Dependency" },
    { .Code = oC_Http_StatusCode_TooEarly, .Message = "Too Early" },
    { .Code = oC_Http_StatusCode_UpgradeRequired, .Message = "Upgrade Required" },
    { .Code = oC_Http_StatusCode_PreconditionRequired, .Message = "Precondition Required" },
    { .Code = oC_Http_StatusCode_TooManyRequests, .Message = "Too Many Requests" },
    { .Code = oC_Http_StatusCode_RequestHeaderFieldsTooLarge, .Message = "Request Header Fields Too Large" },
    { .Code = oC_Http_StatusCode_UnavailableForLegalReasons, .Message = "Unavailable For Legal Reasons" },
    { .Code = oC_Http_StatusCode_InternalServerError, .Message = "Internal Server Error" },
    { .Code = oC_Http_StatusCode_NotImplemented, .Message = "Not Implemented" },
    { .Code = oC_Http_StatusCode_BadGateway, .Message = "Bad Gateway" },
    { .Code = oC_Http_StatusCode_ServiceUnavailable, .Message = "Service Unavailable" },
    { .Code = oC_Http_StatusCode_GatewayTimeout, .Message = "Gateway Timeout" },
    { .Code = oC_Http_StatusCode_HTTPVersionNotSupported, .Message = "HTTP Version Not Supported" },
    { .Code = oC_Http_StatusCode_VariantAlsoNegotiates, .Message = "Variant Also Negotiates" },
    { .Code = oC_Http_StatusCode_InsufficientStorage, .Message = "Insufficient Storage" },
    { .Code = oC_Http_StatusCode_LoopDetected, .Message = "Loop Detected" },
    { .Code = oC_Http_StatusCode_NotExtended, .Message = "Not Extended" },
    { .Code = oC_Http_StatusCode_NetworkAuthenticationRequired, .Message = "Network Authentication Required" },
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Parses HTTP request
 *
 * The function is for parsing HTTP request. The function parses the request and stores it in the `outRequest` structure.
 *
 * @param[out] outRequest    Request that is parsed
 * @param[in]  Data          Data to parse 
 * @note 
 * To save memory, the function does not copy the data. The data must be stored in the RAM memory. Please also be aware that the data 
 * can be modified by the function.
 * @param[in]  DataSize      Size of the buffer with data (it does not need to be string length)
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 *  Code of error                                 | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  
 *
 *  @see oC_Http_ParseResponse
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Http_ParseRequest( oC_Http_Request_t* outRequest , char* Data, oC_MemorySize_t DataSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( 
        ErrorCondition( isram(outRequest)          , oC_ErrorCode_OutputAddressNotInRAM  )
     && ErrorCondition( isram(Data)                , oC_ErrorCode_OutputAddressNotInRAM  )
     && ErrorCondition( Data[0] != '\0'            , oC_ErrorCode_StringIsEmpty          )
        )
    {
        char* DataEnd = Data + DataSize;
        char* nextLine = strchr(Data, '\n');
        if(
            ErrorCode( ParseRequestLine( outRequest , Data , DataEnd ) )
         && ErrorCondition( nextLine != NULL, oC_ErrorCode_UnexpectedEndOfHttpHeader )
            )
        {
            nextLine++;
            errorCode = ParseHeaders( outRequest, nextLine, DataEnd );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prepares status line
 * 
 * The function is for preparing the status line. The function prepares the status line and stores it in the `outData` buffer.
 * 
 * @param[in]  Response             Response to prepare
 * @param[out] outData              Buffer for the status line
 * @param[in,out] DataSize          Size of the buffer with data (it does not need to be string length)
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 * 
 * List of standard error codes, that can be returned by the function:
 * 
 *  Code of error                                 | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_WrongAddress                    | Wrong address of the response
 *  #oC_ErrorCode_OutputAddressNotInRAM           | Output address is not in the RAM memory
 *  #oC_ErrorCode_OutputBufferTooSmall            | Output buffer is too small
 *  #oC_ErrorCode_HttpVersionNotSupported         | HTTP version is not supported
 *  #oC_ErrorCode_HttpStatusCodeNotSupported      | HTTP status code is not supported
 * 
 * @warning More error codes can be returned by the function #snprintf
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Http_PrepareStatusLine( const oC_Http_Response_t* Response, char** outData, oC_MemorySize_t * DataSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Response)                                  , oC_ErrorCode_WrongAddress                 )   
     && ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(*outData)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(DataSize)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( *DataSize > 0                                               , oC_ErrorCode_OutputBufferTooSmall )
     && ErrorCondition( Response->Version < oC_Http_Version_NumberOfElements        , oC_ErrorCode_HttpVersionNotSupported      )
     && ErrorCondition( Response->StatusCode >= 100 && Response->StatusCode < 600   , oC_ErrorCode_HttpStatusCodeNotSupported   )
        )
    {
        const char* statusMessage = GetStatusMessage( Response->StatusCode );
        const char* version = oC_Http_Versions[Response->Version];
        char* buffer = *outData;
        oC_MemorySize_t bufferSize = *DataSize;

        int length = snprintf(buffer, bufferSize, "%s %d %s\r\n", version, Response->StatusCode, statusMessage);
        if(length > 0)
        {
            if(length < *DataSize)
            {
                *DataSize -= length;
            }
            *outData = &buffer[length];
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = -length;
        }
    }
    
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prepares header
 * 
 * The function is for preparing the header. The function prepares the header and stores it in the `outData` buffer.
 * 
 * @param[in]  Header               Header to prepare (ID or Name must be set)
 * @param[out] outData              Buffer for the header
 * @param[in,out] DataSize          Size of the buffer with data (it does not need to be string length)
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 * 
 * List of standard error codes, that can be returned by the function:
 * 
 *  Code of error                                 | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_WrongAddress                    | Wrong address of the header
 *  #oC_ErrorCode_OutputAddressNotInRAM           | Output address is not in the RAM memory
 *  #oC_ErrorCode_OutputBufferTooSmall            | Output buffer is too small
 *  #oC_ErrorCode_HttpHeaderIdNotSupported        | HTTP header ID is not supported
 * 
 * @warning More error codes can be returned by the function #snprintf
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Http_PrepareHeader( const oC_Http_Header_t* Header, char** outData, oC_MemorySize_t * DataSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Header)                                    , oC_ErrorCode_WrongAddress                 )   
     && ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(*outData)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(DataSize)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( *DataSize > 0                                               , oC_ErrorCode_OutputBufferTooSmall         )
     && ErrorCondition( Header->Id < oC_Http_HeaderId_NumberOfElements              , oC_ErrorCode_HttpHeaderIdNotSupported     )
     && ErrorCondition( Header->Name == NULL || isaddresscorrect(Header->Name)      , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( isaddresscorrect(Header->Value)                             , oC_ErrorCode_WrongAddress                 )
        )
    {
        const char* headerName = Header->Name;
        if(Header->Id != oC_Http_HeaderId_None && Header->Id < oC_ARRAY_SIZE(oC_Http_HeaderIds) && oC_Http_HeaderIds[Header->Id] != NULL)
        {
            headerName = oC_Http_HeaderIds[Header->Id];
        }
        if(headerName != NULL)
        {
            char* buffer = *outData;
            oC_MemorySize_t bufferSize = *DataSize;

            int length = snprintf(buffer, bufferSize, "%s: %s\r\n", headerName, Header->Value);
            if(length > 0)
            {
                if(length < *DataSize)
                {
                    *DataSize -= length;
                }
                *outData  = &buffer[length];
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = -length;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prepares content length
 * 
 * The function is for preparing the content length. The function prepares the content length and stores it in the `outData` buffer.
 * 
 * @param[in]  ContentLength        Content length to prepare
 * @param[out] outData              Buffer for the content length
 * @param[in,out] DataSize          Size of the buffer with data (it does not need to be string length)
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 * 
 * List of standard error codes, that can be returned by the function:
 * 
 *  Code of error                                 | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_OutputAddressNotInRAM           | Output address is not in the RAM memory
 *  #oC_ErrorCode_OutputBufferTooSmall            | Output buffer is too small
 * 
 * @warning More error codes can be returned by the function #snprintf
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Http_PrepareContentLength( oC_MemorySize_t ContentLength, char** outData, oC_MemorySize_t * DataSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(DataSize)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( *DataSize > 0                                               , oC_ErrorCode_OutputBufferTooSmall         )
        )
    {
        char* buffer = *outData;
        oC_MemorySize_t bufferSize = *DataSize;

        int length = snprintf(buffer, bufferSize, "Content-Length: %lld\r\n", ContentLength);
        if(length > 0)
        {
            if(length < *DataSize)
            {
                *DataSize -= length;
            }
            *outData = &buffer[length];
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = -length;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prepares content type
 * 
 * The function is for preparing the content type. The function prepares the content type and stores it in the `outData` buffer.
 * 
 * @param[in]  ContentType          Content type to prepare
 * @param[out] outData              Buffer for the content type
 * @param[in,out] DataSize          Size of the buffer with data (it does not need to be string length)
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 * 
 * List of standard error codes, that can be returned by the function:
 * 
 *  Code of error                                 | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_OutputAddressNotInRAM           | Output address is not in the RAM memory
 *  #oC_ErrorCode_OutputBufferTooSmall            | Output buffer is too small
 *  #oC_ErrorCode_HttpContentTypeNotSupported     | HTTP content type is not supported
 * 
 * @warning More error codes can be returned by the function #snprintf
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Http_PrepareContentType( oC_Http_ContentType_t ContentType, char** outData, oC_MemorySize_t * DataSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ContentType < oC_Http_ContentType_NumberOfElements          , oC_ErrorCode_HttpContentTypeNotSupported )
     && ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(DataSize)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( *DataSize > 0                                               , oC_ErrorCode_OutputBufferTooSmall         )
        )
    {
        const char* contentType = oC_Http_ContentTypes[ContentType];
        if(ErrorCondition(contentType != NULL, oC_ErrorCode_WrongAddress))
        {
            char* buffer = *outData;
            oC_MemorySize_t bufferSize = *DataSize;

            int length = snprintf(buffer, bufferSize, "Content-Type: %s\r\n", contentType);
            if(length > 0)
            {
                if(length < *DataSize)
                {
                    *DataSize -= length;
                }
                *outData = &buffer[length];
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = -length;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prepares headers
 * 
 * The function is for preparing the headers. The function prepares the headers and stores them in the `outData` buffer.
 * 
 * @param[in]  Headers              Headers to prepare
 * @param[in]  NumberOfHeaders      Number of headers
 * @param[out] outData              Buffer for the headers
 * @param[in,out] DataSize          Size of the buffer with data (it does not need to be string length)
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 * 
 * List of standard error codes, that can be returned by the function:
 * 
 *  Code of error                                 | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_WrongAddress                    | Wrong address of the headers
 *  #oC_ErrorCode_SizeNotCorrect                  | Number of headers is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM           | Output address is not in the RAM memory
 *  #oC_ErrorCode_OutputBufferTooSmall            | Output buffer is too small
 * 
 * @warning More error codes can be returned by the function #oC_Http_PrepareHeader
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Http_PrepareHeaders( const oC_Http_Header_t* Headers, uint16_t NumberOfHeaders, char** outData, oC_MemorySize_t * DataSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Headers)                                   , oC_ErrorCode_WrongAddress                 )   
     && ErrorCondition( NumberOfHeaders > 0                                         , oC_ErrorCode_SizeNotCorrect               ) 
     && ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(*outData)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(DataSize)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( *DataSize > 0                                               , oC_ErrorCode_OutputBufferTooSmall         )
        )
    {
        errorCode = oC_ErrorCode_None;

        for(uint16_t i = 0 ; i < NumberOfHeaders ; i++)
        {
            if(Headers[i].Id != oC_Http_HeaderId_None || Headers[i].Name != NULL)
            {
                if(!ErrorCode( oC_Http_PrepareHeader( &Headers[i], outData, DataSize ) ))
                {
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prepares response
 * 
 * The function is for preparing the response. The function prepares the response and stores it in the `outData` buffer.
 * 
 * @param[in]  Response             Response to prepare
 * @param[out] outData              Buffer for the response
 * @param[in,out] DataSize          Size of the buffer with data (it does not need to be string length)
 * 
 * @return code of error or `oC_ErrorCode_None` if success
 * 
 * List of standard error codes, that can be returned by the function:
 * 
 *  Code of error                                 | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_WrongAddress                    | Wrong address of the response
 *  #oC_ErrorCode_OutputAddressNotInRAM           | Output address is not in the RAM memory
 *  #oC_ErrorCode_OutputBufferTooSmall            | Output buffer is too small
 *  #oC_ErrorCode_HttpVersionNotSupported         | HTTP version is not supported
 *  #oC_ErrorCode_HttpStatusCodeNotSupported      | HTTP status code is not supported
 * 
 * @warning More error codes can be returned by the functions #oC_Http_PrepareStatusLine and #oC_Http_PrepareHeaders
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Http_PrepareResponse( const oC_Http_Response_t* Response, char** outData, oC_MemorySize_t * DataSize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Response)                                  , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(DataSize)                                             , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( *DataSize > 0                                               , oC_ErrorCode_OutputBufferTooSmall )
     && ErrorCondition( Response->Version < oC_Http_Version_NumberOfElements        , oC_ErrorCode_HttpVersionNotSupported      )
     && ErrorCondition( Response->StatusCode >= 100 && Response->StatusCode < 600   , oC_ErrorCode_HttpStatusCodeNotSupported   )
        )
    {
        char* buffer = *outData;
        oC_MemorySize_t bufferSize = *DataSize;
        if(
            ErrorCode( oC_Http_PrepareStatusLine( Response, &buffer, &bufferSize ) )
         && ErrorCode( oC_Http_PrepareContentLength( Response->ContentLength, &buffer, &bufferSize ) )
         && ErrorCode( oC_Http_PrepareContentType( Response->ContentType, &buffer, &bufferSize ) )
         && ErrorCode( oC_Http_PrepareHeaders( Response->Packet.Headers, oC_ARRAY_SIZE(Response->Packet.Headers), &buffer, &bufferSize ) )
            )
        {
            int length = snprintf(buffer, bufferSize, "\r\n%s%c", Response->Packet.Body != NULL ? Response->Packet.Body : "", 0);
            if(length > 0)
            {
                if(length <= bufferSize)
                {
                    bufferSize -= length;
                }
                *DataSize = bufferSize;
                *outData = &buffer[length];
                buffer[length] = 0;
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = -length;
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Parses HTTP version
 *
 * The function is for parsing HTTP version. The function parses the version and returns the value of the version.
 *
 * @param[in] Data    Data to parse
 *
 * @return HTTP version or `oC_Http_Version_Unsupported` if the version is not supported
 */
//==========================================================================================================================================
static oC_Http_Version_t ParseVersion( char* Data )
{
    oC_Http_Version_t version = oC_Http_Version_Unsupported;

    oC_STATIC_ASSERT( oC_Http_Version_NumberOfElements == oC_ARRAY_SIZE(oC_Http_Versions), 
            "HTTP Version names array does not match the enum" );

    const char* versionStart = Data;
    for( oC_Http_Version_t i = 0 ; i < oC_Http_Version_NumberOfElements ; i++ )
    {
        if( strncmp( versionStart , oC_Http_Versions[i], 8 ) == 0 )
        {
            version = i;
            break;
        }
    }

    return version;
}

//==========================================================================================================================================
/**
 * @brief Parses HTTP method
 *
 * The function is for parsing HTTP method. The function parses the method and returns the value of the method.
 *
 * @param[in] Data    Data to parse
 *
 * @return HTTP method or `oC_Http_Method_Unsupported` if the method is not supported
 */
//==========================================================================================================================================
static oC_Http_Method_t ParseMethod( char* Data, char** outEnd )
{
    oC_Http_Method_t method = oC_Http_Method_Unsupported;

    oC_STATIC_ASSERT( oC_Http_Method_NumberOfElements == oC_ARRAY_SIZE(oC_Http_Methods), 
            "HTTP Method names array does not match the enum" );
    char* space = strchr( Data , ' ' );
    if(space != NULL)
    {
        space[0] = '\0';
        *outEnd = space;
        for( oC_Http_Method_t i = 0 ; i < oC_Http_Method_NumberOfElements ; i++ )
        {
            if( strncmp( Data , oC_Http_Methods[i], strlen(oC_Http_Methods[i]) ) == 0 )
            {
                method = i;
                break;
            }
        }
    }

    return method;
}

//==========================================================================================================================================
/**
 * @brief Parses HTTP path
 *
 * The function is for parsing HTTP path. The function parses the path and returns the value of the path.
 *
 * @param[in]  Data      Data to parse
 * @param[out] outEnd   Pointer to the end of the path
 *
 * @return HTTP path or `NULL` if the path is not supported
 */
//==========================================================================================================================================
static const char* ParsePath( char* Data, char** outEnd )
{
    char* path = NULL;

    if( Data != NULL )
    {
        char* space = Data;
        path = space + 1;
        space = strchr( path , ' ' );
        if( space != NULL )
        {
            char* end = strchr( path , ' ' );
            if( end != NULL )
            {
                end[0] = '\0';
                if( outEnd != NULL )
                {
                    *outEnd = end;
                }
            }
        }
    }

    return path;
}

//==========================================================================================================================================
/**
 * @brief Parses HTTP request line
 *
 * The function is for parsing HTTP request line. The function parses the request line and stores it in the `outRequest` structure.
 *
 * @param[out] outRequest    Request that is parsed
 * @param[in]  Data          Data to parse
 * @param[in]  DataEnd       End of the data
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 *   Code of error                                | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_InvalidHttpHeader               | Invalid HTTP header
 *  #oC_ErrorCode_HttpPathNotFound                | HTTP path request not found
 *  #oC_ErrorCode_HttpVersionNotSupported         | HTTP version not supported
 *  #oC_ErrorCode_UnexpectedEndOfHttpHeader       | Unexpected end of HTTP header
 * 
 *  @see oC_Http_ParseRequest
 */
//==========================================================================================================================================
static oC_ErrorCode_t ParseRequestLine( oC_Http_Request_t* outRequest, char* Data, char* DataEnd )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( Data < DataEnd, oC_ErrorCode_UnexpectedEndOfHttpHeader ) )
    {
        char* methodEnd = NULL;
        char* pathEnd   = NULL;
        outRequest->Method = ParseMethod( Data, &methodEnd );
        outRequest->Path   = ParsePath( methodEnd, &pathEnd );
        if( 
           ErrorCondition( outRequest->Method < oC_Http_Method_NumberOfElements    , oC_ErrorCode_InvalidHttpHeader            ) 
        && ErrorCondition( outRequest->Path   != NULL                               , oC_ErrorCode_HttpPathNotFound             )
        && ErrorCondition( outRequest->Path < DataEnd                               , oC_ErrorCode_UnexpectedEndOfHttpHeader    )
        && ErrorCondition( pathEnd != NULL                                          , oC_ErrorCode_InvalidHttpHeader            )
            )
        {
            outRequest->Version = ParseVersion( ++pathEnd );
            if( ErrorCondition( outRequest->Version < oC_Http_Version_NumberOfElements, oC_ErrorCode_HttpVersionNotSupported ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Parses HTTP header
 *
 * The function is for parsing HTTP header. The function parses the header and stores it in the `outHeader` structure.
 *
 * @param[out] outHeader    Destination for the parsed header
 * @param[in]  Data         Data to parse
 * @param[in]  DataEnd      End of the data
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 *   Code of error                                | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 *  #oC_ErrorCode_InvalidHttpHeader               | Invalid HTTP header
 *  #oC_ErrorCode_UnexpectedEndOfHttpHeader       | Unexpected end of HTTP header
 * 
 *  @see oC_Http_ParseRequest
 */
//==========================================================================================================================================
static oC_ErrorCode_t ParseHeader( oC_Http_Header_t* outHeader, char* Data, char* DataEnd  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( Data < DataEnd, oC_ErrorCode_UnexpectedEndOfHttpHeader ) )
    {
        outHeader->Name     = Data;
        outHeader->Value    = strchr( Data , ':' );
        if( 
            ErrorCondition( outHeader->Value    != NULL, oC_ErrorCode_InvalidHttpHeader         )
         && ErrorCondition( outHeader->Value < DataEnd , oC_ErrorCode_UnexpectedEndOfHttpHeader )
            )
        {
            outHeader->Value[0] = '\0';
            outHeader->Value+=2;

            for( oC_Http_HeaderId_t i = 0 ; i < oC_Http_HeaderId_NumberOfElements ; i++ )
            {
                if( strcmp( outHeader->Name , oC_Http_HeaderIds[i] ) == 0 )
                {
                    outHeader->Id = i;
                    break;
                }
            }
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Parses HTTP headers
 *
 * The function is for parsing HTTP headers. The function parses the headers and stores them in the `outRequest` structure.
 *
 * @param[out] outRequest    Request that is parsed
 * @param[in]  Data          Data to parse
 * @param[in]  DataEnd       End of the data
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 *   Code of error                                | Description
 * -----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                  | Unexpected error occurred
 * 
 *  @see oC_Http_ParseRequest
 */
//==========================================================================================================================================
static oC_ErrorCode_t ParseHeaders( oC_Http_Request_t* outRequest, char* Data, char* DataEnd )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    int headerIndex = 0;
    bool bodyFound = false;
    char* line = Data;

    while(headerIndex < oC_HTTP_MAX_HEADERS_COUNT && line < DataEnd)
    {
        char* nextLine = strchr( line , '\r' );
        if(nextLine == NULL)
        {
            nextLine = strchr( line , '\n' );
        }
        if( strcmp(line, oC_Http_HeaderEnd) == 0 )
        {
            bodyFound = true;
            outRequest->Packet.Body = line + strlen(oC_Http_HeaderEnd);
            break;
        }

        oC_Http_Header_t* header = &outRequest->Packet.Headers[headerIndex];
        if( ErrorCode( ParseHeader( header , line, nextLine != NULL ? nextLine : DataEnd ) ) )
        {
            headerIndex++;
        }
        else
        {
            break;
        }
        line = nextLine;
        if(line == NULL)
        {
            break;
        }
        if(line[0] == '\r')
        {
            line[0] = 0;
            line++;
        }
        if(line[0] == '\n')
        {
            line[0] = 0;
            line++;
        }
        AnalyzeHeader(header, outRequest);
    }
    if( 
        !oC_ErrorOccur( errorCode )
     && ErrorCondition( bodyFound, oC_ErrorCode_TooManyHttpHeaders ) 
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Analyzes HTTP header
 *
 * The function is for analyzing HTTP header. The function analyzes the header and stores it in the `outRequest` structure.
 *
 * @param[out] header       Header to analyze
 * @param[out] outRequest   Request that is parsed
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 *  @see oC_Http_ParseRequest
 */
//==========================================================================================================================================
void AnalyzeHeader(oC_Http_Header_t *header, oC_Http_Request_t *outRequest)
{
    if (header->Id == oC_Http_HeaderId_ContentLength)
    {
        outRequest->ContentLength = atoi(header->Value);
    }
    else if (header->Id == oC_Http_HeaderId_ContentType)
    {
        outRequest->ContentType = oC_Http_ContentType_Unsupported;
        for (oC_Http_ContentType_t i = 0; i < oC_Http_ContentType_NumberOfElements; i++)
        {
            if (strcmp(header->Value, oC_Http_ContentTypes[i]) == 0)
            {
                outRequest->ContentType = i;
                break;
            }
        }
    }
    else if (header->Id == oC_Http_HeaderId_Connection)
    {
        outRequest->Connection = oC_Http_Connection_Unsupported;
        for (oC_Http_Connection_t i = 0; i < oC_Http_Connection_NumberOfElements; i++)
        {
            if (strcmp(header->Value, oC_Http_Connections[i]) == 0)
            {
                outRequest->Connection = i;
                break;
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief Gets status message
 *
 * The function is for getting status message. The function gets the message of the status code.
 *
 * @param[in] StatusCode    Status code
 *
 * @return status message or `Unknown` if the status code is not supported
 */
//==========================================================================================================================================
static const char* GetStatusMessage( oC_Http_StatusCode_t StatusCode )
{
    const char* message = "Unknown";

    for( oC_MemorySize_t i = 0 ; i < oC_ARRAY_SIZE(oC_Http_StatusMessages) ; i++ )
    {
        if( oC_Http_StatusMessages[i].Code == StatusCode )
        {
            message = oC_Http_StatusMessages[i].Message;
            break;
        }
    }

    return message;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
