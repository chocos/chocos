/** ****************************************************************************************************************************************
 *
 * @brief      Stores source of the ICMP module functions
 *
 * @file       oc_icmp.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_icmp.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_mutex.h>
#include <oc_netifman.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_Icmp_Type_t      Type;
    oC_Process_t        Process;
    oC_Mutex_t          Busy;
} ReservationData_t;

typedef struct
{
    oC_Icmp_Packet_t*   Packet;
    oC_Netif_t          Netif;
} SavedPacket_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static ReservationData_t* FindReservation       ( oC_Icmp_Type_t Type , oC_Process_t Process );
static uint16_t           CalculateChecksum     ( oC_Icmp_Packet_t * Packet );
static bool               PacketFilterFunction  ( oC_Icmp_Packet_t * ReceivedPacket , oC_Icmp_Type_t * ExpectedType , oC_Netif_t Netif );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_List(ReservationData_t*)  ReservedTypes   = NULL;                 //!< List of already used types
static oC_Event_t                   TypeReleased    = NULL;                 //!< Event that is active, when some type is released
static oC_Mutex_t                   ModuleBusy      = NULL;                 //!< Mutex taken, when the module is busy
static const oC_Allocator_t         Allocator       = {
                .Name = "icmp-module"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on ICMP module
 *
 * The function is for turning on ICMP module. The module has to be turned on before usage.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleIsTurnedOn                | UDP module is already turned on.
 *  oC_ErrorCode_AllocationError                 | Cannot allocate memory
 *
 *  @see oC_Icmp_TurnOff
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();
    if(
        oC_Module_TurnOffVerification(&errorCode, oC_Module_Icmp)
        )
    {
        ReservedTypes   = oC_List_New(&Allocator, AllocationFlags_Default);
        TypeReleased    = oC_Event_New( oC_Event_State_Inactive, &Allocator, AllocationFlags_Default );
        ModuleBusy      = oC_Mutex_New( oC_Mutex_Type_Normal   , &Allocator, AllocationFlags_Default);

        if(
            ErrorCondition( ReservedTypes   != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( ModuleBusy      != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( TypeReleased    != NULL , oC_ErrorCode_AllocationError )
            )
        {
            oC_Module_TurnOn(oC_Module_Icmp);
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            bool listDeleted        = oC_List_Delete( TypeReleased      , AllocationFlags_Default);
            bool mutexDeleted       = oC_Mutex_Delete(&ModuleBusy       , AllocationFlags_Default);
            bool eventDeleted       = oC_Event_Delete(&TypeReleased     , AllocationFlags_Default);

            oC_SaveIfFalse("ICMP:TurnOn - Cannot delete list "         , listDeleted         , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("ICMP:TurnOn - Cannot delete mutex "        , mutexDeleted        , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("ICMP:TurnOn - Cannot delete event"         , eventDeleted        , oC_ErrorCode_ReleaseError );
        }
    }
    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off ICMP module
 *
 * The function is for turning off ICMP module. The module has to be turned off before usage and turned off when is not needed.
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_ReleaseError                    | Cannot release memory
 *
 *  @see oC_Icmp_TurnOn
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();
    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_Icmp)
        )
    {
        oC_Module_TurnOff(oC_Module_Icmp);

        bool allPacketsReleased = true;

        bool listDeleted        = oC_List_Delete ( ReservedTypes   , AllocationFlags_Default );
        bool eventDeleted       = oC_Event_Delete( &TypeReleased   , AllocationFlags_Default );
        bool mutexDeleted       = oC_Mutex_Delete( &ModuleBusy     , AllocationFlags_Default );

        if(
            ErrorCondition( listDeleted                                    , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( allPacketsReleased                             , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( eventDeleted                                   , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( mutexDeleted                                   , oC_ErrorCode_ReleaseError  )
         && ErrorCondition( oC_MemMan_FreeAllMemoryOfAllocator(&Allocator) , oC_ErrorCode_ReleaseError  )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }
    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the given ICMP type is reserved
 *
 * The function checks if the given type is reserved. If `Process` parameter is not #NULL, the function will check if the given port
 * is reserved by this process.
 *
 * @param Type          Type to check
 * @param Process       Process which check if it is an owner of the port
 *
 * @return true if the port is reserved
 */
//==========================================================================================================================================
bool oC_Icmp_IsTypeReserved( oC_Icmp_Type_t Type , oC_Process_t Process )
{
    bool reserved = false;

    if(
        oC_Module_IsTurnedOn(oC_Module_Icmp)
     && oC_SaveIfFalse("UDP::IsPortReserved - ", Type != oC_Icmp_Type_Empty                         , oC_ErrorCode_TypeNotCorrect    )
     && oC_SaveIfFalse("UDP::IsPortReserved - ", Process == NULL || oC_Process_IsCorrect(Process)   , oC_ErrorCode_ProcessNotCorrect )
     )
    {
        ReservationData_t * reservation = FindReservation(Type,Process);
        reserved = reservation != NULL;
    }

    return reserved;
}
//==========================================================================================================================================
/**
 * @brief reserves a ICMP type
 *
 * Only one application can use a type at once, otherwise it can cause loosing of datagrams. This function allows to reserve a type before
 * usage. Reservation is performed for a current process and only this process will be able to use the reserved type.
 *
 * @param Type          Type to reserve
 * @param Timeout       Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | ICMP module is not turned on. Please call #oC_Icmp_TurnOn first
 *  oC_ErrorCode_PortNotAvailable                | The are no free ports or the selected one is already reserved
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Type is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_ModuleBusy                      | Module is still busy and maximum time for this operation has elapsed
 *
 *  @see oC_Icmp_Receive, oC_Icmp_Send, oC_Icmp_TurnOn, oC_Icmp_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_ReserveType( oC_Icmp_Type_t Type    , oC_Time_t    Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_Icmp)
     && ErrorCondition( Type >= 0                           , oC_ErrorCode_TypeNotCorrect      )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect      )
        )
    {
        oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

        while(oC_KTime_GetTimestamp() <= endTimestamp)
        {
            if( ErrorCondition( oC_Mutex_Take(ModuleBusy, oC_KTime_CalculateTimeout(endTimestamp)) , oC_ErrorCode_ModuleBusy  ) )
            {
                if( ErrorCondition( oC_Icmp_IsTypeReserved(Type,NULL) == false , oC_ErrorCode_TypeBusy ))
                {
                    ReservationData_t * reservation = kmalloc( sizeof(ReservationData_t), &Allocator, AllocationFlags_Default );

                    if(ErrorCondition( reservation != NULL , oC_ErrorCode_AllocationError ))
                    {
                        reservation->Busy = oC_Mutex_New( oC_Mutex_Type_Normal , &Allocator, AllocationFlags_Default );

                        if( ErrorCondition( reservation->Busy != NULL , oC_ErrorCode_AllocationError ) )
                        {
                            reservation->Type       = Type;
                            reservation->Process    = getcurprocess();

                            bool addedToList = oC_List_PushBack(ReservedTypes, reservation, &Allocator);

                            if( ErrorCondition( addedToList, oC_ErrorCode_CannotAddObjectToList ) )
                            {
                                errorCode = oC_ErrorCode_None;
                            }
                            else
                            {
                                oC_SaveIfFalse("ICMP:Reserve - cannot release reservation memory ", kfree(reservation, AllocationFlags_Default), oC_ErrorCode_ReleaseError);
                            }
                        }
                        else
                        {
                            oC_SaveIfFalse("ICMP:Reserve - cannot release reservation memory ", kfree(reservation, AllocationFlags_Default), oC_ErrorCode_ReleaseError);
                        }

                    }

                    oC_Mutex_Give(ModuleBusy);
                    break;
                }
                else
                {
                    oC_Mutex_Give(ModuleBusy);

                    if(oC_Event_WaitForState(TypeReleased,Type,oC_Event_StateMask_Full, oC_KTime_CalculateTimeout(endTimestamp)) == false)
                    {
                        errorCode = oC_ErrorCode_Timeout;
                        break;
                    }
                }
            }
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief releases a type
 *
 * Only one application can use a type at once, otherwise it can cause loosing of datagrams. This function allows to release reserved type
 * when it is not needed anymore. Reservation is performed for a current process and only this process is able to use the reserved type.
 * Usually all reserved types are automatically released, when the process is killed.
 *
 * @param Type              Local type reserved by calling a function #oC_Icmp_ReservePort
 * @param Timeout           Maximum time to wait for the module readiness
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | ICMP  module is not turned on. Please call #oC_Icmp_TurnOn first
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Type is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_ModuleBusy                      | Module is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_TypeNotReserved                 | The given `Type` is not reserved by using the function #oC_Icmp_ReservePort
 *  oC_ErrorCode_TypeReservedByDifferentProcess  | The given `Type` is reserved by different process.
 *  oC_ErrorCode_TypeBusy                        | The given `Type` is currently in use.
 *  oC_ErrorCode_CannotDeleteObject              | Cannot delete Mutex related with the type
 *
 *  @see oC_Icmp_Receive, oC_Icmp_Send, oC_Icmp_TurnOn, oC_Icmp_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_ReleaseType( oC_Icmp_Type_t Type    , oC_Time_t    Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        oC_Module_TurnOnVerification( &errorCode , oC_Module_Udp )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect       )
     && ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout)   , oC_ErrorCode_ModuleBusy           )
        )
    {
        ReservationData_t * reservation = FindReservation(Type,NULL);

        if(
            ErrorCondition( reservation != NULL                                                         , oC_ErrorCode_TypeNotReserved                  )
         && ErrorCondition( reservation->Process == getcurprocess()                                     , oC_ErrorCode_TypeReservedByDifferentProcess   )
         && ErrorCondition( oC_Mutex_Take(reservation->Busy,oC_KTime_CalculateTimeout(endTimestamp))    , oC_ErrorCode_TypeBusy                         )
            )
        {
            bool mutexDeleted      = oC_Mutex_Delete(&reservation->Busy, AllocationFlags_Default);
            bool deletedFromList   = oC_List_RemoveAll(ReservedTypes,reservation);
            bool deleted           = kfree(reservation,AllocationFlags_Default);

            if(
                ErrorCondition( mutexDeleted                , oC_ErrorCode_CannotDeleteObject           )
             && ErrorCondition( deletedFromList             , oC_ErrorCode_CannotRemoveObjectFromList   )
             && ErrorCondition( deleted                     , oC_ErrorCode_ReleaseError                 )
                )
            {
                oC_Event_SetState(TypeReleased,Type);
                errorCode = oC_ErrorCode_None;
            }
        }

        oC_Mutex_Give(ModuleBusy);
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief releases all types reserved by the process
 *
 * Only one application can use a type at once, otherwise it can cause loosing of datagrams. This function allows to release all reserved
 * types that are owned by the given process when they are not needed anymore.
 *
 * @note
 * This function can be called only by the root user. It is not enough to have admin privileges - you have to be a root.
 *
 * @param Process           Process that reserved some ICMP types
 * @param Timeout           Maximum time to wait for the module readiness
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | ICMP module is not turned on. Please call #oC_Icmp_TurnOn first
 *  oC_ErrorCode_ProcessNotCorrect               | `Process` is not correct or not valid anymore
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Module is still busy and maximum time for this operation has elapsed
 *  oC_ErrorCode_PossibleOnlyForRoot             | Function can be called only by the root user. You are not permitted to perform this
 *
 *  @see oC_Udp_Receive, oC_Udp_Send, oC_Udp_TurnOn, oC_Udp_ReleasePort
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_ReleaseAllTypesReservedBy( oC_Process_t   Process , oC_Time_t    Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        oC_Module_TurnOnVerification( &errorCode, oC_Module_Udp )
     && ErrorCondition( getcuruser() == getrootuser()       , oC_ErrorCode_PossibleOnlyForRoot  )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect       )
     && ErrorCondition( oC_Mutex_Take(ModuleBusy,Timeout)   , oC_ErrorCode_ModuleBusy           )
        )
    {
        ReservationData_t * reservation = FindReservation(oC_Icmp_Type_Empty, Process);

        errorCode = oC_ErrorCode_None;

        while(
                reservation != NULL
             && ErrorCondition( oC_KTime_GetTimestamp() <= endTimestamp , oC_ErrorCode_Timeout )
                )
        {
            reservation->Process = getcurprocess();

            ErrorCode( oC_Icmp_ReleaseType( reservation->Type, oC_KTime_CalculateTimeout(endTimestamp) ) );

            reservation = FindReservation( oC_Icmp_Type_Empty, Process );
        }

        oC_Mutex_Give(ModuleBusy);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Calculates checksum for ICMP packet
 *
 * The function is for calculating checksum for ICMP packets. The packet has to be in the host byte endianess.
 *
 * @param Packet                    Packet to calculate a checksum
 * @param outChecksum               Destination for a checksum
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | ICMP module is not turned on. Please call #oC_Icmp_TurnOn first
 *  oC_ErrorCode_AddressNotInRam                 | `Packet` does not point to the RAM section
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outChecksumCorrect` or `outExpectedChecksum` does not point to the RAM section
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_CalculateChecksum( oC_Icmp_Packet_t * Packet , uint16_t * outChecksum )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_Udp)
     && ErrorCondition( isram(Packet)           , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( isram(outChecksum)      , oC_ErrorCode_OutputAddressNotInRAM        )
        )
    {
        oC_Icmp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

        if( ErrorCondition(isram(datagram), oC_ErrorCode_PacketNotCorrect) )
        {
            *outChecksum = CalculateChecksum(Packet);
            errorCode    = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a ICMP packet
 *
 * The function is for allocating memory for the ICMP packet. It calculates memory required for the packet including all headers of sub-layers.
 * Memory should be released, when is not needed anymore by using the function #oC_Icmp_Packet_Delete
 *
 * @note
 * In case of failure, error can be read from the errors stack
 *
 * @param PacketAllocator       Allocator for the Memory Manager
 * @param Type                  Type of the packet (IPv4 or IPv6)
 * @param IcmpType              Type of the ICMP message
 * @param Code                  Code (SubType) of the ICMP message
 * @param Data                  Address of the data to put inside the UDP packet. It can be NULL if you dont want to fill it now
 * @param Size                  Size of the data inside UDP packet
 *
 * @return pointer to the allocated memory
 *
 * @see oC_Udp_Packet_Delete
 */
//==========================================================================================================================================
oC_Icmp_Packet_t* oC_Icmp_Packet_New( Allocator_t PacketAllocator , oC_Net_PacketType_t Type , oC_Icmp_Type_t IcmpType , uint8_t Code , const void * Data , uint16_t Size )
{
    oC_Icmp_Packet_t * packet        = NULL;
    uint16_t           packetSize    = Size + sizeof(oC_Icmp_Header_t);

    if(
        oC_SaveIfFalse("Icmp::PacketNew: "                           , oC_Module_IsTurnedOn(oC_Module_Icmp)              , oC_ErrorCode_ModuleNotStartedYet  )
     && oC_SaveIfFalse("Icmp::PacketNew: Packet Type incorrect - "   ,    Type == oC_Net_PacketType_IPv4
                                                                       || Type == oC_Net_PacketType_IPv6                 , oC_ErrorCode_TypeNotCorrect       )
     && oC_SaveIfFalse("Icmp::PacketNew: Size is not correct - "     , Size > 0                                          , oC_ErrorCode_SizeNotCorrect       )
     && oC_SaveIfFalse("Icmp::PacketNew: Data address incorrect - "  , Data == NULL || isaddresscorrect(Data)            , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("Icmp::PacketNew: Allocator incorrect - "     , isaddresscorrect(PacketAllocator)                 , oC_ErrorCode_AllocatorNotCorrect  )
        )
    {
        packet = (oC_Icmp_Packet_t*) oC_Net_Packet_New( PacketAllocator, Type, NULL, packetSize );

        if(oC_SaveIfFalse("Icmp::PacketNew: Cannot allocate memory for a packet - ", packet != NULL , oC_ErrorCode_AllocationError))
        {
            oC_Icmp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&packet->Packet);

            datagram->Header.Type = IcmpType;
            datagram->Header.Code = Code;

            if(Data != NULL)
            {
                void * dataDestination = datagram;

                dataDestination += sizeof(oC_Icmp_Header_t);

                memcpy(dataDestination,Data,Size);
            }
        }
    }

    return packet;
}

//==========================================================================================================================================
/**
 * @brief sets size of the packet
 */
//==========================================================================================================================================
bool oC_Icmp_Packet_SetSize( oC_Icmp_Packet_t * Packet , uint16_t Size )
{
    bool        success    = false;
    uint16_t    packetSize = Size + sizeof(oC_Icmp_Header_t);

    if(
        oC_SaveIfFalse("ICMP::SetSize - " , isram(Packet)       , oC_ErrorCode_AddressNotInRam    )
     && oC_SaveIfFalse("ICMP::SetSize - " , Size > 0            , oC_ErrorCode_SizeNotCorrect     )
     && oC_SaveIfFalse("ICMP::SetSize - " , packetSize > Size   , oC_ErrorCode_SizeTooBig         )
        )
    {
        success = oC_Net_Packet_SetSize(&Packet->Packet,packetSize);
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief release memory allocated for a ICMP packet
 */
//==========================================================================================================================================
bool oC_Icmp_Packet_Delete( oC_Icmp_Packet_t ** Packet )
{
    bool success = false;

    if(isram(Packet) && isram(*Packet))
    {
        success = kfree(*Packet,AllocationFlags_Default);
        *Packet = NULL;
    }

    return success;
}
//==========================================================================================================================================
/**
 * @brief returns reference to the message inside ICMP packet
 */
//==========================================================================================================================================
void * oC_Icmp_Packet_GetMessageReference( oC_Icmp_Packet_t * Packet )
{
    void *               data     = NULL;
    oC_Icmp_Datagram_t * datagram = oC_Net_Packet_GetDataReference(&Packet->Packet);

    if(isaddresscorrect(datagram))
    {
        data = &datagram->Message;
    }

    return data;
}

//==========================================================================================================================================
/**
 * @brief sends ICMP packet
 *
 * The function prepares ICMP packet and sends it to the recipient.
 *
 * @note
 * Don't reserve a ICMP type if you only want to send ICMP packets of a type. Reserving packets is a mechanism for receiving
 *
 * @param Netif             Pointer to the configured network interface or NULL if not used
 * @param Destination       Destination address where the packet should be sent
 * @param Packet            ICMP packet to send allocated by the function #oC_Icmp_Packet_New
 * @param Timeout           Maximum time for the send request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_NetifNotCorrect                 | `Netif` is not correct (address is not correct or not valid anymore) and it is not NULL
 *  oC_ErrorCode_AddressNotInRam                 | `Packet` does not point to the RAM section
 *  oC_ErrorCode_WrongAddress                    | `Destination` is not correct memory address
 *  oC_ErrorCode_IpAddressNotCorrect             | `Destination` stores IP address that is not correct (probably type is not set)
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *  oC_ErrorCode_PacketNotCorrect                | ICMP packet is not correct (probably IP type is not set correctly)
 *  oC_ErrorCode_TypeNotCorrect                  | ICMP packet type and destination address type are not the same
 *  oC_ErrorCode_CannotFindNetifForTheAddress    | Netif is not given (#NULL) but we cannot find any that can achieve the given address
 *  oC_ErrorCode_ModuleBusy                      | Module is busy (Maximum time for the request elapsed)
 *
 *  @see oC_Udp_Receive, oC_Udp_TurnOn, oC_Udp_ReservePort, oC_Udp_Packet_New
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_Send( oC_Netif_t Netif , const oC_Net_Address_t * Destination , oC_Icmp_Packet_t * Packet , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t timestamp    = oC_KTime_GetTimestamp();
    oC_Timestamp_t endTimestamp = timestamp + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Icmp))
    {
        if(
            ErrorCondition( Netif == NULL || oC_Netif_IsCorrect(Netif)              , oC_ErrorCode_NetifNotCorrect                  )
         && ErrorCondition( isaddresscorrect(Destination)                           , oC_ErrorCode_WrongAddress                     )
         && ErrorCondition( oC_Net_IsAddressCorrect(Destination)                    , oC_ErrorCode_IpAddressNotCorrect              )
         && ErrorCondition( isram(Packet)                                           , oC_ErrorCode_AddressNotInRam                  )
         && ErrorCondition( Timeout >= 0                                            , oC_ErrorCode_TimeNotCorrect                   )
            )
        {
            oC_Icmp_Datagram_t * datagram     = oC_Net_Packet_GetDataReference(&Packet->Packet);
            oC_Net_PacketType_t  packetType   = oC_Net_Packet_GetType((oC_Net_Packet_t*)Packet);
            oC_Net_AddressType_t addressType  = Destination->Type ;

            Netif = Netif == NULL ? oC_NetifMan_GetNetifForAddress(Destination) : Netif;

            if(
                ErrorCondition( oC_Netif_IsCorrect(Netif)                          , oC_ErrorCode_CannotFindNetifForTheAddress  )
             && ErrorCondition( datagram != NULL                                   , oC_ErrorCode_PacketNotCorrect              )
             && ErrorCondition( ((oC_Net_PacketType_t)addressType) == packetType   , oC_ErrorCode_TypeNotCorrect                )
                )
            {
                if(packetType == oC_Net_PacketType_IPv4)
                {
                    Packet->Packet.IPv4.Header.DF               = 0;
                    Packet->Packet.IPv4.Header.DestinationIp    = Destination->IPv4;
                    Packet->Packet.IPv4.Header.SourceIp         = oC_Netif_GetIpv4Address(Netif);
                    Packet->Packet.IPv4.Header.FragmentOffset   = 0;
                    Packet->Packet.IPv4.Header.ID               = 0;
                    Packet->Packet.IPv4.Header.IHL              = 5;
                    Packet->Packet.IPv4.Header.MF               = 0;
                    Packet->Packet.IPv4.Header.Protocol         = oC_Net_Protocol_ICMP;
                    Packet->Packet.IPv4.Header.QoS              = 0;
                    Packet->Packet.IPv4.Header.TTL              = 0xFF;
                    Packet->Packet.IPv4.Header.Checksum         = 0;

                }
                else
                {
                    memcpy(&Packet->Packet.IPv6.Header.Destination,&Destination->IPv6,sizeof(oC_Net_Ipv6_t));
                    oC_Netif_ReadIpv6Address(Netif,&Packet->Packet.IPv6.Header.Source);

                    oC_SaveError("UDP::Send - Function is not implemented for IPv6!", oC_ErrorCode_NotImplemented);
                }

                datagram->Header.Checksum = 0;
                datagram->Header.Checksum = CalculateChecksum(Packet);

                errorCode = oC_Netif_SendPacket( Netif, &Packet->Packet, oC_KTime_CalculateTimeout(endTimestamp) );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives ICMP packet
 *
 * The function is for receiving ICMP packets at the selected UDP type. The type has to be reserved for the current process, otherwise
 * receiving is not possible.
 *
 * @note
 * Remember that you have to reserve a ICMP type by using function #oC_Icmp_ReserveType before usage.
 *
 * @warning
 * Function will receive only packets that matches `Packet` type, so if the type of the `Packet` is set to IPv4, the function will receive
 * only packets that contains IPv4 address!
 *
 * @param Netif             Network interface to receive packet from or #NULL if not used
 * @param outPacket         Destination for a received packet.
 * @param Type              ICMP type of the message to receive the packet from. The type should be reserved before by calling a function #oC_Icmp_ReserveType
 * @param Timeout           Maximum time for the send request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | UDP module is not turned on. Please call #oC_Udp_TurnOn first
 *  oC_ErrorCode_NetifNotCorrect                 | `Netif` is not correct (address is not correct or not valid anymore) and it is not NULL
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` value is lower than 0!!
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outPacket` does not point to the RAM section
 *  oC_ErrorCode_TypeNotReserved                 | The given `Type` in `Packet` is not reserved by using the function #oC_Udp_ReservePort
 *  oC_ErrorCode_TypeReservedByDifferentProcess  | The given `Type` in `Packet` is already reserved by different process.
 *  oC_ErrorCode_TypeBusy                        | The given `Type` is busy (Maximum time for the request elapsed)
 *  oC_ErrorCode_ModuleBusy                      | Module is busy (Maximum time for the request elapsed)
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *
 *  @see oC_Icmp_Receive, oC_Icmp_TurnOn, oC_Icmp_ReserveType, oC_Icmp_Packet_New
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Icmp_Receive( oC_Netif_t Netif , oC_Icmp_Packet_t * outPacket , oC_Icmp_Type_t Type , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_Icmp))
    {
        if(
            ErrorCondition( Netif == NULL || oC_Netif_IsCorrect(Netif)                        , oC_ErrorCode_NetifNotCorrect          )
         && ErrorCondition( isram(outPacket)                                                  , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Timeout >= 0                                                      , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( oC_Mutex_Take(ModuleBusy,oC_KTime_CalculateTimeout(endTimestamp)) , oC_ErrorCode_ModuleBusy               )
            )
        {
            oC_Mutex_Give(ModuleBusy); // It is taken only to ensure, that there is not any important operation in progress

            ReservationData_t * reservation = FindReservation( Type , NULL );

            if(
                ErrorCondition( reservation != NULL                                                          , oC_ErrorCode_TypeNotReserved                  )
             && ErrorCondition( reservation->Process == getcurprocess()                                      , oC_ErrorCode_TypeReservedByDifferentProcess   )
             && ErrorCondition( oC_Mutex_Take( reservation->Busy , oC_KTime_CalculateTimeout(endTimestamp) ) , oC_ErrorCode_TypeBusy                         )
                )
            {
                oC_Netif_t        receivedNetif  = NULL;
                oC_Net_Address_t  addressFilter;

                memset(&addressFilter, 0 , sizeof(addressFilter));

                addressFilter.Protocol  = oC_Net_Protocol_ICMP;

                if( ErrorCode( oC_NetifMan_ReceivePacket(&addressFilter, &outPacket->Packet, oC_KTime_CalculateTimeout(endTimestamp), &receivedNetif, (oC_NetifMan_PacketFilterFunction_t)PacketFilterFunction, &Type) ) )
                {
                   errorCode = oC_ErrorCode_None;
                }

                oC_Mutex_Give(reservation->Busy);
            }

        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief finds reservation of port or process
 */
//==========================================================================================================================================
static ReservationData_t* FindReservation( oC_Icmp_Type_t Type , oC_Process_t Process )
{
    ReservationData_t * foundReservation = NULL;

    foreach(ReservedTypes,reservation)
    {
        if(
            (reservation->Type      == Type    && Type    != oC_Icmp_Type_Empty  )
         || (reservation->Process   == Process && Process != NULL                )
            )
        {
            foundReservation = reservation;
        }
    }

    return foundReservation;
}

//==========================================================================================================================================
/**
 * @brief calculates checksum for a packet
 */
//==========================================================================================================================================
static uint16_t CalculateChecksum( oC_Icmp_Packet_t * Packet )
{
    uint32_t              checksum       = 0;
    uint16_t              size           = oC_Net_GetPacketSize(&Packet->Packet,false);
    oC_Icmp_Datagram_t*   datagram       = oC_Net_Packet_GetDataReference(&Packet->Packet);
    uint16_t              savedChecksum  = datagram->Header.Checksum;

    datagram->Header.Checksum = 0;

    checksum = oC_Net_CalculateChecksum(datagram,size,0,false,true);

    datagram->Header.Checksum = savedChecksum;

    return (uint16_t)checksum;
}

//==========================================================================================================================================
/**
 * @brief checks if the packet that we received is that we expect
 */
//==========================================================================================================================================
static bool PacketFilterFunction( oC_Icmp_Packet_t * ReceivedPacket , oC_Icmp_Type_t * ExpectedType , oC_Netif_t Netif )
{
    bool expected = false;

    if(isram(ReceivedPacket) && isaddresscorrect(ExpectedType))
    {
        expected = ReceivedPacket->IPv4.IcmpDatagram.Header.Type == (*ExpectedType);
    }

    return expected;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

