/** ****************************************************************************************************************************************
 *
 * @brief      The file with sources for the DHCP module
 *
 * @file       oc_dhcp.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_dhcp.h>
#include <oc_netif.h>
#include <oc_debug.h>
#include <oc_memman.h>
#include <oc_math.h>
#include <oc_ktime.h>
#include <oc_udp.h>
#include <oc_system_cfg.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________

typedef enum
{
    MessageType_Discover    = 1 ,
    MessageType_Offer       = 2 ,
    MessageType_Request     = 3 ,
    MessageType_Declide     = 4 ,
    MessageType_Acknowledge = 5 ,
    MessageType_NAK         = 6 ,
    MessageType_Release     = 7 ,
    MessageType_Inform      = 8 ,
} MessageType_t;

#undef  _________________________________________LOCAL_TYPES_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static oC_Dhcp_HardwareType_t TranslateToDhcpHardwareType   ( oC_Net_HardwareType_t HardwareType );
static uint32_t               GetHardwareAddressLength      ( oC_Dhcp_HardwareType_t HardwareType );
static uint8_t*               AddOption                     ( uint8_t * OptionsArray , uint32_t * LeftBytes , oC_Dhcp_Option_t Option , oC_Dhcp_OptionLength_t Length , uint8_t * Data );
static bool                   FindNextOption                ( uint8_t * OptionsArray , uint32_t   LeftBytes , oC_Dhcp_Option_t * outOption , oC_Dhcp_OptionLength_t * outLength );
static uint8_t*               ReadOption                    ( uint8_t * OptionsArray , uint32_t * LeftBytes , uint8_t * outData );
static void                   ConvertToNetworkEndianess     ( oC_Dhcp_Message_t * Message );
static void                   ConvertFromNetworkEndianess   ( oC_Dhcp_Message_t * Message );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
/**
 * @addtogroup Dhcp
 * @{
 */

//==========================================================================================================================================
/**
 * @brief sends DHCP message to DHCP server
 *
 * The function sends the DHCP message to the DHCP server.
 *
 * @param Netif         Pointer to the configured network interface
 * @param Message       DHCP message to send
 * @param Size          Aligned size of the DHCP message
 * @param Timeout       Maximum time for the request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_WrongAddress                    | `Message` address is not correct
 *  oC_ErrorCode_NotConfiguredYet                | The `Netif` is not configured (#oC_Netif_Configure has not been called)
 *  oC_ErrorCode_AllocationError                 | Cannot allocate memory for a packet
 *  oC_ErrorCode_PortNotReserved                 | DHCP client UDP port is not reserved - please reserve it by using #oC_Udp_ReservePort interface
 *
 *  More error codes can be returned from the function #oC_Udp_Send
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Dhcp_SendMessage( oC_Netif_t Netif , oC_Dhcp_Message_t * Message , uint16_t Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)                                          , oC_ErrorCode_ObjectNotCorrect    )
     && ErrorCondition( isaddresscorrect(Message)                                          , oC_ErrorCode_WrongAddress        )
     && ErrorCondition( oC_Netif_IsConfigured(Netif)                                       , oC_ErrorCode_NotConfiguredYet    )
     && ErrorCondition( oC_Udp_IsPortReserved( oC_Udp_Port_BOOTPClient, getcurprocess() )  , oC_ErrorCode_PortNotReserved     )
        )
    {
        ConvertToNetworkEndianess(Message);

        oC_Udp_Packet_t * packet = oC_Udp_Packet_New( getcurallocator() , oC_Net_PacketType_IPv4, Message, Size);
        oC_Net_Ipv4Info_t info;

        ConvertFromNetworkEndianess(Message);

        bzero( &info, sizeof(info) );

        if(
            ErrorCondition  ( packet != NULL , oC_ErrorCode_AllocationError )
         && ErrorCode       ( oC_Netif_ReadIpv4Info( Netif, &info )         )
            )
        {
            oC_Net_Address_t destination = {
                            .Type       = oC_Net_AddressType_IPv4,
                            .IPv4       = info.DhcpIP,
                            .Protocol   = oC_Net_Protocol_UDP ,
                            .Port       = oC_Udp_Port_BOOTPServer
            };


            if( ErrorCode( oC_Udp_Send( Netif, oC_Udp_Port_BOOTPClient , &destination, packet, Timeout  ))   )
            {
                errorCode = oC_ErrorCode_None;
            }
        }

        oC_SaveIfFalse("DHCP::SendMessage - ", packet == NULL || oC_Udp_Packet_Delete(&packet), oC_ErrorCode_ReleaseError);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives DHCP message from DHCP server
 *
 * The function receives DHCP message from DHCP server.
 *
 * @param Netif         Pointer to the configured network interface
 * @param outMessage    Destination for the DHCP message
 * @param Timeout       Maximum time for the receive request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outMessage` address does not contain address in RAM section
 *  oC_ErrorCode_NotConfiguredYet                | The `Netif` is not configured (#oC_Netif_Configure has not been called)
 *  oC_ErrorCode_PortNotReserved                 | DHCP client UDP port is not reserved - please reserve it by using #oC_Udp_ReservePort interface
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Dhcp_ReceiveMessage( oC_Netif_t Netif , oC_Dhcp_Message_t * outMessage, oC_Net_Address_t* outSrc, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)                                          , oC_ErrorCode_ObjectNotCorrect          )
     && ErrorCondition( isram(outMessage)                                                  , oC_ErrorCode_OutputAddressNotInRAM     )
     && ErrorCondition( isram(outSrc) || NULL                                              , oC_ErrorCode_OutputAddressNotInRAM     )
     && ErrorCondition( oC_Netif_IsConfigured(Netif)                                       , oC_ErrorCode_NotConfiguredYet          )
     && ErrorCondition( oC_Udp_IsPortReserved( oC_Udp_Port_BOOTPClient, getcurprocess() )  , oC_ErrorCode_PortNotReserved           )
        )
    {
        oC_Udp_Packet_t * packet = oC_Udp_Packet_New( getcurallocator() , oC_Net_PacketType_IPv4, NULL, sizeof(oC_Dhcp_Message_t));

        if(
            ErrorCondition  ( packet != NULL    , oC_ErrorCode_AllocationError                          )
         && ErrorCode       ( oC_Udp_Receive( Netif, oC_Udp_Port_BOOTPClient, packet, Timeout )         )
         && ErrorCode       ( oC_Udp_Packet_ReadData( packet, outMessage, sizeof(oC_Dhcp_Message_t))    )
            )
        {
            if(outSrc != NULL)
            {
                outSrc->IPv4 = packet->IPv4.Header.SourceIp;
                outSrc->Type = oC_Net_PacketType_IPv4;
                outSrc->Port = packet->IPv4.UdpDatagram.Header.SourcePort;
                outSrc->Protocol = packet->IPv4.Header.Protocol;
            }
            ConvertFromNetworkEndianess(outMessage);
            errorCode = oC_ErrorCode_None;
        }
        oC_SaveIfFalse("DHCP::ReceiveMessage - ", packet == NULL || oC_Udp_Packet_Delete(&packet), oC_ErrorCode_ReleaseError);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief performs DHCP IP request procedure
 *
 * The function is for performing DHCP request IP procedure. It can be called only by the root user.
 *
 * @param Netif         Pointer to the configured network interface
 * @param Timeout       Maximum time for the request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_NetifNotCorrect                 | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The `Netif` is not configured (#oC_Netif_Configure has not been called)
 *  oC_ErrorCode_LinkNotDetected                 | The `Netif` list state is not UP
 *  oC_ErrorCode_TimeNotCorrect                  | Timeout is smaller than 0!
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *  oC_ErrorCode_PermissionDenied                | You are not root user!
 *
 *  @see oC_Dhcp_ReceiveOffer
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Dhcp_RequestIp( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;
    oC_Udp_Port_t  udpPort      = oC_Udp_Port_BOOTPClient;

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif )                             , oC_ErrorCode_NetifNotCorrect          )
     && ErrorCondition( oC_Netif_IsConfigured(Netif)                            , oC_ErrorCode_NotConfiguredYet         )
     && ErrorCondition( oC_Netif_GetLinkStatus(Netif) == oC_Net_LinkStatus_Up   , oC_ErrorCode_LinkNotDetected          )
     && ErrorCondition( Timeout >= 0                                            , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( iscurroot()                                             , oC_ErrorCode_PermissionDenied         )
     && ErrorCode     ( oC_Udp_ReservePort(&udpPort, oC_KTime_CalculateTimeout(endTimestamp))                           )
        )
    {
        if(
            ErrorCode     ( oC_Dhcp_SendDiscovery        ( Netif, Timeout )                                                 )
         && ErrorCode     ( oC_Dhcp_ReceiveOffer         ( Netif, Timeout )                                                 )
         && ErrorCode     ( oC_Dhcp_SendRequest          ( Netif, Timeout )                                                 )
         && ErrorCode     ( oC_Dhcp_ReceiveAcknowledge   ( Netif, Timeout )                                                 )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        ErrorCode( oC_Udp_ReleasePort( udpPort, oC_KTime_CalculateTimeout(endTimestamp) ));
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends DHCP discovery message to DHCP server
 *
 * The function sends DHCP discovery message to the DHCP server, which begins the IP reservation process. The next function that should be
 * called after this one is #oC_Dhcp_ReceiveOffer.
 *
 * @param Netif         Pointer to the configured network interface
 * @param Timeout       Maximum time for the receive request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_UnknownHardwareType             | `Netif` is an network interface with unknown or unsupported hardware type.
 *  oC_ErrorCode_NotConfiguredYet                | The `Netif` is not configured (#oC_Netif_Configure has not been called)
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *
 *  @see oC_Dhcp_ReceiveOffer
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Dhcp_SendDiscovery( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode    = oC_ErrorCode_ImplementError;
    oC_Dhcp_HardwareType_t  hardwareType = TranslateToDhcpHardwareType( oC_Netif_GetHardwareType(Netif) );

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif )     , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( hardwareType != 0               , oC_ErrorCode_UnknownHardwareType      )
     && ErrorCondition( oC_Netif_IsConfigured(Netif)    , oC_ErrorCode_NotConfiguredYet         )
        )
    {
        oC_Net_Ipv4Info_t           info;
        oC_Dhcp_Message_t*          message     = kmalloc(sizeof(oC_Dhcp_Message_t), getcurallocator(), AllocationFlags_ZeroFill);
        oC_Net_HardwareAddress_t    hwAddress;

        bzero( &info,       sizeof( info    )   );
        bzero( &hwAddress,  sizeof( hwAddress ) );

        if(
            ErrorCondition  ( message != NULL , oC_ErrorCode_AllocationError     )
         && ErrorCode       ( oC_Netif_ReadIpv4Info       ( Netif, &info       ) )
         && ErrorCode       ( oC_Netif_ReadHardwareAddress( Netif, &hwAddress  ) )
            )
        {
            uint8_t*        arrayEndPointer = &message->Options[0];
            uint32_t        leftBytes       = sizeof(message->Options);
            oC_Net_Ipv4_t   requestedIp     = info.IP;
            MessageType_t   messageType     = MessageType_Discover;
            uint8_t         clientId[7]     = {0};
            uint8_t         requestedList[] = {
                oC_Dhcp_Option_SubnetMask   ,
                oC_Dhcp_Option_Router       ,
                oC_Dhcp_Option_Broadcast    ,
                oC_Dhcp_Option_DnsServer    ,
                oC_Dhcp_Option_DefaultTTL   ,
                oC_Dhcp_Option_MTU          ,
                oC_Dhcp_Option_Ntp          ,
                oC_Dhcp_Option_LeaseTime    ,
            };

            info.DhcpIP         = IP(255,255,255,255);
//            info.IP             = IP(0,0,0,0);
            info.BroadcastIP    = IP(255,255,255,255);
            info.NetIP          = IP(0,0,0,0);
            info.Netmask        = IP(0,0,0,0);

            if(ErrorCode( oC_Netif_SetIpv4Info(Netif,&info) ))
            {
                //=====================================================================
                //              Preparing default message parameters                 //
                //=====================================================================
                message->Operation               = oC_Dhcp_OperationCode_Request;
                message->HardwareType            = hardwareType;
                message->HardwareAddressLength   = GetHardwareAddressLength(hardwareType);
                message->Hops                    = 0;
                message->XID                     = (uint32_t)Netif;
                message->Secs                    = 0;
                message->Flags                   = oC_Dhcp_Flags_Broadcast;
                message->MagicCookie             = oC_DHCP_MAGIC_COOKIE;
                message->Options[0]              = oC_DHCP_END_OPTION;

                memcpy(&message->CHAddr,&hwAddress,message->HardwareAddressLength);
                memcpy(&clientId[1], &hwAddress, message->HardwareAddressLength);

                clientId[0] = hardwareType;

                //=====================================================================
                //                  Adding options to the message                    //
                //=====================================================================

                /* Add information about the message type - it is marking that this *
                 * message stores `Discover` message                                */
                arrayEndPointer = AddOption(arrayEndPointer,&leftBytes,oC_Dhcp_Option_MessageType,oC_Dhcp_OptionLength_MessageType, (uint8_t*)&messageType);

                /* Add MAC address as Client ID                                     */
                arrayEndPointer = AddOption(arrayEndPointer,&leftBytes,oC_Dhcp_Option_ClientId,sizeof(clientId), (uint8_t*)&clientId);

                /* Add MAC address as Client ID                                     */
                arrayEndPointer = AddOption(arrayEndPointer,&leftBytes,oC_Dhcp_Option_Hostname,sizeof(CFG_STRING_HOSTNAME), (uint8_t*)CFG_STRING_HOSTNAME);

                /* If we already have an assigned IP, we want to request it again   */
                if(requestedIp != 0)
                {
                    arrayEndPointer = AddOption(arrayEndPointer,&leftBytes,oC_Dhcp_Option_RequestedIp,oC_Dhcp_OptionLength_RequestedIp,(uint8_t*)&requestedIp);
                }

                /* This adds list of parameters that we want to request.            */
                arrayEndPointer = AddOption(arrayEndPointer,&leftBytes,oC_Dhcp_Option_RequestedList,sizeof(requestedList),(uint8_t*)&requestedList);

                /* counting size of the message */
                uint16_t size = sizeof(oC_Dhcp_Message_t) - leftBytes + 1;

                /* Trailer */
                while((size & 3) && size < sizeof(oC_Dhcp_Message_t))
                {
                    size++;
                }

                //=====================================================================
                //              Sending message via Network Interface                //
                //=====================================================================
                kdebuglog( oC_LogType_Info, "Sending DHCP Discovery ... \n" );
                errorCode = oC_Dhcp_SendMessage(Netif, message, size, Timeout);
            }
        }

        oC_SaveIfFalse( "DHCP::Discovery - cannot release message memory - ", message == NULL || kfree(message,0), oC_ErrorCode_ReleaseError );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives offer from the DHCP server
 *
 * The function allows to receive an DHCP offer from the DHCP server. The function #oC_Dhcp_SendDiscovery should be called before this one.
 *
 * @warning
 * When more than 2 threads tries to receive an offer at the same time, from the same network interface, they can disturb each other, as
 * this function reads all messages transfered by the given `Netif`, and it throws out the not matched messages.
 *
 * @param Netif         Pointer to the configured network interface
 * @param Timeout       Maximum time for the receive request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The `Netif` is not configured (#oC_Netif_Configure has not been called)
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *
 *  @see oC_Dhcp_SendDiscovery
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Dhcp_ReceiveOffer( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif )     , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( oC_Netif_IsConfigured(Netif)    , oC_ErrorCode_NotConfiguredYet         )
        )
    {
        oC_Net_Ipv4Info_t   info   ;
        oC_Dhcp_Message_t * message = malloc(sizeof(oC_Dhcp_Message_t), AllocationFlags_ZeroFill);

        bzero( &info,       sizeof( info    )   );

        if(
            ErrorCondition  ( message != NULL , oC_ErrorCode_AllocationError      )
         && ErrorCode       ( oC_Netif_ReadIpv4Info ( Netif, &info              ) )
            )
        {
            oC_Timestamp_t      startTime    = oC_KTime_GetTimestamp();
            oC_Timestamp_t      endTime      = startTime + Timeout;
            oC_Timestamp_t      currentTime  = oC_KTime_GetTimestamp();
            MessageType_t       messageType  = 0;
            oC_Net_Ipv4Info_t   tempInfo;
            oC_Net_Address_t    serverAddress;
            oC_Net_HardwareAddress_t routerAddress;

            memset(&serverAddress, 0, sizeof(serverAddress));
            memset(&routerAddress, 0, sizeof(routerAddress));

            //=====================================================================
            //      Looking offer response message from the network interface    //
            //=====================================================================

            /*            Receive next message from network interface            */
            while(ErrorCode(oC_Dhcp_ReceiveMessage(Netif,message, &serverAddress,Timeout)))
            {

                if( ErrorCode( oC_Netif_ReadHardwareAddressOf(Netif, &routerAddress, &serverAddress, gettimeout(endTime)) ))
                {
                    oC_DefaultString_t hwAddrStr = {0};
                    oC_Net_HardwareAddressToString(&routerAddress, oC_Net_HardwareType_Ethernet, hwAddrStr, sizeof(hwAddrStr));
                    memcpy(&info.HardwareRouterAddress, &routerAddress, sizeof(info.HardwareRouterAddress));
                    kdebuglog(oC_LogType_Info, "Received HW Router address: %s", hwAddrStr);
                }
                kdebuglog(oC_LogType_Info, "Received DHCP message...\n");
                uint32_t                leftBytes                           = sizeof(message->Options);
                uint8_t*                options                             = message->Options;
                oC_Dhcp_Option_t        option                              = 0;
                oC_Dhcp_OptionLength_t  length                              = 0;
                uint8_t                 data[oC_DHCP_OPTIONS_BUFFER_SIZE];

                messageType = 0;

                memcpy(&tempInfo,&info,sizeof(tempInfo));

                tempInfo.DhcpIP     = message->SIAddr;
                tempInfo.GatewayIP  = message->GIAddr;
                tempInfo.IP         = message->YIAddr;

                if(tempInfo.DhcpIP == 0)
                {
                    tempInfo.DhcpIP = serverAddress.IPv4;
                }

                memcpy(tempInfo.DhcpServerName, message->SName, sizeof(tempInfo.DhcpServerName));

                if(
                    message->Operation == oC_Dhcp_OperationCode_Reply
                 && message->XID       == ((uint32_t)Netif)
                    )
                {
                    //=====================================================================
                    //                  Reading options from the message                 //
                    //=====================================================================

                    /*              Checking if next option is available                 */
                    while(FindNextOption(options,leftBytes,&option,&length))
                    {
                        memset(data,0,sizeof(data));

                        /*                  Read option from the message                     */
                        options = ReadOption(options,&leftBytes,data);

                        //=====================================================================
                        //                        Parsing option                             //
                        //=====================================================================
                        if(option == oC_Dhcp_Option_MessageType)
                        {
                            memcpy(&messageType, data, sizeof(messageType));
                        }
                        else if(option == oC_Dhcp_Option_SubnetMask)
                        {
                            memcpy(&tempInfo.Netmask, data, sizeof(tempInfo.Netmask));
                            tempInfo.Netmask = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.Netmask);
                        }
                        else if(option == oC_Dhcp_Option_Router)
                        {
                            memcpy(&tempInfo.NetIP, data, sizeof(tempInfo.NetIP));
                            tempInfo.NetIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.NetIP);
                        }
                        else if(option == oC_Dhcp_Option_Broadcast)
                        {
                            memcpy(&tempInfo.BroadcastIP, data, sizeof(tempInfo.BroadcastIP));
                            tempInfo.BroadcastIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.BroadcastIP);
                        }
                        else if(option == oC_Dhcp_Option_DnsServer)
                        {
                            memcpy(&tempInfo.DnsIP, data, sizeof(tempInfo.DnsIP));
                            tempInfo.DnsIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.DnsIP);
                        }
                        else if(option == oC_Dhcp_Option_DefaultTTL)
                        {
                            memcpy(&tempInfo.DefaultTTL, data, sizeof(tempInfo.DefaultTTL));
                        }
                        else if(option == oC_Dhcp_Option_MTU)
                        {
                            memcpy(&tempInfo.MTU, data, sizeof(tempInfo.MTU));
                            tempInfo.MTU = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.MTU);
                        }
                        else if(option == oC_Dhcp_Option_Ntp)
                        {
                            memcpy(&tempInfo.NtpIP, data, sizeof(tempInfo.NtpIP));
                            tempInfo.NtpIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.NtpIP);
                        }
                        else if(option == oC_Dhcp_Option_LeaseTime)
                        {
                            uint32_t leaseTime = 0;

                            memcpy(&leaseTime, data, sizeof(uint32_t));

                            leaseTime = oC_Net_ConvertUint32FromNetworkEndianess(leaseTime);

                            tempInfo.LeaseTime = (oC_Time_t)leaseTime;
                            tempInfo.IpExpiredTimestamp = oC_KTime_GetTimestamp() + tempInfo.LeaseTime;
                        }
                    }
                }

                memcpy(&info,&tempInfo,sizeof(info));
                oC_DefaultString_t ipStr = {0};
                oC_Net_Ipv4AddressToString(info.IP, ipStr, sizeof(ipStr));

                /* If this is the offer message, we can use it, otherwise we have to *
                 * wait for the next message                                         */
                if(messageType == MessageType_Offer)
                {
                    kdebuglog( oC_LogType_Info, "Received DHCP Offer with IP: %s\n", ipStr );
                    break;
                }
                else if(messageType == MessageType_NAK)
                {
                    kdebuglog(oC_LogType_Warning, "DHCP NAK received. IP: %s\n", ipStr);
                    break;
                }

                //=====================================================================
                //                        Parsing option                             //
                //=====================================================================
                currentTime = oC_KTime_GetTimestamp();

                if(currentTime <= endTime)
                {
                    Timeout = endTime - currentTime;
                }
                else
                {
                    errorCode = oC_ErrorCode_Timeout;
                    break;
                }
            }


            //=====================================================================
            //      If we've received message, we can use data of it             //
            //=====================================================================
            if(messageType == MessageType_Offer)
            {
                info.StaticIP = false;
                errorCode = oC_Netif_SetIpv4Info(Netif,&info);
            }
            else if(messageType == MessageType_NAK)
            {
                info.IP = IP(0,0,0,0);
                info.StaticIP = false;
                kdebuglog(oC_LogType_Warning, "DHCP: Clearing the IP due to the NAK response\n");
                errorCode = oC_Netif_SetIpv4Info(Netif,&info);
            }
        }
        oC_SaveIfFalse("DHCP::Offer - cannot release message memory - ", message == NULL || free(message,0) , oC_ErrorCode_ReleaseError);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends request message to the DHCP server
 *
 * The function sends DHCP requet message to the DHCP server.
 *
 * @param Netif         Pointer to the configured network interface
 * @param Timeout       Maximum time for the request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_UnknownHardwareType             | Type of the network interface hardware is not supported or not correct
 *  oC_ErrorCode_NotConfiguredYet                | The `Netif` is not configured (#oC_Netif_Configure has not been called)
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *
 *  @see oC_Dhcp_SendDiscovery
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Dhcp_SendRequest( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode    = oC_ErrorCode_ImplementError;
    oC_Dhcp_HardwareType_t  hardwareType = TranslateToDhcpHardwareType( oC_Netif_GetHardwareType(Netif) );

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif )     , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( hardwareType != 0               , oC_ErrorCode_UnknownHardwareType      )
     && ErrorCondition( oC_Netif_IsConfigured(Netif)    , oC_ErrorCode_NotConfiguredYet         )
        )
    {
        oC_Net_Ipv4Info_t           info;
        oC_Dhcp_Message_t *         message     = malloc( sizeof(oC_Dhcp_Message_t) , AllocationFlags_ZeroFill );
        oC_Net_HardwareAddress_t    hwAddress;

        bzero( &info,       sizeof( info    )   );
        bzero( &hwAddress,  sizeof( hwAddress ) );

        if(
            ErrorCondition( message != NULL , oC_ErrorCode_AllocationError )
         && ErrorCode( oC_Netif_ReadIpv4Info       ( Netif, &info       )  )
         && ErrorCode( oC_Netif_ReadHardwareAddress( Netif, &hwAddress  )  )
            )
        {
            uint8_t*        arrayEndPointer = &message->Options[0];
            uint32_t        leftBytes       = sizeof(message->Options);
            oC_Net_Ipv4_t   dhcpIp          = oC_Net_ConvertUint32ToNetworkEndianess(info.DhcpIP);
            oC_Net_Ipv4_t   requestedIp     = oC_Net_ConvertUint32ToNetworkEndianess(info.IP);
            MessageType_t   messageType     = MessageType_Request;

            if(ErrorCode( oC_Netif_SetIpv4Info(Netif,&info) ))
            {
                //=====================================================================
                //              Preparing default message parameters                 //
                //=====================================================================
                message->Operation               = oC_Dhcp_OperationCode_Request;
                message->HardwareType            = hardwareType;
                message->HardwareAddressLength   = GetHardwareAddressLength(hardwareType);
                message->Hops                    = 0;
                message->XID                     = (uint32_t)Netif;
                message->Secs                    = 0;
                message->Flags                   = oC_Dhcp_Flags_Broadcast;
                message->MagicCookie             = oC_DHCP_MAGIC_COOKIE;
                message->Options[0]              = oC_DHCP_END_OPTION;
                message->SIAddr                  = info.DhcpIP;  // This will be converted before send by the 'SendMessage' function
                message->CIAddr                  = info.IP;      // This will be converted before send by the 'SendMessage' function

                memcpy(&message->CHAddr,&hwAddress,message->HardwareAddressLength);

                //=====================================================================
                //                  Adding options to the message                    //
                //=====================================================================

                /* Add information about the message type - it is marking that this *
                 * message stores `Discover` message                                */
                arrayEndPointer = AddOption(arrayEndPointer, &leftBytes, oC_Dhcp_Option_MessageType , oC_Dhcp_OptionLength_MessageType  , (uint8_t*)&messageType   );
                arrayEndPointer = AddOption(arrayEndPointer, &leftBytes, oC_Dhcp_Option_RequestedIp , oC_Dhcp_OptionLength_RequestedIp  , (uint8_t*)&requestedIp   );
                arrayEndPointer = AddOption(arrayEndPointer, &leftBytes, oC_Dhcp_Option_ServerId    , oC_Dhcp_OptionLength_ServerId     , (uint8_t*)&dhcpIp        );
                arrayEndPointer = AddOption(arrayEndPointer, &leftBytes, oC_Dhcp_Option_Hostname    , sizeof(CFG_STRING_HOSTNAME)       , (uint8_t*)CFG_STRING_HOSTNAME);

                /* counting size of the message */
                uint16_t size = sizeof(oC_Dhcp_Message_t) - leftBytes + 1;

                /* Trailer */
                while((size & 3) && size < sizeof(oC_Dhcp_Message_t))
                {
                    size++;
                }

                //=====================================================================
                //              Sending message via Network Interface                //
                //=====================================================================
                oC_DefaultString_t ipStr = {0};
                oC_Net_Ipv4AddressToString(info.IP, ipStr, sizeof(ipStr));
                kdebuglog( oC_LogType_Info, "Sending DHCP Request for IP: %s\n", ipStr );
                errorCode = oC_Dhcp_SendMessage(Netif,message,size,Timeout);
            }
        }

        oC_SaveIfFalse("DHCP::Request - cannot release message memory - ", message == NULL || free(message,0) , oC_ErrorCode_ReleaseError);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives ACK message from DHCP server
 *
 * The function allows to receive an DHCP acknowledge from the DHCP server. The function #oC_Dhcp_SendRequest should be called before this one.
 *
 * @warning
 * When more than 2 threads tries to receive an acknowledge at the same time, from the same network interface, they can disturb each other, as
 * this function reads all messages transfered by the given `Netif`, and it throws out the not matched messages.
 *
 * @param Netif         Pointer to the configured network interface
 * @param Timeout       Maximum time for the receive request
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The `Netif` is not configured (#oC_Netif_Configure has not been called)
 *  oC_ErrorCode_Timeout                         | Maximum time for the request elapsed
 *
 *  @see oC_Dhcp_SendRequest
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Dhcp_ReceiveAcknowledge( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif )     , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( oC_Netif_IsConfigured(Netif)    , oC_ErrorCode_NotConfiguredYet         )
        )
    {
        oC_Net_Ipv4Info_t   info   ;
        oC_Dhcp_Message_t * message = malloc(sizeof(oC_Dhcp_Message_t) , AllocationFlags_ZeroFill);

        bzero( &info,       sizeof( info    )   );

        if(
            ErrorCondition  ( message != NULL , oC_ErrorCode_AllocationError      )
         && ErrorCode       ( oC_Netif_ReadIpv4Info ( Netif, &info              ) )
            )
        {
            oC_Timestamp_t      startTime    = oC_KTime_GetTimestamp();
            oC_Timestamp_t      endTime      = startTime + Timeout;
            oC_Timestamp_t      currentTime  = oC_KTime_GetTimestamp();
            MessageType_t       messageType  = 0;
            oC_Net_Ipv4Info_t   tempInfo;
            oC_Net_Address_t    serverAddress = {0};

            //=====================================================================
            //  Looking acknowledge response message from the network interface    //
            //=====================================================================
            kdebuglog(oC_LogType_Info, "Waiting for DHCP ACK");
            /*            Receive next message from network interface            */
            while(ErrorCode(oC_Dhcp_ReceiveMessage(Netif,message,&serverAddress,Timeout)))
            {
                kdebuglog(oC_LogType_Info, "DHCP message received\n");
                uint32_t                leftBytes                           = sizeof(message->Options);
                uint8_t*                options                             = message->Options;
                oC_Dhcp_Option_t        option                              = 0;
                oC_Dhcp_OptionLength_t  length                              = 0;
                uint8_t                 data[oC_DHCP_OPTIONS_BUFFER_SIZE];

                messageType = 0;

                memcpy(&tempInfo,&info,sizeof(tempInfo));

                if(
                    message->Operation == oC_Dhcp_OperationCode_Reply
                 && message->XID       == ((uint32_t)Netif)
                    )
                {
                    //=====================================================================
                    //                  Reading options from the message                 //
                    //=====================================================================

                    /*              Checking if next option is available                 */
                    while(FindNextOption(options,leftBytes,&option,&length))
                    {
                        memset(data,0,sizeof(data));

                        /*                  Read option from the message                     */
                        options = ReadOption(options,&leftBytes,data);

                        //=====================================================================
                        //                        Parsing option                             //
                        //=====================================================================
                        if(option == oC_Dhcp_Option_MessageType)
                        {
                            memcpy(&messageType, data, sizeof(messageType));
                        }
                        else if(option == oC_Dhcp_Option_SubnetMask)
                        {
                            memcpy(&tempInfo.Netmask, data, sizeof(tempInfo.Netmask));
                            tempInfo.Netmask = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.Netmask);
                        }
                        else if(option == oC_Dhcp_Option_Router)
                        {
                            memcpy(&tempInfo.NetIP, data, sizeof(tempInfo.NetIP));
                            tempInfo.NetIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.NetIP);
                        }
                        else if(option == oC_Dhcp_Option_Broadcast)
                        {
                            memcpy(&tempInfo.BroadcastIP, data, sizeof(tempInfo.BroadcastIP));
                            tempInfo.BroadcastIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.BroadcastIP);
                        }
                        else if(option == oC_Dhcp_Option_DnsServer)
                        {
                            memcpy(&tempInfo.DnsIP, data, sizeof(tempInfo.DnsIP));
                            tempInfo.DnsIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.DnsIP);
                        }
                        else if(option == oC_Dhcp_Option_DefaultTTL)
                        {
                            memcpy(&tempInfo.DefaultTTL, data, sizeof(tempInfo.DefaultTTL));
                        }
                        else if(option == oC_Dhcp_Option_MTU)
                        {
                            memcpy(&tempInfo.MTU, data, sizeof(tempInfo.MTU));
                            tempInfo.MTU = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.MTU);
                        }
                        else if(option == oC_Dhcp_Option_Ntp)
                        {
                            memcpy(&tempInfo.NtpIP, data, sizeof(tempInfo.NtpIP));
                            tempInfo.NtpIP = oC_Net_ConvertUint32FromNetworkEndianess(tempInfo.NtpIP);
                        }
                        else if(option == oC_Dhcp_Option_LeaseTime)
                        {
                            uint32_t leaseTime = 0;

                            memcpy(&leaseTime, data, sizeof(uint32_t));

                            leaseTime = oC_Net_ConvertUint32FromNetworkEndianess(leaseTime);

                            tempInfo.LeaseTime          = (oC_Time_t)leaseTime;
                            tempInfo.IpExpiredTimestamp = oC_KTime_GetTimestamp() + (oC_Timestamp_t)tempInfo.LeaseTime;
                        }
                    }
                }

                /* If this is the offer message, we can use it, otherwise we have to *
                 * wait for the next message                                         */
                if(messageType == MessageType_Acknowledge || messageType == MessageType_NAK)
                {
                    oC_DefaultString_t ipStr = {0};
                    oC_DefaultString_t dhcpIpStr = {0};
                    oC_Net_Ipv4AddressToString(info.IP, ipStr, sizeof(ipStr));
                    oC_Net_Ipv4AddressToString(info.DhcpIP, dhcpIpStr, sizeof(dhcpIpStr));
                    kdebuglog(oC_LogType_Info, "Received ack for DHCP request. IP: %s DhcpIP: %s\n", ipStr, dhcpIpStr);
                    memcpy(&info,&tempInfo,sizeof(info));
                    info.IP         = message->YIAddr;
                    info.DhcpIP     = message->SIAddr;
                    break;
                }

                //=====================================================================
                //                        Parsing option                             //
                //=====================================================================
                currentTime = oC_KTime_GetTimestamp();

                if(currentTime <= endTime)
                {
                    Timeout = endTime - currentTime;
                }
                else
                {
                    errorCode = oC_ErrorCode_Timeout;
                    break;
                }
            }

            //=====================================================================
            //      If we've received message, we can use data of it             //
            //=====================================================================
            if(messageType == MessageType_Acknowledge || messageType == MessageType_NAK)
            {
                oC_Net_Address_t routerAddress = {
                                .Type = oC_Net_AddressType_IPv4 ,
                                .IPv4 = info.NetIP
                };
                /* We don't check this status to provide work of local network also in case of failure */
                oC_SaveIfErrorOccur("DHCP::Cannot read HW address of the router!\n",oC_Netif_ReadHardwareAddressOf(Netif,&info.HardwareRouterAddress,&routerAddress,oC_KTime_CalculateTimeout(endTime)));

                errorCode = oC_Netif_SetIpv4Info(Netif,&info);
            }
        }

        oC_SaveIfFalse("DHCP::Acknowledge - cannot release message memory - ", message == NULL || free(message,0) , oC_ErrorCode_ReleaseError);
    }

    return errorCode;
}

/** @} */
#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION___________________________________________________________________

//==========================================================================================================================================
/**
 * @brief translates #oC_Net_HardwareType_t to #oC_Dhcp_HardwareType_t
 */
//==========================================================================================================================================
static oC_Dhcp_HardwareType_t TranslateToDhcpHardwareType( oC_Net_HardwareType_t HardwareType )
{
    oC_Dhcp_HardwareType_t result = 0;

    if(HardwareType == oC_Net_HardwareType_Ethernet)
    {
        result = oC_Dhcp_HardwareType_Ethernet;
    }
    else
    {
        oC_SaveError("Dhcp::TranslateToDhcpHardwareType: unsupported hardware type - %d\n", HardwareType);
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief returns number of bytes required for storing hardware address
 */
//==========================================================================================================================================
static uint32_t GetHardwareAddressLength( oC_Dhcp_HardwareType_t HardwareType )
{
    uint32_t length = 0;

    if(HardwareType == oC_Dhcp_HardwareType_Ethernet)
    {
        length = sizeof(oC_Net_MacAddress_t);
    }
    else
    {
        kdebuglog(oC_LogType_Error , "Dhcp::GetHardwareAddressLength: unsupported hardware type - %d\n", HardwareType);
    }

    return length;
}

//==========================================================================================================================================
/**
 * @brief adds option to the options array
 *
 * The function adds DHCP option do the options array.
 *
 * @param OptionsArray      Pointer to the options array
 * @param LeftBytes         Pointer to the variable that stores number of bytes that left in the options array. It is updated by the function after
 * @param Option            Option to add
 * @param Length            Length of the option
 * @param Data              Optional (can be #NULL) pointer to the data that should be added after this option
 *
 * @return new pointer to the end of the array
 */
//==========================================================================================================================================
static uint8_t* AddOption( uint8_t * OptionsArray , uint32_t * LeftBytes , oC_Dhcp_Option_t Option , oC_Dhcp_OptionLength_t Length , uint8_t * Data )
{
    uint32_t  realOptionSize = Length + 2;

    if((*LeftBytes) >= (realOptionSize + 1))
    {
        OptionsArray[0] = (uint8_t)Option;
        OptionsArray[1] = Length;

        if(Data != NULL)
        {
            for(uint32_t i = 2; i < realOptionSize; i++ )
            {
                OptionsArray[i] = Data[i-2];
            }
        }
        OptionsArray[realOptionSize] = oC_Dhcp_Option_End;
        (*LeftBytes)  = (*LeftBytes) - realOptionSize;
        OptionsArray += realOptionSize;
    }
    else
    {
        kdebuglog(oC_LogType_Error, "Dhcp::AddOption - Cannot add option %d (length %d) - there is no more space in array\n", Option, Length);
    }

    return OptionsArray;
}

//==========================================================================================================================================
/**
 * @brief checks if is there any more option in the options array
 */
//==========================================================================================================================================
static bool FindNextOption( uint8_t * OptionsArray , uint32_t LeftBytes , oC_Dhcp_Option_t * outOption , oC_Dhcp_OptionLength_t * outLength )
{
    bool nextOptionFound = false;

    if(LeftBytes >= 3 && OptionsArray[0] != oC_Dhcp_Option_End)
    {
        *outOption = (oC_Dhcp_Option_t)       OptionsArray[0];
        *outLength = (oC_Dhcp_OptionLength_t) OptionsArray[1];

        nextOptionFound = true;
    }

    return nextOptionFound;
}

//==========================================================================================================================================
/**
 * @brief reads option in the Options array and returns pointer to the next option
 */
//==========================================================================================================================================
static uint8_t* ReadOption( uint8_t * OptionsArray , uint32_t * LeftBytes , uint8_t * outData )
{
    oC_Dhcp_Option_t        option          = 0;
    oC_Dhcp_OptionLength_t  length          = 0;
    uint32_t                realOptionSize  = 2;

    if(FindNextOption(OptionsArray,*LeftBytes,&option,&length))
    {
        realOptionSize += length;

        if(outData != NULL)
        {
            for(uint32_t i = 2;i < realOptionSize; i++)
            {
                outData[i-2] = OptionsArray[i];
            }
        }

        OptionsArray = &OptionsArray[realOptionSize];
        *LeftBytes  -= realOptionSize;
    }

    return OptionsArray;
}

//==========================================================================================================================================
/**
 * @brief converts message to network endianess
 */
//==========================================================================================================================================
static void ConvertToNetworkEndianess( oC_Dhcp_Message_t * Message )
{
    Message->CIAddr      = oC_Net_ConvertUint32ToNetworkEndianess(Message->CIAddr);
    Message->YIAddr      = oC_Net_ConvertUint32ToNetworkEndianess(Message->YIAddr);
    Message->SIAddr      = oC_Net_ConvertUint32ToNetworkEndianess(Message->SIAddr);
    Message->GIAddr      = oC_Net_ConvertUint32ToNetworkEndianess(Message->GIAddr);
    Message->MagicCookie = oC_Net_ConvertUint32ToNetworkEndianess(Message->MagicCookie);
}
//==========================================================================================================================================
/**
 * @brief converts message from network endianess
 */
//==========================================================================================================================================
static void ConvertFromNetworkEndianess( oC_Dhcp_Message_t * Message )
{
    Message->CIAddr      = oC_Net_ConvertUint32FromNetworkEndianess(Message->CIAddr);
    Message->YIAddr      = oC_Net_ConvertUint32FromNetworkEndianess(Message->YIAddr);
    Message->SIAddr      = oC_Net_ConvertUint32FromNetworkEndianess(Message->SIAddr);
    Message->GIAddr      = oC_Net_ConvertUint32FromNetworkEndianess(Message->GIAddr);
    Message->MagicCookie = oC_Net_ConvertUint32FromNetworkEndianess(Message->MagicCookie);
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION___________________________________________________________________

