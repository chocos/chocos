/** ****************************************************************************************************************************************
 *
 * @brief      The file with functions of TELNET
 * 
 * @file       oc_telnet.c
 *
 * @author     Patryk Kubiak - (Created on: 16.01.2017 21:32:17) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_telnet.h>
#include <oc_tcp.h>
#include <oc_thread.h>
#include <oc_list.h>
#include <oc_service.h>
#include <oc_dynamic_config.h>
#include <oc_process.h>
#include <oc_program.h>
#include <oc_programman.h>
#include <oc_intman.h>
#include <oc_mutex.h>
#include <oc_semaphore.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define MODULE_NAME         "Telnet"

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores options of the telnet
 */
//==========================================================================================================================================
typedef enum
{
    Option_Echo                 = 1 ,
    Option_Reconnection         = 2 ,
    Option_GoAhead              = 3 ,
    Option_Status               = 5,
    Option_TimingMark           = 6 ,
    Option_TerminalType         = 24 ,
    Option_WindowSize           = 31 ,
    Option_TerminalSpeed        = 32 ,
    Option_RemoteFlowControl    = 33 ,
    Option_LineMode             = 34 ,
    Option_EnvironmentVariables = 36 ,
    Option_NewEnvironmentOption = 39 ,

    Option_Count
} Option_t;

//==========================================================================================================================================
/**
 * @brief stores commands for Telnet
 */
//==========================================================================================================================================
typedef enum
{
    Command_IAC         = 255 ,
} Command_t;

//==========================================================================================================================================
/**
 * @brief stores operation codes
 */
//==========================================================================================================================================
typedef enum
{
    Operation_None              = 0,
    Operation_SE                = 240 ,
    Operation_NOP               = 241 ,
    Operation_DataMark          = 242 ,
    Operation_Break             = 243 ,
    Operation_Interrupt         = 244 ,
    Operation_AbortOutput       = 245 ,
    Operation_AreYouThere       = 246 ,
    Operation_EraseCharacter    = 247 ,
    Operation_EraseLine         = 248 ,
    Operation_GoAhead           = 249 ,
    Operation_SB                = 250 ,
    Operation_Will              = 251 ,
    Operation_Wont              = 252 ,
    Operation_Do                = 253 ,
    Operation_Dont              = 254 ,
    Operation_IAC               = 255 ,
} Operation_t;

//==========================================================================================================================================
/**
 * @brief stores connection role
 */
//==========================================================================================================================================
typedef enum
{
    ConnectionRole_Server, /**< Server */
    ConnectionRole_Client, /**< Client */
    ConnectionRole_Count   /**< Number of entries */
} ConnectionRole_t;

//==========================================================================================================================================
/**
 * @brief context of the Telnet Service
 *
 * The object stores informations required for handling Telnet Service
 */
//==========================================================================================================================================
typedef struct Context_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_Tcp_Server_t         Server;
} * ServiceContext_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef struct
{
    uint16_t        Width;
    uint16_t        Height;
} WindowSize_t;

//==========================================================================================================================================
//==========================================================================================================================================
typedef union
{
    WindowSize_t    WindowSize;
    uint8_t         Generic[20];
} TelnetOptionData_t;

//==========================================================================================================================================
/**
 * @brief stores telnet confituration
 */
//==========================================================================================================================================
typedef bool TelnetConfiguration_t[ConnectionRole_Count][Option_Count];

//==========================================================================================================================================
/**
 * @brief context of the Telnet connection
 *
 * The object stores informations required for handling Telnet TCP connection
 */
//==========================================================================================================================================
typedef struct ConnectionContext_t
{
    oC_ObjectControl_t      ObjectControl;
    const char *            Name;
    oC_Tcp_Connection_t     Connection;
    oC_Process_t            Process;
    oC_Stream_t             Stream;
    TelnetConfiguration_t   Configuration;

    struct
    {
        oC_Timestamp_t          InactiveTimestamp;
        uint8_t *               Buffer;
        oC_MemorySize_t         Size;
        oC_MemorySize_t         FilledSize;
        oC_Thread_t             Thread;
        oC_Mutex_t              BusyMutex;
        oC_Semaphore_t          CountingSemaphore;
    } NagleAlgorithm;
} * ConnectionContext_t;

//==========================================================================================================================================
/**
 * @brief configuration for a virtual driver
 */
//==========================================================================================================================================
typedef struct
{
    oC_Tcp_Connection_t     Connection;
    oC_Process_t            Process;
    oC_Tcp_Server_t         Server;
} VirtualDriverConfig_t;

//==========================================================================================================================================
/**
 * @brief stores Telnet option data
 */
//==========================================================================================================================================
typedef struct
{
    uint8_t         Command;
    uint8_t         Operation;
    uint8_t         Option;
} TelnetOptionParser_t;

//==========================================================================================================================================
/**
 * @brief stores default configuration of the telnet protocol
 */
//==========================================================================================================================================
static const TelnetConfiguration_t DefaultConfiguration = {
    [ConnectionRole_Server] = {
         [Option_Echo]              = true,
         [Option_GoAhead]           = true,
    },
    [ConnectionRole_Client] = {
         [Option_Echo]              = true,
         [Option_GoAhead]           = true,
         [Option_WindowSize]        = false,
    },
};


#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool                 IsConnectionContextCorrect  ( ConnectionContext_t  Context );
static ConnectionContext_t  ConnectionContext_New       ( oC_Tcp_Connection_t  Connection , oC_Process_t Process );
static bool                 ConnectionContext_Delete    ( ConnectionContext_t  Context );
static bool                 ServiceContext_IsCorrect    ( ServiceContext_t     Context );
static ServiceContext_t     ServiceContext_New          ( void );
static bool                 ServiceContext_Delete       ( ServiceContext_t Context );
static oC_ErrorCode_t       StartService                ( ServiceContext_t * outContext );
static oC_ErrorCode_t       StopService                 ( ServiceContext_t *    Context );
static oC_ErrorCode_t       RunProgramForConnection     ( oC_Tcp_Connection_t Connection, oC_Tcp_Server_t Server );
static oC_ErrorCode_t       ServiceThread               ( ServiceContext_t Context );
static oC_ErrorCode_t       Configure                   ( VirtualDriverConfig_t * Config , ConnectionContext_t * outContext );
static oC_ErrorCode_t       Unconfigure                 ( void * Dummy                   , ConnectionContext_t *    Context );
static oC_ErrorCode_t       Write                       ( ConnectionContext_t Context , const char *    Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
static oC_ErrorCode_t       Read                        ( ConnectionContext_t Context ,       char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout );
static oC_ErrorCode_t       HandleIoctl                 ( ConnectionContext_t Context , oC_Ioctl_Command_t Command , void * Data );
static bool                 ConnectionFinished          ( oC_Tcp_Connection_t Connection , void * Context, void* EventParameter );
static void                 MainThreadFinished          ( oC_Thread_t Thread , void * Connection );
static oC_ErrorCode_t       SaveDataInNagleBuffer       ( ConnectionContext_t Context , const char * Data, oC_MemorySize_t * Size , oC_Time_t Timeout );
static oC_ErrorCode_t       Fflush                      ( ConnectionContext_t Context , oC_Time_t Timeout );
static void                 NagleThread                 ( void * Context );
static bool                 IsOptionEnabled             ( ConnectionContext_t Context, ConnectionRole_t ConnectionRole, Option_t Option );
static oC_ErrorCode_t       SetOptionEnabled            ( ConnectionContext_t Context, ConnectionRole_t ConnectionRole, Option_t Option, bool Enabled );
static const char*          GetOptionName               ( Option_t Option );
static const char*          GetOperationName            ( Operation_t Operation );
static oC_ErrorCode_t       ParseIac                    ( ConnectionContext_t Context, void* Buffer, oC_MemorySize_t* Size, oC_Time_t Timeout );
static oC_ErrorCode_t       ParseOptions                ( ConnectionContext_t Context, TelnetOptionParser_t* Options, oC_MemorySize_t ArraySize, oC_Time_t Timeout );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with globals
 *
 *  ======================================================================================================================================*/
#define _________________________________________GLOBALS_SECTION____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief registration of the Telnet service
 */
//==========================================================================================================================================
const oC_Service_Registration_t Telnet = {
                .Name                   = MODULE_NAME ,
                .StartFunction          = StartService ,
                .StopFunction           = StopService ,
                .MainFunction           = ServiceThread ,
                .MainThreadStackSize    = B(4096),
                .HeapMapSize            = 0 ,
                .AllocationLimit        = 0 ,
                .RequiredModules        = { oC_Module_Tcp , oC_Module_ProcessMan , oC_Module_ThreadMan , oC_Module_NetifMan } ,
                .Priority               = oC_Process_Priority_NetworkHandlerProcess ,
};
//==========================================================================================================================================
/**
 * @brief registration of the Telnet dummy driver
 */
//==========================================================================================================================================
static const oC_Driver_Registration_t  DriverRegistration = {
                .FileName           = MODULE_NAME,
                .ConfigurationSize  = sizeof(oC_Tcp_Connection_t),
                .Configure          = (oC_Driver_ConfigureFunction_t)  Configure ,
                .Unconfigure        = (oC_Driver_UnconfigureFunction_t)Unconfigure ,
                .Write              = (oC_Driver_WriteFunction_t)      Write ,
                .Read               = (oC_Driver_ReadFunction_t)       Read ,
                .HandleIoctl        = (oC_Driver_HandleIoctlFunction_t)HandleIoctl,
};

//==========================================================================================================================================
/**
 * @brief array with names for the options
 */
//==========================================================================================================================================
static const char* OptionNames[Option_Count] = {
        [Option_Echo                ] = "Echo",
        [Option_Reconnection        ] = "Reconnection",
        [Option_GoAhead             ] = "SuppressGoAhead",
        [Option_Status              ] = "Status",
        [Option_TimingMark          ] = "TimingMark",
        [Option_TerminalType        ] = "TerminalType",
        [Option_WindowSize          ] = "WindowSize",
        [Option_TerminalSpeed       ] = "TerminalSpeed",
        [Option_RemoteFlowControl   ] = "RemoteFlowControl",
        [Option_LineMode            ] = "LineMode",
        [Option_EnvironmentVariables] = "EnvironmentVariables",
        [Option_NewEnvironmentOption] = "New Environment Option",
};

//==========================================================================================================================================
/**
 * @brief list of supported configuration options
 */
//==========================================================================================================================================


#undef  _________________________________________GLOBALS_SECTION____________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________


#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the telnet connection context is correct
 */
//==========================================================================================================================================
static bool IsConnectionContextCorrect( ConnectionContext_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_TelnetConnectionContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief allocates memory for the telnet connection context
 */
//==========================================================================================================================================
static ConnectionContext_t ConnectionContext_New( oC_Tcp_Connection_t  Connection , oC_Process_t Process )
{
    ConnectionContext_t context   = kmalloc( sizeof(struct ConnectionContext_t), oC_Process_GetAllocator(Process), AllocationFlags_ZeroFill );
    Allocator_t         allocator = oC_Process_GetAllocator(Process);

    if(oC_SaveIfFalse("context", context != NULL, oC_ErrorCode_AllocationError ))
    {
        context->Connection     = Connection;
        context->ObjectControl  = oC_CountObjectControl(context,oC_ObjectId_TelnetConnectionContext);
        context->Stream         = NULL;
        context->Process        = Process;
        context->Name           = oC_Tcp_Connection_GetName(Connection);

        memcpy(context->Configuration, DefaultConfiguration, sizeof(context->Configuration));

        /* Nagle's Algorithm */
        context->NagleAlgorithm.Size                = oC_DynamicConfig_GetValue(Telnet, NagleBufferSize);
        context->NagleAlgorithm.Buffer              = kmalloc( context->NagleAlgorithm.Size, allocator, AllocationFlags_ZeroFill );
        context->NagleAlgorithm.FilledSize          = 0;
        context->NagleAlgorithm.Thread              = oC_Thread_New(1, oC_DynamicConfig_GetValue(Telnet, NagleThreadStackSize), Process, context->Name, NagleThread, context);
        context->NagleAlgorithm.InactiveTimestamp   = oC_DynamicConfig_GetValue(Telnet, NagleMaximumInactiveTime) + gettimestamp();
        context->NagleAlgorithm.BusyMutex           = oC_Mutex_New( oC_Mutex_Type_Normal, allocator, 0 );
        context->NagleAlgorithm.CountingSemaphore   = oC_Semaphore_New(context->NagleAlgorithm.Size,context->NagleAlgorithm.Size, allocator, 0 );

        if(
            oC_SaveIfFalse("Thread", context->NagleAlgorithm.Thread             != NULL      , oC_ErrorCode_AllocationError  )
         && oC_SaveIfFalse("Size"  , context->NagleAlgorithm.Size                > 0         , oC_ErrorCode_SizeNotCorrect   )
         && oC_SaveIfFalse("Buffer", context->NagleAlgorithm.Buffer             != NULL      , oC_ErrorCode_AllocationError  )
         && oC_SaveIfFalse("Mutex" , context->NagleAlgorithm.BusyMutex          != NULL      , oC_ErrorCode_AllocationError  )
         && oC_SaveIfFalse("Semaph", context->NagleAlgorithm.CountingSemaphore  != NULL      , oC_ErrorCode_AllocationError  )
         && oC_SaveIfFalse("Thread", oC_Thread_Run(context->NagleAlgorithm.Thread)           , oC_ErrorCode_CannotRunThread  )
            )
        {
            telnetlog( oC_LogType_Track, "Connection context for '%s' has been allocated", context->Name );
        }
        else
        {
            telnetlog( oC_LogType_Error, "Cannot allocate context for '%s'", context->Name );

            oC_SaveIfFalse( "thread" , oC_Thread_Delete(&context->NagleAlgorithm.Thread)                , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "mutex"  , oC_Mutex_Delete (&context->NagleAlgorithm.BusyMutex,0)           , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "semaph" , oC_Semaphore_Delete(&context->NagleAlgorithm.CountingSemaphore,0), oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "buffer" , kfree(context->NagleAlgorithm.Buffer,0)                          , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "context", kfree(context,0)                                                 , oC_ErrorCode_ReleaseError );

            context = NULL;
        }
    }
    else
    {
        telnetlog( oC_LogType_Error, "Cannot allocate memory for context");
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases memory of the telnet connection context
 */
//==========================================================================================================================================
static bool ConnectionContext_Delete( ConnectionContext_t  Context )
{
    bool deleted = false;

    oC_IntMan_EnterCriticalSection();

    if( oC_SaveIfFalse("Context" , IsConnectionContextCorrect( Context ) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        Context->ObjectControl = 0;

        bool thread  = oC_SaveIfFalse( "thread" , oC_Thread_Delete(&Context->NagleAlgorithm.Thread)                , oC_ErrorCode_ReleaseError );
        bool mutex   = oC_SaveIfFalse( "mutex"  , oC_Mutex_Delete (&Context->NagleAlgorithm.BusyMutex,0)           , oC_ErrorCode_ReleaseError );
        bool semaph  = oC_SaveIfFalse( "semaph" , oC_Semaphore_Delete(&Context->NagleAlgorithm.CountingSemaphore,0), oC_ErrorCode_ReleaseError );
        bool buffer  = oC_SaveIfFalse( "buffer" , kfree(Context->NagleAlgorithm.Buffer,0)                          , oC_ErrorCode_ReleaseError );
        bool context = oC_SaveIfFalse( "context", kfree(Context,0)                                                 , oC_ErrorCode_ReleaseError );

        deleted = thread && mutex && semaph && buffer && context;
    }

    oC_IntMan_ExitCriticalSection();

    return deleted;
}
//==========================================================================================================================================
/**
 * @brief checks if the telnet context is correct
 */
//==========================================================================================================================================
static bool ServiceContext_IsCorrect( ServiceContext_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_TelnetServiceContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a service context
 */
//==========================================================================================================================================
static ServiceContext_t ServiceContext_New( void )
{
    ServiceContext_t context = malloc( sizeof(struct Context_t), AllocationFlags_ZeroFill );

    if(oC_SaveIfFalse("Telnet::ServiceContext_New: ", context != NULL, oC_ErrorCode_AllocationError ))
    {
        context->ObjectControl  = oC_CountObjectControl(context,oC_ObjectId_TelnetServiceContext);
        context->Server         = NULL;
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases memory of a service context
 */
//==========================================================================================================================================
static bool ServiceContext_Delete( ServiceContext_t Context )
{
    bool deleted = false;

    if(ServiceContext_IsCorrect(Context))
    {
        bool serverDeleted = Context->Server == NULL || oC_Tcp_Server_Delete(&Context->Server, oC_DynamicConfig_GetValue(Telnet,StopServerTimeout));

        Context->ObjectControl = 0;

        deleted = free( Context, AllocationFlags_Default ) && serverDeleted;
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief prepares Telnet service to work
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartService( ServiceContext_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition( isram(outContext), oC_ErrorCode_OutputAddressNotInRAM ))
    {
        *outContext = ServiceContext_New();

        if(ErrorCondition( (*outContext) != NULL, oC_ErrorCode_AllocationError ))
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases Telnet service resources
 */
//==========================================================================================================================================
static oC_ErrorCode_t StopService( ServiceContext_t *    Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(Context)                      , oC_ErrorCode_AddressNotInRam    )
     && ErrorCondition( ServiceContext_IsCorrect(*Context)  , oC_ErrorCode_ObjectNotCorrect   )
        )
    {
        if(ErrorCondition( ServiceContext_Delete(*Context), oC_ErrorCode_ReleaseError ))
        {
            *Context  = NULL;
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief runs default program for specific TCP connection
 */
//==========================================================================================================================================
static oC_ErrorCode_t RunProgramForConnection( oC_Tcp_Connection_t Connection , oC_Tcp_Server_t Server )
{
    oC_ErrorCode_t          errorCode = oC_ErrorCode_ImplementError;
    oC_Stream_t             stream    = NULL;
    VirtualDriverConfig_t   config    = {
                    .Connection     = Connection,
                    .Server         = Server,
    };

    if(ErrorCondition( oC_Tcp_Connection_IsCorrect(Connection), oC_ErrorCode_ObjectNotCorrect ))
    {
        oC_Program_t program     = oC_ProgramMan_GetTerminalProgram();
        oC_Process_t process     = oC_Program_NewProcess( program, getrootuser() );
        oC_Thread_t  thread      = NULL;
        const char * programName = oC_Program_GetName(program);
        int argc = 2;
        const char * args[2]     = {
                     programName,
                     "--textlogin"
        };
        if(
            ErrorCondition( program != NULL && programName != NULL                   , oC_ErrorCode_ProgramNotCorrect )
         && ErrorCondition( process != NULL                                          , oC_ErrorCode_AllocationError   )
            )
        {
            config.Process = process;

            stream = oC_Stream_New( oC_Process_GetAllocator(process),
                                    AllocationFlags_Default,
                                    oC_Stream_Type_Input | oC_Stream_Type_Output,
                                    MODULE_NAME,
                                    &DriverRegistration,
                                    &config
                                    );

            if( ErrorCondition( stream != NULL, oC_ErrorCode_AllocationError ) )
            {
                ConnectionContext_t connectionContext = oC_Stream_GetDriverContext(stream);
                if(ErrorCondition( connectionContext != NULL, oC_ErrorCode_ContextNotCorrect ))
                {
                    oC_IoFlags_t ioFlags = oC_Process_GetIoFlags(process);
                    if(IsOptionEnabled(connectionContext, ConnectionRole_Server, Option_Echo))
                    {
                        ioFlags |= oC_IoFlags_EchoWhenRead;
                    }
                    else
                    {
                        ioFlags &= ~oC_IoFlags_EchoWhenRead;
                    }
                    if( ErrorCode( oC_Process_SetIoFlags(process, ioFlags)                  )
                     && ErrorCode( oC_Process_SetOutputStream(process,stream)               )
                     && ErrorCode( oC_Process_SetInputStream(process,stream)                )
                     && ErrorCode( oC_Program_Exec(program,process,argc,args,&thread,NULL)  )
                        )
                    {
                        oC_SaveIfFalse("Cannot set Thread Finished Function",
                                       oC_Thread_SetFinishedFunction(thread,MainThreadFinished, Connection),
                                       oC_ErrorCode_ThreadNotCorrect );
                        errorCode = oC_ErrorCode_None;
                    }
                }
                if(oC_ErrorOccur(errorCode))
                {
                    oC_SaveIfFalse("Telnet - cannot delete stream ", oC_Stream_Delete(&stream), oC_ErrorCode_ReleaseError );
                }
            }
            if(oC_ErrorOccur(errorCode))
            {
                oC_SaveIfFalse("Telnet - cannot delete process ", oC_Process_Delete(&process), oC_ErrorCode_ReleaseError );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief main Telnet service thread
 */
//==========================================================================================================================================
static oC_ErrorCode_t ServiceThread( ServiceContext_t Context )
{
    oC_ErrorCode_t      errorCode        = oC_ErrorCode_ImplementError;
    bool                serviceActive    = true;
    oC_Tcp_Connection_t connection       = NULL;
    uint32_t            maxConnections   = oC_DynamicConfig_GetValue( Telnet, MaxConnections );
    oC_Time_t           startTimeout     = oC_DynamicConfig_GetValue( Telnet, StartServerTimeout );
    oC_Net_Address_t    source           = {
                    .Port       = oC_DynamicConfig_GetValue(Telnet, TcpPort),
                    .Type       = oC_Net_AddressType_IPv4 ,
                    .Protocol   = oC_Net_Protocol_TCP ,
                    .IPv4       = 0 ,
    };

    if(
        ErrorCondition( ServiceContext_IsCorrect(Context), oC_ErrorCode_ContextNotCorrect         )
     && ErrorCode     ( oC_Tcp_Listen( &source, &Context->Server, maxConnections, startTimeout )  )
        )
    {
        telnetlog( oC_LogType_Info, "Telnet server has been started" );

        while(serviceActive)
        {
            if(ErrorCode( oC_Tcp_Accept(Context->Server,&connection, day(365)) ))
            {
                if(ErrorCode( RunProgramForConnection(connection,Context->Server) ))
                {
                    connection = NULL;
                    telnetlog( oC_LogType_Info, "Telnet connection has been accepted" );
                }
                else
                {
                    oC_SaveIfErrorOccur("Disconnecting after failure", oC_Tcp_Connection_Disconnect(connection, oC_DynamicConfig_GetValue(Telnet,DisconnectTimeout)));
                }
            }
            else
            {
                telnetlog(oC_LogType_Error, "Cannot accept telnet connection: %R", errorCode);
                serviceActive = errorCode == oC_ErrorCode_Timeout;
            }
        }
        telnetlog( oC_LogType_Warning, "Telnet server has been closed" );

        oC_SaveError("Telnet::ServiceThread: ", errorCode);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function for establishing telnet connection
 */
//==========================================================================================================================================
static oC_ErrorCode_t EstablishTelnetConnection( ConnectionContext_t Context , oC_Tcp_Connection_t Connection)
{
    oC_ErrorCode_t          errorCode   = oC_ErrorCode_ImplementError;
    TelnetOptionParser_t    options[50] = {{0}};
    oC_MemorySize_t size = sizeof(options);

    if(ErrorCode( oC_Tcp_Receive(Connection,options,&size,ms(100)) ))
    {
        oC_MemorySize_t size = sizeof(options);
        errorCode = ParseIac(Context, options, &size, s(3));
    }
    else if(errorCode == oC_ErrorCode_Timeout)
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures TELNET connection to work
 */
//==========================================================================================================================================
static oC_ErrorCode_t Configure( VirtualDriverConfig_t * Config , ConnectionContext_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Config)             , oC_ErrorCode_WrongConfigAddress    )
     && ErrorCondition( isram(outContext)                    , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( isram(outContext)                    , oC_ErrorCode_AddressNotInRam       )
                    )
    {
        *outContext = ConnectionContext_New( Config->Connection, Config->Process );
        oC_Tcp_Connection_Callback_t callback = {
                                              .Function  = ConnectionFinished,
                                              .Parameter = *outContext
        };

        if(
            ErrorCondition( (*outContext) != NULL                             , oC_ErrorCode_AllocationError     )
         && ErrorCode     ( oC_Tcp_Connection_SetCallback(Config->Connection, oC_Tcp_CallbackGroup_User, oC_Tcp_EventId_ConnectionFinished, &callback)  )
            )
        {
            errorCode = EstablishTelnetConnection(*outContext, Config->Connection);
        }
        else
        {
            oC_SaveIfFalse("Context", ConnectionContext_Delete(*outContext), oC_ErrorCode_ReleaseError);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures virtual telnet driver
 *
 * The function is for deconfiguration of the virtual telnet driver (closing TCP connection). It should disconnect TCP connection and delete
 * connection object.
 *
 * @param Dummy         This parameter CANNOT BE USED - it can points to different configuration, that we have received during call of `Configure` function
 * @param Context       Context of the virtual driver
 *
 * @return code of error or `oC_ErrorCode_None` if not used
 */
//==========================================================================================================================================
static oC_ErrorCode_t Unconfigure( void * Dummy, ConnectionContext_t *    Context )
{
    oC_ErrorCode_t errorCode                = oC_ErrorCode_ImplementError;
    oC_Time_t      closingConnectionTimeout = oC_DynamicConfig_GetValue( Telnet, DisconnectTimeout );

    if( ErrorCondition( IsConnectionContextCorrect(*Context)            , oC_ErrorCode_ContextNotCorrect   ) )
    {
        errorCode = oC_ErrorCode_None;
        ErrorCode(      oC_Tcp_Disconnect( &((*Context)->Connection), closingConnectionTimeout )           );
        ErrorCondition( oC_Stream_Delete(  &((*Context)->Stream))       , oC_ErrorCode_ReleaseError         );
        ErrorCondition( ConnectionContext_Delete(*Context)              , oC_ErrorCode_ReleaseError        );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function called when process want to write data on STDOUT stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t Write( ConnectionContext_t Context , const char *    Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( IsConnectionContextCorrect(Context)  , oC_ErrorCode_ContextNotCorrect   )
     && ErrorCondition( isaddresscorrect(Buffer)             , oC_ErrorCode_WrongAddress        )
     && ErrorCondition( isram(Size)                          , oC_ErrorCode_AddressNotInRam     )
     && ErrorCondition( (*Size) > 0                          , oC_ErrorCode_SizeNotCorrect      )
     && ErrorCondition( Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect      )
                    )
    {
        if(!ErrorCondition(oC_Tcp_Connection_IsConnected(Context->Connection), oC_ErrorCode_NotConnected))
        {
            ConnectionFinished(Context->Connection, Context, NULL);
        }
        else if(gettimestamp() >= Context->NagleAlgorithm.InactiveTimestamp)
        {
            telnetlog(oC_LogType_Warning, "%s: Nagle's thread seems to be inactive. Fflushing and sending data by our own", Context->Name);

            if(
                ErrorCode( Fflush(Context,gettimeout(endTimestamp))                                  )
             && ErrorCode( oC_Tcp_Send(Context->Connection, Buffer, *Size, gettimeout(endTimestamp)) )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
        else
        {
            if( ErrorCode( SaveDataInNagleBuffer(Context,Buffer,Size,gettimeout(endTimestamp)) ) )
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveError("Cannot save data in Nagle's Buffer", errorCode);
                telnetlog( oC_LogType_Error, "%s: Cannot save data in Nagle's buffer - %R, sending data without buffering", Context->Name, errorCode );

                if(
                    ErrorCode( Fflush(Context,gettimeout(endTimestamp))                                  )
                 && ErrorCode( oC_Tcp_Send(Context->Connection, Buffer, *Size, gettimeout(endTimestamp)) )
                    )
                {
                    errorCode = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function called when process want to read data from STDIN stream
 */
//==========================================================================================================================================
static oC_ErrorCode_t Read( ConnectionContext_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    if(
        ErrorCondition( IsConnectionContextCorrect(Context)  , oC_ErrorCode_ContextNotCorrect     )
     && ErrorCondition( isram(outBuffer)                     , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( (*Size) > 0                          , oC_ErrorCode_SizeNotCorrect        )
     && ErrorCondition( Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect        )
                                )
    {
        if(!ErrorCondition(oC_Tcp_Connection_IsConnected(Context->Connection), oC_ErrorCode_NotConnected))
        {
            ConnectionFinished(Context->Connection, Context, NULL);
        }
        else if( ErrorCode( oC_Tcp_Receive(Context->Connection, outBuffer, Size, Timeout) ) )
        {
            errorCode = ParseIac(Context, outBuffer, Size, Timeout);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief called when handling of IOCTL is required
 */
//==========================================================================================================================================
oC_ErrorCode_t HandleIoctl( ConnectionContext_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    if(ErrorCondition( IsConnectionContextCorrect(Context)  , oC_ErrorCode_ContextNotCorrect     ))
    {
        if(!ErrorCondition(oC_Tcp_Connection_IsConnected(Context->Connection), oC_ErrorCode_NotConnected))
        {
            ConnectionFinished(Context->Connection, Context, NULL);
        }
        else
        {
            switch(Command)
            {
                case oC_IoCtl_SpecialCommand_ClearRxFifo:
                    errorCode = oC_ErrorCode_None;
                    break;
                default:
                    errorCode = oC_ErrorCode_NotHandledByDriver;
                    break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function called when the connection has been finished
 */
//==========================================================================================================================================
static bool ConnectionFinished( oC_Tcp_Connection_t Connection , void * Context, void* EventParameter )
{
    ConnectionContext_t context = Context;

    telnetlog(oC_LogType_Track, "%s: TCP Connection has finished, killing process and unconfigure the stream", oC_Tcp_Connection_GetName(Connection));

    oC_SaveIfFalse("Context"   , IsConnectionContextCorrect(Context)    , oC_ErrorCode_ObjectNotCorrect );
    oC_SaveIfFalse("Process", oC_Process_Kill(context->Process), oC_ErrorCode_CannotKillProcess);
    oC_SaveIfErrorOccur("Unconfigure", Unconfigure(NULL,&context));

    return true;
}

//==========================================================================================================================================
/**
 * @brief function called when the main thread is finished
 */
//==========================================================================================================================================
static void MainThreadFinished( oC_Thread_t Thread , void * Connection )
{
    oC_Tcp_Connection_t connection = Connection;

    telnetlog(oC_LogType_Track, "%s: Main thread (%s) has finished", oC_Tcp_Connection_GetName(connection), oC_Thread_GetName(Thread));

    if(
        oC_SaveIfFalse( "Thread"    , oC_Thread_IsCorrect(Thread)            , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "Connection", oC_Tcp_Connection_IsCorrect(connection), oC_ErrorCode_ObjectNotCorrect )
        )
    {
        if(oC_SaveIfFalse("Interrupts", oC_IntMan_AreInterruptsTurnedOn() , oC_ErrorCode_InterruptsNotEnabled))
        {
            static const char * string = "Closing connection with ChocoOS - the main thread has finished";
            oC_Tcp_Connection_Send(Connection,string,strlen(string) + 1, oC_DynamicConfig_GetValue(Telnet,DisconnectTimeout));
        }
        oC_SaveIfErrorOccur("TCP disconnect", oC_Tcp_Connection_Disconnect(connection,oC_DynamicConfig_GetValue(Telnet,DisconnectTimeout)));
    }
}

//==========================================================================================================================================
/**
 * @brief saves data in Nagle's Buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t SaveDataInNagleBuffer( ConnectionContext_t Context , const char * Data, oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if(
        ErrorCondition( oC_Semaphore_TakeCounting(Context->NagleAlgorithm.CountingSemaphore, *Size, gettimeout(endTimestamp)), oC_ErrorCode_Timeout )
     && ErrorCondition( oC_Mutex_Take(Context->NagleAlgorithm.BusyMutex, gettimeout(endTimestamp))                           , oC_ErrorCode_Timeout )
        )
    {
        uint8_t *           buffer      = &Context->NagleAlgorithm.Buffer[ Context->NagleAlgorithm.FilledSize ];
        oC_MemorySize_t     leftSize    = Context->NagleAlgorithm.Size - Context->NagleAlgorithm.FilledSize;
        oC_MemorySize_t     sizeToCopy  = oC_MIN(leftSize,*Size);

        memcpy( buffer, Data, sizeToCopy );

        Context->NagleAlgorithm.FilledSize += sizeToCopy;
        *Size                               = sizeToCopy;
        errorCode                           = oC_ErrorCode_None;

        oC_Mutex_Give(Context->NagleAlgorithm.BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief flushes data from Nagle's buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t Fflush( ConnectionContext_t Context , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = gettimestamp() + Timeout;

    if( ErrorCondition( oC_Mutex_Take(Context->NagleAlgorithm.BusyMutex, gettimeout(endTimestamp)) , oC_ErrorCode_Timeout ) )
    {
        if(Context->NagleAlgorithm.FilledSize > 0)
        {
            if( ErrorCode( oC_Tcp_Send(Context->Connection, Context->NagleAlgorithm.Buffer, Context->NagleAlgorithm.FilledSize, gettimeout(endTimestamp)) ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }

        memset(Context->NagleAlgorithm.Buffer, 0, Context->NagleAlgorithm.Size);
        Context->NagleAlgorithm.FilledSize = 0;
        oC_Mutex_Give(Context->NagleAlgorithm.BusyMutex);
        oC_Semaphore_GiveCounting(Context->NagleAlgorithm.CountingSemaphore, Context->NagleAlgorithm.Size);
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief thread for handling Nagle's buffering
 */
//==========================================================================================================================================
static void NagleThread( void * Context )
{
    ConnectionContext_t context     = Context;
    oC_ErrorCode_t      errorCode   = oC_ErrorCode_ImplementError;

    telnetlog( oC_LogType_Info, "Telnet Nagle's thread has been started" );

    if(
        oC_SaveIfFalse("ConnectionContext", IsConnectionContextCorrect(context), oC_ErrorCode_ObjectNotCorrect )
        )
    {
        while(oC_Tcp_Connection_IsConnected(context->Connection))
        {
            context->NagleAlgorithm.InactiveTimestamp = timeouttotimestamp(oC_DynamicConfig_GetValue(Telnet, NagleMaximumInactiveTime));
            errorCode = Fflush(context,oC_DynamicConfig_GetValue(Telnet,NagleSendingTimeout));

            if(errorCode == oC_ErrorCode_Timeout)
            {
                telnetlog( oC_LogType_Warning, "%s: Cannot fflush data, mutex busy", context->Name);
            }
            else if(!oC_SaveIfErrorOccur( "Fflush", errorCode ))
            {
                telnetlog( oC_LogType_Error, "%s: Cannot fflush data. Error - %R", context->Name, errorCode );
                break;
            }
            sleep(oC_DynamicConfig_GetValue(Telnet,NaglePeriod));
        }
    }

    telnetlog( oC_LogType_Warning, "Telnet Nagle's thread has been finished" );
}

//==========================================================================================================================================
/**
 * @brief Checks if the given telnet option is enabled
 *
 * @param Context       Context of the connection
 * @param Option        Option to check
 *
 * @return true if enabled
 */
//==========================================================================================================================================
static bool IsOptionEnabled( ConnectionContext_t Context, ConnectionRole_t ConnectionRole, Option_t Option )
{
    bool Enabled = false;

    if( IsConnectionContextCorrect(Context)
     && ConnectionRole  < oC_ARRAY_SIZE(Context->Configuration)
     && Option          < oC_ARRAY_SIZE(Context->Configuration[ConnectionRole])
     && Context->Configuration[ConnectionRole][Option] != Operation_None
        )
    {
        Enabled = Context->Configuration[ConnectionRole][Option];
    }

    return Enabled;
}

//==========================================================================================================================================
/**
 * @brief enables/disables the option
 *
 * The function enables the telnet option
 *
 * @param Context       Context of the connection
 * @param Option        Option to set
 * @param Enabled       true if it should be enabled
 *
 * @return code of error or `oC_ErrorCode_None` if success
 * 
 * Possible errors:
 * 
 *      Error Code                         | Description
 * ----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                     | Operation success
 *  #oC_ErrorCode_ImplementError           | There was unexpected error in implementation
 *  #oC_ErrorCode_ObjectNotCorrect         | The context is not correct
 *  #oC_ErrorCode_OptionNotFound           | The option is not found (out of range)
 *  #oC_ErrorCode_ConnectionRoleNotCorrect | The connection role is not correct
 * 
 */
//==========================================================================================================================================
static oC_ErrorCode_t SetOptionEnabled( ConnectionContext_t Context, ConnectionRole_t ConnectionRole, Option_t Option, bool Enabled )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsConnectionContextCorrect(Context)                             , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( ConnectionRole < oC_ARRAY_SIZE(Context->Configuration)          , oC_ErrorCode_ConnectionRoleNotCorrect )
     && ErrorCondition( Option < oC_ARRAY_SIZE(Context->Configuration[ConnectionRole])  , oC_ErrorCode_OptionNotFound           )
        )
    {
        Context->Configuration[ConnectionRole][Option] = Enabled;
        telnetlog(oC_LogType_Info, "%s: %s option has been set to %s for %s"
            , Context->Name, GetOptionName(Option)
            , Enabled ? "enabled" : "disabled"
            , ConnectionRole == ConnectionRole_Server ? "server" : "client"
            );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief gets the name of the option
 *
 * @param Option        Option to get the name
 *
 * @return name of the option
 */
//==========================================================================================================================================
static const char* GetOptionName( Option_t Option )
{
    return Option < oC_ARRAY_SIZE(OptionNames) ? OptionNames[Option] : "Unknown Option";
}

//==========================================================================================================================================
/**
 * @brief gets the name of the operation
 *
 * @param Operation     Operation to get the name
 *
 * @return name of the operation
 */
//==========================================================================================================================================
static const char* GetOperationName( Operation_t Operation )
{
    const char* operationName = "Unknown Operation";

    switch(Operation)
    {
        case Operation_None          : operationName = "None";              break;
        case Operation_SE            : operationName = "SE";                break;
        case Operation_NOP           : operationName = "NOP";               break;
        case Operation_DataMark      : operationName = "DataMark";          break;
        case Operation_Break         : operationName = "Break";             break;
        case Operation_Interrupt     : operationName = "Interrupt";         break;
        case Operation_AbortOutput   : operationName = "AbortOutput";       break;
        case Operation_AreYouThere   : operationName = "AreYouThere";       break;
        case Operation_EraseCharacter: operationName = "EraseCharacter";    break;
        case Operation_EraseLine     : operationName = "EraseLine";         break;
        case Operation_GoAhead       : operationName = "GoAhead";           break;
        case Operation_SB            : operationName = "SB";                break;
        case Operation_Will          : operationName = "Will";              break;
        case Operation_Wont          : operationName = "Wont";              break;
        case Operation_Do            : operationName = "Do";                break;
        case Operation_Dont          : operationName = "Dont";              break;
        case Operation_IAC           : operationName = "IAC";               break;
        default:
            break;
    }

    return operationName;
}

//==========================================================================================================================================
/**
 * @brief parses IAC commands
 *
 * The function searches for IAC commands, parses them and responds if needed
 *
 * @param Context       Context of the connection
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer
 * @param Timeout       Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
static oC_ErrorCode_t ParseIac( ConnectionContext_t Context, void* Buffer, oC_MemorySize_t* Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    
    if(
        ErrorCondition( IsConnectionContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect     )
     && ErrorCondition( isram(Buffer)                       , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isram(Size)                         , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( *Size > 0                           , oC_ErrorCode_SizeNotCorrect        )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect        )

        )
    {
        TelnetOptionParser_t options[50];
        oC_MemorySize_t optionIndex = 0;
        uint8_t* buffer = Buffer;
        oC_MemorySize_t size = *Size;
        oC_MemorySize_t iacOffset = size;
        oC_MemorySize_t commandSize = sizeof(TelnetOptionParser_t);

        memset(options, 0, sizeof(options));

        for(oC_MemorySize_t offset = 0; (offset + commandSize) < size; offset++)
        {
            if(buffer[offset] == Command_IAC)
            {
                iacOffset = offset;
                memcpy(&options[optionIndex], &buffer[iacOffset], sizeof(options[optionIndex]));
                optionIndex++;

                memset(&buffer[iacOffset], 0, commandSize);

                oC_MemorySize_t dataOffset = iacOffset + commandSize;
                if(dataOffset < size)
                {
                    oC_MemorySize_t leftSize = size - dataOffset;
                    memcpy(&buffer[iacOffset], &buffer[dataOffset], leftSize);
                }
                if(*Size >= commandSize)
                {
                    *Size -= commandSize;
                }

            }
        }
        if(optionIndex > 0)
        {
            errorCode = ParseOptions(Context, options, optionIndex, Timeout);
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief parses telnet options
 *
 * The function parses telnet options and responds if needed
 *
 * @param Context       Context of the connection
 * @param Options       Array with options
 * @param ArraySize     Size of the array
 * @param Timeout       Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t ParseOptions( ConnectionContext_t Context, TelnetOptionParser_t* Options, oC_MemorySize_t ArraySize, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ArraySize > 0   , oC_ErrorCode_SizeNotCorrect        )
     && ErrorCondition( Timeout >= 0    , oC_ErrorCode_TimeNotCorrect        )
        )
    {
        for(oC_MemorySize_t optionIndex = 0; optionIndex < ArraySize; optionIndex++)
        {
            TelnetOptionParser_t* option = &Options[optionIndex];
            if(option->Command == Command_IAC)
            {
                if(option->Operation == Operation_Do)
                {
                    if(IsOptionEnabled(Context,ConnectionRole_Server,option->Option))
                    {
                        option->Operation = Operation_Will;
                    }
                    else
                    {
                        option->Operation = Operation_Wont;
                    }
                    if(option->Option == Option_Echo)
                    {
                        if(!ErrorCode(SetOptionEnabled(Context,ConnectionRole_Client,Option_Echo,false) ) )
                        {
                            break;
                        }
                    }
                }
                else if(option->Operation == Operation_Will)
                {
                    if(IsOptionEnabled(Context,ConnectionRole_Client,option->Option))
                    {
                        option->Operation = Operation_Do;
                        if(option->Option == Option_Echo)
                        {
                            if(!ErrorCode( SetOptionEnabled(Context,ConnectionRole_Server,Option_Echo,false) ))
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        option->Operation = Operation_Dont;
                    }

                }
                telnetlog(oC_LogType_Info, "%s: Sending %s (%d) => %s", Context->Name, GetOptionName(option->Option), option->Option, GetOperationName(option->Operation));
            }
        }
        errorCode = oC_Tcp_Send(Context->Connection, Options, ArraySize * sizeof(*Options), Timeout);
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

