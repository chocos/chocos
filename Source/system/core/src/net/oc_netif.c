/** ****************************************************************************************************************************************
 *
 * @brief      File with functions of Netif object
 *
 * @file       oc_netif.c
 *
 * @author     Patryk Kubiak 
 * @author     Joanna "Kotel" Kuczkowska
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_netif.h>
#include <oc_object.h>
#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_intman.h>
#include <oc_string.h>
#include <oc_semaphore.h>
#include <oc_dhcp.h>
#include <oc_debug.h>
#include <oc_ktime.h>
#include <oc_compiler.h>
#include <oc_news.h>
#include <oc_system_cfg.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define LINK_STATUS_CHECKING_PERIOD         ms(100)

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_Net_HardwareAddress_t    HardwareAddress;
    oC_Net_Address_t            IpAddress;
} AddressResolutionEntry_t;

typedef enum
{
    ArpHardwareType_Unknown     = 0,
    ArpHardwareType_Ethernet    = 1,
} ArpHardwareType_t;

typedef enum
{
    ArpProtocolType_Unknown     = 0,
    ArpProtocolType_IPv4        = oC_Net_FrameType_IPv4,
    ArpProtocolType_IPv6        = oC_Net_FrameType_IPv6,
} ArpProtocolType_t;

typedef enum
{
    ArpOperation_Request        = 1 ,
    ArpOperation_Reply          = 2
} ArpOperation_t;

typedef struct PACKED
{
    uint16_t    HTYPE;              //!< Hardware type
    uint16_t    PTYPE;              //!< Protocol type
    uint8_t     HLEN;               //!< Hardware address length
    uint8_t     PLEN;               //!< Protocol address length
    uint16_t    OPER;               //!< Operation (1 is request, 2 is reply)
    uint8_t     AddressArray[4];    //!< Array with addresses
} ArpPacket_t;

struct Netif_t
{
    oC_ObjectControl_t                  ObjectControl;
    oC_Driver_t                         Driver;
    void *                              Context;
    void *                              Config;
    bool                                Configured;
    oC_Net_LinkStatus_t                 LinkStatus;
    oC_Netif_FriendlyName_t             FriendlyName;
    oC_List(oC_Net_Packet_t*)           PacketsList;
    oC_List(AddressResolutionEntry_t*)  AddressResolutionList;
    uint32_t                            SizeOfArpList;
    oC_Event_t                          NewArpAvailableEvent;
    bool                                LoopbackMode;
    oC_Net_Layer_t                      LoopbackLayer;
    oC_Semaphore_t                      LoopbackSemaphore;
    oC_BaudRate_t                       BaudRate;
    oC_Net_HardwareType_t               HardwareType;
    oC_Net_HardwareAddress_t            HardwareAddress;
    oC_Net_HardwareAddress_t            HardwareBroadcastAddress;
    oC_Net_Ipv4Info_t                   IPv4Info;
    oC_Net_Ipv6Info_t                   IPv6Info;
    oC_Thread_t                         ReceiveThread;
    bool                                ListenMode;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION__________________________________________________________________

static ArpHardwareType_t    HardwareTypeToArpHardwareType  ( oC_Net_HardwareType_t Type );
static oC_Net_PacketType_t  ArpProtocolToPacketType        ( ArpProtocolType_t Type );
static ArpProtocolType_t    PacketTypeToArpProtocol        ( oC_Net_PacketType_t Type );
static bool                 ReadHardwareAddressOf          ( oC_Netif_t Netif , const oC_Net_Address_t * Address , oC_Net_HardwareAddress_t * outHardwareAddress );
static oC_ErrorCode_t       RequestHardwareAddressViaArp   ( oC_Netif_t Netif , const oC_Net_Address_t * Address , oC_Net_HardwareAddress_t * outHardwareAddress , oC_Time_t Timeout );
static void                 SaveHardwareAddress            ( oC_Netif_t Netif , oC_Net_HardwareAddress_t * HardwareAddress, oC_Net_Address_t * IpAddress );
static void                 SaveHardwareAddressFromArp     ( oC_Netif_t Netif , ArpPacket_t * Packet , oC_Time_t Timeout );
static oC_ErrorCode_t       SendArpPacket                  ( oC_Netif_t Netif , ArpOperation_t Operation , oC_Net_HardwareAddress_t * TargetHwAddress, oC_Net_Address_t * IpAddress , oC_Time_t Timeout );
static void                 ConvertArpFromNetworkEndianess ( ArpPacket_t * Packet );
static void                 ConvertArpToNetworkEndianess   ( ArpPacket_t * Packet );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION__________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * Allocator for Netif object
 */
//==========================================================================================================================================
static oC_Allocator_t Allocator = {
                .Name       = "Netif" ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
/**
 * @addtogroup Netif
 * @{
 */

//==========================================================================================================================================
/**
 * @brief Creates Netif object
 *
 * The function is for creating new #Netif object. It allocates memory and initializes variables to work. The function does not configure
 * the related network driver, to do it, you have to call #oC_Netif_Configure function.
 *
 * @param FriendlyName      String with friendly name
 * @param Address           String with IPv4 or IPv6 address of the network interface or #NULL if DHCP should be used
 * @param NetMaskAddress    String with IPv4 or IPv6 address of the network mask or #NULL if DHCP should be used
 * @param Driver            Network driver to use for the #Netif object
 * @param Config            Pointer to the configuration of the network driver
 *
 * @return **Netif** object or #NULL in case of failure (check system errors stack for more info)
 *
 */
//==========================================================================================================================================
oC_Netif_t oC_Netif_New( oC_Netif_FriendlyName_t FriendlyName , const char * AddressString , const char * NetMaskAddressString , oC_Driver_t Driver , const void * Config )
{
    oC_Netif_t          netif               = NULL;
    const char *        localAddressString  = (AddressString        == NULL) ? "0.0.0.0" : AddressString;
    const char *        localNetMaskString  = (NetMaskAddressString == NULL) ? "0.0.0.0" : NetMaskAddressString;
    bool                staticIp            = AddressString != NULL;
    oC_Net_Address_t    localAddress;
    oC_Net_Address_t    localNetMaskAddress;

    bzero(&localAddress,sizeof(localAddress));
    bzero(&localNetMaskAddress,sizeof(localNetMaskAddress));

    if(
        oC_SaveIfFalse("Netif::New - FriendlyName ",   isaddresscorrect(FriendlyName)                                          , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("Netif::New - FriendlyName ",   strlen(FriendlyName) > 0                                                , oC_ErrorCode_StringIsEmpty        )
     && oC_SaveIfFalse("Netif::New - FriendlyName ",   strlen(FriendlyName) < sizeof(oC_Netif_FriendlyName_t)                  , oC_ErrorCode_StringIsTooLong      )
     && oC_SaveIfFalse("Netif::New - Driver ",         isaddresscorrect(Driver)                                                , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("Netif::New - Driver ",         oC_Driver_GetDriverType(Driver) == NETWORK_DRIVER                       , oC_ErrorCode_DriverTypeNotCorrect )
     && oC_SaveIfFalse("Netif::New - Address ",        isaddresscorrect(AddressString) || AddressString == NULL                , oC_ErrorCode_WrongAddress         )
     && oC_SaveIfFalse("Netif::New - NetMaskAddress ", isaddresscorrect(NetMaskAddressString) || NetMaskAddressString == NULL  , oC_ErrorCode_WrongAddress         )
        )
    {
        netif = kmalloc( sizeof(struct Netif_t) , &Allocator , AllocationFlags_ZeroFill );

        if(oC_SaveIfFalse("Netif::New cannot allocate memory" , netif != NULL , oC_ErrorCode_AllocationError))
        {
            if(
                oC_SaveIfErrorOccur("Netif::New - Address - ",        oC_Net_AddressFromString(localAddressString, &localAddress                            ))
             && oC_SaveIfErrorOccur("Netif::New - NetMaskAddress - ", oC_Net_AddressFromString(localNetMaskString, &localNetMaskAddress                     ))
             && oC_SaveIfFalse("Netif::New - NetMask and Address - ", localAddress.Type == localNetMaskAddress.Type , oC_ErrorCode_AddressTypeNotCorrect    )
                )
            {
                netif->Config                   = kmalloc( Driver->ConfigurationSize , &Allocator, AllocationFlags_CanWait1Second);
                netif->AddressResolutionList    = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);
                netif->NewArpAvailableEvent     = oC_Event_New(oC_Event_State_Inactive, &Allocator, AllocationFlags_ZeroFill);

                if(
                    oC_SaveIfFalse("Netif::New - Config   - ", netif->Config                != NULL , oC_ErrorCode_AllocationError)
                 && oC_SaveIfFalse("Netif::New - ARP List - ", netif->AddressResolutionList != NULL , oC_ErrorCode_AllocationError)
                    )
                {
                    netif->ObjectControl            = oC_CountObjectControl(netif,oC_ObjectId_Netif);
                    netif->Driver                   = Driver;
                    netif->Context                  = NULL;
                    netif->Configured               = false;
                    netif->LinkStatus               = oC_Net_LinkStatus_Down;
                    netif->PacketsList              = NULL;
                    netif->LoopbackMode             = false;
                    netif->LoopbackSemaphore        = NULL;
                    netif->BaudRate                 = 0;
                    netif->HardwareType             = oC_Net_HardwareType_Invalid;
                    netif->IPv4Info.IP              = localAddress.Type         == oC_Net_AddressType_IPv4 ? localAddress.IPv4        : 0;
                    netif->IPv4Info.Netmask         = localNetMaskAddress.Type  == oC_Net_AddressType_IPv4 ? localNetMaskAddress.IPv4 : 0;
                    netif->IPv4Info.NetIP           = netif->IPv4Info.IP & netif->IPv4Info.Netmask;
                    netif->IPv6Info.IPv6.LowPart    = localAddress.Type         == oC_Net_AddressType_IPv6 ? localAddress.IPv6.LowPart         : 0;
                    netif->IPv6Info.IPv6.HighPart   = localNetMaskAddress.Type  == oC_Net_AddressType_IPv6 ? localNetMaskAddress.IPv6.HighPart : 0;
                    netif->IPv4Info.DefaultTTL      = 0xFF;
                    netif->IPv4Info.BroadcastIP     = IP(255,255,255,255);
                    netif->IPv4Info.StaticIP        = staticIp;
                    netif->ListenMode               = false;
                    netif->SizeOfArpList            = 0;
                    netif->IPv4Info.LeaseTime       = CFG_TIME_DEFAULT_LEASE_TIME;

                    strncpy(netif->FriendlyName,FriendlyName,sizeof(netif->FriendlyName));
                    memcpy(netif->Config,Config,netif->Driver->ConfigurationSize);
                }
                else
                {
                    bool deleted = netif->AddressResolutionList == NULL ? true : oC_List_Delete(netif->AddressResolutionList, AllocationFlags_CanWait1Second);

                    oC_SaveIfFalse("Netif::New - Event delete - "   , netif->NewArpAvailableEvent == NULL || oC_Event_Delete(&netif->NewArpAvailableEvent,AllocationFlags_Default)  , oC_ErrorCode_ReleaseError );
                    oC_SaveIfFalse("Netif::New - netif structure - ", kfree(netif,AllocationFlags_CanWait1Second)                                                                   , oC_ErrorCode_ReleaseError );
                    oC_SaveIfFalse("Netif::New - ARP List delete - ", deleted                                                                                                       , oC_ErrorCode_ReleaseError );
                    netif = NULL;
                }
            }
            else
            {
                oC_SaveIfFalse("Netif::New - netif structure - ", kfree(netif,AllocationFlags_CanWait1Second),oC_ErrorCode_ReleaseError);
                netif = NULL;
            }
        }
    }

    return netif;
}

//==========================================================================================================================================
/**
 * @brief deletes netif object
 *
 * The function is for deleting the Netif object. It release all memory and unconfigures the related driver if it is possible.
 *
 * @param outNetif          Netif object from the #oC_Netif_New function
 *
 * @return true if success (check system errors stack for more info)
 */
//==========================================================================================================================================
bool oC_Netif_Delete( oC_Netif_t * outNetif )
{
    bool success = false;

    if(
        oC_SaveIfFalse("Netif::Delete - outNetif not correct - " , isram(outNetif)                          , oC_ErrorCode_OutputAddressNotInRAM    )
     && oC_SaveIfFalse("Netif::Delete - outNetif not correct - " , oC_Netif_IsCorrect(*outNetif)            , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        oC_IntMan_EnterCriticalSection();

        oC_Netif_t netif     = *outNetif;
        netif->ObjectControl = 0;

        oC_IntMan_ExitCriticalSection();

        if(netif->Configured)
        {
            oC_SaveIfErrorOccur("Netif::Delete - cannot unconfigure driver - ", oC_Driver_Unconfigure(netif->Driver,netif->Config,&netif->Context));
        }
        oC_SaveIfFalse("Netif::Delete - cannot release config ", kfree(netif->Config, AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);

        if(netif->LoopbackSemaphore)
        {
            oC_SaveIfFalse("Netif::Delete - cannot delete semaphore - " , oC_Semaphore_Delete(&netif->LoopbackSemaphore,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
        }

        if(netif->PacketsList)
        {
            oC_SaveIfFalse("Netif::Delete - cannot delete Packets list - " , List_Delete((oC_List_t*)&netif->PacketsList,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
        }

        if(netif->NewArpAvailableEvent)
        {
            oC_SaveIfFalse("Netif::Delete - cannot delete event - " , oC_Event_Delete(&netif->NewArpAvailableEvent,AllocationFlags_Default) , oC_ErrorCode_ReleaseError);
        }

        if(netif->AddressResolutionList)
        {
            foreach(netif->AddressResolutionList,entry)
            {
                kfree(entry,AllocationFlags_Default);
            }
            oC_SaveIfFalse("Netif::Delete - cannot delete ARP list - " , List_Delete((oC_List_t*)&netif->AddressResolutionList,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
        }

        if(oC_SaveIfFalse("Netif::Delete - release memory fault", kfree(netif,AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError))
        {
            *outNetif   = NULL;
            success     = true;
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief checks if the Netif object is correct
 *
 * The function is for checking if the #Netif object is correct
 *
 * @param Netif     Netif object from the #oC_Netif_New function
 *
 * @return true if object is correct
 */
//==========================================================================================================================================
bool oC_Netif_IsCorrect( oC_Netif_t Netif )
{
    return isram(Netif) && oC_CheckObjectControl(Netif, oC_ObjectId_Netif, Netif->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is configured
 *
 * The function is for checking if a driver related with the object was configured before by function #oC_Netif_Configure
 *
 * @param Netif     Netif object from the #oC_Netif_New function
 *
 * @return true if object is correct and configured
 */
//==========================================================================================================================================
bool oC_Netif_IsConfigured( oC_Netif_t Netif )
{
    return oC_Netif_IsCorrect(Netif) && Netif->Configured;
}

//==========================================================================================================================================
/**
 * @brief returns friendly name of the network interface
 *
 * The function is to read friendly name of the network interface
 *
 * @param Netif     Netif object from the #oC_Netif_New function
 *
 * @return string with the name of the network interface
 */
//==========================================================================================================================================
const char * oC_Netif_GetFriendlyName( oC_Netif_t Netif )
{
    const char * name = "Netif object not correct";

    if(oC_SaveIfFalse("Netif::GetFriendlyName: " , oC_Netif_IsCorrect(Netif) , oC_ErrorCode_ObjectNotCorrect))
    {
        name = Netif->FriendlyName;
    }

    return name;
}


//==========================================================================================================================================
/**
 * @brief returns driver related with the Netif
 *
 * The function returns the network driver related with the Netif object or #NULL in case of error.
 *
 * @param Netif     Netif object from the #oC_Netif_New function
 *
 * @return driver related with the Netif or #NULL in case of error
 */
//==========================================================================================================================================
oC_Driver_t oC_Netif_GetDriver( oC_Netif_t Netif )
{
    oC_Driver_t driver = NULL;

    if(oC_SaveIfFalse("Netif::GetDriver: " , oC_Netif_IsCorrect(Netif) , oC_ErrorCode_ObjectNotCorrect))
    {
        driver = Netif->Driver;
    }

    return driver;
}

//==========================================================================================================================================
/**
 * @brief configures network driver to work with netif
 *
 * The function is for configuration of the network driver related with the network interface by using configuration structure given to the
 * #oC_Netif_New function.
 *
 * @param Netif     Netif object from the #oC_Netif_New function
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_AlreadyConfigured               | Network interface was configured before
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_Configure( oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)   , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( Netif->Configured == false  , oC_ErrorCode_AlreadyConfigured    )
        )
    {
        if(ErrorCode(oC_Driver_Configure(Netif->Driver,Netif->Config,&Netif->Context)))
        {
            Netif->Configured   = true;
            errorCode           = oC_ErrorCode_None;

            oC_Netif_UpdateLinkStatus(Netif);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures network driver related with the `Netif`
 *
 * The function unconfigures the network driver related with the network interface.
 *
 * @param Netif     Netif object from the #oC_Netif_New function
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_Unconfigure( oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif) , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Netif->Configured         , oC_ErrorCode_NotConfiguredYet       )
        )
    {
        errorCode = oC_Driver_Unconfigure( Netif->Driver , Netif->Config , &Netif->Context );

        Netif->Context      = NULL;
        Netif->Configured   = false;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads informations about network interface
 *
 * The function is for read informations about the network interface. The network driver related with this object has to be configured before
 * by using the function #oC_Netif_Configure.
 *
 * @note
 * The function also updates the link status for future use
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 * @param outInfo       Destination address for the network interface informations
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outInfo` pointer is not valid RAM pointer
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_ReadNetInfo( oC_Netif_t Netif , oC_Net_Info_t * outInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif) , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Netif->Configured         , oC_ErrorCode_NotConfiguredYet       )
     && ErrorCondition( isram(outInfo)            , oC_ErrorCode_OutputAddressNotInRAM  )
        )
    {
        if(
            ErrorCode(oC_Driver_ReadNetInfo( Netif->Driver , Netif->Context , outInfo ))
            )
        {
            memcpy(&outInfo->NetworkLayer.IPv4,&Netif->IPv4Info,sizeof(oC_Net_Ipv4Info_t));
            memcpy(&outInfo->NetworkLayer.IPv6,&Netif->IPv6Info,sizeof(oC_Net_Ipv6Info_t));

            memcpy(&Netif->HardwareAddress,             &outInfo->HardwareAddress,           sizeof(oC_Net_HardwareAddress_t));
            memcpy(&Netif->HardwareBroadcastAddress,    &outInfo->HardwareBroadcastAddress,  sizeof(oC_Net_HardwareAddress_t));

            Netif->LinkStatus   = outInfo->LinkStatus;
            Netif->BaudRate     = outInfo->BaudRate;
            Netif->HardwareType = outInfo->HardwareType;

            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends packet by using the network interface
 *
 * Allows for sending a packet to the network location by using the interface. The interface has to be configured by calling the function
 * #oC_Netif_Configure before.
 *
 * @param Netif                     Netif object from the #oC_Netif_New function
 * @param Packet                    Packet to send
 * @param Timeout                   Maximum time to wait for the packet sending
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_WrongAddress                    | `Packet` address is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SendPacket( oC_Netif_t Netif , oC_Net_Packet_t * Packet , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)               , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Netif->Configured                       , oC_ErrorCode_NotConfiguredYet       )
     && ErrorCondition( isaddresscorrect(Packet)                , oC_ErrorCode_WrongAddress           )
        )
    {
        if(Netif->LoopbackMode && Netif->LoopbackLayer == oC_Net_Layer_Netif)
        {
            oC_Net_Packet_t * packet = kmalloc(oC_Net_GetPacketSize(Packet,true),&Allocator,AllocationFlags_CanWait1Second);
            if(
                ErrorCondition( packet != NULL                                                                           , oC_ErrorCode_AllocationError       )
             && ErrorCondition( oC_Semaphore_GiveCounting(Netif->LoopbackSemaphore,1)                                    , oC_ErrorCode_CannotGiveSemaphore   )
                )
            {

                memcpy(packet,Packet,oC_Net_GetPacketSize(Packet,true));

                if(ErrorCondition( List_PushBack((oC_List_t)Netif->PacketsList,&packet,sizeof(packet),&Allocator)   , oC_ErrorCode_CannotAddObjectToList ))
                {
                    errorCode = oC_ErrorCode_None;
                }
            }
        }
        else
        {
            oC_Net_Frame_t      frame;
            oC_Net_Address_t    address;

            bzero( &frame, sizeof(frame) );

            memcpy( &frame.Source , &Netif->HardwareAddress, sizeof( oC_Net_HardwareAddress_t ));

            frame.ConstPacket = Packet;
            frame.Size        = oC_Net_GetPacketSize(Packet,true);

            if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv4)
            {
                Packet->IPv4.Header.ID       = (uint16_t)oC_KTime_GetCurrentTick();

                Packet->IPv4.Header.SourceIp = Netif->IPv4Info.IP;
                if(Packet->IPv4.Header.IHL == 0)
                {
                    Packet->IPv4.Header.IHL = 5;
                }

                if(Packet->IPv4.Header.TTL == 0)
                {
                    Packet->IPv4.Header.TTL = Netif->IPv4Info.DefaultTTL;
                }

                Packet->IPv4.Header.Checksum = 0;
                Packet->IPv4.Header.Checksum = oC_Net_Packet_CalculateChecksum(Packet);

                frame.FrameType = oC_Net_FrameType_IPv4;
                address.Type    = oC_Net_AddressType_IPv4;
                address.IPv4    = Packet->IPv4.Header.DestinationIp;
            }
            else
            {
                Packet->IPv6.Header.Source.LowPart  = Netif->IPv6Info.IPv6.LowPart;
                Packet->IPv6.Header.Source.HighPart = Netif->IPv6Info.IPv6.HighPart;

                address.Type            = oC_Net_AddressType_IPv6;
                address.IPv6.HighPart   = Packet->IPv6.Header.Destination.HighPart;
                address.IPv6.LowPart    = Packet->IPv6.Header.Destination.LowPart;
            }
            if( ErrorCondition( oC_Net_ConvertHeaderToNetworkEndianess( Packet ), oC_ErrorCode_CannotConvertHeaderEndianess ) )
            {
                if( Netif->LoopbackMode )
                {
                    memcpy(&frame.Destination, &Netif->HardwareBroadcastAddress, sizeof(frame.Destination));

                    if(
                        ErrorCode( oC_Driver_SendFrame( Netif->Driver , Netif->Context , &frame , Timeout ))
                        )
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                }
                else if(
                        ErrorCode( oC_Netif_ReadHardwareAddressOf   ( Netif         , &frame.Destination , &address , Timeout ))
                     && ErrorCode( oC_Driver_SendFrame              ( Netif->Driver , Netif->Context     , &frame   , Timeout ))
                        )
                {
                    errorCode = oC_ErrorCode_None;
                }

                oC_SaveIfFalse("oC_Netif_SendPacket: Cannot restore endianess of header - ", oC_Net_ConvertHeaderFromNetworkEndianess( Packet ), oC_ErrorCode_CannotConvertHeaderEndianess );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives packet by using the interface
 *
 * The function receives packet from the network by using the network interface. The interface has to be configured by the function
 * #oC_Netif_Configure before.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param outPacket         Destination pointer for the packet to receive. It will points to the memory allocated by using the `PacketAllocator` and it should be released by #kfree when it is not needed anymore
 * @param PacketSize        Size of the packet buffer
 * @param Timeout           Maximum time to wait for the packet
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_OutputAddressNotInRAM           | `Packet` address does not point to the RAM section
 *  oC_ErrorCode_SizeNotCorrect                  | `PacketSize` is too small
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_ReceivePacket( oC_Netif_t Netif , oC_Net_Packet_t * Packet , oC_MemorySize_t PacketSize , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)                       , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Netif->Configured                               , oC_ErrorCode_NotConfiguredYet       )
     && ErrorCondition( isram(Packet)                                   , oC_ErrorCode_OutputAddressNotInRAM  )
     && ErrorCondition( PacketSize > sizeof(oC_Net_Ipv4PacketHeader_t)  , oC_ErrorCode_SizeNotCorrect         )
        )
    {
        if(Netif->LoopbackMode && Netif->LoopbackLayer == oC_Net_Layer_Netif)
        {
            oC_Net_Packet_t * packet = NULL;

            if(
                ErrorCondition( oC_Semaphore_TakeCounting( Netif->LoopbackSemaphore, 1, Timeout )           , oC_ErrorCode_Timeout                      )
             && ErrorCondition( List_TakeFirst( (oC_List_t)Netif->PacketsList, &packet, sizeof(packet) )    , oC_ErrorCode_CannotRemoveObjectFromList   )
             && ErrorCondition( oC_Net_GetPacketSize(packet,true) <= PacketSize                             , oC_ErrorCode_SizeNotCorrect               )
                )
            {
                memcpy(Packet,packet,oC_Net_GetPacketSize(packet,true));
                kfree(packet,AllocationFlags_CanWait1Second);

                errorCode = oC_ErrorCode_None;
            }
        }
        else
        {
            oC_Net_Frame_t frame;

            bzero(&frame,sizeof(frame));

            frame.Packet    = Packet;
            frame.Size      = PacketSize;

            while(
                   ErrorCode       ( oC_Driver_ReceiveFrame( Netif->Driver , Netif->Context , &frame , oC_KTime_CalculateTimeout(endTimestamp) )    )
                && ErrorCondition  ( frame.Source.Filled && frame.Destination.Filled        , oC_ErrorCode_HardwareAddressNotFilled                 )
                && ErrorCondition  ( isram(frame.Packet)                                    , oC_ErrorCode_InternalDataAreDamaged                   )
                   )
            {
                if(frame.FrameType == oC_Net_FrameType_ARP && Netif->ListenMode == false)
                {
                    ConvertArpFromNetworkEndianess((ArpPacket_t*)frame.Packet);
                    SaveHardwareAddressFromArp(Netif, (ArpPacket_t*)frame.Packet, oC_KTime_CalculateTimeout(endTimestamp));

                    frame.Packet    = Packet;
                    frame.Size      = PacketSize;
                }
                else if(ErrorCondition( oC_Net_ConvertHeaderFromNetworkEndianess( Packet ) , oC_ErrorCode_CannotConvertHeaderEndianess ))
                {
                    oC_Net_Address_t address = {
                                    .Type = (oC_Net_AddressType_t)Packet->IPv4.Header.Version,
                                    .IPv4 = Packet->IPv4.Header.SourceIp
                    };

                    if(address.Type == oC_Net_AddressType_IPv6)
                    {
                        address.IPv6.HighPart = Packet->IPv6.Header.Source.HighPart;
                        address.IPv6.LowPart  = Packet->IPv6.Header.Source.LowPart;
                    }

                    SaveHardwareAddress(Netif,&frame.Source,&address);
                    errorCode = oC_ErrorCode_None;
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets wake on LAN event
 *
 * The function sets the "Wake on LAN" event. This the event, that should be activated when some packet will be available to receive during
 * sleep mode.
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 * @param WolEvent      Wake on LAN event
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` or `WolEvent` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SetWakeOnLanEvent( oC_Netif_t Netif , oC_Event_t WolEvent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)       , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Netif->Configured               , oC_ErrorCode_NotConfiguredYet       )
     && ErrorCondition( oC_Event_IsCorrect(WolEvent)    , oC_ErrorCode_ObjectNotCorrect       )
        )
    {
        if(
            ErrorCode(oC_Driver_SetWakeOnLanEvent( Netif->Driver , Netif->Context , WolEvent ))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief flushes Network interface
 *
 * When this function is called, the transmit FIFO controller logic is reset to its default values and thus
 * all data in the Tx FIFO are lost/flushed.
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` or `WolEvent` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_Flush( oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)       , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Netif->Configured               , oC_ErrorCode_NotConfiguredYet       )
        )
    {
        if(
            ErrorCode(oC_Driver_Flush( Netif->Driver , Netif->Context ))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief enables or disables loopback
 *
 * The function enables or disables loopback mode at the given network layer.
 *
 * If the `Layer` is set to the #oC_Net_Layer_Netif, the loopback will be set in this object. Note, that this will be only software loopback,
 * and the performance of it can be very low.
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 * @param Layer         Target layer for the loopback
 * @param Enabled       true if loopback should be enabled
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` or `WolEvent` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SetLoopback( oC_Netif_t Netif , oC_Net_Layer_t Layer , bool Enabled )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)       , oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( Netif->Configured               , oC_ErrorCode_NotConfiguredYet       )
        )
    {
        if( Layer == oC_Net_Layer_Netif )
        {
            Netif->LoopbackMode     = Enabled;
            Netif->LoopbackLayer    = Layer;

            if(Enabled)
            {
                Netif->LoopbackSemaphore    = oC_Semaphore_New(10, 0, &Allocator, AllocationFlags_CanWait1Second);
                Netif->PacketsList          = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);

                if(
                    ErrorCondition( Netif->LoopbackSemaphore != NULL , oC_ErrorCode_AllocationError )
                 && ErrorCondition( Netif->PacketsList != NULL       , oC_ErrorCode_AllocationError )
                    )
                {
                    Netif->LinkStatus = oC_Net_LinkStatus_Down;
                    errorCode         = oC_ErrorCode_None;
                }
                else
                {
                    oC_Semaphore_Delete( &Netif->LoopbackSemaphore , AllocationFlags_CanWait1Second);
                    oC_List_Delete(Netif->PacketsList,AllocationFlags_CanWait1Second);

                    Netif->LoopbackMode = false;
                }
            }
            else
            {
                if(Netif->LoopbackSemaphore == NULL)
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    bool deleted = oC_List_Delete(Netif->PacketsList,AllocationFlags_CanWait1Second);

                    if(
                        ErrorCondition( oC_Semaphore_Delete(&Netif->LoopbackSemaphore,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError )
                     && ErrorCondition( deleted                                                                       , oC_ErrorCode_ReleaseError )
                        )
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                }
            }
        }
        else if(
                ErrorCode(oC_Driver_SetLoopback( Netif->Driver , Netif->Context , Layer , Enabled ))
                )
        {
            Netif->LoopbackMode     = true;
            Netif->LoopbackLayer    = Layer;
            errorCode               = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief performs diagnostics of the network interface
 *
 * The function is for performing diagnostics on the selected network interface. The feature has to be handled by the related network driver.
 *
 * @note
 * You can set the `outDiags` field to NULL and `NumberOfDiags` to 0, if you want to know count of diagnostics supported by the driver.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param outDiags          Pointer to the allocated array for diagnostics, or NULL if only number of diagnostics is required
 * @param NumberOfDiags     As input - size of the `outDiags` array. As output - number of performed diagnostics or diagnostics, that can be performed (if the `outDiags` is NULL)
 *
 * Example:
 * @code{.c}
   oC_Diag_t * diags         = NULL;
   uint32_t    numberOfDiags = 0;

   // Reading number of supported diags
   oC_Netif_PerformDiagnostics( netif, NULL, &numberOfDiags );

   diags = malloc( sizeof(oC_Diag_t) * numberOfDiags , 0 );

   oC_Netif_PerformDiagnostics( netif, diags, &numberOfDiags );

   @endcode
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_OutputAddressNotInRAM           | `NumberOfDiags` address does not point to the RAM section
 *  oC_ErrorCode_NotHandledByDriver              | The associated network driver does not handle diagnostics
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_PerformDiagnostics( oC_Netif_t Netif , oC_Diag_t * outDiags , uint32_t * NumberOfDiags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( Netif->Configured               , oC_ErrorCode_NotConfiguredYet         )
     && ErrorCondition( isram(NumberOfDiags)            , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        if(
            ErrorCode(oC_Driver_PerformDiagnostics( Netif->Driver , Netif->Context , outDiags , NumberOfDiags ))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns last known link status of the network interface
 *
 * The function returns link status of the network interface.
 *
 * @warning
 * The function does not check the current link status - it only returns last KNOWN link status. It is updated periodically by the system. To
 * update the link status, call #oC_Netif_UpdateLinkStatus function. If you want to be sure, that your link status is up-to-date, you should
 * call #oC_Netif_ReadNetInfo function.
 *
 * @note
 * If the netif loopback mode is enabled, the function will always return #oC_Net_LinkStatus_Down
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 *
 * @return last known link status or #oC_Net_LinkStatus_Down in case of error
 */
//==========================================================================================================================================
oC_Net_LinkStatus_t oC_Netif_GetLinkStatus( oC_Netif_t Netif )
{
    oC_Net_LinkStatus_t linkStatus = oC_Net_LinkStatus_Down;

    if(oC_Netif_IsCorrect(Netif) && Netif->LoopbackMode == false)
    {
        linkStatus = Netif->LinkStatus;
    }

    return linkStatus;
}

//==========================================================================================================================================
/**
 * @brief returns baud rate of the network interface
 *
 * The function returns baud rate of the network interface.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 *
 * @return Baud rate of the network interface or 0 in case of error
 */
//==========================================================================================================================================
oC_BaudRate_t oC_Netif_GetBaudRate( oC_Netif_t Netif )
{
    oC_BaudRate_t baudRate = 0;

    if(oC_Netif_IsCorrect(Netif))
    {
        baudRate = Netif->BaudRate;
    }

    return baudRate;
}

//==========================================================================================================================================
/**
 * @brief returns hardware type of the network interface
 *
 * The function returns hardware type of the network interface
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 *
 * @return hardware type of the interface
 */
//==========================================================================================================================================
oC_Net_HardwareType_t oC_Netif_GetHardwareType( oC_Netif_t Netif )
{
    oC_Net_HardwareType_t type = oC_Net_HardwareType_Invalid;

    if(oC_Netif_IsCorrect(Netif))
    {
        type = Netif->HardwareType;
    }

    return type;
}


//==========================================================================================================================================
/**
 * @brief reads hardware address of the interface
 *
 * The function reads hardware address of the network interface
 *
 * @param Netif                 Netif object from the #oC_Netif_New function
 * @param outHardwareAddress    Destination for the hardware address
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outHardwareAddress` address does not point to the RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_ReadHardwareAddress( oC_Netif_t Netif , oC_Net_HardwareAddress_t * outHardwareAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)  , oC_ErrorCode_ObjectNotCorrect      )
     && ErrorCondition( Netif->Configured == true  , oC_ErrorCode_NotConfiguredYet      )
     && ErrorCondition( isram(outHardwareAddress)  , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        memcpy(outHardwareAddress,&Netif->HardwareAddress,sizeof(oC_Net_HardwareAddress_t));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads hardware address of the given IP address in LAN
 *
 * The function is for reading hardware address of the machine with the given IP address. It uses ARP protocol to receive it from the Local Area Network (LAN).
 *
 * @warning
 * When the given IP address is not in the LAN, the function will return only hardware address of the local router.
 *
 * @param Netif                 Netif object from the #oC_Netif_New function
 * @param outHardwareAddress    Destination for the hardware address
 * @param Address               IP address to find
 * @param Timeout               Maximum time for operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outHardwareAddress` address does not point to the RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_ReadHardwareAddressOf( oC_Netif_t Netif , oC_Net_HardwareAddress_t * outHardwareAddress , const oC_Net_Address_t * Address , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)                 , oC_ErrorCode_ObjectNotCorrect      )
     && ErrorCondition( Netif->Configured == true                 , oC_ErrorCode_NotConfiguredYet      )
     && ErrorCondition( isram(outHardwareAddress)                 , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( isaddresscorrect(Address)                 , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( Address->Type == oC_Net_AddressType_IPv4  , oC_ErrorCode_UnknownAddressType    )
        )
    {
        bool found = false;

        if(oC_Net_Ipv4_IsAddressInSubnet(Address->IPv4,Netif->IPv4Info.NetIP,Netif->IPv4Info.Netmask))
        {
            foreach(Netif->AddressResolutionList,entry)
            {
                if(entry->IpAddress.IPv4 == Address->IPv4)
                {
                    memcpy(outHardwareAddress,&entry->HardwareAddress,sizeof(oC_Net_HardwareAddress_t));
                    found = true;
                }
            }

            if(found == false)
            {
                if(Address->IPv4 == Netif->IPv4Info.BroadcastIP)
                {
                    memcpy(outHardwareAddress, &Netif->HardwareBroadcastAddress, sizeof(oC_Net_HardwareAddress_t));
                    found       = true;
                    errorCode   = oC_ErrorCode_None;
                }
                else if(ErrorCode(RequestHardwareAddressViaArp(Netif, Address, outHardwareAddress, Timeout)))
                {
                    AddressResolutionEntry_t * entry = kmalloc(sizeof(AddressResolutionEntry_t), &Allocator, AllocationFlags_ZeroFill);

                    if(ErrorCondition( entry != NULL , oC_ErrorCode_AllocationError ))
                    {
                        memcpy(&entry->HardwareAddress, outHardwareAddress, sizeof(oC_Net_HardwareAddress_t) );
                        memcpy(&entry->IpAddress      , Address           , sizeof(oC_Net_Address_t)         );

                        oC_List_PushBack(Netif->AddressResolutionList,entry,&Allocator);

                        found     = true;
                        errorCode = oC_ErrorCode_None;
                    }
                }
            }
            else
            {
                errorCode = oC_ErrorCode_None;
            }
        }
        else
        {
            if(Netif->IPv4Info.HardwareRouterAddress.Filled)
            {
                memcpy(outHardwareAddress, &Netif->IPv4Info.HardwareRouterAddress, sizeof(oC_Net_HardwareAddress_t));

                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_Net_Address_t address;

                address.Type = oC_Net_AddressType_IPv4;
                address.IPv4 = Netif->IPv4Info.NetIP;
                if(address.IPv4 == 0)
                {
                    address.IPv4 = Netif->IPv4Info.DhcpIP;
                    kdebuglog(oC_LogType_Error, "NetIP is missing - cannot read HW address of router. Trying DHCP IP: 0x%08X\n", address.IPv4);
                }

                if(ErrorCode( RequestHardwareAddressViaArp( Netif, &address, &Netif->IPv4Info.HardwareRouterAddress, Timeout) ))
                {
                    memcpy(outHardwareAddress, &Netif->IPv4Info.HardwareRouterAddress, sizeof(oC_Net_HardwareAddress_t));

                    errorCode = oC_ErrorCode_None;
                }
            }
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets informations about IPv4
 *
 * The function sets informations about the IPv4.
 *
 * @warning
 * It does not check if the data is correct - only pointer is validated.
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 * @param Info          Pointer to the IPv4 info structure
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_WrongAddress                    | The `Info` pointer is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SetIpv4Info( oC_Netif_t Netif , const oC_Net_Ipv4Info_t * Info )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif ) , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( isaddresscorrect(Info)      , oC_ErrorCode_WrongAddress     )
        )
    {
        memcpy(&Netif->IPv4Info,Info,sizeof(oC_Net_Ipv4Info_t));

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads IPv4 informations
 *
 * The function reads IPv4 informations from the given network interface.
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 * @param outInfo       Destination for the IPv4 informations
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_OutputAddressNotInRAM           | The `outInfo` pointer is not storred in the RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_ReadIpv4Info( oC_Netif_t Netif , oC_Net_Ipv4Info_t * outInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif ) , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outInfo)              , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        memcpy(outInfo,&Netif->IPv4Info,sizeof(oC_Net_Ipv4Info_t));

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets informations about IPv6
 *
 * The function sets informations about the IPv6.
 *
 * @warning
 * It does not check if the data is correct - only pointer is validated.
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 * @param Info          Pointer to the IPv4 info structure
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_WrongAddress                    | The `Info` pointer is not correct
 */

//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SetIpv6Info( oC_Netif_t Netif , const oC_Net_Ipv6Info_t * Info )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif ) , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( isaddresscorrect(Info)      , oC_ErrorCode_WrongAddress     )
        )
    {
        memcpy(&Netif->IPv6Info,Info,sizeof(oC_Net_Ipv6Info_t));

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads IPv6 informations
 *
 * The function reads IPv6 informations from the given network interface.
 *
 * @param Netif         Netif object from the #oC_Netif_New function
 * @param outInfo       Destination for the IPv4 informations
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_OutputAddressNotInRAM           | The `outInfo` pointer is not storred in the RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_ReadIpv6Info( oC_Netif_t Netif , oC_Net_Ipv6Info_t * outInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect( Netif ) , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outInfo)              , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        memcpy(outInfo,&Netif->IPv6Info,sizeof(oC_Net_Ipv6Info_t));

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the given address belongs to the given subnet
 *
 * The function checks if the given address belongs to the network interface subnet.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param Address           Pointer to the network address to check (IPv4 or IPv6)
 *
 * @return true if the address is correct, and it belongs to the network
 */
//==========================================================================================================================================
bool oC_Netif_IsAddressInSubnet( oC_Netif_t Netif , const oC_Net_Address_t * Address )
{
    bool belongs = false;

    if(
        oC_SaveIfFalse("Netif::IsAddressInSubnet - Netif - "      , oC_Netif_IsCorrect(Netif)                   , oC_ErrorCode_ObjectNotCorrect       )
     && oC_SaveIfFalse("Netif::IsAddressInSubnet - Address - "    , isaddresscorrect(Address)                   , oC_ErrorCode_WrongAddress           )
     && oC_SaveIfFalse("Netif::IsAddressInSubnet - Address - "    , oC_Net_IsAddressCorrect(Address)            , oC_ErrorCode_IpAddressNotCorrect    )
     && oC_SaveIfFalse("Netif::IsAddressInSubnet - link status - ", Netif->LinkStatus == oC_Net_LinkStatus_Up   , oC_ErrorCode_LinkNotDetected        )
        )
    {
        if(Address->Type == oC_Net_AddressType_IPv4)
        {
            belongs = oC_Net_Ipv4_IsAddressInSubnet(Address->IPv4,Netif->IPv4Info.NetIP,Netif->IPv4Info.Netmask);
        }
        else if(Address->Type == oC_Net_AddressType_IPv6)
        {
            oC_SaveError("Netif::IsAddressInSubnet - not implemented for IPv6", oC_ErrorCode_NotImplemented);
        }
        else
        {
            oC_SaveError("Netif::IsAddressInSubnet - address type is not correct or not recognized - ", oC_ErrorCode_IpAddressNotCorrect);
        }
    }

    return belongs;
}


//==========================================================================================================================================
/**
 * @brief returns address of the network interface
 */
//==========================================================================================================================================
oC_Net_Ipv4_t oC_Netif_GetIpv4Address( oC_Netif_t Netif )
{
    oC_Net_Ipv4_t ip = 0;

    if(oC_Netif_IsCorrect(Netif))
    {
        ip = Netif->IPv4Info.IP;
    }

    return ip;
}


//==========================================================================================================================================
/**
 * @brief reads IPv6 address of the network interface
 */
//==========================================================================================================================================
bool oC_Netif_ReadIpv6Address( oC_Netif_t Netif , oC_Net_Ipv6_t * outAddress )
{
    bool success = false;

    if(oC_Netif_IsCorrect(Netif) && isram(outAddress))
    {
        memcpy(outAddress,&Netif->IPv6Info.IPv6,sizeof(oC_Net_Ipv6_t));
        success = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief updates link status in the network interface object
 *
 * The function is for updating link status of the network interface for the function #oC_Netif_GetLinkStatus. It reads current link status
 * from the driver and saves it for later.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_UpdateLinkStatus( oC_Netif_t Netif )
{
    oC_Net_Info_t info;

    memset(&info,0,sizeof(info));

    return oC_Netif_ReadNetInfo(Netif,&info);
}

//==========================================================================================================================================
/**
 * @brief returns true if IP is assigned
 */
//==========================================================================================================================================
bool oC_Netif_IsIpAssigned( oC_Netif_t Netif )
{
    bool assigned = false;

    if(oC_Netif_IsCorrect(Netif))
    {
        assigned =  Netif->IPv4Info.IP > 0
                 && Netif->IPv4Info.IP < IP(255,255,255,255)
                 && Netif->IPv4Info.IpExpiredTimestamp > oC_KTime_GetTimestamp();
    }

    return assigned;
}

//==========================================================================================================================================
/**
 * @brief checks if the static ip is valid
 */
//==========================================================================================================================================
bool oC_Netif_IsStaticIpValid( oC_Netif_t Netif )
{
    bool valid = false;

    if(oC_Netif_IsCorrect(Netif) && Netif->IPv4Info.IP != 0)
    {
        valid = oC_Net_Ipv4_IsAddressInSubnet(Netif->IPv4Info.IP, Netif->IPv4Info.NetIP, Netif->IPv4Info.Netmask);
    }

    return valid;
}

//==========================================================================================================================================
/**
 * @brief returns true if the IP is static
 */
//==========================================================================================================================================
bool oC_Netif_IsIpStatic( oC_Netif_t Netif )
{
    bool staticIp = false;

    if(oC_Netif_IsCorrect(Netif))
    {
        staticIp = Netif->IPv4Info.IP != 0 && Netif->IPv4Info.StaticIP;
    }

    return staticIp;
}

//==========================================================================================================================================
/**
 * @brief waits for the link and updates IP
 *
 * The function waits for the link (#oC_Net_LinkStatus_Up), and updates IP address if it is needed
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param Timeout           Maximum time to wait on the link
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_WaitOnLink( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif) , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( Netif->Configured         , oC_ErrorCode_NotConfiguredYet   )
        )
    {
        oC_Timestamp_t startTimestamp   = oC_KTime_GetTimestamp();
        oC_Timestamp_t endTimestamp     = startTimestamp + Timeout;
        oC_Timestamp_t currentTimestamp = oC_KTime_GetTimestamp();

        errorCode = oC_ErrorCode_Timeout;

        while(currentTimestamp < endTimestamp && Netif->LinkStatus != oC_Net_LinkStatus_Up)
        {
            if(!ErrorCode(oC_Netif_UpdateLinkStatus(Netif)))
            {
                break;
            }
            else
            {
                sleep(LINK_STATUS_CHECKING_PERIOD);
                currentTimestamp = oC_KTime_GetTimestamp();
            }
        }
        if(Netif->LinkStatus == oC_Net_LinkStatus_Up)
        {
            errorCode = oC_Netif_UpdateIp(Netif,Timeout);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief updates IP in the given network interface
 *
 * The function is for updating IP of the given network interface.
 *
 * @warning
 * The network interface has to be connected to the network!
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param Timeout           Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_LinkNotDetected                 | The given network interface has not link up status
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_UpdateIp( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif) , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( Netif->Configured         , oC_ErrorCode_NotConfiguredYet   )
     && ErrorCode     ( oC_Netif_UpdateLinkStatus(Netif)                            )
     && ErrorCondition( Netif->LinkStatus         , oC_ErrorCode_LinkNotDetected    )
     && ErrorCondition( Timeout > 0               , oC_ErrorCode_TimeNotCorrect     )
        )
    {
        oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + Timeout;
        oC_Timestamp_t dhcpEndTimestamp = endTimestamp - oC_MIN(s(3), Timeout / 2);

        if(
            ErrorCode( oC_Dhcp_RequestIp        ( Netif , gettimeout(dhcpEndTimestamp)     ))
         && ErrorCode( oC_Netif_SendAddressProbe( Netif , gettimeout(dhcpEndTimestamp)     ))
            )
        {
            oC_News_SaveFormatted("DHCP IP has been assigned for %s", Netif->FriendlyName);
            char ipString[20] = {0};
            oC_Net_Ipv4AddressToString(Netif->IPv4Info.IP, ipString, sizeof(ipString));
            kdebuglog(oC_LogType_GoodNews, "DHCP IP has been received for %s: %s", Netif->FriendlyName, ipString);
            errorCode = oC_ErrorCode_None;
        }
        else if(errorCode == oC_ErrorCode_Timeout && ErrorCode( oC_Netif_AssignStaticIp(Netif, gettimeout(endTimestamp)) ))
        {
            oC_News_SaveFormatted("Static IP has been assigned for %s", Netif->FriendlyName);
            char ipString[20] = {0};
            oC_Net_Ipv4AddressToString(Netif->IPv4Info.IP, ipString, sizeof(ipString));
            kdebuglog(oC_LogType_GoodNews, "Static IP has been assigned for %s: %s", Netif->FriendlyName, ipString);
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            oC_Timestamp_t currentTimestamp = oC_KTime_GetTimestamp();
            if ( currentTimestamp >= Netif->IPv4Info.IpExpiredTimestamp )
            {
                Netif->IPv4Info.IP = 0;
                kdebuglog(oC_LogType_Warning, "IP has been lost for %s: %s timestamp: %llu ip expired timestamp: %llu",
                          Netif->FriendlyName, oC_GetErrorString(errorCode), currentTimestamp, Netif->IPv4Info.IpExpiredTimestamp);
                oC_News_SaveFormatted("IP has been lost for %s", Netif->FriendlyName);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets thread for receiving packets
 *
 * The function sets the thread for receiving packet of the Netif.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param Thread            Thread object to set
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_ThreadNotCorrect                | `Thread` is not correct
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SetReceiveThread( oC_Netif_t Netif , oC_Thread_t Thread )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)  , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( Netif->Configured          , oC_ErrorCode_NotConfiguredYet   )
     && ErrorCondition( oC_Thread_IsCorrect(Thread), oC_ErrorCode_ThreadNotCorrect   )
        )
    {
        Netif->ReceiveThread    = Thread;
        errorCode               = oC_ErrorCode_None;
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief enables listen mode
 *
 * In listen mode packets are not followed to the upper layer. Thanks to that it is possible to listen all packets send to the netif. Only
 * root user can call this function.
 *
 *  @param Netif             Netif object from the #oC_Netif_New function
 *  @param Enabled           true if enabled, false if disabled
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_PermissionDenied                | You are not root user
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SetListenMode( oC_Netif_t Netif , bool Enabled )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)   , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( Netif->Configured           , oC_ErrorCode_NotConfiguredYet   )
     && ErrorCondition( iscurroot()                 , oC_ErrorCode_PermissionDenied   )
        )
    {
        Netif->ListenMode       = Enabled;
        errorCode               = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns true if listen mode is enabled
 */
//==========================================================================================================================================
bool oC_Netif_GetListenMode( oC_Netif_t Netif )
{
    return oC_Netif_IsCorrect(Netif) && Netif->ListenMode;
}

//==========================================================================================================================================
/**
 * @brief sets thread for receiving packets
 *
 * The function reads the thread for receiving packet of the Netif.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param Thread            Thread object to set
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_ThreadNotSet                    | Receive thread has not been set yet
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outThread` does not point to the RAM section
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_ReadReceiveThread( oC_Netif_t Netif , oC_Thread_t * outThread )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)                   , oC_ErrorCode_ObjectNotCorrect      )
     && ErrorCondition( Netif->Configured                           , oC_ErrorCode_NotConfiguredYet      )
     && ErrorCondition( isram(outThread)                            , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( oC_Thread_IsCorrect(Netif->ReceiveThread)   , oC_ErrorCode_ThreadNotSet          )
        )
    {
        *outThread  = Netif->ReceiveThread;
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends frame that probes the address
 *
 * The function sends address probe packet - the special packet that confirms the selected IP address and that prevents address conflicts
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param Timeout           Maximum time to wait for the packet sending
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_NotConfiguredYet                | The configuration function was not called before
 *  oC_ErrorCode_TimeNotCorrect                  | Time is lower than 0
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_SendAddressProbe( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)                   , oC_ErrorCode_ObjectNotCorrect      )
     && ErrorCondition( Netif->Configured                           , oC_ErrorCode_NotConfiguredYet      )
     && ErrorCondition( Timeout > 0                                 , oC_ErrorCode_TimeNotCorrect        )
        )
    {
        oC_Net_Address_t destination = {
                        .Type = oC_Net_AddressType_IPv4 ,
                        .IPv4 = Netif->IPv4Info.IP
        };
        oC_DefaultString_t ipStr = {0};
        oC_Net_AddressToString(&destination, ipStr, sizeof(ipStr));
        kdebuglog(oC_LogType_Info, "Sending Address Probe for %s\n", ipStr);
        errorCode = SendArpPacket(Netif,ArpOperation_Request, &Netif->HardwareBroadcastAddress, &destination, Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief assigns static IP address
 *
 * The function assigns static IP address to the network interface. It sends address probe packet to the network to confirm the address.
 *
 * @param Netif             Netif object from the #oC_Netif_New function
 * @param Timeout           Maximum time to wait for the packet sending
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ObjectNotCorrect                | `Netif` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_StaticIpNotSet                  | The static IP address is not set
 *  oC_ErrorCode_IpAddressNotCorrect             | The IP address is not correct
 *  oC_ErrorCode_TimeNotCorrect                  | Time is lower than 0
 * 
 * @note
 * More error codes can be returned by the function #oC_Netif_SendAddressProbe
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Netif_AssignStaticIp( oC_Netif_t Netif , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Netif_IsCorrect(Netif)                   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( oC_Netif_IsIpStatic(Netif)                  , oC_ErrorCode_StaticIpNotSet           )
     && ErrorCondition( oC_Netif_IsStaticIpValid(Netif)             , oC_ErrorCode_IpAddressNotCorrect      )
     && ErrorCode( oC_Netif_SendAddressProbe(Netif, Timeout) )
        )
    {
        errorCode = oC_ErrorCode_None;
    }
    else
    {
        kdebuglog(oC_LogType_Warning, "Cannot assign static IP: %R.\n", errorCode);
    }

    return errorCode;
}

/** @} */
#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief translates hardware type to ARP hardware type
 */
//==========================================================================================================================================
static ArpHardwareType_t HardwareTypeToArpHardwareType( oC_Net_HardwareType_t Type )
{
    ArpHardwareType_t arpType = ArpHardwareType_Unknown;

    switch(Type)
    {
        case oC_Net_HardwareType_Ethernet: arpType = ArpHardwareType_Ethernet; break;
        default: arpType = ArpHardwareType_Unknown; break;
    }

    return arpType;
}

//==========================================================================================================================================
/**
 * @brief translates ARP protocol type to packet type
 */
//==========================================================================================================================================
static oC_Net_PacketType_t ArpProtocolToPacketType( ArpProtocolType_t Type )
{
    oC_Net_PacketType_t packetType = 0;

    switch(Type)
    {
        case ArpProtocolType_IPv4: packetType = oC_Net_PacketType_IPv4; break;
        case ArpProtocolType_IPv6: packetType = oC_Net_PacketType_IPv6; break;

        default: packetType = oC_Net_PacketType_Invalid; break;
    }

    return packetType;
}

//==========================================================================================================================================
/**
 * @brief translates ARP protocol type from packet type
 */
//==========================================================================================================================================
static ArpProtocolType_t PacketTypeToArpProtocol( oC_Net_PacketType_t Type )
{
    ArpProtocolType_t arpType = 0;

    switch(Type)
    {
        case oC_Net_PacketType_IPv4: arpType = ArpProtocolType_IPv4; break;
        case oC_Net_PacketType_IPv6: arpType = ArpProtocolType_IPv6; break;

        default: arpType = ArpProtocolType_Unknown; break;
    }

    return arpType;
}

//==========================================================================================================================================
/**
 * @brief reads hw address from the ARP list in the netif
 */
//==========================================================================================================================================
static bool ReadHardwareAddressOf( oC_Netif_t Netif , const oC_Net_Address_t * Address , oC_Net_HardwareAddress_t * outHardwareAddress )
{
    bool found = false;

    foreach(Netif->AddressResolutionList,entry)
    {
        if(
            (Address->Type == oC_Net_AddressType_IPv4 && entry->IpAddress.IPv4 == Address->IPv4)
         || (Address->Type == oC_Net_AddressType_IPv6 && entry->IpAddress.IPv6.HighPart == Address->IPv6.HighPart && entry->IpAddress.IPv6.LowPart == Address->IPv6.LowPart)
            )
        {
            memcpy(outHardwareAddress,&entry->HardwareAddress,sizeof(*outHardwareAddress));
            found = true;
        }
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief requests hardware address via ARP protocol
 */
//==========================================================================================================================================
static oC_ErrorCode_t RequestHardwareAddressViaArp( oC_Netif_t Netif , const oC_Net_Address_t * Address , oC_Net_HardwareAddress_t * outHardwareAddress , oC_Time_t Timeout )
{
    oC_ErrorCode_t    errorCode = oC_ErrorCode_ImplementError;
    ArpHardwareType_t arpType   = HardwareTypeToArpHardwareType( Netif->HardwareType );

    if(
        ErrorCondition( Address->Type == oC_Net_AddressType_IPv4 , oC_ErrorCode_AddressTypeNotCorrect  )
     && ErrorCondition( arpType       != ArpHardwareType_Unknown , oC_ErrorCode_UnknownHardwareType    )
     && ErrorCondition( Netif->HardwareAddress.Filled            , oC_ErrorCode_UnknownHardwareAddress )
        )
    {
        oC_MemorySize_t  hardwareAddressSize = oC_Net_GetHardwareAddressSize(Netif->HardwareType);
        oC_MemorySize_t  addressSize         = oC_Net_GetAddressSize(Address);
        oC_MemorySize_t  size                = sizeof(ArpPacket_t)
                                             + ( hardwareAddressSize * 2 )
                                             + ( addressSize         * 2 )
                                             - sizeof(uint32_t);
        oC_Net_Frame_t*  frame               = ksmartalloc( sizeof(oC_Net_Frame_t) , &Allocator, AllocationFlags_ZeroFill );
        uint8_t*         arrayPointer        = NULL;
        oC_Timestamp_t   endTimestamp        = oC_KTime_GetTimestamp() + Timeout;
        uint16_t         arpPacketSize       = sizeof(ArpPacket_t) + 2 * hardwareAddressSize + 2 * addressSize;
        ArpPacket_t*     arpPacket           = ksmartalloc( arpPacketSize , &Allocator, AllocationFlags_ZeroFill );


        if(
            ErrorCondition( frame != NULL && arpPacket != NULL , oC_ErrorCode_AllocationError )
            )
        {
            //==============================================================================================================================
            /*
             *                                              Initialization of the FRAME DATA
             */
            //==============================================================================================================================
            frame->Packet = (void*)arpPacket;

            //==============================================================================================================================
            /*
             *                                              Preparation of ARP packet
             */
            //==============================================================================================================================
            arpPacket->HTYPE = arpType;
            arpPacket->PTYPE = ArpProtocolType_IPv4;
            arpPacket->HLEN  = (uint8_t)hardwareAddressSize;
            arpPacket->PLEN  = (uint8_t)addressSize;
            arpPacket->OPER  = ArpOperation_Request;

            arrayPointer = arpPacket->AddressArray;

            /* Adding Hardware Source Address */
            memcpy(arrayPointer, &Netif->HardwareAddress.Address, hardwareAddressSize);
            arrayPointer += hardwareAddressSize;

            /* Adding Protocol Source Address */
            memcpy(arrayPointer, &Netif->IPv4Info.IP, addressSize);
            arrayPointer += addressSize;

            /* Reserve place for hardware destination address */
            arrayPointer += hardwareAddressSize;

            /* Adding Protocol Destination Address */
            memcpy(arrayPointer, &Address->IPv4, addressSize);
            arrayPointer += addressSize;

            //==============================================================================================================================
            /*
             *                                          Preparation of hardware frame
             */
            //==============================================================================================================================
            memcpy(&frame->Destination, &Netif->HardwareBroadcastAddress, sizeof(oC_Net_HardwareAddress_t));
            memcpy(&frame->Source     , &Netif->HardwareAddress         , sizeof(oC_Net_HardwareAddress_t));

            frame->Size         = size;
            frame->FrameType    = oC_Net_FrameType_ARP;

            ConvertArpToNetworkEndianess(arpPacket);

            //==============================================================================================================================
            /*
             *                                          Sending and receiving request
             */
            //==============================================================================================================================
            if( ErrorCode( oC_Driver_SendFrame( Netif->Driver, Netif->Context, frame, Timeout )) )
            {
                errorCode = oC_ErrorCode_HostNotAvailable;

                while(oC_Event_WaitForState(Netif->NewArpAvailableEvent,Netif->SizeOfArpList,oC_Event_StateMask_DifferentThan,oC_KTime_CalculateTimeout(endTimestamp)))
                {
                    if(ReadHardwareAddressOf(Netif,Address,outHardwareAddress))
                    {
                        errorCode = oC_ErrorCode_None;
                        break;
                    }
                }
            }
        }
        if(arpPacket != NULL)
        {
            ksmartfree(arpPacket,sizeof(oC_Net_Frame_t), AllocationFlags_Default);
        }

        if(frame != NULL)
        {
            ksmartfree(frame,sizeof(oC_Net_Frame_t), AllocationFlags_Default);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief saves HW address in ARP list
 */
//==========================================================================================================================================
static void SaveHardwareAddress( oC_Netif_t Netif , oC_Net_HardwareAddress_t * HardwareAddress, oC_Net_Address_t * IpAddress )
{
    if(oC_Netif_IsAddressInSubnet(Netif,IpAddress)
     && !(IpAddress->Type == oC_Net_AddressType_IPv4 && IpAddress->IPv4 == Netif->IPv4Info.BroadcastIP)
        )
    {
        AddressResolutionEntry_t * foundEntry = NULL;

        foreach(Netif->AddressResolutionList,entry)
        {
            if(
                (IpAddress->Type == oC_Net_AddressType_IPv4 && entry->IpAddress.IPv4 == IpAddress->IPv4)
             || (memcmp(HardwareAddress,&entry->HardwareAddress,sizeof(entry->HardwareAddress)) == 0)
                )
            {
                foundEntry = entry;
            }
        }

        if(foundEntry != NULL)
        {
            memcpy(&foundEntry->HardwareAddress, HardwareAddress, sizeof(foundEntry->HardwareAddress));
            memcpy(&foundEntry->IpAddress      , IpAddress      , sizeof(oC_Net_Address_t)           );
        }
        else
        {
            AddressResolutionEntry_t * entry = kmalloc(sizeof(AddressResolutionEntry_t), &Allocator, AllocationFlags_ZeroFill);


            if(oC_SaveIfFalse("SaveHWAddress - cannot save address - ",  entry != NULL , oC_ErrorCode_AllocationError ))
            {
                memcpy(&entry->HardwareAddress, HardwareAddress   , sizeof(oC_Net_HardwareAddress_t) );
                memcpy(&entry->IpAddress      , IpAddress         , sizeof(oC_Net_Address_t)         );

                oC_List_PushBack(Netif->AddressResolutionList,entry,&Allocator);

                oC_Event_SetState(Netif->NewArpAvailableEvent,++Netif->SizeOfArpList);
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief saves hardware address from ARP message
 */
//==========================================================================================================================================
static void SaveHardwareAddressFromArp( oC_Netif_t Netif , ArpPacket_t * Packet , oC_Time_t Timeout )
{
    ArpHardwareType_t   realArpHwType       = HardwareTypeToArpHardwareType( Netif->HardwareType );
    oC_MemorySize_t     hardwareAddressSize = oC_Net_GetHardwareAddressSize(Netif->HardwareType);
    oC_Net_PacketType_t packetType          = ArpProtocolToPacketType(Packet->PTYPE);

    if( realArpHwType != ArpHardwareType_Unknown && Packet->HTYPE == realArpHwType && hardwareAddressSize > 0 && ((uint8_t)hardwareAddressSize) == Packet->HLEN && packetType != 0)
    {
        uint8_t *                arrayPointer = Packet->AddressArray;
        oC_Net_HardwareAddress_t senderHardwareAddress;
        oC_Net_HardwareAddress_t targetHardwareAddress;
        oC_Net_Address_t         senderAddress;
        oC_Net_Address_t         targetAddress;
        uint16_t                 addressSize  = Packet->PLEN;
        oC_MemorySize_t          size         = sizeof(ArpPacket_t)
                                              + ( hardwareAddressSize * 2 )
                                              + ( addressSize         * 2 )
                                              - sizeof(uint32_t);
        bzero( &senderHardwareAddress, sizeof( senderHardwareAddress ) );
        bzero( &targetHardwareAddress, sizeof( targetHardwareAddress ) );
        bzero( &senderAddress        , sizeof( senderAddress         ) );
        bzero( &targetAddress        , sizeof( targetAddress         ) );

        /* Moving the arrayPointer to the start of the address array */
        arrayPointer = Packet->AddressArray;

        /* Reading Sender Hardware Address */
        memcpy( &senderHardwareAddress.Address, arrayPointer, hardwareAddressSize );
        arrayPointer += hardwareAddressSize;

        /* Reading Sender Protocol Address */
        memcpy( &senderAddress.IPv4, arrayPointer, addressSize );
        arrayPointer += addressSize;

        /* Reading Target Hardware Address */
        memcpy( &targetHardwareAddress.Address, arrayPointer, hardwareAddressSize );
        arrayPointer += hardwareAddressSize;

        /* Adding Protocol Destination Address */
        memcpy( &targetAddress.IPv4, arrayPointer, addressSize);
        arrayPointer += addressSize;

        /* If values of this types were changed, the function require to be refactored */
        oC_STATIC_ASSERT(   oC_Net_PacketType_IPv4 == (oC_Net_PacketType_t)oC_Net_AddressType_IPv4
                         && oC_Net_PacketType_IPv6 == (oC_Net_PacketType_t)oC_Net_AddressType_IPv6 , "Values of types oC_Net_PacketType_t oC_Net_AddressType_t has to be the same!");

        senderAddress.Type = (oC_Net_AddressType_t)packetType;
        targetAddress.Type = (oC_Net_AddressType_t)packetType;

        senderHardwareAddress.Filled = true;
        targetHardwareAddress.Filled = true;

        SaveHardwareAddress(Netif,&senderHardwareAddress,&senderAddress);

        if(Packet->OPER == ArpOperation_Reply)
        {
            SaveHardwareAddress(Netif,&targetHardwareAddress,&targetAddress);
        }
        else if(Packet->OPER == ArpOperation_Request)
        {
            if(packetType == oC_Net_PacketType_IPv4 && targetAddress.IPv4 == Netif->IPv4Info.IP && senderAddress.IPv4 != Netif->IPv4Info.IP)
            {
                /* Preparation of the respond for the ARP */
                oC_Net_Frame_t frame;

                bzero(&frame,sizeof(frame));

                Packet->OPER  = ArpOperation_Reply;
                frame.Packet = (void*)Packet;

                arrayPointer  = Packet->AddressArray;

                /* Adding Hardware Source Address */
                memcpy(arrayPointer, &Netif->HardwareAddress.Address, hardwareAddressSize   );
                arrayPointer += hardwareAddressSize;

                /* Adding Protocol Source Address */
                memcpy(arrayPointer, &Netif->IPv4Info.IP            , addressSize           );
                arrayPointer += addressSize;

                /* Adding Hardware Destination Address */
                memcpy(arrayPointer, &senderHardwareAddress         , hardwareAddressSize   );
                arrayPointer += hardwareAddressSize;

                /* Adding Protocol Destination Address */
                memcpy(arrayPointer, &senderAddress.IPv4            , addressSize           );
                arrayPointer += addressSize;

                //==============================================================================================================================
                /*
                 *                                          Preparation of hardware frame
                 */
                //==============================================================================================================================
                memcpy(&frame.Destination.Address , &senderHardwareAddress          , hardwareAddressSize             );
                memcpy(&frame.Source              , &Netif->HardwareAddress         , sizeof(oC_Net_HardwareAddress_t));

                frame.Size                  = size;
                frame.FrameType             = oC_Net_FrameType_ARP;
                frame.Destination.Filled    = true;

                ConvertArpToNetworkEndianess(Packet);

                oC_SaveIfErrorOccur("SaveHardwareAddressFromArp: Cannot respond for ARP - ", oC_Driver_SendFrame(Netif->Driver,Netif->Context,&frame,Timeout));
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief sends ARP packet
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendArpPacket( oC_Netif_t Netif , ArpOperation_t Operation , oC_Net_HardwareAddress_t * TargetHwAddress, oC_Net_Address_t * IpAddress , oC_Time_t Timeout )
{
    oC_ErrorCode_t      errorCode               = oC_ErrorCode_ImplementError;
    oC_MemorySize_t     hardwareAddressSize     = oC_Net_GetHardwareAddressSize(Netif->HardwareType);
    uint16_t            addressSize             = (IpAddress->Type == oC_Net_AddressType_IPv4) ? sizeof(oC_Net_Ipv4_t) :
                                                  (IpAddress->Type == oC_Net_AddressType_IPv6) ? sizeof(oC_Net_Ipv6_t) : 0;
    uint16_t            packetSize              = sizeof(ArpPacket_t) - sizeof(uint32_t)
                                                + hardwareAddressSize * 2
                                                + addressSize         * 2;

    if(
        ErrorCondition( hardwareAddressSize > 0 , oC_ErrorCode_TypeNotCorrect           )
     && ErrorCondition( addressSize         > 0 , oC_ErrorCode_AddressTypeNotCorrect    )
        )
    {
        ArpPacket_t * arpPacket = kmalloc( packetSize , &Allocator, AllocationFlags_ZeroFill);

        if(ErrorCondition( arpPacket != NULL , oC_ErrorCode_AllocationError ))
        {
            arpPacket->HLEN     = hardwareAddressSize;
            arpPacket->PLEN     = addressSize;
            arpPacket->OPER     = Operation;
            arpPacket->HTYPE    = HardwareTypeToArpHardwareType(Netif->HardwareType);
            arpPacket->PTYPE    = PacketTypeToArpProtocol((oC_Net_PacketType_t)IpAddress->Type);

            void * senderHardwareAddress = &arpPacket->AddressArray[ 0 ];
            void * senderProtocolAddress = &arpPacket->AddressArray[ hardwareAddressSize ];
            void * targetHardwareAddress = &arpPacket->AddressArray[ hardwareAddressSize + addressSize ];
            void * targetProtocolAddress = &arpPacket->AddressArray[ hardwareAddressSize + addressSize + hardwareAddressSize ];

            memcpy( senderHardwareAddress, &Netif->HardwareAddress.Address                              , hardwareAddressSize );
            memcpy( senderProtocolAddress, IpAddress->Type == oC_Net_AddressType_IPv4 ?
                                           (void*)&Netif->IPv4Info.IP : (void*)&Netif->IPv6Info.IPv6    , addressSize         );
            memcpy( targetHardwareAddress, &TargetHwAddress->Address                                    , hardwareAddressSize );
            memcpy( targetProtocolAddress, IpAddress->Type == oC_Net_AddressType_IPv4 ?
                                           (void*)&IpAddress->IPv4 : (void*)&IpAddress->IPv6            , addressSize         );

            ConvertArpToNetworkEndianess(arpPacket);

            oC_Net_Frame_t frame;

            frame.ConstPacket = (void*)arpPacket;
            frame.FrameType   = oC_Net_FrameType_ARP;
            frame.Size        = packetSize;

            memcpy( &frame.Source       , &Netif->HardwareAddress   , sizeof(oC_Net_HardwareAddress_t));
            memcpy( &frame.Destination  , TargetHwAddress           , sizeof(oC_Net_HardwareAddress_t));

            if( ErrorCode( oC_Driver_SendFrame(Netif->Driver, Netif->Context, &frame, Timeout) ) )
            {
                if(Netif->IPv4Info.StaticIP)
                {
                    Netif->IPv4Info.IpExpiredTimestamp = oC_KTime_GetTimestamp() + Netif->IPv4Info.LeaseTime;
                }
                errorCode = oC_ErrorCode_None;
            }

            oC_SaveIfFalse("SendArpPacket: cannot release ARP packet", kfree(arpPacket,0) , oC_ErrorCode_ReleaseError);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief converts ARP packet from network endianess
 */
//==========================================================================================================================================
static void ConvertArpFromNetworkEndianess( ArpPacket_t * Packet )
{
    Packet->HTYPE = oC_Net_ConvertUint16FromNetworkEndianess(Packet->HTYPE);
    Packet->PTYPE = oC_Net_ConvertUint16FromNetworkEndianess(Packet->PTYPE);
    Packet->OPER  = oC_Net_ConvertUint16FromNetworkEndianess(Packet->OPER);

    uint8_t         plen  = Packet->PLEN;
    uint8_t         hlen  = Packet->HLEN;
    oC_Net_Ipv4_t   ip    = 0;

    if(Packet->PTYPE == ArpProtocolType_IPv4)
    {
        uint8_t * arrayPointer = Packet->AddressArray;

        arrayPointer += hlen; // Skipping sender HW address

        /* Creating a temporary variable to prevent unaligned memory access */
        ip = (arrayPointer[0] <<  0)
           | (arrayPointer[1] <<  8)
           | (arrayPointer[2] << 16)
           | (arrayPointer[3] << 24);

        ip = oC_Net_ConvertUint32ToNetworkEndianess(ip);

        arrayPointer[0] = (ip >>  0) & 0xFF ;
        arrayPointer[1] = (ip >>  8) & 0xFF ;
        arrayPointer[2] = (ip >> 16) & 0xFF ;
        arrayPointer[3] = (ip >> 24) & 0xFF ;

        arrayPointer += plen; // sender IP address

        arrayPointer += hlen; // Skipping target HW address

        /* Creating a temporary variable to prevent unaligned memory access */
        ip = (arrayPointer[0] <<  0)
           | (arrayPointer[1] <<  8)
           | (arrayPointer[2] << 16)
           | (arrayPointer[3] << 24);

        ip = oC_Net_ConvertUint32ToNetworkEndianess(ip);

        arrayPointer[0] = (ip >>  0) & 0xFF ;
        arrayPointer[1] = (ip >>  8) & 0xFF ;
        arrayPointer[2] = (ip >> 16) & 0xFF ;
        arrayPointer[3] = (ip >> 24) & 0xFF ;
    }
}

//==========================================================================================================================================
/**
 * @brief converts ARP packet to network endianess
 */
//==========================================================================================================================================
static void ConvertArpToNetworkEndianess( ArpPacket_t * Packet )
{
    ArpProtocolType_t protocolType = Packet->PTYPE;
    oC_Net_Ipv4_t     ip           = 0;

    Packet->HTYPE = oC_Net_ConvertUint16ToNetworkEndianess(Packet->HTYPE);
    Packet->PTYPE = oC_Net_ConvertUint16ToNetworkEndianess(Packet->PTYPE);
    Packet->OPER  = oC_Net_ConvertUint16ToNetworkEndianess(Packet->OPER);
    uint8_t plen  = Packet->PLEN;
    uint8_t hlen  = Packet->HLEN;

    if(protocolType == ArpProtocolType_IPv4)
    {
        uint8_t * arrayPointer = Packet->AddressArray;

        arrayPointer += hlen; // Skipping sender HW address

        /* Creating a temporary variable to prevent unaligned memory access */
        ip = (arrayPointer[0] <<  0)
           | (arrayPointer[1] <<  8)
           | (arrayPointer[2] << 16)
           | (arrayPointer[3] << 24);

        ip = oC_Net_ConvertUint32ToNetworkEndianess(ip);

        arrayPointer[0] = (ip >>  0) & 0xFF ;
        arrayPointer[1] = (ip >>  8) & 0xFF ;
        arrayPointer[2] = (ip >> 16) & 0xFF ;
        arrayPointer[3] = (ip >> 24) & 0xFF ;

        arrayPointer += plen; // sender IP address

        arrayPointer += hlen; // Skipping target HW address

        /* Creating a temporary variable to prevent unaligned memory access */
        ip = (arrayPointer[0] <<  0)
           | (arrayPointer[1] <<  8)
           | (arrayPointer[2] << 16)
           | (arrayPointer[3] << 24);

        ip = oC_Net_ConvertUint32ToNetworkEndianess(ip);

        arrayPointer[0] = (ip >>  0) & 0xFF ;
        arrayPointer[1] = (ip >>  8) & 0xFF ;
        arrayPointer[2] = (ip >> 16) & 0xFF ;
        arrayPointer[3] = (ip >> 24) & 0xFF ;
    }
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

