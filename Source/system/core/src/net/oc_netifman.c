/** ****************************************************************************************************************************************
 *
 * @brief      Files with sources of the netif manager
 *
 * @file       oc_netifman.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_netifman.h>
#include <oc_netif_cfg.c>
#include <oc_intman.h>
#include <oc_driverman.h>
#include <oc_processman.h>
#include <oc_debug.h>
#include <oc_semaphore.h>
#include <oc_module.h>
#include <oc_userman.h>
#include <oc_event.h>
#include <oc_streamman.h>
#include <oc_icmp.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define SOFTWARE_RING_SIZE          10
#define STACK_SIZE                  kB(2)
#define DAEMON_SLEEP_TIME           s(1)

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    bool                Used;
    oC_Timestamp_t      Timestamp;
    oC_Net_Packet_t     Packet;
    oC_Netif_t          Netif;
} Packet_t;

typedef struct
{
    Packet_t            Packets[SOFTWARE_RING_SIZE];
    uint32_t            Count;
    oC_Event_t          DataAvailable;
} SoftwareRing_t;

typedef oC_NetifMan_RoutingTableEntry_t RoutingTableEntry_t;

typedef struct
{
    oC_List(RoutingTableEntry_t*)   RoutingActiveEntryList;
    oC_List(RoutingTableEntry_t*)   RoutingTable;
} RoutingTableData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static        void                  PutToSoftwareRing           ( SoftwareRing_t * SoftwareRing , oC_Netif_t Netif ,  oC_Net_Packet_t * Packet );
static        bool                  GetFromSoftwareRing         ( SoftwareRing_t * SoftwareRing , const oC_Net_Address_t * Address  , oC_Net_Packet_t * outPacket , oC_Netif_t * outNetif , oC_NetifMan_PacketFilterFunction_t FilterFunction , const void * Parameter);
static inline bool                  IsSoftwareRingFull          ( SoftwareRing_t * SoftwareRing );
static        RoutingTableEntry_t*  GetActiveRoutingTableEntry  ( const oC_Net_Address_t * Address );
static        void                  DaemonThread                ( void * Argument );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * Allocator for objects from the module
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "NetifMan" ,
};
static oC_List(oC_Netif_t)  NetifList               = NULL;       //!< List of netif objects
static oC_Process_t         Process                 = NULL;       //!< NetifMan process
static oC_Thread_t          Thread                  = NULL;       //!< Main thread
static uint32_t             NumberOfIgnoredPackets  = 0;          //!< Number of packets removed from the ring
static uint32_t             NumberOfReceivedPackets = 0;          //!< Number of received packets
static SoftwareRing_t       ReceivedPackets;
static RoutingTableData_t   RoutingTableData;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
/**
 * @addtogroup NetifMan
 * @{
 */

//==========================================================================================================================================
/**
 * @brief initializes the module
 *
 * The function is for initializing the module. It also creates and prepares for configuration the module list of `Netif` objects.
 *
 * @warning
 * The function disables interrupts for enabling time! It can take a lot of time (event few seconds!)
 *
 * @return code of error or `oC_ErrorCode_None`
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                         | Description
 * --------------------------------------|--------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleIsTurnedOn         | Module is already turned on
 * oC_ErrorCode_AllocationError          | Cannot allocate memory for the module
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_NetifMan))
    {
        //--------------------------------------------------------//
        //                VARIABLES INITIALIZATION                //
        //--------------------------------------------------------//
        memset(&ReceivedPackets,0,sizeof(ReceivedPackets));
        memset(&RoutingTableData,0,sizeof(RoutingTableData));

        NetifList                               = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);
        RoutingTableData.RoutingActiveEntryList = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);
        RoutingTableData.RoutingTable           = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);
        ReceivedPackets.Count                   = 0;
        ReceivedPackets.DataAvailable           = oC_Event_New(0,&Allocator,AllocationFlags_CanWait1Second);
        Process                                 = NULL;
        Thread                                  = NULL;
        NumberOfIgnoredPackets                  = 0;
        NumberOfReceivedPackets                 = 0;

        if(
            ErrorCondition(NetifList != NULL                                , oC_ErrorCode_AllocationError    )
         && ErrorCondition(ReceivedPackets.DataAvailable    != NULL         , oC_ErrorCode_AllocationError    )
         && ErrorCondition(RoutingTableData.RoutingActiveEntryList != NULL  , oC_ErrorCode_AllocationError    )
         && ErrorCondition(RoutingTableData.RoutingTable     != NULL        , oC_ErrorCode_AllocationError    )
            )
        {
            bool pushed = false;
            errorCode   = oC_ErrorCode_None;

            /* The module is now enabled... */
            oC_Module_TurnOn(oC_Module_NetifMan);

            (void)pushed;

            //--------------------------------------------------------//
            //                ADDING DEFAULT INTERFACES               //
            //--------------------------------------------------------//
#define NETIF_NAME(CONFIG_NAME)     oC_1WORD_FROM_2(CONFIG_NAME,_Netif)
#define AUTO    NULL
#define ADD_NET(FRIENDLY_NAME,IP,NETMASK,DRIVER_NAME,CONFIG_NAME)      \
                                                            oC_Netif_t NETIF_NAME(CONFIG_NAME) = oC_Netif_New( FRIENDLY_NAME, IP, NETMASK , &DRIVER_NAME, &CONFIG_NAME); \
                                                            oC_SaveIfFalse("NetifMan::TurnOn - cannot create interface " FRIENDLY_NAME " - " , NETIF_NAME(CONFIG_NAME) != NULL , oC_ErrorCode_AllocationError ); \
                                                            pushed = oC_List_PushBack(NetifList,NETIF_NAME(CONFIG_NAME),&Allocator);\
                                                            oC_SaveIfFalse("NetifMan::TurnOn - cannot add to list interface " FRIENDLY_NAME " - " , pushed , oC_ErrorCode_CannotAddObjectToList);

            CFG_NETIF_LIST(ADD_NET,UNIVERSAL_DONT_ADD);

#undef NETIF_NAME
#undef ADD_NET
#undef AUTO

            //--------------------------------------------------------//
            //           CREATING DEFAULT ROUTING TABLE               //
            //--------------------------------------------------------//
#define ADD_ENTRY(DESTINATION,NETMASK,COST,FRIENDLY_NAME)       \
                                        { \
                                            oC_Netif_t netif = oC_NetifMan_GetNetif(FRIENDLY_NAME);\
                                            oC_Net_Address_t destination;\
                                            memset(&destination,0,sizeof(destination));\
                                            oC_Net_Address_t netmask;\
                                            memset(&destination,0,sizeof(netmask));\
                                            oC_SaveIfFalse     ("NetifMan::TurnOn - cannot get network interface named '" FRIENDLY_NAME "' - " , netif != NULL , oC_ErrorCode_ObjectNotFoundOnList )\
                                         && oC_SaveIfErrorOccur("NetifMan::TurnOn - cannot read destination address '" DESTINATION "'" , oC_Net_AddressFromString( DESTINATION, &destination ) )\
                                         && oC_SaveIfErrorOccur("NetifMan::TurnOn - cannot read netmask address '" NETMASK "'" ,         oC_Net_AddressFromString( NETMASK, &netmask ) )\
                                         && oC_SaveIfErrorOccur("NetifMan::TurnOn - cannot add routing table entry '[" DESTINATION "]/[" NETMASK "]/[" FRIENDLY_NAME "]' - ", oC_NetifMan_AddRoutingTableEntry( &destination, &netmask, COST, netif ) );\
                                        }
            CFG_ROUTING_TABLE(ADD_ENTRY,UNIVERSAL_DONT_ADD);
#undef ADD_ENTRY

        }
        else
        {
            //--------------------------------------------------------//
            //           RELEASING RESOURCES WHEN FAILED              //
            //--------------------------------------------------------//
            bool deleted = oC_List_Delete(NetifList,AllocationFlags_CanWait1Second);
            oC_SaveIfFalse("NetifMan::TurnOn - NetifList - ", deleted , oC_ErrorCode_ReleaseError);

            deleted = oC_List_Delete(RoutingTableData.RoutingActiveEntryList,AllocationFlags_CanWait1Second);
            oC_SaveIfFalse("NetifMan::TurnOn - RoutingEntryList - ", deleted , oC_ErrorCode_ReleaseError);

            deleted = oC_List_Delete(RoutingTableData.RoutingTable,AllocationFlags_CanWait1Second);
            oC_SaveIfFalse("NetifMan::TurnOn - RoutingTable - ", deleted , oC_ErrorCode_ReleaseError);

            oC_SaveIfFalse("NetifMan::TurnOn - DataAvailable semaphore - ",
                           oC_Event_Delete(&ReceivedPackets.DataAvailable,AllocationFlags_CanWait1Second),
                           oC_ErrorCode_ReleaseError);
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases module resources
 *
 * The function is for turning off the module. It release all module resources and variables.
 *
 * @return code of error or `oC_ErrorCode_None`
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                         | Description
 * --------------------------------------|--------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet      | The module is not turned on
 * oC_ErrorCode_AllocationError          | Cannot release memory of the module
 * oC_ErrorCode_CannotDeleteObject       | Cannot delete some of Netif objects
 * oC_ErrorCode_CannotDeleteProcess      | Cannot delete process
 * oC_ErrorCode_CannotDeleteThread       | Cannot delete thread
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    bool           deleted   = false;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        oC_NetifMan_UnconfigureAll();

        oC_Module_TurnOff(oC_Module_NetifMan);

        errorCode = oC_ErrorCode_None;

        if(Process != NULL)
        {
            ErrorCondition( oC_Process_Delete(&Process) , oC_ErrorCode_CannotDeleteProcess );
        }

        if(Thread != NULL)
        {
            ErrorCondition( oC_Thread_Delete(&Thread) , oC_ErrorCode_CannotDeleteThread );
        }

        oC_List_Foreach(NetifList,netif)
        {
            ErrorCondition(oC_Netif_Delete(&netif),oC_ErrorCode_CannotDeleteObject);
        }

        ErrorCondition(oC_Event_Delete(&ReceivedPackets.DataAvailable,AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError);

        deleted = oC_List_Delete(NetifList,AllocationFlags_CanWait1Second);
        ErrorCondition(deleted,oC_ErrorCode_CannotDeleteObject);

        foreach(RoutingTableData.RoutingTable,entry)
        {
            ErrorCondition(kfree(entry,AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError);
        }

        deleted = oC_List_Delete(RoutingTableData.RoutingTable,AllocationFlags_CanWait1Second);
        ErrorCondition(deleted,oC_ErrorCode_CannotDeleteObject);

        deleted = oC_List_Delete(RoutingTableData.RoutingActiveEntryList,AllocationFlags_CanWait1Second);
        ErrorCondition(deleted,oC_ErrorCode_CannotDeleteObject);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures all network interface
 *
 * The function configures all network drivers related with the netifs. The error can be read from the error stack. The function also
 * updates active Netif.
 */
//==========================================================================================================================================
void oC_NetifMan_ConfigureAll( void )
{
    if(oC_SaveIfFalse("NetifMan::ConfigureAll - " , oC_Module_IsTurnedOn(oC_Module_NetifMan) , oC_ErrorCode_ModuleNotStartedYet))
    {
        oC_List_Foreach(NetifList,netif)
        {
            if(oC_Driver_IsTurnedOn(oC_Netif_GetDriver(netif)) && oC_Netif_IsConfigured(netif) == false)
            {
                oC_SaveIfErrorOccur(oC_Netif_GetFriendlyName(netif),oC_Netif_Configure(netif));
            }
        }

        oC_NetifMan_UpdateRoutingTable();
    }
}

//==========================================================================================================================================
/**
 * @brief unconfigures all network interface
 *
 * The function unconfigures all network drivers related with the netifs. The error can be read from the error stack.
 */
//==========================================================================================================================================
void oC_NetifMan_UnconfigureAll( void )
{
    if(oC_SaveIfFalse("NetifMan::UnconfigureAll - " , oC_Module_IsTurnedOn(oC_Module_NetifMan) , oC_ErrorCode_ModuleNotStartedYet))
    {
        oC_List_Foreach(NetifList,netif)
        {
            if(oC_Driver_IsTurnedOn(oC_Netif_GetDriver(netif)) && oC_Netif_IsConfigured(netif) == true)
            {
                oC_SaveIfErrorOccur(oC_Netif_GetFriendlyName(netif),oC_Netif_Unconfigure(netif));
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief returns #Netif with the given name
 *
 * The function searches for the #Netif object that matches the given `FriendlyName`.
 *
 * @param FriendlyName      Friendly name of the Netif interface
 *
 * @return Netif object or #NULL if not found
 *
 * @note
 * When the function fails, the error code is saved to the errors stack.
 */
//==========================================================================================================================================
oC_Netif_t oC_NetifMan_GetNetif( oC_Netif_FriendlyName_t FriendlyName )
{
    oC_Netif_t netifToReturn = NULL;

    if(
        oC_SaveIfFalse("NetifMan::GetNetif - " , oC_Module_IsTurnedOn(oC_Module_NetifMan) , oC_ErrorCode_ModuleNotStartedYet    )
     && oC_SaveIfFalse("NetifMan::GetNetif - " , isaddresscorrect(FriendlyName)           , oC_ErrorCode_WrongAddress           )
     && oC_SaveIfFalse("NetifMan::GetNetif - " , strlen(FriendlyName) > 0                 , oC_ErrorCode_StringIsEmpty          )
        )
    {
        oC_List_Foreach(NetifList,netif)
        {
            if(strcmp(FriendlyName , oC_Netif_GetFriendlyName(netif)) == 0)
            {
                netifToReturn = netif;
            }
        }
    }

    return netifToReturn;
}

//==========================================================================================================================================
/**
 * @brief returns network interface according to destination address
 *
 * The function returns network interface that can be used to send data to the given destination address.
 */
//==========================================================================================================================================
oC_Netif_t oC_NetifMan_GetNetifForAddress( const oC_Net_Address_t * Destination )
{
    oC_Netif_t netif = NULL;

    if(isaddresscorrect(Destination))
    {
        RoutingTableEntry_t * entry = GetActiveRoutingTableEntry(Destination);

        if(isaddresscorrect(entry))
        {
            netif = entry->Netif;
        }
    }

    return netif;
}

//==========================================================================================================================================
/**
 * @brief adds new netif to the list
 *
 * The function is for adding the `Netif` object to the network interface list. The object has to be valid, and it cannot already exist
 * on the list.
 *
 * @param Netif     Netif object from the #oC_Netif_New function
 *
 * @return code of error or `oC_ErrorCode_None` when success

 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                         | Description
 * --------------------------------------|--------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet      | The module is not turned on
 * oC_ErrorCode_ObjectNotCorrect         | The `Netif` object is not correct or not valid anymore
 * oC_ErrorCode_FoundOnList              | The `Netif` has already been found on the list
 * oC_ErrorCode_CannotAddObjectToList    | Some error occurred during adding the `Netif` object to the list
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_AddNetifToList( oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        bool existOnTheList = oC_List_Contains(NetifList,Netif);

        if(
            ErrorCondition( oC_Netif_IsCorrect(Netif)         , oC_ErrorCode_ObjectNotCorrect )
         && ErrorCondition( existOnTheList == false           , oC_ErrorCode_FoundOnList      )
            )
        {
            bool pushed = oC_List_PushBack( NetifList, Netif, &Allocator);

            if(ErrorCondition(pushed , oC_ErrorCode_CannotAddObjectToList))
            {
                oC_NetifMan_UpdateRoutingTable();
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief removes a `Netif` object from the list
 *
 * The function removes `Netif` from the list. It removes it also if the object is not correct (it should exist on the list if it is not
 * correct).
 *
 * @param Netif     The `Netif` object from the #oC_Netif_New function
 *
 * @return code of error or `oC_ErrorCode_None` when success
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                         | Description
 * --------------------------------------|--------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet      | The module is not turned on
 * oC_ErrorCode_ObjectNotFoundOnList     | The `Netif` object has not been found on the list
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_RemoveNetifFromList( oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        bool deleted = oC_List_RemoveAll(NetifList,Netif);

        if(ErrorCondition(deleted,oC_ErrorCode_ObjectNotFoundOnList))
        {
            oC_NetifMan_UpdateRoutingTable();
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief adds an entry to the routing table
 *
 * The **Routing Table** is a data table stored in a router or a networked computer that lists the routes to particular network destinations,
 * and in some cases, metrics (distances) associated with those routes. The routing table contains information about the topology of
 * the network immediately around it.
 *
 * The function adds an entry to the routing table array.
 *
 * @param Destination       Network destination address (some address that we want to achieve, for example 192.168.0.0)
 * @param Netmask           Subnetwork mask
 * @param Cost              Cost of usage the network interface for the address
 * @param Netif             Network interface to use for this routing table entry
 *
 * @return code of error or `oC_ErrorCode_None` when success
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                         | Description
 * --------------------------------------|--------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet      | The module is not turned on
 * oC_ErrorCode_WrongAddress             | The `Destination` or `Netmask` does not contain correct address
 * oC_ErrorCode_ObjectNotCorrect         | The `Netif` object is not correct
 *
 * @see oC_NetifMan_RemoveRoutingTableEntry
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_AddRoutingTableEntry( const oC_Net_Address_t * Destination, const oC_Net_Address_t * Netmask, oC_Net_Cost_t Cost, oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        if(
            ErrorCondition( oC_Net_IsAddressCorrect(Destination)        , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( oC_Net_IsAddressCorrect(Netmask)            , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( oC_Netif_IsCorrect(Netif)                   , oC_ErrorCode_ObjectNotCorrect         )
            )
        {
            RoutingTableEntry_t * entry = kmalloc( sizeof(RoutingTableEntry_t),&Allocator,AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill );

            if(
                ErrorCondition( entry != NULL , oC_ErrorCode_AllocationError                       )
                )
            {
                entry->Cost  = Cost;
                entry->Netif = Netif;

                bool pushed = oC_List_PushBack( RoutingTableData.RoutingActiveEntryList, entry, &Allocator );

                if(ErrorCondition(pushed, oC_ErrorCode_CannotAddObjectToList))
                {
                    errorCode = oC_ErrorCode_None;
                }
            }
            oC_NetifMan_UpdateRoutingTable();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief removes entry from the Routing Table
 *
 * The **Routing Table** is a data table stored in a router or a networked computer that lists the routes to particular network destinations,
 * and in some cases, metrics (distances) associated with those routes. The routing table contains information about the topology of
 * the network immediately around it.
 *
 * The function removes an entry from the routing table array.
 *
 * @param Destination       Network destination address (some address that we want to remove, for example 192.168.0.0) or #NULL if not used
 * @param Netmask           Subnetwork mask or #NULL if not used
 * @param Netif             Network interface to use for this routing table entry
 *
 * @return code of error or `oC_ErrorCode_None` when success
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                         | Description
 * --------------------------------------|--------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet      | The module is not turned on
 * oC_ErrorCode_WrongAddress             | The `Destination` or `Netmask` does not contain correct address
 * oC_ErrorCode_ObjectNotCorrect         | The `Netif` object is not correct
 *
 * @see oC_NetifMan_AddRoutingTableEntry
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_RemoveRoutingTableEntry( const oC_Net_Address_t * Destination, const oC_Net_Address_t * Netmask, oC_Netif_t Netif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        if(
            ErrorCondition( oC_Netif_IsCorrect(Netif) , oC_ErrorCode_ObjectNotCorrect )
         && ( Destination == NULL || ErrorCondition( isaddresscorrect(Destination) , oC_ErrorCode_WrongAddress ) )
         && ( Netmask     == NULL || ErrorCondition( isaddresscorrect(Netmask)     , oC_ErrorCode_WrongAddress ) )
            )
        {
            errorCode = oC_ErrorCode_ObjectNotFoundOnList;

            foreach(RoutingTableData.RoutingActiveEntryList,entry)
            {
                if(entry->Netif == Netif)
                {
                    if( Destination == NULL || oC_Net_AreAddressesTheSame( Destination, &entry->DestinationAddress ) )
                    {
                        if(Netmask == NULL  || oC_Net_AreAddressesTheSame( Netmask, &entry->Netmask ))
                        {
                            errorCode = oC_ErrorCode_None;

                            ErrorCondition( kfree(entry,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError );
                            oC_List_RemoveCurrentElement(RoutingTableData.RoutingActiveEntryList, entry );
                        }
                    }
                }
            }
            oC_NetifMan_UpdateRoutingTable();
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns list of network interfaces
 *
 * The function returns list with network interfaces.
 *
 * @warning
 * It returns original network list pointer. You cannot change it!
 *
 * @return real network interface list
 */
//==========================================================================================================================================
oC_List(oC_Netif_t) oC_NetifMan_GetList( void )
{
    return NetifList;
}

//==========================================================================================================================================
/**
 * @brief sends packet via active network interface
 *
 * The function sends packet via active network interface.
 *
 * @param Address       Destination address
 * @param Packet        Packet to send
 * @param Timeout       Maximum time for packet transmission
 * @param outNetif      Destination for the Netif used for the transmission or #NULL if not used
 *
 * @return code of error or `oC_ErrorCode_None` when success
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                          | Description
 * ---------------------------------------|--------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet       | The module is not turned on
 * oC_ErrorCode_WrongAddress              | `Packet` address is not correct
 * oC_ErrorCode_OutputAddressNotInRAM     | `outNetif` is not #NULL but the address does not point to the RAM section
 * oC_ErrorCode_RoutingTableEntryNotFound | The given address does not match any entry in the routing table
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_SendPacket( const oC_Net_Address_t * Address , oC_Net_Packet_t * Packet , oC_Time_t Timeout , oC_Netif_t * outNetif )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        RoutingTableEntry_t * entry = GetActiveRoutingTableEntry(Address);

        if(
            ErrorCondition( isaddresscorrect(Packet), oC_ErrorCode_WrongAddress                 )
         && ErrorCondition( entry != NULL           , oC_ErrorCode_RoutingTableEntryNotFound    )
            )
        {
            oC_IntMan_EnterCriticalSection();
            oC_Netif_t netif = entry->Netif;
            oC_IntMan_ExitCriticalSection();

            if(
                ErrorCondition( oC_Netif_IsCorrect(netif)  , oC_ErrorCode_NetifNotCorrect   )
             && ErrorCode(      oC_Netif_SendPacket(netif,Packet,Timeout)                   )
                )
            {
                errorCode = oC_ErrorCode_None;

                if(
                    outNetif != NULL
                 && ErrorCondition( isram(outNetif), oC_ErrorCode_OutputAddressNotInRAM ) )
                {
                    *outNetif = netif;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives packet from the network
 *
 * The function is for receiving packets from the network. At least one network interface (netif) has to be configured. Also the routing table
 * has to contain at least one entry. The function allows to receive packet from the selected source address (with or without port specified),
 * from any address in the given protocol (address is set to 0, only protocol is specified) or just receive packet without address filtering.
 *
 * @param AddressFilter     Filtering address - allows to choose source address of the received packets. Set it to #NULL if packets should not be filtering
 *
 * Otherwise it can be set to:
 * IP           | Protocol  | Description
 * -------------|-----------|--------------------------------------------------------------------------------
 *   0.0.0.0    |        0  | Receives all packets (similar to #NULL pointer)
 *   0.0.0.0    |       17  | Receives only packets at the given protocol (17)
 *  10.10.10.10 |       41  | Receives only packets from the given IP (10.10.10.10) at the given protocol (41)
 *  10.50.30.10 |        0  | Receives only packets from the given IP (10.50.30.10) at all protocols
 *
 * @param outPacket         Destination for the packet
 * @param Timeout           Maximum time for receive operation
 * @param outNetif          Destination for the network interface, that has received the packet. It is optional - can be set to #NULL if not used.
 * @param FilterFunction    Pointer to the function to call when packet is received. It should compare the given packets and returns true if the packet is the one that we expect. Can be #NULL if not used
 * @param Parameter         Parameter for `FilterFunction` or #NULL if not used
 *
 * @return code of error or `oC_ErrorCode_None` when success
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                          | Description
 * ---------------------------------------|---------------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet       | The module is not turned on
 * oC_ErrorCode_OutputAddressNotInRAM     | `outNetif` is not #NULL but the address does not point to the RAM section or `outPacket` is not in RAM
 * oC_ErrorCode_WrongAddress              | `Address` or `FilterFunction` is not #NULL but the pointer is not correct
 * oC_ErrorCode_Timeout                   | Maximum time for operation has been achieved.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_ReceivePacket( const oC_Net_Address_t * AddressFilter , oC_Net_Packet_t * outPacket , oC_Time_t Timeout , oC_Netif_t * outNetif , oC_NetifMan_PacketFilterFunction_t FilterFunction , const void * Parameter )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        if(
            ErrorCondition( isram(outPacket)                                            , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( FilterFunction == NULL || isaddresscorrect(FilterFunction)  , oC_ErrorCode_WrongAddress             )
            )
        {
            if(
                ( outNetif       == NULL || ErrorCondition( isram(outNetif)                 , oC_ErrorCode_OutputAddressNotInRAM    ) )
             && ( AddressFilter  == NULL || ErrorCondition( isaddresscorrect(AddressFilter) , oC_ErrorCode_WrongAddress             ) )
                )
            {
                bool            received    = GetFromSoftwareRing(&ReceivedPackets,AddressFilter,outPacket,outNetif,FilterFunction,Parameter);
                oC_Time_t       currentTime = oC_KTime_GetTimestamp();
                oC_Time_t       time        = currentTime;
                oC_Time_t       endTime     = time + Timeout;

                while(received == false && Timeout > 0)
                {
                    if(ErrorCondition(oC_Event_WaitForState(ReceivedPackets.DataAvailable,NumberOfReceivedPackets,oC_Event_StateMask_DifferentThan,Timeout) , oC_ErrorCode_Timeout))
                    {
                        currentTime = oC_KTime_GetTimestamp();
                        received    = GetFromSoftwareRing(&ReceivedPackets,AddressFilter,outPacket,outNetif,FilterFunction,Parameter);

                        if(received)
                        {
                            errorCode = oC_ErrorCode_None;
                            break;
                        }
                    }

                    if(currentTime < endTime)
                    {
                        Timeout = endTime - oC_KTime_GetTimestamp();
                    }
                    else
                    {
                        Timeout = 0;
                    }
                }

                if(received == false)
                {
                    errorCode = oC_ErrorCode_Timeout;
                }
                else
                {
                    errorCode = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief updates active routing table
 *
 * The function is for updating the routing table. It searches for interfaces in the routing table, updates its link statuses and creates a list
 * of active routing table entries.
 *
 * @note
 * Errors from this function are saved in the errors stack.
 */
//==========================================================================================================================================
void oC_NetifMan_UpdateRoutingTable( void )
{
    if(oC_SaveIfFalse("NetifMan::UpdateRoutingTable - module is not turned on - " , oC_Module_IsTurnedOn(oC_Module_NetifMan) , oC_ErrorCode_ModuleNotStartedYet))
    {
        foreach(RoutingTableData.RoutingTable,entry)
        {
            RoutingTableEntry_t * foundEntry = NULL;

            oC_SaveIfErrorOccur("NetifMan::UpdateRoutingTable - cannot update link status - " , oC_Netif_UpdateLinkStatus(entry->Netif));

            foreach(RoutingTableData.RoutingActiveEntryList,activeEntry)
            {
                oC_ASSERT(activeEntry != NULL);

                if(
                    activeEntry->DestinationAddress.Type == oC_Net_AddressType_IPv4
                 && activeEntry->Netmask.Type            == oC_Net_AddressType_IPv4
                    )
                {
                    if(
                        activeEntry->DestinationAddress.IPv4 == entry->DestinationAddress.IPv4
                     && activeEntry->Netmask.IPv4            == entry->Netmask.IPv4
                        )
                    {
                        foundEntry = entry;
                        break;
                    }
                }
                else
                {
                    oC_SaveError("NetifMan::UpdateRoutingTable - not implemented for IPv6 - " , oC_ErrorCode_NotImplemented);
                }
            }

            if(foundEntry == NULL)
            {
                oC_List_PushBack(RoutingTableData.RoutingActiveEntryList,foundEntry,&Allocator);
            }
            else if(foundEntry != entry)
            {

                if((oC_Netif_GetLinkStatus(foundEntry->Netif) == oC_Net_LinkStatus_Up))
                {
                    oC_List_Swap(RoutingTableData.RoutingActiveEntryList,foundEntry,entry);
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief starts the NetifMan daemon
 *
 * The NetifMan daemon is designed to manage the network interfaces during work of the system. It removes invalid entries from the routing
 * table and swaps the network interfaces if there are duplicated.
 *
 * @return code of error or `oC_ErrorCode_None` when success
 *
 * Here is the list of standard error codes, that can be returned by the function. Note, that it is not full list of them, some of errors can
 * be returned also by the other system functions, that this one calls.
 *
 * Code of error                          | Description
 * ---------------------------------------|---------------------------------------------------------------------------------------------------------
 * oC_ErrorCode_ModuleNotStartedYet       | The module is not turned on
 * oC_ErrorCode_ProcessAlreadyStarted     | `Process` has been started already
 * oC_ErrorCode_CannotCreateProcess       | The process cannot be created
 * oC_ErrorCode_CannotCreateThread        | The thread cannot be created
 * oC_ErrorCode_CannotRunThread           | Some error occurred during starting of the thread
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NetifMan_StartDaemon( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_NetifMan))
    {
        if(ErrorCondition( Process == NULL , oC_ErrorCode_ProcessAlreadyStarted ))
        {
            Process = oC_Process_New( oC_Process_Priority_NetworkHandlerProcess , "NetifMan-Process", oC_UserMan_GetRootUser() , 0 , NULL, oC_StreamMan_GetStdErrorStream(), oC_StreamMan_GetStdErrorStream());

            if(
                ErrorCondition( Process != NULL, oC_ErrorCode_CannotCreateProcess   )
             && ErrorCode     ( oC_ProcessMan_AddProcess(Process)                   )
                )
            {
                Thread = oC_Thread_New(0,STACK_SIZE,Process,"netifman::daemon",DaemonThread,NULL);

                if(
                    ErrorCondition( Thread != NULL        , oC_ErrorCode_CannotCreateThread )
                 && ErrorCondition( oC_Thread_Run(Thread) , oC_ErrorCode_CannotRunThread    )
                    )
                {
                    errorCode = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

/** @} */
#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief puts packet to the software ring
 */
//==========================================================================================================================================
static void PutToSoftwareRing( SoftwareRing_t * SoftwareRing , oC_Netif_t Netif ,  oC_Net_Packet_t * Packet )
{
    oC_IntMan_EnterCriticalSection();
    if(IsSoftwareRingFull(SoftwareRing))
    {
        GetFromSoftwareRing(SoftwareRing,NULL,NULL,NULL,NULL,NULL);
        NumberOfIgnoredPackets++;
    }

    if(Packet->IPv4.Header.Version == oC_Net_PacketType_IPv4)
    {
        for(uint32_t i = 0; i < SOFTWARE_RING_SIZE; i++ )
        {
            if(SoftwareRing->Packets[i].Used == false)
            {
                SoftwareRing->Packets[i].Netif      = Netif;
                SoftwareRing->Packets[i].Timestamp  = oC_KTime_GetTimestamp();
                SoftwareRing->Packets[i].Used       = true;
                memcpy(&SoftwareRing->Packets[i].Packet,Packet,sizeof(oC_Net_Packet_t));
                SoftwareRing->Count++;
                NumberOfReceivedPackets++;
                break;
            }
        }
    }

    oC_IntMan_ExitCriticalSection();

    oC_Event_SetState(SoftwareRing->DataAvailable,NumberOfReceivedPackets);
}

//==========================================================================================================================================
/**
 * @brief reads packet from the software ring
 */
//==========================================================================================================================================
static bool GetFromSoftwareRing( SoftwareRing_t * SoftwareRing , const oC_Net_Address_t * Address  , oC_Net_Packet_t * outPacket , oC_Netif_t * outNetif , oC_NetifMan_PacketFilterFunction_t FilterFunction , const void * Parameter )
{
    bool     packetFound = false;
    uint32_t takenIndex  = 0;

    oC_IntMan_EnterCriticalSection();

    if(Address == NULL || Address->Type == oC_Net_AddressType_IPv4 || Address->Type == 0)
    {
        oC_Net_Ipv4_t     ip        = (Address == NULL || Address->Type == 0) ? 0 : Address->IPv4;
        oC_Net_Protocol_t protocol  = (Address == NULL) ? 0 : Address->Protocol;

        for(uint32_t i = 0; i < SOFTWARE_RING_SIZE; i++)
        {
            if(
                (SoftwareRing->Packets[i].Used == true)
             && (ip       == 0 || ip       == SoftwareRing->Packets[i].Packet.IPv4.Header.SourceIp )
             && (protocol == 0 || protocol == SoftwareRing->Packets[i].Packet.IPv4.Header.Protocol )
                )
            {
                if( FilterFunction == NULL || FilterFunction(&SoftwareRing->Packets[i].Packet,Parameter,SoftwareRing->Packets[i].Netif) )
                {
                    if(!packetFound || SoftwareRing->Packets[takenIndex].Timestamp < SoftwareRing->Packets[i].Timestamp)
                    {
                        takenIndex  = i;
                        packetFound = true;
                    }
                }
            }
        }

        if(packetFound)
        {
            SoftwareRing->Count--;
            SoftwareRing->Packets[takenIndex].Used = false;

            if(outPacket != NULL)
            {
                memcpy(outPacket,&SoftwareRing->Packets[takenIndex].Packet,sizeof(oC_Net_Packet_t));
            }
            if(outNetif != NULL)
            {
                *outNetif = SoftwareRing->Packets[takenIndex].Netif;
            }
        }
    }
    else
    {
        oC_SaveError("NetifMan::GetFromSoftwareRing - IPv6 is not implemented - " , oC_ErrorCode_NotImplemented);
    }


    oC_IntMan_ExitCriticalSection();

    return packetFound;
}

//==========================================================================================================================================
/**
 * @brief checks if the software ring is full
 */
//==========================================================================================================================================
static inline bool IsSoftwareRingFull( SoftwareRing_t * SoftwareRing )
{
    return SoftwareRing->Count == SOFTWARE_RING_SIZE;
}

//==========================================================================================================================================
/**
 * @brief returns active table entry for the given address
 */
//==========================================================================================================================================
static RoutingTableEntry_t* GetActiveRoutingTableEntry( const oC_Net_Address_t * Address )
{
    RoutingTableEntry_t * entry = NULL;

    foreach(RoutingTableData.RoutingActiveEntryList,tempEntry)
    {
        if(oC_Net_IsAddressInSubnet(Address,&tempEntry->DestinationAddress,&tempEntry->Netmask)
        || (tempEntry->Netmask.IPv4 == 0 && tempEntry->DestinationAddress.IPv4 == 0)
           )
        {
            entry = tempEntry;
            break;
        }
    }

    return entry;
}

//==========================================================================================================================================
/**
 * @brief thread for receiving packets via network interface
 */
//==========================================================================================================================================
static void ReceivePacketsThread( oC_Netif_t Netif )
{
    oC_ErrorCode_t   errorCode = oC_ErrorCode_ImplementError;
    oC_Net_Packet_t* packet    = kmalloc( sizeof(oC_Net_Packet_t), &Allocator, AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill );

    while(oC_Netif_IsConfigured(Netif) && packet != NULL)
    {
        if(ErrorCode(oC_Netif_UpdateLinkStatus(Netif)) && oC_Netif_GetLinkStatus(Netif) == oC_Net_LinkStatus_Up && oC_Netif_GetListenMode(Netif) == false)
        {
            errorCode = oC_Netif_ReceivePacket(Netif, packet, sizeof(oC_Net_Packet_t) , min(1));

            if(!oC_ErrorOccur(errorCode))
            {
                PutToSoftwareRing(&ReceivedPackets,Netif,packet);
                memset(packet,0,sizeof(oC_Net_Packet_t));
            }
            else if (errorCode != oC_ErrorCode_Timeout)
            {
                oC_SaveError("NetifMan::ReceivePacketsThread - cannot receive packet - ", errorCode );
            }
        }
        else
        {
            sleep(s(1));
        }
    }

    if(packet != NULL)
    {
        oC_SaveIfFalse("packet", kfree(packet,0), oC_ErrorCode_ReleaseError);
    }
}

//==========================================================================================================================================
/**
 * @brief thread for echo messages
 */
//==========================================================================================================================================
static void EchoThread( void * Thread )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    sleep(s(2));

    while(1)
    {
        if(ErrorCode(oC_Icmp_ReserveType(oC_Icmp_Type_EchoRequest,min(10))))
        {
            oC_Icmp_Packet_t *  packet      = malloc( sizeof(oC_Icmp_Packet_t), AllocationFlags_ZeroFill );
            oC_Net_Address_t    destination;

            destination.Type        = oC_Net_AddressType_IPv4;
            destination.Protocol    = oC_Net_Protocol_ICMP;
            destination.IPv4        = 0;

            while(oC_SaveIfFalse("packet" , packet != NULL, oC_ErrorCode_AllocationError))
            {
                if(
                    ErrorCode     ( oC_Icmp_Receive( NULL, packet, oC_Icmp_Type_EchoRequest, min(10) )  )
                 && ErrorCondition( isram(packet)   , oC_ErrorCode_InternalDataAreDamaged               )
                    )
                {
                    if(oC_Net_Packet_GetType(&packet->Packet) == oC_Net_PacketType_IPv4)
                    {
                        packet->IPv4.IcmpDatagram.Header.Type = oC_Icmp_Type_EchoReply;
                        destination.IPv4 = packet->IPv4.Header.SourceIp;

                        oC_SaveIfErrorOccur("icmp-echo - cannot send packet - ",  ErrorCode( oC_Icmp_Send(NULL, &destination, packet, min(2)) ));

                    }
                }
            }
            oC_Icmp_ReleaseType(oC_Icmp_Type_EchoRequest,min(10));
            oC_SaveIfFalse("packet", kfree(packet, AllocationFlags_Default), oC_ErrorCode_ReleaseError);
        }
        else
        {
            oC_SaveError("icmp-echo: Cannot reserve type - ", errorCode);
            sleep(s(5));
        }
    }
}

//==========================================================================================================================================
/**
 * @brief main thread for handling network interfaces
 */
//==========================================================================================================================================
static void DaemonThread( void * Argument )
{
    oC_Timestamp_t nextIpUpdateTimestamp = oC_KTime_GetTimestamp();

    oC_Thread_t echoThread = oC_Thread_New( 0, kB(1), NULL, "icmp-echo", EchoThread, NULL );

    if(oC_SaveIfFalse("NetifMan::DaemonThread - icmp echo - ", echoThread , oC_ErrorCode_CannotCreateThread))
    {
        if(false == oC_SaveIfFalse("NetifMan::Daemon - icmp echo - " , oC_Thread_Run(echoThread) , oC_ErrorCode_CannotRunThread))
        {
            oC_SaveIfFalse("NetifMan::Daemon - icmp echo - " , oC_Thread_Delete(&echoThread) , oC_ErrorCode_CannotDeleteThread);
        }
    }

    while(1)
    {
        oC_NetifMan_UpdateRoutingTable();

        foreach(NetifList,netif)
        {
            if(oC_Netif_IsConfigured(netif))
            {
                oC_Thread_t relatedThread = NULL;

                if(oC_Netif_ReadReceiveThread(netif,&relatedThread) == oC_ErrorCode_ThreadNotSet)
                {
                    relatedThread = oC_Thread_New( 0, STACK_SIZE , NULL, oC_Netif_GetFriendlyName(netif), (oC_Thread_Function_t)ReceivePacketsThread, netif );

                    if(oC_SaveIfFalse("NetifMan::DeamonThread - Cannot create thread - " , relatedThread != NULL, oC_ErrorCode_CannotCreateThread))
                    {
                        if(
                            oC_SaveIfFalse(     "NetifMan::DaemonThread - Cannot run thread"              , oC_Thread_Run(relatedThread), oC_ErrorCode_CannotRunThread)
                         && oC_SaveIfErrorOccur("NetifMan::DaemonThread - Cannot set thread for netif - " , oC_Netif_SetReceiveThread(netif,relatedThread) )
                            )
                        {
                            kdebuglog(oC_LogType_Info, "NetifMan::Daemon - thread created for %s", oC_Netif_GetFriendlyName(netif));
                        }
                        else
                        {
                            oC_SaveIfFalse("NetifMan::Daemon - cannot delete thread ", oC_Thread_Delete(&relatedThread), oC_ErrorCode_CannotDeleteThread);
                        }
                    }
                }

                if(oC_Netif_IsIpAssigned(netif) == false && oC_KTime_GetTimestamp() >= nextIpUpdateTimestamp && oC_Netif_GetLinkStatus(netif) == oC_Net_LinkStatus_Up)
                {
                    oC_SaveIfErrorOccur("NetifMan::ReceivePacketsThead - cannot update link status - " , oC_Netif_UpdateLinkStatus(netif)   );
                    oC_SaveIfErrorOccur("NetifMan::ReceivePacketsThead - cannot update IP - "          , oC_Netif_UpdateIp(netif,s(30))     );
                    nextIpUpdateTimestamp = oC_KTime_GetTimestamp() + s(5);
                }
            }
        }

        sleep(DAEMON_SLEEP_TIME);
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

