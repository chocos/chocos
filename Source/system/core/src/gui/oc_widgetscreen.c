/** ****************************************************************************************************************************************
 *
 * @brief       The file contains implementation of the widget screen
 * 
 * @file          oc_widgetscreen.c
 *
 * @author     Patryk Kubiak - (Created on: 06.06.2017 19:20:20) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#define WIDGET_SCREEN_DEFINE_SHORT_TYPES
#include <oc_widgetscreen.h>
#include <oc_object.h>
#include <oc_screenman.h>
#include <oc_list.h>
#include <oc_ictrlman.h>
#include <oc_object.h>
#include <oc_intman.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct
{
    oC_WidgetScreen_Option_t            Option;
    bool                                Checked;
    WidgetState_t                       State;
} Option_t;

typedef struct
{
    oC_ObjectControl_t                  ObjectControl;
    oC_Screen_t                         Screen;
    oC_Pixel_Position_t                 RelativePosition;
    oC_Pixel_Position_t                 RelativeEndPosition;
    oC_List(Option_t*)                  Options;
    WidgetValue_t                       Value;
    WidgetValue_t                       MaximumValue;
    Palette_t                           Palette;
    DrawStyle_t                         DrawStyle;
    WidgetState_t                       State;
    oC_WidgetScreen_WidgetDefinition_t  Definition;
    bool                                DrawString;
    void *                              UserContext;
    oC_ICtrlMan_Activity_t              ActiveActivity;
    oC_ICtrlMan_Activity_t              ReleaseActivity;
    oC_IDI_Event_t                      ActiveEvent;
    oC_IDI_Event_t                      ReleaseEvent;
    oC_Time_t                           MaxReleaseTimeout;
    oC_Timestamp_t                      MaxReleaseTimestamp;
    bool                                Visible;
} Widget_t;

struct WidgetScreen_t
{
    oC_ObjectControl_t                  ObjectControl;
    oC_List(Widget_t*)                  Widgets;
    WidgetIndex_t                       NumberOfWidgets;
    oC_Pixel_Position_t                 Position;
    oC_WidgetScreen_ScreenDefinition_t  Definition;
    oC_Screen_t                         Screen;
    oC_ColorMap_t *                     ColorMap;
    oC_WidgetScreen_ZPosition_t         MaxZPosition;
};

typedef struct
{
    void (*DrawWidget)              ( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition );
    bool (*UpdateState)             ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
    bool (*RegisterActivity)        ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
    bool (*CheckDefinition)         ( const WidgetDefinition_t * Definition );
} WidgetHandlersDefinition_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

static void DrawString                  ( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition );
static void DrawTextBox                 ( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition );
static bool RegisterPushButtonActivity  ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
static void DrawPushButton              ( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition );
static bool UpdatePushButtonState       ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
static bool CheckPushButtonDefinition   ( const WidgetDefinition_t * Definition );
static bool RegisterCheckBoxActivity    ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
static void DrawCheckBox                ( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition );
static bool UpdateCheckBoxState         ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
static bool CheckCheckBoxDefinition     ( const WidgetDefinition_t * Definition );
static bool RegisterRadioButtonActivity ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
static void DrawRadioButton             ( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition );
static bool UpdateRadioButtonState      ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition );
static void DrawProgressBar             ( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition );

static oC_ErrorCode_t   CreateNewScreen ( oC_Screen_t Screen , const oC_WidgetScreen_ScreenDefinition_t * ScreenDefinition, oC_WidgetScreen_t * outScreen, void * UserParameter );
static oC_ErrorCode_t   DeleteScreen    ( oC_WidgetScreen_t * outScreen );

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________


static const WidgetHandlersDefinition_t WidgetHandlersDefinition[oC_WidgetScreen_WidgetType_NumberOfTypes] =
{
    [ WidgetType_TextBox     ] = { .DrawWidget = DrawTextBox      , .UpdateState = NULL                     , .RegisterActivity = NULL                          , .CheckDefinition = NULL                           } ,
    [ WidgetType_PushButton  ] = { .DrawWidget = DrawPushButton   , .UpdateState = UpdatePushButtonState    , .RegisterActivity = RegisterPushButtonActivity    , .CheckDefinition = CheckPushButtonDefinition      } ,
    [ WidgetType_CheckBox    ] = { .DrawWidget = DrawCheckBox     , .UpdateState = UpdateCheckBoxState      , .RegisterActivity = RegisterCheckBoxActivity      , .CheckDefinition = CheckCheckBoxDefinition        } ,
    [ WidgetType_RadioButton ] = { .DrawWidget = DrawRadioButton  , .UpdateState = UpdateRadioButtonState   , .RegisterActivity = RegisterRadioButtonActivity   , .CheckDefinition = CheckCheckBoxDefinition        } ,
    [ WidgetType_ProgressBar ] = { .DrawWidget = DrawProgressBar  , .UpdateState = NULL                     , .RegisterActivity = NULL                          , .CheckDefinition = NULL                           } ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with static inline
 *
 *  ======================================================================================================================================*/
#define _________________________________________STATIC_INLINE_SECTION______________________________________________________________________

//===========================================================================================================================================
/**
 * @brief       Draws the widget
 * 
 * The function draws the widget on the screen.
 */
//===========================================================================================================================================
static inline void DrawWidget( oC_WidgetScreen_t Screen, Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition , WidgetIndex_t WidgetIndex )
{
    if(Widget->Definition.Type < oC_WidgetScreen_WidgetType_NumberOfTypes && Widget->Visible)
    {
        if(WidgetHandlersDefinition[Widget->Definition.Type].DrawWidget != NULL)
        {
            WidgetHandlersDefinition[Widget->Definition.Type].DrawWidget(Widget,ColorMap,ScreenPosition);
        }
        if(Widget->DrawString)
        {
            if(Widget->Definition.String.UpdateStringHandler != NULL)
            {
                Widget->Definition.String.UpdateStringHandler(
                                Screen,
                                Widget->UserContext,
                                WidgetIndex,
                                Widget->Definition.String.DefaultString,
                                sizeof(Widget->Definition.String.DefaultString)
                                );
            }
            DrawString(Widget,ColorMap,ScreenPosition);
        }
    }
}

//===========================================================================================================================================
/**
 * @brief       Updates the widget state
 * 
 * The function updates the widget state.
 */
//===========================================================================================================================================
static inline oC_WidgetScreen_ScreenId_t UpdateWidgetState( Widget_t * Widget , oC_WidgetScreen_t Screen , oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_ScreenId_t ScreenID , bool * outReprepareScreen )
{
    oC_WidgetScreen_ScreenId_t nextScreenID = ScreenID;
    bool                       changed      = false;

    if(Widget->Definition.Type < oC_WidgetScreen_WidgetType_NumberOfTypes && Widget->Visible)
    {
        if(WidgetHandlersDefinition[Widget->Definition.Type].UpdateState != NULL)
        {
            changed = WidgetHandlersDefinition[Widget->Definition.Type].UpdateState(Widget, Screen->Position);

            if(changed)
            {
                if(Widget->State < WidgetState_NumberOfStates)
                {
                    if(Widget->Definition.Handlers[Widget->State] != NULL)
                    {
                        nextScreenID = Widget->Definition.Handlers[Widget->State](Screen,Widget->UserContext,WidgetIndex,ScreenID,outReprepareScreen);
                    }
                }
            }
        }
    }

    return nextScreenID;
}

//===========================================================================================================================================
/**
 * @brief       Registers the widget activity
 * 
 * The function registers the widget activity.
 */
//===========================================================================================================================================
static inline bool RegisterWidgetActivity( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition )
{
    bool registered = false;

    if(Widget->Definition.Type < oC_WidgetScreen_WidgetType_NumberOfTypes)
    {
        registered = true;

        if(WidgetHandlersDefinition[Widget->Definition.Type].RegisterActivity != NULL)
        {
            registered = WidgetHandlersDefinition[Widget->Definition.Type].RegisterActivity(Widget, ScreenPosition);
        }
    }

    return registered;
}

//===========================================================================================================================================
/**
 * @brief       Checks if the screen is correct
 * 
 * The function checks if the screen is correct.
 */
//===========================================================================================================================================
static inline bool IsScreenCorrect( oC_WidgetScreen_t Screen )
{
    return isram(Screen) && oC_CheckObjectControl( Screen, oC_ObjectId_WidgetScreen, Screen->ObjectControl );
}

//===========================================================================================================================================
/**
 * @brief       Checks if the widget is correct
 * 
 * The function checks if the widget is correct.
 */
//===========================================================================================================================================
static inline bool IsWidgetCorrect( Widget_t * Widget )
{
    return isram(Widget) && oC_CheckObjectControl( Widget, oC_ObjectId_Widget, Widget->ObjectControl );
}

//===========================================================================================================================================
/**
 * @brief       Prepares the screen
 * 
 * The function prepares the screen.
 */
//===========================================================================================================================================
static inline oC_WidgetScreen_ScreenId_t PrepareScreen( oC_WidgetScreen_t Screen , struct Context_t * Context , oC_WidgetScreen_ScreenId_t ScreenID )
{
    kdebuglog(oC_LogType_Info, "Preparing new screen");
    foreach(Screen->Widgets,widget)
    {
        foreach(widget->Options,option)
        {
            option->Checked = false;
            option->State   = WidgetState_Ready;
        }
        widget->State = WidgetState_Ready;
    }

    if(isaddresscorrect( Screen->Definition.PrepareHandler ))
    {
        ScreenID = Screen->Definition.PrepareHandler(Screen,Context,ScreenID);
    }
    return ScreenID;
}

//===========================================================================================================================================
/**
 * @brief       Draws the screen
 * 
 * The function draws the screen.
 */
//===========================================================================================================================================
static inline void DrawScreen( oC_WidgetScreen_t Screen , struct Context_t * Context , oC_WidgetScreen_ScreenId_t ScreenID )
{
    oC_WidgetScreen_ZPosition_t zPosition       = 0;
    oC_ColorMap_LayerIndex_t    activeLayer     = oC_ColorMap_GetActiveLayer(Screen->ColorMap);
    oC_ColorMap_LayerIndex_t    numberOfLayers  = oC_ColorMap_GetNumberOfLayers(Screen->ColorMap);

    if(numberOfLayers > 1)
    {
        if(activeLayer == 0)
        {
            oC_ColorMap_SwitchLayer(Screen->ColorMap,1);
        }
        else
        {
            oC_ColorMap_SwitchLayer(Screen->ColorMap,0);
        }
    }
    oC_ColorMap_DrawBackground(Screen->ColorMap,Screen->Definition.BackgroundColor,Screen->Definition.ColorFormat);

    for(zPosition = 1; zPosition > 0 && zPosition <= Screen->MaxZPosition; zPosition++)
    {
        WidgetIndex_t widgetIndex = 0;

        foreach(Screen->Widgets,widget)
        {
            if(IsWidgetCorrect(widget) && widget->Definition.ZPosition == zPosition)
            {
                DrawWidget(Screen,widget,Screen->ColorMap,Screen->Position,widgetIndex);
            }

            widgetIndex++;
        }
    }
    if(numberOfLayers > 1)
    {
        if(activeLayer == 0)
        {
            oC_Screen_SwitchLayer(Screen->Screen,1);
        }
        else
        {
            oC_Screen_SwitchLayer(Screen->Screen,0);
        }
    }
}

//===========================================================================================================================================
/**
 * @brief       Handles the screen
 * 
 * The function handles the screen.
 */
//===========================================================================================================================================
static inline oC_WidgetScreen_ScreenId_t HandleScreen( oC_WidgetScreen_t Screen , struct Context_t * Context , oC_WidgetScreen_ScreenId_t ScreenID )
{
    oC_WidgetScreen_ScreenId_t  nextScreenId    = ScreenID;
    bool                        reprepareScreen = false;

    foreach(Screen->Widgets,widget)
    {
        RegisterWidgetActivity(widget,Screen->Position);
    }

    do
    {
        DrawScreen(Screen,Context,ScreenID);

        if(isaddresscorrect( Screen->Definition.Handler ))
        {
            nextScreenId = Screen->Definition.Handler(Screen,Context,ScreenID);
        }

        if(nextScreenId == ScreenID)
        {
            kdebuglog(oC_LogType_Info, "Waiting for new event");
            oC_ErrorCode_t errorCode = oC_ICtrlMan_WaitForAnyNewActivity( min(5) );
            if( errorCode == oC_ErrorCode_None)
            {
                WidgetIndex_t widgetIndex = 0;
                foreach(Screen->Widgets,widget)
                {
                    reprepareScreen = false;

                    nextScreenId = UpdateWidgetState(widget,Screen,widgetIndex,ScreenID,&reprepareScreen);

                    if(nextScreenId != ScreenID || reprepareScreen)
                    {
                        DrawScreen(Screen,Context,ScreenID);
                        break;
                    }

                    widgetIndex++;
                }
            }
            else if(errorCode != oC_ErrorCode_Timeout)
            {
                oC_SaveError("Waiting for activity failed", errorCode);
                break;
            }
        }
    } while(nextScreenId == ScreenID && reprepareScreen == false);

    foreach(Screen->Widgets,widget)
    {
        oC_ICtrlMan_UnregisterActivity(&widget->ActiveActivity);
        oC_ICtrlMan_UnregisterActivity(&widget->ReleaseActivity);
    }

    return nextScreenId;
}

//===========================================================================================================================================
/**
 * @brief       Draws the string
 * 
 * The function draws the string.
 */
//===========================================================================================================================================
static inline bool CheckWidgetDefinition( const WidgetDefinition_t * Definition )
{
    bool correct     = false;
    bool drawString  = strlen(Definition->String.DefaultString) > 0;

    if(
        oC_SaveIfFalse( "WidgetType"    , Definition->Type                                                  , oC_ErrorCode_TypeNotCorrect   )
     && oC_SaveIfFalse( "WidgetWidth"   , Definition->Width  > 0                                            , oC_ErrorCode_WidthNotCorrect  )
     && oC_SaveIfFalse( "WidgetHeight"  , Definition->Height > 0                                            , oC_ErrorCode_HeightNotCorrect )
     && oC_SaveIfFalse( "DrawStyle"     , isaddresscorrect(Definition->DrawStyle)                           , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse( "Palette"       , isaddresscorrect(Definition->Palette)                             , oC_ErrorCode_WrongAddress     )
     && oC_SaveIfFalse( "Font"          , drawString == false || isaddresscorrect(Definition->String.Font)  , oC_ErrorCode_WrongAddress     )
        )
    {
        if(
            WidgetHandlersDefinition[Definition->Type].CheckDefinition == NULL
         || WidgetHandlersDefinition[Definition->Type].CheckDefinition(Definition)
            )
        {
            correct = true;

            oC_ARRAY_FOREACH_IN_ARRAY(Definition->Handlers,handler)
            {
                if( ! oC_SaveIfFalse( "Widget Handler", handler == NULL || isaddresscorrect(handler), oC_ErrorCode_WrongAddress ) )
                {
                    correct = false;
                }
            }
        }
    }

    return correct;
}

//===========================================================================================================================================
/**
 * @brief       Draws the string
 * 
 * The function draws the string.
 */
//===========================================================================================================================================
static inline oC_ErrorCode_t CheckScreenDefinition( const ScreenDefinition_t * Definition )
{
    oC_ErrorCode_t errorCode   = oC_ErrorCode_ImplementError;
    WidgetIndex_t  widgetIndex = 0;

    (void)widgetIndex;

    if(
        ErrorCondition( Definition->Width  > 0                              , oC_ErrorCode_WidthNotCorrect       )
     && ErrorCondition( Definition->Height > 0                              , oC_ErrorCode_HeightNotCorrect      )
     && ErrorCondition( oC_ColorFormat_IsCorrect(Definition->ColorFormat)   , oC_ErrorCode_ColorFormatNotCorrect )
     && ErrorCondition( Definition->PrepareHandler == NULL
                     || isaddresscorrect(Definition->PrepareHandler)        , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( Definition->Handler == NULL
                     || isaddresscorrect(Definition->Handler)               , oC_ErrorCode_WrongAddress          )
        )
    {
        bool allCorrect = true;

        oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(Definition->WidgetsDefinitions,Definition->NumberOfWidgetsDefinitions,widgetDefinition)
        {
            if(!CheckWidgetDefinition(widgetDefinition))
            {
                kdebuglog(oC_LogType_Error, "Definition of the widget index %d is incorrect: %R\n", widgetIndex, errorCode);
                allCorrect = false;
                break;
            }
            widgetIndex++;
        }

        if( ErrorCondition( allCorrect, oC_ErrorCode_WrongWidgetDefinition ) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

#undef  _________________________________________STATIC_INLINE_SECTION______________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief adds an option to the multiple-choice widget
 * 
 * The function is for adding an option to the widgets like CheckBox or RadioButton. The function returns the value of the freshly added option.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Option        Pointer to the option to add (the data will be copied)
 * 
 * @return unique value of the option
 * 
 * @note The function returns 0 if the option cannot be added. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_RemoveOption
 * 
 * @code{.c}
   
    oC_WidgetScreen_Option_t Option = {
        .Label = "Option 1",
        .Font  = oC_Font_(Consolas),
    };
    oC_WidgetScreen_AddOption(Screen,0,&Option);

 * @endcode
 */
//==========================================================================================================================================
oC_WidgetScreen_Value_t oC_WidgetScreen_AddOption( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , const oC_WidgetScreen_Option_t * Option )
{
    WidgetValue_t value = 0;

    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
     && oC_SaveIfFalse( "Option"        , isaddresscorrect(Option)              , oC_ErrorCode_WrongAddress     )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            Option_t * option = malloc(sizeof(Option_t), AllocationFlags_ZeroFill | AllocationFlags_ExternalRamFirst);
            char *     label  = Option->Label != NULL ? malloc(strlen(Option->Label) + 1, AllocationFlags_ZeroFill | AllocationFlags_ExternalRamFirst ) : NULL;

            if(
                oC_SaveIfFalse( "option", option != NULL                        , oC_ErrorCode_AllocationError )
             && oC_SaveIfFalse( "label" , Option->Label == NULL || label != NULL, oC_ErrorCode_AllocationError )
                )
            {
                option->Checked = false;
                option->State   = WidgetState_Ready;
                memcpy(&option->Option,Option,sizeof(oC_WidgetScreen_Option_t));

                if(label != NULL)
                {
                    strcpy(label,Option->Label);
                }

                option->Option.Label = label;

                success = oC_List_PushBack(widget->Options,option,getcurallocator());

                if( oC_SaveIfFalse( "widget->Options", success, oC_ErrorCode_CannotAddObjectToList ) )
                {
                    int indexOfElement = oC_List_IndexOf(widget->Options,option);
                    value = indexOfElement >= 0 ? (WidgetValue_t)indexOfElement : 0;
                }
                else
                {
                    oC_SaveIfFalse("option", free(option,0), oC_ErrorCode_ReleaseError);
                }
            }
        }
    }

    return value;
}
//==========================================================================================================================================
/**
 * @brief removes the option from the multiple-choice widget
 * 
 * The function is for removing the option from the widgets like CheckBox or RadioButton. The function removes the option with the given value.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Value         Value of the option to remove
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_AddOption
 * 
 * @code{.c}
   
    oC_WidgetScreen_RemoveOption(Screen,0,0);

 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_RemoveOption( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value )
{

    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            oC_IntMan_EnterCriticalSection();

            Option_t * option  = NULL;
            bool       success = oC_List_At(widget->Options,Value,option);

            if( oC_SaveIfFalse("OptionsList", success, oC_ErrorCode_ObjectNotFoundOnList) )
            {
                free((void*)option->Option.Label,0);
                free(option,0);
                oC_List_RemoveAll(widget->Options,option);
            }

            oC_IntMan_ExitCriticalSection();
        }
    }
}

//==========================================================================================================================================
/** 
 * @brief getter for the option
 * 
 * The function returns the option with the given value from the multiple-choice widget.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Value         Value of the option to get
 * 
 * @return pointer to the option or NULL if the option cannot be found
 * 
 * @see oC_WidgetScreen_GetCurrentOption, oC_WidgetScreen_AddOption, oC_WidgetScreen_RemoveOption
 * 
 * @code{.c}
   
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetOption(Screen,0,0);

 * @endcode
 */
//==========================================================================================================================================
const oC_WidgetScreen_Option_t * oC_WidgetScreen_GetOption( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value )
{
    const oC_WidgetScreen_Option_t * optionToReturn = NULL;

    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            Option_t * option  = NULL;
            bool       success = oC_List_At(widget->Options,Value,option);

            if( oC_SaveIfFalse( "option", success, oC_ErrorCode_ObjectNotFoundOnList ) )
            {
                optionToReturn = &option->Option;
            }
        }
    }

    return optionToReturn;
}

//==========================================================================================================================================
/**
 * @brief getter for the current option
 * 
 * The function returns the current active option from the multiple-choice widget.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * 
 * @return pointer to the option or NULL if the option cannot be found
 * 
 * @see oC_WidgetScreen_GetOption, oC_WidgetScreen_AddOption, oC_WidgetScreen_RemoveOption
 * 
 * @code{.c}
   
    const oC_WidgetScreen_Option_t * option = oC_WidgetScreen_GetCurrentOption(Screen,0);

 * @endcode
 */
//==========================================================================================================================================
const oC_WidgetScreen_Option_t * oC_WidgetScreen_GetCurrentOption( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex )
{
    const oC_WidgetScreen_Option_t * optionToReturn = NULL;

    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            foreach(widget->Options,option)
            {
                if(option->Checked)
                {
                    optionToReturn = &option->Option;
                    break;
                }
            }
        }
    }

    return optionToReturn;
}

//==========================================================================================================================================
/**
 * @brief removes the current option from the multiple-choice widget
 * 
 * The function is for removing the current active option from the widgets like CheckBox or RadioButton.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_GetCurrentOption, oC_WidgetScreen_AddOption, oC_WidgetScreen_RemoveOption
 * 
 * @code{.c}
   
    oC_WidgetScreen_RemoveCurrentOption(Screen,0);

 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_RemoveCurrentOption ( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            oC_WidgetScreen_RemoveOption(Screen,WidgetIndex,widget->Value);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief clears the options from the multiple-choice widget
 * 
 * The function is for removing all options from the widgets like CheckBox or RadioButton.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_GetCurrentOption, oC_WidgetScreen_AddOption, oC_WidgetScreen_RemoveOption
 * 
 * @code{.c}
   
    oC_WidgetScreen_ClearOptions(Screen,0);

 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_ClearOptions( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            oC_IntMan_EnterCriticalSection();
            foreach( widget->Options, option )
            {
                oC_SaveIfFalse( "option" , free(option,0), oC_ErrorCode_ReleaseError );
            }

            oC_List_Clear(widget->Options);

            widget->Value           = 0;
            widget->MaximumValue    = 0;

            oC_IntMan_ExitCriticalSection();
        }
    }
}
//==========================================================================================================================================
/**
 * @brief getter for the number of options
 * 
 * The function returns the number of options in the multiple-choice widget.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * 
 * @return number of options
 * 
 * @see oC_WidgetScreen_GetOption, oC_WidgetScreen_AddOption, oC_WidgetScreen_RemoveOption
 * 
 * @code{.c}
   
    oC_WidgetScreen_Value_t numberOfOptions = oC_WidgetScreen_GetNumberOfOptions(Screen,0);

 * @endcode
 */
//==========================================================================================================================================
oC_WidgetScreen_Value_t oC_WidgetScreen_GetValue( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex )
{
    WidgetValue_t value = 0;

    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            value = widget->Value;
        }
    }

    return value;
}

//==========================================================================================================================================
/**
 * @brief sets the visibility of the widget
 * 
 * The function sets the visibility of the widget.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Visible       Visibility of the widget
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 */
//==========================================================================================================================================
void oC_WidgetScreen_SetVisible( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , bool Visible )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            widget->Visible = Visible;
        }
    }
}

//==========================================================================================================================================
/**
 * @brief sets the value of the widget
 * 
 * The function sets the value of the widget (for example value of the progress bar).
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Value         Value to set
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_GetValue, oC_WidgetScreen_GetMaximumValue, oC_WidgetScreen_SetMaximumValue
 * 
 * @code{.c}
    
    // Set the value of the widget to 5
    oC_WidgetScreen_SetValue(Screen,0,5);

    // do some stuff ...


 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_SetValue( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if(
            oC_SaveIfFalse( "WidgetsList", success                      , oC_ErrorCode_ObjectNotFoundOnList )
         && oC_SaveIfFalse( "Value"      , Value < widget->MaximumValue , oC_ErrorCode_ValueTooBig          )
            )
        {
            widget->Value = Value;
        }
    }
}

//==========================================================================================================================================
/**
 * @brief gets the maximum value of the widget
 * 
 * The function gets the maximum value of the widget (for example maximum value of the progress bar).
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * 
 * @return maximum value of the widget
 * 
 * @see oC_WidgetScreen_GetValue, oC_WidgetScreen_SetValue, oC_WidgetScreen_SetMaximumValue
 * 
 * @code{.c}
    
    // Get the maximum value of the widget
    oC_WidgetScreen_Value_t maximumValue = oC_WidgetScreen_GetMaximumValue(Screen,0);

    for(oC_WidgetScreen_Value_t i = 0; i < maximumValue; i++)
    {
        // do some stuff ...

        // Set the value of the progress widget to i
        oC_WidgetScreen_SetValue(Screen,0,i);
    }

 * @endcode
 */
//==========================================================================================================================================
oC_WidgetScreen_Value_t oC_WidgetScreen_GetMaximumValue( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex )
{
    WidgetValue_t value = 0;

    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            value = widget->MaximumValue;
        }
    }

    return value;
}

//==========================================================================================================================================
/**
 * @brief sets the maximum value of the widget
 * 
 * The function sets the maximum value of the widget (for example maximum value of the progress bar).
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Value         Maximum value to set
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_GetValue, oC_WidgetScreen_SetValue, oC_WidgetScreen_GetMaximumValue
 * 
 * @code{.c}
    
    // Set the maximum value of the widget to 100
    oC_WidgetScreen_SetMaximumValue(Screen,0,100);

 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_SetMaximumValue( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success , oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            widget->MaximumValue = Value;
        }
    }
}

//==========================================================================================================================================
/**
 * @brief scrolls up the widget
 * 
 * The function scrolls up the widget (for example the RadioButton or CheckBox widget). 
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Value         Number of options to scroll up (0 means scroll up by the number of options on the screen)
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_ScrollDown
 * 
 * @code{.c}
    
    // Scroll up the widget by 1
    oC_WidgetScreen_ScrollUp(Screen,0,1);

 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_ScrollUp( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success , oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            if(Value == 0)
            {
                oC_Pixel_ResolutionUInt_t borderWidth      = widget->DrawStyle[widget->State].BorderWidth;
                WidgetValue_t             optionsPerRow    = (widget->Definition.Width  - (2*borderWidth)) / (widget->Definition.TypeSpecific.CheckBox.OptionWidth  + widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing );
                WidgetValue_t             numberOfRows     = (widget->Definition.Height - (2*borderWidth)) / (widget->Definition.TypeSpecific.CheckBox.OptionHeight + widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing   );
                WidgetValue_t             optionsPerScreen = optionsPerRow * numberOfRows;

                Value = optionsPerScreen;
            }

            if(widget->Value >= Value)
            {
                widget->Value -= Value;
            }
            else
            {
                widget->Value = 0;
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief scrolls down the widget
 * 
 * The function scrolls down the widget (for example the RadioButton or CheckBox widget). 
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * @param Value         Number of options to scroll down (0 means scroll down by the number of options on the screen)
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_ScrollUp
 * 
 * @code{.c}
    
    // Scroll down the widget by 1
    oC_WidgetScreen_ScrollDown(Screen,0,1);

 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_ScrollDown( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex , oC_WidgetScreen_Value_t Value )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success , oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            uint32_t optionsCount = oC_List_Count(widget->Options);
            if(Value == 0)
            {
                oC_Pixel_ResolutionUInt_t borderWidth      = widget->DrawStyle[widget->State].BorderWidth;
                WidgetValue_t             optionsPerRow    = (widget->Definition.Width  - (2*borderWidth)) / (widget->Definition.TypeSpecific.CheckBox.OptionWidth  + widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing );
                WidgetValue_t             numberOfRows     = (widget->Definition.Height - (2*borderWidth)) / (widget->Definition.TypeSpecific.CheckBox.OptionHeight + widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing   );
                WidgetValue_t             optionsPerScreen = optionsPerRow * numberOfRows;

                Value = optionsPerScreen;
            }

            if((widget->Value + Value) < optionsCount )
            {
                widget->Value += Value;
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief gets the palette of the widget
 * 
 * The function returns the palette of the widget.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * 
 * @return pointer to the palette or NULL if the palette cannot be found
 * 
 * @see oC_WidgetScreen_GetDrawStyle
 * 
 * @code{.c}
    
    const oC_WidgetScreen_Palette_t * palette = oC_WidgetScreen_GetPalette(Screen,0);

 * @endcode
 */
//==========================================================================================================================================
oC_WidgetScreen_Palette_t * oC_WidgetScreen_GetPalette( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex )
{
    oC_WidgetScreen_Palette_t * palette = NULL;

    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        if( oC_SaveIfFalse( "WidgetsList", success , oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            palette = &widget->Palette;
        }
    }

    return palette;
}

//==========================================================================================================================================
/**
 * @brief adds the widget to the screen
 * 
 * The function adds the widget to the screen.
 * 
 * @param Screen        Pointer to the screen object
 * @param Definition    Pointer to the widget definition
 * @param UserParameter Pointer to the user parameter (parameter will be passed to the widget handlers)
 * 
 * @return index of the widget
 * 
 * @see oC_WidgetScreen_RemoveWidget
 * 
 * @code{.c}
   
    oC_WidgetScreen_WidgetDefinition_t Definition = {
        .Type = oC_WidgetType_CheckBox,
        .Width = 100,
        .Height = 20,
        .Position = {10,10},
        .DrawStyle = &DrawStyle,
        .Palette = &Palette,
        .String = {
            .DefaultString = "CheckBox",
            .Font = oC_Font_(Consolas),
        },
    };
    oC_WidgetScreen_AddWidget(Screen,&Definition,NULL);

   @endcode
 */
//==========================================================================================================================================
oC_WidgetScreen_WidgetIndex_t oC_WidgetScreen_AddWidget( oC_WidgetScreen_t Screen, const oC_WidgetScreen_WidgetDefinition_t * Definition, void * UserParameter )
{
    oC_WidgetScreen_WidgetIndex_t index = 0;

    if(
        oC_SaveIfFalse( "Screen"    , IsScreenCorrect(Screen)       , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "Definition", isaddresscorrect(Definition)  , oC_ErrorCode_WrongAddress     )
     && CheckWidgetDefinition( Definition )
        )
    {
        Widget_t * widget = malloc( sizeof(Widget_t), AllocationFlags_ZeroFill | AllocationFlags_ExternalRamFirst);
        void *     list   = oC_List_New(getcurallocator(), 0);

        if(
            oC_SaveIfFalse( "widget", widget != NULL, oC_ErrorCode_AllocationError )
         && oC_SaveIfFalse( "list"  , list   != NULL, oC_ErrorCode_AllocationError )
            )
        {
            memcpy(&widget->Definition, Definition              , sizeof(WidgetDefinition_t)    );
            memcpy(&widget->DrawStyle , Definition->DrawStyle   , sizeof(widget->DrawStyle)     );
            memcpy(&widget->Palette   , Definition->Palette     , sizeof(widget->Palette)       );

            widget->ObjectControl           = oC_CountObjectControl(widget,oC_ObjectId_Widget);
            widget->Definition.DrawStyle    = (const DrawStyle_t *)&widget->DrawStyle;
            widget->Definition.Palette      = (const Palette_t *)&widget->Palette;
            widget->DrawString              = strlen(widget->Definition.String.DefaultString) > 0;
            widget->RelativePosition.X      = Definition->Position.X;
            widget->RelativePosition.Y      = Definition->Position.Y;
            widget->RelativeEndPosition.X   = Definition->Position.X + Definition->Width;
            widget->RelativeEndPosition.Y   = Definition->Position.Y + Definition->Height;
            widget->MaxReleaseTimeout       = s(2);
            widget->MaxReleaseTimestamp     = 0;
            widget->MaximumValue            = 100;
            widget->Value                   = 0;
            widget->Options                 = list;
            widget->UserContext             = UserParameter;
            widget->Visible                 = true;
            widget->Screen                  = Screen->Screen;

            Screen->MaxZPosition            = oC_MAX(Screen->MaxZPosition,widget->Definition.ZPosition);

            bool pushed = oC_List_PushBack( Screen->Widgets, widget, getcurallocator() );

            if( oC_SaveIfFalse("Widgets List", pushed, oC_ErrorCode_CannotAddObjectToList    ) )
            {
                index = oC_List_IndexOf(Screen->Widgets,widget);
            }
            else
            {
                oC_SaveIfFalse( "widget", widget == NULL || free(widget,0) , oC_ErrorCode_ReleaseError );
                oC_SaveIfFalse( "list"  , list   == NULL || free(list,0)   , oC_ErrorCode_ReleaseError );
            }
        }
        else
        {
            oC_SaveIfFalse( "widget", widget == NULL || free(widget,0) , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "list"  , list   == NULL || free(list,0)   , oC_ErrorCode_ReleaseError );
        }
    }

    return index;
}

//==========================================================================================================================================
/**
 * @brief removes the widget from the screen
 * 
 * The function removes the widget from the screen.
 * 
 * @param Screen        Pointer to the screen object
 * @param WidgetIndex   Index of the widget on the screen
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_AddWidget
 * 
 * @code{.c}
   
    oC_WidgetScreen_RemoveWidget(Screen,0);

 * @endcode
 */
//==========================================================================================================================================
void oC_WidgetScreen_RemoveWidget( oC_WidgetScreen_t Screen, oC_WidgetScreen_WidgetIndex_t WidgetIndex )
{
    if(
        oC_SaveIfFalse( "Screen"        , IsScreenCorrect(Screen)               , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "WidgetIndex"   , WidgetIndex < Screen->NumberOfWidgets , oC_ErrorCode_IndexNotCorrect  )
        )
    {
        Widget_t * widget  = NULL;
        bool       success = oC_List_At(Screen->Widgets,WidgetIndex,widget);

        oC_IntMan_EnterCriticalSection();

        if( oC_SaveIfFalse( "WidgetsList", success , oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            // We cannot change indexes, so we have to only release widget memory and not remove it from the list
            widget->ObjectControl = 0;

            oC_ICtrlMan_UnregisterActivity(&widget->ActiveActivity  );
            oC_ICtrlMan_UnregisterActivity(&widget->ReleaseActivity );

            oC_SaveIfFalse("list"   , oC_List_Delete(widget->Options,0) , oC_ErrorCode_ReleaseError);
            oC_SaveIfFalse("widget" , free(widget,0)                    , oC_ErrorCode_ReleaseError);
        }

        oC_IntMan_ExitCriticalSection();
    }
}

//==========================================================================================================================================
/**
 * @brief handles the screen
 * 
 * This is the main function to handle the screen. The function is responsible for drawing the screen and handling the widgets. It will 
 * never return until the screen is closed.
 * 
 * To exit from this function you have to return invalid screen ID as the next screen ID.
 * 
 * @param Screen                Pointer to the screen object
 * @param Definitions           Pointer to the screen definitions
 * @param NumberOfScreens       Number of screens
 * @param UserParameter         Pointer to the user parameter (parameter will be passed to the widget handlers)
 * 
 * @return error code or #oC_ErrorCode_None if no error occurs
 * 
 * Possible error codes:
 * 
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_WrongAddress                   | Wrong address given as the parameter (Definitions)
 *  #oC_ErrorCode_SizeNotCorrect                 | Number of screens is less than 1
 *  #oC_ErrorCode_NoSuchScreen                   | No such screen
 *  #oC_ErrorCode_ObjectNotCorrect               | Object is not correct
 *  #oC_ErrorCode_AllocationError                | Cannot allocate memory 
 *  #oC_ErrorCode_CannotAddObjectToList          | Cannot add object to the list
 * 
 * 
 * <b>Example:</b>
 * @code{.c}
   
    oC_WidgetScreen_HandleScreens(Screen,Definitions,NumberOfScreens,NULL);

 * @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_WidgetScreen_HandleScreens( oC_Screen_t Screen , const oC_WidgetScreen_ScreenDefinition_t * Definitions , oC_WidgetScreen_ScreenId_t NumberOfScreens, void * UserParameter )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Definitions)                 , oC_ErrorCode_WrongAddress     )
     && ErrorCondition( NumberOfScreens > 0                           , oC_ErrorCode_SizeNotCorrect   )
        )
    {
        Screen = Screen == NULL ? oC_ScreenMan_GetDefaultScreen() : Screen;

        if(
            ErrorCondition( Screen != NULL              , oC_ErrorCode_NoSuchScreen     )
         && ErrorCondition( oC_Screen_IsCorrect(Screen) , oC_ErrorCode_ObjectNotCorrect )
            )
        {
            oC_List(oC_WidgetScreen_t*) screens     = oC_List_New(getcurallocator(), 0);
            oC_WidgetScreen_ScreenId_t  screenIndex = 0;

            (void)screenIndex;

            if( ErrorCondition( screens != NULL, oC_ErrorCode_AllocationError ) )
            {
                errorCode = oC_ErrorCode_None;

                oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(Definitions,NumberOfScreens,definition)
                {
                    if( ErrorCode( CheckScreenDefinition(definition) ) )
                    {
                        oC_WidgetScreen_t screen = NULL;

                        if( ErrorCode( CreateNewScreen(Screen, definition, &screen, UserParameter) ) )
                        {
                            bool pushed = oC_List_PushBack(screens,screen,getcurallocator());

                            if(! ErrorCondition( pushed, oC_ErrorCode_CannotAddObjectToList ))
                            {
                                DeleteScreen(&screen);
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        kdebuglog(oC_LogType_Error, "Definition of the screen %d is incorrect!", screenIndex);
                        break;
                    }
                    screenIndex++;
                }

                if(!oC_ErrorOccur(errorCode))
                {
                    oC_WidgetScreen_t           screen       = NULL;
                    oC_WidgetScreen_t           lastScreen   = NULL;
                    oC_WidgetScreen_ScreenId_t  screenId     = 0;
                    oC_WidgetScreen_ScreenId_t  nextScreenId = 0;

                    oC_List_At(screens,screenId,screen);

                    while(IsScreenCorrect(screen))
                    {
                        nextScreenId = PrepareScreen(screen,UserParameter,screenId);

                        if(nextScreenId == screenId)
                        {
                            nextScreenId = HandleScreen(screen,UserParameter,screenId);
                        }
                        lastScreen = screen;
                        screen = NULL;
                        oC_List_At(screens,nextScreenId,screen);
                        screenId = nextScreenId;
                    }
                    if(lastScreen != NULL)
                    {
                        oC_ColorMap_DrawBackground(lastScreen->ColorMap,lastScreen->Definition.BackgroundColor,lastScreen->Definition.ColorFormat);
                    }
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief draws the widget
 * 
 * The function draws the widget.
 * 
 * @param Widget        Pointer to the widget object
 * @param ColorMap      Pointer to the color map
 * @param ScreenPosition Position of the widget on the screen
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_DrawScreen
 * 
 * @code{.c}
   
    oC_WidgetScreen_DrawWidget(Widget,ColorMap,ScreenPosition);

 * @endcode
 */
//==========================================================================================================================================
static void DrawString( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition )
{
    if(Widget->DrawStyle[Widget->State].DrawText && Widget->DrawStyle[Widget->State].Opacity != 0)
    {
        oC_Pixel_Position_t       widgetPosition;
        oC_Pixel_Position_t       stringPosition;
        oC_Color_t                textColor       = Widget->Palette[Widget->State].TextColor;
        oC_ColorFormat_t          colorFormat     = Widget->Palette[Widget->State].ColorFormat;
        oC_Pixel_ResolutionUInt_t stringWidth     = 0;
        oC_Pixel_ResolutionUInt_t stringHeight    = 0;
        oC_Pixel_ResolutionUInt_t stringBoxWidth  = Widget->Definition.Width - (Widget->DrawStyle[Widget->State].BorderWidth * 2);

        widgetPosition.X = Widget->RelativePosition.X + ScreenPosition.X;
        widgetPosition.Y = Widget->RelativePosition.Y + ScreenPosition.Y;

        if(Widget->DrawStyle[Widget->State].Opacity != 0xFF)
        {
            textColor   = oC_Color_ConvertToFormat(textColor,colorFormat,oC_ColorFormat_ARGB8888);
            colorFormat = oC_ColorFormat_ARGB8888;
            textColor  |= ((oC_Color_t)Widget->DrawStyle[Widget->State].Opacity) << 24;
        }
        else
        {
            textColor   = oC_Color_ConvertToFormat(textColor,colorFormat,ColorMap->ColorFormat);
            colorFormat = ColorMap->ColorFormat;
        }

        oC_Font_ReadStringSize(Widget->Definition.String.Font,
                               Widget->Definition.String.DefaultString,
                               &stringWidth,
                               &stringHeight,
                               stringBoxWidth);

        if(Widget->Definition.String.TextAlign == oC_WidgetScreen_TextAlign_Left)
        {
            stringPosition.X = widgetPosition.X;
        }
        else if(Widget->Definition.String.TextAlign == oC_WidgetScreen_TextAlign_Right)
        {
            stringPosition.X = widgetPosition.X + (Widget->Definition.Width - stringWidth);
        }
        else
        {
            stringPosition.X = widgetPosition.X + ((Widget->Definition.Width - stringWidth)/2);
        }

        if(Widget->Definition.String.VerticalTextAlign == oC_WidgetScreen_VerticalTextAlign_Up)
        {
            stringPosition.Y = widgetPosition.Y;
        }
        else if(Widget->Definition.String.VerticalTextAlign == oC_WidgetScreen_VerticalTextAlign_Down)
        {
            stringPosition.Y = widgetPosition.Y + (Widget->Definition.Height - stringHeight);
        }
        else
        {
            stringPosition.Y = widgetPosition.Y + ((Widget->Definition.Height - stringHeight)/2);
        }

        oC_ColorMap_DrawString(ColorMap,
                               stringPosition,
                               textColor,
                               colorFormat,
                               Widget->Definition.String.DefaultString,
                               Widget->Definition.String.Font,
                               Widget->Definition.Width,
                               Widget->Definition.Height
                               );
    }
}

//==========================================================================================================================================
/**
 * @brief draws the widget
 * 
 * The function draws the widget.
 * 
 * @param Widget        Pointer to the widget object
 * @param ColorMap      Pointer to the color map
 * @param ScreenPosition Position of the widget on the screen
 * 
 * @note The function does not return any value. To check the error, you can use the function #oC_ReadLastError.
 * 
 * @see oC_WidgetScreen_DrawScreen
 * 
 * @code{.c}
   
    oC_WidgetScreen_DrawWidget(Widget,ColorMap,ScreenPosition);

 * @endcode
 */
//==========================================================================================================================================
static void DrawTextBox( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition )
{
    oC_Pixel_ResolutionUInt_t   borderWidth;
    oC_Color_t                  borderColor;
    oC_Color_t                  fillColor;
    oC_ColorFormat_t            colorFormat;
    uint8_t                     opacity;
    bool                        drawFill;
    oC_Pixel_Position_t         absolutePosition, absoluteEndPosition;
    oC_ColorMap_BorderStyle_t   borderStyle;

    borderWidth     = Widget->DrawStyle[Widget->State].BorderWidth;
    borderColor     = Widget->Palette[Widget->State].BorderColor;
    fillColor       = Widget->Palette[Widget->State].FillColor;
    colorFormat     = Widget->Palette[Widget->State].ColorFormat;
    borderStyle     = Widget->DrawStyle[Widget->State].BorderStyle;

    opacity         = Widget->DrawStyle[Widget->State].Opacity;
    drawFill        = Widget->DrawStyle[Widget->State].DrawFill;

    if(opacity != 0xFF)
    {
        borderColor = oC_Color_ConvertToFormat(borderColor  , colorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)opacity) << 24 );
        fillColor   = oC_Color_ConvertToFormat(fillColor    , colorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)opacity) << 24 );
        colorFormat = oC_ColorFormat_ARGB8888;
    }
    else
    {
        borderColor = oC_Color_ConvertToFormat(borderColor  , colorFormat,  ColorMap->ColorFormat);
        fillColor   = oC_Color_ConvertToFormat(fillColor    , colorFormat,  ColorMap->ColorFormat);
    }

    if(opacity != 0x00)
    {
        absolutePosition.X      = Widget->RelativePosition.X    + ScreenPosition.X;
        absolutePosition.Y      = Widget->RelativePosition.Y    + ScreenPosition.Y;
        absoluteEndPosition.X   = Widget->RelativeEndPosition.X + ScreenPosition.X;
        absoluteEndPosition.Y   = Widget->RelativeEndPosition.Y + ScreenPosition.Y;
        oC_ColorMap_DrawRect(ColorMap,absolutePosition,absoluteEndPosition,borderWidth,borderStyle,borderColor,fillColor,colorFormat,drawFill);
    }

}

//==========================================================================================================================================
/**
 * @brief registers the push button activity
 */
//==========================================================================================================================================
static bool RegisterPushButtonActivity( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition )
{
    bool success = false;

    Widget->ActiveActivity.EventsMask     = oC_IDI_EventId_AnyController | oC_IDI_EventId_ClickWithoutRelease;
    Widget->ReleaseActivity.EventsMask    = oC_IDI_EventId_AnyController | oC_IDI_EventId_AnyMove;

    Widget->ActiveActivity.Position.X     = Widget->RelativePosition.X + ScreenPosition.X;
    Widget->ActiveActivity.Position.Y     = Widget->RelativePosition.Y + ScreenPosition.Y;
    Widget->ReleaseActivity.Position.X    = Widget->RelativePosition.X + ScreenPosition.X;
    Widget->ReleaseActivity.Position.Y    = Widget->RelativePosition.Y + ScreenPosition.Y;

    Widget->ActiveActivity.Width          = Widget->Definition.Width;
    Widget->ActiveActivity.Height         = Widget->Definition.Height;
    Widget->ReleaseActivity.Width         = Widget->Definition.Width;
    Widget->ReleaseActivity.Height        = Widget->Definition.Height;

    Widget->ActiveActivity.Screen         = Widget->Screen;
    Widget->ReleaseActivity.Screen        = Widget->Screen;

    if(
        oC_SaveIfErrorOccur("Active  Activity", oC_ICtrlMan_RegisterActivity(&Widget->ActiveActivity , &Widget->ActiveEvent     ))
     && oC_SaveIfErrorOccur("Release Activity", oC_ICtrlMan_RegisterActivity(&Widget->ReleaseActivity, &Widget->ReleaseEvent    ))
        )
    {
        success = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief draws the push button
 */
//==========================================================================================================================================
static void DrawPushButton( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition )
{
    oC_Pixel_Position_t         position;
    oC_Pixel_Position_t         endPosition;
    oC_Pixel_ResolutionUInt_t   borderWidth;
    oC_Color_t                  borderColor;
    oC_Color_t                  fillColor;
    oC_ColorFormat_t            colorFormat;
    uint8_t                     opacity;
    bool                        drawFill;
    oC_ColorMap_BorderStyle_t   borderStyle;

    if(Widget->State == WidgetState_Activated)
    {
        Widget->State = WidgetState_Ready;
    }

    position.X      = Widget->RelativePosition.X + ScreenPosition.X;
    position.Y      = Widget->RelativePosition.Y + ScreenPosition.Y;

    endPosition.X   = Widget->RelativeEndPosition.X + ScreenPosition.X;
    endPosition.Y   = Widget->RelativeEndPosition.Y + ScreenPosition.Y;

    borderWidth     = Widget->DrawStyle[Widget->State].BorderWidth;
    borderColor     = Widget->Palette[Widget->State].BorderColor;
    fillColor       = Widget->Palette[Widget->State].FillColor;
    colorFormat     = Widget->Palette[Widget->State].ColorFormat;
    borderStyle     = Widget->DrawStyle[Widget->State].BorderStyle;

    opacity         = Widget->DrawStyle[Widget->State].Opacity;
    drawFill        = Widget->DrawStyle[Widget->State].DrawFill;

    if(opacity != 0xFF)
    {
        borderColor = oC_Color_ConvertToFormat(borderColor  , colorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)opacity) << 24 );
        fillColor   = oC_Color_ConvertToFormat(fillColor    , colorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)opacity) << 24 );
        colorFormat = oC_ColorFormat_ARGB8888;
    }
    else
    {
        borderColor = oC_Color_ConvertToFormat(borderColor  , colorFormat,  ColorMap->ColorFormat);
        fillColor   = oC_Color_ConvertToFormat(fillColor    , colorFormat,  ColorMap->ColorFormat);
    }

    if(opacity != 0x00)
    {
        if(Widget->Definition.TypeSpecific.PushButton.Type == oC_WidgetScreen_PushButtonType_Standard)
        {
            oC_ColorMap_DrawRect(ColorMap,position,endPosition,borderWidth,borderStyle,borderColor,fillColor,colorFormat,drawFill);
        }
        else if(Widget->Definition.TypeSpecific.PushButton.Type == oC_WidgetScreen_PushButtonType_ArrowUp)
        {
            oC_ColorMap_DrawArrow(ColorMap,position,Widget->Definition.Width,Widget->Definition.Height,oC_ColorMap_Direction_Up,borderWidth,borderColor,fillColor,colorFormat,drawFill);
        }
        else if(Widget->Definition.TypeSpecific.PushButton.Type == oC_WidgetScreen_PushButtonType_ArrowDown)
        {
            oC_ColorMap_DrawArrow(ColorMap,position,Widget->Definition.Width,Widget->Definition.Height,oC_ColorMap_Direction_Down,borderWidth,borderColor,fillColor,colorFormat,drawFill);
        }
        else if(Widget->Definition.TypeSpecific.PushButton.Type == oC_WidgetScreen_PushButtonType_ArrowLeft)
        {
            oC_ColorMap_DrawArrow(ColorMap,position,Widget->Definition.Width,Widget->Definition.Height,oC_ColorMap_Direction_Left,borderWidth,borderColor,fillColor,colorFormat,drawFill);
        }
        else if(Widget->Definition.TypeSpecific.PushButton.Type == oC_WidgetScreen_PushButtonType_ArrowRight)
        {
            oC_ColorMap_DrawArrow(ColorMap,position,Widget->Definition.Width,Widget->Definition.Height,oC_ColorMap_Direction_Right,borderWidth,borderColor,fillColor,colorFormat,drawFill);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief updates the push button state
 */
//==========================================================================================================================================
static bool UpdatePushButtonState( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition )
{
    WidgetState_t oldState = Widget->State;

    if(Widget->State != WidgetState_Disabled)
    {
        oC_Screen_t         screen          = NULL;
        oC_Pixel_Position_t cursorPosition  = { .X = 0 , .Y = 0 };

        oC_ICtrlMan_ReadCursorPosition(&screen,&cursorPosition);

        if(oC_ICtrlMan_HasActivityOccurred(&Widget->ActiveActivity))
        {
            Widget->MaxReleaseTimestamp = gettimestamp() + Widget->MaxReleaseTimeout;
            Widget->State               = WidgetState_Pressed;
        }
        else if(oC_ICtrlMan_HasActivityOccurred(&Widget->ReleaseActivity))
        {
            Widget->State = WidgetState_Activated;
        }
        else if(
                screen == Widget->Screen
             && cursorPosition.X >= (Widget->RelativePosition.X    + ScreenPosition.X)
             && cursorPosition.Y >= (Widget->RelativePosition.Y    + ScreenPosition.Y)
             && cursorPosition.X  < (Widget->RelativeEndPosition.X + ScreenPosition.X)
             && cursorPosition.Y  < (Widget->RelativeEndPosition.Y + ScreenPosition.Y)
                )
        {
            Widget->State = WidgetState_Hovered;
        }
        else if(Widget->State != WidgetState_Activated || gettimestamp() >= Widget->MaxReleaseTimestamp)
        {
            Widget->State = WidgetState_Ready;
        }
    }

    return oldState != Widget->State;
}

//==========================================================================================================================================
/**
 * @brief checks the push button definition
 */
//==========================================================================================================================================
static bool CheckPushButtonDefinition( const WidgetDefinition_t * Definition )
{
    bool correct = false;

    if( oC_SaveIfFalse( "PushButtonType", Definition->TypeSpecific.PushButton.Type < oC_WidgetScreen_PushButtonType_NumberOfElements, oC_ErrorCode_TypeNotCorrect ) )
    {
        correct = true;
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief creates the new push button
 */
//==========================================================================================================================================
static bool RegisterCheckBoxActivity( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition )
{
    bool success = false;

    Widget->ActiveActivity.EventsMask     = oC_IDI_EventId_AnyController | oC_IDI_EventId_ClickWithoutRelease;
    Widget->ReleaseActivity.EventsMask    = oC_IDI_EventId_AnyController | oC_IDI_EventId_AnyMove;

    Widget->ActiveActivity.Position.X     = Widget->RelativePosition.X + ScreenPosition.X;
    Widget->ActiveActivity.Position.Y     = Widget->RelativePosition.Y + ScreenPosition.Y;
    Widget->ReleaseActivity.Position.X    = Widget->RelativePosition.X + ScreenPosition.X;
    Widget->ReleaseActivity.Position.Y    = Widget->RelativePosition.Y + ScreenPosition.Y;

    Widget->ActiveActivity.Width          = Widget->Definition.Width;
    Widget->ActiveActivity.Height         = Widget->Definition.Height;
    Widget->ReleaseActivity.Width         = Widget->Definition.Width;
    Widget->ReleaseActivity.Height        = Widget->Definition.Height;

    Widget->ActiveActivity.Screen         = Widget->Screen;
    Widget->ReleaseActivity.Screen        = Widget->Screen;

    if(
        oC_SaveIfErrorOccur("Active  Activity", oC_ICtrlMan_RegisterActivity(&Widget->ActiveActivity , &Widget->ActiveEvent     ))
     && oC_SaveIfErrorOccur("Release Activity", oC_ICtrlMan_RegisterActivity(&Widget->ReleaseActivity, &Widget->ReleaseEvent    ))
        )
    {
        success = true;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief draws the check box
 */
//==========================================================================================================================================
static void DrawCheckBox( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition )
{
    oC_Pixel_ResolutionUInt_t   borderWidth      = Widget->DrawStyle[Widget->State].BorderWidth;
    WidgetValue_t               optionsPerRow    = (Widget->Definition.Width  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing) / (Widget->Definition.TypeSpecific.CheckBox.OptionWidth  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing );
    WidgetValue_t               numberOfRows     = (Widget->Definition.Height + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing  ) / (Widget->Definition.TypeSpecific.CheckBox.OptionHeight + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing   );
    WidgetValue_t               optionsPerScreen = optionsPerRow * numberOfRows;
    oC_List_ElementHandle_t     elementHandle    = List_ElementOfIndex((oC_List_t)Widget->Options,Widget->Value);
    WidgetValue_t               drawedCount      = 0;
    WidgetValue_t               columnIndex      = 0;
    WidgetValue_t               rowIndex         = 0;
    oC_Pixel_ResolutionInt_t    columnWidth      = Widget->Definition.TypeSpecific.CheckBox.OptionWidth  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing;
    oC_Pixel_ResolutionInt_t    rowHeight        = Widget->Definition.TypeSpecific.CheckBox.OptionHeight + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing;
    oC_Pixel_Position_t         drawPosition;
    oC_Pixel_Position_t         drawEndPosition;
    Option_t *                  option              = NULL;
    oC_ColorMap_BorderStyle_t   borderStyle         = 0;
    uint8_t                     activeOpacity       = Widget->DrawStyle[WidgetState_Activated].Opacity;
    uint8_t                     pressedOpacity      = Widget->DrawStyle[WidgetState_Pressed].Opacity;
    uint8_t                     readyOpacity        = Widget->DrawStyle[WidgetState_Ready].Opacity;
    uint8_t                     hoverOpacity        = Widget->DrawStyle[WidgetState_Hovered].Opacity;
    oC_ColorFormat_t            activeColorFormat   = Widget->Palette[WidgetState_Activated].ColorFormat;
    oC_ColorFormat_t            pressedColorFormat  = Widget->Palette[WidgetState_Pressed].ColorFormat;
    oC_ColorFormat_t            readyColorFormat    = Widget->Palette[WidgetState_Ready].ColorFormat;
    oC_ColorFormat_t            hoverColorFormat    = Widget->Palette[WidgetState_Hovered].ColorFormat;
    oC_Color_t                  activeBorderColor   = Widget->Palette[WidgetState_Activated].BorderColor;
    oC_Color_t                  activeFillColor     = Widget->Palette[WidgetState_Activated].FillColor;
    oC_Color_t                  activeTextColor     = Widget->Palette[WidgetState_Activated].TextColor;
    oC_Color_t                  readyBorderColor    = Widget->Palette[WidgetState_Ready].BorderColor;
    oC_Color_t                  readyFillColor      = Widget->Palette[WidgetState_Ready].FillColor;
    oC_Color_t                  readyTextColor      = Widget->Palette[WidgetState_Ready].TextColor;
    oC_Color_t                  hoverBorderColor    = Widget->Palette[WidgetState_Hovered].BorderColor;
    oC_Color_t                  hoverFillColor      = Widget->Palette[WidgetState_Hovered].FillColor;
    oC_Color_t                  hoverTextColor      = Widget->Palette[WidgetState_Hovered].TextColor;
    oC_Color_t                  pressedBorderColor  = Widget->Palette[WidgetState_Pressed].BorderColor;
    oC_Color_t                  pressedFillColor    = Widget->Palette[WidgetState_Pressed].FillColor;
    oC_Color_t                  pressedTextColor    = Widget->Palette[WidgetState_Pressed].TextColor;
    oC_Color_t                  borderColor         = 0;
    oC_Color_t                  fillColor           = 0;
    oC_Color_t                  textColor           = 0;
    oC_ColorFormat_t            colorFormat         = 0;
    bool                        drawFill            = false;
    uint8_t                     opacity             = 0;

    if(activeOpacity == 0xFF)
    {
        activeBorderColor   = oC_Color_ConvertToFormat(activeBorderColor, activeColorFormat, ColorMap->ColorFormat);
        activeFillColor     = oC_Color_ConvertToFormat(activeFillColor,   activeColorFormat, ColorMap->ColorFormat);
        activeTextColor     = oC_Color_ConvertToFormat(activeTextColor,   activeColorFormat, ColorMap->ColorFormat);
    }
    else
    {
        activeBorderColor   = oC_Color_ConvertToFormat(activeBorderColor,   activeColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)activeOpacity ) << 24 );
        activeFillColor     = oC_Color_ConvertToFormat(activeFillColor,     activeColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)activeOpacity ) << 24 );
        activeTextColor     = oC_Color_ConvertToFormat(activeTextColor,     activeColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)activeOpacity ) << 24 );
        activeColorFormat   = oC_ColorFormat_ARGB8888;
    }

    if(pressedOpacity == 0xFF)
    {
        pressedBorderColor   = oC_Color_ConvertToFormat(pressedBorderColor, pressedColorFormat, ColorMap->ColorFormat);
        pressedFillColor     = oC_Color_ConvertToFormat(pressedFillColor,   pressedColorFormat, ColorMap->ColorFormat);
        pressedTextColor     = oC_Color_ConvertToFormat(pressedTextColor,   pressedColorFormat, ColorMap->ColorFormat);
    }
    else
    {
        pressedBorderColor   = oC_Color_ConvertToFormat(pressedBorderColor,   pressedColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)pressedOpacity ) << 24 );
        pressedFillColor     = oC_Color_ConvertToFormat(pressedFillColor,     pressedColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)pressedOpacity ) << 24 );
        pressedTextColor     = oC_Color_ConvertToFormat(pressedTextColor,     pressedColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)pressedOpacity ) << 24 );
        pressedColorFormat   = oC_ColorFormat_ARGB8888;
    }

    if(readyOpacity == 0xFF)
    {
        readyBorderColor = oC_Color_ConvertToFormat(readyBorderColor,   readyColorFormat,   ColorMap->ColorFormat);
        readyFillColor   = oC_Color_ConvertToFormat(readyFillColor,     readyColorFormat,   ColorMap->ColorFormat);
        readyTextColor   = oC_Color_ConvertToFormat(readyTextColor,     readyColorFormat,   ColorMap->ColorFormat);
    }
    else
    {
        readyBorderColor = oC_Color_ConvertToFormat(readyBorderColor,   readyColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)readyOpacity ) << 24 );
        readyFillColor   = oC_Color_ConvertToFormat(readyFillColor,     readyColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)readyOpacity ) << 24 );
        readyTextColor   = oC_Color_ConvertToFormat(readyTextColor,     readyColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)readyOpacity ) << 24 );
        readyColorFormat = oC_ColorFormat_ARGB8888;
    }

    if(hoverOpacity == 0xFF)
    {
        hoverBorderColor = oC_Color_ConvertToFormat(hoverBorderColor,   hoverColorFormat,   ColorMap->ColorFormat);
        hoverFillColor   = oC_Color_ConvertToFormat(hoverFillColor,     hoverColorFormat,   ColorMap->ColorFormat);
        hoverTextColor   = oC_Color_ConvertToFormat(hoverTextColor,     hoverColorFormat,   ColorMap->ColorFormat);
    }
    else
    {
        hoverBorderColor = oC_Color_ConvertToFormat(hoverBorderColor,   hoverColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)hoverOpacity ) << 24 );
        hoverFillColor   = oC_Color_ConvertToFormat(hoverFillColor,     hoverColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)hoverOpacity ) << 24 );
        hoverTextColor   = oC_Color_ConvertToFormat(hoverTextColor,     hoverColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)hoverOpacity ) << 24 );
        hoverColorFormat = oC_ColorFormat_ARGB8888;
    }

    for( drawedCount = 0, columnIndex = 0; elementHandle != NULL && drawedCount < optionsPerScreen; drawedCount++, columnIndex++)
    {
        if(columnIndex >= optionsPerRow)
        {
            columnIndex = 0;
            rowIndex++;
        }

        drawPosition.X      = columnIndex * columnWidth + Widget->RelativePosition.X + ScreenPosition.X;
        drawPosition.Y      = rowIndex    * rowHeight   + Widget->RelativePosition.Y + ScreenPosition.Y;
        drawEndPosition.X   = drawPosition.X + Widget->Definition.TypeSpecific.CheckBox.OptionWidth;
        drawEndPosition.Y   = drawPosition.Y + Widget->Definition.TypeSpecific.CheckBox.OptionHeight;
        option              = oC_List_ElementHandle_Value(elementHandle,Option_t*);

        if(option != NULL)
        {
            if(option->State == WidgetState_Activated)
            {
                borderStyle = Widget->DrawStyle[WidgetState_Activated].BorderStyle;
                drawFill    = Widget->DrawStyle[WidgetState_Activated].DrawFill;
                borderWidth = Widget->DrawStyle[WidgetState_Activated].BorderWidth;
                borderColor = activeBorderColor;
                fillColor   = activeFillColor;
                textColor   = activeTextColor;
                colorFormat = activeColorFormat;
                opacity     = activeOpacity;
            }
            else if(option->State == WidgetState_Pressed)
            {
                borderStyle = Widget->DrawStyle[WidgetState_Pressed].BorderStyle;
                drawFill    = Widget->DrawStyle[WidgetState_Pressed].DrawFill;
                borderWidth = Widget->DrawStyle[WidgetState_Pressed].BorderWidth;
                borderColor = pressedBorderColor;
                fillColor   = pressedFillColor;
                textColor   = pressedTextColor;
                colorFormat = pressedColorFormat;
                opacity     = pressedOpacity;
            }
            else if(option->State == WidgetState_Hovered)
            {
                borderStyle = Widget->DrawStyle[WidgetState_Hovered].BorderStyle;
                drawFill    = Widget->DrawStyle[WidgetState_Hovered].DrawFill;
                borderWidth = Widget->DrawStyle[WidgetState_Hovered].BorderWidth;
                borderColor = hoverBorderColor;
                fillColor   = hoverFillColor;
                textColor   = hoverTextColor;
                colorFormat = hoverColorFormat;
                opacity     = hoverOpacity;
            }
            else
            {
                borderStyle = Widget->DrawStyle[WidgetState_Ready].BorderStyle;
                drawFill    = Widget->DrawStyle[WidgetState_Ready].DrawFill;
                borderWidth = Widget->DrawStyle[WidgetState_Ready].BorderWidth;
                borderColor = readyBorderColor;
                fillColor   = readyFillColor;
                textColor   = readyTextColor;
                colorFormat = readyColorFormat;
                opacity     = readyOpacity;
            }

            if(option->Option.ChangeFillColor)
            {
                fillColor = option->Option.FillColor | (((oC_Color_t)opacity) << 24);
            }

            oC_ColorMap_DrawRect(ColorMap,drawPosition,drawEndPosition,borderWidth,borderStyle,borderColor,fillColor,colorFormat,drawFill);

            if(option->Option.Label != NULL && option->Option.Font != NULL)
            {
                oC_ColorMap_DrawAlignedString(
                                ColorMap,
                                drawPosition,
                                textColor,
                                colorFormat,
                                option->Option.Label,
                                option->Option.Font,
                                Widget->Definition.TypeSpecific.CheckBox.OptionWidth,
                                Widget->Definition.TypeSpecific.CheckBox.OptionHeight,
                                option->Option.TextAlign,
                                option->Option.VerticalTextAlign,
                                (borderWidth * 2) + option->Option.Margin);
            }
        }

        elementHandle   = elementHandle->Next;
    }
}

//==========================================================================================================================================
/**
 * @brief updates the check box state
 */
//==========================================================================================================================================
static bool UpdateCheckBoxState( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition )
{
    bool stateChanged = false;

    if(Widget->State != WidgetState_Disabled)
    {
        oC_Screen_t                 screen           = NULL;
        oC_Pixel_Position_t         cursorPosition   = { .X = 0 , .Y = 0 };
        bool                        activityOccurred = oC_ICtrlMan_HasActivityOccurred(&Widget->ActiveActivity);
        bool                        releaseOccurred  = oC_ICtrlMan_HasActivityOccurred(&Widget->ReleaseActivity);
        WidgetValue_t               optionsPerRow    = (Widget->Definition.Width  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing) / (Widget->Definition.TypeSpecific.CheckBox.OptionWidth  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing );
        WidgetValue_t               numberOfRows     = (Widget->Definition.Height + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing  ) / (Widget->Definition.TypeSpecific.CheckBox.OptionHeight + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing   );
        WidgetValue_t               optionsPerScreen = optionsPerRow * numberOfRows;
        oC_List_ElementHandle_t     elementHandle    = List_ElementOfIndex((oC_List_t)Widget->Options,Widget->Value);
        WidgetValue_t               drawedCount      = 0;
        WidgetValue_t               columnIndex      = 0;
        WidgetValue_t               rowIndex         = 0;
        oC_Pixel_ResolutionInt_t    columnWidth      = Widget->Definition.TypeSpecific.CheckBox.OptionWidth  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing;
        oC_Pixel_ResolutionInt_t    rowHeight        = Widget->Definition.TypeSpecific.CheckBox.OptionHeight + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing;
        oC_Pixel_Position_t         drawPosition;
        oC_Pixel_Position_t         drawEndPosition;
        Option_t *                  option              = NULL;

        oC_ICtrlMan_ReadCursorPosition(&screen,&cursorPosition);

        if(activityOccurred)
        {
            Widget->MaxReleaseTimestamp = gettimestamp() + Widget->MaxReleaseTimeout;
        }

        for( drawedCount = 0, columnIndex = 0; elementHandle != NULL && drawedCount < optionsPerScreen; drawedCount++, columnIndex++)
        {
            if(columnIndex >= optionsPerRow)
            {
                columnIndex = 0;
                rowIndex++;
            }

            drawPosition.X      = columnIndex * columnWidth + Widget->RelativePosition.X + ScreenPosition.X;
            drawPosition.Y      = rowIndex    * rowHeight   + Widget->RelativePosition.Y + ScreenPosition.Y;
            drawEndPosition.X   = drawPosition.X + Widget->Definition.TypeSpecific.CheckBox.OptionWidth;
            drawEndPosition.Y   = drawPosition.Y + Widget->Definition.TypeSpecific.CheckBox.OptionHeight;
            option              = oC_List_ElementHandle_Value(elementHandle,Option_t*);

            if(option != NULL)
            {
                bool oldState = option->Checked;

                if(
                    activityOccurred
                 && Widget->ActiveEvent.Position[0].X >= drawPosition.X
                 && Widget->ActiveEvent.Position[0].Y >= drawPosition.Y
                 && Widget->ActiveEvent.Position[0].X  < drawEndPosition.X
                 && Widget->ActiveEvent.Position[0].Y  < drawEndPosition.Y
                    )
                {
                    option->State = WidgetState_Pressed;
                }
                else if(
                    releaseOccurred
                 && Widget->ReleaseEvent.Position[0].X >= drawPosition.X
                 && Widget->ReleaseEvent.Position[0].Y >= drawPosition.Y
                 && Widget->ReleaseEvent.Position[0].X  < drawEndPosition.X
                 && Widget->ReleaseEvent.Position[0].Y  < drawEndPosition.Y
                    )
                {
                    option->Checked = !option->Checked;
                    if(option->Checked)
                    {
                        option->State = WidgetState_Activated;
                    }
                    else
                    {
                        option->State = WidgetState_Ready;
                    }
                }
                else if(
                    screen == Widget->Screen
                 && cursorPosition.X >= drawPosition.X
                 && cursorPosition.Y >= drawPosition.Y
                 && cursorPosition.X  < drawEndPosition.X
                 && cursorPosition.Y  < drawEndPosition.Y
                    )
                {
                    option->State = WidgetState_Hovered;
                }
                else
                {
                    if(option->Checked)
                    {
                        option->State = WidgetState_Activated;
                    }
                    else
                    {
                        option->State = WidgetState_Ready;
                    }
                }

                stateChanged = stateChanged || option->Checked != oldState;
            }

            elementHandle   = elementHandle->Next;
        }
    }
    return stateChanged;
}

//==========================================================================================================================================
/**
 * @brief checks the check box definition
 */
//==========================================================================================================================================
static bool CheckCheckBoxDefinition( const WidgetDefinition_t * Definition )
{
    bool correct = false;

    if(
        oC_SaveIfFalse( "OptionWidth"           , Definition->TypeSpecific.CheckBox.OptionWidth <= Definition->Width            , oC_ErrorCode_WidthTooBig      )
     && oC_SaveIfFalse( "OptionHeight"          , Definition->TypeSpecific.CheckBox.OptionHeight<= Definition->Height           , oC_ErrorCode_HeightTooBig     )
     && oC_SaveIfFalse( "OptionWidth"           , Definition->TypeSpecific.CheckBox.OptionWidth  > 0                            , oC_ErrorCode_WidthNotCorrect  )
     && oC_SaveIfFalse( "OptionHeight"          , Definition->TypeSpecific.CheckBox.OptionHeight > 0                            , oC_ErrorCode_HeightNotCorrect )
     && oC_SaveIfFalse( "HorizontalCellSpacing ", Definition->TypeSpecific.CheckBox.HorizontalCellSpacing <= Definition->Width  , oC_ErrorCode_ValueTooBig      )
     && oC_SaveIfFalse( "VerticalCellSpacing "  , Definition->TypeSpecific.CheckBox.VerticalCellSpacing   <= Definition->Height , oC_ErrorCode_ValueTooBig      )
        )
    {
        correct = true;
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief creates the new check box
 */
//==========================================================================================================================================
static bool RegisterRadioButtonActivity ( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition )
{
    return RegisterCheckBoxActivity(Widget, ScreenPosition);
}

//==========================================================================================================================================
/**
 * @brief draws the radio button
 */
//==========================================================================================================================================
static void DrawRadioButton( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition )
{
    DrawCheckBox(Widget,ColorMap,ScreenPosition);
}

//==========================================================================================================================================
/**
 * @brief updates the radio button state
 */
//==========================================================================================================================================
static bool UpdateRadioButtonState( Widget_t * Widget , oC_Pixel_Position_t ScreenPosition )
{
    bool stateChanged = false;

    if(Widget->State != WidgetState_Disabled)
    {
        oC_Screen_t                 screen           = NULL;
        oC_Pixel_Position_t         cursorPosition   = { .X = 0 , .Y = 0 };
        bool                        activityOccurred = oC_ICtrlMan_HasActivityOccurred(&Widget->ActiveActivity);
        bool                        releaseOccurred  = oC_ICtrlMan_HasActivityOccurred(&Widget->ReleaseActivity);
        WidgetValue_t               optionsPerRow    = (Widget->Definition.Width  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing) / (Widget->Definition.TypeSpecific.CheckBox.OptionWidth  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing );
        WidgetValue_t               numberOfRows     = (Widget->Definition.Height + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing  ) / (Widget->Definition.TypeSpecific.CheckBox.OptionHeight + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing   );
        WidgetValue_t               optionsPerScreen = optionsPerRow * numberOfRows;
        oC_List_ElementHandle_t     elementHandle    = NULL;
        WidgetValue_t               drawedCount      = 0;
        WidgetValue_t               columnIndex      = 0;
        WidgetValue_t               rowIndex         = 0;
        oC_Pixel_ResolutionInt_t    columnWidth      = Widget->Definition.TypeSpecific.CheckBox.OptionWidth  + Widget->Definition.TypeSpecific.CheckBox.HorizontalCellSpacing;
        oC_Pixel_ResolutionInt_t    rowHeight        = Widget->Definition.TypeSpecific.CheckBox.OptionHeight + Widget->Definition.TypeSpecific.CheckBox.VerticalCellSpacing;
        oC_Pixel_Position_t         drawPosition;
        oC_Pixel_Position_t         drawEndPosition;
        Option_t *                  option              = NULL;

        oC_ICtrlMan_ReadCursorPosition(&screen,&cursorPosition);

        if(activityOccurred)
        {
            Widget->MaxReleaseTimestamp = gettimestamp() + Widget->MaxReleaseTimeout;
        }

        elementHandle = List_ElementOfIndex((oC_List_t)Widget->Options,0);

        for( WidgetValue_t i = 0; elementHandle != NULL && i < Widget->Value; i++ )
        {
            option              = oC_List_ElementHandle_Value(elementHandle,Option_t*);
            elementHandle       = elementHandle->Next;

            if(option != NULL)
            {
                option->Checked = false;
            }
        }

        elementHandle = List_ElementOfIndex((oC_List_t)Widget->Options,Widget->Value + optionsPerScreen);

        for( WidgetValue_t i = 0; elementHandle != NULL; i++ )
        {
            option              = oC_List_ElementHandle_Value(elementHandle,Option_t*);
            elementHandle       = elementHandle->Next;

            if(option != NULL)
            {
                option->Checked = false;
            }
        }

        elementHandle = List_ElementOfIndex((oC_List_t)Widget->Options,Widget->Value);

        for( drawedCount = 0, columnIndex = 0; elementHandle != NULL && drawedCount < optionsPerScreen; drawedCount++, columnIndex++)
        {
            if(columnIndex >= optionsPerRow)
            {
                columnIndex = 0;
                rowIndex++;
            }

            drawPosition.X      = columnIndex * columnWidth + Widget->RelativePosition.X + ScreenPosition.X;
            drawPosition.Y      = rowIndex    * rowHeight   + Widget->RelativePosition.Y + ScreenPosition.Y;
            drawEndPosition.X   = drawPosition.X + Widget->Definition.TypeSpecific.CheckBox.OptionWidth;
            drawEndPosition.Y   = drawPosition.Y + Widget->Definition.TypeSpecific.CheckBox.OptionHeight;
            option              = oC_List_ElementHandle_Value(elementHandle,Option_t*);

            if(option != NULL)
            {
                bool oldState = option->Checked;

                if(releaseOccurred)
                {
                    option->Checked = false;
                }

                if(
                    activityOccurred
                 && Widget->ActiveEvent.Position[0].X >= drawPosition.X
                 && Widget->ActiveEvent.Position[0].Y >= drawPosition.Y
                 && Widget->ActiveEvent.Position[0].X  < drawEndPosition.X
                 && Widget->ActiveEvent.Position[0].Y  < drawEndPosition.Y
                    )
                {
                    option->State = WidgetState_Pressed;
                }
                else if(
                    releaseOccurred
                 && Widget->ReleaseEvent.Position[0].X >= drawPosition.X
                 && Widget->ReleaseEvent.Position[0].Y >= drawPosition.Y
                 && Widget->ReleaseEvent.Position[0].X  < drawEndPosition.X
                 && Widget->ReleaseEvent.Position[0].Y  < drawEndPosition.Y
                    )
                {
                    option->Checked = true;
                    option->State   = WidgetState_Activated;
                    Widget->State   = WidgetState_Activated;
                }
                else if(
                    screen == Widget->Screen
                 && cursorPosition.X >= drawPosition.X
                 && cursorPosition.Y >= drawPosition.Y
                 && cursorPosition.X  < drawEndPosition.X
                 && cursorPosition.Y  < drawEndPosition.Y
                    )
                {
                    option->State = WidgetState_Hovered;
                }
                else
                {
                    option->State = WidgetState_Ready;
                }

                stateChanged = stateChanged || (option->Checked != oldState && option->Checked == true);
            }

            elementHandle   = elementHandle->Next;
        }
    }
    return stateChanged;
}

//==========================================================================================================================================
/**
 * @brief draws the progress bar
 */
//==========================================================================================================================================
static void DrawProgressBar( Widget_t * Widget , oC_ColorMap_t * ColorMap, oC_Pixel_Position_t ScreenPosition )
{
    uint8_t                     activeOpacity       = Widget->DrawStyle[WidgetState_Activated].Opacity;
    uint8_t                     readyOpacity        = Widget->DrawStyle[WidgetState_Ready].Opacity;
    oC_ColorFormat_t            activeColorFormat   = Widget->Palette[WidgetState_Activated].ColorFormat;
    oC_ColorFormat_t            readyColorFormat    = Widget->Palette[WidgetState_Ready].ColorFormat;
    oC_Color_t                  activeBorderColor   = Widget->Palette[WidgetState_Activated].BorderColor;
    oC_Color_t                  activeFillColor     = Widget->Palette[WidgetState_Activated].FillColor;
    oC_Pixel_ResolutionUInt_t   activeBorderWidth   = Widget->DrawStyle[WidgetState_Activated].BorderWidth;
    oC_ColorMap_BorderStyle_t   activeBorderStyle   = Widget->DrawStyle[WidgetState_Activated].BorderStyle;
    bool                        activeDrawFill      = Widget->DrawStyle[WidgetState_Activated].DrawFill;
    oC_Color_t                  readyBorderColor    = Widget->Palette[WidgetState_Ready].BorderColor;
    oC_Color_t                  readyFillColor      = Widget->Palette[WidgetState_Ready].FillColor;
    oC_Pixel_ResolutionUInt_t   readyBorderWidth    = Widget->DrawStyle[WidgetState_Ready].BorderWidth;
    oC_ColorMap_BorderStyle_t   readyBorderStyle    = Widget->DrawStyle[WidgetState_Ready].BorderStyle;
    bool                        readyDrawFill       = Widget->DrawStyle[WidgetState_Ready].DrawFill;
    oC_Pixel_Position_t         position , endPosition, endBarPosition;

    if(activeOpacity == 0xFF)
    {
        activeBorderColor   = oC_Color_ConvertToFormat(activeBorderColor, activeColorFormat, ColorMap->ColorFormat);
        activeFillColor     = oC_Color_ConvertToFormat(activeFillColor,   activeColorFormat, ColorMap->ColorFormat);
    }
    else
    {
        activeBorderColor   = oC_Color_ConvertToFormat(activeBorderColor,   activeColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)activeOpacity ) << 24 );
        activeFillColor     = oC_Color_ConvertToFormat(activeFillColor,     activeColorFormat,  oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)activeOpacity ) << 24 );
        activeColorFormat   = oC_ColorFormat_ARGB8888;
    }

    if(readyOpacity == 0xFF)
    {
        readyBorderColor = oC_Color_ConvertToFormat(readyBorderColor,   readyColorFormat,   ColorMap->ColorFormat);
        readyFillColor   = oC_Color_ConvertToFormat(readyFillColor,     readyColorFormat,   ColorMap->ColorFormat);
    }
    else
    {
        readyBorderColor = oC_Color_ConvertToFormat(readyBorderColor,   readyColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)readyOpacity ) << 24 );
        readyFillColor   = oC_Color_ConvertToFormat(readyFillColor,     readyColorFormat,   oC_ColorFormat_ARGB8888) | ( ((oC_Color_t)readyOpacity ) << 24 );
        readyColorFormat = oC_ColorFormat_ARGB8888;
    }

    position.X    = Widget->RelativePosition.X    + ScreenPosition.X;
    position.Y    = Widget->RelativePosition.Y    + ScreenPosition.Y;
    endPosition.X = Widget->RelativeEndPosition.X + ScreenPosition.X;
    endPosition.Y = Widget->RelativeEndPosition.Y + ScreenPosition.Y;

    oC_ColorMap_DrawRect(ColorMap,position,endPosition,readyBorderWidth,readyBorderStyle,readyBorderColor,readyFillColor,readyColorFormat,readyDrawFill);

    oC_Pixel_ResolutionUInt_t   barWidth = Widget->Definition.Width - (2 * activeBorderWidth);
    float                       percent  = (float)Widget->Value;

    percent /= ((float)Widget->MaximumValue);
    barWidth = ((oC_Pixel_ResolutionUInt_t)(((float)barWidth) * percent));

    position.X  = position.X + readyBorderWidth;
    position.Y  = position.Y + readyBorderWidth;

    endBarPosition.Y = endPosition.Y - readyBorderWidth;
    endBarPosition.X = position.X    + barWidth;

    oC_ColorMap_DrawRect(ColorMap,position,endBarPosition,activeBorderWidth,activeBorderStyle,activeBorderColor,activeFillColor,activeColorFormat,activeDrawFill);
}

//==========================================================================================================================================
/**
 * @brief creates the new progress bar
 */
//==========================================================================================================================================
static oC_ErrorCode_t CreateNewScreen( oC_Screen_t Screen , const oC_WidgetScreen_ScreenDefinition_t * ScreenDefinition, oC_WidgetScreen_t * outScreen, void * UserParameter )
{
    oC_ErrorCode_t      errorCode   = oC_ErrorCode_ImplementError;
    oC_WidgetScreen_t   screen      = malloc(sizeof(struct WidgetScreen_t), AllocationFlags_ZeroFill | AllocationFlags_ExternalRamFirst);
    void *              list        = oC_List_New(getcurallocator(), 0);

    if(
        ErrorCondition( screen != NULL                      , oC_ErrorCode_AllocationError  )
     && ErrorCondition( list   != NULL                      , oC_ErrorCode_AllocationError  )
     && ErrorCondition( isaddresscorrect(ScreenDefinition)  , oC_ErrorCode_WrongAddress     )
     && ErrorCode     ( oC_Screen_ReadColorMap(Screen,&screen->ColorMap) )
        )
    {
        memcpy(&screen->Definition, ScreenDefinition, sizeof(screen->Definition));

        screen->NumberOfWidgets = ScreenDefinition->NumberOfWidgetsDefinitions;
        screen->Position.X      = ScreenDefinition->Position.X;
        screen->Position.Y      = ScreenDefinition->Position.Y;
        screen->Screen          = Screen;
        screen->Widgets         = list;
        screen->ObjectControl   = oC_CountObjectControl(screen,oC_ObjectId_WidgetScreen);

        *outScreen  = screen;
        errorCode   = oC_ErrorCode_None;

        oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(ScreenDefinition->WidgetsDefinitions,ScreenDefinition->NumberOfWidgetsDefinitions,widgetDefinition)
        {
            if(CheckWidgetDefinition(widgetDefinition))
            {
                oC_WidgetScreen_AddWidget(screen,widgetDefinition,UserParameter);
            }
            else
            {
                errorCode = oC_ErrorCode_WrongWidgetDefinition;
            }
        }
    }
    if(oC_ErrorOccur(errorCode))
    {
        oC_SaveIfFalse( "screen", screen == NULL || free(screen,0)          , oC_ErrorCode_ReleaseError );
        oC_SaveIfFalse( "list"  , list   == NULL || oC_List_Delete(list,0)  , oC_ErrorCode_ReleaseError );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief deletes the screen
 */
//==========================================================================================================================================
static oC_ErrorCode_t DeleteScreen( oC_WidgetScreen_t * outScreen )
{
    oC_ErrorCode_t      errorCode   = oC_ErrorCode_ImplementError;
    oC_WidgetScreen_t   screen      = *outScreen;
    WidgetIndex_t       widgetIndex = 0;

    errorCode = oC_ErrorCode_None;

    oC_IntMan_EnterCriticalSection();

    foreach(screen->Widgets,widget)
    {
        oC_WidgetScreen_RemoveWidget(screen,widgetIndex++);
        ErrorCondition( oC_List_Delete(widget->Options,0) , oC_ErrorCode_ReleaseError );
        ErrorCondition( free(widget,0)                    , oC_ErrorCode_ReleaseError );
    }

    ErrorCondition( oC_List_Delete(screen->Widgets,0) , oC_ErrorCode_ReleaseError );
    ErrorCondition( free(screen,0)                    , oC_ErrorCode_ReleaseError );

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}
