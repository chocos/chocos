/** ****************************************************************************************************************************************
 *
 * @brief      The file contains the implementation of the color map.
 *
 * @file       oc_colormap.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_colormap.h>
#include <stddef.h>
#include <oc_stdlib.h>
#include <oc_errors.h>
#include <oc_string.h>
#include <oc_math.h>
#include <oc_debug.h>

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief creates new color map
 *
 * The function creates new color map with the specified width, height, color format and number of layers. It can be used as an alternative
 * to the #oC_ColorMap_Define macro - this is the dynamic version of the color map.
 *
 * @warning For most of the cases you don't need to create the color map on your own - you should probably 
 * use #oC_ScreenMan_GetDefaultScreen and #oC_Screen_ReadColorMap functions to get the color map instead
 * 
 * @param Allocator         The allocator (use #getcurallocator to get the current allocator)
 * @param AllocationFlags   The allocation flags    
 * @param Width             The width of the color map
 * @param Height            The height of the color map
 * @param ColorFormat       The color format of the color map
 * @param NumberOfLayers    The number of layers
 *
 * @return The pointer to the color map
 * 
 * @see oC_ColorMap_Define, oC_ColorMap_Delete, oC_Screen_ReadColorMap, oC_ScreenMan_GetDefaultScreen
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
oC_ColorMap_t * oC_ColorMap_New( Allocator_t Allocator , AllocationFlags_t AllocationFlags , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_ColorFormat_t ColorFormat , oC_ColorMap_LayerIndex_t NumberOfLayers )
{
    oC_ColorMap_t * colorMap = NULL;

    if(Width > 0 && Height > 0 && ColorFormat != oC_ColorFormat_Unknown)
    {
        colorMap = kmalloc(sizeof(oC_ColorMap_t) , Allocator , AllocationFlags);

        if(colorMap != NULL)
        {
            colorMap->Layers            = kmalloc( sizeof(void*) * NumberOfLayers, Allocator, AllocationFlags | AllocationFlags_ZeroFill);
            colorMap->NumberOfLayers    = NumberOfLayers;

            for(oC_ColorMap_LayerIndex_t layerIndex = 0; layerIndex < NumberOfLayers; layerIndex++)
            {
                colorMap->Map = kmalloc(oC_Color_GetFormatSize(ColorFormat) * Width * Height, Allocator , AllocationFlags);

                if(colorMap->Map != NULL)
                {
                    colorMap->Layers[layerIndex] = colorMap->WriteMap;
                    colorMap->ActiveLayer        = layerIndex;
                    colorMap->ColorFormat        = ColorFormat;
                    colorMap->Height             = Height;
                    colorMap->Width              = Width;
                    colorMap->MagicNumber        = oC_ColorMap_MagicNumber;
                    colorMap->FormatSize         = oC_Color_GetFormatSize(ColorFormat);

                }
                else if(layerIndex == 0)
                {
                    oC_SaveIfFalse("ColorMap::New: cannot release memory" , kfree(colorMap,AllocationFlags), oC_ErrorCode_ReleaseError);

                    colorMap = NULL;
                    break;
                }
                else
                {
                    colorMap->Map                = colorMap->Layers[layerIndex - 1];
                    colorMap->Layers[layerIndex] = colorMap->Layers[layerIndex - 1];
                    kdebuglog(oC_LogType_Error, "Cannot allocate memory for layer of color map. Using only previous %d layer(s)\n", layerIndex);
                    colorMap->NumberOfLayers = layerIndex;
                    break;
                }
            }
        }
    }



    return colorMap;
}

//==========================================================================================================================================
/**
 * @brief deletes color map
 *
 * The function deletes the color map. It releases the memory of the color map and its layers.
 *
 * @warning Don't call this function for static color maps (created with #oC_ColorMap_Define macro).
 * 
 * @param ColorMap          The pointer to the color map
 * @param AllocationFlags   The allocation flags
 *
 * @return true if the color map was deleted, false otherwise
 * 
 * @see oC_ColorMap_New, 
 *
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_Delete(colorMap,AllocationFlags_None);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_Delete( oC_ColorMap_t * ColorMap , AllocationFlags_t AllocationFlags )
{
    bool deleted = false;

    if(oC_ColorMap_IsCorrect(ColorMap))
    {
        if(isram(ColorMap))
        {
            ColorMap->MagicNumber = 0;

            if(kfree(ColorMap->WriteMap,AllocationFlags) && kfree(ColorMap,AllocationFlags))
            {
                deleted = true;
            }
        }
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief switches active layer
 * 
 * The function switches the active layer of the color map. The active layer is the layer that is currently used for drawing.
 * 
 * @param ColorMap          The pointer to the color map (use #oC_ColorMap_New to create the color map)
 * @param Layer             The index of the layer (0 is the first layer)
 * 
 * @return true if the layer was switched, false otherwise
 * 
 * @see oC_ColorMap_New, oC_ColorMap_GetNumberOfLayers
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 2 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_SwitchLayer(colorMap,1);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_SwitchLayer( oC_ColorMap_t * ColorMap , oC_ColorMap_LayerIndex_t Layer )
{
    bool result = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && Layer < ColorMap->NumberOfLayers)
    {
        ColorMap->Map           = ColorMap->Layers[Layer];
        ColorMap->ActiveLayer   = Layer;
        result                  = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief gets number of layers
 * 
 * The function gets the number of layers of the color map.
 * 
 * @param ColorMap          The pointer to the color map (use #oC_ColorMap_New to create the color map)
 * 
 * @return The number of layers
 * 
 * @see oC_ColorMap_New
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 2 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_LayerIndex_t numberOfLayers = oC_ColorMap_GetNumberOfLayers(colorMap);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
oC_ColorMap_LayerIndex_t oC_ColorMap_GetNumberOfLayers( oC_ColorMap_t * ColorMap )
{
    oC_ColorMap_LayerIndex_t numberOfLayers = 0;

    if(oC_ColorMap_IsCorrect(ColorMap))
    {
        numberOfLayers  = ColorMap->NumberOfLayers;
    }

    return numberOfLayers;
}

//==========================================================================================================================================
/**
 * @brief gets active layer
 * 
 * The function gets the active layer of the color map. The active layer is the layer that is currently used for drawing.
 * 
 * @param ColorMap          The pointer to the color map (use #oC_ColorMap_New to create the color map)
 * 
 * @return The active layer
 * 
 * @see oC_ColorMap_New, oC_ColorMap_SwitchLayer
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 2 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_LayerIndex_t activeLayer = oC_ColorMap_GetActiveLayer(colorMap);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
oC_ColorMap_LayerIndex_t oC_ColorMap_GetActiveLayer( oC_ColorMap_t * ColorMap )
{
    oC_ColorMap_LayerIndex_t numberOfLayers = 0;

    if(oC_ColorMap_IsCorrect(ColorMap))
    {
        numberOfLayers  = ColorMap->ActiveLayer;
    }

    return numberOfLayers;
}

//==========================================================================================================================================
/**
 * @brief gets inactive layer
 * 
 * The function gets the inactive layer of the color map. The inactive layer is the layer that is not currently used for drawing.
 * The inactive layer can be used to draw something in the background and then switch the layers on the screen.
 * 
 * @warning
 * If there is no inactive layer, the function returns the active layer.
 * 
 * @param ColorMap          The pointer to the color map (use #oC_ColorMap_New to create the color map)
 * 
 * @return The inactive layer
 * 
 * @see oC_ColorMap_New, oC_ColorMap_SwitchLayer, oC_ScreenMan_GetDefaultScreen, oC_Screen_ReadColorMap
 * 
 * @code{.c}
   
    // get the default screen from the screen manager
    oC_Screen_t screen = oC_ScreenMan_GetDefaultScreen();

    // read the color map from the screen
    oC_ColorMap_t * colorMap = NULL;
    oC_Screen_ReadColorMap(screen, &colorMap);

    if( colorMap == NULL )
    {
        printf("Cannot read the color map from the screen\n");
        return -1;
    }

    // get the inactive layer
    oC_ColorMap_LayerIndex_t inactiveLayer = oC_ColorMap_GetInactiveLayer(colorMap);

    // switch the layer in the color map for drawing
    oC_ColorMap_SwitchLayer(colorMap, inactiveLayer);

    // draw something on the inactive layer
    oC_ColorMap_DrawRectangle(colorMap, 10, 10, 50, 50, oC_Color_Red);

    // switch the layer in the screen for drawing
    oC_Screen_SwitchLayer(screen, inactiveLayer);

   @endcode
 */
//==========================================================================================================================================
oC_ColorMap_LayerIndex_t oC_ColorMap_GetInactiveLayer( oC_ColorMap_t * ColorMap )
{
    oC_ColorMap_LayerIndex_t inactiveLayer = 0;

    if(oC_ColorMap_IsCorrect(ColorMap))
    {
        if(ColorMap->NumberOfLayers > 1)
        {
            inactiveLayer = ColorMap->ActiveLayer + 1;
            if(inactiveLayer >= ColorMap->NumberOfLayers)
            {
                inactiveLayer = 0;
            }
        }
    }

    return inactiveLayer;
}

//==========================================================================================================================================
/**
 * @brief checks if the color map is correct
 * 
 * The function checks if the color map is correct. The color map is correct if the magic number is correct and the pointers are correct.
 * 
 * @param ColorMap          The pointer to the color map
 * 
 * @return true if the color map is correct, false otherwise
 * 
 * @see oC_ColorMap_New
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 2 );
    if(oC_ColorMap_IsCorrect(colorMap))
    {
        // do something with the color map (for example draw something)
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_IsCorrect( const oC_ColorMap_t * ColorMap )
{
    return isaddresscorrect(ColorMap) && ColorMap->MagicNumber == oC_ColorMap_MagicNumber && isaddresscorrect(ColorMap->Map);
}

//==========================================================================================================================================
/**
 * @brief returns reference to the color map at the specified position
 */
//==========================================================================================================================================
static inline uint8_t * GetMapRef(oC_Pixel_Position_t Position , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , uint32_t FormatSize ,  uint8_t ColorMap[Height][Width][FormatSize])
{
    return &ColorMap[Position.Y][Position.X][0];
}

//==========================================================================================================================================
/**
 * @brief copies the color map
 */
//==========================================================================================================================================
static inline void CopyMap(
                oC_Pixel_Position_t         StartPosition,
                uint32_t                    FormatSize ,
                oC_Pixel_ResolutionUInt_t   WidthOut ,
                oC_Pixel_ResolutionUInt_t   HeightOut ,
                uint8_t ColorMapOut[HeightOut][WidthOut][FormatSize] ,
                oC_Pixel_ResolutionUInt_t   WidthIn ,
                oC_Pixel_ResolutionUInt_t   HeightIn ,
                uint8_t ColorMapIn[HeightIn][WidthIn][FormatSize]
                )
{
    if(FormatSize == 1)
    {
        for( oC_Pixel_ResolutionUInt_t Y = StartPosition.Y; Y < HeightOut && Y < HeightIn; Y++ )
        {
            for( oC_Pixel_ResolutionUInt_t X = StartPosition.X ; X < WidthOut && X < WidthIn ; X++ )
            {
                ColorMapOut[Y][X][0] = ColorMapIn[Y][X][0];
            }
        }
    }
    else if(FormatSize == 2)
    {
        for( oC_Pixel_ResolutionUInt_t Y = StartPosition.Y; Y < HeightOut && Y < HeightIn; Y++ )
        {
            for( oC_Pixel_ResolutionUInt_t X = StartPosition.X ; X < WidthOut && X < WidthIn ; X++ )
            {
                ColorMapOut[Y][X][0] = ColorMapIn[Y][X][0];
                ColorMapOut[Y][X][1] = ColorMapIn[Y][X][1];
            }
        }
    }
    else if(FormatSize == 3)
    {
        for( oC_Pixel_ResolutionUInt_t Y = StartPosition.Y; Y < HeightOut && Y < HeightIn; Y++ )
        {
            for( oC_Pixel_ResolutionUInt_t X = StartPosition.X ; X < WidthOut && X < WidthIn ; X++ )
            {
                ColorMapOut[Y][X][0] = ColorMapIn[Y][X][0];
                ColorMapOut[Y][X][1] = ColorMapIn[Y][X][1];
                ColorMapOut[Y][X][2] = ColorMapIn[Y][X][2];
            }
        }
    }
    else
    {
        for( oC_Pixel_ResolutionUInt_t Y = StartPosition.Y; Y < HeightOut && Y < HeightIn; Y++ )
        {
            for( oC_Pixel_ResolutionUInt_t X = StartPosition.X ; X < WidthOut && X < WidthIn ; X++ )
            {
                ColorMapOut[Y][X][0] = ColorMapIn[Y][X][0];
                ColorMapOut[Y][X][1] = ColorMapIn[Y][X][1];
                ColorMapOut[Y][X][2] = ColorMapIn[Y][X][2];
                ColorMapOut[Y][X][3] = ColorMapIn[Y][X][3];
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief clones the color map
 * 
 * The function clones the color map from #ColorMapIn to #ColorMapOut. The function clones the color map from the specified position 
 * to the end of the color map. The function checks if the color map is correct and if the color format is the same.
 * 
 * @warning both color maps must be correct and the color format must be the same
 * 
 * @param ColorMapOut       The pointer to the output color map
 * @param ColorMapIn        The pointer to the input color map
 * @param StartPosition     The start position of the cloning
 * 
 * @return true if the color map was cloned, false otherwise
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMapIn  = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    oC_ColorMap_t * colorMapOut = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 200 , 200 , oC_ColorFormat_RGB888 , 1 );
    if(colorMapIn != NULL && colorMapOut != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_CloneMap(colorMapOut,colorMapIn,(oC_Pixel_Position_t){.X = 50 , .Y = 50});
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_CloneMap( oC_ColorMap_t * ColorMapOut, const oC_ColorMap_t * ColorMapIn , oC_Pixel_Position_t StartPosition )
{
    bool                success         = false;
    oC_Pixel_Position_t endPosition     = {
                    .X = StartPosition.X ,
                    .Y = StartPosition.Y
    };

    if(
           oC_ColorMap_IsCorrect(ColorMapOut)
        && oC_ColorMap_IsCorrect(ColorMapIn)
        && ColorMapOut->ColorFormat == ColorMapIn->ColorFormat
        && oC_ColorMap_IsPositionCorrect(ColorMapOut,StartPosition)
        )
    {
        endPosition.X += ColorMapIn->Width  - 1;
        endPosition.Y += ColorMapIn->Height - 1;

        if(oC_ColorMap_IsPositionCorrect(ColorMapOut,endPosition))
        {
            CopyMap(StartPosition,
                    ColorMapOut->FormatSize,
                    ColorMapOut->Width,
                    ColorMapOut->Height,
                    ColorMapOut->GenericWriteMap,
                    ColorMapIn->Width,
                    ColorMapIn->Height,
                    ColorMapIn->GenericWriteMap);
            success = true;
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief gets color from the color map
 * 
 * The function gets the color from the color map at the specified position. The function checks if the color map is correct and if the 
 * position is correct.
 * 
 * @param ColorMap          The pointer to the color map
 * @param Position          The position of the color
 * @param ColorFormat       The color format of the color
 * 
 * @return The color
 * 
 * @see oC_ColorMap_SetColor 
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_Color_t color = oC_ColorMap_GetColor(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},oC_ColorFormat_RGB888);
        // color of the pixel at position 50,50 is stored in the color variable
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
oC_Color_t oC_ColorMap_GetColor( const oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_ColorFormat_t ColorFormat )
{
    oC_Color_t color = 0;

    if(oC_ColorMap_IsCorrect(ColorMap) && oC_ColorMap_IsPositionCorrect(ColorMap,Position))
    {
        oC_UInt_t        formatSize = oC_Color_GetFormatSize(ColorMap->ColorFormat);
        const uint8_t *  colorRef   = &ColorMap->ReadMap[(Position.X + ColorMap->Width * Position.Y)* formatSize ];

        for(uint8_t byteIndex = 0; byteIndex < formatSize ; byteIndex++)
        {
            color |= (*colorRef) << byteIndex * 8;
            colorRef++;
        }

        color = oC_Color_ConvertToFormat(color,ColorMap->ColorFormat,ColorFormat);
    }

    return color;
}

typedef void (*SetColorFunction_t)(oC_Pixel_Position_t Position , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , uint32_t FormatSize ,  void *ColorMap , oC_Color_t Color , uint8_t Alpha );

//==========================================================================================================================================
/**
 * @brief sets the color
 */
//==========================================================================================================================================
static inline void SetColor(oC_Pixel_Position_t Position , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , uint32_t FormatSize ,  uint8_t ColorMap[Height][Width][FormatSize] , oC_Color_t Color , uint8_t Alpha )
{
    static const uint8_t  bitOffset[4] = {
        0 ,
        8 ,
        16 ,
        32
    };

    oC_ASSERT( Position.X < Width  );
    oC_ASSERT( Position.Y < Height );

    for(uint8_t byteIndex = 0 ; byteIndex < FormatSize ; byteIndex++)
    {
        ColorMap[Position.Y][Position.X][byteIndex] = (uint8_t)((Color >> bitOffset[byteIndex]) & 0xFF);
    }
}

//==========================================================================================================================================
/**
 * @brief sets the color with opacity
 */
//==========================================================================================================================================
static inline void SetColorWithOpacity(oC_Pixel_Position_t Position , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , uint32_t FormatSize ,  uint8_t ColorMap[Height][Width][FormatSize] , oC_Color_t Color , uint8_t Alpha )
{
    static const uint8_t  bitOffset[4] = {
        0 ,
        8 ,
        16 ,
        32
    };
    uint8_t colorPart     = 0;
    uint8_t newColorPart  = 0;
    uint8_t togheter      = 0;
    float   percentAlpha  = ((float)Alpha) / 255.0;

    for(uint8_t byteIndex = 0 ; byteIndex < FormatSize ; byteIndex++)
    {
        colorPart     = ((uint8_t)((Color >> bitOffset[byteIndex]) & 0xFF));
        colorPart     = (uint8_t)(((float)colorPart) * percentAlpha);
        newColorPart  = (uint8_t)ColorMap[Position.Y][Position.X][byteIndex];
        newColorPart  = (uint8_t)(((float)newColorPart) * (1.0 - percentAlpha));
        togheter      = colorPart + newColorPart;
        ColorMap[Position.Y][Position.X][byteIndex] = togheter;
    }
}

//==========================================================================================================================================
/**
 * @brief sets the color
 * 
 * The function sets the color in the color map at the specified position. The function checks if the color map is correct and if the
 * position is correct. The function converts the color to the color format of the color map.
 * 
 * @param ColorMap          The pointer to the color map
 * @param Position          The position of the color
 * @param Color             The color
 * @param ColorFormat       The color format of the color
 * 
 * @return true if the color was set, false otherwise
 * 
 * @see oC_ColorMap_GetColor
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_SetColor(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},0xFF0000,oC_ColorFormat_RGB888);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_SetColor ( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat )
{
    bool colorSet = false;

    /* To speed up this operation it is not checked if the color map is correct, but only if it is not NULL  */
    if(ColorMap != NULL && Position.X < ColorMap->Width && Position.Y < ColorMap->Height)
    {
        oC_Color_t color = oC_Color_ConvertToFormat(Color,ColorFormat,ColorMap->ColorFormat);
        if(ColorFormat == oC_ColorFormat_ARGB8888)
        {
            uint8_t alpha = (Color & 0xFF000000) >> 24;
            SetColorWithOpacity(Position,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,color,alpha);
        }
        else
        {
            SetColor(Position,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,color,0xFF);
        }
        colorSet = true;
    }

    return colorSet;
}

//==========================================================================================================================================
/**
 * @brief checks if the position is correct
 */
//==========================================================================================================================================
bool oC_ColorMap_IsPositionCorrect( const oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position )
{
    return oC_ColorMap_IsCorrect(ColorMap) && Position.X < ColorMap->Width && Position.Y < ColorMap->Height;
}

//==========================================================================================================================================
/**
 * @brief fits the position
 * 
 * The function fits the position to the color map - if the position is out of the color map, the function sets the position to the
 * last pixel of the color map.
 */
//==========================================================================================================================================
bool oC_ColorMap_FitPosition( const oC_ColorMap_t * ColorMap , oC_Pixel_Position_t* Position )
{
    bool success = false;
    if(oC_ColorMap_IsCorrect(ColorMap))
    {
        if(Position->X >= ColorMap->Width)
        {
            Position->X = ColorMap->Width - 1;
        }

        if(Position->Y >= ColorMap->Height)
        {
            Position->Y = ColorMap->Height - 1;
        }
        success = true;
    }
    return success;
}

//==========================================================================================================================================
/**
 * @brief fills the rectangle with the color
 * 
 * The function fills the rectangle with the color at the specified position. 
 * 
 * @param ColorMap          The pointer to the color map
 * @param Position          The position of the rectangle (the top left corner)
 * @param Width             The width of the rectangle
 * @param Height            The height of the rectangle
 * @param Color             The color to fill the rectangle
 * @param ColorFormat       The color format of the color
 * 
 * @return true if the rectangle was filled, false otherwise
 * 
 * @see oC_ColorMap_DrawRect
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_FillRectWithColor(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},10,10,oC_Color_Red,oC_ColorFormat_RGB888);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_FillRectWithColor( oC_ColorMap_t * ColorMap, oC_Pixel_Position_t Position, oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_Color_t Color , oC_ColorFormat_t ColorFormat )
{
    bool filled = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && isram(ColorMap->Map) && ColorFormat != oC_ColorFormat_Unknown  && oC_ColorMap_IsPositionCorrect(ColorMap,Position))
    {
        oC_Color_t                color         = oC_Color_ConvertToFormat(Color,ColorFormat,ColorMap->ColorFormat);
        oC_Pixel_ResolutionUInt_t x             = 0;
        oC_Pixel_ResolutionUInt_t y             = 0;
        oC_Pixel_Position_t       drawPosition  = { .X = Position.X , .Y = Position.Y };

        filled = true;

        for( y = 0; y < Height; y++ )
        {
            drawPosition.Y = Position.Y + y;

            for( x = 0; x < Width; x++ )
            {
                drawPosition.X = Position.X + x;

                oC_ColorMap_SetColor(ColorMap,drawPosition,color,ColorMap->ColorFormat);
            }
        }
    }

    return filled;
}

//==========================================================================================================================================
/**
 * @brief fills the circle with the color
 * 
 * The function fills the circle with the color at the specified position.
 * 
 * @param ColorMap          The pointer to the color map
 * @param Center            The center of the circle
 * @param Radius            The radius of the circle
 * @param Color             The color to fill the circle
 * @param ColorFormat       The color format of the color
 * 
 * @return true if the circle was filled, false otherwise
 */
bool oC_ColorMap_FillCircleWithColor( oC_ColorMap_t * ColorMap, oC_Pixel_Position_t Center, oC_Pixel_ResolutionUInt_t Radius , oC_Color_t Color , oC_ColorFormat_t ColorFormat )
{
    bool filled = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && isram(ColorMap->Map) && ColorFormat != oC_ColorFormat_Unknown  && oC_ColorMap_IsPositionCorrect(ColorMap,Center))
    {
        oC_Color_t                color         = oC_Color_ConvertToFormat(Color,ColorFormat,ColorMap->ColorFormat);
        oC_Pixel_ResolutionUInt_t x             = 0;
        oC_Pixel_ResolutionUInt_t y             = 0;
        oC_Pixel_Position_t       topLeft       = { .X = Center.X - Radius , .Y = Center.Y - Radius };
        oC_Pixel_Position_t       drawPosition  = topLeft; 

        filled = true;

        for( y = 0; y < 2*Radius; y++ )
        {
            drawPosition.Y = topLeft.Y + y;

            for( x = 0; x < 2*Radius; x++ )
            {
                drawPosition.X = topLeft.X + x;

                // calculate distance from the center
                if( (x - Radius)*(x - Radius) + (y - Radius)*(y - Radius) <= Radius*Radius )
                {
                    oC_ColorMap_SetColor(ColorMap,drawPosition,color,ColorMap->ColorFormat);
                }
            }
        }
    }

    return filled;
}

//==========================================================================================================================================
/**
 * @brief draws the character
 * 
 * The function draws the character at the specified position. 
 * 
 * @param ColorMap          The pointer to the color map
 * @param Position          The position of the character (the top left corner)
 * @param Color             The color of the character
 * @param ColorFormat       The color format of the color
 * @param C                 The character to draw
 * @param Font              The font of the character to use
 * 
 * @return true if the character was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawString
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawChar(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},oC_Color_Red,oC_ColorFormat_RGB888,'A', &oC_FontInfo_Consolas);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawChar( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat , uint16_t C , oC_Font_t Font )
{
    bool drawed = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && isram(ColorMap->Map) && isaddresscorrect(Font) && ColorFormat != oC_ColorFormat_Unknown  && oC_ColorMap_IsPositionCorrect(ColorMap,Position))
    {
        oC_Font_CharacterMap_t characterMap = {0};

        if(oC_Font_ReadCharacterMap(Font,C,&characterMap))
        {
            oC_Pixel_ResolutionUInt_t   x                    = 0;
            oC_Pixel_ResolutionUInt_t   y                    = 0;
            uint8_t                     characterMapBitIndex = 0;
            uint8_t                     characterMapByteIndex= 0;
            uint8_t                     characterMapByte     = characterMap.Data[characterMapByteIndex];
            oC_Pixel_Position_t         drawPosition         = {0};
            oC_Color_t                  convertedColor       = oC_Color_ConvertToFormat(Color,ColorFormat,ColorMap->ColorFormat);
            SetColorFunction_t          setColorFunction     = (SetColorFunction_t)SetColor;
            uint8_t                     alpha                = 0;

            drawed = true;

            if(ColorFormat == oC_ColorFormat_ARGB8888)
            {
                setColorFunction = (SetColorFunction_t)SetColorWithOpacity;
                alpha            = ((Color & 0xFF000000)) >> 24;
            }


            for( y = 0 ; y < characterMap.Height && drawed; y++ )
            {
                drawPosition.Y = Position.Y + y;

                for( x = 0 ; x < characterMap.Width && drawed; x++ )
                {
                    drawPosition.X = Position.X + x;

                    // Check if this pixel should be drawed
                    if(characterMapByte & ( 1<< characterMapBitIndex))
                    {
                        // If draw position is not correct (it is outside of the screen)
                        // then just do not draw it.
                        if(oC_ColorMap_IsPositionCorrect(ColorMap,drawPosition))
                        {
                            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);
                        }
                    }
                    // /////////////////////////////////////////////////////
                    // counting next character map byte
                    characterMapBitIndex++;

                    if(characterMapBitIndex == 8)
                    {
                        characterMapBitIndex = 0;
                        characterMapByte     = characterMap.Data[++characterMapByteIndex];
                    }
                    // counting next character map byte
                    // /////////////////////////////////////////////////////

                }

                if(characterMapBitIndex != 0)
                {
                    characterMapBitIndex = 0;
                    characterMapByte     = characterMap.Data[++characterMapByteIndex];
                }
            }
        }
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws the string
 * 
 * The function draws the string at the specified position. 
 * 
 * @param ColorMap          The pointer to the color map
 * @param Position          The position of the string (the top left corner)
 * @param Color             The color of the string
 * @param ColorFormat       The color format of the color
 * @param String            The string to draw
 * @param Font              The font of the string to use
 * @param BoxWidth          The width of the box
 * @param BoxHeight         The height of the box
 * 
 * @return true if the string was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawChar, oC_ColorMap_DrawAlignedString
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawString(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},oC_Color_Red,oC_ColorFormat_RGB888,"Hello World", &oC_FontInfo_Consolas,100,100);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawString( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat , const char * String , oC_Font_t Font , oC_Pixel_ResolutionUInt_t BoxWidth , oC_Pixel_ResolutionUInt_t BoxHeight)
{
    bool drawed = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && isram(ColorMap->Map) && isaddresscorrect(Font) && ColorFormat != oC_ColorFormat_Unknown  && oC_ColorMap_IsPositionCorrect(ColorMap,Position) )
    {
        oC_Font_CharacterMap_t characterMap = {0};
        int                    length       = strlen(String);
        uint16_t               C            = 0;
        oC_Pixel_Position_t    position     = Position;
        oC_Pixel_Position_t    endPosition  = { .X = Position.X + BoxWidth , .Y = Position.Y + BoxHeight };

        drawed = true;

        for(int i = 0; i < length; i++)
        {
            C = String[i];

            if(C == '\n')
            {
                position.X  = Position.X;
                position.Y += characterMap.Height;

                if((position.Y + characterMap.Height) >= endPosition.Y)
                {
                    drawed = i == (length - 1);
                    break;
                }
            }
            else if(oC_Font_ReadCharacterMap(Font,C,&characterMap))
            {
                if((position.X + characterMap.Width) >= endPosition.X)
                {
                    position.X  = Position.X;
                    position.Y += characterMap.Height;

                    if((position.Y + characterMap.Height) >= endPosition.Y)
                    {
                        drawed = false;
                        break;
                    }
                }
                oC_ColorMap_DrawChar(ColorMap,position,Color,ColorFormat,C,Font);
                position.X  += characterMap.Width + 1;
            }
        }
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws the aligned string
 * 
 * The function draws the aligned string at the specified position. 
 * 
 * @param ColorMap          The pointer to the color map
 * @param Position          The position of the string (the top left corner)
 * @param Color             The color of the string
 * @param ColorFormat       The color format of the color
 * @param String            The string to draw
 * @param Font              The font of the string to use
 * @param BoxWidth          The width of the box
 * @param BoxHeight         The height of the box
 * @param TextAlign         The text align
 * @param VerticalAlign     The vertical align
 * @param Margin            The margin
 * 
 * @return true if the string was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawString
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawAlignedString(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},oC_Color_Red,oC_ColorFormat_RGB888,"Hello World", &oC_FontInfo_Consolas,100,100,oC_ColorMap_TextAlign_Center,oC_ColorMap_VerticalTextAlign_Center,5);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawAlignedString(
                oC_ColorMap_t *                     ColorMap ,
                oC_Pixel_Position_t                 Position ,
                oC_Color_t                          Color ,
                oC_ColorFormat_t                    ColorFormat ,
                const char *                        String ,
                oC_Font_t                           Font ,
                oC_Pixel_ResolutionUInt_t           BoxWidth ,
                oC_Pixel_ResolutionUInt_t           BoxHeight,
                oC_ColorMap_TextAlign_t             TextAlign ,
                oC_ColorMap_VerticalTextAlign_t     VerticalAlign ,
                oC_Pixel_ResolutionUInt_t           Margin )
{
    bool drawed = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && isram(ColorMap->Map) && isaddresscorrect(Font) && ColorFormat != oC_ColorFormat_Unknown  && oC_ColorMap_IsPositionCorrect(ColorMap,Position) )
    {
        oC_Pixel_Position_t       boxPosition;
        oC_Pixel_Position_t       stringPosition;
        oC_Pixel_ResolutionUInt_t stringWidth     = 0;
        oC_Pixel_ResolutionUInt_t stringHeight    = 0;
        oC_Pixel_ResolutionUInt_t stringBoxWidth  = BoxWidth  - (Margin * 2);
        oC_Pixel_ResolutionUInt_t stringBoxHeight = BoxHeight - (Margin * 2);

        boxPosition.X = Position.X + Margin;
        boxPosition.Y = Position.Y + Margin;

        oC_Font_ReadStringSize(Font,
                               String,
                               &stringWidth,
                               &stringHeight,
                               stringBoxWidth);

        if(TextAlign == oC_ColorMap_TextAlign_Left)
        {
            stringPosition.X = boxPosition.X;
        }
        else if(TextAlign == oC_ColorMap_TextAlign_Right)
        {
            stringPosition.X = boxPosition.X + (BoxWidth - stringWidth - Margin);
        }
        else  // Center
        {
            stringPosition.X = boxPosition.X + ((BoxWidth - stringWidth)/2 - Margin);
        }

        if(VerticalAlign == oC_ColorMap_VerticalTextAlign_Up)
        {
            stringPosition.Y = boxPosition.Y;
        }
        else if(VerticalAlign == oC_ColorMap_VerticalTextAlign_Down)
        {
            stringPosition.Y = boxPosition.Y + (BoxHeight - stringHeight - Margin);
        }
        else
        {
            stringPosition.Y = boxPosition.Y + ((BoxHeight - stringHeight)/2 - Margin);
        }

        drawed = oC_ColorMap_DrawString(ColorMap,
                               stringPosition,
                               Color,
                               ColorFormat,
                               String,
                               Font,
                               stringBoxWidth,
                               stringBoxHeight
                               );
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws line
 * 
 * The function draws the line from the start position to the end position.
 * 
 * @param ColorMap          The pointer to the color map
 * @param StartPosition     The start position of the line
 * @param EndPosition       The end position of the line
 * @param Color             The color of the line
 * @param ColorFormat       The color format of the color
 * 
 * @return true if the line was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawCircle, oC_ColorMap_DrawRect
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawLine(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},(oC_Pixel_Position_t){.X = 100 , .Y = 100},oC_Color_Red,oC_ColorFormat_RGB888);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawLine( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t StartPosition , oC_Pixel_Position_t EndPosition , oC_Color_t Color , oC_ColorFormat_t ColorFormat )
{
    bool drawed = false;

    if(
        oC_ColorMap_IsCorrect(ColorMap)
     && isram(ColorMap->Map)
     && ColorFormat != oC_ColorFormat_Unknown
     && oC_ColorMap_IsPositionCorrect(ColorMap,StartPosition)
     && oC_ColorMap_FitPosition(ColorMap,&EndPosition)
        )
    {
        oC_Pixel_Position_t drawPosition;
        oC_Color_t          convertedColor = oC_Color_ConvertToFormat(Color,ColorFormat,ColorMap->ColorFormat);
        int                 dx             = ((int)EndPosition.X) - ((int)StartPosition.X);
        int                 dy             = ((int)EndPosition.Y) - ((int)StartPosition.Y);
        int                 g              = (dx > 0) ? 1 : -1;
        int                 h              = (dy > 0) ? 1 : -1;
        int                 c              = 0;

        SetColorFunction_t  setColorFunction    = (SetColorFunction_t)SetColor;
        uint8_t             alpha               = 0;

        if(ColorFormat == oC_ColorFormat_ARGB8888)
        {
            setColorFunction = (SetColorFunction_t)SetColorWithOpacity;
            alpha            = ((Color & 0xFF000000)) >> 24;
        }

        dx = abs(dx);
        dy = abs(dy);

        if(dx > dy)
        {
            c = -dx;

            for(drawPosition.X = StartPosition.X, drawPosition.Y = StartPosition.Y;
                drawPosition.X != EndPosition.X;
                drawPosition.X += g)
            {
                setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);
                c += 2 * dy;
                if( c > 0 )
                {
                    drawPosition.Y += h;
                    c -= 2 * dx;
                }
            }
        }
        else
        {
            c = -dy;
            for(drawPosition.X = StartPosition.X, drawPosition.Y = StartPosition.Y;
                drawPosition.Y != EndPosition.Y;
                drawPosition.Y += h)
            {
                setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);
                c += 2 * dx;
                if( c > 0 )
                {
                    drawPosition.X += g;
                    c -= 2 * dy;
                }
            }
        }

        drawed = true;
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws circle
 * 
 * The function draws the circle at the specified position.
 * 
 * @param ColorMap          The pointer to the color map
 * @param Position          The position of the circle
 * @param Color             The color of the circle
 * @param ColorFormat       The color format of the color
 * @param Radius            The radius of the circle
 * 
 * @return true if the circle was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawLine, oC_ColorMap_DrawRect
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawCircle(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},oC_Color_Red,oC_ColorFormat_RGB888,10);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawCircle( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t Position , oC_Color_t Color , oC_ColorFormat_t ColorFormat , int Radius )
{
    bool drawed = false;

    if(
        oC_ColorMap_IsCorrect(ColorMap)
     && isram(ColorMap->Map)
     && ColorFormat != oC_ColorFormat_Unknown
     && oC_ColorMap_IsPositionCorrect(ColorMap,Position)
        )
    {
        oC_Pixel_Position_t drawPosition;
        oC_Color_t          convertedColor      = oC_Color_ConvertToFormat(Color,ColorFormat,ColorMap->ColorFormat);
        SetColorFunction_t  setColorFunction    = (SetColorFunction_t)SetColor;
        uint8_t             alpha               = 0;
        int                 x                   = Radius;
        int                 y                   = 0;
        int                 err                 = 0;
        int                 x0                  = (int)Position.X;
        int                 y0                  = (int)Position.Y;

        if(ColorFormat == oC_ColorFormat_ARGB8888)
        {
            setColorFunction = (SetColorFunction_t)SetColorWithOpacity;
            alpha            = ((Color & 0xFF000000)) >> 24;
        }

        while (x >= y)
        {
            drawPosition.X = x0 + x;
            drawPosition.Y = y0 + y;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            drawPosition.X = x0 + y;
            drawPosition.Y = y0 + x;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            drawPosition.X = x0 - y;
            drawPosition.Y = y0 + x;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            drawPosition.X = x0 - x;
            drawPosition.Y = y0 + y;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            drawPosition.X = x0 - x;
            drawPosition.Y = y0 - y;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            drawPosition.X = x0 - y;
            drawPosition.Y = y0 - x;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            drawPosition.X = x0 + y;
            drawPosition.Y = y0 - x;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            drawPosition.X = x0 + x;
            drawPosition.Y = y0 - y;

            setColorFunction(drawPosition,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);

            y += 1;
            if (err <= 0)
            {
                err += 2*y + 1;
            }
            if (err > 0)
            {
                x -= 1;
                err -= 2*x + 1;
            }
        }

        drawed = true;
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws rectangle
 * 
 * The function draws the rectangle at the specified position.
 * 
 * @param ColorMap          The pointer to the color map
 * @param StartPosition     The start position of the rectangle
 * @param EndPosition       The end position of the rectangle
 * @param BorderWidth       The width of the border
 * @param BorderStyle       The style of the border
 * @param BorderColor       The color of the border
 * @param FillColor         The color of the fill
 * @param ColorFormat       The color format of the color
 * @param DrawFill          The flag to draw the fill
 * 
 * @return true if the rectangle was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawLine, oC_ColorMap_DrawCircle, oC_ColorMap_FillRectWithColor
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawRect(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},(oC_Pixel_Position_t){.X = 100 , .Y = 100},1,oC_ColorMap_BorderStyle_Rectangle,oC_Color_Red,oC_Color_Green,oC_ColorFormat_RGB888,true);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawRect( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t StartPosition , oC_Pixel_Position_t EndPosition , oC_Pixel_ResolutionUInt_t BorderWidth, oC_ColorMap_BorderStyle_t BorderStyle, oC_Color_t BorderColor, oC_Color_t FillColor, oC_ColorFormat_t ColorFormat , bool DrawFill )
{
    bool drawed = false;

    if(
        oC_ColorMap_IsCorrect(ColorMap)
     && isram(ColorMap->Map)
     && ColorFormat != oC_ColorFormat_Unknown
     && oC_ColorMap_IsPositionCorrect(ColorMap,StartPosition)
     && oC_ColorMap_FitPosition(ColorMap,&EndPosition)
        )
    {
        oC_Pixel_Position_t TopLeftPosition;
        oC_Pixel_Position_t TopRightPosition;
        oC_Pixel_Position_t BottomLeftPosition;
        oC_Pixel_Position_t BottomRightPosition;

        TopRightPosition.X   = EndPosition.X;
        TopRightPosition.Y   = StartPosition.Y;

        TopLeftPosition.X    = StartPosition.X;
        TopLeftPosition.Y    = StartPosition.Y;

        BottomLeftPosition.X = StartPosition.X;
        BottomLeftPosition.Y = EndPosition.Y;

        BottomRightPosition.X = EndPosition.X;
        BottomRightPosition.Y = EndPosition.Y;

        drawed = true;

        if(BorderWidth > 0 && BorderStyle != oC_ColorMap_BorderStyle_None)
        {
            for(oC_Pixel_ResolutionUInt_t i = 0; i < BorderWidth; i++)
            {
                if( oC_ColorMap_BorderStyle_Corner(BorderStyle) == oC_ColorMap_BorderStyle_Rectangle )
                {
                    oC_ColorMap_DrawLine( ColorMap, TopLeftPosition    , TopRightPosition  , BorderColor, ColorFormat );
                    oC_ColorMap_DrawLine( ColorMap, TopLeftPosition    , BottomLeftPosition, BorderColor, ColorFormat );
                    oC_ColorMap_DrawLine( ColorMap, BottomRightPosition,   TopRightPosition  , BorderColor, ColorFormat );
                    oC_ColorMap_DrawLine( ColorMap, BottomRightPosition,   BottomLeftPosition, BorderColor, ColorFormat );
                }
                else if( oC_ColorMap_BorderStyle_Corner(BorderStyle) == oC_ColorMap_BorderStyle_Rounded )
                {
                    oC_ColorMap_DrawLine( ColorMap, TopLeftPosition    , TopRightPosition  , BorderColor, ColorFormat );
                    oC_ColorMap_DrawLine( ColorMap, TopLeftPosition    , BottomLeftPosition, BorderColor, ColorFormat );
                    oC_ColorMap_DrawLine( ColorMap, BottomRightPosition,   TopRightPosition  , BorderColor, ColorFormat );
                    oC_ColorMap_DrawLine( ColorMap, BottomRightPosition,   BottomLeftPosition, BorderColor, ColorFormat );
                }
                TopLeftPosition.X++;
                TopLeftPosition.Y++;
                TopRightPosition.X--;
                TopRightPosition.Y++;
                BottomLeftPosition.X++;
                BottomLeftPosition.Y--;
                BottomRightPosition.X--;
                BottomRightPosition.Y--;
            }
        }

        if(DrawFill)
        {
            oC_Color_t          convertedColor      = oC_Color_ConvertToFormat( FillColor, ColorFormat, ColorMap->ColorFormat );
            SetColorFunction_t  setColorFunction    = (SetColorFunction_t)SetColor;
            uint8_t             alpha               = 0;

            if(ColorFormat == oC_ColorFormat_ARGB8888)
            {
                setColorFunction = (SetColorFunction_t)SetColorWithOpacity;
                alpha            = ((FillColor & 0xFF000000)) >> 24;
            }

            for(oC_Pixel_Position_t position = TopLeftPosition; position.Y <= BottomRightPosition.Y ; position.Y++)
            {
                for(position.X = TopLeftPosition.X; position.X <= BottomRightPosition.X ; position.X++)
                {
                    setColorFunction(position,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,convertedColor,alpha);
                }
            }
        }

    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws triangle
 * 
 * The function draws the triangle at the specified position.
 * 
 * @param ColorMap          The pointer to the color map
 * @param EdgePositions     The positions of the edges of the triangle
 * @param BorderWidth       The width of the border
 * @param BorderColor       The color of the border
 * @param FillColor         The color of the fill
 * @param ColorFormat       The color format of the color
 * @param DrawFill          The flag to draw the fill
 * 
 * @return true if the triangle was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawLine, oC_ColorMap_DrawCircle, oC_ColorMap_DrawRect
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawTriangle(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},(oC_Pixel_Position_t){.X = 100 , .Y = 50},(oC_Pixel_Position_t){.X = 75 , .Y = 100},1,oC_Color_Red,oC_Color_Green,oC_ColorFormat_RGB888,true);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawTriangle( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t EdgePositions[3], oC_Pixel_ResolutionUInt_t BorderWidth, oC_Color_t BorderColor, oC_Color_t FillColor, oC_ColorFormat_t ColorFormat , bool DrawFill )
{
    bool drawed = false;

    if(
        oC_ColorMap_IsCorrect(ColorMap)
     && isram(ColorMap->Map)
     && oC_ColorMap_IsPositionCorrect(ColorMap,EdgePositions[0])
     && oC_ColorMap_FitPosition(ColorMap,&EdgePositions[1])
     && oC_ColorMap_FitPosition(ColorMap,&EdgePositions[2])
     )
    {
        oC_Color_t borderColor = oC_Color_ConvertToFormat(BorderColor, ColorFormat, ColorMap->ColorFormat);
        oC_Color_t fillColor   = oC_Color_ConvertToFormat(FillColor  , ColorFormat, ColorMap->ColorFormat);
        float   yMin             = ColorMap->Height + 1;
        float   yMax             = 0;
        uint8_t p1PointIndex     = 0;
        uint8_t p2PointIndex     = 0;
        int     yMinPointIndex   = 0;
        SetColorFunction_t  setColorFunction    = (SetColorFunction_t)SetColor;
        uint8_t             alpha               = 0;

        if(ColorFormat == oC_ColorFormat_ARGB8888)
        {
            setColorFunction = (SetColorFunction_t)SetColorWithOpacity;
            alpha            = ((FillColor & 0xFF000000)) >> 24;
        }

        /* Searching for yMin */
        for(int i = 0; i < 3; i++)
        {
            if(EdgePositions[i].Y <= yMin)
            {
                yMin             = EdgePositions[i].Y;
                yMinPointIndex   = i;
            }
            if(EdgePositions[i].Y >= yMax)
            {
                yMax = EdgePositions[i].Y;
            }
        }

        p1PointIndex = (yMinPointIndex + 1) % 3;
        p2PointIndex = (yMinPointIndex + 2) % 3;

        float dxToP1            = EdgePositions[p1PointIndex].X - EdgePositions[yMinPointIndex].X;
        float dyToP1            = EdgePositions[p1PointIndex].Y - EdgePositions[yMinPointIndex].Y;
        float dxToP2            = EdgePositions[p2PointIndex].X - EdgePositions[yMinPointIndex].X;
        float dyToP2            = EdgePositions[p2PointIndex].Y - EdgePositions[yMinPointIndex].Y;
        float dxFromP1ToP2      = EdgePositions[p2PointIndex].X - EdgePositions[p1PointIndex].X;
        float dyFromP1ToP2      = EdgePositions[p2PointIndex].Y - EdgePositions[p1PointIndex].Y;
        float dxPerYToP1        = dxToP1 / dyToP1;
        float dxPerYToP2        = dxToP2 / dyToP2;
        float dxPerYFromP1ToP2  = dxFromP1ToP2 / dyFromP1ToP2;
        float xToP1             = EdgePositions[dyToP1 != 0 ? yMinPointIndex : p1PointIndex].X;
        float xToP2             = EdgePositions[dyToP2 != 0 ? yMinPointIndex : p2PointIndex].X;
        float xStart            = 0;
        float xEnd              = 0;

        for(float y = yMin; y <= yMax; y++)
        {
            oC_Pixel_Position_t position;

            position.Y = (oC_Pixel_ResolutionUInt_t)y;

            xStart = oC_MIN(xToP1,xToP2);
            xEnd   = oC_MAX(xToP1,xToP2);

            for(float x = xStart; x <= xEnd && x < ColorMap->Width; x++)
            {
                position.X = (oC_Pixel_ResolutionUInt_t)x;

                if(x >= (xStart + BorderWidth) && x <= (xEnd - BorderWidth) && y <= (yMax - BorderWidth) && y >= (yMin + BorderWidth))
                {
                    if(DrawFill)
                    {
                        setColorFunction(position,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,fillColor, alpha);
                    }
                }
                else
                {
                    setColorFunction(position,ColorMap->Width,ColorMap->Height,ColorMap->FormatSize,ColorMap->GenericWriteMap,borderColor, alpha);
                }
            }

            if(y < EdgePositions[p1PointIndex].Y)
            {
                xToP1  += dxPerYToP1;
            }
            else
            {
                xToP1  += dxPerYFromP1ToP2;
            }

            if(y < EdgePositions[p2PointIndex].Y)
            {
                xToP2  += dxPerYToP2;
            }
            else
            {
                xToP2  += dxPerYFromP1ToP2;
            }
        }

        drawed = true;
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws arrow
 * 
 * The function draws the arrow at the specified position.
 * 
 * @param ColorMap          The pointer to the color map
 * @param TopLeftPosition   The top left position of the arrow
 * @param Width             The width of the arrow
 * @param Height            The height of the arrow
 * @param Direction         The direction of the arrow
 * @param BorderWidth       The width of the border
 * @param BorderColor       The color of the border
 * @param FillColor         The color of the fill
 * @param ColorFormat       The color format of the color
 * @param DrawFill          If true, the fill will be drawn
 * 
 * @return true if the arrow was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawLine, oC_ColorMap_DrawCircle, oC_ColorMap_DrawRect
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawArrow(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},10,10,oC_ColorMap_Direction_Up,1,oC_Color_Red,oC_Color_Green,oC_ColorFormat_RGB888,true);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawArrow( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t TopLeftPosition , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height , oC_ColorMap_Direction_t Direction , oC_Pixel_ResolutionUInt_t BorderWidth, oC_Color_t BorderColor , oC_Color_t FillColor, oC_ColorFormat_t ColorFormat , bool DrawFill )
{
    bool drawed = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && isram(ColorMap) && oC_ColorMap_IsPositionCorrect(ColorMap,TopLeftPosition) && Width > 0 && Height > 0)
    {
        oC_Pixel_Position_t edgePositions[3];

        drawed = true;

        if(Direction == oC_ColorMap_Direction_Up)
        {
            edgePositions[0].X = TopLeftPosition.X;
            edgePositions[0].Y = TopLeftPosition.Y + Height;
            edgePositions[1].X = TopLeftPosition.X + (Width / 2);
            edgePositions[1].Y = TopLeftPosition.Y;
            edgePositions[2].X = TopLeftPosition.X + Width;
            edgePositions[2].Y = TopLeftPosition.Y + Height;
        }
        else if(Direction == oC_ColorMap_Direction_Down)
        {
            edgePositions[0].X = TopLeftPosition.X;
            edgePositions[0].Y = TopLeftPosition.Y;
            edgePositions[1].X = TopLeftPosition.X + (Width / 2);
            edgePositions[1].Y = TopLeftPosition.Y + Height;
            edgePositions[2].X = TopLeftPosition.X + Width;
            edgePositions[2].Y = TopLeftPosition.Y;
        }
        else if(Direction == oC_ColorMap_Direction_Right)
        {
            edgePositions[0].X = TopLeftPosition.X;
            edgePositions[0].Y = TopLeftPosition.Y;
            edgePositions[1].X = TopLeftPosition.X;
            edgePositions[1].Y = TopLeftPosition.Y + Height;
            edgePositions[2].X = TopLeftPosition.X + Width;
            edgePositions[2].Y = TopLeftPosition.Y + (Height / 2);
        }
        else if(Direction == oC_ColorMap_Direction_Left)
        {
            edgePositions[0].X = TopLeftPosition.X;
            edgePositions[0].Y = TopLeftPosition.Y + (Height / 2);
            edgePositions[1].X = TopLeftPosition.X + Width;
            edgePositions[1].Y = TopLeftPosition.Y;
            edgePositions[2].X = TopLeftPosition.X + Width;
            edgePositions[2].Y = TopLeftPosition.Y + Height;
        }
        else
        {
            memset(edgePositions,0,sizeof(edgePositions));
            drawed = false;
        }

        if(drawed)
        {
            drawed = oC_ColorMap_DrawTriangle(ColorMap, edgePositions, BorderWidth, BorderColor, FillColor, ColorFormat, DrawFill);
        }
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief fills the background with the color
 * 
 * The function fills the background with the specified color.
 * 
 * @param ColorMap          The pointer to the color map
 * @param BackgroundColor   The color of the background
 * @param ColorFormat       The color format of the color
 * 
 * @return true if the background was drawn, false otherwise
 * 
 * @see oC_ColorMap_DrawBackgroundImage
 * 
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawBackground(colorMap,oC_Color_Red,oC_ColorFormat_RGB888);
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool oC_ColorMap_DrawBackground( oC_ColorMap_t * ColorMap , oC_Color_t BackgroundColor , oC_ColorFormat_t ColorFormat )
{
    bool drawed = false;

    if(oC_ColorMap_IsCorrect(ColorMap) && isram(ColorMap->Map))
    {
        oC_Color_t color = oC_Color_ConvertToFormat(BackgroundColor,ColorFormat,ColorMap->ColorFormat);

        if(ColorMap->FormatSize == 1)
        {
            memset(ColorMap->GenericWriteMap,(uint8_t)color,ColorMap->Height * ColorMap->Width);
            drawed = true;
        }
        else if(ColorMap->FormatSize == 2)
        {
            memset_u16(ColorMap->GenericWriteMap,(uint16_t)color, ColorMap->Height * ColorMap->Width);
            drawed = true;
        }
        else if(ColorMap->FormatSize == 3)
        {
            uint8_t * endBuffer = &ColorMap->WriteMap[ColorMap->Height * ColorMap->Width * 3];
            uint8_t * buffer    = ColorMap->WriteMap;
            uint8_t r = (color >> 16) & 0xFF;
            uint8_t g = (color >> 8 ) & 0xFF;
            uint8_t b = (color >> 0 ) & 0xFF;

            while(buffer < endBuffer)
            {
                buffer[2] = r;
                buffer[1] = g;
                buffer[0] = b;
                buffer   += 3;
            }
        }
        else if(ColorMap->FormatSize == 4)
        {
            memset_u32(ColorMap->GenericWriteMap,(uint32_t)color, ColorMap->Height * ColorMap->Width);
            drawed = true;
        }
    }

    return drawed;
}

//==========================================================================================================================================
/**
 * @brief draws the image
 * 
 * The function draws the image at the specified position.
 * 
 * @warning The function is not implemented yet
 * 
 * @param ColorMap          The pointer to the color map
 * @param StartPosition     The start position of the image
 * @param Width             The width of the image
 * @param Height            The height of the image
 * @param FilePath          The path to the image file (the file must be in the BMP or PNG format)
 * 
 * @return the error code or #oC_ErrorCode_None if the image was drawn
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_NotImplemented       | The function is not implemented
 * 
 * @see oC_ColorMap_DrawBackground
 *
 * @code{.c}
   
    oC_ColorMap_t * colorMap = oC_ColorMap_New( getcurallocator() , AllocationFlags_None , 100 , 100 , oC_ColorFormat_RGB888 , 1 );
    if(colorMap != NULL)
    {
        // do something with the color map (for example draw something)
        oC_ColorMap_DrawImage(colorMap,(oC_Pixel_Position_t){.X = 50 , .Y = 50},100,100,"image.bmp");
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ColorMap_DrawImage( oC_ColorMap_t * ColorMap , oC_Pixel_Position_t StartPosition, oC_Pixel_ResolutionUInt_t Width, oC_Pixel_ResolutionUInt_t Height , const char * FilePath )
{
    return oC_ErrorCode_NotImplemented;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


