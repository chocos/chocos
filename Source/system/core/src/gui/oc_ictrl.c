/** ****************************************************************************************************************************************
 *
 * @brief      Stores functions of ICTRL object
 * 
 * @file       oc_ictrl.c
 *
 * @author     Patryk Kubiak - (Created on: 03.05.2017 16:46:45) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_ictrl.h>
#include <oc_struct.h>
#include <oc_dynamic_config.h>
#include <oc_array.h>
#include <oc_string.h>
#include <oc_list.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define MAX_SAMPLES         20
#define MAX_FINGERS         5

#define IsSingleEvent(EventId)              ( ((EventId) & oC_IDI_EventId_IndexMask) != 0 )
#define IsSoftwareDetectionForced()         ( oC_DynamicConfig_GetValue(ictrl,ForceSoftwareGestDetection) )

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    MoveDirection_None          = 0,
    MoveDirection_Up            = (1<<0),
    MoveDirection_Down          = (1<<1),
    MoveDirection_Left          = (1<<2),
    MoveDirection_Right         = (1<<3),
    MoveDirection_Force         = (1<<4),
    MoveDirection_UpLeft        = MoveDirection_Up      | MoveDirection_Left  ,
    MoveDirection_UpRight       = MoveDirection_Up      | MoveDirection_Right ,
    MoveDirection_DownLeft      = MoveDirection_Down    | MoveDirection_Left  ,
    MoveDirection_DownRight     = MoveDirection_Down    | MoveDirection_Right ,
} MoveDirection_t;

typedef enum
{
    MoveType_FingerRelease    ,
    MoveType_FingerPress ,
    MoveType_FingerHold     = MoveType_FingerPress,
} MoveType_t;

typedef enum
{
    FingerPosition_DoesNotMatter ,
    FingerPosition_Up           = 1<<0,
    FingerPosition_Down         = 1<<1,
    FingerPosition_Left         = 1<<2,
    FingerPosition_Right        = 1<<3,
    FingerPosition_UpLeft       = FingerPosition_Up     | FingerPosition_Left  ,
    FingerPosition_UpRight      = FingerPosition_Up     | FingerPosition_Right ,
    FingerPosition_DownLeft     = FingerPosition_Down   | FingerPosition_Left  ,
    FingerPosition_DownRight    = FingerPosition_Down   | FingerPosition_Right ,
} FingerPosition_t;

typedef struct
{
    MoveDirection_t         Direction;
    MoveType_t              MoveType;
    FingerPosition_t        FingerPosition;
} Move_t;

struct ICtrl_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_DefaultString_t      Name;
    oC_Driver_t             Driver;
    void *                  Context;
    bool                    Configured;
    oC_Screen_t             Screen;
    void *                  Config;
    oC_ICtrl_Rotate_t       Rotate;
};

typedef uint16_t FingerIndex_t;
typedef FingerIndex_t FingerIndexes_t[MAX_FINGERS];

typedef struct
{
    oC_IDI_EventId_t            ControllerId;
    FingerIndex_t               NumberOfFingers;
    MoveDirection_t             MoveDirection[MAX_FINGERS];
    MoveType_t                  MoveType[MAX_FINGERS];
    FingerPosition_t            FingerPosition[MAX_FINGERS];
    oC_Pixel_ResolutionUInt_t   X[MAX_FINGERS];
    oC_Pixel_ResolutionUInt_t   Y[MAX_FINGERS];
    oC_Pixel_ResolutionInt_t    DeltaX[MAX_FINGERS];
    oC_Pixel_ResolutionInt_t    DeltaY[MAX_FINGERS];
    oC_Pixel_ResolutionUInt_t   Weight[MAX_FINGERS];
} Sample_t;


typedef struct
{
    Sample_t            Samples[MAX_SAMPLES];
    FingerIndex_t       NumberOfFingers;
    uint16_t            NumberOfSamples;
    oC_Timestamp_t      Timestamp;
} EventSamples_t;

typedef struct
{
    oC_IDI_EventId_t        EventId;
    Move_t                  Moves[MAX_FINGERS][MAX_SAMPLES];
    uint16_t                NumberOfMoves[MAX_FINGERS];
    FingerIndex_t           NumberOfFingers;

    void (*ReadEventForGest)( EventSamples_t * EventSamples , FingerIndexes_t FingerIndexes , oC_IDI_Event_t * outEvent );
} GestDefinition_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static oC_ErrorCode_t       ReadEventFromDriver                 ( oC_ICtrl_t ICtrl , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent , oC_Time_t Timeout );
static bool                 IsEventSupportedByDriver            ( oC_ICtrl_t ICtrl , oC_IDI_EventId_t Event );
static bool                 IsEventSupportedBySoftware          ( oC_IDI_EventId_t Event );
static void                 ReadSampleFromEvent                 ( oC_IDI_Event_t * Event, Sample_t * PreviousSample, Sample_t * outSample );
static oC_ErrorCode_t       ReadNextSample                      ( oC_ICtrl_t ICtrl , Sample_t * PreviousSample , Sample_t * NextSample, oC_Time_t Timeout );
static oC_ErrorCode_t       ReadSamples                         ( oC_ICtrl_t ICtrl , EventSamples_t * EventSamples , oC_Time_t Timeout );
static oC_ErrorCode_t       CreateGestFromSamples               ( oC_ICtrl_t ICtrl , EventSamples_t * EventSamples , oC_IDI_EventId_t EventId );
static oC_ErrorCode_t       AnalyzeSamples                      ( EventSamples_t * EventSamples, oC_IDI_Event_t * outEvent );
static void                 ReadEventForRotateClockwise         ( EventSamples_t * EventSamples, FingerIndexes_t FingerIndexes, oC_IDI_Event_t * outEvent );
static void                 ReadEventForRotateCounterClockwise  ( EventSamples_t * EventSamples, FingerIndexes_t FingerIndexes, oC_IDI_Event_t * outEvent );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_Allocator_t               Allocator              = {
                .Name = "ICtrl" ,
};
static oC_List(GestDefinition_t*)   DynamicGestDefinitions = NULL;
static const GestDefinition_t       GestDefinitions[] = {
                {
                    .EventId            = oC_IDI_EventId_RightClick ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 3 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                    } ,
                    .NumberOfMoves[1]   = 3 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerRelease   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress     , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerRelease   , .Direction = MoveDirection_None      } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_TripleClick ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 6 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_DoubleClick ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 4 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_Click ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_Click ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_Click ,
                    .NumberOfFingers    = 3 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[2]   = 2 ,
                    .Moves[2]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_Click ,
                    .NumberOfFingers    = 4 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[2]   = 2 ,
                    .Moves[2]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[3]   = 2 ,
                    .Moves[3]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_Click ,
                    .NumberOfFingers    = 5 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[2]   = 2 ,
                    .Moves[2]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[3]   = 2 ,
                    .Moves[3]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                    .NumberOfMoves[4]   = 2 ,
                    .Moves[4]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveUp ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_Up    } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveDown ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_Down  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveLeft ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_Left  } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveRight ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_Right } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveUpLeft ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_UpLeft } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveUpRight ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_UpRight } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveDownLeft ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_DownLeft } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_MoveDownRight ,
                    .NumberOfFingers    = 1 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None  } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Force | MoveDirection_DownRight } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_RotateClockwise ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .ReadEventForGest   = ReadEventForRotateClockwise ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_Left    , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Right , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_RotateClockwise ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .ReadEventForGest   = ReadEventForRotateClockwise ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_Left     , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_None     , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_RotateClockwise ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .ReadEventForGest   = ReadEventForRotateClockwise ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None     , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Right     , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_RotateCounterClockwise ,
                    .NumberOfFingers    = 2 ,
                    .ReadEventForGest   = ReadEventForRotateCounterClockwise ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Right   , .FingerPosition = FingerPosition_Down  } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Left  , .FingerPosition = FingerPosition_Up    } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_RotateCounterClockwise ,
                    .NumberOfFingers    = 2 ,
                    .ReadEventForGest   = ReadEventForRotateCounterClockwise ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_None   , .FingerPosition = FingerPosition_Down  } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Left  , .FingerPosition = FingerPosition_Up    } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_RotateCounterClockwise ,
                    .NumberOfFingers    = 2 ,
                    .ReadEventForGest   = ReadEventForRotateCounterClockwise ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Right   , .FingerPosition = FingerPosition_Down  } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_None  , .FingerPosition = FingerPosition_Up    } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomIn ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Down      , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Up        , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomIn ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_None      , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Up        , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomIn ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Down      , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_None      , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomIn ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Up        , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomIn ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerRelease , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Down      , .FingerPosition = FingerPosition_Down } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomOut ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Up        , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Down      , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomOut ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_None      , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Down      , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
                {
                    .EventId            = oC_IDI_EventId_ZoomOut ,
                    .NumberOfFingers    = 2 ,
                    .NumberOfMoves[0]   = 2 ,
                    .Moves[0]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_Up        , .FingerPosition = FingerPosition_Down } ,
                    } ,
                    .NumberOfMoves[1]   = 2 ,
                    .Moves[1]           = {
                                            { .MoveType = MoveType_FingerPress   , .Direction = MoveDirection_None      } ,
                                            { .MoveType = MoveType_FingerHold    , .Direction = MoveDirection_None      , .FingerPosition = FingerPosition_Up   } ,
                    } ,
                } ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Creates new ICtrl object
 * 
 * The function is for creating new ICtrl object. The function allocates memory for the object and configuration. 
 * 
 * @warning Probably you don't need to use this function directly. Please try to use the #oC_ICtrlMan_GetICtrl module instead.
 * 
 * @param Name          name of the ICtrl object (can be used later in the #oC_ICtrlMan_GetICtrl module)
 * @param Config        pointer to the configuration of the driver
 * @param Driver        pointer to the driver object
 * @param DefaultScreen default screen for the input controller
 * @param Rotate        rotate of the screen - if the screen position is rotated compare to the controller
 * 
 * @return pointer to the ICtrl object or NULL if error occurs
 * 
 * @see oC_ICtrl_Delete, oC_ICtrlMan_GetICtrl
 * 
 * @note The function allocates memory for the object and configuration. The memory should be released by the #oC_ICtrl_Delete function.
 * 
 * @note The function does not configure the driver. The driver should be configured by the #oC_ICtrl_Configure function.
 * 
 * @code{.c}
    
    
    oC_ICtrl_t ictrl = oC_ICtrl_New( "ICtrl" , &TouchScreenConfig , &FT5336 , oC_ScreenMan_GetDefaultScreen() , oC_ICtrl_Rotate_None );
    if(ictrl != NULL)
    {
        // do something
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
oC_ICtrl_t oC_ICtrl_New( const char * Name, const void * Config, oC_Driver_t Driver, oC_Screen_t DefaultScreen , oC_ICtrl_Rotate_t Rotate )
{
    oC_ICtrl_t ictrl = NULL;

    oC_STATIC_ASSERT( oC_IDI_EventId_NumberOfDefinedEvents < oC_IDI_EventId_IndexMask , "Too many IDI events defined!" );

    if(
        oC_SaveIfFalse( "Name"      , isaddresscorrect(Name)    , oC_ErrorCode_WrongAddress )
     && oC_SaveIfFalse( "Driver"    , isaddresscorrect(Driver)  , oC_ErrorCode_WrongAddress )
        )
    {
        void * config = kmalloc( Driver->ConfigurationSize, &Allocator, AllocationFlags_ZeroFill );

        ictrl = kmalloc( sizeof(struct ICtrl_t), &Allocator, AllocationFlags_ZeroFill );

        if(
            oC_SaveIfFalse( "ictrl" , ictrl  != NULL , oC_ErrorCode_AllocationError )
         && oC_SaveIfFalse( "config", config != NULL , oC_ErrorCode_AllocationError )
            )
        {
            ictrl->ObjectControl    = oC_CountObjectControl(ictrl,oC_ObjectId_ICtrl);
            ictrl->Context          = NULL;
            ictrl->Driver           = Driver;
            ictrl->Screen           = DefaultScreen;
            ictrl->Configured       = false;
            ictrl->Config           = config;
            ictrl->Rotate           = Rotate;

            strcpy(ictrl->Name,Name);
            memcpy(config,Config,Driver->ConfigurationSize);
        }
        else
        {
            oC_SaveIfFalse("config", config == NULL || kfree(config,0) , oC_ErrorCode_ReleaseError);
            oC_SaveIfFalse("ictrl" , ictrl  == NULL || kfree(ictrl ,0) , oC_ErrorCode_ReleaseError);
        }
    }

    return ictrl;
}

//==========================================================================================================================================
/**
 * @brief Deletes the ICtrl object
 * 
 * The function deletes the ICtrl object. The function releases memory allocated for the object and configuration.
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return true if the object was deleted, false if error occurs
 */
//==========================================================================================================================================
bool oC_ICtrl_Delete( oC_ICtrl_t * ICtrl )
{
    bool deleted = false;

    if(
        oC_SaveIfFalse( "ICtrl" , isram(ICtrl)                  , oC_ErrorCode_AddressNotInRam  )
     && oC_SaveIfFalse( "ICtrl" , oC_ICtrl_IsCorrect(*ICtrl)    , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        oC_ICtrl_t ictrl = *ICtrl;

        if(ictrl->Configured == true)
        {
            if(oC_SaveIfErrorOccur("Unconfiguration of the driver", oC_Driver_Unconfigure(ictrl->Driver,NULL,&ictrl->Context)))
            {
                ictrl->Configured = false;
            }
        }
        if( oC_SaveIfFalse( "Unconfigure driver first!", ictrl->Configured == false, oC_ErrorCode_AlreadyConfigured ) )
        {
            ictrl->ObjectControl = 0;

            deleted = kfree(ictrl->Config,0);
            deleted = kfree(ictrl,0) && deleted;

            if(deleted)
            {
                *ICtrl = NULL;
            }
        }
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief checks if the given ICtrl object is correct
 * 
 * The function checks if the given ICtrl object is correct. The function checks if the object is in RAM and if the object control is correct.
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return true if the object is correct, false otherwise
 * 
 * @see oC_ICtrl_New, oC_ICtrl_Delete
 * 
 * @code{.c}
    
    
    oC_ICtrl_t ictrl = oC_ICtrl_New( "ICtrl" , &TouchScreenConfig , &FT5336 , oC_ScreenMan_GetDefaultScreen() , oC_ICtrl_Rotate_None );
    if(oC_ICtrl_IsCorrect(ictrl))
    {
        // do something
    }
    else
    {
        // error handling
    }

   @endcode
 */
//==========================================================================================================================================
bool  oC_ICtrl_IsCorrect( oC_ICtrl_t ICtrl )
{
    return isram(ICtrl) && oC_CheckObjectControl(ICtrl,oC_ObjectId_ICtrl,ICtrl->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief returns true if the driver is configured
 * 
 * The function returns true if the driver is configured. The function checks if the driver is configured and if the ICtrl object is correct.
 * 
 * If the driver is not configured, then the input controller is not ready to use.
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return true if the driver is configured, false otherwise
 */
//==========================================================================================================================================
bool oC_ICtrl_IsConfigured( oC_ICtrl_t ICtrl )
{
    bool configured = false;

    if(oC_ICtrl_IsCorrect(ICtrl))
    {
        configured = ICtrl->Configured;
    }

    return configured;
}

//==========================================================================================================================================
/**
 * @brief returns true if the event is supported by driver
 * 
 * The function checks if the given event is supported by the driver connected to the ICtrl object.
 * 
 * @param ICtrl     pointer to the ICtrl object
 * @param EventMask event mask
 * 
 * @return true if the event is supported by the driver, false otherwise
 */
//==========================================================================================================================================
bool oC_ICtrl_IsEventSupportedByDriver( oC_ICtrl_t ICtrl , oC_IDI_EventId_t EventMask )
{
    bool isSupported = false;

    if(oC_ICtrl_IsCorrect(ICtrl))
    {
        isSupported = IsEventSupportedByDriver(ICtrl,EventMask);
    }

    return isSupported;
}

//==========================================================================================================================================
/**
 * @brief returns name of the ICtrl object
 * 
 * The function returns the name of the ICtrl object. The name is set during the creation of the object.
 * 
 * @note the function never returns NULL (if the ICtrl object is correct then the default name is returned)
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return name of the ICtrl object
 * 
 * @see oC_ICtrl_New
 * 
 * @code{.c}
    
    
    oC_ICtrl_t ictrl = oC_ICtrl_New( "ICtrl" , &TouchScreenConfig , &FT5336 , oC_ScreenMan_GetDefaultScreen() , oC_ICtrl_Rotate_None );
    if(ictrl != NULL)
    {
        const char * name = oC_ICtrl_GetName(ictrl);
        printf("ICtrl name: %s\n",name);
    }

   @endcode
 */
//==========================================================================================================================================
const char * oC_ICtrl_GetName( oC_ICtrl_t ICtrl )
{
    const char * name = "unknown";

    if(oC_SaveIfFalse( "ICtrl object" , oC_ICtrl_IsCorrect(ICtrl), oC_ErrorCode_ObjectNotCorrect ))
    {
        name = ICtrl->Name;
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief configures the driver
 * 
 * The function configures the driver connected to the ICtrl object. The function calls the driver configuration function with the configuration
 * and context from the ICtrl object.
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return Code of the error or #oC_ErrorCode_None if the driver was configured successfully
 * 
 * Possible error codes:
 * 
 *  Code of error                                | Description
 * ----------------------------------------------|-----------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                 | Unexpected error
 *  #oC_ErrorCode_ObjectNotCorrect               | The ICtrl object is not correct
 *  #oC_ErrorCode_AlreadyConfigured              | The driver is already configured
 * 
 * @note More error codes can be returned by the driver configuration function (#oC_Driver_Configure)
 * 
 * @see oC_ICtrl_Unconfigure, oC_ICtrl_IsConfigured
 * 
 * @code{.c}
    
    
    oC_ICtrl_t ictrl = oC_ICtrl_New( "ICtrl" , &TouchScreenConfig , &FT5336 , oC_ScreenMan_GetDefaultScreen() , oC_ICtrl_Rotate_None );
    if(ictrl != NULL)
    {
        if(ErrorCode(oC_ICtrl_Configure(ictrl)))
        {
            // do something
        }
        else
        {
            // error handling
        }
    }

   @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrl_Configure( oC_ICtrl_t ICtrl )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_ICtrl_IsCorrect(ICtrl)   , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( ICtrl->Configured == false  , oC_ErrorCode_AlreadyConfigured  )
        )
    {
        if( ErrorCode( oC_Driver_Configure(ICtrl->Driver, ICtrl->Config, &ICtrl->Context) ) )
        {
            ICtrl->Configured   = true;
            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures the driver
 * 
 * The function unconfigures the driver connected to the ICtrl object. The function calls the driver unconfiguration function with the context
 * from the ICtrl object.
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return Code of the error or #oC_ErrorCode_None if the driver was unconfigured successfully
 * 
 * Possible error codes:
 * 
 *  Code of error                                | Description
 * ----------------------------------------------|-----------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                 | Unexpected error
 *  #oC_ErrorCode_ObjectNotCorrect               | The ICtrl object is not correct
 *  #oC_ErrorCode_DriverNotConfiguredYet         | The driver is not configured yet
 * 
 * @note More error codes can be returned by the driver unconfiguration function (#oC_Driver_Unconfigure)
 * 
 * @see oC_ICtrl_Configure, oC_ICtrl_IsConfigured
 * 
 * @code{.c}
    
    
    oC_ICtrl_t ictrl = oC_ICtrl_New( "ICtrl" , &TouchScreenConfig , &FT5336 , oC_ScreenMan_GetDefaultScreen() , oC_ICtrl_Rotate_None );
    if(ictrl != NULL)
    {
        if(ErrorCode(oC_ICtrl_Configure(ictrl)))
        {
            // do something
        }
        else
        {
            // error handling
        }
        
        if(ErrorCode(oC_ICtrl_Unconfigure(ictrl)))
        {
            // do something
        }
        else
        {
            // error handling
        }
    }

   @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrl_Unconfigure( oC_ICtrl_t ICtrl )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_ICtrl_IsCorrect(ICtrl)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( ICtrl->Configured == true   , oC_ErrorCode_DriverNotConfiguredYet   )
        )
    {
        if( ErrorCode( oC_Driver_Unconfigure(ICtrl->Driver, ICtrl->Config, &ICtrl->Context) ) )
        {
            ICtrl->Configured   = false;
            errorCode           = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns driver associated with the ICtrl object
 * 
 * The function returns the driver associated with the ICtrl object. The driver is set during the creation of the ICtrl object.
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return pointer to the driver object or NULL if error occurs
 */
//==========================================================================================================================================
oC_Driver_t oC_ICtrl_GetDriver( oC_ICtrl_t ICtrl )
{
    oC_Driver_t driver = NULL;

    if(oC_SaveIfFalse( "ICtrl object" , oC_ICtrl_IsCorrect(ICtrl), oC_ErrorCode_ObjectNotCorrect ))
    {
        driver = ICtrl->Driver;
    }

    return driver;
}

//==========================================================================================================================================
/**
 * @brief returns screen associated with the ICtrl object
 * 
 * The function returns the screen associated with the ICtrl object. The screen is set during the creation of the ICtrl object.
 * 
 * @param ICtrl pointer to the ICtrl object
 * 
 * @return pointer to the screen object or NULL if error occurs
 */
//==========================================================================================================================================
oC_Screen_t oC_ICtrl_GetScreen( oC_ICtrl_t ICtrl )
{
    oC_Screen_t screen = NULL;

    if(oC_SaveIfFalse( "ICtrl object" , oC_ICtrl_IsCorrect(ICtrl), oC_ErrorCode_ObjectNotCorrect ))
    {
        screen = ICtrl->Screen;
    }

    return screen;
}

//==========================================================================================================================================
/**
 * @brief sets screen associated with the ICtrl object
 * 
 * The function sets the screen associated with the ICtrl object. The screen should be correct and should be created before.
 * 
 * @param ICtrl  pointer to the ICtrl object
 * @param Screen pointer to the screen object
 * 
 * @return true if the screen was set, false if error occurs
 */
//==========================================================================================================================================
bool oC_ICtrl_SetScreen( oC_ICtrl_t ICtrl, oC_Screen_t Screen )
{
    bool set = false;

    if(
        oC_SaveIfFalse( "ICtrl object"  , oC_ICtrl_IsCorrect(ICtrl)     , oC_ErrorCode_ObjectNotCorrect )
     && oC_SaveIfFalse( "Screen object" , oC_Screen_IsCorrect(Screen)   , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        ICtrl->Screen   = Screen;
        set             = true;
    }

    return set;
}

//==========================================================================================================================================
/**
 * @brief waits for event
 * 
 * The function waits for the event from the driver. The function waits for the event with the given mask. 
 * 
 * @param ICtrl      pointer to the ICtrl object
 * @param EventsMask mask of the events to wait for
 * @param outEvent   pointer to the event structure where the event will be stored
 * @param Timeout    maximum time to wait for the event
 * 
 * @return Code of the error or #oC_ErrorCode_None if the event was received
 * 
 * Possible error codes:
 * 
 *  Code of error                                | Description
 * ----------------------------------------------|-----------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                 | Unexpected error
 *  #oC_ErrorCode_AddressNotInRam                | The ICtrl object is not in RAM
 *  #oC_ErrorCode_ObjectNotCorrect               | The ICtrl object is not correct
 *  #oC_ErrorCode_DriverNotConfiguredYet         | The driver is not configured yet (call the #oC_ICtrl_Configure function)
 *  #oC_ErrorCode_TimeNotCorrect                 | The timeout is not correct
 * 
 * @note more error codes can be returned by the driver function (#oC_Driver_WaitForInput)
 * 
 * @see oC_ICtrl_RecordGesture
 * 
 * @code{.c}
    
    
    oC_ICtrl_t ictrl = oC_ICtrl_New( "ICtrl" , &TouchScreenConfig , &FT5336 , oC_ScreenMan_GetDefaultScreen() , oC_ICtrl_Rotate_None );
    if(ictrl != NULL)
    {
        if(ErrorCode(oC_ICtrl_Configure(ictrl)))
        {
            oC_IDI_Event_t event;
            if(ErrorCode(oC_ICtrl_WaitForEvent(ictrl,oC_IDI_EventId_AnyController,&event,1000)))
            {
                // do something
            }
            else
            {
                // error handling
            }
        }
        else
        {
            // error handling
        }
    }

   @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrl_WaitForEvent( oC_ICtrl_t ICtrl, oC_IDI_EventId_t EventsMask, oC_IDI_Event_t * outEvent, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(ICtrl)             , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( oC_ICtrl_IsCorrect(ICtrl), oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( ICtrl->Configured        , oC_ErrorCode_DriverNotConfiguredYet  )
     && ErrorCondition( Timeout >= 0             , oC_ErrorCode_TimeNotCorrect          )
        )
    {
        oC_Struct_Define( EventSamples_t, eventSamples );

        if(IsEventSupportedByDriver(ICtrl,oC_IDI_EventId_AnyController | oC_IDI_EventId_ClickWithoutRelease) == false)
        {
            errorCode = ReadEventFromDriver(ICtrl,EventsMask,outEvent,Timeout);
        }
        else if(
            IsSingleEvent(EventsMask)
         && IsEventSupportedByDriver(ICtrl,EventsMask)
         && IsSoftwareDetectionForced() == false
            )
        {
            errorCode = ReadEventFromDriver(ICtrl,EventsMask,outEvent,Timeout);
        }
        else if(
            IsSingleEvent(EventsMask)   == false
         && IsSoftwareDetectionForced() == false
         && IsEventSupportedByDriver(ICtrl, oC_IDI_EventId_AnyController | oC_IDI_EventId_AllMoves)
            )
        {
            errorCode = ReadEventFromDriver(ICtrl,EventsMask,outEvent,Timeout);
        }
        else if( ErrorCondition( IsEventSupportedBySoftware(EventsMask) , oC_ErrorCode_EventNotSupported ) )
        {
            if( ErrorCode( ReadSamples(ICtrl, &eventSamples, Timeout ) ) )
            {
                errorCode = AnalyzeSamples(&eventSamples,outEvent);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief records custom gesture
 * 
 * The function allows for recording of custom gestures - it will be associated with the given event ID. 
 * Once the gesture is recorded, it can be detected in the #oC_ICtrl_WaitForEvent function.
 * 
 * Thanks to this function, the user can define custom gestures and use them in the application as a trigger for some actions.
 * 
 * @param ICtrl    pointer to the ICtrl object
 * @param EventId  event ID to associate with the gesture
 * @param Timeout  maximum time to record the gesture
 * 
 * @return Code of the error or #oC_ErrorCode_None if the gesture was recorded
 * 
 * Possible error codes:
 * 
 *  Code of error                                | Description
 * ----------------------------------------------|-----------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError                 | Unexpected error
 *  #oC_ErrorCode_AddressNotInRam                | The ICtrl object is not in RAM
 *  #oC_ErrorCode_ObjectNotCorrect               | The ICtrl object is not correct
 *  #oC_ErrorCode_DriverNotConfiguredYet         | The driver is not configured yet (call the #oC_ICtrl_Configure function)
 *  #oC_ErrorCode_TimeNotCorrect                 | The timeout is not correct
 * 
 * @note more error codes can be returned by the driver function (#oC_Driver_WaitForInput)
 * 
 * @see oC_ICtrl_WaitForEvent
 * 
 * @code{.c}
    
    // create ICtrl object
    oC_ICtrl_t ictrl = oC_ICtrl_New( "ICtrl" , &TouchScreenConfig , &FT5336 , oC_ScreenMan_GetDefaultScreen() , oC_ICtrl_Rotate_None );
    if(ictrl != NULL)
    {
        // configure the driver
        if(ErrorCode(oC_ICtrl_Configure(ictrl)))
        {
            // prepare event ID for the custom gesture (it should be unique)
            oC_IDI_EventId_t eventId = oC_IDI_EventId_TouchScreenEvent | (oC_IDI_EventId_NumberOfDefinedEvents + 1);

            // record the gesture
            if(ErrorCode(oC_ICtrl_RecordGesture(ictrl,eventId,1000)))
            {
                // now the gesture is recorded and can be detected in the application
                if(ErrorCode(oC_ICtrl_WaitForEvent(ictrl,eventId,&event,1000)))
                {
                    printf("Gesture detected!\n");
                }
                else
                {
                    // error handling
                }
            }
            else
            {
                // error handling
            }
        }
        else
        {
            // error handling
        }
    }

   @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrl_RecordGesture( oC_ICtrl_t ICtrl, oC_IDI_EventId_t EventId, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(ICtrl)             , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( oC_ICtrl_IsCorrect(ICtrl), oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( ICtrl->Configured        , oC_ErrorCode_DriverNotConfiguredYet  )
     && ErrorCondition( Timeout >= 0             , oC_ErrorCode_TimeNotCorrect          )
        )
    {
        oC_Struct_Define( EventSamples_t, eventSamples );

        if( ErrorCode( ReadSamples(ICtrl, &eventSamples, Timeout ) ) )
        {
            errorCode = CreateGestFromSamples(ICtrl,&eventSamples,EventId);
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief reads event state from the driver
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadEventFromDriver( oC_ICtrl_t ICtrl , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent , oC_Time_t Timeout )
{
    memset(outEvent,0,sizeof(oC_IDI_Event_t));

    oC_ErrorCode_t errorCode = oC_Driver_WaitForInput(ICtrl->Driver,ICtrl->Context,EventsMask,outEvent,Timeout);

    oC_Pixel_ResolutionUInt_t width  = 0;
    oC_Pixel_ResolutionUInt_t height = 0;

    outEvent->Timestamp = gettimestamp();

    if(ICtrl->Rotate == oC_ICtrl_Rotate_90DegreesClockwise)
    {
        for(FingerIndex_t i = 0; i < outEvent->NumberOfPoints; i++)
        {
            oC_Pixel_ResolutionUInt_t x         = outEvent->Position[i].X;
            oC_Pixel_ResolutionUInt_t y         = outEvent->Position[i].Y;
            oC_Pixel_ResolutionInt_t  dx        = outEvent->DeltaX[i];
            oC_Pixel_ResolutionInt_t  dy        = outEvent->DeltaY[i];

            outEvent->Position[i].X = y;
            outEvent->Position[i].Y = x;

            outEvent->DeltaX[i] = dy;
            outEvent->DeltaY[i] = dx;
        }
    }
    else if(ICtrl->Rotate == oC_ICtrl_Rotate_180DegreesClockwise)
    {
        if(oC_SaveIfErrorOccur("cannot read resolution to rotate", oC_Screen_ReadResolution(ICtrl->Screen,&width,&height) ))
        {
            for(FingerIndex_t i = 0; i < outEvent->NumberOfPoints; i++)
            {
                oC_Pixel_ResolutionUInt_t x         = outEvent->Position[i].X;
                oC_Pixel_ResolutionUInt_t y         = outEvent->Position[i].Y;
                oC_Pixel_ResolutionInt_t  dx        = outEvent->DeltaX[i];
                oC_Pixel_ResolutionInt_t  dy        = outEvent->DeltaY[i];

                outEvent->Position[i].X = width  - x;
                outEvent->Position[i].Y = height - y;

                outEvent->DeltaX[i] = -dx;
                outEvent->DeltaY[i] = -dy;
            }
        }
    }
    else if(ICtrl->Rotate == oC_ICtrl_Rotate_270DegreesClockwise)
    {
        if(oC_SaveIfErrorOccur("cannot read resolution to rotate", oC_Screen_ReadResolution(ICtrl->Screen,&width,&height) ))
        {
            for(FingerIndex_t i = 0; i < outEvent->NumberOfPoints; i++)
            {
                oC_Pixel_ResolutionUInt_t x         = outEvent->Position[i].X;
                oC_Pixel_ResolutionUInt_t y         = outEvent->Position[i].Y;
                oC_Pixel_ResolutionInt_t  dx        = outEvent->DeltaX[i];
                oC_Pixel_ResolutionInt_t  dy        = outEvent->DeltaY[i];

                outEvent->Position[i].X = height - y;
                outEvent->Position[i].Y = width  - x;

                outEvent->DeltaX[i] = -dy;
                outEvent->DeltaY[i] = -dx;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver supports the given Event ID
 */
//==========================================================================================================================================
static bool IsEventSupportedByDriver( oC_ICtrl_t ICtrl , oC_IDI_EventId_t Event )
{
    return oC_Driver_IsEventSupported(ICtrl->Driver,ICtrl->Context,Event);
}

//==========================================================================================================================================
/**
 * @brief checks if the driver supports the given Event ID
 */
//==========================================================================================================================================
static bool IsEventSupportedBySoftware( oC_IDI_EventId_t Event )
{
    bool supported = false;

    if((Event & oC_IDI_EventId_IndexMask) == oC_IDI_EventId_AnyMove )
    {
        supported = true;
    }
    else
    {
        oC_ARRAY_FOREACH_IN_ARRAY(GestDefinitions,gest)
        {
            if(gest->EventId == Event)
            {
                supported = true;
                break;
            }
        }
    }

    return supported;
}

//==========================================================================================================================================
/**
 * @brief reads sample from event
 */
//==========================================================================================================================================
static void ReadSampleFromEvent( oC_IDI_Event_t * Event, Sample_t * PreviousSample, Sample_t * outSample )
{
    memset(outSample,0,sizeof(Sample_t));

    outSample->NumberOfFingers = Event->NumberOfPoints;
    outSample->ControllerId    = Event->EventId & oC_IDI_EventId_AnyController;

    for(FingerIndex_t fingerIndex = 0; fingerIndex < Event->NumberOfPoints; fingerIndex++)
    {
        outSample->X[fingerIndex]       = Event->Position[fingerIndex].X;
        outSample->Y[fingerIndex]       = Event->Position[fingerIndex].Y;
        outSample->Weight[fingerIndex]  = Event->Weight[fingerIndex] / 3;

        oC_Pixel_ResolutionInt_t minMove = (oC_Pixel_ResolutionInt_t)(outSample->Weight[fingerIndex]);

        if(PreviousSample != NULL && fingerIndex < PreviousSample->NumberOfFingers)
        {
            outSample->MoveType[fingerIndex] = MoveType_FingerHold;

            outSample->DeltaX[fingerIndex]    = (oC_Pixel_ResolutionInt_t)outSample->X[fingerIndex] - (oC_Pixel_ResolutionInt_t)PreviousSample->X[fingerIndex];
            outSample->DeltaY[fingerIndex]    = (oC_Pixel_ResolutionInt_t)outSample->Y[fingerIndex] - (oC_Pixel_ResolutionInt_t)PreviousSample->Y[fingerIndex];

            if(outSample->DeltaX[fingerIndex] > minMove)
            {
                outSample->MoveDirection[fingerIndex] |= MoveDirection_Right;
            }
            else if(outSample->DeltaX[fingerIndex] < (0 - minMove))
            {
                outSample->MoveDirection[fingerIndex] |= MoveDirection_Left;
            }
            if(outSample->DeltaY[fingerIndex] > minMove)
            {
                outSample->MoveDirection[fingerIndex] |= MoveDirection_Down;
            }
            else if(outSample->DeltaY[fingerIndex] < (0 - minMove))
            {
                outSample->MoveDirection[fingerIndex] |= MoveDirection_Up;
            }
        }
        else
        {
            outSample->MoveType[fingerIndex]        = MoveType_FingerPress;
            outSample->MoveDirection[fingerIndex]   = MoveDirection_None;
        }

        for(uint16_t secondFingerIndex = 0; secondFingerIndex < Event->NumberOfPoints; secondFingerIndex++)
        {
            if(secondFingerIndex != fingerIndex)
            {
                if(Event->Position[fingerIndex].X > Event->Position[secondFingerIndex].X)
                {
                    outSample->FingerPosition[fingerIndex] |= FingerPosition_Right;
                }
                if(Event->Position[fingerIndex].X < Event->Position[secondFingerIndex].X)
                {
                    outSample->FingerPosition[fingerIndex] |= FingerPosition_Left;
                }
                if(Event->Position[fingerIndex].Y < Event->Position[secondFingerIndex].Y)
                {
                    outSample->FingerPosition[fingerIndex] |= FingerPosition_Up;
                }
                if(Event->Position[fingerIndex].Y > Event->Position[secondFingerIndex].Y)
                {
                    outSample->FingerPosition[fingerIndex] |= FingerPosition_Down;
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief reads next sample
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadNextSample( oC_ICtrl_t ICtrl , Sample_t * PreviousSample , Sample_t * NextSample, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Struct_Define(oC_IDI_Event_t, event);

    memset(NextSample,0,sizeof(Sample_t));

    if( ErrorCode( ReadEventFromDriver( ICtrl, oC_IDI_EventId_AnyController | oC_IDI_EventId_ClickWithoutRelease, &event, Timeout ) ) )
    {
        ReadSampleFromEvent(&event,PreviousSample,NextSample);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief joins samples if they are similar and returns true if it does
 */
//==========================================================================================================================================
static bool JoinSimilarSamples( Sample_t * PreviousSample, Sample_t * NextSample )
{
    bool similar = false;

    if(PreviousSample != NULL && NextSample != NULL)
    {
        if(PreviousSample->NumberOfFingers == NextSample->NumberOfFingers)
        {
            similar = true;

            for(FingerIndex_t i = 0; i < NextSample->NumberOfFingers && similar; i++)
            {
                if(
                    PreviousSample->FingerPosition[i] != NextSample->FingerPosition[i]
                 || PreviousSample->MoveType[i]       != NextSample->MoveType[i]
                 || PreviousSample->MoveDirection[i]  != NextSample->MoveDirection[i]
                    )
                {
                    similar = false;
                }
            }

            if(similar)
            {
                for(FingerIndex_t i = 0; i < NextSample->NumberOfFingers && similar; i++)
                {
                    PreviousSample->X[i]        = NextSample->X[i];
                    PreviousSample->Y[i]        = NextSample->Y[i];
                    PreviousSample->DeltaX[i]  += NextSample->DeltaX[i];
                    PreviousSample->DeltaY[i]  += NextSample->DeltaY[i];
                }
            }
        }
        else
        {

        }
    }

    return similar;
}

//==========================================================================================================================================
/**
 * @brief reads gesture samples
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadSamples( oC_ICtrl_t ICtrl , EventSamples_t * EventSamples , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
    oC_Time_t      timeout      = gettimeout(endTimestamp);

    EventSamples->NumberOfFingers = 0;
    EventSamples->Timestamp       = gettimestamp();

    for(EventSamples->NumberOfSamples = 0; EventSamples->NumberOfSamples < MAX_SAMPLES && gettimestamp() < endTimestamp; EventSamples->NumberOfSamples++)
    {
        Sample_t * previousSample   = EventSamples->NumberOfSamples == 0 ? NULL : &EventSamples->Samples[EventSamples->NumberOfSamples - 1];
        Sample_t * nextSample       = &EventSamples->Samples[EventSamples->NumberOfSamples];

        timeout = oC_MIN( timeout , gettimeout(endTimestamp) );

        if( ErrorCode( ReadNextSample(ICtrl, previousSample, nextSample, timeout) ) )
        {
            kdebuglog(oC_LogType_Track, "Sample read: number of samples = %d\n", EventSamples->NumberOfSamples);

            timeout     = oC_DynamicConfig_GetValue(ictrl,MinFingerReleaseTime);
            errorCode   = oC_ErrorCode_None;

            EventSamples->NumberOfFingers = oC_MAX(EventSamples->NumberOfFingers,nextSample->NumberOfFingers);

            if(previousSample != NULL && JoinSimilarSamples(previousSample,nextSample))
            {
                EventSamples->NumberOfSamples--;
            }

            sleep( oC_DynamicConfig_GetValue(ictrl,GettingSamplesPeriod) );
        }
        else if(errorCode == oC_ErrorCode_Timeout && previousSample != NULL && previousSample->NumberOfFingers != 0)
        {
            kdebuglog(oC_LogType_Track, "Cannot read sample: %R, number of samples = %d, trying once again\n", errorCode, EventSamples->NumberOfSamples);
            timeout = oC_DynamicConfig_GetValue(ictrl,MaxFingerReleaseTime);
        }
        else
        {
            kdebuglog(oC_LogType_Track, "Cannot read sample: %R, number of samples = %d\n", errorCode, EventSamples->NumberOfSamples);
            if(errorCode == oC_ErrorCode_Timeout && previousSample != NULL)
            {
                errorCode = oC_ErrorCode_None;
            }
            break;
        }
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief adds new gesture to list
 */
//==========================================================================================================================================
static oC_ErrorCode_t AddNewGesture( const GestDefinition_t * Gest )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(DynamicGestDefinitions == NULL)
    {
        DynamicGestDefinitions = oC_List_New(&Allocator,AllocationFlags_ZeroFill);
    }
    if(ErrorCondition( DynamicGestDefinitions != NULL, oC_ErrorCode_AllocationError ))
    {
        GestDefinition_t * gest = kmalloc( sizeof(GestDefinition_t), &Allocator, AllocationFlags_ZeroFill );

        if(ErrorCondition( gest != NULL , oC_ErrorCode_AllocationError ))
        {
            memcpy(gest,Gest,sizeof(GestDefinition_t));

            bool pushed = oC_List_PushCopyBack(DynamicGestDefinitions,gest,&Allocator);

            if(ErrorCondition( pushed , oC_ErrorCode_CannotAddObjectToList ))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                kfree(gest,0);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief creates gesture from event samples
 */
//==========================================================================================================================================
static oC_ErrorCode_t CreateGestFromSamples( oC_ICtrl_t ICtrl , EventSamples_t * EventSamples , oC_IDI_EventId_t EventId )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Struct_Define(GestDefinition_t,gest);

    gest.EventId            = EventId;
    gest.NumberOfFingers    = EventSamples->NumberOfFingers;

    for(FingerIndex_t fingerIndex = 0; fingerIndex < gest.NumberOfFingers; fingerIndex++)
    {
        gest.NumberOfMoves[fingerIndex] = EventSamples->NumberOfSamples;

        for(uint16_t moveIndex = 0; moveIndex < gest.NumberOfMoves[fingerIndex]; moveIndex++ )
        {
            gest.Moves[fingerIndex][moveIndex].Direction        = EventSamples->Samples[moveIndex].MoveDirection[fingerIndex];
            gest.Moves[fingerIndex][moveIndex].FingerPosition   = EventSamples->Samples[moveIndex].FingerPosition[fingerIndex];
            gest.Moves[fingerIndex][moveIndex].MoveType         = EventSamples->Samples[moveIndex].MoveType[fingerIndex];
        }
    }

    errorCode = AddNewGesture(&gest);

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads event from gest if it matches
 */
//==========================================================================================================================================
static bool ReadGestIfMatches( const GestDefinition_t * Gest , EventSamples_t * EventSamples , oC_IDI_Event_t * outEvent )
{
    bool            gestMatches = false;
    bool            fingerUsed[MAX_FINGERS];
    FingerIndexes_t fingerIndexes;

    if(Gest->NumberOfFingers == EventSamples->NumberOfFingers)
    {
        gestMatches = true;

        memset(fingerUsed,0,sizeof(fingerUsed));
        memset(fingerIndexes,0,sizeof(fingerIndexes));

        for(FingerIndex_t gestFingerIndex = 0; gestFingerIndex < Gest->NumberOfFingers && gestMatches; gestFingerIndex++)
        {
            bool matchedFingerFound = false;

            if(Gest->NumberOfMoves[gestFingerIndex] <= EventSamples->NumberOfSamples)
            {
                for(FingerIndex_t sampleFingerIndex = 0; sampleFingerIndex < Gest->NumberOfFingers && !matchedFingerFound; sampleFingerIndex++)
                {
                    if(fingerUsed[sampleFingerIndex] == false)
                    {
                        bool fingerMaches = true;

                        for(uint16_t moveIndex = 0; moveIndex < Gest->NumberOfMoves[gestFingerIndex] && fingerMaches; moveIndex++)
                        {
                            const Move_t *   gestMove           = &Gest->Moves[gestFingerIndex][moveIndex];
                            const Sample_t * sample             = &EventSamples->Samples[moveIndex];
                            MoveDirection_t  gestDirection      = gestMove->Direction & (~MoveDirection_Force);
                            MoveDirection_t  moveDirection      = sample->MoveDirection[sampleFingerIndex];
                            bool             forced             = (gestMove->Direction & MoveDirection_Force) != 0 || gestDirection == MoveDirection_None;

                            if(
                                (
                                   (forced == true  &&  gestDirection == moveDirection          )
                                || (forced == false && ((gestDirection & moveDirection) != 0)   )
                                   )
                             && gestMove->MoveType       == sample->MoveType[sampleFingerIndex]
                                )
                            {
                                if((gestMove->FingerPosition & sample->FingerPosition[sampleFingerIndex]) != gestMove->FingerPosition)
                                {
                                    fingerMaches = false;
                                }
                            }
                            else
                            {
                                fingerMaches = false;
                            }
                        }

                        if(fingerMaches)
                        {
                            fingerIndexes[gestFingerIndex]  = sampleFingerIndex;
                            fingerUsed[sampleFingerIndex]   = true;
                            matchedFingerFound              = true;
                        }
                    }
                }
            }

            if(matchedFingerFound == false)
            {
                gestMatches = false;
            }
        }

        if(gestMatches)
        {
            outEvent->EventId           = Gest->EventId | EventSamples->Samples[0].ControllerId;
            outEvent->NumberOfPoints    = Gest->NumberOfFingers;

            for(FingerIndex_t fingerIndex = 0; fingerIndex < Gest->NumberOfFingers; fingerIndex++)
            {
                FingerIndex_t matchedFingerIndex  = fingerIndexes[fingerIndex];
                outEvent->Position[fingerIndex].X = EventSamples->Samples[0].X[matchedFingerIndex];
                outEvent->Position[fingerIndex].Y = EventSamples->Samples[0].Y[matchedFingerIndex];
                outEvent->Weight[fingerIndex]     = EventSamples->Samples[0].Weight[matchedFingerIndex];
                outEvent->DeltaX[fingerIndex]     = EventSamples->Samples[Gest->NumberOfMoves[fingerIndex] - 1].X[matchedFingerIndex] - EventSamples->Samples[0].X[matchedFingerIndex];
                outEvent->DeltaY[fingerIndex]     = EventSamples->Samples[Gest->NumberOfMoves[fingerIndex] - 1].Y[matchedFingerIndex] - EventSamples->Samples[0].Y[matchedFingerIndex];
            }

            if(Gest->ReadEventForGest != NULL)
            {
                Gest->ReadEventForGest(EventSamples,fingerIndexes,outEvent);
            }
        }
    }

    return gestMatches;
}

//==========================================================================================================================================
/**
 * @brief analyzes samples read before and converts it into the IDI event
 */
//==========================================================================================================================================
static oC_ErrorCode_t AnalyzeSamples( EventSamples_t * EventSamples, oC_IDI_Event_t * outEvent )
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_UnknownGesture;

    memset(outEvent,0,sizeof(oC_IDI_Event_t));

    oC_ARRAY_FOREACH_IN_ARRAY(GestDefinitions,gest)
    {
        if(ReadGestIfMatches(gest,EventSamples,outEvent))
        {
            errorCode = oC_ErrorCode_None;
            break;
        }
    }

    if(errorCode == oC_ErrorCode_UnknownGesture)
    {
        foreach(DynamicGestDefinitions,gest)
        {
            if(ReadGestIfMatches(gest,EventSamples,outEvent))
            {
                errorCode = oC_ErrorCode_None;
                break;
            }
        }
    }

    if(errorCode == oC_ErrorCode_UnknownGesture)
    {
        outEvent->EventId           = oC_IDI_EventId_AnyController | oC_IDI_EventId_None;
        outEvent->NumberOfPoints    = EventSamples->NumberOfFingers;

        for(FingerIndex_t fingerIndex = 0; fingerIndex < EventSamples->NumberOfFingers; fingerIndex++)
        {
            for(uint16_t moveIndex = 0; moveIndex < EventSamples->NumberOfSamples; moveIndex++)
            {
                if(EventSamples->Samples[moveIndex].MoveType[fingerIndex] != MoveType_FingerRelease && outEvent->Weight[fingerIndex] == 0)
                {
                    outEvent->Position[fingerIndex].X = EventSamples->Samples[moveIndex].X[fingerIndex];
                    outEvent->Position[fingerIndex].Y = EventSamples->Samples[moveIndex].Y[fingerIndex];
                    outEvent->Weight[fingerIndex]     = EventSamples->Samples[moveIndex].Weight[fingerIndex];
                    break;
                }
            }
        }

        errorCode = oC_ErrorCode_None;
    }

    outEvent->Timestamp = EventSamples->Timestamp;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads event data for RotateClockwise gesture
 */
//==========================================================================================================================================
static void ReadEventForRotateClockwise( EventSamples_t * EventSamples, FingerIndexes_t FingerIndexes, oC_IDI_Event_t * outEvent )
{
    oC_Pixel_ResolutionUInt_t dy = abs(EventSamples->Samples[1].DeltaY[FingerIndexes[0]]);
    oC_Pixel_ResolutionUInt_t h  = abs(EventSamples->Samples[0].Y[FingerIndexes[0]] - EventSamples->Samples[0].Y[FingerIndexes[1]]);

    outEvent->RotateAngle = (180 * dy)/h;
}

//==========================================================================================================================================
/**
 * @brief reads event data for RotateCounterClockwise gesture
 */
//==========================================================================================================================================
static void ReadEventForRotateCounterClockwise ( EventSamples_t * EventSamples, FingerIndexes_t FingerIndexes, oC_IDI_Event_t * outEvent )
{
    oC_Pixel_ResolutionUInt_t dy = abs(EventSamples->Samples[1].DeltaY[FingerIndexes[0]]);
    oC_Pixel_ResolutionUInt_t h  = abs(EventSamples->Samples[0].Y[FingerIndexes[0]] - EventSamples->Samples[0].Y[FingerIndexes[1]]);

    outEvent->RotateAngle = (180 * dy)/h;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________




