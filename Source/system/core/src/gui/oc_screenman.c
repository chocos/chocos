/** ****************************************************************************************************************************************
 *
 * @file       oc_screenman.c
 *
 * @brief      The file with implementation of the Screen Manager module
 *
 * @author     Patryk Kubiak - (Created on: 24.07.2016 20:55:09) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_screenman.h>
#include <oc_screens_cfg.c>
#include <oc_driverman.h>
#include <oc_module.h>
#include <oc_list.h>
#include <oc_string.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_TYPES________________________________________________________________________________



#undef  _________________________________________LOCAL_TYPES________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_List(oC_Screen_t) Screens             = NULL;
static const char *         DefaultScreenName   = NULL;
static oC_Allocator_t       Allocator           = {
                .Name = "ScreenMan" ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS__________________________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup ScreenMan
 *
 * @brief turns on the screen manager module
 *
 * The function initializes the screen manager to work. It should be called by the system at boot time. The function creates screens and adds
 * it to the list. It does not configures drivers associated with the screens.
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ScreenMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode,oC_Module_ScreenMan))
    {
        Screens = oC_List_New(&Allocator,AllocationFlags_ZeroFill | AllocationFlags_CanWait1Second);

        if(ErrorCondition(Screens != NULL , oC_ErrorCode_AllocationError))
        {
            errorCode = oC_ErrorCode_None;

#define CREATE_SCREEN( NAME , DRIVER_NAME , CONFIG_NAME )           \
                        oC_Screen_t NAME = oC_Screen_New( &DRIVER_NAME , &CONFIG_NAME , #NAME );
            CFG_SCREENS_LIST(CREATE_SCREEN);
#undef CREATE_SCREEN

#define ADD_SCREEN( NAME , DRIVER_NAME , CONFIG_NAME )       \
                if(ErrorCondition( NAME != NULL , oC_ErrorCode_ObjectNotCorrect ))\
                {\
                    bool success = oC_List_PushBack(Screens,NAME,&Allocator);\
                    ErrorCondition(success, oC_ErrorCode_CannotAddObjectToList);\
                }
            CFG_SCREENS_LIST(ADD_SCREEN)
#undef ADD_SCREEN

            DefaultScreenName = CFG_STRING_DEFAULT_SCREEN;
            oC_Module_TurnOn(oC_Module_ScreenMan);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup ScreenMan
 *
 * @brief turns off screen manager
 *
 * The function is for turning off the screen manager. All screens will be destroyed.
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ScreenMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode,oC_Module_ScreenMan))
    {
        errorCode = oC_ErrorCode_None;

#define TAKE_SCREEN( NAME , DRIVER_NAME , CONFIG_NAME )       \
                        oC_Screen_t NAME = oC_ScreenMan_GetScreen( #NAME ); \
                        oC_List_RemoveAll(Screens,NAME);
        CFG_SCREENS_LIST(TAKE_SCREEN);
#undef TAKE_SCREEN

#define DELETE_SCREEN(NAME , DRIVER_NAME , CONFIG_NAME )    \
                        oC_Screen_Delete(&NAME);
        CFG_SCREENS_LIST(DELETE_SCREEN);
#undef DELETE_SCREEN

        ErrorCondition( oC_List_Delete(Screens,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);

        oC_Module_TurnOff(oC_Module_ScreenMan);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures all screens
 *
 * The function is for configuration of all not configured screens. The function checks if the associated graphic driver is turned on and the
 * screen is not configured. If both conditions are true, then it configures the screen driver. Each error during these operations is logged
 * into the system errors stack.
 *
 */
//==========================================================================================================================================
void oC_ScreenMan_ConfigureAll( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) )
{
#define PrintWait(...)      if( PrintWaitMessage != NULL ) PrintWaitMessage( __VA_ARGS__ )
#define PrintResult( err )  if( PrintResult      != NULL ) PrintResult( err )

    if( oC_Module_IsTurnedOn( oC_Module_ScreenMan )
        && ( PrintWaitMessage == NULL || isaddresscorrect(PrintWaitMessage)    )
        && ( PrintResult == NULL      || isaddresscorrect(PrintResult)         )
        )
    {
        oC_ErrorCode_t errorCode = oC_ErrorCode_None;

        oC_List_Foreach(Screens,screen)
        {
            if(oC_Screen_IsConfigured(screen) == false && oC_Driver_IsTurnedOn(oC_Screen_GetDriver(screen)) == true)
            {
                PrintWait( "Configuration of screen %s", oC_Screen_GetName(screen) );
                errorCode = oC_Screen_Configure(screen);
                PrintResult( errorCode );

                oC_SaveIfErrorOccur( oC_Screen_GetName(screen) , errorCode );
            }
        }
    }

#undef PrintWait
#undef PrintResult
}

//==========================================================================================================================================
/**
 * @brief unconfigures all screens
 *
 * The function is for deconfiguration of all configured screens. Each error that occurs during screen unconfiguration is logged in to the system
 * errors stack.
 *
 */
//==========================================================================================================================================
void oC_ScreenMan_UnconfigureAll( void )
{
    if(oC_Module_IsTurnedOn(oC_Module_ScreenMan))
    {
        oC_List_Foreach(Screens,screen)
        {
            if(oC_Screen_IsConfigured(screen) == true)
            {
                oC_SaveIfErrorOccur(oC_Screen_GetName(screen) , oC_Screen_Unconfigure(screen));
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @ingroup ScreenMan
 *
 * @brief returns default screen
 *
 * The function returns screen that is set as default screen. If the default screen is not set or it does not exist, the function will return
 * NULL pointer.
 *
 * @return pointer to the default screen or NULL in case of error
 */
//==========================================================================================================================================
oC_Screen_t oC_ScreenMan_GetDefaultScreen( void )
{
    oC_Screen_t screen = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ScreenMan))
    {
        screen = oC_ScreenMan_GetScreen(DefaultScreenName);
    }

    return screen;
}

//==========================================================================================================================================
/**
 * @ingroup ScreenMan
 *
 * @brief returns screens list
 *
 * The function returns list of screens available in the system. It also can return NULL if the module did not start yet.
 *
 * @warning
 * This function returns real list of the screens (IT IS NOT A COPY!!!) You can read it, but don't change anything in this list - otherwise it may
 * cause undefined behavior.
 *
 * @return List of screens
 */
//==========================================================================================================================================
oC_List(oC_Screen_t) oC_ScreenMan_GetList( void )
{
    return Screens;
}

//==========================================================================================================================================
/**
 * @ingroup ScreenMan
 *
 * @brief returns selected screen
 *
 * The function looking for a screen that matches the given name. If none of screens matches, the function returns NULL.
 *
 * @param Name      Name of the screen to find
 *
 * @return screen object or NULL if not found
 */
//==========================================================================================================================================
oC_Screen_t oC_ScreenMan_GetScreen( const char * Name )
{
    oC_Screen_t defaultScreen = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ScreenMan))
    {
        oC_List_Foreach(Screens,screen)
        {
            if(strcmp(oC_Screen_GetName(screen),Name) == 0)
            {
                defaultScreen = screen;
            }
        }
    }

    return defaultScreen;
}

//==========================================================================================================================================
/**
 * @ingroup ScreenMan
 *
 * @brief adds screen to the screens list
 *
 * The function is for adding the screen to the screen manager list. Screen has to be correct and the screen manager module must be turned on.
 *
 * @param Screen        Correct screen to add
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ScreenMan_AddScreen( oC_Screen_t Screen )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ScreenMan))
    {
        if(ErrorCondition(oC_Screen_IsCorrect(Screen) , oC_ErrorCode_ObjectNotCorrect))
        {
            bool pushed = oC_List_PushBack(Screens,Screen,&Allocator);

            if(ErrorCondition(pushed,oC_ErrorCode_CannotAddObjectToList))
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup ScreenMan
 *
 * @brief removes screen from the screens list
 *
 * The function is for removing the screen from the screen manager list. Screen has to be correct and the screen manager module must be turned on.
 *
 * @param Screen        Correct screen to remove
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ScreenMan_RemoveScreen( oC_Screen_t Screen )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ScreenMan))
    {
        if(ErrorCondition(oC_Screen_IsCorrect(Screen) , oC_ErrorCode_ObjectNotCorrect))
        {
            bool removed = oC_List_RemoveAll(Screens,Screen);

            if(ErrorCondition(removed,oC_ErrorCode_CannotRemoveObjectFromList))
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS__________________________________________________________________________________


