/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_tgui.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_tgui.h>
#include <oc_stdlib.h>
#include <oc_null.h>
#include <oc_object.h>
#include <oc_stdio.h>
#include <oc_kprint.h>
#include <string.h>
#include <oc_math.h>
#include <ctype.h>
#include <oc_struct.h>
#include <oc_string.h>
#include <oc_debug.h>
#include <oc_process.h>
#include <oc_string.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct ProgressBar_t
{
    oC_ObjectControl_t                  ObjectControl;
    oC_TGUI_Position_t            StartPosition;
    oC_TGUI_Column_t                    Width;
    oC_TGUI_ProgressBarStyle_t          Style;
    uint32_t                            MaximumValue;
    uint32_t                            CurrentValue;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

static uint32_t                 GetIndexFromPosition    ( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t BottomRight , oC_TGUI_Position_t Position , const char * Buffer );
static oC_TGUI_Position_t       GetPositionFromIndex    ( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t BottomRight , const char * Buffer , uint32_t Index );
static oC_TGUI_ActiveObject_t*  GetActiveObjectOnRight  ( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * ActiveObject );
static oC_TGUI_ActiveObject_t*  GetActiveObjectOnLeft   ( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * ActiveObject );
static oC_TGUI_ActiveObject_t*  GetActiveObjectOnDown   ( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * OldObject );
static oC_TGUI_ActiveObject_t*  GetActiveObjectOnUp     ( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * OldObject );

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Color_t oC_TGUI_GetColorFromName( const char * Name )
{
    oC_TGUI_Color_t color      = 0;
    int             nameLength = strlen(Name);

    oC_ARRAY_FOREACH_IN_ARRAY(oC_TGUI_Colors , colorData)
    {
        int  tempLength = strlen(colorData->Name);
        bool theSame    = tempLength > 0 && nameLength > 0;

        for(int i = 0; i < tempLength && i < nameLength && theSame; i++)
        {
            if(isalnum((int)colorData->Name[i]) && isalnum((int)Name[i]))
            {
                if(tolower( (int)colorData->Name[i]) != tolower((int)Name[i]))
                {
                    theSame = false;
                    break;
                }
            }
            else
            {
                break;
            }
        }
    }

    return color;
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_TGUI_GetNameFromColor( oC_TGUI_Color_t Color )
{
    const char * name = "unknown";

    oC_ARRAY_FOREACH_IN_ARRAY(oC_TGUI_Colors , colorData)
    {
        if(colorData->Color == Color)
        {
            name = colorData->Name;
            break;
        }
    }

    return name;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_ClearScreen( void )
{
    return printf(oC_VT100_ERASE_SCREEN) == 0;
}

//==========================================================================================================================================
/**
 * @brief function for reseting a terminal
 *
 * The function is for reseting terminal device. It restores default text attributes, background and foreground colors, and sets the cursor
 * position to the top left corner.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TGUI_ResetDevice( void )
{
    return printf(oC_VT100_RESET_DEVICE) == 0;
}
//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SetCursorPosition( oC_TGUI_Position_t CursorPosition )
{
    return oC_TGUI_Position_IsCorrect(CursorPosition) && printf(oC_VT100_SET_CURSOR_HOME("%d","%d"),CursorPosition.Line,CursorPosition.Column) == 0;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_ReadCursorPosition( oC_TGUI_Position_t * outCursorPosition )
{
    bool success = false;

    if(isram(outCursorPosition))
    {
        success = true;
        printf(oC_VT100_SEQ_QUERY_CURSOR_POSITION);
        scanf(oC_VT100_SEQ_REPORT_CURSOR_POSITION("%d","%d") , (int*)&outCursorPosition->Line , (int*)&outCursorPosition->Column);
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SetBackgroundColor( oC_TGUI_Color_t Color )
{
    bool      success       = false;
    uint16_t  valueToPrint  = 10;

    if(oC_TGUI_Color_IsCorrect(Color))
    {
        valueToPrint += Color;
        success       = printf("\033[%dm" , valueToPrint) == 0;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SetForegroundColor( oC_TGUI_Color_t Color )
{
    bool      success       = false;
    uint16_t  valueToPrint  = 0;

    if(oC_TGUI_Color_IsCorrect(Color))
    {

        valueToPrint += Color;
        success       = printf("\033[%dm" , valueToPrint) == 0;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SetTextStyle( oC_TGUI_TextStyle_t TextStyle )
{
    return printf(
                "%s%s%s%s%s%s" ,
                (TextStyle & oC_TGUI_TextStyle_Bold         ) ? "\033[1m" : "\033[21m" ,
                (TextStyle & oC_TGUI_TextStyle_Dim          ) ? "\033[2m" : "\033[22m" ,
                (TextStyle & oC_TGUI_TextStyle_Underline    ) ? "\033[4m" : "\033[24m" ,
                (TextStyle & oC_TGUI_TextStyle_Blink        ) ? "\033[5m" : "\033[25m" ,
                (TextStyle & oC_TGUI_TextStyle_Inverted     ) ? "\033[7m" : "\033[27m" ,
                (TextStyle & oC_TGUI_TextStyle_Hidden       ) ? "\033[8m" : "\033[28m"
           ) == 0;
}


//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SetStyle( const oC_TGUI_Style_t * Style )
{
    return isaddresscorrect(Style) && oC_TGUI_SetForegroundColor(Style->Foreground) && oC_TGUI_SetBackgroundColor(Style->Background) && oC_TGUI_SetTextStyle(Style->TextStyle);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SaveAttributes( void )
{
    return printf(oC_VT100_SAVE_CURSOR_AND_ATTRS) == 0;
}
//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_RestoreAttributes ( void )
{
    return printf(oC_VT100_RESTORE_CURSOR_AND_ATTRS) == 0;
}

//==========================================================================================================================================
//==========================================================================================================================================
int oC_TGUI_Position_Compare( oC_TGUI_Position_t P1 , oC_TGUI_Position_t P2)
{
    int result = 0;

    if( P1.Line < P2.Line )
    {
        result = -1;
    }
    else if( P1.Line > P2.Line )
    {
        result = 1;
    }
    else
    {
        result = (P1.Column < P2.Column ) ? -1 : (int)(P1.Column - P2.Column);
    }

    return result;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Position_t oC_TGUI_Position_Increment( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t BottomRight , oC_TGUI_Position_t Position )
{
    if(Position.Column < BottomRight.Column)
    {
        Position.Column++;
    }
    else
    {
        if(Position.Line < BottomRight.Line)
        {
            Position.Column = TopLeft.Column;
            Position.Line++;
        }
        else
        {
            Position.Column = BottomRight.Column;
            Position.Line   = BottomRight.Line;
        }
    }

    return Position;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Position_t oC_TGUI_Position_Decrement( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t BottomRight , oC_TGUI_Position_t Position )
{
    if(Position.Column > TopLeft.Column)
    {
        Position.Column--;
    }
    else
    {
        if(Position.Line > TopLeft.Line)
        {
            Position.Line--;
            Position.Column = BottomRight.Column;
        }
        else
        {
            Position.Column = TopLeft.Column;
            Position.Line   = TopLeft.Line;
        }
    }
    return Position;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SetFont( oC_TGUI_Font_t Font )
{
    bool success = false;

    if(Font == oC_TGUI_Font_G0)
    {
        success = printf(oC_VT100_FONT_SET_G0) == oC_ErrorCode_None;
    }
    else if(Font == oC_TGUI_Font_G1)
    {
        success = printf(oC_VT100_FONT_SET_G1) == oC_ErrorCode_None;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_SetLineWrapping( bool Enabled )
{
    bool success = false;

    if(Enabled)
    {
        success = printf(oC_VT100_ENABLE_LINE_WRAP) == oC_ErrorCode_None;
    }
    else
    {
        success = printf(oC_VT100_DISABLE_LINE_WRAP) == oC_ErrorCode_None;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DoesSupportRealLines( void )
{
    bool supported = false;

    supported = printf(oC_VT100_DETECT_DL_SUPPORT) == oC_VT100_DL_SUPPORTED_VALUE;

    return supported;
}


//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawRealLine( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition )
{
    bool  drawed = false;

    drawed = printf( oC_VT100_DRAW_LINE("%d","%d","%d","%d"), StartPosition.Column, StartPosition.Line, EndPosition.Column, EndPosition.Line);

    return drawed;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawLine( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition )
{
    bool                        success  = false;
    oC_TGUI_Position_t    position = {0};

    if(oC_TGUI_Position_IsCorrect(StartPosition) && oC_TGUI_Position_IsCorrect(EndPosition))
    {
        if(StartPosition.Line == EndPosition.Line && StartPosition.Column < EndPosition.Column)
        {
            success  = true;
            position = StartPosition;

            while(position.Column < EndPosition.Column && success)
            {
                success = oC_TGUI_DrawAtPosition(position,oC_VT100_BORDER_LR);
                position.Column++;
            }
        }
        else if(StartPosition.Column == EndPosition.Column && StartPosition.Line < EndPosition.Line)
        {
            success  = true;
            position = StartPosition;

            while(position.Line < EndPosition.Line && success)
            {
                success = oC_TGUI_DrawAtPosition(position,oC_VT100_BORDER_UD);
                position.Line++;
            }
        }
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawBorder( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t BottomRight )
{
    bool success = false;

    if(TopLeft.Column < BottomRight.Column          &&
       TopLeft.Line   < BottomRight.Line            &&
       oC_TGUI_Position_IsCorrect(TopLeft)    &&
       oC_TGUI_Position_IsCorrect(BottomRight) )
    {
        oC_TGUI_Position_t topRight   = { .Line = TopLeft.Line     , .Column = BottomRight.Column };
        oC_TGUI_Position_t bottomLeft = { .Line = BottomRight.Line , .Column = TopLeft.Column     };

        success = oC_TGUI_DrawLine( TopLeft   ,topRight    ) &&
                  oC_TGUI_DrawLine( bottomLeft,BottomRight ) &&
                  oC_TGUI_DrawLine( TopLeft   ,bottomLeft  ) &&
                  oC_TGUI_DrawLine( topRight  ,BottomRight ) &&
                  oC_TGUI_DrawAtPosition( TopLeft,     oC_VT100_BORDER_DR ) &&
                  oC_TGUI_DrawAtPosition( topRight,    oC_VT100_BORDER_LD ) &&
                  oC_TGUI_DrawAtPosition( BottomRight, oC_VT100_BORDER_LU ) &&
                  oC_TGUI_DrawAtPosition( bottomLeft,  oC_VT100_BORDER_UR );
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawAtPosition( oC_TGUI_Position_t StartPosition , const char * String )
{
    return oC_TGUI_SetCursorPosition(StartPosition) && oC_KPrint_WriteToStdOut(oC_IoFlags_Default , String , strlen(String)) == 0;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawAtPositionWithSize ( oC_TGUI_Position_t StartPosition , const char * String , int Size )
{
    bool success = false;

    if(oC_TGUI_SetCursorPosition(StartPosition))
    {
        int len = strlen(String);

        oC_TGUI_DrawNCharAtPosition(StartPosition,' ',Size);
        oC_TGUI_SetCursorPosition(StartPosition);
        oC_KPrint_WriteToStdOut(oC_IoFlags_Default , String , oC_MIN(len,Size));
        success = true;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawCharAtPosition( oC_TGUI_Position_t StartPosition , char C )
{
    return oC_TGUI_SetCursorPosition(StartPosition) && oC_KPrint_WriteToStdOut(oC_IoFlags_Default , &C , 1) == oC_ErrorCode_None;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawNCharAtPosition( oC_TGUI_Position_t StartPosition , char C , int N )
{
    bool success = oC_TGUI_SetCursorPosition(StartPosition);

    for(int i = 0; i < N && success ; i++)
    {
        success = oC_KPrint_WriteToStdOut(oC_IoFlags_Default , &C , 1) == oC_ErrorCode_None;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawFillBetween( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition , char C )
{
    bool success = false;

    if(oC_TGUI_Position_IsCorrect(StartPosition) && oC_TGUI_Position_IsCorrect(EndPosition) &&
       StartPosition.Line   <= EndPosition.Line   &&
       StartPosition.Column <= EndPosition.Column )
    {
        oC_TGUI_Position_t position = StartPosition;
        oC_TGUI_Column_t   width    = EndPosition.Column - StartPosition.Column + 1;

        success = true;

        while(position.Line <= EndPosition.Line && success)
        {
            success = oC_TGUI_DrawNCharAtPosition(position,C,width);
            position.Line++;
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief clears selected part of the screen
 *
 * The function is for quickly clear part of the screen by using the given background color.
 *
 * @param StartPosition     Position of the top left corner to clear
 * @param Width             Width of part of screen to clear
 * @param Height            Height of part of screen to clear
 * @param Color             Background color to use for clearing
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TGUI_ClearPartOfScreen( oC_TGUI_Position_t StartPosition , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , oC_TGUI_Color_t Color )
{
    bool success = false;

    if(oC_TGUI_Position_IsCorrect(StartPosition) && Width > 0 && Height > 0)
    {
        oC_TGUI_Position_t endPosition = { .Column = StartPosition.Column + Width , .Line = StartPosition.Line + Height };
        success = oC_TGUI_SetBackgroundColor(Color) &&
                  oC_TGUI_DrawFillBetween(StartPosition,endPosition,' ');

    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawMultiLineText( oC_TGUI_Position_t StartPosition , oC_TGUI_Position_t EndPosition , const char * String , bool AsPassword )
{
    bool success = false;

    if(oC_TGUI_Position_IsCorrect(StartPosition) && oC_TGUI_Position_IsCorrect(EndPosition) && isaddresscorrect(String))
    {
        int                      stringLength = strlen(String);
        oC_TGUI_Position_t position     = StartPosition;

        success = oC_TGUI_SetCursorPosition(position);

        for(int characterIndex = 0 ; success && characterIndex < stringLength && position.Line <= EndPosition.Line; characterIndex++)
        {
            if(String[characterIndex] == '\r')
            {
                continue;
            }
            else if(String[characterIndex] == '\t')
            {
                success         = oC_KPrint_WriteToStdOut(oC_IoFlags_Default,"  ",2) == oC_ErrorCode_None;
                position.Column+= 2;

            }
            else if( String[characterIndex] == '\n' || position.Column > EndPosition.Column )
            {
                position.Column = StartPosition.Column;
                position.Line++;
                success         = oC_TGUI_SetCursorPosition(position);
                if(String[characterIndex] != '\n')
                {
                    characterIndex--;
                }
            }
            else
            {
                success         = oC_KPrint_WriteToStdOut(oC_IoFlags_Default, AsPassword ? "*" : &String[characterIndex],1) == oC_ErrorCode_None;
                position.Column++;
            }
        }
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawFormatAtPosition( oC_TGUI_Position_t StartPosition , const char * Format , ... )
{
    bool            success     = false;
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;

    if(oC_TGUI_Position_IsCorrect(StartPosition))
    {
        if(oC_TGUI_SetCursorPosition(StartPosition))
        {
            va_list argumentList;
            va_start(argumentList, Format);
            errorCode = vprintf(Format,argumentList);
            va_end(argumentList);
            success = oC_ErrorOccur(errorCode) == false;
        }
    }
    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_FindPositionInBuffer( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t * Position ,oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const char * String , uint32_t * outIndex )
{
    bool found = false;

    if(oC_TGUI_Position_IsCorrect(TopLeft) && isram(Position) && Width > 0 && Height > 0 && isaddresscorrect(String) && isram(outIndex))
    {
        oC_TGUI_Position_t bottomRight = { .Column = TopLeft.Column + Width , .Line = TopLeft.Line + Height };
        oC_TGUI_Position_t position    = TopLeft;
        uint32_t                 strLength   = strlen(String);
        uint32_t                 signIndex   = 0;

        for(signIndex = 0; signIndex < strLength && position.Line < bottomRight.Line ; signIndex++)
        {
            if(position.Column == Position->Column && position.Line == Position->Line)
            {
                *outIndex = signIndex;
                found     = true;
                break;
            }

            if(String[signIndex] == '\n')
            {
                if(position.Line == Position->Line)
                {
                    break;
                }
                position.Line++;
            }
            else if(String[signIndex] == '\r')
            {
                position.Column = TopLeft.Column;
            }
            else
            {
                position.Column++;
            }

            if(position.Column >= bottomRight.Column)
            {
                position.Column = TopLeft.Column;
                position.Line++;
            }
        }

        if(!found)
        {
            Position->Column = position.Column;
            Position->Line   = position.Line;
            *outIndex        = signIndex;
        }
    }

    return found;
}

//==========================================================================================================================================
//==========================================================================================================================================
void oC_TGUI_DrawSpiner( oC_TGUI_Spiner_t * outSpiner )
{
    if(isram(outSpiner))
    {
        static const char string[] = "/-\\|";
        if(outSpiner->SignIndex >= (sizeof(string)-1))
        {
            outSpiner->SignIndex = 0;
        }
        oC_KPrint_WriteToStdOut(oC_Process_GetIoFlags(getcurprocess()),&string[outSpiner->SignIndex],1);
        outSpiner->SignIndex++;
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawProgressBar( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , uint32_t CurrentValue , uint32_t MaximumValue , const oC_TGUI_ProgressBarStyle_t * Style )
{
    bool success = false;

    if( oC_TGUI_Position_IsCorrect(Position) && Width > 8 && MaximumValue > 0 && CurrentValue <= MaximumValue && isaddresscorrect(Style) )
    {
        oC_TGUI_Position_t topLeft         = Position;
        oC_TGUI_Position_t bottomRight     = Position;
        oC_TGUI_Position_t barPosition     = Position;
        oC_TGUI_Column_t         endColumn       = 0;
        oC_TGUI_Column_t         barWidth        = 0;
        double                    loadedRatio    = ((double)CurrentValue) / ((double)MaximumValue);
        oC_TGUI_Column_t         loadedBarWidth  = 0;
        oC_TGUI_Column_t         loadedEndColumn = 0;
        oC_TGUI_Column_t         stringBarColumn = 0;
        oC_TGUI_Column_t         stringBarEndColumn = 0;
        char                     percentString[10];
        char *                   stringReference = percentString;

        bottomRight.Line   += 2;
        bottomRight.Column += Width;

        barPosition.Line   = topLeft.Line   + 1;
        barPosition.Column = topLeft.Column + 1;

        endColumn       = barPosition.Column + Width - 1;
        barWidth        = Width - 2;
        loadedBarWidth  = (oC_TGUI_Column_t)(loadedRatio * ((double)barWidth));
        loadedEndColumn = barPosition.Column + loadedBarWidth;
        stringBarColumn = barPosition.Column + (barWidth / 2);

        oC_TGUI_SaveAttributes();
        success         = true;

        memset(percentString,0,sizeof(percentString));

        sprintf_s(percentString,sizeof(percentString),"%5.1f%%" , loadedRatio * ((double)100));

        stringBarColumn   -= strlen(percentString) / 2;
        stringBarEndColumn = stringBarColumn + strlen(percentString);

        if(Style->BorderStyle.DontDraw != true)
        {
            success = success && oC_TGUI_SetStyle(&Style->BorderStyle) &&
                                 oC_TGUI_DrawBorder(topLeft,bottomRight);
        }

        oC_TGUI_SetTextStyle(oC_TGUI_TextStyle_Default);

        while(success && barPosition.Column < endColumn)
        {
            if(barPosition.Column >= loadedEndColumn)
            {
                success = oC_TGUI_SetBackgroundColor(Style->NonActiveColor) &&
                          oC_TGUI_SetForegroundColor(Style->ActiveColor);
            }
            else
            {
                success = oC_TGUI_SetBackgroundColor(Style->ActiveColor) &&
                          oC_TGUI_SetForegroundColor(Style->NonActiveColor);
            }
            char c  = ' ';

            if(barPosition.Column >= stringBarColumn && barPosition.Column < stringBarEndColumn)
            {
                c = *stringReference;
                stringReference++;
            }

            success = success && oC_TGUI_DrawCharAtPosition(barPosition,c);

            barPosition.Column++;
        }

        oC_TGUI_RestoreAttributes();
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawTextBox( const char * String , oC_TGUI_Position_t Position , oC_TGUI_Column_t FixedWidth , const oC_TGUI_TextBoxStyle_t * Style )
{
    bool success = false;

    if(isaddresscorrect(String) && isaddresscorrect(Style) && oC_TGUI_Position_IsCorrect(Position))
    {
        int stringLength                        = strlen(String);
        int stringToPrintLength                 = 0;
        oC_TGUI_Position_t topLeft        = Position;
        oC_TGUI_Position_t topRight       = Position;
        oC_TGUI_Position_t bottomLeft     = Position;
        oC_TGUI_Position_t bottomRight    = Position;
        oC_TGUI_Position_t stringPosition = {0};

        oC_TGUI_SaveAttributes();

        FixedWidth               = (FixedWidth == 0) ? stringLength + 2 : FixedWidth;
        stringToPrintLength      = FixedWidth - 2;

        topRight.Column         += FixedWidth;
        bottomLeft.Line         += 2;
        bottomRight.Line         = bottomLeft.Line;
        bottomRight.Column       = topRight.Column;

        stringPosition.Line      = topLeft.Line   + 1;
        stringPosition.Column    = topLeft.Column + 1;

        success = true;
        if(Style->BorderStyle.DontDraw != true)
        {
            success = oC_TGUI_SetStyle(&Style->BorderStyle) &&
                      oC_TGUI_DrawBorder(topLeft,bottomRight);
        }
        if(Style->TextStyle.DontDraw != true)
        {
            success = oC_TGUI_SetStyle(&Style->TextStyle) &&
                      oC_TGUI_DrawAtPositionWithSize(stringPosition , String , stringToPrintLength );
        }

        oC_TGUI_RestoreAttributes();
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief draws a box
 *
 * The function is for drawing a box - it is a rectangle with title, text inside and shadow. The example you can see below:
 *
 * ![Box Example](box_tgui_example.png)
 *
 * @param Title         Pointer to the string with title
 * @param TextInside    Pointer to the string with text that should be placed in the box, or NULL if it is not required
 * @param TopLeft       Position of the top left box corner
 * @param Width         Width of the box (including 1 column for the shadow)
 * @param Height        Height of the box (including 1 line for the shadow)
 * @param Style         Pointer to the style of the box.
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TGUI_DrawBox( const char * Title  , const char * TextInside , oC_TGUI_Position_t TopLeft , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const oC_TGUI_BoxStyle_t * Style )
{
    bool success = false;

    if(
         isaddresscorrect(Style) &&
        (isaddresscorrect(Title) || Style->TitleStyle.DontDraw  ) &&
        oC_TGUI_Position_IsCorrect(TopLeft))
    {
        oC_TGUI_Position_t bottomRight    = TopLeft;
        oC_TGUI_Position_t stringPosition = TopLeft;
        oC_TGUI_Position_t shadowRightStart;
        oC_TGUI_Position_t shadowRightEnd;
        oC_TGUI_Position_t shadowBottomStart;
        oC_TGUI_Position_t shadowBottomEnd;
        oC_TGUI_Position_t insideTopLeft;
        oC_TGUI_Position_t insideBottomRight;

        bottomRight.Column += Width  - 2;
        bottomRight.Line   += Height - 2;

        stringPosition.Column   += (Width / 2) - (strlen(Title) / 2);

        shadowRightStart.Column = bottomRight.Column + 1;
        shadowRightStart.Line   = TopLeft.Line + 1;
        shadowRightEnd.Column   = shadowRightStart.Column;
        shadowRightEnd.Line     = bottomRight.Line + 1;

        shadowBottomStart.Column= TopLeft.Column   + 1;
        shadowBottomStart.Line  = bottomRight.Line + 1;
        shadowBottomEnd.Column  = shadowRightEnd.Column;
        shadowBottomEnd.Line    = shadowBottomStart.Line;

        insideTopLeft.Column    = TopLeft.Column + 1;
        insideTopLeft.Line      = TopLeft.Line   + 1;
        insideBottomRight.Column= bottomRight.Column - 1;
        insideBottomRight.Line  = bottomRight.Line   - 1;

        oC_TGUI_SaveAttributes();

        success = true;

        if(Style->BorderStyle.DontDraw != true)
        {
            success = success && oC_TGUI_SetStyle(&Style->BorderStyle) &&
                                 oC_TGUI_DrawBorder(TopLeft,bottomRight);
        }

        if(Style->TitleStyle.DontDraw != true)
        {
            success = success && oC_TGUI_SetStyle(&Style->TitleStyle) &&
                                 oC_TGUI_DrawAtPosition(stringPosition,Title);
        }

        if(Style->ShadowStyle.DontDraw != true)
        {
            success = success && oC_TGUI_SetBackgroundColor(Style->ShadowStyle.Background) &&
                                 oC_TGUI_DrawFillBetween(shadowRightStart,shadowRightEnd,' ') &&
                                 oC_TGUI_DrawFillBetween(shadowBottomStart,shadowBottomEnd,' ');
        }

        if(Style->InsideStyle.DontDraw != true)
        {
            success = success && oC_TGUI_SetStyle(&Style->InsideStyle) &&
                                 oC_TGUI_DrawFillBetween(insideTopLeft,insideBottomRight,' ');

            if(isaddresscorrect(TextInside))
            {
                success = success && oC_TGUI_DrawMultiLineText(insideTopLeft,insideBottomRight,TextInside,false);
            }
        }

        oC_TGUI_RestoreAttributes();
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawProperties( const oC_TGUI_DrawPropertyConfig_t * Config )
{
    bool success = false;

    if(
        isaddresscorrect(Config)                                &&
        isaddresscorrect(Config->Style)                         &&
        isaddresscorrect(Config->Properties)                    &&
        oC_TGUI_Position_IsCorrect(Config->StartPosition) &&
        Config->Height             > 0                          &&
        Config->Width              > 0                          &&
        Config->DescriptionWidth   > 0                          &&
        Config->NumberOfProperties > 0                          &&
        Config->DescriptionWidth   < Config->Width
        )
    {
        oC_TGUI_EntryIndex_t     entryIndex                 = 0;
        oC_TGUI_Position_t position                   = Config->StartPosition;
        oC_TGUI_Position_t valuePosition              = Config->StartPosition;
        oC_TGUI_Position_t bottomRight                = { .Column = Config->StartPosition.Column + Config->Width , .Line = Config->StartPosition.Line + Config->Height };

        success = true;

        /* Drawing menu entries */
        for(position = Config->StartPosition , entryIndex = 0 ; position.Line < bottomRight.Line && success ; position.Line++ , entryIndex++)
        {
            if(entryIndex < Config->NumberOfProperties)
            {

                if(Config->Style->DescriptionStyle.DontDraw != true)
                {
                    success = success && oC_TGUI_SetStyle(&Config->Style->DescriptionStyle) &&
                                         oC_TGUI_DrawAtPositionWithSize(position,Config->Properties[entryIndex].Description,Config->DescriptionWidth);
                }

                if(Config->Style->ValueStyle.DontDraw != true)
                {
                    success = success &&oC_TGUI_SetStyle(&Config->Style->ValueStyle);

                    valuePosition         = position;
                    valuePosition.Column += Config->DescriptionWidth;

                    oC_TGUI_DrawNCharAtPosition(valuePosition,' ', Config->Width - Config->DescriptionWidth);

                    switch(Config->Properties[entryIndex].Type)
                    {
                        case oC_TGUI_ValueType_UINT:        success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueUINT);   break;
                        case oC_TGUI_ValueType_U32:         success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueU32);    break;
                        case oC_TGUI_ValueType_U64:         success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueU64);    break;
                        case oC_TGUI_ValueType_I32:         success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueI32);    break;
                        case oC_TGUI_ValueType_I64:         success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueI64);    break;
                        case oC_TGUI_ValueType_ValueFloat:  success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueFloat);  break;
                        case oC_TGUI_ValueType_ValueDouble: success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueDouble); break;
                        case oC_TGUI_ValueType_ValueChar:   success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueChar);   break;
                        case oC_TGUI_ValueType_ValueString: success = success && oC_TGUI_DrawFormatAtPosition(valuePosition,Config->Properties[entryIndex].Format,Config->Properties[entryIndex].ValueString); break;
                        default:
                            oC_TGUI_DrawAtPositionWithSize(valuePosition,"INCORRECT TYPE!",Config->Width - Config->DescriptionWidth);
                            break;
                    }
                }
            }
            else
            {
                oC_TGUI_SetStyle(&Config->Style->DescriptionStyle);
                oC_TGUI_DrawNCharAtPosition(position,' ', Config->Width);
            }
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief draws a menu
 *
 * The function is for drawing menu. It returns index of the lately selected entry or -1 if there was an error. The function ends, when the
 * selected item has not assigned a correct Handler. If the selected item has a valid Handler pointer, the function calls it and waits for
 * the press of the key. You can use it for example as the **Exit** menu entry.
 *
 * @note The ESCAPE key closes the menu.
 *
 * @param TopLeft           Position of the top left menu corner
 * @param Width             Width including shadow
 * @param Height            Height including shadow
 * @param MenuEntries       Array with menu entries
 * @param NumberOfEntries   Number of menu entries in the array
 * @param Style             Pointer to the menu style
 *
 * @return
 * Index of the selected menu entry.
 */
//==========================================================================================================================================
oC_TGUI_EntryIndex_t oC_TGUI_DrawMenu( oC_TGUI_Position_t TopLeft , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const oC_TGUI_MenuEntry_t * MenuEntries , oC_TGUI_EntryIndex_t NumberOfEntries , const oC_TGUI_MenuStyle_t * Style )
{
    oC_TGUI_EntryIndex_t selectedIndex = -1;

    if(oC_TGUI_Position_IsCorrect(TopLeft) && Width > 0 && Height > 0 && isaddresscorrect(MenuEntries) && NumberOfEntries > 0 && isaddresscorrect(Style))
    {
        oC_TGUI_Position_t bottomRight = { .Column = TopLeft.Column + Width , .Line = TopLeft.Line + Height };
        bool success = true;

        if(Style->Border.DontDraw != true)
        {
            success = success && oC_TGUI_SetStyle(&Style->Border) &&
                                 oC_TGUI_DrawBorder(TopLeft,bottomRight);
            TopLeft.Column++;
            TopLeft.Line++;
            bottomRight.Column--;
            bottomRight.Line--;
            Width  -= 2;
            Height -= 2;
        }

        if(success)
        {
            oC_TGUI_Key_t            key                     = 0;
            oC_TGUI_EntryIndex_t     entryIndex              = 0;
            oC_TGUI_EntryIndex_t     lastIndex               = NumberOfEntries - 1;
            oC_TGUI_EntryIndex_t     displayedStartIndex     = 0;
            oC_TGUI_EntryIndex_t     displayedLastIndex      = lastIndex;
            oC_TGUI_EntryIndex_t     numberOfEntriesOnScreen = oC_MIN(NumberOfEntries,Height);
            oC_TGUI_Position_t position                = TopLeft;

            selectedIndex = 0;

            do
            {
                if(key == oC_TGUI_Key_ArrowDown)
                {
                    if(selectedIndex < lastIndex)
                    {
                        if(   selectedIndex           == displayedLastIndex
                           && numberOfEntriesOnScreen <  NumberOfEntries
                           && displayedLastIndex      <  lastIndex
                           )
                        {
                            displayedStartIndex++;
                            displayedLastIndex++;
                        }
                        selectedIndex++;
                    }
                    else
                    {
                        selectedIndex       = 0;
                        displayedStartIndex = 0;
                        displayedLastIndex  = numberOfEntriesOnScreen;
                    }
                }
                else if(key == oC_TGUI_Key_ArrowUp)
                {
                    if(selectedIndex > 0)
                    {
                        if(   selectedIndex           == displayedStartIndex
                           && numberOfEntriesOnScreen <  NumberOfEntries
                           && displayedStartIndex     >  0
                           )
                        {
                                displayedStartIndex--;
                                displayedLastIndex--;
                        }
                        selectedIndex--;
                    }
                    else
                    {
                        selectedIndex       = lastIndex;
                        displayedLastIndex  = lastIndex;
                        displayedStartIndex = lastIndex - numberOfEntriesOnScreen + 1;
                    }
                }
                else if(key == oC_TGUI_Key_Enter)
                {
                    if(selectedIndex >= 0 && selectedIndex < NumberOfEntries)
                    {
                        if(isaddresscorrect(MenuEntries[selectedIndex].Handler))
                        {
                            MenuEntries[selectedIndex].Handler(MenuEntries[selectedIndex].Parameter);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else if(key == oC_TGUI_Key_ESC)
                {
                    selectedIndex = NumberOfEntries;
                    break;
                }

                /* Drawing menu entries */
                for(position = TopLeft , entryIndex = displayedStartIndex ; position.Line <= bottomRight.Line; position.Line++ , entryIndex++)
                {
                    if(entryIndex < NumberOfEntries)
                    {
                        if(entryIndex == selectedIndex)
                        {
                            oC_TGUI_SetStyle(&Style->ActiveEntry);

                            if(isaddresscorrect(MenuEntries[entryIndex].OnHoverHandler))
                            {
                                MenuEntries[selectedIndex].OnHoverHandler(MenuEntries[selectedIndex].OnHoverParameter);
                            }
                        }
                        else
                        {
                            oC_TGUI_SetStyle(&Style->NotActiveEntry);
                        }

                        oC_TGUI_DrawAtPositionWithSize(position,MenuEntries[entryIndex].Title,Width);
                    }
                    else
                    {
                        oC_TGUI_SetStyle(&Style->NotActiveEntry);
                        oC_TGUI_DrawNCharAtPosition(position,' ',Width);
                    }
                }
            }
            while(oC_TGUI_WaitForKeyPress(&key));
        }
    }

    return selectedIndex;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_DrawListMenu( const oC_TGUI_DrawListMenuConfig_t * Config )
{
    bool success = false;

    if(
        isaddresscorrect(Config)                                 &&
        oC_TGUI_Position_IsCorrect(Config->TopLeft)        &&
        Config->Width > 0                                        &&
        Config->Height> 0                                        &&
        oC_List_IsCorrect(Config->List)                          &&
        isaddresscorrect(Config->DrawHandler)                    &&
        isaddresscorrect(Config->Style)
        )
    {
        oC_TGUI_Column_t            width          = Config->Width;
        oC_TGUI_Line_t              height         = Config->Height;
        oC_TGUI_Position_t    topLeft        = Config->TopLeft;
        oC_TGUI_Position_t    bottomRight    = { .Column = topLeft.Column + width , .Line = topLeft.Line + height };
        oC_TGUI_Position_t    position       = topLeft;
        oC_TGUI_Key_t               key            = 0;
        oC_List_ElementHandle_t     first          = oC_List_GetFirstElementHandle(Config->List);
        oC_List_ElementHandle_t     last           = oC_List_GetLastElementHandle(Config->List);
        uint32_t                    elementsCount  = oC_List_Count(Config->List);
        oC_List_ElementHandle_t     firstToDisplay = first;
        oC_List_ElementHandle_t     lastToDisplay  = last;
        oC_List_ElementHandle_t     selected       = first;
        oC_List_ElementHandle_t     element        = first;
        uint32_t                    onScreenCount  = 0;

        success = true;

        if(Config->Style->Border.DontDraw != true)
        {
            success = success && oC_TGUI_SetStyle(&Config->Style->Border) &&
                                 oC_TGUI_DrawBorder(topLeft,bottomRight);
            topLeft.Column++;
            topLeft.Line++;
            bottomRight.Column--;
            bottomRight.Line--;
            width-=2;
            height-=2;
        }

        onScreenCount = height;

        if(onScreenCount < elementsCount)
        {
            lastToDisplay = List_ElementOfIndex(Config->List,onScreenCount - 1);
        }

        if(success)
        {
            do
            {
                if(key == oC_TGUI_Key_ArrowUp)
                {
                    if(selected != first)
                    {
                        if(
                            selected       == firstToDisplay &&
                            elementsCount  >  onScreenCount &&
                            firstToDisplay != first
                            )
                        {
                            firstToDisplay = oC_List_ElementHandle_Previous(firstToDisplay);
                            lastToDisplay  = oC_List_ElementHandle_Previous(lastToDisplay);
                        }
                        selected = oC_List_ElementHandle_Previous(selected);
                    }
                    else
                    {
                        selected = last;
                        while(lastToDisplay != last && lastToDisplay != NULL)
                        {
                            lastToDisplay   = oC_List_ElementHandle_Next(lastToDisplay);
                            firstToDisplay  = oC_List_ElementHandle_Next(firstToDisplay);
                        }
                    }
                }
                else if(key == oC_TGUI_Key_ArrowDown)
                {
                    if(selected != last)
                    {
                        if(
                            selected       == lastToDisplay &&
                            elementsCount   > onScreenCount &&
                            lastToDisplay  != last
                            )
                        {
                            firstToDisplay = oC_List_ElementHandle_Next(firstToDisplay);
                            lastToDisplay  = oC_List_ElementHandle_Next(lastToDisplay);
                        }
                        selected = oC_List_ElementHandle_Next(selected);
                    }
                    else
                    {
                        selected = first;
                        while(firstToDisplay != first && firstToDisplay != NULL)
                        {
                            lastToDisplay   = oC_List_ElementHandle_Previous(lastToDisplay);
                            firstToDisplay  = oC_List_ElementHandle_Previous(firstToDisplay);
                        }
                    }
                }
                else if(key == oC_TGUI_Key_Enter)
                {
                    if(isaddresscorrect(Config->SelectHandler))
                    {
                        Config->SelectHandler(selected);
                    }
                }
                else if(key == oC_TGUI_Key_ESC)
                {
                    selected = NULL;
                    break;
                }

                for(element = firstToDisplay , position = topLeft; position.Line <= bottomRight.Line ; position.Line++)
                {
                    if(isram(element))
                    {
                        if(element == selected)
                        {
                            oC_TGUI_SetStyle(&Config->Style->ActiveEntry);
                        }
                        else
                        {
                            oC_TGUI_SetStyle(&Config->Style->NotActiveEntry);
                        }

                        oC_TGUI_DrawNCharAtPosition(position,' ',width);
                        oC_TGUI_SetCursorPosition(position);
                        Config->DrawHandler(position,element,width);

                        element = oC_List_ElementHandle_Next(element);
                    }
                    else
                    {
                        oC_TGUI_SetStyle(&Config->Style->NotActiveEntry);
                        oC_TGUI_DrawNCharAtPosition(position,' ',width);
                    }
                }
            } while(oC_TGUI_WaitForKeyPress(&key));
        }
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Key_t oC_TGUI_DrawPushButton( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , const oC_TGUI_PushButton_t * PushButton , bool Active )
{
    oC_TGUI_Key_t key     = 0;
    bool          success = false;

    if(oC_TGUI_Position_IsCorrect(Position) && Width > 0 && Height > 0 && isaddresscorrect(PushButton) && isaddresscorrect(PushButton->Text) && isaddresscorrect(PushButton->Style))
    {
        oC_TGUI_Position_t        topLeft     = Position;
        oC_TGUI_Position_t        bottomRight = { .Column = Position.Column + Width , .Line = Position.Line + Height };
        uint32_t                  textLength  = strlen(PushButton->Text);
        oC_TGUI_Position_t        textPosition= Position;


        success = true;

        if(PushButton->Style->ActiveBorder.DontDraw != true)
        {
            success = success && oC_TGUI_SetStyle(&PushButton->Style->ActiveBorder) &&
                                 oC_TGUI_DrawBorder(topLeft,bottomRight);
            topLeft.Column++;
            topLeft.Line++;
            bottomRight.Column--;
            bottomRight.Line--;
            Width--;
            Height--;
        }

        textPosition.Column = topLeft.Column + (Width/2) - (textLength/2);
        textPosition.Line   = topLeft.Line;


        if(Active)
        {
            success = success && oC_TGUI_SetStyle(&PushButton->Style->Active);
        }
        else
        {
            success = success && oC_TGUI_SetStyle(&PushButton->Style->NotActive);
        }

        success = success && oC_TGUI_DrawNCharAtPosition(topLeft,' ',Width) &&
                             oC_TGUI_DrawAtPosition(textPosition,PushButton->Text);

        if(Active)
        {
            while(oC_TGUI_WaitForKeyPress(&key))
            {
                if(key == oC_TGUI_Key_Enter)
                {
                    if(isaddresscorrect(PushButton->PressHandler))
                    {
                        if(PushButton->PressHandler(PushButton->PressParameter))
                        {
                            key = oC_TGUI_Key_ESC;
                        }
                    }
                    else
                    {
                        key = oC_TGUI_Key_ESC;
                    }
                    break;
                }
                else if(
                        key == oC_TGUI_Key_ArrowDown    ||
                        key == oC_TGUI_Key_ArrowUp      ||
                        key == oC_TGUI_Key_ArrowLeft    ||
                        key == oC_TGUI_Key_ArrowRight   ||
                        key == oC_TGUI_Key_Tab
                        )
                {
                    break;
                }
            }
        }
    }

    return key;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Key_t oC_TGUI_DrawEditBox( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , oC_TGUI_Line_t Height , oC_TGUI_EditBox_t * EditBox , bool Active )
{
    oC_TGUI_Key_t key = 0;

    if(oC_TGUI_Position_IsCorrect(Position) && Width >= 3 && Height > 0 &&
       isaddresscorrect(EditBox) && isram(EditBox->Buffer) && EditBox->BufferSize > 0 &&
       isaddresscorrect(EditBox->Style))
    {
        oC_TGUI_Position_t    topLeft             = Position;
        oC_TGUI_Position_t    bottomRight         = { .Column = Position.Column + Width , .Line = Position.Line + Height };
        oC_TGUI_Position_t    borderTopLeft       = topLeft;
        oC_TGUI_Position_t    borderBottomRight   = bottomRight;
        oC_TGUI_Position_t    insideTopLeft       = { .Column = topLeft.Column     + 1 , .Line = topLeft.Line     + 1 };
        oC_TGUI_Position_t    insideBottomRight   = { .Column = bottomRight.Column - 1 , .Line = bottomRight.Line - 1 };
        oC_TGUI_Position_t    cursorPosition      = insideTopLeft;
        uint32_t              signIndex           = strlen(EditBox->Buffer);
        uint32_t              maxStringLength     = EditBox->BufferSize - 1;

        /* ========================================================================================================== */
        /*                                       Drawing not active first                                             */
        if((EditBox->Style->NotActiveBorder.DontDraw != true && Active == false) ||
           (EditBox->Style->ActiveBorder.DontDraw    != true && Active == true )    )
        {
            oC_TGUI_SetStyle(Active ? &EditBox->Style->ActiveBorder : &EditBox->Style->NotActiveBorder);
            oC_TGUI_DrawBorder(borderTopLeft,borderBottomRight);
            Width--;
            Height--;
        }
        else
        {
            insideTopLeft       = topLeft;
            insideBottomRight   = bottomRight;
        }
        oC_TGUI_SetStyle(&EditBox->Style->DisabledText);
        oC_TGUI_DrawFillBetween(insideTopLeft,insideBottomRight,' ');
        oC_TGUI_DrawMultiLineText(insideTopLeft,insideBottomRight,EditBox->Buffer,oC_Bits_AreBitsSetU32(EditBox->InputType,oC_TGUI_InputType_Password));

        /* ========================================================================================================== */
        /*                                       Drawing active                                                       */
        if(Active)
        {
            bool activated = false;

            do
            {
                if(activated == false)
                {
                    if(key == oC_TGUI_Key_Enter)
                    {
                        key = 0;
                        activated = true;
                    }
                    else if(key == oC_TGUI_Key_ESC
                         || key == oC_TGUI_Key_ArrowUp
                         || key == oC_TGUI_Key_ArrowDown
                         || key == oC_TGUI_Key_ArrowLeft
                         || key == oC_TGUI_Key_ArrowRight
                         || key == oC_TGUI_Key_Tab       )
                    {
                        break;
                    }
                }
                if(activated == true)
                {
                    if(key == oC_TGUI_Key_ESC)
                    {
                        if(isaddresscorrect(EditBox->SaveHandler))
                        {
                            EditBox->SaveHandler(EditBox->Buffer,EditBox->BufferSize,EditBox->SaveParameter);
                        }
                        activated = false;
                    }
                    else if( key == oC_TGUI_Key_ArrowUp   )
                    {
                        if(cursorPosition.Line > insideTopLeft.Line)
                        {
                            cursorPosition.Line--;
                            signIndex      = GetIndexFromPosition(insideTopLeft,insideBottomRight,cursorPosition,EditBox->Buffer);
                            cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,EditBox->Buffer,signIndex);
                        }
                    }
                    else if( key == oC_TGUI_Key_ArrowDown )
                    {
                        if(cursorPosition.Line < insideBottomRight.Line)
                        {
                            cursorPosition.Line++;
                            signIndex      = GetIndexFromPosition(insideTopLeft,insideBottomRight,cursorPosition,EditBox->Buffer);
                            cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,EditBox->Buffer,signIndex);
                        }
                    }
                    else if( key == oC_TGUI_Key_ArrowLeft )
                    {
                        if(cursorPosition.Column > insideTopLeft.Column)
                        {
                            cursorPosition.Column--;
                            signIndex      = GetIndexFromPosition(insideTopLeft,insideBottomRight,cursorPosition,EditBox->Buffer);
                            cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,EditBox->Buffer,signIndex);
                        }
                    }
                    else if( key == oC_TGUI_Key_ArrowRight)
                    {
                        if(cursorPosition.Column < bottomRight.Column)
                        {
                            cursorPosition.Column++;
                            signIndex      = GetIndexFromPosition(insideTopLeft,insideBottomRight,cursorPosition,EditBox->Buffer);
                            cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,EditBox->Buffer,signIndex);
                        }
                    }
                    else if( key == oC_TGUI_Key_Backspace )
                    {
                        signIndex      = string_backspace(EditBox->Buffer,signIndex);
                        cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,EditBox->Buffer,signIndex);
                    }
                    else if( key == oC_TGUI_Key_Enter )
                    {
                        if(oC_Bits_AreBitsSetU32(EditBox->InputType,oC_TGUI_InputType_SpecialChars) && cursorPosition.Line < insideBottomRight.Line)
                        {
                            signIndex      = put_to_string(EditBox->Buffer,EditBox->BufferSize,signIndex,'\n');
                            signIndex      = put_to_string(EditBox->Buffer,EditBox->BufferSize,signIndex,'\r');
                            cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,EditBox->Buffer,signIndex);
                        }
                    }
                    else if(isprint((int)key) && signIndex < maxStringLength && (cursorPosition.Line < insideBottomRight.Line || cursorPosition.Column < insideBottomRight.Column))
                    {
                        if(
                           (oC_Bits_AreBitsSetU32(EditBox->InputType,oC_TGUI_InputType_Digits)       &&  isdigit((int)key)) ||
                           (oC_Bits_AreBitsSetU32(EditBox->InputType,oC_TGUI_InputType_Characters)   &&  isalpha((int)key)) ||
                           (oC_Bits_AreBitsSetU32(EditBox->InputType,oC_TGUI_InputType_SpecialChars) && (isgraph((int)key) || isspace((int)key)))
                            )
                        {
                            signIndex      = put_to_string(EditBox->Buffer,EditBox->BufferSize,signIndex,(char)key);
                            cursorPosition = oC_TGUI_Position_Increment(insideTopLeft,insideBottomRight,cursorPosition);
                        }
                    }

                    oC_TGUI_SetStyle(activated ? &EditBox->Style->Text : &EditBox->Style->DisabledText);
                    oC_TGUI_DrawFillBetween(insideTopLeft,insideBottomRight,' ');
                    oC_TGUI_DrawMultiLineText(insideTopLeft,insideBottomRight,EditBox->Buffer,oC_Bits_AreBitsSetU32(EditBox->InputType,oC_TGUI_InputType_Password));
                    oC_TGUI_SetCursorPosition(cursorPosition);
                }
            } while(oC_TGUI_WaitForKeyPress(&key));
        }

    }

    return key;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Key_t oC_TGUI_DrawQuickEditBox( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , oC_TGUI_QuickEditBox_t * QuickEditBox , bool Active )
{
    oC_TGUI_Key_t key = 0;

    if(oC_TGUI_Position_IsCorrect(Position) && Width > 0 && isram(QuickEditBox) && isram(QuickEditBox->Buffer) && QuickEditBox->BufferSize > 0 && isaddresscorrect(QuickEditBox->Style))
    {
        oC_TGUI_Position_t    topLeft             = Position;
        oC_TGUI_Position_t    bottomRight         = { .Column = Position.Column + Width - 1 , .Line = Position.Line };
        oC_TGUI_Position_t    borderTopLeft       = topLeft;
        oC_TGUI_Position_t    borderBottomRight   = { .Column = bottomRight.Column , .Line = Position.Line + 2 };
        oC_TGUI_Position_t    insideTopLeft       = topLeft;
        oC_TGUI_Position_t    insideBottomRight   = bottomRight;

        /* ========================================================================================================== */
        /*                                       Drawing not active first                                             */
        if((QuickEditBox->Style->NotActiveBorder.DontDraw != true && Active == false) ||
           (QuickEditBox->Style->ActiveBorder.DontDraw    != true && Active == true )    )
        {
            oC_TGUI_SetStyle(Active ? &QuickEditBox->Style->ActiveBorder : &QuickEditBox->Style->NotActiveBorder);
            oC_TGUI_DrawBorder(borderTopLeft,borderBottomRight);
            Width--;
            insideTopLeft.Column++;
            insideBottomRight.Column--;
        }

        oC_TGUI_SetStyle(&QuickEditBox->Style->NotActiveText);
        oC_TGUI_DrawFillBetween(insideTopLeft,insideBottomRight,' ');
        oC_TGUI_DrawMultiLineText(insideTopLeft,insideBottomRight,QuickEditBox->Buffer,oC_Bits_AreBitsSetU32(QuickEditBox->InputType,oC_TGUI_InputType_Password));

        if(Active)
        {
            oC_TGUI_Position_t  cursorPosition  = insideTopLeft;
            uint32_t            maxStringLength = QuickEditBox->BufferSize - 1;
            uint32_t            signIndex       = strlen(QuickEditBox->Buffer);

            cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,QuickEditBox->Buffer,signIndex);

            do
            {
                if( key == oC_TGUI_Key_ESC
                 || key == oC_TGUI_Key_ArrowUp
                 || key == oC_TGUI_Key_ArrowDown
                 || key == oC_TGUI_Key_Tab       )
                {
                    break;
                }
                else if(key == oC_TGUI_Key_Enter)
                {
                    if(isaddresscorrect(QuickEditBox->SaveHandler))
                    {
                        QuickEditBox->SaveHandler(QuickEditBox->Buffer,QuickEditBox->BufferSize,QuickEditBox->SaveParameter);
                    }
                    else
                    {
                        break;
                    }
                }
                else if(key == oC_TGUI_Key_ArrowRight)
                {
                    if(cursorPosition.Column < insideBottomRight.Column && signIndex < strlen(QuickEditBox->Buffer))
                    {
                        cursorPosition.Column++;
                        signIndex      = GetIndexFromPosition(insideTopLeft,insideBottomRight,cursorPosition,QuickEditBox->Buffer);
                        cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,QuickEditBox->Buffer,signIndex);
                    }
                    else
                    {
                        break;
                    }
                }
                else if( key == oC_TGUI_Key_ArrowLeft )
                {
                    if(cursorPosition.Column > insideTopLeft.Column)
                    {
                        cursorPosition.Column--;
                        signIndex      = GetIndexFromPosition(insideTopLeft,insideBottomRight,cursorPosition,QuickEditBox->Buffer);
                        cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,QuickEditBox->Buffer,signIndex);
                    }
                    else
                    {
                        break;
                    }
                }
                else if( key == oC_TGUI_Key_Backspace )
                {
                    signIndex      = string_backspace(QuickEditBox->Buffer,signIndex);
                    cursorPosition = GetPositionFromIndex(insideTopLeft,insideBottomRight,QuickEditBox->Buffer,signIndex);
                }
                else if(isprint((int)key) && signIndex < maxStringLength && (cursorPosition.Line < insideBottomRight.Line || cursorPosition.Column < insideBottomRight.Column))
                {
                    if(
                       (oC_Bits_AreBitsSetU32(QuickEditBox->InputType,oC_TGUI_InputType_Digits)       &&  isdigit((int)key)) ||
                       (oC_Bits_AreBitsSetU32(QuickEditBox->InputType,oC_TGUI_InputType_Characters)   &&  isalpha((int)key)) ||
                       (oC_Bits_AreBitsSetU32(QuickEditBox->InputType,oC_TGUI_InputType_SpecialChars) && (isgraph((int)key) || isspace((int)key)))
                        )
                    {
                        signIndex      = put_to_string(QuickEditBox->Buffer,QuickEditBox->BufferSize,signIndex,(char)key);
                        cursorPosition = oC_TGUI_Position_Increment(insideTopLeft,insideBottomRight,cursorPosition);
                    }
                }

                oC_TGUI_SetStyle(&QuickEditBox->Style->ActiveText);
                oC_TGUI_DrawFillBetween(insideTopLeft,insideBottomRight,' ');
                oC_TGUI_DrawMultiLineText(insideTopLeft,insideBottomRight,QuickEditBox->Buffer,oC_Bits_AreBitsSetU32(QuickEditBox->InputType,oC_TGUI_InputType_Password));
                oC_TGUI_SetCursorPosition(cursorPosition);
            } while(oC_TGUI_WaitForKeyPress(&key));
        }
    }

    return key;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Key_t oC_TGUI_DrawSelectionBox( oC_TGUI_Position_t Position , oC_TGUI_Column_t Width , const oC_TGUI_SelectionBox_t * SelectionBox , bool Active )
{
    oC_TGUI_Key_t key = 0;

    if(
        isaddresscorrect(SelectionBox)
     && isaddresscorrect(SelectionBox->Style)
     && isaddresscorrect(SelectionBox->Options)
     && isram(SelectionBox->SelectedIndex)
     && SelectionBox->NumberOfOptions > 1
     && oC_TGUI_Position_IsCorrect(Position)
     && Width > 0
         )
    {
        if(Active == false)
        {
            oC_TGUI_SetStyle(&SelectionBox->Style->NotActive);
            oC_TGUI_DrawAtPositionWithSize(Position,SelectionBox->Options[*SelectionBox->SelectedIndex],Width);
        }
        else
        {
            oC_TGUI_Column_t    textWidth           = Width - 2;
            oC_TGUI_Position_t  textPosition        = { .Column = Position.Column + 1             , .Line = Position.Line };
            oC_TGUI_Position_t  rightArrowPosition  = { .Column = textPosition.Column + textWidth , .Line = Position.Line };

            do
            {
                if(key == oC_TGUI_Key_ArrowLeft)
                {
                    uint32_t selected = *SelectionBox->SelectedIndex;
                    if( selected > 0 )
                    {
                        *SelectionBox->SelectedIndex = selected - 1;
                    }
                }
                else if(key == oC_TGUI_Key_ArrowRight)
                {
                    uint32_t selected = *SelectionBox->SelectedIndex;
                    if( selected < (SelectionBox->NumberOfOptions - 1) )
                    {
                        *SelectionBox->SelectedIndex = selected + 1;
                    }
                }
                else if(
                        key == oC_TGUI_Key_ArrowDown ||
                        key == oC_TGUI_Key_ArrowUp   ||
                        key == oC_TGUI_Key_Tab       ||
                        key == oC_TGUI_Key_Enter     ||
                        key == oC_TGUI_Key_ESC
                        )
                {
                    break;
                }

                oC_TGUI_SetStyle(&SelectionBox->Style->Arrow);
                oC_TGUI_DrawAtPosition(Position,"<");
                oC_TGUI_SetStyle(&SelectionBox->Style->Active);
                oC_TGUI_DrawAtPositionWithSize(textPosition,SelectionBox->Options[*SelectionBox->SelectedIndex],textWidth);
                oC_TGUI_SetStyle(&SelectionBox->Style->Arrow);
                oC_TGUI_DrawAtPosition(rightArrowPosition,">");
            } while(oC_TGUI_WaitForKeyPress(&key));
        }
    }

    return key;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_TGUI_Key_t oC_TGUI_DrawActiveObject( oC_TGUI_ActiveObject_t * ActiveObject , bool Active )
{
    oC_TGUI_Key_t key = 0;

    if(isaddresscorrect(ActiveObject) && ActiveObject->Width > 0 && oC_TGUI_Position_IsCorrect(ActiveObject->Position))
    {
        if(isaddresscorrect(ActiveObject->LabelStyle) && ActiveObject->LabelStyle->DontDraw != true)
        {
            oC_TGUI_SetStyle(ActiveObject->LabelStyle);
            oC_TGUI_DrawAtPositionWithSize(ActiveObject->LabelPosition,ActiveObject->LabelText,ActiveObject->LabelWidth);
        }
        switch(ActiveObject->Type)
        {
            case oC_TGUI_ActiveObjecType_PushButton:
                key = oC_TGUI_DrawPushButton(ActiveObject->Position,ActiveObject->Width,ActiveObject->Height,&ActiveObject->PushButton,Active);
                break;
            case oC_TGUI_ActiveObjecType_EditBox:
                key = oC_TGUI_DrawEditBox(ActiveObject->Position,ActiveObject->Width,ActiveObject->Height,&ActiveObject->EditBox,Active);
                break;
            case oC_TGUI_ActiveObjecType_QuickEdit:
                key = oC_TGUI_DrawQuickEditBox(ActiveObject->Position,ActiveObject->Width,&ActiveObject->QuickEdit,Active);
                break;
            case oC_TGUI_ActiveObjecType_SelectionBox:
                key = oC_TGUI_DrawSelectionBox(ActiveObject->Position,ActiveObject->Width,&ActiveObject->SelectionBox,Active);
                break;
        }
    }

    return key;
}

//==========================================================================================================================================
/**
 * @brief draws array of active objects and handles it
 *
 * The function is for drawing and handling array of active objects. The function ends, when the pressed key is ESCAPE or when function arguments
 * are not correct.
 *
 * @param ActiveObjects     Pointer to array with active objects
 * @param NumberOfObjects   Number of elements in the ActiveObjects array
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_TGUI_DrawActiveObjects( oC_TGUI_ActiveObject_t * ActiveObjects, uint32_t NumberOfObjects )
{
    bool success = false;

    if(isaddresscorrect(ActiveObjects) && NumberOfObjects > 0)
    {
        oC_TGUI_Key_t            key            = 0;
        oC_TGUI_ActiveObject_t * activeObject   = ActiveObjects;
        oC_TGUI_ActiveObject_t * currentObject  = NULL;

        do
        {
            for(uint32_t i = 0 ; i < NumberOfObjects ; i++)
            {
                oC_TGUI_DrawActiveObject(&ActiveObjects[i],false);
            }

            key = oC_TGUI_DrawActiveObject(activeObject,true);

            if(key == oC_TGUI_Key_ArrowUp)
            {
                activeObject = GetActiveObjectOnUp(ActiveObjects,NumberOfObjects,activeObject);
            }
            else if(key == oC_TGUI_Key_ArrowDown)
            {
                activeObject = GetActiveObjectOnDown(ActiveObjects,NumberOfObjects,activeObject);
            }
            else if(key == oC_TGUI_Key_ArrowLeft)
            {
                activeObject = GetActiveObjectOnLeft(ActiveObjects,NumberOfObjects,activeObject);
            }
            else if(key == oC_TGUI_Key_ArrowRight)
            {
                activeObject = GetActiveObjectOnRight(ActiveObjects,NumberOfObjects,activeObject);
            }
            else if(key == oC_TGUI_Key_Tab || key == oC_TGUI_Key_Enter)
            {
                currentObject = GetActiveObjectOnRight(ActiveObjects,NumberOfObjects,activeObject);

                if(currentObject == activeObject)
                {
                    currentObject = GetActiveObjectOnDown(ActiveObjects,NumberOfObjects,activeObject);
                }
                if(currentObject == activeObject)
                {
                    currentObject = &ActiveObjects[0];
                }
                activeObject = currentObject;
            }
            else if(key == oC_TGUI_Key_ESC)
            {
                break;
            }
        } while(key != 0);

        success = key != 0;
    }

    return success;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_WaitForKeyPress( oC_TGUI_Key_t * outKey )
{
    bool             pressed    = false;
    char             buffer[30];
    oC_ErrorCode_t   errorCode  = oC_ErrorCode_ImplementError;

    memset(buffer,0,sizeof(buffer));

    if(oC_AssignErrorCode(&errorCode,oC_KPrint_ReadFromStdIn(oC_IoFlags_NoTimeout | oC_IoFlags_SleepWhileWaiting | oC_IoFlags_WaitForSomeElements , buffer , sizeof(buffer))))
    {
        pressed = true;

        if(isram(outKey))
        {
            if(buffer[0] == 13)
            {
                *outKey = oC_TGUI_Key_Enter;
            }
            else if(buffer[0] == 127 || buffer[0] == 8)
            {
                *outKey = oC_TGUI_Key_Backspace;
            }
            else if(buffer[0] == 9)
            {
                *outKey = oC_TGUI_Key_Tab;
            }
            else if(buffer[0] == '\033')
            {
                *outKey = oC_TGUI_Key_SpecialKeysId +
                          ((oC_TGUI_Key_t)buffer[1]) +
                          ((oC_TGUI_Key_t)buffer[2]) +
                          ((oC_TGUI_Key_t)buffer[3]) +
                          ((oC_TGUI_Key_t)buffer[4]) +
                          ((oC_TGUI_Key_t)buffer[5]);
            }
            else
            {
                *outKey = (oC_TGUI_Key_t)buffer[0];
            }
        }
    }
    else
    {
        kdebuglog(oC_LogType_Error, "Cannot read from stdin stream: %R\n", errorCode);
        sleep( s(1) );
    }

    return pressed;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TGUI_CheckKeyPressed( oC_TGUI_Key_t * outKey )
{
    bool             pressed    = false;
    char             buffer[30];
    oC_ErrorCode_t   errorCode  = oC_ErrorCode_ImplementError;

    memset(buffer,0,sizeof(buffer));

    if(oC_AssignErrorCode(&errorCode,oC_KPrint_ReadFromStdIn(oC_IoFlags_WaitForSomeElements | oC_IoFlags_0sTimeout , buffer , sizeof(buffer))))
    {
        pressed = true;

        if(isram(outKey))
        {
            if(buffer[0] == 13)
            {
                *outKey = oC_TGUI_Key_Enter;
            }
            else if(buffer[0] == 127)
            {
                *outKey = oC_TGUI_Key_Backspace;
            }
            else if(buffer[0] == 9)
            {
                *outKey = oC_TGUI_Key_Tab;
            }
            else if(buffer[0] == '\033')
            {
                *outKey = oC_TGUI_Key_SpecialKeysId +
                          ((oC_TGUI_Key_t)buffer[1]) +
                          ((oC_TGUI_Key_t)buffer[2]) +
                          ((oC_TGUI_Key_t)buffer[3]) +
                          ((oC_TGUI_Key_t)buffer[4]) +
                          ((oC_TGUI_Key_t)buffer[5]);
            }
            else
            {
                *outKey = (oC_TGUI_Key_t)buffer[0];
            }
        }
    }

    return pressed;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static uint32_t GetIndexFromPosition( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t BottomRight , oC_TGUI_Position_t Position , const char * Buffer )
{
    uint32_t              index           = 0;
    uint32_t              strLength       = strlen(Buffer);
    oC_TGUI_Position_t    currentPosition = TopLeft;

    for(index = 0 ; index < strLength && oC_TGUI_Position_IsSmaller(currentPosition,BottomRight); index++)
    {
        if(oC_TGUI_Position_Compare(currentPosition,Position) >= 0 && isprint((int)Buffer[index]) )
        {
            break;
        }
        else if(Buffer[index] == '\n')
        {
            currentPosition.Line++;
        }
        else if(Buffer[index] == '\r')
        {
            currentPosition.Column = TopLeft.Column;
        }
        else
        {
            currentPosition = oC_TGUI_Position_Increment(TopLeft,BottomRight,currentPosition);
        }
    }

    return index;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_TGUI_Position_t GetPositionFromIndex( oC_TGUI_Position_t TopLeft , oC_TGUI_Position_t BottomRight , const char * Buffer , uint32_t Index )
{
    oC_TGUI_Position_t currentPosition = TopLeft;
    uint32_t           bufferLength    = strlen(Buffer);

    for(uint32_t i = 0 ; i < bufferLength && oC_TGUI_Position_IsSmaller(currentPosition,BottomRight) ; i++ )
    {
        if(Index == i)
        {
            break;
        }
        else if(Buffer[i] == '\n')
        {
            currentPosition.Line++;
        }
        else if(Buffer[i] == '\r')
        {
            currentPosition.Column = TopLeft.Column;
        }
        else
        {
            currentPosition = oC_TGUI_Position_Increment(TopLeft,BottomRight,currentPosition);
        }
    }

    return currentPosition;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_TGUI_ActiveObject_t* GetActiveObjectOnRight( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * ActiveObject )
{
    oC_TGUI_ActiveObject_t* activeObject = ActiveObject;

    for(uint32_t i = 0; i < Size ;i++)
    {
        if(Array[i].Position.Line == activeObject->Position.Line && Array[i].Position.Column > ActiveObject->Position.Column)
        {
            if(activeObject == ActiveObject || Array[i].Position.Column < activeObject->Position.Column)
            {
                activeObject = &Array[i];
            }
        }
    }

    return activeObject;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_TGUI_ActiveObject_t*  GetActiveObjectOnLeft( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * ActiveObject )
{
    oC_TGUI_ActiveObject_t* activeObject = ActiveObject;

    for(uint32_t i = 0; i < Size ;i++)
    {
        if(Array[i].Position.Line == activeObject->Position.Line && Array[i].Position.Column < ActiveObject->Position.Column)
        {
            if(activeObject == ActiveObject || Array[i].Position.Column > activeObject->Position.Column)
            {
                activeObject = &Array[i];
            }
        }
    }

    return activeObject;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_TGUI_ActiveObject_t*  GetActiveObjectOnDown( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * OldObject )
{
    oC_TGUI_ActiveObject_t* selectedObject = OldObject;

    for(uint32_t i = 0; i < Size ;i++)
    {
        if(oC_TGUI_Position_IsLower(Array[i].Position,OldObject->Position))
        {
            if(oC_TGUI_Position_IsLower(selectedObject->Position,OldObject->Position))
            {
                if(oC_TGUI_Position_IsHigher(Array[i].Position,selectedObject->Position))
                {
                    selectedObject = &Array[i];
                }
                else if(Array[i].Position.Line == selectedObject->Position.Line && oC_ABS(Array[i].Position.Column,OldObject->Position.Column) < oC_ABS(selectedObject->Position.Column,OldObject->Position.Column))
                {
                    selectedObject = &Array[i];
                }
            }
            else
            {
                selectedObject = &Array[i];
            }
        }
    }

    return selectedObject;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_TGUI_ActiveObject_t* GetActiveObjectOnUp( oC_TGUI_ActiveObject_t * Array , uint32_t Size , oC_TGUI_ActiveObject_t * OldObject )
{
    oC_TGUI_ActiveObject_t* selectedObject = OldObject;

    for(uint32_t i = 0; i < Size ;i++)
    {
        if(oC_TGUI_Position_IsHigher(Array[i].Position,OldObject->Position))
        {
            if(oC_TGUI_Position_IsHigher(selectedObject->Position,OldObject->Position))
            {
                if(oC_TGUI_Position_IsLower(Array[i].Position,selectedObject->Position))
                {
                    selectedObject = &Array[i];
                }
                else if(Array[i].Position.Line == selectedObject->Position.Line && oC_ABS(Array[i].Position.Column,OldObject->Position.Column) < oC_ABS(selectedObject->Position.Column,OldObject->Position.Column))
                {
                    selectedObject = &Array[i];
                }
            }
            else
            {
                selectedObject = &Array[i];
            }
        }
    }

    return selectedObject;
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

