/** ****************************************************************************************************************************************
 *
 * @brief      File with interface for ICtrlMan
 * 
 * @file       oc_ictrlman.c
 *
 * @author     Patryk Kubiak - (Created on: 05.05.2017 10:37:14) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_ictrlman.h>
#include <oc_module.h>
#include <oc_controllers_cfg.c>
#include <oc_list.h>
#include <oc_service.h>
#include <oc_serviceman.h>
#include <oc_time.h>
#include <oc_debug.h>
#include <oc_semaphore.h>
#include <oc_mutex.h>
#include <oc_event.h>
#include <oc_struct.h>
#include <oc_intman.h>
#include <oc_screenman.h>
#include <oc_dynamic_config.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define SERVICE_STACK_SIZE          B(2048)
#define CHILD_THREAD_STACK_SIZE     B(4096)
#define MAX_INPUT_CONTROLLERS       1000
#define MAX_ACTIVITIES_WAITERS      100
#define IsServiceContextCorrect(Context)        isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_ICtrlManService, (Context)->ObjectControl)

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Context_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_List(oC_ICtrl_t)     ICtrlList;
};

typedef struct
{
    oC_ICtrlMan_Activity_t* Activity;
    oC_Semaphore_t          Semaphore;
    oC_IDI_Event_t *        Event;
    oC_Timestamp_t          RegistrationTimeStamp;
    uint32_t                ActivityID;
} ActivitiesWaiter_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static void                 AnnounceEvent               ( oC_ICtrl_t ICtrl , oC_IDI_Event_t * Event , uint32_t ActivityID );
static oC_ICtrl_t           GetNewICtrl                 ( oC_Service_Context_t Context );
static oC_ErrorCode_t       StartService                ( oC_Service_Context_t * outContext );
static oC_ErrorCode_t       StopService                 ( oC_Service_Context_t *    Context );
static oC_ErrorCode_t       ServiceMain                 ( oC_Service_Context_t Context );
static void                 ChildThread                 ( void * UserPointer );
static void                 ChildThreadFinishedFunction ( oC_Thread_t Thread , void * UserParameter );
static bool                 RevertRegistration          ( oC_Thread_t Thread , void * Object , uint32_t Parameter );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t Allocator = {
                .Name = "ICtrlMan" ,
};
const oC_Service_Registration_t ictrlman = {
                .Name                   = "ictrlman" ,
                .StdoutStreamName       = NULL ,
                .StartFunction          = StartService,
                .StopFunction           = StopService,
                .MainFunction           = ServiceMain,
                .MainThreadStackSize    = SERVICE_STACK_SIZE ,
                .HeapMapSize            = 0 ,
                .AllocationLimit        = 0 ,
                .RequiredModules        = { oC_Module_ICtrlMan, oC_Module_ScreenMan } ,
                .Priority               = oC_Process_Priority_CoreSpaceProcess ,
};
const oC_Module_Registration_t ICtrlMan = {
                .Name               = "ICtrlMan" ,
                .LongName           = "Input Control Manager" ,
                .Module             = oC_Module_ICtrlMan ,
                .TurnOnFunction     = oC_ICtrlMan_TurnOnModule ,
                .TurnOffFunction    = oC_ICtrlMan_TurnOffModule ,
};
static oC_List(oC_ICtrl_t)              ICtrlList               = NULL;
static oC_Mutex_t                       ModuleBusy              = NULL;
static oC_Semaphore_t                   NewICtrlSemaphore       = NULL;
static uint32_t                         ActivitiesCounter       = 0;
static oC_Event_t                       ActivitiesCounterEvent  = NULL;
static oC_Event_t                       ActivityFinishedEvent   = NULL;
static ActivitiesWaiter_t               ActivitiesWaiters[MAX_ACTIVITIES_WAITERS];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_TurnOnModule( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_ICtrlMan))
    {
        bool allSemaphoreCreated = true;

        ICtrlList               = oC_List_New(&Allocator,AllocationFlags_ZeroFill);
        ModuleBusy              = oC_Mutex_New(oC_Mutex_Type_Normal, &Allocator, AllocationFlags_ZeroFill);
        NewICtrlSemaphore       = oC_Semaphore_New(MAX_INPUT_CONTROLLERS,0,&Allocator, AllocationFlags_ZeroFill);
        ActivitiesCounterEvent  = oC_Event_New(0,&Allocator,AllocationFlags_ZeroFill);
        ActivityFinishedEvent   = oC_Event_New(0,&Allocator,AllocationFlags_ZeroFill);

        oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
        {
            waiter->Activity    = NULL;
            waiter->Event       = NULL;
            waiter->Semaphore   = oC_Semaphore_New(oC_Semaphore_Type_Binary,0,&Allocator,AllocationFlags_ZeroFill);

            if(waiter->Semaphore == NULL)
            {
                allSemaphoreCreated = false;
            }
        }

        if(
            ErrorCondition( ICtrlList               != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( ModuleBusy              != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( allSemaphoreCreated             , oC_ErrorCode_AllocationError )
         && ErrorCondition( NewICtrlSemaphore       != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( ActivitiesCounterEvent  != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( ActivityFinishedEvent   != NULL , oC_ErrorCode_AllocationError )
            )
        {
            oC_Module_TurnOn(oC_Module_ICtrlMan);

            errorCode = oC_ErrorCode_None;

#define NONE            oC_ICtrl_Rotate_None
#define ROTATE_90       oC_ICtrl_Rotate_90DegreesClockwise
#define ROTATE_180      oC_ICtrl_Rotate_180DegreesClockwise
#define ROTATE_270      oC_ICtrl_Rotate_270DegreesClockwise

#define ADD_ICTRL( NAME , DRIVER_NAME , CONFIG_NAME , DEFAULT_SCREEN_NAME, ROTATE)    \
            {   \
                oC_ICtrl_t ictrl = oC_ICtrl_New(NAME,&CONFIG_NAME,&DRIVER_NAME,oC_ScreenMan_GetScreen(DEFAULT_SCREEN_NAME),ROTATE); \
                if(ErrorCondition( ictrl != NULL , oC_ErrorCode_AllocationError))  \
                {\
                     ErrorCode( oC_ICtrlMan_AddICtrl(ictrl) );  \
                }\
            } \

            CFG_ICTRL_LIST(ADD_ICTRL);

#undef ADD_ICTRL
        }
        if(oC_ErrorOccur(errorCode))
        {
            oC_Module_TurnOff(oC_Module_ICtrlMan);
            oC_SaveIfFalse("ICtrlList               " , ICtrlList              == NULL || oC_List_Delete(ICtrlList                  ,0) , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("ModuleBusy              " , ModuleBusy             == NULL || oC_Mutex_Delete(&ModuleBusy               ,0) , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("NewICtrlSemaphore       " , NewICtrlSemaphore      == NULL || oC_Semaphore_Delete(&NewICtrlSemaphore    ,0) , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("ActivitiesCounterEvent  " , ActivitiesCounterEvent == NULL || oC_Event_Delete(&ActivitiesCounterEvent   ,0) , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse("ActivityFinishedEvent   " , ActivityFinishedEvent  == NULL || oC_Event_Delete(&ActivityFinishedEvent    ,0) , oC_ErrorCode_ReleaseError );

            oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
            {
                oC_SaveIfFalse("waiter semaphore" , oC_Semaphore_Delete(&waiter->Semaphore,AllocationFlags_Default), oC_ErrorCode_ReleaseError);
            }
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off module
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_TurnOffModule( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_ExitCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if(ErrorCondition( oC_ServiceMan_IsServiceActive(ictrlman.Name) , oC_ErrorCode_ServiceAlreadyStarted))
        {
            oC_ICtrlMan_UnconfigureAll();

            oC_Module_TurnOff(oC_Module_ICtrlMan);

            bool allInputControllersDeleted = true;

            foreach(ICtrlList,ictrl)
            {
                allInputControllersDeleted = oC_ICtrl_Delete(&ictrl) && allInputControllersDeleted;
            }

            bool listDeleted        = oC_List_Delete(ICtrlList,0);
            bool mutexDeleted       = oC_Mutex_Delete(&ModuleBusy,0);
            bool semaphoreDeleted   = oC_Semaphore_Delete(&NewICtrlSemaphore,0);
            bool eventDeleted       = oC_Event_Delete(&ActivitiesCounterEvent,0);
            bool event2Deleted      = oC_Event_Delete(&ActivityFinishedEvent,0);

            oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
            {
                oC_SaveIfFalse("waiter semaphore" , oC_Semaphore_Delete(&waiter->Semaphore,AllocationFlags_Default), oC_ErrorCode_ReleaseError);
            }

            if( ErrorCondition( allInputControllersDeleted && listDeleted && semaphoreDeleted && mutexDeleted && eventDeleted && event2Deleted, oC_ErrorCode_ReleaseError ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    oC_IntMan_EnterCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures all Input Controllers that possible
 */
//==========================================================================================================================================
void oC_ICtrlMan_ConfigureAllPossible( void )
{
    if(oC_SaveIfFalse( "ICtrlMan", oC_Module_IsTurnedOn(oC_Module_ICtrlMan), oC_ErrorCode_ModuleNotStartedYet ))
    {
        foreach(ICtrlList,ictrl)
        {
            if(oC_SaveIfFalse("ictrl" , oC_ICtrl_IsCorrect(ictrl), oC_ErrorCode_ObjectNotCorrect))
            {
                if(oC_ICtrl_IsConfigured(ictrl) == false)
                {
                    oC_ErrorCode_t errorCode = oC_ICtrl_Configure(ictrl);

                    if(oC_SaveIfErrorOccur("ictrl configuration", errorCode))
                    {
                        kdebuglog(oC_LogType_GoodNews, "input controller '%s' correctly configured!\n", oC_ICtrl_GetName(ictrl));
                    }
                    else
                    {
                        kdebuglog(oC_LogType_Error, "Cannot configure input controller %s: %R\n", oC_ICtrl_GetName(ictrl), errorCode);
                    }
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief unconfigures all input controllers
 */
//==========================================================================================================================================
void oC_ICtrlMan_UnconfigureAll( void )
{
    if(oC_SaveIfFalse( "ICtrlMan", oC_Module_IsTurnedOn(oC_Module_ICtrlMan), oC_ErrorCode_ModuleNotStartedYet ))
    {
        foreach(ICtrlList,ictrl)
        {
            if(oC_SaveIfFalse("ictrl" , oC_ICtrl_IsCorrect(ictrl), oC_ErrorCode_ObjectNotCorrect))
            {
                if(oC_ICtrl_IsConfigured(ictrl) == true)
                {
                    oC_ErrorCode_t errorCode = oC_ICtrl_Unconfigure(ictrl);

                    if(oC_SaveIfErrorOccur("ictrl unconfiguration", errorCode))
                    {
                        kdebuglog(oC_LogType_GoodNews, "input controller '%s' correctly unconfigured!\n", oC_ICtrl_GetName(ictrl));
                    }
                    else
                    {
                        kdebuglog(oC_LogType_Error, "Cannot unconfigure input controller %s: %R\n", oC_ICtrl_GetName(ictrl), errorCode);
                    }
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief returns Input Controllers List
 *
 * The function returns pointer to the input controller list.
 *
 * @warning
 * It returns pointer to the original list. Please treat it as read only.
 */
//==========================================================================================================================================
oC_List(oC_ICtrl_t) oC_ICtrlMan_GetList( void )
{
    return ICtrlList;
}

//==========================================================================================================================================
/**
 * @brief Adds new Input Controller to the list
 *
 * The function adds new input controller (ICtrl object) to the list.
 *
 * @param ICtrl         Input Controller to add
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_AddICtrl( oC_ICtrl_t ICtrl )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if( ErrorCondition( oC_ICtrl_IsCorrect(ICtrl) , oC_ErrorCode_ObjectNotCorrect ) )
        {
            bool pushed = oC_List_PushBack(ICtrlList,ICtrl,&Allocator);

            if( ErrorCondition( pushed , oC_ErrorCode_CannotAddObjectToList ) )
            {
                oC_Semaphore_GiveCounting(NewICtrlSemaphore,1);
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Removes Input Controller from the list
 *
 * The function removes input controller (ICtrl object) from the list.
 *
 * @param ICtrl         Input Controller to remove
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_RemoveICtrl( oC_ICtrl_t ICtrl )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if( ErrorCondition( oC_ICtrl_IsCorrect(ICtrl) , oC_ErrorCode_ObjectNotCorrect ) )
        {
            bool removed = oC_List_RemoveAll(ICtrlList,ICtrl);

            if( ErrorCondition( removed , oC_ErrorCode_CannotRemoveObjectFromList ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Returns first ICtrl object related with the given screen
 */
//==========================================================================================================================================
oC_ICtrl_t oC_ICtrlMan_GetICtrl( oC_Screen_t Screen )
{
    oC_ICtrl_t ictrl = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_ICtrlMan))
    {
        foreach(ICtrlList,element)
        {
            if(oC_ICtrl_GetScreen(element) == Screen)
            {
                ictrl = element;
                break;
            }
        }
    }

    return ictrl;
}

//==========================================================================================================================================
/**
 * @brief registers activity and waits for it
 *
 * The function waits for an input activity.
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_WaitForActivity( oC_ICtrlMan_Activity_t * Activity , oC_IDI_Event_t * outEvent, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if(
            ErrorCondition( isram(Activity)                                 , oC_ErrorCode_AddressNotInRam          )
         && ErrorCondition( isram(outEvent)                                 , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Timeout >= 0                                    , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( oC_ServiceMan_IsServiceActive(ictrlman.Name)    , oC_ErrorCode_ServiceNotStarted        )
            )
        {
            ActivitiesWaiter_t * foundWaiter = NULL;

            oC_IntMan_EnterCriticalSection();
            oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
            {
                if(waiter->Activity == NULL)
                {
                    waiter->Activity = Activity;
                    waiter->Event    = outEvent;
                    foundWaiter      = waiter;
                    break;
                }
            }
            oC_IntMan_ExitCriticalSection();

            if( ErrorCondition( foundWaiter != NULL , oC_ErrorCode_NoFreeSlots ) )
            {
                oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

                if( ErrorCondition( oC_Semaphore_Take(foundWaiter->Semaphore,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout ) )
                {
                    oC_IntMan_EnterCriticalSection();
                    errorCode = oC_ErrorCode_None;
                    oC_IntMan_ExitCriticalSection();
                }
                foundWaiter->Activity = NULL;
                foundWaiter->Event    = NULL;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief registers activity without waiting for it finish
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_RegisterActivity( oC_ICtrlMan_Activity_t * Activity , oC_IDI_Event_t * outEvent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if(
            ErrorCondition( isram(Activity)                                                         , oC_ErrorCode_AddressNotInRam          )
         && ErrorCondition( isram(outEvent)                                                         , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( oC_ServiceMan_IsServiceActive(ictrlman.Name)                            , oC_ErrorCode_ServiceNotStarted        )
#if 0
         && ErrorCondition( oC_Thread_SaveToRevert(getcurthread(), RevertRegistration, Activity, 0) , oC_ErrorCode_ThreadNotCorrect         )
#endif
            )
        {
            ActivitiesWaiter_t * foundWaiter = NULL;

            oC_IntMan_EnterCriticalSection();
            oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
            {
                if(waiter->Activity == NULL)
                {
                    waiter->Activity                = Activity;
                    waiter->Event                   = outEvent;
                    waiter->RegistrationTimeStamp   = gettimestamp();
                    oC_Semaphore_ForceValue(waiter->Semaphore,0);

                    foundWaiter                     = waiter;
                    break;
                }
            }
            oC_IntMan_ExitCriticalSection();

            if( ErrorCondition( foundWaiter != NULL , oC_ErrorCode_NoFreeSlots ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief registers activity without waiting for it finish
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_UnregisterActivity( oC_ICtrlMan_Activity_t * Activity )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if(
            ErrorCondition( isram(Activity)                                 , oC_ErrorCode_AddressNotInRam          )
         && ErrorCondition( oC_ServiceMan_IsServiceActive(ictrlman.Name)    , oC_ErrorCode_ServiceNotStarted        )
            )
        {
            ActivitiesWaiter_t * foundWaiter = NULL;

            oC_IntMan_EnterCriticalSection();
            oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
            {
                if(waiter->Activity == Activity)
                {
                    waiter->Activity                = NULL;
                    waiter->Event                   = NULL;
                    waiter->RegistrationTimeStamp   = 0;
                    oC_Semaphore_ForceValue(waiter->Semaphore,0);
                    foundWaiter                     = waiter;
                    break;
                }
            }
            oC_IntMan_ExitCriticalSection();

            if(
                ErrorCondition( foundWaiter != NULL                                                     , oC_ErrorCode_ObjectNotFoundOnList )
             && ErrorCondition( oC_Thread_RemoveFromRevert(getcurthread(), RevertRegistration, Activity), oC_ErrorCode_ThreadNotCorrect     )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns true if the given activity has occurred
 */
//==========================================================================================================================================
bool oC_ICtrlMan_HasActivityOccurred( oC_ICtrlMan_Activity_t * Activity )
{
    bool occurred = false;

    if(oC_Module_IsTurnedOn(oC_Module_ICtrlMan) && isram(Activity))
    {
        oC_IntMan_EnterCriticalSection();
        oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
        {
            if(waiter->Activity == Activity)
            {
                occurred = oC_Semaphore_Take(waiter->Semaphore,s(0));
                break;
            }
        }
        oC_IntMan_ExitCriticalSection();
    }

    return occurred;
}

//==========================================================================================================================================
/**
 * @brief waits until activity is not finished
 */
//==========================================================================================================================================
bool oC_ICtrlMan_WaitForActivityFinish( oC_ICtrlMan_Activity_t * Activity , oC_Time_t Timeout )
{
    bool success = false;

    if(oC_Module_IsTurnedOn(oC_Module_ICtrlMan))
    {
        if(
            oC_SaveIfFalse("Activity" , isram(Activity)                               , oC_ErrorCode_AddressNotInRam          )
         && oC_SaveIfFalse("ICtrlMan" , oC_ServiceMan_IsServiceActive(ictrlman.Name)  , oC_ErrorCode_ServiceNotStarted        )
            )
        {
            ActivitiesWaiter_t * foundWaiter = NULL;

            oC_IntMan_EnterCriticalSection();
            oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
            {
                if(waiter->Activity == Activity)
                {
                    waiter->Activity = NULL;
                    waiter->Event    = NULL;
                    foundWaiter      = waiter;
                    break;
                }
            }
            oC_IntMan_ExitCriticalSection();

            if( oC_SaveIfFalse( "waiter", foundWaiter != NULL, oC_ErrorCode_ObjectNotFoundOnList ) )
            {
                success = oC_Event_WaitForState( ActivityFinishedEvent, foundWaiter->ActivityID, oC_Event_StateMask_DifferentThan, Timeout );
            }
        }
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief reads current position of the cursor
 *
 * The function is for reading the position of the cursor.
 *
 * @param outScreen         Destination for the screen which currently display the position of the cursor
 * @param outPosition       Destination for the cursor position
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_ReadCursorPosition( oC_Screen_t * outScreen , oC_Pixel_Position_t * outPosition )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if( ErrorCondition( isram(outScreen) && isram(outPosition), oC_ErrorCode_OutputAddressNotInRAM) )
        {
            errorCode = oC_ErrorCode_NotImplemented;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for any new activity
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_WaitForAnyNewActivity( oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if( ErrorCondition( Timeout >= 0 , oC_ErrorCode_TimeNotCorrect ) )
        {
            if(oC_Event_WaitForState(ActivitiesCounterEvent,ActivitiesCounter,oC_Event_StateMask_DifferentThan,Timeout))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_Timeout;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Waits for raw input controller event.
 *
 * The function is for reading input controllers events without software gestures analyzes.
 *
 * @note
 * This function can disturb gestures recognition, thats why one of the parameters is `GesturesWindow` - it is a time for ictrlman service.
 * Thanks to that it can still work also if you want to use ICtrl directly despite of the fact, that input controller is used by 2 different
 * threads. If you don't want to use software gestures recognition, you can set this parameter to 0.
 *
 * @warning
 * The function will read from the first ICtrl object on the list, that is associated with the given screen. If you want to read from other
 * input controller, you have to use #oC_ICtrlMan_WaitForActivity
 *
 * @param Screen                Screen associated with the controller you want to read from
 * @param EventsMask            Event ID to wait for - note that it should be single event if you don't want to wait for gestures recognition
 * @param outEvent              Destination for the read event
 * @param Timeout               Maximum time for operation
 * @param GesturesWindow        Time of window for gestures recognition, set it for about 10 ms (more information in notes)
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ICtrlMan_WaitForRawEvent( oC_Screen_t Screen , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent, oC_Time_t Timeout , oC_Time_t GesturesWindow )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ICtrlMan))
    {
        if(
            ErrorCondition( isram(outEvent)                     , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Timeout >= 0 && GesturesWindow >= 0 , oC_ErrorCode_TimeNotCorrect           )
            )
        {
            oC_ICtrl_t ictrl = oC_ICtrlMan_GetICtrl(Screen);

            if(
                ErrorCondition( ictrl != NULL , oC_ErrorCode_ObjectNotFoundOnList           )
             && ErrorCode     ( oC_ICtrl_WaitForEvent(ictrl,EventsMask,outEvent,Timeout)    )
                )
            {
                errorCode = oC_ErrorCode_None;
                sleep(GesturesWindow);
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief returns next ICtrl object to create thread
 */
//==========================================================================================================================================
static oC_ICtrl_t GetNewICtrl( oC_Service_Context_t Context )
{
    oC_ICtrl_t ictrl = NULL;

    oC_List_TakeFirst(Context->ICtrlList,ictrl);

    return ictrl;
}

//==========================================================================================================================================
/**
 * @brief checks if someone is waiting for the event and informs it that it occurs
 */
//==========================================================================================================================================
static void AnnounceEvent( oC_ICtrl_t ICtrl , oC_IDI_Event_t * Event, uint32_t ActivityID )
{
    oC_IDI_EventId_t controllerId = Event->EventId & oC_IDI_EventId_AnyController;
    oC_IDI_EventId_t moveId       = Event->EventId & oC_IDI_EventId_AllMoves;
    oC_Screen_t      screen       = oC_ICtrl_GetScreen(ICtrl);

    oC_IntMan_EnterCriticalSection();

    ActivitiesCounter++;

    oC_Event_SetState(ActivitiesCounterEvent, ActivitiesCounter  );
    oC_Event_SetState(ActivityFinishedEvent , ActivityID         );

    oC_ARRAY_FOREACH_IN_ARRAY(ActivitiesWaiters,waiter)
    {
        if(waiter->Activity != NULL)
        {
            if(screen == waiter->Activity->Screen && Event->Timestamp >= waiter->RegistrationTimeStamp)
            {
                if(controllerId & waiter->Activity->EventsMask)
                {
                    if(
                        (waiter->Activity->EventsMask & oC_IDI_EventId_AllMoves) == oC_IDI_EventId_AnyMove
                     || (waiter->Activity->EventsMask & oC_IDI_EventId_AllMoves) == moveId
                        )
                    {
                        if(
                            Event->Position[0].X >= waiter->Activity->Position.X
                         && Event->Position[0].Y >= waiter->Activity->Position.Y
                         && Event->Position[0].X <= (waiter->Activity->Position.X + waiter->Activity->Width)
                         && Event->Position[0].Y <= (waiter->Activity->Position.Y + waiter->Activity->Height)
                            )
                        {
                            waiter->ActivityID = ActivityID;
                            memcpy(waiter->Event,Event,sizeof(oC_IDI_Event_t));
                            oC_Semaphore_Give(waiter->Semaphore);
                        }
                    }
                }
            }
        }
    }

    oC_IntMan_ExitCriticalSection();
}

//==========================================================================================================================================
/**
 * @brief starts service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartService( oC_Service_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification( &errorCode, oC_Module_ICtrlMan )
     && ErrorCondition( isram(outContext)  , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        oC_Service_Context_t context = malloc( sizeof(struct Context_t) , AllocationFlags_ZeroFill );

        if(ErrorCondition( context != NULL , oC_ErrorCode_AllocationError ))
        {
            context->ICtrlList = oC_List_New(getcurallocator(), AllocationFlags_Default);

            if(ErrorCondition( context->ICtrlList != NULL , oC_ErrorCode_AllocationError ))
            {
                context->ObjectControl = oC_CountObjectControl(context,oC_ObjectId_ICtrlManService);

                errorCode = oC_ErrorCode_None;

                oC_Semaphore_ForceValue(NewICtrlSemaphore,oC_List_Count(ICtrlList));

                foreach(ICtrlList,ictrl)
                {
                    bool pushed = oC_List_PushBack(context->ICtrlList,ictrl,getcurallocator());

                    ErrorCondition( pushed, oC_ErrorCode_CannotAddObjectToList );
                }
            }
            if(oC_ErrorOccur(errorCode))
            {
                bool listDeleted = context->ICtrlList == NULL || oC_List_Delete(context->ICtrlList,AllocationFlags_Default);
                oC_SaveIfFalse("cannot delete list", listDeleted , oC_ErrorCode_ReleaseError );
                oC_SaveIfFalse("cannot release service context memory", free(context,0), oC_ErrorCode_ReleaseError);
            }
            else
            {
                *outContext = context;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief stops service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StopService( oC_Service_Context_t *    Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(Context)                    , oC_ErrorCode_AddressNotInRam    )
     && ErrorCondition( IsServiceContextCorrect(*Context) , oC_ErrorCode_ObjectNotCorrect   )
        )
    {
        oC_Service_Context_t context = *Context;

        context->ObjectControl = 0;

        bool listDeleted    = oC_List_Delete(context->ICtrlList,AllocationFlags_Default);
        bool contextDeleted = free( context, 0 );

        if(ErrorCondition( listDeleted && contextDeleted , oC_ErrorCode_ReleaseError ))
        {
            *Context  = NULL;
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief main function of the service
 */
//==========================================================================================================================================
static oC_ErrorCode_t ServiceMain( oC_Service_Context_t Context )
{
    kdebuglog( oC_LogType_Info, "ictrlman service has been started\n" );

    while(1)
    {
        oC_ICtrlMan_ConfigureAllPossible();

        if(oC_Semaphore_TakeCounting(NewICtrlSemaphore,1,oC_day(365)))
        {
            oC_ICtrl_t ictrl = GetNewICtrl(Context);

            if(ictrl != NULL)
            {
                kdebuglog(oC_LogType_Track, "Received new ICtrl object (%s), creating new thread\n", oC_ICtrl_GetName(ictrl));

                oC_Thread_t thread = oC_Thread_New(0,CHILD_THREAD_STACK_SIZE,NULL,oC_ICtrl_GetName(ictrl),ChildThread,ictrl);

                if(thread != NULL)
                {
                    oC_Thread_SetFinishedFunction(thread,ChildThreadFinishedFunction,ictrl);

                    if(oC_Thread_Run(thread))
                    {
                        kdebuglog( oC_LogType_GoodNews, "New thread for ictrl '%s' has been created!\n", oC_ICtrl_GetName(ictrl) );
                    }
                    else
                    {
                        kdebuglog(oC_LogType_Error, "Cannot run thread for ictrl '%s'!\n", oC_ICtrl_GetName(ictrl));
                    }
                }
                else
                {
                    kdebuglog(oC_LogType_Error, "Cannot allocate memory for a new thread!\n");
                }
            }
            else
            {
                kdebuglog(oC_LogType_Error, "This is weird - we received NewICtrl semaphore, but there is not any new ICtrl object!\n");
            }
        }
        else
        {
            kdebuglog(oC_LogType_Error, "Cannot take semaphore, next attempt after 30 s.\n");
            sleep(s(30));
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief main thread for handling input controller events
 */
//==========================================================================================================================================
static void ChildThread( void * UserPointer )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_ICtrl_t     ictrl        = UserPointer;
    uint32_t       activityID   = 0;
    oC_Struct_Define(oC_IDI_Event_t, event);

    while( oC_SaveIfFalse("ictrl object: " , oC_ICtrl_IsCorrect(ictrl) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        activityID++;

        kdebuglog(oC_LogType_Info, "Waiting for new event");
        if( oC_ICtrl_IsEventSupportedByDriver(ictrl, oC_IDI_EventId_AnyController | oC_IDI_EventId_ClickWithoutRelease) )
        {
            errorCode = oC_ICtrl_WaitForEvent( ictrl, oC_IDI_EventId_AnyController | oC_IDI_EventId_ClickWithoutRelease, &event, oC_DynamicConfig_GetValue(ictrlman,WaitingForEventTimeout));
            if( oC_SaveIfErrorOccur("Click event not occurred", errorCode) )
            {
                kdebuglog(oC_LogType_Info, "Click without release event noticed");
                AnnounceEvent(ictrl, &event, activityID);
            }
        }

        errorCode = oC_ICtrl_WaitForEvent( ictrl, oC_IDI_EventId_AnyController | oC_IDI_EventId_AnyMove, &event, oC_DynamicConfig_GetValue(ictrlman,WaitingForEventTimeout));
        if( oC_SaveIfErrorOccur("event not occurred", errorCode) )
        {
            kdebuglog(oC_LogType_Info, "Some event noticed");
            AnnounceEvent(ictrl, &event, activityID);
        }
        else if(errorCode == oC_ErrorCode_Timeout || errorCode == oC_ErrorCode_UnknownGesture)
        {
            kdebuglog(oC_LogType_Track, "No move detected in %f seconds, next try after 1 second, error = %R\n", oC_DynamicConfig_GetValue(ictrlman,WaitingForEventTimeout) , errorCode);
            sleep(s(1));
        }
        else
        {
            kdebuglog(oC_LogType_Track, "Cannot read gesture: %R\n", errorCode);
            break;
        }
    }

}

//==========================================================================================================================================
/**
 * @brief function called at the end of the input controller thread
 */
//==========================================================================================================================================
static void ChildThreadFinishedFunction ( oC_Thread_t Thread , void * UserParameter )
{
    oC_ICtrl_t ictrl = UserParameter;

    kdebuglog( oC_LogType_Warning, "thread for ICtrl named '%s' has been finished!\n", oC_ICtrl_GetName(ictrl) );
}

//==========================================================================================================================================
/**
 * @brief reverts registration when the thread is finished
 */
//==========================================================================================================================================
static bool RevertRegistration( oC_Thread_t Thread , void * Object , uint32_t Parameter )
{
    return oC_SaveIfErrorOccur("Remove Registration", oC_ICtrlMan_UnregisterActivity(Object));
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________



