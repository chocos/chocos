/** ****************************************************************************************************************************************
 *
 * @file          oc_screen.c
 *
 * @brief        __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 21.07.2016 20:42:12) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_screen.h>
#include <oc_object.h>
#include <oc_string.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores screen object data
 */
//==========================================================================================================================================
struct Screen_t
{
    oC_ObjectControl_t      ObjectControl;          //!< Magic number counted by the Object module. It is for the Screen verification
    oC_Driver_t             Driver;                 //!< Pointer to the graphic driver that handles the screen
    char                    Name[30];               //!< Name of the screen - useful for screen identification
    void *                  Config;                 //!< Pointer to the screen configuration. Note, that it is a copy of the configuration given as #oC_Screen_New argument
    void *                  Context;                //!< Pointer to the driver context. It is created and allocated by the driver. Only for internal use
    bool                    Configured;             //!< This flag is set to true if the driver configuration was done and was finished with successful result
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief the allocator definition for identify the module allocations
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "Screen"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief creates new screen object
 *
 * The function allocates and initializes new #oC_Screen_t object. The object is always allocated with the screen allocator. When the
 * #oC_Screen_t is not needed anymore, it should be destroyed by the function named #oC_Screen_Delete.
 *
 * This function does not configures the driver to work. It should be done manually by calling the function #oC_Screen_Configure.
 *
 * If the error occurs, you can use function '#oC_ReadLastError' to read it or just use the chell's command "system".
 *
 * @param Driver        Pointer to the graphics driver that should be used for screen configuration and managing
 * @param Config        Pointer to the driver configuration. It will be copied
 * @param Name          Friendly name of the screen
 *
 * @return pointer to the initialized and allocated screen object or NULL if there was some error.
 */
//==========================================================================================================================================
oC_Screen_t oC_Screen_New( oC_Driver_t Driver , const void * Config , const char * Name )
{
    oC_Screen_t     screen    = NULL;
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;

    if(
       ErrorCondition( isaddresscorrect(Driver)                             , oC_ErrorCode_WrongAddress        )
    && ErrorCondition( oC_Driver_GetDriverType(Driver) == GRAPHICS_DRIVER   , oC_ErrorCode_NotGraphicDriver    )
    && ErrorCondition( isaddresscorrect(Driver)                             , oC_ErrorCode_WrongAddress        )
    && ErrorCondition( isaddresscorrect(Name)                               , oC_ErrorCode_WrongAddress        )
        )
    {
        screen = kmalloc(sizeof(struct Screen_t) , &Allocator , AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill);

        if(screen != NULL)
        {
            screen->ObjectControl    = oC_CountObjectControl(screen,oC_ObjectId_Screen);
            screen->Driver           = Driver;
            screen->Config           = kmalloc(Driver->ConfigurationSize,&Allocator, AllocationFlags_CanWait1Second);
            screen->Context          = NULL;
            screen->Configured       = false;

            if(screen->Config != NULL)
            {
                strncpy(screen->Name,Name,sizeof(screen->Name));
                memcpy(screen->Config,Config,Driver->ConfigurationSize);
            }
            else
            {
                oC_SaveIfFalse("Screen::new: cannot release screen memory", kfree(screen,AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
                oC_SaveError("Screen::new: cannot allocate memory for config" , oC_ErrorCode_AllocationError);
            }
        }
        else
        {
            oC_SaveError("Screen::new: cannot allocate memory for screen" , oC_ErrorCode_AllocationError);
        }
    }
    else
    {
        oC_SaveError("Screen::new: Cannot create screen - " , errorCode);
    }

    return screen;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief checks if the Screen object is correct
 *
 * The function is for verification if the Screen object is correct. The function returns true if the address is correct and object is initialized.
 *
 * @param Screen        Pointer to the screen object
 *
 * @return true if object is correct
 */
//==========================================================================================================================================
bool oC_Screen_IsCorrect( oC_Screen_t Screen )
{
    return isram(Screen) && oC_CheckObjectControl(Screen,oC_ObjectId_Screen,Screen->ObjectControl);
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief   Destroys the Screen object
 *
 * The function is for destroying the Screen object. It also deconfigures the graphic driver.
 *
 * @warning
 * The function takes pointer to the Screen object pointer. Thanks to that it also fills your screen pointer with NULL value after destroying
 * to prevent usage of the object after destroying. If you give wrong pointer to this function, the function will not delete it and it will
 * return "false" value. Please verify, that the function returns true, otherwise it can causes be memory leak.
 *
 * The function returns only result of the operation (success or failure), so if you want to know what kind of error occurs you can use function '#oC_ReadLastError' to read it or just use the chell's command "system".
 *
 * @param Screen           Pointer to the screen created before by using the function called #oC_Screen_New
 *
 * @return true if success
 */
//==========================================================================================================================================
bool oC_Screen_Delete( oC_Screen_t * Screen )
{
    bool deleted = false;

    if(isaddresscorrect(Screen) && oC_Screen_IsCorrect(*Screen))
    {
        oC_Screen_t localScreen = *Screen;

        if(localScreen->Configured == true)
        {
            oC_SaveIfErrorOccur("Screen::delete: Cannot unconfigure screen - " , oC_Driver_Unconfigure(localScreen->Driver,localScreen->Config,&localScreen->Context));
        }

        deleted = kfree(localScreen->Config,AllocationFlags_CanWait1Second);

        if(!deleted)
        {
            oC_SaveError("Screen::delete: Cannot release config memory - " , oC_ErrorCode_ReleaseError);
        }

        if(kfree(localScreen,AllocationFlags_CanWait1Second))
        {
            deleted = deleted && true;
        }
        else
        {
            oC_SaveError("Screen::delete: Cannot release screen memory - " , oC_ErrorCode_ReleaseError);
        }
    }

    return deleted;
}


//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief checks if the screen is configured to work
 *
 * The function is for verification if the driver associated with the screen was correctly configured before. The function returns true only
 *  if the screen is correct, and the driver configuration function has been called by using the Screen object (#oC_Screen_Configure). If the
 *  driver is configured without using the Screen object, the screen object will be not usable.
 *
 * @param Screen        The screen allocated and initialized before by function #pC_Screen_New
 *
 * @return true if driver was configured and the screen is correct
 */
//==========================================================================================================================================
bool oC_Screen_IsConfigured( oC_Screen_t Screen )
{
    return oC_Screen_IsCorrect(Screen) && Screen->Configured;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief configures graphics driver to work
 *
 * The function is for configuration of the graphic drivers to work with the screen. You can check if the driver was configured before by
 * calling the function #oC_Screen_IsConfigured. The screen will work only if the graphic driver is configured by calling this function
 * (it cannot be configured manually).
 *
 * If the driver is already correctly configured, the function will return error #oC_ErrorCode_AlreadyConfigured. To unconfigure driver
 * please call the function #oC_Screen_Unconfigure.
 *
 * @param Screen        The Screen object created by the function #oC_Screen_New
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Screen_Configure( oC_Screen_t Screen )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Screen_IsCorrect(Screen)  , oC_ErrorCode_ObjectNotCorrect   )
     && ErrorCondition( Screen->Configured == false   , oC_ErrorCode_AlreadyConfigured  )
     && oC_AssignErrorCode(&errorCode , oC_Driver_Configure(Screen->Driver,Screen->Config,&Screen->Context))
        )
    {
        Screen->Configured  = true;
        errorCode           = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief unconfigures graphics driver
 *
 * The function is for unconfiguration of the driver associated with the screen.. When this function is called, the context of the driver
 * should be destroyed, and the screen is not usable.
 *
 * If the driver is not configured, the function will return #oC_ErrorCode_NotConfiguredYet
 *
 * @param Screen        The screen object created by the function #oC_Screen_New
 *
 * @return code of error or #oc_ErrorCode_None if success
 *
 * @see oC_Screen_IsConfigured , oC_Screen_Configure
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Screen_Unconfigure( oC_Screen_t Screen )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Screen_IsCorrect(Screen)    , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( Screen->Configured == true      , oC_ErrorCode_NotConfiguredYet )
     && oC_AssignErrorCode(&errorCode , oC_Driver_Unconfigure(Screen->Driver,Screen->Config,&Screen->Context))
        )
    {
        Screen->Configured = false;
        errorCode          = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief switches active layer in the graphics driver
 *
 * The function is for switching layer in the graphics driver
 *
 * @param Screen        The screen object created by the function #oC_Screen_New
 *
 * @return code of error or #oc_ErrorCode_None if success
 *
 * @see oC_Screen_IsConfigured , oC_Screen_Configure
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Screen_SwitchLayer( oC_Screen_t Screen , oC_ColorMap_LayerIndex_t Layer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Screen_IsCorrect(Screen)     , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( Screen->Configured == true      , oC_ErrorCode_NotConfiguredYet )
        )
    {

        errorCode = oC_Driver_SwitchLayer(Screen->Driver,Screen->Context,Layer);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief reads pointer to the color map
 *
 * The function returns color map that can be used for drawing on the screen. Before usage of this function you have to call the function
 * #oC_Screen_Configure function.
 *
 * @param Screen            The screen object created by the function #oC_Screen_New and configured by the function #oC_Screen_Configure
 * @param outColorMap       Destination pointer for the color map pointer
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Screen_ReadColorMap( oC_Screen_t Screen , oC_ColorMap_t ** outColorMap )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Screen_IsCorrect(Screen)     , oC_ErrorCode_ObjectNotCorrect      )
     && ErrorCondition( Screen->Configured == true      , oC_ErrorCode_NotConfiguredYet      )
     && ErrorCondition( isram(outColorMap)              , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        errorCode = oC_Driver_ReadColorMap(Screen->Driver,Screen->Context,outColorMap);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief reads resolution of the screen
 *
 * The function is for reading resolution of the screen (width and the height). The graphics driver associated with the screen object have to
 * be configured by the function #oC_Screen_Configure.
 *
 * @param Screen        Screen object created by the function #oC_Screen_New and configured by #oC_Screen_Configure
 * @param outWidth      Destination for the width of the screen
 * @param outHeight     Destination for the height of the screen
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Screen_ReadResolution( oC_Screen_t Screen , oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Screen_IsCorrect(Screen)    , oC_ErrorCode_ObjectNotCorrect      )
     && ErrorCondition( Screen->Configured == true      , oC_ErrorCode_NotConfiguredYet      )
     && ErrorCondition( isram(outWidth)                 , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( isram(outHeight)                , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        errorCode = oC_Driver_ReadResolution(Screen->Driver,Screen->Context,outWidth,outHeight);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief returns name of the screen
 *
 * The function returns name of the screen.
 *
 * @param Screen        Screen object created by the function #oC_Screen_New and configured by #oC_Screen_Configure
 *
 * @return string with name of the screen
 */
//==========================================================================================================================================
const char * oC_Screen_GetName( oC_Screen_t Screen )
{
    const char * name = "unknown";

    if(oC_Screen_IsCorrect(Screen))
    {
        name = Screen->Name;
    }
    else
    {
        name = "incorrect";
    }

    return name;
}

//==========================================================================================================================================
/**
 * @ingroup Screen
 *
 * @brief returns driver associated with the screen
 *
 * The function returns driver associated with the screen or NULL if some error has occured.
 *
 * @param Screen        Screen object created by the function #oC_Screen_New
 *
 * @return pointer to the driver
 */
//==========================================================================================================================================
oC_Driver_t oC_Screen_GetDriver( oC_Screen_t Screen )
{
    oC_Driver_t driver = NULL;

    if(oC_Screen_IsCorrect(Screen))
    {
        driver = Screen->Driver;
    }

    return driver;
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
