/** ****************************************************************************************************************************************
 *
 * @file       oc_eth.c
 * 
 * File based on driver.c Ver 1.1.0
 *
 * @brief      The file with source for ETH driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2016-07-28 - 21:37:41)
 *
 * @copyright  Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_eth.h>
#include <oc_eth_lld.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_gpio.h>
#include <oc_stdio.h>
#include <oc_string.h>
#include <oc_eth_phy.h>
#include <oc_debug.h>
#include <oc_mutex.h>
#include <oc_ktime.h>
#include <oc_math.h>

#ifdef oC_ETH_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         ETH
#define DRIVER_FILE_NAME    "eth"
#define DRIVER_TYPE         NETWORK_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager | oC_Boot_Level_RequireNetwork

/* Driver interface definitions */
#define DRIVER_CONFIGURE        oC_ETH_Configure
#define DRIVER_UNCONFIGURE      oC_ETH_Unconfigure
#define DRIVER_TURN_ON          oC_ETH_TurnOn
#define DRIVER_TURN_OFF         oC_ETH_TurnOff
#define IS_TURNED_ON            oC_ETH_IsTurnedOn
#define SEND_FRAME              oC_ETH_SendFrame
#define RECEIVE_FRAME           oC_ETH_ReceiveFrame
#define SET_WAKE_ON_LAN_EVENT   oC_ETH_SetWakeOnLanEvent
#define FLUSH                   oC_ETH_Flush
#define SET_LOOPBACK            oC_ETH_SetLoopback
#define PERFORM_DIAGNOSTIC      oC_ETH_PerformDiagnostics
#define READ_NET_INFO           oC_ETH_ReadNetInfo

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores ETH context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t       ObjectControl;
    oC_ETH_LLD_Result_t      Result;             //!< Result of LLD configuration
    oC_ETH_ChipName_t        ChipName;           //!< Name of the used chip
    oC_ETH_PhyAddress_t      PhyAddress;         //!< Address of the external PHY
    oC_Net_InterfaceName_t   InterfaceName;      //!< Name of the interface
    oC_MemorySize_t          ReceivedBytes;      //!< Number of received bytes
    oC_MemorySize_t          TransmittedBytes;   //!< Number of transmitted bytes
    oC_Net_MacAddress_t      MacAddress;         //!< MAC address of the interface
    oC_BaudRate_t            BaudRate;           //!< Baud Rate of the interface
    oC_ETH_LLD_Descriptor_t  RxDescriptorRing;   //!< Pointer to the RX Descriptor ring (List of DMA descriptors required for receiving packets)
    oC_ETH_LLD_Descriptor_t  TxDescriptorRing;   //!< Pointer to the TX Descriptor ring (List of DMA descriptors required for sending packets)
    oC_Mutex_t               ContextBusyMutex;   //!< Pointer to the Mutex available when the Ethernet context is busy
    oC_Net_Layer_t           LoopbackLayer;      //!< Layer of enabled loopback or 0 if not enabled
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsContextCorrect                ( oC_ETH_Context_t Context );
static oC_ErrorCode_t           CreateContext                   ( const oC_ETH_Config_t * Config , oC_ETH_Context_t * outContext );
static oC_ErrorCode_t           DeleteContext                   ( oC_ETH_Context_t * outContext );
static oC_ErrorCode_t           InitializePhy                   ( const oC_ETH_Config_t * Config , oC_ETH_Context_t Context );
static oC_ErrorCode_t           ReleasePhy                      ( const oC_ETH_Config_t * Config , oC_ETH_Context_t Context );
static oC_Net_LinkStatus_t      GetLinkStatus                   ( oC_ETH_Context_t Context );
static oC_ErrorCode_t           AllocateDescriptors             ( oC_ETH_Context_t Context );
static oC_ErrorCode_t           ReleaseDescriptors              ( oC_ETH_Context_t Context );
static void                     InterruptHandler                ( oC_ETH_LLD_InterruptSource_t Source );
static oC_ErrorCode_t           SendFrame                       ( oC_ETH_Context_t Context , const oC_Net_MacAddress_t Source, const oC_Net_MacAddress_t Destination , const void * Data , uint16_t Size , oC_ETH_LLD_FrameSegment_t FrameSegment , oC_Net_FrameType_t FrameType , oC_Time_t Timeout );
static oC_ErrorCode_t           ReceiveFrame                    ( oC_ETH_Context_t Context , oC_Net_MacAddress_t outSource, oC_Net_MacAddress_t outDestination, void * Data, uint16_t * Size, oC_ETH_LLD_FrameSegment_t * outFrameSegment , oC_Net_FrameType_t * outFrameType , oC_Time_t Timeout  );

static oC_ErrorCode_t           PerformPhyRegisterAccessTest    ( oC_Diag_t * Diag , void * Context );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all ETH driver kernel allocations.
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "eth module"
};
//==========================================================================================================================================
/**
 * Event active when data is ready to read
 */
//==========================================================================================================================================
static oC_Event_t       DataReadyToRead = NULL;
//==========================================================================================================================================
/**
 * Event active when module is ready to send
 */
//==========================================================================================================================================
static oC_Event_t       ModuleReadyToSend = NULL;
//==========================================================================================================================================
/**
 * Array with list of supported diagnostics
 */
//==========================================================================================================================================
static const oC_Diag_SupportedDiagData_t       SupportedDiagsArray[]               = {
                { .Name = "PHY register access test" , .PerformFunction = PerformPhyRegisterAccessTest } ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
/**
 * @addtogroup ETH
 * @{
 */

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the ETH module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 * It also turns on the LLD layer.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_ETH))
    {

        errorCode = oC_ETH_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_ErrorCode_None;
        }

        if(!oC_ErrorOccur(errorCode))
        {
            DataReadyToRead   = oC_Event_New(oC_Event_State_Inactive, &Allocator, AllocationFlags_ZeroFill);
            ModuleReadyToSend = oC_Event_New(oC_Event_State_Active,   &Allocator, AllocationFlags_ZeroFill);

            if(
                ErrorCode       ( oC_ETH_LLD_SetInterruptHandler( InterruptHandler )      )
             && ErrorCondition  ( DataReadyToRead   != NULL, oC_ErrorCode_AllocationError )
             && ErrorCondition  ( ModuleReadyToSend != NULL, oC_ErrorCode_AllocationError )
                )
            {
                /* This must be always at the end of the function */
                oC_Module_TurnOn(oC_Module_ETH);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfFalse("ETH:TurnOn - Cannot delete event DataReadyToRead"  , oC_Event_Delete(&DataReadyToRead,   AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError);
                oC_SaveIfFalse("ETH:TurnOn - Cannot delete event ModuleReadyToSend", oC_Event_Delete(&ModuleReadyToSend, AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the ETH driver
 *
 * The function for turning off the ETH driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_ETH);

        errorCode = oC_ETH_LLD_TurnOffDriver();

        if(
            errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {

            errorCode = oC_ErrorCode_None;
        }

        oC_SaveIfFalse("ETH:TurnOn - Cannot delete event DataReadyToRead"  , oC_Event_Delete(&DataReadyToRead,   AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError);
        oC_SaveIfFalse("ETH:TurnOn - Cannot delete event ModuleReadyToSend", oC_Event_Delete(&ModuleReadyToSend, AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_ETH_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_ETH);
}

//==========================================================================================================================================
/**
 * @brief configures ETH pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_ETH_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * Standard possible error codes:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_ETH_TurnOn function
 *  oC_ErrorCode_WrongConfigAddress              | `Config` pointer stores address that is not correct
 *  oC_ErrorCode_ChipNotDefined                  | `Config` structure does not contain PHY chip info pointer, or it is not correct
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outContext` does not store address in the machine RAM section
 *  oC_ErrorCode_ChipNameNotDefined              | Name of the chip is not defined in the #oC_ETH_ChipInfo_t
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_Configure( const oC_ETH_Config_t * Config , oC_ETH_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        oC_ETH_Context_t context = NULL;

        if(
           ErrorCondition( isaddresscorrect(Config)                             , oC_ErrorCode_WrongConfigAddress           )
        && ErrorCondition( isaddresscorrect(Config->PhyChipInfo)                , oC_ErrorCode_ChipNotDefined               )
        && ErrorCondition( isram(outContext)                                    , oC_ErrorCode_OutputAddressNotInRAM        )
        && ErrorCode     ( CreateContext(Config,&context)                                                                   )
           )
        {
            if(
                ErrorCode( oC_ETH_LLD_InitializeMac         ( &Config->LLD , &Config->PhyChipInfo->LLD , &context->Result                               ))
             && ErrorCode( InitializePhy                    ( Config, context                                                                           ))
             && ErrorCode( AllocateDescriptors              ( context                                                                                   ))
             && ErrorCode( oC_ETH_LLD_InitializeDescriptors ( &context->Result , &Config->LLD, context->TxDescriptorRing , context->RxDescriptorRing    ))
             && ErrorCode( oC_ETH_LLD_InitializeDma         ( &context->Result , &Config->LLD, context->TxDescriptorRing , context->RxDescriptorRing    ))
             && ErrorCode( oC_ETH_LLD_SetMacAddress         ( &context->Result , 0                         , Config->MacAddress                         ))
             && ErrorCode( oC_ETH_LLD_Start                 ( &Config->LLD                                                                              ))
                )
            {
                *outContext = context;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfErrorOccur("ETH: Cannot release PHY    - ", ReleasePhy( Config , context ));
                oC_SaveIfErrorOccur("ETH: Cannot release MAC    - ", oC_ETH_LLD_ReleaseMac( &Config->LLD , &Config->PhyChipInfo->LLD , &context->Result ));
                oC_SaveIfErrorOccur("ETH: Cannot delete context - ", DeleteContext( &context ));
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_ETH_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_Unconfigure( const oC_ETH_Config_t * Config , oC_ETH_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        if(
            ErrorCondition( isaddresscorrect(Config)      , oC_ErrorCode_WrongConfigAddress                             )
         && ErrorCondition( isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM                          )
         && ErrorCondition( IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect                              )
         && ErrorCode     ( ReleasePhy(Config,*outContext)                                                              )
         && ErrorCode     ( oC_ETH_LLD_ReleaseMac(&Config->LLD, &Config->PhyChipInfo->LLD, &(*outContext)->Result)      )
         && ErrorCode     ( ReleaseDescriptors(*outContext)                                                             )
         && ErrorCode     ( DeleteContext(outContext)                                                                   )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends frame via ETH
 *
 * The function is for sending a frame by using Ethernet interface.
 *
 * @param Context       Context of the driver received from the #oC_ETH_Configure function
 * @param Frame         Frame to send
 * @param Timeout       Maximum time for operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_ETH_TurnOn function
 *  oC_ErrorCode_ContextNotCorrect               | `Context` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_WrongAddress                    | `Frame` or `Frame->Packet` points to the wrong address
 *  oC_ErrorCode_SizeNotCorrect                  | `Frame->Size` is 0 (no data to send)
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is smaller than 0
 *  oC_ErrorCode_Timeout                         | `Timeout` has expired
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_SendFrame( oC_ETH_Context_t Context , const oC_Net_Frame_t * Frame , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)           , oC_ErrorCode_ContextNotCorrect       )
         && ErrorCondition( isaddresscorrect(Frame)             , oC_ErrorCode_WrongAddress            )
         && ErrorCondition( isaddresscorrect(Frame->Packet)     , oC_ErrorCode_WrongAddress            )
         && ErrorCondition( Frame->Size > 0                     , oC_ErrorCode_SizeNotCorrect          )
         && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect          )
         && ErrorCondition( Frame->FrameType > 0                , oC_ErrorCode_FrameTypeNotSupported   )
            )
        {
            uint8_t *                   data            = (uint8_t*)Frame->Packet;
            uint16_t                    size            = oC_MIN(Frame->Size , IEEE_802_3_ETHERNET_MTU);
            uint16_t                    sentBytes       = 0;
            uint16_t                    bytesToSend     = Frame->Size;
            oC_ETH_LLD_FrameSegment_t   frameSegment    = Frame->Size > IEEE_802_3_ETHERNET_MTU ? oC_ETH_LLD_FrameSegment_First : oC_ETH_LLD_FrameSegment_Single;
            oC_Timestamp_t              startTimestamp  = oC_KTime_GetCurrentTick();
            oC_Timestamp_t              endTimestamp    = startTimestamp + Timeout;

            errorCode = oC_ErrorCode_None;

            while(
                   bytesToSend > 0
                && ErrorCode     ( SendFrame( Context, Context->MacAddress , Frame->Destination.MacAddress , &data[sentBytes], size, frameSegment, Frame->FrameType, Timeout)   )
                && ErrorCondition( endTimestamp > oC_KTime_GetTimestamp() , oC_ErrorCode_Timeout                                                                                )
                    )
            {
                sentBytes   += size;
                size         = oC_MIN(bytesToSend , IEEE_802_3_ETHERNET_MTU);
                bytesToSend  = (bytesToSend > sentBytes) ? bytesToSend - sentBytes : 0;
                frameSegment    = bytesToSend < IEEE_802_3_ETHERNET_MTU ? oC_ETH_LLD_FrameSegment_Last : oC_ETH_LLD_FrameSegment_Middle;

                Timeout      = endTimestamp - oC_KTime_GetCurrentTick();
            }

            Context->TransmittedBytes += sentBytes;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives frame via ETH
 *
 * The function is for receiving a frame by using Ethernet interface.
 *
 * @warning
 * `outFrame` has to have filled field `Allocator`. The function will allocate memory required for the frame and it should be released when
 * it is not needed anymore
 *
 * @param Context       Context of the driver received from the #oC_ETH_Configure function
 * @param outFrame      Destination for received frame
 * @param Timeout       Maximum time for operation
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_ETH_TurnOn function
 *  oC_ErrorCode_ContextNotCorrect               | `Context` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outFrame` or `outFrame->Packet` is not in RAM
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` is smaller than 0
 *  oC_ErrorCode_Timeout                         | `Timeout` has expired
 *  oC_ErrorCode_SizeNotCorrect                  | `outFrame->Size` is not correct
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_ReceiveFrame( oC_ETH_Context_t Context , oC_Net_Frame_t * outFrame , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)               , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( isram(outFrame)                         , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Timeout >= 0                            , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( isram(outFrame->Packet)                 , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( outFrame->Size > 0                      , oC_ErrorCode_SizeNotCorrect           )
            )
        {
            if(ErrorCondition( oC_Mutex_Take(Context->ContextBusyMutex,Timeout) , oC_ErrorCode_Timeout  ))
            {
                uint8_t *                   buffer          = (uint8_t*)outFrame->Packet;
                uint16_t                    receivedBytes   = 0;
                oC_Time_t                   startTimestamp  = oC_KTime_GetTimestamp();
                oC_Time_t                   endTimestamp    = startTimestamp + Timeout;
                uint16_t                    leftBufferSize  = outFrame->Size;
                oC_ETH_LLD_FrameSegment_t   frameSegment       = oC_ETH_LLD_FrameSegment_NumberOfFrameSegments;
                bool                        frameStarted    = false;
                oC_Net_FrameType_t          frameType       = 0;

                while(
                       ErrorCondition( receivedBytes < outFrame->Size          , oC_ErrorCode_OutputBufferTooSmall  )
                    && ErrorCondition( oC_KTime_GetTimestamp() <= endTimestamp , oC_ErrorCode_Timeout               )
                       )
                {
                    if( ErrorCode( ReceiveFrame( Context, outFrame->Source.MacAddress, outFrame->Destination.MacAddress, &buffer[receivedBytes], &leftBufferSize, &frameSegment, &frameType, Timeout ) ) )
                    {
                        Context->ReceivedBytes += leftBufferSize;

                        if((frameSegment & oC_ETH_LLD_FrameSegment_First) || frameStarted)
                        {
                            if(frameSegment & oC_ETH_LLD_FrameSegment_First)
                            {
                                outFrame->Source.Filled      = true;
                                outFrame->Destination.Filled = true;
                                outFrame->FrameType          = frameType;
                            }
                            frameStarted    = true;
                            receivedBytes  += leftBufferSize;

                            if(frameSegment & oC_ETH_LLD_FrameSegment_Last)
                            {
                                errorCode = oC_ErrorCode_None;
                                break;
                            }
                        }
                        leftBufferSize = outFrame->Size - receivedBytes;
                        kdebuglog( oC_LogType_Info, "ETH: skipping not started frame from 0x%X\n", outFrame->Packet->IPv4.Header.SourceIp );
                    }

                    Timeout = endTimestamp - oC_KTime_GetTimestamp();
                }

                oC_Mutex_Give(Context->ContextBusyMutex);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_SetWakeOnLanEvent( oC_ETH_Context_t Context , oC_Event_t WolEvent )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_Flush( oC_ETH_Context_t Context )
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
/**
 * @brief enables or disables loopback-mode at the given layer
 *
 * The function is for enabling/disabling loopback mode at the given layer
 *
 * @param Context       Context of the driver received from the #oC_ETH_Configure function
 * @param Layer         Layer of the network OSI model
 * @param Enabled       true if loopback should be enabled
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_ETH_TurnOn function
 *  oC_ErrorCode_ContextNotCorrect               | `Context` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_WrongLayer                      | `Layer` points to not supported layer
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_SetLoopback( oC_ETH_Context_t Context , oC_Net_Layer_t Layer , bool Enabled )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)                               , oC_ErrorCode_ContextNotCorrect )
         && ErrorCondition( oC_Net_IsMainLayer(Layer,oC_Net_Layer_Physical) ||
                            oC_Net_IsMainLayer(Layer,oC_Net_Layer_DataLink)         , oC_ErrorCode_WrongLayer        )
            )
        {
            if(oC_Net_IsMainLayer(Layer,oC_Net_Layer_Physical))
            {
                if(
                    ErrorCode( oC_ETH_SetPhyLoopback(Context->PhyAddress,Enabled) )
                    )
                {
                    Context->LoopbackLayer = (Enabled) ? Layer : 0;
                    errorCode              = oC_ErrorCode_None;
                }
            }
            else
            {
                if(
                    ErrorCode( oC_ETH_LLD_SetMacLoopback(Enabled) )
                    )
                {
                    Context->LoopbackLayer = (Enabled) ? Layer : 0;
                    errorCode              = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief performs diagnostics
 *
 * The function performs Ethernet driver diagnostics
 *
 * @param Context           Context of the driver received from the #oC_ETH_Configure function
 * @param Diags             Pointer to the array for Diagnostics informations (you can fill Diags->PrintFunction if you want to print message during diagnostics). It also can be set to #NULL if you want to only know number of supported diags
 * @param NumberOfDiags     On input it is size of the Diags array, on output it is number of supported diags
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_ETH_TurnOn function
 *  oC_ErrorCode_ContextNotCorrect               | `Context` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_OutputAddressNotInRAM           | `Diags` does not point to NULL nor RAM
 *  oC_ErrorCode_OutputAddressNotInRAM           | `NumberOfDiags` does not point to RAM
 *  oC_ErrorCode_WrongAddress                    | `Diags->PrintFunction` is not NULL nor correct address
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_PerformDiagnostics( oC_ETH_Context_t Context , oC_Diag_t * Diags , uint32_t * NumberOfDiags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)                                                                   , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( isram(NumberOfDiags)                                                                        , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Diags == NULL || isram(Diags)                                                               , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Diags == NULL || Diags->PrintFunction == NULL || isaddresscorrect(Diags->PrintFunction)     , oC_ErrorCode_WrongAddress             )
            )
        {
            if(Diags != NULL)
            {
                uint32_t diagsSize = *NumberOfDiags;

                if( ErrorCondition( oC_Mutex_Take(Context->ContextBusyMutex, s(1)) , oC_ErrorCode_Timeout       ) )
                {
                    if(ErrorCode( oC_ETH_LLD_PerformDiagnostics( &Context->Result, Diags, NumberOfDiags )   ))
                    {
                        errorCode       = oC_Diag_PerformDiagnostics( SupportedDiagsArray, &Diags[*NumberOfDiags], diagsSize - (*NumberOfDiags), Context->ChipName, Context );
                        *NumberOfDiags += oC_Diag_GetNumberOfSupportedDiagnostics(SupportedDiagsArray);
                    }
                    oC_Mutex_Give(Context->ContextBusyMutex);
                }

            }
            else
            {
                *NumberOfDiags = 0;

                if( ErrorCode( oC_ETH_LLD_PerformDiagnostics( &Context->Result, NULL, NumberOfDiags) ) )
                {
                    *NumberOfDiags += oC_Diag_GetNumberOfSupportedDiagnostics(SupportedDiagsArray);
                    errorCode       = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns informations about the interface
 *
 * The function reads informations about the given network interface, that is associated with the driver and returns it by using a memory
 * address stored in the `outInfo` variable.
 *
 * @param Context       Context of the driver received from the #oC_ETH_Configure function
 * @param outInfo       Destination for the network interface informations
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_ETH_TurnOn function
 *  oC_ErrorCode_ContextNotCorrect               | `Context` is not correct (address is not correct or not valid anymore)
 *  oC_ErrorCode_OutputAddressNotInRAM           | `outInfo` points to the address that is not stored in the RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_ReadNetInfo( oC_ETH_Context_t Context , oC_Net_Info_t * outInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_ETH))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)       , oC_ErrorCode_ContextNotCorrect            )
         && ErrorCondition( isram(outInfo)                  , oC_ErrorCode_OutputAddressNotInRAM        )
            )
        {
            strncpy(outInfo->InterfaceName,                 Context->InterfaceName, sizeof(outInfo->InterfaceName   ));
            memcpy( outInfo->HardwareAddress.MacAddress,    Context->MacAddress,    sizeof(outInfo->HardwareAddress ));

            oC_Net_MacAddressFromString("FF:FF:FF:FF:FF:FF", outInfo->HardwareBroadcastAddress.MacAddress);

            outInfo->HardwareBroadcastAddress.Filled    = true;
            outInfo->HardwareAddress.Filled             = true;
            outInfo->DriverName                         = ETH.FileName;
            outInfo->InterfaceIndex                     = (oC_Net_InterfaceIndex_t)Context->PhyAddress;
            outInfo->LinkStatus                         = GetLinkStatus(Context);
            outInfo->ReceivedBytes                      = Context->ReceivedBytes;
            outInfo->TransmittedBytes                   = Context->TransmittedBytes;
            outInfo->BaudRate                           = Context->BaudRate;
            outInfo->HardwareAddressLength              = sizeof(oC_Net_MacAddress_t);
            outInfo->HardwareType                       = oC_Net_HardwareType_Ethernet;

            if(ErrorCode(oC_ETH_ReadLinkStatus(Context->PhyAddress,&outInfo->LinkStatus)))
            {
                if(outInfo->LinkStatus == oC_Net_LinkStatus_Up && Context->LoopbackLayer == 0)
                {
                    errorCode = oC_ETH_SetAutoNegotiation(Context->PhyAddress,true,ms(500));
                }
                else
                {
                    errorCode = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the context is correct for ETH driver
 *
 * The function is for checking if the context is correct context for the ETH driver. The ETH driver must not be enabled to use it.
 *
 * @param Context   Context of the ETH driver to check
 *
 * @return true if the Context is correct
 */
//==========================================================================================================================================
bool oC_ETH_IsContextCorrect( oC_ETH_Context_t Context )
{
    return IsContextCorrect(Context);
}

/** @} */
#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the ETH driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_ETH_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_ETHContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief creates context of the driver
 */
//==========================================================================================================================================
static oC_ErrorCode_t CreateContext( const oC_ETH_Config_t * Config , oC_ETH_Context_t * outContext )
{
    oC_ErrorCode_t    errorCode = oC_ErrorCode_ImplementError;
    oC_ETH_Context_t  context   = kmalloc( sizeof(struct Context_t), &Allocator, AllocationFlags_ZeroFill );

    if(
        ErrorCondition( context != NULL                             , oC_ErrorCode_AllocationError      )
     && ErrorCondition( strlen(Config->PhyChipInfo->Name) > 0       , oC_ErrorCode_ChipNameNotDefined   )
        )
    {
        context->ObjectControl      = oC_CountObjectControl(context,oC_ObjectId_ETHContext);
        context->PhyAddress         = Config->PhyAddress;
        context->BaudRate           = 0;
        context->ContextBusyMutex   = oC_Mutex_New(oC_Mutex_Type_Normal,&Allocator,AllocationFlags_CanWait1Second);
        context->LoopbackLayer      = 0;

        if(ErrorCondition( context->ContextBusyMutex != NULL, oC_ErrorCode_AllocationError ))
        {
            strcpy(context->ChipName,Config->PhyChipInfo->Name);
            memcpy(context->MacAddress, Config->MacAddress, sizeof(oC_Net_MacAddress_t));

            *outContext            = context;
            errorCode              = oC_Net_PrepareInterfaceName(&context->InterfaceName,context->ChipName,context->PhyAddress);
        }
        else
        {
            oC_SaveIfFalse("ETH::CreateContext - Cannot release context memory - ", kfree(context,AllocationFlags_CanWait1Second), oC_ErrorCode_ReleaseError);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief deletes context of the driver
 */
//==========================================================================================================================================
static oC_ErrorCode_t DeleteContext( oC_ETH_Context_t * outContext )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_ETH_Context_t    context   = *outContext;

    context->ObjectControl = 0;

    if(
        ErrorCondition( oC_Mutex_Delete(&context->ContextBusyMutex, AllocationFlags_CanWait1Second)     , oC_ErrorCode_ReleaseError )
     && ErrorCondition( kfree(context, AllocationFlags_CanWait1Second)                                  , oC_ErrorCode_ReleaseError )
        )
    {
        *outContext = NULL;
        errorCode   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief initializes PHY to work
 */
//==========================================================================================================================================
static oC_ErrorCode_t InitializePhy( const oC_ETH_Config_t * Config , oC_ETH_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCode( oC_ETH_PhyReset           ( Context->PhyAddress                    ) )
     && ErrorCode( oC_ETH_SetAutoNegotiation ( Context->PhyAddress , true, 0          ) )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases PHY when it is not needed anymore
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReleasePhy( const oC_ETH_Config_t * Config , oC_ETH_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;



    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns current link status
 */
//==========================================================================================================================================
static oC_Net_LinkStatus_t GetLinkStatus( oC_ETH_Context_t Context )
{
    oC_Net_LinkStatus_t linkStatus = oC_Net_LinkStatus_Down;

    oC_SaveIfErrorOccur( "ETH:Cannot get link status - " , oC_ETH_ReadLinkStatus(Context->PhyAddress,&linkStatus) );

    return linkStatus;
}

//==========================================================================================================================================
/**
 * @brief allocates Rx/Tx descriptors
 */
//==========================================================================================================================================
static oC_ErrorCode_t AllocateDescriptors( oC_ETH_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( Context->Result.RxData.Alignment > 0        , oC_ErrorCode_AlignmentNotCorrect  )
     && ErrorCondition( Context->Result.TxData.Alignment > 0        , oC_ErrorCode_AlignmentNotCorrect  )
     && ErrorCondition( Context->Result.RxData.DescriptorSize > 0   , oC_ErrorCode_SizeNotCorrect       )
     && ErrorCondition( Context->Result.TxData.DescriptorSize > 0   , oC_ErrorCode_SizeNotCorrect       )
     && ErrorCondition( Context->Result.RxData.RingSize > 0         , oC_ErrorCode_SizeNotCorrect       )
     && ErrorCondition( Context->Result.TxData.RingSize > 0         , oC_ErrorCode_SizeNotCorrect       )
        )
    {
        AllocationFlags_t allocationFlags = AllocationFlags_ZeroFill
                                          | AllocationFlags_UseInternalRam
                                          | AllocationFlags_UseDmaRam
                                          | AllocationFlags_DmaRamFirst;

        Context->RxDescriptorRing = kamalloc(Context->Result.RxData.DescriptorSize * Context->Result.RxData.RingSize,
                                             &Allocator, allocationFlags, Context->Result.RxData.Alignment);
        Context->TxDescriptorRing = kamalloc(Context->Result.TxData.DescriptorSize * Context->Result.TxData.RingSize ,
                                             &Allocator, allocationFlags, Context->Result.TxData.Alignment);
        if(
            ErrorCondition( Context->RxDescriptorRing != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( Context->TxDescriptorRing != NULL , oC_ErrorCode_AllocationError )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            kafree(Context->RxDescriptorRing, AllocationFlags_CanWait1Second);
            kafree(Context->TxDescriptorRing, AllocationFlags_CanWait1Second);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases Rx/Tx descriptors
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReleaseDescriptors( oC_ETH_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( kafree(Context->RxDescriptorRing, AllocationFlags_CanWait1Second ) , oC_ErrorCode_ReleaseError )
     && ErrorCondition( kafree(Context->TxDescriptorRing, AllocationFlags_CanWait1Second ) , oC_ErrorCode_ReleaseError )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends frame
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendFrame( oC_ETH_Context_t Context , const oC_Net_MacAddress_t Source, const oC_Net_MacAddress_t Destination , const void * Data , uint16_t Size , oC_ETH_LLD_FrameSegment_t FrameSegment, oC_Net_FrameType_t FrameType , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Procedure_Begin
    {
        oC_IntMan_EnterCriticalSection();
        if(oC_ETH_LLD_IsTransmitQueueFull(&Context->Result))
        {
            oC_Event_SetState(ModuleReadyToSend,oC_Event_State_Inactive);
        }
        else
        {
            oC_Event_SetState(ModuleReadyToSend,oC_Event_State_Active);
        }
        oC_IntMan_ExitCriticalSection();

        oC_Procedure_ExitIfFalse( oC_Event_WaitForState(ModuleReadyToSend, oC_Event_State_Active, oC_Event_StateMask_Full, Timeout), oC_ErrorCode_Timeout );

        FrameType = oC_Net_ConvertUint16ToNetworkEndianess(FrameType);
        oC_Procedure_ExitIfError( oC_ETH_LLD_SendFrame( &Context->Result, Source, Destination, Data, Size, FrameSegment , FrameType) );

        errorCode = oC_ErrorCode_None;
    }
    oC_Procedure_End;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives frame
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReceiveFrame( oC_ETH_Context_t Context , oC_Net_MacAddress_t outSource, oC_Net_MacAddress_t outDestination, void * Data, uint16_t * Size, oC_ETH_LLD_FrameSegment_t * outFrameSegment , oC_Net_FrameType_t * outFrameType , oC_Time_t Timeout  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Procedure_Begin
    {
        oC_IntMan_EnterCriticalSection();
        if(oC_ETH_LLD_IsDataReadyToReceive(&Context->Result) == false)
        {
            oC_Event_SetState(DataReadyToRead,oC_Event_State_Inactive);
        }
        else
        {
            oC_Event_SetState(DataReadyToRead,oC_Event_State_Active);
        }
        oC_IntMan_ExitCriticalSection();

        oC_Procedure_ExitIfFalse( oC_Event_WaitForState(DataReadyToRead, oC_Event_State_Active, oC_Event_StateMask_Full, Timeout), oC_ErrorCode_Timeout );

        oC_Procedure_ExitIfError( oC_ETH_LLD_ReceiveFrame( &Context->Result, outSource, outDestination, Data, Size, outFrameSegment , outFrameType) );

        *outFrameType   = oC_Net_ConvertUint16FromNetworkEndianess(*outFrameType);
        errorCode       = oC_ErrorCode_None;
    }
    oC_Procedure_End;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief performs PHY registers access test
 */
//==========================================================================================================================================
static oC_ErrorCode_t PerformPhyRegisterAccessTest( oC_Diag_t * Diag , void * Context )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_ETH_Context_t    context   = Context;

    if(ErrorCondition(IsContextCorrect(context), oC_ErrorCode_ContextNotCorrect))
    {
        oC_ETH_PhyRegister_BSR_t BSR;
        oC_ETH_PhyRegister_BCR_t BCR;

        oC_Procedure_Begin
        {
            BSR.Value = 0;
            BCR.Value = 0;

            if( !ErrorCode( oC_ETH_ReadPhyRegister( context->PhyAddress, oC_ETH_PhyRegister_BCR , &BCR.Value))  )
            {
                Diag->ResultDescription = "Cannot read BCR register";
                break;
            }

            BCR.Loopback = 1;

            if( !ErrorCode( oC_ETH_WritePhyRegister( context->PhyAddress, oC_ETH_PhyRegister_BCR, BCR.Value ) ) )
            {
                Diag->ResultDescription = "Cannot write BCR register";
                break;
            }

            BCR.Value = 0;

            if( !ErrorCode( oC_ETH_ReadPhyRegister( context->PhyAddress, oC_ETH_PhyRegister_BCR , &BCR.Value))  )
            {
                Diag->ResultDescription = "Cannot read BCR register after write";
                break;
            }

            if(BCR.Loopback != 1)
            {
                Diag->ResultDescription = "Value of the BCR register after write `BCR.Loopback = 1` is not correct!";
                errorCode               = oC_ErrorCode_CannotAccessRegister;
                break;
            }

            BCR.Loopback = 0;

            if( !ErrorCode( oC_ETH_WritePhyRegister( context->PhyAddress, oC_ETH_PhyRegister_BCR, BCR.Value ) ) )
            {
                Diag->ResultDescription = "Cannot write BCR register";
                break;
            }

            BCR.Value = 0;

            if(BCR.Loopback != 0)
            {
                Diag->ResultDescription = "Value of the BCR register after write `BCR.Loopback = 0` is not correct!";
                errorCode               = oC_ErrorCode_CannotAccessRegister;
                break;
            }

            if( !ErrorCode( oC_ETH_ReadPhyRegister( context->PhyAddress, oC_ETH_PhyRegister_BSR , &BSR.Value))  )
            {
                Diag->ResultDescription = "Cannot read BSR register";
                break;
            }


            if(BSR.Value == 0)
            {
                Diag->ResultDescription = "BSR value is 0! We expect anything else...";
                errorCode = oC_ErrorCode_CannotAccessRegister;
                break;
            }

            errorCode = oC_ErrorCode_None;
        }
        oC_Procedure_End;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handler for interrupts
 */
//==========================================================================================================================================
static void InterruptHandler( oC_ETH_LLD_InterruptSource_t Source )
{
    if(Source & oC_ETH_LLD_InterruptSource_DataReceived)
    {
        oC_Event_SetState(DataReadyToRead,oC_Event_State_Active);
    }
    if(Source & oC_ETH_LLD_InterruptSource_TransmissionSlotsAvailable)
    {
        oC_Event_SetState(ModuleReadyToSend,oC_Event_State_Active);
    }
    if(Source & oC_ETH_LLD_InterruptSource_ReceiveError)
    {
        oC_SaveError("ETH - Transmission failure - ", oC_ErrorCode_ReceiveError);
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

#endif
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
