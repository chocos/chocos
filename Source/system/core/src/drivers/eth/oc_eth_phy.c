/** ****************************************************************************************************************************************
 *
 * @brief      Stores functions for handling PHY
 *
 * @file       oc_eth_phy.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_eth.h>
#include <oc_eth_phy.h>
#include <oc_module.h>
#include <oc_ktime.h>

#ifdef oC_ETH_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROT_SECTION_________________________________________________________________________

static inline oC_ErrorCode_t WriteRegister ( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t      Value );
static inline oC_ErrorCode_t ReadRegister  ( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t * outValue );

#undef  _________________________________________LOCAL_PROT_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief writes PHY register
 *
 * The function is for writing registers of the PHY.
 *
 * @warning This function is for debug reasons only. It should not be used if you do not know what you are doing!
 *
 * @param PhyAddress    Address of the external PHY
 * @param Register      Register to access
 * @param Value         Value to write
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_WritePhyRegister( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t      Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        errorCode = WriteRegister(PhyAddress,Register,Value);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief writes PHY register
 *
 * The function is for writing registers of the PHY.
 *
 * @warning This function is for debug reasons only. It should not be used if you do not know what you are doing!
 *
 * @param PhyAddress    Address of the external PHY
 * @param Register      Register to access
 * @param Value         Value to write
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_ReadPhyRegister( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        errorCode = ReadRegister(PhyAddress,Register,outValue);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief resets PHY
 *
 * The function is for reseting the PHY. It also waits to ensure, that the PHY was successfully reseted.
 *
 * @param PhyAddress    Address of the external PHY
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_PhyReset( oC_ETH_PhyAddress_t PhyAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        oC_ETH_PhyRegister_BCR_t BCR = { .Value = 0 };

        BCR.SoftReset   = 1;

        if(ErrorCode( WriteRegister( PhyAddress , oC_ETH_PhyRegister_BCR , BCR.Value  ) ))
        {
            /* The reset process takes 0.5 s */
            sleep(ms(500));

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads link status from the PHY
 *
 * The function is for reading link status from the PHY.
 *
 * @param PhyAddress        Address of the external PHY
 * @param outLinkStatus     Destination for the link status
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_ReadLinkStatus( oC_ETH_PhyAddress_t PhyAddress , oC_Net_LinkStatus_t * outLinkStatus )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        oC_ETH_PhyRegister_BSR_t BSR = { .Value = 0 };

        if(
            ErrorCondition( isram(outLinkStatus) , oC_ErrorCode_OutputAddressNotInRAM            )
         && ErrorCode     ( ReadRegister( PhyAddress , oC_ETH_PhyRegister_BSR , &BSR.Value  )    )
         )
        {
            *outLinkStatus  = BSR.LinkStatus == 1 ? oC_Net_LinkStatus_Up : oC_Net_LinkStatus_Down;
            errorCode       = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief enables/disables AutoNegotiation
 *
 * The function is for enabling/disabling auto negotiation mode
 *
 * @param PhyAddress        Address of the external PHY
 * @param Enabled           True if it should be enabled
 * @param Timeout           Maximum time for operation. If 0, the module will not wait for AutoNegotiation complete
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_SetAutoNegotiation( oC_ETH_PhyAddress_t PhyAddress , bool Enabled , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        oC_Timestamp_t           endTimestamp   = oC_KTime_GetTimestamp() + Timeout;
        oC_ETH_PhyRegister_BCR_t BCR            = { .Value = 0 };
        oC_ETH_PhyRegister_BSR_t BSR            = { .Value = 0 };


        if(
            ErrorCode( ReadRegister ( PhyAddress , oC_ETH_PhyRegister_BCR , &BCR.Value  )    )
           )
        {
            BCR.AutoNegotiation = 1;

            if(
                ErrorCode( WriteRegister( PhyAddress , oC_ETH_PhyRegister_BCR ,  BCR.Value  )    )
                )
            {
                errorCode = oC_ErrorCode_None;

                while(
                        Timeout > 0
                     && ErrorCode       ( ReadRegister(PhyAddress,oC_ETH_PhyRegister_BSR,&BSR.Value         ))
                     && BSR.AutoNegotiateComplete == 0
                     && ErrorCondition  ( oC_KTime_GetTimestamp() < endTimestamp , oC_ErrorCode_Timeout      )
                     );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief enables/disables PHY loopback mode
 *
 *  The function is for enabling/disabling auto negotiation mode
 *
 * @param PhyAddress        Address of the external PHY
 * @param Enabled           True if it should be enabled
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_ETH_SetPhyLoopback( oC_ETH_PhyAddress_t PhyAddress , bool Enabled )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_ETH))
    {
        oC_ETH_PhyRegister_BCR_t BCR = { .Value = 0 };

        if( ErrorCode( ReadRegister ( PhyAddress , oC_ETH_PhyRegister_BCR , &BCR.Value  )) )
        {
            if(Enabled)
            {
                BCR.Loopback        = 1;
                BCR.AutoNegotiation = 0;
                BCR.SpeedSelect     = 1;
                BCR.DuplexMode      = 1;
            }
            else
            {
                BCR.Loopback        = 0;
                BCR.AutoNegotiation = 1;
                BCR.SpeedSelect     = 0;
                BCR.DuplexMode      = 0;
            }

            if( ErrorCode( WriteRegister( PhyAddress , oC_ETH_PhyRegister_BCR ,  BCR.Value  )) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief writes PHY register
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t WriteRegister( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t Value )
{
    return oC_ETH_LLD_WritePhyRegister(PhyAddress,(oC_ETH_LLD_PHY_RegisterAddress_t)Register,Value);
}

//==========================================================================================================================================
/**
 * @brief reads PHY register
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t ReadRegister( oC_ETH_PhyAddress_t PhyAddress , oC_ETH_PhyRegister_t Register , uint32_t * outValue )
{
    return oC_ETH_LLD_ReadPhyRegister(PhyAddress,(oC_ETH_LLD_PHY_RegisterAddress_t)Register,outValue);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

#endif
