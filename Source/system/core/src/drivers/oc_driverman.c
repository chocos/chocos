/** ****************************************************************************************************************************************
 *
 * @file       oc_driverman.c
 *
 * @brief      The file with sources for driver manager
 *
 * @author     Patryk Kubiak - (Created on: 14 09 2015 19:14:33)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_driverman.h>
#include <oc_boot.h>
#include <oc_stdlib.h>
#include <string.h>
#include <oc_drivers_list.h>
#include <oc_bits.h>
#include <oc_debug.h>
#include <oc_drivers_cfg.c>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_vt100.h>

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_List(oC_Driver_t)                 Drivers                       = NULL;
static oC_List(oC_AutoConfiguration_t*)     Configurations                = NULL;
static oC_List(oC_DriverInstance_t*)        Instances                     = NULL;
static oC_DriverInstanceAddedCallback_t     InstanceAddedCallback         = NULL;
static void *                               InstanceAddedUserPointer      = NULL;
static oC_DriverInstanceRemovedCallback_t   InstanceRemovedCallback       = NULL;
static void *                               InstanceRemovedUserPointer    = NULL;
static uint32_t                             UniqueInstanceIdCounter       = 0;
static const oC_Allocator_t             Allocator               = {
                .Name = "driver manager",
                .CorePwd = oC_MemMan_CORE_PWD
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * Function turns on the driver manager
 *
 * @return The error code
 *
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_DriverMan))
    {
        Drivers                    = oC_List_New(&Allocator , AllocationFlags_NoWait);
        Configurations             = oC_List_New(&Allocator , AllocationFlags_NoWait);
        Instances                  = oC_List_New(&Allocator , AllocationFlags_NoWait);
        InstanceAddedCallback      = NULL;
        InstanceAddedUserPointer   = NULL;
        InstanceRemovedCallback    = NULL;
        InstanceRemovedUserPointer = NULL;

        if(Drivers && Configurations && Instances)
        {
            oC_Module_TurnOn(oC_Module_DriverMan);

            errorCode = oC_ErrorCode_None;
#define ADD_DRIVER_TO_LIST(NAME)        oC_AssignErrorCode(&errorCode , oC_DriverMan_AddDriver(&NAME));
#define DONT_ADD_DRIVER_TO_LIST(NAME)
            CFG_LIST_DRIVERS(ADD_DRIVER_TO_LIST,DONT_ADD_DRIVER_TO_LIST)
#undef ADD_DRIVER_TO_LIST
#undef DONT_ADD_DRIVER_TO_LIST

#define ADD_CONFIGURATION( DRIVER_NAME , CONFIG_NAME )  oC_AssignErrorCode(&errorCode , oC_DriverMan_AddAutoConfiguration(#CONFIG_NAME,&DRIVER_NAME,&CONFIG_NAME));
#define DONT_ADD_CONFIGURATION(...)
            CFG_AUTO_CONFIGURATION_LIST(ADD_CONFIGURATION,DONT_ADD_CONFIGURATION)
#undef ADD_CONFIGURATION
#undef DONT_ADD_CONFIGURATION

            ErrorCode( oC_Driver_SetInstanceAddedCallback  ( oC_DriverMan_AddInstance       ) );
            ErrorCode( oC_Driver_SetInstanceRemovedCallback( oC_DriverMan_RemoveInstance    ) );

            if( oC_ErrorOccur(errorCode) )
            {
                oC_Module_TurnOff(oC_Module_DriverMan);
                oC_List_Delete(Drivers          , 0);
                oC_List_Delete(Configurations   , 0);
                oC_List_Delete(Instances        , 0);
            }
        }
        else
        {
            oC_List_Delete(Drivers          , 0);
            oC_List_Delete(Configurations   , 0);
            oC_List_Delete(Instances        , 0);
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan))
    {
        oC_Module_TurnOff(oC_Module_DriverMan);

        errorCode = oC_ErrorCode_None;

        ErrorCondition( oC_List_Delete( Drivers         ) , oC_ErrorCode_ReleaseError );
        ErrorCondition( oC_List_Delete( Configurations  ) , oC_ErrorCode_ReleaseError );
        ErrorCondition( oC_List_Delete( Instances       ) , oC_ErrorCode_ReleaseError );
        ErrorCode( oC_Driver_SetInstanceAddedCallback   ( NULL ) );
        ErrorCode( oC_Driver_SetInstanceRemovedCallback ( NULL ) );

        InstanceAddedCallback       = NULL;
        InstanceAddedUserPointer    = NULL;
        InstanceRemovedCallback     = NULL;
        InstanceRemovedUserPointer  = NULL;
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_Driver_t) oC_DriverMan_GetList( void )
{
    return Drivers;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_AddDriver( oC_Driver_t Driver )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan))
    {
        bool added = oC_List_PushBack(Drivers,Driver,&Allocator);

        if(added)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotAddObjectToList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_RemoveDriver( oC_Driver_t Driver )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan))
    {
        bool removed = oC_List_RemoveAll(Drivers,Driver);

        if(removed)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotRemoveObjectFromList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Driver_t oC_DriverMan_GetDriver( const char * FileName )
{
    oC_Driver_t foundDriver = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_DriverMan))
    {
        oC_List_Foreach(Drivers,driver)
        {
            if(strcmp(driver->FileName,FileName) == 0)
            {
                foundDriver = driver;
            }
        }
    }

    return foundDriver;
}

//==========================================================================================================================================
//==========================================================================================================================================
void oC_DriverMan_TurnOnAllDrivers( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) )
{
#define PrintWait(...)      if( PrintWaitMessage != NULL ) PrintWaitMessage( __VA_ARGS__ )
#define PrintResult( err )  if( PrintResult      != NULL ) PrintResult( err )
    if( oC_Module_IsTurnedOn(oC_Module_DriverMan)
     && ( PrintWaitMessage == NULL || isaddresscorrect(PrintWaitMessage)    )
     && ( PrintResult == NULL      || isaddresscorrect(PrintResult)         )
     )
    {
        oC_Boot_Level_t currentLevel = oC_Boot_GetCurrentBootLevel();

        oC_List_Foreach(Drivers,driver)
        {
            if((oC_Driver_IsTurnedOn(driver) == false) && oC_Bits_AreBitsSetU32(currentLevel,driver->RequiredBootLevel))
            {
                PrintWait( "Turning on driver %s", driver->FileName );
                oC_ErrorCode_t errorCode = oC_Driver_TurnOn(driver);
                PrintResult( errorCode );
                oC_SaveIfErrorOccur(driver->FileName, errorCode);
            }
        }
    }
#undef PrintWait
#undef PrintResult
}
//==========================================================================================================================================
//==========================================================================================================================================
void oC_DriverMan_TurnOffAllDrivers( void )
{
    if(oC_Module_IsTurnedOn(oC_Module_DriverMan))
    {
        oC_List_Foreach(Drivers,driver)
        {
            oC_ErrorCode_t errorCode = oC_Driver_TurnOff(driver);

            if(oC_ErrorOccur(errorCode))
            {
                oC_SaveError(driver->FileName,errorCode);
            }
        }
    }
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_AddAutoConfiguration( const char * Name , oC_Driver_t Driver , const void * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
     && ErrorCondition( Configurations    != NULL , oC_ErrorCode_AutoConfigurationsNotAvailable )
     && ErrorCondition( isaddresscorrect(Name)    , oC_ErrorCode_WrongAddress                   )
     && ErrorCondition( isaddresscorrect(Driver)  , oC_ErrorCode_WrongAddress                   )
     && ErrorCondition( isaddresscorrect(Config)  , oC_ErrorCode_WrongAddress                   )
        )
    {
        oC_AutoConfiguration_t * autoConfiguration = kmalloc(sizeof(oC_AutoConfiguration_t),&Allocator,AllocationFlags_NoWait | AllocationFlags_ZeroFill);

        if(ErrorCondition(autoConfiguration != NULL , oC_ErrorCode_AllocationError))
        {
            autoConfiguration->Config = kmalloc(Driver->ConfigurationSize,&Allocator,AllocationFlags_NoWait | AllocationFlags_ZeroFill);
            autoConfiguration->Driver = Driver;

            if(ErrorCondition(autoConfiguration->Config != NULL , oC_ErrorCode_AllocationError))
            {
                strncpy(autoConfiguration->Name,Name,sizeof(autoConfiguration->Name));
                memcpy(autoConfiguration->Config,Config,Driver->ConfigurationSize);

                bool added = oC_List_PushBack(Configurations,autoConfiguration,&Allocator);

                if(added)
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    errorCode = oC_ErrorCode_CannotAddObjectToList;
                }
            }
        }

        if(oC_ErrorOccur(errorCode))
        {
            if(autoConfiguration != NULL)
            {
                if(autoConfiguration->Config != NULL)
                {
                    oC_SaveIfFalse("DRVMAN: Add autoConf kfree config error - " , kfree(autoConfiguration->Config , AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
                }
                oC_SaveIfFalse("DRVMAN: Add autoConf kfree error - " , kfree(autoConfiguration , AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_RemoveAutoConfiguration( const char * Name )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
     && ErrorCondition( Configurations    != NULL , oC_ErrorCode_AutoConfigurationsNotAvailable )
     && ErrorCondition( isaddresscorrect(Name)    , oC_ErrorCode_WrongAddress                   )
        )
    {
        bool removedAll = false;

        errorCode = oC_ErrorCode_ObjectNotFoundOnList;

        oC_List_Foreach(Configurations,autoConfig)
        {
            if(isaddresscorrect(autoConfig) == false)
            {
                kdebuglog(oC_LogType_Error,"DRVMAN: autoConfig address is not correct! (%p)\n" , autoConfig);
            }
            else if(strcmp(autoConfig->Name,Name) == 0)
            {
                oC_SaveIfFalse("DRVMAN: Remove autoConf kfree config error - " , kfree(autoConfig->Config , AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);
                oC_SaveIfFalse("DRVMAN: Remove autoConf kfree error - "        , kfree(autoConfig         , AllocationFlags_CanWait1Second) , oC_ErrorCode_ReleaseError);

                removedAll = oC_List_RemoveAll(Configurations,autoConfig);
                if(removedAll)
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    errorCode = oC_ErrorCode_CannotRemoveObjectFromList;
                }
            }
        }
    }

    return errorCode;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_ReadAutoConfiguration( const char * Name , oC_AutoConfiguration_t * outAutoConfiguration )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
     && ErrorCondition( Configurations    != NULL  , oC_ErrorCode_AutoConfigurationsNotAvailable )
     && ErrorCondition( isaddresscorrect(Name)     , oC_ErrorCode_WrongAddress                   )
     && ErrorCondition( isram(outAutoConfiguration), oC_ErrorCode_OutputAddressNotInRAM          )
        )
    {
        errorCode = oC_ErrorCode_ObjectNotFoundOnList;

        oC_List_Foreach(Configurations,autoConfig)
        {
            if(isaddresscorrect(autoConfig) == false)
            {
                kdebuglog(oC_LogType_Error,"DRVMAN: autoConfig address is not correct! (%p)\n" , autoConfig);
            }
            else if(strcmp(autoConfig->Name,Name) == 0)
            {
                memcpy(outAutoConfiguration,autoConfig,sizeof(oC_AutoConfiguration_t));
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
void oC_DriverMan_ConfigureAllDrivers( void (*PrintWaitMessage)( const char * Format, ... ), void (*PrintResult)( oC_ErrorCode_t ErrorCode ) )
{
#define PrintWait(...)      if( PrintWaitMessage != NULL ) PrintWaitMessage( __VA_ARGS__ )
#define PrintResult( err )  if( PrintResult      != NULL ) PrintResult( err )
    if(oC_Module_IsTurnedOn(oC_Module_DriverMan) && Configurations != NULL
        && ( PrintWaitMessage == NULL || isaddresscorrect(PrintWaitMessage)    )
        && ( PrintResult == NULL      || isaddresscorrect(PrintResult)         )
        )
    {
        oC_List_Foreach(Configurations,autoConfig)
        {
            if(isaddresscorrect(autoConfig) == false)
            {
                kdebuglog(oC_LogType_Error,"DRVMAN: autoConfig address is not correct! (%p)\n" , autoConfig);
            }
            else if(oC_Driver_IsTurnedOn(autoConfig->Driver) && autoConfig->Configured == false)
            {
                PrintWait( "Configuration of %s", autoConfig->Name );
                oC_ErrorCode_t errorCode = oC_Driver_Configure(autoConfig->Driver,autoConfig->Config,&autoConfig->Context);
                PrintResult( errorCode );

                if( oC_SaveIfErrorOccur(autoConfig->Name, errorCode) )
                {
                    autoConfig->Configured = true;
                }
            }
        }
    }
#undef PrintWait
#undef PrintResult
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_ReadAutoConfigurationList ( oC_List(oC_AutoConfiguration_t*) * outList )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
         oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
      && ErrorCondition( Configurations    != NULL  , oC_ErrorCode_AutoConfigurationsNotAvailable )
      && ErrorCondition( isram(outList)             , oC_ErrorCode_OutputAddressNotInRAM          )
        )
    {
        *outList  = Configurations;
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_AddInstance( oC_Driver_t Driver , oC_Driver_Config_t Config , oC_Driver_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
     && ErrorCondition( isaddresscorrect(Driver)  , oC_ErrorCode_WrongAddress               )
     && ErrorCondition( isaddresscorrect(Config)  , oC_ErrorCode_WrongAddress               )
     && ErrorCondition( isaddresscorrect(Context) , oC_ErrorCode_WrongAddress               )
        )
    {
        oC_DriverInstance_t * instance = kmalloc(sizeof(oC_DriverInstance_t), &Allocator, AllocationFlags_ZeroFill);

        if( ErrorCondition( instance != NULL , oC_ErrorCode_AllocationError ) )
        {
            instance->Config        = Config;
            instance->Context       = Context;
            instance->Driver        = Driver;

            bool pushed = oC_List_PushBack(Instances,instance,&Allocator);

            if( ErrorCondition( pushed, oC_ErrorCode_CannotAddObjectToList ) )
            {
                sprintf( instance->Name, "%s-%05X", Driver->FileName, ++UniqueInstanceIdCounter );
                if(InstanceAddedCallback != NULL)
                {
                    InstanceAddedCallback( InstanceAddedUserPointer, instance );
                }
                else
                {
                    kdebuglog( oC_LogType_Track, "New driver instance added (%s), but instance callback is not set", instance->Name );
                }
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfFalse("Cannot release instance memory", kfree(instance,0) , oC_ErrorCode_ReleaseError );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_RemoveInstance( oC_Driver_t Driver , oC_Driver_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
     && ErrorCondition( isaddresscorrect(Driver)  , oC_ErrorCode_WrongAddress               )
     && ErrorCondition( isaddresscorrect(Context) , oC_ErrorCode_WrongAddress               )
        )
    {
        errorCode = oC_ErrorCode_ObjectNotFoundOnList;

        foreach( Instances, instance )
        {
            if( (Context == instance->Context) )
            {
                if(InstanceRemovedCallback != NULL)
                {
                    InstanceRemovedCallback( InstanceRemovedUserPointer, instance );
                }

                bool released   = kfree(instance,0);
                bool removedAll = oC_List_RemoveAll(Instances,instance);

                if(
                    ErrorCondition( released    , oC_ErrorCode_ReleaseError                 )
                 && ErrorCondition( removedAll  , oC_ErrorCode_CannotRemoveObjectFromList   )
                    )
                {
                    errorCode = oC_ErrorCode_None;
                }
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_SetInstanceAddedCallback( oC_DriverInstanceAddedCallback_t Callback, void * UserPointer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
     && ErrorCondition( isaddresscorrect(Callback)    || Callback == NULL   , oC_ErrorCode_WrongAddress               )
     && ErrorCondition( InstanceAddedCallback == NULL || Callback == NULL   , oC_ErrorCode_CallbackAlreadySet         )
        )
    {
        InstanceAddedCallback       = Callback;
        InstanceAddedUserPointer    = UserPointer;
        errorCode                   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DriverMan_SetInstanceRemovedCallback( oC_DriverInstanceRemovedCallback_t Callback, void * UserPointer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_DriverMan)
     && ErrorCondition( isaddresscorrect(Callback)      || Callback == NULL   , oC_ErrorCode_WrongAddress               )
     && ErrorCondition( InstanceRemovedCallback == NULL || Callback == NULL   , oC_ErrorCode_CallbackAlreadySet         )
        )
    {
        InstanceRemovedCallback     = Callback;
        InstanceRemovedUserPointer  = UserPointer;
        errorCode                   = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_DriverInstance_t*) oC_DriverMan_GetInstanceList( void )
{
    return Instances;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


