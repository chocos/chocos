/** ****************************************************************************************************************************************
 *
 * @file       oc_neunet.c
 * 
 * File based on driver.c Ver 1.1.1
 *
 * @brief      The file with source for NEUNET driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2023-05-09 - 12:37:21)
 *
 * @copyright  Copyright (C) 2023 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_neunet.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         NEUNET
#define DRIVER_FILE_NAME    "neunet"
#define DRIVER_DEBUGLOG     false
#define DRIVER_TYPE         COMMUNICATION_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_NEUNET_Configure
#define DRIVER_UNCONFIGURE  oC_NEUNET_Unconfigure
#define DRIVER_TURN_ON      oC_NEUNET_TurnOn
#define DRIVER_TURN_OFF     oC_NEUNET_TurnOff
#define IS_TURNED_ON        oC_NEUNET_IsTurnedOn
#define HANDLE_IOCTL        oC_NEUNET_Ioctl
#define READ_FROM_DRIVER    oC_NEUNET_Read
#define WRITE_TO_DRIVER     oC_NEUNET_Write

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores NEUNET context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t      ObjectControl;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all NEUNET driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "neunet module"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsContextCorrect      ( oC_NEUNET_Context_t Context );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the NEUNET module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NEUNET_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_NEUNET))
    {
        /* This must be always at the end of the function */
        oC_Module_TurnOn(oC_Module_NEUNET);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the NEUNET driver
 *
 * The function for turning off the NEUNET driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NEUNET_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_NEUNET))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_NEUNET);

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_NEUNET_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_NEUNET);
}

//==========================================================================================================================================
/**
 * @brief configures NEUNET pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_NEUNET_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NEUNET_Configure( const oC_NEUNET_Config_t * Config , oC_NEUNET_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_NEUNET))
    {
//        if(
//           oC_AssignErrorCodeIfFalse(&errorCode , IsConfigCorrect(Config)   ,          oC_ErrorCode_WrongConfigAddress )  &&
//           oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext) ,                  oC_ErrorCode_OutputAddressNotInRAM )
//            )
//        {
//
//        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_NEUNET_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NEUNET_Unconfigure( const oC_NEUNET_Config_t * Config , oC_NEUNET_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_NEUNET))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config)      , oC_ErrorCode_WrongConfigAddress) &&
            oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect )     
            )
        {

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of errror
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NEUNET_Ioctl( oC_NEUNET_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_NEUNET) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Context)            , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect )
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief reads buffer from the driver
 * 
 * The function is for reading buffer by using NEUNET driver. It is called when someone will read the driver file. 
 *
 * @param Context       Context of the driver
 * @param outBuffer     Buffer for data
 * @param Size          Size of the buffer on input, on output number of read bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NEUNET_Read( oC_NEUNET_Context_t Context , char * outBuffer , uint32_t * Size , oC_Time_t Timeout  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_NEUNET) 
     && ErrorCondition( IsContextCorrect(Context)            , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isram(outBuffer)                     , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isram(Size)                          , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                          , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect          )
        )
    {

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief write buffer to the driver
 * 
 * The function is for writing buffer by using NEUNET driver. It is called when someone will write the driver file. 
 *
 * @param Context       Context of the driver
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer on input, on output number of written bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_NEUNET_Write( oC_NEUNET_Context_t Context , const char * Buffer , uint32_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_NEUNET) 
     && ErrorCondition( IsContextCorrect(Context)            , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isaddresscorrect(Buffer)             , oC_ErrorCode_WrongAddress            )
     && ErrorCondition( isram(Size)                          , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                          , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect          )
        )
    {

    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the NEUNET driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_NEUNET_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_NEUNETContext,Context->ObjectControl);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
