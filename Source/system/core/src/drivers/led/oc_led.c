/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface source for the LED driver
 *
 * @file       oc_led.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_led.h>
#include <oc_gpio.h>
#include <oc_pwm.h>
#include <oc_module.h>
#include <oc_stdlib.h>
#include <oc_string.h>
#include <oc_stdio.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         LED
#define DRIVER_FILE_NAME    "led"
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO,&PWM
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireClock | oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_LED_Configure
#define DRIVER_UNCONFIGURE  oC_LED_Unconfigure
#define WRITE_TO_DRIVER     oC_LED_Write
#define HANDLE_IOCTL        oC_LED_Ioctl

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Context_t
{
    oC_ObjectControl_t  ObjectControl;
    oC_LED_Config_t     Config;
    oC_PWM_Context_t    PwmContexts[oC_LED_Index_NumberOfElements];
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static oC_Allocator_t Allocator = {
                .Name = "led"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static oC_ErrorCode_t ConfigureLeds     ( oC_LED_Context_t Context );
static oC_ErrorCode_t UnconfigureLeds   ( oC_LED_Context_t Context );
static bool           IsContextContext  ( oC_LED_Context_t Context );
static oC_ErrorCode_t SetLight          ( oC_LED_Context_t Context , oC_LED_Index_t LedIndex , uint8_t Light );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief configures LED pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_LED_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongConfigAddress     | Address of the `Config` parameter is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outContext` parameter is not in RAM section
 *  #oC_ErrorCode_ModeNotCorrect         | `Config->Mode` is not correct
 *  #oC_ErrorCode_AllocationError        | Allocation error
 *  #oC_ErrorCode_ReleaseError           | Release error
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LED_Configure( const oC_LED_Config_t * Config , oC_LED_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config) , oC_ErrorCode_WrongConfigAddress)      &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)        , oC_ErrorCode_OutputAddressNotInRAM)   &&
        oC_AssignErrorCodeIfFalse(&errorCode , Config->Mode & oC_LED_Mode_RGB , oC_ErrorCode_ModeNotCorrect )
        )
    {
        oC_LED_Context_t context = ksmartalloc(sizeof(struct Context_t) , &Allocator , AllocationFlags_CanWait1Second);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(context) , oC_ErrorCode_AllocationError ))
        {
            memcpy(&context->Config,Config,sizeof(oC_LED_Config_t));

            if(oC_AssignErrorCode(&errorCode , ConfigureLeds(context)))
            {
                context->ObjectControl = oC_CountObjectControl(context,oC_ObjectId_LedContext);
                *outContext            = context;
                errorCode              = oC_ErrorCode_None;

            }
            else
            {
                if(ksmartfree(context,sizeof(struct Context_t),AllocationFlags_CanWaitForever)==false)
                {
                    errorCode = oC_ErrorCode_ReleaseError;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Restores default state on pins
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *     Error Code                       | Description
 * -------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                  | Operation success
 *  #oC_ErrorCode_WrongConfigAddress    | Address of the `Config` parameter is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM | Address of the `outContext` parameter is not in RAM section
 *  #oC_ErrorCode_ContextNotCorrect     | `*outContext` is not correct
 * 
 *  @note
 *  More error codes can be returned from the #oC_PWM_Unconfigure or #oC_GPIO_QuickUnconfigure functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LED_Unconfigure( const oC_LED_Config_t * Config , oC_LED_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config)      , oC_ErrorCode_WrongConfigAddress)      &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM)   &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextContext(*outContext) , oC_ErrorCode_ContextNotCorrect )
        )
    {
        oC_LED_Context_t context = *outContext;
        context->ObjectControl   = 0;

        if(ksmartfree(*outContext,sizeof(struct Context_t),AllocationFlags_CanWaitForever))
        {
            if(oC_AssignErrorCode(&errorCode , UnconfigureLeds(context)))
            {
                *outContext = NULL;
                errorCode   = oC_ErrorCode_None;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets light on the LED
 *
 * @param Context   Driver context
 * @param Buffer    Pointer to the buffer with data
 * @param Size      Size of the data
 * @param Timeout   Timeout for the operation
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *     Error Code                       | Description
 * -------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                  | Operation success
 *  #oC_ErrorCode_ContextNotCorrect     | `Context` is not correct
 *  #oC_ErrorCode_WrongAddress          | Address of the `Buffer` parameter is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM | `Size` is not in RAM section
 *  #oC_ErrorCode_SizeNotCorrect        | `*Size` is not correct
 * 
 *  @note
 *  More error codes can be returned from the #oC_PWM_SetDuty or #oC_GPIO_Write functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LED_Write( oC_LED_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextContext(Context)    , oC_ErrorCode_ContextNotCorrect    ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Buffer)     , oC_ErrorCode_WrongAddress         ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(Size)                  , oC_ErrorCode_OutputAddressNotInRAM) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (*Size) > 0                  , oC_ErrorCode_SizeNotCorrect       )
        )
    {
        char * tempBuffer = ksmartalloc(*Size,&Allocator,AllocationFlags_CanWait1Second);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(tempBuffer) , oC_ErrorCode_AllocationError))
        {
            unsigned int color = 0;

            errorCode = sscanf(Buffer,"%x",&color);

            oC_AssignErrorCodeIfFalse(&errorCode,ksmartfree(tempBuffer,*Size,AllocationFlags_CanWaitForever),oC_ErrorCode_ReleaseError);
            oC_AssignErrorCode(&errorCode,oC_LED_SetColor(Context,(oC_Color_t)color));
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Handles ioctl commands
 * 
 * @param Context   Driver context
 * @param Command   Command to handle
 * @param Data      Pointer to the data
 * 
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *    Error Code                       | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ContextNotCorrect    | `Context` is not correct
 *  #oC_ErrorCode_CommandNotCorrect    | `Command` is not correct
 *  #oC_ErrorCode_CommandNotHandled    | `Command` is not handled
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LED_Ioctl( oC_LED_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextContext(Context)          , oC_ErrorCode_ContextNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Ioctl_IsCorrectCommand(Command) , oC_ErrorCode_CommandNotCorrect)
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets color of the LED
 * 
 * @param Context   Driver context
 * @param Color     Color to set
 * 
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *   Error Code                        | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ContextNotCorrect    | `Context` is not correct
 *  
 * @note
 * More error codes can be returned from the #oC_PWM_SetValue or #oC_GPIO_WriteData functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LED_SetColor( oC_LED_Context_t Context , oC_Color_t Color )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsContextContext(Context) , oC_ErrorCode_ContextNotCorrect))
    {
        oC_LED_Mode_t mode = Context->Config.Mode;

        errorCode = oC_ErrorCode_None;

        for(oC_LED_Index_t ledIndex = 0 ; ledIndex < oC_LED_Index_NumberOfElements && !oC_ErrorOccur(errorCode); ledIndex++)
        {
            if(oC_Bits_IsBitSetU32(mode,ledIndex))
            {
                errorCode = SetLight(Context , ledIndex , (uint8_t)((Color & 0xff0000) >> 16 ) );
            }
            Color = Color << 8;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets light of the LED
 * 
 * @param Context   Driver context
 * @param LedIndex  Index of the LED
 * @param Light     Light to set
 * 
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *   Error Code                        | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ContextNotCorrect    | `Context` is not correct
 *  #oC_ErrorCode_IndexNotCorrect      | `LedIndex` is not correct
 *  
 * @note
 * More error codes can be returned from the #oC_PWM_SetValue or #oC_GPIO_WriteData functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LED_SetLight( oC_LED_Context_t Context , oC_LED_Index_t LedIndex , uint8_t Light )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextContext(Context)                , oC_ErrorCode_ContextNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , LedIndex < oC_LED_Index_NumberOfElements , oC_ErrorCode_IndexNotCorrect)
        )
    {
        errorCode = SetLight(Context,LedIndex,Light);
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Configures LEDs
 *
 * The function configures LEDs pins to work. It uses the #oC_LED_Config_t structure to get configuration. Look at the structure description
 *
 * @param Context           Driver context
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 *
 * @note
 * More error codes can be returned from the #oC_PWM_Configure or #oC_GPIO_QuickOutput functions
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureLeds( oC_LED_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    oC_LED_Mode_t  mode      = Context->Config.Mode;
    bool           usePwm    = oC_Bits_AreBitsSetU32(mode,oC_LED_Mode_UsePWM);

    for(oC_LED_Index_t ledIndex = 0 ; ledIndex < oC_LED_Index_NumberOfElements && !oC_ErrorOccur(errorCode) ; ledIndex++ )
    {
        oC_Pins_t pin       = Context->Config.Pins[ledIndex];
        bool      lowActive = oC_Bits_AreBitsSetU32(mode,oC_LED_Mode_RedActiveLow<<ledIndex);

        if(oC_Bits_IsBitSetU32(mode,ledIndex))
        {
            if(usePwm)
            {
                oC_PWM_Config_t pwmConfig   = {
                                .Pin                    = pin ,
                                .ActiveState            = (lowActive) ? oC_PWM_ActiveState_Low : oC_PWM_ActiveState_High ,
                                .MaximumValue           = 255 ,
                                .TickFrequency          = oC_kHz(26) ,
                                .PermissibleDiffernece  = oC_kHz(5)
                };
                errorCode = oC_PWM_Configure(&pwmConfig,&Context->PwmContexts[ledIndex]);
            }
            else
            {
                errorCode = oC_GPIO_QuickOutput(pin);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures LEDs
 *
 * The function restores default state on pins. It uses the #oC_LED_Config_t structure to get configuration. Look at the structure description
 *
 * @param Context       Driver context
 *
 * @return code of error
 *
 * @note
 * More error codes can be returned from the #oC_PWM_Unconfigure or #oC_GPIO_QuickUnconfigure functions
 *
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigureLeds( oC_LED_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    oC_LED_Mode_t  mode      = Context->Config.Mode;
    bool           usePwm    = oC_Bits_AreBitsSetU32(mode,oC_LED_Mode_UsePWM);

    for(oC_LED_Index_t ledIndex = 0 ; ledIndex < oC_LED_Index_NumberOfElements && !oC_ErrorOccur(errorCode) ; ledIndex++ )
    {
        oC_Pins_t pin       = Context->Config.Pins[ledIndex];
        bool      lowActive = oC_Bits_AreBitsSetU32(mode,oC_LED_Mode_RedActiveLow<<ledIndex);

        if(oC_Bits_IsBitSetU32(mode,ledIndex))
        {
            if(usePwm)
            {
                oC_PWM_Config_t pwmConfig   = {
                                .Pin                    = pin ,
                                .ActiveState            = (lowActive) ? oC_PWM_ActiveState_Low : oC_PWM_ActiveState_High ,
                                .MaximumValue           = 255 ,
                                .TickFrequency          = oC_kHz(26) ,
                                .PermissibleDiffernece  = oC_kHz(5)
                };
                errorCode = oC_PWM_Unconfigure(&pwmConfig,&Context->PwmContexts[ledIndex]);
            }
            else
            {
                errorCode = oC_GPIO_QuickUnconfigure(pin);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Checks if the context is correct
 *
 * @param Context           Context to check
 *
 * @return
 * Returns `true` if the context is correct, otherwise `false`
 */
//==========================================================================================================================================
static bool IsContextContext( oC_LED_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_LedContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief Sets light on the LED
 *
 * The function sets light on the LED. It uses the #oC_LED_Context_t structure to get configuration. Look at the structure description
 *
 * @param Context           Driver context
 * @param LedIndex          Index of the LED
 * @param Light             Light to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 */
//==========================================================================================================================================
static oC_ErrorCode_t SetLight( oC_LED_Context_t Context , oC_LED_Index_t LedIndex , uint8_t Light )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    oC_LED_Mode_t  mode      = Context->Config.Mode;
    bool           usePwm    = oC_Bits_AreBitsSetU32(mode,oC_LED_Mode_UsePWM);
    oC_Pins_t      pin       = Context->Config.Pins[LedIndex];

    if(usePwm)
    {
        errorCode = oC_PWM_SetValue(Context->PwmContexts[LedIndex],Light);
    }
    else
    {
        if(Light == 0)
        {
            errorCode = oC_GPIO_WriteData(pin,0);
        }
        else
        {
            errorCode = oC_GPIO_WriteData(pin,oC_GPIO_LLD_PinsState_AllHigh);
        }
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
