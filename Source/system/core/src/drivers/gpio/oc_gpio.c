/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the GPIO driver
 *
 * @file       oc_gpio.c
 *
 * @author     Patryk Kubiak - (Created on: 24-10-2015)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_gpio.h>
#include <oc_gpio_lld.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <string.h>
#include <oc_event.h>
#include <oc_ktime.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         GPIO
#define DRIVER_FILE_NAME    "gpio"
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_GPIO_Configure
#define DRIVER_UNCONFIGURE  oC_GPIO_Unconfigure
#define DRIVER_TURN_ON      oC_GPIO_TurnOn
#define DRIVER_TURN_OFF     oC_GPIO_TurnOff
#define IS_TURNED_ON        oC_GPIO_IsTurnedOn
#define HANDLE_IOCTL        oC_GPIO_Ioctl

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static       oC_Event_t     InterruptEvent  = NULL;
static const oC_Allocator_t Allocator       = {
                .Name = "gpio",
                .CorePwd = oC_MemMan_CORE_PWD
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void InterruptHandler( oC_Pins_t Pins );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the GPIO module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 * It also turns on the LLD layer.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_GPIO))
    {
        errorCode = oC_GPIO_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_ErrorCode_None;
        }

        if(!oC_ErrorOccur(errorCode))
        {
            InterruptEvent = oC_Event_New(oC_Event_State_Inactive , &Allocator , AllocationFlags_CanWait1Second);

            if(
                ErrorCondition( oC_Event_IsCorrect(InterruptEvent) , oC_ErrorCode_AllocationError) &&
                oC_AssignErrorCode( &errorCode , oC_GPIO_LLD_SetDriverInterruptHandler(InterruptHandler))
                )
            {
                oC_Module_TurnOn(oC_Module_GPIO);
                errorCode = oC_ErrorCode_None;
            }
        }

    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the GPIO driver
 *
 * The function for turning off the GPIO driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_ExitCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_GPIO);

        errorCode = oC_GPIO_LLD_TurnOffDriver();

        if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {
            errorCode = oC_ErrorCode_None;
        }

        if(!oC_Event_Delete(&InterruptEvent,AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_CannotDeleteObject;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_GPIO_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_GPIO);
}

//==========================================================================================================================================
/**
 * @brief configures GPIO pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_GPIO_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_Configure( const oC_GPIO_Config_t * Config , oC_GPIO_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config) , oC_ErrorCode_WrongConfigAddress) &&
            oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)        , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            oC_Pins_t pins              = Config->Pins;
            bool      unlocked          = false;
            bool      used              = false;
            oC_Pins_t * pinsReference   = ksmartalloc(sizeof(oC_Pins_t),&Allocator,AllocationFlags_CanWait1Second);


            oC_IntMan_EnterCriticalSection();
            if(
                oC_AssignErrorCodeIfFalse(&errorCode , isram(pinsReference) , oC_ErrorCode_AllocationError ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration( pins))  &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUnlocked(pins,&unlocked) ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUsed(pins,&used) ) &&
                oC_AssignErrorCodeIfFalse(&errorCode , used == false , oC_ErrorCode_PinIsUsed ) &&
                (unlocked == true || oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_UnlockProtection(pins,Config->Protection) )) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetSpeed(pins,Config->Speed) ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetCurrent(pins,Config->Current) ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(pins,Config->Mode) ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPull(pins,Config->Pull) ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetOutputCircuit(pins,Config->OutputCircuit) ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetInterruptTrigger(pins,Config->InterruptTrigger) ) &&
                oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(pins))
                )
            {
                *pinsReference  = pins;
                *outContext     = pinsReference;
                errorCode       = oC_ErrorCode_None;
            }
            else
            {
                if(used)
                {
                    oC_ErrorCode_t tempErrorCode = oC_GPIO_LLD_SetPinsUnused(pins);

                    if(oC_ErrorOccur(tempErrorCode))
                    {
                        oC_SaveError("GPIO-Cfg/Setting pins unused" , tempErrorCode);
                    }
                }
                if(pinsReference)
                {
                    if(ksmartfree(pinsReference,sizeof(oC_Pins_t),AllocationFlags_CanWaitForever)==false)
                    {
                        oC_SaveError("GPIO-Cfg/release context" , oC_ErrorCode_ReleaseError);
                    }
                }
            }
            oC_IntMan_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Restores default state on pins
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_Unconfigure( const oC_GPIO_Config_t * Config , oC_GPIO_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config) , oC_ErrorCode_WrongConfigAddress) &&
            oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)        , oC_ErrorCode_OutputAddressNotInRAM)
            )
        {
            errorCode = oC_GPIO_LLD_SetPinsUnused(Config->Pins);

            if(ksmartfree(*outContext,sizeof(oC_Pins_t),AllocationFlags_CanWaitForever)==false)
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_Ioctl( oC_GPIO_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Context) , oC_ErrorCode_WrongAddress)
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Toggle pins command
             */
            //==============================================================================================================================
            case oC_GPIO_Ioctl_Command_TogglePins:
                oC_GPIO_LLD_TogglePinsState(*Context);
                errorCode = oC_ErrorCode_None;
                break;
            //==============================================================================================================================
            /*
             * Set pins state command
             */
            //==============================================================================================================================
            case oC_GPIO_Ioctl_Command_SetPinsState:
                if(oC_AssignErrorCodeIfFalse(&errorCode,isaddresscorrect(Data),oC_ErrorCode_WrongAddress))
                {
                    oC_GPIO_LLD_SetPinsState(*Context , gen_point_value(oC_GPIO_LLD_PinsState_t,Data));
                    errorCode = oC_ErrorCode_None;
                }
                break;
            //==============================================================================================================================
            /*
             * Get high pins state command
             */
            //==============================================================================================================================
            case oC_GPIO_Ioctl_Command_GetHighPinsState:
                if(
                    oC_AssignErrorCodeIfFalse(&errorCode,isram(Data),oC_ErrorCode_OutputAddressNotInRAM) &&
                    oC_AssignErrorCodeIfFalse(&errorCode,oC_GPIO_LLD_ArePinsCorrect(*Context),oC_ErrorCode_GPIOPinIncorrect)
                    )
                {
                    gen_point_value(oC_GPIO_LLD_PinsState_t,Data) = oC_GPIO_LLD_GetHighStatePins(*Context);
                    errorCode                                     = oC_ErrorCode_None;
                }
                break;
            //==============================================================================================================================
            /*
             * Get low pins state command
             */
            //==============================================================================================================================
            case oC_GPIO_Ioctl_Command_GetLowPinsState:
                if(
                    oC_AssignErrorCodeIfFalse(&errorCode,isram(Data),oC_ErrorCode_OutputAddressNotInRAM) &&
                    oC_AssignErrorCodeIfFalse(&errorCode,oC_GPIO_LLD_ArePinsCorrect(*Context),oC_ErrorCode_GPIOPinIncorrect)
                    )
                {
                    gen_point_value(oC_GPIO_LLD_PinsState_t,Data) = oC_GPIO_LLD_GetHighStatePins(*Context);
                    errorCode                                     = oC_ErrorCode_None;
                }
                break;
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_QuickOutput( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        bool      unlocked          = false;
        bool      used              = false;

        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration( Pins))  &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUnlocked(Pins,&unlocked) ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUsed(Pins,&used) ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , used == false , oC_ErrorCode_PinIsUsed ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , unlocked      , oC_ErrorCode_GPIOPinNeedUnlock ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pins,oC_GPIO_Mode_Output) ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetOutputCircuit(Pins,oC_GPIO_OutputCircuit_PushPull) ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode       = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_QuickInput( oC_Pins_t Pins , oC_GPIO_Pull_t Pull , oC_GPIO_IntTrigger_t InterruptTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        bool      unlocked          = false;
        bool      used              = false;

        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUnlocked(Pins,&unlocked) ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUsed(Pins,&used) ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , used == false , oC_ErrorCode_PinIsUsed ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , unlocked      , oC_ErrorCode_GPIOPinNeedUnlock ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration( Pins))  &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pins,oC_GPIO_Mode_Input) ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPull(Pins,Pull) ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetInterruptTrigger(Pins,InterruptTrigger) ) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_QuickUnconfigure( oC_Pins_t Pins )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        errorCode = oC_GPIO_LLD_SetPinsUnused(Pins);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures speed of pin(s)
 *
 * The function is for configuration of pin(s) speed. It is parameter, that defines maximum speed of pin state changing. In most of cases,
 * more speed choice means more current consumption.
 *
 * @note this function require usage of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Speed             speed to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_SetSpeed( oC_Pins_t Pins , oC_GPIO_Speed_t Speed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pins)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetSpeed(Pins,Speed)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief reads configured speed
 *
 * The function is for reading current configuration of speed that is set on the port. It is parameter, that defines maximum speed of pin
 * state changing. In most of cases, more speed choice means more current consumption.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outSpeed          destination for the speed variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_ReadSpeed( oC_Pins_t Pins , oC_GPIO_Speed_t * outSpeed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();

        if(oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ReadSpeed(Pins,(oC_GPIO_LLD_Speed_t*)outSpeed)))
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures current of pin(s)
 *
 * The function is for configuration of the current on selected port and pin(s). This parameter is for output mode, and it defines strong
 * of the current source. If it is set to #oC_GPIO_Current_Minimum, it will be in low energy mode.
 *
 * @note this function require usage of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Current           current limit to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_SetCurrent( oC_Pins_t Pins , oC_GPIO_Current_t Current )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pins)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetCurrent(Pins,Current)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads configured current
 *
 * The function is for reading configuration of current that is set on the port. This parameter is for output mode, and it defines strong
 * of the current source. If it is set to #oC_GPIO_Current_Minimum, it will be in low energy mode.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outCurrent        destination for current variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_ReadCurrent( oC_Pins_t Pins , oC_GPIO_Current_t * outCurrent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();

        if(oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ReadCurrent(Pins,(oC_GPIO_LLD_Current_t*)outCurrent)))
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief configures mode of pin(s)
 *
 * The function is for configuration of pins mode. This parameter helps to choose if it is output or input mode.
 *
 * @note this function require usage of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Mode              mode to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_SetMode( oC_Pins_t Pins , oC_GPIO_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pins)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetMode(Pins,Mode)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief reads mode configuration
 *
 * The function is for reading pin mode. This parameter helps to choose if it is output or input mode.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outMode           destination for the mode variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_ReadMode( oC_Pins_t Pins , oC_GPIO_Mode_t * outMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();

        if(oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ReadMode(Pins,(oC_GPIO_LLD_Mode_t*)outMode)))
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief configures pull for pin(s)
 *
 * The function is for configuration of pull for pins. This parameter is for input mode, and defines default state for pin(s).
 *
 * @note this function require usage of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param Pull              pull to configure
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_SetPull( oC_Pins_t Pins , oC_GPIO_Pull_t Pull )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pins)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPull(Pins,Pull)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief reads pull configuration
 *
 * The function is for reading pull configuration. This parameter is for input mode, and defines default state for pin(s).
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outPull           destination for the pull variable
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_ReadPull( oC_Pins_t Pins , oC_GPIO_Pull_t * outPull )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();

        if(oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ReadPull(Pins,(oC_GPIO_LLD_Pull_t*)outPull)))
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief configures output circuit
 *
 * The function if for configuration of output circuit for pin(s). This parameter is for output mode, and helps to select between open drain
 * and push-pull mode.
 *
 * @note this function require usage of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param OutputCircuit     value to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_SetOutputCircuit( oC_Pins_t Pins , oC_GPIO_OutputCircuit_t OutputCircuit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pins)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetOutputCircuit(Pins,OutputCircuit)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief reads output circuit configuration
 *
 * The function is for reading configuration of the output circuit parameter. This parameter is for output mode, and helps to select between
 * open drain and push-pull mode.
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins              variable with single or more pins from the same port
 * @param outOutputCircuit  destination for the returned value
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_ReadOutputCircuit( oC_Pins_t Pins , oC_GPIO_OutputCircuit_t * outOutputCircuit )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();

        if(oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ReadOutputCircuit(Pins,(oC_GPIO_LLD_OutputCircuit_t*)outOutputCircuit)))
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief configures interrupt for pin(s)
 *
 * The function is for configuration of the interrupt for pins in GPIO input mode. The parameter defines trigger source for interrupts in
 * input mode. The trigger can be also set to #oC_GPIO_IntTrigger_Off, and in this case it will disable any interrupts on this pin(s).
 *
 * @note this function require usage of #oC_GPIO_BeginConfiguration and #oC_GPIO_FinishConfiguration functions before and after
 * configuration
 *
 * @param Pins              variable with single or more pins from the same port
 * @param InterruptTrigger  value to set
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_SetInterruptTrigger( oC_Pins_t Pins , oC_GPIO_IntTrigger_t InterruptTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();
        if(
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_BeginConfiguration(Pins)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetInterruptTrigger(Pins,InterruptTrigger)) &&
            oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_FinishConfiguration(Pins))
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief reads interrupt trigger
 *
 * The function is for reading configuration of interrupt trigger. The parameter defines trigger source for interrupts in input mode. The
 * trigger can be also set to #oC_GPIO_IntTrigger_Off, and in this case it will disable any interrupts on this pin(s).
 *
 * @warning it is recommended to use this function for single pin, because if some of pins will be set to different value than others, the
 * function will return error.
 *
 * @param Pins                  variable with single or more pins from the same port
 * @param outInterruptTrigger   destination for the returned value
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_ReadInterruptTrigger( oC_Pins_t Pins , oC_GPIO_IntTrigger_t * outInterruptTrigger )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        oC_IntMan_EnterCriticalSection();

        if(oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_ReadInterruptTrigger(Pins,(oC_GPIO_LLD_IntTrigger_t*)outInterruptTrigger)))
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}


const char * oC_GPIO_GetCurrentNameFromEnum(oC_GPIO_Current_t Current)
{
    const char * name;

    switch(Current)
    {
        case oC_GPIO_Current_Default:
            name = "Default";
            break;
        case oC_GPIO_Current_Minimum:
            name = "Minimum";
            break;
        case oC_GPIO_Current_Medium:
            name = "Medium";
            break;
        case oC_GPIO_Current_Maximum:
            name = "Maximum";
            break;
        default:
            name = "Unknown";
            break;
    }

    return name;
}
const char * oC_GPIO_GetModeNameFromEnum(oC_GPIO_Mode_t Mode)
{
    const char * name;

    switch(Mode)
    {
        case oC_GPIO_Mode_Default:
            name = "Default";
            break;
        case oC_GPIO_Mode_Input:
            name = "Input";
            break;
        case oC_GPIO_Mode_Output:
            name = "Output";
            break;
        case oC_GPIO_Mode_Alternate:
            name = "Alternate";
            break;
        default:
            name = "Unknown";
            break;
    }

    return name;
}
const char * oC_GPIO_GetOutputCircuitNameFromEnum(oC_GPIO_OutputCircuit_t OutputCircuit)
{
    const char * name;

    switch(OutputCircuit)
    {
        case oC_GPIO_OutputCircuit_Default:
            name = "Default";
            break;
        case oC_GPIO_OutputCircuit_OpenDrain:
            name = "OpenDrain";
            break;
        case oC_GPIO_OutputCircuit_PushPull:
            name = "PushPull";
            break;
        default:
            name = "Unknown";
            break;
    }

    return name;
}

const char * oC_GPIO_GetPullNameFromEnum(oC_GPIO_Pull_t Pull)
{
    const char * name;

    switch(Pull)
    {
        case oC_GPIO_Pull_Default:
            name = "Default";
            break;
        case oC_GPIO_Pull_Down:
            name = "Down";
            break;
        case oC_GPIO_Pull_Up:
            name = "Up";
            break;
        default:
            name = "Unknown";
            break;
    }

    return name;
}

const char * oC_GPIO_GetSpeedNameFromEnum(oC_GPIO_Speed_t Speed)
{
    const char * name;

    switch(Speed)
    {
        case oC_GPIO_Speed_Default:
            name = "Default";
            break;
        case oC_GPIO_Speed_Maximum:
            name = "Maximum";
            break;
        case oC_GPIO_Speed_Minimum:
            name = "Minimum";
            break;
        case oC_GPIO_Speed_Medium:
            name = "Medium";
            break;
        default:
            name = "Unknown";
            break;
    }

    return name;
}

const char * oC_GPIO_GetIntTriggerNameFromEnum(oC_GPIO_IntTrigger_t IntTrigger)
{
    const char * name;

    switch(IntTrigger)
    {
        default:
        case oC_GPIO_IntTrigger_Off        :
            name = "Off";
            break;
        case oC_GPIO_IntTrigger_RisingEdge :
            name = "RisingEdge";
            break;
        case oC_GPIO_IntTrigger_FallingEdge:
            name = "FallingEdge";
            break;
        case oC_GPIO_IntTrigger_BothEdges  :
            name = "BothLevels";
            break;
        case oC_GPIO_IntTrigger_HighLevel  :
            name = "HighLevel";
            break;
        case oC_GPIO_IntTrigger_LowLevel   :
            name = "LowLevel";
            break;
        case oC_GPIO_IntTrigger_BothLevels :
            name = "BothLevels";
            break;
            name = "Unknown";
            break;
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief searching pin by its name
 *
 * The function is for searching a pin definition according to its name
 *
 * @param Name      Name of the pin to find (for example "PF4")
 * @param outPin    [out] Destination for a pin
 *
 * @return code of error
 *
 * Example of usage:
 * @code{.c}
   oC_Pin_t pin = 0;

   if(oC_GPIO_FindPinByName( "PF4" , &pin) == oC_ErrorCode_None")
   {
       printf("Found pin PF4!\n\r");
   }
   else
   {
       printf("Pin PF4 does not exist!\n\r");
   }
   @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_FindPinByName( const char * Name , oC_Pin_t * outPin )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        if(
            ErrorCondition(isram(outPin)          , oC_ErrorCode_OutputAddressNotInRAM) &&
            ErrorCondition(isaddresscorrect(Name) , oC_ErrorCode_WrongAddress)
            )
        {
            errorCode = oC_ErrorCode_PinNotDefined;

            oC_Pin_ForeachDefined(pin)
            {
                if(strcmp(pin->Name,Name) == 0)
                {
                    *outPin   = pin->Pin;
                    errorCode = oC_ErrorCode_None;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for interrupt in the pins
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_WaitForPins( oC_Pins_t Pins , oC_Time_t Timeout, oC_Time_t Period )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO)
     && ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins), oC_ErrorCode_GPIOPinIncorrect )
     && ErrorCondition( Timeout >= 0, oC_ErrorCode_TimeNotCorrect )
     && ErrorCondition( Period >= 0 , oC_ErrorCode_TimeNotCorrect )
        )
    {
        if ( Period > 0 )
        {
            errorCode = oC_ErrorCode_Timeout;
            oC_IntMan_EnterCriticalSection();
            oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
            oC_GPIO_LLD_PinsState_t pinsState = oC_GPIO_LLD_GetPinsState(Pins);
            oC_IntMan_ExitCriticalSection();
            while( gettimestamp() <= endTimestamp )
            {
                sleep( Period );
                oC_IntMan_EnterCriticalSection();
                if ( pinsState != oC_GPIO_LLD_GetPinsState(Pins) )
                {
                    errorCode = oC_ErrorCode_None;
                    break;
                }
                oC_IntMan_ExitCriticalSection();
            }
        }
        else
        {
            oC_Event_ClearStateBits(InterruptEvent , oC_GPIO_LLD_GetPinsMaskOfPins(Pins));
            if(oC_Event_WaitForState(InterruptEvent , Pins , oC_GPIO_LLD_GetImportantBitsMaskForPins(Pins) , Timeout))
            {
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_Timeout;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for a given pins state
 *
 * The function is similar to #oC_GPIO_WaitForPins, but it waits for the specified pins state - it is helpful for interrupt trigger
 * #oC_GPIO_IntTrigger_BothEdges and #oC_GPIO_IntTrigger_BothLevels
 *
 * @param Pins          variable with single or more pins from the same port
 * @param PinsState     expected state of the given pins
 * @param Timeout       maximum time for the operation
 * @param Period        period of polling the pins state - set it to 0 if interrupt should be used
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * Standard possible error codes:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | Module is turned off. Please call #oC_GPIO_TurnOn function
 *  oC_ErrorCode_GPIOPinIncorrect                | `Pins` are not correct (please check if all defined pins comes from the same channel
 *  oC_ErrorCode_TimeNotCorrect                  | `Timeout` cannot be negative
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GPIO_WaitForPinsState( oC_Pins_t Pins , oC_GPIO_PinsState_t PinsState , oC_Time_t Timeout, oC_Time_t Period )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GPIO))
    {
        if (
             ErrorCondition( oC_GPIO_LLD_ArePinsCorrect(Pins), oC_ErrorCode_GPIOPinIncorrect )
          && ErrorCondition( Timeout >= 0                    , oC_ErrorCode_TimeNotCorrect   )
                )
        {

        }
        oC_Timestamp_t endTimestamp = oC_KTime_GetTimestamp() + (oC_Timestamp_t)Timeout;
        errorCode = oC_ErrorCode_Timeout;
        while( oC_KTime_GetTimestamp() <= endTimestamp )
        {
            if ( oC_GPIO_IsPinsState(Pins, PinsState)
              || ErrorCode( oC_GPIO_WaitForPins(Pins,gettimeout(endTimestamp), Period) )
                )
            {
                if ( oC_GPIO_IsPinsState(Pins, PinsState) )
                {
                    errorCode = oC_ErrorCode_None;
                    break;
                }
            }
            else if ( errorCode != oC_ErrorCode_Timeout
                   && errorCode != oC_ErrorCode_None )
            {
                break;
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interrupts
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static void InterruptHandler( oC_Pins_t Pins )
{
    oC_Event_SetState(InterruptEvent,Pins);
}

#undef  _________________________________________INTERRUPTS_SECTION_________________________________________________________________________

