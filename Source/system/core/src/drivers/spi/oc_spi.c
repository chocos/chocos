/** ****************************************************************************************************************************************
 *
 * @file       oc_spi.c
 * 
 * File based on driver.c Ver 1.1.1
 *
 * @brief      The file with source for SPI driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2017-07-20 - 00:42:25)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_spi.h>
#include <oc_spi_lld.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_mutex.h>
#include <oc_semaphore.h>
#include <oc_intman.h>

#ifdef oC_SPI_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         SPI
#define DRIVER_FILE_NAME    "spi"
#define DRIVER_DEBUGLOG     true
#define DRIVER_TYPE         COMMUNICATION_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_SPI_Configure
#define DRIVER_UNCONFIGURE  oC_SPI_Unconfigure
#define DRIVER_TURN_ON      oC_SPI_TurnOn
#define DRIVER_TURN_OFF     oC_SPI_TurnOff
#define IS_TURNED_ON        oC_SPI_IsTurnedOn
#define HANDLE_IOCTL        oC_SPI_Ioctl
#define READ_FROM_DRIVER    oC_SPI_Read
#define WRITE_TO_DRIVER     oC_SPI_Write

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define NUMBER_OF_CHANNELS                      oC_ModuleChannel_NumberOfElements(SPI)
#define DEFAULT_QUEUE_SIZE                      kB(1)

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores SPI context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t      ObjectControl;
    oC_SPI_Config_t         Config;
    oC_SPI_Channel_t        Channel;
    oC_Mutex_t              ChannelBusy;
    oC_Semaphore_t          DataReadyToReceive;
    oC_Semaphore_t          ReadyToSend;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all SPI driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "spi module"
};
//==========================================================================================================================================
/**
 * Stores contextes for channels
 */
//==========================================================================================================================================
static oC_SPI_Context_t Contextes[NUMBER_OF_CHANNELS];

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsContextCorrect            ( oC_SPI_Context_t Context );
static inline oC_ErrorCode_t    CheckConfiguration          ( const oC_SPI_Config_t * Config );
static inline oC_ErrorCode_t    CreateContext               ( const oC_SPI_Config_t * Config , oC_SPI_Context_t * outContext );
static inline oC_ErrorCode_t    ReserveChannel              ( oC_SPI_Context_t Context );
static inline oC_ErrorCode_t    ReleaseChannel              ( oC_SPI_Context_t Context );
static inline oC_ErrorCode_t    DeleteContext               ( oC_SPI_Context_t * Context );
static inline oC_ErrorCode_t    ConfigureLLD                ( oC_SPI_Context_t Context );
static inline oC_ErrorCode_t    UnconfigureLLD              ( oC_SPI_Context_t Context );
static inline oC_ErrorCode_t    ConfigureChipSelectPins     ( oC_SPI_Context_t Context );
static inline oC_ErrorCode_t    UnconfigureChipSelectPins   ( oC_SPI_Context_t Context );
static inline oC_ErrorCode_t    SetChipSelectState          ( oC_SPI_Context_t Context , oC_SPI_ChipSelectId_t ChipSelectID , bool Active );
static        void              InterruptHandler            ( oC_SPI_Channel_t Channel , oC_SPI_LLD_InterruptSource_t Source );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the SPI module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 * It also turns on the LLD layer.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_SPI))
    {
        errorCode = oC_SPI_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_ErrorCode_None;
        }

        if(!oC_ErrorOccur(errorCode))
        {
            if(ErrorCode( oC_SPI_LLD_SetInterruptHandler( InterruptHandler ) ) )
            {
                memset(Contextes,0,sizeof(Contextes));

                /* This must be always at the end of the function */
                oC_Module_TurnOn(oC_Module_SPI);
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the SPI driver
 *
 * The function for turning off the SPI driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_SPI);

        errorCode = oC_SPI_LLD_TurnOffDriver();

        if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_SPI_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_SPI);
}

//==========================================================================================================================================
/**
 * @brief configures SPI pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_SPI_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_Configure( const oC_SPI_Config_t * Config , oC_SPI_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI))
    {
        driverlog( oC_LogType_Track, "Configuration of the SPI channel" );

        if(
            ErrorCondition( isaddresscorrect(Config)        , oC_ErrorCode_WrongConfigAddress       )
         && ErrorCondition( isram(outContext)               , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCode     ( CheckConfiguration(Config)                                              )
            )
        {
            if( ErrorCode( CreateContext(Config,outContext)    ) )
            {
                if( ErrorCode( ConfigureLLD( *outContext ) ) )
                {
                    if( ErrorCode( ConfigureChipSelectPins( *outContext ) ) )
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                    if( oC_ErrorOccur(errorCode) && oC_SaveIfErrorOccur( "Cannot unconfigure LLD", UnconfigureLLD(*outContext) ) )
                    {
                        driverlog( oC_LogType_Error, "Cannot unconfigure LLD" );
                    }
                }
                if( oC_ErrorOccur(errorCode) && oC_SaveIfErrorOccur( "Cannot delete context" , DeleteContext(outContext) ) )
                {
                    driverlog( oC_LogType_Error, "Cannot release SPI context memory" );
                }
            }
        }
        else
        {
            driverlog( oC_LogType_Error , "Input parameter is incorrect: %R", errorCode );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_SPI_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_Unconfigure( const oC_SPI_Config_t * Config , oC_SPI_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI))
    {
        driverlog( oC_LogType_Track, "Unconfiguration of the SPI channel" );

        if(
            ErrorCondition( isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM  )
         && ErrorCondition( IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect      )
            )
        {
            oC_SPI_Context_t context = *outContext;

            if( ErrorCondition( oC_Mutex_Take(context->ChannelBusy,s(1)), oC_ErrorCode_ModuleBusy ) )
            {
                oC_IntMan_EnterCriticalSection();

                if(
                    ErrorCode( UnconfigureChipSelectPins( context   ) )
                 && ErrorCode( UnconfigureLLD           ( context   ) )
                 && ErrorCode( DeleteContext            ( outContext) )
                    )
                {
                    errorCode = oC_ErrorCode_None;
                }

                oC_IntMan_ExitCriticalSection();
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of errror
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_Ioctl( oC_SPI_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI)
     && ErrorCondition( isaddresscorrect(Context)               , oC_ErrorCode_WrongAddress         )
     && ErrorCondition( oC_Ioctl_IsCorrectCommand(Command)      , oC_ErrorCode_CommandNotCorrect    )
     && ErrorCondition( IsContextCorrect(Context)               , oC_ErrorCode_ContextNotCorrect    )
        )
    {
        switch(Command)
        {
            case oC_IoCtl_SpecialCommand_ClearRxFifo:
                errorCode = oC_SPI_LLD_ClearRxFifo(Context->Channel);
                break;
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief reads buffer from the driver
 * 
 * The function is for reading buffer by using SPI driver. It is called when someone will read the driver file. 
 *
 * @param Context       Context of the driver
 * @param outBuffer     Buffer for data
 * @param Size          Size of the buffer on input, on output number of read bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_Read( oC_SPI_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout  )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI) 
     && ErrorCondition( IsContextCorrect(Context)                                    , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outBuffer)                                             , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(Size)                                                  , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                                                  , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                                                 , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_Mutex_Take(Context->ChannelBusy,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout                  )
        )
    {
        if( ErrorCondition( oC_Semaphore_Take(Context->DataReadyToReceive, gettimeout(endTimestamp)) , oC_ErrorCode_Timeout ) )
        {
            oC_IntMan_EnterCriticalSection();

            uint32_t receivedBytes      = 0;
            uint32_t bufferSize         = *Size;
            uint32_t elementSizeInBytes = Context->Config.DataSize / 8 + ((Context->Config.DataSize % 8) != 0 ? 1 : 0);

            while( oC_SPI_LLD_IsRxEmpty(Context->Channel) == false && receivedBytes < bufferSize)
            {
                oC_SPI_LLD_Data_t data = oC_SPI_LLD_GetReceivedData(Context->Channel);

                for(uint32_t byteIndex = 0; byteIndex < elementSizeInBytes && receivedBytes < bufferSize; byteIndex++)
                {
                    outBuffer[receivedBytes++] = (char)(data & 0xFF);
                    data = data >> 8;
                }

                if(data != 0)
                {
                    driverlog( oC_LogType_Warning, "Dropping part of SPI data (0x%X)- output buffer is not aligned", data );
                }
            }

            if(receivedBytes > 0)
            {
                *Size     = receivedBytes;
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                driverlog( oC_LogType_Error, "Unexpected behavior - semaphore is taken, but there is no more data to receive" );
                errorCode = oC_ErrorCode_NoDataToReceive;
            }

            if( oC_SPI_LLD_IsRxEmpty(Context->Channel) == false )
            {
                oC_Semaphore_Give(Context->DataReadyToReceive);
            }

            oC_IntMan_ExitCriticalSection();
        }
        oC_Mutex_Give(Context->ChannelBusy);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief write buffer to the driver
 * 
 * The function is for writing buffer by using SPI driver. It is called when someone will write the driver file. 
 *
 * @param Context       Context of the driver
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer on input, on output number of written bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_Write( oC_SPI_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_SPI) 
     && ErrorCondition( IsContextCorrect(Context)                                    , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isaddresscorrect(Buffer)                                     , oC_ErrorCode_WrongAddress            )
     && ErrorCondition( isram(Size)                                                  , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                                                  , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                                                 , oC_ErrorCode_TimeNotCorrect          )
     && ErrorCondition( oC_Mutex_Take(Context->ChannelBusy,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout                 )
        )
    {
        uint32_t sentBytes          = 0;
        uint32_t bufferSize         = *Size;
        uint32_t elementSizeInBytes = Context->Config.DataSize / 8 + ((Context->Config.DataSize % 8) != 0 ? 1 : 0);

        while( ErrorCondition( oC_Semaphore_Take( Context->ReadyToSend, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout ) )
        {
            oC_IntMan_EnterCriticalSection();

            while( oC_SPI_LLD_IsTxFull(Context->Channel) == false && sentBytes < bufferSize)
            {
                oC_SPI_LLD_Data_t data = 0;

                for(uint32_t byteIndex = 0; byteIndex < elementSizeInBytes && sentBytes < bufferSize; byteIndex++)
                {
                    data  = data << 8;
                    data |= ((oC_SPI_LLD_Data_t)Buffer[sentBytes++]);
                }

                oC_SPI_LLD_PutDataToTransmit(Context->Channel,data);
            }

            if( oC_SPI_LLD_IsTxFull(Context->Channel) == false )
            {
                oC_Semaphore_Give(Context->ReadyToSend);
            }

            oC_IntMan_ExitCriticalSection();

            if(sentBytes == bufferSize)
            {
                driverlog( oC_LogType_GoodNews, "%s - %d bytes of data has been sent" , oC_Channel_GetName(Context->Channel), sentBytes * elementSizeInBytes);
                *Size     = sentBytes;
                errorCode = oC_ErrorCode_None;
                break;
            }

        }

        oC_Mutex_Give(Context->ChannelBusy);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief changes active chip select pin
 *
 * The function is for changing active chip (by choosing active chip select). It is available only in master mode.
 *
 * @note
 * If you want to be sure, that other threads will not change the active chip, you should use functions #oC_SPI_LockChannel and #oC_SPI_UnlockChannel
 * for threads synchronization.
 *
 * @param Context           Context of the driver
 * @param ChipSelectId      ID of the chip select in the chip select array (it was given in the SPI configuration)
 * @param Active            Set it to `true` if the given chip should be activated or `false` if it should be not active
 *
 * @return code of error or #oC_ErrorCode_None if success
 *
 * @see oC_SPI_LockChannel
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_SetChip( oC_SPI_Context_t Context , oC_SPI_ChipSelectId_t ChipSelectId , bool Active )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI) )
    {
        if(
            ErrorCondition( IsContextCorrect(Context)                                       , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( Context->Config.Mode == oC_SPI_Mode_Master                      , oC_ErrorCode_NotMasterMode        )
         && ErrorCondition( ChipSelectId < oC_SPI_MAX_CHIP_SELECT_PINS                      , oC_ErrorCode_ChipIdNotCorrect     )
         && ErrorCondition( Context->Config.Pins.ChipSelect[ChipSelectId] != oC_Pin_NotUsed , oC_ErrorCode_ChipIdNotCorrect     )
            )
        {
            oC_IntMan_EnterCriticalSection();
            errorCode = SetChipSelectState(Context,ChipSelectId,Active);
            oC_IntMan_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief transmits and receives data via SPI
 *
 * The function is for sending and receive data by using SPI
 *
 * @param Context           Context of the driver
 * @param TxData            TX buffer to send
 * @param outRxData         RX buffer for received data
 * @param Count             Number of elements in the buffers (Tx and Rx)
 * @param Timeout           Maximum time to wait for transmission
 *
 * @return code of error or `oC_ErrorCode_None` if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_Tranceive( oC_SPI_Context_t Context , const void * TxData , void * outRxData , uint32_t Count , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)                                       , oC_ErrorCode_ContextNotCorrect      )
         && ErrorCondition( isaddresscorrect(TxData)                                        , oC_ErrorCode_WrongAddress           )
         && ErrorCondition( isram(outRxData)                                                , oC_ErrorCode_OutputAddressNotInRAM  )
         && ErrorCondition( Count > 0                                                       , oC_ErrorCode_SizeNotCorrect         )
         && ErrorCondition( Timeout >= 0                                                    , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( oC_Mutex_Take(Context->ChannelBusy,gettimeout(endTimestamp))    , oC_ErrorCode_Timeout                  )
            )
        {
            uint32_t        receivedBytes      = 0;
            uint32_t        sentBytes          = 0;
            uint32_t        elementSizeInBytes = Context->Config.DataSize / 8 + ((Context->Config.DataSize % 8) != 0 ? 1 : 0);
            uint32_t        bufferSize         = Count * elementSizeInBytes;
            const uint8_t*  txBuffer           = TxData;
            uint8_t*        rxBuffer           = outRxData;

            while( ErrorCondition( oC_Semaphore_Take( Context->ReadyToSend, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout ) )
            {
                oC_IntMan_EnterCriticalSection();

                while( oC_SPI_LLD_IsRxEmpty(Context->Channel) == false && receivedBytes < bufferSize)
                {
                    oC_SPI_LLD_Data_t data = oC_SPI_LLD_GetReceivedData(Context->Channel);

                    for(uint32_t byteIndex = 0; byteIndex < elementSizeInBytes && receivedBytes < bufferSize; byteIndex++)
                    {
                        rxBuffer[receivedBytes++] = (char)(data & 0xFF);
                        data = data >> 8;
                    }

                    if(data != 0)
                    {
                        driverlog( oC_LogType_Warning, "Dropping part of SPI data (0x%X)- output buffer is not aligned", data );
                    }
                }

                while( oC_SPI_LLD_IsTxFull(Context->Channel) == false && sentBytes < bufferSize)
                {
                    oC_SPI_LLD_Data_t data = 0;

                    for(uint32_t byteIndex = 0; byteIndex < elementSizeInBytes && sentBytes < bufferSize; byteIndex++)
                    {
                        data  = data << 8;
                        data |= ((oC_SPI_LLD_Data_t)txBuffer[sentBytes++]);
                    }

                    oC_SPI_LLD_PutDataToTransmit(Context->Channel,data);
                }

                if( oC_SPI_LLD_IsTxFull(Context->Channel) == false )
                {
                    oC_Semaphore_Give(Context->ReadyToSend);
                }

                if( oC_SPI_LLD_IsRxEmpty(Context->Channel) == false )
                {
                    oC_Semaphore_Give(Context->DataReadyToReceive);
                }

                oC_IntMan_ExitCriticalSection();

                if(sentBytes == bufferSize)
                {
                    driverlog( oC_LogType_GoodNews, "All data (%u B) has been sent", sentBytes );
                    break;
                }
            }

            if(receivedBytes < bufferSize)
            {
                driverlog( oC_LogType_Track, "Waiting for the data on RX (expected %d bytes)", bufferSize - receivedBytes);
            }

            while( receivedBytes < bufferSize && ErrorCondition( oC_Semaphore_Take( Context->DataReadyToReceive, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout ) )
            {
                oC_IntMan_EnterCriticalSection();

                while( oC_SPI_LLD_IsRxEmpty(Context->Channel) == false && receivedBytes < bufferSize)
                {
                    oC_SPI_LLD_Data_t data = oC_SPI_LLD_GetReceivedData(Context->Channel);

                    for(uint32_t byteIndex = 0; byteIndex < elementSizeInBytes && receivedBytes < bufferSize; byteIndex++)
                    {
                        rxBuffer[receivedBytes++] = (char)(data & 0xFF);
                        data = data >> 8;
                    }

                    if(data != 0)
                    {
                        driverlog( oC_LogType_Warning, "Dropping part of SPI data (0x%X)- output buffer is not aligned", data );
                    }
                }

                if( oC_SPI_LLD_IsRxEmpty(Context->Channel) == false )
                {
                    oC_Semaphore_Give(Context->DataReadyToReceive);
                }

                oC_IntMan_ExitCriticalSection();
            }

            if(receivedBytes == bufferSize && sentBytes == bufferSize)
            {
                driverlog( oC_LogType_GoodNews, "%d elements has been correctly sent and received", Count);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                driverlog( oC_LogType_Error, "Transmission is not finished - %d B sent and %d B received", sentBytes, receivedBytes);
            }

            oC_Mutex_Give(Context->ChannelBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief takes SPI channel mutex
 *
 * The function is for locking the SPI channel to prevent race issue. You can use it, when you want to prevent a race issue. Remember that
 * you have to also unlock the channel by using #oC_SPI_UnlockChannel, when it is not needed for you anymore.
 *
 * @param Context           Context of the driver
 * @param Timeout           Maximum time to wait for channel ready
 *
 * @return code of error or `oC_ErrorCode_None` when success
 *
 * @see oC_SPI_UnlockChannel
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_LockChannel( oC_SPI_Context_t Context , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)                   , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect       )
         && ErrorCondition( oC_Mutex_Take(Context->ChannelBusy,Timeout) , oC_ErrorCode_Timeout              )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unlocks SPI channel
 *
 * The function is for unlocking the SPI channel previously locked by function #oC_SPI_LockChannel.
 *
 * @param Context           Context of the driver
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * @see oC_SPI_LockChannel
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_UnlockChannel( oC_SPI_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)               , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( oC_Mutex_Give(Context->ChannelBusy)     , oC_ErrorCode_ChannelNotLocked     )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns real configured baud rate
 */
//==========================================================================================================================================
oC_BaudRate_t  oC_SPI_GetConfiguredBaudRate( oC_SPI_Context_t Context )
{
    oC_BaudRate_t baudRate = 0;

    if(oC_Module_IsTurnedOn(oC_Module_SPI) && IsContextCorrect(Context))
    {
        baudRate = oC_SPI_LLD_GetConfiguredBaudRate(Context->Channel);
    }

    return baudRate;
}

//==========================================================================================================================================
/**
 * @brief reads configured SPI channel
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SPI_ReadChannel( oC_SPI_Context_t Context , oC_SPI_Channel_t * outChannel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_SPI)
     && ErrorCondition( IsContextCorrect(Context)  , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isram(outChannel)          , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        *outChannel  = Context->Channel;
        errorCode    = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the SPI driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_SPI_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_SPIContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief checks SPI pins in the configuration structure
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t CheckPins( const oC_SPI_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_GPIO_IsPinDefined(Config->Pins.CLK)      , oC_ErrorCode_PinNotDefined )
     && ErrorCondition( oC_GPIO_IsPinDefined(Config->Pins.MISO)     , oC_ErrorCode_PinNotDefined )
     && ErrorCondition( oC_GPIO_IsPinDefined(Config->Pins.MOSI)     , oC_ErrorCode_PinNotDefined )
        )
    {
        errorCode = oC_ErrorCode_None;

        for(oC_SPI_ChipSelectId_t chipId = 0; chipId < oC_SPI_MAX_CHIP_SELECT_PINS; chipId++ )
        {
            if(Config->Pins.ChipSelect[chipId] != oC_Pin_NotUsed && oC_GPIO_IsPinDefined(Config->Pins.ChipSelect[chipId]) == false)
            {
                driverlog( oC_LogType_Error, "CS [%d] is not defined", chipId );
                errorCode = oC_ErrorCode_PinNotDefined;
                break;
            }
        }
    }
    else
    {
        driverlog( oC_LogType_Error, "CLK,MISO or MOSI is not defined\n" );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks SPI configuration
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t CheckConfiguration( const oC_SPI_Config_t * Config )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( Config->BaudRate > 0                                        , oC_ErrorCode_BaudRateNotCorrect           )
     && ErrorCondition( Config->Tolerance >= 0                                      , oC_ErrorCode_ToleranceNotCorrect          )
     && ErrorCondition( Config->Mode == oC_SPI_Mode_Master
                     || Config->Mode == oC_SPI_Mode_Slave                           , oC_ErrorCode_ModeNotCorrect               )
     && ErrorCondition( Config->FrameFormat == oC_SPI_FrameFormat_MSB
                     || Config->FrameFormat == oC_SPI_FrameFormat_LSB               , oC_ErrorCode_FrameFormatNotCorrect        )
     && ErrorCondition( Config->ClockPolarity == oC_SPI_ClockPolarity_HighActive
                     || Config->ClockPolarity == oC_SPI_ClockPolarity_LowActive     , oC_ErrorCode_ClockPolarityNotSupported    )
     && ErrorCondition( Config->ClockPhase    == oC_SPI_ClockPhase_FirstTransition
                     || Config->ClockPhase    == oC_SPI_ClockPhase_SecondTransition , oC_ErrorCode_ClockPhaseNotSupported       )
     && ErrorCondition( Config->DataSize > 0                                        , oC_ErrorCode_SizeNotCorrect               )
     && ErrorCondition( Config->DefaultChipId < oC_SPI_MAX_CHIP_SELECT_PINS         , oC_ErrorCode_ChipIdNotCorrect             )
     && ErrorCondition( Config->Advanced.Channel == 0
                     || oC_SPI_LLD_IsChannelCorrect(Config->Advanced.Channel)       , oC_ErrorCode_WrongChannel                 )
        )
    {
        errorCode = CheckPins(Config);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a SPI context
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t CreateContext( const oC_SPI_Config_t * Config , oC_SPI_Context_t * outContext )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_SPI_Context_t    context   = kmalloc( sizeof(struct Context_t), &Allocator, AllocationFlags_ZeroFill );

    if( ErrorCondition( context != NULL , oC_ErrorCode_AllocationError ) )
    {
        context->ObjectControl          = oC_CountObjectControl( context, oC_ObjectId_SPIContext );
        context->ChannelBusy            = oC_Mutex_New    ( oC_Mutex_Type_Recursive, &Allocator, AllocationFlags_ZeroFill );
        context->DataReadyToReceive     = oC_Semaphore_New( oC_Semaphore_Type_Binary, 0, &Allocator, AllocationFlags_ZeroFill );
        context->ReadyToSend            = oC_Semaphore_New( oC_Semaphore_Type_Binary, 1, &Allocator, AllocationFlags_ZeroFill );

        memcpy( &context->Config, Config, sizeof(oC_SPI_Config_t) );

        if(
            ErrorCondition( context->ChannelBusy        != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( context->DataReadyToReceive != NULL , oC_ErrorCode_AllocationError )
         && ErrorCondition( context->ReadyToSend        != NULL , oC_ErrorCode_AllocationError )
         && ErrorCode( ReserveChannel( context ) )
            )
        {
            *outContext             = context;
            errorCode               = oC_ErrorCode_None;
        }
        else
        {
            driverlog( oC_LogType_Error, "Cannot create context - %R (%p,%p,%p)", errorCode, context->ChannelBusy, context->DataReadyToReceive, context->ReadyToSend);

            oC_SaveIfFalse( "Channel Busy Mutex"    , oC_Mutex_Delete    ( &context->ChannelBusy, 0 )           , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "Data Ready Semaphore"  , oC_Semaphore_Delete( &context->DataReadyToReceive, 0 )    , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "Ready To Send"         , oC_Semaphore_Delete( &context->ReadyToSend, 0 )           , oC_ErrorCode_ReleaseError );
            oC_SaveIfFalse( "context"               , kfree(context,0)                                          , oC_ErrorCode_ReleaseError );
        }
    }
    else
    {
        driverlog( oC_LogType_Error, "Cannot allocate memory for a context" );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reserves a channel of SPI
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t ReserveChannel( oC_SPI_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Context->Config.Advanced.Channel == 0)
    {
        driverlog( oC_LogType_Track, "SPI channel is not given, searching for a free one" );

        errorCode = oC_ErrorCode_NoChannelAvailable;

        for( oC_ChannelIndex_t channelIndex = 0; channelIndex < NUMBER_OF_CHANNELS; channelIndex++ )
        {
            oC_SPI_Channel_t channel = oC_SPI_LLD_ChannelIndexToChannel(channelIndex);


            if(
                Contextes[channelIndex] == NULL
             && oC_SPI_LLD_IsPinAvailble( channel, Context->Config.Pins.MOSI, oC_SPI_PinFunction_MOSI )
             && oC_SPI_LLD_IsPinAvailble( channel, Context->Config.Pins.MISO, oC_SPI_PinFunction_MISO )
             && oC_SPI_LLD_IsPinAvailble( channel, Context->Config.Pins.CLK , oC_SPI_PinFunction_CLK  )
                )
            {
                driverlog( oC_LogType_GoodNews, "Found free channel (%s) for the given pins", oC_Channel_GetName(channel));
                errorCode               = oC_ErrorCode_None;
                Context->Channel        = channel;
                Contextes[channelIndex] = Context;
                break;
            }
        }

        if( oC_ErrorOccur(errorCode) )
        {
            driverlog( oC_LogType_Error, "Cannot find free SPI channel: %R", errorCode );
        }
    }
    else if( ErrorCondition( oC_SPI_LLD_IsChannelCorrect(Context->Config.Advanced.Channel), oC_ErrorCode_WrongChannel ) )
    {
        driverlog( oC_LogType_Track,
                   "SPI channel is given in the SPI configuration (%d - %s), using it...",
                   Context->Config.Advanced.Channel,
                   oC_Channel_GetName(Context->Config.Advanced.Channel)
                   );
        Context->Channel = Context->Config.Advanced.Channel;

        if( ErrorCondition( Contextes[oC_SPI_LLD_ChannelToChannelIndex(Context->Channel)], oC_ErrorCode_ChannelIsUsed ) )
        {
            driverlog( oC_LogType_GoodNews, "Channel is free, reserving" );
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            driverlog( oC_LogType_Error, "Channel is already used" );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases SPI channel
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t ReleaseChannel( oC_SPI_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_SPI_LLD_IsChannelCorrect(Context->Channel) , oC_ErrorCode_WrongChannel ) )
    {
        oC_SPI_LLD_ChannelIndex_t channelIndex = oC_SPI_LLD_ChannelToChannelIndex(Context->Channel);

        if( ErrorCondition( channelIndex < NUMBER_OF_CHANNELS, oC_ErrorCode_WrongChannel ) )
        {
            Contextes[channelIndex] = NULL;
            errorCode               = oC_ErrorCode_None;
        }
        else
        {
            driverlog( oC_LogType_Error, "Fatal error - SPI channel (%s) seems to be correct, but we cannot convert it to channel index!\n", oC_Channel_GetName(Context->Channel) );
        }
    }
    else
    {
        driverlog( oC_LogType_Error, "Channel in SPI context is not correct: %d", Context->Channel );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases SPI context memory
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t DeleteContext( oC_SPI_Context_t * Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    (*Context)->ObjectControl = 0;

    errorCode = oC_ErrorCode_None;

    ErrorCondition( oC_Mutex_Delete    ( &(*Context)->ChannelBusy       , 0 )  , oC_ErrorCode_ReleaseError );
    ErrorCondition( oC_Semaphore_Delete( &(*Context)->DataReadyToReceive, 0 )  , oC_ErrorCode_ReleaseError );
    ErrorCondition( oC_Semaphore_Delete( &(*Context)->ReadyToSend       , 0 )  , oC_ErrorCode_ReleaseError );
    ErrorCode( ReleaseChannel(*Context) );
    ErrorCondition( kfree( *Context, 0 ) , oC_ErrorCode_ReleaseError );

    *Context = NULL;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures SPI in LLD
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t ConfigureLLD( oC_SPI_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCode( oC_SPI_LLD_SetPower( Context->Channel , oC_Power_On ) ) )
    {
        if(
            ErrorCode( oC_SPI_LLD_ConfigurePin(Context->Channel, Context->Config.Pins.CLK    , oC_SPI_PinFunction_CLK   ) )
         && ErrorCode( oC_SPI_LLD_ConfigurePin(Context->Channel, Context->Config.Pins.MOSI   , oC_SPI_PinFunction_MOSI  ) )
         && ErrorCode( oC_SPI_LLD_ConfigurePin(Context->Channel, Context->Config.Pins.MISO   , oC_SPI_PinFunction_MISO  ) )
            )
        {
            errorCode = oC_SPI_LLD_Configure(
                            Context->Channel,
                            Context->Config.Mode,
                            Context->Config.BaudRate,
                            Context->Config.Tolerance,
                            Context->Config.ClockPhase,
                            Context->Config.ClockPolarity,
                            Context->Config.FrameFormat,
                            Context->Config.DataSize
                            );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures SPI in LLD
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t UnconfigureLLD( oC_SPI_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    errorCode = oC_ErrorCode_None;

    ErrorCode( oC_SPI_LLD_UnconfigurePin(Context->Config.Pins.CLK ) );
    ErrorCode( oC_SPI_LLD_UnconfigurePin(Context->Config.Pins.MISO) );
    ErrorCode( oC_SPI_LLD_UnconfigurePin(Context->Config.Pins.MOSI) );

    ErrorCode( oC_SPI_LLD_RestoreDefaultStateOnChannel(Context->Channel) );

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures chip selects pins
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t ConfigureChipSelectPins( oC_SPI_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( Context->Config.Mode == oC_SPI_Mode_Master )
    {
        errorCode = oC_ErrorCode_None;

        for(oC_SPI_ChipSelectId_t chipId = 0; chipId < oC_SPI_MAX_CHIP_SELECT_PINS; chipId++)
        {
            oC_Pin_t pin = Context->Config.Pins.ChipSelect[chipId];
            if( pin != oC_Pin_NotUsed )
            {
                if( ErrorCode( oC_GPIO_QuickOutput(pin) ) )
                {
                    if( ! ErrorCode( SetChipSelectState( Context, chipId, chipId == Context->Config.DefaultChipId ) ) )
                    {
                        driverlog( oC_LogType_Error, "Cannot set chip select state [%d]: %R", chipId, errorCode );
                    }
                }
                else
                {
                    driverlog( oC_LogType_Error, "Cannot configure CS [%d]: %R", chipId, errorCode );
                    break;
                }
            }
        }

        if(oC_ErrorOccur(errorCode))
        {
            UnconfigureChipSelectPins(Context);
        }
    }
    else
    {
        errorCode = oC_SPI_LLD_ConfigurePin( Context->Channel, Context->Config.Pins.ChipSelect[Context->Config.DefaultChipId], oC_SPI_PinFunction_NSS );
    }

    if(oC_ErrorOccur(errorCode))
    {
        UnconfigureChipSelectPins(Context);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures chip select pins
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t UnconfigureChipSelectPins( oC_SPI_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( Context->Config.Mode == oC_SPI_Mode_Master )
    {
        errorCode = oC_ErrorCode_None;

        for(oC_SPI_ChipSelectId_t chipId = 0; chipId < oC_SPI_MAX_CHIP_SELECT_PINS; chipId++)
        {
            oC_Pin_t pin = Context->Config.Pins.ChipSelect[chipId];
            if( pin != oC_Pin_NotUsed )
            {
                oC_GPIO_SetPinsState(pin, oC_GPIO_PinsState_AllLow);

                if( ! ErrorCode( oC_GPIO_QuickUnconfigure(pin) ) )
                {
                    driverlog( oC_LogType_Error, "Cannot unconfigure CS [%d]: %R", chipId, errorCode );
                }
            }
        }
    }
    else
    {
        errorCode = oC_SPI_LLD_UnconfigurePin( Context->Config.Pins.ChipSelect[Context->Config.DefaultChipId] );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets state of the Chip Select (only in master mode)
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t SetChipSelectState( oC_SPI_Context_t Context , oC_SPI_ChipSelectId_t ChipSelectID , bool Active )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( Context->Config.Mode == oC_SPI_Mode_Master , oC_ErrorCode_NotMasterMode ) )
    {
        oC_GPIO_PinsState_t pinsState = oC_GPIO_PinsState_AllLow;

        if(
            ( Active &&  Context->Config.ChipSelectActiveHigh)
         || (!Active && !Context->Config.ChipSelectActiveHigh)
            )
        {
            pinsState = oC_GPIO_PinsState_AllHigh;

        }
        oC_GPIO_SetPinsState( Context->Config.Pins.ChipSelect[ChipSelectID], pinsState );

        errorCode = oC_ErrorCode_None;
    }
    else
    {
        driverlog( oC_LogType_Error , "Unexpected " );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handler of SPI interrupt
 */
//==========================================================================================================================================
static void InterruptHandler( oC_SPI_Channel_t Channel , oC_SPI_LLD_InterruptSource_t Source )
{
    oC_SPI_LLD_ChannelIndex_t channelIndex = oC_SPI_LLD_ChannelToChannelIndex(Channel);

    if( Source & oC_SPI_LLD_InterruptSource_RxFifoNotEmpty )
    {
        if(Contextes[channelIndex] != NULL)
        {
            oC_Semaphore_Give( Contextes[channelIndex]->DataReadyToReceive );
        }
    }

    if( Source & oC_SPI_LLD_InterruptSource_TxFifoNotFull )
    {
        if(Contextes[channelIndex] != NULL)
        {
            oC_Semaphore_Give( Contextes[channelIndex]->ReadyToSend );
        }
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
#endif /* oC_SPI_LLD_AVAILABLE */
 
 
 
 
 
 
 
 
 
 
 
