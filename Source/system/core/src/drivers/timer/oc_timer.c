/** ****************************************************************************************************************************************
 *
 * @file       oc_timer.c
 *
 * @brief      Contains source code for the timer driver
 *
 * @author     Patryk Kubiak - (Created on: 30 08 2015 13:35:52)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_timer.h>
#define DRIVER_NAME         TIMER
#define DRIVER_FILE_NAME    "timer"
#define DRIVER_VERSION      oC_Driver_MakeVersion(0,1,0)
#define REQUIRED_DRIVERS
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_0
#define DRIVER_SOURCE

// DRIVER INTERFACE
#define DRIVER_CONFIGURE        oC_TIMER_Configure
#define DRIVER_UNCONFIGURE      oC_TIMER_Unconfigure
#define DRIVER_TURN_ON          oC_TIMER_TurnOn
#define DRIVER_TURN_OFF         oC_TIMER_TurnOff
#define IS_TURNED_ON            oC_TIMER_IsTurnedOn
#include <oc_driver.h>
#include <oc_memman.h>
#include <oc_mutex.h>
#include <oc_threadman.h>
#include <oc_gpio_lld.h>
#include <oc_stdlib.h>
#include <oc_object.h>
#include <oc_array.h>
#include <oc_intman.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Context_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_TIMER_Channel_t      Channel;
    oC_TIMER_LLD_SubTimer_t SubTimer;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static void                         EventHandler            ( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber );
static bool                         IsModeCorrect           ( oC_TIMER_Mode_t Mode );
static bool                         IsContextCorrect        ( oC_TIMER_Context_t Context );
static bool                         PinMustBeConnected      ( const oC_TIMER_Config_t * Config );
static oC_ErrorCode_t               ConnectPeripheralPin    ( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , const oC_TIMER_Config_t * Config );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t ModuleAllocator = {
                .Name               = "timer" ,
                .EventHandler       = EventHandler ,
                .EventFlags         = MemoryEventFlags_MemoryReleased
};

static bool         ModuleEnabledFlag = false;
static oC_Mutex_t   ModuleBusyMutex   = NULL;

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @addtogroup TIMER
//! @{

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , !ModuleEnabledFlag , oC_ErrorCode_ModuleIsTurnedOn))
    {
        errorCode = oC_TIMER_LLD_TurnOnDriver();

        if((errorCode == oC_ErrorCode_ModuleIsTurnedOn) || (errorCode == oC_ErrorCode_None))
        {
            ModuleBusyMutex   = oC_Mutex_New(oC_Mutex_Type_Normal,&ModuleAllocator,AllocationFlags_NoWait);
            ModuleEnabledFlag = true;
            errorCode         = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;
        errorCode         = oC_TIMER_LLD_TurnOffDriver();

        oC_Mutex_Delete(&ModuleBusyMutex,AllocationFlags_CanWaitForever);

        oC_MemMan_FreeAllMemoryOfAllocator(&ModuleAllocator);

        if((errorCode == oC_ErrorCode_ModuleNotStartedYet) || (errorCode == oC_ErrorCode_None))
        {
            errorCode         = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_TIMER_IsTurnedOn( void )
{
    return ModuleEnabledFlag;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_Configure( const oC_TIMER_Config_t * Config , oC_TIMER_Context_t * outContext )
{
    oC_ErrorCode_t          errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag                                         , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MemMan_IsRamAddress(outContext)                        , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_MemMan_IsAddressCorrect(Config)                        , oC_ErrorCode_WrongConfigAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsModeCorrect(Config->Mode)                               , oC_ErrorCode_TIMERModeNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Mutex_Take(ModuleBusyMutex,Config->MaximumTimeForWait) , oC_ErrorCode_ModuleIsBusy)
        )
    {
        static const oC_TIMER_LLD_SubTimer_t subTimers[] = {
                            oC_TIMER_LLD_SubTimer_TimerA ,
                            oC_TIMER_LLD_SubTimer_TimerB ,
                            oC_TIMER_LLD_SubTimer_Both
        };

        errorCode = oC_ErrorCode_NoChannelAvailable;

        oC_ARRAY_FOREACH_IN_ARRAY(subTimers,subTimer)
        {
            oC_TIMER_LLD_ForEachChannel(channel)
            {
                oC_IntMan_EnterCriticalSection();
                if(
                    oC_ErrorOccur(errorCode) &&
                    oC_TIMER_LLD_IsChannelUsed(channel,*subTimer) == false &&
                    oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_SetChannelUsed(channel,*subTimer)))
                {
                    oC_IntMan_ExitCriticalSection();

                    if(
                            oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_SetPower(channel,oC_Power_On)                  ) &&
                            oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeMode(channel,*subTimer,Config->Mode)      ) &&
                            oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeFrequency(channel,*subTimer,Config->Frequency,Config->PermissibleDifference) ) &&
                            oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeCountDirection(channel,*subTimer,Config->CountDirection) ) &&
                            oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeMaximumValue(channel,*subTimer,Config->MaximumValue) ) &&
                            oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeMatchValue(channel,*subTimer,Config->MatchValue) ) &&
                            (Config->EventHandler == NULL ||
                                        oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeEventHandler(channel,*subTimer,Config->EventHandler,Config->EventFlags) )
                                            ) &&
                            (PinMustBeConnected(Config) == false ||
                                        oC_AssignErrorCode(&errorCode , ConnectPeripheralPin(channel,*subTimer,Config))
                                            ) &&
                            (Config->Mode != oC_TIMER_Mode_PWM ||
                                        oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeStartPwmState(channel,*subTimer,Config->StartPwmState))
                                            ) &&
                            ((Config->Mode != oC_TIMER_Mode_InputEdgeCount && Config->Mode != oC_TIMER_Mode_InputEdgeTime) ||
                                        oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ChangeTrigger(channel,*subTimer,Config->InputTrigger))
                            )
                        )
                    {
                        struct Context_t * context = ksmartalloc(sizeof(struct Context_t) , &ModuleAllocator ,AllocationFlags_NoWait);

                        if(context)
                        {
                            context->Channel        = channel;
                            context->SubTimer       = *subTimer;
                            context->ObjectControl  = oC_CountObjectControl(context,oC_ObjectId_TimerContext);
                            errorCode               = oC_ErrorCode_None;
                            *outContext             = context;

                            oC_ARRAY_FOREACH_BREAK(subTimers);
                            break;
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_AllocationError;
                        }
                    }

                    if(errorCode != oC_ErrorCode_None)
                    {
                        if(!oC_TIMER_LLD_RestoreDefaultStateOnChannel(channel,*subTimer))
                        {
                            errorCode = oC_ErrorCode_CannotRestoreDefaultState;
                            break;
                        }
                    }

                }
                else
                {
                    oC_IntMan_ExitCriticalSection();
                }
            }
        }

        oC_Mutex_Give(ModuleBusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_Unconfigure( const oC_TIMER_Config_t * Config , oC_TIMER_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode, ModuleEnabledFlag == true ,           oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode, IsContextCorrect(*outContext) ,       oC_ErrorCode_ContextNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode, oC_MemMan_IsAddressCorrect(Config) ,  oC_ErrorCode_WrongConfigAddress)
        )
    {
        oC_TIMER_Context_t context = *outContext;

        if(oC_TIMER_LLD_RestoreDefaultStateOnChannel(context->Channel,context->SubTimer))
        {
            if(ksmartfree(context,sizeof(struct Context_t),AllocationFlags_CanWaitForever))
            {
                *outContext = NULL;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_CannotRestoreDefaultState;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_ReadValue( oC_TIMER_Context_t Context , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outValue) ,           oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        errorCode = oC_TIMER_LLD_ReadCurrentValue(Context->Channel,Context->SubTimer,outValue);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_SetValue( oC_TIMER_Context_t Context , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect))
    {
        errorCode = oC_TIMER_LLD_ChangeCurrentValue(Context->Channel,Context->SubTimer,Value);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_ReadMatchValue( oC_TIMER_Context_t Context , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outValue) ,           oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        errorCode = oC_TIMER_LLD_ReadMatchValue(Context->Channel,Context->SubTimer,outValue);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_SetMatchValue( oC_TIMER_Context_t Context , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect))
    {
        errorCode = oC_TIMER_LLD_ChangeMatchValue(Context->Channel,Context->SubTimer,Value);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_ReadMaxValue( oC_TIMER_Context_t Context , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outValue) ,           oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        errorCode = oC_TIMER_LLD_ReadMaximumValue(Context->Channel,Context->SubTimer,outValue);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_SetMaxValue( oC_TIMER_Context_t Context , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect))
    {
        errorCode = oC_TIMER_LLD_ChangeMaximumValue(Context->Channel,Context->SubTimer,Value);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_Start( oC_TIMER_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect )
            )
    {
        errorCode = oC_TIMER_LLD_TimerStart(Context->Channel,Context->SubTimer);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_TIMER_Stop( oC_TIMER_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
            oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect )
            )
    {
        errorCode = oC_TIMER_LLD_TimerStop(Context->Channel,Context->SubTimer);
    }

    return errorCode;
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________
//! @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static void EventHandler( void * Address , MemoryEventFlags_t Event , const char * Function, uint32_t LineNumber )
{

}

//==========================================================================================================================================
//==========================================================================================================================================
static bool IsModeCorrect( oC_TIMER_Mode_t Mode )
{
    return Mode <= oC_TIMER_Mode_PWM;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool IsContextCorrect( oC_TIMER_Context_t Context )
{
    return oC_MemMan_IsDynamicAllocatedAddress(Context) && oC_CheckObjectControl(Context,oC_ObjectId_TimerContext,Context->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool PinMustBeConnected( const oC_TIMER_Config_t * Config )
{
    return Config->Mode == oC_TIMER_Mode_PWM || Config->Mode == oC_TIMER_Mode_InputEdgeCount || Config->Mode == oC_TIMER_Mode_InputEdgeTime;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t ConnectPeripheralPin( oC_TIMER_Channel_t Channel , oC_TIMER_LLD_SubTimer_t SubTimer , const oC_TIMER_Config_t * Config )
{
    oC_ErrorCode_t               errorCode  = oC_ErrorCode_ImplementError;
    oC_TIMER_Pin_t pin        = 0;
    uint32_t                     size       = 1;
    bool                         pinUsed    = false;

    oC_IntMan_EnterCriticalSection();
    if(
        oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ReadModulePinsOfPin(Config->Pin,&pin,&size)) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_CheckIsPinUsed(Config->Pin,&pinUsed)) &&
        oC_AssignErrorCodeIfFalse(&errorCode , pinUsed == false , oC_ErrorCode_PinIsUsed) &&
        oC_AssignErrorCode(&errorCode , oC_GPIO_LLD_SetPinsUsed(Config->Pin)) &&
        oC_AssignErrorCode(&errorCode , oC_TIMER_LLD_ConnectModulePin(pin))
        )
    {
        errorCode = oC_ErrorCode_None;
    }
    oC_IntMan_ExitCriticalSection();

    return errorCode;
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
