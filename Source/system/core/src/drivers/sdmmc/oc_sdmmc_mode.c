/** ****************************************************************************************************************************************
 *
 * @brief      The file with helper functions for SDMMC mode functions
 * 
 * @file       oc_sdmmc_mode.c
 *
 * @author     Patryk Kubiak - (Created on: 14.08.2017 13:50:33) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define oC_SDMMC_PRIVATE

#include <oc_sdmmc.h>
#include <oc_sdmmc_private.h>
#include <oc_module.h>

#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    oC_SDMMC_DRIVER_FILE_NAME
#define DRIVER_PRIVATE
#define DRIVER_DEBUGLOG     oC_SDMMC_DEBUGLOG_ENABLED
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool IsContextCorrect( oC_SDMMC_Context_t Context );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with FUNCTIONS
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Turns on given SDMMC Mode
 *
 * @param Interface                      Pointer to the structure with SDMMC Mode Interface
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Interface is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->TurnOn
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_TurnOn( const oC_SDMMC_Mode_Interface_t * Interface )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( isaddresscorrect( Interface )           , oC_ErrorCode_WrongAddress )
     && ErrorCondition( isaddresscorrect( Interface->TurnOn )   , oC_ErrorCode_WrongAddress ) )
    {
        errorCode = Interface->TurnOn();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off given SDMMC Mode
 *
 * @param Interface             Pointer to the structure with SDMMC Mode Interface
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Interface is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->TurnOff
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_TurnOff( const oC_SDMMC_Mode_Interface_t * Interface )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( isaddresscorrect( Interface )           , oC_ErrorCode_WrongAddress )
     && ErrorCondition( isaddresscorrect( Interface->TurnOff )  , oC_ErrorCode_WrongAddress ) )
    {
        errorCode = Interface->TurnOff();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Returns true if the mode can be configured with the given configuration
 *
 * @param Config               Pointer to the structure with SDMMC configuration
 * @param Context              SDMMC Context structure
 *
 * @return
 * true if SDMMC Mode is supported
 */
//==========================================================================================================================================
bool oC_SDMMC_Mode_IsSupported( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context )
{
    bool isSupported = false;

    if( IsContextCorrect( Context )
     && isaddresscorrect( Context->Interface )
     && isaddresscorrect( Context->Interface->IsSupported )
     && isaddresscorrect( Config ) )
    {
        isSupported = Context->Interface->IsSupported( Config, Context );
    }

    return isSupported;
}

//==========================================================================================================================================
/**
 * @brief Configures mode to work with the SDMMC configuration
 *
 * @param Config               Pointer to the structure with SDMMC configuration
 * @param Context              SDMMC Context structure
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Interface or Configuration is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->Configure
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_Configure( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsContextCorrect( Context)                       , oC_ErrorCode_ContextNotCorrect )
     && ErrorCondition( isaddresscorrect( Context->Interface)            , oC_ErrorCode_WrongAddress )
     && ErrorCondition( isaddresscorrect( Context->Interface->Configure) , oC_ErrorCode_WrongAddress )
     && ErrorCondition( isaddresscorrect( Config)                        , oC_ErrorCode_WrongAddress ) )
    {
        errorCode = Context->Interface->Configure( Config, Context );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigure SDMMC Mode
 *
 * @param Context              SDMMC Context structure
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Context is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->Unconfigure
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_Unconfigure( oC_SDMMC_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsContextCorrect( Context)                           , oC_ErrorCode_ContextNotCorrect )
     && ErrorCondition( isaddresscorrect( Context->Interface)                , oC_ErrorCode_WrongAddress )
     && ErrorCondition( isaddresscorrect( Context->Interface->Unconfigure)   , oC_ErrorCode_WrongAddress ) )
    {
        errorCode = Context->Interface->Unconfigure( Context );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sends the command by using the mode
 *
 * @param Context              SDMMC Context structure
 * @param Command              pointer with structure for SDMMC Command
 * @param Timeout              timeout for operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Interface or Command is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->SendCommand
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_SendCommand( oC_SDMMC_Context_t Context , oC_SDMMC_Mode_RawCommand_t * Command , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsContextCorrect( Context)                           , oC_ErrorCode_ContextNotCorrect   )
     && ErrorCondition( isaddresscorrect( Context->Interface)                , oC_ErrorCode_InterfaceNotCorrect )
     && ErrorCondition( isaddresscorrect( Context->Interface->SendCommand)   , oC_ErrorCode_InterfaceNotCorrect )
     && ErrorCondition( isaddresscorrect( Command)                           , oC_ErrorCode_WrongAddress        )
     && ErrorCondition( Timeout >= 0                                         , oC_ErrorCode_TimeNotCorrect      ) )
    {
        errorCode = Context->Interface->SendCommand( Context, Command, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Reads data by using the mode
 *
 * @param Context              SDMMC Context structure
 * @param CardInfo             Card Information read during initialization
 * @param StartSector          Index of the first sector of the transmission
 * @param outBuffer            Buffer for data
 * @param Size                 Size of the buffer on input, on output number of read bytes
 * @param Timeout              Maximum time for operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Interface or `CardInfo` is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outBuffer address is not in RAM
 *  #oC_ErrorCode_SizeNotCorrect         | Size of outBuffer is not correct
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->ReadData
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_ReadData( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, char * outBuffer, oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsContextCorrect(Context)                     , oC_ErrorCode_ContextNotCorrect     )
     && ErrorCondition( isaddresscorrect(Context->Interface)          , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isaddresscorrect(Context->Interface->ReadData), oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isaddresscorrect(CardInfo)                    , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isram(outBuffer)                              , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( isram(Size)                                   , oC_ErrorCode_AddressNotInRam       )
     && ErrorCondition( (*Size) > 0                                   , oC_ErrorCode_SizeNotCorrect        )
     && ErrorCondition( Timeout >= 0                                  , oC_ErrorCode_TimeNotCorrect        ) )
    {
        errorCode = Context->Interface->ReadData( Context, CardInfo, StartSector, outBuffer, Size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Writes data by the mode
 *
 * @param Context              SDMMC Context structure
 * @param CardInfo             Card Information read during initialization
 * @param StartSector          Index of the first sector of the transmission
 * @param Buffer               Buffer for data
 * @param Size                 Size of the buffer on input, on output number of read bytes
 * @param Timeout              Maximum time for operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Interface or Size is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outBuffer address is not in RAM
 *  #oC_ErrorCode_SizeNotCorrect         | Size of Buffer is not correct
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->WriteData
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_WriteData( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, const char * Buffer, oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsContextCorrect(Context)                      , oC_ErrorCode_ContextNotCorrect     )
     && ErrorCondition( isaddresscorrect(Context->Interface)           , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isaddresscorrect(Context->Interface->WriteData), oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isram(Buffer)                                  , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( isram(Size)                                    , oC_ErrorCode_AddressNotInRam       )
     && ErrorCondition( (*Size) > 0                                    , oC_ErrorCode_SizeNotCorrect        )
     && ErrorCondition( Timeout >= 0                                   , oC_ErrorCode_TimeNotCorrect        ) )
    {
        errorCode = Context->Interface->WriteData( Context, CardInfo, StartSector, Buffer, Size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Initializes card to work with the given mode (called when the card has been detected)
 *
 * @param Context              SDMMC Context structure
 * @param outCardInfo          Destination for the read information about the card
 * @param Timeout              Maximum time for operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_WrongAddress           | Address of the Interface or Size is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->InitializeCard
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_InitializeCard( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect     )
     && ErrorCondition( isaddresscorrect(Context->Interface)                , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isaddresscorrect(Context->Interface->InitializeCard), oC_ErrorCode_WrongAddress          )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect        ) )
    {
        errorCode = Context->Interface->InitializeCard( Context, outCardInfo, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets transfer mode
 *
 * The function configures transfer mode
 *
 * @param Context               SDMMC Context structure
 * @param TransferMode          Transfer mode to configure
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_InterfaceNotCorrect    | Address of the Interface is not correct
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TransferModeNotCorrect | `TransferMode` is not valid
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->InitializeCard
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Mode_SetTransferMode( oC_SDMMC_Context_t Context, oC_SDMMC_TransferMode_t TransferMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( IsContextCorrect(Context)                               , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isaddresscorrect(Context->Interface)                    , oC_ErrorCode_InterfaceNotCorrect      )
     && ErrorCondition( isaddresscorrect(Context->Interface->SetTransferMode)   , oC_ErrorCode_NotSupportedByInterface  )
     && ErrorCondition( TransferMode < oC_SDMMC_TransferMode_NumberOfElements   , oC_ErrorCode_TransferModeNotCorrect   )
        )
    {
        errorCode = Context->Interface->SetTransferMode(Context, TransferMode);
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the SDMMC driver is correct.
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_SDMMC_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_SDMMCContext,Context->ObjectControl);
}

#undef  _________________________________________LOCAL_SECTION______________________________________________________________________________

