/** ****************************************************************************************************************************************
 *
 * @brief      Definitions for SDIO commands
 * 
 * @file       oc_sdmmc_cmd.c
 *
 * @author     Patryk Kubiak - (Created on: 05.07.2022 10:24:00)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define oC_SDMMC_PRIVATE

#include <string.h>
#include <ctype.h>
#include <math.h>

#include <oc_sdmmc_cmd.h>
#include <oc_sdmmc_private.h>

#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    oC_SDMMC_DRIVER_FILE_NAME
#define DRIVER_PRIVATE
#define DRIVER_DEBUGLOG     oC_SDMMC_DEBUGLOG_ENABLED
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores name of the command
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_Cmd_Command_t Command;         //!< Index of the command
    const char* Name;                       //!< Human-friendly name
} CommandName_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________
//! @addtogroup SDMMC-Cmd
//! @{

//==========================================================================================================================================
/**
 * @brief array with human-friendly names for the commands
 */
//==========================================================================================================================================
static const CommandName_t CommandNames[] = {
                                       { .Command = oC_SDMMC_Cmd_Command_GO_IDLE_STATE          , "GO_IDLE_STATE" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_OP_COND           , "SEND_OP_COND" },
                                       { .Command = oC_SDMMC_Cmd_Command_ALL_SEND_CID           , "ALL_SEND_CID" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_RELATIVE_ADDR     , "SEND_RELATIVE_ADDR" },
                                       { .Command = oC_SDMMC_Cmd_Command_SET_DSR                , "SET_DSR" },
                                       { .Command = oC_SDMMC_Cmd_Command_IO_SEND_OP_COND        , "IO_SEND_OP_COND" },
                                       { .Command = oC_SDMMC_Cmd_Command_SELECT_CARD            , "SELECT_CARD" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_IF_COND           , "SEND_IF_COND" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_CSD               , "SEND_CSD" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_CID               , "SEND_CID" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_STOP_TRANSMISSION , "SEND_STOP_TRANSMISSION" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_STATUS            , "SEND_STATUS" },
                                       { .Command = oC_SDMMC_Cmd_Command_GO_INACTIVE_STATE      , "GO_INACTIVE_STATE" },
                                       { .Command = oC_SDMMC_Cmd_Command_SET_BLOCKLEN           , "SET_BLOCKLEN" },
                                       { .Command = oC_SDMMC_Cmd_Command_READ_SINGLE_BLOCK      , "READ_SINGLE_BLOCK" },
                                       { .Command = oC_SDMMC_Cmd_Command_READ_MULTIPLE_BLOCK    , "READ_MULTIPLE_BLOCK" },
                                       { .Command = oC_SDMMC_Cmd_Command_WRITE_BLOCK            , "WRITE_BLOCK" },
                                       { .Command = oC_SDMMC_Cmd_Command_WRITE_MULTIPLE_BLOCK   , "WRITE_MULTIPLE_BLOCK" },
                                       { .Command = oC_SDMMC_Cmd_Command_PROGRAM_CSD            , "PROGRAM_CSD" },
                                       { .Command = oC_SDMMC_Cmd_Command_SET_WRITE_PROT         , "SET_WRITE_PROT" },
                                       { .Command = oC_SDMMC_Cmd_Command_CLR_WRITE_PROT         , "CLR_WRITE_PROT" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_WRITE_PROT        , "SEND_WRITE_PROT" },
                                       { .Command = oC_SDMMC_Cmd_Command_ERASE_WR_BLOCK_START   , "ERASE_WR_BLOCK_START" },
                                       { .Command = oC_SDMMC_Cmd_Command_ERASE_WR_BLOCK_END     , "ERASE_WR_BLOCK_END" },
                                       { .Command = oC_SDMMC_Cmd_Command_ERASE                  , "ERASE" },
                                       { .Command = oC_SDMMC_Cmd_Command_LOCK_UNLOCK            , "LOCK_UNLOCK" },
                                       { .Command = oC_SDMMC_Cmd_Command_IO_RW_DIRECT           , "IO_RW_DIRECT" },
                                       { .Command = oC_SDMMC_Cmd_Command_IO_RW_EXTENDED         , "IO_RW_EXTENDED" },
                                       { .Command = oC_SDMMC_Cmd_Command_APP_CMD                , "APP_CMD" },
                                       { .Command = oC_SDMMC_Cmd_Command_GEN_CMD                , "GEN_CMD" },
                                       { .Command = oC_SDMMC_Cmd_Command_READ_OCR               , "READ_OCR" },
                                       { .Command = oC_SDMMC_Cmd_Command_CRC_ON_OFF             , "CRC_ON_OFF" },
                                       { .Command = oC_SDMMC_Cmd_Command_SET_BUS_WIDTH          , "SET_BUS_WIDTH" },
                                       { .Command = oC_SDMMC_Cmd_Command_SD_STATUS              , "SD_STATUS" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_NUM_WR_BLOCKS     , "SEND_NUM_WR_BLOCKS" },
                                       { .Command = oC_SDMMC_Cmd_Command_SET_WR_BLK_ERASE_COUNT , "SET_WR_BLK_ERASE_COUNT" },
                                       { .Command = oC_SDMMC_Cmd_Command_SD_SEND_OP_COND        , "SD_SEND_OP_COND" },
                                       { .Command = oC_SDMMC_Cmd_Command_SET_CLR_CARD_DETECT    , "SET_CLR_CARD_DETECT" },
                                       { .Command = oC_SDMMC_Cmd_Command_SEND_SCR               , "SEND_SCR" },
};
static const char* ClassNames[oC_SDMMC_Cmd_CommandClass__NumberOfElements] = {
                                   "Basic"          ,
                                   "StreamRead"     ,
                                   "BlockRead"      ,
                                   "StreamWrite"    ,
                                   "BlockWrite"     ,
                                   "Erase"          ,
                                   "WriteProtection",
                                   "LockCard"
};

#undef  _________________________________________VARIABLES__________________________________________________________________________________
//!< @}

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static oC_ErrorCode_t SendAppCommand( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeCardAddress, oC_Time_t Timeout );
static oC_ErrorCode_t ParseCsd      ( const oC_SDMMC_Response_t* Response, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData );
static oC_ErrorCode_t ParseCsdVer1  ( const oC_SDMMC_Response_t* Response, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData );
static oC_ErrorCode_t ParseCsdVer2  ( const oC_SDMMC_Response_t* Response, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with FUNCTIONS
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief reads command index according to the name
 *
 * The function is for reading of command index according to the command name string
 *
 * @param CommandName           string with command name
 * @param outCommandIndex       destination for the command index
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | Address of the `CommandName` parameter is not correct
 *  #oC_ErrorCode_StringIsEmpty          | `CommandName` string is empty
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outCommandIndex` parameter is not in RAM section
 *  #oC_ErrorCode_CommandNotHandled      | The given CommandName could not be recognized
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->InitializeCard
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_ReadCommandIndex( const char* CommandName, oC_SDMMC_Cmd_Command_t * outCommandIndex )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( isaddresscorrect(CommandName)   , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outCommandIndex)          , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( strlen(CommandName) > 0         , oC_ErrorCode_StringIsEmpty            )
        )
    {
        errorCode = oC_ErrorCode_CommandNotHandled;

        oC_ARRAY_FOREACH_IN_ARRAY(CommandNames, commandName)
        {
            if ( strcmp(CommandName,commandName->Name) == 0 )
            {
                *outCommandIndex = commandName->Command;
                errorCode = oC_ErrorCode_None;
                break;
            }
        }
        if ( errorCode == oC_ErrorCode_CommandNotHandled )
        {
            bool appCommand = false;
            uint8_t charIndex = 0;
            if ( oC_tolower(CommandName[0]) == 'c'
              && oC_tolower(CommandName[1]) == 'm'
              && oC_tolower(CommandName[2]) == 'd' )
            {
                charIndex = 3;
            }
            else if (oC_tolower(CommandName[0]) == 'a'
                  && oC_tolower(CommandName[1]) == 'c'
                  && oC_tolower(CommandName[2]) == 'm'
                  && oC_tolower(CommandName[3]) == 'd'
                                  )
            {
                charIndex = 4;
                appCommand = true;
            }
            errorCode = oC_ErrorCode_CommandNotCorrect;
            oC_SDMMC_Cmd_CommandIndex_t commandIndex = 0;
            if ( CommandName[charIndex] >= '0' && CommandName[charIndex] <= '9' && CommandName[charIndex + 1] == 0 )
            {
                commandIndex = CommandName[charIndex] - '0';
            }
            else if ( CommandName[charIndex] >= '0' && CommandName[charIndex] <= '9'
                   && CommandName[charIndex + 1] >= '0' && CommandName[charIndex + 1] <= '9'
                   && CommandName[charIndex + 2] == 0)
            {
                commandIndex = (CommandName[charIndex] - '0') * 10 + (CommandName[charIndex + 1] - '0');
            }
            bool found = false;
            oC_ARRAY_FOREACH_IN_ARRAY(CommandNames, commandName)
            {
                if ( oC_SDMMC_Cmd_CommandToIndex( commandName->Command ) == commandIndex && oC_SDMMC_Cmd_IsApplicationCommand(commandName->Command) == appCommand )
                {
                    *outCommandIndex = commandName->Command;
                    errorCode = oC_ErrorCode_None;
                    found = true;
                    break;
                }
            }
            if ( !found && oC_SDMMC_Cmd_IsCommandIndexValid(commandIndex))
            {
                *outCommandIndex = oC_SDMMC_Cmd_Command_Make(appCommand ? 1:0, commandIndex, Unknown, Unknown, None);
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads command name according to the command index
 *
 * The function reads a name of the command
 *
 * @param CommandIndex          Index of the command to read a name
 * @param BufferLength          Length of the `outCommandName` buffer
 * @param outCommandName        Destination for the read command name
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_CommandNotCorrect      | The given `Command` is not correct
 *  #oC_ErrorCode_CommandNotHandled      | The given `Command` is not supported
 *  #oC_ErrorCode_StringLengthNotCorrect | The given `BufferLength` is too small
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outCommandName` parameter is not in RAM section
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->InitializeCard
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_ReadCommandName( oC_SDMMC_Cmd_Command_t Command, size_t BufferLength, char outCommandName[BufferLength] )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( oC_SDMMC_Cmd_IsCommandValid(Command)            , oC_ErrorCode_CommandNotCorrect        )
     && ErrorCondition( BufferLength >= 5                               , oC_ErrorCode_StringLengthNotCorrect   )
     && ErrorCondition( isram(outCommandName)                           , oC_ErrorCode_OutputAddressNotInRAM    )
            )
    {
        errorCode = oC_ErrorCode_CommandNotHandled;
        oC_ARRAY_FOREACH_IN_ARRAY(CommandNames, commandName)
        {
            if ( Command == commandName->Command )
            {
                strncpy( outCommandName, commandName->Name, BufferLength );
                errorCode = oC_ErrorCode_None;
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads command class from a string
 *
 * The function is designed for reading of a command class from a given string.
 *
 * @note
 * Please check #oC_SDMMC_Cmd_CommandClass_t enumeration to know the list of supported classes
 *
 * @param ClassName         string with name of the class
 * @param outClass          destination variable for the read class
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | Address of the `ClassName` parameter is not correct
 *  #oC_ErrorCode_StringIsEmpty          | `CommandName` string is empty
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outClass` parameter is not in RAM section
 *  #oC_ErrorCode_CommandNotCorrect      | The given ClassName could not be recognized
 *  #oC_ErrorCode_CommandNotHandled      | The given ClassName is not supported (custom class names can use only 2 digits as max)
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_ReadClassIndex( const char* ClassName, oC_SDMMC_Cmd_CommandClass_t* outClass )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
            ErrorCondition( isaddresscorrect(ClassName)     , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( strlen(ClassName)               , oC_ErrorCode_StringIsEmpty            )
         && ErrorCondition( isram(outClass)                 , oC_ErrorCode_OutputAddressNotInRAM    )
            )
    {
        bool found = false;
        for ( oC_SDMMC_Cmd_CommandClass_t class = oC_SDMMC_Cmd_CommandClass_Class0; class < oC_SDMMC_Cmd_CommandClass__NumberOfElements; class++ )
        {
            if ( strcmp( ClassNames[0], ClassName ) == 0 )
            {
                found = true;
                *outClass = class;
                errorCode = oC_ErrorCode_None;
                break;
            }
        }
        if ( !found )
        {
            uint32_t index = 0;
            if (
                oC_tolower(ClassName[0]) == 'c'
             && oC_tolower(ClassName[1]) == 'l'
             && oC_tolower(ClassName[2]) == 'a'
             && oC_tolower(ClassName[3]) == 's'
             && oC_tolower(ClassName[4]) == 's'
             && (ClassName[5] >= '0' && ClassName[5] <= '9')
             && ((ClassName[6] >= '0' && ClassName[6] <= '9') || ClassName[6] == 0)
                )
            {
                if ( ClassName[6] == 0 )
                {
                    index = ClassName[5] - '0';
                    *outClass = index;
                    errorCode = oC_ErrorCode_None;
                }
                else if ( ClassName[7] == 0 )
                {
                    index = ((ClassName[5] - '0') * 10)
                           + (ClassName[6] - '0');
                    *outClass = index;
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    errorCode = oC_ErrorCode_CommandNotHandled;
                }
            }
            else
            {
                errorCode = oC_ErrorCode_CommandNotCorrect;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads class name from the given class index
 *
 * The function reads a class name for the given class
 *
 * @param Class             class to get a string for
 * @param BufferLength      length of the `outClassName` buffer
 * @param outClassName      destination buffer for the string
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ClassNotCorrect        | The given `Class` is not correct
 *  #oC_ErrorCode_ClassNotSupported      | The given `Class` is not supported (name for the class is not available)
 *  #oC_ErrorCode_StringLengthNotCorrect | The given `BufferLength` is 0
 *  #oC_ErrorCode_OutputBufferTooSmall   | The given `outClassName` buffer is too small to store the full name
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outCommandName` parameter is not in RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDDMC_Cmd_ReadClassName( oC_SDMMC_Cmd_CommandClass_t Class, size_t BufferLength, char outClassName[BufferLength] )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( oC_SDMMC_Cmd_IsCommandClassValid(Class) , oC_ErrorCode_ClassNotCorrect          )
     && ErrorCondition( BufferLength > 0                        , oC_ErrorCode_StringLengthNotCorrect   )
     && ErrorCondition( isram(outClassName)                     , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        size_t nameLength = 0;
        if ( Class < oC_ARRAY_SIZE(ClassNames) && (nameLength = strlen(ClassNames[Class])) > 0 )
        {
            strncpy( outClassName, ClassNames[Class], BufferLength );
            errorCode = nameLength < BufferLength ?
                            oC_ErrorCode_None : oC_ErrorCode_OutputBufferTooSmall;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends command via the given interface and reads the response
 *
 * The function is responsible for sending of a command and getting of the response
 *
 * @param Context               driver context
 * @param RelativeCardAddress   Relative Card Address (RCA)
 * @param CommandData           command data to send and store the response
 * @param Timeout               maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
 *  #oC_ErrorCode_OutputAddressNotInRAM  | The given `Command` is not in RAM section
 *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
 *  #oC_ErrorCode_CommandNotCorrect      | The given `CommandData` does not store valid Command
 *  #oC_ErrorCode_InterfaceNotCorrect    | Interface in the context is not valid - probably context is damaged or not initialized
 *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
 *  #oC_ErrorCode_ResponseNotReceived    | The command was properly sent, but the card did not respond for the command
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SendCommand( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeCardAddress, oC_SDMMC_Cmd_CommandData_t* CommandData, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( oC_SDMMC_IsContextCorrect(Context)                  , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isram(CommandData)                                  , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_SDMMC_Cmd_IsCommandValid( CommandData->Command ) , oC_ErrorCode_CommandNotCorrect        )
        )
    {
#if oC_SDMMC_DEBUGLOG_ENABLED
        char commandName[30] = {0};
        oC_SDMMC_Cmd_ReadCommandName(CommandData->Command, sizeof(commandName), commandName);
#endif
        uint8_t numberOfRetries = Context->NumberOfRetries > 0 ? Context->NumberOfRetries : 1;
        oC_Time_t tryTimeout = Timeout / numberOfRetries;
        for ( uint8_t tryIndex = 0; tryIndex < numberOfRetries; tryIndex++ )
        {
            oC_Timestamp_t endTimestamp = timeouttotimestamp(tryTimeout);
            if ( !oC_SDMMC_Cmd_IsApplicationCommand( CommandData->Command ) || ErrorCode( SendAppCommand( Context, RelativeCardAddress, gettimeout(endTimestamp) ) ) )
            {
                oC_SDMMC_Mode_RawCommand_t rawCommand = {
                      .CommandIndex = oC_SDMMC_Cmd_CommandToIndex( CommandData->Command ),
                      .CommandArgument = CommandData->Argument.Generic,
                      .Response.Type = CommandData->Response.Type
                };
                if ( ErrorCode( oC_SDMMC_Mode_SendCommand( Context, &rawCommand, gettimeout(endTimestamp) ) ) )
                {
                    memcpy(CommandData->Response.LongResponse, rawCommand.Response.LongResponse, sizeof(CommandData->Response.LongResponse));
                    if ( ErrorCode( oC_SDMMC_Responses_Handle(Context, oC_SDMMC_Cmd_CommandToResponseIndex(CommandData->Command), &rawCommand.Response) ) )
                    {
                        driverlog(oC_LogType_Track, "Command %s(With argument: 0x%02X) has been sent. Response: 0x%02X\n", commandName, CommandData->Argument.Generic, rawCommand.Response);
                        errorCode = oC_ErrorCode_None;
                    }
                    else
                    {
                        driverlog(oC_LogType_Error, "Command %s(With argument: 0x%02X) has failed. Response: 0x%02X\n", commandName, CommandData->Argument.Generic, rawCommand.Response);
                    }
                    break;
                }
            }
            if ( errorCode != oC_ErrorCode_Timeout && errorCode != oC_ErrorCode_ResponseNotReceived )
            {
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Resets all cards to idle state
 *
 * The function sends a command to resets all the cards to idle state.
 *
 * @param Context       Context of the driver
 * @param Timeout       Maximum time for the operation
 *
 * @return
 * The function can only return error codes that are returned by the function #oC_SDMMC_Cmd_SendCommand - please check its description
 * to find a list of possible commands
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_GoIdleState( oC_SDMMC_Context_t Context, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

    commandData.Command = oC_SDMMC_Cmd_Command_GO_IDLE_STATE;
    commandData.Response.Type = oC_SDMMC_Responses_ResponseType_None;

    errorCode = oC_SDMMC_Cmd_SendCommand(Context, 0, &commandData, Timeout);
    driverlog( oC_LogType_Info, "Reseting all the cards to idle state: %R", errorCode );
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the SDCv2 interface is supported
 *
 * Sends SD Memory Card interface condition, which includes host supply voltage information and asks the card whether card supports voltage.
 * Reserved bits shall be set to '0'.
 *
 * @param Context       Context of the driver
 * @param Timeout       Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ResponseNotCorrect     | The card has responded for the command, but the response was invalid
 *
 * @note
 * More error codes can be returned by the function #oC_SDMMC_Cmd_SendCommand
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SendIfCondition( oC_SDMMC_Context_t Context, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

    commandData.Command = oC_SDMMC_Cmd_Command_SEND_IF_COND;
    commandData.Argument.SendIfCond.VoltageAccepted = oC_SDMMC_Arguments_VoltageAccepted_2p7to3p6;
    commandData.Argument.SendIfCond.Pattern = 0xAA;
    commandData.Response.Type = oC_SDMMC_Responses_ResponseType_Short;

    if (
        ErrorCode     ( oC_SDMMC_Cmd_SendCommand(Context, 0, &commandData, Timeout) )
     && ErrorCondition( commandData.Argument.Generic == commandData.Response.ShortResponse, oC_ErrorCode_ResponseNotCorrect )
        )
    {
        driverlog( oC_LogType_GoodNews, "SDCv2 confirmed" );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends SD_SEND_OP_COND command
 *
 * The function sends initialization command
 *
 * @param Context        Context of the driver
 * @param Request        Request parameters
 * @param outResult      Destination for the command result
 * @param Timeout        Maximum time for the command
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | Address of the `Request` parameter is invalid
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outResult` does not point to the RAM section
 *
 * @note
 * More error codes can be returned by the functions: #oC_SDMMC_Cmd_SendCommand, #oC_SDMMC_Responses_PrepareVoltageWindow
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SdSendOpCond( oC_SDMMC_Context_t Context, const oC_SDMMC_Cmd_Request_SdSendOpCond_t* Request, oC_SDMMC_Cmd_Result_SdSendOpCond_t* outResult, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_SDMMC_Responses_VoltageWindow_t voltageWindow = 0;

    if (
        ErrorCondition( isaddresscorrect(Request)   , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outResult)            , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCode( oC_SDMMC_Responses_PrepareVoltageWindow(Request->VoltageRange.Min, Request->VoltageRange.Max, &voltageWindow) )
        )
    {
        oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

        commandData.Command = oC_SDMMC_Cmd_Command_SD_SEND_OP_COND;
        commandData.Response.Type = oC_SDMMC_Responses_ResponseType_Short;
        commandData.Argument.SdSendOpCond.VoltageWindow = voltageWindow;
        commandData.Argument.SdSendOpCond.HostCapacitySupport = Request->HighCapacitySupported ? 1 : 0;
        commandData.Argument.SdSendOpCond.SDXCPowerControl = Request->MaximumPerformanceEnabled ? 1 : 0;

        if ( ErrorCode( oC_SDMMC_Cmd_SendCommand(Context, 0, &commandData, Timeout) ) )
        {
            outResult->VoltageSupported              = oC_Bits_AreBitsSetU8(commandData.Response.R3.VoltageWindow, voltageWindow);
            outResult->SwitchingToLowVoltageAccepted = commandData.Response.R3.SwitchingTo18Accepted;
            outResult->InitializationCompleted       = commandData.Response.R3.InitializationComplete == 1;
            outResult->HighCapacityCard              = commandData.Response.R3.CardCapacityStatus == 1;

            driverlog( oC_LogType_Track, "SD_SEND_OP_COND response: { VoltageSupported=%b, SwitchingToLowVoltage=%b, InitializationCompleted=%b, HighCapacityCard=%b }",
                       outResult->VoltageSupported, outResult->SwitchingToLowVoltageAccepted, outResult->InitializationCompleted, outResult->HighCapacityCard );

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Asks any card to send the CID numbers on the CMD line
 *
 * The function sends a ALL_SEND_CID command, which asks any card to send the CID numbers on the CMD line (any card that is connected to the
 * host will respond)
 *
 * @param Context               Context of the driver
 * @param outCardId             Destination for the read Card ID
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outCardId` does not point to the RAM section
 *
 * @note
 * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
 *
 * @see oC_SDMMC_Cmd_SendCommand
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_AllSendCardId( oC_SDMMC_Context_t Context, oC_SDMMC_CardId_t* outCardId, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Struct_Define(oC_SDMMC_Cmd_CommandData_t,commandData);

    commandData.Command         = oC_SDMMC_Cmd_Command_ALL_SEND_CID;
    commandData.Response.Type   = oC_SDMMC_Responses_ResponseType_Long;

    if (
        ErrorCondition( isram(outCardId), oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCode( oC_SDMMC_Cmd_SendCommand(Context, 0, &commandData, Timeout) )
     && ErrorCode( oC_SDMMC_Responses_SwapBytes(&commandData.Response)                              )
        )
    {
        bzero(outCardId, sizeof(*outCardId));
        outCardId->ManufacturerId = commandData.Response.CardId.ManufacturerId;
        outCardId->ManufacturingDate.Month  = commandData.Response.CardId.ManufacturingDate & 0xF;
        outCardId->ManufacturingDate.Year   = (commandData.Response.CardId.ManufacturingDate & 0xFF0) >> 4;
        memcpy(outCardId->OemApplicationId, commandData.Response.CardId.OEMApplicationId, sizeof(commandData.Response.CardId.OEMApplicationId));
        memcpy(outCardId->ProductName, commandData.Response.CardId.ProductName, sizeof(commandData.Response.CardId.ProductName));
        outCardId->ProductRevision.Major = commandData.Response.CardId.ProductRevisionMajor;
        outCardId->ProductRevision.Minor = commandData.Response.CardId.ProductRevisionMinor;
        outCardId->SerialNumber = commandData.Response.CardId.ProductSerialNumber;
        driverlog( oC_LogType_GoodNews, "Card ID: { MID: 0x%02X, MDate: %04d-%02d, OEM-ID: %s, Prod-Name: %s, Rev: %d.%d, Serial-Number: 0x%08X}",
                   outCardId->ManufacturerId,
                   outCardId->ManufacturingDate.Year + 2000,
                   outCardId->ManufacturingDate.Month,
                   outCardId->OemApplicationId,
                   outCardId->ProductName,
                   outCardId->ProductRevision.Major,
                   outCardId->ProductRevision.Minor,
                   outCardId->SerialNumber
                   );
        errorCode = oC_ErrorCode_None;
    }
    
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Ask the card to publish a new relative address (RCA)
 *
 * The function sends the command `SEND_RELATIVE_ADDR` which asks the card to publish a new relative address (RCA)
 *
 * @param Context                   Context of the driver
 * @param outRelativeAddress        Destination for the read relative address
 * @param Timeout                   Maximum time for the command
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outRelativeAddress` does not point to the RAM section
 *
 * @note
 * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
 *
 * @see oC_SDMMC_Cmd_SendCommand
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SendRelativeAddress( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t* outRelativeAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define(oC_SDMMC_Cmd_CommandData_t,commandData);

    commandData.Command         = oC_SDMMC_Cmd_Command_SEND_RELATIVE_ADDR;
    commandData.Response.Type   = oC_SDMMC_Responses_ResponseType_Short;

    if (
        ErrorCondition( isram(outRelativeAddress), oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCode( oC_SDMMC_Cmd_SendCommand(Context, 0, &commandData, Timeout) )
        )
    {
        *outRelativeAddress = commandData.Response.R6.RelativeCardAddress;
        driverlog( oC_LogType_GoodNews, "Relative Card Address (RCA) has been received: 0x%04X", commandData.Response.R6.RelativeCardAddress );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief selects a card with the given RCA
 *
 * Command toggles a card between the stand-by and transfer states or between the programming and disconnect states. In both cases, the card
 * is selected by its own relative address and gets deselected by any other address; address 0 deselects all. In the case that the RCA equals 0,
 * the host may do one of the following:
 *  - Use other RCA number to perform card de-selection
 *  - Re-send CMD3 to change its number to other than 0 and use CMD7 with RCA=0 for card de-selection
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card to select
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * All the returned error codes comes from #oC_SDMMC_Cmd_SendCommand function
 *
 * @see oC_SDMMC_Cmd_SendCommand
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SelectCard( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define(oC_SDMMC_Cmd_CommandData_t,commandData);

    commandData.Command                                 = oC_SDMMC_Cmd_Command_SELECT_CARD;
    commandData.Argument.SelectCard.CardRelativeAddress = RelativeAddress;
    commandData.Response.Type                           = oC_SDMMC_Responses_ResponseType_Short;

    if ( ErrorCode( oC_SDMMC_Cmd_SendCommand(Context, RelativeAddress, &commandData, Timeout) ) )
    {
        driverlog( oC_LogType_GoodNews, "Card 0x%04x has been selected", RelativeAddress );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets bus width
 *
 * The function sends the command SEND_BUS_WIDTH to change bus width
 *
 * @param Context               context of the driver
 * @param RelativeAddress       relative address of a card to access
 * @param TransferMode          bus width to set
 * @param Timeout               maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress              | `RelativeAddress` is not valid
 *  #oC_ErrorCode_TransferModeNotSupported  | The given `TransferMode` is not supported
 *
 * @note
 * More error codes can be returned by the function: #oC_SDMMC_Cmd_SendCommand
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SetBusWidth( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_SDMMC_TransferMode_t TransferMode, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

    commandData.Command         = oC_SDMMC_Cmd_Command_SET_BUS_WIDTH;
    commandData.Response.Type   = oC_SDMMC_Responses_ResponseType_Short;
    commandData.Argument.SetBusWidth.BusWidth = TransferMode == oC_SDMMC_TransferMode_4Bit ?
                    oC_SDMMC_Arguments_SetBusWidth_4Bit :
                    oC_SDMMC_Arguments_SetBusWidth_1Bit;

    if (
        ErrorCondition( RelativeAddress != 0                        , oC_ErrorCode_WrongAddress )
     && ErrorCondition( TransferMode == oC_SDMMC_TransferMode_1Bit
                     || TransferMode == oC_SDMMC_TransferMode_4Bit  , oC_ErrorCode_TransferModeNotSupported )
     && ErrorCode( oC_SDMMC_Cmd_SendCommand(Context, RelativeAddress, &commandData, Timeout) )
        )
    {
        driverlog( oC_LogType_GoodNews, "Bus width has been changed to %s", TransferMode == oC_SDMMC_TransferMode_4Bit ? "4Bit" : "1Bit" );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends CSD request
 *
 * The function sends request for a CSD register.
 *
 * @param Context                   Context of the driver
 * @param RelativeAddress           Relative Address of the device
 * @param outCardSpecificData       Destination for the CSD read data
 * @param Timeout                   Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress                      | The given `RelativeAddress` is not valid
 *  #oC_ErrorCode_OutputAddressNotInRAM             | The given `outCardSpecificData` does not point to the RAM section
 *  #oC_ErrorCode_TimeNotCorrect                    | The given `Timeout` stores invalid time
 *
 * @note More error codes can be returned by function #oC_SDMMC_Cmd_SendCommand and #ParseCsd
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SendCardSpecificData( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

    commandData.Command                                 = oC_SDMMC_Cmd_Command_SEND_CSD;
    commandData.Response.Type                           = oC_SDMMC_Responses_ResponseType_Long;
    commandData.Argument.SendCsd.CardRelativeAddress    = RelativeAddress;

    if (
        ErrorCondition( RelativeAddress != 0                , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outCardSpecificData)          , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCode( oC_SDMMC_Cmd_SendCommand(Context, RelativeAddress, &commandData, Timeout)        )
     && ErrorCode( ParseCsd(&commandData.Response, outCardSpecificData)                             )
        )
    {
        driverlog( oC_LogType_GoodNews, "Card Specific Data (CSD) has been received. Card Size: %.2M (%Lu) Transfer Speed: %.0T (%Lu)", outCardSpecificData->CardSize, outCardSpecificData->CardSize, outCardSpecificData->TransferSpeed, outCardSpecificData->TransferSpeed );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sends a command to set a block length
 *
 * The function sends a request to set the block length.
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative Address of the device
 * @param BlockSize             Size of the block to set
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress                      | The given `RelativeAddress` is not valid
 *  #oC_ErrorCode_TimeNotCorrect                    | The given `Timeout` stores invalid time
 *  #oC_ErrorCode_SizeNotCorrect                    | The given `BlockSize` is not valid
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_SetBlockLength( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemorySize_t BlockSize, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

    commandData.Command             = oC_SDMMC_Cmd_Command_SET_BLOCKLEN;
    commandData.Response.Type       = oC_SDMMC_Responses_ResponseType_Short;
    commandData.Argument.Generic    = (uint32_t)BlockSize;

    if (
        ErrorCondition( RelativeAddress != 0                    , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( BlockSize > 0 && BlockSize < UINT32_MAX , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                            , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCode     ( oC_SDMMC_Cmd_SendCommand(Context, RelativeAddress, &commandData, Timeout)   )
        )
    {
        driverlog( oC_LogType_GoodNews, "Block Length has been set to %.0M", BlockSize);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends a command to read a multiple block
 *
 * The function is for sending a command to read of a multiple blocks of data from the card until #oC_SDMMC_Cmd_StopTransmission command is send
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative Address of the device
 * @param StartAddress          Starting address for read
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress                      | The given `RelativeAddress` is not valid
 *  #oC_ErrorCode_TimeNotCorrect                    | The given `Timeout` stores invalid time
 *  #oC_ErrorCode_AddressNotCorrect                 | The given `StartAddress` points to a too big value
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_ReadMultipleBlock( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

    commandData.Command             = oC_SDMMC_Cmd_Command_READ_MULTIPLE_BLOCK;
    commandData.Response.Type       = oC_SDMMC_Responses_ResponseType_Short;
    commandData.Argument.Generic    = (oC_SDMMC_Cmd_Argument_t)StartAddress;

    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if (
        ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( RelativeAddress != 0                , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( StartAddress < UINT32_MAX           , oC_ErrorCode_AddressNotCorrect        )
     && ErrorCode     ( oC_SDMMC_Cmd_SendCommand(Context, RelativeAddress, &commandData      , gettimeout(endTimestamp)) )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends a command to write a multiple block
 *
 * The function is for sending a command to write of a multiple blocks of data from the card until #oC_SDMMC_Cmd_StopTransmission command is send
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative Address of the device
 * @param StartAddress          Starting address for read
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress                      | The given `RelativeAddress` is not valid
 *  #oC_ErrorCode_TimeNotCorrect                    | The given `Timeout` stores invalid time
 *  #oC_ErrorCode_AddressNotCorrect                 | The given `StartAddress` points to a too big value
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_WriteMultipleBlock( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );

    commandData.Command             = oC_SDMMC_Cmd_Command_WRITE_MULTIPLE_BLOCK;
    commandData.Response.Type       = oC_SDMMC_Responses_ResponseType_Short;
    commandData.Argument.Generic    = (oC_SDMMC_Cmd_Argument_t)StartAddress;

    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if (
        ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( RelativeAddress != 0                , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( StartAddress < UINT32_MAX           , oC_ErrorCode_AddressNotCorrect        )
     && ErrorCode     ( oC_SDMMC_Cmd_SendCommand(Context, RelativeAddress, &commandData      , gettimeout(endTimestamp)) )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends a request to stop a transmission
 *
 * The function should be used after #oC_SDMMC_Cmd_ReadMultipleBlock or #oC_SDMMC_Cmd_WriteMultipleBlock to stop sending/reading of blocks
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card to stop the transmission
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress                      | The given `RelativeAddress` is not valid
 *  #oC_ErrorCode_TimeNotCorrect                    | The given `Timeout` stores invalid time
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Cmd_StopTransmission( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_Struct_Define( oC_SDMMC_Cmd_CommandData_t, commandData );
    commandData.Command             = oC_SDMMC_Cmd_Command_SEND_STOP_TRANSMISSION;
    commandData.Response.Type       = oC_SDMMC_Responses_ResponseType_Short;

    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
    if (
        ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( RelativeAddress != 0                , oC_ErrorCode_WrongAddress             )
        )
    {
        for ( uint8_t tryIndex = 0; tryIndex < 3; tryIndex++ )
        {
            if ( ErrorCode( oC_SDMMC_Cmd_SendCommand(Context, RelativeAddress, &commandData      , gettimeout(endTimestamp)) ))
            {
                errorCode = oC_ErrorCode_None;
                break;
            }
        }
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief sends APP command
 *
 * The function sends application command and verifies the response
 *
 * @param Context               context of the library
 * @param RelativeCardAddress   Address of the card to send a commend to
 * @param Timeout               maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_TimeNotCorrect                    | The given `Timeout` is not correct
 *  #oC_ErrorCode_ApplicationCommandNotConfirmed    | APP_CMD flag is not set in the application command response
 *
 *  @note more errors can be returned by the interface function `SendCommand` function from the type #oC_SDMMC_Mode_Interface_t and from function #oC_SDMMC_Responses_VerifyR1
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendAppCommand( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeCardAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_SDMMC_Mode_RawCommand_t rawCommand = {
                                             .CommandIndex = oC_SDMMC_Cmd_CommandToIndex(oC_SDMMC_Cmd_Command_APP_CMD),
                                             .CommandArgument = RelativeCardAddress << 16,
                                             .Response.Type = oC_SDMMC_Responses_ResponseType_Short,
                                             .Response.ShortResponse = 0
    };

    if (
         ErrorCondition( Timeout >= 0,                      oC_ErrorCode_TimeNotCorrect                 )
      && ErrorCode     ( oC_SDMMC_Mode_SendCommand(Context, &rawCommand, Timeout)                       )
      && ErrorCode     ( oC_SDMMC_Responses_VerifyR1(&rawCommand.Response.R1)                           )
      && ErrorCondition( rawCommand.Response.R1.AppCmd,     oC_ErrorCode_ApplicationCommandNotConfirmed )
         )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t ParseCsd( const oC_SDMMC_Response_t* Response, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;


    switch(Response->CardSpecificData.Version1p0.CsdStructure)
    {
        case 0x0:
            driverlog( oC_LogType_Track, "CSD response is in version 1.0" );
            errorCode = ParseCsdVer1(Response, outCardSpecificData);
            break;
        case 0x1:
            driverlog( oC_LogType_Track, "CSD response is in version 2.0" );
            errorCode = ParseCsdVer2(Response, outCardSpecificData);
            break;
        default:
            errorCode = oC_ErrorCode_VersionNotSupported;
            break;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief parses CSD register in Version 1.0
 *
 * The function parses CSD register in version 1.0
 *
 * @param Response                  Pointer to the response structure
 * @param outCardSpecificData       Destination for the CSD register data
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_VersionNotCorrect                 | Response stores CSD in invalid version
 *
 *  @note More error codes can come from function #oC_SDMMC_Responses_ReadTransferSpeed
 */
//==========================================================================================================================================
static oC_ErrorCode_t ParseCsdVer1( const oC_SDMMC_Response_t* Response, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( Response->CardSpecificData.Version1p0.CsdStructure == 0x00, oC_ErrorCode_VersionNotCorrect )
        )
    {
        oC_MemorySize_t READ_BL_LEN = Response->CardSpecificData.Version1p0.ReadBlockLength;
        oC_MemorySize_t C_SIZE_MULT = Response->CardSpecificData.Version1p0.CardSizeMultiplier;
        oC_MemorySize_t C_SIZE = Response->CardSpecificData.Version1p0.CardSizeLow | (Response->CardSpecificData.Version1p0.CardSizeHigh << 2);
        oC_MemorySize_t BLOCK_LEN = pow(2, READ_BL_LEN);
        oC_MemorySize_t MULT = pow( 2, C_SIZE_MULT+2 );
        oC_MemorySize_t BLOCKNR = (C_SIZE + 1) * MULT;
        oC_MemorySize_t memoryCapacity = BLOCKNR * BLOCK_LEN;

        outCardSpecificData->CardSize = memoryCapacity;
        outCardSpecificData->BlockLength = Response->CardSpecificData.Version1p0.ReadBlockLength;
        outCardSpecificData->DsrImplemented = Response->CardSpecificData.Version1p0.DSRImplemented == 1;
        outCardSpecificData->FileFormat = oC_SDMMC_FileFormat_Unknown;
        outCardSpecificData->PartialBlockReadAllowed = Response->CardSpecificData.Version1p0.PartialBlocksForWriteAllowed == 1;
        outCardSpecificData->PartialBlockWriteAllowed = Response->CardSpecificData.Version1p0.PartialBlocksForWriteAllowed == 1;
        outCardSpecificData->PermanentWriteProtection = Response->CardSpecificData.Version1p0.PermanentWriteProtection == 1;
        outCardSpecificData->ReadBlockMisalign = Response->CardSpecificData.Version1p0.ReadBlockMisalign == 1;
        outCardSpecificData->WriteBlockMisalign = Response->CardSpecificData.Version1p0.WriteBlockMisalign == 1;
        outCardSpecificData->SectorSize = 512;
        outCardSpecificData->SupportedClasses = Response->CardSpecificData.Version1p0.CardCommandClasses;
        outCardSpecificData->TemporaryWriteProtection = Response->CardSpecificData.Version1p0.TemporaryWriteProtection == 1;

        oC_SDMMC_Responses_TransferSpeed_t transferSpeed;
        transferSpeed.Generic = Response->CardSpecificData.Version1p0.TransferSpeed;

        errorCode = oC_SDMMC_Responses_ReadTransferSpeed(&transferSpeed, &outCardSpecificData->TransferSpeed);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief parses CSD register in Version 2.0
 *
 * The function parses CSD register in version 2.0
 *
 * @param Response                  Pointer to the response structure
 * @param outCardSpecificData       Destination for the CSD register data
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                                  | Description
 * -------------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                              | Operation success
 *  #oC_ErrorCode_ImplementError                    | There was unexpected error in implementation
 *  #oC_ErrorCode_VersionNotCorrect                 | Response stores CSD in invalid version
 *  #oC_ErrorCode_ResponseNotCorrect                | Response data is not valid and cannot be parsed
 *
 *  @note More error codes can come from function #oC_SDMMC_Responses_ReadTransferSpeed
 */
//==========================================================================================================================================
static oC_ErrorCode_t ParseCsdVer2( const oC_SDMMC_Response_t* Response, oC_SDMMC_Cmd_Result_CardSpecificData_t* outCardSpecificData )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( Response->CardSpecificData.Version2p0.CsdStructure == 0x01      , oC_ErrorCode_VersionNotCorrect    )
     && ErrorCondition( Response->CardSpecificData.Version2p0.EraseSectorSize == 0x7F   , oC_ErrorCode_ResponseNotCorrect   )
        )
    {
        oC_MemorySize_t C_SIZE = Response->CardSpecificData.Version2p0.CardSizeLow | (Response->CardSpecificData.Version2p0.CardSizeHigh << 16);
        oC_MemorySize_t memoryCapacity = (C_SIZE+1) * kB(512);
        outCardSpecificData->CardSize = memoryCapacity;
        outCardSpecificData->BlockLength = Response->CardSpecificData.Version2p0.ReadBlockLength;
        outCardSpecificData->DsrImplemented = Response->CardSpecificData.Version2p0.DSRImplemented == 1;
        outCardSpecificData->FileFormat = oC_SDMMC_FileFormat_Unknown;
        outCardSpecificData->PartialBlockReadAllowed = true;
        outCardSpecificData->PartialBlockWriteAllowed = true;
        outCardSpecificData->PermanentWriteProtection = Response->CardSpecificData.Version2p0.PermanentWriteProtection;
        outCardSpecificData->ReadBlockMisalign = Response->CardSpecificData.Version2p0.ReadBlockMisalign == 1;
        outCardSpecificData->WriteBlockMisalign = Response->CardSpecificData.Version2p0.WriteBlockMisalign == 1;
        outCardSpecificData->SectorSize = 512;
        outCardSpecificData->SupportedClasses = Response->CardSpecificData.Version2p0.CardCommandClasses;
        outCardSpecificData->TemporaryWriteProtection = Response->CardSpecificData.Version2p0.TemporaryWriteProtection == 1;

        oC_SDMMC_Responses_TransferSpeed_t transferSpeed;
        transferSpeed.Generic = Response->CardSpecificData.Version2p0.TransferSpeed;

        errorCode = oC_SDMMC_Responses_ReadTransferSpeed(&transferSpeed, &outCardSpecificData->TransferSpeed);
    }
    else
    {
        driverlog( oC_LogType_Error, "Parsing CSD Error: %R EraseSectorSize: 0x%02x", errorCode, Response->CardSpecificData.Version2p0.EraseSectorSize);
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_SECTION______________________________________________________________________________
