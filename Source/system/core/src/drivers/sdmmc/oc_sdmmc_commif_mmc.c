/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_commif_mmc.c
 *
 * @brief      Function definitions for MMC communication interface
 *
 * @author     Patryk Kubiak - (Created on: 09 4, 2022 1:57:28 PM)
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#define oC_SDMMC_PRIVATE

#include <oc_sdmmc_commif_mmc.h>
#include <oc_sdmmc_cmd.h>
#include <oc_sdmmc_private.h>
#include <oc_sys_lld.h>

#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    oC_SDMMC_DRIVER_FILE_NAME
#define DRIVER_PRIVATE
#define DRIVER_DEBUGLOG     oC_SDMMC_DEBUGLOG_ENABLED
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static oC_ErrorCode_t ConfirmIdentification             ( oC_SDMMC_Context_t Context, oC_Time_t Timeout );
static oC_ErrorCode_t InitializeCard                    ( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout );
static oC_ErrorCode_t StartReadingSectors               ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );
static oC_ErrorCode_t FinishReadingSectors              ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );
static oC_ErrorCode_t StartWritingSectors               ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );
static oC_ErrorCode_t FinishWritingSectors              ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________


const oC_SDMMC_CommIf_Handlers_t oC_SDMMC_CommIf_Handler_MMC = {
                                                                  .InterfaceId              = oC_SDMMC_CommunicationInterfaceId_MMC,
                                                                  .Name                     = "MMC",
                                                                  .ConfirmIdentification    = ConfirmIdentification,
                                                                  .InitializeCard           = InitializeCard,
                                                                  .SetTransferMode          = oC_SDMMC_Cmd_SetBusWidth,
                                                                  .StartReadingSectors      = StartReadingSectors,
                                                                  .FinishReadingSectors     = FinishReadingSectors,
                                                                  .StartWritingSectors      = StartWritingSectors,
                                                                  .FinishWritingSectors     = FinishWritingSectors
};


#undef  _________________________________________VARIABLES__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief confirms identification of the card interface
 *
 * The function is used to confirm, that the given card supports this communication interface
 *
 * @param Context           Context of the driver
 * @param Timeout           Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | The function is not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_InterfaceNotSupported  | The card does not support this interface
 *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfirmIdentification( oC_SDMMC_Context_t Context, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief initializes the card
 *
 * The function is used to initialize the card and get information about it
 *
 * @param Context           Context of the driver
 * @param outCardInfo       Pointer to the structure, where information about the card will be stored
 * @param Timeout           Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | The function is not implemented
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_InterfaceNotSupported  | The card does not support this interface
 *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
 */
//==========================================================================================================================================
static oC_ErrorCode_t InitializeCard( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief starts reading sectors from the card
 *
 * The function is used to start reading sectors from the card
 *
 * @param Context           Context of the driver
 * @param RelativeAddress   Relative address of the card
 * @param StartAddress      Address of the first sector to read
 * @param Timeout           Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_NotImplemented         | The function is not implemented
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartReadingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes reading of sectors
 *
 * The function finishes reading of sectors started by #StartReadingSectors
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *     Error Code                       | Description
 * -------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                  | Operation success
 *  #oC_ErrorCode_NotImplemented        | The function is not implemented
 *  #oC_ErrorCode_ImplementError        | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect     | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect        | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout               | Maximum time for the command has expired, but the command has not been sent
 *  #oC_ErrorCode_ResponseNotReceived   | Card did not respond with initialization complete response
 *  
 */
//==========================================================================================================================================
static oC_ErrorCode_t FinishReadingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief begins writing section
 *
 * The function initializes MMC to start writing of sectors
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card
 * @param StartAddress          Starting address of the sectors to write
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *     Error Code                       | Description
 * -------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                  | Operation success
 *  #oC_ErrorCode_NotImplemented        | The function is not implemented
 *  #oC_ErrorCode_ImplementError        | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect     | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect        | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout               | Maximum time for the command has expired, but the command has not been sent
 *  #oC_ErrorCode_ResponseNotReceived   | Card did not respond with initialization complete response
 *  
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartWritingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes writing of sectors
 *
 * The function finishes writing of sectors started by #StartWritingSectors
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *    Error Code                       | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_NotImplemented       | The function is not implemented
 *  #oC_ErrorCode_ImplementError       | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect    | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect       | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout              | Maximum time for the command has expired, but the command has not been sent
 *  #oC_ErrorCode_ResponseNotReceived  | Card did not respond with initialization complete response
 *  
 */
//==========================================================================================================================================
static oC_ErrorCode_t FinishWritingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotImplemented;

    return errorCode;
}


#undef  _________________________________________LOCAL_FUNCTIONS_____________________________________________________________________________
