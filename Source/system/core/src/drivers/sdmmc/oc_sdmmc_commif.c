/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_cardif.c
 *
 * @brief      Stores functions for card interface handling
 *
 * @author     Patryk Kubiak - (Created on: 09 4, 2022 2:16:54 PM)
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define oC_SDMMC_PRIVATE

#include <oc_sdmmc_private.h>
#include <oc_sdmmc_commif.h>
#include <oc_sdmmc_commif_sdcv2.h>
#include <oc_sdmmc_cmd.h>

#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    oC_SDMMC_DRIVER_FILE_NAME
#define DRIVER_PRIVATE
#define DRIVER_DEBUGLOG     oC_SDMMC_DEBUGLOG_ENABLED
#include <oc_driver.h>
/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

const oC_SDMMC_CommIf_Handlers_t* oC_SDMMC_CommIf_Handlers[ oC_SDMMC_CommunicationInterfaceId_NumberOfElements ] = {
         [oC_SDMMC_CommunicationInterfaceId_SDCv2] = &oC_SDMMC_CommIf_Handler_SDCv2
};

#undef  _________________________________________VARIABLES__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with FUNCTIONS
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief detects interface of a card
 *
 * The function identifies communication interface supported by the card
 *
 * @param Context               Context of the driver
 * @param outCardInterfaceId    Destination for the read card interface ID
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM  | `outCommInterfaceId` does not point to the RAM section
 *  #oC_ErrorCode_InterfaceNotDetected   | Communication interface could not be detected
 *
 *  More error codes can come from `ConfirmIdentification` functions and from #oC_SDMMC_Cmd_GoIdleState
 *
 *  @see oC_SDMMC_Cmd_GoIdleState
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_DetectInterface( oC_SDMMC_Context_t Context, oC_SDMMC_CommunicationInterfaceId_t* outCommInterfaceId, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( ErrorCondition( isram(outCommInterfaceId), oC_ErrorCode_OutputAddressNotInRAM )
      && ErrorCode( oC_SDMMC_Cmd_GoIdleState(Context, Timeout) ))
    {
        oC_Time_t timeoutPerInterface = Timeout / ((oC_Time_t)oC_ARRAY_SIZE(oC_SDMMC_CommIf_Handlers));
        errorCode = oC_ErrorCode_InterfaceNotDetected;
        oC_ARRAY_FOREACH_IN_ARRAY( oC_SDMMC_CommIf_Handlers, handlerElement )
        {
            oC_AUTO_TYPE( *handlerElement ) handler = *handlerElement;
            if ( isaddresscorrect(handler) && isaddresscorrect(handler->ConfirmIdentification) )
            {
                if ( ErrorCode( handler->ConfirmIdentification(Context, timeoutPerInterface) ) )
                {
                    driverlog( oC_LogType_GoodNews, "Detected communication interface: %s", handler->Name );
                    *outCommInterfaceId = handler->InterfaceId;
                    errorCode = oC_ErrorCode_None;
                    break;
                }
                else if( errorCode != oC_ErrorCode_InterfaceNotSupported )
                {
                    driverlog( oC_LogType_Warning, "A problem occurred during confirmation of %s interface: %R", handler->Name, errorCode );
                }
                errorCode = oC_ErrorCode_InterfaceNotDetected;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads handlers of the given communication interface
 *
 * The function reads communication interface handlers according to the ID
 *
 * @param InterfaceId               ID of the communication interface
 * @param outHandler                Destination for the communication interface handlers
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM  | `outHandler` does not point to the RAM section
 *  #oC_ErrorCode_InterfaceNotCorrect    | Communication interface ID is not valid
 *  #oC_ErrorCode_InterfaceNotSupported  | Communication interface ID is not supported yet
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_ReadHandlers( oC_SDMMC_CommunicationInterfaceId_t InterfaceId, const oC_SDMMC_CommIf_Handlers_t** outHandler )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outHandler)                                               , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( InterfaceId < oC_SDMMC_CommunicationInterfaceId_NumberOfElements, oC_ErrorCode_InterfaceNotCorrect      )
     && ErrorCondition( InterfaceId < oC_ARRAY_SIZE(oC_SDMMC_CommIf_Handlers)           , oC_ErrorCode_InterfaceNotSupported    )
     && ErrorCondition( oC_SDMMC_CommIf_Handlers[InterfaceId] != NULL                   , oC_ErrorCode_InterfaceNotSupported    )
        )
    {
        *outHandler = oC_SDMMC_CommIf_Handlers[InterfaceId];
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Calls interface specific initialization
 *
 * The function calls initialization specific for the card communication interface
 *
 * @param Context               Context of the driver
 * @param CardInfo              Destination for the read card info (`CardInfo->CardInterface` field has to be filled before)
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM  | `outCardInfo` does not point to the RAM section
 *  #oC_ErrorCode_ContextNotCorrect      | Driver `Context` is invalid
 *  #oC_ErrorCode_TimeNotCorrect         | `Timeout` cannot be negative
 *  #oC_ErrorCode_InterfaceNotCorrect    | `InitializeCard` function is not available for the selected interface
 *
 *  More error codes can come from `InitializeCard` functions and from #oC_SDMMC_CommIf_ReadHandlers
 *
 *  @see oC_SDMMC_CommIf_ReadHandlers
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_InitializeCard( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *CardInfo, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    const oC_SDMMC_CommIf_Handlers_t* handler = NULL;

    if(
        ErrorCondition( isram(CardInfo)                             , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_SDMMC_IsContextCorrect( Context )        , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCode     ( oC_SDMMC_CommIf_ReadHandlers( CardInfo->CardInterface, &handler )                   )
     && ErrorCondition( isaddresscorrect(handler->InitializeCard)   , oC_ErrorCode_InterfaceNotCorrect      )
        )
    {
        errorCode = handler->InitializeCard(Context, CardInfo, Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sets bus width
 *
 * The function calls communication interface to set bus width
 *
 * @param Context               Context of the driver
 * @param CardInfo              Card information about the card
 * @param TransferMode          Transfer mode to set
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | `CardInfo` does not point to the valid address
 *  #oC_ErrorCode_ContextNotCorrect      | Driver `Context` is invalid
 *  #oC_ErrorCode_TimeNotCorrect         | `Timeout` cannot be negative
 *  #oC_ErrorCode_InterfaceNotCorrect    | `SetTransferMode` function is not available for the selected interface
 *
 *  More error codes can come from `InitializeCard` functions and from #oC_SDMMC_CommIf_ReadHandlers
 *
 *  @see oC_SDMMC_CommIf_ReadHandlers
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_SetTransferMode( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SDMMC_TransferMode_t TransferMode, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    const oC_SDMMC_CommIf_Handlers_t* handler = NULL;

    if(
        ErrorCondition( isaddresscorrect(CardInfo)                  , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_SDMMC_IsContextCorrect( Context )        , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCode     ( oC_SDMMC_CommIf_ReadHandlers( CardInfo->CardInterface, &handler )                   )
     && ErrorCondition( isaddresscorrect(handler->SetTransferMode)  , oC_ErrorCode_NotSupportedByInterface  )
        )
    {
        errorCode = handler->SetTransferMode(Context, CardInfo->RelativeCardAddress, TransferMode, Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Initializes communication interface for reading of sectors
 *
 * The function calls a communication interface to send a request for starting of reading of sectors. Once all the required sectors are read
 * the user should call #oC_SDMMC_CommIf_FinishReadingSectors function
 *
 * @param Context               Context of the driver
 * @param CardInfo              Card information about the card
 * @param StartSector           Number of starting sector
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | `CardInfo` does not point to the valid address
 *  #oC_ErrorCode_ContextNotCorrect      | Driver `Context` is invalid
 *  #oC_ErrorCode_TimeNotCorrect         | `Timeout` cannot be negative
 *  #oC_ErrorCode_InterfaceNotCorrect    | `StartReadingSectors` function is not available for the selected interface
 *  #oC_ErrorCode_WrongSectorNumber      | `StartSector` is too big
 *  #oC_ErrorCode_SizeNotCorrect         | `CardInfo->CardSize` or `StartSector` is not valid
 *
 *  More error codes can come from `InitializeCard` functions and from #oC_SDMMC_CommIf_ReadHandlers
 *
 *  @see oC_SDMMC_CommIf_ReadHandlers
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_StartReadingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SectorNumber_t StartSector, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    const oC_SDMMC_CommIf_Handlers_t* handler = NULL;

    if(
        ErrorCondition( isaddresscorrect(CardInfo)                                  , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_SDMMC_IsContextCorrect( Context )                        , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCode     ( oC_SDMMC_CommIf_ReadHandlers( CardInfo->CardInterface, &handler )                   )
     && ErrorCondition( isaddresscorrect(handler->StartReadingSectors)              , oC_ErrorCode_NotSupportedByInterface  )
     && ErrorCondition( StartSector < CardInfo->NumberOfSectors                     , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( CardInfo->SectorSize > 0                                    , oC_ErrorCode_SectorSizeNotCorrect     )
     && ErrorCondition( (StartSector * CardInfo->SectorSize) < CardInfo->CardSize   , oC_ErrorCode_SizeNotCorrect           )
        )
    {
        errorCode = handler->StartReadingSectors(Context,CardInfo->RelativeCardAddress,StartSector * CardInfo->SectorSize,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes of a reading sectors process
 *
 * The function is for sending of a request to finish reading of the sectors started by #oC_SDMMC_CommIf_StartReadingSectors
 *
 * @param Context               Context of the driver
 * @param CardInfo              Card information about the card
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | `CardInfo` does not point to the valid address
 *  #oC_ErrorCode_ContextNotCorrect      | Driver `Context` is invalid
 *  #oC_ErrorCode_TimeNotCorrect         | `Timeout` cannot be negative
 *  #oC_ErrorCode_InterfaceNotCorrect    | `FinishReadingSectors` function is not available for the selected interface
 *
 *  More error codes can come from `InitializeCard` functions and from #oC_SDMMC_CommIf_ReadHandlers
 *
 *  @see oC_SDMMC_CommIf_ReadHandlers
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_FinishReadingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    const oC_SDMMC_CommIf_Handlers_t* handler = NULL;

    if(
        ErrorCondition( isaddresscorrect(CardInfo)                      , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( Timeout >= 0                                    , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_SDMMC_IsContextCorrect( Context )            , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCode     ( oC_SDMMC_CommIf_ReadHandlers( CardInfo->CardInterface, &handler )                       )
     && ErrorCondition( isaddresscorrect(handler->FinishReadingSectors) , oC_ErrorCode_NotSupportedByInterface  )
        )
    {
        errorCode = handler->FinishReadingSectors(Context,CardInfo->RelativeCardAddress,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Initializes communication interface for writing of sectors
 *
 * The function calls a communication interface to send a request for starting of writing of sectors. Once all the required sectors are written
 * the user should call #oC_SDMMC_CommIf_FinishReadingSectors function
 *
 * @param Context               Context of the driver
 * @param CardInfo              Card information about the card
 * @param StartSector           Number of starting sector
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | `CardInfo` does not point to the valid address
 *  #oC_ErrorCode_ContextNotCorrect      | Driver `Context` is invalid
 *  #oC_ErrorCode_TimeNotCorrect         | `Timeout` cannot be negative
 *  #oC_ErrorCode_InterfaceNotCorrect    | `StartReadingSectors` function is not available for the selected interface
 *  #oC_ErrorCode_WrongSectorNumber      | `StartSector` is too big
 *  #oC_ErrorCode_SizeNotCorrect         | `CardInfo->CardSize` or `StartSector` is not valid
 *
 *  More error codes can come from `InitializeCard` functions and from #oC_SDMMC_CommIf_ReadHandlers
 *
 *  @see oC_SDMMC_CommIf_ReadHandlers
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_StartWritingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SectorNumber_t StartSector, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    const oC_SDMMC_CommIf_Handlers_t* handler = NULL;

    if(
        ErrorCondition( isaddresscorrect(CardInfo)                                  , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_SDMMC_IsContextCorrect( Context )                        , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCode     ( oC_SDMMC_CommIf_ReadHandlers( CardInfo->CardInterface, &handler )                   )
     && ErrorCondition( isaddresscorrect(handler->StartWritingSectors)              , oC_ErrorCode_NotSupportedByInterface  )
     && ErrorCondition( StartSector < CardInfo->NumberOfSectors                     , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( CardInfo->SectorSize > 0                                    , oC_ErrorCode_SectorSizeNotCorrect     )
     && ErrorCondition( (StartSector * CardInfo->SectorSize) < CardInfo->CardSize   , oC_ErrorCode_SizeNotCorrect           )
        )
    {
        errorCode = handler->StartWritingSectors(Context,CardInfo->RelativeCardAddress,StartSector * CardInfo->SectorSize,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes of a writing sectors process
 *
 * The function is for sending of a request to finish writing of the sectors started by #oC_SDMMC_CommIf_StartWritingSectors
 *
 * @param Context               Context of the driver
 * @param CardInfo              Card information about the card
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | `CardInfo` does not point to the valid address
 *  #oC_ErrorCode_ContextNotCorrect      | Driver `Context` is invalid
 *  #oC_ErrorCode_TimeNotCorrect         | `Timeout` cannot be negative
 *  #oC_ErrorCode_InterfaceNotCorrect    | `FinishReadingSectors` function is not available for the selected interface
 *
 *  More error codes can come from `InitializeCard` functions and from #oC_SDMMC_CommIf_ReadHandlers
 *
 *  @see oC_SDMMC_CommIf_ReadHandlers
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_CommIf_FinishWritingSectors( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    const oC_SDMMC_CommIf_Handlers_t* handler = NULL;

    if(
        ErrorCondition( isaddresscorrect(CardInfo)                      , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( Timeout >= 0                                    , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( oC_SDMMC_IsContextCorrect( Context )            , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCode     ( oC_SDMMC_CommIf_ReadHandlers( CardInfo->CardInterface, &handler )                       )
     && ErrorCondition( isaddresscorrect(handler->FinishWritingSectors) , oC_ErrorCode_NotSupportedByInterface  )
        )
    {
        errorCode = handler->FinishWritingSectors(Context,CardInfo->RelativeCardAddress,Timeout);
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________
