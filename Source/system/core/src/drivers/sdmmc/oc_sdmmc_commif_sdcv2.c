/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_cardif_sdcv2.c
 *
 * @brief      Function definitions for SDCv2
 *
 * @author     Patryk Kubiak - (Created on: 09 4, 2022 1:57:28 PM)
 *
 * @note       Copyright (C) 2022 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define oC_SDMMC_PRIVATE

#include <oc_sdmmc_commif_sdcv2.h>
#include <oc_sdmmc_cmd.h>
#include <oc_sdmmc_private.h>
#include <oc_sys_lld.h>

#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    oC_SDMMC_DRIVER_FILE_NAME
#define DRIVER_PRIVATE
#define DRIVER_DEBUGLOG     oC_SDMMC_DEBUGLOG_ENABLED
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static oC_ErrorCode_t ConfirmIdentification             ( oC_SDMMC_Context_t Context, oC_Time_t Timeout );
static oC_ErrorCode_t InitializeCard                    ( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout );
static oC_ErrorCode_t WaitForOperatingConditionComplete ( oC_SDMMC_Context_t Context, oC_SDMMC_CardType_t* outCardType, oC_Time_t Timeout );
static oC_ErrorCode_t StartReadingSectors               ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );
static oC_ErrorCode_t FinishReadingSectors              ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );
static oC_ErrorCode_t StartWritingSectors               ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout );
static oC_ErrorCode_t FinishWritingSectors              ( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

const oC_SDMMC_CommIf_Handlers_t oC_SDMMC_CommIf_Handler_SDCv2 = {
                                                                  .InterfaceId              = oC_SDMMC_CommunicationInterfaceId_SDCv2,
                                                                  .Name                     = "SDCv2",
                                                                  .ConfirmIdentification    = ConfirmIdentification,
                                                                  .InitializeCard           = InitializeCard,
                                                                  .SetTransferMode          = oC_SDMMC_Cmd_SetBusWidth,
                                                                  .StartReadingSectors      = StartReadingSectors,
                                                                  .FinishReadingSectors     = FinishReadingSectors,
                                                                  .StartWritingSectors      = StartWritingSectors,
                                                                  .FinishWritingSectors     = FinishWritingSectors
};

#undef  _________________________________________VARIABLES__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief confirms identification of the card interface
 *
 * The function is used to confirm, that the given card supports this communication interface
 *
 * @param Context           Context of the driver
 * @param Timeout           Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_InterfaceNotSupported  | The card does not support this interface
 *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfirmIdentification( oC_SDMMC_Context_t Context, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( ErrorCode( oC_SDMMC_Cmd_SendIfCondition(Context, Timeout) ) )
    {
        driverlog( oC_LogType_GoodNews, "SDCv2 interface confirmed" );
        errorCode = oC_ErrorCode_None;
    }
    else if ( errorCode == oC_ErrorCode_ResponseNotReceived )
    {
        driverlog( oC_LogType_Info, "The card does not support SDCv2" );
        errorCode = oC_ErrorCode_InterfaceNotSupported;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief initializes the card to work
 *
 * The function initializes the card to work and reads the Card Info
 *
 * @param Context           Context of the driver
 * @param outCardInfo       Destination for the read card info
 * @param Timeout           maximum time for the operation
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_InterfaceNotSupported  | The card does not support this interface
 *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
 */
//==========================================================================================================================================
static oC_ErrorCode_t InitializeCard( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( oC_SDMMC_IsContextCorrect(Context)      , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isram(outCardInfo)                      , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Timeout >= 0                            , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
        oC_Struct_Define(oC_SDMMC_Cmd_Result_CardSpecificData_t,cardSpecificData);

        if (
            ErrorCode( WaitForOperatingConditionComplete( Context, &outCardInfo->CardType                                   , gettimeout(endTimestamp)  ) )
         && ErrorCode( oC_SDMMC_Cmd_AllSendCardId       ( Context, &outCardInfo->CardId                                     , gettimeout(endTimestamp)  ) )
         && ErrorCode( oC_SDMMC_Cmd_SendRelativeAddress ( Context, &outCardInfo->RelativeCardAddress                        , gettimeout(endTimestamp)  ) )
         && ErrorCode( oC_SDMMC_Cmd_SendCardSpecificData( Context, outCardInfo->RelativeCardAddress, &cardSpecificData      , gettimeout(endTimestamp)  ) )
         && ErrorCode( oC_SDMMC_Cmd_SelectCard          ( Context, outCardInfo->RelativeCardAddress                         , gettimeout(endTimestamp)  ) )
         && ErrorCode( oC_SDMMC_Cmd_SetBlockLength      ( Context, outCardInfo->RelativeCardAddress, Context->SectorSize    , gettimeout(endTimestamp)  ) )
            )
        {
            outCardInfo->CardSize           = cardSpecificData.CardSize;
            outCardInfo->CardType           = outCardInfo->CardSize <= oC_GB(2)  ? oC_SDMMC_CardType_SDSC :
                                              outCardInfo->CardSize <= oC_GB(32) ? oC_SDMMC_CardType_SDHC :
                                              outCardInfo->CardSize <= oC_TB(2)  ? oC_SDMMC_CardType_SDXC : oC_SDMMC_CardType_Unknown;
            outCardInfo->FileFormat         = cardSpecificData.FileFormat;
            outCardInfo->SectorSize         = cardSpecificData.SectorSize;
            outCardInfo->NumberOfSectors    = (cardSpecificData.CardSize / outCardInfo->SectorSize);

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for SDCv2 operating condition complete
 * @param Context       Context of the driver
 * @param outCardType   Destination for the read card type
 * @param Timeout       Maximum time for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM  | `outCardType` does not point to the RAM section
 *  #oC_ErrorCode_ContextNotCorrect      | The given `Context` is not valid
 *  #oC_ErrorCode_TimeNotCorrect         | The given `Timeout` is not correct
 *  #oC_ErrorCode_Timeout                | Maximum time for the command has expired, but the command has not been sent
 *  #oC_ErrorCode_VoltageNotSupported    | Voltage used by the system is not supported by the card
 *  #oC_ErrorCode_ResponseNotReceived    | Card did not respond with initialization complete response
 *
 *  @note
 *  More error codes can come from #oC_SYS_LLD_ReadPowerState and #oC_SDMMC_Cmd_SdSendOpCond functions
 *
 *  @see oC_SYS_LLD_ReadPowerState, oC_SDMMC_Cmd_SdSendOpCond
 */
//==========================================================================================================================================
static oC_ErrorCode_t WaitForOperatingConditionComplete( oC_SDMMC_Context_t Context, oC_SDMMC_CardType_t* outCardType, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
    float vcc = 0;
    if (
        ErrorCondition( isram(outCardType), oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCode( oC_SYS_LLD_ReadPowerState(&vcc) )
        )
    {
        oC_Struct_Define( oC_SDMMC_Cmd_Request_SdSendOpCond_t, request );
        oC_Struct_Define( oC_SDMMC_Cmd_Result_SdSendOpCond_t, result );

        request.HighCapacitySupported       = true;
        request.MaximumPerformanceEnabled   = Context->PowerMode == oC_SDMMC_PowerMode_MaxPerformance;
        request.SwitchToLowVoltage          = false;
        request.VoltageRange.Min            = vcc;
        request.VoltageRange.Max            = vcc;

        errorCode = oC_ErrorCode_Timeout;

        while( ErrorCode( oC_SDMMC_Cmd_SdSendOpCond( Context, &request, &result, gettimeout(endTimestamp) ) ) )
        {
            if (
                ErrorCondition( result.VoltageSupported       , oC_ErrorCode_VoltageNotSupported    )
             && ErrorCondition( result.InitializationCompleted, oC_ErrorCode_ResponseNotReceived    )
                )
            {
                driverlog( oC_LogType_GoodNews, "Operating condition complete, High-Capacity-Card: %b Voltage-Supported: %b", result.HighCapacityCard,
                           result.VoltageSupported);
                *outCardType    = result.HighCapacityCard ? oC_SDMMC_CardType_SDHC_or_SDXC : oC_SDMMC_CardType_SDSC;
                errorCode       = oC_ErrorCode_None;
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief begins reading section
 *
 * The function initializes SDCv2 to start reading of sectors
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card
 * @param StartAddress          Starting address of the sectors to read
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * All returned codes comes from #oC_SDMMC_Cmd_SelectCard and #oC_SDMMC_Cmd_ReadMultipleBlock
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartReadingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if ( ErrorCode( oC_SDMMC_Cmd_ReadMultipleBlock(Context, RelativeAddress, StartAddress   , gettimeout(endTimestamp)) ) )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes reading of sectors
 *
 * The function finishes reading of sectors started by #StartReadingSectors
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * All returned codes comes from #oC_SDMMC_Cmd_SelectCard and #oC_SDMMC_Cmd_StopTransmission
 */
//==========================================================================================================================================
static oC_ErrorCode_t FinishReadingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if ( ErrorCode( oC_SDMMC_Cmd_StopTransmission(Context, RelativeAddress, gettimeout(endTimestamp)) ) )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief begins writing section
 *
 * The function initializes SDCv2 to start writing of sectors
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card
 * @param StartAddress          Starting address of the sectors to write
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * All returned codes comes from #oC_SDMMC_Cmd_SelectCard and #oC_SDMMC_Cmd_WriteMultipleBlock
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartWritingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_MemoryOffset_t StartAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if ( ErrorCode( oC_SDMMC_Cmd_WriteMultipleBlock(Context, RelativeAddress, StartAddress   , gettimeout(endTimestamp)) ) )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes writing of sectors
 *
 * The function finishes writing of sectors started by #StartWritingSectors
 *
 * @param Context               Context of the driver
 * @param RelativeAddress       Relative address of the card
 * @param Timeout               Maximum time for the operation
 *
 * @return
 * All returned codes comes from #oC_SDMMC_Cmd_SelectCard and #oC_SDMMC_Cmd_StopTransmission
 */
//==========================================================================================================================================
static oC_ErrorCode_t FinishWritingSectors( oC_SDMMC_Context_t Context, oC_SDMMC_RelativeCardAddress_t RelativeAddress, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if ( ErrorCode( oC_SDMMC_Cmd_StopTransmission(Context, RelativeAddress, gettimeout(endTimestamp)) ) )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_SECTION______________________________________________________________________________
