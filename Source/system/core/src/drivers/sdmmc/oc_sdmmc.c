/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc.c
 * 
 * File based on driver.c Ver 1.1.1
 *
 * @brief      The file with source for SDMMC driver interface
 *
 * @author     Kamil Drobienko - (Created on: 2017-08-07 - 22:44:51)
 *
 * @copyright  Copyright (C) 2017 Kamil Drobienko <kamil.drobienko@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#define oC_SDMMC_PRIVATE
#include <oc_sdmmc.h>
#include <oc_sdmmc_lld.h>
#include <oc_sdmmc_mode_lld.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_sdmmc_private.h>
#include <oc_list.h>
#include <oc_dynamic_config.h>

#include <oc_gpio.h>
#include <oc_spi.h>
#include <oc_ktime.h>
#include <oc_struct.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    oC_SDMMC_DRIVER_FILE_NAME
#define DRIVER_DEBUGLOG     oC_SDMMC_DEBUGLOG_ENABLED
#define DRIVER_TYPE         DISK_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE        oC_SDMMC_Configure
#define DRIVER_UNCONFIGURE      oC_SDMMC_Unconfigure
#define DRIVER_TURN_ON          oC_SDMMC_TurnOn
#define DRIVER_TURN_OFF         oC_SDMMC_TurnOff
#define IS_TURNED_ON            oC_SDMMC_IsTurnedOn
#define HANDLE_IOCTL            oC_SDMMC_Ioctl
#define WAIT_FOR_NEW_DISK       oC_SDMMC_WaitForNewDisk
#define READ_SECTOR_SIZE        oC_SDMMC_ReadSectorSize
#define READ_NUMBER_OF_SECTORS  oC_SDMMC_ReadNumberOfSectors
#define READ_SECTORS            oC_SDMMC_ReadSectors
#define WRITE_SECTORS           oC_SDMMC_WriteSectors
#define ERASE_SECTORS           oC_SDMMC_EraseSectors
#define WAIT_FOR_DISK_EJECT     oC_SDMMC_WaitForDiskEject

#define IS_CONTEXT_CORRECT(Context) oC_SDMMC_IsContextCorrect(Context)

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all SDMMC driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "sdmmc module"
};

static const oC_SDMMC_Mode_Interface_t *ModeInterfaces[]={
    #ifdef oC_SDMMC_LLD_AVAILABLE
    &oC_SDMMC_Mode_Interface_LLD,
    #endif
    #ifdef oC_SDMMC_SPI_AVAILABLE
    &oC_SDMMC_Mode_Interface_SPI,
    #endif
    #ifdef oC_SDMMC_SW1BIT_AVAILABLE
    &oC_SDMMC_Mode_Interface_SW1BIT
    #endif
};

//==========================================================================================================================================
/**
 * Stores contextes for channels
 */
//==========================================================================================================================================
static oC_List(oC_SDMMC_Context_t) Contextes = NULL;

//==========================================================================================================================================
/**
 * @brief An array of strings representing the names of different transfer modes for the SDMMC module.
 *
 * The array is indexed by the oC_SDMMC_TransferMode enum values, so TransferModeNames[oC_SDMMC_TransferMode_Auto]
 * corresponds to the string "Auto", TransferModeNames[oC_SDMMC_TransferMode_1Bit] corresponds to the string "1Bit",
 * and so on.
 *
 * @see oC_SDMMC_TransferMode_t
 */
//==========================================================================================================================================
static const char* TransferModeNames[oC_SDMMC_TransferMode_NumberOfElements] = {
            "Auto",
            "1Bit",
            "4Bit",
            "8Bit",
            "SPI"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool                 WaitForCardDetection                    ( oC_SDMMC_Context_t Context , oC_Time_t Timeout );
static oC_SDMMC_Context_t   Context_New                             ( const oC_SDMMC_Config_t * Config );
static oC_ErrorCode_t       DeleteContext                           ( oC_SDMMC_Context_t * Context );
static bool                 CheckConfigAndSetModeInterfaceIfNotSet  ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context );
static const char *         GetTransferModeNameFromEnum             ( oC_SDMMC_TransferMode_t TransferMode );
static oC_Pin_t             GetDetectionPin                         ( const oC_SDMMC_Config_t * Config );
static oC_SDMMC_CardInfo_t* GetCardInfo                             ( oC_SDMMC_Context_t Context, oC_DiskId_t DiskId );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Turns on the module
 *
 * The function is for turning on the SDMMC module.
 * It also turns on the LLD layer, sdmmc spi mode and sdmmc sw1bit mode.
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                              | Description
 * ---------------------------------------------|-------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                          | Operation success
 *  #oC_ErrorCode_ImplementError                | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleIsTurnedOn              | Module is turned on
 *  #oC_ErrorCode_SdmmcModeInterfaceNotFound    | SDMMC modes are unavailable interface
 *
 *  @note
 *  More error codes can be returned by function oC_SDMMC_Mode_TurnOn
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_TurnOn( void )
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_SDMMC))
    {
        Contextes = oC_List_New(&Allocator, AllocationFlags_ZeroFill);

        if( ErrorCondition( Contextes != NULL, oC_ErrorCode_AllocationError ) )
        {
            if ( ErrorCondition( oC_ARRAY_SIZE(ModeInterfaces) > 0, oC_ErrorCode_SdmmcModeInterfaceNotFound ) )
            {
                for(uint8_t modeIdx = 0; modeIdx < oC_ARRAY_SIZE(ModeInterfaces); modeIdx++)
                {
                    errorCode = oC_SDMMC_Mode_TurnOn( ModeInterfaces[modeIdx] );

                    if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                    else if(oC_ErrorOccur(errorCode))
                    {
                        driverlog( oC_LogType_Error, "Cannot turn on %s, %R", ModeInterfaces[modeIdx]->ModeName, errorCode );
                    }
                }

            }
            if(!oC_ErrorOccur(errorCode))
            {
                driverlog( oC_LogType_GoodNews, "Driver SDMMC has been successfully turned on" );

                /* This must be always at the end of the function */
                oC_Module_TurnOn(oC_Module_SDMMC);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfFalse("Cannot release context list", oC_List_Delete(Contextes), oC_ErrorCode_ReleaseError);
            }
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the SDMMC driver
 *
 * The function for turning off the SDMMC driver.
 * It also turns off the LLD layer, sdmmc spi mode and sdmmc sw1bit mode.
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                              | Description
 * ---------------------------------------------|-------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                          | Operation success
 *  #oC_ErrorCode_ImplementError                | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet           | Module is not turned on
 *  #oC_ErrorCode_SdmmcModeInterfaceNotFound    | SDMMC modes are unavailable interface
 *
 *  @note
 *  More error codes can be returned by function oC_SDMMC_Mode_TurnOff
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_SDMMC);

        if(ErrorCondition( oC_ARRAY_SIZE(ModeInterfaces) > 0, oC_ErrorCode_SdmmcModeInterfaceNotFound ))
        {
            foreach(Contextes, context)
            {
                errorCode = oC_SDMMC_Unconfigure(NULL, &context);

                if(oC_ErrorOccur(errorCode))
                {
                    driverlog( oC_LogType_Error, "Cannot unconfigure sdmmc driver: %R", errorCode );
                }
            }


            for(uint8_t modeIdx = 0; modeIdx < oC_ARRAY_SIZE(ModeInterfaces); modeIdx++)
            {
                errorCode = oC_SDMMC_Mode_TurnOff( ModeInterfaces[modeIdx] );

                if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
                {
                    errorCode = oC_ErrorCode_None;
                }
                else if(oC_ErrorOccur(errorCode))
                {
                    driverlog( oC_LogType_Error, "Cannot turn on %s, %R", ModeInterfaces[modeIdx]->ModeName, errorCode );
                }
            }
            oC_MemMan_FreeAllMemoryOfAllocator(&Allocator);
            oC_List_Delete(Contextes , 0);
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_SDMMC_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_SDMMC);
}

//==========================================================================================================================================
/**
 * @brief configures SDMMC driver to work
 *
 * The function is for configuration of the driver. Look at the #oC_SDMMC_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_WrongConfigAddress     | SDMMC config address is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Output address is not in RAM
 *  #oC_ErrorCode_AllocationError        | Could not allocate memory
 *  #oC_ErrorCode_ReleaseError           | Could not release memory
 *  #oC_ErrorCode_TimeNotCorrect         | Detection polling period is not correct
 *
 *  @note
 *  More error codes can be returned by function oC_SDMMC_Mode_Configure
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Configure( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC))
    {
        if( ErrorCondition( isaddresscorrect(Config)  , oC_ErrorCode_WrongConfigAddress )
         && ErrorCondition( isram(outContext)         , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( Config->DetectionMode < oC_SDMMC_DetectionMode_NumberOfElements, oC_ErrorCode_DetectionModeNotCorrect  )
         && ErrorCondition( Config->Advanced.DetectionPollingPeriod >= 0, oC_ErrorCode_TimeNotCorrect )
            )
        {
            oC_SDMMC_Context_t context = Context_New(Config);

            if( ErrorCondition( context != NULL, oC_ErrorCode_AllocationError ) )
            {
                if ( ErrorCode( oC_GPIO_QuickInput( context->DetectionPin , oC_GPIO_Pull_Up , Config->DetectionMode == oC_SDMMC_DetectionMode_Interrupt ?
                                  oC_GPIO_IntTrigger_BothEdges : oC_GPIO_IntTrigger_Off )) )
                {
                    if ( ErrorCondition( CheckConfigAndSetModeInterfaceIfNotSet(Config,context) , oC_ErrorCode_SdmmcConfigNotSupported )
                      && ErrorCode     ( oC_SDMMC_Mode_Configure(Config,context) )
                         )
                    {
                        *outContext = context;
                        oC_List_PushBack(Contextes, context, &Allocator);
                        errorCode   = oC_ErrorCode_None;
                    }
                }
            }
            if ( oC_ErrorOccur(errorCode) )
            {
               DeleteContext(&context);
               driverlog( oC_LogType_Error, "Cannot configure selected mode: %R", errorCode );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_SDMMC_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                           | Description
 * ------------------------------------------|----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                       | Operation success
 *  #oC_ErrorCode_ImplementError             | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet        | Module is not turned on
 *  #oC_ErrorCode_WrongConfigAddress         | SDMMC config address is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM      | Output address is not in RAM
 *  #oC_ErrorCode_ModuleBusy                 | Module is busy
 *  #oC_ErrorCode_ReleaseError               | Could not release memory
 *  #oC_ErrorCode_CannotRemoveObjectFromList | Cannot remove SDMMC from the object list
 *
 *  @note
 *  More error codes can be returned by function oC_SDMMC_Mode_Unconfigure
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Unconfigure( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t * outContext )
{
    oC_ErrorCode_t errorCode          = oC_ErrorCode_ImplementError;
    oC_Time_t      takeMutexTimeout   = oC_DynamicConfig_GetValue( sdmmc, TakeMutexTimeout );

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC))
    {
        if( ErrorCondition( isram(outContext)                               , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( IS_CONTEXT_CORRECT(*outContext)                 , oC_ErrorCode_ContextNotCorrect     )
         )
        {
            bool removedFromList    = oC_List_RemoveAll(Contextes, *outContext);
            bool isMutexTaken       = oC_Mutex_Take((*outContext)->ModuleBusy, takeMutexTimeout);

            if( ErrorCode(oC_SDMMC_Mode_Unconfigure(*outContext))
             && ErrorCondition( removedFromList , oC_ErrorCode_CannotRemoveObjectFromList )
             && ErrorCondition( isMutexTaken    , oC_ErrorCode_ModuleBusy                 )
                 )
            {
                errorCode = DeleteContext(outContext);
            }
            else
            {
                oC_SaveIfErrorOccur("Cannot delete SDMMC context", DeleteContext(outContext));
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of errror
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Ioctl( oC_SDMMC_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Context)            , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect )
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for storage
 *
 * The function is for waiting for the storage - it blocks a current thread until SD card is available or timeout expires.
 *
 * @param Context       Context of the driver
 * @param outDiskId     Destination for the ID of the new storage
 * @param Timeout       Maximum time to wait for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardType address is not in RAM
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *
 *  @note
 *  More error codes can be returned by oC_SDMMC_Mode_InitializeCard function
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_WaitForNewDisk( oC_SDMMC_Context_t Context , oC_DiskId_t * outDiskId, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC)
     && ErrorCondition( IS_CONTEXT_CORRECT(Context) , oC_ErrorCode_ContextNotCorrect     )
     && ErrorCondition( Timeout >= 0                , oC_ErrorCode_TimeNotCorrect        )
     && ErrorCondition( isram(outDiskId)            , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        oC_SDMMC_CardInfo_t* cardInfo = kmalloc(sizeof(oC_SDMMC_CardInfo_t), &Allocator, AllocationFlags_ZeroFill);

        if(
            ErrorCondition( WaitForCardDetection( Context, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout )
         && ErrorCode     ( oC_SDMMC_Mode_InitializeCard( Context, cardInfo, gettimeout(endTimestamp)  )    )
            )
        {
            cardInfo->DiskId        = (oC_DiskId_t)cardInfo->RelativeCardAddress;

            oC_List_PushBack(Context->DetectedCards, cardInfo, &Allocator);
            *outDiskId              = (oC_DiskId_t)cardInfo->RelativeCardAddress;
            errorCode               = oC_ErrorCode_None;
        }
        else
        {
            oC_SaveIfFalse("Cannot release CardInfo memory", kfree(cardInfo, AllocationFlags_Default), oC_ErrorCode_ReleaseError);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads sector size
 *
 * The function returns size of the sector in function parameter `outSectorSize`.
 *
 * @param Context       Context of the driver
 * @param DiskId        Disc ID
 * @param outSectorSize Destination for the sector size value
 * @param Timeout       Maximum time to wait for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardType address is not in RAM
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadSectorSize( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_MemorySize_t * outSectorSize , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC) )
    {
        oC_IntMan_EnterCriticalSection();
        if(
            ErrorCondition( IS_CONTEXT_CORRECT(Context)                 , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( isram(outSectorSize)                        , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)    , oC_ErrorCode_CardNotDetected          )
            )
        {
            oC_SDMMC_CardInfo_t* cardInfo = GetCardInfo(Context, DiskId);
            if ( ErrorCondition( cardInfo != NULL, oC_ErrorCode_NoSuchDisk ) )
            {
                *outSectorSize  = cardInfo->SectorSize;
                errorCode       = oC_ErrorCode_None;
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads number of sectors
 *
 * The function returns number of sectors in function parameter `outSectorNumber`.
 *
 * @param Context         Context of the driver
 * @param DiskId          Disc ID
 * @param outSectorNumber Destination for the number of sectors
 * @param Timeout         Maximum time to wait for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardType address is not in RAM
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadNumberOfSectors( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t * outSectorNumber , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC) )
    {
        oC_IntMan_EnterCriticalSection();

        if(
            ErrorCondition( IS_CONTEXT_CORRECT(Context)                 , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( isram(outSectorNumber)                      , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)    , oC_ErrorCode_CardNotDetected          )
            )
        {
            oC_SDMMC_CardInfo_t* cardInfo = GetCardInfo(Context, DiskId);
            if ( ErrorCondition( cardInfo != NULL, oC_ErrorCode_NoSuchDisk ) )
            {
                *outSectorNumber= cardInfo->NumberOfSectors;
                errorCode       = oC_ErrorCode_None;
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads sectors
 *
 * The function reads data from sectors and returning it by outBuffer pointer.
 *
 * @param Context         Context of the driver
 * @param DiskId          Disc ID
 * @param StartSector     Address of first sector to read
 * @param outBuffer       Buffer for received data
 * @param Size            Sector size to read
 * @param Timeout         Maximum time to wait for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outBuffer address is not in RAM
 *  #oC_ErrorCode_SizeNotCorrect         | Given size is not correct
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 *  #oC_ErrorCode_SizeNotAligned         | Given size is not aligned
 *  #oC_ErrorCode_WrongSectorNumber      | StartSector is not correct (bigger than sector size)
 *  #oC_ErrorCode_Timeout                | Timeout occurred
 *
 *  @note
 *  More error codes can be returned by SendCommand and ReadData functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadSectors( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, void * outBuffer , oC_MemorySize_t Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC) )
    {
        if(
            ErrorCondition( IS_CONTEXT_CORRECT(Context)                                 , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( isram(outBuffer)                                            , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Size > 0                                                    , oC_ErrorCode_SizeNotCorrect           )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)                    , oC_ErrorCode_CardNotDetected          )
         && ErrorCondition( (Size % Context->SectorSize) == 0                           , oC_ErrorCode_SizeNotAligned           )
         && ErrorCondition( oC_Mutex_Take(Context->ModuleBusy,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout                  )
            )
        {
            oC_SDMMC_CardInfo_t* cardInfo = GetCardInfo(Context, DiskId);
            if ( ErrorCondition( cardInfo != NULL, oC_ErrorCode_NoSuchDisk ) )
            {
                errorCode = oC_SDMMC_Mode_ReadData(Context, cardInfo, StartSector, outBuffer, &Size, Timeout);
            }
            oC_Mutex_Give(Context->ModuleBusy);
        }
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Write data to sectors
 *
 * The function is writing data to card sectors.
 *
 * @param Context         Context of the driver
 * @param DiskId          Disc ID
 * @param StartSector     Address of first sector to write
 * @param Buffer          Buffer for writing data
 * @param Size            Sector size to write
 * @param Timeout         Maximum time to wait for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_WrongAddress           | Buffer address is not correct
 *  #oC_ErrorCode_SizeNotCorrect         | Given size is not correct
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 *  #oC_ErrorCode_SizeNotAligned         | Given size is not aligned
 *  #oC_ErrorCode_WrongSectorNumber      | StartSector is not correct (bigger than sector size)
 *  #oC_ErrorCode_Timeout                | Timeout occurred
 *
 *  @note
 *  More error codes can be returned by SendCommand and WriteData functions
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_WriteSectors( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, const void * Buffer , oC_MemorySize_t Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC) )
    {
        if(
            ErrorCondition( IS_CONTEXT_CORRECT(Context)                                 , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( isaddresscorrect(Buffer)                                    , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( Size > 0                                                    , oC_ErrorCode_SizeNotCorrect           )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)                    , oC_ErrorCode_CardNotDetected          )
         && ErrorCondition( (Size % Context->SectorSize) == 0                           , oC_ErrorCode_SizeNotAligned           )
         && ErrorCondition( oC_Mutex_Take(Context->ModuleBusy,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout                  )
            )
        {
            oC_SDMMC_CardInfo_t* cardInfo = GetCardInfo(Context, DiskId);
            if ( ErrorCondition( cardInfo != NULL, oC_ErrorCode_NoSuchDisk ) )
            {
                errorCode = oC_SDMMC_Mode_WriteData(Context, cardInfo, StartSector, Buffer, &Size, Timeout);
            }
            oC_Mutex_Give(Context->ModuleBusy);
        }
    }

    return errorCode;
}
//==========================================================================================================================================
/**
 * @brief Erases card sector
 *
 * The function is erasing data on card sectors.
 *
 * @param Context         Context of the driver
 * @param DiskId          Disc ID
 * @param StartSector     Address of first sector to erase
 * @param Size            Sector size to erase
 * @param Timeout         Maximum time to wait for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_SizeNotCorrect         | Given size is not correct
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 *  #oC_ErrorCode_SizeNotAligned         | Given size is not aligned
 *  #oC_ErrorCode_WrongSectorNumber      | StartSector is not correct (bigger than sector size)
 *  #oC_ErrorCode_Timeout                | Timeout occurred
 *
 *  @note
 *  More error codes can be returned by SendCommand function
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_EraseSectors( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, oC_MemorySize_t Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC) )
    {
        if(
            ErrorCondition( IS_CONTEXT_CORRECT(Context)                                 , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( Size > 0                                                    , oC_ErrorCode_SizeNotCorrect           )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)                    , oC_ErrorCode_CardNotDetected          )
//         && ErrorCondition( (Size % Context->SectorSize) == 0                           , oC_ErrorCode_SizeNotAligned           )
         && ErrorCondition( StartSector < Context->NumberOfSectors                      , oC_ErrorCode_WrongSectorNumber        )
         && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect           )
         && ErrorCondition( oC_Mutex_Take(Context->ModuleBusy,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout                  )
            )
        {
#if 0
            oC_SDMMC_Command_t eraseStartAddrCmd = {
                                                 .Index    = 0,
                                                 .Argument = StartSector,
                                                 .Response = {
                                                              .Type = oC_SDMMC_Mode_CommandResponseType_Short,
                                                              .ResponseIndex = oC_SDMMC_Mode_ResponseIndex_R1
                                                 }
            };
            oC_SDMMC_Command_t eraseEndAddrCmd = {
                                                 .Index    = 0,
                                                 .Argument = StartSector + ( Size * (Context->SectorSize) ),
                                                 .Response = {
                                                              .Type = oC_SDMMC_Mode_CommandResponseType_Short,
                                                              .ResponseIndex = oC_SDMMC_Mode_ResponseIndex_R1
                                                 }
            };
            oC_SDMMC_Command_t eraseCmd = {
                                                 .Index    = oC_SDMMC_Mode_CommandIndex_CMD38,
                                                 .Argument = 0,
                                                 .Response = {
                                                              .Type = oC_SDMMC_Mode_CommandResponseType_Short,
                                                              .ResponseIndex = oC_SDMMC_Mode_ResponseIndex_R1b
                                                 }
            };
            if( Context->CardType == oC_SDMMC_CardType_SDCv1 || Context->CardType == oC_SDMMC_CardType_SDCv2 )
            {
                eraseStartAddrCmd.Index = oC_SDMMC_Mode_CommandIndex_CMD32;
                eraseEndAddrCmd.Index = oC_SDMMC_Mode_CommandIndex_CMD33;
            }
            else
            {
                eraseStartAddrCmd.Index = oC_SDMMC_Mode_CommandIndex_CMD35;
                eraseEndAddrCmd.Index = oC_SDMMC_Mode_CommandIndex_CMD36;
            }

            if( ErrorCode( oC_SDMMC_Mode_SendCommand( Context, &eraseStartAddrCmd, Timeout ))
             && ErrorCode( oC_SDMMC_Mode_SendCommand( Context, &eraseEndAddrCmd  , Timeout ))
             && ErrorCode( oC_SDMMC_Mode_SendCommand( Context, &eraseCmd         , Timeout )))
            {
                errorCode = oC_ErrorCode_None;
            }
#else
            errorCode = oC_ErrorCode_NotImplemented;
#endif
            oC_Mutex_Give(Context->ModuleBusy);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Waits until the card is not ejected
 *
 * The function is for waiting until the card is not ejected.
 *
 * @param Context       Context of the driver
 * @param outDiskId     Destination for the ID of the ejected storage
 * @param Timeout       Maximum time to wait for the operation
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ModuleNotStartedYet    | Module is not turned on
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_TimeNotCorrect         | Given Timeout is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outBuffer address is not in RAM
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_WaitForDiskEject( oC_SDMMC_Context_t Context , oC_DiskId_t * outDiskId, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode     = oC_ErrorCode_ImplementError;
#if 0
    oC_Timestamp_t endTimestamp  = timeouttotimestamp(Timeout);
#endif
    oC_Time_t      sleepTime     = oC_DynamicConfig_GetValue( sdmmc, DiscEjectSleepTime );

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC)
     && ErrorCondition( IS_CONTEXT_CORRECT(Context)                 , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( isram(outDiskId)                            , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)    , oC_ErrorCode_CardNotDetected          )
     && ErrorCondition( sleepTime > 0                               , oC_ErrorCode_TimeNotCorrect           )
        )
    {
#if 0
        oC_SDMMC_Command_t selectCardCmd = {
                                             .Index    = oC_SDMMC_Mode_CommandIndex_CMD7,
                                             .Argument = 0,
                                             .Response = {
                                                          .Type = oC_SDMMC_Mode_CommandResponseType_Short,
                                             }
        };

        if( Context->CardType == oC_SDMMC_CardType_SDCv1 || Context->CardType == oC_SDMMC_CardType_SDCv2 )
        {
            selectCardCmd.Response.ResponseIndex = oC_SDMMC_Mode_ResponseIndex_R1b;
        }
        else
        {
            selectCardCmd.Response.ResponseIndex = oC_SDMMC_Mode_ResponseIndex_R1;
        }

        while( Timeout > 0 || errorCode == oC_ErrorCode_None )
        {
            if( ErrorCondition( IS_CONTEXT_CORRECT(Context)                                 , oC_ErrorCode_ContextNotCorrect )
             && ErrorCondition( oC_Mutex_Take(Context->ModuleBusy,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout           ))
            {
                if( !ErrorCode(oC_SDMMC_Mode_SendCommand( Context, &selectCardCmd  , Timeout ))
                 && ( errorCode == oC_ErrorCode_Timeout ) )
                {
                    *outDiskId   = 0;
                    errorCode = oC_ErrorCode_None;
                    break;
                }
                else if(oC_ErrorOccur(errorCode))
                {
                    driverlog( oC_LogType_Error, "Failed when sending command: %R", errorCode );
                }

                oC_Mutex_Give(Context->ModuleBusy);
            }

            sleep(sleepTime);
        }
#else
        errorCode = oC_ErrorCode_NotImplemented;
#endif
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads card information
 *
 * The function is for reading of card information. It does not update the information, but it uses the cached values from the initialization
 * sequence
 *
 * @param Context               Context of the driver
 * @param DiskId                ID of the card to read the information
 * @param outCardInfo           Destination for the read card information
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardInfo address is not in RAM
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_CardNotDetected        | No card has been detected
 *  #oC_ErrorCode_NoSuchDisk             | Disk with the given DiskId has not been detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadCardInfo( oC_SDMMC_Context_t Context , oC_DiskId_t DiskId , oC_SDMMC_CardInfo_t* outCardInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( oC_Module_TurnOnVerification(&errorCode , oC_Module_SDMMC)
      && ErrorCondition( IS_CONTEXT_CORRECT(Context)                    , oC_ErrorCode_ContextNotCorrect        )
      && ErrorCondition( isram(outCardInfo)                             , oC_ErrorCode_OutputAddressNotInRAM    )
      && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)       , oC_ErrorCode_CardNotDetected          )
        )
    {
        oC_SDMMC_CardInfo_t* cardInfo = GetCardInfo(Context, DiskId);
        if ( ErrorCondition( cardInfo != NULL, oC_ErrorCode_NoSuchDisk ) )
        {
            memcpy(outCardInfo, cardInfo, sizeof(*cardInfo));
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads card type
 *
 * The function is reads card type from SMMC Context and store it in outCardType parameter.
 *
 * @param Context       Context of the driver
 * @param outCardType   Destination for card type
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardType address is not in RAM
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadCardType ( oC_SDMMC_Context_t Context , oC_SDMMC_CommunicationInterfaceId_t * outCardType )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC ) )
    {
        oC_IntMan_EnterCriticalSection();

        if( ErrorCondition( isram(outCardType)                          , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( IS_CONTEXT_CORRECT(Context)                 , oC_ErrorCode_ContextNotCorrect     )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)    , oC_ErrorCode_CardNotDetected       )
            )
        {
            *outCardType = oC_SDMMC_CommunicationInterfaceId_Unknown;

            errorCode = oC_ErrorCode_NotImplemented;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads  mode type
 *
 * The function reads mode type from SMMC Context and store it in outMode and outName parameters.
 *
 * @param Context       Context of the driver
 * @param outMode       Destination for mode type
 * @param outName       Destination for mode name
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardType address is not in RAM
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadMode( oC_SDMMC_Context_t Context , oC_SDMMC_Mode_t * outMode , const char ** outName )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC ) )
    {
        oC_IntMan_EnterCriticalSection();

        if( ErrorCondition( isram(outMode)                              , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( isram(outName) || outName == NULL           , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( IS_CONTEXT_CORRECT(Context)                 , oC_ErrorCode_ContextNotCorrect     )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)    , oC_ErrorCode_CardNotDetected       )
            )
        {
            *outMode = Context->Interface->Mode;

            if( outName != NULL )
            {
                *outName = Context->Interface->ModeName;
            }
        }

        oC_IntMan_ExitCriticalSection();
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads transfer mode type
 *
 * The function reads transfer mode type from SMMC Context and store it in outMode and outName parameters.
 *
 * @param Context           Context of the driver
 * @param outTransferMode   Destination for transfer mode type
 * @param outName           Destination for transfer mode name
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardType address is not in RAM
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadTransferMode ( oC_SDMMC_Context_t Context , oC_SDMMC_TransferMode_t * outTransferMode , const char ** outName )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC ))
    {
        oC_IntMan_EnterCriticalSection();

        if( ErrorCondition( isram(outTransferMode)                      , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( isram(outName) || outName == NULL           , oC_ErrorCode_OutputAddressNotInRAM )
         && ErrorCondition( IS_CONTEXT_CORRECT(Context)                 , oC_ErrorCode_ContextNotCorrect     )
         && ErrorCondition( !oC_List_IsEmpty(Context->DetectedCards)    , oC_ErrorCode_CardNotDetected       )
            )
        {
            *outTransferMode = Context->TransferMode;

            if( outName != NULL )
            {
                *outName = GetTransferModeNameFromEnum(Context->TransferMode);
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief changes transfer mode
 *
 * The function changes transfer mode if possible
 *
 * @param Context           Context of the driver
 * @param TransferMode      Transfer mode to set
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_OutputAddressNotInRAM  | outCardType address is not in RAM
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect      | Given Context is not correct
 *  #oC_ErrorCode_CardNotDetected        | Card is not detected
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_SetTransferMode( oC_SDMMC_Context_t Context , oC_SDMMC_TransferMode_t TransferMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC ))
    {
        oC_IntMan_EnterCriticalSection();

        if ( ErrorCondition( IS_CONTEXT_CORRECT(Context), oC_ErrorCode_ContextNotCorrect )
          && ErrorCode( oC_SDMMC_Mode_SetTransferMode(Context, TransferMode) )
            )
        {
            Context->TransferMode = TransferMode;
            errorCode = oC_ErrorCode_None;
            driverlog( oC_LogType_Info, "Transfer Mode has been set to %s\n", GetTransferModeNameFromEnum(TransferMode) );
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads transfer mode name
 *
 * The function reads a string with name for the given transfer mode
 *
 * @param TransferMode          Transfer mode
 * @param outName               destination for the string with name of the transfer mode
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM     | `outName` address is not in RAM
 *  #oC_ErrorCode_TransferModeNotCorrect    | Given `TransferMode` is not correct
 *  #oC_ErrorCode_TransferModeNotSupported  | Given `TransferMode` is not supported by the function
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadTransferModeName( oC_SDMMC_TransferMode_t TransferMode, const char** outName )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC ))
    {
        if( ErrorCondition( isram(outName)                                       , oC_ErrorCode_OutputAddressNotInRAM   )
         && ErrorCondition( TransferMode < oC_SDMMC_TransferMode_NumberOfElements, oC_ErrorCode_TransferModeNotCorrect  )
            )
        {
            *outName = GetTransferModeNameFromEnum(TransferMode);
            if ( ErrorCondition( strcmp(*outName, "Unknown") == 0, oC_ErrorCode_TransferModeNotSupported ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads transfer mode by its name
 *
 * The function is for reading of transfer mode according to its name
 *
 * @param Name                  name of the transfer mode to find
 * @param outTransferMode       destination for the transfer mode
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress              | Given `Name` is not correct address of the string
 *  #oC_ErrorCode_StringIsEmpty             | Given `Name` is empty
 *  #oC_ErrorCode_OutputAddressNotInRAM     | `outTransferMode` address is not in RAM
 *  #oC_ErrorCode_TransferModeNotSupported  | Given `Name` is not supported by the function
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_ReadTransferModeByName( const char* Name, oC_SDMMC_TransferMode_t* outTransferMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC ))
    {
        if( ErrorCondition( isaddresscorrect(Name)          , oC_ErrorCode_WrongAddress             )
         && ErrorCondition( strlen(Name) > 0                , oC_ErrorCode_StringIsEmpty            )
         && ErrorCondition( isram(outTransferMode)          , oC_ErrorCode_OutputAddressNotInRAM    )
            )
        {
            errorCode = oC_ErrorCode_TransferModeNotSupported;

            for ( oC_SDMMC_TransferMode_t transferMode = 0; transferMode < oC_SDMMC_TransferMode_NumberOfElements; transferMode++ )
            {
                if ( strcmp(TransferModeNames[transferMode], Name) == 0 )
                {
                    *outTransferMode = transferMode;
                    errorCode = oC_ErrorCode_None;
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns card state name
 *
 * The function converts Card State to a string
 *
 * @param State         state to convert
 *
 * @return string with name of the state
 */
//==========================================================================================================================================
const char* oC_SDMMC_GetCardStateName( oC_SDMMC_State_t State )
{
    const char* stateName = "Unknown";
    static const char* stateNames[] = {
         "Idle",
         "Ready",
         "Identification",
         "StandBy",
         "Transfer",
         "SendingData",
         "ReceiveData",
         "Programming",
         "Disconnect"
    };
    if ( State < oC_ARRAY_SIZE(stateNames) )
    {
        stateName = stateNames[State];
    }

    return stateName;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief waits until card is detected
 */
//==========================================================================================================================================
static bool WaitForCardDetection( oC_SDMMC_Context_t Context , oC_Time_t Timeout )
{
    bool            detected        = false;
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;
    oC_Timestamp_t  endTimestamp    = timeouttotimestamp(Timeout);

    while( ErrorCode( oC_GPIO_WaitForPinsState(Context->DetectionPin, oC_GPIO_PinsState_AllLow, gettimeout(endTimestamp), Context->DetectionPeriod) )  )
    {
        if( oC_GPIO_GetLowStatePins(Context->DetectionPin) == Context->DetectionPin )
        {
            detected = true;
            break;
        }
    }

    if( !detected )
    {
        driverlog( oC_LogType_Warning, "SD card not detected: %R", errorCode );
    }

    return detected;
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a new context
 */
//==========================================================================================================================================
static oC_SDMMC_Context_t Context_New( const oC_SDMMC_Config_t * Config )
{
    oC_SDMMC_Context_t              context                = kmalloc( sizeof(struct oC_SDMMC_Context_t), &Allocator, AllocationFlags_ZeroFill );
    oC_Mutex_t                      busyMutex              = oC_Mutex_New(oC_Mutex_Type_Normal,&Allocator,AllocationFlags_Default);
    oC_List(oC_SDMMC_CardInfo_t*)   detectedCards          = oC_List_New(&Allocator, AllocationFlags_ZeroFill);

    if(
        oC_SaveIfFalse("context"        , context       != NULL, oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("busyMutex"      , busyMutex     != NULL, oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("detectedCards"  , detectedCards != NULL, oC_ErrorCode_AllocationError )
        )
    {
        context->ObjectControl      = oC_CountObjectControl(context, oC_ObjectId_SDMMCContext);
        context->SectorSize         = Config->SectorSize;
        context->ModuleBusy         = busyMutex;
        context->TransferMode       = Config->TransferMode;
        context->DetectionPin       = GetDetectionPin(Config);
        context->NumberOfRetries    = Config->Advanced.NumberOfRetries ? Config->Advanced.NumberOfRetries : 10;
        context->PowerMode          = Config->PowerMode;
        context->DetectionPeriod    = Config->Advanced.DetectionPollingPeriod;
        context->DetectedCards      = detectedCards;

        if ( Config->DetectionMode == oC_SDMMC_DetectionMode_Polling && context->DetectionPeriod == 0 )
        {
            context->DetectionPeriod = oC_s(1);
        }

        if( Config->Mode != oC_SDMMC_Mode_Auto )
        {
            for( uint8_t modeIdx = 0; modeIdx < oC_ARRAY_SIZE(ModeInterfaces) && context->Interface == NULL; modeIdx++)
            {
                context->Interface = ( ModeInterfaces[modeIdx]->Mode == Config->Mode ) ? ModeInterfaces[modeIdx] : NULL;
            }
        }

    }
    else
    {
        oC_SaveIfFalse("context"            , context       == NULL || kfree(context,0)                 , oC_ErrorCode_ReleaseError );
        oC_SaveIfFalse("busyMutex"          , busyMutex     == NULL || oC_Mutex_Delete(&busyMutex,0)    , oC_ErrorCode_ReleaseError );
        oC_SaveIfFalse("detectedCards"      , detectedCards == NULL || oC_List_Delete(detectedCards)   , oC_ErrorCode_ReleaseError );
        context = NULL;
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases SDMMC context memory
 */
//==========================================================================================================================================
static oC_ErrorCode_t DeleteContext( oC_SDMMC_Context_t * Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    (*Context)->ObjectControl = 0;

    errorCode = oC_ErrorCode_None;

    ErrorCode( oC_GPIO_QuickUnconfigure( (*Context)->DetectionPin) );
    ErrorCondition( oC_Mutex_Delete    ( &(*Context)->ModuleBusy        , 0 )  , oC_ErrorCode_ReleaseError );
    foreach( (*Context)->DetectedCards, detectedCard )
    {
        ErrorCondition( kfree(detectedCard, 0), oC_ErrorCode_ReleaseError );
    }
    ErrorCondition( oC_List_Delete( (*Context)->DetectedCards), oC_ErrorCode_ReleaseError );
    ErrorCondition( kfree( *Context, 0 ) , oC_ErrorCode_ReleaseError );

    *Context = NULL;

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Function checks if current configuration is supported and if current Mode is set to 'Auto',
 * then function will search all available modes for supported one
 */
//==========================================================================================================================================
static bool CheckConfigAndSetModeInterfaceIfNotSet( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context )
{
    bool isSupported = FALSE;

    if( Context->Interface != NULL )
    {
        if( oC_SDMMC_Mode_IsSupported(Config, Context) )
        {
            isSupported = TRUE;
        }
    }
    else
    {
        for( uint8_t modeIdx = 0; modeIdx < oC_ARRAY_SIZE(ModeInterfaces) && Context->Interface == NULL; modeIdx++)
        {
            Context->Interface = ModeInterfaces[modeIdx];

            if( oC_SDMMC_Mode_IsSupported(Config, Context) )
            {
                isSupported = TRUE;
            }
            else
            {
                Context->Interface = NULL;
            }
        }
    }

    return isSupported;
}

//==========================================================================================================================================
/**
 * @brief Gets Transfer Mode name from enumerator
 */
//==========================================================================================================================================
const char * GetTransferModeNameFromEnum( oC_SDMMC_TransferMode_t TransferMode )
{
    const char * name;

    if ( TransferMode < oC_ARRAY_SIZE(TransferModeNames) )
    {
        name = TransferModeNames[TransferMode];
    }
    else
    {
        name = "Unknown";
    }

    return name;
}

//==========================================================================================================================================
/**
 * @brief returns detection pin from the configuration
 */
//==========================================================================================================================================
static oC_Pin_t GetDetectionPin( const oC_SDMMC_Config_t * Config )
{
    oC_Pin_t pin = oC_Pin_NotUsed;

    switch(Config->TransferMode)
    {
        case oC_SDMMC_TransferMode_SPI:
            pin = Config->Pins[oC_SDMMC_PinIndex_SpiMode_nIRQ];
            break;
        case oC_SDMMC_TransferMode_1Bit:
            pin = Config->Pins[oC_SDMMC_PinIndex_1BitMode_nIRQ];
            break;
        case oC_SDMMC_TransferMode_4Bit:
            pin = Config->Pins[oC_SDMMC_PinIndex_4BitMode_nIRQ];
            break;
        default:
            driverlog(oC_LogType_Error, "Not handled transfer mode for detection pin");
            break;
    }

    return pin;
}

//==========================================================================================================================================
/**
 * @brief searches for a card information for the given DiskId
 */
//==========================================================================================================================================
static oC_SDMMC_CardInfo_t* GetCardInfo( oC_SDMMC_Context_t Context, oC_DiskId_t DiskId )
{
    oC_SDMMC_CardInfo_t* cardInfo = NULL;

    foreach(Context->DetectedCards, detectedCard)
    {
        if ( detectedCard->DiskId == DiskId )
        {
            cardInfo = detectedCard;
            break;
        }
    }

    return cardInfo;
}
#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
