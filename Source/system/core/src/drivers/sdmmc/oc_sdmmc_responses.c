/** ****************************************************************************************************************************************
 *
 * @brief      Definitions for SDIO commands
 *
 * @file       oc_sdmmc_cmd.c
 *
 * @author     Patryk Kubiak - (Created on: 05.07.2022 10:24:00)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define oC_SDMMC_PRIVATE

#include <oc_sdmmc_responses.h>
#include <oc_sdmmc_private.h>
#include <oc_endianess.h>

#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    oC_SDMMC_DRIVER_FILE_NAME
#define DRIVER_PRIVATE
#define DRIVER_DEBUGLOG     oC_SDMMC_DEBUGLOG_ENABLED
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Stores pointer to a function that should handle the response in generic way
 * @param Context       Context of the driver
 * @param Response      Pointer to the response to handle
 * @return code of error
 */
//==========================================================================================================================================
typedef oC_ErrorCode_t (*ResponseHandler_t)( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response );

//==========================================================================================================================================
/**
 * @brief stores response name
 */
//==========================================================================================================================================
typedef struct
{
    oC_SDMMC_ResponseIndex_t Index;    //!< Index of the response
    const char* Name;                  //!< Human-friendly name of the response
    ResponseHandler_t Handler;         //!< Pointer to a function that should handle the response in generic way
} ResponseData_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static const ResponseData_t*    GetResponseData( oC_SDMMC_ResponseIndex_t ResponseIndex );
static oC_ErrorCode_t           HandleR1       ( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response );
static oC_ErrorCode_t           HandleR2       ( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response );
static oC_ErrorCode_t           HandleR3       ( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response );
static oC_ErrorCode_t           HandleR6       ( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response );
static oC_ErrorCode_t           HandleR7       ( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________
//! @addtogroup SDMMC-Responses
//! @{

static const ResponseData_t ResponsesData[] = {
                                       { .Index = oC_SDMMC_ResponseIndex_R1 , "R1" , .Handler = HandleR1 },
                                       { .Index = oC_SDMMC_ResponseIndex_R1b, "R1b", .Handler = HandleR1 },
                                       { .Index = oC_SDMMC_ResponseIndex_R2 , "R2" , .Handler = HandleR2 },
                                       { .Index = oC_SDMMC_ResponseIndex_R3 , "R3" , .Handler = HandleR3 },
                                       { .Index = oC_SDMMC_ResponseIndex_R6 , "R6" , .Handler = HandleR6 },
                                       { .Index = oC_SDMMC_ResponseIndex_R7 , "R7" , .Handler = HandleR7 },
};

#undef  _________________________________________VARIABLES__________________________________________________________________________________
//!< @}

/** ========================================================================================================================================
 *
 *              The section with FUNCTIONS
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief reads response index from a string
 *
 * The function reads a response index from a string with human-friendly response name
 *
 * @param ResponseName          string with response name
 * @param outResponseIndex      destination for the read response index
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress           | Address of the `ResponseName` parameter is not correct
 *  #oC_ErrorCode_StringIsEmpty          | `ResponseName` string is empty
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outResponseIndex` parameter is not in RAM section
 *  #oC_ErrorCode_ResponseNotSupported   | The given `ResponseName` could not be recognized or is not supported
 *  #oC_ErrorCode_ResponseNotCorrect     | The given `ResponseName` is not valid (expected format: R%d)
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->InitializeCard
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Responses_ReadResponseIndex( const char* ResponseName, oC_SDMMC_ResponseIndex_t* outResponseIndex )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( isaddresscorrect(ResponseName)   , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outResponseIndex)          , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( strlen(ResponseName) > 0         , oC_ErrorCode_StringIsEmpty            )
        )
    {
        bool found = false;
        oC_ARRAY_FOREACH_IN_ARRAY(ResponsesData, responseData)
        {
            if ( strcmp(ResponseName,responseData->Name) == 0 )
            {
                *outResponseIndex = responseData->Index;
                found = true;
                break;
            }
        }
        if ( !found )
        {
            if (
                ErrorCondition( ResponseName[0] == 'R' && ResponseName[1] >= '0' && ResponseName[1] <= '9'
                                                            , oC_ErrorCode_ResponseNotCorrect )
             && ErrorCondition( ResponseName[2] == 0        , oC_ErrorCode_ResponseNotSupported )
                                )
            {
                *outResponseIndex = (oC_SDMMC_ResponseIndex_t)(ResponseName[1] - '0');
                errorCode = oC_ErrorCode_None;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads response name from the response index
 *
 * The function converts response index into a human-friendly name
 *
 * @param ResponseIndex         response index to convert
 * @param BufferLength          size of the buffer `outResponseName`
 * @param outResponseName       destination for the read name
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                       | Description
 * --------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                   | Operation success
 *  #oC_ErrorCode_ImplementError         | There was unexpected error in implementation
 *  #oC_ErrorCode_StringLengthNotCorrect | The given `BufferLength` is too small
 *  #oC_ErrorCode_OutputAddressNotInRAM  | Address of the `outResponseName` parameter is not in RAM section
 *  #oC_ErrorCode_ResponseNotSupported   | The given `ResponseIndex` is not supported
 *
 *  @note
 *  More error codes can be returned by function, which is called by pointer: Interface->InitializeCard
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Responses_ReadResponseName( oC_SDMMC_ResponseIndex_t ResponseIndex, size_t BufferLength, char outResponseName[BufferLength] )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
         ErrorCondition( ResponseIndex < oC_SDMMC_ResponseIndex_MAX         , oC_ErrorCode_ResponseNotSupported     )
      && ErrorCondition( BufferLength >= 2                                  , oC_ErrorCode_StringLengthNotCorrect   )
      && ErrorCondition( isram(outResponseName)                             , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        const ResponseData_t* responseData = GetResponseData(ResponseIndex);
        if ( responseData != NULL )
        {
            strncpy( outResponseName, responseData->Name, BufferLength );
            errorCode = strlen(responseData->Name) >= BufferLength ? oC_ErrorCode_StringLengthNotCorrect : oC_ErrorCode_None;
        }
        else
        {
            outResponseName[0] = 'R';
            outResponseName[1] = '0' + ResponseIndex;
            outResponseName[2] = 0;
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief verifies errors in the R1 response
 *
 * The function checks the R1 response to verify if there is any error available and translates it to the #oC_ErrorCode_t if error has been
 * found
 *
 * @param R1        response to verify
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress              | `R1` address is not valid
 *  #oC_ErrorCode_OutOfRange                | The commands argument was out of allowed range for this card.
 *  #oC_ErrorCode_AddressNotAligned         | A misaligned address, which did not match the block length was used in the command.
 *  #oC_ErrorCode_InvalidBlockLength        | The transferred block length is not allowed for this card or the number of transferred bytes does not match the block length
 *  #oC_ErrorCode_EraseSequenceError        | An error in the sequence of erase commands occurred.
 *  #oC_ErrorCode_EraseParameterNotCorrect  | An invalid selection, sectors or groups, for erase.
 *  #oC_ErrorCode_WritingNotPermitted       | The command tried to write a write protected block.
 *  #oC_ErrorCode_CardIsLocked              | When set, signals that the card is locked by the host.
 *  #oC_ErrorCode_LockUnlockFailed          | Set when a sequence or password error has been detected in lock/unlock card command or it there was an attempt to access a locked card.
 *  #oC_ErrorCode_CommunicationCrcError     | The CRC check of the previous command failed.
 *  #oC_ErrorCode_IllegalCommand            | Command not legal for the current state
 *  #oC_ErrorCode_CardEccFailed             | Card internal ECC was applied but the correction of data is failed
 *  #oC_ErrorCode_UnknownCardError          | A general or an unknown error occurred during the operation.
 *  #oC_ErrorCode_NotEfficientEnough        | The card could not sustain data transfer in stream read or write mode.
 *  #oC_ErrorCode_CIDCSDOverwrite           | CID/CSD Overwrite (please check `CIDCSDOverwrite` in #oC_SDMMC_Responses_R1_t for more details)
 *  #oC_ErrorCode_NotEverythingErased       | Only partial address space was erased due to existing WP blocks.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Responses_VerifyR1( const oC_SDMMC_Responses_R1_t* R1 )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( ErrorCondition( isaddresscorrect(R1), oC_ErrorCode_WrongAddress ) )
    {
        if (
               ErrorCondition( R1->OutOfRange           == 0       , oC_ErrorCode_OutOfRange                   )
            && ErrorCondition( R1->AddressError         == 0       , oC_ErrorCode_AddressNotAligned            )
            && ErrorCondition( R1->BlockLenError        == 0       , oC_ErrorCode_InvalidBlockLength           )
            && ErrorCondition( R1->EraseSeqError        == 0       , oC_ErrorCode_EraseSequenceError           )
            && ErrorCondition( R1->WpViolation          == 0       , oC_ErrorCode_WritingNotPermitted          )
            && ErrorCondition( R1->CardIsLocked         == 0       , oC_ErrorCode_CardIsLocked                 )
            && ErrorCondition( R1->LockUnlockFailed     == 0       , oC_ErrorCode_LockUnlockFailed             )
            && ErrorCondition( R1->ComCrcError          == 0       , oC_ErrorCode_CommunicationCrcError        )
            && ErrorCondition( R1->IllegalCommand       == 0       , oC_ErrorCode_IllegalCommand               )
            && ErrorCondition( R1->CardEccFailed        == 0       , oC_ErrorCode_CardEccFailed                )
            && ErrorCondition( R1->Error                == 0       , oC_ErrorCode_UnknownCardError             )
            && ErrorCondition( R1->Underrun             == 0       , oC_ErrorCode_NotEfficientEnough           )
            && ErrorCondition( R1->Overrun              == 0       , oC_ErrorCode_NotEfficientEnough           )
            && ErrorCondition( R1->CIDCSDOverwrite      == 0       , oC_ErrorCode_CIDCSDOverwrite              )
            && ErrorCondition( R1->WpEraseSkip          == 0       , oC_ErrorCode_NotEverythingErased          )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            driverlog( oC_LogType_Error, "Current Card State: %s, Error Flags: %s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
                       oC_SDMMC_GetCardStateName(R1->CurrentState),
                       R1->OutOfRange           == 0 ? "" : "OutOfRange" ,
                       R1->AddressError         == 0 ? "" : "AddressError" ,
                       R1->BlockLenError        == 0 ? "" : "BlockLenError" ,
                       R1->EraseSeqError        == 0 ? "" : "EraseSeqError" ,
                       R1->WpViolation          == 0 ? "" : "WpViolation" ,
                       R1->CardIsLocked         == 0 ? "" : "CardIsLocked" ,
                       R1->LockUnlockFailed     == 0 ? "" : "LockUnlockFailed" ,
                       R1->ComCrcError          == 0 ? "" : "ComCrcError" ,
                       R1->IllegalCommand       == 0 ? "" : "IllegalCommand" ,
                       R1->CardEccFailed        == 0 ? "" : "CardEccFailed" ,
                       R1->Error                == 0 ? "" : "Error" ,
                       R1->Underrun             == 0 ? "" : "Underrun" ,
                       R1->Overrun              == 0 ? "" : "Overrun" ,
                       R1->CIDCSDOverwrite      == 0 ? "" : "CIDCSDOverwrite" ,
                       R1->WpEraseSkip          == 0 ? "" : "WpEraseSkip"
                       );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles response in generic way
 *
 * The function is for handling of a command response in a generic way
 *
 * @param Context           context of the driver
 * @param Response          pointer to the response to handle
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ContextNotCorrect         | `Context` is not valid
 *  #oC_ErrorCode_WrongAddress              | The pointer `Response` stores invalid address
 *  #oC_ErrorCode_ResponseIndexNotCorrect   | The given `ResponseIndex` is not valid
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Responses_Handle( oC_SDMMC_Context_t Context, oC_SDMMC_ResponseIndex_t ResponseIndex, const oC_SDMMC_Response_t* Response )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( oC_SDMMC_IsContextCorrect(Context)                      , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isaddresscorrect(Response)                              , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( oC_SDMMC_Responses_IsResponseIndexValid(ResponseIndex)  , oC_ErrorCode_ResponseIndexNotCorrect  )
        )
    {
        const ResponseData_t* responseData = GetResponseData(ResponseIndex);
        if ( isaddresscorrect(responseData) && isaddresscorrect(responseData->Handler) )
        {
            errorCode = responseData->Handler( Context, Response );
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief prepares voltage window
 *
 * The function is for preparation of the voltage window according to the voltage range
 *
 * @param VoltageMin            minimum range for the voltage window
 * @param VoltageMax            maximum range for the voltage window
 * @param outVoltageWindow      destination for prepared voltage window
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_VoltageNotCorrect         | `VoltageMin` cannot be bigger than `VoltageMax`
 *  #oC_ErrorCode_VoltageNotSupported       | The given voltage is not in the supported range
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The parameter `outVoltageWindow` is not in RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Responses_PrepareVoltageWindow( float VoltageMin, float VoltageMax, oC_SDMMC_Responses_VoltageWindow_t* outVoltageWindow )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( VoltageMin <= VoltageMax              , oC_ErrorCode_VoltageNotCorrect      )
     && ErrorCondition( VoltageMin >= 2.7 && VoltageMin <= 3.6, oC_ErrorCode_VoltageNotSupported    )
     && ErrorCondition( VoltageMax >= 2.7 && VoltageMax <= 3.6, oC_ErrorCode_VoltageNotSupported    )
     && ErrorCondition( isram(outVoltageWindow)               , oC_ErrorCode_OutputAddressNotInRAM  )
        )
    {
        uint8_t voltageMinInt = (uint8_t)(VoltageMin * 10);
        uint8_t voltageMaxInt = (uint8_t)(VoltageMax * 10);

        uint8_t bitIndexMin = voltageMinInt - 27;
        uint8_t bitIndexMax = voltageMaxInt - 27;

        *outVoltageWindow = 0;

        if ( bitIndexMin > oC_SDMMC_Responses_VoltageWindow__MinBitIndex )
        {
            bitIndexMin--;
        }
        if ( bitIndexMax < oC_SDMMC_Responses_VoltageWindow__MaxBitIndex )
        {
            bitIndexMax++;
        }

        for ( uint8_t bitIndex = bitIndexMin; bitIndex <= bitIndexMax; bitIndex++ )
        {
            *outVoltageWindow |= 1<<bitIndex;
        }
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief swaps bytes in the response
 *
 * The function is for swapping of bytes in the response that is required for parsing some of the commands
 *
 * @param Response          Pointer to the response to swap
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The parameter `Response` is not in RAM section
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Responses_SwapBytes( oC_SDMMC_Response_t* Response )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( ErrorCondition(isram(Response), oC_ErrorCode_OutputAddressNotInRAM) )
    {
        for ( uint8_t wordIndex = 0; wordIndex < oC_ARRAY_SIZE(Response->LongResponse); wordIndex++ )
        {
            Response->LongResponse[wordIndex] = oC_Endianess_SwapU32( Response->LongResponse[wordIndex] );
        }
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Reads transfer speed from the response
 * @param TransferSpeed             Response with transfer speed
 * @param outTransferSpeed          Destination for the transfer speed
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                          | Description
 * -----------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress              | The parameter `TransferSpeed` is not valid pointer
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The parameter `outTransferSpeed` is not in RAM section
 *  #oC_ErrorCode_UnitNotSupported          | The parameter `TransferSpeed->Unit` is not supported by the function (it is too big)
 *  #oC_ErrorCode_ValueTooBig               | The parameter `TransferSpeed->Value` is not supported by the function (it is too big)
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_SDMMC_Responses_ReadTransferSpeed( const oC_SDMMC_Responses_TransferSpeed_t* TransferSpeed, oC_TransferSpeed_t* outTransferSpeed )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    static const oC_TransferSpeed_t speedUnits[] = {
                    oC_kBitsPerSecond(100),
                    oC_MBitsPerSecond(1),
                    oC_MBitsPerSecond(10),
                    oC_MBitsPerSecond(100),
                    oC_GBitsPerSecond(1),
                    oC_GBitsPerSecond(10),
                    oC_GBitsPerSecond(100)
    };
    static const double speedValues[] = {
                    0.0,
                    1.0,
                    1.2,
                    1.3,
                    1.5,
                    2.0,
                    2.5,
                    3.0,
                    3.5,
                    4.0,
                    4.5,
                    5.0,
                    5.5,
                    6.0,
                    7.0,
                    8.0
    };

    if (
        ErrorCondition( isaddresscorrect(TransferSpeed)                     , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outTransferSpeed)                             , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( TransferSpeed->Unit < oC_ARRAY_SIZE(speedUnits)     , oC_ErrorCode_UnitNotSupported         )
     && ErrorCondition( TransferSpeed->Value < oC_ARRAY_SIZE(speedValues)   , oC_ErrorCode_ValueTooBig              )
        )
    {
        *outTransferSpeed = (oC_TransferSpeed_t)(((double)speedUnits[TransferSpeed->Unit]) * speedValues[TransferSpeed->Value]);
        driverlog( oC_LogType_Track, "Reading of TransferSpeed from value %02X", TransferSpeed->Generic );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief searches for a response data with the given response index
 * @param ResponseIndex         response index to find a response data for
 * @return NULL if not found
 */
//==========================================================================================================================================
static const ResponseData_t*  GetResponseData( oC_SDMMC_ResponseIndex_t ResponseIndex )
{
    const ResponseData_t* responseData = NULL;

    oC_ARRAY_FOREACH_IN_ARRAY(ResponsesData, rd)
    {
        if ( rd->Index == ResponseIndex )
        {
            responseData = rd;
            break;
        }
    }

    return responseData;
}

//==========================================================================================================================================
/**
 * @brief verifies R1 response
 */
//==========================================================================================================================================
static oC_ErrorCode_t HandleR1( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response )
{
    return oC_SDMMC_Responses_VerifyR1(&Response->R1);
}

//==========================================================================================================================================
/**
 * @brief verifies R2 response
 */
//==========================================================================================================================================
static oC_ErrorCode_t HandleR2( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief verifies R3 response
 */
//==========================================================================================================================================
static oC_ErrorCode_t HandleR3( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief verifies R6 response
 */
//==========================================================================================================================================
static oC_ErrorCode_t HandleR6( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_SDMMC_Responses_R1_t r1 = {0};
    if (ErrorCondition( isaddresscorrect(Response), oC_ErrorCode_WrongAddress ) )
    {
        r1.CurrentState     = Response->R6.CurrentState;
        r1.ComCrcError      = Response->R6.ComCrcError;
        r1.IllegalCommand   = Response->R6.IllegalCommand;
        r1.Error            = Response->R6.Error;
        errorCode = oC_SDMMC_Responses_VerifyR1(&r1);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief verifies R7 response
 */
//==========================================================================================================================================
static oC_ErrorCode_t HandleR7( oC_SDMMC_Context_t Context, const oC_SDMMC_Response_t* Response )
{
    return oC_ErrorCode_None;
}

#undef  _________________________________________LOCAL_SECTION______________________________________________________________________________

