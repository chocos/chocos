/** ****************************************************************************************************************************************
 *
 * @file       oc_sdmmc_mode_lld.c
 *
 * @brief      contains implementation of LLD mode
 *
 * @author     Patryk Kubiak - (Created on: 3 10 2017 19:03:31)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/
#define oC_SDMMC_PRIVATE
#include <oc_sdmmc_mode_lld.h>
#include <oc_sdmmc_cmd.h>
#include <oc_sdmmc_commif.h>
#include <oc_module.h>
#include <oc_object.h>
#include <oc_semaphore.h>
#include <oc_intman.h>
#include <oc_memman.h>
#include <oc_struct.h>
#include <oc_math.h>
#include <oc_sys_lld.h>
#include <oc_struct.h>

#if defined(oC_SDMMC_LLD_AVAILABLE) || defined(DOXYGEN)

#define DRIVER_NAME         oC_SDMMC_DRIVER_NAME
#define DRIVER_FILE_NAME    "sdmmc-lld-mode"
#define DRIVER_PRIVATE
#define LOCAL_DEBUGLOG      false
#define DRIVER_DEBUGLOG     (oC_SDMMC_DEBUGLOG_ENABLED && LOCAL_DEBUGLOG)
#include <oc_driver.h>

#define CARD_DETECTION_TIMEOUT(Timeout)          oC_MIN( Timeout, ms(500) )
#define SET_VOLTAGE_TIMEOUT(Timeout)             oC_MIN( Timeout, ms(500) )

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________
//! @addtogroup SDMMC-LLD-Mode
//! @{

//==========================================================================================================================================
/**
 * @brief type for storing CardID from CID register
 */
//==========================================================================================================================================
typedef oC_SDMMC_LongResponseData_t CardID_t;

//==========================================================================================================================================
/**
 * @brief type for storing relative address of a card
 */
//==========================================================================================================================================
typedef oC_SDMMC_Mode_CommandArgument_t RelativeAddress_t;

//==========================================================================================================================================
/**
 * @brief stores context of the mode
 *
 * The type is for storing context of the mode (of channel configuration)
 */
//==========================================================================================================================================
struct ModeContext_t
{
    /** @brief Field for object content verification (calculated by function #oC_CountObjectControl ) */
    oC_ObjectControl_t                  ObjectControl;
    /** @brief The semaphore is ready to take if new data is ready to receive */
    oC_Semaphore_t                      DataReadyToReceiveSemaphore;
    /** @brief The semaphore is ready to take if a module is ready to send new data */
    oC_Semaphore_t                      ReadyToSendNewDataSemaphore;
    /** @brief The semaphore is ready to take if last command has been sent */
    oC_Semaphore_t                      CommandSentSemaphore;
    /** @brief The semaphore is ready to take if last command response has been received */
    oC_Semaphore_t                      CommandResponseReceivedSemaphore;
    /** @brief The semaphore is ready to take if the transmission is finished */
    oC_Semaphore_t                      TransmissionFinished;
    /** @brief The configuration of the LLD */
    oC_SDMMC_LLD_Config_t               LLDConfig;
    /** @brief The array with configured pins */
    oC_SDMMC_Pins_t                     Pins;
    /** @brief Configured transfer mode */
    oC_SDMMC_TransferMode_t             TransferMode;
    /** @brief Configured LLD channel */
    oC_SDMMC_Channel_t                  Channel;
    /** @brief Data package used for transfer data */
    oC_DataPackage_t                    DataPackage;
};

#undef  _________________________________________TYPES______________________________________________________________________________________
//!< @}

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_________________________________________________________________________________

static oC_ErrorCode_t   TurnOn              ( void );
static oC_ErrorCode_t   TurnOff             ( void );
static bool             IsSupported         ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context );
static oC_ErrorCode_t   Configure           ( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context );
static oC_ErrorCode_t   Unconfigure         ( oC_SDMMC_Context_t Context );
static oC_ErrorCode_t   SendCommand         ( oC_SDMMC_Context_t Context , oC_SDMMC_Mode_RawCommand_t * Command , oC_Time_t Timeout );
static oC_ErrorCode_t   ReadData            ( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, char * outBuffer, oC_MemorySize_t * Size , oC_Time_t Timeout );
static oC_ErrorCode_t   TestRead            ( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_Time_t Timeout );
static oC_ErrorCode_t   WriteData           ( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_SectorNumber_t StartSector, const char * Buffer, oC_MemorySize_t * Size , oC_Time_t Timeout ); // Writes data by using the given mode
static oC_ErrorCode_t   InitializeCard      ( oC_SDMMC_Context_t Context , oC_SDMMC_CardInfo_t *outCardInfo, oC_Time_t Timeout ); // Initializes the given card to work (after card detection)
static oC_ErrorCode_t   SetTransferMode     ( oC_SDMMC_Context_t Context , oC_SDMMC_TransferMode_t TransferMode );
static oC_ErrorCode_t   SendCommandHelper   ( struct ModeContext_t * Context, oC_SDMMC_Mode_CommandIndex_t Index, oC_SDMMC_Mode_CommandArgument_t Argument, oC_SDMMC_Response_t * outResponse, oC_Time_t Timeout );
static bool             FindFreeChannelAndTransferMode  ( const oC_SDMMC_Pins_t Pins, oC_SDMMC_TransferMode_t * TransferMode, oC_SDMMC_Channel_t * outChannel );
static bool             FindFreeChannel     ( const oC_SDMMC_Pins_t Pins, oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_Channel_t * outChannel );
static bool             IsChannelConfigPossible( const oC_SDMMC_Pins_t Pins, oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_Channel_t Channel );
static oC_ErrorCode_t   PrepareLLDConfig    ( const oC_SDMMC_Config_t * Config, oC_SDMMC_Channel_t Channel, oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_LLD_Config_t * outConfig );
static bool             TransferModeToWideBus   ( oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_LLD_WideBus_t * outWideBus );
static bool             IsTransferModeCorrect   ( oC_SDMMC_TransferMode_t TransferMode );
static bool             IsModeContextCorrect    ( struct ModeContext_t * Context );
static oC_ErrorCode_t   ConfigurePins       ( const oC_SDMMC_Pins_t Pins , oC_SDMMC_Channel_t Channel, oC_SDMMC_TransferMode_t Mode );
static oC_ErrorCode_t   UnconfigurePins     ( const oC_SDMMC_Pins_t Pins , oC_SDMMC_TransferMode_t Mode );
static void             InterruptHandler    ( oC_SDMMC_Channel_t Channel , oC_SDMMC_LLD_InterruptSource_t InterruptSource );

#undef  _________________________________________PROTOTYPES_________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________
//! @addtogroup SDMMC-LLD-Mode
//! @{

//==========================================================================================================================================
/**
 * @brief array with contexts for each channel of SDMMC
 */
//==========================================================================================================================================
static oC_SDMMC_Context_t Contexts[ oC_ModuleChannel_NumberOfElements(SDMMC) ] = { 0 };

//==========================================================================================================================================
/**
 * @brief Allocator definition for local allocations
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
   .Name = "sdmmc-lld-mode"
};

//==========================================================================================================================================
/**
 * @brief array with functions for the SDMMC pins
 */
//==========================================================================================================================================
static const oC_SDMMC_PinFunction_t PinFunctions[ oC_SDMMC_TransferMode_NumberOfElements ][ oC_SDMMC_PinIndex_NumberOfPins ] =
{
    [ oC_SDMMC_TransferMode_1Bit ] = {
           [ oC_SDMMC_PinIndex_1BitMode_CD   ] = 0 ,
           [ oC_SDMMC_PinIndex_1BitMode_CMD  ] = oC_SDMMC_PinFunction_SDMMC_CMD ,
           [ oC_SDMMC_PinIndex_1BitMode_CLK  ] = oC_SDMMC_PinFunction_SDMMC_CK ,
           [ oC_SDMMC_PinIndex_1BitMode_DAT0 ] = oC_SDMMC_PinFunction_SDMMC_D0 ,
           [ oC_SDMMC_PinIndex_1BitMode_nIRQ ] = 0 ,
    } ,
    [ oC_SDMMC_TransferMode_4Bit ] = {
           [ oC_SDMMC_PinIndex_4BitMode_DAT3 ] = oC_SDMMC_PinFunction_SDMMC_D3 ,
           [ oC_SDMMC_PinIndex_4BitMode_CMD  ] = oC_SDMMC_PinFunction_SDMMC_CMD ,
           [ oC_SDMMC_PinIndex_4BitMode_CLK  ] = oC_SDMMC_PinFunction_SDMMC_CK ,
           [ oC_SDMMC_PinIndex_4BitMode_DAT0 ] = oC_SDMMC_PinFunction_SDMMC_D0 ,
           [ oC_SDMMC_PinIndex_4BitMode_nIRQ ] = 0 ,
           [ oC_SDMMC_PinIndex_4BitMode_DAT1 ] = oC_SDMMC_PinFunction_SDMMC_D1 ,
           [ oC_SDMMC_PinIndex_4BitMode_DAT2 ] = oC_SDMMC_PinFunction_SDMMC_D2 ,
    } ,
    [ oC_SDMMC_TransferMode_8Bit ] = {
           [ oC_SDMMC_PinIndex_8BitMode_DAT3 ] = oC_SDMMC_PinFunction_SDMMC_D3 ,
           [ oC_SDMMC_PinIndex_8BitMode_CMD  ] = oC_SDMMC_PinFunction_SDMMC_CMD ,
           [ oC_SDMMC_PinIndex_8BitMode_CLK  ] = oC_SDMMC_PinFunction_SDMMC_CK ,
           [ oC_SDMMC_PinIndex_8BitMode_DAT0 ] = oC_SDMMC_PinFunction_SDMMC_D0 ,
           [ oC_SDMMC_PinIndex_8BitMode_DAT1 ] = oC_SDMMC_PinFunction_SDMMC_D1 ,
           [ oC_SDMMC_PinIndex_8BitMode_DAT2 ] = oC_SDMMC_PinFunction_SDMMC_D2 ,
           [ oC_SDMMC_PinIndex_8BitMode_DAT4 ] = oC_SDMMC_PinFunction_SDMMC_D4 ,
           [ oC_SDMMC_PinIndex_8BitMode_DAT5 ] = oC_SDMMC_PinFunction_SDMMC_D5 ,
           [ oC_SDMMC_PinIndex_8BitMode_DAT6 ] = oC_SDMMC_PinFunction_SDMMC_D6 ,
           [ oC_SDMMC_PinIndex_8BitMode_DAT7 ] = oC_SDMMC_PinFunction_SDMMC_D7 ,
    } ,
};

//==========================================================================================================================================
/**
 * Interface for SDMMC LLD Mode
 */
//==========================================================================================================================================
const oC_SDMMC_Mode_Interface_t oC_SDMMC_Mode_Interface_LLD = {
    .Mode                   = oC_SDMMC_Mode_LLD,
    .ModeName               = "lld",
    .TurnOn                 = TurnOn ,
    .TurnOff                = TurnOff ,
    .IsSupported            = IsSupported ,
    .Configure              = Configure ,
    .Unconfigure            = Unconfigure,
    .SendCommand            = SendCommand,
    .ReadData               = ReadData,
    .WriteData              = WriteData,
    .InitializeCard         = InitializeCard,
    .SetTransferMode        = SetTransferMode
};

#undef  _________________________________________VARIABLES__________________________________________________________________________________
//!< @}

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS__________________________________________________________________________________
//! @addtogroup SDMMC-LLD-Mode
//! @{

//==========================================================================================================================================
/**
 * @brief turns on SDMMC-LLD mode
 *
 * The function initializes SDMMC-LLD mode to work. The function also enables SDMMC-LLD module and initializes all global variables with
 * default values.
 *
 * @return
 * #oC_ErrorCode_None if success, otherwise it returns one of following error codes:
 *
 *  ErrorCode                                 | Description
 * -------------------------------------------|---------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError              | Unexpected behavior - developer forgot about one of the paths
 *  #oC_ErrorCode_ModuleIsTurnedOn            | The SDMMC-LLD mode or SDMMC-LLD module has been enabled before
 *  #oC_ErrorCode_ModuleNotStartedYet         | SDMMC-LLD does not handle correctly module enabling
 *  #oC_ErrorCode_WrongEventHandlerAddress    | SDMMC-LLD incorrectly checks if the event handler is set
 *  #oC_ErrorCode_InterruptHandlerAlreadySet  | SDMMC-LLD does not set #NULL to interrupt handler during startup
 *  #oC_ErrorCode_UnknownInterruptSource      | At least one of the interrupt source flags is unknown on the target
 *  #oC_ErrorCode_NotSupportedOnTargetMachine | At least one of the required interrupts is not supported on the target
 */
//==========================================================================================================================================
static oC_ErrorCode_t TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOffVerification( &errorCode, oC_Module_SDMMC_LLD_Mode ) )
    {
        oC_SDMMC_LLD_InterruptSource_t enabledSources = oC_SDMMC_LLD_InterruptSource_CommandResponseReceived
                                                      | oC_SDMMC_LLD_InterruptSource_CommandSent
                                                      | oC_SDMMC_LLD_InterruptSource_CommandTransmissionError
                                                      | oC_SDMMC_LLD_InterruptSource_DataReadyToReceive
                                                      | oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData
                                                      | oC_SDMMC_LLD_InterruptSource_TransmissionError
                                                      | oC_SDMMC_LLD_InterruptSource_TransmissionRestarted;

        if(
            ErrorCode( oC_SDMMC_LLD_TurnOnDriver() )
         && ErrorCode( oC_SDMMC_LLD_SetInterruptHandler( InterruptHandler, enabledSources ) )
            )
        {
            oC_ARRAY_FOREACH_IN_ARRAY( Contexts, context )
            {
                *context = NULL;
            }

            oC_Module_TurnOn( oC_Module_SDMMC_LLD_Mode );
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off SDMMC-LLD-Mode
 *
 * The function turns off `SDMMC-LLD` mode and `SDMMC-LLD` module. It also unconfigures all channels and releases all memory allocated with
 * the local allocator.
 *
 * @return
 * #oC_ErrorCode_None if success, otherwise it returns one of following error codes:
 *
 *  ErrorCode                                 | Description
 * -------------------------------------------|---------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError              | Unexpected behavior - developer forgot about one of the paths
 *  #oC_ErrorCode_ModuleNotStartedYet         | The has not been enabled before or `SDMMC-LLD` has been disabled by manual
 */
//==========================================================================================================================================
static oC_ErrorCode_t TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_SDMMC_LLD_Mode ) )
    {
        oC_Module_TurnOff( oC_Module_SDMMC_LLD_Mode );

        oC_SDMMC_LLD_ForEachChannel( channel )
        {
            oC_SDMMC_LLD_ChannelIndex_t channelIndex = oC_SDMMC_LLD_ChannelToChannelIndex( channel );
            if( Contexts[ channelIndex ] )
            {
                if( ErrorCode( Unconfigure( Contexts[ channelIndex ] ) ) )
                {
                    driverlog( oC_LogType_Error, "%s: unconfiguration error: %R", oC_Channel_GetName(channel), errorCode );
                }
                Contexts[ channelIndex ] = NULL;
            }
        }

        if( ErrorCode( oC_SDMMC_LLD_TurnOffDriver() ) )
        {
            oC_MemMan_FreeAllMemoryOfAllocator( &Allocator );
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            driverlog( oC_LogType_Error, "Cannot disable SDMMC-LLD module: %R", errorCode );
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Checks if the given configuration is possible to configure in the given mode
 *
 * The function is for checking if the given configuration can use `SDMMC LLD` mode to work.
 *
 * @param Config        Pointer to the configuration of `SDMMC`
 * @param Context       Context of the SDMMC driver
 *
 * @return true if configuration is supported by the mode
 */
//==========================================================================================================================================
static bool IsSupported( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context )
{
    bool isSupported = false;

    if( oC_Module_IsTurnedOn( oC_Module_SDMMC_LLD_Mode ) )
    {
        oC_IntMan_EnterCriticalSection();

        if( isaddresscorrect( Config ) )
        {
            if( Config->Mode == oC_SDMMC_Mode_LLD || Config->Mode == oC_SDMMC_Mode_Auto )
            {
                if( Config->TransferMode < oC_SDMMC_TransferMode_NumberOfElements && Config->TransferMode != oC_SDMMC_TransferMode_SPI )
                {
                    oC_SDMMC_Channel_t channel = Config->Advanced.Channel;
                    oC_SDMMC_TransferMode_t transferMode = Config->TransferMode;

                    if( FindFreeChannelAndTransferMode( Config->Pins, &transferMode, &channel ) )
                    {
                        oC_SDMMC_LLD_Config_t lldConfig;
                        oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

                        if( ErrorCode( PrepareLLDConfig( Config, channel, transferMode, &lldConfig ) ) )
                        {
                            driverlog( oC_LogType_Track, "Configuration supported on channel %s", oC_Channel_GetName(channel) );
                            isSupported = true;
                        }
                        else
                        {
                            driverlog( oC_LogType_Error, "Cannot prepare LLD configuration: %R", errorCode );
                        }
                    }
                    else
                    {
                        driverlog( oC_LogType_Info, "Cannot find free channel for the configuration" );
                    }
                }
                else
                {
                    driverlog( oC_LogType_Error, "Transfer mode is not correct: %d", Config->TransferMode );
                }
            }
            else
            {
                driverlog( oC_LogType_Info, "Not supported mode in configuration: %d", Config->Mode );
            }
        }
        else
        {
            driverlog( oC_LogType_Error, "Address of configuration is not correct" );
        }

        oC_IntMan_ExitCriticalSection();
    }
    else
    {
        driverlog( oC_LogType_Info, "Configuration not possible - module is disabled" );
    }

    return isSupported;
}

//==========================================================================================================================================
/**
 * @brief Configures the given mode in the given configuration
 *
 * The function allows for configuration of the SDMMC to work with LLD (in LLD mode).
 *
 * @param Config        Pointer to SDMMC driver configuration
 * @param Context       allocated SDMMC context
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_WrongAddress              | Configuration address is not correct
 *  #oC_ErrorCode_ContextNotCorrect         | The given context is not correct
 *  #oC_ErrorCode_AllocationError           | Cannot allocate memory for a context
 *  #oC_ErrorCode_ModeNotSupported          | Transfer mode is not correct or not supported by the LLD
 *  #oC_ErrorCode_WrongChannel              | The given SDMMC channel is not 0 nor correct
 *  #oC_ErrorCode_ChannelIsUsed             | The given SDMMC channel is already configured
 *  #oC_ErrorCode_NoChannelAvailable        | Cannot find free SDMMC channel
 *  #oC_ErrorCode_SizeNotCorrect            | Sector size in configuration is not correct
 *
 *  More error codes can be returned by #oC_SDMMC_LLD_Configure and #oC_SDMMC_LLD_ConfigurePin
 */
//==========================================================================================================================================
static oC_ErrorCode_t Configure( const oC_SDMMC_Config_t * Config , oC_SDMMC_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_SDMMC_LLD_Mode ) )
    {
        if(
            ErrorCondition( isaddresscorrect( Config )                      , oC_ErrorCode_WrongAddress         )
         && ErrorCondition( oC_SDMMC_IsContextCorrect( Context )            , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( IsTransferModeCorrect( Config->TransferMode )   , oC_ErrorCode_ModeNotCorrect       )
            )
        {
            struct ModeContext_t * modeContext = kmalloc( sizeof( struct ModeContext_t ), &Allocator, AllocationFlags_ZeroFill );

            if( ErrorCondition( modeContext != NULL, oC_ErrorCode_AllocationError ) )
            {
                modeContext->CommandResponseReceivedSemaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary, 0, &Allocator, 0 );
                modeContext->CommandSentSemaphore             = oC_Semaphore_New( oC_Semaphore_Type_Binary, 1, &Allocator, 0 );
                modeContext->DataReadyToReceiveSemaphore      = oC_Semaphore_New( oC_Semaphore_Type_Binary, 0, &Allocator, 0 );
                modeContext->ReadyToSendNewDataSemaphore      = oC_Semaphore_New( oC_Semaphore_Type_Binary, 1, &Allocator, 0 );
                modeContext->TransmissionFinished             = oC_Semaphore_New( oC_Semaphore_Type_Binary, 0, &Allocator, 0 );

                if(
                    ErrorCondition( modeContext->CommandResponseReceivedSemaphore != NULL, oC_ErrorCode_AllocationError )
                 && ErrorCondition( modeContext->CommandSentSemaphore             != NULL, oC_ErrorCode_AllocationError )
                 && ErrorCondition( modeContext->DataReadyToReceiveSemaphore      != NULL, oC_ErrorCode_AllocationError )
                 && ErrorCondition( modeContext->ReadyToSendNewDataSemaphore      != NULL, oC_ErrorCode_AllocationError )
                 && ErrorCondition( modeContext->TransmissionFinished             != NULL, oC_ErrorCode_AllocationError )
                    )
                {
                    oC_SDMMC_TransferMode_t     transferMode = Config->TransferMode;
                    oC_SDMMC_Channel_t          channel      = Config->Advanced.Channel;
                    oC_SDMMC_LLD_ChannelIndex_t channelIndex = oC_SDMMC_LLD_ChannelToChannelIndex( channel );
                    if(
                        ErrorCondition( channel == 0 || oC_SDMMC_LLD_IsChannelCorrect(channel)                  , oC_ErrorCode_WrongChannel         )
                     && ErrorCondition( channel == 0 || Contexts[ channelIndex ] == NULL                        , oC_ErrorCode_ChannelIsUsed        )
                     && ErrorCondition( FindFreeChannelAndTransferMode( Config->Pins, &transferMode, &channel)  , oC_ErrorCode_NoChannelAvailable   )
                     && ErrorCode( PrepareLLDConfig( Config, channel, transferMode, &modeContext->LLDConfig )   )
                     && ErrorCode( oC_SDMMC_LLD_Configure( &modeContext->LLDConfig )                            )
                     && ErrorCode( ConfigurePins( Config->Pins, channel, transferMode )                         )
                        )
                    {
                        // We have to recalculate channel index after finding a free channel
                        channelIndex = oC_SDMMC_LLD_ChannelToChannelIndex( channel );

                        memcpy( modeContext->Pins, Config->Pins, sizeof(oC_SDMMC_Pins_t) );
                        Contexts[channelIndex]      = Context;
                        modeContext->ObjectControl  = oC_CountObjectControl( modeContext, oC_ObjectId_SDMMCModeLLD );
                        modeContext->TransferMode   = transferMode;
                        modeContext->Channel        = channel;
                        Context->ModeContext        = modeContext;
                        errorCode                   = oC_ErrorCode_None;
                    }
                }
                if( oC_ErrorOccur( errorCode ) )
                {
                    oC_Semaphore_Delete( &modeContext->CommandResponseReceivedSemaphore , 0 );
                    oC_Semaphore_Delete( &modeContext->CommandSentSemaphore             , 0 );
                    oC_Semaphore_Delete( &modeContext->DataReadyToReceiveSemaphore      , 0 );
                    oC_Semaphore_Delete( &modeContext->ReadyToSendNewDataSemaphore      , 0 );
                    oC_Semaphore_Delete( &modeContext->TransmissionFinished             , 0 );

                    kfree( modeContext, 0 );
                    Context->ModeContext = NULL;
                }
            }
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the given configuration
 *
 * The function is for unconfiguration of SDMMC LLD mode. It restores default state on the configured channel, releases memory of the LLD context
 * and unconfigures all configured pins.
 *
 * @param Context       correct SDMMC context
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_ContextNotCorrect         | The given context is not correct
 *  #oC_ErrorCode_ReleaseError              | There was a problem with releasing memory of context
 *
 * More error codes can be returned by #oC_SDMMC_LLD_UnconfigurePin and #oC_SDMMC_LLD_Unconfigure
 */
//==========================================================================================================================================
static oC_ErrorCode_t Unconfigure( oC_SDMMC_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_SDMMC_LLD_Mode ) )
    {
        if(
            ErrorCondition( oC_SDMMC_IsContextCorrect( Context )        , oC_ErrorCode_ContextNotCorrect )
         && ErrorCondition( IsModeContextCorrect( Context->ModeContext ), oC_ErrorCode_ContextNotCorrect )
            )
        {
            errorCode = oC_ErrorCode_None;

            Context->ModeContext->ObjectControl = 0;
            Contexts[ oC_SDMMC_LLD_ChannelToChannelIndex( Context->ModeContext->Channel ) ] = NULL;

            ErrorCode( UnconfigurePins( Context->ModeContext->Pins, Context->ModeContext->TransferMode ) );
            ErrorCode( oC_SDMMC_LLD_Unconfigure( Context->ModeContext->Channel ) );

            ErrorCondition( oC_Semaphore_Delete( &Context->ModeContext->CommandResponseReceivedSemaphore, 0 ), oC_ErrorCode_ReleaseError );
            ErrorCondition( oC_Semaphore_Delete( &Context->ModeContext->CommandSentSemaphore            , 0 ), oC_ErrorCode_ReleaseError );
            ErrorCondition( oC_Semaphore_Delete( &Context->ModeContext->DataReadyToReceiveSemaphore     , 0 ), oC_ErrorCode_ReleaseError );
            ErrorCondition( oC_Semaphore_Delete( &Context->ModeContext->ReadyToSendNewDataSemaphore     , 0 ), oC_ErrorCode_ReleaseError );
            ErrorCondition( oC_Semaphore_Delete( &Context->ModeContext->TransmissionFinished            , 0 ), oC_ErrorCode_ReleaseError );

            ErrorCondition( kfree( Context->ModeContext, 0 ), oC_ErrorCode_AllocationError );

            Context->ModeContext = NULL;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Sends the SD/MMC command
 *
 * The function is responsible for sending a command to the SD/MMC card.
 *
 * @param Context       Pointer to the context of a driver
 * @param Command       Command data to send
 * @param Timeout       Maximum time for operation
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_ContextNotCorrect         | The given context is not correct
 *  #oC_ErrorCode_AddressNotInRam           | The given `Command` address is not in RAM
 *  #oC_ErrorCode_TimeNotCorrect            | The given `Timeout` is below 0
 *  #oC_ErrorCode_Timeout                   | Maximum time for the operation has expired
 *
 * More error codes can be returned by #SendCommandHelper
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendCommand( oC_SDMMC_Context_t Context , oC_SDMMC_Mode_RawCommand_t * Command , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_SDMMC_LLD_Mode ) )
    {
        if(
            ErrorCondition( oC_SDMMC_IsContextCorrect( Context )            , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( IsModeContextCorrect( Context->ModeContext )    , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( isram(Command)                                  , oC_ErrorCode_AddressNotInRam      )
         && ErrorCondition( Timeout >= 0                                    , oC_ErrorCode_TimeNotCorrect       )
            )
        {
            if( ErrorCode( SendCommandHelper( Context->ModeContext, Command->CommandIndex, Command->CommandArgument, &Command->Response, Timeout ) ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Reads data by using the given mode
 *
 * The function is for reading data by using the LLD. It does not send required commands to read data, it only uses the LLD module to read
 * data from.
 *
 * @param Context           Pointer to the context of the SDMMC driver
 * @param CardInfo          Information about the card read during the initialization
 * @param StartSector       Index of the starting sector to read
 * @param outBuffer         Destination buffer for read data
 * @param Size              Reference to the variable with size. On input should store size of the buffer, on output it is number of read bytes.
 * @note If address of `Size` is not correct, it is not filled by the function, but in any other case, you can expect that the `Size` will be filled on output (even if error has occurred)
 * @param Timeout           Maximum time for operation
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_ContextNotCorrect         | The given `Context` structure is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The given `outBuffer` address does not point to the RAM section
 *  #oC_ErrorCode_AddressNotInRam           | The given `Size` address does not point to the RAM section
 *  #oC_ErrorCode_SizeNotCorrect            | `Size` is 0
 *  #oC_ErrorCode_TimeNotCorrect            | The given `Timeout` is below 0
 *  #oC_ErrorCode_Timeout                   | Maximum time for operation has expired
 *
 * More error codes can be returned by #oC_SDMMC_LLD_ReadDataPackage
 *
 * @see oC_SDMMC_LLD_ReadDataPackage, WriteData
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadData( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SectorNumber_t StartSector, char * outBuffer, oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_SDMMC_LLD_Mode ) )
    {
        if(
            ErrorCondition( oC_SDMMC_IsContextCorrect(Context)          , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( IsModeContextCorrect(Context->ModeContext)  , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( isram(outBuffer)                            , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( isram(Size)                                 , oC_ErrorCode_AddressNotInRam          )
         && ErrorCondition( (*Size) > 0                                 , oC_ErrorCode_SizeNotCorrect           )
         && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect           )
            )
        {
            oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
            struct ModeContext_t * modeContext  = Context->ModeContext;
            oC_Struct_Define(oC_SDMMC_LLD_TransferConfig_t, transferConfig);

            transferConfig.Timeout = min(1);
            transferConfig.BlockSize = Context->SectorSize;
            transferConfig.DataSize  = *Size;
            transferConfig.TransferDirection = oC_SDMMC_LLD_TransferDirection_Read;
            transferConfig.TransferMode = oC_SDMMC_LLD_TransferMode_BlockDataTransfer;

            if (
                ErrorCode( oC_SDMMC_LLD_PrepareForTransfer(modeContext->Channel, &transferConfig)                           )
             && ErrorCode( oC_SDMMC_CommIf_StartReadingSectors(Context, CardInfo, StartSector, gettimeout(endTimestamp))    )
                )
            {
                oC_DataPackage_Initialize( &modeContext->DataPackage, outBuffer, *Size, 0, false );

                while(
                    !oC_DataPackage_IsFull(&modeContext->DataPackage)
                 && ErrorCondition( oC_Semaphore_Take( modeContext->DataReadyToReceiveSemaphore, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout )
                 && ErrorCodeIgnore( oC_SDMMC_LLD_ReadDataPackage( modeContext->Channel, &modeContext->DataPackage ), oC_ErrorCode_NoDataToReceive )
                    )
                {
                    errorCode = oC_ErrorCode_None;
                }

                *Size -= oC_DataPackage_GetLeftSize(&modeContext->DataPackage);

                if ( oC_ErrorOccur(errorCode) )
                {
                    oC_SaveIfErrorOccur( "Cannot finish reading sectors", oC_SDMMC_CommIf_FinishReadingSectors(Context, CardInfo, oC_MAX(gettimeout(endTimestamp), ms(100))) );
                }
                else if ( ErrorCondition( oC_Semaphore_Take( modeContext->TransmissionFinished, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout ) )
                {
                    errorCode = oC_SDMMC_CommIf_FinishReadingSectors(Context, CardInfo, gettimeout(endTimestamp));
                }
                else
                {
                    driverlog(oC_LogType_Error, "Timeout during waiting for transmission to end\n");
                    oC_SaveIfErrorOccur("Finishing reading of sectors", oC_SDMMC_CommIf_FinishReadingSectors(Context, CardInfo, gettimeout(endTimestamp)));
                }
            }
        }
        else if( isram(Size) )
        {
            Size = 0;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Performs test read
 *
 * The function performs a test read of the first sector to confirm the communication is working properly.
 *
 * @param Context           Context of the driver
 * @param CardInfo          Card information
 * @param Timeout           Maximum time for the operation
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_ContextNotCorrect         | The given `Context` structure is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The given `outBuffer` address does not point to the RAM section
 *  #oC_ErrorCode_AddressNotInRam           | The given `Size` address does not point to the RAM section
 *  #oC_ErrorCode_SizeNotCorrect            | `Size` is 0
 *  #oC_ErrorCode_TimeNotCorrect            | The given `Timeout` is below 0
 *  #oC_ErrorCode_Timeout                   | Maximum time for operation has expired
 *
 * More error codes can be returned by #ReadData
 *
 * @see oC_SDMMC_LLD_ReadDataPackage, WriteData
 */
//==========================================================================================================================================
static oC_ErrorCode_t TestRead( oC_SDMMC_Context_t Context , const oC_SDMMC_CardInfo_t* CardInfo , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_MemorySize_t size = CardInfo->SectorSize;
    char* buffer = kmalloc( size, &Allocator, AllocationFlags_ZeroFill );
    if (
        ErrorCondition( buffer != NULL, oC_ErrorCode_AllocationError )
     && ErrorCode( ReadData(Context, CardInfo, 0, buffer, &size, Timeout) )
        )
    {
        driverlog( oC_LogType_GoodNews, "Test read from the card 0x%04X has finished successfully: 0x%02X 0x%02X", CardInfo->RelativeCardAddress, buffer[0], buffer[1] );
        kfree(buffer, 0);
        errorCode = oC_ErrorCode_None;
    }
    else if( buffer != NULL )
    {
        oC_SaveIfFalse( "Cannot release segment buffer", kfree(buffer, 0), oC_ErrorCode_ReleaseError );
        driverlog( oC_LogType_Error, "Test read has failed: %R\n", errorCode );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Writes data by using the given mode
 *
 * The function is for writing data with the LLD. It does not send command to the SD/MMC, but it only writes data into the LLD module.
 *
 * @param Context           Structure with context of the driver
 * @param CardInfo          Information about the card read during the initialization
 * @param StartSector       Index of the starting sector to write
 * @param Buffer            Pointer to the buffer to write
 * @param Size              Reference to the variable with size. On input should store size of the buffer, on output it is number of written bytes.
 * @note If address of `Size` is not correct, it is not filled by the function, but in any other case, you can expect that the `Size` will be filled on output (even if error has occurred)
 * @param Timeout           Maximum time for operation
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_ContextNotCorrect         | The given `Context` structure is not correct
 *  #oC_ErrorCode_WrongAddress              | The given `Buffer` address is not correct
 *  #oC_ErrorCode_AddressNotInRam           | The given `Size` address does not point to the RAM section
 *  #oC_ErrorCode_SizeNotCorrect            | `Size` is 0
 *  #oC_ErrorCode_TimeNotCorrect            | The given `Timeout` is below 0
 *  #oC_ErrorCode_Timeout                   | Maximum time for operation has expired
 *
 * More error codes can be returned by #oC_SDMMC_LLD_WriteDataPackage
 *
 * @see oC_SDMMC_LLD_WriteDataPackage, ReadData
 */
//==========================================================================================================================================
static oC_ErrorCode_t WriteData( oC_SDMMC_Context_t Context, const oC_SDMMC_CardInfo_t* CardInfo, oC_SectorNumber_t StartSector, const char * Buffer, oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_SDMMC_LLD_Mode ) )
    {
        if(
            ErrorCondition( oC_SDMMC_IsContextCorrect(Context)          , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( IsModeContextCorrect(Context->ModeContext)  , oC_ErrorCode_ContextNotCorrect    )
         && ErrorCondition( isaddresscorrect(Buffer)                    , oC_ErrorCode_WrongAddress         )
         && ErrorCondition( isram(Size)                                 , oC_ErrorCode_AddressNotInRam      )
         && ErrorCondition( (*Size) > 0                                 , oC_ErrorCode_SizeNotCorrect       )
         && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect       )
            )
        {
            oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
            struct ModeContext_t * modeContext  = Context->ModeContext;
            oC_Struct_Define(oC_SDMMC_LLD_TransferConfig_t, transferConfig);

            transferConfig.Timeout = min(1);
            transferConfig.BlockSize = Context->SectorSize;
            transferConfig.DataSize  = *Size;
            transferConfig.TransferDirection = oC_SDMMC_LLD_TransferDirection_Write;
            transferConfig.TransferMode = oC_SDMMC_LLD_TransferMode_BlockDataTransfer;

            if (
                ErrorCode( oC_SDMMC_LLD_PrepareForTransfer(modeContext->Channel, &transferConfig)                           )
             && ErrorCode( oC_SDMMC_CommIf_StartWritingSectors(Context, CardInfo, StartSector, gettimeout(endTimestamp))    )
                )
            {
                oC_DataPackage_t dataPackage;

                oC_DataPackage_Initialize( &dataPackage, Buffer, *Size, *Size, true );

//                while(
//                    !oC_DataPackage_IsEmpty(&dataPackage)
//                 && ErrorCondition( oC_Semaphore_Take( modeContext->ReadyToSendNewDataSemaphore, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout )
//                 && ErrorCode( oC_SDMMC_LLD_WriteDataPackage( modeContext->Channel, &dataPackage ) )
//                    )
//                {
//                    errorCode = oC_ErrorCode_None;
//                }
                if (
                    ErrorCondition( oC_Semaphore_Take( modeContext->ReadyToSendNewDataSemaphore, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout )
                 && ErrorCode( oC_SDMMC_LLD_WriteDataPackage( modeContext->Channel, &dataPackage ) )
                 && ErrorCondition( oC_Semaphore_Take( modeContext->TransmissionFinished, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout )
                 )
                {
                    errorCode = oC_SDMMC_CommIf_FinishWritingSectors(Context, CardInfo, gettimeout(endTimestamp));
                }
                else
                {
                   oC_SaveIfErrorOccur( "Cannot finish writing sectors", oC_SDMMC_CommIf_FinishWritingSectors(Context, CardInfo, oC_MAX(gettimeout(endTimestamp), ms(100))) );
                }
                *Size -= oC_DataPackage_GetLeftSize(&dataPackage);
            }
        }
        else if( isram(Size) )
        {
            Size = 0;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Initializes the given card to work (after card detection)
 *
 * The function is responsible for initialization of SD/MMC cards to work. It should be called after card detection. The flow of the initialization
 * is as follows:
 *
 * ![SDMMC Initialization flow](SD-MMC-Init-Flow.png)
 *
 * The card type is detected by accessing registers that are or are not available in the given SD/MMC version. When the card type is detected,
 * the card-type-specific initialization is called.
 *
 * @param Context       Pointer to the context of a driver
 * @param Timeout       Maximum time for operation
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_ContextNotCorrect         | The given `Context` structure is not correct
 *  #oC_ErrorCode_TimeNotCorrect            | The given `Timeout` is below 0
 *  #oC_ErrorCode_OutputAddressNotInRAM     | `outCardInfo` address does not point to the RAM section
 *  #oC_ErrorCode_TypeNotCorrect            | Unexpected behavior - card type is not supported by the driver
 *
 * More error codes can be returned by the function from #InitializeSDCv1, #InitializeSDCv2, #InitializeMMC, #ResetCard, #DetectCardType
 */
//==========================================================================================================================================
static oC_ErrorCode_t InitializeCard( oC_SDMMC_Context_t Context, oC_SDMMC_CardInfo_t *outCardInfo , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC_LLD_Mode) )
    {
        if(
            ErrorCondition( oC_SDMMC_IsContextCorrect(Context)          , oC_ErrorCode_ContextNotCorrect )
         && ErrorCondition( IsModeContextCorrect(Context->ModeContext)  , oC_ErrorCode_ContextNotCorrect )
         && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect    )
         && ErrorCondition( isram(outCardInfo)                          , oC_ErrorCode_OutputAddressNotInRAM )
            )
        {
            oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
            if(
                ErrorCode( oC_SDMMC_LLD_SetWideBus(Context->ModeContext->Channel, oC_SDMMC_LLD_WideBus_1Bit ) )
             && ErrorCode( oC_SDMMC_CommIf_DetectInterface( Context, &outCardInfo->CardInterface        , gettimeout(endTimestamp)) )
             && ErrorCode( oC_SDMMC_CommIf_InitializeCard ( Context, outCardInfo                        , gettimeout(endTimestamp)) )
             && ErrorCode( oC_SDMMC_LLD_SetWideBus        ( Context->ModeContext->Channel,                Context->ModeContext->LLDConfig.WideBus))
             && ErrorCode( oC_SDMMC_CommIf_SetTransferMode( Context, outCardInfo, Context->TransferMode , gettimeout(endTimestamp)) )
                )
            {
                errorCode = TestRead( Context, outCardInfo, gettimeout(endTimestamp));
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets transfer mode
 *
 * The function configures transfer mode
 *
 * @param Context           context of the driver
 * @param TransferMode      transfer mode to set
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module has not been enabled before
 *  #oC_ErrorCode_ContextNotCorrect         | The given `Context` structure is not correct
 *  #oC_ErrorCode_TransferModeNotCorrect    | The given `TransferMode` is not correct
 *  #oC_ErrorCode_TransferModeNotSupported  | The given `TransferMode` is not supported by the mode
 */
//==========================================================================================================================================
static oC_ErrorCode_t SetTransferMode( oC_SDMMC_Context_t Context , oC_SDMMC_TransferMode_t TransferMode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_SDMMC_LLD_Mode) )
    {
        if(
            ErrorCondition( oC_SDMMC_IsContextCorrect(Context)                      , oC_ErrorCode_ContextNotCorrect       )
         && ErrorCondition( IsModeContextCorrect(Context->ModeContext)              , oC_ErrorCode_ContextNotCorrect       )
         && ErrorCondition( TransferMode  < oC_SDMMC_TransferMode_NumberOfElements  , oC_ErrorCode_TransferModeNotCorrect  )
            )
        {
            switch(TransferMode)
            {
                case oC_SDMMC_TransferMode_1Bit:
                    errorCode = oC_SDMMC_LLD_SetWideBus(Context->ModeContext->Channel, oC_SDMMC_LLD_WideBus_1Bit);
                    break;
                case oC_SDMMC_TransferMode_4Bit:
                    errorCode = oC_SDMMC_LLD_SetWideBus(Context->ModeContext->Channel, oC_SDMMC_LLD_WideBus_4Bit);
                    break;
                case oC_SDMMC_TransferMode_8Bit:
                    errorCode = oC_SDMMC_LLD_SetWideBus(Context->ModeContext->Channel, oC_SDMMC_LLD_WideBus_8Bit);
                    break;
                default:
                    errorCode = oC_ErrorCode_TransferModeNotSupported;
                    break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends command by using LLD
 *
 * The function is for sending a command with LLD. It is local helper function to make the code shorter.
 *
 * @param Context       Pointer to the mode context
 * @param Index         Index of command to send
 * @param Argument      Argument for the command
 * @param outResponse   Destination for the command response or #NULL if there is no respone expected
 * @param Timeout       Maximum time for operation
 *
 * @return
 * If success the function returns #oC_ErrorCode_None, otherwise the user should expect one of the following error codes:
 *
 *  Error Code                              | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected behavior, in some path error code is not handled correctly
 *  #oC_ErrorCode_Timeout                   | Maximum time for the command has expired, but the command has not been sent
 *  #oC_ErrorCode_ResponseNotReceived       | The command was properly sent, but the card did not respond for the command
 *
 * More error codes can be returned by #oC_SDMMC_LLD_SendCommand and #oC_SDMMC_LLD_ReadCommandResponse
 */
//==========================================================================================================================================
static oC_ErrorCode_t SendCommandHelper(
    struct ModeContext_t *          Context ,
    oC_SDMMC_Mode_CommandIndex_t     Index ,
    oC_SDMMC_Mode_CommandArgument_t Argument ,
    oC_SDMMC_Response_t *   outResponse ,
    oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    oC_Struct_Define(oC_SDMMC_Response_t, appResponse);
    appResponse.Type = oC_SDMMC_LLD_ResponseType_ShortResponse;

    if( ErrorCondition( oC_Semaphore_Take( Context->CommandSentSemaphore, gettimeout(endTimestamp) ), oC_ErrorCode_Timeout ) )
    {
        oC_Struct_Define( oC_SDMMC_LLD_Command_t, command );

        command.CommandIndex    = (Index & 0xFF);
        command.Argument        = Argument;
        command.ResponseType    = outResponse != NULL ? outResponse->Type : oC_SDMMC_LLD_ResponseType_None;

        driverlog( oC_LogType_Track, "Sending command CMD%d(0x%X), expecting response type: %d", command.CommandIndex, Argument, command.ResponseType );

        if( ErrorCode( oC_SDMMC_LLD_SendCommand( Context->Channel, &command ) ) )
        {
            if( command.ResponseType == oC_SDMMC_LLD_ResponseType_None )
            {
                errorCode = oC_ErrorCode_None;
            }
            else if( ErrorCondition( oC_Semaphore_Take( Context->CommandResponseReceivedSemaphore, gettimeout(endTimestamp) ), oC_ErrorCode_ResponseNotReceived ) )
            {
                oC_SDMMC_LLD_CommandResponse_t response;

                if( ErrorCode( oC_SDMMC_LLD_ReadCommandResponse( Context->Channel, &response ) ) )
                {
                    driverlog( oC_LogType_Track, "Response for CMD%d(0x%X) has been received: { 0x%08X, 0x%08X, 0x%08X, 0x%08X }", command.CommandIndex,
                               Argument, response.Response[0], response.Response[1], response.Response[2], response.Response[3] );
                    memcpy( outResponse->LongResponse, response.Response, sizeof(oC_SDMMC_LongResponseData_t) );
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    driverlog( oC_LogType_Error, "Cannot read response for CMD%d(0x%X): %R", command.CommandIndex, Argument, errorCode );
                }
            }
            else
            {
                driverlog( oC_LogType_Warning, "Response for CMD%d(0x%X) not received, semaphore value: %d", command.CommandIndex, Argument, oC_Semaphore_Value(Context->CommandResponseReceivedSemaphore) );
            }
        }
    }
    else
    {
        driverlog( oC_LogType_Error, "Cannot take CommandSentSemaphore - timeout was set to %f", Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief searches for free LLD channel and transfer mode
 *
 * The function is for searching for free SDMMC LLD channel according to used pins with searching also for transfer mode if it is set to auto.
 *
 * @param Pins              Array with pins
 * @param TransferMode      Used transfer mode on input and found available transfer mode on output
 * @param outChannel        Destination for found channel
 *
 * @return true if found
 */
//==========================================================================================================================================
static bool FindFreeChannelAndTransferMode( const oC_SDMMC_Pins_t Pins, oC_SDMMC_TransferMode_t * TransferMode, oC_SDMMC_Channel_t * outChannel )
{
    bool found = false;

    if( (*TransferMode) != oC_SDMMC_TransferMode_Auto )
    {
        found = FindFreeChannel( Pins, *TransferMode, outChannel );
    }
    else
    {
        for( oC_SDMMC_TransferMode_t transferMode = oC_SDMMC_TransferMode_1Bit; transferMode <= oC_SDMMC_TransferMode_8Bit; transferMode++ )
        {
            if( FindFreeChannel( Pins, transferMode, outChannel ) )
            {
                *TransferMode = transferMode;
                found         = true;
                break;
            }
        }
    }

    return found;
}
//==========================================================================================================================================
/**
 * @brief searches for free LLD channel
 *
 * The function is for searching free SDMMC LLD channel according to the used pins.
 *
 * @param Pins          Array with used pins
 * @param outChannel    Destination for found channel
 *
 * @return true if free channel has been found
 */
//==========================================================================================================================================
static bool FindFreeChannel( const oC_SDMMC_Pins_t Pins, oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_Channel_t * outChannel )
{
    bool found = false;

    if( (*outChannel) != 0 )
    {
        if( oC_SDMMC_LLD_IsChannelCorrect( *outChannel ) )
        {
            found = IsChannelConfigPossible( Pins, TransferMode, *outChannel );
        }
        else
        {
            driverlog( oC_LogType_Error, "Channel %d is not correct", *outChannel );
        }
    }
    else
    {
        oC_SDMMC_LLD_ForEachChannel( channel )
        {
            if( IsChannelConfigPossible( Pins, TransferMode, channel ) )
            {
                *outChannel = channel;
                found       = true;
                break;
            }
        }
    }

    return found;
}

//==========================================================================================================================================
/**
 * @brief returns true if configuration of channel is possible
 */
//==========================================================================================================================================
static bool IsChannelConfigPossible( const oC_SDMMC_Pins_t Pins, oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_Channel_t Channel )
{
    bool possible = false;

    oC_SDMMC_LLD_ChannelIndex_t index = oC_SDMMC_LLD_ChannelToChannelIndex( Channel );
    if( Contexts[index] == NULL )
    {
        bool allSupported = true;

        for( oC_SDMMC_PinIndex_t pinIndex = 0; pinIndex < oC_SDMMC_PinIndex_NumberOfPins && allSupported; pinIndex++ )
        {
            if( PinFunctions[TransferMode][pinIndex] != 0 )
            {
                allSupported = oC_SDMMC_LLD_IsPinAvailable( Channel, Pins[pinIndex], PinFunctions[TransferMode][pinIndex] );
                if ( !allSupported )
                {
                    driverlog( oC_LogType_Info, "Channel %s is used, because pin %s is used", oC_Channel_GetName(Channel), oC_GPIO_GetPinName(Pins[pinIndex]) );
                }
            }
        }

        if( allSupported )
        {
            possible = true;
        }
    }
    else
    {
        driverlog( oC_LogType_Track, "channel %s is used", oC_Channel_GetName(Channel) );
    }

    return possible;
}

//==========================================================================================================================================
/**
 * @brief prepares LLD configuration
 *
 * The function is for preparation of LLD configuration structure according to driver configuration. It verifies all config fields and returns
 * code of error if something is not correct
 *
 * @param Config        Pointer to the SDMMC configuration
 * @param Channel       Channel to use in configuration
 * @param TransferMode  Transfer Mode
 * @param outConfig     Destination for LLD configuration
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
static oC_ErrorCode_t PrepareLLDConfig( const oC_SDMMC_Config_t * Config , oC_SDMMC_Channel_t Channel , oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_LLD_Config_t * outConfig )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( TransferModeToWideBus( TransferMode, &outConfig->WideBus ), oC_ErrorCode_ModeNotCorrect )
     && ErrorCondition( Config->SectorSize > 0                                    , oC_ErrorCode_SizeNotCorrect )
        )
    {
        outConfig->Channel      = Channel;
        outConfig->SectorSize   = Config->SectorSize;

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief converts transfer mode to wide bus
 *
 * The function converts enum #oC_SDMMC_TransferMode_t to enum #oC_SDMMC_LLD_WideBus_t
 *
 * @param TransferMode      Transfer mode to convert
 * @param outWideBus        Destination for the wide bus
 *
 * @return true if success, false if transfer mode cannot be converted to wide bus
 */
//==========================================================================================================================================
static bool TransferModeToWideBus( oC_SDMMC_TransferMode_t TransferMode, oC_SDMMC_LLD_WideBus_t * outWideBus )
{
    bool success = false;

    switch(TransferMode)
    {
        case oC_SDMMC_TransferMode_1Bit:
            *outWideBus = oC_SDMMC_LLD_WideBus_1Bit;
            success = true;
            break;
        case oC_SDMMC_TransferMode_4Bit:
            *outWideBus = oC_SDMMC_LLD_WideBus_4Bit;
            success = true;
            break;
        case oC_SDMMC_TransferMode_8Bit:
            *outWideBus = oC_SDMMC_LLD_WideBus_8Bit;
            success = true;
            break;
        default:
            success = false;
            break;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief returns true if the transfer mode is correct
 */
//==========================================================================================================================================
static bool IsTransferModeCorrect( oC_SDMMC_TransferMode_t TransferMode )
{
    return TransferMode == oC_SDMMC_TransferMode_Auto
        || TransferMode == oC_SDMMC_TransferMode_1Bit
        || TransferMode == oC_SDMMC_TransferMode_4Bit
        || TransferMode == oC_SDMMC_TransferMode_8Bit;
}

//==========================================================================================================================================
/**
 * @brief returns true if the mode context is correct
 */
//==========================================================================================================================================
static bool IsModeContextCorrect( struct ModeContext_t * Context )
{
    return isram(Context) && oC_CheckObjectControl( Context, oC_ObjectId_SDMMCModeLLD, Context->ObjectControl );
}

//==========================================================================================================================================
/**
 * @brief configures pins to work with the module
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigurePins( const oC_SDMMC_Pins_t Pins , oC_SDMMC_Channel_t Channel, oC_SDMMC_TransferMode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    errorCode = oC_ErrorCode_None;

    for( oC_SDMMC_PinIndex_t pinIndex = 0; pinIndex < oC_SDMMC_PinIndex_NumberOfPins; pinIndex++ )
    {
        if( PinFunctions[Mode][pinIndex] != 0 )
        {
            if( !ErrorCode( oC_SDMMC_LLD_ConfigurePin( Channel, Pins[pinIndex], PinFunctions[Mode][pinIndex] ) ) )
            {
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures SDMMC pins
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigurePins( const oC_SDMMC_Pins_t Pins , oC_SDMMC_TransferMode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    errorCode = oC_ErrorCode_None;

    for( oC_SDMMC_PinIndex_t pinIndex = 0; pinIndex < oC_SDMMC_PinIndex_NumberOfPins; pinIndex++ )
    {
        if( PinFunctions[Mode][pinIndex] != 0 )
        {
            ErrorCode( oC_SDMMC_LLD_UnconfigurePin( Pins[pinIndex] ) );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles LLD interrupts
 */
//==========================================================================================================================================
static void InterruptHandler( oC_SDMMC_Channel_t Channel , oC_SDMMC_LLD_InterruptSource_t InterruptSource )
{
    oC_SDMMC_LLD_ChannelIndex_t channelIndex = oC_SDMMC_LLD_ChannelToChannelIndex( Channel );

    if( Contexts[ channelIndex ] != NULL )
    {
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_DataReadyToReceive )
        {
            oC_Semaphore_Give( Contexts[channelIndex]->ModeContext->DataReadyToReceiveSemaphore );
        }
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_ReadyToSendNewData )
        {
            oC_Semaphore_Give( Contexts[channelIndex]->ModeContext->ReadyToSendNewDataSemaphore );
        }
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_CommandSent )
        {
            oC_Semaphore_Give( Contexts[channelIndex]->ModeContext->CommandSentSemaphore );
        }
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_CommandResponseReceived )
        {
            oC_Semaphore_Give( Contexts[channelIndex]->ModeContext->CommandResponseReceivedSemaphore );
        }
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_TransmissionFinished)
        {
            oC_Semaphore_Give( Contexts[channelIndex]->ModeContext->TransmissionFinished );
        }
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_TransmissionRestarted )
        {
            oC_Semaphore_ForceValue(Contexts[channelIndex]->ModeContext->DataReadyToReceiveSemaphore, 0);
            oC_Semaphore_ForceValue(Contexts[channelIndex]->ModeContext->ReadyToSendNewDataSemaphore, 1);
            oC_Semaphore_ForceValue(Contexts[channelIndex]->ModeContext->TransmissionFinished       , 0);
        }
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_TransmissionError )
        {
            driverlog( oC_LogType_Warning, "%s: transmission error", oC_Channel_GetName(Channel) );
        }
        if( InterruptSource & oC_SDMMC_LLD_InterruptSource_CommandTransmissionError )
        {
            driverlog( oC_LogType_Warning, "%s: command transmission error", oC_Channel_GetName(Channel) );
        }
    }
    else
    {
        driverlog( oC_LogType_Error, "%s: context is not set", oC_Channel_GetName(Channel) );
    }
}

#undef  _________________________________________FUNCTIONS__________________________________________________________________________________
//!< @}

#endif /* oC_SDMMC_LLD_AVAILABLE */
