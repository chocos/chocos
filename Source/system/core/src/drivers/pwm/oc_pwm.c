/** ****************************************************************************************************************************************
 *
 * @brief      The file with interface for the PWM driver
 *
 * @file       oc_pwm.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_pwm.h>
#include <oc_timer.h>
#include <oc_object.h>
#include <oc_stdlib.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         PWM
#define DRIVER_FILE_NAME    "pwm"
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &TIMER,&GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireClock | oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_PWM_Configure
#define DRIVER_UNCONFIGURE  oC_PWM_Unconfigure

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Context_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_TIMER_Context_t      TimerContext;
    uint64_t                MaximumValue;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool IsContextCorrect( oC_PWM_Context_t Context );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t Allocator = {
                .Name = "pwm"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_PWM_Configure( const oC_PWM_Config_t * Config , oC_PWM_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config) , oC_ErrorCode_WrongConfigAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)        , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Config->MaximumValue > 0 , oC_ErrorCode_MaximumValueNotCorrect )
        )
    {
        oC_PWM_Context_t context = ksmartalloc(sizeof(struct Context_t) , &Allocator , AllocationFlags_CanWait1Second);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(context) , oC_ErrorCode_AllocationError))
        {
            oC_TIMER_Config_t config = {
                            .Mode                   = oC_TIMER_Mode_PWM ,
                            .MaximumTimeForWait     = s(1) ,
                            .Frequency              = Config->TickFrequency,
                            .PermissibleDifference  = Config->PermissibleDiffernece ,
                            .MaximumValue           = Config->MaximumValue ,
                            .MatchValue             = 0 ,
                            .EventFlags             = 0 ,
                            .EventHandler           = NULL ,
                            .Pin                    = Config->Pin ,
                            .CountDirection         = oC_TIMER_CountDirection_DoesntMatter
            };

            if(
                oC_AssignErrorCode(&errorCode , oC_TIMER_Configure(&config,&context->TimerContext)) &&
                oC_AssignErrorCode(&errorCode , oC_TIMER_Start(context->TimerContext))
                )
            {
                context->ObjectControl  = oC_CountObjectControl(context,oC_ObjectId_PwmContext);
                context->MaximumValue   = Config->MaximumValue;
                *outContext             = context;
                errorCode               = oC_ErrorCode_None;
            }
            else if(ksmartfree(context,sizeof(struct Context_t),AllocationFlags_CanWaitForever)==false)
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_PWM_Unconfigure( const oC_PWM_Config_t * Config , oC_PWM_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Config)         , oC_ErrorCode_WrongConfigAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)                , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(*outContext)    , oC_ErrorCode_ContextNotCorrect )
        )
    {
        oC_PWM_Context_t  context = *outContext;
        oC_TIMER_Config_t config  = {
                        .Mode                   = oC_TIMER_Mode_PWM ,
                        .MaximumTimeForWait     = s(1) ,
                        .Frequency              = Config->TickFrequency,
                        .PermissibleDifference  = Config->PermissibleDiffernece ,
                        .MaximumValue           = Config->MaximumValue ,
                        .MatchValue             = 0 ,
                        .EventFlags             = 0 ,
                        .EventHandler           = NULL ,
                        .Pin                    = Config->Pin ,
                        .CountDirection         = oC_TIMER_CountDirection_DoesntMatter
        };

        context->ObjectControl = 0;

        if(oC_AssignErrorCode(&errorCode , oC_TIMER_Unconfigure(&config,&context->TimerContext)))
        {
            context->ObjectControl  = oC_CountObjectControl(context,oC_ObjectId_PwmContext);
            *outContext             = context;
            errorCode               = oC_ErrorCode_None;
        }

        if(ksmartfree(context,sizeof(struct Context_t),AllocationFlags_CanWaitForever)==false)
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_PWM_SetDuty( oC_PWM_Context_t Context , float PercentDuty )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , PercentDuty <= 100        , oC_ErrorCode_ValueTooBig )
        )
    {
        errorCode = oC_TIMER_SetMatchValue(Context,(uint64_t)((PercentDuty/100)*(float)Context->MaximumValue));
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_PWM_ReadDuty( oC_PWM_Context_t Context , float * outPercentDuty )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outPercentDuty)     , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Context->MaximumValue > 0 , oC_ErrorCode_MaximumValueNotCorrect )
        )
    {
        uint64_t value = 0;
        if(oC_AssignErrorCode(&errorCode , oC_TIMER_ReadMatchValue(Context,&value)))
        {
            *outPercentDuty = (((float)value)/((float)Context->MaximumValue))*100;
            errorCode       = oC_ErrorCode_None;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_PWM_SetValue( oC_PWM_Context_t Context , uint64_t Value )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ))
    {
        errorCode = oC_TIMER_SetMatchValue(Context,Value);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_PWM_ReadValue( oC_PWM_Context_t Context , uint64_t * outValue )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outValue)           , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        if(oC_AssignErrorCode(&errorCode , oC_TIMER_ReadMatchValue(Context,outValue)))
        {
            errorCode   = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static bool IsContextCorrect( oC_PWM_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_PwmContext,Context->ObjectControl);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
