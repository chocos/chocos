/** ****************************************************************************************************************************************
 *
 * @file       oc_i2c.c
 * 
 * File based on driver.c Ver 1.1.0
 *
 * @brief      The file with source for I2C driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2017-02-17 - 19:39:55)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_i2c.h>
#include <oc_i2c_lld.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_mutex.h>
#include <oc_semaphore.h>
#include <oc_event.h>
#include <oc_gpio.h>

#ifdef oC_I2C_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         I2C
#define DRIVER_FILE_NAME    "i2c"
#define DRIVER_DEBUGLOG     true
#define DRIVER_TYPE         COMMUNICATION_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_I2C_Configure
#define DRIVER_UNCONFIGURE  oC_I2C_Unconfigure
#define DRIVER_TURN_ON      oC_I2C_TurnOn
#define DRIVER_TURN_OFF     oC_I2C_TurnOff
#define IS_TURNED_ON        oC_I2C_IsTurnedOn
#define HANDLE_IOCTL        oC_I2C_Ioctl
#define READ_FROM_DRIVER    oC_I2C_Read
#define WRITE_TO_DRIVER     oC_I2C_Write

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define NUMBER_OF_CHANNELS                      oC_ModuleChannel_NumberOfElements(I2C)
#define IsChannelCorrect(Channel)               oC_I2C_LLD_IsChannelCorrect(Channel)
#define IsChannelUsed(Channel)                  (((oC_I2C_LLD_ChannelToChannelIndex(Channel) < NUMBER_OF_CHANNELS) ? \
                                                    Contextes[oC_I2C_LLD_ChannelToChannelIndex(Channel)] != NULL : \
                                                    true ) || oC_I2C_LLD_IsChannelUsed(Channel) )
#define SetChannelUsed(Channel,Context)         Contextes[oC_I2C_LLD_ChannelToChannelIndex(Channel)] = Context
#define SetChannelNotUsed(Channel,Context)      Contextes[oC_I2C_LLD_ChannelToChannelIndex(Channel)] = NULL
#define IsModeCorrect(Mode)                         ( (Mode) == oC_I2C_Mode_Master || (Mode) == oC_I2C_Mode_Slave )
#define IsAddressModeCorrect(AddressMode)           ( (AddressMode) == oC_I2C_AddressMode_7Bits || (AddressMode) == oC_I2C_AddressMode_10Bits)
#define IsAddressCorrect(Address,AddressMode)       ( (Address) <= (AddressMode) )
#define IsClockFrequencyCorrect(Freq,SpeedMode)     ( (Freq) > 0 && (Freq) <= SpeedMode )

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
     EventFlags_TransferComplete                = oC_I2C_LLD_InterruptSource_TransferComplete       ,
     EventFlags_AcknowledgeNotReceived          = oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived ,
     EventFlags_ArbitrationLost                 = oC_I2C_LLD_InterruptSource_ArbitrationLost        ,
     EventFlags_BusError                        = oC_I2C_LLD_InterruptSource_BusError               ,
     EventFlags_EarlyStopDetected               = oC_I2C_LLD_InterruptSource_EarlyStopDetected      ,
     EventFlags_DataReadyToReceive              = oC_I2C_LLD_InterruptSource_DataReadyToReceive     ,
     EventFlags_ReadyToSendNewData              = oC_I2C_LLD_InterruptSource_ReadyToSendNewData     ,
     EventFlags_AddressMatched                  = oC_I2C_LLD_InterruptSource_AddressMatched         ,
     EventFlags_Overrun                         = oC_I2C_LLD_InterruptSource_Overrun                ,
     EventFlags_Underrun                        = oC_I2C_LLD_InterruptSource_Underrun               ,
     EventFlags_Timeout                         = oC_I2C_LLD_InterruptSource_Timeout                ,
     EventFlags_PecError                        = oC_I2C_LLD_InterruptSource_PecError               ,
     EventFlags_SmbusAlert                      = oC_I2C_LLD_InterruptSource_SmbusAlert             ,
     EventFlags_NoMoreDataToSend                = oC_I2C_LLD_InterruptSource_NoMoreDataToSend       ,
     EventFlags_InitializationValue             = EventFlags_ReadyToSendNewData ,

     EventFlags_TransferErrors                  = EventFlags_AcknowledgeNotReceived
                                                | EventFlags_ArbitrationLost
                                                | EventFlags_BusError
                                                | EventFlags_EarlyStopDetected
                                                | EventFlags_PecError
                                                | EventFlags_Overrun
                                                | EventFlags_Underrun
                                                | EventFlags_Timeout,

} EventFlags_t;

//==========================================================================================================================================
/**
 * @brief stores I2C context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t      ObjectControl;
    oC_I2C_Address_t        LocalAddress;                   //!< Address of the local device
    oC_I2C_Address_t        RemoteAddress;                  //!< Address of the remote device
    oC_I2C_AddressMode_t    AddressMode;                    //!< I2C address mode
    oC_Mutex_t              BusyMutex;                      //!< Busy Mutex
    oC_I2C_Channel_t        Channel;                        //!< Used I2C channel
    oC_Event_t              TransmissionEvents;             //!< Event for transmission synchronization
    oC_I2C_Mode_t           Mode;                           //!< Mode of the I2C (master/slave)
    oC_I2C_Pin_t            SclModulePin;                   //!< I2C module pin for SCL signal
    oC_I2C_Pin_t            SdaModulePin;                   //!< I2C module pin for SDA signal
    bool                    GeneralCallsEnabled;            //!< Allow for general calls
    bool                    RawTransmissionStarted;         //!< RAW transmission started
    bool                    RawTransmissionStartedForRead;  //!< RAW transmission started for read
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all I2C driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "i2c module"
};
//==========================================================================================================================================
/**
 * Array with contextes of I2C for each channel
 */
//==========================================================================================================================================
static oC_I2C_Context_t Contextes[NUMBER_OF_CHANNELS] = {0};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsContextCorrect            ( oC_I2C_Context_t Context );
static inline bool              IsDeviceAddressCorrect      ( oC_I2C_Address_t Address , oC_I2C_AddressMode_t Mode );
static        oC_ErrorCode_t    CheckConfiguration          ( const oC_I2C_Config_t * Config );
static        oC_I2C_Context_t  Context_New                 ( const oC_I2C_Config_t * Config );
static        bool              Context_Delete              ( oC_I2C_Context_t Context );
static        oC_ErrorCode_t    ConfigureLLD                ( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context );
static        oC_ErrorCode_t    UnconfigureLLD              ( oC_I2C_Context_t Context );
static        oC_ErrorCode_t    TransmitData                ( oC_I2C_Context_t Context, const void *    Data, oC_MemorySize_t   Size, oC_Timestamp_t EndTimestamp );
static        oC_ErrorCode_t    ReceiveData                 ( oC_I2C_Context_t Context,       void * outData, oC_MemorySize_t * Size, oC_Timestamp_t EndTimestamp );
static        oC_ErrorCode_t    ChangeRemoteAddress         ( oC_I2C_Context_t Context, oC_I2C_Address_t RemoteAddress );
static        oC_ErrorCode_t    WaitForEvent                ( oC_I2C_Context_t Context , EventFlags_t EventFlags , oC_ErrorCode_t DefaultError , oC_Timestamp_t EndTimestamp );
static        oC_ErrorCode_t    WaitForTransmissionFinish   ( oC_I2C_Context_t Context , oC_Timestamp_t EndTimestamp );
static        oC_ErrorCode_t    StartTransmission           ( oC_I2C_Context_t Context , bool StartForRead, oC_MemorySize_t Size );
static        oC_ErrorCode_t    FinishTransmission          ( oC_I2C_Context_t Context , oC_Timestamp_t EndTimestamp );
static        void              InterruptHandler            ( oC_I2C_Channel_t Channel , oC_I2C_LLD_InterruptSource_t ActiveEvents , oC_I2C_LLD_InterruptSource_t InactiveEvents );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the I2C module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 * It also turns on the LLD layer.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_I2C))
    {
        errorCode = oC_I2C_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_ErrorCode_None;
        }
        else if(!oC_ErrorOccur(errorCode))
        {
            errorCode = oC_I2C_LLD_SetInterruptHandler( InterruptHandler );
        }

        if(!oC_ErrorOccur(errorCode))
        {
            /* This must be always at the end of the function */
            oC_Module_TurnOn(oC_Module_I2C);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the I2C driver
 *
 * The function for turning off the I2C driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_I2C);

        errorCode = oC_I2C_LLD_TurnOffDriver();

        if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_I2C_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_I2C);
}

//==========================================================================================================================================
/**
 * @brief returns true if context is correct I2C context
 */
//==========================================================================================================================================
bool oC_I2C_IsContextCorrect( oC_I2C_Context_t Context )
{
    return IsContextCorrect(Context);
}

//==========================================================================================================================================
/**
 * @brief configures I2C pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_I2C_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_Configure( const oC_I2C_Config_t * Config , oC_I2C_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C))
    {
        if(
           ErrorCondition( isram(outContext) , oC_ErrorCode_OutputAddressNotInRAM   )
        && ErrorCode     ( CheckConfiguration(Config)                               )
            )
        {
            oC_I2C_Context_t context = Context_New(Config);

            if(
                ErrorCondition( context != NULL , oC_ErrorCode_AllocationError )
             && ErrorCode     ( ConfigureLLD(Config,context)                   )
                )
            {
                SetChannelUsed(context->Channel,context);
                *outContext = context;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfFalse( "context", context == NULL || Context_Delete(context) , oC_ErrorCode_ReleaseError );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_I2C_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_Unconfigure( const oC_I2C_Config_t * Config , oC_I2C_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C))
    {
        if(
            ErrorCondition( isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM      )
         && ErrorCondition( IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect          )
            )
        {
            errorCode = oC_ErrorCode_None;

            SetChannelNotUsed((*outContext)->Channel,*outContext);
            ErrorCode( UnconfigureLLD(*outContext) );
            ErrorCondition( Context_Delete(*outContext), oC_ErrorCode_ReleaseError );

            *outContext = NULL;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of errror
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_Ioctl( oC_I2C_Context_t Context , oC_I2C_IoctlCommand_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Context)            , oC_ErrorCode_WrongAddress) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect )
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief reads buffer from the driver
 * 
 * The function is for reading buffer by using I2C driver. It is called when someone will read the driver file. 
 *
 * @param Context       Context of the driver
 * @param outBuffer     Buffer for data
 * @param Size          Size of the buffer on input, on output number of read bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_Read( oC_I2C_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout  )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                                   , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isram(outBuffer)                                            , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isram(Size)                                                 , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                                                 , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect          )
     && ErrorCondition( oC_Mutex_Take(Context->BusyMutex,gettimeout(endTimestamp))  , oC_ErrorCode_Timeout                 )
        )
    {
        if( ErrorCode(StartTransmission(Context,true,*Size)) )
        {
            if( ErrorCode( ReceiveData(Context,outBuffer,Size,endTimestamp) ) )
            {
                errorCode = FinishTransmission(Context,endTimestamp);
            }
            else
            {
                oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context,endTimestamp));
            }
        }
        oC_Mutex_Give(Context->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief write buffer to the driver
 * 
 * The function is for writing buffer by using I2C driver. It is called when someone will write the driver file. 
 *
 * @param Context       Context of the driver
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer on input, on output number of written bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_Write( oC_I2C_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                                   , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isaddresscorrect(Buffer)                                    , oC_ErrorCode_WrongAddress            )
     && ErrorCondition( isram(Size)                                                 , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                                                 , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect          )
     && ErrorCondition( oC_Mutex_Take(Context->BusyMutex,gettimeout(endTimestamp))  , oC_ErrorCode_Timeout                 )
        )
    {
        if( ErrorCode(StartTransmission(Context,false,*Size)) )
        {
            if( ErrorCode( TransmitData(Context,Buffer,*Size,endTimestamp) ) )
            {
                errorCode = FinishTransmission(Context,endTimestamp);
            }
            else
            {
                oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context,endTimestamp));
            }
        }
        oC_Mutex_Give(Context->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief transmits data
 *
 * The function is for transmission of data to the remote device. It can be used in both modes - as a master and as a slave. If the `RemoteAddress`
 * in `Master Mode` is 0, the function will communicate with default remote address given during driver configuration. To change the default
 * remote address you can use function #oC_I2C_Ioctl with command #oC_I2C_IoctlCommand_SetRemoteAddress.
 *
 * @note
 * In `Slave Mode` the `RemoteAddress` different than 0 will change the `Master Address`.
 *
 * @param Context           Context of the driver received from #oC_I2C_Configure function
 * @param RemoteAddress     Address of the remote device. If it is set to 0, the driver will use the default address
 * @param Data              Buffer with data to send
 * @param Size              Size of the buffer with data to send
 * @param Timeout           Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` in case of success
 *
 * Here is the list of standard error codes returned by the function:
 *
 *  Error Code                                | Description
 * -------------------------------------------|---------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ContextNotCorrect            | The given `Context` is not correct I2C driver context
 *  oC_ErrorCode_DeviceAddressNotCorrect      | The given `RemoteAddress` is not correct (it is too big)
 *  oC_ErrorCode_WrongAddress                 | The given `Data` does not point to correct memory address
 *  oC_ErrorCode_SizeNotCorrect               | Size of the data to send (`Size`) is incorrect
 *  oC_ErrorCode_TimeNotCorrect               | Maximum time for operation (The `Timeout` value) is below 0!
 *  oC_ErrorCode_Timeout                      | Maximum time for operation has expired
 *
 * @see oC_I2C_Write, oC_I2C_Ioctl, oC_I2C_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_Transmit( oC_I2C_Context_t Context , oC_I2C_Address_t RemoteAddress, const void * Data, oC_MemorySize_t Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                                   , oC_ErrorCode_ContextNotCorrect          )
     && ErrorCondition( IsDeviceAddressCorrect(RemoteAddress,Context->AddressMode)  , oC_ErrorCode_DeviceAddressNotCorrect    )
     && ErrorCondition( isaddresscorrect(Data)                                      , oC_ErrorCode_WrongAddress               )
     && ErrorCondition( Size > 0                                                    , oC_ErrorCode_SizeNotCorrect             )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect             )
     && ErrorCondition( Context->RawTransmissionStarted == false                    , oC_ErrorCode_RawTransmissionStarted     )
     && ErrorCondition( oC_Mutex_Take(Context->BusyMutex,gettimeout(endTimestamp))  , oC_ErrorCode_Timeout                    )
        )
    {
        if( ErrorCode( ChangeRemoteAddress(Context,RemoteAddress) ) )
        {
            if( ErrorCode(StartTransmission(Context,false,Size)) )
            {
                if( ErrorCode( TransmitData(Context,Data,Size,endTimestamp) ) )
                {
                    errorCode = FinishTransmission(Context,endTimestamp);
                }
                else
                {
                    oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context,endTimestamp));
                }
            }
        }

        oC_Mutex_Give(Context->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives data
 *
 * The function is for receiving DATA via I2C from the remote device. It can be used in both modes - as a master and as a slave. If the `RemoteAddress`
 * in `Master Mode` is 0, the function will communicate with default remote address given during driver configuration. To change the default
 * remote address you can use function #oC_I2C_Ioctl with command #oC_I2C_IoctlCommand_SetRemoteAddress.
 *
 * @note
 * In `Slave Mode` the `RemoteAddress` different than 0 will change the `Master Address`.
 *
 * @param Context           Context of the driver received from #oC_I2C_Configure function
 * @param RemoteAddress     Address of the remote device. If it is set to 0, the driver will use the default address
 * @param outData           Buffer for received data
 * @param Size              Size of the buffer for received data on input, number of received bytes on output
 * @param Timeout           Maximum time for the operation
 *
 * @return code of error or `oC_ErrorCode_None` in case of success
 *
 * Here is the list of standard error codes returned by the function:
 *
 *  Error Code                                | Description
 * -------------------------------------------|---------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ContextNotCorrect            | The given `Context` is not correct I2C driver context
 *  oC_ErrorCode_DeviceAddressNotCorrect      | The given `RemoteAddress` is not correct (it is too big)
 *  oC_ErrorCode_OutputAddressNotInRAM        | The given `outData` does not point to correct RAM address
 *  oC_ErrorCode_SizeNotCorrect               | Size of the data to send (`Size`) is incorrect
 *  oC_ErrorCode_TimeNotCorrect               | Maximum time for operation (The `Timeout` value) is below 0!
 *  oC_ErrorCode_Timeout                      | Maximum time for operation has expired
 *
 * @see oC_I2C_Write, oC_I2C_Ioctl, oC_I2C_Receive
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_Receive( oC_I2C_Context_t Context , oC_I2C_Address_t RemoteAddress, void * outData, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                                   , oC_ErrorCode_ContextNotCorrect          )
     && ErrorCondition( IsDeviceAddressCorrect(RemoteAddress,Context->AddressMode)  , oC_ErrorCode_DeviceAddressNotCorrect    )
     && ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM      )
     && ErrorCondition( isram(Size)                                                 , oC_ErrorCode_AddressNotInRam            )
     && ErrorCondition( (*Size) > 0                                                 , oC_ErrorCode_SizeNotCorrect             )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect             )
     && ErrorCondition( Context->RawTransmissionStarted == false                    , oC_ErrorCode_RawTransmissionStarted     )
     && ErrorCondition( oC_Mutex_Take(Context->BusyMutex,gettimeout(endTimestamp))  , oC_ErrorCode_Timeout                    )
        )
    {
        if( ErrorCode( ChangeRemoteAddress(Context,RemoteAddress) ) )
        {
            if( ErrorCode(StartTransmission(Context,false,*Size)) )
            {
                if( ErrorCode( ReceiveData(Context,outData,Size,endTimestamp) ) )
                {
                    errorCode = FinishTransmission(Context,endTimestamp);
                }
                else
                {
                    oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context,endTimestamp));
                }
            }
        }

        oC_Mutex_Give(Context->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_WriteRegister( oC_I2C_Context_t Context, oC_I2C_Address_t RemoteAddress, oC_I2C_Register_t Register, const void * Data, oC_MemorySize_t   Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                                   , oC_ErrorCode_ContextNotCorrect          )
     && ErrorCondition( IsDeviceAddressCorrect(RemoteAddress,Context->AddressMode)  , oC_ErrorCode_DeviceAddressNotCorrect    )
     && ErrorCondition( isaddresscorrect(Data)                                      , oC_ErrorCode_WrongAddress               )
     && ErrorCondition( Size > 0                                                    , oC_ErrorCode_SizeNotCorrect             )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect             )
     && ErrorCondition( Context->RawTransmissionStarted == false                    , oC_ErrorCode_RawTransmissionStarted     )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master                         , oC_ErrorCode_NotMasterMode              )
     && ErrorCondition( oC_Mutex_Take(Context->BusyMutex,gettimeout(endTimestamp))  , oC_ErrorCode_Timeout                    )
        )
    {
        if(ErrorCode( ChangeRemoteAddress(Context,RemoteAddress) ))
        {
            if(ErrorCode( StartTransmission(Context,false,sizeof(oC_I2C_Register_t) + Size) ))
            {
                if(
                    ErrorCode( TransmitData(Context, &Register, sizeof(Register), endTimestamp)             )
                 && ErrorCode( TransmitData(Context, Data     , Size            , endTimestamp)             )
                    )
                {
                    errorCode = FinishTransmission(Context,endTimestamp);
                }
                else
                {
                    oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context, endTimestamp));
                }

            }
        }

        oC_Mutex_Give(Context->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_ReadRegister( oC_I2C_Context_t Context, oC_I2C_Address_t RemoteAddress, oC_I2C_Register_t Register, void *    outData, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                                   , oC_ErrorCode_ContextNotCorrect          )
     && ErrorCondition( IsDeviceAddressCorrect(RemoteAddress,Context->AddressMode)  , oC_ErrorCode_DeviceAddressNotCorrect    )
     && ErrorCondition( isram(outData)                                              , oC_ErrorCode_OutputAddressNotInRAM      )
     && ErrorCondition( isram(Size)                                                 , oC_ErrorCode_AddressNotInRam            )
     && ErrorCondition( (*Size) > 0                                                 , oC_ErrorCode_SizeNotCorrect             )
     && ErrorCondition( Timeout >= 0                                                , oC_ErrorCode_TimeNotCorrect             )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master                         , oC_ErrorCode_NotMasterMode              )
     && ErrorCondition( oC_Mutex_Take(Context->BusyMutex,gettimeout(endTimestamp))  , oC_ErrorCode_Timeout                    )
        )
    {
        if(
            ErrorCondition( Context->RawTransmissionStarted == false , oC_ErrorCode_RawTransmissionStarted      )
         && ErrorCode( ChangeRemoteAddress(Context,RemoteAddress)                                               )
            )
        {

            if(ErrorCode( StartTransmission(Context,false,sizeof(oC_I2C_Register_t)) ))
            {
                if(
                    ErrorCode( TransmitData(Context, &Register, sizeof(Register), endTimestamp)                     )
                 && ErrorCode( WaitForEvent(Context,EventFlags_NoMoreDataToSend,oC_ErrorCode_Timeout,endTimestamp)  )
                 && ErrorCode( oC_I2C_LLD_StartAsMaster(Context->Channel,true,Context->RemoteAddress,*Size)         )
                    )
                {
                    if(ErrorCode( ReceiveData(Context,outData,Size,endTimestamp) ))
                    {
                        errorCode = FinishTransmission(Context,endTimestamp);
                    }
                    else
                    {
                        oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context, endTimestamp));
                    }
                }
                else
                {
                    oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context, endTimestamp));
                }
            }
        }

        oC_Mutex_Give(Context->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_TakeBusOwnership( oC_I2C_Context_t Context , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                   , oC_ErrorCode_ContextNotCorrect          )
     && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect             )
     && ErrorCondition( oC_Mutex_Take(Context->BusyMutex,Timeout)   , oC_ErrorCode_Timeout                    )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_ReleaseBusOwnership( oC_I2C_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)           , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( oC_Mutex_Give(Context->BusyMutex)   , oC_ErrorCode_WrongThread              )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_SetSlaveAddress( oC_I2C_Context_t Context , oC_I2C_Address_t SlaveAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( IsAddressCorrect(SlaveAddress,Context->AddressMode) , oC_ErrorCode_AddressNotCorrect        )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master                 , oC_ErrorCode_NotMasterMode            )
        )
    {
        Context->RemoteAddress  = SlaveAddress;
        errorCode               = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_ReadSlaveAddress( oC_I2C_Context_t Context , oC_I2C_Address_t * outSlaveAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)           , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isram(outSlaveAddress)              , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master , oC_ErrorCode_NotMasterMode            )
        )
    {
        *outSlaveAddress        = Context->RemoteAddress;
        errorCode               = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_ReadChannel( oC_I2C_Context_t Context , oC_I2C_Channel_t * outChannel )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)  , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isram(outChannel)          , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        *outChannel  = Context->Channel;
        errorCode    = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_RawStartAsMaster( oC_I2C_Context_t Context , bool StartForRead, oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master                 , oC_ErrorCode_NotMasterMode            )
     && ErrorCondition( Size > 0                                            , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( oC_Mutex_IsTakenByCurrentThread(Context->BusyMutex) , oC_ErrorCode_BusOwnershipNotTaken     )
        )
    {
        if(Context->RawTransmissionStarted)
        {
            errorCode = oC_I2C_LLD_StartAsMaster(Context->Channel,StartForRead,Context->RemoteAddress,Size);
        }
        else
        {
            errorCode = StartTransmission(Context,StartForRead,Size);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_RawStopAsMaster( oC_I2C_Context_t Context , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect            )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master                 , oC_ErrorCode_NotMasterMode                )
     && ErrorCondition( Context->RawTransmissionStarted == true             , oC_ErrorCode_RawTransmissionNotStarted    )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect               )
     && ErrorCondition( oC_Mutex_IsTakenByCurrentThread(Context->BusyMutex) , oC_ErrorCode_BusOwnershipNotTaken         )
        )
    {
        errorCode = FinishTransmission(Context,endTimestamp);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_RawWriteAsMaster( oC_I2C_Context_t Context, const void * Data , oC_MemorySize_t Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect            )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master                 , oC_ErrorCode_NotMasterMode                )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect               )
     && ErrorCondition( isaddresscorrect(Data)                              , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( Size > 0                                            , oC_ErrorCode_SizeNotCorrect               )
     && ErrorCondition( oC_Mutex_IsTakenByCurrentThread(Context->BusyMutex) , oC_ErrorCode_BusOwnershipNotTaken         )
     && ErrorCondition( Context->RawTransmissionStarted == true             , oC_ErrorCode_RawTransmissionNotStarted    )
     && ErrorCondition( Context->RawTransmissionStartedForRead == false     , oC_ErrorCode_TransmissionStartedForRead   )
        )
    {
        errorCode = TransmitData(Context,Data,Size,timeouttotimestamp(Timeout));
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_RawReadAsMaster( oC_I2C_Context_t Context , void * outData , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect            )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Master                 , oC_ErrorCode_NotMasterMode                )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect               )
     && ErrorCondition( oC_Mutex_IsTakenByCurrentThread(Context->BusyMutex) , oC_ErrorCode_BusOwnershipNotTaken         )
     && ErrorCondition( Context->RawTransmissionStarted == true             , oC_ErrorCode_RawTransmissionNotStarted    )
     && ErrorCondition( Context->RawTransmissionStartedForRead == true      , oC_ErrorCode_TransmissionStartedForWrite  )
     && ErrorCondition( isram(outData)                                      , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(Size)                                         , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( (*Size) > 0                                         , oC_ErrorCode_SizeNotCorrect               )
        )
    {
        errorCode = ReceiveData(Context,outData,Size,timeouttotimestamp(Timeout));
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_RawWriteAsSlave( oC_I2C_Context_t Context, const void * Data , oC_MemorySize_t Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect            )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Slave                  , oC_ErrorCode_MasterMode                   )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect               )
     && ErrorCondition( isaddresscorrect(Data)                              , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( Size > 0                                            , oC_ErrorCode_SizeNotCorrect               )
     && ErrorCondition( oC_Mutex_IsTakenByCurrentThread(Context->BusyMutex) , oC_ErrorCode_BusOwnershipNotTaken         )
        )
    {
        if( ErrorCode( StartTransmission(Context,false,Size) ))
        {
            if( ErrorCode( TransmitData(Context,Data,Size,endTimestamp) ) )
            {
                errorCode = FinishTransmission(Context,endTimestamp);
            }
            else
            {
                oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context,endTimestamp));
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_I2C_RawReadAsSlave( oC_I2C_Context_t Context , void * outData , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        oC_Module_TurnOnVerification(&errorCode, oC_Module_I2C)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect            )
     && ErrorCondition( Context->Mode == oC_I2C_Mode_Slave                  , oC_ErrorCode_MasterMode                   )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect               )
     && ErrorCondition( isram(outData)                                      , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( isram(Size)                                         , oC_ErrorCode_AddressNotInRam              )
     && ErrorCondition( (*Size) > 0                                         , oC_ErrorCode_SizeNotCorrect               )
     && ErrorCondition( oC_Mutex_IsTakenByCurrentThread(Context->BusyMutex) , oC_ErrorCode_BusOwnershipNotTaken         )
        )
    {
        if( ErrorCode( StartTransmission(Context,false,*Size) ))
        {
            if( ErrorCode( ReceiveData(Context,outData,Size,endTimestamp) ) )
            {
                errorCode = FinishTransmission(Context,endTimestamp);
            }
            else
            {
                oC_SaveIfErrorOccur("Cannot finish transmission", FinishTransmission(Context,endTimestamp));
            }
        }
    }

    return errorCode;
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the I2C driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_I2C_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_I2CContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief checks if the address is correct
 */
//==========================================================================================================================================
static inline bool IsDeviceAddressCorrect( oC_I2C_Address_t Address , oC_I2C_AddressMode_t Mode )
{
    return Address <= Mode;
}

//==========================================================================================================================================
/**
 * @brief returns true if speed mode is correct
 */
//==========================================================================================================================================
static inline bool IsSpeedModeCorrect( oC_I2C_SpeedMode_t Mode )
{
    bool correct = false;

    if(
        Mode == oC_I2C_SpeedMode_Normal
     || Mode == oC_I2C_SpeedMode_Fast
     || Mode == oC_I2C_SpeedMode_FastPlus
     || Mode == oC_I2C_SpeedMode_HighSpeed
     || Mode == oC_I2C_SpeedMode_UltraFast
        )
    {
        correct = true;
    }

    return correct;
}

//==========================================================================================================================================
/**
 * @brief searches for a free channel for the selected configuration
 */
//==========================================================================================================================================
static oC_ErrorCode_t FindChannelForConfig( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( Config->Advanced.Channel == 0 || IsChannelCorrect( Config->Advanced.Channel ), oC_ErrorCode_WrongChannel    )
     && ErrorCondition( Config->Advanced.Channel == 0 || IsChannelUsed   ( Config->Advanced.Channel ), oC_ErrorCode_ChannelIsUsed   )
        )
    {
        if(Config->Advanced.Channel != 0)
        {
            driverlog( oC_LogType_Info, "Correct and not used channel '%s' is given - using it\n", oC_Channel_GetName(Config->Advanced.Channel) );
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            driverlog( oC_LogType_Track, "Channel is not given, trying to find the free one\n");

            oC_I2C_Pin_t * sclPins      = NULL;
            oC_I2C_Pin_t * sdaPins      = NULL;
            uint32_t       sclArraySize = 0;
            uint32_t       sdaArraySize = 0;

            if(
                ErrorCode( oC_I2C_LLD_ReadModulePinsOfPin(Config->Pins.SCL,NULL,&sclArraySize,oC_I2C_PinFunction_SCL) )
             && ErrorCode( oC_I2C_LLD_ReadModulePinsOfPin(Config->Pins.SDA,NULL,&sdaArraySize,oC_I2C_PinFunction_SDA) )
                )
            {
                sclPins = kmalloc( sizeof(oC_I2C_Pin_t) * sclArraySize, &Allocator, AllocationFlags_ZeroFill );
                sdaPins = kmalloc( sizeof(oC_I2C_Pin_t) * sdaArraySize, &Allocator, AllocationFlags_ZeroFill );

                if(
                    ErrorCondition( sclPins != NULL, oC_ErrorCode_AllocationError )
                 && ErrorCondition( sdaPins != NULL, oC_ErrorCode_AllocationError )
                 && ErrorCode( oC_I2C_LLD_ReadModulePinsOfPin(Config->Pins.SCL,sclPins,&sclArraySize,oC_I2C_PinFunction_SCL) )
                 && ErrorCode( oC_I2C_LLD_ReadModulePinsOfPin(Config->Pins.SDA,sdaPins,&sdaArraySize,oC_I2C_PinFunction_SDA) )
                    )
                {
                    errorCode = oC_ErrorCode_NoChannelAvailable;

                    oC_I2C_LLD_ForEachChannel(channel,channelIndex)
                    {
                        if( IsChannelUsed(channel) )
                        {
                            driverlog( oC_LogType_Track, "Skipping channel '%s' - it is used\n", oC_Channel_GetName(channel) );
                        }
                        else
                        {
                            oC_I2C_Pin_t foundScl = (oC_I2C_Pin_t)oC_ModulePinIndex_NumberOfElements;
                            oC_I2C_Pin_t foundSda = (oC_I2C_Pin_t)oC_ModulePinIndex_NumberOfElements;

                            oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(sclPins,sclArraySize,sclPin)
                            {
                                if(oC_I2C_LLD_GetChannelOfModulePin(*sclPin) == (oC_I2C_Channel_t)channel)
                                {
                                    foundScl = *sclPin;
                                    break;
                                }
                            }
                            oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(sdaPins,sdaArraySize,sdaPin)
                            {
                                if(oC_I2C_LLD_GetChannelOfModulePin(*sdaPin) == (oC_I2C_Channel_t)channel)
                                {
                                    foundSda = *sdaPin;
                                    break;
                                }
                            }

                            if(((oC_ModulePinIndex_t)foundScl) < oC_ModulePinIndex_NumberOfElements && ((oC_ModulePinIndex_t)foundSda) < oC_ModulePinIndex_NumberOfElements)
                            {
                                driverlog( oC_LogType_GoodNews, "Found free channel - '%s'\n", oC_Channel_GetName(channel) );

                                Context->Channel        = channel;
                                Context->SclModulePin   = foundScl;
                                Context->SdaModulePin   = foundSda;
                                errorCode               = oC_ErrorCode_None;
                                break;
                            }
                            else
                            {
                                driverlog( oC_LogType_Track,
                                           "Skipping channel '%s' - pins '%s' and '%s' are not related with it\n",
                                           oC_Channel_GetName(channel),
                                           oC_GPIO_GetPinName(Config->Pins.SCL),
                                           oC_GPIO_GetPinName(Config->Pins.SDA)
                                           );
                            }
                        }
                    }
                }

                oC_SaveIfFalse("sclPins", sclPins == NULL || kfree(sclPins,0), oC_ErrorCode_ReleaseError);
                oC_SaveIfFalse("sclPins", sdaPins == NULL || kfree(sdaPins,0), oC_ErrorCode_ReleaseError);
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks configuration
 */
//==========================================================================================================================================
static oC_ErrorCode_t CheckConfiguration( const oC_I2C_Config_t * Config )
{
    oC_ErrorCode_t          errorCode   = oC_ErrorCode_ImplementError;
    oC_I2C_AddressMode_t    addressMode = isaddresscorrect(Config) && Config->AddressMode != 0 ? Config->AddressMode : oC_I2C_AddressMode_7Bits;

    if(
        ErrorCondition( isaddresscorrect(Config)                                            , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( IsModeCorrect(Config->Mode)                                         , oC_ErrorCode_ModeNotCorrect           )
     && ErrorCondition( IsSpeedModeCorrect(Config->SpeedMode)                               , oC_ErrorCode_SpeedModeNotCorrect      )
     && ErrorCondition( IsAddressModeCorrect(addressMode)                                   , oC_ErrorCode_AddressModeNotCorrect    )
     && ErrorCondition( IsAddressCorrect(Config->Master.DefaultSlaveAddress,addressMode)    , oC_ErrorCode_AddressNotCorrect        )
     && ErrorCondition( oC_GPIO_IsPinDefined(Config->Pins.SCL)                              , oC_ErrorCode_PinNotDefined            )
     && ErrorCondition( oC_GPIO_IsPinDefined(Config->Pins.SDA)                              , oC_ErrorCode_PinNotDefined            )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a new context
 */
//==========================================================================================================================================
static oC_I2C_Context_t Context_New( const oC_I2C_Config_t * Config )
{
    oC_I2C_Context_t context                = kmalloc( sizeof(struct Context_t), &Allocator, AllocationFlags_ZeroFill );
    oC_Mutex_t       busyMutex              = oC_Mutex_New(oC_Mutex_Type_Normal,&Allocator,AllocationFlags_Default);
    oC_Event_t       transmissionEvent      = oC_Event_New(EventFlags_InitializationValue,&Allocator,AllocationFlags_Default);

    if(
        oC_SaveIfFalse("context"            , context               != NULL, oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("busyMutex"          , busyMutex             != NULL, oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("transmissionEvent"  , transmissionEvent     != NULL, oC_ErrorCode_AllocationError )
        )
    {
        context->ObjectControl                  = oC_CountObjectControl(context,oC_ObjectId_I2CContext);
        context->BusyMutex                      = busyMutex;
        context->Channel                        = Config->Advanced.Channel;
        context->LocalAddress                   = Config->Slave.LocalAddress;
        context->RemoteAddress                  = Config->Master.DefaultSlaveAddress;
        context->Mode                           = Config->Mode;
        context->GeneralCallsEnabled            = Config->Slave.GeneralCallDetectionEnabled;
        context->RawTransmissionStarted         = false;
        context->RawTransmissionStartedForRead  = false;
        context->AddressMode                    = Config->AddressMode;
        context->TransmissionEvents             = transmissionEvent;
    }
    else
    {
        oC_SaveIfFalse("context"            , context               == NULL || kfree(context,0)                             , oC_ErrorCode_ReleaseError );
        oC_SaveIfFalse("busyMutex"          , busyMutex             == NULL || oC_Mutex_Delete(&busyMutex,0)                , oC_ErrorCode_ReleaseError );
        oC_SaveIfFalse("transmissionEvent"  , transmissionEvent     == NULL || oC_Event_Delete(&transmissionEvent,0)        , oC_ErrorCode_ReleaseError );
        context = NULL;
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief deletes context
 */
//==========================================================================================================================================
static bool Context_Delete( oC_I2C_Context_t Context )
{
    bool deleted = false;

    oC_IntMan_EnterCriticalSection();

    if(oC_SaveIfFalse( "context", oC_CheckObjectControl(Context,oC_ObjectId_I2CContext,Context->ObjectControl), oC_ErrorCode_ObjectNotCorrect ))
    {
        Context->ObjectControl = 0;

        bool deletedMutex       = oC_SaveIfFalse("busyMutex"          , oC_Mutex_Delete(&Context->BusyMutex,0)                , oC_ErrorCode_ReleaseError );
        bool eventDeleted       = oC_SaveIfFalse("transmissionEvent"  , oC_Event_Delete(&Context->TransmissionEvents,0)       , oC_ErrorCode_ReleaseError );
        bool deletedContext     = oC_SaveIfFalse("context"            , kfree(Context,0)                                      , oC_ErrorCode_ReleaseError );

        deleted = deletedMutex && deletedContext && eventDeleted;
    }

    oC_IntMan_ExitCriticalSection();

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief configures LLD
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureLLD( const oC_I2C_Config_t * Config , oC_I2C_Context_t Context )
{
    oC_ErrorCode_t                  errorCode               = oC_ErrorCode_ImplementError;
    oC_I2C_LLD_InterruptSource_t    masterEnabledInterrupts = oC_I2C_LLD_InterruptSource_TransferComplete
                                                            | oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived
                                                            | oC_I2C_LLD_InterruptSource_ArbitrationLost
                                                            | oC_I2C_LLD_InterruptSource_BusError
                                                            | oC_I2C_LLD_InterruptSource_DataReadyToReceive
                                                            | oC_I2C_LLD_InterruptSource_ReadyToSendNewData
                                                            | oC_I2C_LLD_InterruptSource_EarlyStopDetected
                                                            | oC_I2C_LLD_InterruptSource_Overrun
                                                            | oC_I2C_LLD_InterruptSource_Underrun
                                                            | oC_I2C_LLD_InterruptSource_NoMoreDataToSend;
    oC_I2C_LLD_InterruptSource_t    slaveEnabledInterrupts  = oC_I2C_LLD_InterruptSource_TransferComplete
                                                            | oC_I2C_LLD_InterruptSource_AcknowledgeNotReceived
                                                            | oC_I2C_LLD_InterruptSource_ArbitrationLost
                                                            | oC_I2C_LLD_InterruptSource_BusError
                                                            | oC_I2C_LLD_InterruptSource_EarlyStopDetected
                                                            | oC_I2C_LLD_InterruptSource_DataReadyToReceive
                                                            | oC_I2C_LLD_InterruptSource_ReadyToSendNewData
                                                            | oC_I2C_LLD_InterruptSource_AddressMatched
                                                            | oC_I2C_LLD_InterruptSource_Overrun
                                                            | oC_I2C_LLD_InterruptSource_Underrun
                                                            | oC_I2C_LLD_InterruptSource_Timeout;
    if(
        ErrorCode( FindChannelForConfig(Config,Context)                                         )
     && ErrorCode( oC_I2C_LLD_ConnectModulePin(Context->SdaModulePin)                           )
     && ErrorCode( oC_I2C_LLD_ConnectModulePin(Context->SclModulePin)                           )
        )
    {
        if(Config->Mode == oC_I2C_Mode_Master)
        {
            errorCode = oC_I2C_LLD_ConfigureAsMaster(
                            Context->Channel,
                            Config->SpeedMode,
                            Config->AddressMode,
                            Config->Advanced.DataHoldTime,
                            Config->Advanced.DataSetupTime,
                            Config->Advanced.ClockLowTime,
                            Config->Advanced.ClockHighTime,
                            Config->Advanced.ClockFrequency,
                            masterEnabledInterrupts
                            );
        }
        else
        {
            errorCode = oC_I2C_LLD_ConfigureAsSlave(
                            Context->Channel,
                            Config->SpeedMode,
                            Config->AddressMode,
                            Context->LocalAddress,
                            Config->Slave.GeneralCallDetectionEnabled,
                            Config->Slave.ClockStretchingEnabled,
                            slaveEnabledInterrupts
                            );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures LLD
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigureLLD( oC_I2C_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(
        ErrorCode( oC_I2C_LLD_DisconnectModulePin(Context->SdaModulePin)    )
     && ErrorCode( oC_I2C_LLD_DisconnectModulePin(Context->SclModulePin)    )
     && ErrorCode( oC_I2C_LLD_UnconfigureChannel(Context->Channel)          )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief transmits data via I2C interface
 */
//==========================================================================================================================================
static oC_ErrorCode_t TransmitData( oC_I2C_Context_t Context, const void * Data, oC_MemorySize_t   Size, oC_Timestamp_t EndTimestamp )
{
    oC_ErrorCode_t  errorCode  = oC_ErrorCode_ImplementError;
    oC_MemorySize_t sentBytes  = 0;
    const uint8_t * buffer     = Data;
    oC_MemorySize_t leftSize   = Size;
    oC_MemorySize_t bufferSize = Size;

    oC_ErrorCode_t (*writeFunctionPointer)( oC_I2C_Channel_t Channel, const uint8_t * Data, oC_MemorySize_t * Size ) =
                    Context->Mode == oC_I2C_Mode_Master ? oC_I2C_LLD_WriteAsMaster : oC_I2C_LLD_WriteAsSlave;

    errorCode = oC_ErrorCode_NoDataToSend;

    while(
        leftSize > 0
     && ErrorCode( WaitForEvent(Context, EventFlags_ReadyToSendNewData, oC_ErrorCode_Timeout, EndTimestamp)     )
     && ErrorCode( writeFunctionPointer(Context->Channel,&buffer[sentBytes],&leftSize)                          )
        )
    {
        sentBytes += leftSize;
        leftSize   = bufferSize - leftSize;

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives data via I2C interface
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReceiveData( oC_I2C_Context_t Context, void * outData, oC_MemorySize_t * Size, oC_Timestamp_t EndTimestamp )
{
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;
    oC_MemorySize_t receivedBytes   = 0;
    oC_MemorySize_t bufferSize      = *Size;
    oC_MemorySize_t leftSize        = *Size;
    uint8_t *       buffer          = outData;

    oC_ErrorCode_t (*readFunctionPointer)( oC_I2C_Channel_t Channel, uint8_t * outData, oC_MemorySize_t * Size ) =
                    Context->Mode == oC_I2C_Mode_Master ? oC_I2C_LLD_ReadAsMaster : oC_I2C_LLD_ReadAsSlave;

    errorCode = oC_ErrorCode_NoDataToReceive;

    while(
        leftSize > 0
     && ErrorCode( WaitForEvent(Context, EventFlags_DataReadyToReceive, oC_ErrorCode_Timeout, EndTimestamp) )
     && ErrorCode( readFunctionPointer(Context->Channel,&buffer[receivedBytes],&leftSize)                   )
        )
    {
        receivedBytes += leftSize;
        leftSize       = bufferSize - leftSize;

        errorCode = oC_ErrorCode_None;
    }

    *Size = receivedBytes;

    if(receivedBytes > 0 && errorCode == oC_ErrorCode_Timeout)
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief changes the remote I2C address
 */
//==========================================================================================================================================
static oC_ErrorCode_t ChangeRemoteAddress( oC_I2C_Context_t Context, oC_I2C_Address_t RemoteAddress )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;

    if(RemoteAddress != 0)
    {
        Context->RemoteAddress = RemoteAddress;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for specified event
 */
//==========================================================================================================================================
static oC_ErrorCode_t WaitForEvent( oC_I2C_Context_t Context , EventFlags_t EventFlags , oC_ErrorCode_t DefaultError , oC_Timestamp_t EndTimestamp )
{
    EventFlags_t    eventFlags = EventFlags | EventFlags_TransferErrors;
    oC_ErrorCode_t  errorCode  = oC_ErrorCode_ImplementError;

    if(ErrorCondition(oC_Event_WaitForBitSet(Context->TransmissionEvents,eventFlags,gettimeout(EndTimestamp)), DefaultError ))
    {
        EventFlags_t activeEvents = oC_Event_GetState(Context->TransmissionEvents);

        if(
            ErrorCondition( (activeEvents & EventFlags_AcknowledgeNotReceived) == 0 , oC_ErrorCode_AcknowledgeNotReceived    )
         && ErrorCondition( (activeEvents & EventFlags_EarlyStopDetected     ) == 0 , oC_ErrorCode_EarlyStopDetected         )
         && ErrorCondition( (activeEvents & EventFlags_ArbitrationLost       ) == 0 , oC_ErrorCode_ArbitrationLost           )
         && ErrorCondition( (activeEvents & EventFlags_BusError              ) == 0 , oC_ErrorCode_BusError                  )
         && ErrorCondition( (activeEvents & EventFlags_PecError              ) == 0 , oC_ErrorCode_PecError                  )
         && ErrorCondition( (activeEvents & EventFlags_Overrun               ) == 0 , oC_ErrorCode_OverrunError              )
         && ErrorCondition( (activeEvents & EventFlags_Underrun              ) == 0 , oC_ErrorCode_UnderrunError             )
         && ErrorCondition( (activeEvents & EventFlags_Timeout               ) == 0 , oC_ErrorCode_Timeout                   )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for specified event
 */
//==========================================================================================================================================
static oC_ErrorCode_t WaitForTransmissionFinish( oC_I2C_Context_t Context , oC_Timestamp_t EndTimestamp )
{
    static const EventFlags_t eventFlags = EventFlags_TransferComplete | EventFlags_TransferErrors;
    oC_ErrorCode_t            errorCode  = oC_ErrorCode_ImplementError;

    if( ErrorCode(WaitForEvent(Context,eventFlags,oC_ErrorCode_TransmissionNotComplete,EndTimestamp)) )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief prepares module to new transmission
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartTransmission( oC_I2C_Context_t Context , bool StartForRead, oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    oC_Event_ClearStateBits(Context->TransmissionEvents,EventFlags_TransferErrors | EventFlags_TransferComplete);

    if(Context->Mode == oC_I2C_Mode_Master)
    {
        if(Context->RawTransmissionStarted == false)
        {
            oC_Event_ClearStateBits(Context->TransmissionEvents,EventFlags_DataReadyToReceive);

            if( ErrorCode(oC_I2C_LLD_StartAsMaster(Context->Channel,StartForRead,Context->RemoteAddress,Size)) )
            {
                Context->RawTransmissionStarted         = true;
                Context->RawTransmissionStartedForRead  = StartForRead;
                errorCode                               = oC_ErrorCode_None;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }
    }
    else
    {
        errorCode = oC_ErrorCode_None;
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for specified event
 */
//==========================================================================================================================================
static oC_ErrorCode_t FinishTransmission( oC_I2C_Context_t Context , oC_Timestamp_t EndTimestamp )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;

    ErrorCode( WaitForTransmissionFinish(Context,EndTimestamp) );

    if(Context->Mode == oC_I2C_Mode_Master)
    {
        Context->RawTransmissionStarted = false;

        if(errorCode == oC_ErrorCode_None)
        {
            errorCode = oC_I2C_LLD_StopAsMaster(Context->Channel);
        }
        else
        {
            oC_SaveIfErrorOccur("Cannot stop transmission - ", oC_I2C_LLD_StopAsMaster(Context->Channel));
        }
    }
    else
    {
        errorCode = oC_ErrorCode_None;
    }

    oC_Event_SetState(Context->TransmissionEvents,EventFlags_InitializationValue);

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function called when interrupt occurs
 */
//==========================================================================================================================================
static void InterruptHandler( oC_I2C_Channel_t Channel , oC_I2C_LLD_InterruptSource_t ActiveEvents , oC_I2C_LLD_InterruptSource_t InactiveEvents )
{
    oC_I2C_Context_t context = NULL;

    if(oC_SaveIfFalse("Channel", oC_I2C_LLD_IsChannelCorrect(Channel), oC_ErrorCode_WrongChannel))
    {
        oC_I2C_LLD_ChannelIndex_t index = oC_I2C_LLD_ChannelToChannelIndex(Channel);

        if(oC_SaveIfFalse("Index" , index < oC_ARRAY_SIZE(Contextes) , oC_ErrorCode_ValueTooBig))
        {
            context = Contextes[index];

            if(context != NULL && oC_SaveIfFalse("context", IsContextCorrect(context), oC_ErrorCode_ContextNotCorrect))
            {
                oC_Event_ClearStateBits(context->TransmissionEvents,InactiveEvents );
                oC_Event_SetStateBits  (context->TransmissionEvents,ActiveEvents   );
            }
            else
            {
                driverlog( oC_LogType_Error, "Channel '%s' context is not correct", oC_Channel_GetName(Channel));
            }
        }
        else
        {
            driverlog( oC_LogType_Error, "Channel '%d' index '%d' is not correct", Channel, index);
        }
    }
    else
    {
        driverlog( oC_LogType_Error, "Channel '%d' is not correct", Channel );
    }
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

#endif /* oC_I2C_LLD_AVAILABLE */
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
