/** ****************************************************************************************************************************************
 *
 * @file       oc_uart.c
 *
 * @brief      The file with source for UART driver
 *
 * @author     Patryk Kubiak - (Created on: 19 09 2015 15:03:45)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_uart.h>
#include <oc_gpio.h>
#include <oc_object.h>
#include <oc_uart_lld.h>
#include <oc_stdlib.h>
#include <oc_array.h>
#include <oc_intman.h>
#include <oc_mutex.h>
#include <oc_memman.h>
#include <oc_event.h>
#include <oc_ktime.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_DEBUGLOG     true
#define DRIVER_NAME         UART
#define DRIVER_FILE_NAME    "uart"
#define DRIVER_TYPE         COMMUNICATION_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireClock | oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_UART_Configure
#define DRIVER_UNCONFIGURE  oC_UART_Unconfigure
#define DRIVER_TURN_ON      oC_UART_TurnOn
#define DRIVER_TURN_OFF     oC_UART_TurnOff
#define IS_TURNED_ON        oC_UART_IsTurnedOn
#define READ_FROM_DRIVER    oC_UART_Read
#define WRITE_TO_DRIVER     oC_UART_Write
#define HANDLE_IOCTL        oC_UART_Ioctl

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This should be always at the end of includes list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Context_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_UART_Channel_t       Channel;
    oC_UART_Dma_t           Dma;
    oC_UART_Mode_t          Mode;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool                 ModuleEnabledFlag   = false;
static bool                 InterruptsConfigured= false;
static oC_Event_t           RxNotEmptyEvent     = NULL;
static oC_Event_t           TxNotFullEvent      = NULL;
static const oC_Allocator_t ModuleAllocator     = {
                .Name = "uart",
                .CorePwd = oC_MemMan_CORE_PWD
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsConfigCorrect( const oC_UART_Config_t * Config );
static inline bool              IsContextCorrect( oC_UART_Context_t Context );
static        oC_ErrorCode_t    FindFreeChannelForPins( oC_Pins_t Tx , oC_Pins_t Rx , oC_UART_Channel_t * outChannel );
static        void              RxNotEmptyInterruptHandler( oC_UART_Channel_t Channel );
static        void              TxNotFullInterruptHandler( oC_UART_Channel_t Channel );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the UART driver
 * 
 * The function turns on the UART driver - the driver cannot be used before it is turned on. 
 * 
 * @note usually you dont need to call this function - the driver is turned on automatically by the driver manager during the boot process
 * 
 * @return The error code or #oC_ErrorCode_None if success
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ModuleIsTurnedOn     | The module is already turned on
 *  #oC_ErrorCode_ImplementError       | The was unexpected error in the implementation
 * 
 *  @note 
 *  More error codes can be returned by #oC_UART_LLD_TurnOnDriver and #oC_UART_LLD_SetDriverInterruptHandlers functions
 * 
 * @see oC_UART_TurnOff, oC_UART_IsTurnedOn
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn))
    {
        errorCode = oC_UART_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_None || errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_UART_LLD_SetDriverInterruptHandlers(RxNotEmptyInterruptHandler,TxNotFullInterruptHandler);

            if(oC_ErrorOccur(errorCode))
            {
                oC_SaveError("UART: Cannot set driver interrupt handler: " , errorCode);
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                RxNotEmptyEvent         = oC_Event_New(oC_Event_State_Inactive , &ModuleAllocator , AllocationFlags_CanWait1Second);
                TxNotFullEvent          = oC_Event_New(oC_Event_State_Inactive , &ModuleAllocator , AllocationFlags_CanWait1Second);
                InterruptsConfigured    = oC_Event_IsCorrect(RxNotEmptyEvent);
            }

            ModuleEnabledFlag = true;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off the UART driver
 * 
 * The function turns off the UART driver - the driver cannot be used after it is turned off. 
 * 
 * @note usually you dont need to call this function - the driver is turned off automatically by the driver manager during the shutdown process
 * 
 * @return The error code or #oC_ErrorCode_None if success
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet  | The module is not started yet
 *  #oC_ErrorCode_ReleaseError         | The was unexpected error in the release
 * 
 *  @note 
 *  More error codes can be returned by #oC_UART_LLD_TurnOffDriver function
 * 
 * @see oC_UART_TurnOn, oC_UART_IsTurnedOn
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;
        errorCode         = oC_UART_LLD_TurnOffDriver();

        if(errorCode == oC_ErrorCode_None || errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {
            if(InterruptsConfigured && !oC_Event_Delete(&RxNotEmptyEvent , AllocationFlags_CanWaitForever))
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
            else
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the UART driver is turned on
 * 
 * The function checks if the UART driver is turned on
 * 
 * @return true if the driver is turned on, false otherwise
 */
//==========================================================================================================================================
bool oC_UART_IsTurnedOn( void )
{
    return ModuleEnabledFlag;
}

//==========================================================================================================================================
/**
 * @brief configures the UART
 * 
 * The function configures the UART with the specified configuration. Once the UART is configured, the context is returned - you can use 
 * it to read or write data from/to the UART by using the #oC_UART_Read and #oC_UART_Write functions.
 * 
 * @param Config        The configuration of the UART
 * @param outContext    The output context - it will be created by the function
 * 
 * @return The error code or #oC_ErrorCode_None if success 
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet  | The module is not started yet
 *  #oC_ErrorCode_WrongConfigAddress   | The configuration address is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM| The output address is not in RAM
 *  #oC_ErrorCode_DmaModeNotCorrect    | The DMA mode is not correct
 *  #oC_ErrorCode_AllocationError      | The was unexpected error in the allocation (out of memory?)
 * 
 * @note
 * The function can return more error codes - see the #oC_UART_LLD_SetPower, #oC_UART_LLD_DisableOperations, #oC_UART_LLD_SetBitRate,
 * #oC_UART_LLD_SetWordLength, #oC_UART_LLD_SetParity, #oC_UART_LLD_SetStopBit, #oC_UART_LLD_SetBitOrder, #oC_UART_LLD_SetInvert,
 * #oC_UART_LLD_SetLoopback, #oC_UART_LLD_EnableOperations functions.
 * 
 * @see oC_UART_Unconfigure, oC_UART_Read, oC_UART_Write
 * 
 * <b>Example:</b>
 * @code{.c}
 
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);
    if(errorCode == oC_ErrorCode_None)
    {
        // read or write data
        errorCode = oC_UART_Read(context,buffer,&size,oC_s(1));
    }
    else
    {
        // error handling
    }
 
    @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Configure( const oC_UART_Config_t * Config , oC_UART_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true ,          oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsConfigCorrect(Config)   ,          oC_ErrorCode_WrongConfigAddress )  &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext) ,                  oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Config->Dma <= oC_UART_Dma_DontUse,  oC_ErrorCode_DmaModeNotCorrect )
        )
    {
        oC_UART_Channel_t channel = 0;

        if(
            oC_AssignErrorCode(&errorCode,FindFreeChannelForPins(Config->Tx,Config->Rx,&channel))
            )
        {
            struct Context_t * context = ksmartalloc(sizeof(struct Context_t),&ModuleAllocator,AllocationFlags_CanWait1Second);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , oC_MemMan_IsDynamicAllocatedAddress(context) , oC_ErrorCode_AllocationError ) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetPower(channel,oC_Power_On)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_DisableOperations(channel)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetBitRate(channel,Config->BitRate)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetWordLength(channel,Config->WordLength)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetParity(channel,Config->Parity)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetStopBit(channel,Config->StopBit)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetBitOrder(channel,Config->BitOrder)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetInvert(channel,Config->Invert)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetLoopback(channel,Config->Loopback)) &&
                oC_AssignErrorCode(&errorCode , oC_UART_LLD_EnableOperations(channel))
                )
            {
                context->ObjectControl = oC_CountObjectControl(context,oC_ObjectId_UartContext);
                context->Channel       = channel;
                context->Dma           = Config->Dma;

                *outContext            = context;

                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_UART_LLD_RestoreDefaultStateOnChannel(channel);
                ksmartfree(context,sizeof(struct Context_t),AllocationFlags_CanWaitForever);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures the UART
 * 
 * The function unconfigures the UART - the context is released and the UART is turned off.
 * 
 * @warning DMA usage is not supported for all platforms - the function can return an error if the DMA is not supported.
 * 
 * @param Config        The configuration of the UART
 * @param outContext    The output context - it will be created by the function
 * 
 * @return The error code or #oC_ErrorCode_None if success 
 * 
 * Possible codes:
 *  Error Code                              | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet       | The module is not started yet
 *  #oC_ErrorCode_ContextNotCorrect         | The context is not correct
 *  #oC_ErrorCode_CannotRestoreDefaultState | The default state cannot be restored
 *  #oC_ErrorCode_ReleaseError              | The was unexpected error in the release
 * 
 *  @note 
 *  More error codes can be returned by #oC_UART_LLD_RestoreDefaultStateOnChannel function
 * 
 * @see oC_UART_Configure, oC_UART_Read, oC_UART_Write
 * 
 * <b>Example:</b>
 * @code{.c}
 
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);
    if(errorCode == oC_ErrorCode_None)
    {
        // read or write data
        errorCode = oC_UART_Read(context,buffer,&size,oC_s(1));
        if(errorCode == oC_ErrorCode_None)
        {
            // unconfigure the UART
            errorCode = oC_UART_Unconfigure(&config,&context);
        }
    }
    else
    {
        // error handling
    }

    @endcode
*/
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Unconfigure( const oC_UART_Config_t * Config , oC_UART_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( ModuleEnabledFlag == true     , oC_ErrorCode_ModuleNotStartedYet )
     && ErrorCondition( IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect   )
        )
    {
        oC_UART_Context_t context = *outContext;

        if(
            ErrorCode( oC_UART_LLD_SetChannelUnused(context->Channel) )
         && ErrorCode( oC_GPIO_LLD_SetPinsUnused(Config->Rx) )
         && ErrorCode( oC_GPIO_LLD_SetPinsUnused(Config->Tx) )
         && ErrorCode( oC_UART_LLD_RestoreDefaultStateOnChannel(context->Channel) )
         && ErrorCondition( ksmartfree(context,sizeof(struct Context_t),AllocationFlags_CanWaitForever) , oC_ErrorCode_ReleaseError )
            )
        {
            *outContext = NULL;
            errorCode   = oC_ErrorCode_None;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads data from the UART
 * 
 * The function reads data from the UART. The function reads data from the UART and stores it in the buffer. 
 * 
 * @warning
 * This function does not wait for all the data to be read - it only waits for some data to be available. 
 * If you want to read all the data, you should call this function in a loop until all the data is read, 
 * or use the #oC_UART_Receive function.
 * 
 * @param Context       The context of the UART
 * @param outBuffer     The output buffer
 * @param Size          The size of the buffer - the function will store the number of read bytes in this variable
 * @param Timeout       The timeout for reading data
 * 
 * @return The error code or #oC_ErrorCode_None if success 
 * 
 * Possible codes:
 *  Error Code                              | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet       | The module is not started yet
 *  #oC_ErrorCode_ContextNotCorrect         | The context is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The output address is not in RAM (Buffer or Size)
 *  #oC_ErrorCode_SizeNotCorrect            | The size is not correct
 *  #oC_ErrorCode_Timeout                   | The timeout occurred
 *  #oC_ErrorCode_NoAllBytesRead            | Not all bytes were read
 * 
 *  @note 
 *  More error codes can be returned by #oC_UART_LLD_ReadWithDma, #oC_UART_LLD_IsRxFifoEmpty, #oC_Event_WaitForState, #oC_UART_LLD_IsRxFifoEmpty,
 *  #oC_UART_LLD_GetData functions
 * 
 * @see oC_UART_Write, oC_UART_Configure, oC_UART_Receive
 * 
 * <b>Example:</b>
 * @code{.c}
    
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);

    if(errorCode == oC_ErrorCode_None)
    {
        char buffer[100];
        oC_MemorySize_t size = 100;
        errorCode = oC_UART_Read(context,buffer,&size,oC_s(1));
        if(errorCode == oC_ErrorCode_None)
        {
            // do something with the read data
        }
    }
    else
    {
        // error handling
    }

    @endcode
*/
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Read( oC_UART_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet )   &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect )     &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outBuffer)          , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(Size)               , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (*Size) > 0               , oC_ErrorCode_SizeNotCorrect )
        )
    {
        if(Context->Dma != oC_UART_Dma_DontUse)
        {
            errorCode = oC_UART_LLD_ReadWithDma(Context->Channel,outBuffer,*Size);
        }

        if(oC_ErrorOccur(errorCode))
        {
            oC_Event_SetState(RxNotEmptyEvent,oC_Event_State_Inactive);

            oC_Time_t timeStart = oC_KTime_GetTimestamp();
            oC_Time_t timeEnd   = timeStart + Timeout;
            uint32_t  readBytes = 0;

            if(oC_UART_LLD_IsRxFifoEmpty(Context->Channel) && !oC_Event_WaitForState(RxNotEmptyEvent , Context->Channel , oC_Event_StateMask_Full , Timeout ))
            {
                errorCode = oC_ErrorCode_Timeout;
            }
            else
            {
                errorCode = oC_ErrorCode_NoneElementReceived;

                for(readBytes = 0 ; readBytes < (*Size) ; readBytes++)
                {
                    if(oC_UART_LLD_IsRxFifoEmpty(Context->Channel))
                    {
                        break;
                    }
                    errorCode = oC_ErrorCode_NoAllBytesRead;

                    outBuffer[readBytes] = oC_UART_LLD_GetData(Context->Channel);
                    Timeout              = timeEnd - oC_KTime_GetTimestamp();
                }

                if((*Size) == readBytes)
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    *Size = readBytes;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief receives data from the UART
 * 
 * The function receives data from the UART. The function receives data from the UART and stores it in the buffer. 
 * The function waits for the buffer to be full or the timeout to be reached. 
 * 
 * @param Context       The context of the UART
 * @param outBuffer     The output buffer
 * @param Size          The size of the buffer
 * @param Timeout       The timeout for receiving data
 * 
 * @return The error code or #oC_ErrorCode_None if success
 * 
 * Possible codes:
 * 
 *  Error Code                              | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet       | The module is not started yet
 *  #oC_ErrorCode_ContextNotCorrect         | The context is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The output address is not in RAM (Buffer)
 *  #oC_ErrorCode_SizeNotCorrect            | The size is not correct
 *  #oC_ErrorCode_Timeout                   | The timeout occurred
 * 
 * @note
 * More error codes can be returned by #oC_UART_LLD_ReadWithDma
 * 
 * @code{.c}
   
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);

    if(errorCode == oC_ErrorCode_None)
    {
        char buffer[100];
        oC_MemorySize_t size = 100;
        errorCode = oC_UART_Receive(context,buffer,size,oC_s(1));
        if(errorCode == oC_ErrorCode_None)
        {
            // do something with the received data
        }
    }
    else
    {
        // error handling
    }

    @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Receive( oC_UART_Context_t Context , char * outBuffer , oC_MemorySize_t Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet )   &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect )     &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outBuffer)          , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size > 0                  , oC_ErrorCode_SizeNotCorrect )
        )
    {
        if(Context->Dma != oC_UART_Dma_DontUse)
        {
            errorCode = oC_UART_LLD_ReadWithDma(Context->Channel,outBuffer,Size);
        }

        if(oC_ErrorOccur(errorCode))
        {
            oC_Event_SetState(RxNotEmptyEvent,oC_Event_State_Inactive);

            oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);
            uint32_t  readBytes   = 0;
            uint32_t  bytesToRead = Size;

            errorCode = oC_ErrorCode_Timeout;
            while(
                    bytesToRead > 0
                 && endTimestamp >= gettimestamp()
                 && (
                         !oC_UART_LLD_IsRxFifoEmpty(Context->Channel)
                      || oC_Event_WaitForState(RxNotEmptyEvent , Context->Channel , oC_Event_StateMask_Full , gettimeout(endTimestamp))
                      )
                  )
            {
                if(oC_UART_LLD_IsRxFifoEmpty(Context->Channel))
                {
                    continue;
                }
                outBuffer[readBytes] = oC_UART_LLD_GetData(Context->Channel);
                readBytes++;
                bytesToRead--;

                errorCode = oC_ErrorCode_NoAllBytesRead;
                if(bytesToRead == 0)
                {
                    errorCode = oC_ErrorCode_None;
                    break;
                }
                else
                {
                    oC_Event_SetState(RxNotEmptyEvent,oC_Event_State_Inactive);
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief writes data to the UART
 * 
 * The function writes data to the UART. The function writes data from the buffer to the UART. The function writes data until the buffer is
 * empty or the timeout is reached. The function can use the DMA for writing data - it depends on the configuration of the UART.
 * 
 * @param Context       The context of the UART
 * @param Buffer        The buffer with data
 * @param Size          The size of the buffer - the function will store the number of written bytes in this variable
 * @param Timeout       The timeout for writing data
 * 
 * @return The error code or #oC_ErrorCode_None if success 
 * 
 * Possible codes:
 *  Error Code                              | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet       | The module is not started yet
 *  #oC_ErrorCode_ContextNotCorrect         | The context is not correct
 *  #oC_ErrorCode_WrongAddress              | The buffer address is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The output address is not in RAM (Size)
 *  #oC_ErrorCode_SizeNotCorrect            | The size is not correct
 *  #oC_ErrorCode_Timeout                   | The timeout occurred
 *  #oC_ErrorCode_NotAllSent                | Not all bytes were sent
 * 
 *  @note 
 *  More error codes can be returned by #oC_UART_LLD_WriteWithDma, #oC_UART_LLD_IsTxFifoFull, #oC_Event_WaitForState, #oC_UART_LLD_PutData,
 *  #oC_UART_LLD_IsTxFifoFull functions
 * 
 * @see oC_UART_Read, oC_UART_Configure
 * 
 * <b>Example:</b>
 * @code{.c}
    
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);

    if(errorCode == oC_ErrorCode_None)
    {
        char buffer[100];
        oC_MemorySize_t size = 100;
        errorCode = oC_UART_Write(context,buffer,&size,oC_s(1));
        if(errorCode == oC_ErrorCode_None)
        {
            // do something with the written data
        }
    }
    else
    {
        // error handling
    }

    @endcode
*/
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Write( oC_UART_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet )   &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect )     &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Buffer)  , oC_ErrorCode_WrongAddress)           &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(Size)               , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , (*Size) > 0               , oC_ErrorCode_SizeNotCorrect )
        )
    {
        if(Context->Dma != oC_UART_Dma_DontUse)
        {
            errorCode = oC_UART_LLD_WriteWithDma(Context->Channel,Buffer,*Size);
        }

        if(oC_ErrorOccur(errorCode) && (Context->Dma != oC_UART_Dma_AlwaysUse))
        {
            uint32_t sentBytes = 0;

            oC_Event_SetState(TxNotFullEvent,oC_Event_State_Inactive);

            if(oC_UART_LLD_IsTxFifoFull(Context->Channel) && !oC_Event_WaitForState(TxNotFullEvent , Context->Channel , oC_Event_StateMask_Full , Timeout ))
            {
                errorCode = oC_ErrorCode_Timeout;
            }
            else
            {
                errorCode = oC_ErrorCode_NotAllSent;

                for(sentBytes = 0 ; sentBytes < (*Size) ; sentBytes++)
                {
                    if(oC_UART_LLD_IsTxFifoFull(Context->Channel))
                    {
                        break;
                    }
                    oC_UART_LLD_PutData(Context->Channel,Buffer[sentBytes]);
                }
                if((*Size) == sentBytes)
                {
                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    *Size = sentBytes;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles the IOCTL command
 * 
 * The function handles the IOCTL command. The function can handle the special commands like getting the driver version, clearing the Rx FIFO
 * or the normal commands like enabling or disabling the UART.
 * 
 * @param Context       The context of the UART
 * @param Command       The IOCTL command
 * @param Data          The data for the command
 * 
 * @return The error code or #oC_ErrorCode_None if success 
 * 
 * Possible codes:
 *  Error Code                              | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet       | The module is not started yet
 *  #oC_ErrorCode_ContextNotCorrect         | The context is not correct
 *  #oC_ErrorCode_CommandNotCorrect         | The command is not correct
 *  #oC_ErrorCode_CommandNotHandled         | The command is not handled
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The output address is not in RAM
 * 
 *  @note 
 *  More error codes can be returned by #oC_UART_Enable, #oC_UART_Disable, #oC_UART_LLD_ClearRxFifo functions
 * 
 * @see oC_UART_Enable, oC_UART_Disable
 * 
 * <b>Example:</b>
 * @code{.c}
    
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);

    if(errorCode == oC_ErrorCode_None)
    {
        // enable the UART
        errorCode = oC_UART_Ioctl(context,oC_Ioctl_NormalCommand_Enable,NULL);
        if(errorCode == oC_ErrorCode_None)
        {
            // do something with the UART
        }
    }
    else
    {
        // error handling
    }

    @endcode
*/
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Ioctl( oC_UART_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet )   &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)            , oC_ErrorCode_ContextNotCorrect )     &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect )
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Returns driver version
             */
            //==============================================================================================================================
            case oC_IoCtl_SpecialCommand_GetDriverVersion:
                if(isram(Data))
                {
                    *((uint32_t*)Data)  = UART.Version;
                    errorCode           = oC_ErrorCode_None;
                }
                else
                {
                    errorCode = oC_ErrorCode_OutputAddressNotInRAM;
                }
                break;
            //==============================================================================================================================
            /*
             * Enables UART operations
             */
            //==============================================================================================================================
            case oC_Ioctl_NormalCommand_Enable:
                errorCode = oC_UART_Enable(Context);
                break;
            //==============================================================================================================================
            /*
             * Disables UART operations
             */
            //==============================================================================================================================
            case oC_Ioctl_NormalCommand_Disable:
                errorCode = oC_UART_Disable(Context);
                break;
            //==============================================================================================================================
            /*
             * Clears Rx FIFO
             */
            //==============================================================================================================================
            case oC_IoCtl_SpecialCommand_ClearRxFifo:
                errorCode = oC_UART_LLD_ClearRxFifo(Context->Channel);
                break;
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief disables the UART
 * 
 * The function disables operations on the given UART channel. Once the UART is enabled again, the operations will be restored and the UART
 * will be ready to use.
 * 
 * @param Context       The context of the UART from #oC_UART_Configure function
 * 
 * @return The error code or #oC_ErrorCode_None if success
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet  | The module is not started yet
 *  #oC_ErrorCode_ContextNotCorrect    | The context is not correct
 *  #oC_ErrorCode_ImplementError       | The was unexpected error in the implementation
 * 
 * @note
 * The function can return more error codes - see the #oC_UART_LLD_DisableOperations function
 * 
 * @see oC_UART_Enable, oC_UART_Configure
 * 
 * <b>Example:</b>
 * @code{.c}
    
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);

    if(errorCode == oC_ErrorCode_None)
    {
        // disable the UART
        errorCode = oC_UART_Disable(context);
        if(errorCode == oC_ErrorCode_None)
        {
            // the UART is disabled
        }
    }
    else
    {
        // error handling
    }

    @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Disable( oC_UART_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet )   &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)            , oC_ErrorCode_ContextNotCorrect )
        )
    {
        errorCode = oC_UART_LLD_DisableOperations(Context->Channel);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief enables the UART
 * 
 * The function enables operations on the given UART channel. Once the UART is enabled, the operations will be restored and the UART will be
 * ready to use.
 * 
 * @param Context       The context of the UART from #oC_UART_Configure function
 * 
 * @return The error code or #oC_ErrorCode_None if success
 * 
 * Possible codes:
 *  Error Code                         | Description
 * ------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                 | Operation success
 *  #oC_ErrorCode_ModuleNotStartedYet  | The module is not started yet
 *  #oC_ErrorCode_ContextNotCorrect    | The context is not correct
 *  #oC_ErrorCode_ImplementError       | The was unexpected error in the implementation
 * 
 * @note
 * The function can return more error codes - see the #oC_UART_LLD_EnableOperations function
 * 
 * @see oC_UART_Disable, oC_UART_Configure
 * 
 * <b>Example:</b>
 * @code{.c}
    
    // configuration of the UART
    oC_UART_Config_t config = {
        .Rx         = oC_Pin_PC4,
        .Tx         = oC_Pin_PC5,
        .WordLength = oC_UART_WordLength_8,
        .BitRate    = 9600,
        .Parity     = oC_UART_Parity_None,
        .StopBit    = oC_UART_StopBit_1,
        .BitOrder   = oC_UART_BitOrder_LSB,
        .Invert     = oC_UART_Invert_Disable,
        .Dma        = oC_UART_Dma_DontUse,
        .Loopback   = false
    };

    // context of the UART
    oC_UART_Context_t context = NULL;

    // call the configuration function
    oC_ErrorCode_t errorCode = oC_UART_Configure(&config,&context);

    if(errorCode == oC_ErrorCode_None)
    {
        // enable the UART
        errorCode = oC_UART_Enable(context);
        if(errorCode == oC_ErrorCode_None)
        {
            // the UART is enabled
        }
    }
    else
    {
        // error handling
    }

    @endcode
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_UART_Enable( oC_UART_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true            , oC_ErrorCode_ModuleNotStartedYet )   &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)            , oC_ErrorCode_ContextNotCorrect )
        )
    {
        errorCode = oC_UART_LLD_EnableOperations(Context->Channel);
    }

    return errorCode;
}


#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the configuration is correct
 */
//==========================================================================================================================================
static inline bool IsConfigCorrect( const oC_UART_Config_t * Config )
{
    return isaddresscorrect(Config);
}

//==========================================================================================================================================
/**
 * @brief checks if the context is correct
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_UART_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_UartContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief finds a free channel for the given pins
 */
//==========================================================================================================================================
static oC_ErrorCode_t FindFreeChannelForPins( oC_Pins_t Tx , oC_Pins_t Rx , oC_UART_Channel_t * outChannel )
{
    oC_ErrorCode_t  errorCode        = oC_ErrorCode_ImplementError;
    oC_UART_Pin_t * txPinsArray      = NULL;
    oC_UART_Pin_t * rxPinsArray      = NULL;
    uint32_t        txArraySize      = 0;
    uint32_t        rxArraySize      = 0;
    oC_UART_Pin_t   txModulePin      = 0;
    oC_UART_Pin_t   rxModulePin      = 0;
    bool            txModulePinFound = false;
    bool            rxModulePinFound = false;

    oC_IntMan_EnterCriticalSection();

    /* *******************************************************************
     *                      Transmit/Receive Mode
     * ******************************************************************/
    if(Tx != 0 && Rx != 0)
    {
        /* Searching for size of module pins arrays */
        if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Tx,NULL,&txArraySize,oC_UART_PinFunction_Tx)) &&
           oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Rx,NULL,&rxArraySize,oC_UART_PinFunction_Rx)) &&
           ErrorCondition(txArraySize > 0 && rxArraySize > 0 , oC_ErrorCode_NoneOfModulePinAssigned)
           )
        {
            /* Allocation memory for module pins array */
            txPinsArray = ksmartalloc(sizeof(oC_UART_Pin_t)*txArraySize,&ModuleAllocator,AllocationFlags_CanWait1Second);
            rxPinsArray = ksmartalloc(sizeof(oC_UART_Pin_t)*rxArraySize,&ModuleAllocator,AllocationFlags_CanWait1Second);

            if(ErrorCondition(txPinsArray != NULL && rxPinsArray != NULL , oC_ErrorCode_AllocationError))
            {
                /* Searching for size of module pins arrays */
                if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Tx,txPinsArray,&txArraySize,oC_UART_PinFunction_Tx)) &&
                   oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Rx,rxPinsArray,&rxArraySize,oC_UART_PinFunction_Rx))
                   )
                {
                    /* Searching for free Tx and Rx pins with the same channel */
                    oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(txPinsArray,txArraySize,txPin)
                    {
                        oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(rxPinsArray,rxArraySize,rxPin)
                        {
                            oC_UART_Channel_t txChannel = oC_UART_LLD_GetChannelOfModulePin(*txPin);
                            oC_UART_Channel_t rxChannel = oC_UART_LLD_GetChannelOfModulePin(*rxPin);


                            if(ErrorCondition(txChannel == rxChannel               , oC_ErrorCode_WrongModulePinChannel) &&
                               ErrorCondition(!oC_UART_LLD_IsChannelUsed(txChannel), oC_ErrorCode_ChannelIsUsed)
                               )
                            {
                                *outChannel         = txChannel;
                                txModulePin         = *txPin;
                                rxModulePin         = *rxPin;
                                txModulePinFound    = true;
                                rxModulePinFound    = true;
                                break;
                            }
                            else
                            {
                                driverlog(oC_LogType_Error, "Channel %s is used\n", oC_Channel_GetName(txChannel));
                            }
                        }
                    }
                }
            }
        }
    }
    /* *******************************************************************
     *                      Transmit Only Mode
     * ******************************************************************/
    else if(Tx != 0)
    {
        /* Searching for size of module pins arrays */
        if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Tx,NULL,&txArraySize,oC_UART_PinFunction_Tx)) &&
           ErrorCondition(txArraySize > 0 , oC_ErrorCode_NoneOfModulePinAssigned)
           )
        {
            /* Allocation memory for module pins array */
            txPinsArray = ksmartalloc(sizeof(oC_UART_Pin_t)*txArraySize,&ModuleAllocator,AllocationFlags_CanWait1Second);

            if(ErrorCondition(txPinsArray != NULL , oC_ErrorCode_AllocationError))
            {
                /* Searching for size of module pins arrays */
                if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Tx,txPinsArray,&txArraySize,oC_UART_PinFunction_Tx)))
                {
                    /* Searching for free Tx pins with the same channel */
                    oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(txPinsArray,txArraySize,txPin)
                    {
                        oC_UART_Channel_t txChannel = oC_UART_LLD_GetChannelOfModulePin(*txPin);

                        if(ErrorCondition(!oC_UART_LLD_IsChannelUsed(txChannel) , oC_ErrorCode_ChannelIsUsed))
                        {
                            *outChannel      = txChannel;
                            txModulePin      = *txPin;
                            txModulePinFound = true;
                            break;
                        }
                    }
                }
            }
        }
    }
    /* *******************************************************************
     *                      Receive Only Mode
     * ******************************************************************/
    else if(Rx != 0)
    {
        /* Searching for size of module pins arrays */
        if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Rx,NULL,&rxArraySize,oC_UART_PinFunction_Rx)) &&
           ErrorCondition(rxArraySize > 0 , oC_ErrorCode_NoneOfModulePinAssigned)
           )
        {
            /* Allocation memory for module pins array */
            rxPinsArray = ksmartalloc(sizeof(oC_UART_Pin_t)*rxArraySize,&ModuleAllocator,AllocationFlags_CanWait1Second);

            if(ErrorCondition(rxPinsArray != NULL , oC_ErrorCode_AllocationError))
            {
                /* Searching for size of module pins arrays */
                if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ReadModulePinsOfPin(Rx,rxPinsArray,&rxArraySize,oC_UART_PinFunction_Rx)))
                {
                    /* Searching for free Rx pins with the same channel */
                    oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(rxPinsArray,rxArraySize,rxPin)
                    {
                        oC_UART_Channel_t rxChannel = oC_UART_LLD_GetChannelOfModulePin(*rxPin);

                        if(ErrorCondition(!oC_UART_LLD_IsChannelUsed(rxChannel) , oC_ErrorCode_ChannelIsUsed))
                        {
                            *outChannel       = rxChannel;
                            rxModulePin       = *rxPin;
                            rxModulePinFound  = true;
                            break;
                        }
                    }
                }
            }
        }
    }
    /* *******************************************************************
     *                      None pin is given
     * ******************************************************************/
    else
    {
        errorCode = oC_ErrorCode_ModulePinIsNotGiven;
    }

    /* *******************************************************************
     *                      Both pins found
     * ******************************************************************/
    if(txModulePinFound && rxModulePinFound)
    {
        if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ConnectModulePin(txModulePin)) &&
           oC_AssignErrorCode(&errorCode , oC_UART_LLD_ConnectModulePin(rxModulePin)) &&
           oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetChannelUsed(*outChannel))
           )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            oC_UART_LLD_DisconnectModulePin(txModulePin);
            oC_UART_LLD_DisconnectModulePin(rxModulePin);
        }
    }
    /* *******************************************************************
     *                      Transmit pin found
     * ******************************************************************/
    else if(txModulePin)
    {
        if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ConnectModulePin(txModulePin)) &&
           oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetChannelUsed(*outChannel))
           )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            oC_UART_LLD_DisconnectModulePin(txModulePin);
        }
    }
    /* *******************************************************************
     *                      Receive pin found
     * ******************************************************************/
    else if(rxModulePin)
    {
        if(oC_AssignErrorCode(&errorCode , oC_UART_LLD_ConnectModulePin(rxModulePin)) &&
           oC_AssignErrorCode(&errorCode , oC_UART_LLD_SetChannelUsed(*outChannel))
           )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            oC_UART_LLD_DisconnectModulePin(rxModulePin);
        }
    }
    /* *******************************************************************
     *                      None of pin found
     * ******************************************************************/
    else
    {
        /* Module pin not found */
    }

    oC_IntMan_ExitCriticalSection();

    /* *******************************************************************
     *                      Releasing memory
     * ******************************************************************/
    if(txPinsArray != NULL)
    {
        if( !ksmartfree(txPinsArray,sizeof(oC_UART_Pin_t)*txArraySize,AllocationFlags_CanWait1Second) )
        {
            oC_SaveError("UART:FindFreeChannelForPins" , oC_ErrorCode_ReleaseError);
        }
    }
    if(rxPinsArray != NULL)
    {
        if( !ksmartfree(rxPinsArray,sizeof(oC_UART_Pin_t)*rxArraySize,AllocationFlags_CanWait1Second) )
        {
            oC_SaveError("UART:FindFreeChannelForPins" , oC_ErrorCode_ReleaseError);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief initializes the UART
 */
//==========================================================================================================================================
static void RxNotEmptyInterruptHandler( oC_UART_Channel_t Channel )
{
    oC_Event_SetState(RxNotEmptyEvent,Channel);
}

//==========================================================================================================================================
/**
 * @brief initializes the UART
 */
//==========================================================================================================================================
static void TxNotFullInterruptHandler( oC_UART_Channel_t Channel )
{
    oC_Event_SetState(TxNotFullEvent,Channel);
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
