/** ****************************************************************************************************************************************
 *
 * @file       oc_gtd.c
 * 
 * File based on driver.c Ver 1.1.0
 *
 * @brief      The file with source for GTD driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2017-02-06 - 19:07:41)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_gtd.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_vt100.h>
#include <oc_screenman.h>
#include <oc_mutex.h>
#include <ctype.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         GTD
#define DRIVER_FILE_NAME    "gtd"
#define DRIVER_TYPE         COMMUNICATION_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_GTD_Configure
#define DRIVER_UNCONFIGURE  oC_GTD_Unconfigure
#define DRIVER_TURN_ON      oC_GTD_TurnOn
#define DRIVER_TURN_OFF     oC_GTD_TurnOff
#define IS_TURNED_ON        oC_GTD_IsTurnedOn
#define HANDLE_IOCTL        oC_GTD_Ioctl
#define READ_FROM_DRIVER    oC_GTD_Read
#define WRITE_TO_DRIVER     oC_GTD_Write

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores information about the window
 */
//==========================================================================================================================================
typedef struct
{
    oC_Pixel_Position_t         Position;   //!< Top left corner of the terminal window
    oC_Pixel_ResolutionUInt_t   Width;      //!< Width of the terminal window
    oC_Pixel_ResolutionUInt_t   Height;     //!< Height of the terminal window
} Window_t;

//==========================================================================================================================================
/**
 * @brief stores console attributes
 */
//==========================================================================================================================================
typedef enum
{
    Attributes_Bright           = (1<<0) , //!< Bright
    Attributes_Dim              = (1<<1) , //!< Dim
    Attributes_G0Font           = (1<<2) , //!< G0Font
    Attributes_Underscore       = (1<<3) , //!< Underscore
    Attributes_Blink            = (1<<4) , //!< Blink
    Attributes_Reverse          = (1<<5) , //!< Reverse
    Attributes_Hidden           = (1<<6) , //!< Hidden
    Attributes_Default          = Attributes_G0Font
} Attributes_t;

//==========================================================================================================================================
/**
 * @brief stores VT100 border type (needed for drawing)
 */
//==========================================================================================================================================
typedef enum
{
    BorderType_LeftToMiddle     = (1<<0) ,
    BorderType_RightToMiddle    = (1<<1) ,
    BorderType_UpToMiddle       = (1<<2) ,
    BorderType_DownToMiddle     = (1<<3) ,
    BorderType_LU   = BorderType_LeftToMiddle | BorderType_UpToMiddle,
    BorderType_LD   = BorderType_LeftToMiddle | BorderType_DownToMiddle ,
    BorderType_DR   = BorderType_DownToMiddle | BorderType_RightToMiddle ,
    BorderType_UR   = BorderType_UpToMiddle   | BorderType_RightToMiddle ,
    BorderType_LUDR = BorderType_LeftToMiddle | BorderType_UpToMiddle    | BorderType_DownToMiddle | BorderType_RightToMiddle ,
    BorderType_LR   = BorderType_LeftToMiddle | BorderType_RightToMiddle ,
    BorderType_UDR  = BorderType_UpToMiddle   | BorderType_DownToMiddle  | BorderType_RightToMiddle ,
    BorderType_LUD  = BorderType_LeftToMiddle | BorderType_UpToMiddle    | BorderType_DownToMiddle ,
    BorderType_LUR  = BorderType_LeftToMiddle | BorderType_UpToMiddle    | BorderType_RightToMiddle ,
    BorderType_LDR  = BorderType_LeftToMiddle | BorderType_DownToMiddle  | BorderType_RightToMiddle ,
    BorderType_UD   = BorderType_UpToMiddle   | BorderType_DownToMiddle ,
} BorderType_t;

//==========================================================================================================================================
/**
 * @brief stores information about the terminal cell
 */
//==========================================================================================================================================
typedef struct
{
    oC_Color_t    BackgroundColor;        //!< Background color of the cell
    oC_Color_t    ForegroundColor;        //!< Foreground color of the cell
    char          C;                      //!< Character to print
    Attributes_t  Attributes;             //!< Attributes of the cell
} Cell_t;

//==========================================================================================================================================
/**
 * @brief stores GTD context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
typedef struct Context_t
{
    oC_ObjectControl_t          ObjectControl;          //!< Magic number used for object validation
    oC_Screen_t                 Screen;                 //!< Screen associated with the given terminal
    const char *                Name;                   //!< Name of the screen to display terminal
    oC_ColorFormat_t            ColorFormat;            //!< Format of stored colors
    Window_t                    Window;                 //!< Structure with data of the window to display
    oC_Font_t                   FontG0;                 //!< Font used to display characters as G0
    oC_Font_t                   FontG1;                 //!< Font used to display characters as G1
    oC_GTD_ColorsArray_t        ColorsArray;            //!< Array with colors definitions
    oC_TGUI_Position_t          CursorPosition;         //!< Current position of the cursor
    oC_TGUI_Position_t          SavedCursorPosition;    //!< Saved position of the cursor
    oC_TGUI_Position_t          HomePosition;           //!< Home position of the cursor
    oC_Color_t                  BackgroundColor;        //!< Current color of the terminal background
    oC_Color_t                  ForegroundColor;        //!< Current color of the terminal foreground
    Attributes_t                Attributes;             //!< Current attributes
    Attributes_t                SavedAttributes;        //!< Saved attributes
    oC_ColorMap_t *             ColorMap;               //!< Pointer to the color map of the screen
    oC_Mutex_t                  ObjectBusy;             //!< Mutex to mark the object as busy
    oC_TGUI_Column_t            NumberOfColumns;        //!< Number of columns in the terminal
    oC_TGUI_Line_t              NumberOfLines;          //!< Number of lines in the terminal
    Cell_t *                    Cells;                  //!< Stored cells state
    oC_Pixel_ResolutionUInt_t   G0FontWidth;            //!< Width of the font G0
    oC_Pixel_ResolutionUInt_t   G1FontWidth;            //!< Width of the font G1
    oC_Pixel_ResolutionUInt_t   G0FontHeight;           //!< Height of the font G0
    oC_Pixel_ResolutionUInt_t   G1FontHeight;           //!< Height of the font G1
    bool                        LineWrappingDisabled;   //!< Line wrapping disabled
} Context_t;

//==========================================================================================================================================
/**
 * @brief handles VT100 command
 *
 * The function handles VT100 command with the given terminal
 *
 * @param Context       Context of the driver
 * @param Command       Pointer to the parsed command
 *
 * @return code of error
 */
//==========================================================================================================================================
typedef oC_ErrorCode_t (*HandleCommandFunction_t)( Context_t * Context, void * Command );

//==========================================================================================================================================
/**
 * @brief stores definition of the VT100 command
 */
//==========================================================================================================================================
typedef struct
{
    HandleCommandFunction_t     HandlerFunction;    //!< Function that should be called if the command occurs
    const char *                Vt100String;        //!< String that is used for recognizing commands
} CommandDefinition_t;

//==========================================================================================================================================
/**
 * @brief helpful definition for storing command data
 */
//==========================================================================================================================================
typedef struct Command_t
{
    const CommandDefinition_t *     Definition;     //!< Reference to the definition of the command
    union
    {
        struct
        {
            uint32_t                        Argument1;      //!< First argument of the command   (if given)
            uint32_t                        Argument2;      //!< Second argument of the command  (if given)
            uint32_t                        Argument3;      //!< Second argument of the command  (if given)
            uint32_t                        Argument4;      //!< Second argument of the command  (if given)
        };
        uint32_t            Arguments[4];
    };
} Command_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline oC_ErrorCode_t        CheckConfiguration          ( const oC_GTD_Config_t * Config  );
static inline bool                  IsContextCorrect            ( oC_GTD_Context_t        Context );
static inline Context_t *           Context_New                 ( const oC_GTD_Config_t * Config  );
static inline bool                  Context_Delete              ( oC_GTD_Context_t      * Context );
static        Cell_t *              GetCell                     ( Context_t * Context, oC_TGUI_Position_t Position );
static        bool                  ParseCommand                ( Context_t * Context, const char * Buffer, uint32_t * Size , Command_t * Command );
static        bool                  ReadCommand                 ( Context_t * Context, const char * Buffer, uint32_t * Size , Command_t * outCommand );
static        oC_ErrorCode_t        PerformCommand              ( Context_t * Context, const char * Buffer, uint32_t * Size );
static        void                  ClearCursor                 ( Context_t * Context );
static        void                  DrawCursor                  ( Context_t * Context );
static        void                  DrawCell                    ( Context_t * Context, Cell_t * Cell , oC_TGUI_Position_t Position );
static        void                  DrawCharacter               ( Context_t * Context, char C );
static        void                  RefreshScreen               ( Context_t * Context );
static        oC_Pixel_Position_t   GetPixelPosition            ( Context_t * Context, oC_TGUI_Position_t Position );
static        void                  IncrementCursorPosition     ( Context_t * Context );

/* VT100 Interface Commands Functions */
static        oC_ErrorCode_t    VT100_ResetDevice           ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_SelectFont            ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_EnableLineWrap        ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DisableLineWrap       ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_SetCursorHome         ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_MoveCursorToHome      ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_MoveCursorUp          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_MoveCursorDown        ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_MoveCursorForward     ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_MoveCursorBackward    ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_ForceCursorPosition   ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_SaveCursorPosition    ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_UnsaveCursorPosition  ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_SaveCursorAndAttrs    ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_RestoreCursorAndAttrs ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_ScrollScreen          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_ScrollDown            ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_ScrollUp              ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_SetTab                ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_ClearTab              ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_ClearAllTabs          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_EraseEndOfTheLine     ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_EraseStartOfTheLine   ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_EraseLine             ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_EraseDown             ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_EraseUp               ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_EraseScreen           ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_SetAttribute          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderLU          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderLD          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderDR          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderUR          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderLUDR        ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderLR          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderUDR         ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderLUD         ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderLUR         ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderLDR         ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawBorderUD          ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawingLineSupport    ( Context_t * Context, void * Command );
static        oC_ErrorCode_t    VT100_DrawRealLine          ( Context_t * Context, void * Command );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all GTD driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "gtd driver"
};

//==========================================================================================================================================
/**
 * @brief array with default colors for VT100 console
 */
//==========================================================================================================================================
static const oC_GTD_ColorsArray_t DefaultColors = {
                [oC_GTD_TerminalColorIndex_DefaultBackground] = { .TerminalColorId = oC_TGUI_Color_Default        , .Color = oC_Color_Black     , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_DefaultForeground] = { .TerminalColorId = oC_TGUI_Color_Default        , .Color = oC_Color_White     , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Cursor           ] = { .TerminalColorId = oC_TGUI_Color_Default        , .Color = 0x00AA00           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Black            ] = { .TerminalColorId = oC_TGUI_Color_Black          , .Color = oC_Color_Black     , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Red              ] = { .TerminalColorId = oC_TGUI_Color_Red            , .Color = oC_Color_Red       , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Green            ] = { .TerminalColorId = oC_TGUI_Color_Green          , .Color = 0x00BB00           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Yellow           ] = { .TerminalColorId = oC_TGUI_Color_Yellow         , .Color = 0xBBBB00           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Blue             ] = { .TerminalColorId = oC_TGUI_Color_Blue           , .Color = 0x0000BB           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Magenda          ] = { .TerminalColorId = oC_TGUI_Color_Magenda        , .Color = 0xBB00BB           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_Cyan             ] = { .TerminalColorId = oC_TGUI_Color_Cyan           , .Color = 0x00BBBB           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_LightGray        ] = { .TerminalColorId = oC_TGUI_Color_LightGray      , .Color = 0xBBBBBB           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_DarkGray         ] = { .TerminalColorId = oC_TGUI_Color_DarkGray       , .Color = 0x555555           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_LightRed         ] = { .TerminalColorId = oC_TGUI_Color_LightRed       , .Color = 0xFF5555           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_LightGreen       ] = { .TerminalColorId = oC_TGUI_Color_LightGreen     , .Color = 0x55FF55           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_LightYellow      ] = { .TerminalColorId = oC_TGUI_Color_LightYellow    , .Color = 0xFFFF55           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_LightBlue        ] = { .TerminalColorId = oC_TGUI_Color_LightBlue      , .Color = 0x5555FF           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_LightMagenda     ] = { .TerminalColorId = oC_TGUI_Color_LightMagenda   , .Color = 0xFF55FF           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_LightCyan        ] = { .TerminalColorId = oC_TGUI_Color_LightCyan      , .Color = 0x55FFFF           , .ColorFormat = oC_ColorFormat_RGB888  } ,
                [oC_GTD_TerminalColorIndex_White            ] = { .TerminalColorId = oC_TGUI_Color_White          , .Color = 0xDDDDDD           , .ColorFormat = oC_ColorFormat_RGB888  } ,
};

//==========================================================================================================================================
/**
 * @brief array with list of handled commands
 */
//==========================================================================================================================================
static const CommandDefinition_t CommandsDefinitions[] = {
                { .Vt100String = oC_VT100_DETECT_DL_SUPPORT                , .HandlerFunction = VT100_DrawingLineSupport    } ,
                { .Vt100String = oC_VT100_DRAW_LINE("#","#","#","#")       , .HandlerFunction = VT100_DrawRealLine          } ,
                { .Vt100String = oC_VT100_BORDER_LU                        , .HandlerFunction = VT100_DrawBorderLU          } ,
                { .Vt100String = oC_VT100_BORDER_LD                        , .HandlerFunction = VT100_DrawBorderLD          } ,
                { .Vt100String = oC_VT100_BORDER_DR                        , .HandlerFunction = VT100_DrawBorderDR          } ,
                { .Vt100String = oC_VT100_BORDER_UR                        , .HandlerFunction = VT100_DrawBorderUR          } ,
                { .Vt100String = oC_VT100_BORDER_LUDR                      , .HandlerFunction = VT100_DrawBorderLUDR        } ,
                { .Vt100String = oC_VT100_BORDER_LR                        , .HandlerFunction = VT100_DrawBorderLR          } ,
                { .Vt100String = oC_VT100_BORDER_UDR                       , .HandlerFunction = VT100_DrawBorderUDR         } ,
                { .Vt100String = oC_VT100_BORDER_LUD                       , .HandlerFunction = VT100_DrawBorderLUD         } ,
                { .Vt100String = oC_VT100_BORDER_LUR                       , .HandlerFunction = VT100_DrawBorderLUR         } ,
                { .Vt100String = oC_VT100_BORDER_LDR                       , .HandlerFunction = VT100_DrawBorderLDR         } ,
                { .Vt100String = oC_VT100_BORDER_UD                        , .HandlerFunction = VT100_DrawBorderUD          } ,
                { .Vt100String = oC_VT100_RESET_DEVICE                     , .HandlerFunction = VT100_ResetDevice           } ,
                { .Vt100String = oC_VT100_FONT_SET_G0                      , .HandlerFunction = VT100_SelectFont            } ,
                { .Vt100String = oC_VT100_FONT_SET_G1                      , .HandlerFunction = VT100_SelectFont            } ,
                { .Vt100String = oC_VT100_ENABLE_LINE_WRAP                 , .HandlerFunction = VT100_EnableLineWrap        } ,
                { .Vt100String = oC_VT100_DISABLE_LINE_WRAP                , .HandlerFunction = VT100_DisableLineWrap       } ,
                { .Vt100String = oC_VT100_SET_CURSOR_HOME("#","#")         , .HandlerFunction = VT100_SetCursorHome         } ,
                { .Vt100String = oC_VT100_CURSOR_HOME                      , .HandlerFunction = VT100_MoveCursorToHome      } ,
                { .Vt100String = oC_VT100_CURSOR_UP("#")                   , .HandlerFunction = VT100_MoveCursorUp          } ,
                { .Vt100String = oC_VT100_CURSOR_DOWN("#")                 , .HandlerFunction = VT100_MoveCursorDown        } ,
                { .Vt100String = oC_VT100_CURSOR_FORWARD("#")              , .HandlerFunction = VT100_MoveCursorForward     } ,
                { .Vt100String = oC_VT100_CURSOR_BACKWARD("#")             , .HandlerFunction = VT100_MoveCursorBackward    } ,
                { .Vt100String = oC_VT100_FORCE_CURSOR_POSITION("#","#")   , .HandlerFunction = VT100_ForceCursorPosition   } ,
                { .Vt100String = oC_VT100_SAVE_CURSOR                      , .HandlerFunction = VT100_SaveCursorPosition    } ,
                { .Vt100String = oC_VT100_UNSAVE_CURSOR                    , .HandlerFunction = VT100_UnsaveCursorPosition  } ,
                { .Vt100String = oC_VT100_SAVE_CURSOR_AND_ATTRS            , .HandlerFunction = VT100_SaveCursorAndAttrs    } ,
                { .Vt100String = oC_VT100_RESTORE_CURSOR_AND_ATTRS         , .HandlerFunction = VT100_RestoreCursorAndAttrs } ,
                { .Vt100String = oC_VT100_SCROLL_SCREEN                    , .HandlerFunction = VT100_ScrollScreen          } ,
                { .Vt100String = oC_VT100_SCROLL_SCREEN_TO("#","#")        , .HandlerFunction = VT100_ScrollScreen          } ,
                { .Vt100String = oC_VT100_SCROLL_DOWN                      , .HandlerFunction = VT100_ScrollDown            } ,
                { .Vt100String = oC_VT100_SCROLL_UP                        , .HandlerFunction = VT100_ScrollUp              } ,
                { .Vt100String = oC_VT100_SET_TAB                          , .HandlerFunction = VT100_SetTab                } ,
                { .Vt100String = oC_VT100_CLEAR_TAB                        , .HandlerFunction = VT100_ClearTab              } ,
                { .Vt100String = oC_VT100_CLEAR_ALL_TABS                   , .HandlerFunction = VT100_ClearAllTabs          } ,
                { .Vt100String = oC_VT100_ERASE_END_OF_LINE                , .HandlerFunction = VT100_EraseEndOfTheLine     } ,
                { .Vt100String = oC_VT100_ERASE_START_OF_LINE              , .HandlerFunction = VT100_EraseStartOfTheLine   } ,
                { .Vt100String = oC_VT100_ERASE_LINE                       , .HandlerFunction = VT100_EraseLine             } ,
                { .Vt100String = oC_VT100_ERASE_DOWN                       , .HandlerFunction = VT100_EraseDown             } ,
                { .Vt100String = oC_VT100_ERASE_UP                         , .HandlerFunction = VT100_EraseUp               } ,
                { .Vt100String = oC_VT100_ERASE_SCREEN                     , .HandlerFunction = VT100_EraseScreen           } ,
                { .Vt100String = oC_VT100_CONSOLE_ATTRIBUTE("#")           , .HandlerFunction = VT100_SetAttribute          } ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the GTD module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GTD_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_GTD))
    {
        /* This must be always at the end of the function */
        oC_Module_TurnOn(oC_Module_GTD);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the GTD driver
 *
 * The function for turning off the GTD driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GTD_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GTD))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_GTD);

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_GTD_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_GTD);
}

//==========================================================================================================================================
/**
 * @brief configures GTD pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_GTD_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_WrongConfigAddress              | Address of the `Config` is not correct
 *  oC_ErrorCode_OutputAddressNotInRAM           | The `outContext` parameter does not point to the RAM section
 *  oC_ErrorCode_WrongAddress                    | Address of the `Config->ScreenName` or `Config->ColorsArray` parameter is not correct
 *  oC_ErrorCode_StringIsEmpty                   | `Config->ScreenName` stores string with 0 length
 *  oC_ErrorCode_NoSuchScreen                    | Cannot find screen named `Config->ScreenName`
 *  oC_ErrorCode_NotConfiguredYet                | The given screen has not been configured before
 *  oC_ErrorCode_PositionNotCorrect              | `Config->Position` is not correct (or window does not fit in the screen)
 *  oC_ErrorCode_WidthNotCorrect                 | `Config->Width` cannot be 0
 *  oC_ErrorCode_WidthTooBig                     | `Config->Width` is too big
 *  oC_ErrorCode_HeightNotCorrect                | `Config->Height` cannot be 0
 *  oC_ErrorCode_HeightTooBig                    | `Config->Height` is too big
 *  oC_ErrorCode_FontNotCorrect                  | `Config->FontG0` or `Config->FontG1` is not correct
 *  oC_ErrorCode_AllocationError                 | Cannot allocate memory for a context
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GTD_Configure( const oC_GTD_Config_t * Config , oC_GTD_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GTD))
    {
        if(
           ErrorCondition( isaddresscorrect(Config)                     , oC_ErrorCode_WrongConfigAddress       )
        && ErrorCondition( isram(outContext)                            , oC_ErrorCode_OutputAddressNotInRAM    )
        && ErrorCode     ( CheckConfiguration(Config) )
        && ErrorCondition( (*outContext = Context_New(Config)) != NULL  , oC_ErrorCode_AllocationError          )
           )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_GTD_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error or `oC_ErrorCode_None` if success
 *
 * List of standard error codes, that can be returned by the function:
 *
 * Code of error                                 | Description
 * ----------------------------------------------|------------------------------------------------------------------------------------------
 *  oC_ErrorCode_ModuleNotStartedYet             | The module has not been turned on earlier
 *  oC_ErrorCode_WrongConfigAddress              | Address of the `Config` is not correct
 *  oC_ErrorCode_OutputAddressNotInRAM           | The `outContext` parameter does not point to the RAM section
 *  oC_ErrorCode_ContextNotCorrect               | `outContext` stores incorrect `Context` pointer
 *  oC_ErrorCode_ReleaseError                    | Cannot release memory allocated for the context
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GTD_Unconfigure( const oC_GTD_Config_t * Config , oC_GTD_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_GTD))
    {
        if(
            ErrorCondition( isaddresscorrect(Config)        , oC_ErrorCode_WrongConfigAddress       )
         && ErrorCondition( isram(outContext)               , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( IsContextCorrect(*outContext)   , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( Context_Delete(outContext)      , oC_ErrorCode_ReleaseError             )
           )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GTD_Ioctl( oC_GTD_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_GTD)
     && ErrorCondition( isaddresscorrect(Context)            , oC_ErrorCode_WrongAddress        )
     && ErrorCondition( oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect   )
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief reads buffer from the driver
 * 
 * The function is for reading buffer by using GTD driver. It is called when someone will read the driver file. 
 *
 * @param Context       Context of the driver
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer on input, on output number of written bytes
 * @param Timeout       Maximum time for operation
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GTD_Read( oC_GTD_Context_t Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_GTD)
     && ErrorCondition( IsContextCorrect(Context)            , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isram(outBuffer)                     , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isram(Size)                          , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                          , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                         , oC_ErrorCode_TimeNotCorrect          )
        )
    {
        errorCode = oC_ErrorCode_NotHandledByDriver;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief write buffer to the driver
 * 
 * The function is for writing buffer by using GTD driver. It is called when someone will write the driver file. 
 *
 * @param Context       Context of the driver
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer on input, on output number of written bytes
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_GTD_Write( oC_GTD_Context_t Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification(&errorCode , oC_Module_GTD)
     && ErrorCondition( IsContextCorrect(Context)                   , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isaddresscorrect(Buffer)                    , oC_ErrorCode_WrongAddress            )
     && ErrorCondition( isram(Size)                                 , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0                                 , oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( Timeout >= 0                                , oC_ErrorCode_TimeNotCorrect          )
     && ErrorCondition( oC_Screen_IsConfigured(Context->Screen)     , oC_ErrorCode_NotConfiguredYet        )
     && ErrorCondition( oC_Mutex_Take(Context->ObjectBusy,Timeout)  , oC_ErrorCode_Timeout                 )
        )
    {
        uint32_t leftLength = *Size;

        errorCode = oC_ErrorCode_None;

        ClearCursor(Context);

        for(uint32_t i = 0; i < (*Size); i++)
        {
            if(Buffer[i] == oC_VT100_ESC_CHAR)
            {
                leftLength = (*Size) - i;
                if( ErrorCode( PerformCommand(Context,&Buffer[i],&leftLength) ) )
                {
                    i += leftLength - 1;
                }
                else if(errorCode != oC_ErrorCode_CommandNotHandled)
                {
                    break;
                }
            }
            else
            {
                DrawCharacter(Context,Buffer[i]);
            }
        }

        DrawCursor(Context);

        oC_Mutex_Give(Context->ObjectBusy);
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the configuration is correct
 */
//==========================================================================================================================================
static inline oC_ErrorCode_t CheckConfiguration( const oC_GTD_Config_t * Config  )
{
    oC_ErrorCode_t      errorCode   = oC_ErrorCode_ImplementError;
    oC_Screen_t         screen      = oC_ScreenMan_GetScreen(Config->ScreenName);
    oC_ColorMap_t *     colorMap    = NULL;
    oC_Pixel_Position_t endPosition = { .X = Config->Position.X + Config->Width - 1, .Y = Config->Position.Y + Config->Height - 1 };

    if(
       ErrorCondition( isaddresscorrect(Config->ScreenName)                                , oC_ErrorCode_WrongAddress         )
    && ErrorCondition( strlen(Config->ScreenName) > 0                                      , oC_ErrorCode_StringIsEmpty        )
    && ErrorCondition( screen != NULL                                                      , oC_ErrorCode_NoSuchScreen         )
    && ErrorCode     ( oC_Screen_ReadColorMap(screen,&colorMap)                                                                )
    && ErrorCondition( oC_ColorMap_IsPositionCorrect(colorMap,Config->Position)            , oC_ErrorCode_PositionNotCorrect   )
    && ErrorCondition( Config->Width  >  0                                                 , oC_ErrorCode_WidthNotCorrect      )
    && ErrorCondition( Config->Width  <= colorMap->Width                                   , oC_ErrorCode_WidthTooBig          )
    && ErrorCondition( Config->Height >  0                                                 , oC_ErrorCode_HeightNotCorrect     )
    && ErrorCondition( Config->Height <= colorMap->Height                                  , oC_ErrorCode_HeightTooBig         )
    && ErrorCondition( oC_ColorMap_IsPositionCorrect(colorMap,endPosition)                 , oC_ErrorCode_PositionNotCorrect   )
    && ErrorCondition( Config->ColorsArray == NULL || isaddresscorrect(Config->ColorsArray), oC_ErrorCode_WrongAddress         )
    && ErrorCondition( Config->FontG0 == NULL || isaddresscorrect(Config->FontG0)          , oC_ErrorCode_FontNotCorrect       )
    && ErrorCondition( Config->FontG1 == NULL || isaddresscorrect(Config->FontG1)          , oC_ErrorCode_FontNotCorrect       )
       )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Checks if the context of the GTD driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_GTD_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_GTDContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief sets default colors to all cells
 */
//==========================================================================================================================================
static void ResetCells( oC_GTD_Context_t Context )
{
    /* Filling cells with default values */
    for(uint32_t i = 0; i < Context->NumberOfLines * Context->NumberOfColumns; i++)
    {
        Context->Cells[i].Attributes         = Context->Attributes;
        Context->Cells[i].BackgroundColor    = Context->BackgroundColor;
        Context->Cells[i].ForegroundColor    = Context->ForegroundColor;
        Context->Cells[i].C                  = ' ';
    }
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a driver context
 */
//==========================================================================================================================================
static inline Context_t * Context_New( const oC_GTD_Config_t * Config  )
{
    Context_t *         context             = kmalloc( sizeof(Context_t), &Allocator, AllocationFlags_ZeroFill );
    oC_Font_t           fontG0              = Config->FontG0 == NULL ? oC_Font_(Consolas7pt) : Config->FontG0;
    oC_Font_t           fontG1              = Config->FontG1 == NULL ? oC_Font_(Consolas7pt) : Config->FontG1;
    oC_Pixel_ResolutionUInt_t g0FontWidth   = oC_Font_GetMaximumWidth(fontG0);
    oC_Pixel_ResolutionUInt_t g1FontWidth   = oC_Font_GetMaximumWidth(fontG1);
    oC_TGUI_Column_t    numberOfColumns     = Config->Width  / oC_MAX(g0FontWidth, g1FontWidth);
    oC_TGUI_Line_t      numberOfLines       = Config->Height / oC_MAX(fontG0->HeightPages, fontG1->HeightPages);
    oC_Mutex_t          mutex       = oC_Mutex_New(oC_Mutex_Type_Normal, &Allocator, AllocationFlags_Default);
    Cell_t *            cells       = kmalloc( sizeof(Cell_t) * numberOfColumns * numberOfLines, &Allocator, AllocationFlags_ZeroFill | AllocationFlags_ExternalRamFirst );
    oC_ColorMap_t*      colorMap    = NULL;
    oC_Screen_t         screen      = oC_ScreenMan_GetScreen(Config->ScreenName);

    oC_Screen_ReadColorMap(screen,&colorMap);

    if(
        oC_SaveIfFalse("context"   , context  != NULL , oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("mutex"     , mutex    != NULL , oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("cells"     , cells    != NULL , oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("screen"    , screen   != NULL , oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse("colorMap"  , colorMap != NULL , oC_ErrorCode_AllocationError )
        )
    {
        const oC_GTD_ColorDefinition_t * colorsIn = Config->ColorsArray == NULL ? DefaultColors: *(Config->ColorsArray);

        for(uint8_t i = 0; i < oC_GTD_TerminalColorIndex_NumberOfElements; i++)
        {
            context->ColorsArray[i].TerminalColorId = colorsIn[i].TerminalColorId;
            context->ColorsArray[i].ColorFormat     = colorMap->ColorFormat;
            context->ColorsArray[i].Color           = oC_Color_ConvertToFormat(colorsIn[i].Color,colorsIn[i].ColorFormat,colorMap->ColorFormat);
        }

        context->ColorFormat        = colorMap->ColorFormat;
        context->ObjectBusy         = mutex;
        context->Attributes         = Attributes_Default;
        context->BackgroundColor    = context->ColorsArray[oC_GTD_TerminalColorIndex_DefaultBackground].Color;
        context->ForegroundColor    = context->ColorsArray[oC_GTD_TerminalColorIndex_DefaultForeground].Color;
        context->Cells              = cells;
        context->FontG0             = fontG0;
        context->FontG1             = fontG1;
        context->G0FontHeight       = context->FontG0->HeightPages;
        context->G1FontHeight       = context->FontG1->HeightPages;
        context->G0FontWidth        = g0FontWidth;
        context->G1FontWidth        = g1FontWidth;
        context->Screen             = screen;
        context->Name               = oC_Screen_GetName(screen);
        context->NumberOfColumns    = numberOfColumns;
        context->NumberOfLines      = numberOfLines;
        context->ObjectControl      = oC_CountObjectControl(context,oC_ObjectId_GTDContext);
        context->Window.Width       = Config->Width;
        context->Window.Height      = Config->Height;
        context->Window.Position.X  = Config->Position.X;
        context->Window.Position.Y  = Config->Position.Y;
        context->ColorMap           = colorMap;
        context->SavedAttributes    = context->Attributes;

        context->SavedCursorPosition.Column = 0;
        context->SavedCursorPosition.Line   = 0;
        context->CursorPosition.Column  = 0;
        context->CursorPosition.Line    = 0;

        ResetCells(context);
    }
    else
    {
        oC_SaveIfFalse("context", context == NULL || kfree(context,0)           , oC_ErrorCode_ReleaseError    );
        oC_SaveIfFalse("mutex"  , mutex   == NULL || oC_Mutex_Delete(&mutex,0)  , oC_ErrorCode_ReleaseError    );
        oC_SaveIfFalse("cells"  , cells   == NULL || kfree(cells,0)             , oC_ErrorCode_ReleaseError    );
        context = NULL;
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases memory allocated for a context
 */
//==========================================================================================================================================
static inline bool Context_Delete( oC_GTD_Context_t * Context )
{
    Context_t *     context     = *Context;
    Cell_t *        cells       = context->Cells;
    oC_Mutex_t      mutex       = context->ObjectBusy;

    context->ObjectControl = 0;

    bool contextDeleted     = kfree(context,0);
    bool cellsDeleted       = kfree(cells,0);
    bool mutexDeleted       = oC_Mutex_Delete(&mutex,0);

    return contextDeleted && cellsDeleted && mutexDeleted;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the Cell
 */
//==========================================================================================================================================
static Cell_t * GetCell( Context_t * Context, oC_TGUI_Position_t Position )
{
    Cell_t * cell = NULL;

    if(Position.Column < Context->NumberOfColumns && Position.Line < Context->NumberOfLines)
    {
        cell = &Context->Cells[Position.Line * Context->NumberOfColumns + Position.Column];
    }

    return cell;
}

//==========================================================================================================================================
/**
 * @brief parses command according to the pattern
 */
//==========================================================================================================================================
static bool ParseCommand( Context_t * Context, const char * Buffer, uint32_t * Size , Command_t * Command )
{
    bool            parsed          = true;
    const char *    pattern         = Command->Definition->Vt100String;
    uint32_t        patternLength   = strlen(pattern);
    uint8_t         argumentIndex   = 0;
    bool            digitFound      = false;
    uint32_t        i               = 0;
    uint32_t        j               = 0;

    for(i = 0, j = 0; i < (*Size) && j < patternLength; i++)
    {
        if(Buffer[i] == pattern[j])
        {
            j++;
        }
        else if(pattern[j] == '#' && isdigit((int)Buffer[i]))
        {
            digitFound = true;

            if(argumentIndex < 4)
            {
                Command->Arguments[argumentIndex] = Command->Arguments[argumentIndex] * 10 + (Buffer[i] - '0');
            }
        }
        else if(digitFound == true && pattern[j] == '#')
        {
            j++;
            i--;
            argumentIndex++;
            digitFound = false;
        }
        else
        {
            parsed = false;
            break;
        }
    }

    if(parsed)
    {
        *Size = i;
    }

    return parsed;
}

//==========================================================================================================================================
/**
 * @brief finds and reads command according to the buffer
 */
//==========================================================================================================================================
static bool ReadCommand( Context_t * Context, const char * Buffer, uint32_t * Size, Command_t * outCommand )
{
    bool read = false;

    oC_ARRAY_FOREACH_IN_ARRAY(CommandsDefinitions,definition)
    {
        outCommand->Definition = definition;
        outCommand->Argument1  = 0;
        outCommand->Argument2  = 0;
        outCommand->Argument3  = 0;
        outCommand->Argument4  = 0;

        if(ParseCommand(Context,Buffer,Size,outCommand))
        {
            read = true;
            break;
        }
    }

    return read;
}

//==========================================================================================================================================
/**
 * @brief performs a command
 */
//==========================================================================================================================================
static oC_ErrorCode_t PerformCommand( Context_t * Context, const char * Buffer, uint32_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_CommandNotHandled;
    Command_t      command;

    if(ReadCommand(Context,Buffer,Size,&command))
    {
        errorCode = command.Definition->HandlerFunction(Context,&command);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief clears cursor
 */
//==========================================================================================================================================
static void ClearCursor( Context_t * Context )
{
    oC_TGUI_Position_t savedCursor;

    savedCursor.Column = Context->CursorPosition.Column;
    savedCursor.Line   = Context->CursorPosition.Line;

    IncrementCursorPosition( Context );

    Cell_t * cell = GetCell(Context,savedCursor);
    DrawCell(Context,cell,savedCursor);

    Context->CursorPosition.Column = savedCursor.Column;
    Context->CursorPosition.Line   = savedCursor.Line;
}

//==========================================================================================================================================
/**
 * @brief draws cursor
 */
//==========================================================================================================================================
static void DrawCursor( Context_t * Context )
{
    Cell_t * cell = GetCell(Context,Context->CursorPosition);
    DrawCell(Context,cell,Context->CursorPosition);
}

//==========================================================================================================================================
/**
 * @brief draws the given cell
 */
//==========================================================================================================================================
static void DrawCell( Context_t * Context, Cell_t * Cell , oC_TGUI_Position_t Position )
{
    if(Cell != NULL)
    {
        oC_Font_t                   font            = Cell->Attributes & Attributes_G0Font ? Context->FontG0 : Context->FontG1;
        oC_Pixel_ResolutionUInt_t   width           = oC_MAX(Context->G0FontWidth,  Context->G1FontWidth);
        oC_Pixel_ResolutionUInt_t   height          = oC_MAX(Context->G0FontHeight, Context->G1FontHeight);
        oC_Pixel_Position_t         pixelPosition   = GetPixelPosition(Context,Position);
        oC_Color_t                  backgroundColor = Cell->BackgroundColor;

        if(Context->CursorPosition.Column == Position.Column && Context->CursorPosition.Line == Position.Line)
        {
            backgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Cursor].Color;
        }

        oC_ColorMap_FillRectWithColor(Context->ColorMap,pixelPosition,width,height,backgroundColor,Context->ColorFormat);
        oC_ColorMap_DrawChar(Context->ColorMap,pixelPosition,Cell->ForegroundColor,Context->ColorFormat, Cell->C, font);
    }
}

//==========================================================================================================================================
/**
 * @brief reprints terminal at the screen
 */
//==========================================================================================================================================
static void RefreshScreen( Context_t * Context )
{
    oC_TGUI_Position_t drawPosition = { .Line = 0 , .Column = 0 };
    oC_ColorMap_t *    colorMapOut  = NULL;

    for( drawPosition.Line = 0; drawPosition.Line < Context->NumberOfLines; drawPosition.Line++ )
    {
        for( drawPosition.Column = 0; drawPosition.Column < Context->NumberOfColumns; drawPosition.Column++ )
        {
            DrawCell(Context,GetCell(Context,drawPosition),drawPosition);
        }
    }
    oC_Screen_ReadColorMap(Context->Screen,&colorMapOut);
}

//==========================================================================================================================================
/**
 * @brief returns pixel position according to TGUI cursor position
 */
//==========================================================================================================================================
static oC_Pixel_Position_t GetPixelPosition( Context_t * Context, oC_TGUI_Position_t Position )
{
    oC_Pixel_ResolutionUInt_t   width           = oC_MAX(Context->G0FontWidth,  Context->G1FontWidth);
    oC_Pixel_ResolutionUInt_t   height          = oC_MAX(Context->G0FontHeight, Context->G1FontHeight);
    oC_Pixel_Position_t         pixelPosition   = { .X = width  * Position.Column + Context->Window.Position.X,
                                                    .Y = height * Position.Line   + Context->Window.Position.Y
    };

    return pixelPosition;
}

//==========================================================================================================================================
/**
 * @brief adds new line to the cells array
 */
//==========================================================================================================================================
static void AddNewLine( Context_t * Context )
{
    oC_TGUI_Position_t upperPosition;
    oC_TGUI_Position_t position;
    Cell_t *           downCell     = NULL;
    Cell_t *           upperCell    = NULL;

    for(position.Line = 1, upperPosition.Line = 0; position.Line < Context->NumberOfLines ; position.Line++, upperPosition.Line++)
    {
        for(position.Column = 0; position.Column < Context->NumberOfColumns; position.Column++)
        {
            upperPosition.Column = position.Column;

            downCell    = GetCell(Context,position);
            upperCell   = GetCell(Context,upperPosition);

            if(downCell && upperCell)
            {
                memcpy(upperCell ,  downCell , sizeof(Cell_t));

                if(position.Line == (Context->NumberOfLines - 1))
                {
                    downCell->C = ' ';
                }
            }
        }
    }
    RefreshScreen(Context);
}

//==========================================================================================================================================
/**
 * @brief increments current cursor position
 */
//==========================================================================================================================================
static void IncrementCursorPosition( Context_t * Context )
{
    Context->CursorPosition.Column++;
    if(Context->CursorPosition.Column >= Context->NumberOfColumns)
    {
        if(Context->LineWrappingDisabled)
        {
            Context->CursorPosition.Column = Context->NumberOfColumns - 1;
        }
        else
        {
            Context->CursorPosition.Line++;

            if(Context->CursorPosition.Line >= Context->NumberOfLines)
            {
                AddNewLine(Context);
                Context->CursorPosition.Line    = Context->NumberOfLines - 1;
                Context->CursorPosition.Column  = 0;
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief draws a character
 */
//==========================================================================================================================================
static void DrawCharacter( Context_t * Context, char C )
{
    Cell_t *            cell            = GetCell(Context,Context->CursorPosition);
    oC_TGUI_Position_t  drawPosition    = { .Column = Context->CursorPosition.Column, .Line = Context->CursorPosition.Line };

    if(cell)
    {
        if(C == '\n')
        {
            Context->CursorPosition.Line++;
            if(Context->CursorPosition.Line >= Context->NumberOfLines)
            {
                AddNewLine(Context);
                Context->CursorPosition.Line = Context->NumberOfLines - 1;
            }
        }
        else if(C == '\r')
        {
            Context->CursorPosition.Column = 0;
        }
        else
        {
            cell->C                 = C;
            cell->Attributes        = Context->Attributes;
            cell->BackgroundColor   = Context->BackgroundColor;
            cell->ForegroundColor   = Context->ForegroundColor;

            IncrementCursorPosition(Context);

            DrawCell(Context,cell,drawPosition);
        }
    }
}

//==========================================================================================================================================
/**
 * @brief performs VT100 reset device command
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_ResetDevice( Context_t * Context, void * Command )
{
    Context->BackgroundColor        = Context->ColorsArray[oC_GTD_TerminalColorIndex_DefaultBackground].Color;
    Context->ForegroundColor        = Context->ColorsArray[oC_GTD_TerminalColorIndex_DefaultForeground].Color;
    Context->CursorPosition.Column  = 0;
    Context->CursorPosition.Line    = 0;

    ResetCells(Context);
    RefreshScreen(Context);

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief selects font
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_SelectFont( Context_t * Context, void * Command )
{
    Command_t * command = Command;

    if(strcmp(command->Definition->Vt100String,oC_VT100_FONT_SET_G0) == 0)
    {
        Context->Attributes |= Attributes_G0Font;
    }
    else
    {
        Context->Attributes &= ~Attributes_G0Font;
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief enables line wrapping
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_EnableLineWrap( Context_t * Context, void * Command )
{
    Context->LineWrappingDisabled = false;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief disables line wrapping
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DisableLineWrap( Context_t * Context, void * Command )
{
    Context->LineWrappingDisabled = true;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief selects cursor home
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_SetCursorHome( Context_t * Context, void * Command )
{
    oC_ErrorCode_t      errorCode    = oC_ErrorCode_ImplementError;
    Command_t *         command      = Command;
    oC_TGUI_Position_t  homePosition = {
                    .Line   = command->Argument1 - 1,
                    .Column = command->Argument2 - 1
    };

    if(
        ErrorCondition( homePosition.Column < Context->NumberOfColumns , oC_ErrorCode_PositionNotCorrect )
     && ErrorCondition( homePosition.Line   < Context->NumberOfLines   , oC_ErrorCode_PositionNotCorrect )
        )
    {
        Context->HomePosition.Column    = homePosition.Column;
        Context->HomePosition.Line      = homePosition.Line;
        Context->CursorPosition.Column  = homePosition.Column;
        Context->CursorPosition.Line    = homePosition.Line;
        errorCode                       = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief moves current cursor position to home
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_MoveCursorToHome( Context_t * Context, void * Command )
{
    Context->CursorPosition.Column = Context->HomePosition.Column;
    Context->CursorPosition.Line   = Context->HomePosition.Line;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief moves cursor to up
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_MoveCursorUp( Context_t * Context, void * Command )
{
    if(Context->CursorPosition.Line > 0)
    {
        Context->CursorPosition.Line--;
    }
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief moves cursor to down
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_MoveCursorDown( Context_t * Context, void * Command )
{
    Context->CursorPosition.Line++;

    if(Context->CursorPosition.Line >= Context->NumberOfLines)
    {
        Context->CursorPosition.Line = Context->NumberOfLines - 1;
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief moves cursor to the right
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_MoveCursorForward( Context_t * Context, void * Command )
{
    Context->CursorPosition.Column++;

    if(Context->CursorPosition.Column >= Context->NumberOfColumns)
    {
        Context->CursorPosition.Column = Context->NumberOfColumns - 1;
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief moves cursor to the left
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_MoveCursorBackward( Context_t * Context, void * Command )
{
    if(Context->CursorPosition.Column > 0)
    {
        Context->CursorPosition.Column--;
    }
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief changes cursor position to the given one
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_ForceCursorPosition( Context_t * Context, void * Command )
{
    oC_ErrorCode_t      errorCode    = oC_ErrorCode_ImplementError;
    Command_t *         command      = Command;
    oC_TGUI_Position_t  newPosition  = {
                    .Line   = command->Argument1 - 1,
                    .Column = command->Argument2 - 1
    };

    if(
        ErrorCondition( newPosition.Column < Context->NumberOfColumns , oC_ErrorCode_PositionNotCorrect )
     && ErrorCondition( newPosition.Line   < Context->NumberOfLines   , oC_ErrorCode_PositionNotCorrect )
        )
    {
        Context->CursorPosition.Column  = newPosition.Column;
        Context->CursorPosition.Line    = newPosition.Line;
        errorCode                       = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief saves cursor position for later
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_SaveCursorPosition( Context_t * Context, void * Command )
{
    Context->SavedCursorPosition.Column = Context->CursorPosition.Column;
    Context->SavedCursorPosition.Line   = Context->CursorPosition.Line;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief restores saved cursor position
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_UnsaveCursorPosition( Context_t * Context, void * Command )
{
    Context->CursorPosition.Column = Context->SavedCursorPosition.Column;
    Context->CursorPosition.Line   = Context->SavedCursorPosition.Line;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief saves current cursor position and current attributes for later
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_SaveCursorAndAttrs( Context_t * Context, void * Command )
{
    VT100_SaveCursorPosition(Context,Command);
    Context->SavedAttributes = Context->Attributes;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief restores saved cursor position and saved attributes
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_RestoreCursorAndAttrs ( Context_t * Context, void * Command )
{
    VT100_UnsaveCursorPosition(Context,Command);
    Context->Attributes = Context->SavedAttributes;
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief scrolls the screen
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_ScrollScreen( Context_t * Context, void * Command )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief scrolls the screen down
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_ScrollDown( Context_t * Context, void * Command )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief scrolls the screen up
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_ScrollUp( Context_t * Context, void * Command )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief sets a tabulator at current position
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_SetTab( Context_t * Context, void * Command )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief clears tabulator at current position
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_ClearTab( Context_t * Context, void * Command )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief clears all tabulators
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_ClearAllTabs( Context_t * Context, void * Command )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief erases characters till end of the line
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_EraseEndOfTheLine( Context_t * Context, void * Command )
{
    oC_TGUI_Position_t position;

    position.Line = Context->CursorPosition.Line;

    for(position.Column = Context->CursorPosition.Column; position.Column < Context->NumberOfColumns; position.Column++)
    {
        Cell_t * cell = GetCell(Context,position);

        if(cell)
        {
            cell->BackgroundColor   = Context->BackgroundColor;
            cell->ForegroundColor   = Context->ForegroundColor;
            cell->Attributes        = Context->Attributes;
            cell->C                 = ' ';
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief erases characters till start of the line
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_EraseStartOfTheLine( Context_t * Context, void * Command )
{
    oC_TGUI_Position_t position;

    position.Line = Context->CursorPosition.Line;

    for(position.Column = Context->CursorPosition.Column; position.Column < Context->NumberOfColumns; position.Column--)
    {
        Cell_t * cell = GetCell(Context,position);

        if(cell)
        {
            cell->BackgroundColor   = Context->BackgroundColor;
            cell->ForegroundColor   = Context->ForegroundColor;
            cell->Attributes        = Context->Attributes;
            cell->C                 = ' ';
        }

        if(position.Column == 0)
        {
            break;
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief erases current line
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_EraseLine( Context_t * Context, void * Command )
{
    oC_TGUI_Position_t position;

    position.Line = Context->CursorPosition.Line;

    for(position.Column = 0; position.Column < Context->NumberOfColumns; position.Column++)
    {
        Cell_t * cell = GetCell(Context,position);

        if(cell)
        {
            cell->BackgroundColor   = Context->BackgroundColor;
            cell->ForegroundColor   = Context->ForegroundColor;
            cell->Attributes        = Context->Attributes;
            cell->C                 = ' ';
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief erases all lines below current cursor position
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_EraseDown( Context_t * Context, void * Command )
{
    oC_TGUI_Position_t position;

    for(position.Line = Context->CursorPosition.Line; position.Line < Context->NumberOfLines ; position.Line++ )
    {
        for(position.Column = 0; position.Column < Context->NumberOfColumns; position.Column++)
        {
            Cell_t * cell = GetCell(Context,position);

            if(cell)
            {
                cell->BackgroundColor   = Context->BackgroundColor;
                cell->ForegroundColor   = Context->ForegroundColor;
                cell->Attributes        = Context->Attributes;
                cell->C                 = ' ';
            }
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief erases all lines above current cursor position
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_EraseUp( Context_t * Context, void * Command )
{
    oC_TGUI_Position_t position;

    for(position.Line = 0; position.Line < Context->NumberOfLines && position.Line <= Context->CursorPosition.Line; position.Line++ )
    {
        for(position.Column = 0; position.Column < Context->NumberOfColumns; position.Column++)
        {
            Cell_t * cell = GetCell(Context,position);

            if(cell)
            {
                cell->BackgroundColor   = Context->BackgroundColor;
                cell->ForegroundColor   = Context->ForegroundColor;
                cell->Attributes        = Context->Attributes;
                cell->C                 = ' ';
            }
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief erases all the screen
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_EraseScreen( Context_t * Context, void * Command )
{
    oC_TGUI_Position_t position;

    for(position.Line = 0; position.Line < Context->NumberOfLines; position.Line++ )
    {
        for(position.Column = 0; position.Column < Context->NumberOfColumns; position.Column++)
        {
            Cell_t * cell = GetCell(Context,position);

            if(cell)
            {
                cell->BackgroundColor   = Context->BackgroundColor;
                cell->ForegroundColor   = Context->ForegroundColor;
                cell->Attributes        = Context->Attributes;
                cell->C                 = ' ';
            }
        }
    }

    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief sets attribute
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_SetAttribute( Context_t * Context, void * Command )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;
    Command_t *    command   = Command;

    switch(command->Argument1)
    {
        case 0:
            Context->Attributes = Attributes_Default;
            break;
        case 1:
            Context->Attributes |= Attributes_Bright;
            break;
        case 2:
            Context->Attributes |= Attributes_Dim;
            break;
        case 4:
            Context->Attributes |= Attributes_Underscore;
            break;
        case 5:
            Context->Attributes |= Attributes_Blink;
            break;
        case 7:
            Context->Attributes |= Attributes_Reverse;
            break;
        case 8:
            Context->Attributes |= Attributes_Hidden;
            break;
        case 20:
            Context->Attributes &= ~Attributes_Default;
            break;
        case 21:
            Context->Attributes &= ~Attributes_Bright;
            break;
        case 22:
            Context->Attributes &= ~Attributes_Dim;
            break;
        case 24:
            Context->Attributes &= ~Attributes_Underscore;
            break;
        case 25:
            Context->Attributes &= ~Attributes_Blink;
            break;
        case 27:
            Context->Attributes &= ~Attributes_Reverse;
            break;
        case 28:
            Context->Attributes &= ~Attributes_Hidden;
            break;
        case 30:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Black].Color;
            break;
        case 31:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Red].Color;
            break;
        case 32:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Green].Color;
            break;
        case 33:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Yellow].Color;
            break;
        case 34:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Blue].Color;
            break;
        case 35:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Magenda].Color;
            break;
        case 36:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Cyan].Color;
            break;
        case 37:
            Context->ForegroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_White].Color;
            break;
        case 90: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_DarkGray    ].Color; break;
        case 91: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightRed    ].Color; break;
        case 92: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightGreen  ].Color; break;
        case 93: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightYellow ].Color; break;
        case 94: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightBlue   ].Color; break;
        case 95: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightMagenda].Color; break;
        case 96: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightCyan   ].Color; break;
        case 97: Context->ForegroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_White       ].Color; break;

        case 40:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Black].Color;
            break;
        case 41:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Red].Color;
            break;
        case 42:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Green].Color;
            break;
        case 43:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Yellow].Color;
            break;
        case 44:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Blue].Color;
            break;
        case 45:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Magenda].Color;
            break;
        case 46:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_Cyan].Color;
            break;
        case 47:
            Context->BackgroundColor = Context->ColorsArray[oC_GTD_TerminalColorIndex_White].Color;
            break;

        case 100: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_DarkGray    ].Color; break;
        case 101: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightRed    ].Color; break;
        case 102: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightGreen  ].Color; break;
        case 103: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightYellow ].Color; break;
        case 104: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightBlue   ].Color; break;
        case 105: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightMagenda].Color; break;
        case 106: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_LightCyan   ].Color; break;
        case 107: Context->BackgroundColor = Context->ColorsArray[ oC_GTD_TerminalColorIndex_White       ].Color; break;

        default:
            errorCode = oC_ErrorCode_WrongParameters;
            break;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief draws specified border
 */
//==========================================================================================================================================
static void DrawBorder( Context_t * Context, BorderType_t BorderType )
{
    oC_Pixel_Position_t         topLeftPosition     = GetPixelPosition(Context,Context->CursorPosition);
    oC_Pixel_ResolutionUInt_t   width               = oC_MAX(Context->G0FontWidth,  Context->G1FontWidth);
    oC_Pixel_ResolutionUInt_t   height              = oC_MAX(Context->G0FontHeight, Context->G1FontHeight);
    oC_Pixel_Position_t         bottomRightPosition = { .X = topLeftPosition.X + width     , .Y = topLeftPosition.Y + height     };
    oC_Pixel_Position_t         middlePosition      = { .X = topLeftPosition.X + (width/2) , .Y = topLeftPosition.Y + (height/2) };
    oC_Pixel_Position_t         leftMiddlePosition  = { .X = topLeftPosition.X      , .Y = middlePosition.Y };
    oC_Pixel_Position_t         rightMiddlePosition = { .X = bottomRightPosition.X  , .Y = middlePosition.Y };
    oC_Pixel_Position_t         upMiddlePosition    = { .X = middlePosition.X       , .Y = topLeftPosition.Y };
    oC_Pixel_Position_t         downMiddlePosition  = { .X = middlePosition.X       , .Y = bottomRightPosition.Y };

    oC_ColorMap_FillRectWithColor(Context->ColorMap,topLeftPosition,width,height,Context->BackgroundColor,Context->ColorFormat);

    if(BorderType & BorderType_LeftToMiddle)
    {
        oC_ColorMap_DrawLine(Context->ColorMap,leftMiddlePosition,middlePosition,Context->ForegroundColor,Context->ColorFormat);
    }
    if(BorderType & BorderType_RightToMiddle)
    {
        oC_ColorMap_DrawLine(Context->ColorMap,rightMiddlePosition,middlePosition,Context->ForegroundColor,Context->ColorFormat);
    }
    if(BorderType & BorderType_UpToMiddle)
    {
        oC_ColorMap_DrawLine(Context->ColorMap,upMiddlePosition,middlePosition,Context->ForegroundColor,Context->ColorFormat);
    }
    if(BorderType & BorderType_DownToMiddle)
    {
        oC_ColorMap_DrawLine(Context->ColorMap,downMiddlePosition,middlePosition,Context->ForegroundColor,Context->ColorFormat);
    }
}

//==========================================================================================================================================
/**
 * @brief draws border from left to up
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderLU( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_LU);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}
//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderLD( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_LD);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderDR( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_DR);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderUR( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_UR);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderLUDR( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_LUDR);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderLR( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_LR);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderUDR( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_UDR);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderLUD( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_LUD);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderLUR( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_LUR);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderLDR( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_LDR);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief draws border the border
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawBorderUD( Context_t * Context, void * Command )
{
    DrawBorder(Context,BorderType_UD);
    IncrementCursorPosition(Context);
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
/**
 * @brief handles DL support verification
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawingLineSupport( Context_t * Context, void * Command )
{
    return oC_VT100_DL_SUPPORTED_VALUE;
}

//==========================================================================================================================================
/**
 * @brief draws real line
 */
//==========================================================================================================================================
static oC_ErrorCode_t VT100_DrawRealLine( Context_t * Context, void * Command )
{
    oC_ErrorCode_t      errorCode       = oC_ErrorCode_ImplementError;
    Command_t *         command         = Command;
    oC_TGUI_Position_t  fromPosition    = { .Column = command->Argument1, .Line = command->Argument2 };
    oC_TGUI_Position_t  toPosition      = { .Column = command->Argument3, .Line = command->Argument4 };
    oC_Pixel_Position_t startPixel      = GetPixelPosition(Context,fromPosition);
    oC_Pixel_Position_t endPixel        = GetPixelPosition(Context,toPosition);

    if(
        ErrorCondition( fromPosition.Column < Context->NumberOfColumns , oC_ErrorCode_PositionNotCorrect )
     && ErrorCondition( fromPosition.Line   < Context->NumberOfLines   , oC_ErrorCode_PositionNotCorrect )
     && ErrorCondition(   toPosition.Column < Context->NumberOfColumns , oC_ErrorCode_PositionNotCorrect )
     && ErrorCondition(   toPosition.Line   < Context->NumberOfLines   , oC_ErrorCode_PositionNotCorrect )
        )
    {
        oC_ColorMap_DrawLine(Context->ColorMap,startPixel,endPixel,Context->ForegroundColor,Context->ColorFormat);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
