/** ****************************************************************************************************************************************
 *
 * @file       oc_ft5336.c
 * 
 * File based on driver.c Ver 1.1.1
 *
 * @brief      The file with source for FT5336 driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2017-05-02 - 18:34:20)
 *
 * @copyright  Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_ft5336.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_gpio.h>
#include <oc_driverman.h>
#include <oc_struct.h>
#include <oc_threadman.h>

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         FT5336
#define DRIVER_FILE_NAME    "ft5336"
#define DRIVER_DEBUGLOG     false
#define DRIVER_TYPE         INPUT_EVENT_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_FT5336_Configure
#define DRIVER_UNCONFIGURE  oC_FT5336_Unconfigure
#define DRIVER_TURN_ON      oC_FT5336_TurnOn
#define DRIVER_TURN_OFF     oC_FT5336_TurnOff
#define IS_TURNED_ON        oC_FT5336_IsTurnedOn
#define WAIT_FOR_INPUT      oC_FT5336_WaitForInput
#define IS_EVENT_SUPPORTED  oC_FT5336_IsEventSupported

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

/* Maximum number of touches handled by the module */
#define MAX_TOUCHES         5

/* Default device address on I2C bus */
#define DEVICE_ADDRESS      0x70

/* Value for CHIP ID register */
#define CHIP_ID             0x51

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef enum
{
    RegisterOffset_DEV_MODE                 = 0x00 ,
    RegisterOffset_GEST_ID                  = 0x01 ,
    RegisterOffset_TD_STAT                  = 0x02 ,

    RegisterOffset_P1_XH                    = 0x03 ,
    RegisterOffset_P1_XL                    = 0x04 ,
    RegisterOffset_P1_YH                    = 0x05 ,
    RegisterOffset_P1_YL                    = 0x06 ,
    RegisterOffset_P1_WEIGHT                = 0x07 ,
    RegisterOffset_P1_MISC                  = 0x08 ,

    RegisterOffset_P2_XH                    = 0x09 ,
    RegisterOffset_P2_XL                    = 0x0A ,
    RegisterOffset_P2_YH                    = 0x0B ,
    RegisterOffset_P2_YL                    = 0x0C ,
    RegisterOffset_P2_WEIGHT                = 0x0D ,
    RegisterOffset_P2_MISC                  = 0x0E ,

    RegisterOffset_P3_XH                    = 0x0F ,
    RegisterOffset_P3_XL                    = 0x10 ,
    RegisterOffset_P3_YH                    = 0x11 ,
    RegisterOffset_P3_YL                    = 0x12 ,
    RegisterOffset_P3_WEIGHT                = 0x13 ,
    RegisterOffset_P3_MISC                  = 0x14 ,

    RegisterOffset_P4_XH                    = 0x15 ,
    RegisterOffset_P4_XL                    = 0x16 ,
    RegisterOffset_P4_YH                    = 0x17 ,
    RegisterOffset_P4_YL                    = 0x18 ,
    RegisterOffset_P4_WEIGHT                = 0x19 ,
    RegisterOffset_P4_MISC                  = 0x1A ,

    RegisterOffset_P5_XH                    = 0x1B ,
    RegisterOffset_P5_XL                    = 0x1C ,
    RegisterOffset_P5_YH                    = 0x1D ,
    RegisterOffset_P5_YL                    = 0x1E ,
    RegisterOffset_P5_WEIGHT                = 0x1F ,
    RegisterOffset_P5_MISC                  = 0x20 ,

    RegisterOffset_P6_XH                    = 0x21 ,
    RegisterOffset_P6_XL                    = 0x22 ,
    RegisterOffset_P6_YH                    = 0x23 ,
    RegisterOffset_P6_YL                    = 0x24 ,
    RegisterOffset_P6_WEIGHT                = 0x25 ,
    RegisterOffset_P6_MISC                  = 0x26 ,

    RegisterOffset_P7_XH                    = 0x27 ,
    RegisterOffset_P7_XL                    = 0x28 ,
    RegisterOffset_P7_YH                    = 0x29 ,
    RegisterOffset_P7_YL                    = 0x2A ,
    RegisterOffset_P7_WEIGHT                = 0x2B ,
    RegisterOffset_P7_MISC                  = 0x2C ,

    RegisterOffset_P8_XH                    = 0x2D ,
    RegisterOffset_P8_XL                    = 0x2E ,
    RegisterOffset_P8_YH                    = 0x2F ,
    RegisterOffset_P8_YL                    = 0x30 ,
    RegisterOffset_P8_WEIGHT                = 0x31 ,
    RegisterOffset_P8_MISC                  = 0x32 ,

    RegisterOffset_P9_XH                    = 0x33 ,
    RegisterOffset_P9_XL                    = 0x34 ,
    RegisterOffset_P9_YH                    = 0x35 ,
    RegisterOffset_P9_YL                    = 0x36 ,
    RegisterOffset_P9_WEIGHT                = 0x37 ,
    RegisterOffset_P9_MISC                  = 0x38 ,

    RegisterOffset_P10_XH                   = 0x39 ,
    RegisterOffset_P10_XL                   = 0x3A ,
    RegisterOffset_P10_YH                   = 0x3B ,
    RegisterOffset_P10_YL                   = 0x3C ,
    RegisterOffset_P10_WEIGHT               = 0x3D ,
    RegisterOffset_P10_MISC                 = 0x3E ,

    RegisterOffset_TH_GROUP                 = 0x80 ,
    RegisterOffset_TH_DIFF                  = 0x85 ,
    RegisterOffset_CTRL                     = 0x86 ,
    RegisterOffset_TIMEENTERMONITOR         = 0x87 ,
    RegisterOffset_PERIODACTIVE             = 0x88 ,
    RegisterOffset_PERIODMONITOR            = 0x89 ,
    RegisterOffset_RADIAN_VALUE             = 0x91 ,
    RegisterOffset_OFFSET_LEFT_RIGHT        = 0x92 ,
    RegisterOffset_OFFSET_UP_DOWN           = 0x93 ,
    RegisterOffset_DISTANCE_LEFT_RIGHT      = 0x94 ,
    RegisterOffset_DISTANCE_UP_DOWN         = 0x95 ,
    RegisterOffset_DISTANCE_ZOOM            = 0x96 ,
    RegisterOffset_LIB_VER_H                = 0xA1 ,
    RegisterOffset_LIB_VER_L                = 0xA2 ,
    RegisterOffset_CIPHER                   = 0xA3 ,
    RegisterOffset_GMODE                    = 0xA4 ,
    RegisterOffset_PWR_MODE                 = 0xA5 ,
    RegisterOffset_FIRMID                   = 0xA6 ,
    RegisterOffset_CHIP_ID                  = 0xA8 ,
} RegisterOffset_t;

//==========================================================================================================================================
/**
 * @brief stores FT5336 context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t      ObjectControl;
    oC_I2C_Context_t        I2CContext;
    bool                    I2CBusConfiguredByDriver;
    oC_FT5336_Mode_t        Mode;
    oC_Pin_t                InterruptPin;
    oC_Time_t               RequestPeriod;
    oC_I2C_Address_t        DeviceAddress;
    oC_GPIO_Context_t       InterruptPinContext;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all FT5336 driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "ft5336 module"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool                  IsContextCorrect      ( oC_FT5336_Context_t Context );
static        oC_FT5336_Context_t   Context_New           ( const oC_FT5336_Config_t * Config );
static        bool                  Context_Delete        ( oC_FT5336_Context_t Context );
static        oC_ErrorCode_t        ConfigureI2CBus       ( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context );
static        oC_ErrorCode_t        UnconfigureI2CBus     ( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context );
static        oC_ErrorCode_t        DetectChip            ( oC_FT5336_Context_t Context );
static        oC_ErrorCode_t        ConfigureChip         ( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context );
static        oC_ErrorCode_t        UnconfigureChip       ( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context );
static        oC_ErrorCode_t        ReadRegister          ( oC_FT5336_Context_t Context, oC_I2C_Register_t Offset, uint8_t * outValue, oC_Time_t Timeout );
static        oC_ErrorCode_t        WriteRegister         ( oC_FT5336_Context_t Context, oC_I2C_Register_t Offset, uint8_t      Value, oC_Time_t Timeout );
static        bool                  WaitForTouch          ( oC_FT5336_Context_t Context , uint16_t * outNumberOfTouches, oC_Time_t Timeout );
static        oC_ErrorCode_t        ReadTouchPosition     ( oC_FT5336_Context_t Context , uint16_t TouchId, oC_Pixel_Position_t * outPosition, oC_Pixel_ResolutionUInt_t * outWidth, oC_Time_t Timeout );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the FT5336 module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FT5336_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_FT5336))
    {
        /* This must be always at the end of the function */
        oC_Module_TurnOn(oC_Module_FT5336);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the FT5336 driver
 *
 * The function for turning off the FT5336 driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FT5336_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FT5336))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_FT5336);

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_FT5336_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_FT5336);
}

//==========================================================================================================================================
/**
 * @brief configures FT5336 pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_FT5336_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FT5336_Configure( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FT5336))
    {
        if(
            ErrorCondition( isaddresscorrect(Config)        , oC_ErrorCode_WrongConfigAddress       )
         && ErrorCondition( isram(outContext)               , oC_ErrorCode_OutputAddressNotInRAM    )
            )
        {
            oC_FT5336_Context_t context = Context_New(Config);

            if(ErrorCondition( context != NULL, oC_ErrorCode_AllocationError ))
            {
                if(ErrorCode( ConfigureI2CBus(Config,context) ))
                {
                    if(ErrorCode( ConfigureChip(Config,context) ))
                    {
                        *outContext = context;
                        errorCode   = oC_ErrorCode_None;
                    }
                    if(oC_ErrorOccur(errorCode))
                    {
                        oC_SaveIfErrorOccur("Cannot unconfigure I2C bus: ", UnconfigureI2CBus(Config,context));
                    }
                }
                if(oC_ErrorOccur(errorCode))
                {
                    oC_SaveIfFalse("Cannot delete context: ", Context_Delete(context), oC_ErrorCode_ReleaseError);
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_FT5336_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FT5336_Unconfigure( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FT5336))
    {
        if(
            ErrorCondition( isaddresscorrect(Config)      , oC_ErrorCode_WrongConfigAddress     )
         && ErrorCondition( isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM  )
         && ErrorCondition( IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect      )
            )
        {
            oC_FT5336_Context_t context = *outContext;

            errorCode   = oC_ErrorCode_None;
            *outContext = NULL;

            ErrorCode( UnconfigureChip  (Config, context) );
            ErrorCode( UnconfigureI2CBus(Config, context) );
            ErrorCondition( Context_Delete(context), oC_ErrorCode_ReleaseError );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief waits for input event
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FT5336_WaitForInput( oC_FT5336_Context_t Context , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_FT5336))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)                               , oC_ErrorCode_ContextNotCorrect        )
         && ErrorCondition( isram(outEvent)                                         , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( (EventsMask & oC_IDI_EventId_TouchScreenEvent) != 0     , oC_ErrorCode_IncorrectMask            )
         && ErrorCondition( oC_FT5336_IsEventSupported(Context,EventsMask)          , oC_ErrorCode_EventNotSupported        )
         && ErrorCondition( Timeout >= 0                                            , oC_ErrorCode_TimeNotCorrect           )
            )
        {
            outEvent->EventId = oC_IDI_EventId_None;

            if( ErrorCondition( WaitForTouch(Context, &outEvent->NumberOfPoints, gettimeout(endTimestamp) ) , oC_ErrorCode_Timeout ) )
            {
                if( ErrorCondition( outEvent->NumberOfPoints >  0
                                 && outEvent->NumberOfPoints <= MAX_TOUCHES , oC_ErrorCode_UnexpectedRegisterValue )
                    )
                {
                    outEvent->EventId   = oC_IDI_EventId_TouchScreen_ClickWithoutRelease;

                    oC_STATIC_ASSERT( MAX_TOUCHES <= oC_IDI_MAX_POINTS, "MAX_TOUCHES handled by the driver is too big!" );

                    for(uint16_t i = 0; i < outEvent->NumberOfPoints; i++)
                    {
                        if( ErrorCode( ReadTouchPosition(Context, i, &outEvent->Position[i], &outEvent->Weight[i], gettimeout(endTimestamp)) ) )
                        {
                            errorCode = oC_ErrorCode_None;
                        }
                        else
                        {
                            break;
                        }
                    }

                }
            }

        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if event is supported
 */
//==========================================================================================================================================
bool oC_FT5336_IsEventSupported( oC_FT5336_Context_t Context , oC_IDI_EventId_t EventId )
{
    /* The other gestures are not supported because the register 0x01 still returns 0x0 and does not recognize it.
     * I don't know why, so there will be software support for events other than click                             */
    return (EventId & oC_IDI_EventId_TouchScreenEvent) != 0
        && (
                (EventId & oC_IDI_EventId_IndexMask) == oC_IDI_EventId_AnyMove
             || (EventId & oC_IDI_EventId_IndexMask) == oC_IDI_EventId_ClickWithoutRelease
             );
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the FT5336 driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_FT5336_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_FT5336Context,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief allocates memory for the driver context
 */
//==========================================================================================================================================
static oC_FT5336_Context_t Context_New( const oC_FT5336_Config_t * Config )
{
    oC_FT5336_Context_t context = kmalloc( sizeof(struct Context_t), &Allocator, AllocationFlags_ZeroFill );

    if(oC_SaveIfFalse("context", context != NULL, oC_ErrorCode_AllocationError))
    {
        context->ObjectControl  = oC_CountObjectControl(context,oC_ObjectId_FT5336Context);
        context->DeviceAddress  = DEVICE_ADDRESS;
        context->I2CContext     = Config->I2C.Context;
        context->Mode           = Config->Mode;
        context->InterruptPin   = Config->InterruptPin;
        context->RequestPeriod  = Config->PollingTime;
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases memory of the driver context
 */
//==========================================================================================================================================
static bool Context_Delete( oC_FT5336_Context_t Context )
{
    Context->ObjectControl = 0;

    return kfree(Context,0);
}

//==========================================================================================================================================
/**
 * @brief configures I2C bus
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureI2CBus( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Config->I2C.Context != NULL)
    {
        if(ErrorCondition( oC_I2C_IsContextCorrect(Config->I2C.Context), oC_ErrorCode_ContextNotCorrect ))
        {
            Context->I2CContext = Config->I2C.Context;
            errorCode           = oC_ErrorCode_None;
        }
    }
    else if(Config->I2C.AutoConfigurationName != NULL)
    {
        oC_AutoConfiguration_t autoConfiguration;

        if( ErrorCode( oC_DriverMan_ReadAutoConfiguration(Config->I2C.AutoConfigurationName,&autoConfiguration) ) )
        {
            if(ErrorCondition( oC_I2C_IsContextCorrect(autoConfiguration.Context), oC_ErrorCode_DriverNotConfiguredYet ))
            {
                Context->I2CContext = autoConfiguration.Context;
                errorCode           = oC_ErrorCode_None;
            }
        }
    }
    else
    {
        if(
            ErrorCondition( Config->I2C.SCL != oC_Pin_NotUsed       , oC_ErrorCode_PinNotSet        )
         && ErrorCondition( Config->I2C.SDA != oC_Pin_NotUsed       , oC_ErrorCode_PinNotSet        )
         && ErrorCondition( oC_GPIO_IsPinDefined(Config->I2C.SCL)   , oC_ErrorCode_PinNotDefined    )
         && ErrorCondition( oC_GPIO_IsPinDefined(Config->I2C.SDA)   , oC_ErrorCode_PinNotDefined    )
            )
        {
            oC_Struct_Define(oC_I2C_Config_t,i2cConfig);

            i2cConfig.Mode                          = oC_I2C_Mode_Master;
            i2cConfig.AddressMode                   = oC_I2C_AddressMode_7Bits;
            i2cConfig.Pins.SDA                      = Config->I2C.SDA;
            i2cConfig.Pins.SCL                      = Config->I2C.SCL;
            i2cConfig.SpeedMode                     = oC_I2C_SpeedMode_Normal;
            i2cConfig.Master.DefaultSlaveAddress    = Context->DeviceAddress;

            if( ErrorCode( oC_I2C_Configure(&i2cConfig,&Context->I2CContext) ) )
            {
                Context->I2CBusConfiguredByDriver = true;
                errorCode                         = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures I2C bus
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigureI2CBus( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Context->I2CBusConfiguredByDriver)
    {
        errorCode = oC_I2C_Unconfigure(NULL,&Context->I2CContext);
    }
    else
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief detects FT5336 chip on I2C bus
 */
//==========================================================================================================================================
static oC_ErrorCode_t DetectChip( oC_FT5336_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    uint8_t        chipId    = 0;

    if(
        ErrorCode     ( ReadRegister( Context, RegisterOffset_CHIP_ID, &chipId, ms(100)) )
     && ErrorCondition( chipId == CHIP_ID , oC_ErrorCode_ChipNotDetected                 )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sets work mode in the chip
 */
//==========================================================================================================================================
static oC_ErrorCode_t SetMode( oC_FT5336_Context_t Context, oC_FT5336_Mode_t Mode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Mode == oC_FT5336_Mode_InterruptMode)
    {
        errorCode = WriteRegister(Context, RegisterOffset_GMODE, 0x01, ms(100));
    }
    else
    {
        errorCode = WriteRegister(Context, RegisterOffset_GMODE, 0x00, ms(100));
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief prepares chip to work
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureChip( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCode( DetectChip(Context)              )
     && ErrorCode( SetMode(Context, Config->Mode)   )
        )
    {
        if(Config->Mode == oC_FT5336_Mode_InterruptMode)
        {
            if(
                ErrorCondition( Config->InterruptPin != oC_Pin_NotUsed      , oC_ErrorCode_PinNotSet        )
             && ErrorCondition( oC_GPIO_IsPinDefined(Config->InterruptPin)  , oC_ErrorCode_PinNotDefined    )
             && ErrorCondition( oC_GPIO_IsPinDefined(Config->InterruptPin)  , oC_ErrorCode_PinNotDefined    )
                )
            {
                oC_GPIO_Config_t config;

                memset(&config,0,sizeof(config));

                config.Pins             = Config->InterruptPin;
                config.Mode             = oC_GPIO_Mode_Input;
                config.OutputCircuit    = oC_GPIO_OutputCircuit_Default;
                config.Pull             = oC_GPIO_Pull_Down;
                config.Speed            = oC_GPIO_Speed_Maximum;
                config.InterruptTrigger = oC_GPIO_IntTrigger_RisingEdge;

                Context->InterruptPin   = Config->InterruptPin;

                errorCode = oC_GPIO_Configure( &config, &Context->InterruptPinContext );
            }
        }
        else if(ErrorCondition( Config->PollingTime > 0 , oC_ErrorCode_TimeNotCorrect ))
        {
            Context->RequestPeriod  = Config->PollingTime;
            errorCode               = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases chip
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigureChip( const oC_FT5336_Config_t * Config , oC_FT5336_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Context->Mode == oC_FT5336_Mode_InterruptMode)
    {
        oC_GPIO_Config_t config;

        memset(&config,0,sizeof(config));

        config.Pins             = Config->InterruptPin;
        config.Mode             = oC_GPIO_Mode_Input;
        config.OutputCircuit    = oC_GPIO_OutputCircuit_Default;
        config.Pull             = oC_GPIO_Pull_Default;
        config.Speed            = oC_GPIO_Speed_Maximum;
        config.InterruptTrigger = oC_GPIO_IntTrigger_RisingEdge;

        errorCode = oC_GPIO_Unconfigure(&config, &Context->InterruptPinContext );
    }
    else
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief reads device register value
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadRegister( oC_FT5336_Context_t Context, oC_I2C_Register_t Offset, uint8_t * outValue, oC_Time_t Timeout )
{
    oC_MemorySize_t size = sizeof(uint8_t);
    return oC_I2C_ReadRegister(Context->I2CContext, Context->DeviceAddress, Offset, outValue, &size, Timeout);
}

//==========================================================================================================================================
/**
 * @brief reads device register value
 */
//==========================================================================================================================================
static oC_ErrorCode_t WriteRegister( oC_FT5336_Context_t Context, oC_I2C_Register_t Offset, uint8_t Value, oC_Time_t Timeout )
{
    oC_MemorySize_t size = sizeof(uint8_t);
    return oC_I2C_WriteRegister(Context->I2CContext, Context->DeviceAddress, Offset, &Value, size, Timeout);
}

//==========================================================================================================================================
/**
 * @brief waits for touch detection
 */
//==========================================================================================================================================
static bool WaitForTouch( oC_FT5336_Context_t Context , uint16_t * outNumberOfTouches, oC_Time_t Timeout )
{
    bool            detected        = false;
    oC_Timestamp_t  endTimestamp    = timeouttotimestamp(Timeout);
    oC_Time_t       sleepTime       = 0;
    oC_Time_t       timeout         = 0;
    oC_ErrorCode_t  errorCode       = oC_ErrorCode_ImplementError;
    uint8_t         numberOfTouches = 0;

    if(Context->Mode == oC_FT5336_Mode_InterruptMode)
    {
        while( ErrorCondition( endTimestamp >= gettimestamp() , oC_ErrorCode_Timeout ) )
        {
            if(ErrorCode( ReadRegister(Context, RegisterOffset_TD_STAT, &numberOfTouches, gettimeout(endTimestamp)) ))
            {
                driverlog(oC_LogType_Track, "TD_STAT read: %d\n", numberOfTouches);

                errorCode = oC_ErrorCode_None;

                if(numberOfTouches > 0 && numberOfTouches <= MAX_TOUCHES)
                {
                    *outNumberOfTouches = (uint16_t)numberOfTouches;
                    detected            = true;
                    break;
                }
                else if( ErrorCode( oC_GPIO_WaitForPins(Context->InterruptPin,gettimeout(endTimestamp),0) ) == false )
                {
                    if(errorCode != oC_ErrorCode_Timeout)
                    {
                        driverlog(oC_LogType_Warning, "Unexpected error during waiting for touch: %R\n", errorCode);
                        break;
                    }
                    else
                    {
                        driverlog(oC_LogType_Track, "Waiting for interrupt pin timeout\n");
                    }
                }
            }
            else if(errorCode != oC_ErrorCode_Timeout)
            {
                driverlog(oC_LogType_Warning, "Unexpected error during waiting for touch: %R\n", errorCode);
            }
            else
            {
                driverlog(oC_LogType_Track, "Cannot read register TD_STAT, timeout\n");
            }
        }
    }
    else
    {
        errorCode = oC_ErrorCode_None;

        while( ErrorCode( ReadRegister(Context, RegisterOffset_TD_STAT, &numberOfTouches, gettimeout(endTimestamp)) ) )
        {
            if(numberOfTouches > 0 && numberOfTouches <= MAX_TOUCHES)
            {
                *outNumberOfTouches = (uint16_t)numberOfTouches;
                detected            = true;
                break;
            }
            timeout   = gettimeout(endTimestamp);
            sleepTime = oC_MIN( Context->RequestPeriod, timeout );
            sleep( sleepTime );
        }
    }

    if(errorCode != oC_ErrorCode_Timeout)
    {
        oC_SaveIfErrorOccur("Cannot read TD_STAT register: ", errorCode);
    }

    return detected;
}

//==========================================================================================================================================
/**
 * @brief reads touch position
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadTouchPosition( oC_FT5336_Context_t Context , uint16_t TouchId, oC_Pixel_Position_t * outPosition, oC_Pixel_ResolutionUInt_t * outWidth, oC_Time_t Timeout )
{
    oC_ErrorCode_t      errorCode       = oC_ErrorCode_ImplementError;
    oC_Timestamp_t      endTimestamp    = timeouttotimestamp(Timeout);
    oC_I2C_Register_t   xhOffset        = RegisterOffset_P1_XH     + (TouchId * 6);
    oC_I2C_Register_t   xlOffset        = RegisterOffset_P1_XL     + (TouchId * 6);
    oC_I2C_Register_t   yhOffset        = RegisterOffset_P1_YH     + (TouchId * 6);
    oC_I2C_Register_t   ylOffset        = RegisterOffset_P1_YL     + (TouchId * 6);
    oC_I2C_Register_t   weightOffset    = RegisterOffset_P1_WEIGHT + (TouchId * 6);
    uint8_t             xh = 0, xl = 0, yh = 0, yl = 0, weight = 0;

    if(
        ErrorCode( ReadRegister( Context, xhOffset    , &xh    , gettimeout(endTimestamp) ) )
     && ErrorCode( ReadRegister( Context, xlOffset    , &xl    , gettimeout(endTimestamp) ) )
     && ErrorCode( ReadRegister( Context, yhOffset    , &yh    , gettimeout(endTimestamp) ) )
     && ErrorCode( ReadRegister( Context, ylOffset    , &yl    , gettimeout(endTimestamp) ) )
     && ErrorCode( ReadRegister( Context, weightOffset, &weight, gettimeout(endTimestamp) ) )
        )
    {
        outPosition->X = ((xh & 0xF) << 8) | xl;
        outPosition->Y = ((yh & 0xF) << 8) | yl;
        *outWidth      = weight;
        errorCode      = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
