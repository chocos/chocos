/** ****************************************************************************************************************************************
 *
 * @brief      The file with definitions of chips for FMC module
 *
 * @file       oc_fmc_chips.c
 *
 * @author     Patryk Kubiak 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 * @copyright  This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_fmc.h>
#include <oc_fmc_chips.h>
#include <oc_struct.h>

#ifdef oC_FMC_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with SDRAM initialization prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________CHIP_INITIALIZATION_SECTION________________________________________________________________

static oC_ErrorCode_t Initialize_MT48LC4M32B2   ( const void * Config , oC_FMC_Context_t    Context , oC_Time_t * Timeout );

#undef  _________________________________________CHIP_INITIALIZATION_SECTION________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with definitions of FMC chips
 *
 *  ======================================================================================================================================*/
#define _________________________________________CHIPS_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief chip informations for MT48LC4M32B2
 */
//==========================================================================================================================================
const oC_FMC_ChipInfo_t oC_FMC_ChipInfo_MT48LC4M32B2 = {
    .ChipParameters.MemoryType                          = oC_FMC_LLD_MemoryType_SDRAM ,
    .ChipParameters.Name                                = "MT48LC4M32B2",
    .ChipParameters.SDRAM.Size                          = oC_MemorySize_Mib(128),
    .ChipParameters.SDRAM.BankSize                      = oC_MemorySize_Mib(32),
    .ChipParameters.SDRAM.NumberOfBanks                 = 4,
    .ChipParameters.SDRAM.DataBusWidth                  = oC_FMC_LLD_DataBusWidth_32Bits,
    .ChipParameters.SDRAM.CasLatencyMask                = oC_FMC_LLD_SDRAM_CasLatency_1
                                                        | oC_FMC_LLD_SDRAM_CasLatency_2
                                                        | oC_FMC_LLD_SDRAM_CasLatency_3 ,
    .ChipParameters.SDRAM.CasLatency                    = ns(20) ,
    .ChipParameters.SDRAM.BurstLengthMask               = oC_FMC_LLD_SDRAM_BurstLength_1
                                                        | oC_FMC_LLD_SDRAM_BurstLength_2
                                                        | oC_FMC_LLD_SDRAM_BurstLength_4
                                                        | oC_FMC_LLD_SDRAM_BurstLength_8
                                                        | oC_FMC_LLD_SDRAM_BurstLength_FullPage ,
    .ChipParameters.SDRAM.AutoPrechargePossible         = true,
    .ChipParameters.SDRAM.AutoRefreshPossible           = true,
    .ChipParameters.SDRAM.SelfPrechargePossible         = true,
    .ChipParameters.SDRAM.AutoRefreshPeriod             = ms(64),
    .ChipParameters.SDRAM.MaximumClockFrequency         = MHz(167) ,
    .ChipParameters.SDRAM.NumberOfRowAddressBits        = 12 ,
    .ChipParameters.SDRAM.NumberOfColumnAddressBits     = 8 ,
    .ChipParameters.SDRAM.ActiveToReadWriteDelay        = ns(20),
    .ChipParameters.SDRAM.PrechargeDelay                = ns(20),
    .ChipParameters.SDRAM.WriteRecoveryDelay            = ns(14),
    .ChipParameters.SDRAM.RefreshToActivateDelay        = ns(70),
    .ChipParameters.SDRAM.MinimumSelfRefreshPeriod      = ns(70),
    .ChipParameters.SDRAM.ExitSelfRefreshDelay          = ns(70),
    .ChipParameters.SDRAM.CyclesToDelayAfterLoadMode    = 2,
    .ChipParameters.SDRAM.Advanced.ReadPipeDelay        = ns(0) ,
    .ChipParameters.SDRAM.Advanced.UseBurstRead         = true ,
    .InitializationFunction                             = Initialize_MT48LC4M32B2 ,
};


#undef  _________________________________________CHIPS_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with SDRAM initialization functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________CHIP_INITIALIZATION_FUNCTIONS_SECTION______________________________________________________

//==========================================================================================================================================
/**
 * @brief function that initializes the chip
 */
//==========================================================================================================================================
static oC_ErrorCode_t Initialize_MT48LC4M32B2( const void * Config , oC_FMC_Context_t    Context , oC_Time_t * Timeout )
{
    oC_ErrorCode_t              errorCode = oC_ErrorCode_ImplementError;
    oC_FMC_SDRAM_CommandData_t  data;

    oC_Struct_Initialize(data,0);

    oC_Procedure_Begin
    {
        /* 1. Simultaneously apply power to VDD and VDDQ. */

        /*                                                                                               *
         * 2. Assert and hold CKE at a LVTTL logic LOW since all inputs and outputs are LVTTLcompatible. *
         * 3. Provide stable CLOCK signal. Stable clock is defined as a signal cycling within timing     *
         *    constraints specified for the clock pin.                                                   *
         *                                                                                               *
         *                                                                                               */
        oC_Procedure_ExitIfError(oC_FMC_SDRAM_SendCommand(Context,Timeout,oC_FMC_SDRAM_Command_EnableClock,NULL));

        /*                                                                                               *
         * 4. Wait at least 100μs prior to issuing any command other than a COMMAND INHIBIT              *
         *    or NOP.                                                                                    *
         * 5. Starting at some point during this 100μs period, bring CKE HIGH. Continuing at             *
         *    least through the end of this period, 1 or more COMMAND INHIBIT or NOP commands            *
         *    must be applied.                                                                           *
         *                                                                                               */
        sleep(us(50));

        oC_Procedure_ExitIfError(oC_FMC_SDRAM_SendCommand(Context,Timeout,oC_FMC_SDRAM_Command_Nop,NULL));

        sleep(us(50));

        /* 6. Perform a PRECHARGE ALL command. */
        data.Precharge.AllBanks     = true;
        data.Precharge.BankAddress  = 0;
        data.Precharge.Reserved     = 0;

        oC_Procedure_ExitIfError(oC_FMC_SDRAM_SendCommand(Context,Timeout,oC_FMC_SDRAM_Command_Precharge,&data));

        /*                                                                                               *
         * 7. Wait at least tRP time; during this time NOPs or DESELECT commands must be                 *
         *    given. All banks will complete their precharge, thereby placing the device in the all      *
         *    banks idle state.                                                                          *
         *                                                                                               */
        sleep(ns(20));

        oC_Procedure_ExitIfError(oC_FMC_SDRAM_SendCommand(Context,Timeout,oC_FMC_SDRAM_Command_Nop,NULL));

        data.AutoRefresh.NumberOfAutoRefresh = 8;
        data.AutoRefresh.Reserved            = 0;
        data.AutoRefresh.Reserved2           = false;

        /*                                                                                               *
         * 8. Issue an AUTO REFRESH command.                                                             *
         * 9. Wait at least tRFC time, during which only NOPs or COMMAND INHIBIT commands                *
         *    are allowed                                                                                *
         *                                                                                               */
        oC_Procedure_ExitIfError(oC_FMC_SDRAM_SendCommand(Context,Timeout,oC_FMC_SDRAM_Command_AutoRefresh,&data));

        sleep(ns(70));

        /*                                                                                               *
         * 10. Issue an AUTO REFRESH command.                                                            *
         * 11. Wait at least tRFC time, during which only NOPs or COMMAND INHIBIT commands               *
         *     are allowed                                                                               *
         *                                                                                               */
        data.AutoRefresh.NumberOfAutoRefresh = 8;

        oC_Procedure_ExitIfError(oC_FMC_SDRAM_SendCommand(Context,Timeout,oC_FMC_SDRAM_Command_AutoRefresh,&data));

        sleep(ns(70));

        /*
         * 12. The SDRAM is now ready for mode register programming. Because the mode register           *
         *     will power up in an unknown state, it should be loaded with desired bit values            *
         *     prior to applying any operational command. Using the LMR command, program                 *
         *     the mode register. The mode register is programmed via the MODE REGISTER SET              *
         *     command with BA1 = 0, BA0 = 0 and retains the stored information until it is programmed   *
         *     again or the device loses power. Not programming the mode register                        *
         *     upon initialization will result in default settings which may not be desired. Outputs     *
         *     are guaranteed High-Z after the LMR command is issued. Outputs should be                  *
         *     High-Z already before the LMR command is issued.                                          *
         *                                                                                               */
        data.LoadModeRegister.AllBanks          = true;
        data.LoadModeRegister.BurstLength       = 0;
        data.LoadModeRegister.BurstType         = 0;
        data.LoadModeRegister.CasLatency        = 2;
        data.LoadModeRegister.OperatingMode     = 0;
        data.LoadModeRegister.WriteBurstMode    = 1;

        oC_Procedure_ExitIfError(oC_FMC_SDRAM_SendCommand(Context,Timeout,oC_FMC_SDRAM_Command_LoadModeRegister,&data));

        errorCode = oC_FMC_FinishInitialization(Config,Context);
    }
    oC_Procedure_End

    return errorCode;
}

#undef  _________________________________________CHIP_INITIALIZATION_FUNCTIONS_SECTION______________________________________________________

#endif /* oC_FMC_LLD_AVAILABLE */
