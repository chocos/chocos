/** ****************************************************************************************************************************************
 *
 * @file       oc_fmc.c
 * 
 * File based on driver.c Ver 1.1.0
 *
 * @brief      The file with source for FMC driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2016-04-02 - 16:12:02)
 *
 * @copyright  Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_fmc.h>
#include <oc_fmc_lld.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_math.h>
#include <oc_memman.h>
#include <oc_struct.h>
#include <oc_gpio.h>

#ifdef oC_FMC_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         FMC
#define DRIVER_FILE_NAME    "fmc"
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_FMC_Configure
#define DRIVER_UNCONFIGURE  oC_FMC_Unconfigure
#define DRIVER_TURN_ON      oC_FMC_TurnOn
#define DRIVER_TURN_OFF     oC_FMC_TurnOff
#define IS_TURNED_ON        oC_FMC_IsTurnedOn
#define HANDLE_IOCTL        oC_FMC_Ioctl
#define READ_FROM_DRIVER    oC_FMC_Read
#define WRITE_TO_DRIVER     oC_FMC_Write

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores FMC context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t      ObjectControl;
    oC_FMC_LLD_Result_t     ConfigurationResult;
    oC_UInt_t               FileOffset;
    oC_FMC_Protection_t     Protection;
    oC_FMC_LLD_MemoryType_t MemoryType;
    oC_FMC_ChipInfo_t       ChipInfo;
    oC_Time_t               Timeout;
    bool                    UsedAsHeap;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all FMC driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "fmc module"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsContextCorrect                ( oC_FMC_Context_t        Context     );
static inline bool              IsMemoryTypeCorrect             ( oC_FMC_LLD_MemoryType_t MemoryType  );
static        oC_FMC_Context_t  Context_New                     ( const oC_FMC_Config_t * Config );
static        bool              Context_Delete                  ( oC_FMC_Context_t * outContext );
static        oC_ErrorCode_t    ReadData                        ( oC_FMC_Context_t Context , char * outData );
static        oC_ErrorCode_t    WriteData                       ( oC_FMC_Context_t Context , char Data );
static        oC_ErrorCode_t    ConfigureHeap                   ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    UnconfigureHeap                 ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    ConfigureMemoryInLld            ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    UnconfigureMemoryInLld          ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    FinishMemoryInitializationInLld ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    CopyChipInfo                    ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    VerifyChipInfo                  ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    VerifyNANDFlashChipParameters   ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    VerifyNORFlashChipParameters    ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    VerifySDRAMChipParameters       ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    VerifyPSRAMChipParameters       ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );
static        oC_ErrorCode_t    InitializeChip                  ( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * @ingroup FMC
 *
 * The function is for turning on the FMC module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 * It also turns on the LLD layer.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_FMC))
    {
        errorCode = oC_FMC_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_ErrorCode_None;
        }

        if(!oC_ErrorOccur(errorCode))
        {
            /* This must be always at the end of the function */
            oC_Module_TurnOn(oC_Module_FMC);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the FMC driver
 *
 * @ingroup FMC
 *
 * The function for turning off the FMC driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_FMC);

        errorCode = oC_FMC_LLD_TurnOffDriver();

        if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_FMC_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_FMC);
}

//==========================================================================================================================================
/**
 * @brief configures FMC pins to work
 *
 * @ingroup FMC
 *
 * The function is for configuration of the driver. Look at the #oC_FMC_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_Configure( const oC_FMC_Config_t * Config , oC_FMC_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC))
    {
        if(
           ErrorCondition( isaddresscorrect(Config) , oC_ErrorCode_WrongConfigAddress     ) &&
           ErrorCondition( isram(outContext)        , oC_ErrorCode_OutputAddressNotInRAM  )
           )
        {
            oC_FMC_Context_t context = Context_New(Config);

            if(ErrorCondition(context != NULL , oC_ErrorCode_AllocationError))
            {
                if(
                    oC_AssignErrorCode(&errorCode , CopyChipInfo(Config,context)        ) &&
                    oC_AssignErrorCode(&errorCode , VerifyChipInfo(Config,context)      ) &&
                    oC_AssignErrorCode(&errorCode , ConfigureMemoryInLld(Config,context)) &&
                    oC_AssignErrorCode(&errorCode , InitializeChip(Config,context)      ) &&
                    oC_AssignErrorCode(&errorCode , ConfigureHeap(Config,context)       )
                    )
                {
                    *outContext = context;
                    errorCode   = oC_ErrorCode_None;
                }
                else
                {
                    oC_SaveIfErrorOccur("FMC:Configure - cannot unconfigure after failure", UnconfigureMemoryInLld(Config,context));
                    oC_SaveIfFalse("FMC:Configure - cannot delete context: " , Context_Delete(&context) , oC_ErrorCode_ReleaseError);
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * @ingroup FMC
 *
 * The function is for reverting configuration from the #oC_FMC_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_Unconfigure( const oC_FMC_Config_t * Config , oC_FMC_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC))
    {
        if(
            ErrorCondition(isaddresscorrect(Config)      , oC_ErrorCode_WrongConfigAddress) &&
            ErrorCondition(isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM) &&
            ErrorCondition(IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect )
            )
        {
            if(
                oC_AssignErrorCode(&errorCode , UnconfigureHeap(Config,*outContext))
             && oC_AssignErrorCode(&errorCode , UnconfigureMemoryInLld(Config,*outContext))
                )
            {
                errorCode = Context_Delete(outContext);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * @ingroup FMC
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of errror
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_Ioctl( oC_FMC_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC                                 ) &&
        ErrorCondition( IsContextCorrect(Context)            , oC_ErrorCode_ContextNotCorrect   ) &&
        ErrorCondition( oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect   )
        )
    {
        switch(Command)
        {
            case oC_IoCtl_SpecialCommand_SetFileOffset:
                Context->FileOffset = *((uint32_t*)(Data));
                errorCode = oC_ErrorCode_None;
                break;
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief reads buffer from the driver
 * 
 * @ingroup FMC
 *
 * The function is for reading buffer by using FMC driver. It is called when someone will read the driver file. 
 *
 * @param Context 		Context of the driver
 * @param outBuffer     Buffer for data
 * @param Size          Size of the buffer
 * @param IoFlags       Input/Output mode flags
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_Read( oC_FMC_Context_t Context , char * outBuffer , oC_MemorySize_t Size , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC) &&
        ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect          ) &&
        ErrorCondition( isram(outBuffer)                                    , oC_ErrorCode_OutputAddressNotInRAM      ) &&
        ErrorCondition( Size > 0                                            , oC_ErrorCode_SizeNotCorrect             ) &&
        ErrorCondition( Context->Protection & oC_FMC_Protection_AllowRead   , oC_ErrorCode_ReadingNotPermitted        )
        )
    {
        errorCode = oC_ErrorCode_None;

        for(uint32_t index = 0; index < Size && !oC_ErrorOccur(errorCode) ; index++)
        {
            errorCode = ReadData(Context,&outBuffer[index]);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief write buffer to the driver
 * 
 * @ingroup FMC
 *
 * The function is for writing buffer by using FMC driver. It is called when someone will write the driver file. 
 *
 * @param Context 		Context of the driver
 * @param Buffer        Buffer with data
 * @param Size          Size of the buffer
 * @param IoFlags       Input/Output mode flags
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_Write( oC_FMC_Context_t Context , const char * Buffer , oC_MemorySize_t Size , oC_IoFlags_t IoFlags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC) &&
        ErrorCondition( IsContextCorrect(Context)                               , oC_ErrorCode_ContextNotCorrect      ) &&
        ErrorCondition( isaddresscorrect(Buffer)                                , oC_ErrorCode_WrongAddress           ) &&
        ErrorCondition( Size > 0                                                , oC_ErrorCode_SizeNotCorrect         ) &&
        ErrorCondition( Context->Protection & oC_FMC_Protection_AllowWrite      , oC_ErrorCode_WritingNotPermitted    )
        )
    {
        errorCode = oC_ErrorCode_None;

        for(uint32_t index = 0; index < Size && !oC_ErrorOccur(errorCode) ; index++)
        {
            errorCode = WriteData(Context,Buffer[index]);
        }

    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief sends command to SDRAM chips
 *
 * @ingroup FMC
 *
 * The function for sending SDRAM commands mainly required during chip initialization.
 *
 * @param Context       Context of the driver received during configuration
 * @param Timeout       Pointer to the maximum time for the initialization (will be reduced about initialization time). If the time will left before end of initialization, the function should ends with timeout error
 * @param Command       Command to send
 * @param Data          Data of command to send (some of commands does not require the argument and then it can be set to NULL. For more info look at the #oC_FMC_LLD_SDRAM_Command_t and #oC_FMC_SDRAM_CommandData_t types description)
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_SDRAM_SendCommand( oC_FMC_Context_t Context , oC_Time_t * Timeout , oC_FMC_SDRAM_Command_t Command , const oC_FMC_SDRAM_CommandData_t * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC)
     && ErrorCondition( IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect )
     && ErrorCondition( isaddresscorrect(Timeout) , oC_ErrorCode_WrongAddress      )
     )
    {
        errorCode = oC_FMC_LLD_SendSDRAMCommand(&Context->ConfigurationResult,Timeout,Command,Data);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns an address for direct access to the memory
 *
 * The function returns an address to direct access to the memory (if it is available)
 *
 * @param Context               Context of the FMC configuration
 * @param outAddress            Destination for the address
 * @param outMemorySize         Size of the memory
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_ReadDirectAddress( oC_FMC_Context_t Context , void ** outAddress , oC_MemorySize_t * outMemorySize )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC)
     && ErrorCondition( IsContextCorrect(Context)                           , oC_ErrorCode_ContextNotCorrect        )
     && ErrorCondition( isram(outAddress)                                   , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(outMemorySize)                                , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Context->ConfigurationResult.MemoryStart != NULL    , oC_ErrorCode_DirectAccessNotPossible  )
     )
    {
        *outAddress     = Context->ConfigurationResult.MemoryStart;
        *outMemorySize  = Context->ConfigurationResult.MemorySize;
        errorCode       = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes initialization
 *
 * The function should be called at the end of the chip initialization procedure. It finishes initialization by performing all required
 * additional configuration.
 *
 * @param Config             Pointer to the FMC configuration structure
 * @param Context            Context of the FMC from the #oC_FMC_Configure function
 *
 * @return code of error or #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_FMC_FinishInitialization( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_FMC))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)      , oC_ErrorCode_ContextNotCorrect     )
         && ErrorCondition( isaddresscorrect(Config)       , oC_ErrorCode_WrongConfigAddress    )
            )
        {
            errorCode = FinishMemoryInitializationInLld(Config,Context);
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the FMC driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_FMC_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_FmcContext,Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief returns true if memory type is correct
 */
//==========================================================================================================================================
static inline bool IsMemoryTypeCorrect( oC_FMC_LLD_MemoryType_t MemoryType  )
{
    return (MemoryType == oC_FMC_LLD_MemoryType_NAND_Flash)
        || (MemoryType == oC_FMC_LLD_MemoryType_NOR_Flash )
        || (MemoryType == oC_FMC_LLD_MemoryType_PSRAM     )
        || (MemoryType == oC_FMC_LLD_MemoryType_SDRAM     );
}

//==========================================================================================================================================
/**
 * @brief allocates memory for new context
 */
//==========================================================================================================================================
static oC_FMC_Context_t Context_New( const oC_FMC_Config_t * Config )
{
    oC_FMC_Context_t context = kmalloc(sizeof(struct Context_t) , &Allocator , AllocationFlags_ZeroFill);

    if(context)
    {
        context->ObjectControl = oC_CountObjectControl(context,oC_ObjectId_FmcContext);
        context->FileOffset    = 0;
        context->Timeout       = Config->MaximumTimeForConfiguration;
    }

    return context;
}
//==========================================================================================================================================
/**
 * @brief releases memory allocated for context
 */
//==========================================================================================================================================
static bool Context_Delete( oC_FMC_Context_t * outContext )
{
    bool result              = false;
    oC_FMC_Context_t context = *outContext;

    context->ObjectControl = 0;

    if(kfree(context,AllocationFlags_CanWaitForever))
    {
        *outContext = NULL;
        result      = true;
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief Reads one byte from the configured buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadData( oC_FMC_Context_t Context , char * outData )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Context->ConfigurationResult.DirectAccessProtection & oC_FMC_LLD_Protection_AllowRead)
    {
        *outData = Context->ConfigurationResult.MemoryStart[Context->FileOffset++];
        errorCode = oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_NotImplemented;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Writes one byte to the configured buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t WriteData( oC_FMC_Context_t Context , char Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Context->ConfigurationResult.DirectAccessProtection & oC_FMC_LLD_Protection_AllowWrite)
    {
        Context->ConfigurationResult.MemoryStart[Context->FileOffset++] = Data;
        errorCode = oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_NotImplemented;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures memory as heap
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureHeap( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
       Config->HeapUsage == oC_FMC_HeapUsage_UseAsHeapIfPossible &&
       oC_Bits_AreBitsSetU32(Context->MemoryType                                 , oC_FMC_LLD_MemoryType_RAM                                    ) &&
       oC_Bits_AreBitsSetU32(Context->ConfigurationResult.DirectAccessProtection , oC_FMC_Protection_AllowRead | oC_FMC_Protection_AllowWrite   )
       )
    {
        errorCode = oC_MemMan_ConfigureExternalHeapMap(Context->ConfigurationResult.MemoryStart,Context->ConfigurationResult.MemorySize);
        if(!oC_ErrorOccur(errorCode))
        {
            Context->UsedAsHeap = true;
        }
    }
    else
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures memory as heap
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigureHeap( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(Context->UsedAsHeap == true)
    {
        errorCode = oC_MemMan_UnconfigureExternalHeapMap();
    }
    else
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief configures memory (calls a LLD function according to the memory type)
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConfigureMemoryInLld( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(Context->MemoryType)
    {
        case oC_FMC_LLD_MemoryType_NAND_Flash:
            errorCode = oC_FMC_LLD_ConfigureNANDFlash(&Config->NANDFlash,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_NOR_Flash:
            errorCode = oC_FMC_LLD_ConfigureNORFlash(&Config->NORFlash,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_SDRAM:
            errorCode = oC_FMC_LLD_ConfigureSDRAM(&Config->SDRAM,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_PSRAM:
            errorCode = oC_FMC_LLD_ConfigurePSRAM(&Config->PSRAM,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        default:
            errorCode = oC_ErrorCode_MemoryTypeNotCorrect;
            break;
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unconfigures memory (calls a LLD function according to the memory type)
 */
//==========================================================================================================================================
static oC_ErrorCode_t UnconfigureMemoryInLld( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(Context->MemoryType)
    {
        case oC_FMC_LLD_MemoryType_NAND_Flash:
            errorCode = oC_FMC_LLD_UnconfigureNANDFlash(&Config->NANDFlash,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_NOR_Flash:
            errorCode = oC_FMC_LLD_UnconfigureNORFlash(&Config->NORFlash,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_SDRAM:
            errorCode = oC_FMC_LLD_UnconfigureSDRAM(&Config->SDRAM,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_PSRAM:
            errorCode = oC_FMC_LLD_UnconfigurePSRAM(&Config->PSRAM,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        default:
            errorCode = oC_ErrorCode_MemoryTypeNotCorrect;
            break;
    }
    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief finishes memory initialization (calls a LLD function according to the memory type)
 */
//==========================================================================================================================================
static oC_ErrorCode_t FinishMemoryInitializationInLld( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(Context->MemoryType)
    {
        case oC_FMC_LLD_MemoryType_NAND_Flash:
            errorCode = oC_FMC_LLD_FinishNANDFlashInitialization(&Config->NANDFlash,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_NOR_Flash:
            errorCode = oC_FMC_LLD_FinishNORFlashInitialization(&Config->NORFlash,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_SDRAM:
            errorCode = oC_FMC_LLD_FinishSDRAMInitialization(&Config->SDRAM,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        case oC_FMC_LLD_MemoryType_PSRAM:
            errorCode = oC_FMC_LLD_FinishPSRAMInitialization(&Config->PSRAM,&Context->ChipInfo.ChipParameters,&Context->ConfigurationResult);
            break;
        default:
            errorCode = oC_ErrorCode_MemoryTypeNotCorrect;
            break;
    }
    return errorCode;
}


//==========================================================================================================================================
/**
 * @brief searches for informations about chip
 */
//==========================================================================================================================================
static oC_ErrorCode_t CopyChipInfo( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(isaddresscorrect(Config->ChipInfo))
    {
        memcpy(&Context->ChipInfo,Config->ChipInfo,sizeof(Context->ChipInfo));
        Context->MemoryType = Config->ChipInfo->ChipParameters.MemoryType;
        errorCode           = oC_ErrorCode_None;
    }
    else
    {
        errorCode = oC_ErrorCode_ChipNotDefined;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief verify, that the given chip info is correct
 */
//==========================================================================================================================================
static oC_ErrorCode_t VerifyChipInfo( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(IsMemoryTypeCorrect(Context->ChipInfo.ChipParameters.MemoryType) , oC_ErrorCode_MemoryTypeNotCorrect )
     && ErrorCondition(isaddresscorrect(Context->ChipInfo.InitializationFunction)       , oC_ErrorCode_WrongAddress         )
        )
    {
        switch(Context->MemoryType)
        {
            case oC_FMC_LLD_MemoryType_NAND_Flash:
                errorCode = VerifyNANDFlashChipParameters(Config,Context);
                break;
            case oC_FMC_LLD_MemoryType_NOR_Flash:
                errorCode = VerifyNORFlashChipParameters(Config,Context);
                break;
            case oC_FMC_LLD_MemoryType_SDRAM:
                errorCode = VerifySDRAMChipParameters(Config,Context);
                break;
            case oC_FMC_LLD_MemoryType_PSRAM:
                errorCode = VerifyPSRAMChipParameters(Config,Context);
                break;
            default:
                errorCode = oC_ErrorCode_MemoryTypeNotCorrect;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief verifies chip info structure for the NAND flash memory type
 */
//==========================================================================================================================================
static oC_ErrorCode_t VerifyNANDFlashChipParameters( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;



    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief verifies chip info structure for the NOR Flash memory type
 */
//==========================================================================================================================================
static oC_ErrorCode_t VerifyNORFlashChipParameters( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;



    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief verifies chip parameters structure for the SDRAM memory type
 *
 * @warning It verifies only basic fields of the structure. The others are verified during configuration
 */
//==========================================================================================================================================
static oC_ErrorCode_t VerifySDRAMChipParameters( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(Context->ChipInfo.ChipParameters.SDRAM.Size          > 0  ,   oC_ErrorCode_SizeNotCorrect)
     && ErrorCondition(Context->ChipInfo.ChipParameters.SDRAM.BankSize      > 0  ,   oC_ErrorCode_SizeNotCorrect)
     && ErrorCondition(Context->ChipInfo.ChipParameters.SDRAM.NumberOfBanks > 0  ,   oC_ErrorCode_NumberOfBanksNotCorrect)
     )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief verifies chip info structure for the PSRAM memory type
 */
//==========================================================================================================================================
static oC_ErrorCode_t VerifyPSRAMChipParameters( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context  )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;



    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief calls initialization of the chip
 */
//==========================================================================================================================================
static oC_ErrorCode_t InitializeChip( const oC_FMC_Config_t * Config , oC_FMC_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(ErrorCondition(isaddresscorrect(Config->ChipInfo),oC_ErrorCode_ChipNotDefined))
    {
        if(isaddresscorrect(Context->ChipInfo.InitializationFunction))
        {
            errorCode = Context->ChipInfo.InitializationFunction(Config,Context,&Context->Timeout);
        }
        else
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

#endif
