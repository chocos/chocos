/** ****************************************************************************************************************************************
 *
 * @file       oc_driver.c
 *
 * @brief      __FILE__DESCRIPTION__
 *
 * @author     Patryk Kubiak - (Created on: 6 wrz 2015 17:40:49) 
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_driver.h>
#include <oc_stdlib.h>
#include <oc_ioctl.h>

/** ========================================================================================================================================
 *
 *              The section with local variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

static oC_Driver_InstanceAddedCallback_t        InstanceAddedCallback   = NULL;
static oC_Driver_InstanceRemovedCallback_t      InstanceRemovedCallback = NULL;

#undef  _________________________________________VARIABLES__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with if functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________IF_FUNCTIONS_SECTION_______________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Driver_IsDriverRequiredBy( oC_Driver_t Driver , oC_Driver_t RequiredDriver )
{
    bool required = false;

    if(isaddresscorrect(Driver) && isaddresscorrect(RequiredDriver))
    {
        for(uint16_t driverIndex = 0; driverIndex < Driver->RequiredCount ; driverIndex++)
        {
            if(Driver->RequiredList[driverIndex] == RequiredDriver)
            {
                required = true;
                break;
            }
        }
    }

    return required;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Driver_Type_t oC_Driver_GetDriverType( oC_Driver_t Driver )
{
    oC_Driver_Type_t type = oC_Driver_Type_Incorrect;

    if(isaddresscorrect(Driver))
    {
        type = Driver->Type;
    }

    return type;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Driver_ReadRequiredList( oC_Driver_t Driver , oC_Driver_RequiredList_t * outRequiredList , uint16_t * outRequiredListCount )
{
    bool read = false;

    if(isaddresscorrect(Driver) && isram(outRequiredList) && isram(outRequiredListCount))
    {
        *outRequiredList        = Driver->RequiredList;
        *outRequiredListCount   = Driver->RequiredCount;
    }

    return read;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_TurnOn( oC_Driver_t Driver )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) , oC_ErrorCode_WrongAddress )
        )
    {
        if(Driver->IsTurnedOn == NULL)
        {
            errorCode = oC_ErrorCode_None;
        }
        else if(isaddresscorrect(Driver->IsTurnedOn) == false)
        {
            errorCode = oC_ErrorCode_WrongAddress;
        }
        else if(Driver->IsTurnedOn())
        {
            errorCode = oC_ErrorCode_ModuleIsTurnedOn;
        }
        else
        {
            if(Driver->TurnOn == NULL)
            {
                errorCode = oC_ErrorCode_None;
            }
            if(isaddresscorrect(Driver->TurnOn))
            {
                errorCode = Driver->TurnOn();
            }
            else
            {
                errorCode = oC_ErrorCode_WrongAddress;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_TurnOff( oC_Driver_t Driver )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) , oC_ErrorCode_WrongAddress )
        )
    {
        if(Driver->IsTurnedOn == NULL)
        {
            errorCode = oC_ErrorCode_None;
        }
        else if(isaddresscorrect(Driver->IsTurnedOn) == false)
        {
            errorCode = oC_ErrorCode_WrongAddress;
        }
        else if(Driver->IsTurnedOn() == false)
        {
            errorCode = oC_ErrorCode_ModuleNotStartedYet;
        }
        else
        {
            if(Driver->TurnOff == NULL)
            {
                errorCode = oC_ErrorCode_None;
            }
            if(isaddresscorrect(Driver->TurnOff))
            {
                errorCode = Driver->TurnOff();
            }
            else
            {
                errorCode = oC_ErrorCode_WrongAddress;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Driver_IsTurnedOn( oC_Driver_t Driver )
{
    bool turnedOn = false;

    if(isaddresscorrect(Driver))
    {
        if(Driver->IsTurnedOn == NULL)
        {
            turnedOn = true;
        }
        else if(isaddresscorrect(Driver->IsTurnedOn))
        {
            turnedOn = Driver->IsTurnedOn();
        }
    }

    return turnedOn;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_Configure( oC_Driver_t Driver , const void * Config , void ** outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,            oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->Configure) , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->Configure(Config,outContext);

        if( InstanceAddedCallback != NULL && !oC_ErrorOccur(errorCode) )
        {
            oC_SaveIfErrorOccur("Cannot add driver instance", InstanceAddedCallback( Driver, Config, *outContext ));
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_Unconfigure( oC_Driver_t Driver , const void * Config , void ** outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,             oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->Unconfigure), oC_ErrorCode_NotHandledByDriver ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext)                    , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        oC_Driver_Context_t context = *outContext;

        errorCode = Driver->Unconfigure(Config,outContext);

        if( InstanceRemovedCallback != NULL && !oC_ErrorOccur(errorCode) )
        {
            oC_SaveIfErrorOccur("Cannot remove driver instance", InstanceRemovedCallback( Driver, context ));
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_Read( oC_Driver_t Driver , void * Context , char * outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,             oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->Read),        oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->Read(Context,outBuffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_Write( oC_Driver_t Driver , void * Context , const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,             oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->Write),       oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->Write(Context,Buffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_HandleIoctl( oC_Driver_t Driver , void * Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,             oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , oC_Ioctl_IsCorrectCommand(Command),    oC_ErrorCode_CommandNotCorrect )
        )
    {
        if(Command == oC_IoCtl_SpecialCommand_GetDriverVersion)
        {
            if(isram(Data))
            {
                *((uint32_t*)Data) = Driver->Version;
                errorCode          = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_OutputAddressNotInRAM;
            }
        }
        else if(ErrorCondition(isaddresscorrect(Driver->HandleIoctl), oC_ErrorCode_NotHandledByDriver ))
        {
            errorCode = Driver->HandleIoctl(Context,Command,Data);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_ReadColorMap( oC_Driver_t Driver , void * Context , oC_ColorMap_t ** outColorMap )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,              oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->ReadColorMap), oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->ReadColorMap(Context,outColorMap);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_SetResolution( oC_Driver_t Driver , void * Context , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,               oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->SetResolution), oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->SetResolution(Context,Width,Height);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_ReadResolution( oC_Driver_t Driver , void * Context , oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,                oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->ReadResolution), oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->ReadResolution(Context,outWidth,outHeight);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_SendFrame( oC_Driver_t Driver ,  void * Context , const oC_Net_Frame_t * Frame , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,                oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->SendFrame) ,     oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->SendFrame(Context,Frame,Timeout);
    }

    return errorCode;
}

//=================================1=========================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_ReceiveFrame( oC_Driver_t Driver ,  void * Context , oC_Net_Frame_t * outFrame , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,                oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->ReceiveFrame)  , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->ReceiveFrame(Context,outFrame,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_SetWakeOnLanEvent( oC_Driver_t Driver ,  void * Context , oC_Event_t WolEvent )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,                    oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->SetWakeOnLanEvent) , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->SetWakeOnLanEvent(Context,WolEvent);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_Flush( oC_Driver_t Driver ,  void * Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,               oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->Flush) ,        oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->Flush(Context);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_SetLoopback( oC_Driver_t Driver ,  void * Context , oC_Net_Layer_t Layer , bool Enabled )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,               oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->SetLoopback) ,  oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->SetLoopback(Context,Layer,Enabled);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_PerformDiagnostics( oC_Driver_t Driver ,  void * Context , oC_Diag_t * outDiags , uint32_t * NumberOfDiags )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,                       oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->PerformDiagnostics) ,   oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->PerformDiagnostics(Context,outDiags,NumberOfDiags);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_ReadNetInfo( oC_Driver_t Driver , void * Context , oC_Net_Info_t * outInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,                       oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->ReadNetInfo) ,          oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->ReadNetInfo(Context,outInfo);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_WaitForInput( oC_Driver_t Driver , void * Context , oC_IDI_EventId_t EventsMask , oC_IDI_Event_t * outEvent , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver) ,                       oC_ErrorCode_WrongAddress       ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->WaitForInput) ,         oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->WaitForInput(Context,EventsMask,outEvent,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Driver_IsEventSupported( oC_Driver_t Driver , void * Context , oC_IDI_EventId_t EventId )
{
    bool isSupported = false;

    if( isaddresscorrect(Driver) && isaddresscorrect(Driver->IsEventSupported) )
    {
        isSupported = Driver->IsEventSupported(Context,EventId);
    }

    return isSupported;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_SwitchLayer( oC_Driver_t Driver , void * Context , oC_ColorMap_LayerIndex_t Layer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                 , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->SwitchLayer)    , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->SwitchLayer(Context,Layer);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_WaitForNewDisk( oC_Driver_t Driver , void * Context , oC_DiskId_t * outDiskId , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                     , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->WaitForNewDisk)  , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->WaitForNewDisk(Context,outDiskId,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_ReadSectorSize( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_MemorySize_t * outSectorSize , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                     , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->ReadSectorSize)     , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->ReadSectorSize(Context,DiskId,outSectorSize,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_ReadNumberOfSectors( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t * outSectorNumber , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                         , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->ReadNumberOfSectors)    , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->ReadNumberOfSectors(Context,DiskId,outSectorNumber,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_ReadSectors( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, void *    outBuffer , oC_MemorySize_t Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                 , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->ReadSectors)    , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->ReadSectors(Context,DiskId,StartSector,outBuffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_WriteSectors( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, const void * Buffer , oC_MemorySize_t Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                 , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->WriteSectors)   , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->WriteSectors(Context,DiskId,StartSector,Buffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_EraseSectors( oC_Driver_t Driver , void * Context , oC_DiskId_t DiskId , oC_SectorNumber_t StartSector, oC_MemorySize_t Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                 , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->EraseSectors)   , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->EraseSectors(Context,DiskId,StartSector,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_WaitForDiskEject( oC_Driver_t Driver , void * Context , oC_DiskId_t * outDiskId , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver)                         , oC_ErrorCode_WrongAddress       )
     && oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Driver->WaitForDiskEject)    , oC_ErrorCode_NotHandledByDriver )
        )
    {
        errorCode = Driver->WaitForDiskEject(Context,outDiskId,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_SetInstanceAddedCallback( oC_Driver_InstanceAddedCallback_t     Callback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( Callback == NULL || isaddresscorrect(Callback) , oC_ErrorCode_WrongAddress ) )
    {
        InstanceAddedCallback = Callback;
        errorCode             = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Driver_SetInstanceRemovedCallback( oC_Driver_InstanceRemovedCallback_t   Callback )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( Callback == NULL || isaddresscorrect(Callback) , oC_ErrorCode_WrongAddress ) )
    {
        InstanceRemovedCallback = Callback;
        errorCode               = oC_ErrorCode_None;
    }

    return errorCode;
}

#undef  _________________________________________IF_FUNCTIONS_SECTION_______________________________________________________________________

