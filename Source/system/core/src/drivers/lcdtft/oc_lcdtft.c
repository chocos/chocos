/** ****************************************************************************************************************************************
 *
 * @file       oc_lcdtft.c
 * 
 * File based on driver.c Ver 1.1.0
 *
 * @brief      The file with source for LCDTFT driver interface
 *
 * @author     Patryk Kubiak - (Created on: 2016-02-06 - 11:43:03)
 *
 * @copyright  Copyright (C) [YEAR] Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *             This program is free software; you can redistribute it and/or modify
 *             it under the terms of the GNU General Public License as published by
 *             the Free Software Foundation; either version 2 of the License, or
 *             (at your option) any later version.
 *
 *             This program is distributed in the hope that it will be useful,
 *             but WITHOUT ANY WARRANTY; without even the implied warranty of
 *             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *             GNU General Public License for more details.
 *
 *             You should have received a copy of the GNU General Public License
 *             along with this program; if not, write to the Free Software
 *             Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

 
#include <oc_lcdtft.h>
#include <oc_lcdtft_lld.h>
#include <oc_compiler.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_null.h>
#include <oc_gpio.h>
#include <string.h>

#ifdef oC_LCDTFT_LLD_AVAILABLE

/** ========================================================================================================================================
 *
 *              The section with driver definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

#define DRIVER_SOURCE

/* Driver basic definitions */
#define DRIVER_NAME         LCDTFT
#define DRIVER_FILE_NAME    "lcdtft"
#define DRIVER_TYPE         GRAPHICS_DRIVER
#define DRIVER_VERSION      oC_Driver_MakeVersion(1,0,0)
#define REQUIRED_DRIVERS    &GPIO
#define REQUIRED_BOOT_LEVEL oC_Boot_Level_RequireMemoryManager | oC_Boot_Level_RequireDriversManager

/* Driver interface definitions */
#define DRIVER_CONFIGURE    oC_LCDTFT_Configure
#define DRIVER_UNCONFIGURE  oC_LCDTFT_Unconfigure
#define DRIVER_TURN_ON      oC_LCDTFT_TurnOn
#define DRIVER_TURN_OFF     oC_LCDTFT_TurnOff
#define IS_TURNED_ON        oC_LCDTFT_IsTurnedOn
#define HANDLE_IOCTL        oC_LCDTFT_Ioctl
#define READ_COLOR_MAP      oC_LCDTFT_ReadColorMap
#define SET_RESOLUTION      oC_LCDTFT_SetResolution
#define READ_RESOLUTION     oC_LCDTFT_ReadResolution
#define SWITCH_LAYER        oC_LCDTFT_SwitchLayer

#undef  _________________________________________DRIVER_DEFINITIONS_SECTION_________________________________________________________________

/* This header must be always at the end of include list */
#include <oc_driver.h>

/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores LCDTFT context 
 * 
 * The structure is for storing context of the driver. 
 */
//==========================================================================================================================================
struct Context_t
{
    /** The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t                  ObjectControl;
    oC_LCDTFT_LLD_TimingParameters_t    TimingParameters;
    union
    {
        oC_ColorMap_t*          ColorMap;
        const oC_ColorMap_t*    ConstColorMap;
    };
    bool                        ColorMapAllocated;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * The 'Allocator' for this driver. It should be used for all LCDTFT driver kernel allocations. 
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
                .Name = "lcdtft"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static inline bool              IsContextCorrect      ( oC_LCDTFT_Context_t Context );
static        oC_ErrorCode_t    Context_New           ( const oC_LCDTFT_Config_t * Config , oC_LCDTFT_Context_t * outContext );
static        oC_ErrorCode_t    Context_Delete        ( oC_LCDTFT_Context_t * outContext );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on the LCDTFT module. If the module is already turned on, it will return `oC_ErrorCode_ModuleIsTurnedOn` error.
 * It also turns on the LLD layer.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOffVerification(&errorCode , oC_Module_LCDTFT))
    {
        errorCode = oC_LCDTFT_LLD_TurnOnDriver();

        if(errorCode == oC_ErrorCode_ModuleIsTurnedOn)
        {
            errorCode = oC_ErrorCode_None;
        }

        if(!oC_ErrorOccur(errorCode))
        {
            /* This must be always at the end of the function */
            oC_Module_TurnOn(oC_Module_LCDTFT);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Turns off the LCDTFT driver
 *
 * The function for turning off the LCDTFT driver. If the driver not started yet, it will return `oC_ErrorCode_ModuleNotStartedYet` error code.
 * It also turns off the LLD.
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        /* This must be at the start of the function */
        oC_Module_TurnOff(oC_Module_LCDTFT);

        errorCode = oC_LCDTFT_LLD_TurnOffDriver();

        if(errorCode == oC_ErrorCode_ModuleNotStartedYet)
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief checks if the driver is turned on
 *
 * @return true if driver is turned on
 */
//==========================================================================================================================================
bool oC_LCDTFT_IsTurnedOn( void )
{
    return oC_Module_IsTurnedOn(oC_Module_LCDTFT);
}

//==========================================================================================================================================
/**
 * @brief configures LCDTFT pins to work
 *
 * The function is for configuration of the driver. Look at the #oC_LCDTFT_Config_t structure description and fields list to get more info.
 *
 * @param Config        Pointer to the configuration structure
 * @param outContext    Destination for the driver context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_Configure( const oC_LCDTFT_Config_t * Config , oC_LCDTFT_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
           ErrorCondition( isram(Config) || isrom(Config)                , oC_ErrorCode_WrongConfigAddress        ) &&
           ErrorCondition( isram(outContext)                             , oC_ErrorCode_OutputAddressNotInRAM     ) &&
           ErrorCondition( Config->Width > 0 && Config->Height > 0       , oC_ErrorCode_ResolutionNotSet          )
           )
        {
            oC_IntMan_EnterCriticalSection();
            oC_LCDTFT_Context_t context = NULL;
            if(
                oC_AssignErrorCode(&errorCode , Context_New(Config,&context)                                                                    ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetPower(oC_Power_On)                                                             ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_DisableOperations()                                                               ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetPixelFormat(Config->ColorFormat)                                               ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_ConnectPins(&Config->Pins)                                                        ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetFrequency(Config->ClockFrequency , Config->PermissibleDifference)              ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetPixelClockPolarity(Config->PixelClockPolarity)                                 ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetPolarities(Config->HSyncPolarity,Config->VSyncPolarity,Config->DESyncPolarity) ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetResolution(Config->Width,Config->Height)                                       ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetTimingParameters(&Config->TimingParameters)                                    ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_SetColormapBuffer(context->ColorMap->Map)                                         ) &&
                oC_AssignErrorCode(&errorCode , oC_LCDTFT_LLD_EnableOperations()                                                                )
                )
            {
                *outContext = context;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfErrorOccur("LCDTFT-Configure: Error while deleting context - " , Context_Delete(&context));
            }
            oC_IntMan_ExitCriticalSection();
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unconfigures the driver
 * 
 * The function is for reverting configuration from the #oC_LCDTFT_Configure function. 
 *
 * @param Config        Pointer to the configuration
 * @param outContext    Destination for the context structure
 *
 * @return code of error
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_Unconfigure( const oC_LCDTFT_Config_t * Config , oC_LCDTFT_Context_t * outContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
            ErrorCondition( isaddresscorrect(Config)      , oC_ErrorCode_WrongConfigAddress) &&
            ErrorCondition( isram(outContext)             , oC_ErrorCode_OutputAddressNotInRAM) &&
            ErrorCondition( IsContextCorrect(*outContext) , oC_ErrorCode_ContextNotCorrect )
            )
        {
            if(
                oC_AssignErrorCode( &errorCode , oC_LCDTFT_LLD_RestoreDefaultState()            ) &&
                oC_AssignErrorCode( &errorCode , oC_LCDTFT_LLD_DisconnectPins(&Config->Pins)    ) &&
                oC_AssignErrorCode( &errorCode , Context_Delete(outContext)                     )
                )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief handles input/output driver commands
 *
 * The function is for handling input/output control commands. It will be called for non-standard operations from the userspace. 
 * 
 * @param Context    Context of the driver 
 * @param Command    Command to execute
 * @param Data       Data for the command or NULL if not used
 * 
 * @return code of errror
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_Ioctl( oC_LCDTFT_Context_t Context , oC_Ioctl_Command_t Command , void * Data )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT                              ) &&
        ErrorCondition( isaddresscorrect(Context)            , oC_ErrorCode_WrongAddress        ) &&
        ErrorCondition( oC_Ioctl_IsCorrectCommand(Command)   , oC_ErrorCode_CommandNotCorrect   ) &&
        ErrorCondition( IsContextCorrect(Context)            , oC_ErrorCode_ContextNotCorrect   )
        )
    {
        switch(Command)
        {
            //==============================================================================================================================
            /*
             * Not handled commands
             */
            //==============================================================================================================================
            default:
                errorCode = oC_ErrorCode_CommandNotHandled;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_SetBackgroundColor( oC_LCDTFT_Context_t Context , oC_Color_t Color , oC_ColorFormat_t ColorFormat )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)             , oC_ErrorCode_ContextNotCorrect      ) &&
            ErrorCondition( oC_Color_IsCorrect(Color)             , oC_ErrorCode_ColorNotCorrect        ) &&
            ErrorCondition( oC_ColorFormat_IsCorrect(ColorFormat) , oC_ErrorCode_ColorFormatNotCorrect  )
            )
        {
            errorCode = oC_LCDTFT_LLD_SetBackgroundColor(Color,ColorFormat);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_ReadBackgroundColor( oC_LCDTFT_Context_t Context , oC_Color_t * outColor , oC_ColorFormat_t ColorFormat )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)             , oC_ErrorCode_ContextNotCorrect      ) &&
            ErrorCondition( isram(outColor)                       , oC_ErrorCode_OutputAddressNotInRAM  ) &&
            ErrorCondition( oC_ColorFormat_IsCorrect(ColorFormat) , oC_ErrorCode_ColorFormatNotCorrect  )
            )
        {
            errorCode = oC_LCDTFT_LLD_ReadBackgroundColor(outColor,ColorFormat);
        }
    }

    return errorCode;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_ReadColorMap( oC_LCDTFT_Context_t Context , oC_ColorMap_t ** outColorMap )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)             , oC_ErrorCode_ContextNotCorrect      ) &&
            ErrorCondition( isram(outColorMap)                    , oC_ErrorCode_OutputAddressNotInRAM  )
            )
        {
            *outColorMap    = Context->ColorMap;
            errorCode       = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_SetResolution( oC_LCDTFT_Context_t Context , oC_Pixel_ResolutionUInt_t Width , oC_Pixel_ResolutionUInt_t Height )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
            ErrorCondition( IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect                      ) &&
            oC_AssignErrorCode( &errorCode , oC_LCDTFT_LLD_DisableOperations()                              ) &&
            oC_AssignErrorCode( &errorCode , oC_LCDTFT_LLD_SetResolution(Width,Height)                      ) &&
            oC_AssignErrorCode( &errorCode , oC_LCDTFT_LLD_SetTimingParameters(&Context->TimingParameters)  ) &&
            oC_AssignErrorCode( &errorCode , oC_LCDTFT_LLD_EnableOperations()                               )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_ReadResolution( oC_LCDTFT_Context_t Context , oC_Pixel_ResolutionUInt_t * outWidth , oC_Pixel_ResolutionUInt_t * outHeight )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
            ErrorCondition( IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect      ) &&
            ErrorCondition( isram(outWidth)           , oC_ErrorCode_OutputAddressNotInRAM  ) &&
            ErrorCondition( isram(outHeight)          , oC_ErrorCode_OutputAddressNotInRAM  )
            )
        {
            errorCode = oC_LCDTFT_LLD_ReadResolution(outWidth,outHeight);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_LCDTFT_SwitchLayer( oC_LCDTFT_Context_t Context , oC_ColorMap_LayerIndex_t Layer )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode , oC_Module_LCDTFT))
    {
        if(
            ErrorCondition( IsContextCorrect(Context)                                , oC_ErrorCode_ContextNotCorrect           )
         && ErrorCondition( Layer < oC_ColorMap_GetNumberOfLayers(Context->ColorMap) , oC_ErrorCode_LayerIndexNotCorrect        )
         && ErrorCondition( oC_ColorMap_SwitchLayer(Context->ColorMap,Layer)         , oC_ErrorCode_CannotSwitchColorMapLayer   )
            )
        {
            errorCode = oC_LCDTFT_LLD_SetColormapBuffer(Context->ColorMap->Layers[Layer]);
        }
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief Checks if the context of the LCDTFT driver is correct. 
 */
//==========================================================================================================================================
static inline bool IsContextCorrect( oC_LCDTFT_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_LcdTftContext,Context->ObjectControl) && isaddresscorrect(Context->ColorMap->Map);
}

//==========================================================================================================================================
/**
 * Allocates all memory needed by context
 */
//==========================================================================================================================================
static oC_ErrorCode_t Context_New( const oC_LCDTFT_Config_t * Config , oC_LCDTFT_Context_t * outContext )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_LCDTFT_Context_t context   = kmalloc(sizeof(struct Context_t) , &Allocator , AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill );

    if(ErrorCondition(context != NULL , oC_ErrorCode_AllocationError))
    {
        context->ConstColorMap      = Config->ColorMap;
        context->ColorMapAllocated  = false;

        if(context->ColorMap == NULL)
        {
            context->ColorMap           = oC_ColorMap_New(&Allocator,AllocationFlags_CanWait1Second | AllocationFlags_ExternalRamFirst | AllocationFlags_ZeroFill ,Config->Width,Config->Height,Config->ColorFormat, 2);
            context->ColorMapAllocated  = true;
        }

        if(ErrorCondition(context->ColorMap != NULL                 , oC_ErrorCode_AllocationError      ) &&
           ErrorCondition(oC_ColorMap_IsCorrect(context->ColorMap)  , oC_ErrorCode_ColorMapNotCorrect   )
           )
        {
            *outContext            = context;
            context->ObjectControl = oC_CountObjectControl(context,oC_ObjectId_LcdTftContext);
            errorCode              = oC_ErrorCode_None;

            memcpy( &context->TimingParameters , &Config->TimingParameters , sizeof(oC_LCDTFT_LLD_TimingParameters_t));
        }
        else
        {
            oC_SaveIfErrorOccur("Context_New: Delete context error: " , Context_Delete(&context));
        }
    }


    return errorCode;
}

//==========================================================================================================================================
/**
 * Release all memory needed by context
 */
//==========================================================================================================================================
static oC_ErrorCode_t Context_Delete( oC_LCDTFT_Context_t * outContext )
{
    oC_ErrorCode_t      errorCode = oC_ErrorCode_ImplementError;
    oC_LCDTFT_Context_t context   = *outContext;

    errorCode = oC_ErrorCode_None;

    if(context->ColorMapAllocated)
    {
        if(!oC_ColorMap_Delete(context->ColorMap,AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }
    if(!kfree(context,AllocationFlags_CanWaitForever))
    {
        errorCode = oC_ErrorCode_ReleaseError;
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
#endif
