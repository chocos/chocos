/** ****************************************************************************************************************************************
 *
 * @file       oc_devfs.c
 *
 * @brief      The file with source for Device File System
 *
 * @author     Patryk Kubiak - (Created on: 3 09 2015 11:37:13)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_devfs.h>
#include <oc_list.h>
#include <oc_object.h>
#include <oc_stdio.h>
#include <oc_driverman.h>
#include <string.h>
#include <oc_struct.h>
#include <oc_service.h>
#include <oc_module.h>
#include <oc_intman.h>
#include <oc_queue.h>
#include <oc_storageman.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define MAX_PATH_LENGTH         30
#define FILE_READ_TIMEOUT       s(30)
#define FILE_WRITE_TIMEOUT      s(30)
#define DEBUGLOG_ENABLED        true

#if DEBUGLOG_ENABLED == true
#   define debuglog( LogType, ... )     kdebuglog( LogType, "devfs :" __VA_ARGS__ )
#else
#   define debuglog( LogType, ... )
#endif

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct File_t
{
    oC_ObjectControl_t  ObjectControl;
    oC_Driver_t         Driver;
    oC_Storage_t        Storage;
    void *              DriverContext;
    oC_MemoryOffset_t   Offset;
    oC_MemorySize_t     Size;
    oC_ErrorCode_t      ErrorCode;
};

typedef enum
{
    SubDirectoryId_Drivers ,
    SubDirectoryId_AutoConfig ,
    SubDirectoryId_Instances ,
    SubDirectoryId_Storages ,
    SubDirectoryId_Root ,
    SubDirectoryId_NumberOfElements ,
} SubDirectoryId_t;

struct Dir_t
{
    oC_ObjectControl_t  ObjectControl;
    uint32_t            CurrentFileIndex;
    SubDirectoryId_t    SubDirectoryId;
};

typedef struct
{

} * ServiceContext_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool                 IsFileCorrect           ( oC_File_t File );
static bool                 IsDirCorrect            ( oC_Dir_t Dir );
static SubDirectoryId_t     GetDirectoryId          ( const char * Path , const char ** outFileName );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t Allocator = {
                .Name = "devfs"
};

//==========================================================================================================================================
//==========================================================================================================================================
const char * SubDirectories[SubDirectoryId_NumberOfElements] = {
                [SubDirectoryId_Root]       = "/" ,
                [SubDirectoryId_Drivers]    = "/drivers/" ,
                [SubDirectoryId_AutoConfig] = "/autoconfig/" ,
                [SubDirectoryId_Instances]  = "/instances/" ,
                [SubDirectoryId_Storages]   = "/storages/" ,
};
//==========================================================================================================================================
//==========================================================================================================================================
const oC_FileSystem_Registration_t DevFs = {
                .Name     = "DevFs" ,
                .init     = oC_DevFs_init     ,
                .deinit   = oC_DevFs_deinit   ,
                .fopen    = oC_DevFs_fopen    ,
                .fclose   = oC_DevFs_fclose   ,
                .fread    = oC_DevFs_fread    ,
                .fwrite   = oC_DevFs_fwrite   ,
                .lseek    = oC_DevFs_lseek    ,
                .ioctl    = oC_DevFs_ioctl    ,
                .sync     = oC_DevFs_sync     ,
                .Getc     = oC_DevFs_getc     ,
                .Putc     = oC_DevFs_putc     ,
                .tell     = oC_DevFs_tell     ,
                .eof      = oC_DevFs_eof      ,
                .size     = oC_DevFs_size     ,
                .fflush   = oC_DevFs_flush    ,

                .opendir  = oC_DevFs_opendir  ,
                .closedir = oC_DevFs_closedir ,
                .readdir  = oC_DevFs_readdir  ,

                .stat     = oC_DevFs_stat     ,
                .unlink   = oC_DevFs_unlink   ,
                .rename   = oC_DevFs_rename   ,
                .chmod    = oC_DevFs_chmod    ,
                .utime    = oC_DevFs_utime    ,
                .mkdir    = oC_DevFs_mkdir    ,
                .DirExists= oC_DevFs_DirExists,

};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_init( oC_DevFs_Context_t * outContext , oC_Storage_t Storage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOffVerification(&errorCode, oC_Module_DevFs) )
    {
        oC_Module_TurnOn(oC_Module_DevFs);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_deinit( oC_DevFs_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_DevFs) )
    {
        oC_Module_TurnOff(oC_Module_DevFs);
        errorCode = oC_ErrorCode_None;
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_fopen( oC_DevFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes )
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Path) , oC_ErrorCode_PathNotCorrect         )
     && ErrorCondition( isram(outFile)         , oC_ErrorCode_OutputAddressNotInRAM  )
        )
    {
        const char *        fileName    = NULL;
        SubDirectoryId_t    directoryId = GetDirectoryId(Path,&fileName);

        debuglog( oC_LogType_Track, "Opening file %s (%d,%s)", Path, directoryId, fileName );

        if(
            ErrorCondition( directoryId < SubDirectoryId_NumberOfElements   , oC_ErrorCode_NoSuchFile )
         && ErrorCondition( fileName != NULL                                , oC_ErrorCode_NoSuchFile )
         && ErrorCondition( strlen(fileName) > 0                            , oC_ErrorCode_NoSuchFile )
            )
        {
            oC_Storage_t storage  = NULL;
            oC_Driver_t  driver   = NULL;
            void *       context  = NULL;

            if(directoryId == SubDirectoryId_Drivers)
            {
                driver = oC_DriverMan_GetDriver(fileName);
            }
            else if(directoryId == SubDirectoryId_AutoConfig)
            {
                oC_Struct_Define(oC_AutoConfiguration_t, autoConfiguration);

                if( ErrorCode( oC_DriverMan_ReadAutoConfiguration( fileName, &autoConfiguration ) ) )
                {
                    driver  = autoConfiguration.Driver;
                    context = autoConfiguration.Context;
                }
                else
                {
                    debuglog( oC_LogType_Error, "Cannot read auto-configuration '%s': %R", fileName, errorCode );
                }
            }
            else if(directoryId == SubDirectoryId_Storages)
            {
                oC_List(oC_Storage_t) storages = oC_StorageMan_GetList();

                foreach( storages, st )
                {
                    if( strcmp( fileName, oC_Storage_GetName(st) ) == 0 )
                    {
                        storage = st;
                        break;
                    }
                }
            }
            else if(directoryId == SubDirectoryId_Instances)
            {
                oC_List(oC_DriverInstance_t*) instances = oC_DriverMan_GetInstanceList();

                foreach( instances, instance )
                {
                    if( strcmp( fileName, instance->Name ) == 0 )
                    {
                        driver  = instance->Driver;
                        context = instance->Context;
                        break;
                    }
                }
            }

            if( ErrorCondition( driver != NULL || storage != NULL, oC_ErrorCode_NoSuchFile ) )
            {
                oC_File_t file = smartalloc(sizeof(struct File_t),AllocationFlags_CanWait1Second);

                if(file)
                {
                    file->ObjectControl = oC_CountObjectControl(file,oC_ObjectId_DevFsFile);
                    file->Driver        = driver;
                    file->Storage       = storage;
                    file->DriverContext = context;
                    file->Offset        = 0;
                    file->Size          = storage != NULL ? oC_Storage_GetSize(storage) : 1;
                    *outFile            = file;
                    errorCode           = oC_ErrorCode_None;

                    debuglog( oC_LogType_GoodNews, "File '%s' has been opened", fileName );
                }
                else
                {
                    errorCode = oC_ErrorCode_AllocationError;
                }
            }
            else
            {
                debuglog( oC_LogType_Error, "cannot find file %s", fileName );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_fclose( oC_DevFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode,IsFileCorrect(File),oC_ErrorCode_ObjectNotCorrect)
        )
    {
        File->ObjectControl = 0;

        debuglog( oC_LogType_Track, "closing file of driver '%s'", File->Driver->FileName );

        if(smartfree(File,sizeof(struct File_t),AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_fread( oC_DevFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsFileCorrect(File),           oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isram(outBuffer),              oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isram(Size),                   oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( *Size > 0,                     oC_ErrorCode_SizeNotCorrect          )
     && ErrorCondition( isarrayinram(outBuffer,*Size), oC_ErrorCode_ArraySizeTooBig         )
        )
    {
        if( File->Storage != NULL )
        {
            if( ErrorCode( oC_Storage_Read(File->Storage,File->Offset,outBuffer,Size,FILE_READ_TIMEOUT) ) )
            {
                File->Offset   += *Size;
                errorCode       = oC_ErrorCode_None;
            }
        }
        else if(ErrorCondition( File->Driver != NULL, oC_ErrorCode_InternalDataAreDamaged ))
        {
            if( ErrorCode( oC_Driver_Read(File->Driver, File->DriverContext, outBuffer, Size, FILE_READ_TIMEOUT) ) )
            {
                File->Offset   += *Size;
                errorCode       = oC_ErrorCode_None;
            }
        }
    }
    if ( IsFileCorrect(File) )
    {
        File->ErrorCode = errorCode;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_fwrite( oC_DevFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsFileCorrect(File),           oC_ErrorCode_ObjectNotCorrect       )
     && ErrorCondition( isaddresscorrect(Buffer),      oC_ErrorCode_WrongAddress           )
     && ErrorCondition( isram(Size),                   oC_ErrorCode_OutputAddressNotInRAM  )
     && ErrorCondition( *Size > 0,                     oC_ErrorCode_SizeNotCorrect         )
     && ErrorCondition( isarraycorrect(Buffer,*Size),  oC_ErrorCode_ArraySizeTooBig        )
        )
    {
        if( File->Storage != NULL )
        {
            if( ErrorCode( oC_Storage_Write(File->Storage,File->Offset,Buffer,Size,FILE_WRITE_TIMEOUT) ) )
            {
                File->Offset   += *Size;
                errorCode       = oC_ErrorCode_None;
            }
        }
        else if(ErrorCondition( File->Driver != NULL, oC_ErrorCode_InternalDataAreDamaged ))
        {
            if( ErrorCode( oC_Driver_Write(File->Driver, File->DriverContext, Buffer, Size, FILE_WRITE_TIMEOUT) ) )
            {
                File->Offset   += *Size;
                errorCode       = oC_ErrorCode_None;
            }
        }
    }

    if ( IsFileCorrect(File) )
    {
        File->ErrorCode = errorCode;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_lseek( oC_DevFs_Context_t Context , oC_File_t File , uint32_t Offset )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsFileCorrect(File) , oC_ErrorCode_FileNotCorrect ) )
    {
        if(ErrorCondition( IsFileCorrect(File) , oC_ErrorCode_OffsetTooBig  ) )
        {
            File->Offset    = Offset;
            errorCode       = oC_ErrorCode_None;
        }
        else
        {
            debuglog( oC_LogType_Error, "Offset is too big - setting it to the max" );
            File->Offset = File->Size;
        }
    }

    if ( IsFileCorrect(File) )
    {
        File->ErrorCode = errorCode;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_ioctl( oC_DevFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode,IsFileCorrect(File),                oC_ErrorCode_ObjectNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode,oC_Ioctl_IsCorrectCommand(Command), oC_ErrorCode_CommandNotCorrect)
        )
    {
        if(Command == oC_IoCtl_SpecialCommand_SetDriverContext)
        {
            File->DriverContext = Pointer;
            errorCode           = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_Driver_HandleIoctl(File->Driver,File->DriverContext,Command,Pointer);
        }
    }

    if ( IsFileCorrect(File) )
    {
        File->ErrorCode = errorCode;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_sync( oC_DevFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotHandledByFileSystem;
    if ( IsFileCorrect(File) )
    {
        File->ErrorCode = errorCode;
    }
    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_getc( oC_DevFs_Context_t Context , char * outCharacter , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode,IsFileCorrect(File),           oC_ErrorCode_ObjectNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode,isram(outCharacter),           oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        oC_MemorySize_t bytesToRead = 1;
        errorCode = oC_DevFs_fread(Context,File,outCharacter,&bytesToRead);
    }

    if ( IsFileCorrect(File) )
    {
        File->ErrorCode = errorCode;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_putc( oC_DevFs_Context_t Context , char Character , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode,IsFileCorrect(File), oC_ErrorCode_ObjectNotCorrect)
        )
    {
        oC_MemorySize_t bytesToWrite = 1;
        errorCode = oC_DevFs_fwrite(Context,File,&Character,&bytesToWrite);
        File->ErrorCode = errorCode;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
int32_t oC_DevFs_tell( oC_DevFs_Context_t Context , oC_File_t File )
{
    int32_t offset = -1;

    if( IsFileCorrect(File) )
    {
        offset = (int32_t)File->Offset;
    }

    return offset;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_DevFs_eof( oC_DevFs_Context_t Context , oC_File_t File )
{
    return !IsFileCorrect(File) || File->Offset >= File->Size;
}

//==========================================================================================================================================
//==========================================================================================================================================
uint32_t oC_DevFs_size( oC_DevFs_Context_t Context , oC_File_t File )
{
    return IsFileCorrect(File) ? File->Size : 0;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_flush( oC_DevFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_NotHandledByFileSystem;
    if ( IsFileCorrect(File) )
    {
        File->ErrorCode = errorCode;
    }
    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_error( oC_DevFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    if ( ErrorCondition(IsFileCorrect(File), oC_ErrorCode_FileNotCorrect) )
    {
        errorCode = File->ErrorCode;
    }
    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_opendir( oC_DevFs_Context_t Context , oC_Dir_t * outDir , const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outDir)          , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isaddresscorrect(Path) , oC_ErrorCode_WrongAddress             )
        )
    {
        SubDirectoryId_t subDirectoryId = GetDirectoryId(Path,NULL);

        if( ErrorCondition( subDirectoryId < SubDirectoryId_NumberOfElements , oC_ErrorCode_NoSuchFile ) )
        {
            oC_Dir_t dir = smartalloc(sizeof(struct Dir_t),AllocationFlags_CanWaitForever);

            if(dir)
            {
                dir->ObjectControl      = oC_CountObjectControl(dir,oC_ObjectId_DevFsDir);
                dir->CurrentFileIndex   = 0;
                dir->SubDirectoryId     = subDirectoryId;
                *outDir                 = dir;
                errorCode               = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_AllocationError;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_closedir( oC_DevFs_Context_t Context , oC_Dir_t Dir )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsDirCorrect(Dir) , oC_ErrorCode_ObjectNotCorrect ))
    {
        Dir->ObjectControl = 0;

        if(smartfree(Dir,sizeof(struct Dir_t),AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_readdir( oC_DevFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsDirCorrect(Dir) , oC_ErrorCode_ObjectNotCorrect           )
     && ErrorCondition( isram(outFileInfo), oC_ErrorCode_OutputAddressNotInRAM       )
        )
    {
        if(Dir->SubDirectoryId == SubDirectoryId_Root)
        {
            if(Dir->CurrentFileIndex == SubDirectoryId_Root)
            {
                Dir->CurrentFileIndex++;
            }

            if( ErrorCondition( Dir->CurrentFileIndex < SubDirectoryId_NumberOfElements, oC_ErrorCode_NoSuchFile ) )
            {
                outFileInfo->FileType   = oC_FileSystem_FileType_Directory;
                outFileInfo->Name       = &SubDirectories[Dir->CurrentFileIndex++][1];
                outFileInfo->Size       = 0;
                outFileInfo->Timestamp  = 0;
                errorCode               = oC_ErrorCode_None;
            }
        }
        else if(Dir->SubDirectoryId == SubDirectoryId_Drivers)
        {
            oC_List(oC_Driver_t) drivers = oC_DriverMan_GetList();

            if(oC_List_IsCorrect(drivers))
            {
                oC_Driver_t driver = NULL;

                oC_List_At(drivers,Dir->CurrentFileIndex++,driver);

                if(driver)
                {
                    outFileInfo->Name       = driver->FileName;
                    outFileInfo->Size       = 1;
                    outFileInfo->Timestamp  = 0;
                    outFileInfo->FileType   = oC_FileSystem_FileType_File;

                    errorCode = oC_ErrorCode_None;
                }
                else
                {
                    errorCode = oC_ErrorCode_NoSuchFile;
                }
            }
            else
            {
                errorCode = oC_ErrorCode_ListNotCorrect;
            }
        }
        else if(Dir->SubDirectoryId == SubDirectoryId_AutoConfig)
        {
            oC_List(oC_AutoConfiguration_t*) autoConfigurations = NULL;

            if( ErrorCode( oC_DriverMan_ReadAutoConfigurationList(&autoConfigurations) ) )
            {
                oC_AutoConfiguration_t * autoConfiguration = NULL;

                oC_List_At(autoConfigurations, Dir->CurrentFileIndex++, autoConfiguration);

                if(
                    ErrorCondition( isaddresscorrect(autoConfiguration)         , oC_ErrorCode_NoSuchFile       )
                 && ErrorCondition( isaddresscorrect(autoConfiguration->Driver) , oC_ErrorCode_WrongAddress     )
                    )
                {
                    outFileInfo->Name       = autoConfiguration->Name;
                    outFileInfo->FileType   = oC_FileSystem_FileType_File;
                    outFileInfo->Size       = 1;
                    outFileInfo->Timestamp  = 0;

                    errorCode = oC_ErrorCode_None;
                }
            }
        }
        else if(Dir->SubDirectoryId == SubDirectoryId_Instances)
        {
            oC_List(oC_DriverInstance_t*) instances = oC_DriverMan_GetInstanceList();

            if( ErrorCondition( instances != NULL, oC_ErrorCode_ListNotCorrect ) )
            {
                oC_DriverInstance_t * instance = NULL;

                oC_List_At(instances,Dir->CurrentFileIndex++, instance);

                if(
                    ErrorCondition( instance != NULL            , oC_ErrorCode_NoSuchFile               )
                 && ErrorCondition( isaddresscorrect(instance)  , oC_ErrorCode_InternalDataAreDamaged   )
                    )
                {
                    outFileInfo->Name       = instance->Name;
                    outFileInfo->FileType   = oC_FileSystem_FileType_File;
                    outFileInfo->Size       = 1;
                    outFileInfo->Timestamp  = 0;

                    errorCode = oC_ErrorCode_None;
                }
            }
        }
        else if(Dir->SubDirectoryId == SubDirectoryId_Storages)
        {
            oC_List(oC_Storage_t) storages = oC_StorageMan_GetList();
            oC_Storage_t          storage  = NULL;

            oC_List_At( storages, Dir->CurrentFileIndex++, storage );

            if(
                ErrorCondition( storage != NULL             , oC_ErrorCode_NoSuchFile               )
             && ErrorCondition( isaddresscorrect(storage)   , oC_ErrorCode_InternalDataAreDamaged   )
                )
            {
                outFileInfo->Name       = oC_Storage_GetName(storage);
                outFileInfo->FileType   = oC_FileSystem_FileType_File;
                outFileInfo->Size       = oC_Storage_GetSize(storage);
                outFileInfo->Timestamp  = oC_Storage_GetTimestamp(storage);

                errorCode = oC_ErrorCode_None;
            }

        }
        else
        {
            errorCode = oC_ErrorCode_ContextNotCorrect;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_stat( oC_DevFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Path) , oC_ErrorCode_WrongAddress          )
     && ErrorCondition( isram(outFileInfo)     , oC_ErrorCode_OutputAddressNotInRAM )
     && ErrorCondition( strlen(Path) >= 2      , oC_ErrorCode_NoSuchFile            )
     && ErrorCondition( Path[0] == '/'         , oC_ErrorCode_NoSuchFile            )
        )
    {
        const char *     fileName       = NULL;
        SubDirectoryId_t subDirectoryId = GetDirectoryId(Path,&fileName);

        if( ErrorCondition( subDirectoryId < SubDirectoryId_NumberOfElements , oC_ErrorCode_NoSuchFile ) )
        {
            errorCode = oC_ErrorCode_NoSuchFile;

            if(subDirectoryId == SubDirectoryId_Drivers)
            {
                oC_List(oC_Driver_t) drivers = oC_DriverMan_GetList();

                oC_List_Foreach(drivers,driver)
                {
                    if(strcmp(fileName,driver->FileName)==0)
                    {
                        outFileInfo->Name      = driver->FileName;
                        outFileInfo->Size      = sizeof(oC_Driver_Registration_t);
                        outFileInfo->Timestamp = 0;

                        errorCode = oC_ErrorCode_None;

                        break;
                    }
                }
            }
            else if(subDirectoryId == SubDirectoryId_AutoConfig)
            {
                oC_List(oC_AutoConfiguration_t*) autoConfigurations = NULL;

                if( ErrorCode( oC_DriverMan_ReadAutoConfigurationList(&autoConfigurations) ) )
                {
                    foreach( autoConfigurations, config )
                    {
                        if(strcmp(config->Name,fileName) == 0)
                        {
                            outFileInfo->Name       = config->Name;
                            outFileInfo->FileType   = oC_FileSystem_FileType_File;
                            outFileInfo->Size       = 1;
                            outFileInfo->Timestamp  = 0;

                            errorCode = oC_ErrorCode_None;
                            break;
                        }
                    }
                }
            }
            else if(subDirectoryId == SubDirectoryId_Instances)
            {
                oC_List(oC_DriverInstance_t*) instances = oC_DriverMan_GetInstanceList();

                if( ErrorCondition( instances != NULL, oC_ErrorCode_ListNotCorrect ) )
                {
                    foreach( instances, instance )
                    {
                        if(strcmp(fileName,instance->Name) == 0)
                        {
                            outFileInfo->Name       = instance->Name;
                            outFileInfo->FileType   = oC_FileSystem_FileType_File;
                            outFileInfo->Size       = 1;
                            outFileInfo->Timestamp  = 0;

                            errorCode = oC_ErrorCode_None;
                            break;
                        }
                    }
                }
            }
            else if(subDirectoryId == SubDirectoryId_Storages)
            {
                oC_List(oC_Storage_t) storages = oC_StorageMan_GetList();

                foreach( storages, storage )
                {
                    if(strcmp(fileName,oC_Storage_GetName(storage)) == 0)
                    {
                        outFileInfo->Name       = oC_Storage_GetName(storage);
                        outFileInfo->FileType   = oC_FileSystem_FileType_File;
                        outFileInfo->Size       = oC_Storage_GetSize(storage);
                        outFileInfo->Timestamp  = oC_Storage_GetTimestamp(storage);

                        errorCode = oC_ErrorCode_None;
                        break;
                    }
                }
            }
            else if(subDirectoryId == SubDirectoryId_Root)
            {
                strncpy(outFileInfo->_nameBuffer,fileName,sizeof(outFileInfo->_nameBuffer));

                outFileInfo->Name       = outFileInfo->_nameBuffer;
                outFileInfo->Size       = 0;
                outFileInfo->Timestamp  = 0;
                outFileInfo->FileType   = oC_FileSystem_FileType_Directory;

                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_unlink( oC_DevFs_Context_t Context , const char * Path)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_rename( oC_DevFs_Context_t Context , const char * OldName , const char * NewName)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_chmod( oC_DevFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_utime( oC_DevFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp )
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_mkdir( oC_DevFs_Context_t Context , const char * Path )
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_DevFs_DirExists( oC_DevFs_Context_t Context , const char * Path)
{
    return GetDirectoryId(Path,NULL) < SubDirectoryId_NumberOfElements;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_AddStorage( oC_Storage_t Storage, char * outPath , oC_MemorySize_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_DevFs))
    {
        if(
            ErrorCondition( oC_Storage_IsCorrect(Storage)   , oC_ErrorCode_ObjectNotCorrect         )
         && ErrorCondition( isram(outPath)                  , oC_ErrorCode_OutputAddressNotInRAM    )
         && ErrorCondition( Size > 0                        , oC_ErrorCode_SizeNotCorrect           )
            )
        {
            oC_List(oC_Storage_t)   storages    = oC_StorageMan_GetList();
            bool                    foundOnList = oC_List_Contains(storages,Storage);

            oC_IntMan_EnterCriticalSection();

            if( ErrorCondition( foundOnList == false, oC_ErrorCode_ObjectAlreadyExist ) )
            {
                const char *       baseNameOfStorage    = oC_Storage_GetName(Storage);
                oC_DefaultString_t nameForStorage       = {0};
                bool               nameIsUsed           = false;
                uint32_t           indexForStorage      = 0;

                oC_STATIC_ASSERT( sizeof(nameForStorage) > 10, "Size of the defaultString type is too small! (min 10)" );

                strncpy( nameForStorage, baseNameOfStorage, sizeof(nameForStorage) - 10 );

                do
                {
                    if(nameIsUsed)
                    {
                        memset(nameForStorage,0,sizeof(nameForStorage));
                        strncpy( nameForStorage, baseNameOfStorage, sizeof(nameForStorage) - 10 );

                        sprintf_s(&nameForStorage[strlen(nameForStorage)], sizeof(nameForStorage) - strlen(nameForStorage) , "%d", indexForStorage++);

                        nameIsUsed = false;

                        debuglog( oC_LogType_Info, "Changing name of the storage '%s' to '%s' (old name is already in use)", baseNameOfStorage, nameForStorage );
                    }
                    foreach( storages, storage )
                    {
                        if( strcmp(oC_Storage_GetName(storage), nameForStorage) == 0 )
                        {
                            nameIsUsed = true;
                            break;
                        }
                    }
                } while(nameIsUsed);

                if( ErrorCode( oC_Storage_SetName(Storage,nameForStorage) ) )
                {
                    sprintf_s( outPath, Size, "%s%s", SubDirectories[SubDirectoryId_Storages], nameForStorage );

                    if( ErrorCode( oC_StorageMan_AddStorage(Storage) ) )
                    {
                        debuglog( oC_LogType_GoodNews, "Storage '%s' has been added to list", nameForStorage );
                        errorCode = oC_ErrorCode_None;
                    }
                }
                else
                {
                    debuglog( oC_LogType_Error, "Cannot set name of the storage '%s' to '%s': %R", baseNameOfStorage, nameForStorage, errorCode);
                }
            }
            else
            {
                debuglog( oC_LogType_Warning, "Cannot add storage '%s' - it already exist on the list", oC_Storage_GetName(Storage) );
            }

            oC_IntMan_ExitCriticalSection();
        }
        else
        {
            debuglog( oC_LogType_Error, "Cannot add storage - %R", errorCode );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_DevFs_RemoveStorage( oC_Storage_t Storage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_DevFs))
    {
        debuglog( oC_LogType_Track, "Trying to remove storage '%s' from the list", oC_Storage_GetName(Storage) );

        if( ErrorCode( oC_StorageMan_RemoveStorage(Storage) ) )
        {
            debuglog( oC_LogType_Track, "Storage '%s' has been removed from list", oC_Storage_GetName(Storage) );
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static bool IsFileCorrect( oC_File_t File )
{
    return isram(File) && oC_CheckObjectControl(File,oC_ObjectId_DevFsFile,File->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool IsDirCorrect( oC_Dir_t Dir )
{
    return isram(Dir) && oC_CheckObjectControl(Dir,oC_ObjectId_DevFsDir,Dir->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
static SubDirectoryId_t GetDirectoryId( const char * Path , const char ** outFileName )
{
    SubDirectoryId_t subDirectoryId = SubDirectoryId_NumberOfElements;

    for(SubDirectoryId_t id = 0 ; id < SubDirectoryId_NumberOfElements; id++)
    {
        int len = strlen(SubDirectories[id]);
        if(strncmp(Path,SubDirectories[id],len) == 0)
        {
            if(outFileName != NULL)
            {
                *outFileName = &Path[len];
            }
            subDirectoryId = id;
            break;
        }
    }

    return subDirectoryId;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
