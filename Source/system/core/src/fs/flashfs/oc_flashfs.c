/** ****************************************************************************************************************************************
 *
 * @file       oc_flashfs.c
 *
 * @brief      The file with source for the FLASH file system
 *
 * @author     Patryk Kubiak - (Created on: 16.07.2016 14:46:23) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_flashfs.h>
#include <oc_object.h>
#include <oc_array.h>
#include <oc_string.h>
#include <oc_fs.h>
#include <oc_bits.h>

/** ========================================================================================================================================
 *
 *              The section with local functions prototyeps
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________


struct File_t
{
    oC_ObjectControl_t                  ObjectControl;
    uint32_t                            Offset;
    const oC_FlashFs_FileDefinition_t * FileDefinition;
};

struct Dir_t
{
    oC_ObjectControl_t ObjectControl;
    uint32_t           FileIndex;
};


#undef  _________________________________________TYPES______________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
const oC_FileSystem_Registration_t FlashFs = {
                .Name     = "FlashFs" ,
                .init     = oC_FlashFs_init     ,
                .deinit   = oC_FlashFs_deinit   ,
                .fopen    = oC_FlashFs_fopen    ,
                .fclose   = oC_FlashFs_fclose   ,
                .fread    = oC_FlashFs_fread    ,
                .fwrite   = oC_FlashFs_fwrite   ,
                .lseek    = oC_FlashFs_lseek    ,
                .ioctl    = oC_FlashFs_ioctl    ,
                .sync     = oC_FlashFs_sync     ,
                .Getc     = oC_FlashFs_getc     ,
                .Putc     = oC_FlashFs_putc     ,
                .tell     = oC_FlashFs_tell     ,
                .eof      = oC_FlashFs_eof      ,
                .size     = oC_FlashFs_size     ,
                .fflush   = oC_FlashFs_flush    ,

                .opendir  = oC_FlashFs_opendir  ,
                .closedir = oC_FlashFs_closedir ,
                .readdir  = oC_FlashFs_readdir  ,

                .stat     = oC_FlashFs_stat     ,
                .unlink   = oC_FlashFs_unlink   ,
                .rename   = oC_FlashFs_rename   ,
                .chmod    = oC_FlashFs_chmod    ,
                .utime    = oC_FlashFs_utime    ,
                .mkdir    = oC_FlashFs_mkdir    ,
                .DirExists= oC_FlashFs_DirExists,
};


#undef  _________________________________________VARIABLES__________________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static bool                                 IsFileCorrect       ( oC_File_t File );
static bool                                 IsDirCorrect        ( oC_Dir_t Dir   );
static const oC_FlashFs_FileDefinition_t *  GetFileDefinition   ( const char * Path );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS________________________________________________________________________


//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_init( oC_FlashFs_Context_t * outContext , oC_Storage_t Storage )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_deinit( oC_FlashFs_Context_t Context )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_fopen( oC_FlashFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outFile)                                                  , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isaddresscorrect(Path)                                          , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( Path[0] == '/'                                                  , oC_ErrorCode_PathNotCorrect           )
     && ErrorCondition( oC_Bits_AreBitsClearU32(Mode , oC_FileSystem_ModeFlags__Create) , oC_ErrorCode_FileIsReadOnly           )
        )
    {
        const oC_FlashFs_FileDefinition_t * fileDefinition = GetFileDefinition(Path);

        if(ErrorCondition(isrom(fileDefinition) , oC_ErrorCode_PathNotCorrect))
        {
            oC_File_t file = smartalloc(sizeof(struct File_t),AllocationFlags_CanWait1Second);

            if(file)
            {
                file->ObjectControl     = oC_CountObjectControl(file,oC_ObjectId_FlashFsFile);
                file->Offset            = Mode & oC_FileSystem_ModeFlags_SeekToTheEnd ? file->FileDefinition->Size : 0;
                file->FileDefinition    = fileDefinition;
                *outFile                = file;
                errorCode               = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_AllocationError;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_fclose( oC_FlashFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition(IsFileCorrect(File),oC_ErrorCode_ObjectNotCorrect)
        )
    {
        File->ObjectControl = 0;

        if(smartfree(File,sizeof(struct File_t),AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_fread( oC_FlashFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsFileCorrect(File) , oC_ErrorCode_FileNotCorrect           )
     && ErrorCondition( isram(outBuffer)    , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(Size)         , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        uint32_t bytesRead = 0;
        uint8_t* buffer    = outBuffer;

        for(;File->Offset < File->FileDefinition->Size && bytesRead < (*Size); File->Offset++, bytesRead++)
        {
            buffer[bytesRead] = File->FileDefinition->Data[File->Offset];
        }

        *Size     = bytesRead;
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_fwrite( oC_FlashFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size )
{
    return oC_ErrorCode_FileIsReadOnly;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_lseek( oC_FlashFs_Context_t Context , oC_File_t File , uint32_t Offset )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsFileCorrect(File) ,                           oC_ErrorCode_FileNotCorrect     )
     && ErrorCondition( Offset < File->FileDefinition->Size ,           oC_ErrorCode_OffsetTooBig       )
        )
    {
        File->Offset = Offset;
        errorCode    = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_ioctl( oC_FlashFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_sync( oC_FlashFs_Context_t Context , oC_File_t File )
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_getc( oC_FlashFs_Context_t Context , char * outCharacter , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsFileCorrect(File)                         , oC_ErrorCode_FileNotCorrect           )
     && ErrorCondition( isram(outCharacter)                         , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( File->Offset < File->FileDefinition->Size   , oC_ErrorCode_EndOfFile                )
        )
    {
        *outCharacter = File->FileDefinition->Data[File->Offset++];
        errorCode     = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_putc( oC_FlashFs_Context_t Context , char Character , oC_File_t File )
{
    return oC_ErrorCode_FileIsReadOnly;
}

//==========================================================================================================================================
//==========================================================================================================================================
int32_t oC_FlashFs_tell( oC_FlashFs_Context_t Context , oC_File_t File )
{
    return -1;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_FlashFs_eof( oC_FlashFs_Context_t Context , oC_File_t File )
{
    bool eof = true;

    if(IsFileCorrect(File))
    {
        eof = File->Offset >= File->FileDefinition->Size;
    }
    else
    {
        oC_SaveError("FlashFs: EOF error - " , oC_ErrorCode_FileNotCorrect);
    }

    return eof;
}

//==========================================================================================================================================
//==========================================================================================================================================
uint32_t oC_FlashFs_size( oC_FlashFs_Context_t Context , oC_File_t File )
{
    uint32_t size = 0;

    if(IsFileCorrect(File))
    {
        size = File->FileDefinition->Size;
    }

    return size;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_flush( oC_FlashFs_Context_t Context , oC_File_t File )
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_opendir( oC_FlashFs_Context_t Context , oC_Dir_t * outDir , const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outDir)            , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isaddresscorrect(Path)   , oC_ErrorCode_WrongAddress            )
     && ErrorCondition( strcmp(Path,"/") == 0    , oC_ErrorCode_NoSuchFile              )
        )
    {
        oC_Dir_t dir = smartalloc(sizeof(struct Dir_t),AllocationFlags_CanWaitForever);

        if(dir)
        {
            dir->ObjectControl = oC_CountObjectControl(dir,oC_ObjectId_FlashFsDir);
            dir->FileIndex     = 0;
            *outDir            = dir;
            errorCode          = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_closedir( oC_FlashFs_Context_t Context , oC_Dir_t Dir )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , IsDirCorrect(Dir) , oC_ErrorCode_ObjectNotCorrect ))
    {
        Dir->ObjectControl = 0;

        if(smartfree(Dir,sizeof(struct Dir_t),AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_readdir( oC_FlashFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsDirCorrect(Dir)                         , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outFileInfo)                        , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Dir->FileIndex < oC_FlashFs_NumberOfFiles , oC_ErrorCode_NoSuchFile               )
        )
    {
        outFileInfo->Name       = oC_FlashFs_FileDefinitions[Dir->FileIndex].Name;
        outFileInfo->Size       = oC_FlashFs_FileDefinitions[Dir->FileIndex].Size;
        outFileInfo->FileType   = oC_FileSystem_FileType_File;
        outFileInfo->Timestamp  = 0;

        Dir->FileIndex++;
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_stat( oC_FlashFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Path)          , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outFileInfo)              , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Path[0] == '/'                  , oC_ErrorCode_PathNotCorrect           )
        )
    {
        const oC_FlashFs_FileDefinition_t * fileDefinition = GetFileDefinition(Path);

        if(ErrorCondition(isrom(fileDefinition) , oC_ErrorCode_NoSuchFile))
        {
            outFileInfo->Name       = fileDefinition->Name;
            outFileInfo->Size       = fileDefinition->Size;
            outFileInfo->FileType   = oC_FileSystem_FileType_File;
            outFileInfo->Timestamp  = 0;
            errorCode               = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_unlink( oC_FlashFs_Context_t Context , const char * Path)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_rename( oC_FlashFs_Context_t Context , const char * OldName , const char * NewName)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_chmod( oC_FlashFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_utime( oC_FlashFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp )
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FlashFs_mkdir( oC_FlashFs_Context_t Context , const char * Path)
{
    return oC_ErrorCode_NotHandledByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_FlashFs_DirExists( oC_FlashFs_Context_t Context , const char * Path)
{
    return strcmp(Path,"/") == 0;
}

#undef  _________________________________________INTERFACE_FUNCTIONS________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

static bool IsFileCorrect( oC_File_t File )
{
    return isram(File) && oC_CheckObjectControl(File,oC_ObjectId_FlashFsFile,File->ObjectControl);
}

static bool IsDirCorrect( oC_Dir_t Dir )
{
    return isram(Dir) && oC_CheckObjectControl(Dir,oC_ObjectId_FlashFsDir,Dir->ObjectControl);
}

static const oC_FlashFs_FileDefinition_t * GetFileDefinition( const char * Path )
{
    const oC_FlashFs_FileDefinition_t * fileDefinition = NULL;

    oC_ARRAY_FOREACH_IN_ARRAY_WITH_SIZE(oC_FlashFs_FileDefinitions,oC_FlashFs_NumberOfFiles,file)
    {
        if(strcmp(&Path[1],file->Name) == 0)
        {
            fileDefinition = file;
            break;
        }
    }

    return fileDefinition;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

