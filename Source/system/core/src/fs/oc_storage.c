/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          oc_storage.c
 *
 * @author     Patryk Kubiak - (Created on: 20.08.2017 18:56:10) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_storage.h>
#include <oc_object.h>
#include <oc_stdlib.h>
#include <oc_ktime.h>
#include <oc_string.h>
#include <oc_intman.h>

#define MAX_NAME_LENGTH         (sizeof(oC_DefaultString_t) - 1)

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef const oC_Storage_DiskInterface_t * DiskInterface_t;

struct Storage_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_DefaultString_t      Name;
    oC_SectorNumber_t       StartSector;
    oC_SectorNumber_t       SectorCount;
    oC_MemorySize_t         Size;
    oC_MemorySize_t         SectorSize;
    void *                  DiskContext;
    DiskInterface_t         DiskInterface;
    oC_Timestamp_t          Timestamp;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_Storage_t oC_Storage_New( const oC_Storage_DiskInterface_t * Interface, void * Context, const char * Name, oC_SectorNumber_t StartSector, oC_MemorySize_t SectorSize , oC_SectorNumber_t NumberOfSectors )
{
    oC_Storage_t storage = NULL;

    if(
        oC_SaveIfFalse("Interface"      ,  isaddresscorrect(Interface)              , oC_ErrorCode_WrongAddress             )
     && oC_SaveIfFalse("Name"           ,  isaddresscorrect(Name)                   , oC_ErrorCode_WrongAddress             )
     && oC_SaveIfFalse("Name"           ,  strlen(Name) > 0                         , oC_ErrorCode_StringIsEmpty            )
     && oC_SaveIfFalse("Name"           ,  strlen(Name) < MAX_NAME_LENGTH           , oC_ErrorCode_StringIsTooLong          )
     && oC_SaveIfFalse("SectorSize"     ,  SectorSize > 0                           , oC_ErrorCode_SizeNotCorrect           )
     && oC_SaveIfFalse("SectorCount"    ,  NumberOfSectors > 0                      , oC_ErrorCode_SizeNotCorrect           )
     && oC_SaveIfFalse("ReadSectors"    ,  isaddresscorrect(Interface->ReadSectors) , oC_ErrorCode_WrongEventHandlerAddress )
     && oC_SaveIfFalse("WriteSectors"   ,  isaddresscorrect(Interface->WriteSectors), oC_ErrorCode_WrongEventHandlerAddress )
     && oC_SaveIfFalse("EraseSectors"   ,  isaddresscorrect(Interface->EraseSectors), oC_ErrorCode_WrongEventHandlerAddress )
        )
    {
        storage = malloc( sizeof(struct Storage_t) );

        if( oC_SaveIfFalse( "storage" , storage != NULL, oC_ErrorCode_AllocationError ) )
        {
            storage->ObjectControl   = oC_CountObjectControl(storage,oC_ObjectId_Storage);
            storage->DiskContext     = Context;
            storage->DiskInterface   = Interface;
            storage->SectorCount     = NumberOfSectors;
            storage->SectorSize      = SectorSize;
            storage->Size            = SectorSize * NumberOfSectors;
            storage->StartSector     = StartSector;
            storage->Timestamp       = oC_KTime_GetTimestamp();

            strcpy(storage->Name, Name);
        }
    }

    return storage;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Storage_Delete( oC_Storage_t * Storage )
{
    bool deleted = false;

    if(
        oC_SaveIfFalse( "Storage" , isram(Storage)                  , oC_ErrorCode_AddressNotInRam  )
     && oC_SaveIfFalse( "Storage" , oC_Storage_IsCorrect(*Storage)  , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        (*Storage)->ObjectControl = 0;

        deleted     = free(*Storage);
        *Storage    = NULL;
    }

    return deleted;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Storage_IsCorrect( oC_Storage_t Storage )
{
    return isram(Storage) && oC_CheckObjectControl(Storage, oC_ObjectId_Storage, Storage->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_Storage_GetSectorSize( oC_Storage_t Storage )
{
    oC_MemorySize_t size = 0;

    if(oC_Storage_IsCorrect(Storage))
    {
        size = Storage->SectorSize;
    }

    return size;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_SectorNumber_t oC_Storage_GetSectorCount( oC_Storage_t Storage )
{
    oC_SectorNumber_t count = 0;

    if(oC_Storage_IsCorrect(Storage))
    {
        count = Storage->SectorCount;
    }

    return count;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_Storage_GetSize( oC_Storage_t Storage )
{
    oC_MemorySize_t size = 0;

    if(oC_Storage_IsCorrect(Storage))
    {
        size = Storage->Size;
    }

    return size;
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_Storage_GetName( oC_Storage_t Storage )
{
    const char * name = "incorrect storage";

    if(oC_Storage_IsCorrect(Storage))
    {
        name = Storage->Name;
    }

    return name;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_SetName( oC_Storage_t Storage, const char * Name )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(
        ErrorCondition( oC_Storage_IsCorrect(Storage)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(Name)                     , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( strlen(Name) > 0                , oC_ErrorCode_StringIsEmpty            )
     && ErrorCondition( strlen(Name) < MAX_NAME_LENGTH  , oC_ErrorCode_StringIsTooLong          )
        )
    {
        strcpy(Storage->Name,Name);
        errorCode = oC_ErrorCode_None;
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Timestamp_t oC_Storage_GetTimestamp( oC_Storage_t Storage )
{
    oC_Timestamp_t timestamp = 0;

    if(oC_Storage_IsCorrect(Storage))
    {
        timestamp = Storage->Timestamp;
    }

    return timestamp;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_ReadMasterBootRecord( oC_Storage_t Storage, oC_Storage_MasterBootRecord_t outMBR, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Storage_IsCorrect( Storage ) , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outMBR)                   , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( Timeout >= 0                    , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        oC_MemorySize_t size = sizeof(oC_Storage_MasterBootRecord_t);
        errorCode = oC_Storage_ReadSectors( Storage, 0, outMBR, &size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_WriteMasterBootRecord( oC_Storage_t Storage, oC_Storage_MasterBootRecord_t MBR, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Storage_IsCorrect( Storage ) , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isaddresscorrect(MBR)           , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( Timeout >= 0                    , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        oC_MemorySize_t size = sizeof(oC_Storage_MasterBootRecord_t);
        errorCode = oC_Storage_WriteSectors( Storage, 0, MBR, &size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_Read( oC_Storage_t Storage, oC_MemoryOffset_t Offset, void * outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Storage_IsCorrect(Storage)       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( Offset < Storage->Size              , oC_ErrorCode_OffsetTooBig             )
     && ErrorCondition( isram(outBuffer)                    , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(Size)                         , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                         , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        errorCode = Storage->DiskInterface->ReadSectors( Storage->DiskContext, Storage->StartSector + (Offset / Storage->SectorSize), outBuffer, Size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_Write( oC_Storage_t Storage, oC_MemoryOffset_t Offset, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Storage_IsCorrect(Storage)       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( Offset < Storage->Size              , oC_ErrorCode_OffsetTooBig             )
     && ErrorCondition( isaddresscorrect(Buffer)            , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(Size)                         , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                         , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        errorCode = Storage->DiskInterface->WriteSectors( Storage->DiskContext, Storage->StartSector + (Offset / Storage->SectorSize), Buffer, Size, Timeout );
    }

    return errorCode;
}
//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_ReadSectors( oC_Storage_t Storage, oC_SectorNumber_t StartSector, void * outBuffer, oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Storage_IsCorrect(Storage)       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( StartSector > 0                     , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( StartSector < Storage->SectorCount  , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( isram(outBuffer)                    , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(Size)                         , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                         , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        errorCode = Storage->DiskInterface->ReadSectors( Storage->DiskContext, Storage->StartSector + StartSector, outBuffer, Size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_WriteSectors( oC_Storage_t Storage, oC_SectorNumber_t StartSector, const void * Buffer, oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Storage_IsCorrect(Storage)       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( StartSector > 0                     , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( StartSector < Storage->SectorCount  , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( isaddresscorrect(Buffer)            , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(Size)                         , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                         , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                        , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        errorCode = Storage->DiskInterface->WriteSectors( Storage->DiskContext, Storage->StartSector + StartSector, Buffer, Size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Storage_EraseSectors( oC_Storage_t Storage, oC_SectorNumber_t StartSector, oC_SectorNumber_t SectorCount, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Storage_IsCorrect(Storage)                       , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( StartSector > 0                                     , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( StartSector < Storage->SectorCount                  , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( SectorCount > 0                                     , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( (StartSector + SectorCount) < Storage->SectorCount  , oC_ErrorCode_WrongSectorNumber        )
     && ErrorCondition( Timeout >= 0                                        , oC_ErrorCode_TimeNotCorrect           )
        )
    {
        errorCode = Storage->DiskInterface->EraseSectors( Storage->DiskContext, Storage->StartSector + StartSector, SectorCount, Timeout );
    }

    return errorCode;
}

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

