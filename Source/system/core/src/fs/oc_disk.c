/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          oc_disk.c
 *
 * @author     Patryk Kubiak - (Created on: 21.08.2017 18:42:51) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_disk.h>
#include <oc_intman.h>
#include <oc_struct.h>
#include <oc_mutex.h>
#include <oc_devfs.h>

#define MAX_NAME_LENGTH     (sizeof(oC_DefaultString_t) - 1)
#define DEBUGLOG_ENABLED    true
#if DEBUGLOG_ENABLED
#   define debuglog( LogType , ... )        kdebuglog(LogType, __VA_ARGS__)
#else
#   define debuglog( LogType , ... )
#endif

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef const oC_Storage_DiskInterface_t * Interface_t;
typedef oC_Disk_MasterBootRecord_t MasterBootRecord_t;
typedef oC_Partition_Location_t Location_t;

struct Disk_t
{
    oC_ObjectControl_t      ObjectControl;
    Interface_t             DiskInterface;
    void *                  DiskContext;
    oC_DefaultString_t      Name;
    oC_MemorySize_t         SectorSize;
    oC_SectorNumber_t       SectorCount;
    oC_MemorySize_t         Size;
    bool                    Bootable;
    oC_List(oC_Partition_t) Partitions;
    MasterBootRecord_t      MasterBootRecord;
    uint8_t *               SectorBuffer;
    oC_Mutex_t              BusyMutex;
    oC_Storage_t            Storage;
    oC_MemorySize_t         FreeSize;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static oC_ErrorCode_t           CreatePartitionsFromMBR     ( oC_Disk_t Disk , oC_Partition_Location_t * Location, const MasterBootRecord_t * MBR , oC_Time_t Timeout );
static oC_ErrorCode_t           DeletePartitionsFromMBR     ( oC_Disk_t Disk , const MasterBootRecord_t * MBR , oC_Time_t Timeout );
static oC_ErrorCode_t           CreatePartitionFromEntry    ( oC_Disk_t Disk , oC_Partition_Location_t * Location, const oC_Partition_Entry_t * Entry , oC_Time_t Timeout );
static oC_ErrorCode_t           DeletePartitionFromEntry    ( oC_Disk_t Disk , const oC_Partition_Entry_t * Entry , oC_Time_t Timeout );
static oC_ErrorCode_t           ReserveSpaceForPartition    ( oC_Disk_t Disk , oC_MemorySize_t Size , oC_Partition_Type_t Type, oC_Partition_Entry_t * outEntry, oC_Partition_Location_t * outLocation, oC_Time_t Timeout );
static oC_ErrorCode_t           ReserveSpaceInMBR           ( oC_Disk_t Disk , oC_MemorySize_t Size , oC_Partition_Type_t Type, oC_MemorySize_t StorageSize , oC_Storage_t * Storage,MasterBootRecord_t * MBR, oC_Partition_Entry_t * outEntry, oC_Partition_Location_t * outLocation, oC_Time_t Timeout );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_Disk_t oC_Disk_New( const oC_Storage_DiskInterface_t * Interface , void * Context, const char * Name, oC_MemorySize_t SectorSize, oC_SectorNumber_t NumberOfSectors , oC_Time_t Timeout )
{
    oC_Disk_t       disk            = NULL;
    bool            success         = false;
    oC_Timestamp_t  endTimestamp    = timeouttotimestamp(Timeout);

    if(
        oC_SaveIfFalse("Interface"      , isaddresscorrect(Interface)               , oC_ErrorCode_WrongAddress             )
     && oC_SaveIfFalse("EraseSectors"   , isaddresscorrect(Interface->EraseSectors) , oC_ErrorCode_WrongEventHandlerAddress )
     && oC_SaveIfFalse("ReadSectors"    , isaddresscorrect(Interface->ReadSectors)  , oC_ErrorCode_WrongEventHandlerAddress )
     && oC_SaveIfFalse("WriteSectors"   , isaddresscorrect(Interface->WriteSectors) , oC_ErrorCode_WrongEventHandlerAddress )
     && oC_SaveIfFalse("Name"           , isaddresscorrect(Name)                    , oC_ErrorCode_WrongAddress             )
     && oC_SaveIfFalse("Name"           , strlen(Name)  > 0                         , oC_ErrorCode_StringIsEmpty            )
     && oC_SaveIfFalse("Name"           , strlen(Name)  < MAX_NAME_LENGTH           , oC_ErrorCode_StringIsTooLong          )
     && oC_SaveIfFalse("SectorSize"     , SectorSize > 0                            , oC_ErrorCode_SizeNotCorrect           )
     && oC_SaveIfFalse("NumberOfSectors", NumberOfSectors > 0                       , oC_ErrorCode_WrongSectorNumber        )
     && oC_SaveIfFalse("Timeout"        , Timeout >= 0                              , oC_ErrorCode_Timeout                  )
        )
    {
        disk = malloc( sizeof(struct Disk_t) , AllocationFlags_ZeroFill );

        if( oC_SaveIfFalse("disk", disk != NULL, oC_ErrorCode_AllocationError) )
        {
            disk->ObjectControl = oC_CountObjectControl(disk,oC_ObjectId_Disk);
            disk->DiskInterface = Interface;
            disk->DiskContext   = Context;
            disk->SectorSize    = SectorSize;
            disk->SectorCount   = NumberOfSectors;
            disk->Size          = SectorSize * NumberOfSectors;
            disk->Partitions    = oC_List_New( getcurallocator() );
            disk->SectorBuffer  = malloc( SectorSize );
            disk->BusyMutex     = oC_Mutex_New(oC_Mutex_Type_Recursive, getcurallocator(), AllocationFlags_ZeroFill);
            disk->Storage       = oC_Storage_New(Interface,Context,Name,0,SectorSize,NumberOfSectors);

            strncpy(disk->Name,Name,sizeof(disk->Name));

            if(
                oC_SaveIfFalse( "disk->Partitions"  , disk->Partitions   != NULL , oC_ErrorCode_AllocationError )
             && oC_SaveIfFalse( "disk->SectorBuffer", disk->SectorBuffer != NULL , oC_ErrorCode_AllocationError )
             && oC_SaveIfFalse( "disk->BusyMutex"   , disk->BusyMutex    != NULL , oC_ErrorCode_AllocationError )
             && oC_SaveIfFalse( "disk->Storage"     , disk->Storage      != NULL , oC_ErrorCode_AllocationError )
                )
            {
                oC_Struct_Define(MasterBootRecord_t, mbr);
                oC_Struct_Define(oC_Partition_Location_t, location);

                if(
                    oC_SaveIfErrorOccur( "Reading MBR"              , oC_Storage_ReadMasterBootRecord( disk->Storage, disk->MasterBootRecord.Data   , gettimeout(endTimestamp)  ) )
                 && oC_SaveIfErrorOccur( "CreatePartitionsFromMBR"  , CreatePartitionsFromMBR( disk, &location, &mbr                                , gettimeout(endTimestamp)  ) )
                    )
                {
                    success = true;
                }
            }
            if(!success && disk->BusyMutex != NULL)
            {
                oC_Mutex_Delete(&disk->BusyMutex,AllocationFlags_ZeroFill);
            }
            if(!success && disk->SectorBuffer != NULL)
            {
                free(disk->SectorBuffer);
            }
            if(!success && disk->Partitions != NULL)
            {
                oC_List_Delete(disk->Partitions);
            }
            if(!success)
            {
                disk->ObjectControl = 0;
                free(disk);
            }
        }
    }

    if(!success)
    {
        disk = NULL;
    }

    return disk;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Disk_Delete( oC_Disk_t * Disk )
{
    bool deleted = false;

    oC_IntMan_EnterCriticalSection();

    if(
        oC_SaveIfFalse( "Disk" , isram(Disk)                , oC_ErrorCode_AddressNotInRam      )
     && oC_SaveIfFalse( "Disk" , oC_Disk_IsCorrect(*Disk)   , oC_ErrorCode_ObjectNotCorrect     )
        )
    {
        oC_Disk_t disk = *Disk;
        (*Disk)->ObjectControl = 0;

        oC_Mutex_Take(disk->BusyMutex,0);

        deleted = true;

        if( ! oC_Storage_Delete(&disk->Storage) )
        {
            deleted = false;
            debuglog( oC_LogType_Error, "Cannot delete master storage" );
        }

        foreach( disk->Partitions, partition )
        {
            debuglog( oC_LogType_Track, "deleting partition '%s'", oC_Partition_GetName(partition) );

            oC_Storage_t storage = oC_Partition_GetStorage(partition);

            if( ! oC_SaveIfErrorOccur( "DevFs",  oC_DevFs_RemoveStorage(storage) ) )
            {
                deleted = false;
                debuglog( oC_LogType_Error, "Cannot remove storage '%s' from DevFs", oC_Partition_GetName(partition) );
            }

            if( ! oC_SaveIfErrorOccur( "Storage" , oC_Storage_Delete( &storage ) ) )
            {
                deleted = false;
                debuglog( oC_LogType_Error, "Cannot delete storage '%s'", oC_Partition_GetName(partition) );
            }

            if( ! oC_Partition_Delete(&partition) )
            {
                deleted = false;
                debuglog( oC_LogType_Error, "Cannot delete partition" );
            }
        }

        if( ! oC_Mutex_Delete(&disk->BusyMutex, 0) )
        {
            debuglog( oC_LogType_Error, "Cannot delete busy mutex" );
            deleted = false;
        }

        if( ! free(disk->SectorBuffer) )
        {
            debuglog( oC_LogType_Error, "Cannot release sector buffer" );
            deleted = false;
        }

        if( ! oC_List_Delete(disk->Partitions) )
        {
            debuglog( oC_LogType_Error, "Cannot delete partitions list" );
            deleted = false;
        }
        if( ! free(disk) )
        {
            debuglog( oC_LogType_Error, "Cannot release disk memory" );
            deleted = false;
        }

        *Disk = NULL;
    }

    oC_IntMan_ExitCriticalSection();

    return deleted;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Disk_IsCorrect( oC_Disk_t Disk )
{
    return isram(Disk) && oC_CheckObjectControl(Disk,oC_ObjectId_Disk, Disk->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_Disk_GetSize( oC_Disk_t Disk )
{
    return oC_Disk_IsCorrect(Disk) ? Disk->Size : 0;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_Disk_GetSectorSize( oC_Disk_t Disk )
{
    return oC_Disk_IsCorrect(Disk) ? Disk->SectorSize : 0;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_SectorNumber_t oC_Disk_GetSectorCount( oC_Disk_t Disk )
{
    return oC_Disk_IsCorrect(Disk) ? Disk->SectorCount : 0;
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_Disk_GetName( oC_Disk_t Disk )
{
    return oC_Disk_IsCorrect(Disk) ? Disk->Name : "incorrect disk";
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Disk_IsBootable( oC_Disk_t Disk )
{
    return oC_Disk_IsCorrect(Disk) ?
#ifdef LITTLE_ENDIAN
              Disk->MasterBootRecord.BootSignature == 0xAA55
#else
              Disk->MasterBootRecord.BootSignature == 0x55AA
#endif
              : false;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_ReadBootstrapCode( oC_Disk_t Disk, oC_Disk_BootstrapCode_t * outCode )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Disk_IsCorrect(Disk) , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outCode)          , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(*outCode)         , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        memcpy( &(*outCode)[0], Disk->MasterBootRecord.BootstrapCode, sizeof(oC_Disk_BootstrapCode_t) );

        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_ReadPartitions( oC_Disk_t Disk, oC_List(oC_Partition_t) outPartitions )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Disk_IsCorrect(Disk)         , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( oC_List_IsCorrect(outPartitions), oC_ErrorCode_ListNotCorrect   )
        )
    {
        errorCode = oC_ErrorCode_None;

        foreach(Disk->Partitions,partition)
        {
            bool pushed = oC_List_PushBack(outPartitions,partition,getcurallocator());

            if(!pushed)
            {
                errorCode = oC_ErrorCode_CannotAddObjectToList;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_AddPartition( oC_Disk_t Disk, oC_MemorySize_t Size, oC_Partition_Type_t Type, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        ErrorCondition( oC_Disk_IsCorrect(Disk)         , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( Size > 0                        , oC_ErrorCode_SizeNotCorrect       )
     && ErrorCondition( Timeout >= 0                    , oC_ErrorCode_AddressNotInRam      )
     && ErrorCondition( Type != oC_Partition_Type_Empty , oC_ErrorCode_TypeNotCorrect       )
        )
    {
        oC_IntMan_EnterCriticalSection();
        oC_Struct_Define( oC_Partition_Entry_t   , entry    );
        oC_Struct_Define( oC_Partition_Location_t, location );

        if(
            ErrorCode( ReserveSpaceForPartition( Disk, Size, Type, &entry, &location    , gettimeout(endTimestamp)  ) )
         && ErrorCode( CreatePartitionFromEntry( Disk, &location, &entry                , gettimeout(endTimestamp)  ) )
            )
        {
            errorCode = oC_ErrorCode_None;
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_RemovePartition( oC_Disk_t Disk, oC_Partition_t Partition )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Disk_IsCorrect(Disk), oC_ErrorCode_ObjectNotCorrect ) )
    {
        oC_IntMan_EnterCriticalSection();
        bool            deleted = oC_List_RemoveAll(Disk->Partitions,Partition);
        oC_Storage_t    storage = oC_Partition_GetStorage(Partition);

        if(
            ErrorCondition( storage != NULL , oC_ErrorCode_StorageNotFound  )
         && ErrorCondition( deleted         , oC_ErrorCode_NoSuchPartition  )
            )
        {
            oC_Storage_t storage = oC_Partition_GetStorage(Partition);

            errorCode = oC_ErrorCode_None;

            if( ! ErrorCode( oC_DevFs_RemoveStorage(storage) ) )
            {
                debuglog( oC_LogType_Error, "Cannot remove storage '%s' from DevFs", oC_Partition_GetName(Partition) );
            }

            if( ! ErrorCondition( oC_Storage_Delete( &storage ) , oC_ErrorCode_CannotDeleteObject ) )
            {
                debuglog( oC_LogType_Error, "Cannot delete storage '%s'", oC_Partition_GetName(Partition) );
            }

            if( ! ErrorCondition( oC_Partition_Delete(&Partition) , oC_ErrorCode_CannotDeleteObject ) )
            {
                debuglog( oC_LogType_Error, "Cannot delete partition" );
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_ReadSectors( oC_Disk_t Disk, oC_SectorNumber_t StartSector, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Disk_IsCorrect(Disk)         , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outBuffer)                , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(Size)                     , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                     , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                    , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( StartSector < Disk->SectorCount , oC_ErrorCode_WrongSectorNumber        )
        )
    {
        errorCode = Disk->DiskInterface->ReadSectors(Disk->DiskContext,StartSector,outBuffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_WriteSectors( oC_Disk_t Disk, oC_SectorNumber_t StartSector, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Disk_IsCorrect(Disk)         , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isaddresscorrect(Buffer)        , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(Size)                     , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                     , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                    , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( StartSector < Disk->SectorCount , oC_ErrorCode_WrongSectorNumber        )
        )
    {
        errorCode = Disk->DiskInterface->WriteSectors(Disk->DiskContext,StartSector,Buffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_Read( oC_Disk_t Disk, oC_MemoryOffset_t Offset, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        ErrorCondition( oC_Disk_IsCorrect(Disk)                                 , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outBuffer)                                        , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(Size)                                             , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                                             , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                                            , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( Offset < Disk->Size                                     , oC_ErrorCode_OffsetTooBig             )
     && ErrorCondition( oC_Mutex_Take(Disk->BusyMutex,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout                  )
        )
    {
        oC_SectorNumber_t startSector           = Offset / Disk->SectorSize;
        oC_MemoryOffset_t endOffset             = (Offset + (*Size));
        oC_SectorNumber_t endSector             = (endOffset / Disk->SectorSize) + (endOffset % Disk->SectorSize != 0 ? 1 : 0);
        uint8_t *         outputBuffer          = outBuffer;
        oC_MemoryOffset_t offsetInOutputBuffer  = 0;
        oC_MemorySize_t   bytesToCopyFromSector = 0;
        oC_MemorySize_t   leftOutputBufferSize  = *Size;
        oC_MemorySize_t   leftBufferInSectorSize= Disk->SectorSize;
        oC_MemoryOffset_t offsetInSector        = Offset - (startSector * Disk->SectorSize);

        errorCode = oC_ErrorCode_None;

        for( oC_SectorNumber_t sectorIndex = startSector; sectorIndex < endSector; sectorIndex++ )
        {
            oC_MemorySize_t sectorSize = Disk->SectorSize;

            if( ErrorCode( Disk->DiskInterface->ReadSectors(Disk->DiskContext,sectorIndex,Disk->SectorBuffer,&sectorSize,gettimeout(endTimestamp)) ) )
            {
                leftBufferInSectorSize  = Disk->SectorSize - offsetInSector;
                bytesToCopyFromSector   = oC_MIN( leftOutputBufferSize, leftBufferInSectorSize );
                offsetInOutputBuffer    = (sectorIndex * Disk->SectorSize) + offsetInSector;
                memcpy(&outputBuffer[offsetInOutputBuffer], Disk->SectorBuffer, bytesToCopyFromSector);

                offsetInSector          = 0;
                offsetInOutputBuffer   += bytesToCopyFromSector;
            }
            else
            {
                debuglog( oC_LogType_Error, "Cannot read sector %d: %R (read bytes: %d)", sectorIndex, errorCode, offsetInOutputBuffer);
                *Size = offsetInOutputBuffer;
                break;
            }
        }
        oC_Mutex_Give(Disk->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Disk_Write( oC_Disk_t Disk, oC_MemoryOffset_t Offset, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    if(
        ErrorCondition( oC_Disk_IsCorrect(Disk)                                 , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isaddresscorrect(Buffer)                                , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isram(Size)                                             , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                                             , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( Timeout >= 0                                            , oC_ErrorCode_TimeNotCorrect           )
     && ErrorCondition( Offset < Disk->Size                                     , oC_ErrorCode_OffsetTooBig             )
     && ErrorCondition( oC_Mutex_Take(Disk->BusyMutex,gettimeout(endTimestamp)) , oC_ErrorCode_Timeout                  )
        )
    {
        oC_SectorNumber_t startSector           = Offset / Disk->SectorSize;
        oC_MemoryOffset_t endOffset             = (Offset + (*Size));
        oC_SectorNumber_t endSector             = (endOffset / Disk->SectorSize) + (endOffset % Disk->SectorSize != 0 ? 1 : 0);
        const uint8_t *   inputBuffer           = Buffer;
        oC_MemoryOffset_t offsetInInputBuffer   = 0;
        oC_MemorySize_t   bytesToCopyFromSector = 0;
        oC_MemorySize_t   leftInputBufferSize   = *Size;
        oC_MemorySize_t   leftBufferInSectorSize= Disk->SectorSize;
        oC_MemoryOffset_t offsetInSector        = Offset - (startSector * Disk->SectorSize);

        errorCode = oC_ErrorCode_None;

        for( oC_SectorNumber_t sectorIndex = startSector; sectorIndex < endSector; sectorIndex++ )
        {
            oC_MemorySize_t sectorSize = Disk->SectorSize;

            leftBufferInSectorSize  = Disk->SectorSize - offsetInSector;
            bytesToCopyFromSector   = oC_MIN( leftInputBufferSize, leftBufferInSectorSize );
            offsetInInputBuffer     = (sectorIndex * Disk->SectorSize) + offsetInSector;

            if( bytesToCopyFromSector >= Disk->SectorSize
             || ErrorCode( Disk->DiskInterface->ReadSectors(Disk->DiskContext,sectorIndex,Disk->SectorBuffer,&sectorSize,gettimeout(endTimestamp)) )
                 )
            {
                memcpy(Disk->SectorBuffer, &inputBuffer[offsetInInputBuffer], bytesToCopyFromSector);

                if( ErrorCode( Disk->DiskInterface->WriteSectors(Disk->DiskContext,sectorIndex,Disk->SectorBuffer,&sectorSize,gettimeout(endTimestamp)) ) )
                {
                    offsetInSector          = 0;
                    offsetInInputBuffer    += bytesToCopyFromSector;
                }
                else
                {
                    debuglog( oC_LogType_Error, "Cannot write sector %d: %R (write bytes: %d)", sectorIndex, errorCode, offsetInInputBuffer);
                    *Size = offsetInInputBuffer;
                    break;
                }
            }
            else
            {
                debuglog( oC_LogType_Error, "Cannot read sector %d: %R (read bytes: %d)", sectorIndex, errorCode, offsetInInputBuffer);
                *Size = offsetInInputBuffer;
                break;
            }

        }
        oC_Mutex_Give(Disk->BusyMutex);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_Partition_t) oC_Disk_GetPartitionsList( oC_Disk_t Disk )
{
    return oC_Disk_IsCorrect(Disk) ? Disk->Partitions : NULL;
}
#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t CreatePartitionsFromMBR( oC_Disk_t Disk , oC_Partition_Location_t * Location, const MasterBootRecord_t * MBR , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode       = oC_ErrorCode_ImplementError;
    oC_Timestamp_t          endTimestamp    = timeouttotimestamp(Timeout);
    oC_Partition_Level_t    level           = Location->Level;
    oC_Partition_Index_t    partitionIndex  = 0;

    Location->Filled = true;

    if( ErrorCondition( level < oC_PARTITION_MAX_LEVEL , oC_ErrorCode_LevelNotCorrect))
    {
        errorCode = oC_ErrorCode_None;

        oC_ARRAY_FOREACH_IN_ARRAY(MBR->PartitionEntries,partitionEntry)
        {
            Location->Level             = level;
            Location->Indexes[level]    = partitionIndex++;

            if(partitionEntry->Type != oC_Partition_Type_Empty)
            {
                if( ! ErrorCode( CreatePartitionFromEntry(Disk,Location,partitionEntry,gettimeout(endTimestamp)) ) )
                {
                    break;
                }
            }
        }

        if(oC_ErrorOccur(errorCode))
        {
            oC_SaveIfErrorOccur( "Delete Partition", DeletePartitionsFromMBR(Disk,MBR,gettimeout(endTimestamp)) );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t DeletePartitionsFromMBR( oC_Disk_t Disk, const MasterBootRecord_t * MBR, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t endTimestamp = timeouttotimestamp(Timeout);

    errorCode = oC_ErrorCode_None;

    oC_ARRAY_FOREACH_IN_ARRAY(MBR->PartitionEntries,partitionEntry)
    {
        if(partitionEntry->Type != oC_Partition_Type_Empty)
        {
            if( ! ErrorCode( DeletePartitionFromEntry(Disk,partitionEntry,gettimeout(endTimestamp)) ) )
            {
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t CreatePartitionFromEntry( oC_Disk_t Disk , oC_Partition_Location_t * Location, const oC_Partition_Entry_t * Entry , oC_Time_t Timeout )
{
    oC_ErrorCode_t          errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t          endTimestamp = timeouttotimestamp(Timeout);
    oC_Partition_Level_t    level        = Location->Level;

    if(
        ErrorCondition( Entry->LbaStartSector > 0 && Entry->LbaStartSector < Disk->SectorCount  , oC_ErrorCode_WrongSectorNumber    )
     && ErrorCondition( Entry->LbaSectorCount > 0                                               , oC_ErrorCode_WrongSectorCount     )
        )
    {
        oC_Storage_t storage = oC_Storage_New(Disk->DiskInterface,Disk->DiskContext,Disk->Name,Entry->LbaStartSector,Disk->SectorSize,Entry->LbaSectorCount);

        if( ErrorCondition( storage != NULL , oC_ErrorCode_AllocationError ) )
        {
            if( ErrorCode( oC_DevFs_AddStorage(storage,NULL,0) ) )
            {
                oC_Partition_t partition = oC_Partition_New(Entry,Location,storage);

                if( ErrorCondition( partition != NULL , oC_ErrorCode_AllocationError ) )
                {
                    oC_Struct_Define( MasterBootRecord_t, mbr );

                    if( Entry->Type == oC_Partition_Type_ExtendedLBA )
                    {
                        Location->Level = level + 1;
                    }
                    if(
                        Entry->Type != oC_Partition_Type_ExtendedLBA
                     || (
                          ErrorCode( oC_Storage_ReadMasterBootRecord( storage, &mbr.Data[0] , gettimeout(endTimestamp)  ) )
                       && ErrorCode( CreatePartitionsFromMBR(Disk,Location,&mbr             , gettimeout(endTimestamp)  ) )
                          )
                        )
                    {
                        bool pushed = oC_List_PushBack(Disk->Partitions,partition,getcurallocator());

                        if( ErrorCondition( pushed, oC_ErrorCode_CannotAddObjectToList ) )
                        {
                            errorCode = oC_ErrorCode_None;
                        }
                        if(oC_ErrorOccur(errorCode))
                        {
                            DeletePartitionsFromMBR(Disk,&mbr,gettimeout(endTimestamp));
                        }
                    }
                    if(oC_ErrorOccur(errorCode))
                    {
                        oC_Partition_Delete(&partition);
                    }
                }
                if(oC_ErrorOccur(errorCode))
                {
                    oC_DevFs_RemoveStorage(storage);
                }
            }
            if(oC_ErrorOccur(errorCode))
            {
                oC_Storage_Delete(&storage);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t DeletePartitionFromEntry( oC_Disk_t Disk , const oC_Partition_Entry_t * Entry , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    errorCode = oC_ErrorCode_NoSuchPartition;

    oC_IntMan_EnterCriticalSection();

    foreach( Disk->Partitions, partition )
    {
        oC_Partition_Entry_t entry;

        if( ErrorCode( oC_Partition_ReadEntry(partition,&entry) ) )
        {
            if( entry.LbaStartSector == Entry->LbaStartSector )
            {
                oC_Storage_t storage = oC_Partition_GetStorage(partition);


                errorCode = oC_ErrorCode_None;

                ErrorCondition( storage != NULL, oC_ErrorCode_CannotRemoveObjectFromList );
                ErrorCode( oC_DevFs_RemoveStorage(storage) );
                ErrorCondition( oC_Storage_Delete(&storage)     , oC_ErrorCode_CannotDeleteObject );
                ErrorCondition( oC_Partition_Delete(&partition) , oC_ErrorCode_CannotDeleteObject );

                oC_List_RemoveCurrentElement(Disk->Partitions,partition);
            }
        }
        else
        {
            break;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t ReserveSpaceForPartition( oC_Disk_t Disk , oC_MemorySize_t Size , oC_Partition_Type_t Type, oC_Partition_Entry_t * outEntry, oC_Partition_Location_t * outLocation, oC_Time_t Timeout )
{
    oC_ErrorCode_t      errorCode    = oC_ErrorCode_ImplementError;
    oC_Timestamp_t      endTimestamp = timeouttotimestamp(Timeout);
    MasterBootRecord_t  mbr;
    oC_Storage_t        storage = Disk->Storage;

    memcpy(&mbr, &Disk->MasterBootRecord, sizeof(mbr));

    if(
        ErrorCode( ReserveSpaceInMBR( Disk, Size, Type, Disk->Size, &storage, &mbr, outEntry , outLocation, gettimeout(endTimestamp)    ) )
     && ErrorCode( oC_Storage_WriteMasterBootRecord( storage, &mbr.Data[0], gettimeout(endTimestamp)                                    ) )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t ReserveSpaceInMBR(
                oC_Disk_t                   Disk ,
                oC_MemorySize_t             Size ,
                oC_Partition_Type_t         Type ,
                oC_MemorySize_t             StorageSize ,
                oC_Storage_t *              Storage,
                MasterBootRecord_t *        MBR,
                oC_Partition_Entry_t *      outEntry,
                oC_Partition_Location_t *   Location,
                oC_Time_t                   Timeout )
{
    oC_ErrorCode_t          errorCode           = oC_ErrorCode_ImplementError;
    oC_MemorySize_t         freeSize            = Disk->Size;
    oC_Partition_Entry_t *  freeEntry           = NULL;
    oC_Partition_Entry_t *  sortedEntries[4]    = {0};
    oC_Partition_Level_t    level               = Location->Level;
    oC_Partition_Index_t    index               = 0;
    oC_Timestamp_t          endTimestamp        = timeouttotimestamp(Timeout);

    oC_ARRAY_FOREACH_IN_ARRAY(MBR->PartitionEntries,partitionEntry)
    {
        if(partitionEntry->Type != oC_Partition_Type_Empty)
        {
            oC_MemorySize_t partitionSize = partitionEntry->LbaSectorCount * Disk->SectorSize;
            if(freeSize >= partitionSize)
            {
                freeSize -= partitionEntry->LbaSectorCount * Disk->SectorSize;

                for(int i = 0; i < 4; i++)
                {
                    if(sortedEntries[i] == NULL)
                    {
                        sortedEntries[i] = partitionEntry;
                        break;
                    }
                    else if(partitionEntry->LbaStartSector < sortedEntries[i]->LbaStartSector)
                    {
                        for(int j = 3; j > i; j--)
                        {
                            sortedEntries[j] = sortedEntries[j-1];
                        }
                        sortedEntries[i] = partitionEntry;
                        break;
                    }
                }
            }
            else
            {
                debuglog( oC_LogType_Error, "Wrong partition size: %d B > %d B", partitionSize, freeSize );
                freeSize = 0;
                break;
            }
        }
        else
        {
            freeEntry                   = partitionEntry;
            Location->Indexes[level]    = index;
        }
        index++;
    }

    if( freeEntry == NULL && freeSize >= Size )
    {
        errorCode = oC_ErrorCode_NoFreeSlots;

        foreach( Disk->Partitions, partition )
        {
            if( oC_Partition_IsExtended(partition) && oC_Partition_GetSize(partition) >= freeSize )
            {
                if( ErrorCode( oC_Partition_ReadLocation( partition, Location ) ) )
                {
                    oC_Storage_t storage = oC_Partition_GetStorage(partition);

                    Location->Level++;

                    *Storage  = storage;

                    if(
                        ErrorCode( oC_Storage_ReadMasterBootRecord  ( storage, &MBR->Data[0], gettimeout(endTimestamp)                          ) )
                     && ErrorCode( ReserveSpaceInMBR( Disk, Size, Type, StorageSize, Storage, MBR, outEntry, Location, gettimeout(endTimestamp) ) )
                        )
                    {
                        errorCode = oC_ErrorCode_None;
                    }
                }
            }
        }
    }
    else if(
        ErrorCondition( freeSize >= Size  , oC_ErrorCode_NoSpaceAvailable   )
     && ErrorCondition( freeEntry != NULL , oC_ErrorCode_NoFreeSlots        )
         )
    {
        uint32_t sectorStart            = sizeof(MasterBootRecord_t);
        uint32_t requiredSectors        = (Size / Disk->SectorSize) + ( (Size % Disk->SectorSize != 0) ? 1 : 0 );
        uint32_t foundFreeSectorCount   = Size > sizeof(MasterBootRecord_t) ? (Size - sizeof(MasterBootRecord_t)) / Disk->SectorSize : 0;

        for(int i = 0; i < 4; i++)
        {
            if(sortedEntries[i] != NULL)
            {
                if(sortedEntries[i]->LbaStartSector >= sectorStart)
                {
                    foundFreeSectorCount = sortedEntries[i]->LbaStartSector - sectorStart;
                }
                else
                {
                    foundFreeSectorCount = 0;
                }

                if(foundFreeSectorCount >= requiredSectors)
                {
                    break;
                }
                else
                {
                    sectorStart = sortedEntries[i]->LbaStartSector + sortedEntries[i]->LbaSectorCount;
                }
            }
        }

        if( ErrorCondition( requiredSectors >= foundFreeSectorCount , oC_ErrorCode_FreeSliceNotFound ) )
        {
            freeEntry->Type             = Type;
            freeEntry->IsBootable       = false;
            freeEntry->LbaStartSector   = sectorStart;
            freeEntry->LbaSectorCount   = requiredSectors;

            Location->Filled = true;

            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________

