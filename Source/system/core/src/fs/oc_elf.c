/** ****************************************************************************************************************************************
 *
 * @brief       The function loads the ELF file from the given path
 * 
 * @file          oc_elf.c
 *
 * @author     Patryk Kubiak - (Created on: 23.06.2017 18:32:52) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_elf.h>
#include <oc_stdlib.h>
#include <oc_stdio.h>
#include <oc_vfs.h>
#include <oc_object.h>
#include <oc_streamman.h>
#include <oc_list.h>
#include <oc_struct.h>
#include <oc_cbin.h>

/** ========================================================================================================================================
 *
 *              The section with local definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#define EI_MAG0     0x7F
#define EI_MAG1     'E'
#define EI_MAG2     'L'
#define EI_MAG3     'F'
#define EI_SIGNATURE_LENGTH     4

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief context of the program execution
 */
//==========================================================================================================================================
struct File_t
{
    /** @brief The field for verification that the object is correct. For more information look at the #oc_object.h description. */
    oC_ObjectControl_t                  ObjectControl;
    char*                               Name;
    void *                              FileData;
    oC_MemorySize_t                     FileSize;
    oC_Elf_FileHeader_t *               FileHeader;
    oC_Elf_ProgramHeader_t *            ProgramHeader;
    oC_Elf_SectionHeader_t *            SectionHeaderNameString;
    oC_Elf_SectionHeader_t *            dynstr;
    oC_Elf_SectionHeader_t *            dynsym;
    oC_Elf_SectionHeader_t *            strtab;
    oC_Elf_SectionHeader_t *            symtab;
    oC_Elf_SectionHeader_t *            text;
    oC_Elf_SectionHeader_t *            rodata;
    oC_Elf_SectionHeader_t *            got;
    oC_Elf_SectionHeader_t *            bss;
    oC_Elf_SectionHeader_t *            data;
    oC_Elf_SectionHeader_t *            syscalls;
    oC_Program_t                        Program;
    oC_Process_t                        Process;
    oC_Thread_t                         MainThread;
    void*                               ProgramContext;
    void*                               LoadableSegment;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool                     IsElfSignatureCorrect( uint8_t * Data );
static oC_Elf_File_t            File_New( const char* ProgramName, oC_MemorySize_t FileSize );
static bool                     File_Delete( oC_Elf_File_t ElfFile );
static bool                     File_IsCorrect( const oC_Elf_File_t File );
static oC_ErrorCode_t           UpdateGlobalOffsetTable( oC_Elf_File_t File );
static oC_ErrorCode_t           ClearBss( oC_Elf_File_t File );
static oC_ErrorCode_t           FindLoadableSegment( oC_Elf_File_t File );
static oC_ErrorCode_t           LoadSegment( oC_Elf_File_t File );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief loads ELF file from the given path
 *
 * The function is responsible for loading of an ELF file from the given path.
 *
 * @param Path              Path to a file with ELF data
 * @param outFile           destination for ELF file context
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 *
 * Possible codes:
 *     Error Code                           | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress              | The `Path` does not point to the valid address
 *  #oC_ErrorCode_OutputAddressNotInRAM     | The `outFile` destination variable does not point to the RAM section
 *  #oC_ErrorCode_FileIsEmpty               | The given file is too small
 *  #oC_ErrorCode_AllocationError           | Cannot allocate memory for the file context
 *  #oC_ErrorCode_NotElfFile                | The given file is not ELF
 *
 * @note
 * Please note, that more error codes can be returned by #oC_VirtualFileSystem_fread, #oC_VirtualFileSystem_fopen functions.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Elf_LoadFile( const char * Path , oC_Elf_File_t * outFile )
{
    oC_ErrorCode_t                  errorCode   = oC_ErrorCode_ImplementError;
    oC_File_t                       file        = NULL;
    oC_FileSystem_ModeFlags_t       mode        = oC_FileSystem_ModeFlags_OpenExisting | oC_FileSystem_ModeFlags_Read;
    oC_FileSystem_FileAttributes_t  attributes  = 0;

    if(
        ErrorCondition( isaddresscorrect(Path)  , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outFile)          , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCode     ( oC_VirtualFileSystem_fopen(&file, Path, mode, attributes )      )
        )
    {
        oC_MemorySize_t fileSize = oC_VirtualFileSystem_size(file);

        if( ErrorCondition( fileSize > EI_SIGNATURE_LENGTH, oC_ErrorCode_FileIsEmpty ) )
        {
            oC_Elf_File_t elfFile = File_New(Path,fileSize);
            if (
                ErrorCondition( File_IsCorrect(elfFile), oC_ErrorCode_AllocationError                   )
             && ErrorCode     ( oC_VirtualFileSystem_fread(file, elfFile->FileData, &fileSize)          )
             && ErrorCondition( IsElfSignatureCorrect(elfFile->FileData), oC_ErrorCode_NotElfFile       )
                )
            {
                elfFile->FileHeader                 = elfFile->FileData;
                elfFile->SectionHeaderNameString    = oC_Elf_GetSectionHeader(elfFile,elfFile->FileHeader->SectionHeaderNameStringTableIndex);
                elfFile->dynstr                     = oC_Elf_GetSectionHeaderByName(elfFile,".dynstr");
                elfFile->dynsym                     = oC_Elf_GetSectionHeaderByName(elfFile,".dynsym");
                elfFile->strtab                     = oC_Elf_GetSectionHeaderByName(elfFile,".strtab");
                elfFile->symtab                     = oC_Elf_GetSectionHeaderByName(elfFile,".symtab");
                elfFile->text                       = oC_Elf_GetSectionHeaderByName(elfFile,".text");
                elfFile->rodata                     = oC_Elf_GetSectionHeaderByName(elfFile,".rodata");
                elfFile->got                        = oC_Elf_GetSectionHeaderByName(elfFile,".got");
                elfFile->bss                        = oC_Elf_GetSectionHeaderByName(elfFile,".bss");
                elfFile->data                       = oC_Elf_GetSectionHeaderByName(elfFile,".data");
                elfFile->syscalls                   = oC_Elf_GetSectionHeaderByName(elfFile,".syscalls");

                *outFile = elfFile;
                errorCode = oC_ErrorCode_None;
            }
            else
            {
                oC_SaveIfFalse("Cannot delete elfFile", elfFile == NULL || File_Delete(elfFile), oC_ErrorCode_ReleaseError);
            }
        }
        oC_SaveIfErrorOccur("Cannot close file", oC_VirtualFileSystem_fclose(file));
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief unloads ELF file
 *
 * The function is responsible for unloading of an ELF file.
 *
 * @param File              ELF file context
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 *
 * Possible codes:
 *     Error Code                           | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_ObjectNotCorrect          | The `*File` does not point to the valid Elf object
 *  #oC_ErrorCode_WrongAddress              | The `File` does not point to the valid address
 *  #oC_ErrorCode_CannotDeleteProcess       | Cannot delete the process
 *  #oC_ErrorCode_CannotDeleteObject        | Cannot delete the program
 *  #oC_ErrorCode_ReleaseError              | Cannot release allocated resources
 *
 * @note
 * Please note, that more error codes can be returned by #oC_Process_Delete, #oC_Program_Delete functions.
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Elf_UnloadFile( oC_Elf_File_t * File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(File)                 , oC_ErrorCode_WrongAddress         )
     && ErrorCondition( File_IsCorrect(*File)       , oC_ErrorCode_ObjectNotCorrect     )
        )
    {
        oC_Elf_File_t file = *File;
        if (
            ErrorCondition( file->Process == NULL || oC_Process_Delete(&file->Process), oC_ErrorCode_CannotDeleteProcess )
         && ErrorCondition( file->Program == NULL || oC_Program_Delete(file->Program) , oC_ErrorCode_CannotDeleteObject  )
         && ErrorCondition( File_Delete(file)                                         , oC_ErrorCode_ReleaseError        )
            )
        {
            *File = NULL;
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns an address to the section's data
 *
 * The function returns pointer to the data of the given section
 *
 * @param File                  ELF file from #oC_Elf_LoadFile function
 * @param SectionHeader         Pointer to the SectionHeader
 *
 * @return #NULL if not found or in case of error
 */
//==========================================================================================================================================
void* oC_Elf_GetSectionData( oC_Elf_File_t File, oC_Elf_SectionHeader_t* SectionHeader )
{
    void* sectionData = NULL;

    if (File_IsCorrect(File) && isaddresscorrect(File->FileData) && isaddresscorrect(SectionHeader))
    {
        sectionData = File->FileData + SectionHeader->Offset;
    }

    return sectionData;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the entry in the table section header
 */
//==========================================================================================================================================
void * oC_Elf_GetSectionEntry( oC_Elf_File_t File, oC_Elf_SectionHeader_t * SectionHeader, oC_Elf_Word_t EntryIndex )
{
    void * entry = NULL;

    if(isaddresscorrect(SectionHeader) && SectionHeader->Size > 0)
    {
        uint8_t * sectionData = File->FileData + SectionHeader->Offset;

        if(SectionHeader->Type == oC_Elf_SectionType_STRTAB)
        {
            if(EntryIndex < SectionHeader->Size)
            {
                entry = &sectionData[EntryIndex];
            }
        }
        else if(SectionHeader->EntrySize > 0)
        {
            oC_Elf_Word_t   numberOfEntries     = SectionHeader->Size / SectionHeader->EntrySize;

            if(EntryIndex < numberOfEntries)
            {
                entry = &sectionData[ EntryIndex * SectionHeader->EntrySize ];
            }
        }

    }

    return entry;
}

//==========================================================================================================================================
/**
 * @brief getter for a string from string table
 *
 * The function searches for a string with the given index in the string table
 *
 * @param File                  File from #oC_Elf_LoadFile
 * @param NameIndex             Index of the string to get
 *
 * @return pointer to the string or #NULL if not found or error
 */
//==========================================================================================================================================
const char* oC_Elf_GetString( oC_Elf_File_t File, oC_Elf_Word_t NameIndex )
{
    const char* str = NULL;

    if ( File_IsCorrect(File) && isaddresscorrect(File->SectionHeaderNameString) )
    {
        str = oC_Elf_GetSectionEntry(File, File->SectionHeaderNameString, NameIndex);
    }

    return str;
}

//==========================================================================================================================================
/**
 * @brief searches for a symbol with the given name in the symbol table
 *
 * The function searches for a symbol-table entry with the given name
 *
 * @param File                  File from the function #oC_Elf_LoadFile
 * @param SymbolName            Name of the symbol to find
 *
 * @return
 * Symbol table entry or #NULL if not found or error
 */
//==========================================================================================================================================
oC_Elf_SymbolTableEntry_t* oC_Elf_FindSymbol( oC_Elf_File_t File, const char* SymbolName )
{
    oC_Elf_SymbolTableEntry_t* foundEntry = NULL;
    if( File_IsCorrect(File) && isaddresscorrect(SymbolName) && strlen(SymbolName) > 0 && isaddresscorrect(File->symtab) )
    {
        oC_Elf_Word_t numberOfEntries = File->symtab->Size / File->symtab->EntrySize;
        for(oC_Elf_Word_t index = 0; index < numberOfEntries; index++)
        {
            oC_Elf_SymbolTableEntry_t* currentEntry = oC_Elf_GetSectionEntry(File, File->symtab, index);
            if (isaddresscorrect(currentEntry))
            {
                const char* symbolName = oC_Elf_GetString(File, currentEntry->NameIndex);
                if ( strcmp(symbolName, SymbolName) == 0 )
                {
                    foundEntry = currentEntry;
                    break;
                }
            }
        }
    }

    return foundEntry;
}

//==========================================================================================================================================

//==========================================================================================================================================
void* oC_Elf_GetProgramEntryAddress( oC_Elf_File_t File )
{
    void *entryAddress = NULL;

    if ( File_IsCorrect( File ) && isaddresscorrect(File->ProgramHeader) && File->FileHeader->Entry <= File->ProgramHeader->MemorySize && isaddresscorrect(File->LoadableSegment)  )
    {
        entryAddress = File->LoadableSegment + File->FileHeader->Entry;
    }

    return entryAddress;
}

//==========================================================================================================================================
/**
 * @brief Getter for program entry offset
 *
 * The function returns offset of an entry point for the program
 *
 * @param File              ELF file from #oC_Elf_LoadFile
 *
 * @return #NULL if file is not correct or entry offset is not set
 */
//==========================================================================================================================================
oC_Elf_Addr_t oC_Elf_GetProgramEntryOffset( oC_Elf_File_t File )
{
    oC_Elf_Addr_t programEntryOffset = 0;

    if ( File_IsCorrect( File ) && File->FileHeader->Entry <= File->FileSize )
    {
        programEntryOffset = File->FileHeader->Entry;
    }

    return programEntryOffset;
}

//==========================================================================================================================================
/**
 * @brief Loads a program from the given ELF file.
 *
 * This function is responsible for loading a program from the provided ELF file.
 *
 * @param File The ELF file structure obtained from the #oC_Elf_LoadFile function.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If the operation is successful, it will
 * be set to #oC_ErrorCode_None value.
 *
 * Possible codes:
 *     Error Code                           | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | Unexpected error in implementation
 *  #oC_ErrorCode_ObjectNotCorrect          | The ELF file structure is not correct
 *  #oC_ErrorCode_WrongAddress              | Invalid address in the ELF file structure
 *  #oC_ErrorCode_NameNotAvailable          | The ELF file name is empty
 *  #oC_ErrorCode_ProcessNotCorrect         | The current process is not correct
 *  #oC_ErrorCode_StreamNotCorrect          | Invalid stream address for stdin, stdout, or stderr
 *  #oC_ErrorCode_EntryPointNotValid        | The program entry point is not correct
 *  #oC_ErrorCode_AlreadyConfigured         | The program is already loaded
 *  #oC_ErrorCode_MissingArgument           | The loadable segment is missing in the file header
 *  #oC_ErrorCode_NotImplemented            | Currently the function supports only 1 loadable segment
 *
 * @note
 * After the function call, the program is created and associated with the ELF file.
 * To release allocated resources, use the `getlogs` command to check for errors and handle accordingly.
 * The error codes can be retrieved using the `getlogs` command in the command line.
 *
 * @see oC_Elf_LoadFile
 * @see oC_Program_New
 * @see oC_Process_GetPriority
 * @see oC_ErrorCode_t
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Elf_LoadProgram( oC_Elf_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_Process_t currentProcess = getcurprocess();
    oC_Stream_t inputStream   = oC_Process_GetInputStream(currentProcess);
    oC_Stream_t outputStream  = oC_Process_GetOutputStream(currentProcess);
    oC_Stream_t errorStream   = oC_Process_GetErrorStream(currentProcess);
    const char* stdinName     = oC_Stream_GetName(inputStream);
    const char* stdoutName    = oC_Stream_GetName(outputStream);
    const char* stderrName    = oC_Stream_GetName(errorStream);

    if (
        ErrorCondition( File_IsCorrect(File)                  , oC_ErrorCode_ObjectNotCorrect          )
     && ErrorCondition( isaddresscorrect(File->Name)          , oC_ErrorCode_WrongAddress              )
     && ErrorCondition( strlen(File->Name) > 0                , oC_ErrorCode_NameNotAvailable          )
     && ErrorCondition( oC_Process_IsCorrect(currentProcess)  , oC_ErrorCode_ProcessNotCorrect         )
     && ErrorCondition( isaddresscorrect(stdinName)           , oC_ErrorCode_StreamNotCorrect          )
     && ErrorCondition( isaddresscorrect(stdoutName)          , oC_ErrorCode_StreamNotCorrect          )
     && ErrorCondition( isaddresscorrect(stderrName)          , oC_ErrorCode_StreamNotCorrect          )
     && ErrorCondition( File->Program == NULL                 , oC_ErrorCode_AlreadyConfigured         )
     && ErrorCode( FindLoadableSegment(File) )
     && ErrorCode( LoadSegment(File) )
        )
    {
        void* entryPoint= oC_Elf_GetProgramEntryAddress(File);
        if ( ErrorCondition( isaddresscorrect(entryPoint), oC_ErrorCode_EntryPointNotValid ) )
        {
            uint16_t* instr = entryPoint;
            kdebuglog( oC_LogType_Track, "entryPoint: %p %04x\n", entryPoint, *instr );
            File->Program = oC_Program_New(oC_Process_GetPriority(currentProcess), File->Name, entryPoint, stdinName, stdoutName, stderrName, 0);
            if ( ErrorCondition( oC_Program_IsCorrect(File->Program), oC_ErrorCode_ProgramNotPrepared ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Executes the program from the given ELF file.
 *
 * This function is responsible for executing the program from the provided ELF file.
 *
 * @param File              The ELF file structure obtained from the #oC_Elf_LoadFile function.
 * @param Argc              Number of arguments passed to the program.
 * @param Argv              Array of arguments passed to the program.
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If the operation is successful, it will
 * be set to #oC_ErrorCode_None value.
 *
 * Possible codes:
 *     Error Code                           | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | Unexpected error in implementation
 *  #oC_ErrorCode_ObjectNotCorrect          | The ELF file structure is not correct
 *  #oC_ErrorCode_ProgramNotPrepared        | The program is not prepared
 *  #oC_ErrorCode_WrongUser                 | The current user is not correct
 *  #oC_ErrorCode_MissingArgument           | The number of arguments is less than 1
 *  #oC_ErrorCode_WrongAddress              | Invalid address in the ELF file structure
 *  #oC_ErrorCode_StreamNotCorrect          | Invalid stream address for stdin, stdout, or stderr
 *  #oC_ErrorCode_CannotCreateProcess       | Cannot create a new process
 *  #oC_ErrorCode_ProcessAlreadyStarted     | The process is already started
 *  #oC_ErrorCode_ReleaseError              | Cannot release allocated resources
 *
 * @note
 * After the function call, the program is created and associated with the ELF file.
 * To release allocated resources, use the #oC_Elf_UnloadFile function.
 *
 * @see oC_Elf_LoadFile
 * @see oC_Program_New
 * @see oC_Process_GetPriority
 * @see oC_ErrorCode_t
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Elf_Execute( oC_Elf_File_t File, int Argc, const char** Argv)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;
    oC_User_t currentUser = getcuruser();

    if (
        ErrorCondition( File_IsCorrect(File)                    , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( oC_Program_IsCorrect(File->Program)     , oC_ErrorCode_ProgramNotPrepared       )       // #oC_Elf_LoadProgram has to be called first
     && ErrorCondition( oC_User_IsCorrect(currentUser)          , oC_ErrorCode_WrongUser                )
     && ErrorCondition( Argc >= 1                               , oC_ErrorCode_MissingArgument          )
     && ErrorCondition( isaddresscorrect(Argv)                  , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isaddresscorrect(Argv[0])               , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( File->Process == NULL                   , oC_ErrorCode_ProcessAlreadyStarted    )       // #oC_Elf_Kill has to be called first
        )
    {
        File->Process = oC_Program_NewProcess(File->Program, currentUser);
        if (
             ErrorCondition( oC_Process_IsCorrect(File->Process), oC_ErrorCode_CannotCreateProcess )
          && ErrorCode( oC_Program_Exec(File->Program, File->Process, Argc, Argv, &File->MainThread, &File->ProgramContext) )
             )
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            oC_SaveIfFalse( "Cannot delete ELF process", oC_Process_Delete(&File->Process), oC_ErrorCode_ReleaseError );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Kills the program from the given ELF file.
 *
 * This function is responsible for killing the program from the provided ELF file.
 *
 * @param File The ELF file structure obtained from the #oC_Elf_LoadFile function and started by #oC_Elf_Execute
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If the operation is successful, it will
 * be set to #oC_ErrorCode_None value.
 *
 * Possible codes:
 *     Error Code                           | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | Unexpected error in implementation
 *  #oC_ErrorCode_ObjectNotCorrect          | The ELF file structure is not correct
 *  #oC_ErrorCode_ProcessNotCorrect         | The process is not correct
 *  #oC_ErrorCode_NotRunning                | The process is not running
 *  #oC_ErrorCode_ReleaseError              | Cannot release allocated resources
 *  #oC_ErrorCode_CannotDeleteProcess       | Cannot delete the process
 *
 * @note
 * After the function call, the program is created and associated with the ELF file.
 * To release allocated resources, use the #oC_Elf_UnloadFile function.
 *
 * @see oC_Elf_LoadFile
 * @see oC_Elf_Execute
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Elf_Kill( oC_Elf_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( File_IsCorrect(File)                    , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( oC_Process_IsCorrect(File->Process)     , oC_ErrorCode_ProcessNotCorrect    )
     && ErrorCondition( oC_Process_IsActive(File->Process)      , oC_ErrorCode_NotRunning           )
        )
    {
        if ( ErrorCondition( oC_Process_Delete( &File->Process ), oC_ErrorCode_CannotDeleteProcess ) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the process from the given ELF file
 *
 * @param File              ELF file from #oC_Elf_LoadFile
 *
 * @return #NULL if file is not correct or process is not set
 */
//==========================================================================================================================================
oC_Process_t oC_Elf_GetProcess( oC_Elf_File_t File )
{
    oC_Process_t process = NULL;

    if ( File_IsCorrect( File ) )
    {
        process = File->Process;
    }

    return process;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the program from the given ELF file
 *
 * @param File              ELF file from #oC_Elf_LoadFile (executed by #oC_Elf_Execute)
 *
 * @return #NULL if file is not correct or program is not set
 */
//==========================================================================================================================================
oC_Program_t oC_Elf_GetProgram( oC_Elf_File_t File )
{
    oC_Program_t program = NULL;

    if ( File_IsCorrect( File ) )
    {
        program = File->Program;
    }

    return program;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the program from the given ELF file
 *
 * @param File              ELF file from #oC_Elf_LoadFile (executed by #oC_Elf_Execute)
 *
 * @return #NULL if file is not correct or program is not set
 */
//==========================================================================================================================================
void* oC_Elf_GetProgramContext( oC_Elf_File_t File )
{
    void *programContext = NULL;

    if ( File_IsCorrect( File ) )
    {
        programContext = File->ProgramContext;
    }

    return programContext;
}

//==========================================================================================================================================
/**
 * @brief Waits for the program to be finished
 *
 * @param File              ELF file from #oC_Elf_LoadFile (executed by #oC_Elf_Execute)
 *
 * @return #NULL if file is not correct or main thread is not set
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_Elf_WaitForFinish( oC_Elf_File_t File, oC_Time_t CheckPeriod, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( File_IsCorrect(File)                    , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( oC_Process_IsCorrect(File->Process)     , oC_ErrorCode_ProcessNotCorrect    )  // #oC_Elf_Execute has to be called first
        )
    {
        errorCode = oC_Program_WaitForFinish(File->ProgramContext, CheckPeriod, Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the program header
 */
//==========================================================================================================================================
oC_Elf_ProgramHeader_t * oC_Elf_GetProgramHeader( oC_Elf_File_t File , oC_Elf_Word_t ProgramIndex )
{
    oC_Elf_ProgramHeader_t * programHeader = NULL;

    if(
        isaddresscorrect(File)
     && isaddresscorrect(File->FileData)
     && isaddresscorrect(File->FileHeader)
     && ProgramIndex < File->FileHeader->ProgramHeaderTableNumberOfEntries
         )
    {
        programHeader = File->FileData + File->FileHeader->ProgramHeaderTableOffset + (ProgramIndex * File->FileHeader->ProgramHeaderTableEntrySize);
    }

    return programHeader;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the section header table entry
 */
//==========================================================================================================================================
oC_Elf_SectionHeader_t * oC_Elf_GetSectionHeader( oC_Elf_File_t File , oC_Elf_Word_t SectionIndex )
{
    oC_Elf_SectionHeader_t * sectionHeader = NULL;

    if(
        isaddresscorrect(File)
     && isaddresscorrect(File->FileData)
     && isaddresscorrect(File->FileHeader)
     && SectionIndex < File->FileHeader->SectionHeaderTableNumberOfEntries
         )
    {
        sectionHeader = File->FileData + File->FileHeader->SectionHeaderTableOffset + (SectionIndex * File->FileHeader->SectionHeaderTableEntrySize);
    }

    return sectionHeader;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the first section header of the given type
 */
//==========================================================================================================================================
oC_Elf_SectionHeader_t * oC_Elf_GetSectionHeaderOfType( oC_Elf_File_t File , oC_Elf_SectionType_t Type )
{
    oC_Elf_SectionHeader_t * sectionHeader = NULL;

    if(
        isaddresscorrect(File)
     && isaddresscorrect(File->FileHeader)
        )
    {
        oC_Elf_Word_t            index   = 0;
        oC_Elf_SectionHeader_t * section = NULL;

        while( (section = oC_Elf_GetSectionHeader(File,index++)) != NULL )
        {
            if(section->Type == Type)
            {
                sectionHeader = section;
                break;
            }
        }
    }

    return sectionHeader;
}

//==========================================================================================================================================
/**
 * @brief returns reference to the first section header with the given name
 */
//==========================================================================================================================================
oC_Elf_SectionHeader_t * oC_Elf_GetSectionHeaderByName( oC_Elf_File_t File , const char * SectionName )
{
    oC_Elf_SectionHeader_t * sectionHeader = NULL;

    if(
        isaddresscorrect(File)
     && isaddresscorrect(File->FileHeader)
        )
    {
        oC_Elf_Word_t            index   = 0;
        oC_Elf_SectionHeader_t * section = NULL;

        while( (section = oC_Elf_GetSectionHeader(File,index++)) != NULL )
        {
            char * name = oC_Elf_GetSectionEntry(File,File->SectionHeaderNameString,section->NameIndex);

            if( strcmp(name,SectionName) == 0 )
            {
                sectionHeader = section;
                break;
            }
        }
    }

    return sectionHeader;
}

//==========================================================================================================================================
/**
 * @brief returns string associated with the ELF type
 */
//==========================================================================================================================================
const char * oC_Elf_GetTypeString( oC_Elf_Half_t ElfType )
{
    const char * string = NULL;

    switch(ElfType)
    {
        case oC_Elf_Type_CoreFile:              string = "Core file";               break;
        case oC_Elf_Type_ExecutableFile:        string = "Executable file";         break;
        case oC_Elf_Type_HiProc:                string = "HIPROC";                  break;
        case oC_Elf_Type_LoProc:                string = "LOPROC";                  break;
        case oC_Elf_Type_None:                  string = "NONE (INCORRECT)";        break;
        case oC_Elf_Type_RelocatableFile:       string = "Relocatable file";        break;
        case oC_Elf_Type_SharedObjectFile:      string = "Shared object file";      break;
        default: string = "unknown type"; break;
    }

    return string;
}

//==========================================================================================================================================
/**
 * @brief returns string associated with the ELF machine type
 */
//==========================================================================================================================================
const char * oC_Elf_GetMachineString( oC_Elf_Half_t MachineType )
{
    const char * string = NULL;

    switch(MachineType)
    {
        case oC_Elf_MachineType_NoMachine   : string =  "No machine    "; break;
        case oC_Elf_MachineType_M32         : string =  "AT&T WE 32100 "; break;
        case oC_Elf_MachineType_SPARC       : string =  "SPARC         "; break;
        case oC_Elf_MachineType_386         : string =  "Intel 80386   "; break;
        case oC_Elf_MachineType_68K         : string =  "Motorola 68000"; break;
        case oC_Elf_MachineType_88K         : string =  "Motorola 88000"; break;
        case oC_Elf_MachineType_860         : string =  "Intel 80860   "; break;
        case oC_Elf_MachineType_MIPS        : string =  "MIPS RS3000   "; break;
        case oC_Elf_MachineType_ArmCortexM7 : string =  "ARM Cortex M7 "; break;
        default: string = "unknown machine"; break;
    }

    return string;
}

//==========================================================================================================================================
/**
 * @brief returns string associated with the ELF machine type
 */
//==========================================================================================================================================
const char * oC_Elf_GetSectionTypeString( oC_Elf_SectionType_t SectionType )
{
    const char * string = NULL;

    switch(SectionType)
    {
        case oC_Elf_SectionType_NULL        : string = "NULL        "; break;
        case oC_Elf_SectionType_PROGBITS    : string = "PROGBITS    "; break;
        case oC_Elf_SectionType_SYMTAB      : string = "SYMTAB      "; break;
        case oC_Elf_SectionType_STRTAB      : string = "STRTAB      "; break;
        case oC_Elf_SectionType_RELA        : string = "RELA        "; break;
        case oC_Elf_SectionType_HASH        : string = "HASH        "; break;
        case oC_Elf_SectionType_DYNAMIC     : string = "DYNAMIC     "; break;
        case oC_Elf_SectionType_NOTE        : string = "NOTE        "; break;
        case oC_Elf_SectionType_NOBITS      : string = "NOBITS      "; break;
        case oC_Elf_SectionType_REL         : string = "REL         "; break;
        case oC_Elf_SectionType_SHLIB       : string = "SHLIB       "; break;
        case oC_Elf_SectionType_DYNSYM      : string = "DYNSYM      "; break;
        case oC_Elf_SectionType_LOPROC      : string = "LOPROC      "; break;
        case oC_Elf_SectionType_HIPROC      : string = "HIPROC      "; break;
        case oC_Elf_SectionType_LOUSER      : string = "LOUSER      "; break;
        case oC_Elf_SectionType_HIUSER      : string = "HIUSER      "; break;
        default: string = "unknown section type"; break;
    }

    return string;
}

//==========================================================================================================================================
/**
 * @brief returns string with section flags
 */
//==========================================================================================================================================
char * oC_Elf_GetSectionFlagsString( oC_Elf_Word_t Flags, char * Buffer , oC_MemorySize_t BufferSize )
{
    char * result = NULL;

    if(isram(Buffer) && BufferSize > 0)
    {
        result = Buffer;

        memset(Buffer,0,BufferSize);

        if((Flags & oC_Elf_SectionFlag_WRITE) && BufferSize >= 1)
        {
            *Buffer++ = 'W';
            BufferSize -= 1;
        }
        if((Flags & oC_Elf_SectionFlag_ALLOC) && BufferSize >= 1)
        {
            *Buffer++ = 'A';
            BufferSize -= 1;
        }
        if((Flags & oC_Elf_SectionFlag_EXECINSTR) && BufferSize >= 1)
        {
            *Buffer++ = 'E';
            BufferSize -= 1;
        }
    }

    return result;
}

//==========================================================================================================================================
/**
 * @brief returns string associated with the symbol binding
 */
//==========================================================================================================================================
const char * oC_Elf_GetSymbolBindingString( uint8_t SymbolBinding )
{
    const char * string = NULL;

    switch(SymbolBinding)
    {
        case oC_Elf_SymbolBinding_LOCAL:    string = "LOCAL "; break;
        case oC_Elf_SymbolBinding_GLOBAL:   string = "GLOBAL"; break;
        case oC_Elf_SymbolBinding_WEAK:     string = "WEAK  "; break;
        default: string = "unknown binding"; break;
    }

    return string;
}

//==========================================================================================================================================
/**
 * @brief returns string associated with the symbol binding
 */
//==========================================================================================================================================
const char * oC_Elf_GetSymbolTypeString( uint8_t SymbolType )
{
    const char * string = NULL;

    switch(SymbolType)
    {
        case oC_Elf_SymbolType_NOTYPE  : string = "NOTYPE  "; break;
        case oC_Elf_SymbolType_OBJECT  : string = "OBJECT  "; break;
        case oC_Elf_SymbolType_FUNC    : string = "FUNC    "; break;
        case oC_Elf_SymbolType_SECTION : string = "SECTION "; break;
        case oC_Elf_SymbolType_FILE    : string = "FILE    "; break;
        case oC_Elf_SymbolType_LOPROC  : string = "LOPROC  "; break;
        case oC_Elf_SymbolType_HIPROC  : string = "HIPROC  "; break;
        default: string = "unknown symbol type"; break;
    }

    return string;
}

//==========================================================================================================================================
/**
 * @brief returns string associated with the symbol binding
 */
//==========================================================================================================================================
const char * oC_Elf_GetProgramTypeString( uint8_t ProgramType )
{
    const char * string = NULL;

    switch(ProgramType)
    {
        case oC_Elf_ProgramType_NULL   : string = "NULL   "; break;
        case oC_Elf_ProgramType_LOAD   : string = "LOAD   "; break;
        case oC_Elf_ProgramType_DYNAMIC: string = "DYNAMIC"; break;
        case oC_Elf_ProgramType_INTERP : string = "INTERP "; break;
        case oC_Elf_ProgramType_NOTE   : string = "NOTE   "; break;
        case oC_Elf_ProgramType_SHLIB  : string = "SHLIB  "; break;
        case oC_Elf_ProgramType_PHDR   : string = "PHDR   "; break;
        default: string = "unknown program type"; break;
    }

    return string;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks if the ELF signature is correct
 *
 * @warning
 * Data has to be at least 4 bytes length
 */
//==========================================================================================================================================
static bool IsElfSignatureCorrect( uint8_t * Data )
{
    return Data[0] == EI_MAG0
        && Data[1] == EI_MAG1
        && Data[2] == EI_MAG2
        && Data[3] == EI_MAG3;
}

//==========================================================================================================================================
/**
 * @brief creates new ELF File object
 *
 * @param ProgramName           Name of the program to copy into the file object
 * @param FileSize              Size of the file data
 *
 * @return pointer to the created file object or NULL if allocation failed
 */
//==========================================================================================================================================
static oC_Elf_File_t File_New( const char* ProgramName, oC_MemorySize_t FileSize )
{
    oC_Elf_File_t file = malloc( sizeof(struct File_t) );
    void* fileData = malloc( FileSize, AllocationFlags_ExternalRamFirst | AllocationFlags_ZeroFill);
    char* fileName = malloc( strlen(ProgramName) + 1);

    if (
        oC_SaveIfFalse( "Cannot allocate memory for ELF file context", file     != NULL, oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse( "Cannot allocate memory for ELF file data"   , fileData != NULL, oC_ErrorCode_AllocationError )
     && oC_SaveIfFalse( "Cannot allocate memory for ELF file data"   , fileName != NULL, oC_ErrorCode_AllocationError )
        )
    {
        file->ObjectControl     = oC_CountObjectControl(file, oC_ObjectId_ElfFile);
        file->FileData          = fileData;
        file->FileSize          = FileSize;
        file->Name              = fileName;
        strcpy(file->Name, ProgramName);
    }
    else
    {
        oC_SaveIfFalse( "Cannot release memory", file     == NULL || free(file)               , oC_ErrorCode_ReleaseError );
        oC_SaveIfFalse( "Cannot release memory", fileData == NULL || free(fileData)           , oC_ErrorCode_ReleaseError );
        oC_SaveIfFalse( "Cannot release memory", fileName == NULL || free(fileName)           , oC_ErrorCode_ReleaseError );
        file = NULL;
    }

    return file;
}


//==========================================================================================================================================
/**
 * @brief Deletes ELF file memoryoC_Process_GetAllocator(ProgramContext->Process)
 * @param elfFile           file to delete
 * @return true if success
 */
//==========================================================================================================================================
static bool File_Delete( oC_Elf_File_t elfFile )
{
    bool success = false;

    if( oC_SaveIfFalse("Invalid ELF file", File_IsCorrect(elfFile), oC_ErrorCode_ObjectNotCorrect) )
    {
        elfFile->ObjectControl = 0;
        bool releasedData    = oC_SaveIfFalse("Releasing ELF file data"    , free(elfFile->FileData)                    , oC_ErrorCode_ReleaseError);
        bool releasedName    = oC_SaveIfFalse("Releasing file name"        , free(elfFile->Name)                        , oC_ErrorCode_ReleaseError);
        bool releasedContext = oC_SaveIfFalse("Releasing ELF file context" , free(elfFile)                              , oC_ErrorCode_ReleaseError);
        success = releasedData && releasedContext && releasedName;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief Checks if the object is correct
 *
 * The function checks if the given object file is valid
 *
 * @param File          File to check
 *
 * @return true if correct
 */
//==========================================================================================================================================
static bool File_IsCorrect( const oC_Elf_File_t File )
{
    return isaddresscorrect(File) && oC_CheckObjectControl(File, oC_ObjectId_ElfFile, File->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief updates .got section
 *
 * @param File          File with loaded sections
 *
 * @return #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
static oC_ErrorCode_t UpdateGlobalOffsetTable( oC_Elf_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( File_IsCorrect( File )           , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( isaddresscorrect(File->got)      , oC_ErrorCode_GotSectionIsMissing  )
     && ErrorCondition( isaddresscorrect(File->got)      , oC_ErrorCode_GotSectionIsMissing  )
        )
    {
        errorCode = oC_ErrorCode_None;
        oC_Elf_Word_t numberOfEntries = File->got->Size / File->got->EntrySize;
        for( oC_Elf_Word_t index = 0; index < numberOfEntries; index++ )
        {
            oC_Elf_Addr_t *gotEntry = File->FileData + File->got->Offset + (index * File->got->EntrySize);
            if ( ErrorCondition( isaddresscorrect( gotEntry ), oC_ErrorCode_WrongAddress ) )
            {
                *gotEntry += (oC_Elf_Addr_t)File->LoadableSegment;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Updates .syscalls section with the correct addresses
 *
 * @param File          File with loaded sections
 *
 * @return #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectSystemCalls( oC_Elf_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( File_IsCorrect(File)                    , oC_ErrorCode_FileNotCorrect           )
     && ErrorCondition( isaddresscorrect(File->LoadableSegment) , oC_ErrorCode_FileNotLoaded            )
     && ErrorCondition( isaddresscorrect(File->syscalls)        , oC_ErrorCode_SyscallsSectionIsMissing )
        )
    {
        oC_SystemCall_t* systemCalls = oC_Elf_GetSectionData(File, File->syscalls);
        if ( ErrorCondition( isaddresscorrect(systemCalls), oC_ErrorCode_WrongAddress ) )
        {
            errorCode = oC_ErrorCode_None;
            uint32_t numberOfCalls = File->syscalls->Size / sizeof(oC_SystemCall_t);
            for( uint32_t i = 0; i < numberOfCalls; i++ , systemCalls++)
            {
                oC_SystemCall_t * systemCall = systemCalls;

                systemCall->CallAddress = NULL;

        #define ADD_FUNCTION(NAME)      if(strcmp(#NAME,systemCall->Name) == 0) \
                                        { \
                                            systemCall->CallAddress = NAME;\
                                        }

                POSIX_FUNCTIONS_LIST(ADD_FUNCTION);

        #undef ADD_FUNCTION

                if(systemCall->CallAddress == NULL)
                {
                    kdebuglog( oC_LogType_Error, "Unknown system call '%s'\n", systemCall->Name );
                    errorCode = oC_ErrorCode_UnknownSystemCall;
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * Clears the bss table
 * @param File          File with loaded sections
 * @return #oC_ErrorCode_None if success
 */
//==========================================================================================================================================
static oC_ErrorCode_t ClearBss( oC_Elf_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( File_IsCorrect( File )              , oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( isaddresscorrect( File->bss )       , oC_ErrorCode_BssSectionIsMissing )
     )
    {
        memset( File->FileData + File->bss->Offset, 0, File->bss->Size );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief searches for a loadable segments
 *
 * The function searches for a loadable segments in the given ELF file
 *
 * @param File          File to check
 *
 * @return
 * Code of error from the #oC_ErrorCode_t type. If the operation is successful, it will
 * be set to #oC_ErrorCode_None value.
 *
 * Possible codes:
 *    Error Code                           | Description
 *    -------------------------------------|---------------------------------------------------------------------------------------------------
 *     #oC_ErrorCode_None                  | Operation success
 *     #oC_ErrorCode_ImplementError        | Unexpected error in implementation
 *     #oC_ErrorCode_ObjectNotCorrect      | The ELF file structure is not correct
 *     #oC_ErrorCode_WrongAddress          | Invalid address in the ELF file structure
 *     #oC_ErrorCode_MissingArgument       | The number of program headers is less than 1
 */
//==========================================================================================================================================
static oC_ErrorCode_t FindLoadableSegment( oC_Elf_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( File_IsCorrect( File )                                   , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( isaddresscorrect(File->FileHeader)                       , oC_ErrorCode_WrongAddress         )
     && ErrorCondition( File->FileHeader->ProgramHeaderTableNumberOfEntries > 0  , oC_ErrorCode_MissingArgument      )
        )
    {
        errorCode = oC_ErrorCode_NoSuchFile;
        File->ProgramHeader = NULL;
        oC_Elf_Word_t numberOfEntries = File->FileHeader->ProgramHeaderTableNumberOfEntries;
        for( oC_Elf_Word_t index = 0; index < numberOfEntries; index++ )
        {
            oC_Elf_ProgramHeader_t* programHeader = oC_Elf_GetProgramHeader( File, index );
            if ( programHeader->Type == oC_Elf_ProgramType_LOAD )
            {
                kdebuglog(oC_LogType_Track, "Found programHeader with offset 0x%lx  and memory size 0x%lx\n", programHeader->Offset, programHeader->MemorySize);
                File->ProgramHeader = programHeader;
                errorCode = oC_ErrorCode_None;
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief loads loadable segments
 */
//==========================================================================================================================================
static oC_ErrorCode_t LoadSegment( oC_Elf_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( File_IsCorrect(File)                        , oC_ErrorCode_ObjectNotCorrect     )
     && ErrorCondition( isaddresscorrect(File->ProgramHeader)       , oC_ErrorCode_ProgramNotCorrect    )
     && ErrorCondition( File->ProgramHeader->MemorySize > 0           , oC_ErrorCode_ProgramNotCorrect    )
     && ErrorCondition( File->ProgramHeader->MemorySize < oC_MemMan_GetExternalHeapSize(), oC_ErrorCode_NoSuchMemory )
        )
    {
        kdebuglog( oC_LogType_Track, "Allocating memory of size %lx for the loadable segment\n", File->ProgramHeader->MemorySize);
        File->LoadableSegment = kamalloc( File->ProgramHeader->MemorySize, getcurallocator(), AllocationFlags_ZeroFill, oC_MEM_LLD_STACK_ALIGNMENT );
        if (
             ErrorCondition( isaddresscorrect(File->LoadableSegment), oC_ErrorCode_AllocationError )
          && ErrorCode( ClearBss(File) )
          && ErrorCode( UpdateGlobalOffsetTable(File) )
          && ErrorCode( ConnectSystemCalls(File) )
             )
        {
            void* segmentSource = File->FileData + File->ProgramHeader->Offset;
            memcpy(File->LoadableSegment, segmentSource, File->ProgramHeader->MemorySize);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


