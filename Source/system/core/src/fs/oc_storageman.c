/** ****************************************************************************************************************************************
 *
 * @brief        __FILE__DESCRIPTION__
 * 
 * @file          oc_storageman.c
 *
 * @author     Patryk Kubiak - (Created on: 20.08.2017 23:38:37) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_module.h>
#include <oc_storageman.h>
#include <oc_intman.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static const oC_Allocator_t    Allocator = {
                .Name = "StorageMan" ,
};
static oC_List(oC_Storage_t)   Storages = NULL;
const oC_Module_Registration_t StorageMan = {
                .Name               = "StorageMan" ,
                .LongName           = "Storage Manager" ,
                .Module             = oC_Module_StorageMan ,
                .TurnOnFunction     = oC_StorageMan_TurnOn ,
                .TurnOffFunction    = oC_StorageMan_TurnOff ,
                .RequiredModules    = { oC_Module_None } ,
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StorageMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if(oC_Module_TurnOffVerification(&errorCode, oC_Module_StorageMan))
    {
        Storages = oC_List_New(&Allocator);

        if( ErrorCondition( Storages != NULL, oC_ErrorCode_AllocationError ) )
        {
            oC_Module_TurnOn(oC_Module_StorageMan);
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StorageMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_StorageMan) )
    {
        oC_Module_TurnOff(oC_Module_StorageMan);

        bool deleted = oC_List_Delete(Storages,0);

        if( ErrorCondition( deleted, oC_ErrorCode_ReleaseError ) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StorageMan_AddStorage( oC_Storage_t Storage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_Module_TurnOnVerification(&errorCode, oC_Module_StorageMan))
    {
        oC_IntMan_EnterCriticalSection();

        bool foundOnList = oC_List_Contains(Storages,Storage);

        if(
            ErrorCondition( oC_Storage_IsCorrect(Storage)                               , oC_ErrorCode_ObjectNotCorrect     )
         && ErrorCondition( foundOnList == false                                        , oC_ErrorCode_ObjectAlreadyExist   )
         && ErrorCondition( oC_StorageMan_IsNameAvailable(oC_Storage_GetName(Storage))  , oC_ErrorCode_NameNotAvailable     )
            )
        {
            bool pushed = oC_List_PushBack(Storages,Storage,&Allocator);

            if( ErrorCondition( pushed , oC_ErrorCode_CannotAddObjectToList ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_StorageMan_RemoveStorage( oC_Storage_t Storage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOnVerification(&errorCode, oC_Module_StorageMan) )
    {
        bool removed = oC_List_RemoveAll(Storages,Storage);

        if( ErrorCondition( removed, oC_ErrorCode_ObjectNotFoundOnList ) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_List(oC_Storage_t) oC_StorageMan_GetList( void )
{
    return Storages;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_StorageMan_IsNameAvailable( const char * Name )
{
    bool available = false;

    if(oC_Module_IsTurnedOn(oC_Module_StorageMan) && isaddresscorrect(Name) && strlen(Name) > 0)
    {
        available = true;

        foreach( Storages, storage )
        {
            if(strcmp(Name,oC_Storage_GetName(storage)) == 0)
            {
                available = false;
                break;
            }
        }
    }

    return available;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Storage_t oC_StorageMan_GetStorage( const char * Name )
{
    oC_Storage_t foundStorage = NULL;

    if(oC_Module_IsTurnedOn(oC_Module_StorageMan) && isaddresscorrect(Name) && strlen(Name) > 0)
    {
        foreach( Storages, storage )
        {
            if(strcmp(Name,oC_Storage_GetName(storage)) == 0)
            {
                foundStorage = storage;
                break;
            }
        }
    }

    return foundStorage;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________



