/** ****************************************************************************************************************************************
 *
 * @brief       The file with the implementation of the CBIN file format
 * 
 * @file        oc_cbin.c
 *
 * @author     Patryk Kubiak - (Created on: 09.07.2017 16:46:51) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_cbin.h>
#include <oc_system_cfg.h>
#include <oc_mem_lld.h>
#include <oc_posix.h>

/** ========================================================================================================================================
 *
 *              The section with definitions
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITIONS_SECTION________________________________________________________________________

#if CFG_ENABLE_CBIN_DEBUG_LOGS == ON
#   define debuglog( LogType, ... )            kdebuglog( LogType, "cbin: " __VA_ARGS__ )
#else
#   define debuglog( ... )
#endif

#undef  _________________________________________DEFINITIONS_SECTION________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________



#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool             IsSignatureCorrect  ( oC_Program_Header_t * Header );
static oC_ErrorCode_t   PrepareSections     ( oC_CBin_File_t * File , oC_CBin_ProgramContext_t * ProgramContext );
static oC_ErrorCode_t   ConnectSystemCalls  ( char * ProgramName, void * SystemCalls, uint32_t SystemCallSize, uint32_t NumberOfCalls );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_SECTION__________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief       The function loads the CBIN file and prepares the sections
 * 
 * The function loads the CBIN file and prepares the sections. It also checks the
 * correctness of the file. If the file is not correct, then it will return the
 * error code. If the file is correct, then it will prepare the sections and
 * return the #oC_ErrorCode_None value.
 * 
 * @param       Path            The path to the CBIN file
 * @param       ProgramContext  The pointer to the structure with the program context
 * 
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * Possible codes:
 *      Error Code                           | Description
 * ------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                       | Operation success
 *  #oC_ErrorCode_ImplementError             | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress               | The `Path` address is not correct
 *  #oC_ErrorCode_OutputAddressNotInRAM      | `outFile` address is not in RAM
 *  #oC_ErrorCode_NoSuchFile                 | There is no such file
 *  #oC_ErrorCode_FileIsEmpty                | The file is empty
 *  #oC_ErrorCode_AllocationError            | There was an error during memory allocation
 *  #oC_ErrorCode_NotCBinFile                | The file is not CBIN file
 *  #oC_ErrorCode_ProgramHeaderTooSmall      | The program header is too small
 *  #oC_ErrorCode_SectionsDefinitionTooSmall | The sections definition is too small
 *  #oC_ErrorCode_SectionSizeNotCorrect      | The section size is not correct
 * 
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CBin_LoadFile( const char * Path , oC_CBin_File_t * outFile )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(Path)  , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(outFile)          , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        FILE * fp = fopen( Path, "r" );

        if( ErrorCondition( fp != NULL , oC_ErrorCode_NoSuchFile ) )
        {
            oC_MemorySize_t size = fsize(fp);

            if( ErrorCondition( size > 0 , oC_ErrorCode_FileIsEmpty ) )
            {
                outFile->FileSize   = size;
                outFile->FileBuffer = malloc( size, AllocationFlags_ZeroFill );

                if( ErrorCondition( outFile->FileBuffer != NULL , oC_ErrorCode_AllocationError ) )
                {
                    outFile->ProgramHeader = outFile->FileBuffer;

                    fread(outFile->FileBuffer,outFile->FileSize,1,fp);

                    if(
                        ErrorCondition( IsSignatureCorrect(outFile->ProgramHeader)                                           , oC_ErrorCode_NotCBinFile                  )
                     && ErrorCondition( outFile->ProgramHeader->HeaderSize               >= sizeof(oC_Program_Header_t)      , oC_ErrorCode_ProgramHeaderTooSmall        )
                     && ErrorCondition( outFile->ProgramHeader->SectionsDefinitionSize   >= sizeof(oC_Program_Sections_t)    , oC_ErrorCode_SectionsDefinitionTooSmall   )
                     && ErrorCondition( outFile->ProgramHeader->SectionsDefinitionSize   <  size                             , oC_ErrorCode_SectionSizeNotCorrect        )
                        )
                    {
                        if(outFile->ProgramHeader->HeaderSize > sizeof(oC_Program_Header_t))
                        {
                            debuglog( oC_LogType_Warning, "Program header (%s) is bigger than expected. Some functionality can be not recognized!", outFile->ProgramHeader->ProgramRegistration.Name );
                        }
                        if(outFile->ProgramHeader->Version != oC_VERSION)
                        {
                            debuglog( oC_LogType_Warning, "The program '%s' has been compiled for different ChocoOS version\n", outFile->ProgramHeader->ProgramRegistration.Name );
                        }
                        if(outFile->ProgramHeader->SectionsDefinitionSize > sizeof(oC_Program_Sections_t))
                        {
                            debuglog( oC_LogType_Warning, "Sections definition (%s) is bigger than expected. The program may not work correct\n", outFile->ProgramHeader->ProgramRegistration.Name);
                        }

                        oC_Program_Sections_t * sections = outFile->FileBuffer + (size - outFile->ProgramHeader->SectionsDefinitionSize);

                        if(
                            ErrorCondition( sections->text.Size       > 0 , oC_ErrorCode_SectionSizeNotCorrect   )
                         && ErrorCondition( sections->syscalls.Size   > 0 , oC_ErrorCode_SectionSizeNotCorrect   )
                            )
                        {
                            outFile->Sections.text.Address      = outFile->FileBuffer + sections->text.Offset;
                            outFile->Sections.text.Size         = sections->text.Size;
                            outFile->Sections.data.Address      = outFile->FileBuffer + sections->data.Offset;
                            outFile->Sections.data.Size         = sections->data.Size;
                            outFile->Sections.bss.Address       = outFile->FileBuffer + sections->bss.Offset;
                            outFile->Sections.bss.Size          = sections->bss.Size;
                            outFile->Sections.got.Address       = outFile->FileBuffer + sections->got.Offset;
                            outFile->Sections.got.Size          = sections->got.Size;
                            outFile->Sections.syscalls.Address  = outFile->FileBuffer + sections->syscalls.Offset;
                            outFile->Sections.syscalls.Size     = sections->syscalls.Size;
                            outFile->Sections.sections.Address  = sections;
                            outFile->Sections.sections.Size     = outFile->ProgramHeader->SectionsDefinitionSize;

                            errorCode = oC_ErrorCode_None;
                        }
                    }
                    if(oC_ErrorOccur(errorCode))
                    {
                        free( outFile->FileBuffer, 0 );
                        memset(outFile, 0, sizeof(oC_CBin_File_t));
                    }
                }
            }

            fclose(fp);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Unloads the CBIN file from memory
 * 
 * This function unloads the CBIN file from memory. After this operation, the `File`
 * structure will be empty.
 * 
 * @param File Pointer to the CBIN file structure
 * 
 * @return 
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * 
 * Possible codes:
 *      Error Code                           | Description
 * ------------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                       | Operation success
 *  #oC_ErrorCode_ImplementError             | There was unexpected error in implementation
 *  #oC_ErrorCode_AddressNotInRam            | The `File` address is not in RAM
 *  #oC_ErrorCode_FileNotLoaded              | The file is not loaded
 *  #oC_ErrorCode_ReleaseError               | There was an error during memory release
 * 
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_CBin_UnloadFile( oC_CBin_File_t * File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(File)                 , oC_ErrorCode_AddressNotInRam  )
     && ErrorCondition( File->FileBuffer != NULL    , oC_ErrorCode_FileNotLoaded    )
     && ErrorCondition( isram(File->FileBuffer)     , oC_ErrorCode_AddressNotInRam  )
     && ErrorCondition( free(File->FileBuffer,0)    , oC_ErrorCode_ReleaseError     )
        )
    {
        memset(File,0,sizeof(oC_CBin_File_t));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Prepares the CBIN file for execution
 * 
 * This function prepares the CBIN file for execution. It will check the file
 * integrity and will set the pointers to the sections.
 * 
 * @param File Pointer to the CBIN file structure
 * @param ProgramContext Pointer to the program context structure
 * 
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * 
 * Possible codes:
 *     Error Code                           | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_WrongAddress              | The `File` address is not correct
 *  #oC_ErrorCode_AddressNotInRam           | The `File` address is not in RAM
 *  #oC_ErrorCode_SizeNotCorrect            | The size of the section is not correct
 *  #oC_ErrorCode_RequiredSectionNotFound   | The required section is not found
 *  #oC_ErrorCode_SectionSizeNotCorrect     | The size of the section is not correct
 * 
 * @note
 * Please note, that more error codes can be returned by #PrepareSections function.
 */ 
//==========================================================================================================================================
oC_ErrorCode_t oC_CBin_PrepareForExecution( oC_CBin_File_t * File , oC_CBin_ProgramContext_t * ProgramContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isaddresscorrect(File)                                                  , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(ProgramContext)                                                   , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( isram(File->ProgramHeader)                                              , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( File->ProgramHeader->SystemCallDefinitionSize > sizeof(void*)           , oC_ErrorCode_SizeNotCorrect           )
     && ErrorCondition( isaddresscorrect(File->Sections.text.Address)                           , oC_ErrorCode_RequiredSectionNotFound  )
     && ErrorCondition( File->Sections.text.Size > 0                                            , oC_ErrorCode_RequiredSectionNotFound  )
     && ErrorCondition( isaddresscorrect(File->Sections.syscalls.Address)                       , oC_ErrorCode_RequiredSectionNotFound  )
     && ErrorCondition( File->Sections.syscalls.Size > 0                                        , oC_ErrorCode_RequiredSectionNotFound  )
     && ErrorCondition( isaddresscorrect(File->Sections.sections.Address)                       , oC_ErrorCode_RequiredSectionNotFound  )
     && ErrorCondition( File->Sections.sections.Size >= sizeof(oC_Program_Sections_t)           , oC_ErrorCode_SectionSizeNotCorrect    )
     && ErrorCondition( ProgramContext->Argc > 0                                                , oC_ErrorCode_ArgumentCountNotCorrect  )
     && ErrorCondition( isaddresscorrect(ProgramContext->Argv)                                  , oC_ErrorCode_WrongAddress             )
        )
    {
        ProgramContext->Process = oC_Process_New(ProgramContext->Priority,
                                                 File->ProgramHeader->ProgramRegistration.Name,
                                                 getcuruser(),
                                                 File->ProgramHeader->ProgramRegistration.HeapMapSize,
                                                 ProgramContext->StreamIn,
                                                 ProgramContext->StreamOut,
                                                 ProgramContext->StreamErr);

        if( ErrorCondition( ProgramContext->Process != NULL , oC_ErrorCode_CannotCreateProcess ) )
        {
            uint32_t mainOffset = (uint32_t)File->ProgramHeader->ProgramRegistration.MainFunction;

            ProgramContext->Program = kmalloc( sizeof(oC_Program_Registration_t), oC_Process_GetAllocator(ProgramContext->Process), AllocationFlags_ZeroFill );

            if(
                ErrorCondition( ProgramContext->Program != NULL             , oC_ErrorCode_AllocationError   )
             && ErrorCode     ( PrepareSections( File, ProgramContext )                                      )
                )
            {
                memcpy( ProgramContext->Program, &File->ProgramHeader->ProgramRegistration, sizeof(oC_Program_Registration_t) );

                void * functionReference = ProgramContext->SectionsBuffer;

                functionReference += mainOffset;

                ProgramContext->Program->Priority     = ProgramContext->Priority;
                ProgramContext->Program->MainFunction = functionReference;

                oC_MemMan_ChangeAllocatorOfAddress(File->FileBuffer, oC_Process_GetAllocator(ProgramContext->Process));

                errorCode = oC_ErrorCode_None;
            }
        }
        if(oC_ErrorOccur(errorCode))
        {
            if(ProgramContext->Program != NULL)
            {
                oC_SaveIfFalse( "Program", kfree(ProgramContext->Program,0), oC_ErrorCode_ReleaseError );
            }
            if(ProgramContext->Process != NULL)
            {
                oC_SaveIfFalse( "Process", oC_Process_Delete(&ProgramContext->Process), oC_ErrorCode_CannotDeleteProcess);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief Executes the prepared CBIN file
 * 
 * This function executes the prepared CBIN file.
 * 
 * @param ProgramContext Pointer to the program context structure
 * 
 * @return
 * Code of error from the #oC_ErrorCode_t type. If operation success, then it will
 * be set to #oC_ErrorCode_None value.
 * 
 * Possible codes:
 *     Error Code                           | Description
 * -----------------------------------------|---------------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_None                      | Operation success
 *  #oC_ErrorCode_ImplementError            | There was unexpected error in implementation
 *  #oC_ErrorCode_AddressNotInRam           | The `ProgramContext` address is not in RAM
 *  #oC_ErrorCode_ProgramNotPrepared        | The program is not prepared
 *  #oC_ErrorCode_ProcessNotCorrect         | The process is not correct
 *  #oC_ErrorCode_ProcessAlreadyStarted     | The process is already started
 * 
 * @note
 * Please note, that more error codes can be returned by #oC_Program_Exec function.
 */ 
//==========================================================================================================================================
oC_ErrorCode_t oC_CBin_Execute( oC_CBin_ProgramContext_t * ProgramContext )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(ProgramContext)                           , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( isram(ProgramContext->Program)                  , oC_ErrorCode_ProgramNotPrepared       )
     && ErrorCondition( oC_Process_IsCorrect(ProgramContext->Process)   , oC_ErrorCode_ProcessNotCorrect        )
     && ErrorCondition( ProgramContext->ProgramContext == NULL          , oC_ErrorCode_ProcessAlreadyStarted    )
        )
    {
        errorCode = oC_Program_Exec(ProgramContext->Program,ProgramContext->Process,ProgramContext->Argc,(const char**)ProgramContext->Argv,&ProgramContext->MainThread,&ProgramContext->ProgramContext);
    }

    return errorCode;
}

#undef  _________________________________________INTERFACE_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_SECTION______________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief checks the CBIN signature
 */
//==========================================================================================================================================
static bool IsSignatureCorrect( oC_Program_Header_t * Header )
{
    return strcmp(Header->MagicString,PROGRAM_HEADER_MAGIC_STRING) == 0;
}

//==========================================================================================================================================
/**
 * @brief allocates memory and prepares program sections to being executed
 */
//==========================================================================================================================================
static oC_ErrorCode_t PrepareSections( oC_CBin_File_t * File, oC_CBin_ProgramContext_t * ProgramContext )
{
    oC_ErrorCode_t          errorCode       = oC_ErrorCode_ImplementError;
    oC_Program_Sections_t * sectionsHeader  = File->Sections.sections.Address;
    oC_MemorySize_t         sectionsSize    = sectionsHeader->BinSize;

    ProgramContext->SectionsBuffer = kamalloc( sectionsSize, oC_Process_GetAllocator(ProgramContext->Process), AllocationFlags_ZeroFill, oC_MEM_LLD_STACK_ALIGNMENT );

    if(File->ProgramHeader->HeaderSize > sizeof(oC_Program_Header_t))
    {
        debuglog( oC_LogType_Warning, "Program header (%s) is bigger than expected. Some functionality can be not recognized!", File->ProgramHeader->ProgramRegistration.Name );
    }
    if(File->ProgramHeader->Version != oC_VERSION)
    {
        debuglog( oC_LogType_Warning, "The program '%s' has been compiled for different ChocoOS version\n", File->ProgramHeader->ProgramRegistration.Name );
    }
    if(File->ProgramHeader->SectionsDefinitionSize > sizeof(oC_Program_Sections_t))
    {
        debuglog( oC_LogType_Warning, "Sections definition (%s) is bigger than expected. The program may not work correct\n", File->ProgramHeader->ProgramRegistration.Name);
    }

    if( ErrorCondition( ProgramContext->SectionsBuffer != NULL, oC_ErrorCode_AllocationError ) )
    {
        ProgramContext->Sections.text.Address       = ProgramContext->SectionsBuffer + sectionsHeader->text.Offset;
        ProgramContext->Sections.text.Size          = sectionsHeader->text.Size;
        ProgramContext->Sections.bss.Address        = ProgramContext->SectionsBuffer + sectionsHeader->bss.Offset;
        ProgramContext->Sections.bss.Size           = sectionsHeader->bss.Size;
        ProgramContext->Sections.data.Address       = ProgramContext->SectionsBuffer + sectionsHeader->data.Offset;
        ProgramContext->Sections.data.Size          = sectionsHeader->data.Size;
        ProgramContext->Sections.syscalls.Address   = ProgramContext->SectionsBuffer + sectionsHeader->syscalls.Offset;
        ProgramContext->Sections.syscalls.Size      = sectionsHeader->syscalls.Size;
        ProgramContext->Sections.got.Address        = ProgramContext->SectionsBuffer + sectionsHeader->got.Offset;
        ProgramContext->Sections.got.Size           = sectionsHeader->got.Size;

        memcpy( ProgramContext->SectionsBuffer, File->FileBuffer, sectionsSize );

        uint32_t * gotEntries = ProgramContext->Sections.got.Address;
        uint32_t   leftSize   = ProgramContext->Sections.got.Size;

        while(leftSize > 0)
        {
            (*gotEntries) += (uint32_t)ProgramContext->SectionsBuffer;
            leftSize -= sizeof(uint32_t);

            gotEntries++;
        }

        memset( ProgramContext->Sections.bss.Address, 0, ProgramContext->Sections.bss.Size );

        if( ErrorCode( ConnectSystemCalls( File->ProgramHeader->ProgramRegistration.Name , ProgramContext->Sections.syscalls.Address, File->ProgramHeader->SystemCallDefinitionSize, File->ProgramHeader->NumberOfSystemCalls ) ))
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief connects available system calls to the system calls buffer
 */
//==========================================================================================================================================
static oC_ErrorCode_t ConnectSystemCalls( char * ProgramName, void * SystemCalls, uint32_t SystemCallSize, uint32_t NumberOfCalls )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_None;

    for( uint32_t i = 0; i < NumberOfCalls; i++ , SystemCalls += SystemCallSize )
    {
        oC_SystemCall_t * systemCall = SystemCalls;

        systemCall->CallAddress = NULL;

#define ADD_FUNCTION(NAME)      if(strcmp(#NAME,systemCall->Name) == 0) \
                                { \
                                    systemCall->CallAddress = NAME;\
                                }

        POSIX_FUNCTIONS_LIST(ADD_FUNCTION);

#undef ADD_FUNCTION

        if(systemCall->CallAddress == NULL)
        {
            debuglog( oC_LogType_Error, "Unknown system call '%s'\n", systemCall->Name );
            errorCode = oC_ErrorCode_UnknownSystemCall;
            break;
        }
    }

    return errorCode;
}

#undef  _________________________________________LOCAL_SECTION______________________________________________________________________________


