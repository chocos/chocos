/** ****************************************************************************************************************************************
 *
 * @file       oc_vfs.c
 *
 * @brief      The file with sources for the VFS
 *
 * @author     Patryk Kubiak - (Created on: 2 06 2015 19:07:17)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_vfs.h>
#include <oc_filesystem_list.h>
#include <oc_list.h>
#include <oc_string.h>
#include <oc_processman.h>
#include <oc_ifnot.h>
#include <oc_storage.h>
#include <oc_storageman.h>

#define IsDirSign(Char)             ( (Char) == '/' || (Char) == '\\' )
#define IsAbsolutePath(Path)        ( IsDirSign((Path)[0]) )

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

typedef struct _MountedEntry_t
{
    oC_FileSystem_t         FileSystem;
    oC_FileSystem_Context_t Context;
    char *                  MountedPoint;
    oC_Storage_t            Storage;
    oC_File_t               VirtualStorageFile;
} * MountedEntry_t;

typedef struct _OpenedFileEntry_t
{
    union
    {
        oC_File_t       File;
        oC_Dir_t        Dir;
    };

    MountedEntry_t  MountedEntry;
    oC_Process_t    Process;
} * OpenedFileEntry_t;

typedef enum
{
    Cd_NewDir = 0,
    Cd_NoMove = 1,
    Cd_DirUp  = 2
} Cd_t;

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________

static MountedEntry_t GetMountedFileSystem( const char * Path , bool WithParentDirs );
static const char * GetPwd( void );
static uint32_t ConvertRelativeToAbsolute( const char * Relative , char * outAbsolute , uint32_t AbsoluteSize , bool TreatAsDir );
static char * ConvertToAbsoluteWithAllocation(const char * Relative , bool TreatAsDir );
static bool CheckIfDirExists( const char * Absolute );
static oC_Storage_t         GetStorageFromPath            ( const char * DevicePath, oC_File_t * outVirtualStorageFile );
static oC_Storage_t         NewVirtualStorage             ( const char * DevicePath, oC_File_t * outVirtualStorageFile );
static bool                 DeleteVirtualStorage          ( oC_Storage_t VirtualStorage , oC_File_t VirtualStorageFile );
static oC_ErrorCode_t       ReadVirtualStorageSectors     ( void * Context, oC_SectorNumber_t SectorNumber, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
static oC_ErrorCode_t       WriteVirtualStorageSectors    ( void * Context, oC_SectorNumber_t SectorNumber, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
static oC_ErrorCode_t       EraseVirtualStorageSectors    ( void * Context, oC_SectorNumber_t SectorNumber, oC_SectorNumber_t NumberOfSectors, oC_Time_t Timeout );

#undef  _________________________________________LOCAL_PROTOTYPES_SECTION___________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

static bool                             ModuleEnabledFlag   = false;
static oC_List(oC_FileSystem_t)         FileSystemsList     = NULL;
static oC_List(MountedEntry_t)          MountedEntries      = NULL;
static oC_List(OpenedFileEntry_t)       OpenedFilesEntries  = NULL;
static const oC_Allocator_t Allocator = {
             .Name = "Virtual File System"
};
static const oC_Storage_DiskInterface_t VirtualStorageInterface = {
                .ReadSectors    = ReadVirtualStorageSectors ,
                .WriteSectors   = WriteVirtualStorageSectors ,
                .EraseSectors   = EraseVirtualStorageSectors
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________


//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == false , oC_ErrorCode_ModuleIsTurnedOn))
    {
        FileSystemsList   = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);
        MountedEntries    = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);
        OpenedFilesEntries= oC_List_New(&Allocator,AllocationFlags_CanWait1Second);

        if(FileSystemsList && MountedEntries && OpenedFilesEntries)
        {
            ModuleEnabledFlag = true;
            errorCode         = oC_ErrorCode_None;

#define ADD_FILE_SYSTEM(FileSystemName)         oC_AssignErrorCode(&errorCode , oC_VirtualFileSystem_AddFileSystem(&FileSystemName));
#define DONT_ADD(FileSystemName)
            CFG_LIST_FILE_SYSTEMS(ADD_FILE_SYSTEM,DONT_ADD)
#undef ADD_FILE_SYSTEM
#undef DONT_ADD
#define MOUNT(FileSystemName,DeviceName,MountPoint)        \
                                            if(CheckIfDirExists(MountPoint)==false) { oC_AssignErrorCode(&errorCode , oC_VirtualFileSystem_mkdir(MountPoint)); }\
                                            oC_AssignErrorCode(&errorCode , oC_VirtualFileSystem_mount(FileSystemName,DeviceName,MountPoint));
            CFG_LIST_MOUNTED_FILE_SYSTEMS(MOUNT)
#undef MOUNT
            ErrorCode( oC_VirtualFileSystem_mkdir( CFG_PATH_DEFAULT_MEDIA_MOUNT_PATH ) );
        }
        else
        {
            if(oC_List_IsCorrect(FileSystemsList) && !oC_List_Delete(FileSystemsList,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("VFS: Cannot delete FileSystemsList" , oC_ErrorCode_ReleaseError);
            }
            if(oC_List_IsCorrect(MountedEntries) && !oC_List_Delete(MountedEntries,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("VFS: Cannot delete MountedFileSystems" , oC_ErrorCode_ReleaseError);
            }
            if(oC_List_IsCorrect(OpenedFilesEntries) && !oC_List_Delete(OpenedFilesEntries,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("VFS: Cannot delete OpenedFilesEntries" , oC_ErrorCode_ReleaseError);
            }

            errorCode = oC_ErrorCode_AllocationError;
        }
    }


    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        ModuleEnabledFlag = false;

        oC_List_Foreach(MountedEntries,mountedFileSystem)
        {
            if(kfree(mountedFileSystem->MountedPoint,AllocationFlags_CanWaitForever)==false)
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }

            if(kfree(mountedFileSystem,AllocationFlags_CanWaitForever)==false)
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }

        oC_List_Foreach(OpenedFilesEntries,openedFileEntry)
        {
            if(kfree(openedFileEntry,AllocationFlags_CanWaitForever)==false)
            {
                errorCode = oC_ErrorCode_ReleaseError;
            }
        }

        bool fileSystemsListDeleted = oC_List_Delete(FileSystemsList,AllocationFlags_CanWaitForever);
        bool mountedListDeleted     = oC_List_Delete(MountedEntries,AllocationFlags_CanWaitForever);
        bool openedDeleted          = oC_List_Delete(OpenedFilesEntries,AllocationFlags_CanWaitForever);
        bool allReleased            = oC_MemMan_FreeAllMemoryOfAllocator(&Allocator);

        if(fileSystemsListDeleted && mountedListDeleted && openedDeleted && allReleased)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_AddFileSystem( oC_FileSystem_t FileSystem )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        bool pushed = oC_List_PushBack(FileSystemsList,FileSystem,&Allocator);

        if(pushed)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotAddObjectToList;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_RemoveFileSystem( oC_FileSystem_t FileSystem )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet))
    {
        bool removed = oC_List_RemoveAll(FileSystemsList,FileSystem);

        if(removed)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotRemoveObjectFromList;
        }
    }

    return errorCode;
}


//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_mount( const char * FileSystem , const char * DevicePath , const char * Path )
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true    , oC_ErrorCode_ModuleNotStartedYet) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)       , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(FileSystem) , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , DevicePath == NULL
                                            || isaddresscorrect(DevicePath) , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) >= 1            , oC_ErrorCode_SizeNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Path[0] == '/'               , oC_ErrorCode_PathNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Path[strlen(Path)-1] == '/'  , oC_ErrorCode_PathNotCorrect)
        )
    {
        oC_FileSystem_t fileSystemEntry   = NULL;
        char *          absolute          = ConvertToAbsoluteWithAllocation(Path,true);
        bool            storageRequired   = DevicePath != NULL && strlen(DevicePath) > 0;

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t  mountedFileSystem = GetMountedFileSystem(absolute,false);
            MountedEntry_t  mountedEntry      = kmalloc(sizeof(struct _MountedEntry_t),&Allocator,AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill);
            bool            dirExists         = CheckIfDirExists(absolute);

            oC_List_Foreach(FileSystemsList,fileSystem)
            {
                if(strcmp(FileSystem,fileSystem->Name) == 0)
                {
                    fileSystemEntry = fileSystem;
                    break;
                }
            }

            if( ErrorCondition( mountedEntry, oC_ErrorCode_AllocationError ) )
            {
                if( storageRequired )
                {
                    mountedEntry->Storage = GetStorageFromPath( DevicePath, &mountedEntry->VirtualStorageFile );
                }

                if(
                    ErrorCondition( fileSystemEntry   != NULL                    , oC_ErrorCode_ObjectNotFoundOnList    )
                 && ErrorCondition( mountedFileSystem == NULL                    , oC_ErrorCode_PathAlreadyUsed         )
                 && ErrorCondition( dirExists                                    , oC_ErrorCode_NoSuchFile              )
                 && ErrorCondition( isaddresscorrect(fileSystemEntry->init)      , oC_ErrorCode_FileSystemNotCorrect    )
                 && ErrorCondition( mountedEntry != NULL                         , oC_ErrorCode_AllocationError         )
                 && ErrorCode     ( fileSystemEntry->init(&mountedEntry->Context, mountedEntry->Storage)                )
                    )
                {
                    oC_UInt_t pathLength       = strlen(absolute) + 1;
                    mountedEntry->FileSystem   = fileSystemEntry;
                    mountedEntry->MountedPoint = kmalloc(pathLength ,&Allocator , AllocationFlags_CanWait1Second);

                    if(oC_AssignErrorCodeIfFalse(&errorCode , mountedEntry->MountedPoint , oC_ErrorCode_AllocationError ))
                    {
                        bool      pushed    = false;
                        oC_UInt_t pushIndex = 0;

                        if( storageRequired == false || ErrorCondition( mountedEntry->Storage != NULL, oC_ErrorCode_NoSuchFile ) )
                        {
                            strcpy(mountedEntry->MountedPoint,absolute);

                            oC_List_Foreach(MountedEntries,mountedEntryForeach)
                            {
                                if(strlen(mountedEntryForeach->MountedPoint) <= pathLength )
                                {
                                    break;
                                }
                                else
                                {
                                    pushIndex++;
                                }
                            }

                            pushed = oC_List_InsertBefore(MountedEntries,mountedEntry,pushIndex,&Allocator);

                            if(pushed)
                            {
                                errorCode = oC_ErrorCode_None;
                            }
                            else
                            {
                                errorCode = oC_ErrorCode_CannotAddObjectToList;
                            }
                        }
                    }
                }
            }

            if(oC_ErrorOccur(errorCode))
            {
                if(mountedEntry)
                {
                    if(mountedEntry->VirtualStorageFile != NULL && DeleteVirtualStorage(mountedEntry->Storage, mountedEntry->VirtualStorageFile))
                    {
                        oC_SaveError("Cannot delete virtual storage", oC_ErrorCode_ReleaseError);
                    }
                    if(mountedEntry->MountedPoint && !kfree(mountedEntry->MountedPoint,AllocationFlags_CanWaitForever))
                    {
                        oC_SaveError("Cannot release mounted point",oC_ErrorCode_ReleaseError);
                    }
                    if(!kfree(mountedEntry,AllocationFlags_CanWaitForever))
                    {
                        oC_SaveError("Cannot release mounted entry",oC_ErrorCode_ReleaseError);
                    }
                }
            }

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-mount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
    }


    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_umount( const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) > 1          , oC_ErrorCode_SizeNotCorrect) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Path[0] == '/'            , oC_ErrorCode_PathNotCorrect)
        )
    {
        oC_FileSystem_t fileSystem   = NULL;
        char *          absolute     = ConvertToAbsoluteWithAllocation(Path,true);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t  mountedEntry = GetMountedFileSystem(absolute,false);

            if(oC_AssignErrorCodeIfFalse(&errorCode,mountedEntry != NULL , oC_ErrorCode_NoFileSystemMounted))
            {
                fileSystem   = mountedEntry->FileSystem;
                errorCode    = oC_ErrorCode_None;

                oC_List_Foreach(OpenedFilesEntries,openedEntry)
                {
                    if(openedEntry->MountedEntry==mountedEntry)
                    {
                        oC_AssignErrorCode(&errorCode , oC_VirtualFileSystem_fclose(openedEntry->File));
                    }
                }

                oC_AssignErrorCodeIfFalse(&errorCode , mountedEntry->VirtualStorageFile == NULL
                                                    || DeleteVirtualStorage(mountedEntry->Storage,mountedEntry->VirtualStorageFile)
                                                                                                                          , oC_ErrorCode_CannotDeleteObject);
                oC_AssignErrorCodeIfFalse(&errorCode , kfree(mountedEntry->MountedPoint , AllocationFlags_CanWaitForever) , oC_ErrorCode_ReleaseError );
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(fileSystem->deinit)                               , oC_ErrorCode_FileSystemNotCorrect );
                oC_AssignErrorCode(       &errorCode , fileSystem->deinit(mountedEntry->Context) );
                oC_AssignErrorCodeIfFalse(&errorCode , kfree(mountedEntry , AllocationFlags_CanWaitForever)               , oC_ErrorCode_ReleaseError );

                bool removed = oC_List_RemoveAll(MountedEntries,mountedEntry);

                oC_AssignErrorCodeIfFalse(&errorCode , removed , oC_ErrorCode_CannotRemoveObjectFromList );

            }
            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-umount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }

    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_fopen( oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outFile)            , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) > 1          , oC_ErrorCode_SizeNotCorrect)
        )
    {
        char *         absolute     = ConvertToAbsoluteWithAllocation(Path,false);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t mountedEntry = GetMountedFileSystem(absolute,true);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , mountedEntry != NULL                              , oC_ErrorCode_NoFileSystemMounted ) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem)        , oC_ErrorCode_FileSystemNotCorrect) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->fopen) , oC_ErrorCode_NoSupportedByFileSystem)
                )
            {
                oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);

                errorCode = mountedEntry->FileSystem->fopen( mountedEntry->Context , outFile , &absolute[mountedPathLength-1] , Mode , Attributes );

                if(!oC_ErrorOccur(errorCode))
                {
                    OpenedFileEntry_t openedEntry = smartalloc(sizeof(struct _OpenedFileEntry_t),AllocationFlags_CanWait1Second);

                    if(openedEntry)
                    {
                        openedEntry->File           = *outFile;
                        openedEntry->MountedEntry   = mountedEntry;
                        openedEntry->Process        = oC_ProcessMan_GetCurrentProcess();

                        bool pushed = oC_List_PushBack(OpenedFilesEntries,openedEntry,&Allocator);

                        if(!pushed)
                        {
                            if(!smartfree(openedEntry,sizeof(struct _OpenedFileEntry_t),AllocationFlags_CanWaitForever))
                            {
                                oC_SaveError("VFS: fopen " , oC_ErrorCode_ReleaseError);
                            }

                            errorCode = oC_ErrorCode_CannotAddObjectToList;
                        }
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_AllocationError;
                    }
                }
            }

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-umount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_fclose( oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File)
            {
                MountedEntry_t mountedEntry = openedEntry->MountedEntry;

                if(
                    oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem)         , oC_ErrorCode_FileSystemNotCorrect) &&
                    oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->fclose) , oC_ErrorCode_NoSupportedByFileSystem)
                    )
                {
                    errorCode = mountedEntry->FileSystem->fclose(mountedEntry->Context,File);
                }

                if(!smartfree(openedEntry,sizeof(struct _OpenedFileEntry_t),AllocationFlags_CanWaitForever))
                {
                    oC_SaveError("VFS: fclose " , errorCode);
                }

                bool removed = oC_List_RemoveAll(OpenedFilesEntries,openedEntry);

                oC_AssignErrorCodeIfFalse(&errorCode , removed , oC_ErrorCode_CannotRemoveObjectFromList);
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_fcloseFromProcess( oC_Process_t Process )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_None;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->Process == Process )
            {
                oC_AssignErrorCode(&errorCode , oC_VirtualFileSystem_fclose(openedEntry->File));
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_fread( oC_File_t File , void * outBuffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->fread) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->fread(openedEntry->MountedEntry->Context,File,outBuffer,Size);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_fwrite( oC_File_t File , const void * Buffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->fwrite) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->fwrite(openedEntry->MountedEntry->Context,File,Buffer,Size);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_lseek( oC_File_t File , uint32_t Offset )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->lseek) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->lseek(openedEntry->MountedEntry->Context,File,Offset);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_ioctl( oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->ioctl) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->ioctl(openedEntry->MountedEntry->Context,File,Command,Pointer);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_sync( oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->sync) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->sync(openedEntry->MountedEntry->Context,File);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_getc( char * outCharacter , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->Getc) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->Getc(openedEntry->MountedEntry->Context,outCharacter,File);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_putc( char Character , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->Putc) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->Putc(openedEntry->MountedEntry->Context,Character,File);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
int32_t oC_VirtualFileSystem_tell( oC_File_t File )
{
    int32_t bytes = 0;

    if(ModuleEnabledFlag)
    {
        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(isaddresscorrect(openedEntry->MountedEntry->FileSystem->tell))
                {
                    bytes = openedEntry->MountedEntry->FileSystem->tell(openedEntry->MountedEntry->Context,File);
                    break;
                }
            }
        }
    }

    return bytes;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_VirtualFileSystem_eof( oC_File_t File )
{
    bool eof = false;

    if(ModuleEnabledFlag)
    {
        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(isaddresscorrect(openedEntry->MountedEntry->FileSystem->eof))
                {
                    eof = openedEntry->MountedEntry->FileSystem->eof(openedEntry->MountedEntry->Context,File);
                    break;
                }
            }
        }
    }

    return eof;
}

//==========================================================================================================================================
//==========================================================================================================================================
uint32_t oC_VirtualFileSystem_size( oC_File_t File )
{
    uint32_t bytes = 0;

    if(ModuleEnabledFlag)
    {
        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(isaddresscorrect(openedEntry->MountedEntry->FileSystem->size))
                {
                    bytes = openedEntry->MountedEntry->FileSystem->size(openedEntry->MountedEntry->Context,File);
                    break;
                }
            }
        }
    }

    return bytes;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_flush( oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->fflush) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->fflush(openedEntry->MountedEntry->Context,File);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_error( oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->File == File )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->error) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->error(openedEntry->MountedEntry->Context,File);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_opendir( oC_Dir_t * outDir , const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outDir)             , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) >= 1         , oC_ErrorCode_SizeNotCorrect)
        )
    {
        char *         absolute     = ConvertToAbsoluteWithAllocation(Path,true);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t mountedEntry = GetMountedFileSystem(absolute,true);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , mountedEntry != NULL                                , oC_ErrorCode_NoFileSystemMounted ) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem)          , oC_ErrorCode_FileSystemNotCorrect) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->opendir) , oC_ErrorCode_NoSupportedByFileSystem)
                )
            {
                oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);

                errorCode = mountedEntry->FileSystem->opendir( mountedEntry->Context , outDir , &absolute[mountedPathLength-1] );

                if(!oC_ErrorOccur(errorCode))
                {
                    OpenedFileEntry_t openedEntry = smartalloc(sizeof(struct _OpenedFileEntry_t),AllocationFlags_CanWait1Second);

                    if(openedEntry)
                    {
                        openedEntry->Dir            = *outDir;
                        openedEntry->MountedEntry   = mountedEntry;
                        openedEntry->Process        = oC_ProcessMan_GetCurrentProcess();

                        bool pushed = oC_List_PushBack(OpenedFilesEntries,openedEntry,&Allocator);

                        if(!pushed)
                        {
                            if(!smartfree(openedEntry,sizeof(struct _OpenedFileEntry_t),AllocationFlags_CanWaitForever))
                            {
                                oC_SaveError("VFS: opendir " , oC_ErrorCode_ReleaseError);
                            }

                            errorCode = oC_ErrorCode_CannotAddObjectToList;
                        }
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_AllocationError;
                    }
                }
            }

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-umount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_closedir( oC_Dir_t Dir )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->Dir == Dir)
            {
                MountedEntry_t mountedEntry = openedEntry->MountedEntry;

                if(
                    oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem)           , oC_ErrorCode_FileSystemNotCorrect) &&
                    oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->closedir) , oC_ErrorCode_NoSupportedByFileSystem)
                    )
                {
                    errorCode = mountedEntry->FileSystem->closedir(mountedEntry->Context,Dir);
                }

                if(!smartfree(openedEntry,sizeof(struct _OpenedFileEntry_t),AllocationFlags_CanWaitForever))
                {
                    oC_SaveError("VFS: closedir " , errorCode);
                }

                bool removed = oC_List_RemoveAll(OpenedFilesEntries,openedEntry);

                oC_AssignErrorCodeIfFalse(&errorCode , removed , oC_ErrorCode_CannotRemoveObjectFromList);
                break;
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_readdir( oC_Dir_t Dir , oC_FileInfo_t * outFileInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        errorCode = oC_ErrorCode_NoSuchFile;

        oC_List_Foreach(OpenedFilesEntries,openedEntry)
        {
            if(openedEntry->Dir == Dir )
            {
                if(oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(openedEntry->MountedEntry->FileSystem->readdir) , oC_ErrorCode_NoSupportedByFileSystem))
                {
                    errorCode = openedEntry->MountedEntry->FileSystem->readdir(openedEntry->MountedEntry->Context,Dir,outFileInfo);
                    break;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_stat( const char * Path , oC_FileInfo_t * outFileInfo)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outFileInfo)        , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) >= 1         , oC_ErrorCode_SizeNotCorrect)
        )
    {
        char * absolute = ConvertToAbsoluteWithAllocation(Path,true);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t mountedEntry = GetMountedFileSystem(absolute,true);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry)                   , oC_ErrorCode_NoFileSystemMounted) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->stat) , oC_ErrorCode_NoSupportedByFileSystem)
                )
            {
                oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);
                errorCode = mountedEntry->FileSystem->stat(mountedEntry->Context,&absolute[mountedPathLength],outFileInfo);
            }

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-umount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_unlink( const char * Path)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) >= 1         , oC_ErrorCode_SizeNotCorrect)
        )
    {
        char * absolute = ConvertToAbsoluteWithAllocation(Path,true);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t mountedEntry = GetMountedFileSystem(absolute,true);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry)                   , oC_ErrorCode_NoFileSystemMounted) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->unlink) , oC_ErrorCode_NoSupportedByFileSystem)
                )
            {
                oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);
                errorCode = mountedEntry->FileSystem->unlink(mountedEntry->Context,&absolute[mountedPathLength]);
            }

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-umount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_rename( const char * OldName , const char * NewName)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if (
        ErrorCondition( isaddresscorrect(OldName)   , oC_ErrorCode_WrongAddress         )    
     && ErrorCondition( isaddresscorrect(NewName)   , oC_ErrorCode_WrongAddress         )
     && ErrorCondition( strlen(OldName) > 0         , oC_ErrorCode_StringIsEmpty        )
     && ErrorCondition( strlen(NewName) > 0         , oC_ErrorCode_StringIsEmpty        )
        )
    {
        MountedEntry_t mountedEntry = GetMountedFileSystem(OldName,true);

        if(
            ErrorCondition( isaddresscorrect(mountedEntry)                     , oC_ErrorCode_NoFileSystemMounted) 
         && ErrorCondition( isaddresscorrect(mountedEntry->FileSystem->rename) , oC_ErrorCode_NoSupportedByFileSystem)
            )
        {
            oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);
            errorCode = mountedEntry->FileSystem->rename(mountedEntry->Context,&OldName[mountedPathLength],&NewName[mountedPathLength]);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_chmod( const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) >= 1         , oC_ErrorCode_SizeNotCorrect)
        )
    {
        char * absolute = ConvertToAbsoluteWithAllocation(Path,true);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t mountedEntry = GetMountedFileSystem(absolute,true);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry)                    , oC_ErrorCode_NoFileSystemMounted) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->chmod) , oC_ErrorCode_NoSupportedByFileSystem)
                )
            {
                oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);
                errorCode = mountedEntry->FileSystem->chmod(mountedEntry->Context,&absolute[mountedPathLength],Attributes,Mask);
            }

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-umount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_utime( const char * Path , oC_Timestamp_t Timestamp )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) >= 1         , oC_ErrorCode_SizeNotCorrect)
        )
    {
        char * absolute = ConvertToAbsoluteWithAllocation(Path,true);

        if(oC_AssignErrorCodeIfFalse(&errorCode , isram(absolute) , oC_ErrorCode_AllocationError ))
        {
            MountedEntry_t mountedEntry = GetMountedFileSystem(absolute,true);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry)                    , oC_ErrorCode_NoFileSystemMounted) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->utime) , oC_ErrorCode_NoSupportedByFileSystem)
                )
            {
                oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);
                errorCode = mountedEntry->FileSystem->utime(mountedEntry->Context,&absolute[mountedPathLength],Timestamp);
            }

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-umount: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_mkdir( const char * Path)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , strlen(Path) >= 1         , oC_ErrorCode_SizeNotCorrect)
        )
    {
        char *    absolutePath      = ConvertToAbsoluteWithAllocation(Path,true);

        if(absolutePath)
        {
            MountedEntry_t mountedEntry = GetMountedFileSystem(absolutePath,true);

            if(
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry)                    , oC_ErrorCode_NoFileSystemMounted) &&
                oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry->FileSystem->mkdir) , oC_ErrorCode_NoSupportedByFileSystem)
                )
            {
                oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);

                errorCode = mountedEntry->FileSystem->mkdir(mountedEntry->Context,&absolutePath[mountedPathLength-1]);
            }

            ifnot(ksmartfree(absolutePath,strlen(absolutePath)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-mkdir: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }

    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_chdir( const char * Path)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag == true , oC_ErrorCode_ModuleNotStartedYet ))
    {
        MountedEntry_t mountedEntry = GetMountedFileSystem(Path,true);

        if(
            oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(mountedEntry)                    , oC_ErrorCode_NoFileSystemMounted)
            )
        {
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_getcwd( char * outBuffer , uint32_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag            , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outBuffer)             , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size > 0                     , oC_ErrorCode_SizeNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isarrayinram(outBuffer,Size) , oC_ErrorCode_ArraySizeTooBig )

        )
    {

    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_getpwd( char * outPwd , uint32_t Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag            , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outPwd)                , oC_ErrorCode_OutputAddressNotInRAM ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , Size > 0                     , oC_ErrorCode_SizeNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isarrayinram(outPwd,Size)    , oC_ErrorCode_ArraySizeTooBig )

        )
    {
        const char * pwd = GetPwd();

        if( strlen(pwd) > Size )
        {
            errorCode = oC_ErrorCode_OutputArrayToSmall;
        }
        else
        {
            strcpy(outPwd,pwd);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_VirtualFileSystem_ConvertRelativeToAbsolute( const char * Relative , char * outAbsolute , uint32_t * AbsoluteSize , bool TreatAsDir )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , ModuleEnabledFlag            , oC_ErrorCode_ModuleNotStartedYet ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Relative)   , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(AbsoluteSize)          , oC_ErrorCode_OutputAddressNotInRAM )
        )
    {
        *AbsoluteSize   = ConvertRelativeToAbsolute(Relative,outAbsolute,*AbsoluteSize,TreatAsDir);
        errorCode       = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_VirtualFileSystem_DirExists( const char * Path )
{
    bool exists = false;

    if(ModuleEnabledFlag && isaddresscorrect(Path))
    {
        char * absolute = ConvertToAbsoluteWithAllocation(Path,true);

        if(isram(absolute))
        {
            exists = CheckIfDirExists(absolute);

            ifnot(ksmartfree(absolute,strlen(absolute)+1,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("vfs-dir-exists: cannot release temp buffer ",oC_ErrorCode_ReleaseError);
            }
        }
        else
        {
            oC_SaveError("vfs-dir-exists: cannot allocate memory for temp buffer",oC_ErrorCode_AllocationError);
        }
    }

    return exists;
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION___________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static MountedEntry_t GetMountedFileSystem( const char * Path , bool WithParentDirs )
{
    MountedEntry_t  mountedEntry = NULL;
    oC_UInt_t       pathLength   = strlen(Path);

    oC_List_Foreach(MountedEntries,mounted)
    {
        oC_UInt_t mountPointLength = strlen(mounted->MountedPoint);

        if(
            (WithParentDirs && pathLength >= mountPointLength && strncmp(mounted->MountedPoint,Path,mountPointLength)==0) ||
            (!WithParentDirs && pathLength == mountPointLength && strcmp(mounted->MountedPoint,Path)==0)
            )
        {
            mountedEntry = mounted;
            break;
        }
    }

    return mountedEntry;
}

//==========================================================================================================================================
//==========================================================================================================================================
static const char * GetPwd( void )
{
    return oC_Process_GetPwd(oC_ProcessMan_GetCurrentProcess());
}

//==========================================================================================================================================
//==========================================================================================================================================
static uint32_t ConvertRelativeToAbsolute( const char * Relative , char * outAbsolute , uint32_t AbsoluteSize , bool TreatAsDir )
{
    uint32_t requiredSize       = 0;
    uint32_t relativeLength     = strlen(Relative);
    const char * pwd            = GetPwd();
    uint32_t pwdLength          = strlen(pwd);
    bool     lastSlashPushed    = false;

#define _putc(c)        if( ((c) == '/' && !lastSlashPushed) || ((c) != '/') ) {if(outAbsolute != NULL && requiredSize < AbsoluteSize) { outAbsolute[requiredSize] = c; } requiredSize++; lastSlashPushed = (c) == '/';}

    if(relativeLength>0 && pwdLength > 0)
    {
        uint32_t tempBufferLength = pwdLength + relativeLength;

        if(!IsDirSign(pwd[pwdLength-1]))
        {
            /* Plus 1 for '/' sign */
            tempBufferLength++;
        }

        if(IsAbsolutePath(Relative))
        {
            tempBufferLength = relativeLength;
        }

        if(TreatAsDir && !IsDirSign(Relative[relativeLength-1]))
        {
            /* For a '/' sign at the end of the string */
            tempBufferLength++;
        }

        char * tempBuffer = ksmartalloc(tempBufferLength+1, &Allocator, AllocationFlags_CanWait1Second);

        if(oC_SaveIfFalse("VFS::ConvertRelativeToAbsolute - Cannot allocate memory - ", tempBuffer != NULL, oC_ErrorCode_AllocationError))
        {
            if(IsAbsolutePath(Relative))
            {
                strcpy(tempBuffer,Relative);
            }
            else
            {
                strncpy(tempBuffer,pwd,pwdLength);

                if(!IsDirSign(pwd[pwdLength-1]))
                {
                    tempBuffer[pwdLength] = '/';
                    strncpy(&tempBuffer[pwdLength+1],Relative,relativeLength);
                }
                else
                {
                    strncpy(&tempBuffer[pwdLength],Relative,relativeLength);
                }

            }

            if(TreatAsDir && !IsDirSign(Relative[relativeLength-1]))
            {
                /* For a '/' sign at the end of the string */
                tempBuffer[tempBufferLength-1] = '/';
            }

            tempBuffer[tempBufferLength] = 0;

            uint32_t dirsToSkip     = 0;
            uint32_t foundDots      = 0;
            bool     foundFileName  = TreatAsDir;

            for(uint32_t tempBufferIndex = tempBufferLength - 1; tempBufferIndex > 0 ; tempBufferIndex--)
            {
                if(IsDirSign(tempBuffer[tempBufferIndex]))
                {
                    if(foundFileName)
                    {
                        if( dirsToSkip == 0)
                        {
                            _putc('/');
                        }

                        if( dirsToSkip > 0 )
                        {
                            dirsToSkip--;
                        }
                    }
                    else
                    {
                        if(foundDots >= 2)
                        {
                            dirsToSkip++;
                        }
                    }
                    foundFileName = false;
                    foundDots     = 0;
                }
                else if(!foundFileName && tempBuffer[tempBufferIndex] == '.')
                {
                    foundDots++;
                }
                else
                {
                    while(foundDots>0)
                    {
                        if( dirsToSkip == 0 )
                        {
                            _putc('.');
                        }
                        foundDots--;
                    }

                    if( dirsToSkip == 0 )
                    {
                        _putc(tempBuffer[tempBufferIndex]);
                    }

                    foundFileName = true;
                }
            }

            /* We are skipping the first sign, so we should put the '/' to the begin of the string (the string will be reverted) */
            _putc('/');

            /* Put the NULL terminator to end of the string (the null terminator in reversion is not moved) */
            _putc(0);

            if(outAbsolute != NULL)
            {
                /* Reverse a string, because it was pushed from the end */
                string_reverse(outAbsolute);
            }

            if(ksmartfree(tempBuffer,tempBufferLength+1,AllocationFlags_CanWaitForever)==false)
            {
                oC_SaveError("VFS,ConvertRelativeToAbsolute: cannot relase temp buffer" , oC_ErrorCode_ReleaseError);
            }

        }
        else
        {
            strcpy(outAbsolute,"VFS error");
        }
    }

#undef _putc
    return requiredSize;
}

//==========================================================================================================================================
//==========================================================================================================================================
static char * ConvertToAbsoluteWithAllocation(const char * Relative , bool TreatAsDir )
{
    uint32_t requiredSize = ConvertRelativeToAbsolute(Relative,NULL,0,TreatAsDir);
    char *   absolute     = ksmartalloc(requiredSize,&Allocator,AllocationFlags_CanWait1Second);

    if(absolute)
    {
        ConvertRelativeToAbsolute(Relative,absolute,requiredSize,TreatAsDir);
    }

    return absolute;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool CheckIfDirExists( const char * Absolute )
{
    bool exists = strcmp(Absolute,"/") == 0;

    MountedEntry_t mountedEntry = GetMountedFileSystem(Absolute,true);

    if(isaddresscorrect(mountedEntry) && isaddresscorrect(mountedEntry->FileSystem->DirExists))
    {
        oC_UInt_t mountedPathLength = strlen(mountedEntry->MountedPoint);
        exists                      = mountedEntry->FileSystem->DirExists(mountedEntry->Context,&Absolute[mountedPathLength-1]);
    }

    return exists;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_Storage_t GetStorageFromPath( const char * DevicePath, oC_File_t * outVirtualStorageFile )
{
    oC_Storage_t storage = oC_StorageMan_GetStorage( DevicePath );

    if( storage == NULL )
    {
        storage = NewVirtualStorage(DevicePath,outVirtualStorageFile);
    }

    return storage;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_Storage_t NewVirtualStorage( const char * DevicePath, oC_File_t * outVirtualStorageFile )
{
    oC_Storage_t storage = NULL;
    oC_File_t    file    = NULL;

    if( (file = fopen(DevicePath, "rw" )) || (file = fopen(DevicePath, "r" )) )
    {
        storage                = oC_Storage_New(&VirtualStorageInterface, file, "virtual_storage", 0, 1, fsize(file));
        *outVirtualStorageFile = file;

        if( storage == NULL )
        {
            fclose(file);
        }
    }

    return storage;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool DeleteVirtualStorage( oC_Storage_t VirtualStorage , oC_File_t VirtualStorageFile )
{
    fclose(VirtualStorageFile);
    return oC_Storage_Delete(&VirtualStorage);
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t ReadVirtualStorageSectors( void * Context, oC_SectorNumber_t SectorNumber, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
    oC_File_t       file        = Context;

    if( ErrorCode( oC_VirtualFileSystem_lseek(file,SectorNumber) ) )
    {
        errorCode = oC_VirtualFileSystem_fread(file,outBuffer,Size);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t WriteVirtualStorageSectors( void * Context, oC_SectorNumber_t SectorNumber, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
    oC_File_t       file        = Context;

    if( ErrorCode( oC_VirtualFileSystem_lseek(file,SectorNumber) ) )
    {
        errorCode = oC_VirtualFileSystem_fwrite(file,Buffer,Size);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_ErrorCode_t EraseVirtualStorageSectors( void * Context, oC_SectorNumber_t SectorNumber, oC_SectorNumber_t NumberOfSectors, oC_Time_t Timeout )
{
    return oC_ErrorCode_NotImplemented;
}

#undef  _________________________________________LOCAL_FUNCTIONS_SECTION___________________________________________________________________
