/** ****************************************************************************************************************************************
 *
 * @file       oc_ramfs.c
 *
 * @brief      The file with source for the RAM file system
 *
 * @author     Patryk Kubiak - (Created on: 10 10 2015 17:31:02)
 *
 * @note       Copyright (C) 2015 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_ramfs.h>
#include <oc_list.h>
#include <oc_stdlib.h>
#include <oc_object.h>
#include <oc_ifnot.h>
#include <string.h>
#include <oc_intman.h>
#include <oc_array.h>
#include <oc_math.h>
#include <oc_ktime.h>
#include <oc_errors.h>

/** ========================================================================================================================================
 *
 *              The section with local macros
 *
 *  ======================================================================================================================================*/
#define _________________________________________MACROS_SECTION_____________________________________________________________________________

#define IsFileCorrect(File)     (oC_CheckObjectControl((File),oC_ObjectId_RamFsFile,(File)->ObjectControl))
#define IsDirCorrect(Dir)       (oC_CheckObjectControl((Dir),oC_ObjectId_RamFsDir,(Dir)->ObjectControl))
#define IsFileReadOnly(file)    ((file)->Attributes & oC_FileSystem_FileAttributes_ReadOnly )
#define IsFileLocked(File)      ((file) != NULL && (file)->Lock)

#undef  _________________________________________MACROS_SECTION_____________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct File_t
{
    oC_ObjectControl_t              ObjectControl;
    char *                          Path;
    uint8_t *                       Data;
    uint32_t                        Size;
    uint32_t                        Offset;
    oC_FileSystem_FileType_t        FileType;
    oC_FileSystem_FileAttributes_t  Attributes;
    bool                            Lock;
    oC_Timestamp_t                  Timestamp;
    oC_ErrorCode_t                  ErrorCode;
};

struct Dir_t
{
    oC_ObjectControl_t      ObjectControl;
    char *                  Path;
    oC_File_t               LastFile;
};

struct Context_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_List(oC_File_t)      Files;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions prototyeps
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

static bool         IsContextCorrect(oC_RamFs_Context_t Context );
static const char * GetLastEntryNameFromPath( const char * Path );
static char *       GetParentDirOfPath( const char * Path , char * outDir , oC_UInt_t Size );
static bool         CheckIfParentDirectoryExists( oC_RamFs_Context_t Context , const char * Path );
static bool         CheckIfPathExists( oC_RamFs_Context_t Context , const char * Path );
static oC_File_t    GetFile( oC_RamFs_Context_t Context , const char * Path );
static oC_File_t    CreateFile( oC_RamFs_Context_t Context ,  const char * Path , oC_FileSystem_FileType_t FileType , oC_FileSystem_FileAttributes_t Attributes );
static bool         DeleteFile( oC_RamFs_Context_t Context ,  const char * Path );
static bool         LockFile( oC_File_t File );
static void         UnlockFile( oC_File_t File );

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES_SECTION__________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
const oC_FileSystem_Registration_t RamFs = {
                .Name     = "RamFs" ,
                .init     = oC_RamFs_init     ,
                .deinit   = oC_RamFs_deinit   ,
                .fopen    = oC_RamFs_fopen    ,
                .fclose   = oC_RamFs_fclose   ,
                .fread    = oC_RamFs_fread    ,
                .fwrite   = oC_RamFs_fwrite   ,
                .lseek    = oC_RamFs_lseek    ,
                .ioctl    = oC_RamFs_ioctl    ,
                .sync     = oC_RamFs_sync     ,
                .Getc     = oC_RamFs_getc     ,
                .Putc     = oC_RamFs_putc     ,
                .tell     = NULL              ,
                .eof      = oC_RamFs_eof      ,
                .size     = oC_RamFs_size     ,
                .fflush   = oC_RamFs_flush    ,

                .opendir  = oC_RamFs_opendir  ,
                .closedir = oC_RamFs_closedir ,
                .readdir  = oC_RamFs_readdir  ,

                .stat     = oC_RamFs_stat     ,
                .unlink   = oC_RamFs_unlink   ,
                .rename   = oC_RamFs_rename   ,
                .chmod    = oC_RamFs_chmod    ,
                .utime    = oC_RamFs_utime    ,
                .mkdir    = oC_RamFs_mkdir    ,
                .DirExists= oC_RamFs_DirExists ,
};

//==========================================================================================================================================
//==========================================================================================================================================
const oC_Allocator_t Allocator = {
                .Name = "RamFs"
};

#undef  _________________________________________VARIABLES_SECTION__________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with interface functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_init( oC_RamFs_Context_t * outContext , oC_Storage_t Storage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outContext) , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        oC_RamFs_Context_t context = kmalloc(sizeof(struct Context_t) , &Allocator , AllocationFlags_CanWait1Second );

        if(isram(context))
        {
            context->ObjectControl = oC_CountObjectControl(context,oC_ObjectId_RamFsContext);
            context->Files         = oC_List_New(&Allocator,AllocationFlags_CanWait1Second);

            if(oC_List_IsCorrect(context->Files))
            {
                *outContext            = context;
                errorCode              = oC_ErrorCode_None;
            }
            else
            {
                errorCode = oC_ErrorCode_ListNotCorrect;

                ifnot(kfree(context,AllocationFlags_CanWaitForever))
                {
                    oC_SaveError("RamFs - init: Cannot release context memory" , oC_ErrorCode_ReleaseError);
                }
            }
        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_deinit( oC_RamFs_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect )
        )
    {
        Context->ObjectControl  = 0;
        errorCode               = oC_ErrorCode_None;

        oC_List_Foreach(Context->Files,file)
        {
            UnlockFile(file);

            ifnot(DeleteFile(Context,file->Path))
            {
                oC_SaveError("RamFs:deinit, cannot delete file" , oC_ErrorCode_ReleaseError);
            }
        }

        oC_AssignErrorCodeIfFalse(&errorCode , oC_List_Delete(Context->Files,AllocationFlags_CanWaitForever) , oC_ErrorCode_ReleaseError );
        oC_AssignErrorCodeIfFalse(&errorCode , kfree(Context,AllocationFlags_CanWaitForever)                 , oC_ErrorCode_ReleaseError );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_fopen( oC_RamFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)    , oC_ErrorCode_WrongAddress ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outFile)            , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        oC_File_t file = GetFile(Context,Path);

        if(
            oC_AssignErrorCodeIfFalse(&errorCode ,  (file != NULL) || ((Mode & oC_FileSystem_ModeFlags__FailIfNotExists ) == 0 ) , oC_ErrorCode_NoSuchFile          ) &&
            oC_AssignErrorCodeIfFalse(&errorCode ,  (file == NULL) || ((Mode & oC_FileSystem_ModeFlags__FailIfExists    ) == 0 ) , oC_ErrorCode_FileAlreadyExists   ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , file == NULL || IsFileCorrect(file)                                           , oC_ErrorCode_FileIsBusy          ) &&
            oC_AssignErrorCodeIfFalse(&errorCode , !IsFileLocked(file)                                                           , oC_ErrorCode_FileNotCorrect      )
            )
        {
            if(file != NULL)
            {
                file->Offset = 0;

                if(!IsFileCorrect(file) && !(Mode & oC_FileSystem_ModeFlags__Create))
                {
                    errorCode = oC_ErrorCode_ExistingFileNotCorrect;
                }
                else if((Mode & oC_FileSystem_ModeFlags_Write) && IsFileReadOnly(file))
                {
                    errorCode = oC_ErrorCode_FileIsReadOnly;
                }
                else if(Mode & oC_FileSystem_ModeFlags__Open)
                {
                    *outFile = file;
                    errorCode= oC_ErrorCode_None;
                }
                else if(Mode & oC_FileSystem_ModeFlags__Create)
                {
                    if(IsFileReadOnly(file))
                    {
                        errorCode = oC_ErrorCode_FileIsReadOnly;
                    }
                    else
                    {
                        if(DeleteFile(Context,Path))
                        {
                            file = CreateFile(Context,Path,oC_FileSystem_FileType_File,Attributes);

                            if(file)
                            {
                                *outFile = file;
                                errorCode = oC_ErrorCode_None;
                            }
                            else
                            {
                                errorCode = oC_ErrorCode_CannotCreateFile;
                            }
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_CannotDeleteFile;
                        }
                    }
                }
            }
            else
            {
                if(Mode & oC_FileSystem_ModeFlags__Create)
                {
                    if(CheckIfParentDirectoryExists(Context,Path))
                    {
                        file = CreateFile(Context,Path,oC_FileSystem_FileType_File,Attributes);

                        if(file)
                        {
                            *outFile = file;
                            errorCode = oC_ErrorCode_None;
                        }
                        else
                        {
                            errorCode = oC_ErrorCode_CannotCreateFile;
                        }
                    }
                    else
                    {
                        errorCode = oC_ErrorCode_DirectoryNotExists;
                    }
                }
                else
                {
                    errorCode = oC_ErrorCode_ModeNotCorrect;
                }
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_fclose( oC_RamFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsFileCorrect(Context)    , oC_ErrorCode_FileNotCorrect )
        )
    {
        UnlockFile(File);
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_fread( oC_RamFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) ,    oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsFileCorrect(File)       ,    oC_ErrorCode_FileNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , LockFile(File),                oC_ErrorCode_FileIsBusy)
        )
    {
        if(
            oC_AssignErrorCodeIfFalse(&errorCode , isram(outBuffer),              oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , isram(Size),                   oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , *Size > 0,                     oC_ErrorCode_SizeNotCorrect)        &&
            oC_AssignErrorCodeIfFalse(&errorCode , isarrayinram(outBuffer,*Size), oC_ErrorCode_ArraySizeTooBig)       
            )
        {
            uint8_t * buffer    = outBuffer;
            uint32_t  byteIndex = 0;


            for(byteIndex = 0 ; isram(&File->Data[File->Offset]) && byteIndex < *Size && File->Offset < File->Size ; byteIndex++ )
            {
                buffer[byteIndex] = File->Data[File->Offset++];
            }

            if(byteIndex < *Size)
            {
                errorCode = oC_ErrorCode_NoAllBytesRead;
            }
            else
            {
                errorCode = oC_ErrorCode_None;
            }
            *Size = byteIndex;
        }
        File->ErrorCode = errorCode;

        UnlockFile(File);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_fwrite( oC_RamFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) ,    oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsFileCorrect(File)       ,    oC_ErrorCode_FileNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , LockFile(File),                oC_ErrorCode_FileIsBusy)
        )
    {
        if ( 
            oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Buffer),      oC_ErrorCode_WrongAddress) &&
            oC_AssignErrorCodeIfFalse(&errorCode , isram(Size),                   oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , *Size > 0,                     oC_ErrorCode_SizeNotCorrect) &&
            oC_AssignErrorCodeIfFalse(&errorCode , isarraycorrect(Buffer,*Size),  oC_ErrorCode_ArraySizeTooBig) )
        {
            uint32_t        newSize       = oC_MAX(*Size + File->Offset , File->Size);
            uint8_t *       newBuffer     = kmalloc(newSize ,&Allocator,AllocationFlags_CanWait1Second);
            const uint8_t * buffer        = Buffer;
            const uint8_t * bufferEnd     = &buffer[*Size];

            if(oC_AssignErrorCodeIfFalse(&errorCode , isram(newBuffer) , oC_ErrorCode_AllocationError))
            {
                for(uint32_t newBufferIndex = 0; newBufferIndex < newSize ; newBufferIndex++)
                {
                    if(newBufferIndex < File->Offset || buffer >= bufferEnd)
                    {
                        newBuffer[newBufferIndex] = File->Data[newBufferIndex];
                    }
                    else
                    {
                        newBuffer[newBufferIndex] = *buffer;
                        buffer++;
                    }
                }

                if(oC_AssignErrorCodeIfFalse(&errorCode , File->Data == NULL || kfree(File->Data,AllocationFlags_CanWaitForever) , oC_ErrorCode_ReleaseError))
                {
                    File->Size      = newSize;
                    File->Data      = newBuffer;
                    File->Offset   += *Size;
                    File->Timestamp = oC_KTime_GetTimestamp();

                    errorCode     = oC_ErrorCode_None;
                }
                else
                {
                    ifnot(kfree(newBuffer,AllocationFlags_CanWaitForever))
                    {
                        oC_SaveError("RamFs: fwrite - relasing temp buffer error" , oC_ErrorCode_ReleaseError);
                    }
                }

            }
        }
        File->ErrorCode = errorCode;

        UnlockFile(File);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_lseek( oC_RamFs_Context_t Context , oC_File_t File , uint32_t Offset )
{
   oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

   if(
       oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
       oC_AssignErrorCodeIfFalse(&errorCode , IsFileCorrect(File)       , oC_ErrorCode_FileNotCorrect ) &&
       oC_AssignErrorCodeIfFalse(&errorCode , LockFile(File),             oC_ErrorCode_FileIsBusy)
       )
   {
        if( oC_AssignErrorCodeIfFalse(&errorCode , Offset < File->Size       , oC_ErrorCode_OffsetTooBig ))
        {
            File->Offset = Offset;
            errorCode    = oC_ErrorCode_None;
        }

        File->ErrorCode = errorCode;

        UnlockFile(File);
   }

   return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_ioctl( oC_RamFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if ( 
        ErrorCondition( IsFileCorrect(File), oC_ErrorCode_FileNotCorrect ) 
     && ErrorCondition( LockFile(File), oC_ErrorCode_FileIsBusy )
        )
    {
        errorCode = oC_ErrorCode_NotHandledByFileSystem;
        File->ErrorCode = errorCode;
        UnlockFile(File);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_sync( oC_RamFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsFileCorrect(File)       , oC_ErrorCode_FileNotCorrect )
        )
    {
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_getc( oC_RamFs_Context_t Context , char * outCharacter , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context) , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsFileCorrect(File)       , oC_ErrorCode_FileNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , LockFile(File)            , oC_ErrorCode_FileIsBusy)
        )
    {
        if ( 
            oC_AssignErrorCodeIfFalse(&errorCode , isram(outCharacter)       , oC_ErrorCode_OutputAddressNotInRAM) &&
            oC_AssignErrorCodeIfFalse(&errorCode , File->Data != NULL        , oC_ErrorCode_FileIsEmpty) &&
            oC_AssignErrorCodeIfFalse(&errorCode , File->Offset < File->Size , oC_ErrorCode_EndOfFile)  )
        {
            *outCharacter = File->Data[File->Offset++];
            errorCode     = oC_ErrorCode_None;
        }
        File->ErrorCode = errorCode;

        UnlockFile(File);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_putc( oC_RamFs_Context_t Context , char Character , oC_File_t File )
{
    oC_MemorySize_t size = 1;
    return oC_RamFs_fwrite(Context,File,&Character,&size);
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_RamFs_eof( oC_RamFs_Context_t Context , oC_File_t File )
{
    bool eof = true;

    if(IsContextCorrect(Context) && IsFileCorrect(File))
    {
        eof = File->Offset >= File->Size;
    }

    return eof;
}

//==========================================================================================================================================
//==========================================================================================================================================
uint32_t oC_RamFs_size( oC_RamFs_Context_t Context , oC_File_t File )
{
    uint32_t size = 0;

    if(IsContextCorrect(Context) && IsFileCorrect(File))
    {
        size = File->Size;
    }

    return size;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_flush( oC_RamFs_Context_t Context , oC_File_t File )
{
    return oC_ErrorCode_None;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_error( oC_RamFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsFileCorrect(File)       , oC_ErrorCode_FileNotCorrect     )
     && ErrorCondition( LockFile(File)            , oC_ErrorCode_FileIsBusy         )
        )
    {
        errorCode = File->ErrorCode;
        UnlockFile(File);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_opendir( oC_RamFs_Context_t Context , oC_Dir_t * outDir , const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)        , oC_ErrorCode_ContextNotCorrect)      &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outDir)                    , oC_ErrorCode_OutputAddressNotInRAM)  &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)           , oC_ErrorCode_WrongAddress)           &&
        oC_AssignErrorCodeIfFalse(&errorCode , CheckIfPathExists(Context,Path)  , oC_ErrorCode_PathNotCorrect)
        )
    {
        oC_Dir_t dir = smartalloc(sizeof(struct Dir_t),AllocationFlags_CanWaitForever);

        if(dir)
        {
            dir->ObjectControl = oC_CountObjectControl(dir,oC_ObjectId_RamFsDir);
            dir->Path          = smartalloc(strlen(Path)+1,AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill);

            if(dir->Path != NULL)
            {
                strcpy(dir->Path,Path);
                dir->LastFile   = NULL;
                *outDir         = dir;
                errorCode       = oC_ErrorCode_None;
            }
            else
            {
                if(smartfree(dir,sizeof(struct Dir_t),AllocationFlags_CanWaitForever)==false)
                {
                    oC_SaveError("RamFs-opendir: Cannot release memory previously allocated for DIR: " , oC_ErrorCode_ReleaseError);
                }
                errorCode = oC_ErrorCode_AllocationError;
            }

        }
        else
        {
            errorCode = oC_ErrorCode_AllocationError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_closedir( oC_RamFs_Context_t Context , oC_Dir_t Dir )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)        , oC_ErrorCode_ContextNotCorrect)      &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsDirCorrect(Dir)                , oC_ErrorCode_ObjectNotCorrect)
        )
    {
        errorCode = oC_ErrorCode_None;

        ifnot(smartfree(Dir->Path,strlen(Dir->Path)+1,AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }

        ifnot(smartfree(Dir,sizeof(struct Dir_t),AllocationFlags_CanWaitForever))
        {
            errorCode = oC_ErrorCode_ReleaseError;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_readdir( oC_RamFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)        , oC_ErrorCode_ContextNotCorrect)      &&
        oC_AssignErrorCodeIfFalse(&errorCode , IsDirCorrect(Dir)                , oC_ErrorCode_ObjectNotCorrect)       &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outFileInfo)               , oC_ErrorCode_OutputAddressNotInRAM)
        )
    {
        bool        lastFileFound = Dir->LastFile == NULL;
        bool        nextFileFound = false;
        oC_UInt_t   pathLength    = strlen(Dir->Path);
        char *      tempDir       = smartalloc(pathLength+1,AllocationFlags_ZeroFill);


        oC_List_Foreach(Context->Files,file)
        {
            char * path = GetParentDirOfPath(file->Path,tempDir,pathLength+1);
            if(path != NULL)
            {
                if(strcmp(path,Dir->Path) == 0)
                {
                    if(lastFileFound)
                    {
                        Dir->LastFile = file;
                        nextFileFound = true;
                        break;
                    }
                    else
                    {
                        lastFileFound = file == Dir->LastFile;
                    }
                }
            }
        }

        if(nextFileFound)
        {
            outFileInfo->FileType = Dir->LastFile->FileType;
            outFileInfo->Size     = Dir->LastFile->Size;
            outFileInfo->Timestamp= Dir->LastFile->Timestamp;
            outFileInfo->Name     = GetLastEntryNameFromPath(Dir->LastFile->Path);

            errorCode             = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_NoSuchFile;
        }

        ifnot(smartfree(tempDir,pathLength+1,AllocationFlags_CanWaitForever))
        {
            oC_SaveError("RamFs-readdir: cannot release temp buffer",oC_ErrorCode_ReleaseError);
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_stat( oC_RamFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)        , oC_ErrorCode_ContextNotCorrect)      &&
        oC_AssignErrorCodeIfFalse(&errorCode , isram(outFileInfo)               , oC_ErrorCode_OutputAddressNotInRAM)  &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)           , oC_ErrorCode_WrongAddress)           &&
        oC_AssignErrorCodeIfFalse(&errorCode , CheckIfPathExists(Context,Path)  , oC_ErrorCode_PathNotCorrect)
        )
    {
        oC_File_t file = GetFile(Context,Path);

        if(file != NULL)
        {
            outFileInfo->FileType = file->FileType;
            outFileInfo->Name     = GetLastEntryNameFromPath(file->Path);
            outFileInfo->Size     = file->Size;
            outFileInfo->Timestamp= file->Timestamp;
            errorCode             = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_NoSuchFile;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_unlink( oC_RamFs_Context_t Context , const char * Path)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)        , oC_ErrorCode_ContextNotCorrect)   &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)           , oC_ErrorCode_WrongAddress)        &&
        oC_AssignErrorCodeIfFalse(&errorCode , CheckIfPathExists(Context,Path)  , oC_ErrorCode_PathNotCorrect)
        )
    {
        oC_File_t file = GetFile(Context,Path);

        if(file)
        {
            if(DeleteFile(Context,Path))
            {
                errorCode = oC_ErrorCode_None;

                if(file->FileType == oC_FileSystem_FileType_Directory)
                {
                    oC_UInt_t pathLength = strlen(Path);
                    oC_List_Foreach(Context->Files,file)
                    {
                        if(strncmp(Path,file->Path,pathLength)==0)
                        {
                            ifnot(DeleteFile(Context,file->Path))
                            {
                                oC_SaveError("RamFs-unlink: Cannot delete one of subfiles",oC_ErrorCode_ReleaseError);
                            }
                        }
                    }
                }
            }
            else
            {
                errorCode = oC_ErrorCode_CannotDeleteFile;
            }
        }
        else
        {
            errorCode = oC_ErrorCode_NoSuchFile;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_rename( oC_RamFs_Context_t Context , const char * OldName , const char * NewName)
{
    return oC_ErrorCode_NotImplemented;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_chmod( oC_RamFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)        , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)           , oC_ErrorCode_WrongAddress )      &&
        oC_AssignErrorCodeIfFalse(&errorCode , CheckIfPathExists(Context,Path)  , oC_ErrorCode_PathNotCorrect)
        )
    {
        oC_File_t file = GetFile(Context,Path);

        if(file)
        {
            file->Attributes &= ~Mask;
            file->Attributes |= Attributes;

            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_NoSuchFile;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_utime( oC_RamFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)        , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)           , oC_ErrorCode_WrongAddress )      &&
        oC_AssignErrorCodeIfFalse(&errorCode , CheckIfPathExists(Context,Path)  , oC_ErrorCode_PathNotCorrect)
        )
    {
        oC_File_t file = GetFile(Context,Path);

        if(file)
        {
            file->Timestamp = Timestamp;
            errorCode       = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_NoSuchFile;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_RamFs_mkdir( oC_RamFs_Context_t Context , const char * Path)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        oC_AssignErrorCodeIfFalse(&errorCode , IsContextCorrect(Context)                  , oC_ErrorCode_ContextNotCorrect ) &&
        oC_AssignErrorCodeIfFalse(&errorCode , isaddresscorrect(Path)                     , oC_ErrorCode_WrongAddress )      &&
        oC_AssignErrorCodeIfFalse(&errorCode , !CheckIfPathExists(Context,Path)           , oC_ErrorCode_PathAlreadyUsed)    &&
        oC_AssignErrorCodeIfFalse(&errorCode , CheckIfParentDirectoryExists(Context,Path) , oC_ErrorCode_PathNotCorrect)
        )
    {
        if(CreateFile(Context,Path,oC_FileSystem_FileType_Directory,0)!=NULL)
        {
            errorCode = oC_ErrorCode_None;
        }
        else
        {
            errorCode = oC_ErrorCode_CannotCreateFile;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_RamFs_DirExists( oC_RamFs_Context_t Context , const char * Path)
{
    return IsContextCorrect(Context) && CheckIfPathExists(Context,Path);
}

#undef  _________________________________________INTERFACE_FUNCTIONS_SECTION________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
static bool IsContextCorrect(oC_RamFs_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context,oC_ObjectId_RamFsContext,Context->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
static const char * GetLastEntryNameFromPath( const char * Path )
{
    oC_UInt_t    pathLength = strlen(Path);
    const char * entryName  = (pathLength > 1)? &Path[pathLength-2] :&Path[pathLength-1];

    while(entryName >= Path)
    {
        if(*entryName == '/')
        {
            entryName++;
            break;
        }

        entryName--;
    }

    return entryName;
}

//==========================================================================================================================================
//==========================================================================================================================================
static char * GetParentDirOfPath( const char * Path , char * outDir , oC_UInt_t Size )
{
    char *          dir         = outDir;
    const char *    entryName   = GetLastEntryNameFromPath(Path);
    oC_UInt_t       dirLength   = strlen(Path)-strlen(entryName);

    if(dirLength <= (Size-1))
    {
        strncpy(dir,Path,dirLength);
        dir[dirLength] = 0;
    }
    else
    {
        dir = NULL;
    }

    return dir;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool CheckIfParentDirectoryExists( oC_RamFs_Context_t Context , const char * Path )
{
    bool            exists              = false;
    const char *    lastEntry           = GetLastEntryNameFromPath(Path);
    oC_UInt_t       parentPathLength    = strlen(Path) - strlen(lastEntry);

    exists = strncmp("/" , Path , parentPathLength) == 0;

    oC_List_Foreach(Context->Files,file)
    {
        if(
            file->FileType == oC_FileSystem_FileType_Directory &&
            strncmp(file->Path , Path , parentPathLength) == 0
                        )
        {
            exists = true;
            break;
        }
    }


    return exists;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool CheckIfPathExists( oC_RamFs_Context_t Context , const char * Path )
{
    bool exists = strcmp(Path,"/") == 0;

    oC_List_Foreach(Context->Files,file)
    {
        if(strcmp(file->Path,Path)==0)
        {
            exists = true;
            break;
        }
    }

    return exists;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_File_t GetFile( oC_RamFs_Context_t Context , const char * Path )
{
    oC_File_t fileToReturn = NULL;

    oC_List_Foreach(Context->Files,file)
    {
        if(strcmp(file->Path,Path)==0)
        {
            fileToReturn = file;
            break;
        }
    }

    return fileToReturn;
}

//==========================================================================================================================================
//==========================================================================================================================================
static oC_File_t CreateFile( oC_RamFs_Context_t Context , const char * Path , oC_FileSystem_FileType_t FileType , oC_FileSystem_FileAttributes_t Attributes )
{
    oC_File_t file = kmalloc(sizeof(struct File_t),&Allocator,AllocationFlags_CanWait1Second);

    if(file)
    {
        file->Path = kmalloc(strlen(Path)+1,&Allocator,AllocationFlags_CanWait1Second | AllocationFlags_ZeroFill);

        if(file->Path)
        {
            strcpy(file->Path,Path);
            file->Attributes    = Attributes;
            file->FileType      = FileType;
            file->Lock          = false;
            file->Size          = 0;
            file->Offset        = 0;
            file->Data          = NULL;
            file->ObjectControl = oC_CountObjectControl(file,oC_ObjectId_RamFsFile);
            file->Timestamp     = oC_KTime_GetTimestamp();

            bool pushed         = oC_List_PushBack(Context->Files,file,&Allocator);

            ifnot(pushed)
            {
                file->ObjectControl = 0;

                ifnot(kfree(file->Path,AllocationFlags_CanWaitForever))
                {
                    oC_SaveError("RamFs: Create file error: " , oC_ErrorCode_ReleaseError);
                }
                ifnot(kfree(file,AllocationFlags_CanWaitForever))
                {
                    oC_SaveError("RamFs: Create file error: " , oC_ErrorCode_ReleaseError);
                }
                file = NULL;
            }
        }
        else
        {
            ifnot(kfree(file,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("RamFs: Create file error: " , oC_ErrorCode_ReleaseError);
            }
            file = NULL;
        }
    }

    return file;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool DeleteFile( oC_RamFs_Context_t Context , const char * Path )
{
    bool        deleted = false;
    oC_File_t   file    = GetFile(Context,Path);

    if(file && LockFile(file))
    {
        bool removedFromList = oC_List_RemoveAll(Context->Files,file);

        ifnot(removedFromList)
        {
            oC_SaveError("RamFs:DeleteFile" , oC_ErrorCode_CannotRemoveObjectFromList);
        }
        else
        {
            file->ObjectControl = 0;

            deleted = true;

            ifnot(file->Data == NULL || kfree(file->Data,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("RamFs: DeleteFile, cannot delete data buffer" , oC_ErrorCode_ReleaseError);
                deleted = false;
            }

            ifnot(kfree(file->Path,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("RamFs: DeleteFile, cannot delete path buffer" , oC_ErrorCode_ReleaseError);
                deleted = false;
            }

            ifnot(kfree(file,AllocationFlags_CanWaitForever))
            {
                oC_SaveError("RamFs: DeleteFile, cannot release file memory" , oC_ErrorCode_ReleaseError);
                deleted = false;
            }

        }
    }

    return deleted;
}

//==========================================================================================================================================
//==========================================================================================================================================
static bool LockFile( oC_File_t File )
{
    bool locked = false;
    oC_IntMan_EnterCriticalSection();
    if(File->Lock==false)
    {
        File->Lock = true;
        locked     = true;
    }
    oC_IntMan_ExitCriticalSection();

    return locked;
}

//==========================================================================================================================================
//==========================================================================================================================================
static void UnlockFile( oC_File_t File )
{
    oC_IntMan_EnterCriticalSection();
    File->Lock = false;
    oC_IntMan_ExitCriticalSection();
}


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________
