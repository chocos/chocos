/** ****************************************************************************************************************************************
 *
 * @file       oc_diskman.c
 *
 * @brief      contains implementation of DiskMan functions
 *
 * @author     Patryk Kubiak - (Created on: 26 09 2017 19:59:39)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_diskman.h>
#include <oc_module.h>
#include <oc_object.h>
#include <oc_debug.h>
#include <oc_intman.h>
#include <oc_thread.h>
#include <oc_semaphore.h>
#include <oc_compiler.h>
#include <oc_dynamic_config.h>
#include <oc_driverman.h>

/** ========================================================================================================================================
 *
 *              The section with definition
 *
 *  ======================================================================================================================================*/
#define _________________________________________DEFINITION_________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief flag if debug logs should be enabled or not
 *
 * The flag allows to turn on or turn off debug logs in the module. Set it to `false` if debug logs from the module are not required.
 */
//==========================================================================================================================================
#define DEBUGLOGS_ENABLED               true

#if DEBUGLOGS_ENABLED
#   define debuglog( LogType, ... )     kdebuglog( LogType, "diskman: " __VA_ARGS__ )
#else
#   define debuglog( LogType, ... )
#endif

#undef  _________________________________________DEFINITION_________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief stores context for storage driver interface
 */
//==========================================================================================================================================
typedef struct DiskContext_t
{
    oC_ObjectControl_t      ObjectControl;      //!< Value calculated during object creation. It is used for object validation
    oC_Disk_t               Disk;               //!< Pointer to the related disk
    oC_DiskId_t             DiskId;             //!< ID of the disk to use
    oC_DriverInstance_t*    DriverInstance;     //!< Instance of a driver connected with the context
} * DiskContext_t;

//==========================================================================================================================================
/**
 * @brief stores driver instance context
 *
 * The structure is created for each driver instance.
 */
//==========================================================================================================================================
typedef struct DriverInstanceContext_t
{
    oC_ObjectControl_t          ObjectControl;              //!< Value calculated during object creation. It is used for object validation
    oC_DriverInstance_t *       DriverInstance;             //!< driver instance of the context
    oC_Thread_t                 WaitForNewDiskThread;       //!< thread that waits for new disk thread
    oC_Thread_t                 WaitForDiskEjectThread;     //!< thread that waits for disk eject
    oC_List(DiskContext_t)      DiskContexts;               //!< list with disk contexts
} * DriverInstanceContext_t;

//==========================================================================================================================================
/**
 * @brief stores context of service
 *
 * The type is for storing context of a DiskMan service.
 */
//==========================================================================================================================================
struct Context_t
{
    oC_ObjectControl_t                  ObjectControl;              //!< Value calculated during object creation. It is used for object validation
    oC_List(DriverInstanceContext_t)    DriverInstanceContexts;     //!< List with driver instance contexts
    oC_Semaphore_t                      NewDriverInstanceSemaphore; //!< Semaphore given when new driver instance is available
};

#undef  _________________________________________TYPES______________________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_________________________________________________________________________________

static oC_Service_Context_t     ServiceContext_New              ( void );
static bool                     ServiceContext_Delete           ( oC_Service_Context_t Context );
static bool                     ServiceContext_IsCorrect        ( oC_Service_Context_t Context );
static DiskContext_t            DiskContext_New                 ( oC_DiskId_t DiskId, oC_DriverInstance_t * DriverInstance , oC_Time_t Timeout );
static bool                     DiskContext_Delete              ( DiskContext_t DiskContext );
static bool                     DiskContext_IsCorrect           ( DiskContext_t DiskContext );
static DriverInstanceContext_t  DriverInstanceContext_New       ( oC_DriverInstance_t * Instance );
static bool                     DriverInstanceContext_Delete    ( DriverInstanceContext_t Context );
static bool                     DriverInstanceContext_IsCorrect ( DriverInstanceContext_t Context );
static oC_ErrorCode_t           ReadSectorsByDriver             ( void * Context, oC_SectorNumber_t SectorNumber, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
static oC_ErrorCode_t           WriteSectorsByDriver            ( void * Context, oC_SectorNumber_t SectorNumber, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout );
static oC_ErrorCode_t           EraseSectorsByDriver            ( void * Context, oC_SectorNumber_t SectorNumber, oC_SectorNumber_t NumberOfSectors, oC_Time_t Timeout );

static void                     DriverInstanceAddedCallback     ( void * UserCallback, oC_DriverInstance_t * Instance );
static void                     DriverInstanceRemovedCallback   ( void * UserCallback, oC_DriverInstance_t * Instance );
static void                     WaitForNewDiskThread            ( void * UserParameter );
static void                     WaitForDiskEjectThread          ( void * UserParameter );
static oC_ErrorCode_t           StartService                    ( oC_Service_Context_t * outContext );
static oC_ErrorCode_t           StopService                     ( oC_Service_Context_t * Context );
static oC_ErrorCode_t           ServiceMain                     ( oC_Service_Context_t Context );

#undef  _________________________________________PROTOTYPES_________________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief definition of module registration
 *
 * The variable is for module manager. Thanks to that it is not required to turn on the module by manual, but it can be done by
 * the module manager
 */
//==========================================================================================================================================
const oC_Module_Registration_t DiskMan = {
      .Name               = "DiskMan" ,
      .LongName           = "Disks Manager" ,
      .Module             = oC_Module_DiskMan ,
      .TurnOnFunction     = oC_DiskMan_TurnOn ,
      .TurnOffFunction    = oC_DiskMan_TurnOff ,
      .RequiredModules    = { oC_Module_ServiceMan, oC_Module_DriverMan } ,
};
//==========================================================================================================================================
/**
 * @brief definition of service registration
 *
 * The variable is for service manager, that is responsible for managing system services. It calls `StartService` and `StopService` functions
 * when it is necessary.
 */
//==========================================================================================================================================
const oC_Service_Registration_t diskman = {
    .Name                   = "diskman" ,
    .StartFunction          = StartService ,
    .StopFunction           = StopService ,
    .MainFunction           = ServiceMain ,
    .MainThreadStackSize    = B(2048),
    .HeapMapSize            = 0 ,
    .AllocationLimit        = 0 ,
    .RequiredModules        = { oC_Module_ServiceMan , oC_Module_DiskMan } ,
    .Priority               = oC_Process_Priority_NetworkHandlerProcess ,
};
//==========================================================================================================================================
/**
 * @brief definition of interface for handling disk with drivers
 *
 * The variable is for storing interface for `Storage` object. The interface uses drivers to access a disk.
 */
//==========================================================================================================================================
const oC_Storage_DiskInterface_t DriverDiskInterface = {
    .ReadSectors    = ReadSectorsByDriver ,
    .WriteSectors   = WriteSectorsByDriver ,
    .EraseSectors   = EraseSectorsByDriver ,
};
//==========================================================================================================================================
/**
 * @brief stores disks
 *
 * The variable is a list for storing disks. It should be created during module initialization and released during module turning off.
 */
//==========================================================================================================================================
oC_List(oC_Disk_t) Disks = NULL;
//==========================================================================================================================================
/**
 * @brief stores module allocator
 *
 * The variable is for allocations of the module.
 */
//==========================================================================================================================================
static const oC_Allocator_t Allocator = {
         .Name      = "diskman"
};

#undef  _________________________________________VARIABLES__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS__________________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief turns on the module
 *
 * The function is for turning on and initializing `DiskMan` module. It should be automatically called by module manager.
 *
 * @return
 * If the module has been correctly turned on, the function returns #oC_ErrorCode_None, otherwise one of the following codes should
 * be expected:
 *
 *   Code of error                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected implementation error. Probably error code is not correctly set
 *  #oC_ErrorCode_ModuleIsTurnedOn          | The module is already enabled
 *  #oC_ErrorCode_AllocationError           | Cannot allocate memory for a `Disks` list
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DiskMan_TurnOn( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOffVerification( &errorCode, oC_Module_DiskMan ) )
    {
        Disks = oC_List_New( &Allocator );

        if( ErrorCondition( Disks != NULL , oC_ErrorCode_AllocationError ) )
        {
            oC_Module_TurnOn( oC_Module_DiskMan );
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief turns off the module
 *
 * The function is for turning off and release all memory of `DiskMan` module. It should be automatically called by module manager.
 *
 * @return
 * If the module has been correctly turned off, the function returns #oC_ErrorCode_None, otherwise one of the following codes should
 * be expected:
 *
 *   Code of error                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected implementation error. Probably error code is not correctly set
 *  #oC_ErrorCode_ModuleNotStartedYet       | Module is not enabled
 *  #oC_ErrorCode_ReleaseError              | Cannot release memory allocated for `Disks`
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DiskMan_TurnOff( void )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    oC_IntMan_EnterCriticalSection();

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_DiskMan ) )
    {
        oC_Module_TurnOff( oC_Module_DiskMan );

        bool allDisksDeleted = true;

        foreach( Disks, disk )
        {
            allDisksDeleted = oC_Disk_Delete( &disk ) && allDisksDeleted;
        }

        if(
            ErrorCondition( oC_List_Delete( Disks ) , oC_ErrorCode_ReleaseError )
         && ErrorCondition( allDisksDeleted         , oC_ErrorCode_ReleaseError )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    oC_IntMan_ExitCriticalSection();

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief the function returns list of disks
 *
 * The function is for getting list of disks. If the list is not available, the function returns #NULL
 *
 * @warning
 * The function returns original pointer to the list. Don't change it's entries.
 *
 * @return List of disks or #NULL in case of error
 */
//==========================================================================================================================================
oC_DiskList_t oC_DiskMan_GetList( void )
{
    oC_DiskList_t list = NULL;

    if( oC_Module_IsTurnedOn(oC_Module_DiskMan) )
    {
        list = Disks;
    }

    return list;
}

//==========================================================================================================================================
/**
 * @brief returns disk with the given name
 *
 * The function is for getting disk according to the name.
 *
 * @return Pointer to the Disk object if found, otherwise #NULL
 */
//==========================================================================================================================================
oC_Disk_t oC_DiskMan_GetDisk( const char * DiskName )
{
    oC_Disk_t foundDisk = NULL;

    if( oC_Module_IsTurnedOn(oC_Module_DiskMan) && isaddresscorrect(DiskName) && strlen(DiskName) > 0 )
    {
        foreach( Disks, disk )
        {
            if( strcmp( oC_Disk_GetName(disk), DiskName ) == 0 )
            {
                foundDisk = disk;
                break;
            }
        }
    }

    return foundDisk;
}

//==========================================================================================================================================
/**
 * @brief adds new disk to the disks list
 *
 * The function is for adding a disk into the disks list. The disk has to be correct, and each disk can be added to the list only once. If
 * the disk you want to add already exist on the list, the function returns error #oC_ErrorCode_ObjectAlreadyExist
 *
 * @param Disk      Pointer to Disk object to add
 *
 * @return
 * The function returns #oC_ErrorCode_None if the disk has been added, otherwise one of the following codes should
 * be expected:
 *
 *   Code of error                          | Description
 * -----------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError            | Unexpected implementation error. Probably error code is not correctly set
 *  #oC_ErrorCode_ModuleNotStartedYet       | `DiskMan` is not enabled
 *  #oC_ErrorCode_ObjectNotCorrect          | The given `Disk` is not correct
 *  #oC_ErrorCode_ObjectAlreadyExist        | The given `Disk` already exist on the disks list.
 *  #oC_ErrorCode_CannotAddObjectToList     | Cannot add object to the list
 *
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DiskMan_AddDisk( oC_Disk_t Disk )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_DiskMan ) )
    {
        oC_IntMan_EnterCriticalSection();

        bool diskFoundOnList = oC_List_Contains(Disks, Disk);

        if(
            ErrorCondition( oC_Disk_IsCorrect(Disk)     , oC_ErrorCode_ObjectNotCorrect     )
         && ErrorCondition( diskFoundOnList == false    , oC_ErrorCode_ObjectAlreadyExist   )
            )
        {
            bool pushed = oC_List_PushBack( Disks, Disk, &Allocator );

            if( ErrorCondition( pushed, oC_ErrorCode_CannotAddObjectToList ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief removes a disk from the disks list
 *
 * The function is for removing a disk from the disks list.
 *
 * @param Disk      Pointer to Disk object to remove
 *
 * @return
 * The function returns #oC_ErrorCode_None if the disk has been removed, otherwise one of the following codes should
 * be expected:
 *
 *   Code of error                           | Description
 * ------------------------------------------|-----------------------------------------------------------------------------------------------
 *  #oC_ErrorCode_ImplementError             | Unexpected implementation error. Probably error code is not correctly set
 *  #oC_ErrorCode_ModuleNotStartedYet        | `DiskMan` is not enabled
 *  #oC_ErrorCode_ObjectNotCorrect           | The given `Disk` is not correct
 *  #oC_ErrorCode_ObjectNotFoundOnList       | The given `Disk` does not exist on the list
 *  #oC_ErrorCode_CannotRemoveObjectFromList | Cannot remove `Disk` from the list
 */
//==========================================================================================================================================
oC_ErrorCode_t oC_DiskMan_RemoveDisk( oC_Disk_t Disk )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( oC_Module_TurnOnVerification( &errorCode, oC_Module_DiskMan ) )
    {
        oC_IntMan_EnterCriticalSection();

        bool diskFoundOnList = oC_List_Contains(Disks, Disk);

        if(
            ErrorCondition( oC_Disk_IsCorrect(Disk)     , oC_ErrorCode_ObjectNotCorrect     )
         && ErrorCondition( diskFoundOnList == false    , oC_ErrorCode_ObjectNotFoundOnList )
            )
        {
            bool pushed = oC_List_RemoveAll( Disks, Disk );

            if( ErrorCondition( pushed, oC_ErrorCode_CannotRemoveObjectFromList ) )
            {
                errorCode = oC_ErrorCode_None;
            }
        }

        oC_IntMan_ExitCriticalSection();
    }

    return errorCode;
}

#undef  _________________________________________FUNCTIONS__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________


//==========================================================================================================================================
/**
 * @brief allocates memory for a service context
 */
//==========================================================================================================================================
static oC_Service_Context_t ServiceContext_New( void )
{
    oC_Service_Context_t context = malloc( sizeof( struct Context_t ) );
    bool success = false;

    if( oC_SaveIfFalse( "context", context != NULL, oC_ErrorCode_AllocationError ) )
    {
        context->DriverInstanceContexts     = oC_List_New( getcurallocator() );

        if( oC_SaveIfFalse( "list", context->DriverInstanceContexts != NULL, oC_ErrorCode_AllocationError ) )
        {
            context->NewDriverInstanceSemaphore = oC_Semaphore_New( oC_Semaphore_Type_Binary, 0, getcurallocator(), 0 );

            if( oC_SaveIfFalse( "semaphore", context->NewDriverInstanceSemaphore != NULL, oC_ErrorCode_AllocationError ) )
            {
                context->ObjectControl = oC_CountObjectControl( context, oC_ObjectId_DiskManServiceContext );
                success = true;
            }
            if( !success )
            {
                oC_Semaphore_Delete( &context->NewDriverInstanceSemaphore, 0 );
            }
        }
        if( !success )
        {
            context->ObjectControl = 0;
            free( context );
            context = NULL;
        }
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases memory allocated for a service context
 */
//==========================================================================================================================================
static bool ServiceContext_Delete( oC_Service_Context_t Context )
{
    bool deleted = false;

    if( oC_SaveIfFalse( "service context" , ServiceContext_IsCorrect( Context ), oC_ErrorCode_ObjectNotCorrect ) )
    {
        oC_IntMan_EnterCriticalSection();
        Context->ObjectControl = 0;

        foreach( Context->DriverInstanceContexts, instanceContext )
        {
            deleted = oC_SaveIfFalse( instanceContext->DriverInstance->Name, DriverInstanceContext_Delete(instanceContext), oC_ErrorCode_ReleaseError )
                   && deleted;
        }
        deleted = oC_Semaphore_Delete( &Context->NewDriverInstanceSemaphore , 0 ) && deleted;
        deleted = oC_List_Delete( Context->DriverInstanceContexts ) && deleted;

        oC_IntMan_ExitCriticalSection();
    }

    return deleted;
}

//==========================================================================================================================================
/**
 * @brief returns true if service context is correct
 */
//==========================================================================================================================================
static bool ServiceContext_IsCorrect( oC_Service_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl( Context, oC_ObjectId_DiskManServiceContext, Context->ObjectControl );
}

//==========================================================================================================================================
/**
 * @brief allocates memory for a disk context and initializes it
 */
//==========================================================================================================================================
static DiskContext_t DiskContext_New( oC_DiskId_t DiskId, oC_DriverInstance_t * DriverInstance , oC_Time_t Timeout )
{
    DiskContext_t       context         = NULL;
    oC_MemorySize_t     sectorSize      = 0;
    oC_SectorNumber_t   sectorCount     = 0;
    oC_Timestamp_t      endTimestamp    = timeouttotimestamp(Timeout);

    if(
        oC_SaveIfErrorOccur( "reading sector size" , oC_Driver_ReadSectorSize       ( DriverInstance->Driver, DriverInstance->Context, DiskId, &sectorSize , gettimeout(endTimestamp) ) )
     && oC_SaveIfErrorOccur( "reading sector count", oC_Driver_ReadNumberOfSectors  ( DriverInstance->Driver, DriverInstance->Context, DiskId, &sectorCount, gettimeout(endTimestamp) ) )
        )
    {
        context = malloc( sizeof(struct DiskContext_t) );
        if( oC_SaveIfFalse( "context", context != NULL, oC_ErrorCode_AllocationError ) )
        {
            context->ObjectControl  = oC_CountObjectControl( context, oC_ObjectId_DiskContext );
            context->Disk           = oC_Disk_New( &DriverDiskInterface, context, DriverInstance->Name, sectorSize, sectorCount, Timeout );
            context->DiskId         = DiskId;
            context->DriverInstance = DriverInstance;

            if( ! oC_SaveIfFalse( "context->Disk", context->Disk != NULL, oC_ErrorCode_AllocationError ) )
            {
                oC_SaveIfFalse( "context", free(context), oC_ErrorCode_ReleaseError );
                context = NULL;
            }
            else
            {
                debuglog( oC_LogType_Error, "driver instance %s - cannot create disk", DriverInstance->Name );
            }
        }
        else
        {
            debuglog( oC_LogType_Error, "driver instance %s - cannot allocate memory for disk context", DriverInstance->Name );
        }
    }
    else
    {
        debuglog( oC_LogType_Error, "driver instance %s - cannot read sector size or count", DiskId, DriverInstance->Name );
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases memory allocated for DiskContext
 */
//==========================================================================================================================================
static bool DiskContext_Delete( DiskContext_t DiskContext )
{
    bool success = false;

    if( oC_SaveIfFalse("DiskContext", DiskContext_IsCorrect(DiskContext), oC_ErrorCode_ObjectNotCorrect ) )
    {
        DiskContext->ObjectControl = 0;
        success = true;

        success = oC_SaveIfFalse( "DiskContext->Disk", oC_Disk_Delete( &DiskContext->Disk ), oC_ErrorCode_ReleaseError ) && success;
        success = oC_SaveIfFalse( "DiskContext"      , free( DiskContext )                 , oC_ErrorCode_ReleaseError ) && success;
    }

    return success;
}

//==========================================================================================================================================
/**
 * @brief returns true if object is correct
 */
//==========================================================================================================================================
static bool DiskContext_IsCorrect( DiskContext_t DiskContext )
{
    return isram( DiskContext ) && oC_CheckObjectControl( DiskContext, oC_ObjectId_DiskContext, DiskContext->ObjectControl );
}

//==========================================================================================================================================
/**
 * @brief allocates memory for driver instance context
 */
//==========================================================================================================================================
static DriverInstanceContext_t DriverInstanceContext_New( oC_DriverInstance_t * Instance )
{
    DriverInstanceContext_t context = malloc( sizeof(struct DriverInstanceContext_t) );

    if( oC_SaveIfFalse( "context", context != NULL, oC_ErrorCode_AllocationError ) )
    {
        oC_MemorySize_t stackSize = oC_DynamicConfig_GetValue(diskman,WaitForDiskStackSize);

        context->ObjectControl          = oC_CountObjectControl( context, oC_ObjectId_DriverInstanceContext );
        context->DiskContexts           = oC_List_New( getcurallocator() );
        context->DriverInstance         = Instance;
        context->WaitForDiskEjectThread = oC_Thread_New( 0, stackSize , NULL, Instance->Name, WaitForDiskEjectThread, context );
        context->WaitForNewDiskThread   = oC_Thread_New( 0, stackSize , NULL, Instance->Name, WaitForNewDiskThread  , context );

        bool threadRunned = oC_Thread_Run( context->WaitForDiskEjectThread )
                         && oC_Thread_Run( context->WaitForNewDiskThread   );

        if(
            !oC_SaveIfFalse( "disk contexts", context->DiskContexts != NULL     , oC_ErrorCode_AllocationError  )
         || !oC_SaveIfFalse( "thread"       , context->WaitForDiskEjectThread   , oC_ErrorCode_AllocationError  )
         || !oC_SaveIfFalse( "thread"       , context->WaitForNewDiskThread     , oC_ErrorCode_AllocationError  )
         || !oC_SaveIfFalse( "thread"       , threadRunned                      , oC_ErrorCode_CannotRunThread  )
            )
        {
            debuglog( oC_LogType_Error, "cannot create at least one of objects: %p, %p, %p", context->DiskContexts, context->WaitForDiskEjectThread,
                      context->WaitForNewDiskThread );
            oC_List_Delete( context->DiskContexts );
            oC_Thread_Delete( &context->WaitForDiskEjectThread );
            oC_Thread_Delete( &context->WaitForNewDiskThread   );
            context->ObjectControl = 0;
            free( context );
            context = NULL;
        }
    }
    else
    {
        debuglog( oC_LogType_Error, "Cannot allocate memory for a context" );
    }

    return context;
}

//==========================================================================================================================================
/**
 * @brief releases memory of driver instance context
 */
//==========================================================================================================================================
static bool DriverInstanceContext_Delete( DriverInstanceContext_t Context )
{
    bool deleted = false;

    if( oC_SaveIfFalse( "Context", DriverInstanceContext_IsCorrect(Context), oC_ErrorCode_ObjectNotCorrect ) )
    {
        Context->ObjectControl = 0;

        deleted = true;

        deleted = oC_SaveIfFalse("thread", oC_Thread_Delete( &Context->WaitForDiskEjectThread ), oC_ErrorCode_ReleaseError ) && deleted;
        deleted = oC_SaveIfFalse("thread", oC_Thread_Delete( &Context->WaitForNewDiskThread   ), oC_ErrorCode_ReleaseError ) && deleted;

        foreach( Context->DiskContexts, diskContext )
        {
            deleted = oC_SaveIfFalse( "diskContext", DiskContext_Delete( diskContext ), oC_ErrorCode_ReleaseError ) && deleted;
        }

        deleted = oC_SaveIfFalse( "disk contexts", oC_List_Delete( Context->DiskContexts ), oC_ErrorCode_ReleaseError ) && deleted;
    }
    else
    {
        debuglog( oC_LogType_Error, "cannot delete instance - it is not correct" );
    }

    return deleted;
}


//==========================================================================================================================================
/**
 * @brief returns true if object is correct
 */
//==========================================================================================================================================
static bool DriverInstanceContext_IsCorrect ( DriverInstanceContext_t Context )
{
    return isram( Context ) && oC_CheckObjectControl( Context, oC_ObjectId_DriverInstanceContext, Context->ObjectControl );
}

//==========================================================================================================================================
/**
 * @brief uses driver to read sectors
 *
 * The function uses driver to read disk sectors
 */
//==========================================================================================================================================
static oC_ErrorCode_t ReadSectorsByDriver( void * Context, oC_SectorNumber_t SectorNumber, void * outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    DiskContext_t  diskContext  = Context;

    if(
        ErrorCondition( DiskContext_IsCorrect( diskContext ), oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( isram(Size)                         , oC_ErrorCode_AddressNotInRam  )
        )
    {
        errorCode = oC_Driver_ReadSectors( diskContext->DriverInstance->Driver, diskContext->DriverInstance->Context, diskContext->DiskId, SectorNumber, outBuffer, *Size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief uses driver to write sectors
 *
 * The function uses driver to write disk sectors
 */
//==========================================================================================================================================
static oC_ErrorCode_t WriteSectorsByDriver( void * Context, oC_SectorNumber_t SectorNumber, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    DiskContext_t  diskContext  = Context;

    if(
        ErrorCondition( DiskContext_IsCorrect( diskContext ), oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( isram(Size)                         , oC_ErrorCode_AddressNotInRam  )
        )
    {
        errorCode = oC_Driver_WriteSectors( diskContext->DriverInstance->Driver, diskContext->DriverInstance->Context, diskContext->DiskId, SectorNumber, Buffer, *Size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief uses driver to erase sectors
 *
 * The function uses driver to erase disk sectors
 */
//==========================================================================================================================================
static oC_ErrorCode_t EraseSectorsByDriver( void * Context, oC_SectorNumber_t SectorNumber, oC_SectorNumber_t NumberOfSectors, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode    = oC_ErrorCode_ImplementError;
    DiskContext_t  diskContext  = Context;

    if(
        ErrorCondition( DiskContext_IsCorrect( diskContext ), oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( NumberOfSectors > 0                 , oC_ErrorCode_WrongSectorCount )
        )
    {
        oC_MemorySize_t size = NumberOfSectors * oC_Disk_GetSectorSize( diskContext->Disk );
        errorCode = oC_Driver_EraseSectors( diskContext->DriverInstance->Driver, diskContext->DriverInstance->Context, diskContext->DiskId,
                                            SectorNumber, size, Timeout );
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief function is called when a new driver instance is added
 */
//==========================================================================================================================================
static void DriverInstanceAddedCallback( void * UserCallback, oC_DriverInstance_t * Instance )
{
    oC_Service_Context_t context = UserCallback;

    if(
        oC_SaveIfFalse( "service context"   , ServiceContext_IsCorrect( context )   , oC_ErrorCode_ObjectNotCorrect     )
     && oC_SaveIfFalse( "instance"          , isaddresscorrect(Instance)            , oC_ErrorCode_WrongAddress         )
        )
    {
        if( oC_Driver_GetDriverType( Instance->Driver ) == oC_Driver_Type_DiskDriver )
        {
            oC_Semaphore_Give( context->NewDriverInstanceSemaphore );
        }
    }
}

//==========================================================================================================================================
/**
 * @brief function is called when a driver instance is removed
 */
//==========================================================================================================================================
static void DriverInstanceRemovedCallback( void * UserCallback, oC_DriverInstance_t * Instance )
{
    oC_Service_Context_t context = UserCallback;

    if(
        oC_SaveIfFalse( "service context"   , ServiceContext_IsCorrect( context )   , oC_ErrorCode_ObjectNotCorrect     )
     && oC_SaveIfFalse( "instance"          , isaddresscorrect(Instance)            , oC_ErrorCode_WrongAddress         )
        )
    {
        if( oC_Driver_GetDriverType( Instance->Driver ) == oC_Driver_Type_DiskDriver )
        {
            foreach( context->DriverInstanceContexts, driverInstanceContext )
            {
                if( driverInstanceContext->DriverInstance == Instance )
                {
                    oC_SaveIfFalse( "driver instance context" , DriverInstanceContext_Delete( driverInstanceContext ), oC_ErrorCode_ReleaseError );
                    oC_List_RemoveCurrentElement( context->DriverInstanceContexts, driverInstanceContext );
                    break;
                }
            }
        }
    }
}

//==========================================================================================================================================
/**
 * @brief thread that waits for new disk
 */
//==========================================================================================================================================
static void WaitForNewDiskThread( void * UserParameter )
{
    DriverInstanceContext_t context     = UserParameter;
    const char *            instanceName= context->DriverInstance->Name;

    MASK_AS_USED(instanceName);

    while(1)
    {
        oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
        oC_DiskId_t     diskId      = 0;

        if( ErrorCode ( oC_Driver_WaitForNewDisk( context->DriverInstance->Driver, context->DriverInstance->Context, &diskId, oC_day(365) ) ) )
        {
            DiskContext_t diskContext = DiskContext_New( diskId, context->DriverInstance, s(5) );
            if( oC_SaveIfFalse("diskContext", diskContext != NULL, oC_ErrorCode_AllocationError ) )
            {
                bool pushed = oC_List_PushBack( context->DiskContexts, diskContext, getcurallocator() );

                if( oC_SaveIfFalse( "diskContext", pushed, oC_ErrorCode_CannotAddObjectToList ) )
                {
                    pushed = oC_List_PushBack( Disks, diskContext->Disk, getcurallocator() );

                    if( oC_SaveIfFalse( "disk", pushed, oC_ErrorCode_CannotAddObjectToList ) )
                    {
                        oC_List( oC_Partition_t ) partitions = oC_Disk_GetPartitionsList( diskContext->Disk );

                        foreach( partitions, partition )
                        {
                            oC_ErrorCode_t errorCode = oC_Partition_Mount( partition , NULL );
                            if( oC_ErrorOccur( errorCode ) )
                            {
                                oC_SaveError( "cannot mount partition", errorCode );
                                debuglog( oC_LogType_Error, "cannot mount partition %s: %R", oC_Partition_GetName(partition), errorCode );
                            }
                            else
                            {
                                debuglog( oC_LogType_GoodNews, "partition '%s' has been mounted", oC_Partition_GetName(partition) );
                            }
                        }
                    }
                    else
                    {
                        bool removed = oC_List_RemoveAll( context->DiskContexts, diskContext );
                        oC_SaveIfFalse( "diskContext", removed, oC_ErrorCode_CannotRemoveObjectFromList );
                        oC_SaveIfFalse( "diskContext", DiskContext_Delete(diskContext), oC_ErrorCode_ReleaseError );
                        debuglog( oC_LogType_Error, "instance '%s', diskID %d: cannot add object to list", instanceName, diskId );
                    }
                }
                else
                {
                    oC_SaveIfFalse( "diskContext", DiskContext_Delete(diskContext), oC_ErrorCode_ReleaseError );
                    debuglog( oC_LogType_Error, "instance '%s', diskID %d: cannot add object to list", instanceName, diskId );
                }
            }
            else
            {
                debuglog( oC_LogType_Error, "instance '%s', diskID %d: cannot create disk", instanceName, diskId );
            }
        }
        else if( errorCode != oC_ErrorCode_Timeout )
        {
            oC_SaveError( context->DriverInstance->Name, errorCode );
            debuglog( oC_LogType_Error, "instance '%s': %R", instanceName, errorCode );
            break;
        }
    }
    debuglog( oC_LogType_Warning, "Loop for instance '%s' has been finished", instanceName );
}

//==========================================================================================================================================
/**
 * @brief thread that waits for disk ejection
 */
//==========================================================================================================================================
static void WaitForDiskEjectThread( void * UserParameter )
{
    DriverInstanceContext_t context     = UserParameter;
    const char *            instanceName= context->DriverInstance->Name;

    MASK_AS_USED(instanceName);

    while(1)
    {
        oC_ErrorCode_t  errorCode   = oC_ErrorCode_ImplementError;
        oC_DiskId_t     diskId      = 0;

        if( ErrorCode( oC_Driver_WaitForDiskEject( context->DriverInstance->Driver, context->DriverInstance->Context, &diskId, day(365) ) ) )
        {
            oC_IntMan_EnterCriticalSection();
            foreach( context->DiskContexts, diskContext )
            {
                if( diskContext->DiskId == diskId )
                {
                    oC_List_RemoveAll( Disks, diskContext->Disk );
                    if( ! oC_SaveIfFalse( "diskContext" , DiskContext_Delete( diskContext ), oC_ErrorCode_ReleaseError ) )
                    {
                        debuglog( oC_LogType_Error, "instance '%s', diskID %d: cannot delete disk context", instanceName, diskId );
                    }
                    oC_List_RemoveCurrentElement( context->DiskContexts, diskContext );
                }
            }
            oC_IntMan_ExitCriticalSection();
        }
        else if( errorCode != oC_ErrorCode_Timeout )
        {
            oC_SaveError( context->DriverInstance->Name, errorCode );
            debuglog( oC_LogType_Error, "instance '%s': %R", instanceName, errorCode );
            break;
        }
    }
    debuglog( oC_LogType_Warning, "Loop for instance '%s' has been finished", instanceName );
}

//==========================================================================================================================================
/**
 * @brief initializes diskman service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StartService( oC_Service_Context_t * outContext )
{
    oC_ErrorCode_t       errorCode = oC_ErrorCode_ImplementError;
    oC_Service_Context_t context   = ServiceContext_New();

    if( ErrorCondition( context != NULL, oC_ErrorCode_AllocationError ) )
    {
        if(
            ErrorCode( oC_DriverMan_SetInstanceAddedCallback    ( DriverInstanceAddedCallback   , context ) )
         && ErrorCode( oC_DriverMan_SetInstanceRemovedCallback  ( DriverInstanceRemovedCallback , context ) )
            )
        {
            *outContext = context;
            errorCode   = oC_ErrorCode_None;
        }
        else
        {
            debuglog( oC_LogType_Error, "Cannot set callback in driver manager: %R", errorCode );
            oC_SaveIfFalse( "service context", ServiceContext_Delete( context ), oC_ErrorCode_ReleaseError );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief releases diskman service
 */
//==========================================================================================================================================
static oC_ErrorCode_t StopService( oC_Service_Context_t * Context )
{
    oC_ErrorCode_t          errorCode = oC_ErrorCode_ImplementError;
    oC_Service_Context_t    context   = NULL;

    if( ErrorCondition( isram(Context), oC_ErrorCode_AddressNotInRam ) )
    {
        context = *Context;


        if( ErrorCondition( ServiceContext_IsCorrect( context ) , oC_ErrorCode_ObjectNotCorrect     ) )
        {
            errorCode = oC_ErrorCode_None;

            ErrorCode( oC_DriverMan_SetInstanceAddedCallback  ( NULL, NULL ) );
            ErrorCode( oC_DriverMan_SetInstanceRemovedCallback( NULL, NULL ) );
            ErrorCondition( ServiceContext_Delete( context ), oC_ErrorCode_CannotDeleteObject );
        }
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief main function of diskman service
 */
//==========================================================================================================================================
static oC_ErrorCode_t ServiceMain( oC_Service_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    while( ErrorCondition( ServiceContext_IsCorrect( Context ) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        if( oC_Semaphore_Take( Context->NewDriverInstanceSemaphore, day(365) ) )
        {
            oC_IntMan_EnterCriticalSection();
            oC_List( oC_DriverInstance_t * ) instances = oC_DriverMan_GetInstanceList();

            if( ErrorCondition( instances != NULL, oC_ErrorCode_ListNotCorrect ) )
            {

                foreach( instances, instance )
                {
                    bool found = false;

                    foreach( Context->DriverInstanceContexts, driverInstanceContext )
                    {
                        if( driverInstanceContext->DriverInstance == instance )
                        {
                            found = true;
                            break;
                        }
                    }

                    if( !found )
                    {
                        DriverInstanceContext_t instanceContext = DriverInstanceContext_New( instance );

                        if( ErrorCondition( instanceContext != NULL, oC_ErrorCode_AllocationError ) )
                        {
                            bool pushed = oC_List_PushBack( Context->DriverInstanceContexts, instanceContext, getcurallocator() );

                            if( ErrorCondition( pushed, oC_ErrorCode_CannotAddObjectToList ) )
                            {
                                debuglog( oC_LogType_GoodNews, "diskman: new instance for '%s' has been created and added to list", instance->Name );
                            }
                            else
                            {
                                debuglog( oC_LogType_Error, "diskman: cannot add instance context to list - '%s'", instance->Name );
                            }
                        }
                        else
                        {
                            debuglog( oC_LogType_Error, "diskman: cannot allocate memory for instance context - '%s'", instance->Name );
                        }
                    }
                }
            }
            else
            {
                break;
            }
            oC_IntMan_ExitCriticalSection();
        }
        else if( ! oC_Semaphore_IsCorrect( Context->NewDriverInstanceSemaphore ) )
        {
            errorCode = oC_ErrorCode_ObjectNotCorrect;
            oC_SaveError( "new driver semaphore", errorCode );
            break;
        }
    }

    debuglog( oC_LogType_Error, "DiskMan service thread has finished with result %R", errorCode );

    return errorCode;
}

#undef  _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________
