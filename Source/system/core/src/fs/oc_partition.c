/** ****************************************************************************************************************************************
 *
 * @brief      Stores implementation of partition object
 * 
 * @file       oc_partition.c
 *
 * @author     Patryk Kubiak - (Created on: 22.08.2017 18:38:12) 
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_partition.h>
#include <oc_object.h>
#include <oc_string.h>
#include <oc_intman.h>
#include <oc_storageman.h>
#include <oc_stdio.h>
#include <oc_filesystem_list.h>
#include <oc_vfs.h>

#define MAX_NAME_LENGTH         (sizeof(oC_DefaultString_t) - 1)

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES_SECTION______________________________________________________________________________

struct Partition_t
{
    oC_ObjectControl_t      ObjectControl;
    oC_Storage_t            Storage;
    oC_MemorySize_t         Size;
    oC_Partition_Entry_t    PartitionEntry;
    oC_Partition_Location_t Location;
    oC_DefaultPathString_t  DefaultMountPath;
};

#undef  _________________________________________TYPES_SECTION______________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local prototypes
 *
 *  ======================================================================================================================================*/
#define _________________________________________PROTOTYPES_SECTION_________________________________________________________________________

#undef  _________________________________________PROTOTYPES_SECTION_________________________________________________________________________



/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_Partition_t oC_Partition_New( const oC_Partition_Entry_t * Entry, const oC_Partition_Location_t * Location, oC_Storage_t Storage )
{
    oC_Partition_t partition = NULL;

    if(
        oC_SaveIfFalse( "Location"       , isaddresscorrect(Location)                   , oC_ErrorCode_WrongAddress             )
     && oC_SaveIfFalse( "PartitionEntry" , isaddresscorrect(Entry)                      , oC_ErrorCode_WrongAddress             )
     && oC_SaveIfFalse( "Storage"        , oC_Storage_IsCorrect(Storage)                , oC_ErrorCode_ObjectNotCorrect         )
     && oC_SaveIfFalse( "Location"       , Location->Filled                             , oC_ErrorCode_LocationNotFilled        )
     && oC_SaveIfFalse( "Location->Level", Location->Level < oC_PARTITION_MAX_LEVEL     , oC_ErrorCode_LevelNotCorrect          )
        )
    {
        partition = malloc( sizeof(struct Partition_t), AllocationFlags_ZeroFill );

        if( oC_SaveIfFalse( "partition", partition != NULL, oC_ErrorCode_AllocationError   ) )
        {
            partition->ObjectControl    = oC_CountObjectControl(partition,oC_ObjectId_Partition);
            partition->Size             = oC_Storage_GetSize(Storage);
            partition->Storage          = Storage;

            sprintf( partition->DefaultMountPath, CFG_PATH_DEFAULT_MEDIA_MOUNT_PATH "%s", oC_Storage_GetName(Storage) );
            memcpy( &partition->PartitionEntry, Entry       , sizeof(partition->PartitionEntry)    );
            memcpy( &partition->Location,       Location    , sizeof(partition->Location)          );

        }
    }

    return partition;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Partition_Delete( oC_Partition_t * Partition )
{
    bool deleted = false;

    oC_IntMan_EnterCriticalSection();

    if(
        oC_SaveIfFalse( "Partition", isram(Partition)                   , oC_ErrorCode_AddressNotInRam  )
     && oC_SaveIfFalse( "Partition", oC_Partition_IsCorrect(*Partition) , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        oC_Partition_t partition = *Partition;

        deleted = true;

        (*Partition)->ObjectControl = 0;

        if( ! free(partition) )
        {
            deleted = false;
        }
        *Partition = NULL;
    }

    oC_IntMan_ExitCriticalSection();

    return deleted;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Partition_IsCorrect( oC_Partition_t Partition )
{
    return isram(Partition) && oC_CheckObjectControl(Partition,oC_ObjectId_Partition,Partition->ObjectControl);
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Storage_t oC_Partition_GetStorage( oC_Partition_t Partition )
{
    oC_Storage_t storage = NULL;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition)   , oC_ErrorCode_ObjectNotCorrect ) )
    {
        storage = Partition->Storage;
    }

    return storage;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Partition_IsBootable( oC_Partition_t Partition )
{
    bool bootable = false;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition)   , oC_ErrorCode_ObjectNotCorrect ) )
    {
        bootable = Partition->PartitionEntry.IsBootable == 1;
    }

    return bootable;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Partition_IsPrimary( oC_Partition_t Partition )
{
    bool isPrimary = false;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition)   , oC_ErrorCode_ObjectNotCorrect ) )
    {
        isPrimary = Partition->Location.Level == 0;
    }

    return isPrimary;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_Partition_IsExtended( oC_Partition_t Partition )
{
    bool isExtended = false;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition), oC_ErrorCode_ObjectNotCorrect ) )
    {
        isExtended = Partition->PartitionEntry.Type == oC_Partition_Type_ExtendedLBA;
    }

    return isExtended;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Partition_Index_t oC_Partition_GetPrimaryIndex( oC_Partition_t Partition )
{
    oC_Partition_Index_t index = 0;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition)   , oC_ErrorCode_ObjectNotCorrect ) )
    {
        index = Partition->Location.Indexes[0];
    }

    return index;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_Partition_Type_t oC_Partition_GetType( oC_Partition_t Partition )
{
    oC_Partition_Type_t type = oC_Partition_Type_Empty;

    if( oC_SaveIfFalse( "Partition", oC_Partition_IsCorrect(Partition), oC_ErrorCode_ObjectNotCorrect ) )
    {
        type = (oC_Partition_Type_t)Partition->PartitionEntry.Type;
    }

    return type;
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_Partition_GetName( oC_Partition_t Partition )
{
    const char * name = "incorrect partition";

    if(oC_Partition_IsCorrect(Partition))
    {
        name = oC_Storage_GetName(Partition->Storage);
    }

    return name;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_ReadLocation( oC_Partition_t Partition, oC_Partition_Location_t * outLocation )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Partition_IsCorrect(Partition)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outLocation)                  , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        memcpy(outLocation, &Partition->Location, sizeof(oC_Partition_Location_t));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_ReadEntry( oC_Partition_t Partition, oC_Partition_Entry_t * outEntry )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( oC_Partition_IsCorrect(Partition)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outEntry)                     , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        memcpy(outEntry, &Partition->Location, sizeof(oC_Partition_Location_t));
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_Read( oC_Partition_t Partition, oC_MemoryOffset_t Offset, char *    outBuffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Partition_IsCorrect(Partition) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        errorCode = oC_Storage_Read(Partition->Storage,Offset,outBuffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_Write( oC_Partition_t Partition, oC_MemoryOffset_t Offset, const char * Buffer , oC_MemorySize_t * Size , oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Partition_IsCorrect(Partition) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        errorCode = oC_Storage_Write(Partition->Storage,Offset,Buffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_Partition_GetSectorSize( oC_Partition_t Partition )
{
    oC_MemorySize_t size = 0;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition), oC_ErrorCode_ObjectNotCorrect) )
    {
        size = oC_Storage_GetSectorSize(Partition->Storage);
    }

    return size;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_SectorNumber_t oC_Partition_GetSectorCount( oC_Partition_t Partition )
{
    oC_SectorNumber_t sectorCount = 0;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition), oC_ErrorCode_ObjectNotCorrect) )
    {
        sectorCount = oC_Storage_GetSectorCount(Partition->Storage);
    }

    return sectorCount;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_MemorySize_t oC_Partition_GetSize( oC_Partition_t Partition )
{
    oC_MemorySize_t size = 0;

    if( oC_SaveIfFalse("Partition", oC_Partition_IsCorrect(Partition), oC_ErrorCode_ObjectNotCorrect) )
    {
        size = Partition->Size;
    }

    return size;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_ReadSectors( oC_Partition_t Partition, oC_SectorNumber_t StartSector, void *    outBuffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Partition_IsCorrect(Partition) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        errorCode = oC_Storage_ReadSectors(Partition->Storage,StartSector,outBuffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_WriteSectors( oC_Partition_t Partition, oC_SectorNumber_t StartSector, const void * Buffer, oC_MemorySize_t * Size, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Partition_IsCorrect(Partition) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        errorCode = oC_Storage_WriteSectors(Partition->Storage,StartSector,Buffer,Size,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_EraseSectors( oC_Partition_t Partition, oC_SectorNumber_t StartSector, oC_SectorNumber_t SectorCount, oC_Time_t Timeout )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Partition_IsCorrect(Partition) , oC_ErrorCode_ObjectNotCorrect ) )
    {
        errorCode = oC_Storage_EraseSectors(Partition->Storage,StartSector,SectorCount,Timeout);
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_Mount( oC_Partition_t Partition , const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Partition_IsCorrect( Partition ), oC_ErrorCode_ObjectNotCorrect ) )
    {
        if( Path == NULL )
        {
            Path = Partition->DefaultMountPath;
        }
        switch( Partition->PartitionEntry.Type )
        {
            case oC_Partition_Type_FAT16:
                errorCode = oC_VirtualFileSystem_mount( "fatfs", oC_Storage_GetName(Partition->Storage), Path );
                break;
            default:
                errorCode = oC_ErrorCode_WrongPartitionType;
                break;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_Partition_Umount( oC_Partition_t Partition , const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( oC_Partition_IsCorrect( Partition ), oC_ErrorCode_ObjectNotCorrect ) )
    {
        if( Path == NULL )
        {
            Path = Partition->DefaultMountPath;
        }
        errorCode = oC_VirtualFileSystem_umount( Path );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
const char * oC_Partition_GetDefaultMountPath( oC_Partition_t Partition )
{
    const char * path = NULL;

    if( oC_Partition_IsCorrect(Partition) )
    {
        return Partition->DefaultMountPath;
    }

    return path;
}

#undef  _________________________________________FUNCTIONS_SECTION__________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


#undef  _________________________________________LOCAL_FUNCTIONS_SECTION____________________________________________________________________


