/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2014        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include <oc_fs.h>
#include <oc_storage.h>
#include <oc_debug.h>

/* Definitions of physical drive number for each drive */
#define ATA		0	/* Example: Map ATA drive to drive number 0 */
#define MMC		1	/* Example: Map MMC/SD card to drive number 1 */
#define USB		2	/* Example: Map USB drive to drive number 2 */
#define STORAGE_ACCESS_TIMEOUT      s(5)

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
    BYTE    pdrv,               /* Physical drive nmuber to identify the drive */
    void *  storage             /* Pointer to the storage of the disk */
)
{
    if ( oC_Storage_IsCorrect( storage ) )
    {
        return RES_OK;
    }
    else
    {
        return STA_NOINIT;
    }
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/
DSTATUS disk_initialize (
	BYTE    pdrv,				/* Physical drive nmuber to identify the drive */
	void *  storage             /* Pointer to the storage of the disk */
)
{
    if ( oC_Storage_IsCorrect( storage ) )
    {
        return RES_OK;
    }
    else
    {
        return STA_NOINIT;
    }
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
    BYTE    pdrv,               /* Physical drive nmuber to identify the drive */
    void *  storage,            /* Pointer to the storage of the disk */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Sector address in LBA */
	UINT count		/* Number of sectors to read */
)
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;
    oC_MemorySize_t size      = count * oC_Storage_GetSectorSize(storage);
    if ( ErrorCode( oC_Storage_ReadSectors(storage, sector, buff, &size, STORAGE_ACCESS_TIMEOUT ) ) )
    {
        return RES_OK;
    }
    else
    {
        oC_SaveError( oC_Storage_GetName(storage), errorCode );
        kdebuglog( oC_LogType_Error, "%s: cannot read storage: %R", oC_Storage_GetName(storage), errorCode );
        return RES_ERROR;
    }
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write (
    BYTE    pdrv,               /* Physical drive nmuber to identify the drive */
    void *  storage,            /* Pointer to the storage of the disk */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Sector address in LBA */
	UINT count			/* Number of sectors to write */
)
{
    oC_ErrorCode_t  errorCode = oC_ErrorCode_ImplementError;
    oC_MemorySize_t size      = count * oC_Storage_GetSectorSize(storage);
    if ( ErrorCode( oC_Storage_WriteSectors(storage, sector, buff, &size, STORAGE_ACCESS_TIMEOUT ) ) )
    {
        return RES_OK;
    }
    else
    {
        oC_SaveError( oC_Storage_GetName(storage), errorCode );
        kdebuglog( oC_LogType_Error, "%s: cannot read storage: %R", oC_Storage_GetName(storage), errorCode );
        return RES_ERROR;
    }
}
#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl (
    BYTE    pdrv,               /* Physical drive nmuber to identify the drive */
    void *  storage,            /* Pointer to the storage of the disk */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
    if ( cmd == GET_SECTOR_COUNT
      || cmd == GET_SECTOR_SIZE )
    {
        if ( !isram(buff) )
        {
            kdebuglog( oC_LogType_Error, "Buffer address is not correct: %p", buff );
            return RES_PARERR;
        }
        if ( cmd == GET_SECTOR_COUNT )
        {
            *((DWORD*)buff) = (DWORD) oC_Storage_GetSectorCount( storage );
        }
        else
        {
            *((WORD*)buff) = (WORD) oC_Storage_GetSectorSize( storage );
        }
        return RES_OK;
    }
	return RES_PARERR;
}
#endif
