/** ****************************************************************************************************************************************
 *
 * @file       oc_fatfs.c
 *
 * @brief      contains implementation of file system interface
 *
 * @author     Patryk Kubiak - (Created on: 29 10 2017 14:54:18)
 *
 * @note       Copyright (C) 2017 Patryk Kubiak <patryk.kubiak@chocoos.org>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_fatfs.h>
#include <ff.h>
#include <oc_debug.h>
#include <oc_intman.h>
#include <oc_struct.h>

/** ========================================================================================================================================
 *
 *              The section with types
 *
 *  ======================================================================================================================================*/
#define _________________________________________TYPES______________________________________________________________________________________

struct Context_t
{
    oC_ObjectControl_t     ObjectControl;
    FATFS                  FatFsContext;
};

struct File_t
{
    oC_ObjectControl_t     ObjectControl;
    FIL                    FatFsFile;
};

struct Dir_t
{
    oC_ObjectControl_t      ObjectControl;
    DIR                     FatFsDir;
};


#undef  _________________________________________TYPES______________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________local_prototypes___________________________________________________________________________

static oC_ErrorCode_t FatFsResultToErrorCode ( FRESULT Result );
static bool           IsContextCorrect       ( oC_FatFs_Context_t Context );
static bool           IsFileCorrect          ( oC_File_t File );
static bool           IsDirCorrect           ( oC_Dir_t File );
static oC_Timestamp_t FatFsTimeToTimestamp   ( WORD      Date, WORD      Time );
static void           FatFsTimeFromTimestamp ( WORD * outDate, WORD * outTime , oC_Timestamp_t Timestamp );

#undef  _________________________________________local_prototypes___________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________

static const oC_Allocator_t Allocator = {
             .Name = "fatfs"
};

const oC_FileSystem_Registration_t FatFs = {
            .Name     = "fatfs" ,
            .init     = oC_FatFs_init     ,
            .deinit   = oC_FatFs_deinit   ,
            .fopen    = oC_FatFs_fopen    ,
            .fclose   = oC_FatFs_fclose   ,
            .fread    = oC_FatFs_fread    ,
            .fwrite   = oC_FatFs_fwrite   ,
            .lseek    = oC_FatFs_lseek    ,
            .ioctl    = oC_FatFs_ioctl    ,
            .sync     = oC_FatFs_sync     ,
            .Getc     = oC_FatFs_getc     ,
            .Putc     = oC_FatFs_putc     ,
            .tell     = oC_FatFs_tell     ,
            .eof      = oC_FatFs_eof      ,
            .size     = oC_FatFs_size     ,
            .fflush   = oC_FatFs_flush    ,
            .error    = oC_FatFs_error    ,

            .opendir  = oC_FatFs_opendir  ,
            .closedir = oC_FatFs_closedir ,
            .readdir  = oC_FatFs_readdir  ,

            .stat     = oC_FatFs_stat     ,
            .unlink   = oC_FatFs_unlink   ,
            .rename   = oC_FatFs_rename   ,
            .chmod    = oC_FatFs_chmod    ,
            .utime    = oC_FatFs_utime    ,
            .mkdir    = oC_FatFs_mkdir    ,
            .DirExists= oC_FatFs_DirExists,
};

#undef  _________________________________________VARIABLES__________________________________________________________________________________


/** ========================================================================================================================================
 *
 *              The section with variables
 *
 *  ======================================================================================================================================*/
#define _________________________________________VARIABLES__________________________________________________________________________________


#undef  _________________________________________VARIABLES__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________FUNCTIONS__________________________________________________________________________________

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_init( oC_FatFs_Context_t * outContext , oC_Storage_t Storage )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( isram(outContext)               , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( oC_Storage_IsCorrect(Storage)   , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        oC_FatFs_Context_t context = kmalloc( sizeof(struct Context_t), &Allocator, AllocationFlags_ZeroFill );
        if ( ErrorCondition( context != NULL, oC_ErrorCode_AllocationError ) )
        {
            context->ObjectControl          = oC_CountObjectControl( context, oC_ObjectId_FatFs );
            context->FatFsContext.storage   = Storage;

            FRESULT result = f_mount( &context->FatFsContext, "/", 1 );

            if ( ErrorCondition( result == FR_OK , FatFsResultToErrorCode(result) ) )
            {
                *outContext = context;
                errorCode   = oC_ErrorCode_None;
            }
            else
            {
                context->ObjectControl = 0;
                kfree(context,0);
                kdebuglog( oC_LogType_Error, "Cannot mount fatfs with %s: %d, %R", oC_Storage_GetName(Storage), result, errorCode );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_deinit( oC_FatFs_Context_t Context )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if( ErrorCondition( IsContextCorrect(Context), oC_ErrorCode_ObjectNotCorrect ) )
    {
        Context->ObjectControl = 0;
        if( ErrorCondition( kfree(Context,0), oC_ErrorCode_ReleaseError ) )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_fopen( oC_FatFs_Context_t Context , oC_File_t * outFile , const char * Path , oC_FileSystem_ModeFlags_t Mode , oC_FileSystem_FileAttributes_t Attributes )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isram(outFile)              , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isaddresscorrect(Path)      , oC_ErrorCode_OutputAddressNotInRAM   )
        )
    {
        oC_File_t file = malloc( sizeof(struct File_t), AllocationFlags_ZeroFill );
        if( ErrorCondition( file != NULL, oC_ErrorCode_AllocationError ) )
        {
            BYTE mode = 0;

            mode |= (Mode & oC_FileSystem_ModeFlags_Read            ) ? FA_READ             : 0;
            mode |= (Mode & oC_FileSystem_ModeFlags_OpenExisting    ) ? FA_OPEN_EXISTING    : 0;
            mode |= (Mode & oC_FileSystem_ModeFlags_OpenAlways      ) ? FA_OPEN_ALWAYS      : 0;
            mode |= (Mode & oC_FileSystem_ModeFlags_Write           ) ? FA_WRITE            : 0;
            mode |= (Mode & oC_FileSystem_ModeFlags_CreateNew       ) ? FA_CREATE_NEW       : 0;
            mode |= (Mode & oC_FileSystem_ModeFlags_CreateNewAlways ) ? FA_CREATE_ALWAYS    : 0;

            FRESULT result = f_open( &file->FatFsFile, &Context->FatFsContext, Path, mode );
            if( ErrorCondition( result == FR_OK, FatFsResultToErrorCode(result) ) )
            {
                file->ObjectControl = oC_CountObjectControl(file, oC_ObjectId_FatFsFile);
                *outFile            = file;
                errorCode           = oC_ErrorCode_None;
            }
            else
            {
                free( file, 0 );
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_fclose( oC_FatFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context), oC_ErrorCode_ObjectNotCorrect )
     && ErrorCondition( IsFileCorrect(File)      , oC_ErrorCode_ObjectNotCorrect )
        )
    {
        FRESULT result = f_close(&File->FatFsFile);

        File->ObjectControl = 0;

        if(
            ErrorCondition( free(File, 0 ) , oC_ErrorCode_ReleaseError          )
         && ErrorCondition( result == FR_OK, FatFsResultToErrorCode(result)     )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_fread( oC_FatFs_Context_t Context , oC_File_t File , void * outBuffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context), oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( IsFileCorrect(File)      , oC_ErrorCode_ObjectNotCorrect        )
     && ErrorCondition( isram(outBuffer)         , oC_ErrorCode_OutputAddressNotInRAM   )
     && ErrorCondition( isram(Size)              , oC_ErrorCode_AddressNotInRam         )
     && ErrorCondition( (*Size) > 0              , oC_ErrorCode_SizeNotCorrect          )
        )
    {
        UINT br   = 0;
        errorCode = FatFsResultToErrorCode( f_read( &File->FatFsFile, outBuffer, (UINT)*Size, &br ) );
        *Size     = (oC_MemorySize_t)br;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_fwrite( oC_FatFs_Context_t Context , oC_File_t File , const void * Buffer , oC_MemorySize_t * Size )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsFileCorrect(File)         , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isaddresscorrect(Buffer)    , oC_ErrorCode_WrongAddress             )
     && ErrorCondition( isram(Size)                 , oC_ErrorCode_AddressNotInRam          )
     && ErrorCondition( (*Size) > 0                 , oC_ErrorCode_SizeNotCorrect          )
        )
    {
        UINT bw   = 0;
        errorCode = FatFsResultToErrorCode( f_write( &File->FatFsFile, Buffer, (UINT)*Size, &bw ) );
        *Size     = (oC_MemorySize_t)bw;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_lseek( oC_FatFs_Context_t Context , oC_File_t File , uint32_t Offset )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsFileCorrect(File)         , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        errorCode = FatFsResultToErrorCode( f_lseek( &File->FatFsFile, Offset ) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_ioctl( oC_FatFs_Context_t Context , oC_File_t File , oC_Ioctl_Command_t Command , void * Pointer)
{
    return oC_ErrorCode_NotSupportedByFileSystem;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_sync( oC_FatFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsFileCorrect(File)         , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        errorCode = FatFsResultToErrorCode( f_sync( &File->FatFsFile ) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_getc( oC_FatFs_Context_t Context , char * outCharacter , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsFileCorrect(File)         , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outCharacter)         , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        f_gets( (TCHAR*)outCharacter, 1, &File->FatFsFile );
        errorCode = oC_ErrorCode_None;
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_putc( oC_FatFs_Context_t Context , char Character , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsFileCorrect(File)         , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        errorCode = FatFsResultToErrorCode( f_putc( Character, &File->FatFsFile ) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
int32_t oC_FatFs_tell( oC_FatFs_Context_t Context , oC_File_t File )
{
    int32_t result = -1;

    oC_IntMan_EnterCriticalSection();

    if( IsContextCorrect( Context ) && IsFileCorrect( File ) )
    {
        result = File->FatFsFile.fptr;
    }

    oC_IntMan_ExitCriticalSection();

    return result;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_FatFs_eof( oC_FatFs_Context_t Context , oC_File_t File )
{
    bool result = true;

    oC_IntMan_EnterCriticalSection();

    if( IsContextCorrect( Context ) && IsFileCorrect( File ) )
    {
        result = File->FatFsFile.fptr >= File->FatFsFile.fsize;
        if( File->FatFsFile.fptr > File->FatFsFile.fsize )
        {
           kdebuglog( oC_LogType_Error, "File offset is too big: 0x%08X > 0x%08X", File->FatFsFile.fptr, File->FatFsFile.fsize );
        }
    }

    oC_IntMan_ExitCriticalSection();

    return result;
}

//==========================================================================================================================================
//==========================================================================================================================================
uint32_t oC_FatFs_size( oC_FatFs_Context_t Context , oC_File_t File )
{
    uint32_t result = true;

    oC_IntMan_EnterCriticalSection();

    if( IsContextCorrect( Context ) && IsFileCorrect( File ) )
    {
        result = File->FatFsFile.fsize;
    }

    oC_IntMan_ExitCriticalSection();

    return result;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_flush( oC_FatFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsFileCorrect(File)         , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        errorCode = FatFsResultToErrorCode( f_sync( &File->FatFsFile ) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_error( oC_FatFs_Context_t Context , oC_File_t File )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsFileCorrect(File)         , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        errorCode = FatFsResultToErrorCode( File->FatFsFile.err );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_opendir( oC_FatFs_Context_t Context , oC_Dir_t * outDir , const char * Path )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outDir)               , oC_ErrorCode_OutputAddressNotInRAM    )
     && ErrorCondition( isaddresscorrect(Path)      , oC_ErrorCode_WrongAddress             )
        )
    {
        oC_Dir_t dir = malloc( sizeof(struct Dir_t), AllocationFlags_ZeroFill );
        if( ErrorCondition( dir != NULL, oC_ErrorCode_AllocationError ) )
        {
            FRESULT result = f_opendir( &dir->FatFsDir, Path );
            if( ErrorCondition( result == FR_OK, FatFsResultToErrorCode(result) ) )
            {
                *outDir             = dir;
                dir->ObjectControl  = oC_CountObjectControl( dir, oC_ObjectId_FatFsDir );
                errorCode           = oC_ErrorCode_None;
            }
            else
            {
                free(dir,0);
            }
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_closedir( oC_FatFs_Context_t Context , oC_Dir_t Dir )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsDirCorrect(Dir)           , oC_ErrorCode_ObjectNotCorrect         )
        )
    {
        FRESULT result = f_closedir( &Dir->FatFsDir );

        Dir->ObjectControl = 0;

        if(
            ErrorCondition( free( Dir,0 )       , oC_ErrorCode_ReleaseError         )
         && ErrorCondition( result == FR_OK     , FatFsResultToErrorCode(result)    )
            )
        {
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_readdir( oC_FatFs_Context_t Context , oC_Dir_t Dir , oC_FileInfo_t * outFileInfo )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( IsDirCorrect(Dir)           , oC_ErrorCode_ObjectNotCorrect         )
     && ErrorCondition( isram(outFileInfo)          , oC_ErrorCode_OutputAddressNotInRAM    )
        )
    {
        oC_Struct_Define(FILINFO, finfo);

        FRESULT result = f_readdir( &Dir->FatFsDir, &finfo );

        if( ErrorCondition( result == FR_OK, FatFsResultToErrorCode(result) ) )
        {
            strcpy(outFileInfo->_nameBuffer,finfo.fname);

            outFileInfo->FileType   = (finfo.fattrib & AM_DIR) != 0 ? oC_FileSystem_FileType_Directory : oC_FileSystem_FileType_File;
            outFileInfo->Name       = outFileInfo->_nameBuffer;
            outFileInfo->Size       = (oC_MemorySize_t)finfo.fsize;
            outFileInfo->Timestamp  = FatFsTimeToTimestamp(finfo.ftime, finfo.fdate);
            errorCode               = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_stat( oC_FatFs_Context_t Context , const char * Path , oC_FileInfo_t * outFileInfo)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect             )
     && ErrorCondition( isaddresscorrect(Path)      , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( isram(outFileInfo)          , oC_ErrorCode_OutputAddressNotInRAM        )
     && ErrorCondition( strlen(Path) > 0            , oC_ErrorCode_StringIsEmpty                )
        )
    {
        oC_Struct_Define( FILINFO, finfo );
        FRESULT result = f_stat( &Context->FatFsContext, Path, &finfo );

        if( ErrorCondition( result == FR_OK     , FatFsResultToErrorCode(result)    ) )
        {
            strcpy(outFileInfo->_nameBuffer,finfo.fname);

            outFileInfo->FileType   = (finfo.fattrib & AM_DIR) != 0 ? oC_FileSystem_FileType_Directory : oC_FileSystem_FileType_File;
            outFileInfo->Name       = outFileInfo->_nameBuffer;
            outFileInfo->Size       = (oC_MemorySize_t)finfo.fsize;
            outFileInfo->Timestamp  = FatFsTimeToTimestamp(finfo.ftime, finfo.fdate);
            errorCode = oC_ErrorCode_None;
        }
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_unlink( oC_FatFs_Context_t Context , const char * Path)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect             )
     && ErrorCondition( isaddresscorrect(Path)      , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( strlen(Path) > 0            , oC_ErrorCode_StringIsEmpty                )
        )
    {
        errorCode = FatFsResultToErrorCode( f_unlink( &Context->FatFsContext, Path) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_rename( oC_FatFs_Context_t Context , const char * OldName , const char * NewName)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect             )
     && ErrorCondition( isaddresscorrect(OldName)   , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( isaddresscorrect(NewName)   , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( strlen(OldName) > 0         , oC_ErrorCode_StringIsEmpty                )
     && ErrorCondition( strlen(NewName) > 0         , oC_ErrorCode_StringIsEmpty                )
        )
    {
        errorCode = FatFsResultToErrorCode( f_rename( &Context->FatFsContext, OldName, NewName ) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_chmod( oC_FatFs_Context_t Context , const char * Path, oC_FileSystem_FileAttributes_t Attributes , oC_FileSystem_FileAttributes_t Mask)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect             )
     && ErrorCondition( isaddresscorrect(Path)      , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( strlen(Path) > 0            , oC_ErrorCode_StringIsEmpty                )
        )
    {
        BYTE value = 0, mask = 0;

        value |= ( Attributes & oC_FileSystem_FileAttributes_Archive ) != 0 ? AM_ARC    : 0;
        value |= ( Attributes & oC_FileSystem_FileAttributes_Hidden  ) != 0 ? AM_HID    : 0;
        value |= ( Attributes & oC_FileSystem_FileAttributes_ReadOnly) != 0 ? AM_RDO    : 0;
        value |= ( Attributes & oC_FileSystem_FileAttributes_System  ) != 0 ? AM_SYS    : 0;

        mask  |= ( Mask & oC_FileSystem_FileAttributes_Archive ) != 0 ? AM_ARC    : 0;
        mask  |= ( Mask & oC_FileSystem_FileAttributes_Hidden  ) != 0 ? AM_HID    : 0;
        mask  |= ( Mask & oC_FileSystem_FileAttributes_ReadOnly) != 0 ? AM_RDO    : 0;
        mask  |= ( Mask & oC_FileSystem_FileAttributes_System  ) != 0 ? AM_SYS    : 0;

        errorCode = FatFsResultToErrorCode( f_chmod( &Context->FatFsContext, Path, value, mask) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_utime( oC_FatFs_Context_t Context , const char * Path , oC_Timestamp_t Timestamp )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect             )
     && ErrorCondition( isaddresscorrect(Path)      , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( strlen(Path) > 0            , oC_ErrorCode_StringIsEmpty                )
        )
    {
        oC_Struct_Define( FILINFO, finfo );

        FatFsTimeFromTimestamp( &finfo.fdate, &finfo.ftime, Timestamp );

        errorCode = FatFsResultToErrorCode( f_utime( &Context->FatFsContext, Path, &finfo) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
oC_ErrorCode_t oC_FatFs_mkdir( oC_FatFs_Context_t Context , const char * Path)
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    if(
        ErrorCondition( IsContextCorrect(Context)   , oC_ErrorCode_ObjectNotCorrect             )
     && ErrorCondition( isaddresscorrect(Path)      , oC_ErrorCode_WrongAddress                 )
     && ErrorCondition( strlen(Path) > 0            , oC_ErrorCode_StringIsEmpty                )
        )
    {
        errorCode = FatFsResultToErrorCode( f_mkdir( &Context->FatFsContext, Path ) );
    }

    return errorCode;
}

//==========================================================================================================================================
//==========================================================================================================================================
bool oC_FatFs_DirExists( oC_FatFs_Context_t Context , const char * Path)
{
    bool exist = false;

    if( IsContextCorrect(Context) && isaddresscorrect(Path) && strlen(Path) > 0 )
    {
        if( strcmp(Path,"/") == 0 )
        {
            exist = true;
        }
        else
        {
            oC_Struct_Define( FILINFO, finfo );
            FRESULT result = f_stat( &Context->FatFsContext, Path, &finfo );
            exist = result == FR_OK && (finfo.fattrib & AM_DIR) != 0;
        }
    }

    return exist;
}

#undef  _________________________________________FUNCTIONS__________________________________________________________________________________

/** ========================================================================================================================================
 *
 *              The section with local functions
 *
 *  ======================================================================================================================================*/
#define _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________

//==========================================================================================================================================
/**
 * @brief converts fat fs result to system error codes
 */
//==========================================================================================================================================
static oC_ErrorCode_t FatFsResultToErrorCode( FRESULT Result )
{
    oC_ErrorCode_t errorCode = oC_ErrorCode_ImplementError;

    switch(Result)
    {
        case FR_OK:
            errorCode = oC_ErrorCode_None;
            break;
        case FR_INT_ERR:             /* (2) Assertion failed */
            errorCode = oC_ErrorCode_InternalDataAreDamaged;
            break;
        case FR_NO_FILE:             /* (4) Could not find the file */
        case FR_NO_PATH:             /* (5) Could not find the path */
        case FR_INVALID_NAME:
            errorCode = oC_ErrorCode_PathNotCorrect;
            break;
        case FR_DENIED:
            errorCode = oC_ErrorCode_PermissionDenied;
            break;
        case FR_EXIST:
            errorCode = oC_ErrorCode_FileAlreadyExists;
            break;
        case FR_INVALID_OBJECT:
            errorCode = oC_ErrorCode_ObjectNotCorrect;
            break;
        case FR_WRITE_PROTECTED:
            errorCode = oC_ErrorCode_WritingNotPermitted;
            break;
        case FR_INVALID_DRIVE:
            errorCode = oC_ErrorCode_WrongParameters;
            break;
        case FR_NOT_READY:
        case FR_NOT_ENABLED:
            errorCode = oC_ErrorCode_ModuleNotStartedYet;
            break;
        case FR_TIMEOUT:
            errorCode = oC_ErrorCode_Timeout;
            break;
        case FR_LOCKED:
            errorCode = oC_ErrorCode_FileIsLocked;
            break;
        case FR_NOT_ENOUGH_CORE:
            errorCode = oC_ErrorCode_CannotAllocateLfnBuffer;
            break;
        case FR_TOO_MANY_OPEN_FILES:
            errorCode = oC_ErrorCode_TooManyOpenFiles;
            break;
        case FR_INVALID_PARAMETER:
            errorCode = oC_ErrorCode_WrongParameters;
            break;
        case FR_NO_FILESYSTEM:
        case FR_MKFS_ABORTED:
        case FR_DISK_ERR:
        default:
            errorCode = oC_ErrorCode_CannotInitializeModule;
            break;
    }

    return errorCode;
}

//==========================================================================================================================================
/**
 * @brief returns true if context is correct
 */
//==========================================================================================================================================
static bool IsContextCorrect( oC_FatFs_Context_t Context )
{
    return isram(Context) && oC_CheckObjectControl(Context, oC_ObjectId_FatFs, Context->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief returns true if file is correct
 */
//==========================================================================================================================================
static bool IsFileCorrect( oC_File_t File )
{
    return isram(File) && oC_CheckObjectControl(File, oC_ObjectId_FatFsFile, File->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief returns true if dir is correct
 */
//==========================================================================================================================================
static bool IsDirCorrect( oC_Dir_t Dir )
{
    return isram(Dir) && oC_CheckObjectControl(Dir, oC_ObjectId_FatFsDir, Dir->ObjectControl);
}

//==========================================================================================================================================
/**
 * @brief converts FATFS time to the system time
 */
//==========================================================================================================================================
static oC_Timestamp_t FatFsTimeToTimestamp( WORD Date, WORD Time )
{
    return 0;
}

//==========================================================================================================================================
/**
 * @brief converts system time to FATFS time
 */
//==========================================================================================================================================
static void FatFsTimeFromTimestamp( WORD * outDate, WORD * outTime , oC_Timestamp_t Timestamp )
{
    *outDate = 0;
    *outTime = 0;
}

#undef  _________________________________________LOCAL_FUNCTIONS____________________________________________________________________________
