############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/04/27
##  Description:    makefile for the portable space
##
############################################################################################################################################


##============================================================================================================================
##                                          
##              PREPARATION VARIABLES               
##                                          
##============================================================================================================================
ifeq ($(PROJECT_DIR),)
    PROJECT_DIR = ../../..
endif

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
SYSTEM_CORE_OPTIMALIZE             = O0
SYSTEM_CORE_WARNING_FLAGS          = -Wall -Werror
SYSTEM_CORE_CSTD                   = gnu11
SYSTEM_CORE_DEFINITIONS            = oC_CORE_SPACE

##============================================================================================================================
##                                          
##          INCLUDE MAKEFILES       
##                                          
##============================================================================================================================
include $(GLOBAL_DEFS_MK_FILE_PATH)
include $(PORTABLE_SPACE_MK_FILE_PATH)

##============================================================================================================================
##                                          
##              EXTENSIONS CONFIGURATION
##                                          
##============================================================================================================================
C_EXT                       = c
OBJ_EXT                     = o

##============================================================================================================================
##                                          
##              PREPARATION OF DIRECTORIES LIST             
##                                          
##============================================================================================================================
SYSTEM_CORE_SOURCES_DRIVERS_SUBDIRS    = $(filter %/, $(wildcard $(SYSTEM_CORE_SOURCES_DRIVERS_DIR)/*/))
SYSTEM_CORE_SOURCES_TERMINALS_SUBDIRS  = $(filter %/, $(wildcard $(SYSTEM_CORE_SOURCES_TERMINALS_DIR)/*/))
SYSTEM_CORE_SOURCES_FS_SUBDIRS         = $(filter %/, $(wildcard $(SYSTEM_CORE_SOURCES_FS_DIR)/*/))
SYSTEM_CORE_SOURCES_GUI_SUBDIRS        = $(filter %/, $(wildcard $(SYSTEM_CORE_SOURCES_GUI_DIR)/*/))
SYSTEM_CORE_SOURCES_PROTOCOLS_SUBDIRS  = $(filter %/, $(wildcard $(SYSTEM_CORE_SOURCES_PROTOCOLS_DIR)/*/))
SYSTEM_CORE_SOURCES_DIRS               = $(SYSTEM_CORE_SOURCES_DRIVERS_SUBDIRS) \
                                         $(SYSTEM_CORE_SOURCES_DRIVERS_DIR) \
                                         $(SYSTEM_CORE_SOURCES_TERMINALS_SUBDIRS) \
                                         $(SYSTEM_CORE_SOURCES_TERMINALS_DIR) \
                                         $(SYSTEM_CORE_SOURCES_FS_SUBDIRS) \
                                         $(SYSTEM_CORE_SOURCES_FS_DIR) \
                                         $(SYSTEM_CORE_SOURCES_GUI_SUBDIRS) \
                                         $(SYSTEM_CORE_SOURCES_GUI_DIR) \
                                         $(SYSTEM_CORE_SOURCES_KERNEL_DIR) \
                                         $(SYSTEM_CORE_SOURCES_DIR) \
                                         $(SYSTEM_CORE_SOURCES_BOOT_DIR) \
                                         $(SYSTEM_CORE_SOURCES_SCALLS_DIR) \
                                         $(SYSTEM_CORE_SOURCES_POSIX_DIR) \
                                         $(SYSTEM_CORE_SOURCES_NET_DIR) \
                                         $(SYSTEM_CORE_SOURCES_PROTOCOLS_DIR) \
                                         $(SYSTEM_CORE_SOURCES_PROTOCOLS_SUBDIRS) \

SYSTEM_CORE_INCLUDES_DRIVERS_SUBDIRS   = $(filter %/, $(wildcard $(SYSTEM_CORE_INCLUDES_DRIVERS_DIR)/*/))
SYSTEM_CORE_INCLUDES_TERMINALS_SUBDIRS = $(filter %/, $(wildcard $(SYSTEM_CORE_INCLUDES_TERMINALS_DIR)/*/))
SYSTEM_CORE_INCLUDES_FS_SUBDIRS        = $(filter %/, $(wildcard $(SYSTEM_CORE_INCLUDES_FS_DIR)/*/))
SYSTEM_CORE_INCLUDES_GUI_SUBDIRS       = $(filter %/, $(wildcard $(SYSTEM_CORE_INCLUDES_GUI_DIR)/*/))
SYSTEM_CORE_INCLUDES_PROTOCOLS_SUBDIRS = $(filter %/, $(wildcard $(SYSTEM_CORE_INCLUDES_PROTOCOLS_DIR)/*/))
SYSTEM_CORE_INCLUDES_DIRS              = $(SYSTEM_CORE_INCLUDES_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_DRIVERS_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_DRIVERS_SUBDIRS) \
                                         $(SYSTEM_CORE_INCLUDES_TERMINALS_SUBDIRS) \
                                         $(SYSTEM_CORE_INCLUDES_TERMINALS_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_FS_SUBDIRS) \
                                         $(SYSTEM_CORE_INCLUDES_FS_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_GUI_SUBDIRS) \
                                         $(SYSTEM_CORE_INCLUDES_GUI_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_KERNEL_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_BOOT_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_SCALLS_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_POSIX_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_NET_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_PROTOCOLS_DIR) \
                                         $(SYSTEM_CORE_INCLUDES_PROTOCOLS_SUBDIRS) \
                                         $(SYSTEM_CONFIG_DIR) \
                                         $(SYSTEM_CONFIG_MACHINE_DIR) \
                                         $(SYSTEM_LIBRARIES_INCLUDES_DIRS) \
                                         $(SYSTEM_PORTABLE_INCLUDES_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_LLD_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_LLD_SUBDIRS) \
                                         $(SYSTEM_PORTABLE_INCLUDES_PRODUCENT_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_MACHINE_DIR) \
                                         $(SYSTEM_PORTABLE_INCLUDES_MCS_CORE_DIR)
                                    

##============================================================================================================================
##                                          
##              ADD EXTRA DEFINITIONS        
##                                          
##============================================================================================================================
SYSTEM_CORE_DEFINITIONS               += $(BUILD_VERSION_DEFINITIONS) $(ARCHITECTURE_DEFINITIONS) __POSIX_VISIBLE=200809
                                  
##============================================================================================================================
##                                          
##              PREPARATION OF SOURCE FILES         
##                                          
##============================================================================================================================
SYSTEM_CORE_SOURCES                    = $(foreach src,$(foreach subdir, $(SYSTEM_CORE_SOURCES_DIRS), $(wildcard $(subdir)/*.c)),$(subst //,/,$(src)))
SYSTEM_CORE_ASM_SOURCES                = $(MACHINE_STARTUP_FILE_PATH)
##============================================================================================================================
##                                          
##              PREPARATION OF COMPILER ARGUMENTS   
##                                          
##============================================================================================================================
SYSTEM_CORE_INCLUDES                   = $(foreach dir,$(SYSTEM_CORE_INCLUDES_DIRS),-I$(dir))
SYSTEM_CORE_DEFINITIONS_FLAGS          = $(foreach def,$(SYSTEM_CORE_DEFINITIONS),-D$(def))
SYSTEM_CORE_CFLAGS                     = -c -g -std=$(SYSTEM_CORE_CSTD) $(SYSTEM_CORE_WARNING_FLAGS) -$(SYSTEM_CORE_OPTIMALIZE) $(CPUCONFIG_CFLAGS) \
                                         -fdata-sections -ffunction-sections -fstack-usage $(ADDITIONAL_CFLAGS) 
SYSTEM_CORE_AFLAGS                     = -c -g -ggdb3 $(CPUCONFIG_AFLAGS)

##============================================================================================================================
##                                          
##              TARGETS 
##                                          
##============================================================================================================================
print_core_info:
	@echo "=================================================================================================================="
	@echo "                                           Building Core Space"
	@echo "=================================================================================================================="
	@echo " "
	@echo "Core includes list:"
	@echo -e " $(foreach inc,$(SYSTEM_CORE_INCLUDES_DIRS), include directory: $(inc)\n)"
	@echo "Core sources list:"
	@echo -e " $(foreach src,$(SYSTEM_CORE_SOURCES), source file: $(src)\n)"
	@echo "Core asm sources list:"
	@echo -e " $(foreach src,$(SYSTEM_CORE_ASM_SOURCES), source file: $(src)\n)"
	@echo "Creating directories..."
	@$(MKDIR) $(MACHINE_OUTPUT_DIR)
build_core_dependences:
	@echo "Creating dependeces..."
	@$(MKDEP) -f core.mak $(SYSTEM_CORE_INCLUDES) -- $(SYSTEM_CORE_CFLAGS) -- $(SYSTEM_CORE_SOURCES) -o .$(OBJ_EXT)
    
build_core_objects:
	@echo "Building core..."
	@$(CC) $(SYSTEM_CORE_SOURCES) $(SYSTEM_CORE_INCLUDES) $(SYSTEM_CORE_CFLAGS) $(SYSTEM_CORE_DEFINITIONS_FLAGS)
	@$(AS) $(SYSTEM_CORE_ASM_SOURCES) $(SYSTEM_CORE_AFLAGS)
    
archieve_core:
	@echo "Archieve core space..."
	@$(AR) rcs $(CORE_SPACE_LIB_FILE_PATH) *.o
	@$(RM) *.o
	
core_move_stack_analyze_result:
	@echo "Moving stack analysis files to the machine output directory..."
	@$(MV) *.su $(MACHINE_OUTPUT_DIR)
    
core_status:
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Core Space building success"
	@echo ""
    
build_core: print_core_info clean_core_output build_core_objects archieve_core core_move_stack_analyze_result core_status
    
clean_core_output:
	@echo "Removing old core library file..."
	$(RM) -f $(CORE_SPACE_LIB_FILE_PATH)
	
clean_core:
	@echo "=================================================================================================================="
	@echo "                                           Cleaning Core Space"
	@echo "=================================================================================================================="
	@echo " "
	$(RM) -f *.o
	$(RM) -f *.a
	$(RM) -f $(CORE_SPACE_LIB_FILE_PATH)
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Core Space cleaning success"
	@echo ""
    
