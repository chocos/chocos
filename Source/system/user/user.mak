############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/04/27
##  Description:    makefile for the portable space
##
############################################################################################################################################


##============================================================================================================================
##                                          
##              PREPARATION VARIABLES               
##                                          
##============================================================================================================================
ifeq ($(PROJECT_DIR),)
    PROJECT_DIR = ../../..
endif

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================
USER_OPTIMALIZE             = Os
USER_WARNING_FLAGS          = -Wall
USER_CSTD                   = gnu11
USER_DEFINITIONS            = oC_USER_SPACE

##============================================================================================================================
##                                          
##          INCLUDE MAKEFILES       
##                                          
##============================================================================================================================
include $(GLOBAL_DEFS_MK_FILE_PATH)
include $(CORE_SPACE_MK_FILE_PATH)

##============================================================================================================================
##                                          
##              EXTENSIONS CONFIGURATION
##                                          
##============================================================================================================================
C_EXT                       = c
OBJ_EXT                     = o

##============================================================================================================================
##                                          
##              PREPARATION OF DIRECTORIES LIST             
##                                          
##============================================================================================================================
USER_SOURCES_LIBRARIES_SUBDIRS  = $(filter %/, $(wildcard $(USER_LIBRARIES_DIR)/*/))
USER_SOURCES_DIRS               = $(USER_LIBRARIES_DIR) \
                                  $(USER_SOURCES_LIBRARIES_SUBDIRS) \
                                  $(USER_SOURCES_DIR) \

USER_INCLUDES_LIBRARIES_SUBDIRS = $(filter %/, $(wildcard $(USER_LIBRARIES_DIR)/*/))
USER_INCLUDES_DIRS              = $(SYSTEM_CORE_INCLUDES_DIR) \
                                  $(SYSTEM_CORE_INCLUDES_TERMINALS_SUBDIRS) \
                                  $(SYSTEM_CORE_INCLUDES_TERMINALS_DIR) \
                                  $(SYSTEM_CORE_INCLUDES_FS_SUBDIRS) \
                                  $(SYSTEM_CORE_INCLUDES_FS_DIR) \
                                  $(SYSTEM_CORE_INCLUDES_GUI_SUBDIRS) \
                                  $(SYSTEM_CORE_INCLUDES_GUI_DIR) \
                                  $(SYSTEM_CORE_INCLUDES_SCALLS_DIR) \
                                  $(SYSTEM_CORE_INCLUDES_POSIX_DIR) \
                                  $(SYSTEM_CONFIG_DIR) \
                                  $(SYSTEM_LIBRARIES_INCLUDES_DIRS) \
                                  $(USER_LIBRARIES_DIR) \
                                  $(USER_INCLUDES_LIBRARIES_SUBDIRS) \
                                  $(USER_INCLUDES_DIR) \

##============================================================================================================================
##                                          
##              ADD EXTRA DEFINITIONS        
##                                          
##============================================================================================================================
USER_DEFINITIONS                += $(BUILD_VERSION_DEFINITIONS) $(ARCHITECTURE_DEFINITIONS) __POSIX_VISIBLE=200809
                                  
##============================================================================================================================
##                                          
##              PREPARATION OF SOURCE FILES         
##                                          
##============================================================================================================================
USER_SOURCES                    = $(foreach src,$(foreach subdir, $(USER_SOURCES_DIRS), $(wildcard $(subdir)/*.c)),$(subst //,/,$(src)))

##============================================================================================================================
##                                          
##              PREPARATION OF COMPILER ARGUMENTS   
##                                          
##============================================================================================================================
USER_INCLUDES                   = $(foreach dir,$(USER_INCLUDES_DIRS),-I$(dir))
USER_DEFINITIONS_FLAGS          = $(foreach def,$(USER_DEFINITIONS),-D$(def))
USER_CFLAGS                     = -c -g -fPIC -std=$(USER_CSTD) $(USER_WARNING_FLAGS) -$(USER_OPTIMALIZE) $(CPUCONFIG_CFLAGS) \
                                  -fdata-sections -ffunction-sections -nodefaultlibs -fstack-usage -Wno-builtin-macro-redefined -Wno-builtin-declaration-mismatch -fno-builtin \

##============================================================================================================================
##                                          
##              TARGETS 
##                                          
##============================================================================================================================
print_user_info:
	@echo "=================================================================================================================="
	@echo "                                           Building User Space"
	@echo "=================================================================================================================="
	@echo " "
	@echo "User includes list:"
	@echo -e " $(foreach inc,$(USER_INCLUDES_DIRS), include directory: $(inc)\n)"
	@echo "User sources list:"
	@echo -e " $(foreach src,$(USER_SOURCES), source file: $(src)\n)"
	@echo "Creating directories..."
	@$(MKDIR) $(MACHINE_OUTPUT_DIR)
build_user_dependences:
	@echo "Creating dependeces..."
	@$(MKDEP) -f $(USER_SPACE_MK_FILE_NAME) $(USER_INCLUDES) -- $(USER_CFLAGS) -- $(USER_SOURCES) -o .$(OBJ_EXT)
    
build_user_objects:
	@echo "Building user..."
	@$(CC) $(USER_SOURCES) $(USER_INCLUDES) $(USER_CFLAGS) $(USER_DEFINITIONS_FLAGS)
    
archieve_user:
	@echo "Preparing user space..."
	@$(LD) -nostdlib -shared -o $(USER_SPACE_LIB_FILE_PATH) *.o
	@$(RM) -rf *.o
	
user_move_stack_analyze_result:
	@echo "Moving stack analysis files to the machine output directory..."
	@$(MV) *.su $(MACHINE_OUTPUT_DIR)
    
user_status:
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     User Space building success"
	@echo ""
    
build_user: print_user_info clean_user_output build_user_objects archieve_user user_move_stack_analyze_result user_status
    
clean_user_output:
	@echo "Removing old user space library file..."
	$(RM) -f $(USER_SPACE_LIB_FILE_PATH)	
	
clean_user:
	@echo "=================================================================================================================="
	@echo "                                           Cleaning User Space"
	@echo "=================================================================================================================="
	@echo " "
	$(RM) -f *.o
	$(RM) -f *.a
	$(RM) -f $(USER_SPACE_LIB_FILE_PATH)
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     User Space cleaning success"
	@echo ""
    
