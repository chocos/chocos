#!/bin/sh
#================================================================================================================================
#	Author:			Patryk Kubiak
#	Date:			2016/07/2
#	Description:	script for selection architecture
#================================================================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR!                                                     ="
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

Usage()
{
    printf "\033[1;31;40m"
    echo "==========================================================================   "
    echo "=                                                                            "
    echo "=      Usage:                                                                "
    echo "=         $0 MACHINE_NAME                                                    "
    echo "=				                                               "
    echo "=             where:                                                         "
    echo "=                  MACHINE_NAME is the name of the machine (stm32f746ngh6)   "
    echo "=                                                                            "
    echo "==========================================================================   "
    printf "\033[0;40m"
    exit 1
}

MachineFound()
{
    SetForegroundColor "green"
    echo "--------------------------------------------------------------------------"
    echo "                          Machine has been found!                         "
    echo "--------------------------------------------------------------------------"
    echo "              Found data:                                                 "
    echo "                      Machine producent:       $1                         "
    echo "                      Machine family:          $2                         "
    echo "                      Machine name:            $3                         "
    ResetAllAttributes
    . $SELECT_ARCH_FILE_PATH $1 $2 $3
}

MachineNotFound()
{
    AVAILABLE_MACHINES=`PrintAvailableMachines`
    SetForegroundColor "red"
    echo "--------------------------------------------------------------------------"
    echo "-                                                                        -"
    echo "-                                   ERROR                                -"
    echo "-                                                                        -"
    echo "-           Cannot find the machine:                                     -" 
    echo "-                            $MACHINE_TO_FIND                             "
    echo "-                                                                        -"
    echo "-           If the name is correctly given, then it does mean that       -"
    echo "-           the system does not support it yet.                          -"
    echo "--------------------------------------------------------------------------"
    echo "-                                                                        -"
    echo "- List of available machines:                                            -"
    echo "-                                                                        -"
    echo "- $AVAILABLE_MACHINES                                                     "
    echo "--------------------------------------------------------------------------"
}

PrintAvailableMachines()
{
    for PRODUCENT_DIR in `ls -d $SYSTEM_PORTABLE_SCRIPTS_DIR/*`;
    do 
        TEMP_PRODUCENT=`basename $PRODUCENT_DIR`
        
        for FAMILY_DIR in `ls -d $PRODUCENT_DIR/*`;
        do
            TEMP_FAMILY=`basename $FAMILY_DIR`
            
            for MACHINE_NAME_DIR in `ls -d $FAMILY_DIR/*`;
            do
                TEMP_MACHINE_NAME=`basename $MACHINE_NAME_DIR`
                printf "$TEMP_MACHINE_NAME,"
            done
        done
    done
}

#=====================================================================================================
#	PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR" = "" ]; then PROJECT_DIR=..; fi
CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh
SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh
SKIP_CHECKING_ARCH="true"
MACHINE_TO_FIND=$1

#=====================================================================================================
#	PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#	IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH

#=====================================================================================================
#	PORTABLE PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $SYSTEM_PORTABLE_SCRIPTS_DIR   ]; then PathDoesNotExist $SYSTEM_PORTABLE_SCRIPTS_DIR;     fi

#=====================================================================================================
#	ARGUMENTS VERIFICATION
#=====================================================================================================
if   [   "$1" = "" ]; then Usage;                 
elif [ ! "$2" = "" ]; then Usage; fi

#=====================================================================================================
#	Searching architecture
#=====================================================================================================
for PRODUCENT_DIR in `ls -d $SYSTEM_PORTABLE_SCRIPTS_DIR/*`;
do 
    TEMP_PRODUCENT=`basename $PRODUCENT_DIR`
    
    for FAMILY_DIR in `ls -d $PRODUCENT_DIR/*`;
    do
        TEMP_FAMILY=`basename $FAMILY_DIR`
        
        for MACHINE_NAME_DIR in `ls -d $FAMILY_DIR/*`;
        do
            TEMP_MACHINE_NAME=`basename $MACHINE_NAME_DIR`
            
            if [ "$TEMP_MACHINE_NAME" = "$MACHINE_TO_FIND" ];
            then
                MachineFound $TEMP_PRODUCENT $TEMP_FAMILY $TEMP_MACHINE_NAME
            fi  
        done
    done
done

#=====================================================================================================
#       MACHINE NOT FOUND
#=====================================================================================================
if [ ! "$MACHINE_SELECTED" = "true" ]; then
    MachineNotFound
fi