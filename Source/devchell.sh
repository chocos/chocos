#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/04
#   Description:    Script for interactions in the linux environmental
#================================================================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR!                                                     ="
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

Help()
{
    SetForegroundColor "white"
    echo "Welcome to the simple development chell!"
    echo "It is designed for the managing ChocoOS development under UNIX systems"
    echo "Here you can find list of available commands:"
    echo "  help                - this help                                             "
    echo "  exit                - closes the devchell terminal                          "
    echo "  connect             - connects to the board                                 "
    echo "  flash               - flashes to the board                                  "
    echo "  build               - builds the project                                    "
}

UnknownCommand()
{
    SetForegroundColor "red"
    echo "Command $1 is not recognized!"
}

ExitCommand()
{
    if [ ! $CONNECT_PID = "" ]; then 
        SetForegroundColor "blue"
        echo "Closing connection with the debugger ($$)..."
        pkill -f openocd 2>&1 > /dev/null
    fi
    SetForegroundColor "green"
    echo "Closing the terminal... See you soon $USER!"
    ResetAllAttributes
}

#=====================================================================================================
#	HELPER FUNCTIONS
#=====================================================================================================
IsMachineSelected()
{
    if [ "$MACHINE_SELECTED" = "true" ]; then
        return 0;
    else
        return 1;
    fi
}

ExecuteCommand()
{
    # This splits full command to words (words are available in $1,$2,etc)
    set $COMMAND
    # This splits the function argument
    COMMAND=$1
    
      if [ "$1" = "help"              ]; then Help; 
    elif [ "$1" = "connect"           ]; then Connect $2 $3 $4; 
    elif [ "$1" = "flash"             ]; then Flash $2 $3 $4; 
    elif [ "$1" = "build"             ]; then Build $2 $3 $4; 
    elif [ "$1" = "exit"              ]; then ExitCommand;
    else UnknownCommand $COMMAND; fi
}

SearchMachine()
{
    . $SELECT_ARCH_FILE_PATH $1
}

Connect()
{
    SetForegroundColor "blue" 
    echo "Connecting to the board by $CONNECT_FILE_PATH script..."
    $CONNECT_FILE_PATH $1 $2 $3 &
    CONNECT_PID=$!
    sleep 3
}

Flash()
{
    SetForegroundColor "blue"
    echo "Flashing the board by $FLASH_FILE_PATH script ..."
    $FLASH_FILE_PATH
}

Build()
{
    chmod +x $BUILD_FILE_PATH
    SetForegroundColor "blue"
    echo "Building the project"
    SetForegroundColor "white"
    $BUILD_FILE_PATH $1 $2 $3 $4
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
PROJECT_DIR=$PWD/..
CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh
SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh
SELECT_ARCH_FILE_PATH=$PROJECT_DIR/Source/select_arch.sh
FLASH_MACHINE="false"
SKIP_CHECKING_ARCH="true"

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH

#=====================================================================================================
#    READ ARCHITECTURE
#=====================================================================================================
while ! IsMachineSelected ; do
    SetForegroundColor "white"
    printf "Please enter machine name: "
    read MACHINE_NAME
    
    SearchMachine $MACHINE_NAME
done

#=====================================================================================================
#    IMPORT PATHS
#=====================================================================================================
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#   OTHER PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $CONNECT_FILE_PATH     ]; then PathDoesNotExist $CONNECT_FILE_PATH;       fi
if [ ! -e $FLASH_FILE_PATH       ]; then PathDoesNotExist $FLASH_FILE_PATH;         fi
if [ ! -e $BUILD_FILE_PATH       ]; then PathDoesNotExist $BUILD_FILE_PATH;         fi

#=====================================================================================================
#    HANDLE COMMANDS
#=====================================================================================================
SetForegroundColor "white"
echo "Hello $USER!"
echo "Type 'exit' to close the development chell or 'help' for more info"

while [ ! "$COMMAND" = "exit" ]; do
    SetForegroundColor "yellow"
    printf "$USER@$MACHINE_NAME:/$ "
    read COMMAND
    ExecuteCommand "$COMMAND"
done

#=====================================================================================================
#    ADD ALL PERMISSIONS TO ALL FILES
#=====================================================================================================
if IsRootUser; then 
    echo "Adding 777 permissions to all files inside the project"
    find $PROJECT_DIR -exec sudo chmod 777 {} \;
fi
