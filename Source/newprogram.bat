::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/11/03
::	Description:	Script for generating new program. 
::================================================================================================================================
@echo off
::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=%cd:\=/%/..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat
SET PROGRAM_DIR_NAME=%1

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Required paths verification
::----------------------------------------------------------
IF NOT EXIST %PROGRAM_TEMPLATES_DIR% GOTO ProgramTemplatesDirNotExist
IF NOT EXIST %TOOLS_DIR% GOTO ToolsDirNotExist
IF NOT EXIST %TMPLPARS_FILE_PATH% GOTO TmplparsNotExist

::----------------------------------------------------------
:: Check if user was set
::----------------------------------------------------------
IF NOT DEFINED CHOCOOS_USER_DEFINED GOTO UserNotSet
IF "CHOCOOS_USER_DEFINED"=="FALSE"  GOTO UserNotSet

::----------------------------------------------------------
:: Preparing variables
::----------------------------------------------------------
SET PROGRAM_NAME=%1
IF NOT "%2"=="" (
    SET /A STACK_SIZE="%2"*1 
) ELSE (
    SET STACK_SIZE=0
)
IF NOT "%3"=="" (
    SET TARGET_SPACE=%3
) ELSE (
    SET TARGET_SPACE=CORE
)
IF NOT "%4"=="" (
    SET PROGRAM_DIR=%4
) ELSE (
    SET PROGRAM_DIR=USER
)

::----------------------------------------------------------
:: Checking arguments
::----------------------------------------------------------
IF "%1"==""	GOTO Usage
IF NOT "%2"=="" IF NOT "%2" == "0" IF %STACK_SIZE% EQU 0 GOTO Usage
IF NOT "%3"=="" IF NOT "%3"=="USER" IF NOT "%3"=="CORE" GOTO Usage
IF NOT "%4"=="" IF NOT "%4"=="USER" IF NOT "%4"=="SYSTEM" GOTO Usage
IF NOT "%5"=="" IF NOT "%5"=="CBIN" IF NOT "%5"=="ELF" GOTO Usage

::----------------------------------------------------------
:: Preparing paths
::----------------------------------------------------------
IF "%PROGRAM_DIR%" == "SYSTEM" (
    SET OUTPUT_DIR=%SYSTEM_PROGRAMS_DIR%/%PROGRAM_DIR_NAME%
) ELSE (
    SET OUTPUT_DIR=%USER_PROGRAMS_DIR%/%PROGRAM_DIR_NAME%
) 

IF NOT EXIST %OUTPUT_DIR% mkdir "%OUTPUT_DIR%"

::----------------------------------------------------------
:: Parsing each file in the template directory
::----------------------------------------------------------
:ParseFiles
for %%f in (%PROGRAM_TEMPLATES_DIR%\*.*) do (
    call :ParseFile %%f %OUTPUT_DIR%/%%~nxf %%~nxf
)
GOTO Success
exit /b

:ParseFile
echo Parsing file %3...
call "%TMPLPARS_FILE_PATH%" "%1" "%2" NAME=%CHOCOOS_USER_NAME% SURNAME=%CHOCOOS_USER_SURNAME% PROGRAM_NAME=%PROGRAM_NAME% SPACE=%TARGET_SPACE% STACK_SIZE=%STACK_SIZE% EMAIL=%CHOCOOS_USER_EMAIL% FILE_TYPE=%FILE_TYPE%
exit /b

GOTO Success
::----------------------------------------------------------
:: Messages
::----------------------------------------------------------
:Success
echo ==========================================================================
echo          The program %PROGRAM_NAME% successfully created
GOTO EOF

:TmplparsNotExist
echo ==========================================================================
echo =                                                                        =
echo =  ERROR!                                                                =
echo =          Tmplpars file does not exist!                                 =
echo =          file: '%TMPLPARS_FILE_PATH%'                                 
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:UserNotSet
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target user data not set
echo = 			run set_user.bat first
echo =
echo =========================================================================
GOTO EOF

:ToolsDirNotExist
echo ==========================================================================
echo =                                                                        =
echo =  ERROR!                                                                =
echo =          Tools dir does not exist!                                     =
echo =          dir: '%TOOLS_DIR%'                                 
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:ProgramTemplatesDirNotExist
echo ==========================================================================
echo =                                                                        =
echo =  ERROR!                                                                =
echo =          Program templates dir does not exist!                         =
echo =          dir: '%PROGRAM_TEMPLATES_DIR%'                                 
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =                                                                        =
echo =  ERROR!                                                                =
echo =          Scripts for setting paths not exists!                         =
echo =          file: '%SET_PATHS_FILE_PATH%'                                 
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 PROGRAM_NAME [STACK_SIZE] [TARGET_SPACE] [PROGRAM_DIR] [FILE_TYPE]
echo =
echo =           where:
echo =              PROGRAM_NAME    - Name of the program to create
echo =              STACK_SIZE      - Size of main thread stack in [B]ytes
echo =              TARGET_SPACE    - Target space - set it to USER or CORE
echo =              PROGRAM_DIR     - Directory of the program - SYSTEM/USER
echo =              FILE_TYPE       - (only USER) target file of the program - CBIN/ELF
echo = 
echo ==========================================================================

:EOF
