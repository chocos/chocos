::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	Build script for programs
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Check architecture
::----------------------------------------------------------
IF NOT DEFINED MACHINE_SELECTED GOTO ArchitectureNotSelected
IF "%MACHINE_SELECTED%"=="FALSE" GOTO ArchitectureNotSelected

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=%cd:\=/%/..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Default values
::----------------------------------------------------------
SET TARGET=build_program
SET PROGRAM_TYPE=%1
SET PROGRAM_DIR_NAME=%2
SET RESULT=0

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Check parameters
::----------------------------------------------------------
IF NOT "%1"=="user" IF NOT "%1"=="system" GOTO Usage
IF "%2"=="" GOTO Usage
IF NOT "%3"=="" IF NOT "%3"=="all" IF NOT "%3"=="clean" GOTO Usage
IF "%3"=="clean" SET TARGET=clean_portable

::----------------------------------------------------------
:: Check if program path exists
::----------------------------------------------------------
SET PROGRAM_PATH=%PROGRAM_TYPE%/programs/%PROGRAM_DIR_NAME%
IF NOT EXIST %PROGRAM_PATH% GOTO ProgramNotFound

::----------------------------------------------------------
:: Compile program
::----------------------------------------------------------
cd %PROGRAM_PATH%
make.exe -f %PROGRAM_DEFS_MK_FILE_NAME% %TARGET%
if errorlevel 1 (
	echo Build failed for %1
	SET RESULT=1
)
cd ../../..

GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	                                                                      =
echo = 	ERROR!                                                                =
echo = 			Scripts for setting paths not exists!                         =
echo =          file: '%SET_PATHS_FILE_PATH%'                                 =
echo =                                                                        =
echo ==========================================================================
GOTO EOF

:ProgramNotFound
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Program not found at %cd%/%PROGRAM_PATH%
echo =
echo ==========================================================================
GOTO EOF

:ArchitectureNotSelected
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target architecture is not selected
echo = 			run select_arch.bat first
echo =
echo =========================================================================
GOTO EOF

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 [PROGRAM_TYPE] [PROGRAM_DIR_NAME] [TARGET]
echo =				
echo =			where PROGRAM_TYPE is:
echo =					system
echo =					user
echo = 			where TARGET is:
echo = 					all
echo = 					clean
echo ==========================================================================

:EOF
exit /b %RESULT%