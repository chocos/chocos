::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2016/08/17
::	Description:	This is the script for preparation of the flash file system. 
::                  The script searches for all files stored in the flash directory
::                  and puts it in the /flash/ system path. 
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
IF NOT DEFINED PROJECT_DIR SET PROJECT_DIR=%cd:\=/%/..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::=====================================================================================================
::   PREPARE VARIABLES
::=====================================================================================================
SET FLASH_MACHINE="false"
SET ARGUMENTS=
SET NUMBER_OF_FILES=0

::=====================================================================================================
::   PATHS VERIFICATIONS
::=====================================================================================================
IF NOT EXIST "%PROJECT_DIR%" (
	CALL :PathDoesNotExist "%PROJECT_DIR%"             
)
IF NOT EXIST "%SET_PATHS_FILE_PATH%" (
	CALL :PathDoesNotExist "%SET_PATHS_FILE_PATH%"    
)

::=====================================================================================================
::    IMPORT MODULES
::=====================================================================================================
call %SET_PATHS_FILE_PATH% skip-checking-arch

::=====================================================================================================
::    OTHER PATHS VERIFICATIONS
::=====================================================================================================
IF NOT DEFINED SYSTEM_FLASH_DIR          CALL :PathNotSet "SYSTEM_FLASH_DIR"      & GOTO EOF 
IF NOT DEFINED TMPLPARS_FILE_PATH        CALL :PathNotSet "TMPLPARS_FILE_PATH"    & GOTO EOF
IF NOT DEFINED FLASHFS_TEMPLATES_DIR     CALL :PathNotSet "FLASHFS_TEMPLATES_DIR" & GOTO EOF
IF NOT EXIST   "%SYSTEM_FLASH_DIR%"      CALL :PathDoesNotExist "%SYSTEM_FLASH_DIR%"
IF NOT EXIST   "%TMPLPARS_FILE_PATH%"    CALL :PathDoesNotExist "%TMPLPARS_FILE_PATH%"
IF NOT EXIST   "%FLASHFS_TEMPLATES_DIR%" CALL :PathDoesNotExist "%FLASHFS_TEMPLATES_DIR%"

::=====================================================================================================
::    PREPARING 
::=====================================================================================================

CALL :FindFilesForFlash
GOTO EOF

:: ===========================================
:: Function for searching for files in 
:: the flash directory
:: ===========================================
:FindFilesForFlash
	SETLOCAL EnableDelayedExpansion
	for %%f in (%SYSTEM_FLASH_DIR%\*) do (
		SET FILE_NAME="%%~nxf"
		SET FILE_PATH="%%f"
		CALL :AddFileToArgumentsList !FILE_PATH! !FILE_NAME! 
	)
	
	ECHO "Found %NUMBER_OF_FILES% files"

	for %%f in (%FLASHFS_TEMPLATES_DIR%\*) do (
		SET FILE_NAME="%%~nxf"
		SET FILE_PATH="%%f"
		CALL :ParseTemplateFile !FILE_PATH! !FILE_NAME! 
	)
GOTO EOF

:: ===========================================
:: Function for adding files to the tmplpars 
:: arguments list
:: ===========================================
:AddFileToArgumentsList
	SET ARGUMENTS=%ARGUMENTS% FILE%NUMBER_OF_FILES%=[%~1]
	SET /A NUMBER_OF_FILES=%NUMBER_OF_FILES%+1
	ECHO Found %~2
GOTO EOF

:ParseTemplateFile
	SET TEMPLATE_FILE_NAME=%~2
	ECHO Parsing template file %TEMPLATE_FILE_NAME%
	%TMPLPARS_FILE_PATH% %FLASHFS_TEMPLATES_DIR%/%TEMPLATE_FILE_NAME% %SYSTEM_CORE_SOURCES_FLASHFS_DIR%/%TEMPLATE_FILE_NAME% %ARGUMENTS% NUMBER_OF_FILES=%NUMBER_OF_FILES%
GOTO EOF

ECHO "Found %NUMBER_OF_FILES% files"

GOTO EOF

::=====================================================================================================
::	MESSAGES
::=====================================================================================================
:PathDoesNotExist
echo ==============================================================
echo =                                                            =
echo = ERROR!                                                     =
echo =                                                            =
echo =  Path: '%~1'                                                 
echo =     Does not exist!                                        =
echo =                                                            =
echo ==============================================================
GOTO EOF
	
:PathNotSet
echo ==============================================================
echo =                                                            =
echo = ERROR!                                                     =
echo =                                                            =
echo =  Path named : '%~1'                                                 
echo =     is not set!                                            =
echo =                                                            =
echo ==============================================================
GOTO EOF
		
:EOF