#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/06
#   Description:    script for configuration of the environmental
#================================================================================================================================

#=====================================================================================================
#   MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR! ($0)                                                 "
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

CommandFound()
{
    SetForegroundColor "green"
    echo "Detect $1 tool"
}

LibraryFound()
{
    SetForegroundColor "green"
    echo "Detect $1 library"
}

#=====================================================================================================
#   HELPER FUNCTIONS
#=====================================================================================================
IsCommandAvailable()
{
    command -v $1 >/dev/null 2>&1
    if [ $? = 0 ]; then 
        return 0;
    else 
        return 1;
    fi
}

IsLibraryInstalled()
{
    OUT=`ldconfig -p | grep $1`
    
    if [ $? = 0 ]; then
        return 0
    else
        return 1
    fi
}

# ########################################
#   Installs package via package manager
# ########################################
InstallViaPackageManager()
{
    PACKAGE_NAME=$1
    
    if IsCommandAvailable apt-get; then
        SetForegroundColor "green"
        echo "Detect apt-get package manager, installing $PACKAGE_NAME..."
        apt-get -y install $PACKAGE_NAME
        
        if [ $? = 0 ]; then 
            SetForegroundColor "green"
            echo "$PACKAGE_NAME successfully installed"
        else
            SetForegroundColor "red"
            echo "Cannot install $PACKAGE_NAME"
        fi
    else 
        SetForegroundColor "red"
        echo "None of known package managers detected. You must install $PACKAGE_NAME on your own"
    fi 
}

# ########################################
#   Installs arm-none-eabi tools
# ########################################
InstallArmNoneEabi()
{
    SYSTEM_NAME=`uname`;
    
    if [ "$SYSTEM_NAME" = "Linux" ]; then
        SetForegroundColor "green"
        echo "Linux detected. Adding arm-none-eabi from the project"
        
        if [ $PATH = *$ARM_NONE_EABI_GCC_BIN_DIR* ]; then
            SetForegroundColor "green"
            echo "Skipping adding $ARM_NONE_EABI_GCC_BIN_DIR to the paths - it already exist"
        else 
            SetForegroundColor "blue"
            echo "Adding $ARM_NONE_EABI_GCC_BIN_DIR to the PATH"
            # Adding all needed paths
            PATH=$PATH:$ARM_NONE_EABI_GCC_BIN_DIR
        fi
    else
        SetForegroundColor "red"
        echo "Unknown system '$SYSTEM_NAME' detected. You must install arm-none-eabi-gcc tool on your own"
        exit 1
    fi
}

# ########################################
#   Installs make tools
# ########################################
InstallMake()
{
    SYSTEM_NAME=`uname`;
    
    if [ "$SYSTEM_NAME" = "Linux" ]; then
        SetForegroundColor "green"
        echo "Linux detected. Trying to install make"
        InstallViaPackageManager make
    else
        SetForegroundColor "red"
        echo "Unknown system '$SYSTEM_NAME' detected. You must install 'make' tool on your own"
        exit 1
    fi
}

# ########################################
#   Installs openocd tools
# ########################################
InstallOpenocd()
{
    SYSTEM_NAME=`uname`;
    
    if [ "$SYSTEM_NAME" = "Linux" ]; then
        SetForegroundColor "green"
        echo "Linux detected. Adding openocd from the project"
        
        if [ $PATH = *$OPENOCD_BIN_DIR* ]; then
            SetForegroundColor "green"
            echo "Skipping adding $OPENOCD_BIN_DIR to the paths - it already exist"
        else 
            SetForegroundColor "blue"
            echo "Adding $OPENOCD_BIN_DIR to the PATH"
            # Adding all needed paths
            PATH=$PATH:$OPENOCD_BIN_DIR:$OPENOCD_SCRIPTS_DIR
        fi
    else
        SetForegroundColor "red"
        echo "Unknown system '$SYSTEM_NAME' detected. You must install openocd tool on your own"
        exit 1
    fi
}

InstallLibUsb()
{
    if [ ! "$PIPELINE" = "1" ]; then 
        InstallViaPackageManager libusb-1.0
    fi
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR"         = "" ]; then PROJECT_DIR=$PWD/..;                          fi
if [ "$SET_PATHS_FILE_PATH" = "" ]; then SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh;fi
if [ "$CONSOLE_FILE_PATH"   = "" ]; then CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh;    fi
SKIP_CHECKING_ARCH="true"

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#    COMMANDS VERIFICATIONS
#=====================================================================================================
if ! IsCommandAvailable arm-none-eabi-gcc; then InstallArmNoneEabi; else CommandFound arm-none-eabi-gcc; fi
if ! IsCommandAvailable make;              then InstallMake;        else CommandFound make;              fi
if ! IsCommandAvailable openocd;           then InstallOpenocd;     else CommandFound openocd;           fi
if ! IsLibraryInstalled libusb-1.0;        then InstallLibUsb;      else LibraryFound libusb-1.0;        fi
