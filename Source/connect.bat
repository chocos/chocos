::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	Script for connecting to the programmer
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROJECT_DIR=..
SET SET_PATHS_FILE_PATH=%PROJECT_DIR%/set_paths.bat

::----------------------------------------------------------
:: Checking set paths file exist 
::----------------------------------------------------------
IF NOT EXIST %SET_PATHS_FILE_PATH% GOTO SetPathsFileNotExist

::----------------------------------------------------------
:: Call set paths script
::----------------------------------------------------------
call %SET_PATHS_FILE_PATH%

::----------------------------------------------------------
:: Set default values
::----------------------------------------------------------
SET FLASH_MACHINE=FALSE

::----------------------------------------------------------
:: Check parameters
::----------------------------------------------------------
IF NOT "%1"=="flash" IF NOT "%1"=="" GOTO Usage
IF "%1"=="flash" SET FLASH_MACHINE=TRUE

::----------------------------------------------------------
:: Check architecture
::----------------------------------------------------------
IF NOT DEFINED MACHINE_PRODUCENT GOTO ArchitectureNotSelected
IF NOT DEFINED MACHINE_FAMILY GOTO ArchitectureNotSelected
IF NOT DEFINED MACHINE_NAME GOTO ArchitectureNotSelected

::----------------------------------------------------------
:: Checking other paths 
::----------------------------------------------------------
IF NOT EXIST %MACHINE_SCRIPTS_DIR% GOTO ScriptsDirNotExist
IF NOT EXIST %MACHINE_CONNECT_FILE_PATH% GOTO ConnectFileNotExist

::----------------------------------------------------------
:: Connect to the programmer
::----------------------------------------------------------
SET SAVED_PATH=%CD%
@cd %MACHINE_SCRIPTS_DIR%

echo ---=== Create connection in new window ===---
start %MACHINE_CONNECT_FILE_NAME%

@cd %SAVED_PATH%
::----------------------------------------------------------
:: Flash the machine
::----------------------------------------------------------
IF "%FLASH_MACHINE%"=="TRUE" (
	echo waiting 3 seconds for connection before flashing...
	sleep 1
	echo waiting 2 seconds for connection before flashing...
	sleep 1
	echo waiting 1 seconds for connection before flashing...
	sleep 1
	echo waiting 0 seconds for connection before flashing...
	call %FLASH_FILE_NAME%
)

GOTO EOF

:SetPathsFileNotExist
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Scripts for setting paths not exists!
echo =          file: '%SET_PATHS_FILE_PATH%'
echo =
echo ==========================================================================
GOTO EOF

:ConnectFileNotExist
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Connection file not exists!
echo =          file: '%MACHINE_CONNECT_FILE_PATH%'
echo =
echo ==========================================================================
GOTO EOF

:ScriptsDirNotExist
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Scripts for the target architecture not exists!
echo =
echo ==========================================================================
GOTO EOF

:ArchitectureNotSelected
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Target architecture is not selected
echo = 			run select_arch.bat first
echo =
echo ==========================================================================
GOTO EOF

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 [FLASH]
echo =				
echo = 			where FLASH is:
echo = 					flash - when you want to also flash the target uC
echo = 					
echo ==========================================================================

:EOF