#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/28
#   Description:    script for generating new program
#================================================================================================================================

#=====================================================================================================
#   MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR! ($0)                                                 "
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

UserNotSet()
{
    SetForegroundColor "red"
    echo "========================================================================= "
    echo "=                                                                         "
    echo "= ERROR!                                                                  "
    echo "=     Target user data not set                                            "
    echo "=     run set_user.sh first                                               "
    echo "=                                                                         "
    echo "========================================================================= "
    ResetAllAttributes
    exit 1
}

Usage()
{
    echo "==================================================================================="
    echo "=                                                                                  "
    echo "=      Usage:                                                                      "
    echo "=         $0 PROGRAM_NAME [STACK_SIZE] [TARGET_SPACE] [PROGRAM_DIR] [FILE_TYPE]    "
    echo "=                                                                                  "
    echo "=           where:                                                                 "
    echo "=              PROGRAM_NAME    - Name of the program to create                     "
    echo "=              STACK_SIZE      - Size of main thread stack in [B]ytes              "
    echo "=              TARGET_SPACE    - Target space - set it to USER or CORE             "
    echo "=              PROGRAM_DIR     - Directory of the program - SYSTEM/USER            "
    echo "=              FILE_TYPE       - (only USER) target file of the program - CBIN/ELF "
    echo "=                                                                                  "
    echo "==================================================================================="

    exit 1
}

#=====================================================================================================
#   HELPER FUNCTIONS
#=====================================================================================================
IsNumeric()
{
    if [ "$1" -eq "$1" ] 2>/dev/null
    then
        return 0;
    else 
        return 1;
    fi
}

#=====================================================================================================
#   Function for parsing file
#=====================================================================================================
ParseFile()
{
    echo "Parsing file $3..."
    $TMPLPARS_FILE_PATH $1 $2 NAME=$CHOCOOS_USER_NAME SURNAME=$CHOCOOS_USER_SURNAME PROGRAM_NAME=$PROGRAM_NAME SPACE=$TARGET_SPACE STACK_SIZE=$STACK_SIZE EMAIL=$CHOCOOS_USER_EMAIL FILE_TYPE=$FILE_TYPE
    chmod 777 $2
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR"         = "" ]; then PROJECT_DIR=$PWD/..;                          fi
if [ "$SET_PATHS_FILE_PATH" = "" ]; then SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh;fi
if [ "$CONSOLE_FILE_PATH"   = "" ]; then CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh;    fi

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#   OTHER PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $LOAD_USER_FILE_PATH   ]; then UserNotSet;                                      fi
if [ ! -e $TMPLPARS_FILE_PATH    ]; then PathDoesNotExist $TMPLPARS_FILE_PATH;            fi
if [ ! -e $PROGRAM_TEMPLATES_DIR ]; then PathDoesNotExist $PROGRAM_TEMPLATES_DIR;         fi
if [ ! -e $TOOLS_DIR             ]; then PathDoesNotExist $TOOLS_DIR;                     fi
if [ ! -e $SYSTEM_PROGRAMS_DIR   ]; then PathDoesNotExist $SYSTEM_PROGRAMS_DIR;           fi
if [ ! -e $USER_PROGRAMS_DIR     ]; then PathDoesNotExist $USER_PROGRAMS_DIR;             fi

#=====================================================================================================
#   OTHER ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $LOAD_USER_FILE_PATH

#=====================================================================================================
#   IMPORT OTHER MODULES
#=====================================================================================================
. $LOAD_USER_FILE_PATH

#=====================================================================================================
#   VERIFICATION OF USER DATA
#=====================================================================================================
if [ ! "$CHOCOOS_USER_DEFINED" = "true" ]; then UserNotSet;                               fi

#=====================================================================================================
#   ARGUMENTS VERIFICATION
#=====================================================================================================
PROGRAM_NAME=$1
STACK_SIZE=$2
TARGET_SPACE=$3
PROGRAM_DIR=$4
FILE_TYPE=$5

if   [ "$1" = "" ]; then Usage;                                                                                 fi
if   [ "$2" = "" ]; then STACK_SIZE="0";        elif ! IsNumeric $2;     then Usage;                            fi
if   [ "$3" = "" ]; then TARGET_SPACE="CORE";   elif [ ! "$3" = "CORE"  ] && [ ! "$3" = "USER" ];   then Usage; fi
if   [ "$4" = "" ]; then PROGRAM_DIR="USER";    elif [ ! "$4" = "USER"  ] && [ ! "$4" = "SYSTEM" ]; then Usage; fi
if   [ "$5" = "" ]; then FILE_TYPE="CBIN";      elif [ ! "$5" = "CBIN"  ] && [ ! "$5" = "ELF"  ];   then Usage; fi

#=====================================================================================================
#   Preparing paths
#=====================================================================================================
if [ "$PROGRAM_DIR" = "SYSTEM" ]; then
    OUTPUT_DIR=$SYSTEM_PROGRAMS_DIR/$PROGRAM_NAME
else 
    OUTPUT_DIR=$USER_PROGRAMS_DIR/$PROGRAM_NAME
fi

# Making a directory if it does not exist
if [ ! -e $OUTPUT_DIR ]; then mkdir $OUTPUT_DIR; fi

#=====================================================================================================
#   Parsing each file in the templates directory
#=====================================================================================================
for FILE_NAME in `ls $PROGRAM_TEMPLATES_DIR/`;
do
    ParseFile $PROGRAM_TEMPLATES_DIR/$FILE_NAME $OUTPUT_DIR/$FILE_NAME $FILE_NAME
done

#=====================================================================================================
#    ADD ALL PERMISSIONS TO ALL FILES
#=====================================================================================================
find $SOURCE_DIR -exec sudo chmod 777 {} \;

#=====================================================================================================
#   RETURN TO THE SAVED PATH
#=====================================================================================================
echo "=========================================================================="
echo "         The program $PROGRAM_NAME successfully created                   "
exit 0
