#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/06
#   Description:    Build script for the system (builds all)
#================================================================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR! ($0)                                                "
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

ArchitectureNotSelected()
{
    SetForegroundColor "red"
    echo "=========================================================================="
    echo "=	                                                                    "
    echo "= 	ERROR! ($0)                                                         "
    echo "= 			Target architecture is not selected                 "
    echo "= 			run select_arch.bat first                           "
    echo "=                                                                         "
    echo "=========================================================================="
    ResetAllAttributes
    exit 1
}

BuildFailed()
{
    SetForegroundColor "red"
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=     ERROR! ($0)                                                         "
    echo "=         Build of the $1 failed!                                         "
    echo "=                                                                         "
    echo "=========================================================================="
    ResetAllAttributes
    exit 1
}

Usage()
{
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=      Usage:                                                             "
    echo "=         %0 [SPACE] [TARGET]                                             "
    echo "=				                                            "
    echo "=			where PRODUCENT,FAMILY,MACHINE_NAME specify target architecture "
    echo "=				                                            "
    echo "=			where SPACE is:                                     "
    echo "=					libraries_space                     "
    echo "=					portable_space                      "
    echo "=					core_space                          "
    echo "=					user_space                          "
    echo "=                  system_programs                                        "
    echo "=                  user_programs                                          "
    echo "=					all                                 "
    echo "=			                                                    "
    echo "= 			where TARGET is:                                    "
    echo "= 					all                                 "
    echo "= 					clean                               "
    echo "=========================================================================="
    exit 1;
}

EclipseUsage()
{
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=      Usage:                                                             "
    echo "=         %0 eclipse [PRODUCENT] [FAMILY] [MACHINE_NAME] [SPACE] [TARGET] "
    echo "=                                                                         "
    echo "=     SYSTEM_PORTABLE_DIR where PRODUCENT,FAMILY,MACHINE_NAME specify target architecture  "
    echo "=                                                                         "
    echo "=     where SPACE is:                                     "
    echo "=         libraries_space                     "
    echo "=         portable_space                      "
    echo "=         core_space                          "
    echo "=         user_space                                                      "
    echo "=         system_programs                                                 "
    echo "=         user_programs                                                   "
    echo "=         all                                                             "
    echo "=                                                                         "
    echo "=         where TARGET is:                                                "
    echo "=                     all                                                 "
    echo "=                     clean                                               "
    echo "=========================================================================="
    exit 1;
}

#=====================================================================================================
#	HELPER FUNCTIONS
#=====================================================================================================
IsMachineSelected()
{
    if [ "$MACHINE_SELECTED" = "true" ]; then
        return 0;
    else
        return 1;
    fi
}

#============================================================
#   Builds target that is given as the argument 
#       @param FILE_PATH        Path to the build script
#============================================================
BuildTarget()
{
    DIR_PATH=$1
    FILE_NAME=$2
    TARGET=$3
    OUTPUT_FILE=$4
    
    if [ ! -e "$DIR_PATH"            ]; then PathDoesNotExist $DIR_PATH;            fi
    if [ ! -e "$DIR_PATH/$FILE_NAME" ]; then PathDoesNotExist $DIR_PATH/$FILE_NAME; fi
    
    chmod +x $DIR_PATH/$FILE_NAME
    
    cd $DIR_PATH
    ./$FILE_NAME $TARGET
    EXIT_CODE=$?
    cd $SOURCE_DIR
    
    if [ ! "$OUTPUT_FILE" = "" ] && [ ! -e $OUTPUT_FILE ]; then BuildFailed $DIR_PATH; fi
    if [ ! $EXIT_CODE = 0 ]; then exit $EXIT_CODE; fi
}

#============================================================
#   Links all spaces (if all are built)
#============================================================
LinkProject()
{
    if [ ! -e $LINK_FILE_PATH ]; then PathDoesNotExist $LINK_FILE_PATH; fi
    if [ -e $LIBRARIES_SPACE_LIB_FILE_PATH ] &&
       [ -e $PORTABLE_SPACE_LIB_FILE_PATH  ] &&
       [ -e $CORE_SPACE_LIB_FILE_PATH      ] &&
       [ -e $USER_SPACE_LIB_FILE_PATH      ]; then
       chmod +x $LINK_FILE_PATH
       $LINK_FILE_PATH
    fi
}

#============================================================
#   Selects architecture
#============================================================
SelectArchitecture()
{
    . $SELECT_ARCH_FILE_PATH $1 $2 $3 $4
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR"         = "" ]; then PROJECT_DIR=$PWD/..;                          fi
if [ "$SET_PATHS_FILE_PATH" = "" ]; then SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh;fi
if [ "$CONSOLE_FILE_PATH"   = "" ]; then CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh;    fi
SKIP_CHECKING_ARCH="true"

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#   LIST OF SPACES TO BUILD
#=====================================================================================================
BUILD_LIBRARIES="false"
BUILD_PORTABLE="false"
BUILD_CORE="false"
BUILD_USER="false"
BUILD_SYSTEM_PROGRAMS="false"
BUILD_USER_PROGRAMS="false"
RUN_LINKING="true"
BUILD_SPACE="all"
BUILD_TARGET="all"

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $SELECT_ARCH_FILE_PATH        ]; then PathDoesNotExist $SELECT_ARCH_FILE_PATH;    fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $SELECT_ARCH_FILE_PATH

#=====================================================================================================
#   ARGUMENTS VERIFICATION
#=====================================================================================================
if [ "$1" = "eclipse"  ]; then
    if [ "$2" = "" ]; then EclipseUsage;        fi
    if [ "$3" = "" ]; then EclipseUsage;        fi
    if [ "$4" = "" ]; then EclipseUsage;        fi
    if [ ! "$5" = "" ]; then BUILD_SPACE="$5";  fi
    if [ ! "$6" = "" ]; then BUILD_TARGET="$6"; fi
    
    SelectArchitecture $2 $3 $4 "eclipse"
else
    if [ ! "$1" = "" ]; then BUILD_SPACE="$1";  fi
    if [ ! "$2" = "" ]; then BUILD_TARGET="$2"; fi
fi

#=====================================================================================================
#    ARCHITECTURE SELECTED VERIFICATION
#=====================================================================================================
if ! IsMachineSelected ; then ArchitectureNotSelected; fi

#=====================================================================================================
#   LISTING SPACES TO BUILD
#=====================================================================================================
  if [ "$BUILD_SPACE" = "libraries_space" ]; then BUILD_LIBRARIES="true"; 
elif [ "$BUILD_SPACE" = "portable_space"  ]; then BUILD_PORTABLE="true";
elif [ "$BUILD_SPACE" = "core_space"      ]; then 
    BUILD_CORE="true"; 
    RUN_LINKING="false";
elif [ "$BUILD_SPACE" = "user_programs"   ]; then BUILD_USER="true";
elif [ "$BUILD_SPACE" = "system_programs" ]; then BUILD_SYSTEM_PROGRAMS="true";
elif [ "$BUILD_SPACE" = "user_programs"   ]; then BUILD_USER_PROGRAMS="true";
elif [ "$BUILD_SPACE" = "all"             ]; then 
    BUILD_LIBRARIES="true"
    BUILD_PORTABLE="true"
    BUILD_CORE="true"
    BUILD_USER="true"
    BUILD_SYSTEM_PROGRAMS="true"
    BUILD_USER_PROGRAMS="true"
else
    Usage
fi

#=====================================================================================================
#   BUILDING SPACES
#=====================================================================================================
if [ "$BUILD_LIBRARIES"       = "true" ]; then BuildTarget $SYSTEM_LIBRARIES_DIR    build.sh    $BUILD_TARGET $LIBRARIES_SPACE_LIB_FILE_PATH; fi
if [ "$BUILD_PORTABLE"        = "true" ]; then BuildTarget $SYSTEM_PORTABLE_DIR     build.sh    $BUILD_TARGET $PORTABLE_SPACE_LIB_FILE_PATH;  fi
if [ "$BUILD_CORE"            = "true" ]; then BuildTarget $SYSTEM_CORE_DIR         build.sh    $BUILD_TARGET $CORE_SPACE_LIB_FILE_PATH;      fi
if [ "$BUILD_USER"            = "true" ]; then BuildTarget $SYSTEM_USER_DIR         build.sh    $BUILD_TARGET $USER_SPACE_LIB_FILE_PATH;      fi
if [ "$BUILD_SYSTEM_PROGRAMS" = "true" ]; then BuildTarget $SYSTEM_PROGRAMS_DIR     buildall.sh $BUILD_TARGET;                                fi
if [ "$BUILD_USER_PROGRAMS"   = "true" ]; then BuildTarget $USER_PROGRAMS_DIR       buildall.sh $BUILD_TARGET;                                fi
if [ "$RUN_LINKING"           = "true" ]; then LinkProject; fi
