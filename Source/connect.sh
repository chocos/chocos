#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2015/04/27
#   Description:    Script for connecting to the programmer
#================================================================================================================================

#=====================================================================================================
#	MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR!                                                     ="
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

Usage()
{
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=      Usage:                                                             "
    echo "=         $0 [FLASH]                                                      "
    echo "=                                                                         "
    echo "=         where FLASH is:                                                 "
    echo "=                     flash - when you want to also flash the target uC   "
    echo "=                                                                         "
    echo "=========================================================================="
    exit 1
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
PROJECT_DIR=$PWD/..
CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh
SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh
FLASH_MACHINE="false"

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#   OPENOCD PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $MACHINE_SCRIPTS_DIR          ]; then PathDoesNotExist $MACHINE_SCRIPTS_DIR;        fi
if [ ! -e $MACHINE_CONNECT_FILE_PATH    ]; then PathDoesNotExist $MACHINE_CONNECT_FILE_PATH;  fi
if [ ! -e $MACHINE_FLASH_FILE_PATH      ]; then PathDoesNotExist $MACHINE_FLASH_FILE_PATH;    fi

#=====================================================================================================
#	ARGUMENTS VERIFICATION
#=====================================================================================================
  if [ "$1" = ""      ]; then FLASH_MACHINE="false";
elif [ "$1" = "flash" ]; then FLASH_MACHINE="true" ; 
else Usage; fi

#=====================================================================================================
#   EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $MACHINE_CONNECT_FILE_PATH
chmod +x $MACHINE_FLASH_FILE_PATH

#=====================================================================================================
#	CONNECTING TO THE BOARD
#=====================================================================================================
if [ ! "$NO_ROOT_PRIVILAGES" = "true" ]; then 
    . $MACHINE_CONNECT_FILE_PATH
else 
    SetForegroundColor "red"
    echo "No root privilages - probably connect will not work, but we will try"
    SetForegroundColor "white"
    . $MACHINE_CONNECT_FILE_PATH
fi
    
#=====================================================================================================
#	FLASHING THE MACHINE
#=====================================================================================================
if [ "$FLASH_MACHINE" = "true" ]; then
    echo "Waiting 3 seconds before flashing..."
    sleep 1
    echo "Waiting 2 seconds before flashing..."
    sleep 1
    echo "Waiting 1 seconds before flashing..."
    sleep 1
    $MACHINE_FLASH_FILE_PATH
fi