#!/bin/sh
#================================================================================================================================
#   Author:         Patryk Kubiak
#   Date:           2016/07/28
#   Description:    script for setting user
#================================================================================================================================

#=====================================================================================================
#   MESSAGES
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR! ($0)                                                 "
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit 1
}

Usage()
{
    echo "=========================================================================="
    echo "=                                                                         "
    echo "=      Usage:                                                             "
    echo "=         $0 NAME SURNAME EMAIL                                           "
    echo "=                                                                         "
    echo "=           where:                                                        "
    echo "=              NAME     - Your name                                       "
    echo "=              SURNAME  - Your surname                                    "
    echo "=              EMAIL    - Your email                                      "
    echo "=                                                                         "
    echo "=      Note:                                                              "
    echo "=          The script saves this informations only in your OS in          "
    echo "=          environmental variables. It will be used only for code         "
    echo "=          generators such as drivergen. It is not collected              "
    echo "=          anywhere else.                                                 "
    echo "=                                                                         "
    echo "=========================================================================="
    exit 1
}

#=====================================================================================================
#   PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR"         = "" ]; then PROJECT_DIR=$PWD/..;                          fi
if [ "$SET_PATHS_FILE_PATH" = "" ]; then SET_PATHS_FILE_PATH=$PROJECT_DIR/set_paths.sh;fi
if [ "$CONSOLE_FILE_PATH"   = "" ]; then CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh;    fi
SKIP_CHECKING_ARCH="true"

#=====================================================================================================
#   PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $PROJECT_DIR           ]; then PathDoesNotExist $PROJECT_DIR;             fi
if [ ! -e $CONSOLE_FILE_PATH     ]; then PathDoesNotExist $CONSOLE_FILE_PATH;       fi
if [ ! -e $SET_PATHS_FILE_PATH   ]; then PathDoesNotExist $SET_PATHS_FILE_PATH;     fi

#=====================================================================================================
#   ADD EXECUTION PERMISSIONS
#=====================================================================================================
chmod +x $CONSOLE_FILE_PATH
chmod +x $SET_PATHS_FILE_PATH

#=====================================================================================================
#    IMPORT MODULES
#=====================================================================================================
. $CONSOLE_FILE_PATH
. $SET_PATHS_FILE_PATH 

#=====================================================================================================
#   ARGUMENTS VERIFICATION
#=====================================================================================================
  if [ "$1" = "" ]; then Usage;
elif [ "$2" = "" ]; then Usage;
elif [ "$3" = "" ]; then Usage;
else
    export CHOCOOS_USER_NAME=$1 
    export CHOCOOS_USER_SURNAME=$2 
    export CHOCOOS_USER_EMAIL=$3
    export CHOCOOS_USER_DEFINED="true"
fi

#=====================================================================================================
#   PREPARATION OF FILE WITH USER DEFINITIONS
#=====================================================================================================
echo "#!/bin/sh" > $LOAD_USER_FILE_PATH
echo "export CHOCOOS_USER_NAME=$CHOCOOS_USER_NAME" >> $LOAD_USER_FILE_PATH
echo "export CHOCOOS_USER_SURNAME=$CHOCOOS_USER_SURNAME" >> $LOAD_USER_FILE_PATH
echo "export CHOCOOS_USER_EMAIL=$CHOCOOS_USER_EMAIL" >> $LOAD_USER_FILE_PATH
echo "export CHOCOOS_USER_DEFINED=$CHOCOOS_USER_DEFINED" >> $LOAD_USER_FILE_PATH

#=====================================================================================================
#   PRINTING STATUS
#=====================================================================================================
echo "=========================================================================="
echo "=                              SUCCESS                                   ="
echo "=========================================================================="
echo "                 Name    set to $CHOCOOS_USER_NAME                        "
echo "                 Surname set to $CHOCOOS_USER_SURNAME                     "  
echo "                 Email   set to $CHOCOOS_USER_EMAIL                       "