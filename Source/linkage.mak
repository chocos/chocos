############################################################################################################################################
##
##  Author:         Patryk Kubiak
##  Date:           2015/04/27
##  Description:    makefile for linking the system
##
############################################################################################################################################


##============================================================================================================================
##                                          
##              PREPARATION VARIABLES               
##                                          
##============================================================================================================================
ifeq ($(PROJECT_DIR),)
    PROJECT_DIR = ..
endif

##============================================================================================================================
##                                          
##              GENERAL CONFIGURATION               
##                                          
##============================================================================================================================

##============================================================================================================================
##                                          
##          INCLUDE MAKEFILES       
##                                          
##============================================================================================================================
include $(GLOBAL_DEFS_MK_FILE_PATH)
include $(USER_SPACE_MK_FILE_PATH)

##============================================================================================================================
##                                          
##          PREPARE FLAGS
##                                          
##============================================================================================================================
LFLAGS                       = -g \
                                $(CPUCONFIG_LDFLAGS) \
                                -nostartfiles \
                                -T$(MACHINE_MEMORY_LD_FILE_PATH) \
                                -T$(MACHINE_COMMON_LD_FILE_PATH) \
                                -Wl,--gc-sections \
                                -Wl,-Map=$(PROJECT_MAP_FILE_PATH),--cref,--no-warn-mismatch \
                                -Wall \
                                -lm \
                                -L$(MACHINE_OUTPUT_DIR) \
                                -static \
                                -o $(PROJECT_ELF_FILE_PATH)

##============================================================================================================================
##                                          
##          PREPARE LIST OF PROGRAM DIRECTORIES 
##                                          
##============================================================================================================================
PROGRAMS_DIRS                = $(patsubst $(SYSTEM_PROGRAMS_DIR)/%/,%,$(filter %/, $(wildcard $(SYSTEM_PROGRAMS_DIR)/*/))) \
                               $(patsubst $(USER_PROGRAMS_DIR)/%/,%,$(filter %/, $(wildcard $(USER_PROGRAMS_DIR)/*/)))

                                                        
##============================================================================================================================
##                                          
##          PREPARE LIST OF PROGRAM LIBRARIES FILES NAMES 
##                                          
##============================================================================================================================
PROGRAMS_LIBRARIES_FILE_PATHS = $(foreach program_dir,$(PROGRAMS_DIRS),$(MACHINE_OUTPUT_DIR)/lib$(program_dir).a)

##============================================================================================================================
##                                          
##          PREPARE LIST OF LIBRARIES 
##                                          
##============================================================================================================================
LIBRARIES_FILE_PATHS         = $(CORE_SPACE_LIB_FILE_PATH) $(PORTABLE_SPACE_LIB_FILE_PATH) $(LIBRARIES_SPACE_LIB_FILE_PATH)
							   
##============================================================================================================================
##                                          
##              TARGETS 
##                                          
##============================================================================================================================
print_linking_info:
	@echo "=================================================================================================================="
	@echo "                                           Linking Project"
	@echo "=================================================================================================================="
	@echo " "
	@echo "Libraries list:"
	@echo -e " $(foreach lib,$(LIBRARIES_FILE_PATHS), library: $(lib)\n)"
	
link:
	$(LD) $(LFLAGS) $(LIBRARIES_FILE_PATHS)
	
create_output_files:
	@echo " "
	@echo 'Creating IHEX image...'
	@$(OBJCOPY) $(PROJECT_ELF_FILE_PATH) -O ihex $(PROJECT_HEX_FILE_PATH)

	@echo 'Creating binary image...'
	@$(OBJCOPY) $(PROJECT_ELF_FILE_PATH) -O binary $(PROJECT_BIN_FILE_PATH)

	@echo 'Creating memory dump...'
	@$(OBJDUMP) -x --syms $(PROJECT_ELF_FILE_PATH) > $(PROJECT_DMP_FILE_PATH)

	@echo 'Creating extended listing....'
	@$(OBJDUMP) -S $(PROJECT_ELF_FILE_PATH) > $(PROJECT_LST_FILE_PATH)

	@echo 'Creating objects size list...'
	@$(SIZE) -B -t --common $(foreach var,$(OBJECTS),$(OBJ_PATH)/$(var)) > $(PROJECT_SIZE_FILE_PATH) $(PROJECT_ELF_FILE_PATH)

	@echo -e "Flash image size: $$($(SIZEOF) $(PROJECT_BIN_FILE_PATH)) bytes\n"

print_status:
	@echo "------------------------------------------------------------------------------------------------------------------"
	@echo "     Linking success"
	@echo ""
    
linking_all: print_linking_info link create_output_files print_status
