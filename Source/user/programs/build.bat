::================================================================================================================================
::	Author:			Patryk Kubiak
::	Date:			2015/04/27
::	Description:	Build script for programs
::================================================================================================================================
@echo off

::----------------------------------------------------------
:: Configuration paths
::----------------------------------------------------------
SET PROGRAM_DIR_NAME=%1

::----------------------------------------------------------
:: Check parameters
::----------------------------------------------------------
IF "%1"=="" GOTO Usage

::----------------------------------------------------------
:: Check if program path exists
::----------------------------------------------------------
SET PROGRAM_PATH=%PROGRAM_DIR_NAME%
IF NOT EXIST %PROGRAM_PATH% GOTO ProgramNotFound

::----------------------------------------------------------
:: Building program
::----------------------------------------------------------
SET SAVED_PATH=%cd%
cd ../..
call build_program user %1 %2
cd %SAVED_PATH%
GOTO EOF

:ProgramNotFound
echo ==========================================================================
echo =	
echo = 	ERROR!
echo = 			Program %1 not found at %cd%/%PROGRAM_PATH%
echo =
echo ==========================================================================
echo =
echo =	List of available program dirs:
echo =
for /d %%d in (*.*) do (
	IF NOT "%%d"==".settings" echo =		%%d
)
GOTO EOF

:Usage
echo ==========================================================================
echo =                                                                        
echo =      Usage:                                                            
echo =         %0 [PROGRAM_DIR_NAME] [TARGET]
echo =				
echo =			where TARGET is:
echo = 					all
echo = 					clean
echo ==========================================================================

:EOF