/** ****************************************************************************************************************************************
 *
 * @file       main.c
 *
 * @brief      Main file of the TguiExample program
 *
 * @author     Patryk Kubiak - (Created on: 2016-05-10 - 19:45:12) 
 *
 * @note       Copyright (C) 2016 Patryk Kubiak <patryk.kubiak90@gmail.com>
 *
 *                This program is free software; you can redistribute it and/or modify
 *                it under the terms of the GNU General Public License as published by
 *                the Free Software Foundation; either version 2 of the License, or
 *                (at your option) any later version.
 *
 *                This program is distributed in the hope that it will be useful,
 *                but WITHOUT ANY WARRANTY; without even the implied warranty of
 *                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *                GNU General Public License for more details.
 *
 *                You should have received a copy of the GNU General Public License
 *                along with this program; if not, write to the Free Software
 *                Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 ******************************************************************************************************************************************/

#include <oc_stdio.h>
#include <oc_tgui.h>

typedef struct
{
    struct
    {
        oC_TGUI_Position_t          Position;
        oC_TGUI_Column_t            Width;
        oC_TGUI_Line_t              Height;
    } MainWindow , Menu , SupportWindow;

    oC_TGUI_MenuStyle_t             MenuStyle;
    oC_TGUI_BoxStyle_t              BoxStyle;
    oC_TGUI_Style_t                 LabelStyle;
    oC_TGUI_SelectionBoxStyle_t     SelectionBoxStyle;
    oC_TGUI_QuickEditBoxStyle_t     QuickEditBoxStyle;
} Screen_t;

static const oC_String_t EnabledDisabledStrings[] = {
                "disabled" ,
                "enabled" ,
};

static const Screen_t Screen = {
                .MainWindow = {
                    .Position   = { .Column = 1 , .Line = 1 } ,
                    .Width      = oC_TGUI_DefaultScreenWidth ,
                    .Height     = oC_TGUI_DefaultScreenHeight ,
                } ,
                .SupportWindow = {
                    .Position   = { .Column = 10 , .Line = 4 } ,
                    .Width      = 60 ,
                    .Height     = 10 ,
                } ,
                .Menu       = {
                    .Position   = { .Column = 18 , .Line = 15 } ,
                    .Width      = 42 ,
                    .Height     = 5 ,
                } ,
                .MenuStyle  = {
                     .ActiveEntry.Foreground    = oC_TGUI_Color_White ,
                     .ActiveEntry.Background    = oC_TGUI_Color_LightRed ,
                     .NotActiveEntry.Foreground = oC_TGUI_Color_Black ,
                     .NotActiveEntry.Background = oC_TGUI_Color_White ,
                     .Border.DontDraw           = true ,
                } ,
                .BoxStyle   = {
                     .BorderStyle.Foreground    = oC_TGUI_Color_White ,
                     .BorderStyle.Background    = oC_TGUI_Color_DarkGray ,
                     .TitleStyle.Foreground     = oC_TGUI_Color_White ,
                     .TitleStyle.Background     = oC_TGUI_Color_DarkGray ,
                     .ShadowStyle.DontDraw      = true ,
                     .InsideStyle.Foreground    = oC_TGUI_Color_White ,
                     .InsideStyle.Background    = oC_TGUI_Color_DarkGray ,
                } ,
                .LabelStyle = {
                     .Foreground                = oC_TGUI_Color_White ,
                     .Background                = oC_TGUI_Color_DarkGray ,
                     .TextStyle                 = oC_TGUI_TextStyle_Bold ,
                } ,
                .SelectionBoxStyle = {
                     .Arrow.Foreground          = oC_TGUI_Color_Red,
                     .Arrow.Background          = oC_TGUI_Color_White ,
                     .Active.Foreground         = oC_TGUI_Color_White ,
                     .Active.Background         = oC_TGUI_Color_Red ,
                     .NotActive.Foreground      = oC_TGUI_Color_Red ,
                     .NotActive.Background      = oC_TGUI_Color_White ,
                } ,
                .QuickEditBoxStyle = {
                     .ActiveBorder.DontDraw     = true ,
                     .NotActiveBorder.DontDraw  = true ,
                     .ActiveText.Foreground     = oC_TGUI_Color_White ,
                     .ActiveText.Background     = oC_TGUI_Color_Red ,
                     .NotActiveText.Foreground  = oC_TGUI_Color_Red ,
                     .NotActiveText.Background  = oC_TGUI_Color_White ,
                } ,
};

//==========================================================================================================================================
/**
 * Shows kitchen menu
 */
//==========================================================================================================================================
static void ShowKitchenMenu( void * Parameter )
{
    uint32_t               selectedIndexes[]= { 1 , 1 , 0};
    char                   enabledLightBuffer[100];
    oC_TGUI_Column_t       valueWidth       = 15;
    oC_TGUI_Column_t       labelWidth       = Screen.SupportWindow.Width - valueWidth;
    oC_TGUI_ActiveObject_t activeObjects[]  = {
        {
              .Type             = oC_TGUI_ActiveObjecType_SelectionBox  ,
              .Position         = { .Column = Screen.SupportWindow.Position.Column + labelWidth , .Line = Screen.SupportWindow.Position.Line } ,
              .Width            = valueWidth ,
              .LabelPosition    = { .Column = Screen.SupportWindow.Position.Column              , .Line = Screen.SupportWindow.Position.Line } ,
              .LabelWidth       = labelWidth ,
              .LabelStyle       = &Screen.LabelStyle ,
              .LabelText        = "Electricity:" ,
              .SelectionBox.SelectedIndex       = &selectedIndexes[0] ,
              .SelectionBox.Options             = EnabledDisabledStrings ,
              .SelectionBox.NumberOfOptions     = oC_ARRAY_SIZE(EnabledDisabledStrings) ,
              .SelectionBox.Style               = &Screen.SelectionBoxStyle ,
              .SelectionBox.OnChangeHandler     = NULL ,
              .SelectionBox.OnChangeParameter   = NULL ,
        } ,
        {
              .Type             = oC_TGUI_ActiveObjecType_SelectionBox  ,
              .Position         = { .Column = Screen.SupportWindow.Position.Column + labelWidth , .Line = Screen.SupportWindow.Position.Line + 1} ,
              .Width            = valueWidth ,
              .LabelPosition    = { .Column = Screen.SupportWindow.Position.Column              , .Line = Screen.SupportWindow.Position.Line + 1} ,
              .LabelWidth       = labelWidth ,
              .LabelStyle       = &Screen.LabelStyle ,
              .LabelText        = "Main Light:" ,
              .SelectionBox.SelectedIndex       = &selectedIndexes[1] ,
              .SelectionBox.Options             = EnabledDisabledStrings ,
              .SelectionBox.NumberOfOptions     = oC_ARRAY_SIZE(EnabledDisabledStrings) ,
              .SelectionBox.Style               = &Screen.SelectionBoxStyle ,
              .SelectionBox.OnChangeHandler     = NULL ,
              .SelectionBox.OnChangeParameter   = NULL ,
        } ,
        {
              .Type             = oC_TGUI_ActiveObjecType_SelectionBox  ,
              .Position         = { .Column = Screen.SupportWindow.Position.Column + labelWidth , .Line = Screen.SupportWindow.Position.Line + 2} ,
              .Width            = valueWidth ,
              .LabelPosition    = { .Column = Screen.SupportWindow.Position.Column              , .Line = Screen.SupportWindow.Position.Line + 2} ,
              .LabelWidth       = labelWidth ,
              .LabelStyle       = &Screen.LabelStyle ,
              .LabelText        = "Cooker:" ,
              .SelectionBox.SelectedIndex       = &selectedIndexes[2] ,
              .SelectionBox.Options             = EnabledDisabledStrings ,
              .SelectionBox.NumberOfOptions     = oC_ARRAY_SIZE(EnabledDisabledStrings) ,
              .SelectionBox.Style               = &Screen.SelectionBoxStyle ,
              .SelectionBox.OnChangeHandler     = NULL ,
              .SelectionBox.OnChangeParameter   = NULL ,
        } ,
        {
              .Type             = oC_TGUI_ActiveObjecType_QuickEdit  ,
              .Position         = { .Column = Screen.SupportWindow.Position.Column + labelWidth , .Line = Screen.SupportWindow.Position.Line + 3} ,
              .Width            = valueWidth ,
              .LabelPosition    = { .Column = Screen.SupportWindow.Position.Column              , .Line = Screen.SupportWindow.Position.Line + 3} ,
              .LabelWidth       = labelWidth ,
              .LabelStyle       = &Screen.LabelStyle ,
              .LabelText        = "Timeout for enabled light [s]:" ,
              .QuickEdit.Buffer     = enabledLightBuffer ,
              .QuickEdit.BufferSize = sizeof(enabledLightBuffer) ,
              .QuickEdit.InputType  = oC_TGUI_InputType_Digits ,
              .QuickEdit.SaveHandler= NULL ,
              .QuickEdit.Style      = &Screen.QuickEditBoxStyle ,
        } ,
    };

    memset(enabledLightBuffer,0,sizeof(enabledLightBuffer));

    strcpy(enabledLightBuffer,"15");

    oC_TGUI_DrawActiveObjects(activeObjects,oC_ARRAY_SIZE(activeObjects));

    oC_TGUI_ClearPartOfScreen(Screen.SupportWindow.Position,Screen.SupportWindow.Width,Screen.SupportWindow.Height,Screen.BoxStyle.InsideStyle.Background);
}

//==========================================================================================================================================
/**
 * Shows kitchen menu
 */
//==========================================================================================================================================
static void ShowBathroomMenu( void * Parameter )
{

}

//==========================================================================================================================================
/**
 * Shows kitchen menu
 */
//==========================================================================================================================================
static void ShowLivingRoomMenu( void * Parameter )
{

}

//==========================================================================================================================================
/**
 * Shows main menu
 */
//==========================================================================================================================================
static void ShowMainMenu( void )
{
    oC_TGUI_MenuEntry_t menuEntries[] = {
       { .Title = "Kitchen"         , .Help = "Manage kitchen"     , .Handler = ShowKitchenMenu    , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
       { .Title = "Bathroom"        , .Help = "Manage bathroom"    , .Handler = ShowBathroomMenu   , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
       { .Title = "Living room"     , .Help = "Manage living room" , .Handler = ShowLivingRoomMenu , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
       { .Title = "Exit"            , .Help = "Exit the program"   , .Handler = NULL               , .Parameter = NULL , .OnHoverHandler = NULL , .OnHoverParameter = NULL } ,
    };
    oC_TGUI_DrawMenu(Screen.Menu.Position , Screen.Menu.Width , Screen.Menu.Height , menuEntries , oC_ARRAY_SIZE(menuEntries) , &Screen.MenuStyle );
}

//==========================================================================================================================================
/**
 * Show main screen
 */
//==========================================================================================================================================
static void ShowMainScreen( void )
{
    oC_TGUI_DrawBox("[ My Home Manager ]" , NULL , Screen.MainWindow.Position , Screen.MainWindow.Width , Screen.MainWindow.Height , &Screen.BoxStyle);
    ShowMainMenu();
}

//==========================================================================================================================================
/**
 * The main entry of the application
 *
 * @param Argc      Argument counter
 * @param Argv      Arguments array
 *
 * @return result
 */
//==========================================================================================================================================
int main( int Argc , char ** Argv )
{
	oC_TGUI_ResetDevice();
	
	ShowMainScreen();

	oC_TGUI_ResetDevice();
    return 0;
}
