#!/bin/bash
#
#       Flashes the device with the ChocoOS system binary
#

#
#   Path to the directory with this script
#
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
export PROJECT_DIR=$THIS_DIR
THIS_SCRIPT=$PROJECT_DIR/flash.sh

#
#   Path to the configuration file
#
CONFIGURATION_FILE_PATH=~/.choco-scripts.cfg
SCRIPT_DESCRIPTION=""

#
#   Installs choco-scripts
#
function installChocoScripts()
{
    echo "Installation of the choco-scripts"

    # This line installs wget tool - you don't need to use it if you already have it
    apt-get update && apt-get install -y wget

    # This downloads an installation script and run it 
    wget -O - https://release.choco-technologies.com/scripts/install-choco-scripts.sh | bash
}

#
#   Verification of the choco scripts installation
#
if [ -f "$CONFIGURATION_FILE_PATH" ]
then 
    source $CONFIGURATION_FILE_PATH
else 
    printf "\033[31;1mChoco-Scripts are not installed for this user\033[0m\n\n"
    printf "      \033[37;1mYou can find the installation instruction here: \033[0m\n"
    printf "            \033[34;1mhttps://bitbucket.org/chocotechnologies/scripts/src/master/\033[0m\n\n"

    while true
    do
        read -p "Do you want to try to auto-install it? [Y/n]: " answer
        case $answer in 
            [Yy]* ) installChocoScripts; break;;
            [Nn]* ) echo "Skipping installation"; exit 1;;
            * ) echo "Please answer Y or n";;
        esac
    done
    exit 1
fi

#
#   Information message
#
echo "Using choco-scripts from path $CHOCO_SCRIPTS_PATH in version $CHOCO_SCRIPTS_VERSION"

#
#   Importing of the framework main script
#
source $(getChocoScriptsPath)

#
#   The function prepares a framework script to work
#
function prepareScript()
{
    defineScript "$0" "Flashes the device with the ChocoOS system binary"
    
    addCommandLineOptionalArgument 'SET_PATHS_FILE_PATH' '--paths-file' existing_file 'Name of a script with project paths' "$THIS_DIR/set_paths.sh" ''
    addCommandLineRequiredArgument 'MACHINE_NAME' '--machine-name' options 'Name of the target machine to flash' 'stm32f746ngh6 lm4f120h5qr'
    addCommandLineOptionalArgument 'CONNECT' '--connect' bool 'If true, the script will also connect to the target device before flashing' 'FALSE' ''
    addCommandLineOptionalArgument 'DONT_USE_DOCKER' '--dont-use-docker' bool 'If true, the script will not use docker, but it will run the commands natively' 'FALSE' ''
    addCommandLineOptionalArgument 'FLASH' '--flash' bool 'If true, the script will flash the board as well' 'FALSE' ''
    addCommandLineOptionalArgument 'DONT_DISCONNECT' '--dont-disconnect' bool 'If true, the script will not disconnect from the board' 'FALSE' ''
    addCommandLineOptionalArgument 'GDB_MODE' '--gdb-mode' bool 'If true, the script will start in GDB mode (for debugging) instead of only flashing' 'FALSE' ''
    
    disableConfigurationPrinting
    parseCommandLineArguments "$@"
}

#
#   Connects to the device
#
function connect()
{
    if isStringEqual "$DONT_USE_DOCKER" "TRUE"
    then 
        "$CONNECT_FILE_PATH" &
        CONNECT_PID=$!
        sleep 3
    elif isStringEqual "$FLASH" "FALSE"
    then
        "$RUN_CONTAINER_FILE_PATH" -w="$SOURCE_DIR" --prepare-for-device --machine-name="$MACHINE_NAME" --interactive-container=false --command-file="$THIS_SCRIPT" --command-args="--connect=$CONNECT --dont-use-docker --machine-name=$MACHINE_NAME --dont-disconnect=$DONT_DISCONNECT"
    fi
}

#
#   Flashes the device
#
function flash()
{
    local gdb_str=''
    local INTERACTIVE_CONTAINER="FALSE"
    if isStringEqual "$GDB_MODE" "TRUE"
    then
        gdb_str='--gdb-mode'
        INTERACTIVE_CONTAINER="TRUE"
    fi
    if isStringEqual "$DONT_USE_DOCKER" "TRUE"
    then 
        "$FLASH_FILE_PATH" $gdb_str
    else
        "$RUN_CONTAINER_FILE_PATH" -w="$SOURCE_DIR" --prepare-for-device --machine-name="$MACHINE_NAME" --interactive-container="$INTERACTIVE_CONTAINER" --command-file="$THIS_SCRIPT" --command-args="--connect=$CONNECT --dont-use-docker --machine-name=$MACHINE_NAME --flash=$FLASH --gdb-mode=$GDB_MODE"
    fi
}

#
#   Disconnects from the device
#
function disconnect()
{
    if isStringEqual "$DONT_USE_DOCKER" "TRUE"
    then 
        kill -9 $CONNECT_PID
    fi
}

#######################################################################################
#
#   MAIN
#
prepareScript "$@"
source "$SET_PATHS_FILE_PATH" skip-checking-arch
if isStringEqual "$DONT_USE_DOCKER" "TRUE"
then 
    if ! isStringEmpty "$MACHINE_NAME"
    then 
        source "$SEARCH_ARCH_FILE_PATH" "$MACHINE_NAME"
    fi    
fi
if isStringEqual "$CONNECT" "TRUE"
then 
    connect
fi
if isStringEqual "$FLASH" "TRUE"
then
    flash
fi
if isStringEqual "$DONT_DISCONNECT" "FALSE"
then 
    disconnect
fi
