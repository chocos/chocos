#!/bin/sh
#================================================================================================================================
#    Author:        Patryk Kubiak
#    Date:          2016/07/03
#    Description:   Script for setting paths
#================================================================================================================================

#=====================================================================================================
#	ERRORS
#=====================================================================================================
PathDoesNotExist()
{
    printf "\033[1;31;40m"
    echo "=============================================================="
    echo "=                                                            ="
    echo "= ERROR!                                                     ="
    echo "=                                                            ="
    echo "=  Path: '$1'                                                 "
    echo "=     Does not exist!                                        ="
    echo "=                                                            ="
    echo "=============================================================="
    printf "\033[0;40m"
    exit
}

ArchitectureNotSelected()
{
   printf "\033[1;31;40m"
   echo "=========================================================================="
   echo "=                                                                         "
   echo "=     ERROR!                                                              "
   echo "=      Target architecture is not selected                                "
   echo "=      run select_arch.sh first                                           "
   echo "=                                                                         "
   echo "=========================================================================="
   printf "\033[0;40m"
   exit
}

#=====================================================================================================
#	PREPARE VARIABLES
#=====================================================================================================
if [ "$PROJECT_DIR" = "" ]; then export PROJECT_DIR=.; fi
export CONSOLE_FILE_PATH=$PROJECT_DIR/console.sh

#=====================================================================================================
#	ARGUMENTS VERIFICATION
#=====================================================================================================
if [ ! "$1" = "skip-checking-arch" ] && [ "$SKIP_CHECKING_ARCH" = "" ]; then
    if [ "$PROJECT_DIR"       = "" ]; then ArchitectureNotSelected; fi
    if [ "$MACHINE_PRODUCENT" = "" ]; then ArchitectureNotSelected; fi
    if [ "$MACHINE_FAMILY"    = "" ]; then ArchitectureNotSelected; fi
    if [ "$MACHINE_NAME"      = "" ]; then ArchitectureNotSelected; fi
fi

#=====================================================================================================
#	PATHS VERIFICATIONS
#=====================================================================================================
if [ ! -e $CONSOLE_FILE_PATH ]; then PathDoesNotExist $CONSOLE_FILE_PATH; fi
if [ ! -e $PROJECT_DIR       ]; then PathDoesNotExist $PROJECT_DIR;       fi

#=====================================================================================================
#	BASIC VARIABLES
#=====================================================================================================
export PROJECT_NAME=ChocoOS

##////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
##
##		DIRECTORIES
##
##////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

##----------------------------------------------------------
## Basic directories
##----------------------------------------------------------
export DOCKER_DIR=$PROJECT_DIR/Docker
export OUTPUT_DIR=$PROJECT_DIR/Output
export SOURCE_DIR=$PROJECT_DIR/Source
export TEST_DIR=$PROJECT_DIR/Tests
export TOOLS_DIR=$PROJECT_DIR/Tools
export RESOURCES_DIR=$PROJECT_DIR/Resources
export SYSTEM_DIR=$SOURCE_DIR/system
export USER_DIR=$SOURCE_DIR/user
export TEMPLATES_DIR=$RESOURCES_DIR/templates/files
export PROGRAM_TEMPLATES_DIR=$TEMPLATES_DIR/program
export FLASHFS_TEMPLATES_DIR=$TEMPLATES_DIR/flashfs

##----------------------------------------------------------
## System directories
##----------------------------------------------------------
export SYSTEM_CONFIG_DIR=$SYSTEM_DIR/config
export SYSTEM_CORE_DIR=$SYSTEM_DIR/core
export SYSTEM_PORTABLE_DIR=$SYSTEM_DIR/portable
export SYSTEM_USER_DIR=$SYSTEM_DIR/user
export SYSTEM_LIBRARIES_DIR=$SYSTEM_DIR/libraries
export SYSTEM_PROGRAMS_DIR=$SYSTEM_DIR/programs
export SYSTEM_PROGRAM_DIR=$SYSTEM_PROGRAMS_DIR/$PROGRAM_DIR_NAME
export SYSTEM_FLASH_DIR=$RESOURCES_DIR/flash

##----------------------------------------------------------
## User directories
##----------------------------------------------------------
export USER_LIBRARIES_DIR=$USER_DIR/libraries
export USER_PROGRAMS_DIR=$USER_DIR/programs
export USER_PROGRAM_DIR=$USER_PROGRAMS_DIR/$PROGRAM_DIR_NAME

##----------------------------------------------------------
## Configuration directories
##----------------------------------------------------------
export SYSTEM_CONFIG_MACHINE_DIR=$SYSTEM_CONFIG_DIR/$MACHINE_NAME

##----------------------------------------------------------
## System core directories
##----------------------------------------------------------
export SYSTEM_CORE_INCLUDES_DIR=$SYSTEM_CORE_DIR/inc
export SYSTEM_CORE_INCLUDES_DRIVERS_DIR=$SYSTEM_CORE_INCLUDES_DIR/drivers
export SYSTEM_CORE_INCLUDES_KERNEL_DIR=$SYSTEM_CORE_INCLUDES_DIR/kernel
export SYSTEM_CORE_INCLUDES_BOOT_DIR=$SYSTEM_CORE_INCLUDES_DIR/boot
export SYSTEM_CORE_INCLUDES_TERMINALS_DIR=$SYSTEM_CORE_INCLUDES_DIR/terminals
export SYSTEM_CORE_INCLUDES_FS_DIR=$SYSTEM_CORE_INCLUDES_DIR/fs
export SYSTEM_CORE_INCLUDES_SCALLS_DIR=$SYSTEM_CORE_INCLUDES_DIR/scalls
export SYSTEM_CORE_INCLUDES_POSIX_DIR=$SYSTEM_CORE_INCLUDES_DIR/posix
export SYSTEM_CORE_INCLUDES_GUI_DIR=$SYSTEM_CORE_INCLUDES_DIR/gui
export SYSTEM_CORE_INCLUDES_FLASHFS_DIR=$SYSTEM_CORE_INCLUDES_FS_DIR/flashfs
export SYSTEM_CORE_INCLUDES_NET_DIR=$SYSTEM_CORE_INCLUDES_DIR/net
export SYSTEM_CORE_INCLUDES_PROTOCOLS_DIR=$SYSTEM_CORE_INCLUDES_NET_DIR/protocols
export SYSTEM_CORE_SOURCES_DIR=$SYSTEM_CORE_DIR/src
export SYSTEM_CORE_SOURCES_DRIVERS_DIR=$SYSTEM_CORE_SOURCES_DIR/drivers
export SYSTEM_CORE_SOURCES_KERNEL_DIR=$SYSTEM_CORE_SOURCES_DIR/kernel
export SYSTEM_CORE_SOURCES_BOOT_DIR=$SYSTEM_CORE_SOURCES_DIR/boot
export SYSTEM_CORE_SOURCES_TERMINALS_DIR=$SYSTEM_CORE_SOURCES_DIR/terminals
export SYSTEM_CORE_SOURCES_FS_DIR=$SYSTEM_CORE_SOURCES_DIR/fs
export SYSTEM_CORE_SOURCES_SCALLS_DIR=$SYSTEM_CORE_SOURCES_DIR/scalls
export SYSTEM_CORE_SOURCES_POSIX_DIR=$SYSTEM_CORE_SOURCES_DIR/posix
export SYSTEM_CORE_SOURCES_GUI_DIR=$SYSTEM_CORE_SOURCES_DIR/gui
export SYSTEM_CORE_SOURCES_FLASHFS_DIR=$SYSTEM_CORE_SOURCES_FS_DIR/flashfs
export SYSTEM_CORE_SOURCES_NET_DIR=$SYSTEM_CORE_SOURCES_DIR/net
export SYSTEM_CORE_SOURCES_PROTOCOLS_DIR=$SYSTEM_CORE_SOURCES_NET_DIR/protocols

##----------------------------------------------------------
## System libraries directories
##----------------------------------------------------------
export SYSTEM_LIBRARIES_INCLUDES_DIR=$SYSTEM_LIBRARIES_DIR/inc
export SYSTEM_LIBRARIES_SOURCES_DIR=$SYSTEM_LIBRARIES_DIR/src

##----------------------------------------------------------
## System portable directories
##----------------------------------------------------------
export SYSTEM_PORTABLE_INCLUDES_DIR=$SYSTEM_PORTABLE_DIR/inc
export SYSTEM_PORTABLE_INCLUDES_PRODUCENT_DIR=$SYSTEM_PORTABLE_INCLUDES_DIR/$MACHINE_PRODUCENT
export SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR=$SYSTEM_PORTABLE_INCLUDES_PRODUCENT_DIR/$MACHINE_FAMILY
export SYSTEM_PORTABLE_INCLUDES_MACHINE_DIR=$SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR/$MACHINE_NAME
export SYSTEM_PORTABLE_INCLUDES_LLD_DIR=$SYSTEM_PORTABLE_INCLUDES_DIR/lld
export SYSTEM_PORTABLE_INCLUDES_MSLLD_DIR=$SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR/mslld
export SYSTEM_PORTABLE_INCLUDES_MCS_DIR=$SYSTEM_PORTABLE_INCLUDES_DIR/mcs
export SYSTEM_PORTABLE_INCLUDES_MCS_CORE_PRODUCENT_DIR=$SYSTEM_PORTABLE_INCLUDES_MCS_DIR/$MACHINE_ARCHITECTURE
export SYSTEM_PORTABLE_INCLUDES_MCS_CORE_DIR=$SYSTEM_PORTABLE_INCLUDES_MCS_CORE_PRODUCENT_DIR/$MACHINE_CORE_ARCHITECTURE_FAMILY
export SYSTEM_PORTABLE_SCRIPTS_DIR=$SYSTEM_PORTABLE_DIR/scripts
export SYSTEM_PORTABLE_SCRIPTS_PRODUCENT_DIR=$SYSTEM_PORTABLE_SCRIPTS_DIR/$MACHINE_PRODUCENT
export SYSTEM_PORTABLE_SCRIPTS_FAMILY_DIR=$SYSTEM_PORTABLE_SCRIPTS_PRODUCENT_DIR/$MACHINE_FAMILY
export SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR=$SYSTEM_PORTABLE_SCRIPTS_FAMILY_DIR/$MACHINE_NAME
export SYSTEM_PORTABLE_SOURCES_DIR=$SYSTEM_PORTABLE_DIR/src
export SYSTEM_PORTABLE_SOURCES_PRODUCENT_DIR=$SYSTEM_PORTABLE_SOURCES_DIR/$MACHINE_PRODUCENT
export SYSTEM_PORTABLE_SOURCES_FAMILY_DIR=$SYSTEM_PORTABLE_SOURCES_PRODUCENT_DIR/$MACHINE_FAMILY
export SYSTEM_PORTABLE_SOURCES_MACHINE_DIR=$SYSTEM_PORTABLE_SOURCES_FAMILY_DIR/$MACHINE_NAME
export SYSTEM_PORTABLE_SOURCES_LLD_DIR=$SYSTEM_PORTABLE_SOURCES_FAMILY_DIR/lld
export SYSTEM_PORTABLE_SOURCES_MSLLD_DIR=$SYSTEM_PORTABLE_SOURCES_FAMILY_DIR/mslld
export SYSTEM_PORTABLE_SOURCES_MCS_DIR=$SYSTEM_PORTABLE_SOURCES_DIR/mcs
export SYSTEM_PORTABLE_SOURCES_MCS_CORE_PRODUCENT_DIR=$SYSTEM_PORTABLE_SOURCES_MCS_DIR/$MACHINE_ARCHITECTURE
export SYSTEM_PORTABLE_SOURCES_MCS_CORE_DIR=$SYSTEM_PORTABLE_SOURCES_MCS_CORE_PRODUCENT_DIR/$MACHINE_CORE_ARCHITECTURE_FAMILY

##----------------------------------------------------------
## System user directories
##----------------------------------------------------------
export SYSTEM_USER_INCLUDES_DIR=$SYSTEM_USER_DIR/inc
export SYSTEM_USER_SOURCES_DIR=$SYSTEM_USER_DIR/src

##----------------------------------------------------------
## User directories
##----------------------------------------------------------
export USER_LIBRARIES_DIR=$USER_DIR/libraries
export USER_PROGRAM_DIR=$USER_DIR/programs
export USER_INCLUDES_DIR=$SYSTEM_USER_DIR/inc
export USER_SOURCES_DIR=$SYSTEM_USER_DIR/src

##----------------------------------------------------------
## Machine directories
##----------------------------------------------------------
export MACHINE_SCRIPTS_DIR=$SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR
export MACHINE_OUTPUT_DIR=$OUTPUT_DIR/$MACHINE_NAME

##----------------------------------------------------------
## Tools directories
##----------------------------------------------------------
export OPENOCD_0_8_0_DIR=$TOOLS_DIR/openocd-0-8-0
export OPENOCD_0_8_0_BIN_DIR=$OPENOCD_0_8_0_DIR/bin
export OPENOCD_0_8_0_SCRIPTS_DIR=$OPENOCD_0_8_0_DIR/scripts
export OPENOCD_DIR=$TOOLS_DIR/openocd
export OPENOCD_BIN_DIR=$OPENOCD_DIR/bin-linux/
export OPENOCD_SCRIPTS_DIR=$OPENOCD_DIR/scripts
export COMPILERS_DIR=$TOOLS_DIR/compilers
export ARM_NONE_EABI_GCC_DIR=$COMPILERS_DIR/arm_none_eabi_gcc_linux
export ARM_NONE_EABI_GCC_BIN_DIR=$ARM_NONE_EABI_GCC_DIR/bin
export ARM_NONE_EABI_DIR=$ARM_NONE_EABI_GCC_DIR/arm_none_eabi
export ARM_NONE_EABI_BIN_DIR=$ARM_NONE_EABI_DIR/bin
export ARM_NONE_EABI_INCLUDE_DIR=$ARM_NONE_EABI_DIR/include
export MAKE32_DIR=$COMPILERS_DIR/make32
export MAKE64_DIR=$COMPILERS_DIR/make64
export DRIVERGEN_DIR=$TOOLS_DIR

##////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
##
##		FILES NAMES
##
##////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#----------------------------------------------------------
# Makefiles files names
#----------------------------------------------------------
export LIBRARIES_SPACE_MK_FILE_NAME=libraries.mak
export PORTABLE_SPACE_MK_FILE_NAME=portable.mak
export CORE_SPACE_MK_FILE_NAME=core.mak
export USER_SPACE_MK_FILE_NAME=user.mak
export PROGRAM_DEFS_MK_FILE_NAME=Makefile.mak
export PORTABLE_DEFS_MK_FILE_NAME=definitions.mak
export GLOBAL_DEFS_MK_FILE_NAME=global_defs.mak
export PROGRAM_MK_FILE_NAME=program.mak
export LINKAGE_MK_FILE_NAME=linkage.mak
export PROGRAM_LD_FILE_NAME=program.ld
export PROGRAM_ELF_LD_FILE_NAME=program-elf.ld

##----------------------------------------------------------
## Dependences files names
##----------------------------------------------------------
export LIBRARIES_SPACE_DEP_FILE_NAME=libraries_dep.d
export PORTABLE_SPACE_DEP_FILE_NAME=portable_dep.d
export CORE_SPACE_DEP_FILE_NAME=core_dep.d
export USER_SPACE_DEP_FILE_NAME=user_dep.d
export PROGRAM_DEP_FILE_NAME=lib$PROGRAM_DIR_NAME.a

##----------------------------------------------------------
## Libraries files names
##----------------------------------------------------------
export LIBRARIES_SPACE_LIB_FILE_NAME=liblibrariesspace.a
export PORTABLE_SPACE_LIB_FILE_NAME=libportablespace.a
export CORE_SPACE_LIB_FILE_NAME=libcorespace.a
export USER_SPACE_LIB_FILE_NAME=libuserspace.so
export PROGRAM_LIB_FILE_NAME=lib$PROGRAM_DIR_NAME.a

##----------------------------------------------------------
## Output files names
##----------------------------------------------------------
export PROJECT_MAP_FILE_NAME=$PROJECT_NAME.map
export PROJECT_ELF_FILE_NAME=$PROJECT_NAME.elf
export PROJECT_HEX_FILE_NAME=$PROJECT_NAME.hex
export PROJECT_BIN_FILE_NAME=$PROJECT_NAME.bin
export PROJECT_DMP_FILE_NAME=$PROJECT_NAME.dmp
export PROJECT_LST_FILE_NAME=$PROJECT_NAME.lst
export PROJECT_SIZE_FILE_NAME=$PROJECT_NAME.size
export PROGRAM_ELF_FILE_NAME=$PROGRAM_DIR_NAME.elf
export PROGRAM_BIN_FILE_NAME=$PROGRAM_DIR_NAME.cbin
export PROGRAM_LST_FILE_NAME=$PROGRAM_DIR_NAME.lst

##----------------------------------------------------------
## Machine files names
##----------------------------------------------------------
export MACHINE_CONNECT_FILE_NAME=connect.sh
export MACHINE_FLASH_FILE_NAME=flash.sh
export MACHINE_ARCH_CFG_FILE_NAME=arch_cfg.sh
export MACHINE_GDB_FILE_NAME=gdb_flash.gdb
export MACHINE_STARTUP_FILE_NAME=startup.s
export MACHINE_MEMORY_LD_FILE_NAME=memory_cfg.ld
export MACHINE_COMMON_LD_FILE_NAME=common.ld

##----------------------------------------------------------
## Special headers names
##----------------------------------------------------------
export MACHINE_TYPES_HEADER_FILE_NAME=oc_machine_types.h
export MACHINE_DEFS_HEADER_FILE_NAME=oc_machine_defs.h
export MACHINE_LIST_HEADER_FILE_NAME=oc_machine_list.h

##----------------------------------------------------------
## Scripts files names
##----------------------------------------------------------
export FLASH_FILE_NAME=flash.sh
export CONNECT_FILE_NAME=connect.sh
export SELECT_ARCH_FILE_NAME=select_arch.sh
export SEARCH_ARCH_FILE_NAME=search_arch.sh
export CONSOLE_FILE_NAME=console.sh
export LINK_FILE_NAME=link.sh
export BUILD_FILE_NAME=build.sh
export BUILD_PROGRAM_FILE_NAME=build_program.sh
export CONFIGURE_FILE_NAME=configure.sh
export PREPARE_FLASHFS_FILE_NAME=pflashfs.sh
export LOAD_USER_FILE_NAME=loaduser.sh
export NEW_DRIVER_FILE_NAME=newdriver.sh
export SET_USER_FILE_NAME=set_user.sh
export RUN_CONTAINER_FILE_NAME=run.sh

##----------------------------------------------------------
## Tools files names
##----------------------------------------------------------
export DRIVERGEN_FILE_NAME=drivergen
export TMPLPARS_FILE_NAME=tmplpars

##////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
##
##		FILES PATHS
##
##////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


##----------------------------------------------------------
## Makefiles files paths
##----------------------------------------------------------
export LIBRARIES_SPACE_MK_FILE_PATH=$SYSTEM_LIBRARIES_DIR/$LIBRARIES_SPACE_MK_FILE_NAME
export PORTABLE_SPACE_MK_FILE_PATH=$SYSTEM_PORTABLE_DIR/$PORTABLE_SPACE_MK_FILE_NAME
export CORE_SPACE_MK_FILE_PATH=$SYSTEM_CORE_DIR/$CORE_SPACE_MK_FILE_NAME
export USER_SPACE_MK_FILE_PATH=$SYSTEM_USER_DIR/$USER_SPACE_MK_FILE_NAME
export SYSTEM_PROGRAM_DEFS_MK_FILE_PATH=$SYSTEM_PROGRAM_DIR/$PROGRAM_DEFS_MK_FILE_NAME
export USER_PROGRAM_DEFS_MK_FILE_PATH=$USER_PROGRAM_DIR/$PROGRAM_DEFS_MK_FILE_NAME
export PORTABLE_DEFS_MK_FILE_PATH=$SYSTEM_PORTABLE_SCRIPTS_MACHINE_DIR/$PORTABLE_DEFS_MK_FILE_NAME
export GLOBAL_DEFS_MK_FILE_PATH=$PROJECT_DIR/$GLOBAL_DEFS_MK_FILE_NAME
export PROGRAM_MK_FILE_PATH=$PROJECT_DIR/$PROGRAM_MK_FILE_NAME
export LINKAGE_MK_FILE_PATH=$SOURCE_DIR/$LINKAGE_MK_FILE_NAME
export PROGRAM_LD_FILE_PATH=$PROJECT_DIR/$PROGRAM_LD_FILE_NAME
export PROGRAM_ELF_LD_FILE_PATH=$PROJECT_DIR/$PROGRAM_ELF_LD_FILE_NAME

##----------------------------------------------------------
## Dependences files paths
##----------------------------------------------------------
export LIBRARIES_SPACE_DEP_FILE_PATH=$MACHINE_OUTPUT_DIR/$LIBRARIES_SPACE_DEP_FILE_NAME
export PORTABLE_SPACE_DEP_FILE_PATH=$MACHINE_OUTPUT_DIR/$PORTABLE_SPACE_DEP_FILE_NAME
export CORE_SPACE_DEP_FILE_PATH=$MACHINE_OUTPUT_DIR/$CORE_SPACE_DEP_FILE_NAME
export USER_SPACE_DEP_FILE_PATH=$MACHINE_OUTPUT_DIR/$USER_SPACE_DEP_FILE_NAME
export PROGRAM_DEP_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROGRAM_DEP_FILE_NAME

##----------------------------------------------------------
## Libraries files paths
##----------------------------------------------------------
export LIBRARIES_SPACE_LIB_FILE_PATH=$MACHINE_OUTPUT_DIR/$LIBRARIES_SPACE_LIB_FILE_NAME
export PORTABLE_SPACE_LIB_FILE_PATH=$MACHINE_OUTPUT_DIR/$PORTABLE_SPACE_LIB_FILE_NAME
export CORE_SPACE_LIB_FILE_PATH=$MACHINE_OUTPUT_DIR/$CORE_SPACE_LIB_FILE_NAME
export USER_SPACE_LIB_FILE_PATH=$MACHINE_OUTPUT_DIR/$USER_SPACE_LIB_FILE_NAME
export PROGRAM_LIB_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROGRAM_LIB_FILE_NAME

##----------------------------------------------------------
## Output files paths
##----------------------------------------------------------
export PROJECT_MAP_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROJECT_MAP_FILE_NAME
export PROJECT_ELF_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROJECT_ELF_FILE_NAME
export PROJECT_HEX_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROJECT_HEX_FILE_NAME
export PROJECT_BIN_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROJECT_BIN_FILE_NAME
export PROJECT_DMP_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROJECT_DMP_FILE_NAME
export PROJECT_LST_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROJECT_LST_FILE_NAME
export PROJECT_SIZE_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROJECT_SIZE_FILE_NAME
export PROGRAM_ELF_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROGRAM_ELF_FILE_NAME
export PROGRAM_BIN_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROGRAM_BIN_FILE_NAME
export PROGRAM_LST_FILE_PATH=$MACHINE_OUTPUT_DIR/$PROGRAM_LST_FILE_NAME

##----------------------------------------------------------
## Machine files paths
##----------------------------------------------------------
export MACHINE_CONNECT_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_CONNECT_FILE_NAME
export MACHINE_FLASH_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_FLASH_FILE_NAME
export MACHINE_ARCH_CFG_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_ARCH_CFG_FILE_NAME
export MACHINE_GDB_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_GDB_FILE_NAME
export MACHINE_STARTUP_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_STARTUP_FILE_NAME
export MACHINE_MEMORY_LD_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_MEMORY_LD_FILE_NAME
export MACHINE_COMMON_LD_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_COMMON_LD_FILE_NAME

##----------------------------------------------------------
## Scripts files paths
##----------------------------------------------------------
export FLASH_FILE_PATH=$SOURCE_DIR/flash.sh
export CONNECT_FILE_PATH=$SOURCE_DIR/connect.sh
export SELECT_ARCH_FILE_PATH=$SOURCE_DIR/$SELECT_ARCH_FILE_NAME
export SEARCH_ARCH_FILE_PATH=$SOURCE_DIR/$SEARCH_ARCH_FILE_NAME
export CONSOLE_FILE_PATH=$PROJECT_DIR/$CONSOLE_FILE_NAME
export LINK_FILE_PATH=$SOURCE_DIR/$LINK_FILE_NAME
export BUILD_FILE_PATH=$SOURCE_DIR/$BUILD_FILE_NAME
export BUILD_PROGRAM_FILE_PATH=$SOURCE_DIR/$BUILD_PROGRAM_FILE_NAME
export CONFIGURE_FILE_PATH=$SOURCE_DIR/$CONFIGURE_FILE_NAME
export PREPARE_FLASHFS_FILE_PATH=$SOURCE_DIR/$PREPARE_FLASHFS_FILE_NAME
export LOAD_USER_FILE_PATH=$SOURCE_DIR/$LOAD_USER_FILE_NAME
export NEW_DRIVER_FILE_PATH=$SOURCE_DIR/$NEW_DRIVER_FILE_NAME
export SET_USER_FILE_PATH=$SOURCE_DIR/$SET_USER_FILE_NAME
export RUN_CONTAINER_FILE_PATH=$DOCKER_DIR/$RUN_CONTAINER_FILE_NAME

##----------------------------------------------------------
## Tools files paths
##----------------------------------------------------------
export DRIVERGEN_FILE_PATH=$DRIVERGEN_DIR/$DRIVERGEN_FILE_NAME
export TMPLPARS_FILE_PATH=$TOOLS_DIR/$TMPLPARS_FILE_NAME

##----------------------------------------------------------
## Machine files paths
##----------------------------------------------------------
export MACHINE_CONNECT_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_CONNECT_FILE_NAME
export MACHINE_FLASH_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_FLASH_FILE_NAME
export MACHINE_GDB_FILE_PATH=$MACHINE_SCRIPTS_DIR/$MACHINE_GDB_FILE_NAME
export MACHINE_TYPES_HEADER_FILE_PATH=$SYSTEM_PORTABLE_INCLUDES_FAMILY_DIR/$MACHINE_TYPES_HEADER_FILE_NAME
export MACHINE_DEFS_HEADER_FILE_PATH=$SYSTEM_PORTABLE_INCLUDES_MACHINE_DIR/$MACHINE_DEFS_HEADER_FILE_NAME
export MACHINE_LIST_HEADER_FILE_NAME=$SYSTEM_PORTABLE_INCLUDES_DIR/$MACHINE_LIST_HEADER_FILE_NAME
