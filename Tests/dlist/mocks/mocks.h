/*
 * mocks.h
 *
 *  Created on: 21 maj 2014
 *      Author: kubiakp
 */
#ifndef MOCKS_H_
#define MOCKS_H_

#include <stdbool.h>
#include <string.h>
#include <stdarg.h>
#include "list.h"
#include "mock_param_list.h"
#include "mock_param.h"
#include "mock_struct.h"
#include "mock_return.h"

#define MEM_ALIGNMENT 4
#define MEM_ALIGN_SIZE(size) (((size) + MEM_ALIGNMENT - 1) & ~(MEM_ALIGNMENT-1))
#define _ARG16(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, ...) _15
#define HAS_COMMA(...) _ARG16(__VA_ARGS__, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0)
#define _TRIGGER_PARENTHESIS_(...) ,

#define ISEMPTY(...)                                                    \
_ISEMPTY(                                                               \
          /* test if there is just one argument, eventually an empty    \
             one */                                                     \
          HAS_COMMA(__VA_ARGS__),                                       \
          /* test if _TRIGGER_PARENTHESIS_ together with the argument   \
             adds a comma */                                            \
          HAS_COMMA(_TRIGGER_PARENTHESIS_ __VA_ARGS__),                 \
          /* test if the argument together with a parenthesis           \
             adds a comma */                                            \
          HAS_COMMA(__VA_ARGS__ (/*empty*/)),                           \
          /* test if placing it between _TRIGGER_PARENTHESIS_ and the   \
             parenthesis adds a comma */                                \
          HAS_COMMA(_TRIGGER_PARENTHESIS_ __VA_ARGS__ (/*empty*/))      \
          )

#define PASTE5(_0, _1, _2, _3, _4) _0 ## _1 ## _2 ## _3 ## _4
#define _ISEMPTY(_0, _1, _2, _3) HAS_COMMA(PASTE5(_IS_EMPTY_CASE_, _0, _1, _2, _3))
#define _IS_EMPTY_CASE_0001 ,

#define PP_NARG(...) \
    PP_NARG_(__VA_ARGS__ ,PP_RSEQ_N())
#define PP_NARG_(...) \
    PP_ARG_N(__VA_ARGS__)
#define PP_ARG_N( \
     _1, _2, _3, _4, _5, _6, _7, _8, _9,_10, \
    _11,_12,_13,_14,_15,_16,_17,_18,_19,_20, \
    _21,_22,_23,_24,_25,_26,_27,_28,_29,_30, \
    _31,_32,_33,_34,_35,_36,_37,_38,_39,_40, \
    _41,_42,_43,_44,_45,_46,_47,_48,_49,_50, \
    _51,_52,_53,_54,_55,_56,_57,_58,_59,_60, \
    _61,_62,_63,  N, ...) N
#define PP_RSEQ_N() \
    63,62,61,60,                   \
    59,58,57,56,55,54,53,52,51,50, \
    49,48,47,46,45,44,43,42,41,40, \
    39,38,37,36,35,34,33,32,31,30, \
    29,28,27,26,25,24,23,22,21,20, \
    19,18,17,16,15,14,13,12,11,10, \
     9, 8, 7, 6, 5, 4, 3, 2, 1, 0

//#define my_func(...)     func(PP_NARG(__VA_ARGS__), __VA_ARGS__)

//#define VA_NUM_ARGS(...) VA_NUM_ARGS_IMPL(__VA_ARGS__, 20 , 19 , 18 , 17 , 16 , 15 , 14 , 13 , 12 , 11 , 10 , 9 , 8 , 7 , 6 , 5,4,3,2,1,0)
//#define VA_NUM_ARGS_IMPL(_0 , _1,_2,_3,_4,_5,_6 , _7 , _8 , _9 , _10,_11,_12,_13,_14,_15,_16,_17,_18,_19,_20,N,...) N
#define VA_NUM_ARGS(...) (ISEMPTY(__VA_ARGS__)) ? 0 : PP_NARG(__VA_ARGS__)

extern void _Mocks_BeginStep();
extern void _Mocks_FinishStep();
extern void _Mocks_FinishLoop();

#define _ADD_PARAM_TO_LIST( _LIST_ , _PARAM_NAME_ ) {\
    MockParam_t *param = MockParam_new(false , &_PARAM_NAME_ , sizeof(_PARAM_NAME_) , #_PARAM_NAME_);   \
    MockParamList_add( _LIST_ , param );}

#define _ADD_PARAM_TO_EXPECT_LIST( _PARAM_TYPE_ , _PARAM_NAME_ )   {                                \
    MockParam_t* _Param = MockParam_new( false , NULL , 0 , #_PARAM_NAME_ );\
    if( arg_nr <= n )                                                                               \
    {                                                                                               \
        MockParam_SetShouldBeChecked( _Param , va_arg(arguments , int) );                           \
        arg_nr++;                                                                                   \
        if ( MockParam_GetShouldBeChecked(_Param) && (arg_nr <= n) )                                \
        {                                                                                           \
            struct _tmp_t {_PARAM_TYPE_ value;} _PARAM_NAME_##Value = va_arg(arguments, struct _tmp_t);\
            arg_nr++;                                                                               \
            MockParam_SetValue( _Param , &_PARAM_NAME_##Value.value , sizeof(_PARAM_NAME_##Value.value) );     \
        }                                                                                           \
        else                                                                                        \
        {                                                                                           \
            MockParam_SetShouldBeChecked( _Param , false );                                         \
        }                                                                                           \
    }                                                                                               \
    MockStruct_AddExpectParameter(mock , _Param);                                                   \
    MockParam_delete(&_Param);                                                                      \
    }                                                                                               \


// mocks files includes


//
//  List of EXPECT MOCKS

#endif /* MOCKS_H_ */

