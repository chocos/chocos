#include <stm.h>

////////////////////////////////////////////////////////////////////////////////
// YOUR INCLUDES
//
#include <dlist/oc_dlist.h>

/*******************************************************************************
 * Test Setup (function is called before every test)
 ******************************************************************************/
BEGIN_SETUP_TEST()
{

}
END_SETUP_TEST

/*******************************************************************************
 * Test destroy (function is called after every test)
 ******************************************************************************/
BEGIN_DESTROY_TEST()
{

}
END_DESTROY_TEST

/*******************************************************************************
 * CorrectListTest
 ******************************************************************************/
BEGIN_TEST( CorrectListTest )
{
    oC_DLIST_t * dlist;

    BEGIN_SINGLE_STEP( "Create new list" );
    {
        dlist = oC_DLIST_New();

        PASSED_IF_NOT_EQUAL( dlist , NULL , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Check if list is corect after create new" )
    {
        bool listCorrect = oC_DLIST_IsListCorrect( dlist );

        PASSED_IF_EQUAL( listCorrect , true , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Delete dynamic list" );
    {
        oC_DLIST_Delete( dlist );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Check if list is corect after delete it" )
    {
        bool listCorrect = oC_DLIST_IsListCorrect( dlist );

        PASSED_IF_EQUAL( listCorrect , false , UINT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * CheckWithNull
 ******************************************************************************/
BEGIN_TEST( CheckWithNull )
{
    BEGIN_SINGLE_STEP("Checking is list correct with NULL pointer")
    {
        bool listCorrect = oC_DLIST_IsListCorrect( NULL );

        PASSED_IF_EQUAL( listCorrect , false , UINT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * List of test to start
 ******************************************************************************/
BEGIN_TEST_SUITE(dlist_IsListCorrect)
{
    PERFORM_TEST(CorrectListTest);
    PERFORM_TEST(CheckWithNull);
}
END_TEST_SUITE
