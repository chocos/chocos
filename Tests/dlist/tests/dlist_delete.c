#include <stm.h>

////////////////////////////////////////////////////////////////////////////////
// YOUR INCLUDES
//
#include <dlist/oc_dlist.h>

/*******************************************************************************
 * Test Setup (function is called before every test)
 ******************************************************************************/
BEGIN_SETUP_TEST()
{

}
END_SETUP_TEST

/*******************************************************************************
 * Test destroy (function is called after every test)
 ******************************************************************************/
BEGIN_DESTROY_TEST()
{

}
END_DESTROY_TEST

/*******************************************************************************
 * CreateAndRemove
 ******************************************************************************/
BEGIN_TEST( CreateAndRemove )
{
    oC_DLIST_t * dlist;

    BEGIN_SINGLE_STEP( "Create new dynamic list" );
    {
        dlist = oC_DLIST_New();

        PASSED_IF_NOT_EQUAL( dlist , NULL , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Delete dynamic list" );
    {
        oC_DLIST_Delete( dlist );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Check if list is correct" )
    {
        bool listCorrect = oC_DLIST_IsListCorrect(dlist);

        PASSED_IF_EQUAL( listCorrect , false , UINT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * List of test to start
 ******************************************************************************/
BEGIN_TEST_SUITE(dlist_delete)
{
    PERFORM_TEST(CreateAndRemove);
}
END_TEST_SUITE
