#include <stm.h>

////////////////////////////////////////////////////////////////////////////////
// YOUR INCLUDES
//
#include <dlist/oc_dlist.h>

/*******************************************************************************
 * Test Setup (function is called before every test)
 ******************************************************************************/
BEGIN_SETUP_TEST()
{

}
END_SETUP_TEST

/*******************************************************************************
 * Test destroy (function is called after every test)
 ******************************************************************************/
BEGIN_DESTROY_TEST()
{

}
END_DESTROY_TEST

/*******************************************************************************
 * CreateNewObject
 ******************************************************************************/
BEGIN_TEST( CreateNewObject )
{
    BEGIN_SINGLE_STEP( "Check if object is correctly created" );
    {
        oC_DLIST_t * dlist = oC_DLIST_New();

        PASSED_IF_NOT_EQUAL( dlist , NULL , UINT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * List of test to start
 ******************************************************************************/
BEGIN_TEST_SUITE(dlist_new)
{
    PERFORM_TEST(CreateNewObject);
}
END_TEST_SUITE
