#include <stm.h>

////////////////////////////////////////////////////////////////////////////////
// YOUR INCLUDES
//
#include <dlist/oc_dlist.h>

/*******************************************************************************
 * Test Setup (function is called before every test)
 ******************************************************************************/
BEGIN_SETUP_TEST()
{

}
END_SETUP_TEST

/*******************************************************************************
 * Test destroy (function is called after every test)
 ******************************************************************************/
BEGIN_DESTROY_TEST()
{

}
END_DESTROY_TEST

/*******************************************************************************
 * CheckWithNull
 ******************************************************************************/
BEGIN_TEST( CheckWithNull )
{
    BEGIN_SINGLE_STEP( "Check if list is empty with null pointer" );
    {
        bool empty = oC_DLIST_IsEmpty( NULL );

        PASSED_IF_EQUAL( empty , true , UINT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * NotInitializedList
 ******************************************************************************/
BEGIN_TEST( NotInitializedList )
{
    oC_DLIST_t * dlist;

    BEGIN_SINGLE_STEP( "Check if dlist is empty with not initialized list" );
    {
        bool empty = oC_DLIST_IsEmpty( dlist );

        PASSED_IF_EQUAL( empty , true , UINT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * InitializedListTests
 ******************************************************************************/
BEGIN_TEST( InitializedListTests )
{
    oC_DLIST_t * dlist;

    BEGIN_SINGLE_STEP( "Create new list" );
    {
        dlist = oC_DLIST_New();

        PASSED_IF_NOT_EQUAL( dlist , NULL , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Check with initialized, empty list" );
    {
        bool empty = oC_DLIST_IsEmpty( dlist );

        PASSED_IF_EQUAL( empty , true , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Push element to back of the list" );
    {
        int i = 10;

        bool pushBackResult = oC_DLIST_EmplacePushBack( dlist , &i , sizeof(int) );

        PASSED_IF_EQUAL( pushBackResult , true , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Check with initialized, not empty list" );
    {
        bool empty = oC_DLIST_IsEmpty( dlist );

        PASSED_IF_EQUAL( empty , false , UINT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * List of test to start
 ******************************************************************************/
BEGIN_TEST_SUITE(dlist_IsEmpty)
{
    PERFORM_TEST(CheckWithNull);
    PERFORM_TEST(NotInitializedList);
    PERFORM_TEST(InitializedListTests);
}
END_TEST_SUITE
