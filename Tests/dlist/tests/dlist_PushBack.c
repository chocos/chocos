#include <stm.h>

////////////////////////////////////////////////////////////////////////////////
// YOUR INCLUDES
//
#include <dlist/oc_dlist.h>

/*******************************************************************************
 * Test Setup (function is called before every test)
 ******************************************************************************/
BEGIN_SETUP_TEST()
{

}
END_SETUP_TEST

/*******************************************************************************
 * Test destroy (function is called after every test)
 ******************************************************************************/
BEGIN_DESTROY_TEST()
{

}
END_DESTROY_TEST

/*******************************************************************************
 * CallsWithWrongParameters
 ******************************************************************************/
BEGIN_TEST( CallsWithWrongParameters )
{
    int value = 10;
    oC_DLIST_t * dlist;

    BEGIN_SINGLE_STEP( "Call with null list pointer" );
    {
        bool pushBackResult = oC_DLIST_PushBack( NULL , &value , sizeof(int) );

        PASSED_IF_EQUAL( pushBackResult , false , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Create new list" );
    {
        dlist = oC_DLIST_New();

        PASSED_IF_NOT_EQUAL( dlist , NULL , UINT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Call with NULL value pointer" );
    {
        bool pushBackResult = oC_DLIST_PushBack( dlist , NULL , sizeof(int) );
        int elementCounter  = oC_DLIST_Count( dlist );

        PASSED_IF_EQUAL( pushBackResult , false , UINT );
        PASSED_IF_EQUAL( elementCounter , 0 , INT );
    }
    END_SINGLE_STEP;

    BEGIN_SINGLE_STEP( "Call with NULL value pointer" );
    {
        bool pushBackResult = oC_DLIST_PushBack( dlist , &value , 0 );
        int elementCounter  = oC_DLIST_Count( dlist );

        PASSED_IF_EQUAL( pushBackResult , false , UINT );
        PASSED_IF_EQUAL( elementCounter , 0 , INT );
    }
    END_SINGLE_STEP;
}
END_TEST

/*******************************************************************************
 * PushNTakeElements
 ******************************************************************************/
BEGIN_TEST( PushNTakeElements )
{
    int expectedElementsCounter = 0;
    oC_DLIST_t * dlist;
    int arrayToPush[10] = {0,1,2,3,4,5,6,7,8,9};

    BEGIN_SINGLE_STEP( "Create new list" );
    {
        dlist = oC_DLIST_New();

        PASSED_IF_NOT_EQUAL( dlist , NULL , UINT );
    }
    END_SINGLE_STEP;


    BEGIN_LOOP_STEP("Push 10 elements to list" , 10)
    {
        bool pushBackResult = oC_DLIST_PushBack( dlist , &FROM_ARRAY_IN_LOOP(arrayToPush) , sizeof(int) );
        int elementCounter  = oC_DLIST_Count( dlist );

        expectedElementsCounter++;

        PASSED_IF_EQUAL( pushBackResult , true , UINT );
        PASSED_IF_EQUAL( elementCounter , expectedElementsCounter , INT );
    }
    END_LOOP_STEP;

    BEGIN_LOOP_STEP("Take 10 elements from list" , 10)
    {
        int * elementAddress= oC_DLIST_TakeFromFront(dlist);

        expectedElementsCounter--;

        PASSED_IF_EQUAL( elementAddress , &FROM_ARRAY_IN_LOOP(arrayToPush) , UINT );
    }
    END_LOOP_STEP;
}
END_TEST

/*******************************************************************************
 * List of test to start
 ******************************************************************************/
BEGIN_TEST_SUITE(dlist_PushBack)
{
    PERFORM_TEST(CallsWithWrongParameters);
    PERFORM_TEST(PushNTakeElements);
}
END_TEST_SUITE
