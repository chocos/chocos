@echo off
echo Cleaning objects...
call save_path.bat
cd %CORE_DIR%
make clean CURRENT_DIR=%PATH_TO_CURRENT_DIR%
cd %CURRENT_DIR%
echo on
