#include "stm.h"
#include "list.h"
#include <stdbool.h>
#include <assert.h>

// Global flags
bool _ANY_ORDER = false;
list_t *_EXPECTED_MOCK_CALL_LIST;
int     _MOCK_CALL_COUNTER;
int     _EXPECT_MOCK_COUNTER;

static bool _GetMockFromListAndCheckIfIsExpected( int index , MockStruct_t **mStruct , MockParamList_t* calledParametersList , const char *name )
{
    _DEBUG_ENTER_FUNC;
    bool isExpected = false;

    /////////////////////////////////////////
    //  GETS MOCK POINTER
    MockStruct_t* expectedMock = list_get_nitem_data(_EXPECTED_MOCK_CALL_LIST, index);

    assert(expectedMock);

    //  GETS MOCK POINTER
    /////////////////////////////////////////


    /////////////////////////////////////////
    //  CHECKING IF IT IS EXPECTED
    if (  MockStruct_CheckIfMockIsExpected(expectedMock , calledParametersList , name) )
    {
        *mStruct   = expectedMock;
        isExpected = true;

        // saving called parameters
        MockStruct_SetCalledParameters(expectedMock , calledParametersList);
    }

    //  CHECKING IF IT IS EXPECTED
    /////////////////////////////////////////

    _DEBUG_EXIT_FUNC;
    return isExpected;
}

static bool _CheckIfMockIsExpected(MockParamList_t* calledParametersList, MockStruct_t **mStruct , const char *name)
{
    _DEBUG_ENTER_FUNC;
    ///////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  PREPARATION OF VARIABLES
    //

    list_t*     ExpectedMocksList       = _EXPECTED_MOCK_CALL_LIST;                 // List of expected mocks
    int         CallCnt                 = _MOCK_CALL_COUNTER;                       // How many times mocks was called
    bool        isExpected              = false;                                    // If this call of mock is expected
    int         numberOfExpectedMocks   = list_get_item_count(ExpectedMocksList);   // Number of expected mocks

    //
    //  PREPARATION OF VARIABLES
    //
    ///////////////////////////////////////////////////////////////////////////////////////////////


    if (_ANY_ORDER)
    {   //  ANY ORDER IS ON

        //  FOR EACH EXPECTED MOCK
        for (int indexOfExpectedMocks = 0; (indexOfExpectedMocks < numberOfExpectedMocks) && (isExpected == false); indexOfExpectedMocks++)
        {
            //  CHECKING IF IT IS EXPECTED
            isExpected = _GetMockFromListAndCheckIfIsExpected(indexOfExpectedMocks , mStruct , calledParametersList , name);
        }

    }
    else
    {   // ANY ORDER IS OFF

        if (CallCnt < numberOfExpectedMocks)
        {   // CORRECT CallCnt
            isExpected = _GetMockFromListAndCheckIfIsExpected(CallCnt , mStruct , calledParametersList , name);
        }

        _MOCK_CALL_COUNTER++;

    }

    _DEBUG_EXIT_FUNC;
    return isExpected;
}

#define UNEXPECTED_CALL false
#define NOT_OCCURRED false


void _Mocks_BeginStep()
{
    _DEBUG_ENTER_FUNC;
    _EXPECTED_MOCK_CALL_LIST    = list_new();

    // TODO init each mocks

    _MOCK_CALL_COUNTER      = 0;
    _EXPECT_MOCK_COUNTER    = 0;
    _DEBUG_EXIT_FUNC;
}

void _Mocks_FinishLoop()
{
    _DEBUG_ENTER_FUNC;
    int n = list_get_item_count(_EXPECTED_MOCK_CALL_LIST);

    for (int i = 0; i < n; i++) {
        MockStruct_t *str  = list_get_nitem_data(_EXPECTED_MOCK_CALL_LIST, i);

        assert(str);

        if ( (!MockStruct_GetCalled(str) && !MockStruct_GetGeneralCall(str) ) )
        {
            char *name = MockStruct_GetName(str);
            assert( name );
            _PutError("Expected call of function '%s' not occurred!\n" , name );
            ck_assert_msg(NOT_OCCURRED , "%s not occurred (FinishLoop)" , name );
        }

    }
    _DEBUG_EXIT_FUNC;
}

void _Mocks_FinishStep()
{
    _DEBUG_ENTER_FUNC;
    /* removes allocated memory by the object */
    int n = list_get_item_count(_EXPECTED_MOCK_CALL_LIST);
    for (int i = 0; i < n; i++) {
        MockStruct_t *str  = list_get_nitem_data(_EXPECTED_MOCK_CALL_LIST, i);

        assert(str);

        char *name          = MockStruct_GetName(str);

        assert(name);

        if ( !MockStruct_GetCalled(str) )
        {
            _PutError("Expected call of function '%s' not occurred!\n" , name);
            ck_assert_msg(NOT_OCCURRED , "%s not occurred (FinishStep)" , name);
        }

        MockStruct_delete(&str);
    }

    list_delete(_EXPECTED_MOCK_CALL_LIST);
    _DEBUG_EXIT_FUNC;
}

/**
 * @brief
 *      Function is checking if each mock was called, if test is passed, and it calling additional callbacks.
 */
bool _Mocks_StepControl()
{
    _DEBUG_ENTER_FUNC;
    bool isPassed = true;

    _DEBUG_EXIT_FUNC;
    return isPassed;

}

