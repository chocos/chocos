#!/bin/bash
#
#       Runs a container that allows for building and flashing of the ChocoOS
#

#
#   Path to the directory with this script
#
THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
if [ "$PROJECT_DIR" = "" ]
then 
    PROJECT_DIR="$THIS_DIR/.."
fi

#
#   Path to the configuration file
#
CONFIGURATION_FILE_PATH=~/.choco-scripts.cfg
SCRIPT_DESCRIPTION=""

#
#   Installs choco-scripts
#
function installChocoScripts()
{
    echo "Installation of the choco-scripts"

    # This line installs wget tool - you don't need to use it if you already have it
    apt-get update && apt-get install -y wget

    # This downloads an installation script and run it 
    wget -O - https://release.choco-technologies.com/scripts/install-choco-scripts.sh | bash
}

#
#   Verification of the choco scripts installation
#
if [ -f "$CONFIGURATION_FILE_PATH" ]
then 
    source $CONFIGURATION_FILE_PATH
else 
    printf "\033[31;1mChoco-Scripts are not installed for this user\033[0m\n\n"
    printf "      \033[37;1mYou can find the installation instruction here: \033[0m\n"
    printf "            \033[34;1mhttps://bitbucket.org/chocotechnologies/scripts/src/master/\033[0m\n\n"

    while true
    do
        read -p "Do you want to try to auto-install it? [Y/n]: " answer
        case $answer in 
            [Yy]* ) installChocoScripts; break;;
            [Nn]* ) echo "Skipping installation"; exit 1;;
            * ) echo "Please answer Y or n";;
        esac
    done
    exit 1
fi

#
#   Information message
#
echo "Using choco-scripts from path $CHOCO_SCRIPTS_PATH in version $CHOCO_SCRIPTS_VERSION"

#
#   Importing of the framework main script
#
source $(getChocoScriptsPath)

#
#   Runs container for the 
#
function runContainer()
{
    local interactive_str=''
    if isStringEqual "$INTERACTIVE_CONTAINER" "TRUE"
    then 
        interactive_str='-it'
    fi
    if isStringEqual "$PREPARE_FOR_DEVICE" "TRUE"
    then 
        docker run $interactive_str --rm --device-cgroup-rule='c 189:* rmw' -v /dev:/dev -p $GDB_PORT:$GDB_PORT -e TERM=xterm -e PROJECT_DIR="$PROJECT_DIR" -v "$PROJECT_DIR:$PROJECT_DIR" -w "$WORK_DIR" "$IMAGE_NAME:$IMAGE_VERSION" "$COMMAND_FILE_PATH" $COMMAND_ARGS
    else 
        docker run $interactive_str --rm -e TERM=xterm -e PROJECT_DIR="$PROJECT_DIR" --network=host -v "$PROJECT_DIR:$PROJECT_DIR" -w "$WORK_DIR" "$IMAGE_NAME:$IMAGE_VERSION" "$COMMAND_FILE_PATH" $COMMAND_ARGS
    fi
}

#
#   The function prepares a framework script to work
#
function prepareScript()
{
    defineScript "$0" "Runs a container that allows for building and flashing of the ChocoOS"
    
    addCommandLineOptionalArgument 'IMAGE_NAME' '--image-name' not_empty_string 'Name of the image to use - it should be the same as given in the build.sh script' 'chocotechnologies/chocos' ''
    addCommandLineOptionalArgument 'IMAGE_VERSION' '--image-version' not_empty_string 'Version of the image to use for building' 'latest' ''
    addCommandLineOptionalArgument 'COMMAND_FILE_PATH' '--command-file' existing_file 'Command file to execute in the container' '/bin/bash' ''
    addCommandLineOptionalArgument 'COMMAND_ARGS' '--command-args' string 'Arguments for the command in the container' '' ''
    addCommandLineOptionalArgument 'INTERACTIVE_CONTAINER' '--interactive-container|-i' bool 'If enabled, the container will be executed in the interactive mode' 'TRUE' ''
    addCommandLineOptionalArgument 'GDB_PORT' --gdb-port int 'Port used for GDB debugging' 3333 '' 
    addCommandLineOptionalArgument 'WORK_DIR' '--work-dir|-w' existing_directory 'Directory to be used as working directory' "$PROJECT_DIR" '' 
    addCommandLineOptionalArgument 'PREPARE_FOR_DEVICE' '--prepare-for-device' bool 'Prepares the container for connection with the device via openocd' 'FALSE' ''
    
    disableConfigurationPrinting
    parseCommandLineArguments "$@"
}

#######################################################################################
#
#   MAIN
#
prepareScript "$@"

runContainer
