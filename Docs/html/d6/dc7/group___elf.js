var group___elf =
[
    [ "oC_Elf_FileHeader_t", "d0/d86/structo_c___elf___file_header__t.html", [
      [ "DataEncoding", "d0/d86/structo_c___elf___file_header__t.html#aa4ac70db8154263fc1378be041a8ca76", null ],
      [ "Entry", "d0/d86/structo_c___elf___file_header__t.html#a882022eb84abd73874e5ee985afb579d", null ],
      [ "FileClass", "d0/d86/structo_c___elf___file_header__t.html#a34cb71f3559250907c771fc60e9a3d8c", null ],
      [ "Flags", "d0/d86/structo_c___elf___file_header__t.html#a83002644420acfb7486ea10aebddd64e", null ],
      [ "HeaderSize", "d0/d86/structo_c___elf___file_header__t.html#a967884af6e03d7156f29459930b56448", null ],
      [ "ID", "d0/d86/structo_c___elf___file_header__t.html#a9218478d8e9a6a65352404cfdab43da9", null ],
      [ "Machine", "d0/d86/structo_c___elf___file_header__t.html#a4f5aa1a5faa179c88e77e072538b205d", null ],
      [ "ObjectFileVersion", "d0/d86/structo_c___elf___file_header__t.html#adc8c7a56e7fe7484dd7387a3f0cd7dad", null ],
      [ "ProgramHeaderTableEntrySize", "d0/d86/structo_c___elf___file_header__t.html#a4b70c4d53195314d0c638478c88b4bd1", null ],
      [ "ProgramHeaderTableNumberOfEntries", "d0/d86/structo_c___elf___file_header__t.html#afa7b048ab596702ba49c936c11467ef5", null ],
      [ "ProgramHeaderTableOffset", "d0/d86/structo_c___elf___file_header__t.html#afa0c72bb9c0ee54cbffdf2cf9f057b95", null ],
      [ "SectionHeaderNameStringTableIndex", "d0/d86/structo_c___elf___file_header__t.html#ad00225bde1fda2f02528dab6fd0d7b5d", null ],
      [ "SectionHeaderTableEntrySize", "d0/d86/structo_c___elf___file_header__t.html#ac43eb2f3a195a193cd89689f6ef3e338", null ],
      [ "SectionHeaderTableNumberOfEntries", "d0/d86/structo_c___elf___file_header__t.html#a9d5f0077d28a1c4be812d4ea121c01f3", null ],
      [ "SectionHeaderTableOffset", "d0/d86/structo_c___elf___file_header__t.html#a46a2db1d98cc29ba0a152972f9bac425", null ],
      [ "Signature", "d0/d86/structo_c___elf___file_header__t.html#a680338b338f6f85a0f6a14cd062a29c4", null ],
      [ "Type", "d0/d86/structo_c___elf___file_header__t.html#a5f65f99c98741ba3ab70b9a15c5618e7", null ],
      [ "Version", "d0/d86/structo_c___elf___file_header__t.html#a21fe3c6f1854cb61b25653e23acb7f1a", null ]
    ] ],
    [ "oC_Elf_SectionHeader_t", "d3/d3b/structo_c___elf___section_header__t.html", [
      [ "Address", "d3/d3b/structo_c___elf___section_header__t.html#aab51e113f1bac3939ee1f52d167ba592", null ],
      [ "AddressAlign", "d3/d3b/structo_c___elf___section_header__t.html#aea5b2d685cd518a1486597058c70c4c8", null ],
      [ "EntrySize", "d3/d3b/structo_c___elf___section_header__t.html#ad0404d6a33f2a857a97e6622f8d24c72", null ],
      [ "Flags", "d3/d3b/structo_c___elf___section_header__t.html#a83002644420acfb7486ea10aebddd64e", null ],
      [ "Info", "d3/d3b/structo_c___elf___section_header__t.html#a0f5f0f1243f89237f9d3ec4120e7545f", null ],
      [ "Link", "d3/d3b/structo_c___elf___section_header__t.html#a23ca5916ecfc69ad8b1b242df0b91077", null ],
      [ "NameIndex", "d3/d3b/structo_c___elf___section_header__t.html#ac446c6017a553ecafe435fbd29a04432", null ],
      [ "Offset", "d3/d3b/structo_c___elf___section_header__t.html#a4e6bc857edae37805b6f3d32fea5e11f", null ],
      [ "Size", "d3/d3b/structo_c___elf___section_header__t.html#a70faef671439c96cda4b0f4782dba496", null ],
      [ "Type", "d3/d3b/structo_c___elf___section_header__t.html#a2fadc8faadb222e2676fc3a9ee8da473", null ]
    ] ],
    [ "oC_Elf_SymbolTableEntry_t", "df/d33/structo_c___elf___symbol_table_entry__t.html", [
      [ "Index", "df/d33/structo_c___elf___symbol_table_entry__t.html#a3643fecfe4cf59891f995ebb275ed314", null ],
      [ "Info", "df/d33/structo_c___elf___symbol_table_entry__t.html#a6f4b6973b7173efc9d3adac6130c85cf", null ],
      [ "NameIndex", "df/d33/structo_c___elf___symbol_table_entry__t.html#ac446c6017a553ecafe435fbd29a04432", null ],
      [ "Other", "df/d33/structo_c___elf___symbol_table_entry__t.html#ad02f6b2f2ac7b5252bfcf1496086c357", null ],
      [ "Size", "df/d33/structo_c___elf___symbol_table_entry__t.html#a70faef671439c96cda4b0f4782dba496", null ],
      [ "Value", "df/d33/structo_c___elf___symbol_table_entry__t.html#a442453df7d834d941b55e6cfb18ef161", null ]
    ] ],
    [ "oC_Elf_RelocationEntry_t", "d9/dc0/structo_c___elf___relocation_entry__t.html", [
      [ "Addend", "d9/dc0/structo_c___elf___relocation_entry__t.html#a5818b38c5c3b56f5d2c19d377bfc17ed", null ],
      [ "Info", "d9/dc0/structo_c___elf___relocation_entry__t.html#a0f5f0f1243f89237f9d3ec4120e7545f", null ],
      [ "Offset", "d9/dc0/structo_c___elf___relocation_entry__t.html#ad14aec814ce215cd7d099fa8dab63788", null ]
    ] ],
    [ "oC_Elf_ProgramHeader_t", "db/df9/structo_c___elf___program_header__t.html", [
      [ "Align", "db/df9/structo_c___elf___program_header__t.html#ac3d38e654e32af1a9f402672a12e93b7", null ],
      [ "FileSize", "db/df9/structo_c___elf___program_header__t.html#a3fcce405e4183842db817b7cce6dbe5c", null ],
      [ "Flags", "db/df9/structo_c___elf___program_header__t.html#a83002644420acfb7486ea10aebddd64e", null ],
      [ "MemorySize", "db/df9/structo_c___elf___program_header__t.html#a0f322a27240457c80950d99ff7b8977b", null ],
      [ "Offset", "db/df9/structo_c___elf___program_header__t.html#a4e6bc857edae37805b6f3d32fea5e11f", null ],
      [ "PhysicalAddress", "db/df9/structo_c___elf___program_header__t.html#a55d19d8b6f9274ef9dea3f1ddd92e971", null ],
      [ "Type", "db/df9/structo_c___elf___program_header__t.html#a2fadc8faadb222e2676fc3a9ee8da473", null ],
      [ "VirtualAddress", "db/df9/structo_c___elf___program_header__t.html#af982f6618657e5b61f2666259d3cedc2", null ]
    ] ],
    [ "oC_Elf_Addr_t", "d6/dc7/group___elf.html#ga477cd9a8c6144d1a0497157e46db4104", null ],
    [ "oC_Elf_Half_t", "d6/dc7/group___elf.html#ga024ced97323844dca3b41c7d357fae68", null ],
    [ "oC_Elf_Off_t", "d6/dc7/group___elf.html#ga40ea58f205eb301e7fd52c3354ba63dc", null ],
    [ "oC_Elf_Sword_t", "d6/dc7/group___elf.html#gaa67076d6de932bf94d4812ba44a1249c", null ],
    [ "oC_Elf_Word_t", "d6/dc7/group___elf.html#ga1f84e3615f8e8a3a122549a7b0b1a527", null ],
    [ "oC_Elf_FileClass_t", "d6/dc7/group___elf.html#ga336eb13ca680cbd9232295c5679cf182", [
      [ "oC_Elf_FileClass_None", "d6/dc7/group___elf.html#gga336eb13ca680cbd9232295c5679cf182ab82bf889f25776af8f5e9fa88de3175e", null ],
      [ "oC_Elf_FileClass_32", "d6/dc7/group___elf.html#gga336eb13ca680cbd9232295c5679cf182a7574d654004eab8a38c6ff1ee37a655c", null ],
      [ "oC_Elf_FileClass_64", "d6/dc7/group___elf.html#gga336eb13ca680cbd9232295c5679cf182ad1dc5430eda5bb1fd420f992be747ba8", null ]
    ] ],
    [ "oC_Elf_MachineType_t", "d6/dc7/group___elf.html#gaa2e20a666460268150e508c489593029", [
      [ "oC_Elf_MachineType_NoMachine", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029a5f55729776a58e89ad512ff829d3577d", null ],
      [ "oC_Elf_MachineType_M32", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029a0da49a84534931dfc91312276d0a3839", null ],
      [ "oC_Elf_MachineType_SPARC", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029aa2ad75e62b4da0bc11f0816d36562b31", null ],
      [ "oC_Elf_MachineType_386", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029a0f6003bc1911c98056dfd699f5a2b5f1", null ],
      [ "oC_Elf_MachineType_68K", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029a466eb7cbadc2ef6541f3a7d60c311fdd", null ],
      [ "oC_Elf_MachineType_88K", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029af5c18935f6337a1baf3c65fff74877a9", null ],
      [ "oC_Elf_MachineType_860", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029aac67ec6efb5889c62ba23b2cb10a999a", null ],
      [ "oC_Elf_MachineType_MIPS", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029a7f69ebd7210831ee42b4e48b134b8eea", null ],
      [ "oC_Elf_MachineType_ArmCortexM7", "d6/dc7/group___elf.html#ggaa2e20a666460268150e508c489593029a4952b4d35f490707977d0ca81a47564a", null ]
    ] ],
    [ "oC_Elf_ProgramType_t", "d6/dc7/group___elf.html#ga0bdf16b2a0265bc2f78b12d0af3c16cb", [
      [ "oC_Elf_ProgramType_NULL", "d6/dc7/group___elf.html#gga0bdf16b2a0265bc2f78b12d0af3c16cba90aea429653d400401966c88a96943b5", null ],
      [ "oC_Elf_ProgramType_LOAD", "d6/dc7/group___elf.html#gga0bdf16b2a0265bc2f78b12d0af3c16cbac62b3fcee030629777877bdff49b269d", null ],
      [ "oC_Elf_ProgramType_DYNAMIC", "d6/dc7/group___elf.html#gga0bdf16b2a0265bc2f78b12d0af3c16cba4aa98697c348e4f1d1e520807226d6e1", null ],
      [ "oC_Elf_ProgramType_INTERP", "d6/dc7/group___elf.html#gga0bdf16b2a0265bc2f78b12d0af3c16cba0c2c90d2b09b23eb69a648cf64d22bd6", null ],
      [ "oC_Elf_ProgramType_NOTE", "d6/dc7/group___elf.html#gga0bdf16b2a0265bc2f78b12d0af3c16cba6617fe820bff1c44a4b556581f865819", null ],
      [ "oC_Elf_ProgramType_SHLIB", "d6/dc7/group___elf.html#gga0bdf16b2a0265bc2f78b12d0af3c16cba1cb9f6437b7623eb7fdd6946e4adf882", null ],
      [ "oC_Elf_ProgramType_PHDR", "d6/dc7/group___elf.html#gga0bdf16b2a0265bc2f78b12d0af3c16cbad51deefb74a293492bc5b4675b05fd01", null ]
    ] ],
    [ "oC_Elf_SectionFlag_t", "d6/dc7/group___elf.html#ga4acfd779f64c7a2d1e27ae9d95205083", [
      [ "oC_Elf_SectionFlag_WRITE", "d6/dc7/group___elf.html#gga4acfd779f64c7a2d1e27ae9d95205083adab597617b73290999f1655b0b6bf506", null ],
      [ "oC_Elf_SectionFlag_ALLOC", "d6/dc7/group___elf.html#gga4acfd779f64c7a2d1e27ae9d95205083ab59050dfdb86e3a639fbf09258f274b1", null ],
      [ "oC_Elf_SectionFlag_EXECINSTR", "d6/dc7/group___elf.html#gga4acfd779f64c7a2d1e27ae9d95205083aaf478fe3f24ac86cf9d1b01fe3eb0b1e", null ],
      [ "oC_Elf_SectionFlag_MASKPROC", "d6/dc7/group___elf.html#gga4acfd779f64c7a2d1e27ae9d95205083a1e892cd3d3f1ec6a66e52300476b5898", null ]
    ] ],
    [ "oC_Elf_SectionType_t", "d6/dc7/group___elf.html#gac1fe9defa83928a5c61940cfc7c7a2da", [
      [ "oC_Elf_SectionType_NULL", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa4d978cac549aac54f1be202b9e6ab77e", null ],
      [ "oC_Elf_SectionType_PROGBITS", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa0196b42ea96457dd9d8cc76a0fe40869", null ],
      [ "oC_Elf_SectionType_SYMTAB", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa2ada116e1af6aa44e8db0edcf3e0d609", null ],
      [ "oC_Elf_SectionType_STRTAB", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa47e4e4f0440f16f0ea7a24edc614c10a", null ],
      [ "oC_Elf_SectionType_RELA", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa922c52f199ba5ee9f45109f118b04be7", null ],
      [ "oC_Elf_SectionType_HASH", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa9bdd39fa0e5ae740e172d19fa65ee9a1", null ],
      [ "oC_Elf_SectionType_DYNAMIC", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daaf47175b9509436f9fcd19f3b2d73af8b", null ],
      [ "oC_Elf_SectionType_NOTE", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa0eec6fb3d7db6a75b1c1b8e176cf98f3", null ],
      [ "oC_Elf_SectionType_NOBITS", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daae6488976c8c938eec84d74c41577561a", null ],
      [ "oC_Elf_SectionType_REL", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daae25696fd8e46f7e7a86b5083b79d1224", null ],
      [ "oC_Elf_SectionType_SHLIB", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa9ca7885f9d2a454c58a3a0c601249df1", null ],
      [ "oC_Elf_SectionType_DYNSYM", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daae47439bcdf99f0e4907b5ce95039d3d8", null ],
      [ "oC_Elf_SectionType_LOPROC", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa2d1abd1606ee5da7b02a191433dd54ed", null ],
      [ "oC_Elf_SectionType_HIPROC", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daafe2b9f7a759beb374b49837678f937ed", null ],
      [ "oC_Elf_SectionType_LOUSER", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daa8b88252c5e4d3748e85ca62a88aad2b9", null ],
      [ "oC_Elf_SectionType_HIUSER", "d6/dc7/group___elf.html#ggac1fe9defa83928a5c61940cfc7c7a2daada6eef5200572355af4d4d51cd4590dd", null ]
    ] ],
    [ "oC_Elf_SymbolBinding_t", "d6/dc7/group___elf.html#gadee675eb0884345408f395245247489c", [
      [ "oC_Elf_SymbolBinding_LOCAL", "d6/dc7/group___elf.html#ggadee675eb0884345408f395245247489caffc158a18507c968940454c621a1636c", null ],
      [ "oC_Elf_SymbolBinding_GLOBAL", "d6/dc7/group___elf.html#ggadee675eb0884345408f395245247489ca98f2fc156a7d4e2e7c2c1e9e3f8ad49d", null ],
      [ "oC_Elf_SymbolBinding_WEAK", "d6/dc7/group___elf.html#ggadee675eb0884345408f395245247489caff464f48be2e96cc6e8654ef0ab5387b", null ],
      [ "oC_Elf_SymbolBinding_LOPROC", "d6/dc7/group___elf.html#ggadee675eb0884345408f395245247489caa3a2fdf36559a6e162f9fea3a600a722", null ],
      [ "oC_Elf_SymbolBinding_HIPROC", "d6/dc7/group___elf.html#ggadee675eb0884345408f395245247489ca8b1a3049c0beddb9bc9c13075be66915", null ]
    ] ],
    [ "oC_Elf_SymbolType_t", "d6/dc7/group___elf.html#gae9355b25628b1c9d641d6111945dfdc0", [
      [ "oC_Elf_SymbolType_NOTYPE", "d6/dc7/group___elf.html#ggae9355b25628b1c9d641d6111945dfdc0a46d1cb0d2f2109f2105f198b0bfd8b29", null ],
      [ "oC_Elf_SymbolType_OBJECT", "d6/dc7/group___elf.html#ggae9355b25628b1c9d641d6111945dfdc0acf9eafd38794cdef82de691144143f0d", null ],
      [ "oC_Elf_SymbolType_FUNC", "d6/dc7/group___elf.html#ggae9355b25628b1c9d641d6111945dfdc0a4f4cdb2620c821f4c40b5f97c96c3316", null ],
      [ "oC_Elf_SymbolType_SECTION", "d6/dc7/group___elf.html#ggae9355b25628b1c9d641d6111945dfdc0ac09bac23c3e977e71bf548293b948651", null ],
      [ "oC_Elf_SymbolType_FILE", "d6/dc7/group___elf.html#ggae9355b25628b1c9d641d6111945dfdc0a8a6f25941897bf97b3c90e2f6e3b0622", null ],
      [ "oC_Elf_SymbolType_LOPROC", "d6/dc7/group___elf.html#ggae9355b25628b1c9d641d6111945dfdc0a0c94fd3095907e661b22b8bd1aac398c", null ],
      [ "oC_Elf_SymbolType_HIPROC", "d6/dc7/group___elf.html#ggae9355b25628b1c9d641d6111945dfdc0a160bc9941f301dabaebeed3c90249294", null ]
    ] ],
    [ "oC_Elf_Type_t", "d6/dc7/group___elf.html#ga28e663a3a8b598dbd3af411e83089d56", [
      [ "oC_Elf_Type_None", "d6/dc7/group___elf.html#gga28e663a3a8b598dbd3af411e83089d56ac45aea9d2d340e47a2be233975c2e788", null ],
      [ "oC_Elf_Type_RelocatableFile", "d6/dc7/group___elf.html#gga28e663a3a8b598dbd3af411e83089d56a91f6b593199db525cc0f4bd13dec3f07", null ],
      [ "oC_Elf_Type_ExecutableFile", "d6/dc7/group___elf.html#gga28e663a3a8b598dbd3af411e83089d56ab5176983bb207f3cd362dff5016a88a5", null ],
      [ "oC_Elf_Type_SharedObjectFile", "d6/dc7/group___elf.html#gga28e663a3a8b598dbd3af411e83089d56a1f8bcfb5510724393630114b5822bfbb", null ],
      [ "oC_Elf_Type_CoreFile", "d6/dc7/group___elf.html#gga28e663a3a8b598dbd3af411e83089d56a8531d9858b203fb237e10eabf366602e", null ],
      [ "oC_Elf_Type_LoProc", "d6/dc7/group___elf.html#gga28e663a3a8b598dbd3af411e83089d56a4ba2fd560ced6374f78dad302df259c3", null ],
      [ "oC_Elf_Type_HiProc", "d6/dc7/group___elf.html#gga28e663a3a8b598dbd3af411e83089d56ae84f60e826057e951765942a204d9779", null ]
    ] ]
];