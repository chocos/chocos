var oc__sdmmc__commif__mmc_8c =
[
    [ "ConfirmIdentification", "d6/d14/oc__sdmmc__commif__mmc_8c.html#a4d64265d7b9b892c3d852beb5bcf56a8", null ],
    [ "FinishReadingSectors", "d6/d14/oc__sdmmc__commif__mmc_8c.html#aa5b97ddc2aae1b1519c6222859aea304", null ],
    [ "FinishWritingSectors", "d6/d14/oc__sdmmc__commif__mmc_8c.html#ae563026708b73a43f0ee3ddd6f74b6a6", null ],
    [ "InitializeCard", "d6/d14/oc__sdmmc__commif__mmc_8c.html#a809e6490120442ebb11fd016cb7d9a11", null ],
    [ "StartReadingSectors", "d6/d14/oc__sdmmc__commif__mmc_8c.html#a5eb15be3563ad0d311a28e0a665c0ad8", null ],
    [ "StartWritingSectors", "d6/d14/oc__sdmmc__commif__mmc_8c.html#a6aa38e5cb6a8895cf30ed317b142c5ce", null ],
    [ "oC_SDMMC_CommIf_Handler_MMC", "d6/d14/oc__sdmmc__commif__mmc_8c.html#afcde5d148a51b57e8e49c22aae051916", null ]
];