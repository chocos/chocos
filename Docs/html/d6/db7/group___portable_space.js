var group___portable_space =
[
    [ "BA - Base Addresses", "d4/d13/group___b_a.html", "d4/d13/group___b_a" ],
    [ "Channels Module", "d9/d7a/group___channels.html", "d9/d7a/group___channels" ],
    [ "Interrupts Module", "d7/dee/group___interrupts.html", "d7/dee/group___interrupts" ],
    [ "(LSF) - Linker Specific Functions", "d6/d05/group___l_s_f.html", null ],
    [ "Machine specific functions module", "d9/d1f/group___machine.html", "d9/d1f/group___machine" ],
    [ "MCS - Machine Core Specific Module", "d1/dd2/group___m_c_s.html", "d1/dd2/group___m_c_s" ],
    [ "Pins Module", "d3/db6/group___pins_module.html", null ],
    [ "Low Level Drivers", "d6/d08/group___low_level_drivers.html", "d6/d08/group___low_level_drivers" ],
    [ "Registers Module", "d1/d9f/group___registers.html", "d1/d9f/group___registers" ],
    [ "RMaps - Register Maps module", "d6/d4a/group___r_m_a_p_s.html", "d6/d4a/group___r_m_a_p_s" ],
    [ "Module for configuration of the SAI PLL", "da/df0/group___s_t_m32_f7-_s_a_i_p_l_l.html", "da/df0/group___s_t_m32_f7-_s_a_i_p_l_l" ],
    [ "Machine Definitions (MDefs)", "d9/d13/group___m_defs.html", "d9/d13/group___m_defs" ]
];