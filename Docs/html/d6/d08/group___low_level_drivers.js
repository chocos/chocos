var group___low_level_drivers =
[
    [ "Analog to Digital Converter", "d4/d97/group___a_d_c-_l_l_d.html", null ],
    [ "Clock", "d8/df6/group___c_l_o_c_k-_l_l_d.html", "d8/df6/group___c_l_o_c_k-_l_l_d" ],
    [ "Digital to Analog Converter", "d8/dbd/group___d_a_c-_l_l_d.html", null ],
    [ "DMA Driver", "d1/dc8/group___d_m_a-_l_l_d.html", null ],
    [ "ETH", "df/d88/group___e_t_h-_l_l_d.html", null ],
    [ "FMC", "db/d8e/group___f_m_c-_l_l_d.html", null ],
    [ "General Purpose Input-Output", "df/dcc/group___g_p_i_o-_l_l_d.html", "df/dcc/group___g_p_i_o-_l_l_d" ],
    [ "I2C", "db/ddc/group___i2_c-_l_l_d.html", null ],
    [ "LCDTFT", "da/db7/group___l_c_d_t_f_t-_l_l_d.html", "da/db7/group___l_c_d_t_f_t-_l_l_d" ],
    [ "Memory", "d4/d30/group___m_e_m-_l_l_d.html", "d4/d30/group___m_e_m-_l_l_d" ],
    [ "SDMMC", "d9/de0/group___s_d_m_m_c-_l_l_d.html", "d9/de0/group___s_d_m_m_c-_l_l_d" ],
    [ "SPI", "dc/d31/group___s_p_i-_l_l_d.html", null ],
    [ "System", "df/d08/group___s_y_s-_l_l_d.html", "df/d08/group___s_y_s-_l_l_d" ],
    [ "Timers", "d5/df8/group___t_i_m_e_r-_l_l_d.html", null ],
    [ "UART", "d4/db3/group___u_a_r_t-_l_l_d.html", null ]
];