var group___kernel =
[
    [ "Exception Handler", "db/dad/group___exc_han.html", "db/dad/group___exc_han" ],
    [ "Interrupt Manager", "d5/d5d/group___int_man.html", "d5/d5d/group___int_man" ],
    [ "Kernel Print", "dc/da9/group___k_print.html", "dc/da9/group___k_print" ],
    [ "Kernel Time", "de/da6/group___k_time.html", "de/da6/group___k_time" ],
    [ "Memory Manager", "d7/d35/group___mem_man.html", "d7/d35/group___mem_man" ],
    [ "News", "d0/d7e/group___news.html", "d0/d7e/group___news" ],
    [ "Process", "d7/d90/group___process.html", "d7/d90/group___process" ],
    [ "Process Manager", "d9/d7b/group___process_man.html", "d9/d7b/group___process_man" ],
    [ "Program", "d0/d42/group___program.html", "d0/d42/group___program" ],
    [ "Program Manager", "da/da9/group___program_man.html", null ],
    [ "Service", "d8/d1a/group___service.html", null ],
    [ "Service Manager", "d7/d77/group___service_man.html", null ],
    [ "Stream", "da/d71/group___stream.html", "da/d71/group___stream" ],
    [ "Stream Manager", "d5/da2/group___stream_man.html", null ],
    [ "Thread", "d9/d71/group___thread.html", "d9/d71/group___thread" ],
    [ "Thread Manager", "d4/dc1/group___thread_man.html", null ],
    [ "User", "d3/d54/group___user.html", null ],
    [ "User Manager", "dd/d8d/group___user_man.html", null ]
];