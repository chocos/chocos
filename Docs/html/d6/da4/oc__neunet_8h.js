var oc__neunet_8h =
[
    [ "oC_NEUNET_Config_t", "d2/da8/structo_c___n_e_u_n_e_t___config__t.html", null ],
    [ "oC_NEUNET_Context_t", "d6/da4/oc__neunet_8h.html#ac572753544fcf8babd8ef50a6d827ce0", null ],
    [ "oC_NEUNET_Configure", "d6/da4/oc__neunet_8h.html#ac5103de69a38de4216b0553c1b0e697a", null ],
    [ "oC_NEUNET_Ioctl", "d6/da4/oc__neunet_8h.html#a29a5f8695c15f4e63193bdc3da8d438d", null ],
    [ "oC_NEUNET_IsTurnedOn", "d6/da4/oc__neunet_8h.html#a3a91740f316b094acc85826e827a52b1", null ],
    [ "oC_NEUNET_Read", "d6/da4/oc__neunet_8h.html#a210f9ecda4a98dd99ce1476e45c04b06", null ],
    [ "oC_NEUNET_TurnOff", "d6/da4/oc__neunet_8h.html#a73cc74ba6c08fbd15ba0bec856b4a6b8", null ],
    [ "oC_NEUNET_TurnOn", "d6/da4/oc__neunet_8h.html#a1098bba71c07c7f3e4671327bcd00e24", null ],
    [ "oC_NEUNET_Unconfigure", "d6/da4/oc__neunet_8h.html#abd790227271560c795facfbe96db83a8", null ],
    [ "oC_NEUNET_Write", "d6/da4/oc__neunet_8h.html#a6602d44c3250482736d737ed39523f3b", null ]
];