var oc__colormap_8c =
[
    [ "CopyMap", "d6/de8/oc__colormap_8c.html#a4bbbf16014f48b3ee1d9564250a391aa", null ],
    [ "GetMapRef", "d6/de8/oc__colormap_8c.html#a17d1c3bdd3f350296365c5b2910b4f88", null ],
    [ "oC_ColorMap_CloneMap", "d8/d36/group___color_map.html#ga98f809a287d0835f95a83d255c4fda85", null ],
    [ "oC_ColorMap_Delete", "d8/d36/group___color_map.html#ga94bdcdf506fc65df45a62ac5e2da2c03", null ],
    [ "oC_ColorMap_DrawAlignedString", "d8/d36/group___color_map.html#ga74f26880d659088987cf217357b49805", null ],
    [ "oC_ColorMap_DrawArrow", "d8/d36/group___color_map.html#gabeb21109fbe5df2cd19dbe2cae48b5e0", null ],
    [ "oC_ColorMap_DrawBackground", "d8/d36/group___color_map.html#ga5f91e1a8230ed55fb8883425b84e5d30", null ],
    [ "oC_ColorMap_DrawChar", "d8/d36/group___color_map.html#ga9c3fc47239626d2afd2efff17a9275a3", null ],
    [ "oC_ColorMap_DrawCircle", "d8/d36/group___color_map.html#ga2ab03b4255617173384ccefcd32b532a", null ],
    [ "oC_ColorMap_DrawImage", "d8/d36/group___color_map.html#gaefdef9d7885065b0eb9186a773a28dcd", null ],
    [ "oC_ColorMap_DrawLine", "d8/d36/group___color_map.html#ga5219d4039e44c9000fdcbea34720935f", null ],
    [ "oC_ColorMap_DrawRect", "d8/d36/group___color_map.html#gac677924e19ca6160f67ac977125fcc39", null ],
    [ "oC_ColorMap_DrawString", "d8/d36/group___color_map.html#ga67c935edf19cd71dd89f3c61b0a2477b", null ],
    [ "oC_ColorMap_DrawTriangle", "d8/d36/group___color_map.html#ga2d56ea66a9e009a9bd58f43fdc5d1637", null ],
    [ "oC_ColorMap_FillCircleWithColor", "d8/d36/group___color_map.html#gac31dfab1d141c3e5e0bedb0f149f5d25", null ],
    [ "oC_ColorMap_FillRectWithColor", "d8/d36/group___color_map.html#gab69b6b1b792ecf68d0b75eccbdd4604a", null ],
    [ "oC_ColorMap_FitPosition", "d8/d36/group___color_map.html#ga27f5ef5248b7a98ec7dbd7aeeb32f7c4", null ],
    [ "oC_ColorMap_GetActiveLayer", "d8/d36/group___color_map.html#ga327b3f811e9e2500092cd80c062c351b", null ],
    [ "oC_ColorMap_GetColor", "d8/d36/group___color_map.html#ga7cdb44fe9304a15a18e5cbc4a47d5c81", null ],
    [ "oC_ColorMap_GetInactiveLayer", "d8/d36/group___color_map.html#ga21143ba2e7ff330f674d5fdb48618aec", null ],
    [ "oC_ColorMap_GetNumberOfLayers", "d8/d36/group___color_map.html#ga3e7188f9245eea572fecc4ea87745492", null ],
    [ "oC_ColorMap_IsCorrect", "d8/d36/group___color_map.html#ga5cd0731be5580dd177f308ce603ed6eb", null ],
    [ "oC_ColorMap_IsPositionCorrect", "d8/d36/group___color_map.html#gab68f3d753784f72c8315cd5e9006b8c7", null ],
    [ "oC_ColorMap_New", "d8/d36/group___color_map.html#ga7dc2303eeb5e9c3da92386303c35ed76", null ],
    [ "oC_ColorMap_SetColor", "d8/d36/group___color_map.html#ga7ca80c7f734b22b7e7f1820fdc204170", null ],
    [ "oC_ColorMap_SwitchLayer", "d8/d36/group___color_map.html#gacde73af0d07d795ff5dc4a83839d4480", null ],
    [ "SetColor", "d6/de8/oc__colormap_8c.html#acc6a6bec58cd16639321f8de239feae3", null ],
    [ "SetColorWithOpacity", "d6/de8/oc__colormap_8c.html#a6a14fdd858afb828dc7e89f7c33e066f", null ]
];