var dir_ee552c4e8661d1efeabf3650f1494f40 =
[
    [ "clock", "dir_8621fe28d7e4baaf27ff7c898266b934.html", "dir_8621fe28d7e4baaf27ff7c898266b934" ],
    [ "dma", "dir_c63e7b41f3337a9f1550345bb073b3c6.html", "dir_c63e7b41f3337a9f1550345bb073b3c6" ],
    [ "eth", "dir_1b27fc31bb84c5fc23f9891fb90ecdae.html", "dir_1b27fc31bb84c5fc23f9891fb90ecdae" ],
    [ "fmc", "dir_7e0213259503b57e15dd38c38ccc2558.html", "dir_7e0213259503b57e15dd38c38ccc2558" ],
    [ "gpio", "dir_99ea39247fb514da32e6558337bafc55.html", "dir_99ea39247fb514da32e6558337bafc55" ],
    [ "i2c", "dir_a7a7e87547578fcc6909c99acc32ffe5.html", "dir_a7a7e87547578fcc6909c99acc32ffe5" ],
    [ "lcdtft", "dir_4d1008d9fcdb654083ddf4ef84731aeb.html", "dir_4d1008d9fcdb654083ddf4ef84731aeb" ],
    [ "mem", "dir_e35f63b485a5bd8801f5a5d0b7fc1935.html", "dir_e35f63b485a5bd8801f5a5d0b7fc1935" ],
    [ "sdmmc", "dir_4236a0807b2b1a5e4a9202fd4425bf6c.html", "dir_4236a0807b2b1a5e4a9202fd4425bf6c" ],
    [ "spi", "dir_b281737da85e0053fa1e7859bc89a683.html", "dir_b281737da85e0053fa1e7859bc89a683" ],
    [ "sys", "dir_cb8de8423bd70080caa24f8a0c28cc56.html", "dir_cb8de8423bd70080caa24f8a0c28cc56" ],
    [ "timer", "dir_d91376840396a90b051e5cc0ee0bfa1f.html", "dir_d91376840396a90b051e5cc0ee0bfa1f" ],
    [ "uart", "dir_79ec92c81fa395a1785d82200c7e9b08.html", "dir_79ec92c81fa395a1785d82200c7e9b08" ],
    [ "oc_saipll.c", "d2/dcd/oc__saipll_8c.html", "d2/dcd/oc__saipll_8c" ]
];