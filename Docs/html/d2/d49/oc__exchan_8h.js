var oc__exchan_8h =
[
    [ "oC_ExcHan_ExceptionType_t", "db/dad/group___exc_han.html#ga0e9ce6971eb74c864cd33f28a4890ac3", [
      [ "oC_ExcHan_ExceptionType_KernelPanic", "db/dad/group___exc_han.html#gga0e9ce6971eb74c864cd33f28a4890ac3ad4d400dc906f50a45f95796873ee97c2", null ],
      [ "oC_ExcHan_ExceptionType_HardFault", "db/dad/group___exc_han.html#gga0e9ce6971eb74c864cd33f28a4890ac3a39137fbfe20fa2760a6607fb03a562bf", null ],
      [ "oC_ExcHan_ExceptionType_MemoryAccess", "db/dad/group___exc_han.html#gga0e9ce6971eb74c864cd33f28a4890ac3a4ac3752103a112ad953d14d3591d1496", null ],
      [ "oC_ExcHan_ExceptionType_MemoryAllocationError", "db/dad/group___exc_han.html#gga0e9ce6971eb74c864cd33f28a4890ac3abceea112741d0e454e4f92d82e6d253c", null ],
      [ "oC_ExcHan_ExceptionType_ProcessDamaged", "db/dad/group___exc_han.html#gga0e9ce6971eb74c864cd33f28a4890ac3a6f3d441edcee8b7b8bdcca52ff85b632", null ],
      [ "oC_ExcHan_ExceptionType_RequiredReboot", "db/dad/group___exc_han.html#gga0e9ce6971eb74c864cd33f28a4890ac3aa9f1f8e2969b3393166a7177dfe3aae0", null ]
    ] ],
    [ "oC_ExcHan_LogEvent", "db/dad/group___exc_han.html#ga5c2e77223f7b8630868cca67dc302548", null ],
    [ "oC_ExcHan_PrintLoggedEvents", "db/dad/group___exc_han.html#gaa18a1156b007fcb9a61ae4e8f4f4f94b", null ],
    [ "oC_ExcHan_RunDaemon", "db/dad/group___exc_han.html#gab58ca31a85962d2c346e142dd4d42dcd", null ],
    [ "oC_ExcHan_TurnOff", "db/dad/group___exc_han.html#ga860f3a7517efd183013dc18530b82002", null ],
    [ "oC_ExcHan_TurnOn", "db/dad/group___exc_han.html#ga7029c50858acfa700b820345d615e5eb", null ]
];