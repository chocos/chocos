var oc__color_8h =
[
    [ "oC_Color_t", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5", [
      [ "oC_Color_White", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5ad5f3eb66b5f0d1670bd6c9d0b971e174", null ],
      [ "oC_Color_Black", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5a5bd2a8cb09672e08df965e31ac5187b9", null ],
      [ "oC_Color_Red", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5af27fb1a2e9a35edbb3e5336c32f12e46", null ],
      [ "oC_Color_Green", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5a1cebe94b45eb81ada6431e2e38abb40c", null ],
      [ "oC_Color_Blue", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5a66703aa208520a58e1e729954baab5cd", null ],
      [ "oC_Color_Yellow", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5ae65a5a54ddf2fba9dcffb5b5acc59f3e", null ],
      [ "oC_Color_Purple", "d2/d95/oc__color_8h.html#a55e0a81d7b5d5279185f7bf05ae0d1b5a7d30efb36211a93038c9365dfef5f45a", null ]
    ] ],
    [ "oC_ColorFormat_t", "d2/d95/oc__color_8h.html#a05cb692408bc5948a2fff3d3807db0a5", [
      [ "oC_ColorFormat_Unknown", "d2/d95/oc__color_8h.html#a05cb692408bc5948a2fff3d3807db0a5a4c85dc61f03c64d0b283eed8ddd6806f", null ],
      [ "oC_ColorFormat_ARGB8888", "d2/d95/oc__color_8h.html#a05cb692408bc5948a2fff3d3807db0a5afa3b3f65571d6249bb30cd4418140a10", null ],
      [ "oC_ColorFormat_RGB888", "d2/d95/oc__color_8h.html#a05cb692408bc5948a2fff3d3807db0a5a7a8a2a43b58c2739db2cd61c60ef8d37", null ],
      [ "oC_ColorFormat_RGB565", "d2/d95/oc__color_8h.html#a05cb692408bc5948a2fff3d3807db0a5a1b55f764b6df70a88407ba7f370c9408", null ]
    ] ]
];