var group___i_ctrl =
[
    [ "oC_ICtrl_t", "d2/d9e/group___i_ctrl.html#ga8fd6f518b35da5a06ff0e68c5f43b57c", null ],
    [ "oC_ICtrl_Rotate_t", "d2/d9e/group___i_ctrl.html#ga84f2e20234ed704bdf9d65961316dcb9", [
      [ "oC_ICtrl_Rotate_None", "d2/d9e/group___i_ctrl.html#gga84f2e20234ed704bdf9d65961316dcb9aaca67186aea3ea140f77cb2d28f8309f", null ],
      [ "oC_ICtrl_Rotate_90DegreesClockwise", "d2/d9e/group___i_ctrl.html#gga84f2e20234ed704bdf9d65961316dcb9ad5f3a0b6068175cfbf28f915c1a01ccd", null ],
      [ "oC_ICtrl_Rotate_180DegreesClockwise", "d2/d9e/group___i_ctrl.html#gga84f2e20234ed704bdf9d65961316dcb9af2de1f4a55486023a1f7face1f0ae127", null ],
      [ "oC_ICtrl_Rotate_270DegreesClockwise", "d2/d9e/group___i_ctrl.html#gga84f2e20234ed704bdf9d65961316dcb9a04eac5c449965a35b9c2e299ac047438", null ]
    ] ],
    [ "oC_ICtrl_Configure", "d2/d9e/group___i_ctrl.html#ga7b108a30cf66c7d625e376f278b13ad9", null ],
    [ "oC_ICtrl_Delete", "d2/d9e/group___i_ctrl.html#gaae940a4145431fd329be7739e62c0b96", null ],
    [ "oC_ICtrl_GetDriver", "d2/d9e/group___i_ctrl.html#ga18fbd3145fbb7fba7916960b870db372", null ],
    [ "oC_ICtrl_GetName", "d2/d9e/group___i_ctrl.html#ga2a73fc05c9fe5e58790dfc2e77e35a38", null ],
    [ "oC_ICtrl_GetScreen", "d2/d9e/group___i_ctrl.html#gafcf4a175e55bde932ae4c666044e1f91", null ],
    [ "oC_ICtrl_IsConfigured", "d2/d9e/group___i_ctrl.html#gab3313f0f8d36338a22d36f058bdf0f2f", null ],
    [ "oC_ICtrl_IsCorrect", "d2/d9e/group___i_ctrl.html#ga185a6e29074b4956c272afd6166cc1c2", null ],
    [ "oC_ICtrl_IsEventSupportedByDriver", "d2/d9e/group___i_ctrl.html#ga3dcb991acdb6d17707402ce6bb00c6a1", null ],
    [ "oC_ICtrl_New", "d2/d9e/group___i_ctrl.html#gab855d97ae17c2957be46786367fe186b", null ],
    [ "oC_ICtrl_RecordGesture", "d2/d9e/group___i_ctrl.html#gaa49f9a2d336ff7f854c12ee0edc82631", null ],
    [ "oC_ICtrl_SetScreen", "d2/d9e/group___i_ctrl.html#gaea6ee621ae038c749db91dfdfe14490a", null ],
    [ "oC_ICtrl_Unconfigure", "d2/d9e/group___i_ctrl.html#ga41b0885b2ffad8ab0276da4112cc406b", null ],
    [ "oC_ICtrl_WaitForEvent", "d2/d9e/group___i_ctrl.html#ga28f1b3fa3999dac8cb053cc657d33856", null ]
];