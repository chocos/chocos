var group___l_c_d_t_f_t =
[
    [ "oC_LCDTFT_Configure", "d2/dea/group___l_c_d_t_f_t.html#gacac3787891de605e00495978037294ff", null ],
    [ "oC_LCDTFT_Ioctl", "d2/dea/group___l_c_d_t_f_t.html#gaaa1409c5f563374b41b1646c68b5e4ee", null ],
    [ "oC_LCDTFT_IsTurnedOn", "d2/dea/group___l_c_d_t_f_t.html#ga5ef3b309ff65c1a31767813568cd413c", null ],
    [ "oC_LCDTFT_TurnOff", "d2/dea/group___l_c_d_t_f_t.html#ga0653553a250b412ae56f9df9bda6018b", null ],
    [ "oC_LCDTFT_TurnOn", "d2/dea/group___l_c_d_t_f_t.html#ga2c358bd78138abff3b1461830bc01c8a", null ],
    [ "oC_LCDTFT_Unconfigure", "d2/dea/group___l_c_d_t_f_t.html#gab79686c48981059d2a62f87cf41bb92d", null ]
];