var oc__gpio__lld_8h =
[
    [ "oC_GPIO_LLD_ForEachPort", "df/dcc/group___g_p_i_o-_l_l_d.html#ga992d217502beb06f2f777713bf3107f7", null ],
    [ "oC_GPIO_LLD_GetImportantBitsMaskForPins", "df/dcc/group___g_p_i_o-_l_l_d.html#gaf385c084ffe1c9f075003520211d8279", null ],
    [ "oC_GPIO_LLD_Interrupt_t", "df/dcc/group___g_p_i_o-_l_l_d.html#ga62898269b08174bf3d06bbaffb82e599", null ],
    [ "oC_GPIO_LLD_Current_t", "df/dcc/group___g_p_i_o-_l_l_d.html#gabc3ad30723b0b745b5bc36a5d3a939b3", [
      [ "oC_GPIO_LLD_Current_Default", "df/dcc/group___g_p_i_o-_l_l_d.html#ggabc3ad30723b0b745b5bc36a5d3a939b3a0172d03cdf8ce67f28b96dc65393d3ae", null ],
      [ "oC_GPIO_LLD_Current_Minimum", "df/dcc/group___g_p_i_o-_l_l_d.html#ggabc3ad30723b0b745b5bc36a5d3a939b3af73d0d184d0f208be7cde90b35efbb8b", null ],
      [ "oC_GPIO_LLD_Current_Medium", "df/dcc/group___g_p_i_o-_l_l_d.html#ggabc3ad30723b0b745b5bc36a5d3a939b3a3c72d9e67b90580d8172719c639c3092", null ],
      [ "oC_GPIO_LLD_Current_Maximum", "df/dcc/group___g_p_i_o-_l_l_d.html#ggabc3ad30723b0b745b5bc36a5d3a939b3a914f73f03ec74da9dc76fb6bd5905046", null ]
    ] ],
    [ "oC_GPIO_LLD_IntTrigger_t", "df/dcc/group___g_p_i_o-_l_l_d.html#ga0bcdb8026ab1d983beb9dc23263889df", [
      [ "oC_GPIO_LLD_IntTrigger_Default", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfa9d2d9ee93c0a0823ce2db63fca93cfed", null ],
      [ "oC_GPIO_LLD_IntTrigger_Off", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfa0d24ccd50fd438e8322f87d382e33436", null ],
      [ "oC_GPIO_LLD_IntTrigger_RisingEdge", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfa2c9f0313c9146a3a15687166c02131bd", null ],
      [ "oC_GPIO_LLD_IntTrigger_FallingEdge", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfab728ea0b006c9e6b9fd55a0016bad298", null ],
      [ "oC_GPIO_LLD_IntTrigger_BothEdges", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfacfda65caf1442e9a8b6328524ceebf89", null ],
      [ "oC_GPIO_LLD_IntTrigger_HighLevel", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfaf9afce4baa8c6066ac017ba0c1dbc744", null ],
      [ "oC_GPIO_LLD_IntTrigger_LowLevel", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfaaa9add372fa38e29a04262bdd9430803", null ],
      [ "oC_GPIO_LLD_IntTrigger_BothLevels", "df/dcc/group___g_p_i_o-_l_l_d.html#gga0bcdb8026ab1d983beb9dc23263889dfa630ec8fa712357414f294a42c26f7db0", null ]
    ] ],
    [ "oC_GPIO_LLD_Mode_t", "df/dcc/group___g_p_i_o-_l_l_d.html#ga26cce05d5614e600554542b1d328c37f", [
      [ "oC_GPIO_LLD_Mode_Default", "df/dcc/group___g_p_i_o-_l_l_d.html#gga26cce05d5614e600554542b1d328c37fa6f30ed6a166eb1bc8b049193e67305e0", null ],
      [ "oC_GPIO_LLD_Mode_Input", "df/dcc/group___g_p_i_o-_l_l_d.html#gga26cce05d5614e600554542b1d328c37fa7d903a9c89f28f81b07f60b10e439365", null ],
      [ "oC_GPIO_LLD_Mode_Output", "df/dcc/group___g_p_i_o-_l_l_d.html#gga26cce05d5614e600554542b1d328c37fa87c74c2f6adfdcaf26d0e5eae5bc2f59", null ],
      [ "oC_GPIO_LLD_Mode_Alternate", "df/dcc/group___g_p_i_o-_l_l_d.html#gga26cce05d5614e600554542b1d328c37faa64e34ecde4b2b86f48f4a6d95131444", null ]
    ] ],
    [ "oC_GPIO_LLD_OutputCircuit_t", "df/dcc/group___g_p_i_o-_l_l_d.html#ga5b8169a039c01781a16eaf60fed7cc88", [
      [ "oC_GPIO_LLD_OutputCircuit_OpenDrain", "df/dcc/group___g_p_i_o-_l_l_d.html#gga5b8169a039c01781a16eaf60fed7cc88acfd7a8526bf37da33783602021144382", null ],
      [ "oC_GPIO_LLD_OutputCircuit_PushPull", "df/dcc/group___g_p_i_o-_l_l_d.html#gga5b8169a039c01781a16eaf60fed7cc88a6a9c83d60bdfbb9ae51ba85eae8a02bb", null ]
    ] ],
    [ "oC_GPIO_LLD_PinsState_t", "df/dcc/group___g_p_i_o-_l_l_d.html#ga7babaff8fd02b89aa145832991b910cf", [
      [ "oC_GPIO_LLD_PinsState_AllLow", "df/dcc/group___g_p_i_o-_l_l_d.html#gga7babaff8fd02b89aa145832991b910cfa14afad305b453119a6ebe3d2a7123e7f", null ],
      [ "oC_GPIO_LLD_PinsState_AllHigh", "df/dcc/group___g_p_i_o-_l_l_d.html#gga7babaff8fd02b89aa145832991b910cfa9a361b2af73dc03b8ea5ce79f59ecb89", null ]
    ] ],
    [ "oC_GPIO_LLD_Protection_t", "df/dcc/group___g_p_i_o-_l_l_d.html#ga3ac41b4f43bac3d6017189c2905bfc13", [
      [ "oC_GPIO_LLD_Protection_DontUnlockProtectedPins", "df/dcc/group___g_p_i_o-_l_l_d.html#gga3ac41b4f43bac3d6017189c2905bfc13ab56f83452d24d376a78d2c99806b3e87", null ],
      [ "oC_GPIO_LLD_Protection_UnlockProtectedPins", "df/dcc/group___g_p_i_o-_l_l_d.html#gga3ac41b4f43bac3d6017189c2905bfc13a5f3d811d8148afeadb687acbd7fe0432", null ]
    ] ],
    [ "oC_GPIO_LLD_Pull_t", "df/dcc/group___g_p_i_o-_l_l_d.html#gadc2b74cf89239842838d5c80a72c9974", [
      [ "oC_GPIO_LLD_Pull_Default", "df/dcc/group___g_p_i_o-_l_l_d.html#ggadc2b74cf89239842838d5c80a72c9974ab5c266cd3100ad9752a52170369c8e3f", null ],
      [ "oC_GPIO_LLD_Pull_Up", "df/dcc/group___g_p_i_o-_l_l_d.html#ggadc2b74cf89239842838d5c80a72c9974af9c8b71c9695335719031b98dda72124", null ],
      [ "oC_GPIO_LLD_Pull_Down", "df/dcc/group___g_p_i_o-_l_l_d.html#ggadc2b74cf89239842838d5c80a72c9974a409f4a685af98eb1888c3ce67152ae16", null ]
    ] ],
    [ "oC_GPIO_LLD_Speed_t", "df/dcc/group___g_p_i_o-_l_l_d.html#gad5ac50bd8bb91ed089ab642625f174ce", [
      [ "oC_GPIO_LLD_Speed_Default", "df/dcc/group___g_p_i_o-_l_l_d.html#ggad5ac50bd8bb91ed089ab642625f174cea49a778554b18f359a9929c98b77c05c7", null ],
      [ "oC_GPIO_LLD_Speed_Minimum", "df/dcc/group___g_p_i_o-_l_l_d.html#ggad5ac50bd8bb91ed089ab642625f174cead84be00e42f55cf56732310066f26d22", null ],
      [ "oC_GPIO_LLD_Speed_Medium", "df/dcc/group___g_p_i_o-_l_l_d.html#ggad5ac50bd8bb91ed089ab642625f174cea107acadc3f08146bbb3c4320eed6e0fc", null ],
      [ "oC_GPIO_LLD_Speed_Maximum", "df/dcc/group___g_p_i_o-_l_l_d.html#ggad5ac50bd8bb91ed089ab642625f174cea9c6433350ed311feeff2215d1012ab29", null ],
      [ "oC_GPIO_LLD_Speed_NumberOfElements", "df/dcc/group___g_p_i_o-_l_l_d.html#ggad5ac50bd8bb91ed089ab642625f174ceabe30d57651da91e57254ad1171942415", null ]
    ] ],
    [ "oC_GPIO_LLD_ArePinsCorrect", "df/dcc/group___g_p_i_o-_l_l_d.html#ga461b5fe89c0fc7bbbb4f6cdda41cc091", null ],
    [ "oC_GPIO_LLD_ArePinsDefined", "df/dcc/group___g_p_i_o-_l_l_d.html#ga0bf550be4970b31a8995af8e4c4d6495", null ],
    [ "oC_GPIO_LLD_ArePinsUnused", "df/dcc/group___g_p_i_o-_l_l_d.html#gaa0aad575cc484d0979fc79f30dae5acc", null ],
    [ "oC_GPIO_LLD_BeginConfiguration", "df/dcc/group___g_p_i_o-_l_l_d.html#ga7fbc8eebb8c3db7ec09d9a4739be5fa1", null ],
    [ "oC_GPIO_LLD_CheckIsPinUnlocked", "df/dcc/group___g_p_i_o-_l_l_d.html#gab26651bf1e99d74c53224220e1d3a174", null ],
    [ "oC_GPIO_LLD_CheckIsPinUsed", "df/dcc/group___g_p_i_o-_l_l_d.html#gab5538f2052f257328c68430e2f4da889", null ],
    [ "oC_GPIO_LLD_FinishConfiguration", "df/dcc/group___g_p_i_o-_l_l_d.html#gae057bd9a5b786425e31683bcee3d3568", null ],
    [ "oC_GPIO_LLD_GetHighStatePins", "df/dcc/group___g_p_i_o-_l_l_d.html#ga07f741bd3826a1311e755dfa24151177", null ],
    [ "oC_GPIO_LLD_GetLowStatePins", "df/dcc/group___g_p_i_o-_l_l_d.html#ga4bd673d6a68c7f71e1f3da8f2044436d", null ],
    [ "oC_GPIO_LLD_GetPinName", "df/dcc/group___g_p_i_o-_l_l_d.html#ga083764e9e9165f69a8f546c33a082d05", null ],
    [ "oC_GPIO_LLD_GetPinsFor", "df/dcc/group___g_p_i_o-_l_l_d.html#ga4beb841744dff386b380e3a2f51eb0d9", null ],
    [ "oC_GPIO_LLD_GetPinsMaskOfPins", "df/dcc/group___g_p_i_o-_l_l_d.html#gabf7f27b79e4d0547e3b8ba7bfaa191e7", null ],
    [ "oC_GPIO_LLD_GetPinsState", "df/dcc/group___g_p_i_o-_l_l_d.html#gad217ab0430790dfb5e96a8164f7260a1", null ],
    [ "oC_GPIO_LLD_GetPortName", "df/dcc/group___g_p_i_o-_l_l_d.html#gae33aca00f62b0886df0d7dd0db4878ed", null ],
    [ "oC_GPIO_LLD_GetPortOfPins", "df/dcc/group___g_p_i_o-_l_l_d.html#ga8fe7124f4d91b9c7034f156330ec3a34", null ],
    [ "oC_GPIO_LLD_IsPinDefined", "df/dcc/group___g_p_i_o-_l_l_d.html#ga9f58b21f62d7dbe8e8cc69e6e6c40199", null ],
    [ "oC_GPIO_LLD_IsPinProtected", "df/dcc/group___g_p_i_o-_l_l_d.html#gaa092c15aa5434652b37c298ba70c40e9", null ],
    [ "oC_GPIO_LLD_IsPinsState", "df/dcc/group___g_p_i_o-_l_l_d.html#ga06069abde7ab1c9e22eeedcc01c42990", null ],
    [ "oC_GPIO_LLD_IsPortCorrect", "df/dcc/group___g_p_i_o-_l_l_d.html#ga1dff4e36cd74d394ea06413f6b2654c8", null ],
    [ "oC_GPIO_LLD_IsPortIndexCorrect", "df/dcc/group___g_p_i_o-_l_l_d.html#gaf26d9deff1317c8e3329b5983806ad06", null ],
    [ "oC_GPIO_LLD_IsSinglePin", "df/dcc/group___g_p_i_o-_l_l_d.html#ga7a42c30526887516f20f6bb03a8547a7", null ],
    [ "oC_GPIO_LLD_LockProtection", "df/dcc/group___g_p_i_o-_l_l_d.html#gaf7926ca41fcdcf1e3375cfe69ba5a1c9", null ],
    [ "oC_GPIO_LLD_PortIndexToPort", "df/dcc/group___g_p_i_o-_l_l_d.html#gace8e6045d5377351b082e3afacfbf696", null ],
    [ "oC_GPIO_LLD_PortToPortIndex", "df/dcc/group___g_p_i_o-_l_l_d.html#gabdf2084703b4bdd29fe16e846b5f314c", null ],
    [ "oC_GPIO_LLD_ReadCurrent", "df/dcc/group___g_p_i_o-_l_l_d.html#ga24053c19a6f45510a8f37ee8b8801cdd", null ],
    [ "oC_GPIO_LLD_ReadData", "df/dcc/group___g_p_i_o-_l_l_d.html#gafebbf1b0bacadf655fd228b20bc54196", null ],
    [ "oC_GPIO_LLD_ReadDataReference", "df/dcc/group___g_p_i_o-_l_l_d.html#ga284eda58ec7b368f8b1eb292bbe84d3b", null ],
    [ "oC_GPIO_LLD_ReadInterruptTrigger", "df/dcc/group___g_p_i_o-_l_l_d.html#ga053ba4071fe19ff9eac782e42a31a859", null ],
    [ "oC_GPIO_LLD_ReadMode", "df/dcc/group___g_p_i_o-_l_l_d.html#ga4920ab0f7538d4b32988d386938ab19e", null ],
    [ "oC_GPIO_LLD_ReadOutputCircuit", "df/dcc/group___g_p_i_o-_l_l_d.html#gac2e11fb55a8a700105ba44afbb3402a7", null ],
    [ "oC_GPIO_LLD_ReadPower", "df/dcc/group___g_p_i_o-_l_l_d.html#ga6c094d5f6b3782dd47964d9cee4a6a95", null ],
    [ "oC_GPIO_LLD_ReadPull", "df/dcc/group___g_p_i_o-_l_l_d.html#ga06366e5cfeaacc4ff1feecdee46c7065", null ],
    [ "oC_GPIO_LLD_ReadSpeed", "df/dcc/group___g_p_i_o-_l_l_d.html#ga0174a019423038dd1308f80c49595a48", null ],
    [ "oC_GPIO_LLD_SetCurrent", "df/dcc/group___g_p_i_o-_l_l_d.html#gabc0c068da82d330600fa5c6cfbc0a3b7", null ],
    [ "oC_GPIO_LLD_SetDriverInterruptHandler", "df/dcc/group___g_p_i_o-_l_l_d.html#ga669bb33ded1eb97d7860e6c86a252c69", null ],
    [ "oC_GPIO_LLD_SetInterruptTrigger", "df/dcc/group___g_p_i_o-_l_l_d.html#ga03f804d6ba01628c6b1eb32000d7120e", null ],
    [ "oC_GPIO_LLD_SetMode", "df/dcc/group___g_p_i_o-_l_l_d.html#ga04285479e26488d3c54f9486a99a174f", null ],
    [ "oC_GPIO_LLD_SetOutputCircuit", "df/dcc/group___g_p_i_o-_l_l_d.html#ga963efb9ebfe5738e937f61202364f608", null ],
    [ "oC_GPIO_LLD_SetPinsState", "df/dcc/group___g_p_i_o-_l_l_d.html#ga6b2f40bc2282ed2bb4c5a1560861ad68", null ],
    [ "oC_GPIO_LLD_SetPinsUnused", "df/dcc/group___g_p_i_o-_l_l_d.html#ga75c01bca7634b539407df7d3b7d601ec", null ],
    [ "oC_GPIO_LLD_SetPinsUsed", "df/dcc/group___g_p_i_o-_l_l_d.html#ga6054d35969bb5743e4d0dcee530125ff", null ],
    [ "oC_GPIO_LLD_SetPower", "df/dcc/group___g_p_i_o-_l_l_d.html#ga832ab3187c0706025d7caf2f775cb054", null ],
    [ "oC_GPIO_LLD_SetPull", "df/dcc/group___g_p_i_o-_l_l_d.html#gaabc366e695d00197a5db999e08a30c3e", null ],
    [ "oC_GPIO_LLD_SetSpeed", "df/dcc/group___g_p_i_o-_l_l_d.html#ga5e215183f5d448abeeb1c15b54d4f3ab", null ],
    [ "oC_GPIO_LLD_TogglePinsState", "df/dcc/group___g_p_i_o-_l_l_d.html#ga455cabcbe28ec1ea50d24180ac8cdcfc", null ],
    [ "oC_GPIO_LLD_TurnOffDriver", "df/dcc/group___g_p_i_o-_l_l_d.html#ga5b709baa403f414bbd77756551761ae4", null ],
    [ "oC_GPIO_LLD_TurnOnDriver", "df/dcc/group___g_p_i_o-_l_l_d.html#gae73979d19d875c7ff3b0486564087068", null ],
    [ "oC_GPIO_LLD_UnlockProtection", "df/dcc/group___g_p_i_o-_l_l_d.html#ga640bebb53444a66ba2ba415b90e3b3fe", null ],
    [ "oC_GPIO_LLD_WriteData", "df/dcc/group___g_p_i_o-_l_l_d.html#ga0cfc992750e90c3938fbb91354e7bf96", null ]
];