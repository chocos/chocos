var structo_c___data_package__t =
[
    [ "BufferSize", "d8/de7/structo_c___data_package__t.html#aee251167c1492c47839f88f1b05913ff", null ],
    [ "DataLength", "d8/de7/structo_c___data_package__t.html#a857741f7bca31e92a65a54e1a7d899cc", null ],
    [ "GetOffset", "d8/de7/structo_c___data_package__t.html#abcaa0d2b521f0ae4b480748bff77928c", null ],
    [ "MagicNumber", "d8/de7/structo_c___data_package__t.html#ac2d9a2b3478db6dc43ad665d6a6eda8c", null ],
    [ "PutOffset", "d8/de7/structo_c___data_package__t.html#af4ce2c95eb4ea1dcb8d32b1b0661889a", null ],
    [ "ReadArrayU16", "d8/de7/structo_c___data_package__t.html#a487dacaed5aedb542907f6828eb79cf7", null ],
    [ "ReadArrayU32", "d8/de7/structo_c___data_package__t.html#a13279a7776a908754371d9c7c9dbfbc2", null ],
    [ "ReadArrayU64", "d8/de7/structo_c___data_package__t.html#acbd54751da081bf0adc407f6d0fa6ecc", null ],
    [ "ReadArrayU8", "d8/de7/structo_c___data_package__t.html#ad8afb28612ccbf86d616d7910c1afd17", null ],
    [ "ReadBuffer", "d8/de7/structo_c___data_package__t.html#a39d8ed5721924b79e2db5cc0a9ac96ae", null ],
    [ "ReadOnly", "d8/de7/structo_c___data_package__t.html#ab9cb362b9ea7845d80cd473e0bdab74e", null ],
    [ "WriteArrayU16", "d8/de7/structo_c___data_package__t.html#a126169390378518a86fea70a2f87553d", null ],
    [ "WriteArrayU32", "d8/de7/structo_c___data_package__t.html#a5a2fa4d52f57785d9aa244b94a2db124", null ],
    [ "WriteArrayU64", "d8/de7/structo_c___data_package__t.html#a10c80553f958fcc1305a075e9e5f6ae4", null ],
    [ "WriteArrayU8", "d8/de7/structo_c___data_package__t.html#a7e26dd9b31c59721a032df8685096293", null ],
    [ "WriteBuffer", "d8/de7/structo_c___data_package__t.html#aad9c85409794def75990f1d02f2edc36", null ]
];