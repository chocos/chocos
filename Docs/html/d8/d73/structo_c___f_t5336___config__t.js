var structo_c___f_t5336___config__t =
[
    [ "AutoConfigurationName", "d8/d73/structo_c___f_t5336___config__t.html#a350b138932a750a47627a84cafcde833", null ],
    [ "Context", "d8/d73/structo_c___f_t5336___config__t.html#a2146c87e54462a0538fdfbbc592b3fda", null ],
    [ "I2C", "d8/d73/structo_c___f_t5336___config__t.html#afc79286691ddc8b36f772bed0f28f302", null ],
    [ "InterruptPin", "d8/d73/structo_c___f_t5336___config__t.html#a41bfb93db6bd7d7101e072670adb3700", null ],
    [ "Mode", "d8/d73/structo_c___f_t5336___config__t.html#a1d7073a450b339651ad5d56c325861f9", null ],
    [ "PollingTime", "d8/d73/structo_c___f_t5336___config__t.html#a0c860d7c4f1451d0ab4cf3c85601808e", null ],
    [ "SCL", "d8/d73/structo_c___f_t5336___config__t.html#a6aaef3a98bf4b16485ab7c8721cb13bd", null ],
    [ "SDA", "d8/d73/structo_c___f_t5336___config__t.html#a2bb0bd1395f78b7a0c636804edbd5776", null ]
];