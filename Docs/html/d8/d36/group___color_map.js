var group___color_map =
[
    [ "oC_ColorMap_t", "d1/da4/structo_c___color_map__t.html", [
      [ "ActiveLayer", "d1/da4/structo_c___color_map__t.html#affa001d126798ef8ec827c4ad831706b", null ],
      [ "ColorFormat", "d1/da4/structo_c___color_map__t.html#adb61686ba228c58e43d161e2348b7f44", null ],
      [ "FormatSize", "d1/da4/structo_c___color_map__t.html#aad29ca23ae97f6cd141ef92efffb0fb8", null ],
      [ "GenericWriteMap", "d1/da4/structo_c___color_map__t.html#a5c4f0c04939357f2c5a1adba1f6a4378", null ],
      [ "Height", "d1/da4/structo_c___color_map__t.html#a184d49528cffcfc8ec211807d1870537", null ],
      [ "Layers", "d1/da4/structo_c___color_map__t.html#a5eaed25bca927c8de5bb0fa985d9d7c8", null ],
      [ "MagicNumber", "d1/da4/structo_c___color_map__t.html#ac2d9a2b3478db6dc43ad665d6a6eda8c", null ],
      [ "Map", "d1/da4/structo_c___color_map__t.html#a904050ddb989fcb44cb632f2836f043d", null ],
      [ "NumberOfLayers", "d1/da4/structo_c___color_map__t.html#abad6a18c1d9b0ea876171aee8739c2a8", null ],
      [ "ReadMap", "d1/da4/structo_c___color_map__t.html#a06cac7deb550ce01f614766d7bd61dd5", null ],
      [ "Width", "d1/da4/structo_c___color_map__t.html#a4ee55a170448d60b1f6fe43fca496673", null ],
      [ "WriteMap", "d1/da4/structo_c___color_map__t.html#a87e0bff60808b211b10cbdd9fc448c03", null ]
    ] ],
    [ "oC_ColorMap_BorderStyle_Corner", "d8/d36/group___color_map.html#gaa2520ec076c41a6a3feb4cde01bb8619", null ],
    [ "oC_ColorMap_BorderStyle_Line", "d8/d36/group___color_map.html#ga5da4636c2c7023c9d7e44abb16212d6c", null ],
    [ "oC_ColorMap_Define", "d8/d36/group___color_map.html#gadf7b6310eab7170412f8de8cbf25d0c0", null ],
    [ "oC_ColorMap_LayerIndex_t", "d8/d36/group___color_map.html#gaa3e5a528f1511e68b434a12cf23e4b27", null ],
    [ "oC_ColorMap_BorderStyle_t", "d8/d36/group___color_map.html#ga87ea436f2d25d921c647ba3187c35c29", [
      [ "oC_ColorMap_BorderStyle_None", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29a2d4337d154cd80e269290d7a305c5d65", null ],
      [ "oC_ColorMap_BorderStyle_CornerMask", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29a9318fe7a2547805d110934ad2b350ef8", null ],
      [ "oC_ColorMap_BorderStyle_Rectangle", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29ad0d24013efb679539d19f6c658677d8e", null ],
      [ "oC_ColorMap_BorderStyle_Rounded", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29a2dd85a00d0626ffd9c7a5c39b4429ad2", null ],
      [ "oC_ColorMap_BorderStyle_LineMask", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29add4bc3c772399add5b0bdd3271aba864", null ],
      [ "oC_ColorMap_BorderStyle_Solid", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29af5d4d52d92e15f12940637edf112809e", null ],
      [ "oC_ColorMap_BorderStyle_Dashed", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29a951c3223f8a2be94e7ba5a44bd63bea2", null ],
      [ "oC_ColorMap_BorderStyle_Dotted", "d8/d36/group___color_map.html#gga87ea436f2d25d921c647ba3187c35c29a11bb937943d2c9cdf9e953663ee7dfd7", null ]
    ] ],
    [ "oC_ColorMap_Direction_t", "d8/d36/group___color_map.html#ga16daf528bdbde644cdd2464c2b4f658d", [
      [ "oC_ColorMap_Direction_Up", "d8/d36/group___color_map.html#gga16daf528bdbde644cdd2464c2b4f658da5c07f2c5827da1891b995bc38517dd43", null ],
      [ "oC_ColorMap_Direction_Down", "d8/d36/group___color_map.html#gga16daf528bdbde644cdd2464c2b4f658dac4d84f9d9f83acb939301625dfb16235", null ],
      [ "oC_ColorMap_Direction_Left", "d8/d36/group___color_map.html#gga16daf528bdbde644cdd2464c2b4f658dafa789876e6b22618b4e653d15ea4bd91", null ],
      [ "oC_ColorMap_Direction_Right", "d8/d36/group___color_map.html#gga16daf528bdbde644cdd2464c2b4f658dabc73956c46e6cc6e66887e92f4f29203", null ],
      [ "oC_ColorMap_Direction_NumberOfElements", "d8/d36/group___color_map.html#gga16daf528bdbde644cdd2464c2b4f658da7c53c516151b4cb0102f1d2da545bf97", null ]
    ] ],
    [ "oC_ColorMap_TextAlign_t", "d8/d36/group___color_map.html#ga951894b293bc7375e5587686e0c0b871", [
      [ "oC_ColorMap_TextAlign_Left", "d8/d36/group___color_map.html#gga951894b293bc7375e5587686e0c0b871a97b98b200d56f90264adbb4f79b3239e", null ],
      [ "oC_ColorMap_TextAlign_Right", "d8/d36/group___color_map.html#gga951894b293bc7375e5587686e0c0b871af7e1feffa7d8bdaaba20a2a804570041", null ],
      [ "oC_ColorMap_TextAlign_Center", "d8/d36/group___color_map.html#gga951894b293bc7375e5587686e0c0b871aca63ef90722e4d8859c66079db8fbdb2", null ]
    ] ],
    [ "oC_ColorMap_VerticalTextAlign_t", "d8/d36/group___color_map.html#gae759255f92146f8105b37fa2bdcfec67", [
      [ "oC_ColorMap_VerticalTextAlign_Up", "d8/d36/group___color_map.html#ggae759255f92146f8105b37fa2bdcfec67a4e37defcf49ea068890902bf0e3f690d", null ],
      [ "oC_ColorMap_VerticalTextAlign_Down", "d8/d36/group___color_map.html#ggae759255f92146f8105b37fa2bdcfec67ad8a8af16fd9c3ae6fe9798058e8a0ee5", null ],
      [ "oC_ColorMap_VerticalTextAlign_Center", "d8/d36/group___color_map.html#ggae759255f92146f8105b37fa2bdcfec67aac8e62119b1595e715562c53fb0c2bb0", null ]
    ] ],
    [ "oC_ColorMap_CloneMap", "d8/d36/group___color_map.html#ga98f809a287d0835f95a83d255c4fda85", null ],
    [ "oC_ColorMap_Delete", "d8/d36/group___color_map.html#ga94bdcdf506fc65df45a62ac5e2da2c03", null ],
    [ "oC_ColorMap_DrawAlignedString", "d8/d36/group___color_map.html#ga74f26880d659088987cf217357b49805", null ],
    [ "oC_ColorMap_DrawArrow", "d8/d36/group___color_map.html#gabeb21109fbe5df2cd19dbe2cae48b5e0", null ],
    [ "oC_ColorMap_DrawBackground", "d8/d36/group___color_map.html#ga5f91e1a8230ed55fb8883425b84e5d30", null ],
    [ "oC_ColorMap_DrawChar", "d8/d36/group___color_map.html#ga9c3fc47239626d2afd2efff17a9275a3", null ],
    [ "oC_ColorMap_DrawCircle", "d8/d36/group___color_map.html#ga2ab03b4255617173384ccefcd32b532a", null ],
    [ "oC_ColorMap_DrawImage", "d8/d36/group___color_map.html#gaefdef9d7885065b0eb9186a773a28dcd", null ],
    [ "oC_ColorMap_DrawLine", "d8/d36/group___color_map.html#ga5219d4039e44c9000fdcbea34720935f", null ],
    [ "oC_ColorMap_DrawRect", "d8/d36/group___color_map.html#gac677924e19ca6160f67ac977125fcc39", null ],
    [ "oC_ColorMap_DrawString", "d8/d36/group___color_map.html#ga67c935edf19cd71dd89f3c61b0a2477b", null ],
    [ "oC_ColorMap_DrawTriangle", "d8/d36/group___color_map.html#ga2d56ea66a9e009a9bd58f43fdc5d1637", null ],
    [ "oC_ColorMap_FillCircleWithColor", "d8/d36/group___color_map.html#gac31dfab1d141c3e5e0bedb0f149f5d25", null ],
    [ "oC_ColorMap_FillRectWithColor", "d8/d36/group___color_map.html#gab69b6b1b792ecf68d0b75eccbdd4604a", null ],
    [ "oC_ColorMap_FitPosition", "d8/d36/group___color_map.html#ga27f5ef5248b7a98ec7dbd7aeeb32f7c4", null ],
    [ "oC_ColorMap_GetActiveLayer", "d8/d36/group___color_map.html#ga327b3f811e9e2500092cd80c062c351b", null ],
    [ "oC_ColorMap_GetColor", "d8/d36/group___color_map.html#ga7cdb44fe9304a15a18e5cbc4a47d5c81", null ],
    [ "oC_ColorMap_GetInactiveLayer", "d8/d36/group___color_map.html#ga21143ba2e7ff330f674d5fdb48618aec", null ],
    [ "oC_ColorMap_GetNumberOfLayers", "d8/d36/group___color_map.html#ga3e7188f9245eea572fecc4ea87745492", null ],
    [ "oC_ColorMap_IsCorrect", "d8/d36/group___color_map.html#ga5cd0731be5580dd177f308ce603ed6eb", null ],
    [ "oC_ColorMap_IsPositionCorrect", "d8/d36/group___color_map.html#gab68f3d753784f72c8315cd5e9006b8c7", null ],
    [ "oC_ColorMap_New", "d8/d36/group___color_map.html#ga7dc2303eeb5e9c3da92386303c35ed76", null ],
    [ "oC_ColorMap_SetColor", "d8/d36/group___color_map.html#ga7ca80c7f734b22b7e7f1820fdc204170", null ],
    [ "oC_ColorMap_SwitchLayer", "d8/d36/group___color_map.html#gacde73af0d07d795ff5dc4a83839d4480", null ]
];