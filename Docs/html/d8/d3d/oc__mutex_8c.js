var oc__mutex_8c =
[
    [ "Mutex_t", "da/dda/struct_mutex__t.html", null ],
    [ "oC_Mutex_Delete", "d8/d7f/group___mutex.html#gad56629d375e44b9208c289951e77a18f", null ],
    [ "oC_Mutex_GetBlockedName", "d8/d7f/group___mutex.html#gaae2399e755a1f05255a2e7ac985f534f", null ],
    [ "oC_Mutex_Give", "d8/d7f/group___mutex.html#gad0a8c644b940124af2b2354615db06e8", null ],
    [ "oC_Mutex_IsCorrect", "d8/d7f/group___mutex.html#gadaab5d58e4a762e952d510fdb806e0de", null ],
    [ "oC_Mutex_IsTakenByCurrentThread", "d8/d7f/group___mutex.html#gaa6101979ecb420a2a59fbcdcc62e4496", null ],
    [ "oC_Mutex_New", "d8/d7f/group___mutex.html#ga8853de4a76d55e57b749b0ef4127ce6d", null ],
    [ "oC_Mutex_Take", "d8/d7f/group___mutex.html#ga7d03988687537c5b7354f9a9aa9df3df", null ],
    [ "RevertFunction", "d8/d3d/oc__mutex_8c.html#a443fd39bee83ba816ba3804c86f82fbb", null ]
];