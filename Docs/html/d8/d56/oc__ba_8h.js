var oc__ba_8h =
[
    [ "oC_BaseAddress_", "d4/d13/group___b_a.html#gacc9d3b84b69ea9cf965a9508b241d8d7", null ],
    [ "oC_BaseAddressMask", "d4/d13/group___b_a.html#ga8212a35a23393a757830745156e896c3", null ],
    [ "oC_PowerBaseAddress_", "d4/d13/group___b_a.html#ga24da3afc8a8041a91f085f61cf0435da", null ],
    [ "oC_PowerBaseAddress_Exist", "d4/d13/group___b_a.html#ga4b5b7c3671c26980d8b59a382e05e994", null ],
    [ "oC_PowerBaseAddress_ExistFor", "d4/d13/group___b_a.html#ga7ba531af72d9b41da043f128835c2629", null ],
    [ "oC_PowerBit_", "d4/d13/group___b_a.html#ga2e08bddee8d85988e7a5d7e738b2a280", null ],
    [ "oC_PowerBit_Exist", "d4/d13/group___b_a.html#ga4d8af9d4752480393e71104b9ded676a", null ],
    [ "oC_PowerBit_ExistFor", "d4/d13/group___b_a.html#ga2ca85f517923bb413a1ea9b40ae8d6b8", null ],
    [ "oC_PowerOffset_", "d4/d13/group___b_a.html#ga795eeb86a220c997fca9e6cd248a7307", null ],
    [ "oC_PowerOffset_Exist", "d4/d13/group___b_a.html#ga49eafe0daa1d1a667501af57ad7c5da8", null ],
    [ "oC_PowerOffset_ExistFor", "d4/d13/group___b_a.html#gaa569cb35704ee003441659d00895fd70", null ],
    [ "oC_BaseAddress_t", "d4/d13/group___b_a.html#gaf28d67b92d721cb7c97ead5896c56838", [
      [ "oC_BaseAddress_None", "d4/d13/group___b_a.html#ggaf28d67b92d721cb7c97ead5896c56838a99efad4c48ddde91d591a53bf20c034f", null ],
      [ "oC_BaseAddress_System", "d4/d13/group___b_a.html#ggaf28d67b92d721cb7c97ead5896c56838a18e056fd107bd51847b7ae07abee6b82", null ]
    ] ],
    [ "oC_PowerBaseAddress_t", "d4/d13/group___b_a.html#ga776fc7d00b0e4bbc59486961076d6d9e", null ],
    [ "oC_PowerBit_t", "d4/d13/group___b_a.html#ga55e45f80aa150f5be46d7d0a7439f1f5", null ],
    [ "oC_PowerOffset_t", "d4/d13/group___b_a.html#ga2a6c94560d74938c7805d7c16a15d927", null ]
];