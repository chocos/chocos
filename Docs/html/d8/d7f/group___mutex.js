var group___mutex =
[
    [ "oC_Mutex_t", "d8/d7f/group___mutex.html#gac72925f43af0381f29953c8880502d83", null ],
    [ "oC_Mutex_Type_t", "d8/d7f/group___mutex.html#ga5b5baec9087be0f40abf058d0b1f7d76", [
      [ "oC_Mutex_Type_NonRecursive", "d8/d7f/group___mutex.html#gga5b5baec9087be0f40abf058d0b1f7d76a7366a6ced20da58d9546ab5c95a59d54", null ],
      [ "oC_Mutex_Type_Recursive", "d8/d7f/group___mutex.html#gga5b5baec9087be0f40abf058d0b1f7d76ab52bd10686503b3af48c155978d3de0a", null ],
      [ "oC_Mutex_Type_Normal", "d8/d7f/group___mutex.html#gga5b5baec9087be0f40abf058d0b1f7d76ab71a8a3548bc38e62cfe667c7b8582ee", null ],
      [ "oC_Mutex_Type_Default", "d8/d7f/group___mutex.html#gga5b5baec9087be0f40abf058d0b1f7d76a452f703c6132a394277372ded668ea5d", null ]
    ] ],
    [ "oC_Mutex_Delete", "d8/d7f/group___mutex.html#gad56629d375e44b9208c289951e77a18f", null ],
    [ "oC_Mutex_GetBlockedName", "d8/d7f/group___mutex.html#gaae2399e755a1f05255a2e7ac985f534f", null ],
    [ "oC_Mutex_Give", "d8/d7f/group___mutex.html#gad0a8c644b940124af2b2354615db06e8", null ],
    [ "oC_Mutex_IsCorrect", "d8/d7f/group___mutex.html#gadaab5d58e4a762e952d510fdb806e0de", null ],
    [ "oC_Mutex_IsTakenByCurrentThread", "d8/d7f/group___mutex.html#gaa6101979ecb420a2a59fbcdcc62e4496", null ],
    [ "oC_Mutex_New", "d8/d7f/group___mutex.html#ga8853de4a76d55e57b749b0ef4127ce6d", null ],
    [ "oC_Mutex_Take", "d8/d7f/group___mutex.html#ga7d03988687537c5b7354f9a9aa9df3df", null ]
];