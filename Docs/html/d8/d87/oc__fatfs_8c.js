var oc__fatfs_8c =
[
    [ "Context_t", "d7/d83/struct_context__t.html", "d7/d83/struct_context__t" ],
    [ "File_t", "d8/d93/struct_file__t.html", "d8/d93/struct_file__t" ],
    [ "Dir_t", "d0/d64/struct_dir__t.html", null ],
    [ "FatFsResultToErrorCode", "d8/d87/oc__fatfs_8c.html#a071bab7adcdd2fd630d3e631f6cf7f7b", null ],
    [ "FatFsTimeFromTimestamp", "d8/d87/oc__fatfs_8c.html#a6095e7a310803b00e27e8e43b6d5ad9e", null ],
    [ "FatFsTimeToTimestamp", "d8/d87/oc__fatfs_8c.html#ae1e4ef175c074c52208367430593addf", null ],
    [ "IsContextCorrect", "d8/d87/oc__fatfs_8c.html#aba7d92049a33a7b052cbcf451bac1efc", null ],
    [ "IsDirCorrect", "d8/d87/oc__fatfs_8c.html#af31a2729baebf4b8aa99d2b7840667bc", null ],
    [ "IsFileCorrect", "d8/d87/oc__fatfs_8c.html#afaa676829deed9bbdfebf3ca94fe5eec", null ]
];