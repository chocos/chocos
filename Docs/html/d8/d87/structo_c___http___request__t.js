var structo_c___http___request__t =
[
    [ "Connection", "d8/d87/structo_c___http___request__t.html#afa6791cf2c2cf161d1a9bcae8e641da7", null ],
    [ "ContentLength", "d8/d87/structo_c___http___request__t.html#a808a5cb9455d68c4bd1767eeca45046f", null ],
    [ "ContentType", "d8/d87/structo_c___http___request__t.html#ac2eda104376da0864363d67d7be42103", null ],
    [ "Method", "d8/d87/structo_c___http___request__t.html#aebd280b222ebf3e1a80a952260e8f390", null ],
    [ "Packet", "d8/d87/structo_c___http___request__t.html#a8e9adfcb15226c8379b096bf4f55e10a", null ],
    [ "Path", "d8/d87/structo_c___http___request__t.html#a92f2f53f10cb856d51b9852cb572a227", null ],
    [ "Version", "d8/d87/structo_c___http___request__t.html#a64e41922235b5e1182641d12c9893f93", null ]
];