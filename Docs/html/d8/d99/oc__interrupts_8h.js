var oc__interrupts_8h =
[
    [ "oC_DefaultInterruptHandler", "d7/dee/group___interrupts.html#ga58915d21416e0ff627f4d22cc68280c1", null ],
    [ "oC_DefaultInterruptHandlerPrototype", "d7/dee/group___interrupts.html#ga7c85d23d9933e5bd66385abb866a032d", null ],
    [ "oC_InterruptBaseAddress_", "d7/dee/group___interrupts.html#ga080f4dea4c0320626a46e297b302aa50", null ],
    [ "oC_InterruptHandler", "d7/dee/group___interrupts.html#gae6d5dc839e10cacc1d82166681b287b0", null ],
    [ "oc_InterruptHandlerName", "d7/dee/group___interrupts.html#ga9998709412a2b08163b664b678dd578c", null ],
    [ "oC_InterruptHandlerPrototype", "d7/dee/group___interrupts.html#ga0c505c3a18c184f8560a36b6e0052e9f", null ],
    [ "oC_InterruptHandlerWeakPrototype", "d7/dee/group___interrupts.html#gaa11910bd2f001653d24aebfe498e8f64", null ],
    [ "oC_InterruptIndex_", "d7/dee/group___interrupts.html#ga7804b7a88ac9839669ebb9b067d45fad", null ],
    [ "oC_InterruptNumber_", "d7/dee/group___interrupts.html#ga78c7f7172bbd546820decc7917365758", null ],
    [ "oC_InterruptType_", "d7/dee/group___interrupts.html#gacea594c50ce6d7023070d0457fae784c", null ],
    [ "oC_ResetInterruptHandler", "d7/dee/group___interrupts.html#gabc5439038115023935346598ba12d6cf", null ],
    [ "oC_ResetInterruptHandlerName", "d7/dee/group___interrupts.html#ga7bff07b4379e7b9a5d15be8173e0ee08", null ],
    [ "oC_ResetInterruptHandlerPrototype", "d7/dee/group___interrupts.html#gad15374f4ac09a2e3b80c48658d848de9", null ],
    [ "oC_InterruptHandler_t", "d7/dee/group___interrupts.html#gacaab9f5b2101decb0a11640167d74494", null ],
    [ "oC_InterruptBaseAddress_t", "d7/dee/group___interrupts.html#gab9c44374b81d032629766acdded6333c", null ],
    [ "oC_InterruptIndex_t", "d7/dee/group___interrupts.html#gaae73f60c0f7fbac9f5defa8ad1e01f27", null ],
    [ "oC_InterruptNumber_t", "d7/dee/group___interrupts.html#ga05ddfa954f94bc7ad4046aa2d92d405f", null ],
    [ "oC_InterruptType_t", "d7/dee/group___interrupts.html#ga45ddbb078aec0b3a5715622d1b092cf4", null ],
    [ "oC_Interrupt_GetData", "d8/d99/oc__interrupts_8h.html#a4a5789ddcea6dec4df1795d19e2691bf", null ],
    [ "oC_InterruptData", "d7/dee/group___interrupts.html#ga94bbe466317dc0d1f30dd97147770900", null ],
    [ "oC_UnexpectedInterruptHandler", "d7/dee/group___interrupts.html#ga1759cc3c6c78af6075de2d0b4d59155a", null ]
];