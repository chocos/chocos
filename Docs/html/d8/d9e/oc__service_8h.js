var oc__service_8h =
[
    [ "oC_Service_Registration_t", "d2/dbc/structo_c___service___registration__t.html", null ],
    [ "oC_Service_t", "d8/d9e/oc__service_8h.html#adb4dfe9119bfdd7b18b1145fd46d5c7f", null ],
    [ "oC_Service_Delete", "d8/d9e/oc__service_8h.html#a47696a340dffe66e69e3e3269a27f1a8", null ],
    [ "oC_Service_GetName", "d8/d9e/oc__service_8h.html#a2df78a38d5f3e759f8dacba1793c4634", null ],
    [ "oC_Service_GetProcess", "d8/d9e/oc__service_8h.html#adc99c1e8690f5e9f49f030fb74278635", null ],
    [ "oC_Service_IsActive", "d8/d9e/oc__service_8h.html#ad0c0af004956ae0990a207614305ce46", null ],
    [ "oC_Service_IsCorrect", "d8/d9e/oc__service_8h.html#a57372c9c4b4df24f5810f798f740f350", null ],
    [ "oC_Service_New", "d8/d9e/oc__service_8h.html#a108a5ff7042aaf2c805361ec27036180", null ],
    [ "oC_Service_Start", "d8/d9e/oc__service_8h.html#a509d8aaef41f56d614cfa80712936504", null ],
    [ "oC_Service_Stop", "d8/d9e/oc__service_8h.html#a402d59007227a5c42ecb54c5a4957835", null ]
];