var oc__clock__lld_8h =
[
    [ "oC_CLOCK_LLD_Interrupt_t", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga0eaae621ac72008f0d07eb380f0f66f3", null ],
    [ "oC_CLOCK_LLD_ClockSource_t", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga0d4526a7fc88ec6c779159544b77bd09", [
      [ "oC_CLOCK_LLD_ClockSource_Internal", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gga0d4526a7fc88ec6c779159544b77bd09a1b455deb2becde8a62130e1b94a86c3d", null ],
      [ "oC_CLOCK_LLD_ClockSource_External", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gga0d4526a7fc88ec6c779159544b77bd09a14bf17a983dcad65dea1acdc5af67bce", null ]
    ] ],
    [ "oC_CLOCK_LLD_ConfigureExternalClock", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga205c216ee32b12d623726c7d7262e74f", null ],
    [ "oC_CLOCK_LLD_ConfigureHibernationClock", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga58824f39f62213a0dd295d8ff9248b98", null ],
    [ "oC_CLOCK_LLD_ConfigureInternalClock", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gace5ac7235288066ca80dab2eb62d0a29", null ],
    [ "oC_CLOCK_LLD_DelayForMicroseconds", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga62c035cd799c19696a6672d462d82cd2", null ],
    [ "oC_CLOCK_LLD_GetClockFrequency", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gaf5300693845bfacad20b288ab640c307", null ],
    [ "oC_CLOCK_LLD_GetClockSource", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga09920d987413a24dcdf802f2c8c3e8de", null ],
    [ "oC_CLOCK_LLD_GetMaximumClockFrequency", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga57f5fb7b7a15952525edbdc73dc07da6", null ],
    [ "oC_CLOCK_LLD_GetOscillatorFrequency", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga17a0e873595892729eff2b2cd57f8934", null ],
    [ "oC_CLOCK_LLD_SetClockConfiguredInterrupt", "d8/df6/group___c_l_o_c_k-_l_l_d.html#ga3d1d77f14159fda01daadbb13c3c40f0", null ],
    [ "oC_CLOCK_LLD_TurnOffDriver", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gaeb7c47238b786785485cf8178da91d15", null ],
    [ "oC_CLOCK_LLD_TurnOnDriver", "d8/df6/group___c_l_o_c_k-_l_l_d.html#gaae3eca0b66e8536a74abf5786b097d41", null ]
];