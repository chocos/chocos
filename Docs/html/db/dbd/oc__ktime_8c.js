var oc__ktime_8c =
[
    [ "MemoryFaultHandler", "db/dbd/oc__ktime_8c.html#a8f231ba2a8022fb19acf2caeb067ef51", null ],
    [ "oC_KTime_GetCurrentTick", "de/da6/group___k_time.html#gae876d1018951a3c5b4777c4fd3125cb9", null ],
    [ "oC_KTime_GetTimestamp", "de/da6/group___k_time.html#gaabe82dcd7b7ab89790197076bebd7a1f", null ],
    [ "oC_KTime_TickToTime", "de/da6/group___k_time.html#gae0ebb643113711e51d9bf6bf25a3a22d", null ],
    [ "oC_KTime_TurnOff", "de/da6/group___k_time.html#gac0111c6abdd26b98b5c3dc60628a3748", null ],
    [ "oC_KTime_TurnOn", "de/da6/group___k_time.html#gae3d84dd30556024ba55d8a06305aab87", null ],
    [ "TimerHandler", "db/dbd/oc__ktime_8c.html#ac17f883ef25b8a7ad4ed6ad3305f81cd", null ]
];