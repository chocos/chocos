var struct_server__t =
[
    [ "ConnectionConfig", "db/d95/struct_server__t.html#aa91240b78c470dd84c3f52932379b28c", null ],
    [ "ConnectionFinishedFunction", "db/d95/struct_server__t.html#aed8f244e06c638d3cde03d91c1a0079d", null ],
    [ "ConnectionFinishedParameter", "db/d95/struct_server__t.html#a2ee868698a43ff4769507d187ed881d8", null ],
    [ "Connections", "db/d95/struct_server__t.html#aa63e726567d9700d39c5d4e45d8460a7", null ],
    [ "FreeSlotsSemaphore", "db/d95/struct_server__t.html#a7969a797a94fec4dc666d2fd0bbf5902", null ],
    [ "MaxConnections", "db/d95/struct_server__t.html#a966ae95b7a0aced50780d00b5e2004be", null ],
    [ "Name", "db/d95/struct_server__t.html#acc89d848bb7fe5a9892ee0a0da5345f5", null ],
    [ "NewConnectionSemaphore", "db/d95/struct_server__t.html#a8f9a25036340b7568212db2ac8492d9f", null ],
    [ "ObjectControl", "db/d95/struct_server__t.html#af8aa619116d8a74934500e135a9dbb43", null ],
    [ "Process", "db/d95/struct_server__t.html#a862c271da330044e3b398bbf95488ce2", null ],
    [ "Running", "db/d95/struct_server__t.html#ae2e97f1ebd7c956e2c68d1209eabd4b1", null ],
    [ "Thread", "db/d95/struct_server__t.html#a175835daeda716ad0bc886a7e5ef5e6f", null ]
];