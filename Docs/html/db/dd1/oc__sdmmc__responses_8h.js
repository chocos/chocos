var oc__sdmmc__responses_8h =
[
    [ "oC_SDMMC_Responses_TransferSpeed_t", "d8/dc1/uniono_c___s_d_m_m_c___responses___transfer_speed__t.html", "d8/dc1/uniono_c___s_d_m_m_c___responses___transfer_speed__t" ],
    [ "PACKED", "d1/d18/structo_c___s_d_m_m_c___responses___transfer_speed__t_1_1_p_a_c_k_e_d.html", "d1/d18/structo_c___s_d_m_m_c___responses___transfer_speed__t_1_1_p_a_c_k_e_d" ],
    [ "oC_SDMMC_Responses_CardSpecificData_t", "df/da6/uniono_c___s_d_m_m_c___responses___card_specific_data__t.html", "df/da6/uniono_c___s_d_m_m_c___responses___card_specific_data__t" ],
    [ "oC_SDMMC_Response_t", "da/db7/structo_c___s_d_m_m_c___response__t.html", "da/db7/structo_c___s_d_m_m_c___response__t" ],
    [ "oC_SDMMC_LongResponseData_t", "db/dd1/oc__sdmmc__responses_8h.html#aa07e6d4f1103e7bdafe76b8bfccfd03b", null ],
    [ "oC_SDMMC_Responses_CardId_t", "db/dd1/oc__sdmmc__responses_8h.html#a04a79313f2b9c21993badf20156b9aa3", null ],
    [ "oC_SDMMC_Responses_CardSpecificData_V1p0_t", "db/dd1/oc__sdmmc__responses_8h.html#a3f943ef160d44b717ac27456bcfe43cb", null ],
    [ "oC_SDMMC_Responses_CardSpecificData_V2p0_t", "db/dd1/oc__sdmmc__responses_8h.html#a5694d14b34c5bf9ffc5fe0544e595a38", null ],
    [ "oC_SDMMC_Responses_R1_t", "db/dd1/oc__sdmmc__responses_8h.html#a8fc979973899d29f65ceb44aa442f0d0", null ],
    [ "oC_SDMMC_Responses_R3_t", "db/dd1/oc__sdmmc__responses_8h.html#a4d95253b678675e28e0809e39504f47b", null ],
    [ "oC_SDMMC_Responses_R6_t", "db/dd1/oc__sdmmc__responses_8h.html#a0e18cff25350e73b18fe26f0d2bceb7e", null ],
    [ "oC_SDMMC_Responses_R7_t", "db/dd1/oc__sdmmc__responses_8h.html#a8cca3aa313cb8662f017d82288b18706", null ],
    [ "oC_SDMMC_Responses_TAAC_t", "db/dd1/oc__sdmmc__responses_8h.html#a225164fabd056839dcee5ac54f515248", null ],
    [ "oC_SDMMC_ShortResponseData_t", "db/dd1/oc__sdmmc__responses_8h.html#a12203135fd196d379bc930dfd561bb62", null ],
    [ "oC_SDMMC_ResponseIndex_t", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153", [
      [ "oC_SDMMC_ResponseIndex_None", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153ae5a5eeea3919e57172207a794d95049e", null ],
      [ "oC_SDMMC_ResponseIndex_R1", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153ad6de0e41034462debd6bd210516206d6", null ],
      [ "oC_SDMMC_ResponseIndex_R1b", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153ab129505dd237292d6953c11821b2f23b", null ],
      [ "oC_SDMMC_ResponseIndex_R2", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153ac6224a45307c7ef5cd2f38fbeefd81be", null ],
      [ "oC_SDMMC_ResponseIndex_R3", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153a63ca8412515f42c32ba34c580f75d9fd", null ],
      [ "oC_SDMMC_ResponseIndex_R6", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153a8b546606f7f63884675a9dbdf1021785", null ],
      [ "oC_SDMMC_ResponseIndex_R7", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153a829534f9b260b15a2db3fb1adf041aae", null ],
      [ "oC_SDMMC_ResponseIndex_MAX", "db/dd1/oc__sdmmc__responses_8h.html#a0e99a8011a49c31bf3a8892ee264a153ab0b49f0613adb5653e4dbdb7fa3a51a8", null ]
    ] ],
    [ "oC_SDMMC_Responses_ResponseType_t", "db/dd1/oc__sdmmc__responses_8h.html#af3733a8d07dee499a799521f0e985f62", [
      [ "oC_SDMMC_Responses_ResponseType_None", "db/dd1/oc__sdmmc__responses_8h.html#af3733a8d07dee499a799521f0e985f62a481eff76a806a02129c23e0060890aed", null ],
      [ "oC_SDMMC_Responses_ResponseType_Short", "db/dd1/oc__sdmmc__responses_8h.html#af3733a8d07dee499a799521f0e985f62a33b5afef97952494c292d4e183fc6200", null ],
      [ "oC_SDMMC_Responses_ResponseType_Long", "db/dd1/oc__sdmmc__responses_8h.html#af3733a8d07dee499a799521f0e985f62a932f7e8bfba2984b5d59face4503c13c", null ],
      [ "oC_SDMMC_Responses_ResponseType_NumberOfTypes", "db/dd1/oc__sdmmc__responses_8h.html#af3733a8d07dee499a799521f0e985f62a370652645554fe83bb30caaff0dd7bf0", null ]
    ] ],
    [ "oC_SDMMC_Responses_VoltageWindow_t", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013", [
      [ "oC_SDMMC_Responses_VoltageWindow_2p7to2p8", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013a467f55b55cf1c08848e0e7f5dbae00e1", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_2p8to2p9", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013ab76e40207895868a2eed47724fd30c6b", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_2p9to3p0", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013a4e4e1440c643495c305b7fe07a6328c2", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_3p0to3p1", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013ad147455a48c1e6c7fbf942eadd54f4dd", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_3p1to3p2", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013a5d60d6228a4108ef93ccee711b94f138", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_3p2to3p3", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013aed1e6a7e6a633d40c4fab617ffb48f9a", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_3p3to3p4", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013a2a9fb31ffe7c5950a1e957cf133d5347", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_3p4to3p5", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013a904a4a4e0a034f357a918ede45a4e1ec", null ],
      [ "oC_SDMMC_Responses_VoltageWindow_3p5to3p6", "db/dd1/oc__sdmmc__responses_8h.html#aeccdd60e374ef05b7db13dda2362a013aa38e148173bbe433310bb64a5532bd56", null ]
    ] ],
    [ "oC_SDMMC_Responses_Handle", "db/dd1/oc__sdmmc__responses_8h.html#a4ee871469ae857a614a5ca2133718de1", null ],
    [ "oC_SDMMC_Responses_IsResponseIndexValid", "db/dd1/oc__sdmmc__responses_8h.html#a304787f326a85a8b883b34fc116c5742", null ],
    [ "oC_SDMMC_Responses_PrepareVoltageWindow", "db/dd1/oc__sdmmc__responses_8h.html#afb5ff17e4168b7f83c71462e31229701", null ],
    [ "oC_SDMMC_Responses_ReadResponseIndex", "db/dd1/oc__sdmmc__responses_8h.html#ae8a06b8c4d21b816e6bfe629c6c822dd", null ],
    [ "oC_SDMMC_Responses_ReadResponseName", "db/dd1/oc__sdmmc__responses_8h.html#a4243a82da59df0e5fcacb964190c9625", null ],
    [ "oC_SDMMC_Responses_ReadTransferSpeed", "db/dd1/oc__sdmmc__responses_8h.html#a4f27820d6862f043c20f2644ec0a2686", null ],
    [ "oC_SDMMC_Responses_SwapBytes", "db/dd1/oc__sdmmc__responses_8h.html#aac44d6fda4c423111d264375980b7501", null ],
    [ "oC_SDMMC_Responses_VerifyR1", "db/dd1/oc__sdmmc__responses_8h.html#afcb600175abae11c9330ccfa9f4b6a06", null ]
];