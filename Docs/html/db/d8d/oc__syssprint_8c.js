var oc__syssprint_8c =
[
    [ "Context_t", "d7/d83/struct_context__t.html", "d7/d83/struct_context__t" ],
    [ "GraphStyle_t", "d7/daa/struct_graph_style__t.html", null ],
    [ "DrawClock", "db/d8d/oc__syssprint_8c.html#adedd6f758158ecbe7b93b658d43c97c0", null ],
    [ "DrawCpuLoad", "db/d8d/oc__syssprint_8c.html#a9ae69f65ebeaee10407e0c8afa241eb1", null ],
    [ "DrawGraph", "db/d8d/oc__syssprint_8c.html#a88f154e71dafecd34d9e972c87951071", null ],
    [ "DrawGraphBorders", "db/d8d/oc__syssprint_8c.html#a09fd945d3a2d00c2ca905c843b39ae21", null ],
    [ "DrawNews", "db/d8d/oc__syssprint_8c.html#abeb0402260beacc3889c9c57d05044b2", null ],
    [ "DrawRamInfo", "db/d8d/oc__syssprint_8c.html#a4d829445e9dc52154e300ad7c91a6521", null ],
    [ "DrawShortNetInfo", "db/d8d/oc__syssprint_8c.html#a9ac08af35aeda41d91449ef25c8abde5", null ],
    [ "DrawSystemVersion", "db/d8d/oc__syssprint_8c.html#a214272fbcf42117bbccf7700f95f82c2", null ],
    [ "MainFunction", "db/d8d/oc__syssprint_8c.html#aa6874831fa32e8d090d759a7b1cf4427", null ],
    [ "StartService", "db/d8d/oc__syssprint_8c.html#a10488aca8479046487cda5163750f60f", null ],
    [ "StopService", "db/d8d/oc__syssprint_8c.html#af85bda8cae13acaeba963423d1be8535", null ]
];