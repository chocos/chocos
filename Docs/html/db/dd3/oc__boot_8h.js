var oc__boot_8h =
[
    [ "oC_Boot_Level_t", "d0/d8d/group___boot.html#ga2c6f6602f7005d163e50f59eb070a08d", null ],
    [ "oC_Boot_Reason_t", "d0/d8d/group___boot.html#gab5ec26e87d06ef0900a444513ee714be", [
      [ "oC_Boot_Reason_Unknown", "d0/d8d/group___boot.html#ggab5ec26e87d06ef0900a444513ee714bea7c3ce1ed9dd564fb912cf85ae1fcc7e5", null ],
      [ "oC_Boot_Reason_UserRequest", "d0/d8d/group___boot.html#ggab5ec26e87d06ef0900a444513ee714bea025e93f792a4c1668d4e4fee269c0300", null ],
      [ "oC_Boot_Reason_DriverError", "d0/d8d/group___boot.html#ggab5ec26e87d06ef0900a444513ee714bea26f0e08194c20064f1cac96436d84788", null ],
      [ "oC_Boot_Reason_SystemException", "d0/d8d/group___boot.html#ggab5ec26e87d06ef0900a444513ee714bea3d9bf270ace0284d9b803689557fe2e9", null ],
      [ "oC_Boot_Reason_MemoryLackout", "d0/d8d/group___boot.html#ggab5ec26e87d06ef0900a444513ee714beaa392c3448a70db708495caf11a16c399", null ]
    ] ],
    [ "oC_Boot_GetCurrentBootLevel", "d0/d8d/group___boot.html#ga0058757f0e2db28bd00edf22986998a0", null ],
    [ "oC_Boot_GetLastShutdownReason", "d0/d8d/group___boot.html#ga844dbfdf6775e5490ae2a07478f7bab1", null ],
    [ "oC_Boot_GetLastShutdownUser", "d0/d8d/group___boot.html#ga797267402213f166f575f8db22bc732a", null ],
    [ "oC_Boot_GetStartupTimestamp", "d0/d8d/group___boot.html#gaa0d0617e939c4562644d4dd205883094", null ],
    [ "oC_Boot_Main", "d0/d8d/group___boot.html#gae65cec61b7bac9d9e57ef7f76ec560f0", null ],
    [ "oC_Boot_Restart", "d0/d8d/group___boot.html#ga7fc1c4012636cc6ebe50f3c437099c3c", null ],
    [ "oC_Boot_Shutdown", "d0/d8d/group___boot.html#ga8539e3504fbeba68ff712c39f3a663a0", null ]
];