var oc__fs_8h =
[
    [ "oC_FileSystem_ModeFlags_t", "de/ddb/group___file_system.html#gab3c5e9fdaf956cc26b64b53d428729fb", [
      [ "oC_FileSystem_ModeFlags_Read", "de/ddb/group___file_system.html#ggab3c5e9fdaf956cc26b64b53d428729fba6854d5776ff674c86ae9728e15479006", null ],
      [ "oC_FileSystem_ModeFlags_Write", "de/ddb/group___file_system.html#ggab3c5e9fdaf956cc26b64b53d428729fba44e235fd38bc26473a4478fbe5886bcd", null ],
      [ "oC_FileSystem_ModeFlags_SeekToTheEnd", "de/ddb/group___file_system.html#ggab3c5e9fdaf956cc26b64b53d428729fbad2de2e416c0fc1cb5bfc5cd524c965ad", null ],
      [ "oC_FileSystem_ModeFlags_OpenExisting", "de/ddb/group___file_system.html#ggab3c5e9fdaf956cc26b64b53d428729fba271f3fee408e34b89495ab7e2bb0e32d", null ],
      [ "oC_FileSystem_ModeFlags_OpenAlways", "de/ddb/group___file_system.html#ggab3c5e9fdaf956cc26b64b53d428729fbad51496bde5cc3fe57983f4a6d8c718e9", null ],
      [ "oC_FileSystem_ModeFlags_CreateNew", "de/ddb/group___file_system.html#ggab3c5e9fdaf956cc26b64b53d428729fbac0c5bd95ba18df185096d1e1bbd789c1", null ],
      [ "oC_FileSystem_ModeFlags_CreateNewAlways", "de/ddb/group___file_system.html#ggab3c5e9fdaf956cc26b64b53d428729fbaaf82203e3fcc3e4ca79c78b584d25bad", null ]
    ] ]
];