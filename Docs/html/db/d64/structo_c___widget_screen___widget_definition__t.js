var structo_c___widget_screen___widget_definition__t =
[
    [ "CheckBox", "db/d64/structo_c___widget_screen___widget_definition__t.html#a3d2deb362d0c5525a21bae1ebc096cab", null ],
    [ "DefaultString", "db/d64/structo_c___widget_screen___widget_definition__t.html#a417bf779ff12966ea1d364ac37c767c2", null ],
    [ "DrawStyle", "db/d64/structo_c___widget_screen___widget_definition__t.html#ab0b21ff3c30e8adb026e96f6fa3d6490", null ],
    [ "Font", "db/d64/structo_c___widget_screen___widget_definition__t.html#ac0e3d2f0de62b32f90de1ed0ce621b19", null ],
    [ "Handlers", "db/d64/structo_c___widget_screen___widget_definition__t.html#a1302686a5e3e6a13f30a85233a65018e", null ],
    [ "Height", "db/d64/structo_c___widget_screen___widget_definition__t.html#a184d49528cffcfc8ec211807d1870537", null ],
    [ "HorizontalCellSpacing", "db/d64/structo_c___widget_screen___widget_definition__t.html#a42d010aa1bc889ba698acfca18968576", null ],
    [ "ImagePath", "db/d64/structo_c___widget_screen___widget_definition__t.html#abb0f47ba85c151b8130180c4b7ba99f6", null ],
    [ "OptionHeight", "db/d64/structo_c___widget_screen___widget_definition__t.html#a806dae67329852536927e9c6540f2fb2", null ],
    [ "OptionWidth", "db/d64/structo_c___widget_screen___widget_definition__t.html#a68571ed144300ebae1ba2e4a134a441d", null ],
    [ "Palette", "db/d64/structo_c___widget_screen___widget_definition__t.html#ac553099ef1dd0850435c8cc2c626bf99", null ],
    [ "Position", "db/d64/structo_c___widget_screen___widget_definition__t.html#a1c09e5e9db86c5d2cb89fc1c5df1aadf", null ],
    [ "PushButton", "db/d64/structo_c___widget_screen___widget_definition__t.html#a5c90330390c262af93b66c06f0c3832d", null ],
    [ "TextAlign", "db/d64/structo_c___widget_screen___widget_definition__t.html#a0b8f7826af99fffa1474bf9bf3e4558c", null ],
    [ "Type", "db/d64/structo_c___widget_screen___widget_definition__t.html#adee4cbd4ca67477bf055ca96bc2d6490", null ],
    [ "Type", "db/d64/structo_c___widget_screen___widget_definition__t.html#a6c014053734fe73f8ddf481252f79c59", null ],
    [ "UpdateStringHandler", "db/d64/structo_c___widget_screen___widget_definition__t.html#a38a74b05f0934a0a5cf21269e6048307", null ],
    [ "VerticalCellSpacing", "db/d64/structo_c___widget_screen___widget_definition__t.html#a657e1cbe2286d527972e6557bcb646b0", null ],
    [ "VerticalTextAlign", "db/d64/structo_c___widget_screen___widget_definition__t.html#a17f659429069cb8a0ffca487d6319112", null ],
    [ "Width", "db/d64/structo_c___widget_screen___widget_definition__t.html#a4ee55a170448d60b1f6fe43fca496673", null ],
    [ "ZPosition", "db/d64/structo_c___widget_screen___widget_definition__t.html#a359c3e954425f5e91dbe4bc940aa4b6d", null ]
];