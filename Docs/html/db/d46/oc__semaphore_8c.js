var oc__semaphore_8c =
[
    [ "Semaphore_t", "d7/db3/struct_semaphore__t.html", null ],
    [ "oC_Semaphore_Delete", "dd/d78/group___semaphore.html#gaef2cecd9f028ef680def073dc254ec5a", null ],
    [ "oC_Semaphore_ForceValue", "dd/d78/group___semaphore.html#gac6eb4a971b6ea81a613ec89d90923dae", null ],
    [ "oC_Semaphore_Give", "dd/d78/group___semaphore.html#ga4c4abcd5927ae6c425defa5961e6e955", null ],
    [ "oC_Semaphore_GiveCounting", "dd/d78/group___semaphore.html#ga3a74c818efb7ec79aef13c4c282cb31a", null ],
    [ "oC_Semaphore_IsCorrect", "dd/d78/group___semaphore.html#ga4181805f8273e371abfada4c46ac1171", null ],
    [ "oC_Semaphore_New", "dd/d78/group___semaphore.html#ga72cc1faa6bb99fd0ca3bcbd96a0182a5", null ],
    [ "oC_Semaphore_Take", "dd/d78/group___semaphore.html#ga36d72a586b69c4ace3c23576afe00263", null ],
    [ "oC_Semaphore_TakeCounting", "dd/d78/group___semaphore.html#ga6c981d0dce90187ad08385b19bd8d09e", null ],
    [ "oC_Semaphore_TakeCountingSoft", "dd/d78/group___semaphore.html#gaea2991c4d02f735b228ffdce8c30f889", null ],
    [ "oC_Semaphore_Value", "dd/d78/group___semaphore.html#gac9a0e0781fe318aba193a57c806be43b", null ]
];