var structo_c___s_d_m_m_c___config__t =
[
    [ "Advanced", "db/dd0/structo_c___s_d_m_m_c___config__t.html#a91e0e121ee73b504692b81225a083922", null ],
    [ "Channel", "db/dd0/structo_c___s_d_m_m_c___config__t.html#af40238f432cf7cca88f956542a453711", null ],
    [ "DetectionMode", "db/dd0/structo_c___s_d_m_m_c___config__t.html#adcb89c41a471d2f1fcfa044030a66c08", null ],
    [ "DetectionPollingPeriod", "db/dd0/structo_c___s_d_m_m_c___config__t.html#abd632ea694ee8af94d45eb796f719804", null ],
    [ "NumberOfRetries", "db/dd0/structo_c___s_d_m_m_c___config__t.html#ae9e9f5c17dd69960a71c24f442076fe4", null ],
    [ "Pins", "db/dd0/structo_c___s_d_m_m_c___config__t.html#a64e4480aa07bbb7946c37af3b3536934", null ],
    [ "PowerMode", "db/dd0/structo_c___s_d_m_m_c___config__t.html#aec55b612ee80c154b760c2e45a0c46e1", null ],
    [ "SectorSize", "db/dd0/structo_c___s_d_m_m_c___config__t.html#a8fd1d60aa3e4d98c596e7476ab66a752", null ],
    [ "TransferMode", "db/dd0/structo_c___s_d_m_m_c___config__t.html#aeeb77d21464922439f3ebe551f76c5a3", null ]
];