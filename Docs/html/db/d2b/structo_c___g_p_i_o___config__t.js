var structo_c___g_p_i_o___config__t =
[
    [ "Current", "db/d2b/structo_c___g_p_i_o___config__t.html#afaa447afb862297c8702a84c0a68ca34", null ],
    [ "InterruptTrigger", "db/d2b/structo_c___g_p_i_o___config__t.html#a323fb3e727dc140be3740408cdeb0647", null ],
    [ "Mode", "db/d2b/structo_c___g_p_i_o___config__t.html#a2e52953a9694d1343dc526e583899373", null ],
    [ "OutputCircuit", "db/d2b/structo_c___g_p_i_o___config__t.html#a165d3bfd6183e352c889aaea26e55d44", null ],
    [ "Pins", "db/d2b/structo_c___g_p_i_o___config__t.html#a5206d190faf8f7e0f266b571d02a96a8", null ],
    [ "Protection", "db/d2b/structo_c___g_p_i_o___config__t.html#a3f669dbfbd1749ac5e46fc3ed9cf5ff4", null ],
    [ "Pull", "db/d2b/structo_c___g_p_i_o___config__t.html#a549f7b2d6fff4a788f1df69550004114", null ],
    [ "Speed", "db/d2b/structo_c___g_p_i_o___config__t.html#a187613368559e269a4421739a009626c", null ]
];