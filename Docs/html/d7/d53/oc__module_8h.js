var oc__module_8h =
[
    [ "oC_Module_Registration_t", "d3/d0a/structo_c___module___registration__t.html", null ],
    [ "oC_Module_IsTurnedOn", "d7/d53/oc__module_8h.html#ac6358af13f1ed27f3114b22c6a36de23", null ],
    [ "oC_Module_TurnOff", "d7/d53/oc__module_8h.html#a688d754d17fbdb672e5f5f7cbb4326ef", null ],
    [ "oC_Module_TurnOffVerification", "d7/d53/oc__module_8h.html#a19d3a07628fb6a0ac5cae65aa2c4936d", null ],
    [ "oC_Module_TurnOn", "d7/d53/oc__module_8h.html#ac0448826f1ccce4fd6a0aaf2e5a7e3a0", null ],
    [ "oC_Module_TurnOnVerification", "d7/d53/oc__module_8h.html#ad1ec4f7859cc2b309a37022483a654d3", null ]
];