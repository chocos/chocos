var structo_c___widget_screen___draw_style__t =
[
    [ "BorderStyle", "d7/d6a/structo_c___widget_screen___draw_style__t.html#a37af5a9e82700cc0d59f35ead30210da", null ],
    [ "BorderWidth", "d7/d6a/structo_c___widget_screen___draw_style__t.html#a3163c22df99486cdde1ced218f39b0c4", null ],
    [ "DrawFill", "d7/d6a/structo_c___widget_screen___draw_style__t.html#adf5bd800e273681651647f70b44f2b83", null ],
    [ "DrawText", "d7/d6a/structo_c___widget_screen___draw_style__t.html#a29818498867ce2ab6ed62e9ea69e2dfa", null ],
    [ "Opacity", "d7/d6a/structo_c___widget_screen___draw_style__t.html#af0b7308da8f3ac96fabb9ba07b8c15bd", null ]
];