var oc__exchan_8c =
[
    [ "EventLog_t", "dc/d47/struct_event_log__t.html", null ],
    [ "MAX_LOGGED_EVENTS", "d7/d82/oc__exchan_8c.html#a6aa0c32a02d86004b00ea9bda97c488a", null ],
    [ "DaemonThreadHandler", "d7/d82/oc__exchan_8c.html#acf9499f1da70bd7e95e714f7ad17017f", null ],
    [ "LogEvent", "d7/d82/oc__exchan_8c.html#a6596ae498078829fa5a4188eb3d792af", null ],
    [ "MemoryEventHandler", "d7/d82/oc__exchan_8c.html#abf50133c3d9fd090387517624fbc0ac7", null ],
    [ "oC_ExcHan_LogEvent", "db/dad/group___exc_han.html#ga5c2e77223f7b8630868cca67dc302548", null ],
    [ "oC_ExcHan_PrintLoggedEvents", "db/dad/group___exc_han.html#gaa18a1156b007fcb9a61ae4e8f4f4f94b", null ],
    [ "oC_ExcHan_RunDaemon", "db/dad/group___exc_han.html#gab58ca31a85962d2c346e142dd4d42dcd", null ],
    [ "oC_ExcHan_TurnOff", "db/dad/group___exc_han.html#ga860f3a7517efd183013dc18530b82002", null ],
    [ "oC_ExcHan_TurnOn", "db/dad/group___exc_han.html#ga7029c50858acfa700b820345d615e5eb", null ],
    [ "SystemEventHandler", "d7/d82/oc__exchan_8c.html#a7323fac8032627af53c201b5d4fab6c3", null ]
];