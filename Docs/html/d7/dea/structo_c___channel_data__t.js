var structo_c___channel_data__t =
[
    [ "BaseAddress", "d7/dea/structo_c___channel_data__t.html#ae7d18e04af7643a30145dc11d82baa2c", null ],
    [ "ChannelName", "d7/dea/structo_c___channel_data__t.html#ae366843255ab7e4ad1636ce80cd7fbd7", null ],
    [ "PowerBaseAddress", "d7/dea/structo_c___channel_data__t.html#a1cad0461137333e4c7b16edc19282013", null ],
    [ "PowerBitIndex", "d7/dea/structo_c___channel_data__t.html#a19148a6693d793a5c884b1d41ac282a6", null ],
    [ "PowerOffset", "d7/dea/structo_c___channel_data__t.html#a9ba0059a347af67cf39a11d92639dc2a", null ],
    [ "RegisterMap", "d7/dea/structo_c___channel_data__t.html#a52c00ff93443ff0be9aa618257661728", null ]
];