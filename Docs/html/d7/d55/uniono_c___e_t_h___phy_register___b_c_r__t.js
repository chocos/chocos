var uniono_c___e_t_h___phy_register___b_c_r__t =
[
    [ "AutoNegotiation", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#aca914039c9b0088669b953a09695fbe3", null ],
    [ "DuplexMode", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#a7532a3f4add21e355c82b5b0a5de5b5c", null ],
    [ "Isolate", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#ab1df1544ad590d65bb915025b403601d", null ],
    [ "Loopback", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#a0ea207d23b44a46480c8c20ebebffb3b", null ],
    [ "PowerDown", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#ac7c47282323e08fd0e9b273a4ee22940", null ],
    [ "Reserved0_7", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#a40db4b66b501b198acac7fa28ba56f96", null ],
    [ "Reserved16_31", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#aba5db68147cabbe25bbb3e00eac220d6", null ],
    [ "RestartAutonegotiate", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#ac6f2f4751bc936c6ce747bd9d80e50cc", null ],
    [ "SoftReset", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#a6708318b834feb25ba72109b35dc920c", null ],
    [ "SpeedSelect", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#a76bf1b73f0751436f5d895af2074c501", null ],
    [ "Value", "d7/d55/uniono_c___e_t_h___phy_register___b_c_r__t.html#a8e0dcce3428a8051614e852b8836d0d1", null ]
];