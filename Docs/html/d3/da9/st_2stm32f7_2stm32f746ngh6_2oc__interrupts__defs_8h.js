var st_2stm32f7_2stm32f746ngh6_2oc__interrupts__defs_8h =
[
    [ "oC_DEFAULT_INTERRUPT_HANDLER_NAME", "d3/da9/st_2stm32f7_2stm32f746ngh6_2oc__interrupts__defs_8h.html#a2c1a3272fe7c9fccda130aa5b2b26ebf", null ],
    [ "oC_MACHINE_INTERRUPTS_LIST", "d3/da9/st_2stm32f7_2stm32f746ngh6_2oc__interrupts__defs_8h.html#a28104929d5e5dd4f8463e32c8e44eb0a", null ],
    [ "oC_MACHINE_INTERRUPTS_TYPES_LIST", "d3/da9/st_2stm32f7_2stm32f746ngh6_2oc__interrupts__defs_8h.html#a9eca7a865a7bb798bb9dc918a901a500", null ],
    [ "oC_MACHINE_PRIO_BITS", "d3/da9/st_2stm32f7_2stm32f746ngh6_2oc__interrupts__defs_8h.html#a56417f2874a94d15b6bbb25571973476", null ],
    [ "oC_MAXIMUM_INTERRUPT_PRIORITY", "d3/da9/st_2stm32f7_2stm32f746ngh6_2oc__interrupts__defs_8h.html#a4d735fff03d5a4d4b76dd0e75935c3f9", null ],
    [ "oC_MINIMUM_INTERRUPT_PRIORITY", "d3/da9/st_2stm32f7_2stm32f746ngh6_2oc__interrupts__defs_8h.html#a6d744cdc8a4742f4af16e87532ffcfc4", null ]
];