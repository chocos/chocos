var structo_c___font_info__t =
[
    [ "CharInfo", "d3/d69/structo_c___font_info__t.html#af77b772db7b49b029f8a5ee000cd487b", null ],
    [ "Data", "d3/d69/structo_c___font_info__t.html#a0a73759b305e9e2ed8c26b4d2bd419db", null ],
    [ "EndChar", "d3/d69/structo_c___font_info__t.html#a08406dcba65642424d87dab7259c267d", null ],
    [ "FontName", "d3/d69/structo_c___font_info__t.html#a817654268417445ea73bbd4986e09531", null ],
    [ "HeightPages", "d3/d69/structo_c___font_info__t.html#ac98357d15a452d1150d50cb1a185aa88", null ],
    [ "StartChar", "d3/d69/structo_c___font_info__t.html#a1451d917c45bae74f4241cfed25e7703", null ]
];