var group___drivers =
[
    [ "Ethernet", "d3/d01/group___e_t_h.html", "d3/d01/group___e_t_h" ],
    [ "Flexible Memory Controller", "d4/dfc/group___f_m_c.html", "d4/dfc/group___f_m_c" ],
    [ "FT5336 Touch screen driver", "df/df9/group___f_t5336.html", null ],
    [ "General Purpose Input/Output driver", "dd/d94/group___g_p_i_o.html", null ],
    [ "Graphic Terminal Drivers", "d0/d50/group___g_t_d.html", "d0/d50/group___g_t_d" ],
    [ "Inter-Integrated Circuit", "d0/d5b/group___i2_c.html", null ],
    [ "LCD-TFT", "d2/dea/group___l_c_d_t_f_t.html", "d2/dea/group___l_c_d_t_f_t" ],
    [ "LED - Light Emitting Diode", "d1/d56/group___l_e_d.html", null ],
    [ "Neural Network driver", "d7/dab/group___n_e_u_n_e_t.html", null ],
    [ "Driver's Interface", "d1/da2/group___driver.html", null ],
    [ "Driver Manager (DriverMan)", "d0/d27/group___driver_man.html", "d0/d27/group___driver_man" ],
    [ "IDI - Input Driver Interface", "da/de3/group___i_d_i.html", "da/de3/group___i_d_i" ],
    [ "Pulse Width Modulation", "d4/d14/group___p_w_m.html", null ],
    [ "SD/MMC cards", "d4/d1b/group___s_d_m_m_c.html", "d4/d1b/group___s_d_m_m_c" ],
    [ "Serial Peripheral Interface", "dd/d3c/group___s_p_i.html", null ],
    [ "Timers", "d9/d3b/group___t_i_m_e_r.html", null ],
    [ "Universal Asynchronous Receiver/Transmitter driver", "d2/d48/group___u_a_r_t.html", "d2/d48/group___u_a_r_t" ],
    [ "Memory - Memory management", "d4/d3a/group___memory.html", null ]
];